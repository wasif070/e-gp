/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblEmployeeFinancialPowerDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEmployeeFinancialPower;
import java.util.List;

/**
 * <b>Class Description goes here</b>
 * $Revision: 1.1.1.1 $
 * @version <version-no.> <Date>
 * @author Administrator
 */
public class TblEmployeeFinancialPowerImpl extends AbcAbstractClass<TblEmployeeFinancialPower> 
        implements TblEmployeeFinancialPowerDao {

    @Override
    public void addEmployeeFinancialPower(TblEmployeeFinancialPower tblEmployeeFinancialPower)
    {
        super.addEntity(tblEmployeeFinancialPower);
    }

    @Override
    public void deleteEmployeeFinancialPower(TblEmployeeFinancialPower tblEmployeeFinancialPower)
    {
        super.deleteEntity(tblEmployeeFinancialPower);
    }

    @Override
    public void updateEmployeeFinancialPower(TblEmployeeFinancialPower tblEmployeeFinancialPower)
    {
        super.updateEntity(tblEmployeeFinancialPower);
    }

    @Override
    public List<TblEmployeeFinancialPower> findEmployeeFinancialPower(Object... values) throws Exception
    {
        return super.findEntity(values);
    }
}
