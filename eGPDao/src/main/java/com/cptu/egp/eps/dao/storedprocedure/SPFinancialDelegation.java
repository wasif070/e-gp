/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Istiak (Dohatec) - Jun 14, 2015
 */
public class SPFinancialDelegation extends StoredProcedure {

    private static final Logger LOGGER = Logger.getLogger(SPFinancialDelegation.class);

    public SPFinancialDelegation(BasicDataSource dataSource, String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);
        this.declareParameter(new SqlParameter("@v_fieldName1Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName2Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName3Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName4Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName5Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName6Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName7Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName8Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName9Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName10Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName11Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName12Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName13Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName14Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName15Vc", Types.VARCHAR));
    }

    /**
     * Execute stored procedure : p_financialdeligation
     * To perform Common search operation for whole application base on function name passed in fieldName1 parameter.
     * @param fieldName1 function name to perform search operation.
     * @param fieldName2
     * @param fieldName3
     * @param fieldName4
     * @param fieldName5
     * @param fieldName6
     * @param fieldName7
     * @param fieldName8
     * @param fieldName9
     * @param fieldName10
     * @param fieldName11
     * @param fieldName12
     * @param fieldName13
     * @param fieldName14
     * @param fieldName15
     * @return list will be item of Financial Deligation.
     */

    public List<FinancialDelegationData> executeProcedure(String fieldName1, String fieldName2, String fieldName3, String fieldName4, String fieldName5, String fieldName6, String fieldName7, String fieldName8, String fieldName9, String fieldName10, String fieldName11, String fieldName12, String fieldName13, String fieldName14, String fieldName15) {
        Map inParams = new HashMap();
        inParams.put("@v_fieldName1Vc", fieldName1);
        inParams.put("@v_fieldName2Vc", fieldName2);
        inParams.put("@v_fieldName3Vc", fieldName3);
        inParams.put("@v_fieldName4Vc", fieldName4);
        inParams.put("@v_fieldName5Vc", fieldName5);
        inParams.put("@v_fieldName6Vc", fieldName6);
        inParams.put("@v_fieldName7Vc", fieldName7);
        inParams.put("@v_fieldName8Vc", fieldName8);
        inParams.put("@v_fieldName9Vc", fieldName9);
        inParams.put("@v_fieldName10Vc", fieldName10);
        inParams.put("@v_fieldName11Vc", fieldName11);
        inParams.put("@v_fieldName12Vc", fieldName12);
        inParams.put("@v_fieldName13Vc", fieldName13);
        inParams.put("@v_fieldName14Vc", fieldName14);
        inParams.put("@v_fieldName15Vc", fieldName15);

        this.compile();

        List<FinancialDelegationData> searchData = new ArrayList<FinancialDelegationData>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            if(list!=null && !list.isEmpty()){
                for (LinkedHashMap<String, Object> linkedHashMap : list) {
                    FinancialDelegationData oFinancialDeligationData = new FinancialDelegationData();
                    oFinancialDeligationData.setFieldName1((String) linkedHashMap.get("FieldValue1"));
                    oFinancialDeligationData.setFieldName2((String) linkedHashMap.get("FieldValue2"));
                    oFinancialDeligationData.setFieldName3((String) linkedHashMap.get("FieldValue3"));
                    oFinancialDeligationData.setFieldName4((String) linkedHashMap.get("FieldValue4"));
                    oFinancialDeligationData.setFieldName5((String) linkedHashMap.get("FieldValue5"));
                    oFinancialDeligationData.setFieldName6((String) linkedHashMap.get("FieldValue6"));
                    oFinancialDeligationData.setFieldName7((String) linkedHashMap.get("FieldValue7"));
                    oFinancialDeligationData.setFieldName8((String) linkedHashMap.get("FieldValue8"));
                    oFinancialDeligationData.setFieldName9((String) linkedHashMap.get("FieldValue9"));
                    oFinancialDeligationData.setFieldName10((String) linkedHashMap.get("FieldValue10"));
                    oFinancialDeligationData.setFieldName11((String) linkedHashMap.get("FieldValue11"));
                    oFinancialDeligationData.setFieldName12((String) linkedHashMap.get("FieldValue12"));
                    oFinancialDeligationData.setFieldName13((String) linkedHashMap.get("FieldValue13"));
                    oFinancialDeligationData.setFieldName14((String) linkedHashMap.get("FieldValue14"));
                    oFinancialDeligationData.setFieldName15((String) linkedHashMap.get("FieldValue15"));

                    searchData.add(oFinancialDeligationData);
                }
            }
            LOGGER.debug("SPFinancialDeligation : No data found for FUNCTION  " + fieldName1);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            LOGGER.error("SPFinancialDeligation : " + sw.toString());
            System.out.println("p_financialdeligation : "+ e);
        }
        return searchData;
    }
}
