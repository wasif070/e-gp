/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblBidderRankDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblBidderRank;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblBidderRankImpl extends AbcAbstractClass<TblBidderRank> implements TblBidderRankDao{

    @Override
    public void addTblBidderRank(TblBidderRank tblBidderRank) {
        super.addEntity(tblBidderRank);
    }

    @Override
    public List<TblBidderRank> findTblBidderRank(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblBidderRank(TblBidderRank tblBidderRank) {

        super.deleteEntity(tblBidderRank);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblBidderRank(Collection tblBidderRank) {
        super.deleteAll(tblBidderRank);
    }

    @Override
    public void updateTblBidderRank(TblBidderRank tblBidderRank) {

        super.updateEntity(tblBidderRank);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblBidderRank> getAllTblBidderRank() {
        return super.getAllEntity();
    }

    @Override
    public long getTblBidderRankCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblBidderRank> findByCountTblBidderRank(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveBidderRank(List<TblBidderRank> bidderRanks) {
        super.updateAll(bidderRanks);
    }
}
