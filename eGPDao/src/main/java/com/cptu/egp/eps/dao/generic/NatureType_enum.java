/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.generic;

/**
 *
 * @author eprocure
 */
public enum NatureType_enum {
    Goods(1), Works(2), Services(3);

    private int natId;

    private NatureType_enum(int nid) {
      natId = nid;
    }

    public int getNatureId() {
      return natId;
    }
}