/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblCmsVendorReqWccDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsVendorReqWcc;
import java.util.List;

/**
 *
 * @author rikin.p
 */

public class TblCmsVendorReqWccDaoImpl extends AbcAbstractClass<TblCmsVendorReqWcc> implements TblCmsVendorReqWccDao{

    @Override
    public void addTblCmsVendorReqWcc(TblCmsVendorReqWcc tblCmsVendorReqWcc) {
        super.addEntity(tblCmsVendorReqWcc);
    }

    @Override
    public List<TblCmsVendorReqWcc> findTblCmsVendorReqWcc(Object... values) throws Exception {
        return super.findEntity(values);       
    }

    @Override
    public void deleteTblCmsVendorReqWcc(TblCmsVendorReqWcc tblCmsVendorReqWcc) {
        super.deleteEntity(tblCmsVendorReqWcc);
    }

    @Override
    public void updateTblCmsVendorReqWcc(TblCmsVendorReqWcc tblCmsVendorReqWcc) {
        super.updateEntity(tblCmsVendorReqWcc);
    }

    @Override
    public List<TblCmsVendorReqWcc> getAllTblCmsVendorReqWcc() {
        return super.getAllEntity();
    }

    @Override
    public long getTblCmsVendorReqWccCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsVendorReqWcc> findByCountTblCmsVendorReqWcc(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
