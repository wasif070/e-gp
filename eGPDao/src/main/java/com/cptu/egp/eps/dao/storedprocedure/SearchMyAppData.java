/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

/**
 *
 * @author Administrator
 */
public class SearchMyAppData {

    private Integer appId;
    private String appCode;
    private String budgetType;
    private String activityName;
    private String prjName;
    private Integer totalPage;
    private Integer totalRec;
    private Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public String getBudgetType() {
        return budgetType;
    }

    public void setBudgetType(String budgetType) {
        this.budgetType = budgetType;
    }

    public String getPrjName() {
        return prjName;
    }

    public void setPrjName(String prjName) {
        this.prjName = prjName;
    }
    
    public String getactivityName() {
        return activityName;
    }

    public void setactivityName(String activityName) {
        this.activityName = activityName;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public Integer getTotalRec() {
        return totalRec;
    }

    public void setTotalRec(Integer totalRec) {
        this.totalRec = totalRec;
    }
}
