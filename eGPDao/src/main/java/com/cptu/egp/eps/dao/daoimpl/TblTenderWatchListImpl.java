/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderWatchListDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderWatchList;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderWatchListImpl extends AbcAbstractClass<TblTenderWatchList> implements TblTenderWatchListDao {

    @Override
    public void addTblTenderWatchList(TblTenderWatchList admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblTenderWatchList(TblTenderWatchList admin) {
        super.deleteEntity(admin);        
    }

    @Override
    public void updateTblTenderWatchList(TblTenderWatchList admin) {
        super.updateEntity(admin);        
    }

    @Override
    public List<TblTenderWatchList> getAllTblTenderWatchList() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderWatchList> findTblTenderWatchList(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderWatchListCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderWatchList> findByCountTblTenderWatchList(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
