package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvWpvari;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsSrvWpVariDao extends GenericDao<TblCmsSrvWpvari> {

    public void addTblCmsSrvWpvari(TblCmsSrvWpvari event);

    public void deleteTblCmsSrvWpvari(TblCmsSrvWpvari event);

    public void updateTblCmsSrvWpvari(TblCmsSrvWpvari event);

    public List<TblCmsSrvWpvari> getAllTblCmsSrvWpvari();

    public List<TblCmsSrvWpvari> findTblCmsSrvWpvari(Object... values) throws Exception;

    public List<TblCmsSrvWpvari> findByCountTblCmsSrvWpvari(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsSrvWpvariCount();

    public void updateOrSaveEstCost(List<TblCmsSrvWpvari> estCost);
}
