/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.math.BigDecimal;
import java.util.List;
import java.util.Date;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
/**
 *
 * @author Ahsan
 */
public class SPGetTenderCellValueForIct extends StoredProcedure{
            private static final Logger LOGGER = Logger.getLogger(SPGetTenderCellValueForIct.class);


       public SPGetTenderCellValueForIct(BasicDataSource dataSource, String procName){

       this.setDataSource(dataSource);
       this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
       LOGGER.debug("Proc Name: "+ procName);

       this.declareParameter(new SqlParameter("colList", Types.VARCHAR));
       this.declareParameter(new SqlParameter("tenderFormId", Types.INTEGER));
       this.declareParameter(new SqlParameter("bidTableId", Types.VARCHAR));
       this.declareParameter(new SqlParameter("tenderTableid", Types.INTEGER));
       this.declareParameter(new SqlParameter("tenderId", Types.INTEGER));
       this.declareParameter(new SqlParameter("rowId", Types.INTEGER));

    }

    public String executeProcedure(String colList,int tenderFormId, int bidTableId, int tenderTableid,int tenderId,int rowId)
    {
        Map inParams = new HashMap();
        inParams.put("colList", colList);
        inParams.put("tenderFormId", tenderFormId);
        inParams.put("bidTableId", bidTableId);
        inParams.put("tenderTableid", tenderTableid);
        inParams.put("tenderId", tenderId);
        inParams.put("rowId", rowId);



        this.compile();
        String cellValue = "";
        try{
             ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
             for (LinkedHashMap<String, Object> linkedHashMap : list) {         
                cellValue = (String)linkedHashMap.get("ColumnValue");  
            }
        }

        catch(Exception e){
        LOGGER.error("SPGetTenderCellValueForIct : "+ e);
        }

        return cellValue;
    }


}
