/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderAdvtDetails;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderAdvtDetailsDao extends GenericDao<TblTenderAdvtDetails>{

    public void addTblTenderAdvtDetails(TblTenderAdvtDetails tblTenderAdvtDetails);

    public void deleteTblTenderAdvtDetails(TblTenderAdvtDetails tblTenderAdvtDetails);

    public void updateTblTenderAdvtDetails(TblTenderAdvtDetails tblTenderAdvtDetails);

    public List<TblTenderAdvtDetails> getAllTblTenderAdvtDetails();

    public List<TblTenderAdvtDetails> findTblTenderAdvtDetails(Object... values) throws Exception;

    public List<TblTenderAdvtDetails> findByCountTblTenderAdvtDetails(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderAdvtDetailsCount();
}
