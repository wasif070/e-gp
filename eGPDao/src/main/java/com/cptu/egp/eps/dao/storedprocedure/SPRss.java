/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;


/**
 *This is the dao for spRSS
 * @author Nishith
 */
public class SPRss extends StoredProcedure{
    private static final Logger LOGGER = Logger.getLogger(SPRss.class);
    /**
     * Get data form p_eGP_RSS
     * @param dataSource
     * @param procName
     */
    public SPRss(BasicDataSource dataSource,String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);
        this.declareParameter(new SqlParameter("@v_Tag_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_Path_inVc", Types.VARCHAR));
    }
    
    /**
     * Get data form stored Procedure: p_eGP_RSS
     * @param tag
     * @param path
     * @return List of CommonRssMsg having Rss details in XML formate.
     */
    public List<CommonRssMsg> executeSPRssProcedure(String tag,String path){
        Map inParams = new HashMap();
        inParams.put("@v_Tag_inVc", tag);
        inParams.put("@v_Path_inVc", path);
        this.compile();

        ArrayList<CommonRssMsg> details = new ArrayList<CommonRssMsg>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonRssMsg commonRssMsg = new CommonRssMsg();
                
                commonRssMsg.setRssXml((String) linkedHashMap.get("RSSXML"));
                details.add(commonRssMsg);
            }
        } catch (Exception e) {
         LOGGER.error("SPRss  : "+ e);
        }
        return details;
    }


}
