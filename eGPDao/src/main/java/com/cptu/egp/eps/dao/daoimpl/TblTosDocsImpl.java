/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTosDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTosDocs;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTosDocsImpl extends AbcAbstractClass<TblTosDocs> implements TblTosDocsDao{

    @Override
    public void addTblTosDocs(TblTosDocs tblTosDocs) {
        super.addEntity(tblTosDocs);
    }

    @Override
    public List<TblTosDocs> findTblTosDocs(Object... values) throws Exception {
        return super.findEntity(values);
        
    }

    @Override
    public void deleteTblTosDocs(TblTosDocs tblTosDocs) {

        super.deleteEntity(tblTosDocs);
       
    }

    @Override
    public void updateTblTosDocs(TblTosDocs tblTosDocs) {

        super.updateEntity(tblTosDocs);
        
    }

    @Override
    public List<TblTosDocs> getAllTblTosDocs() {
        return super.getAllEntity();
    }

    @Override
    public long getTblTosDocsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTosDocs> findByCountTblTosDocs(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
