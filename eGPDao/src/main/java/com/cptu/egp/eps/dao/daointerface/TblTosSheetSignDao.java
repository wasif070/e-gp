package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTosSheetSign;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTosSheetSignDao extends GenericDao<TblTosSheetSign> {

    public void addTblTosSheetSign(TblTosSheetSign sheetSign);

    public void deleteTblTosSheetSign(TblTosSheetSign sheetSign);

    public void updateTblTosSheetSign(TblTosSheetSign sheetSign);

    public List<TblTosSheetSign> getAllTblTosSheetSign();

    public List<TblTosSheetSign> findTblTosSheetSign(Object... values) throws Exception;

    public List<TblTosSheetSign> findByCountTblTosSheetSign(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTosSheetSignCount();
}
