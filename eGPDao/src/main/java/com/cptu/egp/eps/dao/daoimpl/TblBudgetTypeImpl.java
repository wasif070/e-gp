package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblBudgetTypeDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblBudgetType;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblBudgetTypeImpl extends AbcAbstractClass<TblBudgetType> implements TblBudgetTypeDao {

    @Override
    public void addTblBudgetType(TblBudgetType master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblBudgetType(TblBudgetType master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblBudgetType(TblBudgetType master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblBudgetType> getAllTblBudgetType() {
        return super.getAllEntity();
    }

    @Override
    public List<TblBudgetType> findTblBudgetType(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblBudgetTypeCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblBudgetType> findByCountTblBudgetType(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
