/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTemplateColumnsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTemplateColumns;
import java.util.List;

/**
 *
 * @author yanki
 */
public class TblTemplateColumnsImpl extends AbcAbstractClass<TblTemplateColumns> implements TblTemplateColumnsDao{

    @Override
    public void addTblTemplateColumns(TblTemplateColumns templateColumns) {
        super.addEntity(templateColumns);
    }

    @Override
    public void deleteTblTemplateColumns(TblTemplateColumns templateColumns) {
        super.deleteEntity(templateColumns);
    }

    @Override
    public void updateTblTemplateColumns(TblTemplateColumns templateColumns) {
        super.updateEntity(templateColumns);
    }

    @Override
    public List<TblTemplateColumns> getAllTblTemplateColumns() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTemplateColumns> findTblTemplateColumns(Object... values) throws Exception {
        return super.findEntity(values);
    }

}
