/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblNegReplyDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblNegReply;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblNegReplyImpl extends AbcAbstractClass<TblNegReply> implements TblNegReplyDao{

    @Override
    public void addTblNegReply(TblNegReply negReply) {
        super.addEntity(negReply);
    }

    @Override
    public List<TblNegReply> findTblNegReply(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblNegReply(TblNegReply department) {

        super.deleteEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblNegReply(Collection event) {
        super.deleteAll(event);
    }

    @Override
    public void updateTblNegReply(TblNegReply department) {

        super.updateEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblNegReply> getAllTblNegReply() {
        return super.getAllEntity();
    }

    @Override
    public long getTblNegReplyCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblNegReply> findByCountTblNegReply(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
