/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblWorkFlowLevelConfigHistDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblWorkFlowLevelConfigHist;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public class TblWorkFlowLevelConfigHistImpl extends AbcAbstractClass<TblWorkFlowLevelConfigHist> implements TblWorkFlowLevelConfigHistDao {

    @Override
    public void addTblWorkFlowLevelConfigHist(TblWorkFlowLevelConfigHist workFlowLevelConfigHist) {

        super.addEntity(workFlowLevelConfigHist);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblWorkFlowLevelConfigHist(TblWorkFlowLevelConfigHist workFlowLevelConfigHist) {

        super.deleteEntity(workFlowLevelConfigHist);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblWorkFlowLevelConfigHist(TblWorkFlowLevelConfigHist workFlowLevelConfigHist) {

        super.updateEntity(workFlowLevelConfigHist);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblWorkFlowLevelConfigHist> getAllTblWorkFlowLevelConfigHist() {
        return super.getAllEntity();
    }

    @Override
    public List<TblWorkFlowLevelConfigHist> findTblWorkFlowLevelConfigHist(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblWorkFlowLevelConfigHistCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblWorkFlowLevelConfigHist> findByCountTblWorkFlowLevelConfigHist(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
