/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderCoriGrandSumDetail;
import java.util.List;

/**
 *
 * @author dipal.Shah
 */

public interface TblTenderCoriGrandSumDetailDao extends GenericDao<TblTenderCoriGrandSumDetail>{

    public void addTblTenderCoriGrandSumDetail(TblTenderCoriGrandSumDetail tblTenderCoriGrandSumDetail);

    public void deleteTblTenderCoriGrandSumDetail(TblTenderCoriGrandSumDetail tblTenderCoriGrandSumDetail);

    public void updateTblTenderCoriGrandSumDetail(TblTenderCoriGrandSumDetail tblTenderCoriGrandSumDetail);

    public List<TblTenderCoriGrandSumDetail> getAllTblTenderCoriGrandSumDetail();

    public List<TblTenderCoriGrandSumDetail> findTblTenderCoriGrandSumDetail(Object... values) throws Exception;
    
    public void updateSaveAllTenderCoriGrandSumDetail(List<TblTenderCoriGrandSumDetail> lstTenderGrandSumDetail);
    
}
