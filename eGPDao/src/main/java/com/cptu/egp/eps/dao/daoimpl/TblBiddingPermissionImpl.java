/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblBiddingPermissionDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblBiddingPermission;
import java.util.List;

public class TblBiddingPermissionImpl extends AbcAbstractClass<TblBiddingPermission> implements TblBiddingPermissionDao{
    @Override
    public void addTblBiddingPermission(TblBiddingPermission master){
            super.addEntity(master);
    }
    @Override
    public void deleteTblBiddingPermission(TblBiddingPermission master) {
            super.deleteEntity(master);
    }
    @Override
    public void updateTblBiddingPermission(TblBiddingPermission master) {
            super.updateEntity(master);
    }
    @Override
    public List<TblBiddingPermission> getAllTblBiddingPermission() {
            return super.getAllEntity();
    }
    @Override
    public List<TblBiddingPermission> findTblBiddingPermission(Object... values) throws Exception {
            return super.findEntity(values);
    }
    @Override
    public long getTblBiddingPermissionCount() {
            return super.getEntityCount();
    }
    @Override
    public List<TblBiddingPermission> findByCountTblBiddingPermission(int firstResult, int maxResult, Object... values) throws Exception {
            return super.findByCountEntity(firstResult, maxResult, values);
    }
    @Override
    public void updateOrSaveTblBiddingPermission(List<TblBiddingPermission> tblObj) {
            super.updateAll(tblObj);
    }
}
