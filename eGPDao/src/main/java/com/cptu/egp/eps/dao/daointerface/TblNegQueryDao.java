package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblNegQuery;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblNegQueryDao extends GenericDao<TblNegQuery> {

    public void addTblNegQuery(TblNegQuery tblNegQuery);

    public void deleteTblNegQuery(TblNegQuery tblNegQuery);

    public void deleteAllTblNegQuery(Collection tblNegQuery);

    public void updateTblNegQuery(TblNegQuery tblNegQuery);

    public List<TblNegQuery> getAllTblNegQuery();

    public List<TblNegQuery> findTblNegQuery(Object... values) throws Exception;

    public List<TblNegQuery> findByCountTblNegQuery(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblNegQueryCount();
}
