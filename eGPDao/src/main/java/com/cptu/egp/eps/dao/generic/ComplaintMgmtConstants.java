/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.generic;

/**
 *
 * @author RadhaRani
 */
public interface ComplaintMgmtConstants{
	
			// For Complaint management  
			public static final String COMP_STATUS_PENDING="Pending";
			public static final String COMP_STATUS_ACCEPT="Accept";
			public static final String COMP_STATUS_REJECT="Reject";
			public static final String COMP_STATUS_NEED_CLARIFY="Clarification";
			
			// For Complaint process payments
			public static final String COMP_PAY_STATUS_PENDING="Pending";
			public static final String COMP_PAY_STATUS_PAID="paid";
			public static final String COMP_PAY_STATUS_FORFEITED="forfeited";
			public static final String COMP_PAY_MODE_OFFLINE="offline";
			public static final String COMP_PAY_MODE_BANK="bank";
			
			public static final String COMPLAINT_PAYMENT_INST_TYPE_PO="Pay Order";
			public static final String COMPLAINT_PAYMENT_INST_TYPE_BG="Bank Guarantee";
			public static final String PAYMENT_INST_TYPE="payment Inst Tye";
			public static final String COMP_PAY_INST_TYPE_CASH="Cash";
			public static final String COMP_PAY_INST_TYPE_DD="DD";
			
			public static final String COMP_PAY_FOR_REGISTRATION="Registraton Fee";
			public static final String COMP_PAY_FOR_SECURITY="Security Deposit Fee";
			public static final String COMP_PAY_VERIFIED="Yes";
			public static final String COMP_PAY_NOT_VERIFIED="No";
			public static final String COMP_PAY_LIVE="Yes";
			public static final String COMP_PAY_NO_LIVE="No";
			
			//For Complaints
			
			public static final String UPLOAD_PATH="D:/uploadDocs/";
			
			//for reviewPanel creation
			public static final String REGISTRATION_TYPE="Review Panel Member";
			public static final String NO="No";
			public static final String COUNTRY_NAME="Bangladesh";
			public static final String NEXT_SCREEN="ChangePassword";
			public static final String YES="Yes";
			public static final String APPROVED="Approved";
			public static final String NATIONALITY="bangladeshi";
			public static final String FIN_POWER_BY="user";
			public static final String ACTION="create";
			public static final String REVIEW_PANEL_ROLE="Review Panel";
			
			//FOR MAIL
			public static final String MAIL_TEXT="test mail";
			public static final String MAIL_SUBJECT="eGP: new complaint registration";
			
			
			//FOR MESSAGE
			public static final String PRIORITY="Low";
			public static final String MSG_TEXT="Message for Register";
			public static final String MSF_BOX_TYPE="Inbox";
			public static final String MSG_STATUS="Live";
			
			
		}
