package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblLocationMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblLocationMasterDao extends GenericDao<TblLocationMaster> {

    public void addTblLocationMaster(TblLocationMaster tblObj);

    public void deleteTblLocationMaster(TblLocationMaster tblObj);

    public void updateTblLocationMaster(TblLocationMaster tblObj);

    public List<TblLocationMaster> getAllTblLocationMaster();

    public List<TblLocationMaster> findTblLocationMaster(Object... values) throws Exception;

    public List<TblLocationMaster> findByCountTblLocationMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblLocationMasterCount();
}
