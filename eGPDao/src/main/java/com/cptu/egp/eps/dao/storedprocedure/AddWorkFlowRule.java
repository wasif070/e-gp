/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Ramesh.Janagondakuru
 */
public class AddWorkFlowRule extends StoredProcedure{
    private static final Logger LOGGER = Logger.getLogger(AddWorkFlowRule.class);
    public AddWorkFlowRule(BasicDataSource dataSource,String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("v_initiatorprocurementroleidN", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_approverprocurementroleidN", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_initiatorwfroleidN", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_approverwfroleidN", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_ispublishdaterequiredVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_eventidN", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_actionVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_actionbyN", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_actiondated", Types.DATE));
        this.declareParameter(new SqlParameter("v_wfinitruleidN", Types.INTEGER));
    }

    /**
     * Execute stored procedure : p_add_upd_workflowruleengine.
     * to performed insert,update,delete work flow rules Engine.
     * @param initiatorprocurementroleid
     * @param approverprocurementroleid
     * @param initiatorwfroleid
     * @param approverwfroleid
     * @param ispublishdaterequired
     * @param eventid
     * @param action
     * @param actionby
     * @param actiondate
     * @param wfinitruleid
     * @return CommonMsgChk list contains flags indicate operation success status.
     */
    public List<CommonMsgChk> exeProcedure(Integer initiatorprocurementroleid,Integer approverprocurementroleid,String initiatorwfroleid,String approverwfroleid,String  ispublishdaterequired,Integer eventid,String action,Integer actionby,Date actiondate,Integer wfinitruleid ){
         Map inParams = new HashMap();
          inParams.put("v_initiatorprocurementroleidN",initiatorprocurementroleid);
           inParams.put("v_approverprocurementroleidN",approverprocurementroleid);
           inParams.put("v_initiatorwfroleidN",initiatorwfroleid);
           inParams.put("v_approverwfroleidN",approverwfroleid);
           inParams.put("v_ispublishdaterequiredVc",ispublishdaterequired);
           inParams.put("v_eventidN",eventid);
           inParams.put("v_actionVc",action);
           inParams.put("v_actionbyN",actionby);
           inParams.put("v_actiondated",actiondate);
           inParams.put("v_wfinitruleidN",wfinitruleid);
           this.compile();
          List<CommonMsgChk> details = new ArrayList<CommonMsgChk>();
           try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonMsgChk commonMsgChk = new CommonMsgChk();
                commonMsgChk.setFlag((Boolean) linkedHashMap.get("flag"));
                commonMsgChk.setMsg((String) linkedHashMap.get("msg"));
                details.add(commonMsgChk);
            }
        } catch (Exception e) {
            LOGGER.error("AddWorkFlowRule : " + e);
        }

        return details;
    }


}
