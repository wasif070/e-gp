/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblCmsWcCertificateDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsWcCertificate;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public class TblCmsWcCertificateDaoImpl extends AbcAbstractClass<TblCmsWcCertificate> implements TblCmsWcCertificateDao {

    @Override
    public void addTblCmsWcCertificate(TblCmsWcCertificate tblCmsWcCertificate) {
        super.addEntity(tblCmsWcCertificate);
    }

    @Override
    public void deleteTblCmsWcCertificate(TblCmsWcCertificate tblCmsWcCertificate) {
        super.deleteEntity(tblCmsWcCertificate);
    }

    @Override
    public void updateTblCmsWcCertificate(TblCmsWcCertificate tblCmsWcCertificate) {
        super.updateEntity(tblCmsWcCertificate);
    }

    @Override
    public List<TblCmsWcCertificate> getAllTblCmsWcCertificate() {
        super.setPersistentClass(TblCmsWcCertificate.class);
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsWcCertificate> findTblCmsWcCertificate(Object... values) throws Exception {
        super.setPersistentClass(TblCmsWcCertificate.class);
        return super.findEntity(values);
    }

    @Override
    public List<TblCmsWcCertificate> findByCountTblCmsWcCertificate(int firstResult, int maxResult, Object... values) throws Exception {
        super.setPersistentClass(TblCmsWcCertificate.class);
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblCmsWcCertificateCount() {
        super.setPersistentClass(TblCmsWcCertificate.class);
        return super.getEntityCount();
    }
}
