package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblStateMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblStateMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblStateMasterImpl extends AbcAbstractClass<TblStateMaster> implements TblStateMasterDao {
   
    @Override
    public List<TblStateMaster> getAllTblStateMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblStateMaster> findTblStateMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblStateMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblStateMaster> findByCountTblStateMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
