package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblActivityMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblActivityMasterDao extends GenericDao<TblActivityMaster> {

    public void addTblActivityMaster(TblActivityMaster tblObj);

    public void deleteTblActivityMaster(TblActivityMaster tblObj);

    public void updateTblActivityMaster(TblActivityMaster tblObj);

    public List<TblActivityMaster> getAllTblActivityMaster();

    public List<TblActivityMaster> findTblActivityMaster(Object... values) throws Exception;

    public List<TblActivityMaster> findByCountTblActivityMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblActivityMasterCount();
}
