package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsInvoiceDocumentDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsInvoiceDocument;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsInvoiceDocumentImpl extends AbcAbstractClass<TblCmsInvoiceDocument> implements TblCmsInvoiceDocumentDao {

    @Override
    public void addTblCmsInvoiceDocument(TblCmsInvoiceDocument cmsInvoiceDocument){
        super.addEntity(cmsInvoiceDocument);
    }

    @Override
    public void deleteTblCmsInvoiceDocument(TblCmsInvoiceDocument cmsInvoiceDocument) {
        super.deleteEntity(cmsInvoiceDocument);
    }

    @Override
    public void updateTblCmsInvoiceDocument(TblCmsInvoiceDocument cmsInvoiceDocument) {
        super.updateEntity(cmsInvoiceDocument);
    }

    @Override
    public List<TblCmsInvoiceDocument> getAllTblCmsInvoiceDocument() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsInvoiceDocument> findTblCmsInvoiceDocument(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsInvoiceDocumentCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsInvoiceDocument> findByCountTblCmsInvoiceDocument(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
