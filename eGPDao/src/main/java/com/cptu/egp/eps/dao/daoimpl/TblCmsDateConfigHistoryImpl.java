package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsDateConfigHistoryDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsDateConfigHistory;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsDateConfigHistoryImpl extends AbcAbstractClass<TblCmsDateConfigHistory> implements TblCmsDateConfigHistoryDao {

    @Override
    public void addTblCmsDateConfigHistory(TblCmsDateConfigHistory master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblCmsDateConfigHistory(TblCmsDateConfigHistory master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblCmsDateConfigHistory(TblCmsDateConfigHistory master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblCmsDateConfigHistory> getAllTblCmsDateConfigHistory() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsDateConfigHistory> findTblCmsDateConfigHistory(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsDateConfigHistoryCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsDateConfigHistory> findByCountTblCmsDateConfigHistory(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
