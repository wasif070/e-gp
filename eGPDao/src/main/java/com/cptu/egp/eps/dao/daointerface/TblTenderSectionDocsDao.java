/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderSectionDocs;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderSectionDocsDao extends GenericDao<TblTenderSectionDocs>{

    public void addTblTenderSectionDocs(TblTenderSectionDocs tblObj);

    public void deleteTblTenderSectionDocs(TblTenderSectionDocs tblObj);

    public void updateTblTenderSectionDocs(TblTenderSectionDocs tblObj);

    public List<TblTenderSectionDocs> getAllTblTenderSectionDocs();

    public List<TblTenderSectionDocs> findTblTenderSectionDocs(Object... values) throws Exception;

    public List<TblTenderSectionDocs> findByCountTblTenderSectionDocs(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderSectionDocsCount();
}
