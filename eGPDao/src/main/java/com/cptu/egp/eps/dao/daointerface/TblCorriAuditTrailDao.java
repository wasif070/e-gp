package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCorriAuditTrail;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCorriAuditTrailDao extends GenericDao<TblCorriAuditTrail> {

    public void addTblCorriAuditTrail(TblCorriAuditTrail tblObj);

    public void deleteTblCorriAuditTrail(TblCorriAuditTrail tblObj);

    public void updateTblCorriAuditTrail(TblCorriAuditTrail tblObj);

    public List<TblCorriAuditTrail> getAllTblCorriAuditTrail();

    public List<TblCorriAuditTrail> findTblCorriAuditTrail(Object... values) throws Exception;

    public List<TblCorriAuditTrail> findByCountTblCorriAuditTrail(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCorriAuditTrailCount();
}
