package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblReportLots;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblReportLotsDao extends GenericDao<TblReportLots> {

    public void addTblReportLots(TblReportLots tblObj);

    public void deleteTblReportLots(TblReportLots tblObj);

    public void updateTblReportLots(TblReportLots tblObj);

    public List<TblReportLots> getAllTblReportLots();

    public List<TblReportLots> findTblReportLots(Object... values) throws Exception;

    public List<TblReportLots> findByCountTblReportLots(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblReportLotsCount();

    public void updateInsertAllReportLots(List<TblReportLots> list);
}
