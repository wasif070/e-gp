package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblComplaintHistory;
import java.util.List;

public interface TblComplaintHistoryDao  extends GenericDao<TblComplaintHistory> {

    public void addHistory(TblComplaintHistory complaintHistory);

    public List<Object[]> getComplaintHistory(int id,int complaintLevelId,int userTypeId);
    
   // public void deleteTblComplaintHistory(TblComplaintHistory complaintHistory) throws DataAccessException;

   // public void updateTblComplaintHistory(TblComplaintHistory complaintHistory);

   // public List<TblComplaintHistory> getAllComplaintHistories();
    
    
    public List<Object[]> getComplaintHistoryDetails(int id,int complaintLevelId) throws Exception;
    
    public List<Object[]> getComplaintHistoryPe(int tenderId) throws Exception;
    public List<Object[]> getComplaintHistoryRP(int userId) throws Exception;
}