package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblNegReply;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblNegReplyDao extends GenericDao<TblNegReply> {

    public void addTblNegReply(TblNegReply tblNegReply);

    public void deleteTblNegReply(TblNegReply tblNegReply);

    public void deleteAllTblNegReply(Collection tblNegReply);

    public void updateTblNegReply(TblNegReply tblNegReply);

    public List<TblNegReply> getAllTblNegReply();

    public List<TblNegReply> findTblNegReply(Object... values) throws Exception;

    public List<TblNegReply> findByCountTblNegReply(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblNegReplyCount();
}
