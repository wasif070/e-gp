package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEmailVerificationCode;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblEmailVerificationCodeDao extends GenericDao<TblEmailVerificationCode> {

    public void addTblEmailVerificationCode(TblEmailVerificationCode tblObj);

    public void deleteTblEmailVerificationCode(TblEmailVerificationCode tblObj);

    public void updateTblEmailVerificationCode(TblEmailVerificationCode tblObj);

    public List<TblEmailVerificationCode> getAllTblEmailVerificationCode();

    public List<TblEmailVerificationCode> findTblEmailVerificationCode(Object... values) throws Exception;

    public List<TblEmailVerificationCode> findByCountTblEmailVerificationCode(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEmailVerificationCodeCount();
}
