/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblPccSubClauseDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblPccSubClause;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblPccSubClauseImpl extends AbcAbstractClass<TblPccSubClause> implements TblPccSubClauseDao{

    @Override
    public void addTblPccSubClause(TblPccSubClause tblPccSubClause) {
        super.addEntity(tblPccSubClause);
    }

    @Override
    public List<TblPccSubClause> findTblPccSubClause(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblPccSubClause(TblPccSubClause tblPccSubClause) {

        super.deleteEntity(tblPccSubClause);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblPccSubClause(TblPccSubClause tblPccSubClause) {

        super.updateEntity(tblPccSubClause);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblPccSubClause> getAllTblPccSubClause() {
        return super.getAllEntity();
    }

    @Override
    public long getTblPccSubClauseCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblPccSubClause> findByCountTblPccSubClause(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
