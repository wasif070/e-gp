/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblCmsCTReasonTypeDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsCTReasonType;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public class TblCmsCTReasonTypeDaoImpl extends AbcAbstractClass<TblCmsCTReasonType> implements TblCmsCTReasonTypeDao {

    @Override
    public void addTblCmsCTReasonType(TblCmsCTReasonType tblCmsCTReasonType) {
        super.addEntity(tblCmsCTReasonType);
    }

    @Override
    public void deleteTblCmsCTReasonType(TblCmsCTReasonType tblCmsCTReasonType) {
        super.deleteEntity(tblCmsCTReasonType);
    }

    @Override
    public void updateTblCmsCTReasonType(TblCmsCTReasonType tblCmsCTReasonType) {
        super.updateEntity(tblCmsCTReasonType);
    }

    @Override
    public List<TblCmsCTReasonType> getAllTblCmsCTReasonType() {
        super.setPersistentClass(TblCmsCTReasonType.class);
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsCTReasonType> findTblCmsCTReasonType(Object... values) throws Exception {
        super.setPersistentClass(TblCmsCTReasonType.class);
        return super.findEntity(values);
    }

    @Override
    public List<TblCmsCTReasonType> findByCountTblCmsCTReasonType(int firstResult, int maxResult, Object... values) throws Exception {
        super.setPersistentClass(TblCmsCTReasonType.class);
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblCmsCTReasonTypeCount() {
        super.setPersistentClass(TblCmsCTReasonType.class);
        return super.getEntityCount();
    }
}
