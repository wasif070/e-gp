/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblPromisIndicatorDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblPromisIndicator;
import java.util.List;

/**
 *
 * @author Sudhir Chavhan
 */
public class TblPromisIndicatorImpl extends AbcAbstractClass<TblPromisIndicator> implements TblPromisIndicatorDao {

    @Override
    public void addTblPromisIndicator(TblPromisIndicator admin) {
        super.addEntity(admin);
        //  throw new UnsupportedOperationException("Not supported yet.");
    }

    /*
    @Override
    public void deleteTblPromisIndicator(TblPromisIndicator admin) {
        super.deleteEntity(admin);
      //  throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblPromisIndicator(TblPromisIndicator admin) {
         super.updateEntity(admin);
        // throw new UnsupportedOperationException("Not supported yet.");
    }
*/
    @Override
    public List<TblPromisIndicator> getAllTblPromisIndicator() {
         return super.getAllEntity();
        // throw new UnsupportedOperationException("Not supported yet.");
    }
/*
    @Override
    public List<TblPromisIndicator> findTblPromisIndicator(Object... values) throws Exception {
         return super.findEntity(values);
        // throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblPromisIndicator> findByCountTblPromisIndicator(int firstResult, int maxResult, Object... values) throws Exception {
         return super.findByCountEntity(firstResult, maxResult, values);
        // throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public long getTblPromisIndicatorCount() {
         return super.getEntityCount();
        // throw new UnsupportedOperationException("Not supported yet.");
    }
*/
}
