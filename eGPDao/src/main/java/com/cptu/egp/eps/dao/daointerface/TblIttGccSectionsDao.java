package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblIttGccSections;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblIttGccSectionsDao extends GenericDao<TblIttGccSections> {

    public void addTblIttGccSections(TblIttGccSections tblObj);

    public void deleteTblIttGccSections(TblIttGccSections tblObj);

    public void updateTblIttGccSections(TblIttGccSections tblObj);

    public List<TblIttGccSections> getAllTblIttGccSections();

    public List<TblIttGccSections> findTblIttGccSections(Object... values) throws Exception;

    public List<TblIttGccSections> findByCountTblIttGccSections(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblIttGccSectionsCount();
}
