/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTemplateFormulas;
import java.util.List;

/**
 *
 * @author yanki
 */
public interface TblTemplateFormulasDao extends GenericDao<TblTemplateFormulas>{

    public void addTblTemplateFormulas(TblTemplateFormulas templateFormulas);

    public void deleteTblTemplateFormulas(TblTemplateFormulas templateFormulas);

    public void updateTblTemplateFormulas(TblTemplateFormulas templateFormulas);

    public List<TblTemplateFormulas> getAllTblTemplateFormulas();

    public List<TblTemplateFormulas> findTblTemplateFormulas(Object... values) throws Exception;

}
