package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalServiceWeightage;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblEvalServiceWeightageDao extends GenericDao<TblEvalServiceWeightage> {

    public void addTblEvalServiceWeightage(TblEvalServiceWeightage tblObj);

    public void deleteTblEvalServiceWeightage(TblEvalServiceWeightage tblObj);

    public void updateTblEvalServiceWeightage(TblEvalServiceWeightage tblObj);

    public List<TblEvalServiceWeightage> getAllTblEvalServiceWeightage();

    public List<TblEvalServiceWeightage> findTblEvalServiceWeightage(Object... values) throws Exception;

    public List<TblEvalServiceWeightage> findByCountTblEvalServiceWeightage(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalServiceWeightageCount();
}
