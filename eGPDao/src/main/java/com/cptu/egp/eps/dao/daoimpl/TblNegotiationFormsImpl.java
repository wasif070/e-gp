package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblNegotiationFormsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblNegotiationForms;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblNegotiationFormsImpl extends AbcAbstractClass<TblNegotiationForms> implements TblNegotiationFormsDao {

    @Override
    public void addTblNegotiationForms(TblNegotiationForms negotiationForms){
        super.addEntity(negotiationForms);
    }

    @Override
    public void deleteTblNegotiationForms(TblNegotiationForms negotiationForms) {
        super.deleteEntity(negotiationForms);
    }

    @Override
    public void updateTblNegotiationForms(TblNegotiationForms negotiationForms) {
        super.updateEntity(negotiationForms);
    }

    @Override
    public List<TblNegotiationForms> getAllTblNegotiationForms() {
        return super.getAllEntity();
    }

    @Override
    public List<TblNegotiationForms> findTblNegotiationForms(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblNegotiationFormsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblNegotiationForms> findByCountTblNegotiationForms(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
