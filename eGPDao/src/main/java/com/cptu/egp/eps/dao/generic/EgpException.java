package com.cptu.egp.eps.dao.generic;
import org.apache.log4j.Logger;

public class EgpException extends Exception {
	 final static Logger log = Logger.getLogger(EgpException.class);
   public EgpException(){
      super();
      log.error("EXCEPTION :"+ super.getClass().getName());
   }
   public EgpException(String msg){
      super(msg);
      log.error("EXCEPTION MESSAGE:"+msg);
   }
   public EgpException(String msg, String printStack){
	      super(msg);
	      log.error("EXCEPTION IN:"+ msg);
	      log.error("EXCEPTION STACK :"+ printStack);
	   }
   public void getEgpMessage(String msg, String printStack){
	   
	   log.error("EXCEPTION IN:"+ msg);
	      log.error("EXCEPTION STACK :"+ printStack);
   }
}

