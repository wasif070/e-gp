/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.util.Date;
import java.text.*;

/**
 *
 * @author RadhaRani
 */
public class SearchComplaintPaymentBean {
    private int complaintId;
    private String complaintSubject;
    private int tenderId;
    private String emailId;
    private int createdBy;
    private String status;
    private String isLive;
    private String isVerified;
    private String createdDate;
    private String dtOfAction;
    private String paymentFor;
    private String paymentStatus;
    private int complaintPaymentId;
    
	public int getComplaintId() {
		return complaintId;
	}
	public void setComplaintId(int complaintId) {
		this.complaintId = complaintId;
	}
	
	public int getComplaintPaymentId() {
		return complaintPaymentId;
	}
	public void setComplaintPaymentId(int complaintPaymentId) {
		this.complaintPaymentId = complaintPaymentId;
	}
	public String getComplaintSubject() {
		return complaintSubject;
	}
	public void setComplaintSubject(String complaintSubject) {
		this.complaintSubject = complaintSubject;
	}
	public int getTenderId() {
		return tenderId;
	}
	public void setTenderId(int tenderId) {
		this.tenderId = tenderId;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public int getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIsLive() {
		return isLive;
	}
	public void setIsLive(String isLive) {
		this.isLive = isLive;
	}
	public String getIsVerified() {
		return isVerified;
	}
	public void setIsVerified(String isVerified) {
		this.isVerified = isVerified;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getDtOfAction() {
		return dtOfAction;
	}
	public void setDtOfAction(String dtOfAction) {
			
		this.dtOfAction = dtOfAction;
	}
	public String getPaymentFor() {
		return paymentFor;
	}
	public void setPaymentFor(String paymentFor) {
		this.paymentFor = paymentFor;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	
   }
