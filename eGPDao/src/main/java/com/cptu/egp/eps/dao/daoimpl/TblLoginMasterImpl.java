package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblLoginMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblLoginMasterImpl extends AbcAbstractClass<TblLoginMaster> implements TblLoginMasterDao {

    @Override
    public void addTblLoginMaster(TblLoginMaster master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblLoginMaster(TblLoginMaster master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblLoginMaster(TblLoginMaster master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblLoginMaster> getAllTblLoginMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblLoginMaster> findTblLoginMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblLoginMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblLoginMaster> findByCountTblLoginMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
