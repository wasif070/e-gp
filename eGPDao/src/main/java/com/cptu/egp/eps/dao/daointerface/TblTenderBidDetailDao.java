/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderBidDetail;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderBidDetailDao extends GenericDao<TblTenderBidDetail>{

    public void addTblTenderBidDetail(TblTenderBidDetail tblObj);

    public void deleteTblTenderBidDetail(TblTenderBidDetail tblObj);

    public void updateTblTenderBidDetail(TblTenderBidDetail tblObj);

    public List<TblTenderBidDetail> getAllTblTenderBidDetail();

    public List<TblTenderBidDetail> findTblTenderBidDetail(Object... values) throws Exception;

    public List<TblTenderBidDetail> findByCountTblTenderBidDetail(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderBidDetailCount();
}
