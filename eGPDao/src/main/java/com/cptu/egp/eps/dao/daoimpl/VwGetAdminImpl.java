package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.VwGetAdminDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.view.VwGetAdmin;
import java.util.List;

/**
 *
 * @author taher
 */
public class VwGetAdminImpl extends AbcAbstractClass<VwGetAdmin> implements VwGetAdminDao {

    @Override
    public List<VwGetAdmin> getAllVwGetAdmin() {
        return super.getAllEntity();
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<VwGetAdmin> findVwGetAdmin(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<VwGetAdmin> findByVwGetAdmin(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }
}
