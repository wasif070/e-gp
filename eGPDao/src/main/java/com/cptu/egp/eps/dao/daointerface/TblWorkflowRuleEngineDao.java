/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblWorkflowRuleEngine;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public interface TblWorkflowRuleEngineDao extends GenericDao<TblWorkflowRuleEngine> {

    public void addTblWorkflowRuleEngine(TblWorkflowRuleEngine admin);

    public void deleteTblWorkflowRuleEngine(TblWorkflowRuleEngine admin);

    public void updateTblWorkflowRuleEngine(TblWorkflowRuleEngine admin);

    public List<TblWorkflowRuleEngine> getAllTblWorkflowRuleEngine();

    public List<TblWorkflowRuleEngine> findTblWorkflowRuleEngine(Object... values) throws Exception;

    public List<TblWorkflowRuleEngine> findByCountTblWorkflowRuleEngine(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblWorkflowRuleEngineCount();
}
