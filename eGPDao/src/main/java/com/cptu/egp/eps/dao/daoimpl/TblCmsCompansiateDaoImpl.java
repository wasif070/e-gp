package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsCompansiateDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsCompansiateAmt;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsCompansiateDaoImpl extends AbcAbstractClass<TblCmsCompansiateAmt> implements TblCmsCompansiateDao {

    @Override
    public void addTblCmsCompansiateAmt(TblCmsCompansiateAmt tblObj) {
        super.addEntity(tblObj);
    }

    @Override
    public void deleteTblCmsCompansiateAmt(TblCmsCompansiateAmt tblObj) {
        super.deleteEntity(tblObj);
    }

    @Override
    public void updateTblCmsCompansiateAmt(TblCmsCompansiateAmt tblObj) {
        super.updateEntity(tblObj);
    }

    @Override
    public List<TblCmsCompansiateAmt> getAllTblCmsCompansiateAmt() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsCompansiateAmt> findTblCmsCompansiateAmt(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public List<TblCmsCompansiateAmt> findByCountTblCmsCompansiateAmt(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblCmsCompansiateAmtCount() {
        return super.getEntityCount();
    }

    
    
}
