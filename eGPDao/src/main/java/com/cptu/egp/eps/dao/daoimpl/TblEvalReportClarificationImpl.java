package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblEvalReportClarificationDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalReportClarification;
import java.util.List;

/**
 *
 * @author rishita
 */
public class TblEvalReportClarificationImpl extends AbcAbstractClass<TblEvalReportClarification> implements TblEvalReportClarificationDao {

    @Override
    public void addTblEvalReportClarification(TblEvalReportClarification tblEvalReportClarification){
        super.addEntity(tblEvalReportClarification);
    }

    @Override
    public void deleteTblEvalReportClarification(TblEvalReportClarification tblEvalReportClarification) {
        super.deleteEntity(tblEvalReportClarification);
    }

    @Override
    public void updateTblEvalReportClarification(TblEvalReportClarification tblEvalReportClarification) {
        super.updateEntity(tblEvalReportClarification);
    }

    @Override
    public List<TblEvalReportClarification> getAllTblEvalReportClarification() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEvalReportClarification> findTblEvalReportClarification(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEvalReportClarificationCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalReportClarification> findByCountTblEvalReportClarification(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
