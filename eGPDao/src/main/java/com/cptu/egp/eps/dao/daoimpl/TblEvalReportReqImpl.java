/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblEvalReportReqDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalReportReq;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblEvalReportReqImpl extends AbcAbstractClass<TblEvalReportReq> implements TblEvalReportReqDao{

    @Override
    public void addTblEvalReportReq(TblEvalReportReq evalReportReq) {
        super.addEntity(evalReportReq);
    }

    @Override
    public List<TblEvalReportReq> findTblEvalReportReq(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblEvalReportReq(TblEvalReportReq department) {

        super.deleteEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblEvalReportReq(Collection event) {
        super.deleteAll(event);
    }

    @Override
    public void updateTblEvalReportReq(TblEvalReportReq department) {

        super.updateEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblEvalReportReq> getAllTblEvalReportReq() {
        return super.getAllEntity();
    }

    @Override
    public long getTblEvalReportReqCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalReportReq> findByCountTblEvalReportReq(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
