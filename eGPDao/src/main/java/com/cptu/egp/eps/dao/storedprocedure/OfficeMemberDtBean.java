/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

/**
 *
 * @author Administrator
 */
public class OfficeMemberDtBean {

    private int userid;
    private String empName;
    private String desgName;
    private String procureRole;
    private String govUserId;

    public String getGovUserId() {
        return govUserId;
    }

    public void setGovUserId(String govUserId) {
        this.govUserId = govUserId;
    }

    public String getProcureRole() {
        return procureRole;
    }

    public void setProcureRole(String procureRole) {
        this.procureRole = procureRole;
    }

    public String getDesgName() {
        return desgName;
    }

    public void setDesgName(String desgName) {
        this.desgName = desgName;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }
}
