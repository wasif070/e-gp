package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblNoaAcceptance;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblNoaAcceptanceDao extends GenericDao<TblNoaAcceptance> {

    public void addTblNoaAcceptance(TblNoaAcceptance tblNoaAcceptance);

    public void deleteTblNoaAcceptance(TblNoaAcceptance tblNoaAcceptance);

    public void deleteAllTblNoaAcceptance(Collection tblNoaAcceptance);

    public void updateTblNoaAcceptance(TblNoaAcceptance tblNoaAcceptance);

    public List<TblNoaAcceptance> getAllTblNoaAcceptance();

    public List<TblNoaAcceptance> findTblNoaAcceptance(Object... values) throws Exception;

    public List<TblNoaAcceptance> findByCountTblNoaAcceptance(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblNoaAcceptanceCount();
}
