package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsRomap;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsRomapDao extends GenericDao<TblCmsRomap> {

    public void addTblCmsRomap(TblCmsRomap tblObj);

    public void deleteTblCmsRomap(TblCmsRomap tblObj);

    public void updateTblCmsRomap(TblCmsRomap tblObj);

    public List<TblCmsRomap> getAllTblCmsRomap();

    public List<TblCmsRomap> findTblCmsRomap(Object... values) throws Exception;

    public List<TblCmsRomap> findByCountTblCmsRomap(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsRomapCount();
    
    public void updateInsertAllTblCmsRomap(List<TblCmsRomap> list);
}
