/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;
import com.cptu.egp.eps.dao.generic.GenericDao;
import java.util.List;
import com.cptu.egp.eps.model.table.TblTenderFrameWork;
/**
 *
 * @author HP
 */
public interface TblTenderFrameWorkDao extends GenericDao<TblTenderFrameWork> {
    public void addTblTenderFrameWork(TblTenderFrameWork tblObj);

    public void deleteTblTenderFrameWork(TblTenderFrameWork tblObj);

    public void updateTblTenderFrameWork(TblTenderFrameWork tblObj);

    public List<TblTenderFrameWork> getAllTblTenderFrameWork();

    public List<TblTenderFrameWork> findTblTenderFrameWork(Object... values) throws Exception;

    public List<TblTenderFrameWork> findByCountTblTenderFrameWork(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblTenderFrameWorkCount();

    public void updateOrSaveTblTenderFrameWork(List<TblTenderFrameWork> tblObj);
}
