/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderWatchList;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderWatchListDao extends GenericDao<TblTenderWatchList>{

    public void addTblTenderWatchList(TblTenderWatchList tblObj);

    public void deleteTblTenderWatchList(TblTenderWatchList tblObj);

    public void updateTblTenderWatchList(TblTenderWatchList tblObj);

    public List<TblTenderWatchList> getAllTblTenderWatchList();

    public List<TblTenderWatchList> findTblTenderWatchList(Object... values) throws Exception;

    public List<TblTenderWatchList> findByCountTblTenderWatchList(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderWatchListCount();
}
