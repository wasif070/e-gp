/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblJointVenture;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TblJointVentureDao extends GenericDao<TblJointVenture> {

    public void addTblJointVenture(TblJointVenture tblObj);

    public void deleteTblJointVenture(TblJointVenture tblObj);

    public void updateTblJointVenture(TblJointVenture tblObj);

    public List<TblJointVenture> getAllTblJointVenture();

    public List<TblJointVenture> findTblJointVenture(Object... values) throws Exception;

    public List<TblJointVenture> findByCountTblJointVenture(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblJointVentureCount();
}
