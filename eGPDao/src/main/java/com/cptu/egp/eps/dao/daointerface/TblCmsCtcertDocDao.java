/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsCtcertDoc;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public interface TblCmsCtcertDocDao extends GenericDao<TblCmsCtcertDoc> {

    public void addTblCmsCtCertDoc(TblCmsCtcertDoc tblCmsCtcertDoc);

    public void deleteTblCmsCtCertDoc(TblCmsCtcertDoc tblCmsCtcertDoc);

    public void updateTblCmsCtCertDoc(TblCmsCtcertDoc tblCmsCtcertDoc);

    public List<TblCmsCtcertDoc> getAllTblCmsCtCertDoc();

    public List<TblCmsCtcertDoc> findTblCmsCtCertDoc(Object... values) throws Exception;

    public List<TblCmsCtcertDoc> findByCountTblCmsCtCertDoc(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsCtCertDocCount();
}
