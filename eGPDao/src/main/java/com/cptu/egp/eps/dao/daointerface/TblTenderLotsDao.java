/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderLots;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderLotsDao extends GenericDao<TblTenderLots>{

    public void addTblTenderLots(TblTenderLots tblObj);

    public void deleteTblTenderLots(TblTenderLots tblObj);

    public void updateTblTenderLots(TblTenderLots tblObj);

    public List<TblTenderLots> getAllTblTenderLots();

    public List<TblTenderLots> findTblTenderLots(Object... values) throws Exception;

    public List<TblTenderLots> findByCountTblTenderLots(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderLotsCount();
}
