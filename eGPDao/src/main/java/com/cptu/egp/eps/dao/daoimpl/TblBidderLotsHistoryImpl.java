package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblBidderLotsHistoryDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblBidderLotsHistory;
import java.util.List;

/**
 *
 * @author Nishith
 */
public class TblBidderLotsHistoryImpl extends AbcAbstractClass<TblBidderLotsHistory> implements TblBidderLotsHistoryDao {

    @Override
    public void addTblBidderLotsHistory(TblBidderLotsHistory master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblBidderLotsHistory(TblBidderLotsHistory master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblBidderLotsHistory(TblBidderLotsHistory master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblBidderLotsHistory> getAllTblBidderLotsHistory() {
        return super.getAllEntity();
    }

    @Override
    public List<TblBidderLotsHistory> findTblBidderLotsHistory(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblBidderLotsHistoryCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblBidderLotsHistory> findByCountTblBidderLotsHistory(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateInsertAllTblBidderLotsHistory(List<TblBidderLotsHistory> list) {
        super.updateAll(list);
    }
}
