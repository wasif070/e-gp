package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblEvalTscDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalTscDocs;
import java.util.List;

/**
 *
 * @author rishita
 */
public class TblEvalTscDocsImpl extends AbcAbstractClass<TblEvalTscDocs> implements TblEvalTscDocsDao {

    @Override
    public void addTblEvalTscDocs(TblEvalTscDocs tblEvalTscDocs){
        super.addEntity(tblEvalTscDocs);
    }

    @Override
    public void deleteTblEvalTscDocs(TblEvalTscDocs tblEvalTscDocs) {
        super.deleteEntity(tblEvalTscDocs);
    }

    @Override
    public void updateTblEvalTscDocs(TblEvalTscDocs tblEvalTscDocs) {
        super.updateEntity(tblEvalTscDocs);
    }

    @Override
    public List<TblEvalTscDocs> getAllTblEvalTscDocs() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEvalTscDocs> findTblEvalTscDocs(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEvalTscDocsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalTscDocs> findByCountTblEvalTscDocs(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
