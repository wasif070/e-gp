package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsTemplateSrvBoqDetail;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCmsTemplateSrvBoqDetailDao extends GenericDao<TblCmsTemplateSrvBoqDetail> {

    public void addTblCmsTemplateSrvBoqDetail(TblCmsTemplateSrvBoqDetail event);

    public void deleteTblCmsTemplateSrvBoqDetail(TblCmsTemplateSrvBoqDetail event);

    public void updateTblCmsTemplateSrvBoqDetail(TblCmsTemplateSrvBoqDetail event);

    public List<TblCmsTemplateSrvBoqDetail> getAllTblCmsTemplateSrvBoqDetail();

    public List<TblCmsTemplateSrvBoqDetail> findTblCmsTemplateSrvBoqDetail(Object... values) throws Exception;

    public List<TblCmsTemplateSrvBoqDetail> findByCountTblCmsTemplateSrvBoqDetail(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsTemplateSrvBoqDetailCount();
}
