package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsInvoiceMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsInvoiceMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsInvoiceMasterImpl extends AbcAbstractClass<TblCmsInvoiceMaster> implements TblCmsInvoiceMasterDao {

    @Override
    public void addTblCmsInvoiceMaster(TblCmsInvoiceMaster cmsInvoiceMaster){
        super.addEntity(cmsInvoiceMaster);
    }

    @Override
    public void deleteTblCmsInvoiceMaster(TblCmsInvoiceMaster cmsInvoiceMaster) {
        super.deleteEntity(cmsInvoiceMaster);
    }

    @Override
    public void updateTblCmsInvoiceMaster(TblCmsInvoiceMaster cmsInvoiceMaster) {
        super.updateEntity(cmsInvoiceMaster);
    }

    @Override
    public List<TblCmsInvoiceMaster> getAllTblCmsInvoiceMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsInvoiceMaster> findTblCmsInvoiceMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsInvoiceMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsInvoiceMaster> findByCountTblCmsInvoiceMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsInvoiceMaster> estCost) {
         super.updateAll(estCost);
    }
}
