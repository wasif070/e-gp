/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblConfigStdDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblConfigStd;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblConfigStdImpl extends AbcAbstractClass<TblConfigStd> implements TblConfigStdDao{

    @Override
    public void addTblConfigStd(TblConfigStd tblConfigStd) {
        super.addEntity(tblConfigStd);
    }

    @Override
    public List<TblConfigStd> findTblConfigStd(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblConfigStd(TblConfigStd tblConfigStd) {

        super.deleteEntity(tblConfigStd);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblConfigStd(Collection tblConfigStd) {
       super.deleteAll(tblConfigStd);
    }

    @Override
    public void updateTblConfigStd(TblConfigStd tblConfigStd) {

        super.updateEntity(tblConfigStd);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblConfigStd> getAllTblConfigStd() {
        return super.getAllEntity();
    }

    @Override
    public long getTblConfigStdCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblConfigStd> findByCountTblConfigStd(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
