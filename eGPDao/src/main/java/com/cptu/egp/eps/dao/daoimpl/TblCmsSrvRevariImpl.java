package com.cptu.egp.eps.dao.daoimpl;


import com.cptu.egp.eps.dao.daointerface.TblCmsSrvRevariDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvRevari;
import java.util.List;


public class TblCmsSrvRevariImpl extends AbcAbstractClass<TblCmsSrvRevari> implements TblCmsSrvRevariDao {

	@Override 
	public void addTblCmsSrvRevari(TblCmsSrvRevari master){
		super.addEntity(master);
	}
	@Override 
	public void deleteTblCmsSrvRevari(TblCmsSrvRevari master) {
		super.deleteEntity(master);
	}
	@Override 
	public void updateTblCmsSrvRevari(TblCmsSrvRevari master) {
		super.updateEntity(master);
	}
	@Override 
	public List<TblCmsSrvRevari> getAllTblCmsSrvRevari() {
		return super.getAllEntity();
	}
	@Override 
	public List<TblCmsSrvRevari> findTblCmsSrvRevari(Object... values) throws Exception {
		return super.findEntity(values);
	}
	@Override 
	public long getTblCmsSrvRevariCount() {
		return super.getEntityCount();
	}
	@Override 
	public List<TblCmsSrvRevari> findByCountTblCmsSrvRevari(int firstResult, int maxResult, Object... values) throws Exception {
		return super.findByCountEntity(firstResult, maxResult, values);
	}
	@Override 
	public void updateOrSaveTblCmsSrvRevari(List<TblCmsSrvRevari> tblObj) {
		super.updateAll(tblObj);
	}
}