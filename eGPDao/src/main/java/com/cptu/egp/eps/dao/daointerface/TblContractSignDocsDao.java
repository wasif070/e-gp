package com.cptu.egp.eps.dao.daointerface;


import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblContractSignDocs;
import java.util.List;


	public interface TblContractSignDocsDao extends GenericDao<TblContractSignDocs> {

	public void addTblContractSignDocs(TblContractSignDocs tblObj);

	public void deleteTblContractSignDocs(TblContractSignDocs tblObj);

	public void updateTblContractSignDocs(TblContractSignDocs tblObj);

	public List<TblContractSignDocs> getAllTblContractSignDocs();

	public List<TblContractSignDocs> findTblContractSignDocs(Object... values) throws Exception;

	public List<TblContractSignDocs> findByCountTblContractSignDocs(int firstResult,int maxResult,Object... values) throws Exception;

	public long getTblContractSignDocsCount();

	public void updateOrSaveTblContractSignDocs(List<TblContractSignDocs> tblObj);
}