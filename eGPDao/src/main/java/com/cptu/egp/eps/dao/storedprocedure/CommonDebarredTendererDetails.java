package com.cptu.egp.eps.dao.storedprocedure;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * This class is used to know the details of Debarred Tenderers.
 * This class is created for web-service(DebarmentService)
 * @author Sreenu
 */
public class CommonDebarredTendererDetails {
    private int debarmentId;
    /***
     * Name of Tenderer/Consultant
     */
    private String companyName;
    /***
     * Debarred at(Single tender,Package,Project,PE,Organization,e-GP System)
     */
    private String debarredAt;
    private String debarredBy;
    private String debarmentDate;
    private String reason;
    /***
     * Debarred from which Date to which date
     */
    private String debarmentPeriod;
    private Integer officeId;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDebarmentDate() {
        return debarmentDate;
    }

    public void setDebarmentDate(String debarmentDate) {
        this.debarmentDate = debarmentDate;
    }

    public int getDebarmentId() {
        return debarmentId;
    }

    public void setDebarmentId(int debarmentId) {
        this.debarmentId = debarmentId;
    }

    public String getDebarmentPeriod() {
        return debarmentPeriod;
    }

    public void setDebarmentPeriod(String debarmentPeriod) {
        this.debarmentPeriod = debarmentPeriod;
    }

    public String getDebarredAt() {
        return debarredAt;
    }

    public void setDebarredAt(String debarredAt) {
        this.debarredAt = debarredAt;
    }

    public String getDebarredBy() {
        return debarredBy;
    }

    public void setDebarredBy(String debarredBy) {
        this.debarredBy = debarredBy;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getOfficeId() {
        return officeId;
    }

    public void setOfficeId(Integer officeId) {
        this.officeId = officeId;
    }

    
    
}
