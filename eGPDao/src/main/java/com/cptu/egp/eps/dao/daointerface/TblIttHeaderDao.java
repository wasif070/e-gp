/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblIttHeader;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public interface TblIttHeaderDao extends GenericDao<TblIttHeader> {

    public void addTblIttHeader(TblIttHeader admin);

    public void deleteTblIttHeader(TblIttHeader admin);

    public void updateTblIttHeader(TblIttHeader admin);

    public List<TblIttHeader> getAllTblIttHeader();

    public List<TblIttHeader> findTblIttHeader(Object... values) throws Exception;

    public List<TblIttHeader> findByCountTblIttHeader(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblIttHeaderCount();
}
