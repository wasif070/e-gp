/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Administrator
 */
public class SPUserApproval extends StoredProcedure {
    private static final Logger LOGGER = Logger.getLogger(SPUserApproval.class);
    SPUserApproval(BasicDataSource dataSource, String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("@v_Status_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_RegType_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_CompanyRegNo_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_CompanyName_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_EmailId_inVc", Types.VARCHAR));                     
        this.declareParameter(new SqlParameter("@v_RegDateFrom_inDt", Types.DATE));
        this.declareParameter(new SqlParameter("@v_RegDateTo_inDt", Types.DATE));
        this.declareParameter(new SqlParameter("@v_Page_inN", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_RecordPerPage_inN", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_CompanyId_inN", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_OrderBy_inVc", Types.VARCHAR));
    }

    /**
     * Execute stored procedure : p_get_tenderer
     * Get Tenderer Search Information
     * @param email
     * @param cmpRegNo
     * @param cmpName
     * @param status
     * @param regType
     * @param regDateFrom
     * @param regDateTo
     * @param page
     * @param recPerPage
     * @param companyId
     * @param sortString
     * @return Tendere information as list of UserApprovalBean object
     */
    public List<UserApprovalBean> executeProcedure(String email, String cmpRegNo, String cmpName, String status, String regType, Date regDateFrom, Date regDateTo, int page, int recPerPage,Integer companyId,String sortString) {
        Map inParams = new HashMap();
        inParams.put("@v_Status_inVc", status);
        inParams.put("@v_RegType_inVc", regType);
        inParams.put("@v_CompanyRegNo_inVc", cmpRegNo);
        inParams.put("@v_CompanyName_inVc", cmpName);
        inParams.put("@v_EmailId_inVc", email);                         
        inParams.put("@v_RegDateFrom_inDt", regDateFrom);
        inParams.put("@v_RegDateTo_inDt", regDateTo);
        inParams.put("@v_Page_inN", page);
        inParams.put("@v_RecordPerPage_inN", recPerPage);
        inParams.put("@v_CompanyId_inN", companyId);
        inParams.put("@v_OrderBy_inVc", sortString);
        this.compile();
        List<UserApprovalBean> details = new ArrayList<UserApprovalBean>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                UserApprovalBean userApprovalBean = new UserApprovalBean();
                userApprovalBean.setCompanyId((Integer) linkedHashMap.get("companyid"));
                userApprovalBean.setCompanyName((String) linkedHashMap.get("companyName"));
                userApprovalBean.setCountry((String) linkedHashMap.get("country"));
                userApprovalBean.setEmailId((String) linkedHashMap.get("emailId"));
                userApprovalBean.setRegisteredDate((Date) linkedHashMap.get("registeredDate"));
                userApprovalBean.setRegistrationType((String) linkedHashMap.get("registrationType"));
                userApprovalBean.setCity((String)linkedHashMap.get("city"));
                userApprovalBean.setState((String) linkedHashMap.get("state"));
                userApprovalBean.setTotalPages((Integer) linkedHashMap.get("TotalPages"));
                userApprovalBean.setTotalRecords((Integer) linkedHashMap.get("TotalRecords"));
                userApprovalBean.setUserid((Integer) linkedHashMap.get("userid"));
                userApprovalBean.setComments((String)linkedHashMap.get("comments"));
                userApprovalBean.setTendererId((Integer)linkedHashMap.get("tendererId"));
                details.add(userApprovalBean);
            }
        } catch (Exception e) {
            LOGGER.error("SPUserApproval  : "+ e);
            e.printStackTrace();
        }
        return details;
    }
}
