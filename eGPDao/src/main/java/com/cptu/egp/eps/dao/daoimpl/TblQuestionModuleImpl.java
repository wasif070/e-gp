/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblQuestionModuleDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblQuestionModule;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author feroz
 */
public class TblQuestionModuleImpl extends AbcAbstractClass<TblQuestionModule> implements TblQuestionModuleDao{

    @Override
    public void addTblQuestionModule(TblQuestionModule tblQuestionModule) {
        super.addEntity(tblQuestionModule);
    }

    @Override
    public void deleteTblQuestionModule(TblQuestionModule tblQuestionModule) {
        super.deleteEntity(tblQuestionModule);
    }

    @Override
    public void updateTblQuestionModule(TblQuestionModule tblQuestionModule) {
        super.updateEntity(tblQuestionModule);
    }

    @Override
    public List<TblQuestionModule> getAllTblQuestionModule() {
        return super.getAllEntity();
    }

    @Override
    public List<TblQuestionModule> findTblQuestionModule(Object... values) {
        List<TblQuestionModule> tblQuestionModule = null;
        try {
            tblQuestionModule = super.findEntity(values);
        } catch (Exception ex) {
            Logger.getLogger(TblConfigPaThresholdImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tblQuestionModule;
    }
    
}
