package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblModuleMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblModuleMasterDao extends GenericDao<TblModuleMaster> {

    public void addTblModuleMaster(TblModuleMaster tblObj);

    public void deleteTblModuleMaster(TblModuleMaster tblObj);

    public void updateTblModuleMaster(TblModuleMaster tblObj);

    public List<TblModuleMaster> getAllTblModuleMaster();

    public List<TblModuleMaster> findTblModuleMaster(Object... values) throws Exception;

    public List<TblModuleMaster> findByCountTblModuleMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblModuleMasterCount();
}
