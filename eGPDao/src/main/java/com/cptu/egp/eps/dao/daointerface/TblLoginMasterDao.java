package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblLoginMasterDao extends GenericDao<TblLoginMaster> {

    public void addTblLoginMaster(TblLoginMaster login);

    public void deleteTblLoginMaster(TblLoginMaster login);

    public void updateTblLoginMaster(TblLoginMaster login);

    public List<TblLoginMaster> getAllTblLoginMaster();

    public List<TblLoginMaster> findTblLoginMaster(Object... values) throws Exception;

    public List<TblLoginMaster> findByCountTblLoginMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblLoginMasterCount();
}
