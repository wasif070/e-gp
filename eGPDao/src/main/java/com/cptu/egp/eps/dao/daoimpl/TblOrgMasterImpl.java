/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblOrgMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblOrgMaster;
import java.util.List;

/**
 *
 * @author Sudhir Chavhan
 */
public class TblOrgMasterImpl extends AbcAbstractClass<TblOrgMaster> implements TblOrgMasterDao {

    @Override
    public void addTblOrgMaster(TblOrgMaster org) {
        super.addEntity(org);
    }

    @Override
    public void deleteTblOrgMaster(TblOrgMaster org) {
        super.deleteEntity(org);
    }

    @Override
    public void updateTblOrgMaster(TblOrgMaster org) {
        super.updateEntity(org);
    }

    @Override
    public List<TblOrgMaster> getAllTblOrgMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblOrgMaster> findTblOrgMaster(Object... values) throws Exception {
        //System.out.println("TblOrgMasterImpl.java 1:-" + values);
        //System.out.println("TblOrgMasterImpl.java 2:-" + super.findEntity(values));
        return super.findEntity(values);
    }

    @Override
    public List<TblOrgMaster> findByCountTblOrgMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblOrgMasterCount() {
        return super.getEntityCount();
    }
}
