package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvTchistory;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCmsSrvTchistoryDao extends GenericDao<TblCmsSrvTchistory> {

    public void addTblCmsSrvTchistory(TblCmsSrvTchistory tblObj);

    public void deleteTblCmsSrvTchistory(TblCmsSrvTchistory tblObj);

    public void updateTblCmsSrvTchistory(TblCmsSrvTchistory tblObj);

    public List<TblCmsSrvTchistory> getAllTblCmsSrvTchistory();

    public List<TblCmsSrvTchistory> findTblCmsSrvTchistory(Object... values) throws Exception;

    public List<TblCmsSrvTchistory> findByCountTblCmsSrvTchistory(int firstResult,int maxResult,Object... values) throws Exception;
    
    public void updateOrSaveAllCmsSrvTchistory(List<TblCmsSrvTchistory> tblObjList);

    public long getTblCmsSrvTchistoryCount();
}
