package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsWpMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCmsWpMasterDao extends GenericDao<TblCmsWpMaster> {

    public void addTblCmsWpMaster(TblCmsWpMaster event);

    public void deleteTblCmsWpMaster(TblCmsWpMaster event);

    public void updateTblCmsWpMaster(TblCmsWpMaster event);

    public List<TblCmsWpMaster> getAllTblCmsWpMaster();

    public List<TblCmsWpMaster> findTblCmsWpMaster(Object... values) throws Exception;

    public List<TblCmsWpMaster> findByCountTblCmsWpMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsWpMasterCount();

    public void updateOrSaveEstCost(List<TblCmsWpMaster> estCost);
}
