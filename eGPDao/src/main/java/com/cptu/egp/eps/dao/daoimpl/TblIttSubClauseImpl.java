/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblIttSubClauseDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblIttSubClause;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public class TblIttSubClauseImpl extends AbcAbstractClass<TblIttSubClause> implements TblIttSubClauseDao {

    @Override
    public void addTblIttSubClause(TblIttSubClause ittSubClause) {

        super.addEntity(ittSubClause);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblIttSubClause(TblIttSubClause ittSubClause) {

        super.deleteEntity(ittSubClause);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblIttSubClause(TblIttSubClause ittSubClause) {

        super.updateEntity(ittSubClause);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblIttSubClause> getAllTblIttSubClause() {
        return super.getAllEntity();
    }

    @Override
    public List<TblIttSubClause> findTblIttSubClause(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblIttSubClauseCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblIttSubClause> findByCountTblIttSubClause(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
