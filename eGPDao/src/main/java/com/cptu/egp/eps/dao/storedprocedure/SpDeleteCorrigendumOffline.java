/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.math.BigDecimal;
import java.util.List;
import java.util.Date;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author salahuddin
 */
public class SpDeleteCorrigendumOffline  extends StoredProcedure  {

    private static final Logger LOGGER = Logger.getLogger(SpDeleteCorrigendumOffline.class);

    public SpDeleteCorrigendumOffline(BasicDataSource dataSource, String procName)
    {
       this.setDataSource(dataSource);
       this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
       LOGGER.debug("Proc Name: "+ procName);
       //this.declareParameter(new SqlParameter("corrigendumID", Types.INTEGER));
       this.declareParameter(new SqlParameter("tenderOLID", Types.INTEGER));

    }

    public List<CommonSPReturn> executeProcedure(int tenderID)
    {
        boolean isDeleted = false;
        Map inParams = new HashMap();        
        inParams.put("tenderOLID", tenderID);
        this.compile();
        List<CommonSPReturn> details = new ArrayList<CommonSPReturn>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonSPReturn commonSPReturn = new CommonSPReturn();
                //commonSPReturn.setId(new BigDecimal(Integer.toString((Integer)linkedHashMap.get("PackageId"))));
                commonSPReturn.setFlag((Boolean)linkedHashMap.get("flag"));
                commonSPReturn.setMsg((String) linkedHashMap.get("Message"));
                details.add(commonSPReturn);
            }
        } catch (Exception e) {
           LOGGER.error("SpDeleteCorrigendumOffline : " + e);
        }
        return details;
    }

}
