/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblAppEngEstDocDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblAppEngEstDoc;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblAppEngEstDocImpl extends AbcAbstractClass<TblAppEngEstDoc> implements TblAppEngEstDocDao{

    @Override
    public void addTblAppEngEstDoc(TblAppEngEstDoc tblAppEngEstDoc) {
        super.addEntity(tblAppEngEstDoc);
    }

    @Override
    public List<TblAppEngEstDoc> findTblAppEngEstDoc(Object... values) throws Exception {
        return super.findEntity(values);       
    }

    @Override
    public void deleteTblAppEngEstDoc(TblAppEngEstDoc tblAppEngEstDoc) {
        super.deleteEntity(tblAppEngEstDoc);      
    }

    @Override
    public void updateTblAppEngEstDoc(TblAppEngEstDoc tblAppEngEstDoc) {
        super.updateEntity(tblAppEngEstDoc);        
    }

    @Override
    public List<TblAppEngEstDoc> getAllTblAppEngEstDoc() {
        return super.getAllEntity();
    }

    @Override
    public long getTblAppEngEstDocCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblAppEngEstDoc> findByCountTblAppEngEstDoc(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
