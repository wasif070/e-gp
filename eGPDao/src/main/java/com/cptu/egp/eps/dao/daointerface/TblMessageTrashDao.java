package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblMessageTrash;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblMessageTrashDao extends GenericDao<TblMessageTrash> {

    public void addTblMessageTrash(TblMessageTrash tblObj);

    public void deleteTblMessageTrash(TblMessageTrash tblObj);

    public void updateTblMessageTrash(TblMessageTrash tblObj);

    public List<TblMessageTrash> getAllTblMessageTrash();

    public List<TblMessageTrash> findTblMessageTrash(Object... values) throws Exception;

    public List<TblMessageTrash> findByCountTblMessageTrash(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblMessageTrashCount();
}
