package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsInvAccHistory;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCmsInvAccHistoryDao extends GenericDao<TblCmsInvAccHistory> {

    public void addTblCmsInvAccHistory(TblCmsInvAccHistory event);

    public void deleteTblCmsInvAccHistory(TblCmsInvAccHistory event);

    public void updateTblCmsInvAccHistory(TblCmsInvAccHistory event);

    public List<TblCmsInvAccHistory> getAllTblCmsInvAccHistory();

    public List<TblCmsInvAccHistory> findTblCmsInvAccHistory(Object... values) throws Exception;

    public List<TblCmsInvAccHistory> findByCountTblCmsInvAccHistory(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsInvAccHistoryCount();
}
