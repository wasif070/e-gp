package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblProjectFinPower;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblProjectFinPowerDao extends GenericDao<TblProjectFinPower> {

    public void addTblProjectFinPower(TblProjectFinPower tblObj);

    public void deleteTblProjectFinPower(TblProjectFinPower tblObj);

    public void updateTblProjectFinPower(TblProjectFinPower tblObj);

    public List<TblProjectFinPower> getAllTblProjectFinPower();

    public List<TblProjectFinPower> findTblProjectFinPower(Object... values) throws Exception;

    public List<TblProjectFinPower> findByCountTblProjectFinPower(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblProjectFinPowerCount();
}
