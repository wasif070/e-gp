package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblHolidayMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblHolidayMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblHolidayMasterImpl extends AbcAbstractClass<TblHolidayMaster> implements TblHolidayMasterDao {

    @Override
    public void addTblHolidayMaster(TblHolidayMaster master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblHolidayMaster(TblHolidayMaster master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblHolidayMaster(TblHolidayMaster master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblHolidayMaster> getAllTblHolidayMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblHolidayMaster> findTblHolidayMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblHolidayMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblHolidayMaster> findByCountTblHolidayMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
