/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderPaymentDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderPayment;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderPaymentImpl extends AbcAbstractClass<TblTenderPayment> implements TblTenderPaymentDao {

    @Override
    public void addTblTenderPayment(TblTenderPayment tenderPayment) {
        super.addEntity(tenderPayment);
    }

    @Override
    public void deleteTblTenderPayment(TblTenderPayment tenderPayment) {
        super.deleteEntity(tenderPayment);
    }

    @Override
    public void updateTblTenderPayment(TblTenderPayment tenderPayment) {
        super.updateEntity(tenderPayment);
    }

    @Override
    public List<TblTenderPayment> getAllTblTenderPayment() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderPayment> findTblTenderPayment(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderPaymentCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderPayment> findByCountTblTenderPayment(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
