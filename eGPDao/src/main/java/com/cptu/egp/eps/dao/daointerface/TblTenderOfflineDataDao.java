/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.model.table.TblTenderDetailsOffline;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TblTenderOfflineDataDao {

    public void addTblTenderDetailOfflineData(TblTenderDetailsOffline tenderDetailOfflineData);

    public void deleteTblTenderDetailOfflineData(TblTenderDetailsOffline tenderDetailOfflineData);

    public void updateTblTenderDetailOfflineData(TblTenderDetailsOffline tenderDetailOfflineData);

    public List<TblTenderDetailsOffline> getAllTblTenderDetailOfflineData();

    public List<TblTenderDetailsOffline> findTblTenderDetailOfflineData(Object... values) throws Exception;

    public long getTblTenderDetailOfflineDataCount() ;

    public List<TblTenderDetailsOffline> findByCountTblTenderDetailOfflineData(int firstResult, int maxResult, Object... values) throws Exception;
}
