/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTemplateMandatoryDoc;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TblTemplateMandatoryDocDao extends GenericDao<TblTemplateMandatoryDoc>{


    public void addTblTemplateMandatoryDoc(TblTemplateMandatoryDoc tblObj);

    public void deleteTblTemplateMandatoryDoc(TblTemplateMandatoryDoc tblObj);

    public void updateTblTemplateMandatoryDoc(TblTemplateMandatoryDoc tblObj);

    public List<TblTemplateMandatoryDoc> getAllTblTemplateMandatoryDoc();

    public List<TblTemplateMandatoryDoc> findTblTemplateMandatoryDoc(Object... values) throws Exception;

    public List<TblTemplateMandatoryDoc> findByCountTblTemplateMandatoryDoc(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTemplateMandatoryDocCount();
    public void updateOrInsertAll(List<TblTemplateMandatoryDoc> list);

}
