/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.math.BigDecimal;
/**
 *
 * @author Administrator
 */
public class CommonAppPkgDetails
{

    private Integer appId;
    private String appCode;
    private String ministryDetails;
    private String projectNameCode;
    private String financialYear;
    private String budgetType;
    private String projectName;
    private String procurementnature;
    private String servicesType;
    private String packageNo;
    private String packageDesc;
    private String bidderCategory;
    private String workCategory;
    private String depoplanWork;
    private String entrustingAgency;
    private String timeFrame;
    private BigDecimal allocateBudget;
    private BigDecimal pkgEstCost;
    private String isPQRequired;
    private String reoiRfaRequired;
    private Short procurementMethodId;
    private String procurementType;
    private String sourceOfFund;
    private String pkgUrgency;
    private String cpvCategory;
    private String advtDt;
    private String subDt;
    private String tenderAdvertDt;
    private String tenderSubDt;
    private String tenderOpenDt;
    //Proshanto
    private String tenderLetterIntentDt;
    //
    private String tenderNoaIssueDt;
    private String tenderContractSignDt;
    private String tenderContractCompDt;
    private String pe;
    private String district;
    private String techSubCmtRptDt;
    private String tenderEvalRptDt;
    private String tenderEvalRptAppDt;
    private String tenderContractAppDt;
    private String reoiReceiptDt;
    private String rfpTechEvalDt;
    private String rfpFinancialOpenDt;
    private String rfpNegCompDt;
    private String rfpContractAppDt;
    private String rfaAdvertDt;
    private String rfaReceiptDt;
    private String rfaEvalDt;
    private String rfaInterviewDt;
    private String rfaFinalSelDt;
    private String rfaEvalRptSubDt;
    private String rfaAppConsultantDt;
    private String actSubDt;
    private String actAdvtDt;
    private String actEvalRptDt;
    private String actAppLstDt;
    private String actTenderAdvertDt;
    private String actTenderSubDt;
    private String actTenderOpenDt;
    private String actTechSubCmtRptDt;
    private String actTenderEvalRptDt;
    private String acttenderEvalRptAppDt;
    private String actTenderContractAppDt;
    private String actTenderLetterIntentDt;
    private String actTenderNoaIssueDt;
    private String actTenderContractSignDt;
    private String actTenderContractCompDt;
    private String actReoiReceiptDt;
    private String actRfpTechEvalDt;
    private String actRfpFinancialOpenDt;
    private String actRfpNegComDt;
    private String actRfpContractAppDt;
    private String actRfaAdvertDt;
    private String actRfaReceiptDt;
    private String actRfaEvalDt;
    private String actRfaInterviewDt;
    private String actRfaFinalSelDt;
    private String actRfaEvalRptSubDt;
    private String actRfaAppConsultantDt;
    private String evalRptDt;
    private Integer packageId;
    private String lotNo;
    private String lotDesc;
    private BigDecimal quantity;
    private String unit;
    private BigDecimal lotEstCost;
    private Integer aaEmpId;
    private String emeType;
    private String cpvCode;
    private Integer projectId;
    private String appLstDt;
    private Short subDays;
    private Short advtDays;
    private Short openDays;
    private String openDt;
    private Short evalRptDays;
    private Short tenderAdvertDays;
    private Short tenderSubDays;
    private Short tenderOpenDays;
    private Short techSubCmtRptDays;
    private Short tenderEvalRptdays;
    private Short tenderEvalRptAppDays;
    private Short tenderContractAppDays;
    //Proshanto
    private Short tnderLetterIntentDays;
    //
    private Short tenderNoaIssueDays;
    private Short tenderContractSignDays;
    private Short rfaAdvertDays;
    private Short rfaReceiptDays;
    private Short rfaEvalDays;
    private Short rfaInterviewDays;
    private Short rfaFinalSelDays;
    private Short rfaEvalRptSubDays;
    private Short rfpContractAppDays;
    private Short rfpNegCompDays;
    private Short rfpFinancialOpenDays;
    private Short rfpTechEvalDays;
    private Short reoiReceiptDays;
    private Integer pqDtId;

    private String procurementMethod;    
    private String procurementRole;
    private String procurementNature;
    private BigDecimal estimatedCost;

    private String ministry;
    private String division;
    private String agency;
    private String peCode;
    private String officeName;
    private String projectCode;

    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }
  
    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getMinistry() {
        return ministry;
    }

    public void setMinistry(String ministry) {
        this.ministry = ministry;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    public String getPeCode() {
        return peCode;
    }

    public void setPeCode(String peCode) {
        this.peCode = peCode;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public BigDecimal getEstimatedCost() {
        return estimatedCost;
    }

    public void setEstimatedCost(BigDecimal estimatedCost) {
        this.estimatedCost = estimatedCost;
    }

    public String getProcurementNature() {
        return procurementNature;
    }

    public void setProcurementNature(String procurementNature) {
        this.procurementNature = procurementNature;
    }

    public String getProcurementMethod() {
        return procurementMethod;
    }

    public void setProcurementMethod(String procurementMethod) {
        this.procurementMethod = procurementMethod;
    }

    public String getProcurementRole() {
        return procurementRole;
    }

    public void setProcurementRole(String procurementRole) {
        this.procurementRole = procurementRole;
    }

    public Integer getPqDtId()
    {
        return pqDtId;
    }

    public void setPqDtId(Integer pqDtId)
    {
        this.pqDtId = pqDtId;
    }

    public String getLotDesc()
    {
        return lotDesc;
    }

    public void setLotDesc(String lotDesc)
    {
        this.lotDesc = lotDesc;
    }

    public BigDecimal getLotEstCost()
    {
        return lotEstCost;
    }

    public void setLotEstCost(BigDecimal lotEstCost)
    {
        this.lotEstCost = lotEstCost;
    }

    public String getLotNo()
    {
        return lotNo;
    }

    public void setLotNo(String lotNo)
    {
        this.lotNo = lotNo;
    }

    public BigDecimal getQuantity()
    {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity)
    {
        this.quantity = quantity;
    }

    public String getUnit()
    {
        return unit;
    }

    public void setUnit(String unit)
    {
        this.unit = unit;
    }

    /*public String getPE()
    {
        return PE;
    }

    public void setPE(String PE)
    {
        this.PE = PE;
    }*/

    public String getPe() {
        return pe;
    }

    public void setPe(String pe) {
        this.pe = pe;
    }

    
    

    public String getActAdvtDt()
    {
        return actAdvtDt;
    }

    public void setActAdvtDt(String actAdvtDt)
    {
        this.actAdvtDt = actAdvtDt;
    }

    public String getActAppLstDt()
    {
        return actAppLstDt;
    }

    public void setActAppLstDt(String actAppLstDt)
    {
        this.actAppLstDt = actAppLstDt;
    }

    public String getActEvalRptDt()
    {
        return actEvalRptDt;
    }

    public void setActEvalRptDt(String actEvalRptDt)
    {
        this.actEvalRptDt = actEvalRptDt;
    }

    public String getActReoiReceiptDt()
    {
        return actReoiReceiptDt;
    }

    public void setActReoiReceiptDt(String actReoiReceiptDt)
    {
        this.actReoiReceiptDt = actReoiReceiptDt;
    }

    public String getActRfaAdvertDt()
    {
        return actRfaAdvertDt;
    }

    public void setActRfaAdvertDt(String actRfaAdvertDt)
    {
        this.actRfaAdvertDt = actRfaAdvertDt;
    }

    public String getActRfaAppConsultantDt()
    {
        return actRfaAppConsultantDt;
    }

    public void setActRfaAppConsultantDt(String actRfaAppConsultantDt)
    {
        this.actRfaAppConsultantDt = actRfaAppConsultantDt;
    }

    public String getActRfaEvalDt()
    {
        return actRfaEvalDt;
    }

    public void setActRfaEvalDt(String actRfaEvalDt)
    {
        this.actRfaEvalDt = actRfaEvalDt;
    }

    public String getActRfaEvalRptSubDt()
    {
        return actRfaEvalRptSubDt;
    }

    public void setActRfaEvalRptSubDt(String actRfaEvalRptSubDt)
    {
        this.actRfaEvalRptSubDt = actRfaEvalRptSubDt;
    }

    public String getActRfaFinalSelDt()
    {
        return actRfaFinalSelDt;
    }

    public void setActRfaFinalSelDt(String actRfaFinalSelDt)
    {
        this.actRfaFinalSelDt = actRfaFinalSelDt;
    }

    public String getActRfaInterviewDt()
    {
        return actRfaInterviewDt;
    }

    public void setActRfaInterviewDt(String actRfaInterviewDt)
    {
        this.actRfaInterviewDt = actRfaInterviewDt;
    }

    public String getActRfaReceiptDt()
    {
        return actRfaReceiptDt;
    }

    public void setActRfaReceiptDt(String actRfaReceiptDt)
    {
        this.actRfaReceiptDt = actRfaReceiptDt;
    }

    public String getActRfpContractAppDt()
    {
        return actRfpContractAppDt;
    }

    public void setActRfpContractAppDt(String actRfpContractAppDt)
    {
        this.actRfpContractAppDt = actRfpContractAppDt;
    }

    public String getActRfpFinancialOpenDt()
    {
        return actRfpFinancialOpenDt;
    }

    public void setActRfpFinancialOpenDt(String actRfpFinancialOpenDt)
    {
        this.actRfpFinancialOpenDt = actRfpFinancialOpenDt;
    }

    public String getActRfpNegComDt()
    {
        return actRfpNegComDt;
    }

    public void setActRfpNegComDt(String actRfpNegComDt)
    {
        this.actRfpNegComDt = actRfpNegComDt;
    }

    public String getActRfpTechEvalDt()
    {
        return actRfpTechEvalDt;
    }

    public void setActRfpTechEvalDt(String actRfpTechEvalDt)
    {
        this.actRfpTechEvalDt = actRfpTechEvalDt;
    }

    public String getActSubDt()
    {
        return actSubDt;
    }

    public void setActSubDt(String actSubDt)
    {
        this.actSubDt = actSubDt;
    }

    public String getActTechSubCmtRptDt()
    {
        return actTechSubCmtRptDt;
    }

    public void setActTechSubCmtRptDt(String actTechSubCmtRptDt)
    {
        this.actTechSubCmtRptDt = actTechSubCmtRptDt;
    }

    public String getActTenderAdvertDt()
    {
        return actTenderAdvertDt;
    }

    public void setActTenderAdvertDt(String actTenderAdvertDt)
    {
        this.actTenderAdvertDt = actTenderAdvertDt;
    }

    public String getActTenderContractAppDt()
    {
        return actTenderContractAppDt;
    }

    public void setActTenderContractAppDt(String actTenderContractAppDt)
    {
        this.actTenderContractAppDt = actTenderContractAppDt;
    }

    public String getActTenderContractCompDt()
    {
        return actTenderContractCompDt;
    }

    public void setActTenderContractCompDt(String actTenderContractCompDt)
    {
        this.actTenderContractCompDt = actTenderContractCompDt;
    }

    public String getActTenderContractSignDt()
    {
        return actTenderContractSignDt;
    }

    public void setActTenderContractSignDt(String actTenderContractSignDt)
    {
        this.actTenderContractSignDt = actTenderContractSignDt;
    }

    public String getActTenderEvalRptDt()
    {
        return actTenderEvalRptDt;
    }

    public void setActTenderEvalRptDt(String actTenderEvalRptDt)
    {
        this.actTenderEvalRptDt = actTenderEvalRptDt;
    }

    public String getActTenderNoaIssueDt()
    {
        return actTenderNoaIssueDt;
    }

    public void setActTenderNoaIssueDt(String actTenderNoaIssueDt)
    {
        this.actTenderNoaIssueDt = actTenderNoaIssueDt;
    }

    public String getActTenderOpenDt()
    {
        return actTenderOpenDt;
    }

    public void setActTenderOpenDt(String actTenderOpenDt)
    {
        this.actTenderOpenDt = actTenderOpenDt;
    }

    public String getActTenderSubDt()
    {
        return actTenderSubDt;
    }

    public void setActTenderSubDt(String actTenderSubDt)
    {
        this.actTenderSubDt = actTenderSubDt;
    }

    public String getActtenderEvalRptAppDt()
    {
        return acttenderEvalRptAppDt;
    }

    public void setActtenderEvalRptAppDt(String acttenderEvalRptAppDt)
    {
        this.acttenderEvalRptAppDt = acttenderEvalRptAppDt;
    }

    public String getAdvtDt()
    {
        return advtDt;
    }

    public void setAdvtDt(String advtDt)
    {
        this.advtDt = advtDt;
    }

    public BigDecimal getAllocateBudget()
    {
        return allocateBudget;
    }

    public void setAllocateBudget(BigDecimal allocateBudget)
    {
        this.allocateBudget = allocateBudget;
    }

    public String getAppCode()
    {
        return appCode;
    }

    public void setAppCode(String appCode)
    {
        this.appCode = appCode;
    }

    public Integer getAppId()
    {
        return appId;
    }

    public void setAppId(Integer appId)
    {
        this.appId = appId;
    }

    public String getBudgetType()
    {
        return budgetType;
    }

    public void setBudgetType(String budgetType)
    {
        this.budgetType = budgetType;
    }

    public String getDistrict()
    {
        return district;
    }

    public void setDistrict(String district)
    {
        this.district = district;
    }

    public String getEvalRptDt()
    {
        return evalRptDt;
    }

    public void setEvalRptDt(String evalRptDt)
    {
        this.evalRptDt = evalRptDt;
    }

    public String getFinancialYear()
    {
        return financialYear;
    }

    public void setFinancialYear(String financialYear)
    {
        this.financialYear = financialYear;
    }

    public String getIsPQRequired()
    {
        return isPQRequired;
    }

    public void setIsPQRequired(String isPQRequired)
    {
        this.isPQRequired = isPQRequired;
    }

    public String getPackageDesc()
    {
        return packageDesc;
    }

    public void setPackageDesc(String packageDesc)
    {
        this.packageDesc = packageDesc;
    }

    public Integer getPackageId()
    {
        return packageId;
    }

    public void setPackageId(Integer packageId)
    {
        this.packageId = packageId;
    }

    public String getPackageNo()
    {
        return packageNo;
    }

    public void setPackageNo(String packageNo)
    {
        this.packageNo = packageNo;
    }

    public BigDecimal getPkgEstCost()
    {
        return pkgEstCost;
    }

    public void setPkgEstCost(BigDecimal pkgEstCost)
    {
        this.pkgEstCost = pkgEstCost;
    }

    public Short getProcurementMethodId()
    {
        return procurementMethodId;
    }

    public void setProcurementMethodId(Short procurementMethodId)
    {
        this.procurementMethodId = procurementMethodId;
    }

    public String getProcurementType()
    {
        return procurementType;
    }

    public void setProcurementType(String procurementType)
    {
        this.procurementType = procurementType;
    }

    public String getProcurementnature()
    {
        return procurementnature;
    }

    public void setProcurementnature(String procurementnature)
    {
        this.procurementnature = procurementnature;
    }

    public String getProjectName()
    {
        return projectName;
    }

    public void setProjectName(String projectName)
    {
        this.projectName = projectName;
    }

    public String getReoiReceiptDt()
    {
        return reoiReceiptDt;
    }

    public void setReoiReceiptDt(String reoiReceiptDt)
    {
        this.reoiReceiptDt = reoiReceiptDt;
    }

    public String getReoiRfaRequired()
    {
        return reoiRfaRequired;
    }

    public void setReoiRfaRequired(String reoiRfaRequired)
    {
        this.reoiRfaRequired = reoiRfaRequired;
    }

    public String getRfaAdvertDt()
    {
        return rfaAdvertDt;
    }

    public void setRfaAdvertDt(String rfaAdvertDt)
    {
        this.rfaAdvertDt = rfaAdvertDt;
    }

    public String getRfaAppConsultantDt()
    {
        return rfaAppConsultantDt;
    }

    public void setRfaAppConsultantDt(String rfaAppConsultantDt)
    {
        this.rfaAppConsultantDt = rfaAppConsultantDt;
    }

    public String getRfaEvalDt()
    {
        return rfaEvalDt;
    }

    public void setRfaEvalDt(String rfaEvalDt)
    {
        this.rfaEvalDt = rfaEvalDt;
    }

    public String getRfaEvalRptSubDt()
    {
        return rfaEvalRptSubDt;
    }

    public void setRfaEvalRptSubDt(String rfaEvalRptSubDt)
    {
        this.rfaEvalRptSubDt = rfaEvalRptSubDt;
    }

    public String getRfaFinalSelDt()
    {
        return rfaFinalSelDt;
    }

    public void setRfaFinalSelDt(String rfaFinalSelDt)
    {
        this.rfaFinalSelDt = rfaFinalSelDt;
    }

    public String getRfaInterviewDt()
    {
        return rfaInterviewDt;
    }

    public void setRfaInterviewDt(String rfaInterviewDt)
    {
        this.rfaInterviewDt = rfaInterviewDt;
    }

    public String getRfaReceiptDt()
    {
        return rfaReceiptDt;
    }

    public void setRfaReceiptDt(String rfaReceiptDt)
    {
        this.rfaReceiptDt = rfaReceiptDt;
    }

    public String getRfpContractAppDt()
    {
        return rfpContractAppDt;
    }

    public void setRfpContractAppDt(String rfpContractAppDt)
    {
        this.rfpContractAppDt = rfpContractAppDt;
    }

    public String getRfpFinancialOpenDt()
    {
        return rfpFinancialOpenDt;
    }

    public void setRfpFinancialOpenDt(String rfpFinancialOpenDt)
    {
        this.rfpFinancialOpenDt = rfpFinancialOpenDt;
    }

    public String getRfpNegCompDt()
    {
        return rfpNegCompDt;
    }

    public void setRfpNegCompDt(String rfpNegCompDt)
    {
        this.rfpNegCompDt = rfpNegCompDt;
    }

    public String getRfpTechEvalDt()
    {
        return rfpTechEvalDt;
    }

    public void setRfpTechEvalDt(String rfpTechEvalDt)
    {
        this.rfpTechEvalDt = rfpTechEvalDt;
    }

    public String getServicesType()
    {
        return servicesType;
    }

    public void setServicesType(String servicesType)
    {
        this.servicesType = servicesType;
    }

    public String getSourceOfFund()
    {
        return sourceOfFund;
    }

    public void setSourceOfFund(String sourceOfFund)
    {
        this.sourceOfFund = sourceOfFund;
    }

    public String getSubDt()
    {
        return subDt;
    }

    public void setSubDt(String subDt)
    {
        this.subDt = subDt;
    }

    public String getTechSubCmtRptDt()
    {
        return techSubCmtRptDt;
    }

    public void setTechSubCmtRptDt(String techSubCmtRptDt)
    {
        this.techSubCmtRptDt = techSubCmtRptDt;
    }

    public String getTenderAdvertDt()
    {
        return tenderAdvertDt;
    }

    public void setTenderAdvertDt(String tenderAdvertDt)
    {
        this.tenderAdvertDt = tenderAdvertDt;
    }

    public String getTenderContractAppDt()
    {
        return tenderContractAppDt;
    }

    public void setTenderContractAppDt(String tenderContractAppDt)
    {
        this.tenderContractAppDt = tenderContractAppDt;
    }

    public String getTenderContractCompDt()
    {
        return tenderContractCompDt;
    }

    public void setTenderContractCompDt(String tenderContractCompDt)
    {
        this.tenderContractCompDt = tenderContractCompDt;
    }

    public String getTenderContractSignDt()
    {
        return tenderContractSignDt;
    }

    public void setTenderContractSignDt(String tenderContractSignDt)
    {
        this.tenderContractSignDt = tenderContractSignDt;
    }

    public String getTenderEvalRptAppDt()
    {
        return tenderEvalRptAppDt;
    }

    public void setTenderEvalRptAppDt(String tenderEvalRptAppDt)
    {
        this.tenderEvalRptAppDt = tenderEvalRptAppDt;
    }

    public String getTenderEvalRptDt()
    {
        return tenderEvalRptDt;
    }

    public void setTenderEvalRptDt(String tenderEvalRptDt)
    {
        this.tenderEvalRptDt = tenderEvalRptDt;
    }

    public String getTenderNoaIssueDt()
    {
        return tenderNoaIssueDt;
    }

    public void setTenderNoaIssueDt(String tenderNoaIssueDt)
    {
        this.tenderNoaIssueDt = tenderNoaIssueDt;
    }

    public String getTenderOpenDt()
    {
        return tenderOpenDt;
    }

    public void setTenderOpenDt(String tenderOpenDt)
    {
        this.tenderOpenDt = tenderOpenDt;
    }

    public String getTenderSubDt()
    {
        return tenderSubDt;
    }

    public void setTenderSubDt(String tenderSubDt)
    {
        this.tenderSubDt = tenderSubDt;
    }

    public Integer getAaEmpId() {
        return aaEmpId;
    }

    public void setAaEmpId(Integer aaEmpId) {
        this.aaEmpId = aaEmpId;
    }

    public Short getAdvtDays() {
        return advtDays;
    }

    public void setAdvtDays(Short advtDays) {
        this.advtDays = advtDays;
    }

    public String getAppLstDt() {
        return appLstDt;
    }

    public void setAppLstDt(String appLstDt) {
        this.appLstDt = appLstDt;
    }

    public Short getEvalRptDays() {
        return evalRptDays;
    }

    public void setEvalRptDays(Short evalRptDays) {
        this.evalRptDays = evalRptDays;
    }

    public Short getOpenDays() {
        return openDays;
    }

    public void setOpenDays(Short openDays) {
        this.openDays = openDays;
    }

    public String getOpenDt() {
        return openDt;
    }

    public void setOpenDt(String openDt) {
        this.openDt = openDt;
    }

    public Short getReoiReceiptDays() {
        return reoiReceiptDays;
    }

    public void setReoiReceiptDays(Short reoiReceiptDays) {
        this.reoiReceiptDays = reoiReceiptDays;
    }

    public Short getRfaAdvertDays() {
        return rfaAdvertDays;
    }

    public void setRfaAdvertDays(Short rfaAdvertDays) {
        this.rfaAdvertDays = rfaAdvertDays;
    }

    public Short getRfaEvalDays() {
        return rfaEvalDays;
    }

    public void setRfaEvalDays(Short rfaEvalDays) {
        this.rfaEvalDays = rfaEvalDays;
    }

    public Short getRfaEvalRptSubDays() {
        return rfaEvalRptSubDays;
    }

    public void setRfaEvalRptSubDays(Short rfaEvalRptSubDays) {
        this.rfaEvalRptSubDays = rfaEvalRptSubDays;
    }

    public Short getRfaFinalSelDays() {
        return rfaFinalSelDays;
    }

    public void setRfaFinalSelDays(Short rfaFinalSelDays) {
        this.rfaFinalSelDays = rfaFinalSelDays;
    }

    public Short getRfaInterviewDays() {
        return rfaInterviewDays;
    }

    public void setRfaInterviewDays(Short rfaInterviewDays) {
        this.rfaInterviewDays = rfaInterviewDays;
    }

    public Short getRfaReceiptDays() {
        return rfaReceiptDays;
    }

    public void setRfaReceiptDays(Short rfaReceiptDays) {
        this.rfaReceiptDays = rfaReceiptDays;
    }

    public Short getRfpContractAppDays() {
        return rfpContractAppDays;
    }

    public void setRfpContractAppDays(Short rfpContractAppDays) {
        this.rfpContractAppDays = rfpContractAppDays;
    }

    public Short getRfpFinancialOpenDays() {
        return rfpFinancialOpenDays;
    }

    public void setRfpFinancialOpenDays(Short rfpFinancialOpenDays) {
        this.rfpFinancialOpenDays = rfpFinancialOpenDays;
    }

    public Short getRfpNegCompDays() {
        return rfpNegCompDays;
    }

    public void setRfpNegCompDays(Short rfpNegCompDays) {
        this.rfpNegCompDays = rfpNegCompDays;
    }

    public Short getRfpTechEvalDays() {
        return rfpTechEvalDays;
    }

    public void setRfpTechEvalDays(Short rfpTechEvalDays) {
        this.rfpTechEvalDays = rfpTechEvalDays;
    }

    public Short getSubDays() {
        return subDays;
    }

    public void setSubDays(Short subDays) {
        this.subDays = subDays;
    }

    public Short getTechSubCmtRptDays() {
        return techSubCmtRptDays;
    }

    public void setTechSubCmtRptDays(Short techSubCmtRptDays) {
        this.techSubCmtRptDays = techSubCmtRptDays;
    }

    public Short getTenderAdvertDays() {
        return tenderAdvertDays;
    }

    public void setTenderAdvertDays(Short tenderAdvertDays) {
        this.tenderAdvertDays = tenderAdvertDays;
    }

    public Short getTenderContractAppDays() {
        return tenderContractAppDays;
    }

    public void setTenderContractAppDays(Short tenderContractAppDays) {
        this.tenderContractAppDays = tenderContractAppDays;
    }

    public Short getTenderContractSignDays() {
        return tenderContractSignDays;
    }

    public void setTenderContractSignDays(Short tenderContractSignDays) {
        this.tenderContractSignDays = tenderContractSignDays;
    }

    public Short getTenderEvalRptAppDays() {
        return tenderEvalRptAppDays;
    }

    public void setTenderEvalRptAppDays(Short tenderEvalRptAppDays) {
        this.tenderEvalRptAppDays = tenderEvalRptAppDays;
    }

    public Short getTenderEvalRptdays() {
        return tenderEvalRptdays;
    }

    public void setTenderEvalRptdays(Short tenderEvalRptdays) {
        this.tenderEvalRptdays = tenderEvalRptdays;
    }

    public Short getTenderNoaIssueDays() {
        return tenderNoaIssueDays;
    }

    public void setTenderNoaIssueDays(Short tenderNoaIssueDays) {
        this.tenderNoaIssueDays = tenderNoaIssueDays;
    }

    public Short getTenderOpenDays() {
        return tenderOpenDays;
    }

    public void setTenderOpenDays(Short tenderOpenDays) {
        this.tenderOpenDays = tenderOpenDays;
    }

    public Short getTenderSubDays() {
        return tenderSubDays;
    }

    public void setTenderSubDays(Short tenderSubDays) {
        this.tenderSubDays = tenderSubDays;
    }

    public String getEmeType() {
        return emeType;
    }

    public void setEmeType(String emeType) {
        this.emeType = emeType;
    }

    public String getCpvCode() {
        return cpvCode;
    }

    public void setCpvCode(String cpvCode) {
        this.cpvCode = cpvCode;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getPkgUrgency() {
        return pkgUrgency;
    }

    public void setPkgUrgency(String pkgUrgency) {
        this.pkgUrgency = pkgUrgency;
    }

    public String getCpvCategory() {
        return cpvCategory;
    }

    public void setCpvCategory(String cpvCategory) {
        this.cpvCategory = cpvCategory;
    }

    public String getMinistryDetails() {
        return ministryDetails;
    }

    public void setMinistryDetails(String ministryDetails) {
        this.ministryDetails = ministryDetails;
    }

    public String getProjectNameCode() {
        return projectNameCode;
    }

    public void setProjectNameCode(String projectNameCode) {
        this.projectNameCode = projectNameCode;
    }

    
    public String getBidderCategory() {
        return bidderCategory;
    }


    public void setBidderCategory(String bidderCategory) {
        this.bidderCategory = bidderCategory;
    }

 
    public String getWorkCategory() {
        return workCategory;
    }

    public void setWorkCategory(String workCategory) {
        this.workCategory = workCategory;
    }

    /**
     * @return the depoplanWork
     */
    public String getDepoplanWork() {
        return depoplanWork;
    }

    /**
     * @param depoplanWork the depoplanWork to set
     */
    public void setDepoplanWork(String depoplanWork) {
        this.depoplanWork = depoplanWork;
    }

    /**
     * @return the entrustingAgency
     */
    public String getEntrustingAgency() {
        return entrustingAgency;
    }

    /**
     * @param entrustingAgency the entrustingAgency to set
     */
    public void setEntrustingAgency(String entrustingAgency) {
        this.entrustingAgency = entrustingAgency;
    }

    /**
     * @return the timeFrame
     */
    public String getTimeFrame() {
        return timeFrame;
    }

    /**
     * @param timeFrame the timeFrame to set
     */
    public void setTimeFrame(String timeFrame) {
        this.timeFrame = timeFrame;
    }

    //Code by Proshanto Kumar Saha
    /**
     * @return the tnderLetterIntentDays
     */
    public Short getTnderLetterIntentDays() {
        return tnderLetterIntentDays;
    }

    /**
     * @param tnderLetterIntentDays the tnderLetterIntentDays to set
     */
    public void setTnderLetterIntentDays(Short tnderLetterIntentDays) {
        this.tnderLetterIntentDays = tnderLetterIntentDays;
    }

    /**
     * @return the tenderLetterIntentDt
     */
    public String getTenderLetterIntentDt() {
        return tenderLetterIntentDt;
    }

    /**
     * @param tenderLetterIntentDt the tenderLetterIntentDt to set
     */
    public void setTenderLetterIntentDt(String tenderLetterIntentDt) {
        this.tenderLetterIntentDt = tenderLetterIntentDt;
    }
    //Code End by Proshanto Kumar Saha

    /**
     * @return the actTenderLetterIntentDt
     */
    public String getActTenderLetterIntentDt() {
        return actTenderLetterIntentDt;
    }

    /**
     * @param actTenderLetterIntentDt the actTenderLetterIntentDt to set
     */
    public void setActTenderLetterIntentDt(String actTenderLetterIntentDt) {
        this.actTenderLetterIntentDt = actTenderLetterIntentDt;
    }
    

}