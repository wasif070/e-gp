package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTendererAuditTrail;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblTendererAuditTrailDao extends GenericDao<TblTendererAuditTrail> {

    public void addTblTendererAuditTrail(TblTendererAuditTrail audit);

    public void deleteTblTendererAuditTrail(TblTendererAuditTrail audit);

    public void updateTblTendererAuditTrail(TblTendererAuditTrail audit);

    public List<TblTendererAuditTrail> getAllTblTendererAuditTrail();

    public List<TblTendererAuditTrail> findTblTendererAuditTrail(Object... values) throws Exception;

    public List<TblTendererAuditTrail> findByCountTblTendererAuditTrail(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTendererAuditTrailCount();
}
