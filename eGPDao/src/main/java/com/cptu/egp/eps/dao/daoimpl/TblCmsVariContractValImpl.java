package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsVariContractValDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsVariContractVal;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsVariContractValImpl extends AbcAbstractClass<TblCmsVariContractVal> implements TblCmsVariContractValDao {

    @Override
    public void addTblCmsVariContractVal(TblCmsVariContractVal cmsVariContractVal){
        super.addEntity(cmsVariContractVal);
    }

    @Override
    public void deleteTblCmsVariContractVal(TblCmsVariContractVal cmsVariContractVal) {
        super.deleteEntity(cmsVariContractVal);
    }

    @Override
    public void updateTblCmsVariContractVal(TblCmsVariContractVal cmsVariContractVal) {
        super.updateEntity(cmsVariContractVal);
    }

    @Override
    public List<TblCmsVariContractVal> getAllTblCmsVariContractVal() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsVariContractVal> findTblCmsVariContractVal(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsVariContractValCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsVariContractVal> findByCountTblCmsVariContractVal(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsVariContractVal> estCost) {
       super.updateAll(estCost);
    }
}
