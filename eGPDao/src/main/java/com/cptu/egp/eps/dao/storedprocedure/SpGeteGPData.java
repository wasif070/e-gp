/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author TaherT
 */
public class SpGeteGPData extends StoredProcedure {

    private static final Logger LOGGER = Logger.getLogger(SpGeteGPData.class);
    public SpGeteGPData(BasicDataSource dataSource, String procName){

       this.setDataSource(dataSource);
       this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
       LOGGER.debug("Proc Name: "+ procName);
        this.declareParameter(new SqlParameter("@v_fieldName1Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName2Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName3Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName4Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName5Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName6Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName7Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName8Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName9Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName10Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName11Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName12Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName13Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName14Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName15Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName16Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName17Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName18Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName19Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName20Vc", Types.VARCHAR));
    }

    /**
     * Execute stored procedure : p_get_egpdata
     * Get Common data.
     * @param fieldName1
     * @param fieldName2
     * @param fieldName3
     * @param fieldName4
     * @param fieldName5
     * @param fieldName6
     * @param fieldName7
     * @param fieldName8
     * @param fieldName9
     * @param fieldName10
     * @param fieldName11
     * @param fieldName12
     * @param fieldName13
     * @param fieldName14
     * @param fieldName15
     * @param fieldName16
     * @param fieldName17
     * @param fieldName18
     * @param fieldName19
     * @param fieldName20
     * @return Result data as list of SPCommonSearchDataMore object
     */
    public List<SPCommonSearchDataMore> executeProcedure(String fieldName1, String fieldName2, String fieldName3, String fieldName4, String fieldName5, String fieldName6, String fieldName7, String fieldName8, String fieldName9, String fieldName10, String fieldName11, String fieldName12, String fieldName13, String fieldName14, String fieldName15, String fieldName16, String fieldName17, String fieldName18, String fieldName19, String fieldName20) {
        Map inParams = new HashMap();
        inParams.put("@v_fieldName1Vc",fieldName1);
        inParams.put("@v_fieldName2Vc",fieldName2);
        inParams.put("@v_fieldName3Vc",fieldName3);
        inParams.put("@v_fieldName4Vc",fieldName4);
        inParams.put("@v_fieldName5Vc",fieldName5);
        inParams.put("@v_fieldName6Vc",fieldName6);
        inParams.put("@v_fieldName7Vc",fieldName7);
        inParams.put("@v_fieldName8Vc",fieldName8);
        inParams.put("@v_fieldName9Vc",fieldName9);
        inParams.put("@v_fieldName10Vc",fieldName10);
        inParams.put("@v_fieldName11Vc",fieldName11);
        inParams.put("@v_fieldName12Vc",fieldName12);
        inParams.put("@v_fieldName13Vc",fieldName13);
        inParams.put("@v_fieldName14Vc",fieldName14);
        inParams.put("@v_fieldName15Vc",fieldName15);
        inParams.put("@v_fieldName16Vc",fieldName16);
        inParams.put("@v_fieldName17Vc",fieldName17);
        inParams.put("@v_fieldName18Vc",fieldName18);
        inParams.put("@v_fieldName19Vc",fieldName19);
        inParams.put("@v_fieldName20Vc",fieldName20);

        this.compile();

        List<SPCommonSearchDataMore> commonData = new ArrayList<SPCommonSearchDataMore>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            if (list != null && !list.isEmpty()) {
                for (LinkedHashMap<String, Object> linkedHashMap : list) {
                    SPCommonSearchDataMore sPCommonSearchDataMore = new SPCommonSearchDataMore();
                    sPCommonSearchDataMore.setFieldName1((String) linkedHashMap.get("FieldValue1"));
                    sPCommonSearchDataMore.setFieldName2((String) linkedHashMap.get("FieldValue2"));
                    sPCommonSearchDataMore.setFieldName3((String) linkedHashMap.get("FieldValue3"));
                    sPCommonSearchDataMore.setFieldName4((String) linkedHashMap.get("FieldValue4"));
                    sPCommonSearchDataMore.setFieldName5((String) linkedHashMap.get("FieldValue5"));
                    sPCommonSearchDataMore.setFieldName6((String) linkedHashMap.get("FieldValue6"));
                    sPCommonSearchDataMore.setFieldName7((String) linkedHashMap.get("FieldValue7"));
                    sPCommonSearchDataMore.setFieldName8((String) linkedHashMap.get("FieldValue8"));
                    sPCommonSearchDataMore.setFieldName9((String) linkedHashMap.get("FieldValue9"));
                    sPCommonSearchDataMore.setFieldName10((String) linkedHashMap.get("FieldValue10"));
                    sPCommonSearchDataMore.setFieldName11((String) linkedHashMap.get("FieldValue11"));
                    sPCommonSearchDataMore.setFieldName12((String) linkedHashMap.get("FieldValue12"));
                    sPCommonSearchDataMore.setFieldName13((String) linkedHashMap.get("FieldValue13"));
                    sPCommonSearchDataMore.setFieldName14((String) linkedHashMap.get("FieldValue14"));
                    sPCommonSearchDataMore.setFieldName15((String) linkedHashMap.get("FieldValue15"));
                    sPCommonSearchDataMore.setFieldName16((String) linkedHashMap.get("FieldValue16"));
                    sPCommonSearchDataMore.setFieldName17((String) linkedHashMap.get("FieldValue17"));
                    sPCommonSearchDataMore.setFieldName18((String) linkedHashMap.get("FieldValue18"));
                    sPCommonSearchDataMore.setFieldName19((String) linkedHashMap.get("FieldValue19"));
                    sPCommonSearchDataMore.setFieldName20((String) linkedHashMap.get("FieldValue20"));

                    commonData.add(sPCommonSearchDataMore);
                }
            }
            LOGGER.debug("SpGeteGPData : No data found for FUNCTION  " + fieldName1);
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
             LOGGER.error("SpGeteGPData: "+ sw.toString()+ " FUNCTION : "+fieldName1);
        }
        return  commonData;
    }
}

