package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblComplaintType;
import java.util.List;
import com.cptu.egp.eps.model.table.TblComplaintMaster;
public interface TblComplaintTypeDao  {

    public void addTblComplaintType(TblComplaintType complaintType);

   // public void deleteTblComplaintType(TblComplaintType complaintType) throws DataAccessException;

   // public void updateTblComplaintType(TblComplaintType complaintType);

    public List<TblComplaintType> getComplaintTypes();
    
    public TblComplaintMaster getComplaintDetails(int id);
    
    public List<TblComplaintType> getComplaintTypeById(Object... values) throws Exception;
    
   
}
