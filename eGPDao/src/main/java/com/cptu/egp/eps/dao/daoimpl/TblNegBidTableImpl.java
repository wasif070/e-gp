/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblNegBidTableDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblNegBidTable;
import java.util.List;


/**
 *
 * @author dipal.shah
 */
public class TblNegBidTableImpl extends AbcAbstractClass<TblNegBidTable> implements TblNegBidTableDao 
{
    @Override
    public void addTblNegBidTable(TblNegBidTable master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblNegBidTable(TblNegBidTable master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblNegBidTable(TblNegBidTable master) {
        super.updateEntity(master);
    }

}
