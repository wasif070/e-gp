/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblListBoxDetailDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblListBoxDetail;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblListBoxDetailImpl extends AbcAbstractClass<TblListBoxDetail> implements TblListBoxDetailDao{

    @Override
    public void addTblListBoxDetail(TblListBoxDetail tblListBoxDetail) {
        super.addEntity(tblListBoxDetail);
    }

    @Override
    public List<TblListBoxDetail> findTblListBoxDetail(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblListBoxDetail(TblListBoxDetail tblListBoxDetail) {

        super.deleteEntity(tblListBoxDetail);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblListBoxDetail(TblListBoxDetail tblListBoxDetail) {

        super.updateEntity(tblListBoxDetail);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblListBoxDetail> getAllTblListBoxDetail() {
        return super.getAllEntity();
    }

    @Override
    public long getTblListBoxDetailCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblListBoxDetail> findByCountTblListBoxDetail(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveAllListBoxDetail(List<TblListBoxDetail> listtblListBoxDetail) {
        super.updateAll(listtblListBoxDetail);
    }
}
