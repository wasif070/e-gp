/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTemplateSectionDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTemplateSectionDocs;
import java.util.List;

/**
 *
 * @author yanki
 */
public class TblTemplateSectionDocsImpl extends AbcAbstractClass<TblTemplateSectionDocs> implements TblTemplateSectionDocsDao{

    @Override
    public void addTblTemplateSectionDocs(TblTemplateSectionDocs templateSectionDocs) {
        super.addEntity(templateSectionDocs);
    }

    @Override
    public void deleteTblTemplateSectionDocs(TblTemplateSectionDocs templateSectionDocs) {
        super.deleteEntity(templateSectionDocs);
    }

    @Override
    public void updateTblTemplateSectionDocs(TblTemplateSectionDocs templateSectionDocs) {
        super.updateEntity(templateSectionDocs);
    }

    @Override
    public List<TblTemplateSectionDocs> getAllTblTemplateSectionDocs() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTemplateSectionDocs> findTblTemplateSectionDocs(Object... values) throws Exception {
        return super.findEntity(values);
    }

}
