/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

/**
 *
 * @author Administrator
 */
public class CommonMsgChk {

    private Boolean flag;
    private String Msg;
    private Integer Id;
    public String getMsg() {
        return Msg;
    }

    public void setMsg(String msg) {
        this.Msg = msg;
    }

    /**
     * @return the flag
     */
    public Boolean getFlag() {
        return flag;
    }
  
    /**
     * @param flag the flag to set
     */
    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    /**
     * @return the Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     * @param Id the Id to set
     */
    public void setId(Integer id) {
        this.Id = id;
    }




}
