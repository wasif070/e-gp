package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblSubContracting;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblSubContractingDao extends GenericDao<TblSubContracting> {

    public void addTblSubContracting(TblSubContracting tblObj);

    public void deleteTblSubContracting(TblSubContracting tblObj);

    public void updateTblSubContracting(TblSubContracting tblObj);

    public List<TblSubContracting> getAllTblSubContracting();

    public List<TblSubContracting> findTblSubContracting(Object... values) throws Exception;

    public List<TblSubContracting> findByCountTblSubContracting(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblSubContractingCount();
}
