/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTempBiddingPermissionDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTempBiddingPermission;
import java.util.List;

public class TblTempBiddingPermissionImpl extends AbcAbstractClass<TblTempBiddingPermission> implements TblTempBiddingPermissionDao{
    @Override
    public void addTblTempBiddingPermission(TblTempBiddingPermission master){
            super.addEntity(master);
    }
    @Override
    public void deleteTblTempBiddingPermission(TblTempBiddingPermission master) {
            super.deleteEntity(master);
    }
    @Override
    public void updateTblTempBiddingPermission(TblTempBiddingPermission master) {
            super.updateEntity(master);
    }
    @Override
    public List<TblTempBiddingPermission> getAllTblTempBiddingPermission() {
            return super.getAllEntity();
    }
    @Override
    public List<TblTempBiddingPermission> findTblTempBiddingPermission(Object... values) throws Exception {
            return super.findEntity(values);
    }
    @Override
    public long getTblTempBiddingPermissionCount() {
            return super.getEntityCount();
    }
    @Override
    public List<TblTempBiddingPermission> findByCountTblTempBiddingPermission(int firstResult, int maxResult, Object... values) throws Exception {
            return super.findByCountEntity(firstResult, maxResult, values);
    }
    @Override
    public void updateOrSaveTblTempBiddingPermission(List<TblTempBiddingPermission> tblObj) {
            super.updateAll(tblObj);
    }
}
