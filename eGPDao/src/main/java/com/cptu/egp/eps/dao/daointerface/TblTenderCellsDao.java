/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderCells;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderCellsDao extends GenericDao<TblTenderCells>{

    public void addTblTenderCells(TblTenderCells tblObj);

    public void deleteTblTenderCells(TblTenderCells tblObj);

    public void updateTblTenderCells(TblTenderCells tblObj);

    public List<TblTenderCells> getAllTblTenderCells();

    public List<TblTenderCells> findTblTenderCells(Object... values) throws Exception;

    public List<TblTenderCells> findByCountTblTenderCells(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderCellsCount();
}
