package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblNewsMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblNewsMasterDao extends GenericDao<TblNewsMaster> {

    public void addTblNewsMaster(TblNewsMaster tblObj);

    public void deleteTblNewsMaster(TblNewsMaster tblObj);

    public void updateTblNewsMaster(TblNewsMaster tblObj);

    public List<TblNewsMaster> getAllTblNewsMaster();

    public List<TblNewsMaster> findTblNewsMaster(Object... values) throws Exception;

    public List<TblNewsMaster> findByCountTblNewsMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblNewsMasterCount();
}
