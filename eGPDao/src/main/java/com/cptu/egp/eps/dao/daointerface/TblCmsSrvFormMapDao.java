package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvFormMap;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCmsSrvFormMapDao extends GenericDao<TblCmsSrvFormMap> {

    public void addTblCmsSrvFormMap(TblCmsSrvFormMap event);

    public void deleteTblCmsSrvFormMap(TblCmsSrvFormMap event);

    public void updateTblCmsSrvFormMap(TblCmsSrvFormMap event);

    public List<TblCmsSrvFormMap> getAllTblCmsSrvFormMap();

    public List<TblCmsSrvFormMap> findTblCmsSrvFormMap(Object... values) throws Exception;

    public List<TblCmsSrvFormMap> findByCountTblCmsSrvFormMap(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsSrvFormMapCount();

    public void updateOrSaveEstCost(List<TblCmsSrvFormMap> maps);
}
