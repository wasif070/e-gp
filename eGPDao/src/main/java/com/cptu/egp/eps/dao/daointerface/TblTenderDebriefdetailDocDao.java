package com.cptu.egp.eps.dao.daointerface;


import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderDebriefdetailDoc;
import java.util.List;


	public interface TblTenderDebriefdetailDocDao extends GenericDao<TblTenderDebriefdetailDoc> {

	public void addTblTenderDebriefdetailDoc(TblTenderDebriefdetailDoc tblObj);

	public void deleteTblTenderDebriefdetailDoc(TblTenderDebriefdetailDoc tblObj);

	public void updateTblTenderDebriefdetailDoc(TblTenderDebriefdetailDoc tblObj);

	public List<TblTenderDebriefdetailDoc> getAllTblTenderDebriefdetailDoc();

	public List<TblTenderDebriefdetailDoc> findTblTenderDebriefdetailDoc(Object... values) throws Exception;

	public List<TblTenderDebriefdetailDoc> findByCountTblTenderDebriefdetailDoc(int firstResult,int maxResult,Object... values) throws Exception;

	public long getTblTenderDebriefdetailDocCount();

	public void updateOrSaveTblTenderDebriefdetailDoc(List<TblTenderDebriefdetailDoc> tblObj);
}