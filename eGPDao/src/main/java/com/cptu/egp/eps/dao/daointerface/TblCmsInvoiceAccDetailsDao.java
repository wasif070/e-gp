package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsInvoiceAccDetails;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCmsInvoiceAccDetailsDao extends GenericDao<TblCmsInvoiceAccDetails> {

    public void addTblCmsInvoiceAccDetails(TblCmsInvoiceAccDetails event);

    public void deleteTblCmsInvoiceAccDetails(TblCmsInvoiceAccDetails event);

    public void updateTblCmsInvoiceAccDetails(TblCmsInvoiceAccDetails event);

    public List<TblCmsInvoiceAccDetails> getAllTblCmsInvoiceAccDetails();

    public List<TblCmsInvoiceAccDetails> findTblCmsInvoiceAccDetails(Object... values) throws Exception;

    public List<TblCmsInvoiceAccDetails> findByCountTblCmsInvoiceAccDetails(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsInvoiceAccDetailsCount();
}
