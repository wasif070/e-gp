package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTempTendererEsignature;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblTempTendererEsignatureDao extends GenericDao<TblTempTendererEsignature> {

    public void addTblTempTendererEsignature(TblTempTendererEsignature tenderer);

    public void deleteTblTempTendererEsignature(TblTempTendererEsignature tenderer);

    public void updateTblTempTendererEsignature(TblTempTendererEsignature tenderer);

    public List<TblTempTendererEsignature> getAllTblTempTendererEsignature();

    public List<TblTempTendererEsignature> findTblTempTendererEsignature(Object... values) throws Exception;

    public List<TblTempTendererEsignature> findByCountTblTempTendererEsignature(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTempTendererEsignatureCount();
}
