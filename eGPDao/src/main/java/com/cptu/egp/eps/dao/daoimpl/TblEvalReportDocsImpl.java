/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblEvalReportDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalReportDocs;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblEvalReportDocsImpl extends AbcAbstractClass<TblEvalReportDocs> implements TblEvalReportDocsDao{

    @Override
    public void addTblEvalReportDocs(TblEvalReportDocs evalRptDocs) {
        super.addEntity(evalRptDocs);
    }

    @Override
    public List<TblEvalReportDocs> findTblEvalReportDocs(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblEvalReportDocs(TblEvalReportDocs department) {

        super.deleteEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblEvalReportDocs(Collection event) {
        super.deleteAll(event);
    }

    @Override
    public void updateTblEvalReportDocs(TblEvalReportDocs department) {

        super.updateEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblEvalReportDocs> getAllTblEvalReportDocs() {
        return super.getAllEntity();
    }

    @Override
    public long getTblEvalReportDocsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalReportDocs> findByCountTblEvalReportDocs(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
