/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblEvalReportMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalReportMaster;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblEvalReportMasterImpl extends AbcAbstractClass<TblEvalReportMaster> implements TblEvalReportMasterDao{

    @Override
    public void addTblEvalReportMaster(TblEvalReportMaster tblEvalReportMasterImpl) {
        super.addEntity(tblEvalReportMasterImpl);
    }

    @Override
    public List<TblEvalReportMaster> findTblEvalReportMaster(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblEvalReportMaster(TblEvalReportMaster tblEvalReportMasterImpl) {

        super.deleteEntity(tblEvalReportMasterImpl);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblEvalReportMaster(Collection tblEvalReportMasterImpl) {
        super.deleteAll(tblEvalReportMasterImpl);
    }

    @Override
    public void updateTblEvalReportMaster(TblEvalReportMaster tblEvalReportMasterImpl) {

        super.updateEntity(tblEvalReportMasterImpl);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblEvalReportMaster> getAllTblEvalReportMaster() {
        return super.getAllEntity();
    }

    @Override
    public long getTblEvalReportMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalReportMaster> findByCountTblEvalReportMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
