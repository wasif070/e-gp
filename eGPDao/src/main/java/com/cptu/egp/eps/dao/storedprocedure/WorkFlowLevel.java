/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Ramesh.Janagondakuru
 */
public class WorkFlowLevel extends StoredProcedure{
    private static final Logger LOGGER = Logger.getLogger(WorkFlowLevel.class);
    public WorkFlowLevel(BasicDataSource dataSource, String procName){
         this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("v_EventId_insInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_ObjectId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_ActionDate_insDt", Types.DATE));
        this.declareParameter(new SqlParameter("v_ActivityId_insInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_ChildId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_FileOnHand_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Action_inVct", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_worfklowId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_actionBy_inVc", Types.VARCHAR));
         this.declareParameter(new SqlParameter("v_WfLevel_inVc", Types.VARCHAR));
         this.declareParameter(new SqlParameter("v_UserId_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_WfRoleId_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Procurementroleid_inVc",Types.VARCHAR));

    }

    /**
     * Execute stored procedure : p_ins_upd_workflowlevelconfig
     * Perform Insert/Update for work flow level config.
     * @param eventId
     * @param objectId
     * @param actionDate
     * @param activityId
     * @param childId
     * @param fileOnHand
     * @param action
     * @param worfklowId
     * @param actionBy
     * @param wfLevel
     * @param userId
     * @param wfRoleId
     * @param procurementroleid
     * @return CommonMsgChk list contains flags indicate operation successful or not.
     */
 public List<CommonMsgChk> executeProcedure(Integer eventId,Integer objectId,Date actionDate,Integer activityId,Integer childId,String  fileOnHand,String action,Integer worfklowId,String actionBy,String wfLevel,String userId,String wfRoleId,String procurementroleid ){
     Map inParams = new HashMap();
        inParams.put("v_EventId_insInt", eventId);
        inParams.put("v_ObjectId_inInt", objectId);
        inParams.put("v_ActionDate_insDt", actionDate);
        inParams.put("v_ActivityId_insInt", activityId);
        inParams.put("v_ChildId_inInt", childId);
        inParams.put("v_FileOnHand_inVc", fileOnHand);
        inParams.put("v_Action_inVct",action);
        inParams.put("v_worfklowId_inInt", worfklowId );
        inParams.put("v_actionBy_inVc", actionBy);
        inParams.put("v_WfLevel_inVc",wfLevel);
         inParams.put("v_UserId_inVc", userId);
          inParams.put("v_WfRoleId_inVc", wfRoleId);
          inParams.put("v_Procurementroleid_inVc",procurementroleid );
       this.compile();
        List<CommonMsgChk> details = new ArrayList<CommonMsgChk>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonMsgChk commonMsgChk = new CommonMsgChk();
                commonMsgChk.setFlag((Boolean) linkedHashMap.get("flag"));
                commonMsgChk.setMsg((String) linkedHashMap.get("msg"));
                details.add(commonMsgChk);
            }
        } catch (Exception e) {
            LOGGER.error("WorkFlowLevel  : "+ e);
        }
        return details;
 }
}
