package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsPrMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsPrMaster;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsPrMasterImpl extends AbcAbstractClass<TblCmsPrMaster> implements TblCmsPrMasterDao {

    @Override
    public void addTblCmsPrMaster(TblCmsPrMaster cmsPrMaster){
        super.addEntity(cmsPrMaster);
    }

    @Override
    public void deleteTblCmsPrMaster(TblCmsPrMaster cmsPrMaster) {
        super.deleteEntity(cmsPrMaster);
    }

    @Override
    public void updateTblCmsPrMaster(TblCmsPrMaster cmsPrMaster) {
        super.updateEntity(cmsPrMaster);
    }

    @Override
    public List<TblCmsPrMaster> getAllTblCmsPrMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsPrMaster> findTblCmsPrMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsPrMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsPrMaster> findByCountTblCmsPrMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsPrMaster> estCost) {
         super.updateAll(estCost);
    }
}
