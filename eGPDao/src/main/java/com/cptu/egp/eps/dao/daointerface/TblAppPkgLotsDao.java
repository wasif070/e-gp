package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblAppPkgLots;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblAppPkgLotsDao extends GenericDao<TblAppPkgLots> {

    public void addTblAppPkgLots(TblAppPkgLots tblObj);

    public void deleteTblAppPkgLots(TblAppPkgLots tblObj);

    public void updateTblAppPkgLots(TblAppPkgLots tblObj);

    public List<TblAppPkgLots> getAllTblAppPkgLots();

    public List<TblAppPkgLots> findTblAppPkgLots(Object... values) throws Exception;

    public List<TblAppPkgLots> findByCountTblAppPkgLots(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblAppPkgLotsCount();
}
