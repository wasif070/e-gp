package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblProjectOffice;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblProjectOfficeDao extends GenericDao<TblProjectOffice> {

    public void addTblProjectOffice(TblProjectOffice tblObj);

    public void deleteTblProjectOffice(TblProjectOffice tblObj);

    public void updateTblProjectOffice(TblProjectOffice tblObj);

    public List<TblProjectOffice> getAllTblProjectOffice();

    public List<TblProjectOffice> findTblProjectOffice(Object... values) throws Exception;

    public List<TblProjectOffice> findByCountTblProjectOffice(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblProjectOfficeCount();
}
