package com.cptu.egp.eps.dao.daointerface;


import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTendercorriForms;
import java.util.List;


	public interface TblTendercorriFormsDao extends GenericDao<TblTendercorriForms> {

	public void addTblTendercorriForms(TblTendercorriForms tblObj);

	public void deleteTblTendercorriForms(TblTendercorriForms tblObj);

	public void updateTblTendercorriForms(TblTendercorriForms tblObj);

	public List<TblTendercorriForms> getAllTblTendercorriForms();

	public List<TblTendercorriForms> findTblTendercorriForms(Object... values) throws Exception;

	public List<TblTendercorriForms> findByCountTblTendercorriForms(int firstResult,int maxResult,Object... values) throws Exception;

	public long getTblTendercorriFormsCount();

	public void updateOrSaveTblTendercorriForms(List<TblTendercorriForms> tblObj);
}