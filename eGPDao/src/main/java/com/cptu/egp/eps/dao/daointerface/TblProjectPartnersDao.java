package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblProjectPartners;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblProjectPartnersDao extends GenericDao<TblProjectPartners> {

    public void addTblProjectPartners(TblProjectPartners tblObj);

    public void deleteTblProjectPartners(TblProjectPartners tblObj);

    public void updateTblProjectPartners(TblProjectPartners tblObj);

    public List<TblProjectPartners> getAllTblProjectPartners();

    public List<TblProjectPartners> findTblProjectPartners(Object... values) throws Exception;

    public List<TblProjectPartners> findByCountTblProjectPartners(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblProjectPartnersCount();
}
