package com.cptu.egp.eps.dao.daointerface;

import java.util.List;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblComplaintDocs;

public interface TblComplaintDocsDao  extends GenericDao<TblComplaintDocs> {

    public void saveDoc(TblComplaintDocs complaintDoc)throws Exception;

    public void deleteTblComplaintDocs(TblComplaintDocs complaintDoc) throws Exception;

   // public void updateTblComplaintDocs(TblComplaintDocs complaintDoc);

    public List<TblComplaintDocs> getComplaintDocs(Object... values) throws Exception;
    
    public   List<Object[]> getDocs(int complaintId,int govId) throws Exception;    

    public   List<Object[]> getDocsByComplaintId(int complaintId) throws Exception;

   // public List<TblComplaintDocs> findComplaintDocs(Object... values) throws Exception;

   // public List<TblComplaintDocs> findByCountComplaintDocs(int firstResult,int maxResult,Object... values) throws Exception;

   // public long getTblComplaintDocsCount();
    
    public List<Object[]>  getDocsProcessList(int id,int levelId,int uploadedBy) throws Exception;
}
