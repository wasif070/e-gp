/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.model.table.TblContractAwardedOffline;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TblContractAwardedOfflineDao {

     public void addTblContractAwardedOffline(TblContractAwardedOffline tblObj);

    public void deleteTblContractAwardedOffline(TblContractAwardedOffline tblObj);

    public void updateTblContractAwardedOffline(TblContractAwardedOffline tblObj);

    public List<TblContractAwardedOffline> getAllTblContractAwardedOffline();

    public List<TblContractAwardedOffline> findTblContractAwardedOffline(Object... values) throws Exception;

    public List<TblContractAwardedOffline> findByCountTblContractAwardedOffline(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblContractAwardedOfflineCount();

}
