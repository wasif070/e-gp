package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsTrackVariation;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsTrackVariationDao extends GenericDao<TblCmsTrackVariation> {

    public void addTblCmsTrackVariation(TblCmsTrackVariation event);

    public void deleteTblCmsTrackVariation(TblCmsTrackVariation event);

    public void updateTblCmsTrackVariation(TblCmsTrackVariation event);

    public List<TblCmsTrackVariation> getAllTblCmsTrackVariation();

    public List<TblCmsTrackVariation> findTblCmsTrackVariation(Object... values) throws Exception;

    public List<TblCmsTrackVariation> findByCountTblCmsTrackVariation(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsTrackVariationCount();

    public void updateOrSaveEstCost(List<TblCmsTrackVariation> estCost);
}
