package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblHolidayMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblHolidayMasterDao extends GenericDao<TblHolidayMaster> {

    public void addTblHolidayMaster(TblHolidayMaster tblObj);

    public void deleteTblHolidayMaster(TblHolidayMaster tblObj);

    public void updateTblHolidayMaster(TblHolidayMaster tblObj);

    public List<TblHolidayMaster> getAllTblHolidayMaster();

    public List<TblHolidayMaster> findTblHolidayMaster(Object... values) throws Exception;

    public List<TblHolidayMaster> findByCountTblHolidayMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblHolidayMasterCount();
}
