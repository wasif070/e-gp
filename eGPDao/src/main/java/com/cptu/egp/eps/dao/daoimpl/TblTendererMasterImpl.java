package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblTendererMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTendererMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblTendererMasterImpl extends AbcAbstractClass<TblTendererMaster> implements TblTendererMasterDao {

    @Override
    public void addTblTendererMaster(TblTendererMaster master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblTendererMaster(TblTendererMaster master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblTendererMaster(TblTendererMaster master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblTendererMaster> getAllTblTendererMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTendererMaster> findTblTendererMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTendererMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTendererMaster> findByCountTblTendererMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
