/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsVendorRatingMaster;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public interface TblCmsVendorRatingMasterDao extends GenericDao<TblCmsVendorRatingMaster> {

    public void addTblCmsVendorRatingMaster(TblCmsVendorRatingMaster tblCmsVendorRatingMaster);

    public void deleteTblCmsVendorRatingMaster(TblCmsVendorRatingMaster tblCmsVendorRatingMaster);

    public void updateTblCmsVendorRatingMaster(TblCmsVendorRatingMaster tblCmsVendorRatingMaster);

    public List<TblCmsVendorRatingMaster> getAllTblCmsVendorRatingMaster();

    public List<TblCmsVendorRatingMaster> findTblCmsVendorRatingMaster(Object... values) throws Exception;

    public List<TblCmsVendorRatingMaster> findByCountTblCmsVendorRatingMaster(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsVendorRatingMasterCount();
}
