/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;
import com.cptu.egp.eps.dao.generic.GenericDao;
import java.util.List;
import com.cptu.egp.eps.model.table.TblBiddingPermission;
/**
 *
 * @author HP
 */
public interface TblBiddingPermissionDao extends GenericDao<TblBiddingPermission> {
    public void addTblBiddingPermission(TblBiddingPermission tblObj);

    public void deleteTblBiddingPermission(TblBiddingPermission tblObj);

    public void updateTblBiddingPermission(TblBiddingPermission tblObj);

    public List<TblBiddingPermission> getAllTblBiddingPermission();

    public List<TblBiddingPermission> findTblBiddingPermission(Object... values) throws Exception;

    public List<TblBiddingPermission> findByCountTblBiddingPermission(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblBiddingPermissionCount();

    public void updateOrSaveTblBiddingPermission(List<TblBiddingPermission> tblObj);
}
