/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderTablesDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderTables;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderTablesImpl extends AbcAbstractClass<TblTenderTables> implements TblTenderTablesDao {

    @Override
    public void addTblTenderTables(TblTenderTables tenderTables) {
        super.addEntity(tenderTables);
    }

    @Override
    public void deleteTblTenderTables(TblTenderTables tenderTables) {
        super.deleteEntity(tenderTables);
    }

    @Override
    public void updateTblTenderTables(TblTenderTables tenderTables) {
        super.updateEntity(tenderTables);
    }

    @Override
    public List<TblTenderTables> getAllTblTenderTables() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderTables> findTblTenderTables(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderTablesCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderTables> findByCountTblTenderTables(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
