/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblBhutanDebarmentCommitteeDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblBhutanDebarmentCommittee;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author feroz
 */
public class TblBhutanDebarmentCommitteeImpl extends AbcAbstractClass<TblBhutanDebarmentCommittee> implements TblBhutanDebarmentCommitteeDao{

    @Override
    public void addTblBhutanDebarmentCommittee(TblBhutanDebarmentCommittee tblBhutanDebarmentCommittee) {
        super.addEntity(tblBhutanDebarmentCommittee);
    }

    @Override
    public void deleteTblBhutanDebarmentCommittee(TblBhutanDebarmentCommittee tblBhutanDebarmentCommittee) {
        super.deleteEntity(tblBhutanDebarmentCommittee);
    }

    @Override
    public void updateTblBhutanDebarmentCommittee(TblBhutanDebarmentCommittee tblBhutanDebarmentCommittee) {
        super.updateEntity(tblBhutanDebarmentCommittee);
    }

    @Override
    public List<TblBhutanDebarmentCommittee> getAllTblBhutanDebarmentCommittee() {
        return super.getAllEntity();
    }

    @Override
    public List<TblBhutanDebarmentCommittee> findTblBhutanDebarmentCommittee(Object... values) {
        List<TblBhutanDebarmentCommittee> tblBhutanDebarmentCommittee = null;
        try {
            tblBhutanDebarmentCommittee = super.findEntity(values);
        } catch (Exception ex) {
            Logger.getLogger(TblConfigPaThresholdImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tblBhutanDebarmentCommittee;
    }
    
}
