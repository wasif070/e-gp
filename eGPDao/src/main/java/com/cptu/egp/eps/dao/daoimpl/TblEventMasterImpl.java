package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblEventMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEventMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblEventMasterImpl extends AbcAbstractClass<TblEventMaster> implements TblEventMasterDao {

    @Override
    public void addTblEventMaster(TblEventMaster master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblEventMaster(TblEventMaster master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblEventMaster(TblEventMaster master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblEventMaster> getAllTblEventMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEventMaster> findTblEventMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEventMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEventMaster> findByCountTblEventMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
