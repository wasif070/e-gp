package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalTernotifyMember;
import java.util.List;

/**
 *
 * @author nishith
 */
public interface TblEvalTernotifyMemberDao extends GenericDao<TblEvalTernotifyMember> {

    public void addTblEvalTernotifyMember(TblEvalTernotifyMember tblObj);

    public void deleteTblEvalTernotifyMember(TblEvalTernotifyMember tblObj);

    public void updateTblEvalTernotifyMember(TblEvalTernotifyMember tblObj);

    public List<TblEvalTernotifyMember> getAllTblEvalTernotifyMember();

    public List<TblEvalTernotifyMember> findTblEvalTernotifyMember(Object... values) throws Exception;

    public List<TblEvalTernotifyMember> findByCountTblEvalTernotifyMember(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalTernotifyMemberCount();
}
