/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderDocStatisticsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderDocStatistics;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class TblTenderDocStatisticsImpl extends AbcAbstractClass<TblTenderDocStatistics> implements TblTenderDocStatisticsDao {

    @Override
    public void addTblTenderDocStatistics(TblTenderDocStatistics tenderDocStatistics){
        super.addEntity(tenderDocStatistics);
    }

    @Override
    public void deleteTblTenderDocStatistics(TblTenderDocStatistics tenderDocStatistics) {
        super.deleteEntity(tenderDocStatistics);
    }

    @Override
    public void updateTblTenderDocStatistics(TblTenderDocStatistics tenderDocStatistics) {
        super.updateEntity(tenderDocStatistics);
    }

    @Override
    public List<TblTenderDocStatistics> getAllTblTenderDocStatistics() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderDocStatistics> findTblTenderDocStatistics(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderDocStatisticsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderDocStatistics> findByCountTblTenderDocStatistics(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
