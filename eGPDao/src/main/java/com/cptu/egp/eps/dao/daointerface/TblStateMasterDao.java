package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblStateMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblStateMasterDao extends GenericDao<TblStateMaster> {

    public List<TblStateMaster> getAllTblStateMaster();

    public List<TblStateMaster> findTblStateMaster(Object... values) throws Exception;

    public List<TblStateMaster> findByCountTblStateMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblStateMasterCount();
}
