/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblNoaAcceptanceDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblNoaAcceptance;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblNoaAcceptanceImpl extends AbcAbstractClass<TblNoaAcceptance> implements TblNoaAcceptanceDao{

    @Override
    public void addTblNoaAcceptance(TblNoaAcceptance noaAcceptance) {
        super.addEntity(noaAcceptance);
    }

    @Override
    public List<TblNoaAcceptance> findTblNoaAcceptance(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblNoaAcceptance(TblNoaAcceptance noaAcceptance) {

        super.deleteEntity(noaAcceptance);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblNoaAcceptance(Collection noaAcceptance) {
        super.deleteAll(noaAcceptance);
    }

    @Override
    public void updateTblNoaAcceptance(TblNoaAcceptance noaAcceptance) {

        super.updateEntity(noaAcceptance);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblNoaAcceptance> getAllTblNoaAcceptance() {
        return super.getAllEntity();
    }

    @Override
    public long getTblNoaAcceptanceCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblNoaAcceptance> findByCountTblNoaAcceptance(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
