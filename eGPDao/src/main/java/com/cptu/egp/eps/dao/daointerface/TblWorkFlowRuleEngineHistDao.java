/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblWorkFlowRuleEngineHist;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public interface TblWorkFlowRuleEngineHistDao extends GenericDao<TblWorkFlowRuleEngineHist> {

    public void addTblWorkflowRuleEngineHist(TblWorkFlowRuleEngineHist admin);

    public void deleteTblWorkflowRuleEngineHist(TblWorkFlowRuleEngineHist admin);

    public void updateTblWorkflowRuleEngineHist(TblWorkFlowRuleEngineHist admin);

    public List<TblWorkFlowRuleEngineHist> getAllTblWorkflowRuleEngineHist();

    public List<TblWorkFlowRuleEngineHist> findTblWorkflowRuleEngineHist(Object... values) throws Exception;

    public List<TblWorkFlowRuleEngineHist> findByCountTblWorkflowRuleEngineHist(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblWorkflowRuleEngineHistCount();


}
