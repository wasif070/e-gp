package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblEvalSerFormDetailDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalSerFormDetail;
import java.util.List;

/**
 *
 * @author rishita
 */
public class TblEvalSerFormDetailImpl extends AbcAbstractClass<TblEvalSerFormDetail> implements TblEvalSerFormDetailDao {

    @Override
    public void addTblEvalSerFormDetail(TblEvalSerFormDetail tblEvalSerFormDetail){
        super.addEntity(tblEvalSerFormDetail);
    }

    @Override
    public void deleteTblEvalSerFormDetail(TblEvalSerFormDetail tblEvalSerFormDetail) {
        super.deleteEntity(tblEvalSerFormDetail);
    }

    @Override
    public void updateTblEvalSerFormDetail(TblEvalSerFormDetail tblEvalSerFormDetail) {
        super.updateEntity(tblEvalSerFormDetail);
    }

    @Override
    public List<TblEvalSerFormDetail> getAllTblEvalSerFormDetail() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEvalSerFormDetail> findTblEvalSerFormDetail(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEvalSerFormDetailCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalSerFormDetail> findByCountTblEvalSerFormDetail(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateInsertAllEvalSerFormDetail(List<TblEvalSerFormDetail> list) {
        super.updateAll(list);
    }
}
