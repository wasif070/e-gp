/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCompanyMaster;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TblCompanyMasterDao extends GenericDao<TblCompanyMaster>{

    public void addTblCompanyMaster(TblCompanyMaster tblObj);

    public void deleteTblCompanyMaster(TblCompanyMaster tblObj);

    public void updateTblCompanyMaster(TblCompanyMaster tblObj);

    public List<TblCompanyMaster> getAllTblCompanyMaster();

    public List<TblCompanyMaster> findTblCompanyMaster(Object... values) throws Exception;

    public List<TblCompanyMaster> findByCountTblCompanyMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCompanyMasterCount();
}
