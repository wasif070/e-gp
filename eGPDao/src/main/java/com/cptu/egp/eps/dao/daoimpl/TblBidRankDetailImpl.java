package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblBidRankDetailDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblBidRankDetail;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblBidRankDetailImpl extends AbcAbstractClass<TblBidRankDetail> implements TblBidRankDetailDao {

    @Override
    public void addTblBidRankDetail(TblBidRankDetail bidRankDetail){
        super.addEntity(bidRankDetail);
    }

    @Override
    public void deleteTblBidRankDetail(TblBidRankDetail bidRankDetail) {
        super.deleteEntity(bidRankDetail);
    }

    @Override
    public void updateTblBidRankDetail(TblBidRankDetail bidRankDetail) {
        super.updateEntity(bidRankDetail);
    }

    @Override
    public List<TblBidRankDetail> getAllTblBidRankDetail() {
        return super.getAllEntity();
    }

    @Override
    public List<TblBidRankDetail> findTblBidRankDetail(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblBidRankDetailCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblBidRankDetail> findByCountTblBidRankDetail(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveBidRankDetail(List<TblBidRankDetail> bidRankDetails) {
        super.updateAll(bidRankDetails);
    }
}
