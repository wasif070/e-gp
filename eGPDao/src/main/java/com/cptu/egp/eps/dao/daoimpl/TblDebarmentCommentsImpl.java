package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblDebarmentCommentsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblDebarmentComments;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblDebarmentCommentsImpl extends AbcAbstractClass<TblDebarmentComments> implements TblDebarmentCommentsDao {

    @Override
    public void addTblDebarmentComments(TblDebarmentComments debarmentComments){
        super.addEntity(debarmentComments);
    }

    @Override
    public void deleteTblDebarmentComments(TblDebarmentComments debarmentComments) {
        super.deleteEntity(debarmentComments);
    }

    @Override
    public void updateTblDebarmentComments(TblDebarmentComments debarmentComments) {
        super.updateEntity(debarmentComments);
    }

    @Override
    public List<TblDebarmentComments> getAllTblDebarmentComments() {
        return super.getAllEntity();
    }

    @Override
    public List<TblDebarmentComments> findTblDebarmentComments(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblDebarmentCommentsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblDebarmentComments> findByCountTblDebarmentComments(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
