/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblPromisIndicator;
import java.util.List;

/**
 *
 * @author Sudhir Chavhan
 * 
 */
public interface TblPromisIndicatorDao extends GenericDao<TblPromisIndicator>{

    public void addTblPromisIndicator(TblPromisIndicator admin);
/*
    public void deleteTblPromisIndicator(TblPromisIndicator admin);

    public void updateTblPromisIndicator(TblPromisIndicator admin);   */

    public List<TblPromisIndicator> getAllTblPromisIndicator();
/*
    public List<TblPromisIndicator> findTblPromisIndicator(Object... values) throws Exception;

    public List<TblPromisIndicator> findByCountTblPromisIndicator(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblPromisIndicatorCount();
*/
}
