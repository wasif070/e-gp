/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;


import com.cptu.egp.eps.dao.daointerface.TblCommitteeDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCommittee;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblCommitteeImpl extends AbcAbstractClass<TblCommittee> implements TblCommitteeDao {

    @Override
    public List<TblCommittee> getAllTblCommittee() {
        return super.getAllEntity();
    }

    @Override
    public void addTblCommittee(TblCommittee tblCommittee) {
        super.addEntity(tblCommittee);
    }

    @Override
    public void deleteTblCommittee(TblCommittee tblCommittee) {
        super.deleteEntity(tblCommittee);       
    }

    @Override
    public void updateTblCommittee(TblCommittee tblCommittee) {
        super.updateEntity(tblCommittee);       
    }

    @Override
    public List<TblCommittee> findTblCommittee(Object... values) throws Exception {
        return super.findEntity(values);        
    }

    @Override
    public List<TblCommittee> findByCountTblCommittee(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);       
    }

    @Override
    public long getTblCommitteeCount() {
        return super.getEntityCount();       
    }
}
