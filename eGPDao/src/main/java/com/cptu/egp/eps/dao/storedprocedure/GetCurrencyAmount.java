/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.math.BigDecimal;

/**
 *
 * @author salahuddin
 */
public class GetCurrencyAmount {

    private Double value1;
    private Double value2;
    private Double value3;
    private Double value4;

    private String currencyName1;
    private String currencyName2;
    private String currencyName3;
    private String currencyName4;

    private int currencyID1;
    private int currencyID2;
    private int currencyID3;
    private int currencyID4;

    /**
     * @return the value1
     */
    public Double getValue1() {
        return value1;
    }

    /**
     * @param value1 the value1 to set
     */
    public void setValue1(Double value1) {
        this.value1 = value1;
    }

    /**
     * @return the value2
     */
    public Double getValue2() {
        return value2;
    }

    /**
     * @param value2 the value2 to set
     */
    public void setValue2(Double value2) {
        this.value2 = value2;
    }

    /**
     * @return the value3
     */
    public Double getValue3() {
        return value3;
    }

    /**
     * @param value3 the value3 to set
     */
    public void setValue3(Double value3) {
        this.value3 = value3;
    }

    /**
     * @return the value4
     */
    public Double getValue4() {
        return value4;
    }

    /**
     * @param value4 the value4 to set
     */
    public void setValue4(Double value4) {
        this.value4 = value4;
    }

    /**
     * @return the currencyName1
     */
    public String getCurrencyName1() {
        return currencyName1;
    }

    /**
     * @param currencyName1 the currencyName1 to set
     */
    public void setCurrencyName1(String currencyName1) {
        this.currencyName1 = currencyName1;
    }

    /**
     * @return the currencyName2
     */
    public String getCurrencyName2() {
        return currencyName2;
    }

    /**
     * @param currencyName2 the currencyName2 to set
     */
    public void setCurrencyName2(String currencyName2) {
        this.currencyName2 = currencyName2;
    }

    /**
     * @return the currencyName3
     */
    public String getCurrencyName3() {
        return currencyName3;
    }

    /**
     * @param currencyName3 the currencyName3 to set
     */
    public void setCurrencyName3(String currencyName3) {
        this.currencyName3 = currencyName3;
    }

    /**
     * @return the currencyName4
     */
    public String getCurrencyName4() {
        return currencyName4;
    }

    /**
     * @param currencyName4 the currencyName4 to set
     */
    public void setCurrencyName4(String currencyName4) {
        this.currencyName4 = currencyName4;
    }

    /**
     * @return the currencyID1
     */
    public int getCurrencyID1() {
        return currencyID1;
    }

    /**
     * @param currencyID1 the currencyID1 to set
     */
    public void setCurrencyID1(int currencyID1) {
        this.currencyID1 = currencyID1;
    }

    /**
     * @return the currencyID2
     */
    public int getCurrencyID2() {
        return currencyID2;
    }

    /**
     * @param currencyID2 the currencyID2 to set
     */
    public void setCurrencyID2(int currencyID2) {
        this.currencyID2 = currencyID2;
    }

    /**
     * @return the currencyID3
     */
    public int getCurrencyID3() {
        return currencyID3;
    }

    /**
     * @param currencyID3 the currencyID3 to set
     */
    public void setCurrencyID3(int currencyID3) {
        this.currencyID3 = currencyID3;
    }

    /**
     * @return the currencyID4
     */
    public int getCurrencyID4() {
        return currencyID4;
    }

    /**
     * @param currencyID4 the currencyID4 to set
     */
    public void setCurrencyID4(int currencyID4) {
        this.currencyID4 = currencyID4;
    }

    


}
