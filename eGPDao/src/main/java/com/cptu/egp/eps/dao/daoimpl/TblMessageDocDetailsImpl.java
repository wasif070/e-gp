/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblMessageDocDetailsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblMessageDocDetails;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblMessageDocDetailsImpl extends AbcAbstractClass<TblMessageDocDetails> implements TblMessageDocDetailsDao{

    @Override
    public void addTblMessageDocDetails(TblMessageDocDetails tblMessageDocDetails) {
        super.addEntity(tblMessageDocDetails);
    }

    @Override
    public List<TblMessageDocDetails> findTblMessageDocDetails(Object... values) throws Exception {
        return super.findEntity(values);
        
    }

    @Override
    public void deleteTblMessageDocDetails(TblMessageDocDetails tblMessageDocDetails) {

        super.deleteEntity(tblMessageDocDetails);
      
    }

    @Override
    public void updateTblMessageDocDetails(TblMessageDocDetails tblMessageDocDetails) {

        super.updateEntity(tblMessageDocDetails);
      
    }

    @Override
    public List<TblMessageDocDetails> getAllTblMessageDocDetails() {
        return super.getAllEntity();
    }

    @Override
    public long getTblMessageDocDetailsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblMessageDocDetails> findByCountTblMessageDocDetails(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
