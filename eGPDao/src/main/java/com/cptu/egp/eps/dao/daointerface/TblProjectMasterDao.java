package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblProjectMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblProjectMasterDao extends GenericDao<TblProjectMaster> {

    public void addTblProjectMaster(TblProjectMaster tblObj);

    public void deleteTblProjectMaster(TblProjectMaster tblObj);

    public void updateTblProjectMaster(TblProjectMaster tblObj);

    public List<TblProjectMaster> getAllTblProjectMaster();

    public List<TblProjectMaster> findTblProjectMaster(Object... values) throws Exception;

    public List<TblProjectMaster> findByCountTblProjectMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblProjectMasterCount();
}
