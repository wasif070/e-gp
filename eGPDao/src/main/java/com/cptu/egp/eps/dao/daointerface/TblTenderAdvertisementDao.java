/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderAdvertisement;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderAdvertisementDao extends GenericDao<TblTenderAdvertisement>{

    public void addTblTenderAdvertisement(TblTenderAdvertisement tblObj);

    public void deleteTblTenderAdvertisement(TblTenderAdvertisement tblObj);

    public void updateTblTenderAdvertisement(TblTenderAdvertisement tblObj);

    public List<TblTenderAdvertisement> getAllTblTenderAdvertisement();

    public List<TblTenderAdvertisement> findTblTenderAdvertisement(Object... values) throws Exception;

    public List<TblTenderAdvertisement> findByCountTblTenderAdvertisement(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderAdvertisementCount();
}
