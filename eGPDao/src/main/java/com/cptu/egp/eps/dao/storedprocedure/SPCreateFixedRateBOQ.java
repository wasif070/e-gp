/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Kiran
 * it will create a new BOQ form with fixed rates or salvage materials for works
 */
public class SPCreateFixedRateBOQ extends StoredProcedure {
    
     private static final Logger LOGGER = Logger.getLogger(SPTenderCommon.class);
    /* Pass Input Paras to proc*/

    public SPCreateFixedRateBOQ(BasicDataSource dataSource, String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);
        this.declareParameter(new SqlParameter("@v_fieldName1Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName2Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName3Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName4Vc", Types.VARCHAR));
    }

    /**
     * Execute stored procedure : p_ins_boqFormFixedRates
     * Get Tender common data.
     * @param fieldName1 --function name
     * @param fieldName2 --tenderId
     * @param fieldName3  --section id
     * @param fieldName4  --fixed item form or salvage item form
     * @return List of SPTenderCommonData objects having tender details.
     */
    public List<SPTenderCommonData> executeProcedure(String fieldName1, String fieldName2, String fieldName3,String fieldName4) {
        Map inParams = new HashMap();
        inParams.put("@v_fieldName1Vc", fieldName1);
        inParams.put("@v_fieldName2Vc", fieldName2);
        inParams.put("@v_fieldName3Vc", fieldName3);
        inParams.put("@v_fieldName4Vc", fieldName4);
        this.compile();

        List<SPTenderCommonData> tenderData = new ArrayList<SPTenderCommonData>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            if (list != null && !list.isEmpty()) {
                for (LinkedHashMap<String, Object> linkedHashMap : list) {
                    SPTenderCommonData sPTenderCommonData = new SPTenderCommonData();
                    sPTenderCommonData.setFieldName1((String) linkedHashMap.get("FieldValue1"));
                    sPTenderCommonData.setFieldName2((String) linkedHashMap.get("FieldValue2"));
                    sPTenderCommonData.setFieldName3((String) linkedHashMap.get("FieldValue3"));
                    sPTenderCommonData.setFieldName4((String) linkedHashMap.get("FieldValue4"));
                    sPTenderCommonData.setFieldName5((String) linkedHashMap.get("FieldValue5"));
                    sPTenderCommonData.setFieldName6((String) linkedHashMap.get("FieldValue6"));
                    sPTenderCommonData.setFieldName7((String) linkedHashMap.get("FieldValue7"));
                    sPTenderCommonData.setFieldName8((String) linkedHashMap.get("FieldValue8"));
                    sPTenderCommonData.setFieldName9((String) linkedHashMap.get("FieldValue9"));
                    sPTenderCommonData.setFieldName10((String) linkedHashMap.get("FieldValue10"));
                    sPTenderCommonData.setFieldName11((String) linkedHashMap.get("FieldValue11"));

                    tenderData.add(sPTenderCommonData);
                }
            }
            LOGGER.debug("No data found for FUNCTION  " + fieldName1);
        } catch (Exception e) {
            
            e.printStackTrace();
            LOGGER.error("SPTenderCommon  : " + e.toString() + " FUNCTION : " + fieldName1);
            
        }
        return tenderData;
    }


}
