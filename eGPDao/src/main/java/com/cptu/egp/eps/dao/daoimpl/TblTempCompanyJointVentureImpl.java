package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblTempCompanyJointVentureDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTempCompanyJointVenture;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblTempCompanyJointVentureImpl extends AbcAbstractClass<TblTempCompanyJointVenture> implements TblTempCompanyJointVentureDao {

    @Override
    public void addTblTempCompanyJointVenture(TblTempCompanyJointVenture tempCompanyJointVenture){
        super.addEntity(tempCompanyJointVenture);
    }

    @Override
    public void deleteTblTempCompanyJointVenture(TblTempCompanyJointVenture tempCompanyJointVenture) {
        super.deleteEntity(tempCompanyJointVenture);
    }

    @Override
    public void updateTblTempCompanyJointVenture(TblTempCompanyJointVenture tempCompanyJointVenture) {
        super.updateEntity(tempCompanyJointVenture);
    }

    @Override
    public List<TblTempCompanyJointVenture> getAllTblTempCompanyJointVenture() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTempCompanyJointVenture> findTblTempCompanyJointVenture(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTempCompanyJointVentureCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTempCompanyJointVenture> findByCountTblTempCompanyJointVenture(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
