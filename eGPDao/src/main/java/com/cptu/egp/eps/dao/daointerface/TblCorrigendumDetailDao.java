package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCorrigendumDetail;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblCorrigendumDetailDao extends GenericDao<TblCorrigendumDetail> {

    public void addTblCorrigendumDetail(TblCorrigendumDetail tblCorrigendumDetail);

    public void deleteTblCorrigendumDetail(TblCorrigendumDetail tblCorrigendumDetail);

    public void updateTblCorrigendumDetail(TblCorrigendumDetail tblCorrigendumDetail);

    public List<TblCorrigendumDetail> getAllTblCorrigendumDetail();

    public List<TblCorrigendumDetail> findTblCorrigendumDetail(Object... values) throws Exception;

    public List<TblCorrigendumDetail> findByCountTblCorrigendumDetail(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCorrigendumDetailCount();

    public void updateOrSaveAllCorriDetail(List<TblCorrigendumDetail> tblCorrigendumDetail);
}
