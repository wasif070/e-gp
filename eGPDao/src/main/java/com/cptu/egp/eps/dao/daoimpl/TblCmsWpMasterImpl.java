package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsWpMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsWpMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsWpMasterImpl extends AbcAbstractClass<TblCmsWpMaster> implements TblCmsWpMasterDao {

    @Override
    public void addTblCmsWpMaster(TblCmsWpMaster cmsWpMaster){
        super.addEntity(cmsWpMaster);
    }

    @Override
    public void deleteTblCmsWpMaster(TblCmsWpMaster cmsWpMaster) {
        super.deleteEntity(cmsWpMaster);
    }

    @Override
    public void updateTblCmsWpMaster(TblCmsWpMaster cmsWpMaster) {
        super.updateEntity(cmsWpMaster);
    }

    @Override
    public List<TblCmsWpMaster> getAllTblCmsWpMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsWpMaster> findTblCmsWpMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsWpMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsWpMaster> findByCountTblCmsWpMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsWpMaster> estCost) {
         super.updateAll(estCost);
    }
}
