/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderListBox;
import java.util.List;

/**
 *
 * @author parag
 */
public interface TblTenderListBoxDao extends GenericDao<TblTenderListBox>{

    public void addTblTenderListBox(TblTenderListBox tblObj);

    public void deleteTblTenderListBox(TblTenderListBox tblObj);

    public void updateTblTenderListBox(TblTenderListBox tblObj);

    public List<TblTenderListBox> getAllTblTenderListBox();

    public List<TblTenderListBox> findTblTenderListBox(Object... values) throws Exception;

    public List<TblTenderListBox> findByCountTblTenderListBox(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderListBoxCount();

    
}
