/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderValidityExtDateDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderValidityExtDate;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderValidityExtDateImpl extends AbcAbstractClass<TblTenderValidityExtDate> implements TblTenderValidityExtDateDao {

    @Override
    public void addTblTenderValidityExtDate(TblTenderValidityExtDate admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblTenderValidityExtDate(TblTenderValidityExtDate admin) {
        super.deleteEntity(admin);        
    }

    @Override
    public void updateTblTenderValidityExtDate(TblTenderValidityExtDate admin) {
        super.updateEntity(admin);        
    }

    @Override
    public List<TblTenderValidityExtDate> getAllTblTenderValidityExtDate() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderValidityExtDate> findTblTenderValidityExtDate(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderValidityExtDateCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderValidityExtDate> findByCountTblTenderValidityExtDate(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
