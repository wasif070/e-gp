package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblDebarmentDetailsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblDebarmentDetails;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblDebarmentDetailsImpl extends AbcAbstractClass<TblDebarmentDetails> implements TblDebarmentDetailsDao {

    @Override
    public void addTblDebarmentDetails(TblDebarmentDetails master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblDebarmentDetails(TblDebarmentDetails master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblDebarmentDetails(TblDebarmentDetails master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblDebarmentDetails> getAllTblDebarmentDetails() {
        return super.getAllEntity();
    }

    @Override
    public List<TblDebarmentDetails> findTblDebarmentDetails(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblDebarmentDetailsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblDebarmentDetails> findByCountTblDebarmentDetails(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
