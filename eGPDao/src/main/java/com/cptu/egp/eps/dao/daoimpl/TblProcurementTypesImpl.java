/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblProcurementTypesDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblProcurementTypes;
import java.util.List;

/**
 * <b>Class Description goes here</b>
 * $Revision: 1.1.1.1 $
 * @version <version-no.> <Date>
 * @author Administrator
 */
public class TblProcurementTypesImpl extends AbcAbstractClass<TblProcurementTypes> implements TblProcurementTypesDao {

    @Override
    public void addTblProcurementTypes(TblProcurementTypes tblProcurementTypes)
    {
        super.addEntity(tblProcurementTypes);
    }

    @Override
    public void deleteTblProcurementTypes(TblProcurementTypes tblProcurementTypes)
    {
        super.deleteEntity(tblProcurementTypes);
    }

    @Override
    public void updateTblProcurementTypes(TblProcurementTypes tblProcurementTypes)
    {
        super.updateEntity(tblProcurementTypes);
    }

    @Override
    public List<TblProcurementTypes> getAllTblProcurementTypes()
    {
        return super.getAllEntity();
    }

    @Override
    public List<TblProcurementTypes> findTblProcurementTypes(Object... values) throws Exception
    {
        return super.findEntity(values);
    }

    @Override
    public List<TblProcurementTypes> findByCountTblProcurementTypes(int firstResult, int maxResult, Object... values) throws Exception
    {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblProcurementTypesCount()
    {
        return super.getEntityCount();
    }


}
