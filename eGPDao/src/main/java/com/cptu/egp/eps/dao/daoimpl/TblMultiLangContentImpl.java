/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblMultiLangContentDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblMultiLangContent;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblMultiLangContentImpl extends AbcAbstractClass<TblMultiLangContent> implements TblMultiLangContentDao{

    @Override
    public void addTblMultiLangContent(TblMultiLangContent tblMultiLangContent) {
        super.addEntity(tblMultiLangContent);
    }

    @Override
    public List<TblMultiLangContent> findTblMultiLangContent(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblMultiLangContent(TblMultiLangContent tblMultiLangContent) {

        super.deleteEntity(tblMultiLangContent);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblMultiLangContent(Collection tblMultiLangContent) {
        super.deleteAll(tblMultiLangContent);
    }

    @Override
    public void updateTblMultiLangContent(TblMultiLangContent tblMultiLangContent) {

        super.updateEntity(tblMultiLangContent);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblMultiLangContent> getAllTblMultiLangContent() {
        return super.getAllEntity();
    }

    @Override
    public long getTblMultiLangContentCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblMultiLangContent> findByCountTblMultiLangContent(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
