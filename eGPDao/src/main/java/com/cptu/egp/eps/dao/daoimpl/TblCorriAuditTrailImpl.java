package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCorriAuditTrailDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCorriAuditTrail;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCorriAuditTrailImpl extends AbcAbstractClass<TblCorriAuditTrail> implements TblCorriAuditTrailDao {

    @Override
    public void addTblCorriAuditTrail(TblCorriAuditTrail corriAuditTrail){
        super.addEntity(corriAuditTrail);
    }

    @Override
    public void deleteTblCorriAuditTrail(TblCorriAuditTrail corriAuditTrail) {
        super.deleteEntity(corriAuditTrail);
    }

    @Override
    public void updateTblCorriAuditTrail(TblCorriAuditTrail corriAuditTrail) {
        super.updateEntity(corriAuditTrail);
    }

    @Override
    public List<TblCorriAuditTrail> getAllTblCorriAuditTrail() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCorriAuditTrail> findTblCorriAuditTrail(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCorriAuditTrailCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCorriAuditTrail> findByCountTblCorriAuditTrail(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
