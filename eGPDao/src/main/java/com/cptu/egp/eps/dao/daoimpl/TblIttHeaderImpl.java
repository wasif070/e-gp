/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblIttHeaderDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblIttHeader;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public class TblIttHeaderImpl extends AbcAbstractClass<TblIttHeader> implements TblIttHeaderDao {

    @Override
    public void addTblIttHeader(TblIttHeader admin) {

        super.addEntity(admin);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblIttHeader(TblIttHeader admin) {

        super.deleteEntity(admin);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblIttHeader(TblIttHeader admin) {

        super.updateEntity(admin);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblIttHeader> getAllTblIttHeader() {
        return super.getAllEntity();
    }

    @Override
    public List<TblIttHeader> findTblIttHeader(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblIttHeaderCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblIttHeader> findByCountTblIttHeader(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
