/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblPreTenderQryDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblPreTenderQryDocs;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblPreTenderQryDocsImpl extends AbcAbstractClass<TblPreTenderQryDocs> implements TblPreTenderQryDocsDao {

    @Override
    public void addTblPreTenderQryDocs(TblPreTenderQryDocs preTenderQryDocs) {
        super.addEntity(preTenderQryDocs);
    }

    @Override
    public void deleteTblPreTenderQryDocs(TblPreTenderQryDocs preTenderQryDocs) {
        super.deleteEntity(preTenderQryDocs);
    }

    @Override
    public void updateTblPreTenderQryDocs(TblPreTenderQryDocs preTenderQryDocs) {
        super.updateEntity(preTenderQryDocs);
    }

    @Override
    public List<TblPreTenderQryDocs> getAllTblPreTenderQryDocs() {
        return super.getAllEntity();
    }

    @Override
    public List<TblPreTenderQryDocs> findTblPreTenderQryDocs(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblPreTenderQryDocsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblPreTenderQryDocs> findByCountTblPreTenderQryDocs(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
