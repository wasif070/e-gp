package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCurrencyMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblCurrencyMasterDao extends GenericDao<TblCurrencyMaster> {

    public void addTblCurrencyMaster(TblCurrencyMaster tblObj);

    public void deleteTblCurrencyMaster(TblCurrencyMaster tblObj);

    public void updateTblCurrencyMaster(TblCurrencyMaster tblObj);

    public List<TblCurrencyMaster> getAllTblCurrencyMaster();

    public List<TblCurrencyMaster> findTblCurrencyMaster(Object... values) throws Exception;

    public List<TblCurrencyMaster> findByCountTblCurrencyMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCurrencyMasterCount();
}
