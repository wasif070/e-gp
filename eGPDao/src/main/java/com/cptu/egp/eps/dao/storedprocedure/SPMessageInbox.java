/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Administrator
 */
public class SPMessageInbox extends StoredProcedure {
   private static final Logger LOGGER = Logger.getLogger(SPMessageInbox.class);
   public SPMessageInbox(BasicDataSource dataSource, String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("v_MsgBoxTypeVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_UserIdN", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_FolderIdN", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_msgStatusVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_PagePerRecordN", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_PageN", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_TotalPages", Types.INTEGER));
    }

   /**
    * Execute stored procedure : p_view_mailmessage
    * Use for get mail message details
    * @param msgBoxType  Inbox/Sent/Draft/Trash
    * @param fromEmailId
    * @param folderId
    * @param msgType
    * @param noOfRecords
    * @param pageNo
    * @param totalPages
    * @return mailmessage details as List of MsgInbox Object
    */
    public List<MsgInbox> executeProcedure(String msgBoxType, Integer fromEmailId, Integer folderId, String msgType, Integer noOfRecords, Integer pageNo, Integer totalPages) {
        Map inParams = new HashMap();
        inParams.put("v_MsgBoxTypeVc", msgBoxType);
        inParams.put("v_UserIdN", fromEmailId);
        inParams.put("v_FolderIdN", folderId);
        inParams.put("v_msgStatusVc", msgType);
        inParams.put("v_PagePerRecordN", noOfRecords);
        inParams.put("v_PageN", pageNo);
        inParams.put("v_TotalPages", totalPages);
        this.compile();
        List<MsgInbox> details = new ArrayList<MsgInbox>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                MsgInbox msgInbox = new MsgInbox();
                msgInbox.setMsgBoxId((Integer) linkedHashMap.get("msgBoxId"));
                msgInbox.setFolderId((Integer) linkedHashMap.get("folderId"));
                msgInbox.setMsgFromUserId((Integer) linkedHashMap.get("msgFromUserId"));
                msgInbox.setMsgToUserId((Integer) linkedHashMap.get("msgToUserId"));
                msgInbox.setMsgId((Integer) linkedHashMap.get("msgId"));
                msgInbox.setSubject((String) linkedHashMap.get("subject"));
                msgInbox.setCreationDate((Date) linkedHashMap.get("creationDate"));
                msgInbox.setIsDocUploaded((String) linkedHashMap.get("isDocUploaded"));
                msgInbox.setIsMsgRead((String) linkedHashMap.get("isMsgRead"));
                msgInbox.setIsPriority((String) linkedHashMap.get("isPriority"));
                msgInbox.setMsgStatus((String) linkedHashMap.get("msgStatus"));
                msgInbox.setMsgText((String) linkedHashMap.get("msgText"));
                msgInbox.setMsgToCCReply((String) linkedHashMap.get("msgToCCReply"));
                msgInbox.setProcessUrl((String) linkedHashMap.get("processUrl"));
                details.add(msgInbox);
            }
        } catch (Exception e) {
            LOGGER.error("SPMessageInbox  : "+ e);
        }
        return details;
    }
}
