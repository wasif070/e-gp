/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.math.BigDecimal;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Administrator
 */
public class SPInsertMultiTablesXML extends StoredProcedure {
    private static final Logger LOGGER = Logger.getLogger(SPInsertMultiTablesXML.class);
    public SPInsertMultiTablesXML(BasicDataSource dataSource, String procName) {

        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.

        this.declareParameter(new SqlParameter("v_Action_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Tbl1_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Tbl2_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Tbl3_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Tbl4_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Tbl1_XmlTxt_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Tbl2_XmlTxt_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Tbl3_XmlTxt_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Tbl4_XmlTxt_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Tbl1_PKColumn_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Tbl2_PKColumn_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Tbl3_PKColumn_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Condition_inVc", Types.VARCHAR));

    }

    /**
     * Execute stored procedure : p_ins_templatexml_multitables
     * Use to insert multiple table base on relation ship between them using table details into xml formate.
     * @param actioninVc delete/insdel/insert
     * @param tbl1inVc First table name
     * @param tbl2inVc Second table name
     * @param tbl3inVc third table name
     * @param tbl4inVc forth table name
     * @param tbl1XmlTxtinVc First table detail in xml formate
     * @param tbl2XmlTxtinVc Second table detail in xml formate
     * @param tbl3XmlTxtinVc third table detail in xml formate
     * @param tbl4XmlTxtinVc forth table detail in xml formate
     * @param tbl1PKColumninVc Primary key filed name of First table
     * @param tbl2PKColumninVc Primary key filed name of Second table
     * @param tbl3PKColumninVc Primary key filed name of Third table
     * @param conditioninVc String containing condition for delete operation.
     * @return object of CommonSPReturn contains flags indicate operation successful or not.
     */
    public CommonSPReturn executeProcedure(String actioninVc,
            String tbl1inVc,
            String tbl2inVc,
            String tbl3inVc,
            String tbl4inVc,
            String tbl1XmlTxtinVc,
            String tbl2XmlTxtinVc,
            String tbl3XmlTxtinVc,
            String tbl4XmlTxtinVc,
            String tbl1PKColumninVc,
            String tbl2PKColumninVc,
            String tbl3PKColumninVc,
            String conditioninVc) {
        Map inParams = new HashMap();

        inParams.put("v_Action_inVc", actioninVc);
        inParams.put("v_Tbl1_inVc", tbl1inVc);
        inParams.put("v_Tbl2_inVc", tbl2inVc);
        inParams.put("v_Tbl3_inVc", tbl3inVc);
        inParams.put("v_Tbl4_inVc", tbl4inVc);
        inParams.put("v_Tbl1_XmlTxt_inVc", tbl1XmlTxtinVc);
        inParams.put("v_Tbl2_XmlTxt_inVc", tbl2XmlTxtinVc);
        inParams.put("v_Tbl3_XmlTxt_inVc", tbl3XmlTxtinVc);
        inParams.put("v_Tbl4_XmlTxt_inVc", tbl4XmlTxtinVc);
        inParams.put("v_Tbl1_PKColumn_inVc", tbl1PKColumninVc);
        inParams.put("v_Tbl2_PKColumn_inVc", tbl2PKColumninVc);
        inParams.put("v_Tbl3_PKColumn_inVc", tbl3PKColumninVc);
        inParams.put("v_Condition_inVc", conditioninVc);

        this.compile();
        CommonSPReturn details = new CommonSPReturn();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                details.setId(new BigDecimal(linkedHashMap.get("Id").toString()));
                details.setFlag((Boolean) linkedHashMap.get("flag"));
                details.setMsg((String) linkedHashMap.get("Message"));

            }
        } catch (Exception e) {
            LOGGER.error("SPInsertMultiTablesXML  : "+ e);
        }
        return details;

    }
}
