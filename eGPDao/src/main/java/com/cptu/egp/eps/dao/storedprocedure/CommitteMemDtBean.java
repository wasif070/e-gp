/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

/**
 *
 * @author Administrator
 */
public class CommitteMemDtBean {

    private String userId;
    private String empName;
    private String memRole;
    private String memFrom;
    private String commName;
    private String commId;
    private String govUserId;
    private String remarks;
    private String aapDate;

    public String getGovUserId() {
        return govUserId;
    }

    public void setGovUserId(String govUserId) {
        this.govUserId = govUserId;
    }

    public String getCommId() {
        return commId;
    }

    public void setCommId(String commId) {
        this.commId = commId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getMemFrom() {
        return memFrom;
    }

    public void setMemFrom(String memFrom) {
        this.memFrom = memFrom;
    }

    public String getMemRole() {
        return memRole;
    }

    public void setMemRole(String memRole) {
        this.memRole = memRole;
    }

    public String getUserId() {
        return userId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCommName() {
        return commName;
    }

    public void setCommName(String commName) {
        this.commName = commName;
    }

    public String getAapDate() {
        return aapDate;
    }

    public void setAapDate(String aapDate) {
        this.aapDate = aapDate;
    }
    
    

    public CommitteMemDtBean(String userId, String empName, String memRole, String memFrom, String commName, String commId, String govUserId) {
        this.userId = userId;
        this.empName = empName;
        this.memRole = memRole;
        this.memFrom = memFrom;
        this.commName = commName;
        this.commId = commId;
        this.govUserId = govUserId;
    }

    public CommitteMemDtBean(String userId, String empName, String memRole, String memFrom, String commName, String commId, String govUserId, String remarks, String aapDate) {
        this.userId = userId;
        this.empName = empName;
        this.memRole = memRole;
        this.memFrom = memFrom;
        this.commName = commName;
        this.commId = commId;
        this.govUserId = govUserId;
        this.remarks = remarks;
        this.aapDate = aapDate;
    }
    
}
