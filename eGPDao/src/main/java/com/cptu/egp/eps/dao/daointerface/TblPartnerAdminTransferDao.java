package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblPartnerAdminTransfer;
import java.util.List;

/**
 *
 * @author rishita
 */
public interface TblPartnerAdminTransferDao extends GenericDao<TblPartnerAdminTransfer> {

    public void addTblPartnerAdminTransfer(TblPartnerAdminTransfer tblObj);

    public void deleteTblPartnerAdminTransfer(TblPartnerAdminTransfer tblObj);

    public void updateTblPartnerAdminTransfer(TblPartnerAdminTransfer tblObj);

    public List<TblPartnerAdminTransfer> getAllTblPartnerAdminTransfer();

    public List<TblPartnerAdminTransfer> findTblPartnerAdminTransfer(Object... values) throws Exception;

    public List<TblPartnerAdminTransfer> findByCountTblPartnerAdminTransfer(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblPartnerAdminTransferCount();
}
