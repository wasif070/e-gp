/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderCoriGrandSumDetailDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderCoriGrandSumDetail;
import java.util.List;

/**
 *
 * @author rikin.p
 */

public class TblTenderCoriGrandSumDetailDaoImpl extends AbcAbstractClass<TblTenderCoriGrandSumDetail> implements TblTenderCoriGrandSumDetailDao{

    @Override
    public void addTblTenderCoriGrandSumDetail(TblTenderCoriGrandSumDetail tblTenderCoriGrandSumDetail) {
        super.addEntity(tblTenderCoriGrandSumDetail);
    }

    @Override
    public void deleteTblTenderCoriGrandSumDetail(TblTenderCoriGrandSumDetail tblTenderCoriGrandSumDetail) {
        super.deleteEntity(tblTenderCoriGrandSumDetail);
    }

    @Override
    public void updateTblTenderCoriGrandSumDetail(TblTenderCoriGrandSumDetail tblTenderCoriGrandSumDetail) {
        super.updateEntity(tblTenderCoriGrandSumDetail);
    }

    @Override
    public List<TblTenderCoriGrandSumDetail> getAllTblTenderCoriGrandSumDetail() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderCoriGrandSumDetail> findTblTenderCoriGrandSumDetail(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public void updateSaveAllTenderCoriGrandSumDetail(List<TblTenderCoriGrandSumDetail> lstTenderGrandSumDetail)
    {
         super.updateAll(lstTenderGrandSumDetail);
    }
    
   }