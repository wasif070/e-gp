/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblWorkFlowEventConfigHistDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblWorkFlowEventConfigHist;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public class TblWorkFlowEventConfigHistImpl extends AbcAbstractClass<TblWorkFlowEventConfigHist> implements TblWorkFlowEventConfigHistDao {

    @Override
    public void addTblWorkFlowEventConfigHist(TblWorkFlowEventConfigHist workFlowEventConfigHist) {

        super.addEntity(workFlowEventConfigHist);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblWorkFlowEventConfigHist(TblWorkFlowEventConfigHist workFlowEventConfigHist) {

        super.deleteEntity(workFlowEventConfigHist);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblWorkFlowEventConfigHist(TblWorkFlowEventConfigHist workFlowEventConfigHist) {

        super.updateEntity(workFlowEventConfigHist);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblWorkFlowEventConfigHist> getAllTblWorkFlowEventConfigHist() {
        return super.getAllEntity();
    }

    @Override
    public List<TblWorkFlowEventConfigHist> findTblWorkFlowEventConfigHist(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblWorkFlowEventConfigHistCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblWorkFlowEventConfigHist> findByCountTblWorkFlowEventConfigHist(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
