/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Administrator
 */
public class SPInsertJvcaDetails extends StoredProcedure{
    private static final Logger LOGGER = Logger.getLogger(SPInsertJvcaDetails.class);
    SPInsertJvcaDetails(BasicDataSource dataSource, String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("@v_userid_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_jvcompanyid_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_jvrole_inVc", Types.VARCHAR));
    }

    /**
     * Execute stored procedure : p_insert_jvca_details
     * Add Jvca Details into tbl_TempCompanyJointVenture table.
     * @param userId
     * @param jvcaCompId
     * @param jvcaRole
     * @return CommonMsgChk list contains flags indicate operation success status.
     */
    public List<CommonMsgChk> executeProcedure(int userId, int jvcaCompId, String jvcaRole) {
        Map inParams = new HashMap();
        inParams.put("@v_userid_inInt", userId);
        inParams.put("@v_jvcompanyid_inInt", jvcaCompId);
        inParams.put("@v_jvrole_inVc", jvcaRole);
        this.compile();
        List<CommonMsgChk> details = new ArrayList<CommonMsgChk>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonMsgChk commonMsgChk = new CommonMsgChk();
                commonMsgChk.setFlag((Boolean) linkedHashMap.get("flag"));
                commonMsgChk.setMsg((String) linkedHashMap.get("message"));
                details.add(commonMsgChk);
            }
        } catch (Exception e) {
            LOGGER.error("SPInsertJvcaDetails : "+ e);
        }
        return details;
    }

}
