package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblEvalTSCNotificationDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalTscnotification;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblEvalTSCNotificationImpl extends AbcAbstractClass<TblEvalTscnotification> implements TblEvalTSCNotificationDao {

    @Override
    public void addTblEvalTscnotification(TblEvalTscnotification evalTscnotification){
        super.addEntity(evalTscnotification);
    }

    @Override
    public void deleteTblEvalTscnotification(TblEvalTscnotification evalTscnotification) {
        super.deleteEntity(evalTscnotification);
    }

    @Override
    public void updateTblEvalTscnotification(TblEvalTscnotification evalTscnotification) {
        super.updateEntity(evalTscnotification);
    }

    @Override
    public List<TblEvalTscnotification> getAllTblEvalTscnotification() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEvalTscnotification> findTblEvalTscnotification(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEvalTscnotificationCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalTscnotification> findByCountTblEvalTscnotification(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
