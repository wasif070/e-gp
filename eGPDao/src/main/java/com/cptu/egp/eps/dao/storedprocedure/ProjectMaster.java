/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author parag
 */
public class ProjectMaster extends StoredProcedure {
    private static final Logger LOGGER = Logger.getLogger(ProjectMaster.class);
    public ProjectMaster(BasicDataSource dataSource, String procName){
     this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("v_projectName_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_projectCode_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_progId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_projectCost_inM", Types.BIGINT));
        this.declareParameter(new SqlParameter("v_projectStartDate_inD", Types.DATE));
        this.declareParameter(new SqlParameter("v_projectEndDate_inD", Types.DATE));
        this.declareParameter(new SqlParameter("v_sourceOfFund_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_SBankDevelopId_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Action_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_ProjectId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_CreatedBy_inInt", Types.INTEGER));
}

    /**
     * Execute procedure : p_add_upd_projects
     * @param projectName
     * @param projectCode
     * @param progId
     * @param projectCost
     * @param projectStartDate
     * @param projectEndDate
     * @param sourceOfFund
     * @param sBankDevelopId
     * @param action
     * @param projectId
     * @param createdBy
     * @return List of CommonSPRerturn objects.
     */
    public List<CommonSPReturn> executeProcedure(String projectName, String projectCode, Integer progId, BigDecimal projectCost, Date projectStartDate, Date projectEndDate,
            String sourceOfFund, String sBankDevelopId, String action, Integer projectId,Integer createdBy) {

        Map inParams = new HashMap();
        inParams.put("v_projectName_inVc", projectName);
        inParams.put("v_projectCode_inVc", projectCode);
        inParams.put("v_progId_inInt", progId);
        inParams.put("v_projectCost_inM", projectCost);
        inParams.put("v_projectStartDate_inD", projectStartDate);
        inParams.put("v_projectEndDate_inD", projectEndDate);
        inParams.put("v_sourceOfFund_inVc", sourceOfFund);
        inParams.put("v_SBankDevelopId_inVc", sBankDevelopId);
        inParams.put("v_Action_inVc", action);
        inParams.put("v_ProjectId_inInt", projectId);
        inParams.put("v_CreatedBy_inInt",createdBy);

        this.compile();
       
        List<CommonSPReturn> details = new ArrayList<CommonSPReturn>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonSPReturn commonSPReturn = new CommonSPReturn();
                commonSPReturn.setId((BigDecimal) linkedHashMap.get("projectId"));
                commonSPReturn.setFlag((Boolean) linkedHashMap.get("flag"));
                commonSPReturn.setMsg((String) linkedHashMap.get("msg"));
                details.add(commonSPReturn);
            }
        } catch (Exception e) {
            LOGGER.error("Error Man executeProcedure ProjectMaster : "+ e);
        }
        return details;
    }
 

    /**
     * Execute procedure for get data Project Data. Procedure: p_add_upd_projects
     * @param projectName
     * @param projectCode
     * @param progId
     * @param projectCost
     * @param projectStartDate
     * @param projectEndDate
     * @param sourceOfFund
     * @param sBankDevelopId
     * @param action
     * @param projectId
     * @param createdBy
     * @return List of SPProjectDetailsReturn object
     */
    public List<SPProjectDetailReturn> executeGetProcedure(String projectName, String projectCode, Integer progId, BigDecimal projectCost, Date projectStartDate, Date projectEndDate,
            String sourceOfFund, String sBankDevelopId, String action, Integer projectId,Integer createdBy) {

        Map inParams = new HashMap();
        inParams.put("v_projectName_inVc", projectName);
        inParams.put("v_projectCode_inVc", projectCode);
        inParams.put("v_progId_inInt", progId);
        inParams.put("v_projectCost_inM", projectCost);
        inParams.put("v_projectStartDate_inD", projectStartDate);
        inParams.put("v_projectEndDate_inD", projectEndDate);
        inParams.put("v_sourceOfFund_inVc", sourceOfFund);
        inParams.put("v_SBankDevelopId_inVc", sBankDevelopId);
        inParams.put("v_Action_inVc", action);
        inParams.put("v_ProjectId_inInt", projectId);
        inParams.put("v_CreatedBy_inInt",createdBy);

        this.compile();
       

        List<SPProjectDetailReturn> details = new ArrayList<SPProjectDetailReturn>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                SPProjectDetailReturn pProjectDetailReturn = new SPProjectDetailReturn();
                pProjectDetailReturn.setProgId((Integer) linkedHashMap.get("progId"));
                pProjectDetailReturn.setProgName((String) linkedHashMap.get("progName"));
                pProjectDetailReturn.setProjectName((String) linkedHashMap.get("projectName"));
                pProjectDetailReturn.setProjectCode((String) linkedHashMap.get("projectCode"));
                pProjectDetailReturn.setProjectCost((BigDecimal) linkedHashMap.get("projectCost"));
                pProjectDetailReturn.setProjectStartDate((String) linkedHashMap.get("projectStartDate"));
                pProjectDetailReturn.setProjectEndDate((String) linkedHashMap.get("projectEndDate"));
                pProjectDetailReturn.setSourceOfFund((String) linkedHashMap.get("sourceOfFund"));
                pProjectDetailReturn.setsBankDevelopId((Integer) linkedHashMap.get("sBankDevelopId"));
                pProjectDetailReturn.setSbDevelopName((String) linkedHashMap.get("sbDevelopName"));

                details.add(pProjectDetailReturn);
            }
        } catch (Exception e) {
            LOGGER.error("Error Man ProjectMaster executeGetProcedure GetSP: "+ e);
        }
        return details;

    }

    /**
     * Execute procedure for get Project ROle data. Procedure : p_add_upd_projects
     * @param projectName
     * @param projectCode
     * @param progId
     * @param projectCost
     * @param projectStartDate
     * @param projectEndDate
     * @param sourceOfFund
     * @param sBankDevelopId
     * @param action
     * @param projectId
     * @param createdBy
     * @return
     */
    public List<SPProjectRolesReturn> executeProjectRolesProcedure(String projectName, String projectCode, Integer progId, BigDecimal projectCost, Date projectStartDate, Date projectEndDate,
            String sourceOfFund, String sBankDevelopId, String action, Integer projectId,int createdBy) {

        Map inParams = new HashMap();
        inParams.put("v_projectName_inVc", projectName);
        inParams.put("v_projectCode_inVc", projectCode);
        inParams.put("v_progId_inInt", progId);
        inParams.put("v_projectCost_inM", projectCost);
        inParams.put("v_projectStartDate_inD", projectStartDate);
        inParams.put("v_projectEndDate_inD", projectEndDate);
        inParams.put("v_sourceOfFund_inVc", sourceOfFund);
        inParams.put("v_SBankDevelopId_inVc", sBankDevelopId);
        inParams.put("v_Action_inVc", action);
        inParams.put("v_ProjectId_inInt", projectId);
        inParams.put("v_CreatedBy_inInt",createdBy);

        this.compile();

        List<SPProjectRolesReturn> details = new ArrayList<SPProjectRolesReturn>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {

                SPProjectRolesReturn pProjectDetailReturn = new SPProjectRolesReturn();
                pProjectDetailReturn.setUserId((Integer) linkedHashMap.get("userId"));
               // pProjectDetailReturn.setProjectId((Integer) linkedHashMap.get("projectId"));
                pProjectDetailReturn.setOfficeName((String) linkedHashMap.get("officeName"));
                pProjectDetailReturn.setEmpName((String) linkedHashMap.get("empName"));
                pProjectDetailReturn.setRolesName((String) linkedHashMap.get("rolesName"));
                pProjectDetailReturn.setRolesId((String) linkedHashMap.get("rolesId"));
                pProjectDetailReturn.setOfficeId((Integer)linkedHashMap.get("officeId"));
              
                details.add(pProjectDetailReturn);
            }
        } catch (Exception e) {
           LOGGER.error("Error Man ProjectMaster executeProjectRolesProcedure GetSP : "+ e);
        }
        return details;

    }


    /**
     * Execute procedure for perform Operation like Insert / update /delete/ Get data. Procedure : p_add_upd_projects
     * @param projectName
     * @param projectCode
     * @param progId
     * @param projectCost
     * @param projectStartDate
     * @param projectEndDate
     * @param sourceOfFund
     * @param sBankDevelopId
     * @param action
     * @param projectId
     * @return list of SPProjectFPReturn object
     */
     public List<SPProjectFPReturn> executeProjectFPReturns(String projectName, String projectCode, Integer progId, BigDecimal projectCost, Date projectStartDate, Date projectEndDate,
            String sourceOfFund, String sBankDevelopId, String action, Integer projectId) {

        Map inParams = new HashMap();
        inParams.put("v_projectName_inVc", projectName);
        inParams.put("v_projectCode_inVc", projectCode);
        inParams.put("v_progId_inInt", progId);
        inParams.put("v_projectCost_inM", projectCost);
        inParams.put("v_projectStartDate_inD", projectStartDate);
        inParams.put("v_projectEndDate_inD", projectEndDate);
        inParams.put("v_sourceOfFund_inVc", sourceOfFund);
        inParams.put("v_SBankDevelopId_inVc", sBankDevelopId);
        inParams.put("v_Action_inVc", action);
        inParams.put("v_ProjectId_inInt", projectId);

        this.compile();

        List<SPProjectFPReturn> details = new ArrayList<SPProjectFPReturn>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {

                SPProjectFPReturn pProjectFPReturn  = new SPProjectFPReturn();
                
                pProjectFPReturn.setProjectid((Integer) linkedHashMap.get("projectid"));
                pProjectFPReturn.setUserid((Integer) linkedHashMap.get("userid"));
                pProjectFPReturn.setProcurementmethodid((String)(linkedHashMap.get("procurementmethodid")));
                pProjectFPReturn.setProcurementMethod((String)(linkedHashMap.get("procurementMethod")));
                pProjectFPReturn.setOperation((String)(linkedHashMap.get("operation")));
                pProjectFPReturn.setAmount((BigDecimal)(linkedHashMap.get("amount")));

                details.add(pProjectFPReturn);
            }
        } catch (Exception e) {
             LOGGER.error("Error Man ProjectMaster executeProjectFPReturns GetSP:"+ e);
        }
        return details;

    }



}
