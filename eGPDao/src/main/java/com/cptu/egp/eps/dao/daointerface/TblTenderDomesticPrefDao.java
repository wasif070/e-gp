package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderDomesticPref;
import java.util.List;

/**
 *
 * @author rishita
 */
public interface TblTenderDomesticPrefDao extends GenericDao<TblTenderDomesticPref> {

    public void addTblTenderDomesticPref(TblTenderDomesticPref tblObj);

    public void deleteTblTenderDomesticPref(TblTenderDomesticPref tblObj);

    public void updateTblTenderDomesticPref(TblTenderDomesticPref tblObj);

    public List<TblTenderDomesticPref> getAllTblTenderDomesticPref();

    public List<TblTenderDomesticPref> findTblTenderDomesticPref(Object... values) throws Exception;

    public List<TblTenderDomesticPref> findByCountTblTenderDomesticPref(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblTenderDomesticPrefCount();

    public void updateOrSaveTenderDomesticPref(List<TblTenderDomesticPref> tenderDomesticPref);
}
