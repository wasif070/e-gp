/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblNegReplyDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblNegReplyDocs;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblNegReplyDocsImpl extends AbcAbstractClass<TblNegReplyDocs> implements TblNegReplyDocsDao{

    @Override
    public void addTblNegReplyDocs(TblNegReplyDocs tblNegReplyDocs) {
        super.addEntity(tblNegReplyDocs);
    }

    @Override
    public List<TblNegReplyDocs> findTblNegReplyDocs(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblNegReplyDocs(TblNegReplyDocs tblNegReplyDocs) {

        super.deleteEntity(tblNegReplyDocs);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblNegReplyDocs(Collection tblNegReplyDocs) {
        super.deleteAll(tblNegReplyDocs);
    }

    @Override
    public void updateTblNegReplyDocs(TblNegReplyDocs tblNegReplyDocs) {

        super.updateEntity(tblNegReplyDocs);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblNegReplyDocs> getAllTblNegReplyDocs() {
        return super.getAllEntity();
    }

    @Override
    public long getTblNegReplyDocsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblNegReplyDocs> findByCountTblNegReplyDocs(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
