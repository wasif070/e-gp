/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderPayment;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderPaymentDao extends GenericDao<TblTenderPayment>{

    public void addTblTenderPayment(TblTenderPayment tblObj);

    public void deleteTblTenderPayment(TblTenderPayment tblObj);

    public void updateTblTenderPayment(TblTenderPayment tblObj);

    public List<TblTenderPayment> getAllTblTenderPayment();

    public List<TblTenderPayment> findTblTenderPayment(Object... values) throws Exception;

    public List<TblTenderPayment> findByCountTblTenderPayment(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderPaymentCount();
}
