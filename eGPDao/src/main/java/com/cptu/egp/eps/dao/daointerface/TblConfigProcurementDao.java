package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblConfigProcurement;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblConfigProcurementDao extends GenericDao<TblConfigProcurement> {

    public void addTblConfigProcurement(TblConfigProcurement tblConfigProcurement);

    public void deleteTblConfigProcurement(TblConfigProcurement tblConfigProcurement);

    public void deleteAllTblConfigProcurement(Collection tblConfigProcurement);

    public void updateTblConfigProcurement(TblConfigProcurement tblConfigProcurement);

    public List<TblConfigProcurement> getAllTblConfigProcurement();

    public List<TblConfigProcurement> findTblConfigProcurement(Object... values) throws Exception;

    public List<TblConfigProcurement> findByCountTblConfigProcurement(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblConfigProcurementCount();
}
