package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsCompansiateAmt;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCmsCompansiateDao extends GenericDao<TblCmsCompansiateAmt> {

    public void addTblCmsCompansiateAmt(TblCmsCompansiateAmt tblObj);

    public void deleteTblCmsCompansiateAmt(TblCmsCompansiateAmt tblObj);

    public void updateTblCmsCompansiateAmt(TblCmsCompansiateAmt tblObj);

    public List<TblCmsCompansiateAmt> getAllTblCmsCompansiateAmt();

    public List<TblCmsCompansiateAmt> findTblCmsCompansiateAmt(Object... values) throws Exception;

    public List<TblCmsCompansiateAmt> findByCountTblCmsCompansiateAmt(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsCompansiateAmtCount();
}
