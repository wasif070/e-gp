package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this late, choose Tools | lates
 * and open the late in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCompanyDocuments;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCompanyDocumentsDao extends GenericDao<TblCompanyDocuments> {

    public void addTblCompanyDocuments(TblCompanyDocuments document);

    public void deleteTblCompanyDocuments(TblCompanyDocuments document);

    public void updateTblCompanyDocuments(TblCompanyDocuments document);

    public List<TblCompanyDocuments> getAllTblCompanyDocuments();

    public List<TblCompanyDocuments> findTblCompanyDocuments(Object... values) throws Exception;

    public List<TblCompanyDocuments> findByCountTblCompanyDocuments(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCompanyDocumentsCount();
}
