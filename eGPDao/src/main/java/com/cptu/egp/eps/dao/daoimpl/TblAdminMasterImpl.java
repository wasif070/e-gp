/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblAdminMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblAdminMaster;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public class TblAdminMasterImpl extends AbcAbstractClass<TblAdminMaster> implements TblAdminMasterDao {

    @Override
    public void addTblAdminMaster(TblAdminMaster admin) {

        super.addEntity(admin);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblAdminMaster(TblAdminMaster admin) {

        super.deleteEntity(admin);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblAdminMaster(TblAdminMaster admin) {

        super.updateEntity(admin);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblAdminMaster> getAllTblAdminMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblAdminMaster> findTblAdminMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblAdminMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblAdminMaster> findByCountTblAdminMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
