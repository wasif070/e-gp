package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblEvalServiceWeightageDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalServiceWeightage;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblEvalServiceWeightageImpl extends AbcAbstractClass<TblEvalServiceWeightage> implements TblEvalServiceWeightageDao {

    @Override
    public void addTblEvalServiceWeightage(TblEvalServiceWeightage evalServiceWeightage){
        super.addEntity(evalServiceWeightage);
    }

    @Override
    public void deleteTblEvalServiceWeightage(TblEvalServiceWeightage evalServiceWeightage) {
        super.deleteEntity(evalServiceWeightage);
    }

    @Override
    public void updateTblEvalServiceWeightage(TblEvalServiceWeightage evalServiceWeightage) {
        super.updateEntity(evalServiceWeightage);
    }

    @Override
    public List<TblEvalServiceWeightage> getAllTblEvalServiceWeightage() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEvalServiceWeightage> findTblEvalServiceWeightage(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEvalServiceWeightageCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalServiceWeightage> findByCountTblEvalServiceWeightage(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
