package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblReportFormulaMaster;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblReportFormulaMasterDao extends GenericDao<TblReportFormulaMaster> {

    public void addTblReportFormulaMaster(TblReportFormulaMaster tblReportFormulaMaster);

    public void deleteTblReportFormulaMaster(TblReportFormulaMaster tblReportFormulaMaster);

    public void deleteAllTblReportFormulaMaster(Collection tblReportFormulaMaster);

    public void updateTblReportFormulaMaster(TblReportFormulaMaster tblReportFormulaMaster);

    public List<TblReportFormulaMaster> getAllTblReportFormulaMaster();

    public List<TblReportFormulaMaster> findTblReportFormulaMaster(Object... values) throws Exception;

    public List<TblReportFormulaMaster> findByCountTblReportFormulaMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblReportFormulaMasterCount();

    public void updateOrSaveAllRepFormula(List<TblReportFormulaMaster> tblReportFormulaMaster);
}
