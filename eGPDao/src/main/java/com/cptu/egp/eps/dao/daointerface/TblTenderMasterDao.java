/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderMasterDao extends GenericDao<TblTenderMaster>{

    public void addTblTenderMaster(TblTenderMaster Company);

    public void deleteTblTenderMaster(TblTenderMaster Company);

    public void updateTblTenderMaster(TblTenderMaster Company);

    public List<TblTenderMaster> getAllTblTenderMaster();

    public List<TblTenderMaster> findTblTenderMaster(Object... values) throws Exception;

    public List<TblTenderMaster> findByCountTblTenderMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderMasterCount();
}
