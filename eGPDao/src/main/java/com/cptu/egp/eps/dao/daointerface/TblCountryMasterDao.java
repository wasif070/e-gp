package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCountryMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCountryMasterDao extends GenericDao<TblCountryMaster> {

    public List<TblCountryMaster> getAllTblCountryMaster();

    public List<TblCountryMaster> findTblCountryMaster(Object... values) throws Exception;

    public List<TblCountryMaster> findByCountTblCountryMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCountryMasterCount();
}
