package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsWpDetail;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsWpDetailDao extends GenericDao<TblCmsWpDetail> {

    public void addTblCmsWpDetail(TblCmsWpDetail event);

    public void deleteTblCmsWpDetail(TblCmsWpDetail event);

    public void updateTblCmsWpDetail(TblCmsWpDetail event);

    public List<TblCmsWpDetail> getAllTblCmsWpDetail();

    public List<TblCmsWpDetail> findTblCmsWpDetail(Object... values) throws Exception;

    public List<TblCmsWpDetail> findByCountTblCmsWpDetail(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsWpDetailCount();

    public void updateOrSaveEstCost(List<TblCmsWpDetail> estCost) throws Exception;
}
