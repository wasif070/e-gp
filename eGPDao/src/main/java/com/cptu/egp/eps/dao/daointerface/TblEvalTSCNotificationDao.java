package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalTscnotification;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblEvalTSCNotificationDao extends GenericDao<TblEvalTscnotification> {

    public void addTblEvalTscnotification(TblEvalTscnotification tblObj);

    public void deleteTblEvalTscnotification(TblEvalTscnotification tblObj);

    public void updateTblEvalTscnotification(TblEvalTscnotification tblObj);

    public List<TblEvalTscnotification> getAllTblEvalTscnotification();

    public List<TblEvalTscnotification> findTblEvalTscnotification(Object... values) throws Exception;

    public List<TblEvalTscnotification> findByCountTblEvalTscnotification(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalTscnotificationCount();
}
