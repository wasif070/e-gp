package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblWeekEndDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblWeekEnd;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblWeekEndImpl extends AbcAbstractClass<TblWeekEnd> implements TblWeekEndDao {

    @Override
    public void addTblWeekEnd(TblWeekEnd master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblWeekEnd(TblWeekEnd master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblWeekEnd(TblWeekEnd master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblWeekEnd> getAllTblWeekEnd() {
        return super.getAllEntity();
    }

    @Override
    public List<TblWeekEnd> findTblWeekEnd(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblWeekEndCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblWeekEnd> findByCountTblWeekEnd(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
