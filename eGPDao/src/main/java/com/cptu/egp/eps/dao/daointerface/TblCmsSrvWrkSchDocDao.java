package com.cptu.egp.eps.dao.daointerface;


import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvWrkSchDoc;
import java.util.List;


	public interface TblCmsSrvWrkSchDocDao extends GenericDao<TblCmsSrvWrkSchDoc> {

	public void addTblCmsSrvWrkSchDoc(TblCmsSrvWrkSchDoc tblObj);

	public void deleteTblCmsSrvWrkSchDoc(TblCmsSrvWrkSchDoc tblObj);

	public void updateTblCmsSrvWrkSchDoc(TblCmsSrvWrkSchDoc tblObj);

	public List<TblCmsSrvWrkSchDoc> getAllTblCmsSrvWrkSchDoc();

	public List<TblCmsSrvWrkSchDoc> findTblCmsSrvWrkSchDoc(Object... values) throws Exception;

	public List<TblCmsSrvWrkSchDoc> findByCountTblCmsSrvWrkSchDoc(int firstResult,int maxResult,Object... values) throws Exception;

	public long getTblCmsSrvWrkSchDocCount();

	public void updateOrSaveTblCmsSrvWrkSchDoc(List<TblCmsSrvWrkSchDoc> tblObj);
}