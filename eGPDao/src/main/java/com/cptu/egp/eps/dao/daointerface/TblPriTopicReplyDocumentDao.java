package com.cptu.egp.eps.dao.daointerface;


import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblPriTopicReplyDocument;
import java.util.List;


	public interface TblPriTopicReplyDocumentDao extends GenericDao<TblPriTopicReplyDocument> {

	public void addTblPriTopicReplyDocument(TblPriTopicReplyDocument tblObj);

	public void deleteTblPriTopicReplyDocument(TblPriTopicReplyDocument tblObj);

	public void updateTblPriTopicReplyDocument(TblPriTopicReplyDocument tblObj);

	public List<TblPriTopicReplyDocument> getAllTblPriTopicReplyDocument();

	public List<TblPriTopicReplyDocument> findTblPriTopicReplyDocument(Object... values) throws Exception;

	public List<TblPriTopicReplyDocument> findByCountTblPriTopicReplyDocument(int firstResult,int maxResult,Object... values) throws Exception;

	public long getTblPriTopicReplyDocumentCount();

	public void updateOrSaveTblPriTopicReplyDocument(List<TblPriTopicReplyDocument> tblObj);
}