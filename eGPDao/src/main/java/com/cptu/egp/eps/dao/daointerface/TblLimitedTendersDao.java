/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblLimitedTenders;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblLimitedTendersDao extends GenericDao<TblLimitedTenders>{

    public void addTblLimitedTenders(TblLimitedTenders tblObj);

    public void deleteTblLimitedTenders(TblLimitedTenders tblObj);

    public void updateTblLimitedTenders(TblLimitedTenders tblObj);

    public List<TblLimitedTenders> getAllTblLimitedTenders();

    public List<TblLimitedTenders> findTblLimitedTenders(Object... values) throws Exception;

    public List<TblLimitedTenders> findByCountTblLimitedTenders(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblLimitedTendersCount();
}
