package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvPshistoryDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvPshistory;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsSrvPshistoryImpl extends AbcAbstractClass<TblCmsSrvPshistory> implements TblCmsSrvPshistoryDao {
  @Override
    public void addTblCmsSrvPshistory(TblCmsSrvPshistory cmsPsDetail){
        super.addEntity(cmsPsDetail);
    }

    @Override
    public void deleteTblCmsSrvPshistory(TblCmsSrvPshistory cmsPsDetail) {
        super.deleteEntity(cmsPsDetail);
    }

    @Override
    public void updateTblCmsSrvPshistory(TblCmsSrvPshistory cmsPsDetail) {
        super.updateEntity(cmsPsDetail);
    }

    @Override
    public List<TblCmsSrvPshistory> getAllTblCmsSrvPshistory() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvPshistory> findTblCmsSrvPshistory(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvPshistoryCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvPshistory> findByCountTblCmsSrvPshistory(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsSrvPshistory> estCost) {
       super.updateAll(estCost);
    }
}
