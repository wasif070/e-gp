/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblReportColumnMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblReportColumnMaster;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblReportColumnMasterImpl extends AbcAbstractClass<TblReportColumnMaster> implements TblReportColumnMasterDao{

    @Override
    public void addTblReportColumnMaster(TblReportColumnMaster reportColumnMaster) {
        super.addEntity(reportColumnMaster);
    }

    @Override
    public List<TblReportColumnMaster> findTblReportColumnMaster(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblReportColumnMaster(TblReportColumnMaster department) {

        super.deleteEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblReportColumnMaster(Collection event) {
        super.deleteAll(event);
    }

    @Override
    public void updateTblReportColumnMaster(TblReportColumnMaster department) {

        super.updateEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblReportColumnMaster> getAllTblReportColumnMaster() {
        return super.getAllEntity();
    }

    @Override
    public long getTblReportColumnMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblReportColumnMaster> findByCountTblReportColumnMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveReportColumn(List<TblReportColumnMaster> event) {
        super.updateAll(event);
    }
}
