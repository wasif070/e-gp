/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblNegBidDtlSrvDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblNegBidDtlSrv;
import java.util.List;
/**
 *
 * @author dipal.shah
 */
public class TblNegBidDtlSrvImpl extends AbcAbstractClass<TblNegBidDtlSrv> implements TblNegBidDtlSrvDao 
{
    @Override
    public void addTblNegBidDtlSrv(TblNegBidDtlSrv master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblNegBidDtlSrv(TblNegBidDtlSrv master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblNegBidDtlSrv(TblNegBidDtlSrv master) {
        super.updateEntity(master);
    }
}
