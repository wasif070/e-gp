/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Administrator
 */
public class SPCommon extends StoredProcedure {
    private static final Logger LOGGER = Logger.getLogger(SPCommon.class);
    public SPCommon(BasicDataSource dataSource, String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("v_ToEmailId_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_FromEmailId_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_CCEmailId_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Subject_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_MsgText_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_ProcessUrl_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_IsPriority_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_MsgBoxType_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Action_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_MsgStatus_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_FolderId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_MsgReadStatus_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_IsMsgReply_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_MsgDocId_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_UpdMsgId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_msgBoxTypeIdInt",Types.INTEGER));
    }

    /**
     * Execute stored procedure : p_add_upd_mailmessage
     * To perform Insert/Update/Delete Mail Messages.
     * @param toEmailId
     * @param fromEmailId
     * @param ccEmailId
     * @param subject
     * @param msgText
     * @param procURL
     * @param isPriority
     * @param msfBoxType
     * @param action
     * @param msgStatus
     * @param folderId
     * @param msgReadStatus
     * @param isMsgReply
     * @param msgDocId
     * @param updMsgId
     * @param messageInboxId
     * @return CommonMsgChk list contains flags indicate operation success status.
     */
    public List<CommonMsgChk> executeProcedure(String toEmailId,String fromEmailId,String ccEmailId,String subject,String msgText,String procURL,String isPriority,String msfBoxType,String action,String msgStatus,Integer folderId,String msgReadStatus,String isMsgReply,String msgDocId,Integer updMsgId,Integer messageInboxId) {
        Map inParams = new HashMap();
        inParams.put("v_ToEmailId_inVc", toEmailId);
        inParams.put("v_FromEmailId_inVc", fromEmailId);
        inParams.put("v_CCEmailId_inVc", ccEmailId);
        inParams.put("v_Subject_inVc", subject);
        inParams.put("v_MsgText_inVc", msgText);
        inParams.put("v_ProcessUrl_inVc", procURL);
        inParams.put("v_IsPriority_inVc", isPriority);
        inParams.put("v_MsgBoxType_inVc", msfBoxType);
        inParams.put("v_Action_inVc", action);
        inParams.put("v_MsgStatus_inVc", msgStatus);
        inParams.put("v_FolderId_inInt", folderId);
        inParams.put("v_MsgReadStatus_inVc", msgReadStatus);
        inParams.put("v_IsMsgReply_inVc", isMsgReply);
        inParams.put("v_MsgDocId_inVc", msgDocId);
        inParams.put("v_UpdMsgId_inInt", updMsgId);
        inParams.put("v_msgBoxTypeIdInt", messageInboxId);
        
        this.compile();
        List<CommonMsgChk> details = new ArrayList<CommonMsgChk>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
             if(list!=null && !list.isEmpty()){
                for (LinkedHashMap<String, Object> linkedHashMap : list) {
                    CommonMsgChk commonMsgChk = new CommonMsgChk();

                    commonMsgChk.setFlag((Boolean) linkedHashMap.get("flag"));
                    commonMsgChk.setMsg((String) linkedHashMap.get("msg"));
                    commonMsgChk.setId((Integer) linkedHashMap.get("Id"));
                    details.add(commonMsgChk);
                }
              }
            LOGGER.debug("No data found ");
        } catch (Exception e) {
            LOGGER.error("SPCommon : "+ e);
        }
        return details;
    }
}
