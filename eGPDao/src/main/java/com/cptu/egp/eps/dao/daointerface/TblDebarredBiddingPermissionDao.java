package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblDebarredBiddingPermission;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblDebarredBiddingPermissionDao extends GenericDao<TblDebarredBiddingPermission> {

    public void addTblDebarredBiddingPermission(TblDebarredBiddingPermission tblObj);

    public void deleteTblDebarredBiddingPermission(TblDebarredBiddingPermission tblObj);

    public void updateTblDebarredBiddingPermission(TblDebarredBiddingPermission tblObj);

    public List<TblDebarredBiddingPermission> getAllTblDebarredBiddingPermission();

    public List<TblDebarredBiddingPermission> findTblDebarredBiddingPermission(Object... values) throws Exception;

    public List<TblDebarredBiddingPermission> findByCountTblDebarredBiddingPermission(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblDebarredBiddingPermissionCount();
}
