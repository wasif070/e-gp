package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblEvalTsccommentsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalTsccomments;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblEvalTsccommentsImpl extends AbcAbstractClass<TblEvalTsccomments> implements TblEvalTsccommentsDao {

    @Override
    public void addTblEvalTsccomments(TblEvalTsccomments evalTsccomments){
        super.addEntity(evalTsccomments);
    }

    @Override
    public void deleteTblEvalTsccomments(TblEvalTsccomments evalTsccomments) {
        super.deleteEntity(evalTsccomments);
    }

    @Override
    public void updateTblEvalTsccomments(TblEvalTsccomments evalTsccomments) {
        super.updateEntity(evalTsccomments);
    }

    @Override
    public long getTblEvalTsccommentsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalTsccomments> getAllTblEvalTsccomments() {
         return super.getAllEntity();
    }

    @Override
    public List<TblEvalTsccomments> findTblEvalTsccomments(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public List<TblEvalTsccomments> findByCountTblEvalTsccomments(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
