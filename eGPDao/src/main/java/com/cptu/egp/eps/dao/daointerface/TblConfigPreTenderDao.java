package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblConfigPreTender;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblConfigPreTenderDao extends GenericDao<TblConfigPreTender> {

    public void addTblConfigPreTender(TblConfigPreTender login);

    public void deleteTblConfigPreTender(TblConfigPreTender login);

    public void deleteAllTblConfigPreTender(Collection entity);

    public void updateTblConfigPreTender(TblConfigPreTender login);

    public List<TblConfigPreTender> getAllTblConfigPreTender();

    public List<TblConfigPreTender> findTblConfigPreTender(Object... values) throws Exception;

    public List<TblConfigPreTender> findByCountTblConfigPreTender(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblConfigPreTenderCount();
}
