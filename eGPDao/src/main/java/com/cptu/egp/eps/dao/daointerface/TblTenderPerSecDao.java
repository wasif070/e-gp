package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderPerfSecurity;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblTenderPerSecDao extends GenericDao<TblTenderPerfSecurity> {

    public void addTblTenderPerfSecurity(TblTenderPerfSecurity tblTenderPerfSecurity);

    public void deleteTblTenderPerfSecurity(TblTenderPerfSecurity tblTenderPerfSecurity);

    public void updateTblTenderPerfSecurity(TblTenderPerfSecurity tblTenderPerfSecurity);

    public List<TblTenderPerfSecurity> getAllTblTenderPerfSecurity();

    public List<TblTenderPerfSecurity> findTblTenderPerfSecurity(Object... values) throws Exception;

    public List<TblTenderPerfSecurity> findByCountTblTenderPerfSecurity(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderPerfSecurityCount();

    public void updateOrSavePerSec(List<TblTenderPerfSecurity> tblTenderPerfSecurity);
}
