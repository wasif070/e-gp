/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblNegQryDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblNegQryDocs;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblNegQryDocsImpl extends AbcAbstractClass<TblNegQryDocs> implements TblNegQryDocsDao{

    @Override
    public void addTblNegQryDocs(TblNegQryDocs negQryDocs) {
        super.addEntity(negQryDocs);
    }

    @Override
    public List<TblNegQryDocs> findTblNegQryDocs(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblNegQryDocs(TblNegQryDocs department) {

        super.deleteEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblNegQryDocs(Collection event) {
        super.deleteAll(event);
    }

    @Override
    public void updateTblNegQryDocs(TblNegQryDocs department) {

        super.updateEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblNegQryDocs> getAllTblNegQryDocs() {
        return super.getAllEntity();
    }

    @Override
    public long getTblNegQryDocsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblNegQryDocs> findByCountTblNegQryDocs(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
