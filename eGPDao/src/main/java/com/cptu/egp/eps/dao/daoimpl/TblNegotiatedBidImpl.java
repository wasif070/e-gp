package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblNegotiatedBidDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblNegotiatedBid;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblNegotiatedBidImpl extends AbcAbstractClass<TblNegotiatedBid> implements TblNegotiatedBidDao {

    @Override
    public void addTblNegotiatedBid(TblNegotiatedBid master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblNegotiatedBid(TblNegotiatedBid master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblNegotiatedBid(TblNegotiatedBid master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblNegotiatedBid> getAllTblNegotiatedBid() {
        return super.getAllEntity();
    }

    @Override
    public List<TblNegotiatedBid> findTblNegotiatedBid(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblNegotiatedBidCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblNegotiatedBid> findByCountTblNegotiatedBid(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
