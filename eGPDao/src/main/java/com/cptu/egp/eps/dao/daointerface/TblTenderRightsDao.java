/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderRights;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TblTenderRightsDao extends GenericDao<TblTenderRights> {

    public List<Object[]> createNewQuery(String query);

    public List<Object[]> createByCountNewQuery(String query,int firstResult,int maxResult);

    public long countForNewQuery(String from,String where)throws Exception;

    public List<Object> getSingleColQuery(String query);

    public int updateDeleteNewQuery(String query);

    public int updateDeleteSQLNewQuery(String query);
}
