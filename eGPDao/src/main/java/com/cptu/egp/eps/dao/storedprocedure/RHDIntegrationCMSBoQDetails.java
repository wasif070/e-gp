/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.math.BigDecimal;
import java.util.Date;
/**
 *This class is created for web-service(RHD upto CMS BoQ data Integration)
 * @author Sudhir Chavhan
 */
public class RHDIntegrationCMSBoQDetails {

    private String BoQ_ItemGroup;
    private String BoQ_ItemSrNo;
    private String BoQ_ItemDesc;
    private String BoQ_UnitofMeasur;
    private BigDecimal BoQ_ItemQnty;
    private Date BoQ_WPstartDt;
    private Date BoQ_WPEndDt;
    private int BoQ_WPNoOfDays;
    private BigDecimal BoQ_UnitRate;

    public String getBoQ_ItemDesc() {
        return BoQ_ItemDesc;
    }

    public void setBoQ_ItemDesc(String BoQ_ItemDesc) {
        this.BoQ_ItemDesc = BoQ_ItemDesc;
    }

    public String getBoQ_ItemGroup() {
        return BoQ_ItemGroup;
    }

    public void setBoQ_ItemGroup(String BoQ_ItemGroup) {
        this.BoQ_ItemGroup = BoQ_ItemGroup;
    }

    public BigDecimal getBoQ_ItemQnty() {
        return BoQ_ItemQnty;
    }

    public void setBoQ_ItemQnty(BigDecimal BoQ_ItemQnty) {
        this.BoQ_ItemQnty = BoQ_ItemQnty;
    }

    public String getBoQ_ItemSrNo() {
        return BoQ_ItemSrNo;
    }

    public void setBoQ_ItemSrNo(String BoQ_ItemSrNo) {
        this.BoQ_ItemSrNo = BoQ_ItemSrNo;
    }

    public String getBoQ_UnitofMeasur() {
        return BoQ_UnitofMeasur;
    }

    public void setBoQ_UnitofMeasur(String BoQ_UnitofMeasur) {
        this.BoQ_UnitofMeasur = BoQ_UnitofMeasur;
    }

    public Date getBoQ_WPEndDt() {
        return BoQ_WPEndDt;
    }

    public void setBoQ_WPEndDt(Date BoQ_WPEndDt) {
        this.BoQ_WPEndDt = BoQ_WPEndDt;
    }

    public int getBoQ_WPNoOfDays() {
        return BoQ_WPNoOfDays;
    }

    public void setBoQ_WPNoOfDays(int BoQ_WPNoOfDays) {
        this.BoQ_WPNoOfDays = BoQ_WPNoOfDays;
    }

    public Date getBoQ_WPstartDt() {
        return BoQ_WPstartDt;
    }

    public void setBoQ_WPstartDt(Date BoQ_WPstartDt) {
        this.BoQ_WPstartDt = BoQ_WPstartDt;
    }

    public BigDecimal getBoQ_UnitRate() {
        return BoQ_UnitRate;
    }

    public void setBoQ_UnitRate(BigDecimal BoQ_UnitRate) {
        this.BoQ_UnitRate = BoQ_UnitRate;
    }
    
}
