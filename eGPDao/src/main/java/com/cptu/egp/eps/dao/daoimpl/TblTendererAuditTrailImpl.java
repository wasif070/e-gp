package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblTendererAuditTrailDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTendererAuditTrail;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblTendererAuditTrailImpl extends AbcAbstractClass<TblTendererAuditTrail> implements TblTendererAuditTrailDao {

    @Override
    public void addTblTendererAuditTrail(TblTendererAuditTrail tendererAuditTrail){
        super.addEntity(tendererAuditTrail);
    }

    @Override
    public void deleteTblTendererAuditTrail(TblTendererAuditTrail tendererAuditTrail) {
        super.deleteEntity(tendererAuditTrail);
    }

    @Override
    public void updateTblTendererAuditTrail(TblTendererAuditTrail tendererAuditTrail) {
        super.updateEntity(tendererAuditTrail);
    }

    @Override
    public List<TblTendererAuditTrail> getAllTblTendererAuditTrail() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTendererAuditTrail> findTblTendererAuditTrail(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTendererAuditTrailCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTendererAuditTrail> findByCountTblTendererAuditTrail(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
