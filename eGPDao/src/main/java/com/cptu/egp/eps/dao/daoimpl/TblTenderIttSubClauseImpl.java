/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderIttSubClauseDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderIttSubClause;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderIttSubClauseImpl extends AbcAbstractClass<TblTenderIttSubClause> implements TblTenderIttSubClauseDao {

    @Override
    public void addTblTenderIttSubClause(TblTenderIttSubClause tenderIttSubClause) {
        super.addEntity(tenderIttSubClause);
    }

    @Override
    public void deleteTblTenderIttSubClause(TblTenderIttSubClause tenderIttSubClause) {
        super.deleteEntity(tenderIttSubClause);
    }

    @Override
    public void updateTblTenderIttSubClause(TblTenderIttSubClause tenderIttSubClause) {
        super.updateEntity(tenderIttSubClause);
    }

    @Override
    public List<TblTenderIttSubClause> getAllTblTenderIttSubClause() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderIttSubClause> findTblTenderIttSubClause(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderIttSubClauseCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderIttSubClause> findByCountTblTenderIttSubClause(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
