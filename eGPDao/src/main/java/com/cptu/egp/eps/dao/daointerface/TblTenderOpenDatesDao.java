package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderOpenDates;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderOpenDatesDao extends GenericDao<TblTenderOpenDates> {

    public void addTblTenderOpenDates(TblTenderOpenDates tblTenderOpenDates);

    public void deleteTblTenderOpenDates(TblTenderOpenDates tblTenderOpenDates);

    public void deleteAllTblTenderOpenDates(Collection tblTenderOpenDates);

    public void updateTblTenderOpenDates(TblTenderOpenDates tblTenderOpenDates);

    public List<TblTenderOpenDates> getAllTblTenderOpenDates();

    public List<TblTenderOpenDates> findTblTenderOpenDates(Object... values) throws Exception;

    public List<TblTenderOpenDates> findByCountTblTenderOpenDates(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderOpenDatesCount();
}
