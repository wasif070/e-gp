package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvAttSheetMaster;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsSrvAttSheetMasterDao extends GenericDao<TblCmsSrvAttSheetMaster> {

    public void addTblCmsSrvAttSheetMaster(TblCmsSrvAttSheetMaster tblObj);

    public void deleteTblCmsSrvAttSheetMaster(TblCmsSrvAttSheetMaster tblObj);

    public void updateTblCmsSrvAttSheetMaster(TblCmsSrvAttSheetMaster tblObj);

    public List<TblCmsSrvAttSheetMaster> getAllTblCmsSrvAttSheetMaster();

    public List<TblCmsSrvAttSheetMaster> findTblCmsSrvAttSheetMaster(Object... values) throws Exception;

    public List<TblCmsSrvAttSheetMaster> findByCountTblCmsSrvAttSheetMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsSrvAttSheetMasterCount();
    
    public void updateInsertAllTblCmsSrvAttSheetMaster(List<TblCmsSrvAttSheetMaster> list);
}
