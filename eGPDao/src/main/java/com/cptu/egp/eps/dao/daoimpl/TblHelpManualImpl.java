package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblHelpManualDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblHelpManual;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblHelpManualImpl extends AbcAbstractClass<TblHelpManual> implements TblHelpManualDao {

    @Override
    public void addTblHelpManual(TblHelpManual helpManual){
        super.addEntity(helpManual);
    }

    @Override
    public void deleteTblHelpManual(TblHelpManual helpManual) {
        super.deleteEntity(helpManual);
    }

    @Override
    public void updateTblHelpManual(TblHelpManual helpManual) {
        super.updateEntity(helpManual);
    }

    @Override
    public List<TblHelpManual> getAllTblHelpManual() {
        return super.getAllEntity();
    }

    @Override
    public List<TblHelpManual> findTblHelpManual(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblHelpManualCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblHelpManual> findByCountTblHelpManual(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
