package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblComplaintPaymentDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.hibernate.Query;
/**
 *
 * @author Radha
 */
public class TblComplaintPaymentDaoImpl extends AbcAbstractClass<TblComplaintPayments> implements TblComplaintPaymentDao {

	final static Logger log=Logger.getLogger("TblComplaintPaymentDaoImpl.class");
    @Override
    public void addComplaintPayment(TblComplaintPayments complaintPayment){
    	 super.addEntity(complaintPayment);
    }

    @Override
    public void updateComplaintPayment(TblComplaintPayments complaintPayment){
    	super.updateEntity(complaintPayment); 
    }


 /*
    @Override
    public void deleteTblComplaintMaster(TblComplaintMaster complaint) {
        super.deleteEntity(complaint);
    }

    @Override
    public void updateTblComplaintMaster(TblComplaintMaster complaint) {
        super.updateEntity(complaint);
    }

    @Override
    public List<TblComplaintMaster> getAllComplaints() {
        return super.getAllEntity();
    }

   */
       
    public List<TblComplaintPayments>   findComplaintPayments(Object... values) throws Exception{
    	
    	return super.findEntity(values);
    	
    }

    public TblComplaintPayments getPaymentDetails(String feetype, Integer id, Integer compId) throws Exception {
        List<TblComplaintPayments> tcp = super.findEntity("complaintMaster",Operation_enum.EQ,new TblComplaintMaster(compId),"paymentFor",Operation_enum.EQ,feetype);
        TblComplaintPayments payments = null;
        if(tcp!=null && !tcp.isEmpty()){
            payments = tcp.get(0);
        }
        return payments;
    }

    @Override
    public int getGovUserId() throws Exception {
        int gUserId = 0;
        String query = "select te.govUserId from TblLoginMaster tl ,TblEmployeeTrasfer te where tl.tblUserTypeMaster.userTypeId = 16 and tl.userId = te.tblLoginMaster.userId";
        List<Object> list = super.singleColQuery(query);
        if(list!=null && !list.isEmpty()){
            gUserId = (Integer)list.get(0);
        }
        return gUserId;
    }
    
    
   }














