/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblPreTenderMetDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblPreTenderMetDocs;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblPreTenderMetDocsImpl extends AbcAbstractClass<TblPreTenderMetDocs> implements TblPreTenderMetDocsDao {

    @Override
    public void addTblPreTenderMetDocs(TblPreTenderMetDocs preTenderMetDocs) {
        super.addEntity(preTenderMetDocs);
    }

    @Override
    public void deleteTblPreTenderMetDocs(TblPreTenderMetDocs preTenderMetDocs) {
        super.deleteEntity(preTenderMetDocs);
    }

    @Override
    public void updateTblPreTenderMetDocs(TblPreTenderMetDocs preTenderMetDocs) {
        super.updateEntity(preTenderMetDocs);
    }

    @Override
    public List<TblPreTenderMetDocs> getAllTblPreTenderMetDocs() {
        return super.getAllEntity();
    }

    @Override
    public List<TblPreTenderMetDocs> findTblPreTenderMetDocs(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblPreTenderMetDocsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblPreTenderMetDocs> findByCountTblPreTenderMetDocs(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
