/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Ramesh.Janagondakuruba
 */
public class SearchAppData extends StoredProcedure
{
    private static final Logger LOGGER = Logger.getLogger(SearchAppData.class);
    public SearchAppData(BasicDataSource dataSource, String procName)
    {

        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("v_Keyword_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_financialYear_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_procurementnature_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_departmentId_inN", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_employeeId_inN", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_cpvCode_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_projectName_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_procurementType_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_appid_inN", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_appcode_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_packageno_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_pkgEstCost_M", Types.DECIMAL));
        this.declareParameter(new SqlParameter("v_officeId_N", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_budgetType_tint", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_operator_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_value1_inVc", Types.DECIMAL));
        this.declareParameter(new SqlParameter("v_value2_inVc", Types.DECIMAL));
        this.declareParameter(new SqlParameter("v_Page_inN", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_RecordPerPage_inN", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_status_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_userId_inInt", Types.INTEGER));
    }

    /**
     * Execute stored procedure : p_search_appdata
     * Get APP Search data
     * @param keyword
     * @param financialYear
     * @param procurementnature
     * @param departmentId
     * @param employeeId
     * @param cpvCode
     * @param projectName
     * @param procurementType
     * @param appid
     * @param appcode
     * @param packageno
     * @param pkgEstCost
     * @param officeId
     * @param budgetType
     * @param operator
     * @param value1
     * @param value2
     * @param pageNo
     * @param recordOffset
     * @param status
     * @param userId
     * @return App Search information as list of CommonAppSearchData object
     */
    public List<CommonAppSearchData> executeProcedure(String keyword, String financialYear, String procurementnature, Integer departmentId, Integer employeeId, String cpvCode, String projectName, String procurementType, Integer appid, String appcode, String packageno, BigDecimal pkgEstCost, Integer officeId, String budgetType, String operator, BigDecimal value1, BigDecimal value2, Integer pageNo, Integer recordOffset,String status,Integer userId)
    {

        Map inParams = new HashMap();
        inParams.put("v_Keyword_inVc", keyword);
        inParams.put("v_financialYear_inVc", financialYear);
        inParams.put("v_procurementnature_inVc", procurementnature);
        inParams.put("v_departmentId_inN", departmentId);
        inParams.put("v_employeeId_inN", employeeId);
        inParams.put("v_cpvCode_inVc", cpvCode);
        inParams.put("v_projectName_inVc", projectName);
        inParams.put("v_procurementType_inVc", procurementType);
        inParams.put("v_appid_inN", appid);
        inParams.put("v_appcode_inVc", appcode);
        inParams.put("v_packageno_inVc", packageno);
        inParams.put("v_pkgEstCost_M", pkgEstCost);
        inParams.put("v_officeId_N", officeId);
        inParams.put("v_budgetType_tint", budgetType);
        inParams.put("v_operator_inVc", operator);
        inParams.put("v_value1_inVc", value1);
        inParams.put("v_value2_inVc", value2);
        inParams.put("v_Page_inN", pageNo);
        inParams.put("v_RecordPerPage_inN", recordOffset);
        inParams.put("v_status_inVc", status);
        inParams.put("v_userId_inInt", userId);
        this.compile();
        List<CommonAppSearchData> details = new ArrayList<CommonAppSearchData>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {

                CommonAppSearchData cmmAppSearchData=new CommonAppSearchData();
                cmmAppSearchData.setAppId((Integer)linkedHashMap.get("appId"));
                cmmAppSearchData.setAppCode((String)linkedHashMap.get("appCode"));
                cmmAppSearchData.setDepartmentName((String)linkedHashMap.get("departmentName"));
                cmmAppSearchData.setEmployeeName((String)linkedHashMap.get("employeeName"));
                cmmAppSearchData.setStateName((String)linkedHashMap.get("stateName"));
                cmmAppSearchData.setProcurementNature((String)linkedHashMap.get("procurementnature"));
                cmmAppSearchData.setPackageNo((String)linkedHashMap.get("packageNo"));
                cmmAppSearchData.setPackageDesc((String)linkedHashMap.get("packageDesc"));
                cmmAppSearchData.setProjectName((String)linkedHashMap.get("projectName"));
                cmmAppSearchData.setFinancialYear((String)linkedHashMap.get("financialYear"));
                cmmAppSearchData.setDepartmentId((Integer)linkedHashMap.get("departmentId"));
                cmmAppSearchData.setEmployeeId((Integer)linkedHashMap.get("employeeId"));
                cmmAppSearchData.setCpvCode((String)linkedHashMap.get("cpvCode"));
                cmmAppSearchData.setProcurementType((String)linkedHashMap.get("procurementType"));
                cmmAppSearchData.setEstimatedCost((BigDecimal)linkedHashMap.get("estimatedCost"));
                cmmAppSearchData.setPkgEstCost((BigDecimal)linkedHashMap.get("pkgEstCost"));
                cmmAppSearchData.setOfficeId((Integer)linkedHashMap.get("officeId"));
                cmmAppSearchData.setBudgetType((String)linkedHashMap.get("budgetType"));
                cmmAppSearchData.setProcurementMethod((String)linkedHashMap.get("procurementMethod"));
                cmmAppSearchData.setOfficeName((String)linkedHashMap.get("officeName"));
                cmmAppSearchData.setTotalRecords((Integer)linkedHashMap.get("TotalRecords"));
                cmmAppSearchData.setPackageId((Integer)linkedHashMap.get("packageId"));
                cmmAppSearchData.setWorkFlowStatus((String)linkedHashMap.get("workflowStatus"));
                cmmAppSearchData.setStatus((String)linkedHashMap.get("status"));
                details.add(cmmAppSearchData);
            }
        }
        catch (Exception e) {
            LOGGER.error("SearchAppData : "+ e);
        }
        return details;

    }
}

