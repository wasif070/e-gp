package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblGccSubClause;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblGccSubClauseDao extends GenericDao<TblGccSubClause> {

    public void addTblGccSubClause(TblGccSubClause tblObj);

    public void deleteTblGccSubClause(TblGccSubClause tblObj);

    public void updateTblGccSubClause(TblGccSubClause tblObj);

    public List<TblGccSubClause> getAllTblGccSubClause();

    public List<TblGccSubClause> findTblGccSubClause(Object... values) throws Exception;

    public List<TblGccSubClause> findByCountTblGccSubClause(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblGccSubClauseCount();
}
