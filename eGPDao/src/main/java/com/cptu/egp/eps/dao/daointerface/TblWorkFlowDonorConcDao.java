/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblWorkFlowDonorConc;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public interface TblWorkFlowDonorConcDao extends GenericDao<TblWorkFlowDonorConc> {

    public void addTblWorkFlowDonorConc(TblWorkFlowDonorConc admin);

    public void deleteTblWorkFlowDonorConc(TblWorkFlowDonorConc admin);

    public void updateTblWorkFlowDonorConc(TblWorkFlowDonorConc admin);

    public List<TblWorkFlowDonorConc> getAllTblWorkFlowDonorConc();

    public List<TblWorkFlowDonorConc> findTblWorkFlowDonorConc(Object... values) throws Exception;

    public List<TblWorkFlowDonorConc> findByCountTblWorkFlowDonorConc(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblWorkFlowDonorConcCount();
}
