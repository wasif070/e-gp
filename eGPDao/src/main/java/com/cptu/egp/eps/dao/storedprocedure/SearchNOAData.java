/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

/**
 *
 * @author Krishnraj
 */
public class SearchNOAData {

    private Integer departmentId;
    private String keyword;
    private String contractDtFrom;
    private String contractDtTo;
    private Integer contractId;
    private Integer tenderId;
    private Integer officeId;
    private String contractNo;
    private String cpvCode;
    private String tenderRefNo;
    private String contractAmt;
    private Integer RecordPerPage;
    private String totalPage;
    private String procuringEntity;
    private Integer page;
    private Integer totalRec;
    private String totalRowCount;
    private String MDADetails;
    private String procurementMethod;
    private String contractAwardedTo;
    private String District;
    private String NOADate;

    public String getNOADate() {
        return NOADate;
    }

    public void setNOADate(String nOADate) {
        this.NOADate = nOADate;
    }

    public String getDistrict() {
        return District;
    }

    public void setDistrict(String district) {
        this.District = district;
    }

    public String getContractAwardedTo() {
        return contractAwardedTo;
    }

    public void setContractAwardedTo(String contractAwardedTo) {
        this.contractAwardedTo = contractAwardedTo;
    }

    public String getProcurementMethod() {
        return procurementMethod;
    }

    public void setProcurementMethod(String procurementMethod) {
        this.procurementMethod = procurementMethod;
    }

    public String getMDADetails() {
        return MDADetails;
    }

    public void setMDADetails(String mDADetails) {
        this.MDADetails = mDADetails;
    }

    public String getTotalRowCount() {
        return totalRowCount;
    }

    public void setTotalRowCount(String totalRowCount) {
        this.totalRowCount = totalRowCount;
    }

    public Integer getRecordPerPage() {
        return RecordPerPage;
    }

    public void setRecordPerPage(Integer recordPerPage) {
        this.RecordPerPage = recordPerPage;
    }

    public String getContractAmt() {
        return contractAmt;
    }

    public void setContractAmt(String contractAmt) {
        this.contractAmt = contractAmt;
    }

    public String getContractDtFrom() {
        return contractDtFrom;
    }

    public void setContractDtFrom(String contractDtFrom) {
        this.contractDtFrom = contractDtFrom;
    }

    public String getContractDtTo() {
        return contractDtTo;
    }

    public void setContractDtTo(String contractDtTo) {
        this.contractDtTo = contractDtTo;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getProcuringEntity() {
        return procuringEntity;
    }

    public void setProcuringEntity(String procuringEntity) {
        this.procuringEntity = procuringEntity;
    }

    public String getCpvCode() {
        return cpvCode;
    }

    public void setCpvCode(String cpvCode) {
        this.cpvCode = cpvCode;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Integer getOfficeId() {
        return officeId;
    }

    public void setOfficeId(Integer officeId) {
        this.officeId = officeId;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTenderId() {
        return tenderId;
    }

    public void setTenderId(Integer tenderId) {
        this.tenderId = tenderId;
    }

    public String getTenderRefNo() {
        return tenderRefNo;
    }

    public void setTenderRefNo(String tenderRefNo) {
        this.tenderRefNo = tenderRefNo;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public Integer getTotalRec() {
        return totalRec;
    }

    public void setTotalRec(Integer totalRec) {
        this.totalRec = totalRec;
    }
}
