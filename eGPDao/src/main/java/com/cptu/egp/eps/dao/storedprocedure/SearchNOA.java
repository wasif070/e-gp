/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Krishnraj
 */
public class SearchNOA extends StoredProcedure
{
    private static final Logger LOGGER = Logger.getLogger(SearchNOA.class);
    /* Set the Input Parameter for proc in constructor */
    public SearchNOA(BasicDataSource dataSource, String procName)
    {
        this.setDataSource(dataSource);
        this.setSql(procName);
        this.declareParameter(new SqlParameter("@v_Keyword_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_contractDtFrom_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_contractDtTo_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_departmentId_inN", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_contractId_inN", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_contractNo_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_tenderId_inN", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_tenderRefNo_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_cpvCode_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_contractAmt_inM", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_stateName_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_officeId_inN", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_advDt_inVc", Types.VARCHAR));
        //Code by Proshanto Kumar Saha
        this.declareParameter(new SqlParameter("@v_advDtTo_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_procurementMethod_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_noaDt_inVc", Types.VARCHAR));
        //Code By Proshanto Kumar Saha
        this.declareParameter(new SqlParameter("@v_noaDtTo_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_contractAwardTo_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_Page_inN", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_RecordPerPage_inN", Types.INTEGER));
    }

    /**
     * Execute stored procedure : p_search_noa
     * Search NOA Details.
     * @param keyword
     * @param contractDtFrom
     * @param contractDtTo
     * @param departmentId
     * @param contractId
     * @param contractNo
     * @param tenderId
     * @param tenderRefNo
     * @param cpvCode
     * @param contractAmt
     * @param officeId
     * @param page
     * @param recordPerPage
     * @param stateName
     * @param advDt
     * @param procurementMethod
     * @param noaDt
     * @param contractAwardTo
     * @return NOA data as list of SearchNOAData object
     */
    //Added 2 parameters as advDtTo and noaDtTo by Proshanto Kumar Saha
    public List<SearchNOAData> executeProcedure(String keyword, String contractDtFrom, String contractDtTo,
                                                int departmentId, int contractId, String contractNo, int tenderId,
                                                String tenderRefNo, String cpvCode, String contractAmt, int officeId,
                                                int page, int recordPerPage,String stateName,String advDt,String advDtTo,String procurementMethod,String noaDt,String noaDtTo,String contractAwardTo)
    {
        Map inParams = new HashMap();
        inParams.put("@v_Keyword_inVc", keyword);
        inParams.put("@v_contractDtFrom_inVc", contractDtFrom);
        inParams.put("@v_contractDtTo_inVc", contractDtTo);
        inParams.put("@v_departmentId_inN", departmentId);
        inParams.put("@v_contractId_inN", contractId);
        inParams.put("@v_contractNo_inVc", contractNo);
        inParams.put("@v_tenderId_inN", tenderId);
        inParams.put("@v_tenderRefNo_inVc", tenderRefNo);
        inParams.put("@v_cpvCode_inVc", cpvCode);
        inParams.put("@v_contractAmt_inM", contractAmt);
        inParams.put("@v_stateName_inVc", stateName);
        inParams.put("@v_officeId_inN", officeId);
        inParams.put("@v_advDt_inVc", advDt);
        //Code by Proshanto Kumar Saha
        inParams.put("@v_advDtTo_inVc", advDtTo);
        inParams.put("@v_procurementMethod_inVc", procurementMethod);
        inParams.put("@v_noaDt_inVc", noaDt);
        //Code By Proshanto Kumar Saha
        inParams.put("@v_noaDtTo_inVc", noaDtTo);
        inParams.put("@v_contractAwardTo_inVc", contractAwardTo);
        inParams.put("@v_Page_inN", page);
        inParams.put("@v_RecordPerPage_inN", recordPerPage);

        this.compile();

        List<SearchNOAData> reportData = new ArrayList<SearchNOAData>();

        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            if (list != null && !list.isEmpty()) {
                for (LinkedHashMap<String, Object> linkedHashMap : list) {
                    SearchNOAData searchNOAData = new SearchNOAData();
                    searchNOAData.setMDADetails((String)linkedHashMap.get("MDADetails"));
                    searchNOAData.setProcuringEntity((String)linkedHashMap.get("ProcuringEntity"));
                    searchNOAData.setTenderRefNo((String)linkedHashMap.get("ReferenceNo"));
                    searchNOAData.setKeyword((String)linkedHashMap.get("ContractName"));
                    searchNOAData.setDistrict((String)linkedHashMap.get("District"));
                    searchNOAData.setContractAmt((String)linkedHashMap.get("Contractprice"));
                    searchNOAData.setContractAwardedTo((String)linkedHashMap.get("ContractAwardedTo"));
                    searchNOAData.setContractNo((String)linkedHashMap.get("ContractId"));
                    searchNOAData.setNOADate((String)linkedHashMap.get("NOADate"));
                    searchNOAData.setTotalPage((String)linkedHashMap.get("TotalPages"));
                    searchNOAData.setTotalRowCount((String)linkedHashMap.get("TotalRowCount"));
                    searchNOAData.setTenderId((Integer)linkedHashMap.get("TenderId"));
                    searchNOAData.setOfficeId((Integer)linkedHashMap.get("officeId"));
                    reportData.add(searchNOAData);
                }
            }
            LOGGER.debug("SearchNOA : No data found ");
        } catch (Exception ex) {
            LOGGER.error("SearchNOA : "+ ex);
            ex.printStackTrace();
        }

        return reportData;
    }
}
