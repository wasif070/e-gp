package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvReExHistory;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsSrvReExHistoryDao extends GenericDao<TblCmsSrvReExHistory>  {

    public void addTblCmsSrvReExHistory(TblCmsSrvReExHistory event) throws Exception;

    public void deleteTblCmsSrvReExHistory(TblCmsSrvReExHistory event) throws Exception;

    public void updateTblCmsSrvReExHistory(TblCmsSrvReExHistory event) throws Exception;

    public List<TblCmsSrvReExHistory> getAllTblCmsSrvReExHistory() throws Exception;

    public List<TblCmsSrvReExHistory> findTblCmsSrvReExHistory(Object... values) throws Exception;

    public List<TblCmsSrvReExHistory> findByCountTblCmsSrvReExHistory(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsSrvReExHistoryCount() throws Exception;

    public void updateOrSaveEstCost(List<TblCmsSrvReExHistory> estCost) throws Exception;
}
