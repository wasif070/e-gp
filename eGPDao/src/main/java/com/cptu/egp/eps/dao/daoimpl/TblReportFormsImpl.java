package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblReportFormsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblReportForms;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblReportFormsImpl extends AbcAbstractClass<TblReportForms> implements TblReportFormsDao {

    @Override
    public void addTblReportForms(TblReportForms tblReportForms){
        super.addEntity(tblReportForms);
    }

    @Override
    public void deleteTblReportForms(TblReportForms tblReportForms) {
        super.deleteEntity(tblReportForms);
    }

    @Override
    public void updateTblReportForms(TblReportForms tblReportForms) {
        super.updateEntity(tblReportForms);
    }

    @Override
    public List<TblReportForms> getAllTblReportForms() {
        return super.getAllEntity();
    }

    @Override
    public List<TblReportForms> findTblReportForms(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblReportFormsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblReportForms> findByCountTblReportForms(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveAllRepForms(List<TblReportForms> tblReportForms) {
        super.updateAll(tblReportForms);
    }
}
