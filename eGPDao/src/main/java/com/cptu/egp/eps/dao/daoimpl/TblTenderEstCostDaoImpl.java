package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblTenderEstCostDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderEstCost;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblTenderEstCostDaoImpl extends AbcAbstractClass<TblTenderEstCost> implements TblTenderEstCostDao {

    @Override
    public void addTblTenderEstCost(TblTenderEstCost tblTenderEstCost){
        super.addEntity(tblTenderEstCost);
    }

    @Override
    public void deleteTblTenderEstCost(TblTenderEstCost tblTenderEstCost) {
        super.deleteEntity(tblTenderEstCost);
    }

    @Override
    public void updateTblTenderEstCost(TblTenderEstCost tblTenderEstCost) {
        super.updateEntity(tblTenderEstCost);
    }

    @Override
    public List<TblTenderEstCost> getAllTblTenderEstCost() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderEstCost> findTblTenderEstCost(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderEstCostCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderEstCost> findByCountTblTenderEstCost(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblTenderEstCost> tblTenderEstCost) {
        super.updateAll(tblTenderEstCost);
    }
}
