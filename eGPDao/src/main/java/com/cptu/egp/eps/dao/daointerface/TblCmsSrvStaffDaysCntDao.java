package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvStaffDaysCnt;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsSrvStaffDaysCntDao extends GenericDao<TblCmsSrvStaffDaysCnt> {

    public void addTblCmsSrvStaffDaysCnt(TblCmsSrvStaffDaysCnt event);

    public void deleteTblCmsSrvStaffDaysCnt(TblCmsSrvStaffDaysCnt event);

    public void updateTblCmsSrvStaffDaysCnt(TblCmsSrvStaffDaysCnt event);

    public List<TblCmsSrvStaffDaysCnt> getAllTblCmsSrvStaffDaysCnt();

    public List<TblCmsSrvStaffDaysCnt> findTblCmsSrvStaffDaysCnt(Object... values) throws Exception;

    public List<TblCmsSrvStaffDaysCnt> findByCountTblCmsSrvStaffDaysCnt(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsSrvStaffDaysCntCount();

    public void updateOrSaveEstCost(List<TblCmsSrvStaffDaysCnt> estCost);
}
