package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvSshistoryDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvSshistory;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsSrvSshistoryImpl extends AbcAbstractClass<TblCmsSrvSshistory> implements TblCmsSrvSshistoryDao {

    @Override
    public void addTblCmsSrvSshistory(TblCmsSrvSshistory cmsWpDetail){
        super.addEntity(cmsWpDetail);
    }

    @Override
    public void deleteTblCmsSrvSshistory(TblCmsSrvSshistory cmsWpDetail) {
        super.deleteEntity(cmsWpDetail);
    }

    @Override
    public void updateTblCmsSrvSshistory(TblCmsSrvSshistory cmsWpDetail) {
        super.updateEntity(cmsWpDetail);
    }

    @Override
    public List<TblCmsSrvSshistory> getAllTblCmsSrvSshistory() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvSshistory> findTblCmsSrvSshistory(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvSshistoryCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvSshistory> findByCountTblCmsSrvSshistory(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsSrvSshistory> estCost) {
       super.updateAll(estCost);
    }
}
