/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.object.StoredProcedure;

/**
 * This class is helps for execution the procedure <b>p_get_PEOfficeDetail</b>.
 * @author Sreenu
 */
public class SPSharedPEDetailForPromis extends StoredProcedure {

    private static final Logger LOGGER =
            Logger.getLogger(SPSharedPEDetailForPromis.class);

    /***
     * Construction for <b>SPSharedPEDetailForPromis</b>
     * we inject this properties at DaoContext
     * @param dataSource
     * @param procName
     */
    public SPSharedPEDetailForPromis(BasicDataSource dataSource, String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        LOGGER.debug("Proc Name: " + procName);
    }

    /***
     * This method executes the given procedure and returns the list of objects
     * Execute Procedure: p_get_PEOfficeDetail
     * @return List<PromisIntegrationDetails>
     */
    public List<PromisIntegrationDetails> executeProcedure() {
        this.compile();
        List<PromisIntegrationDetails> promisIntegrationDetailsList =
                new ArrayList<PromisIntegrationDetails>();
        Map inParams = new HashMap();
        try {
            ArrayList<LinkedHashMap<String, Object>> list =
                    (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                PromisIntegrationDetails promisIntegrationDetails =
                        new PromisIntegrationDetails();
                promisIntegrationDetails.setPEId((Integer) linkedHashMap.get("PEID"));
                promisIntegrationDetails.setPEOfficeName((String) linkedHashMap.get("PEOfficeName"));
                promisIntegrationDetails.setDeptType((String) linkedHashMap.get("DeptType"));
                promisIntegrationDetails.setMinistryName((String) linkedHashMap.get("MinistryName"));
                promisIntegrationDetails.setDivisionName((String) linkedHashMap.get("DivisionName"));
                promisIntegrationDetails.setOrganizationName((String) linkedHashMap.get("OrganizationName"));
                promisIntegrationDetails.setDistrictName((String) linkedHashMap.get("DistrictName"));
                promisIntegrationDetailsList.add(promisIntegrationDetails);
            }
        } catch (Exception e) {
            LOGGER.error("SPSharedPEDetailForPromis : " + e);
        }
        return promisIntegrationDetailsList;
    }
}
