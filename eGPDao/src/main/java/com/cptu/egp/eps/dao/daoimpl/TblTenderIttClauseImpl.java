/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderIttClauseDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderIttClause;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderIttClauseImpl extends AbcAbstractClass<TblTenderIttClause> implements TblTenderIttClauseDao {

    @Override
    public void addTblTenderIttClause(TblTenderIttClause tenderIttClause) {
        super.addEntity(tenderIttClause);
    }

    @Override
    public void deleteTblTenderIttClause(TblTenderIttClause tenderIttClause) {
        super.deleteEntity(tenderIttClause);
    }

    @Override
    public void updateTblTenderIttClause(TblTenderIttClause tenderIttClause) {
        super.updateEntity(tenderIttClause);
    }

    @Override
    public List<TblTenderIttClause> getAllTblTenderIttClause() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderIttClause> findTblTenderIttClause(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderIttClauseCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderIttClause> findByCountTblTenderIttClause(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
