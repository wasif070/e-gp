package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblAppWatchList;
import java.util.List;

/**
 *
 * @author  Ramesh
 */
public interface TblAppWatchListDao extends GenericDao<TblAppWatchList> {

    public void addTblAppWatchList(TblAppWatchList tblObj);

    public void deleteTblAppWatchList(TblAppWatchList tblObj);

    public void updateTblAppWatchList(TblAppWatchList tblObj);

    public List<TblAppWatchList> getAllTblAppWatchList();

    public List<TblAppWatchList> findTblAppWatchList(Object... values) throws Exception;

    public List<TblAppWatchList> findByCountTblAppWatchList(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblAppWatchListCount();
}
