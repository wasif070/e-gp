package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTempCompanyJointVenture;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblTempCompanyJointVentureDao extends GenericDao<TblTempCompanyJointVenture> {

    public void addTblTempCompanyJointVenture(TblTempCompanyJointVenture venture);

    public void deleteTblTempCompanyJointVenture(TblTempCompanyJointVenture venture);

    public void updateTblTempCompanyJointVenture(TblTempCompanyJointVenture venture);

    public List<TblTempCompanyJointVenture> getAllTblTempCompanyJointVenture();

    public List<TblTempCompanyJointVenture> findTblTempCompanyJointVenture(Object... values) throws Exception;

    public List<TblTempCompanyJointVenture> findByCountTblTempCompanyJointVenture(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTempCompanyJointVentureCount();
}
