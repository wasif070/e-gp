package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblNegotiationDocs;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblNegotiationDocsDao extends GenericDao<TblNegotiationDocs> {

    public void addTblNegotiationDocs(TblNegotiationDocs tblObj);

    public void deleteTblNegotiationDocs(TblNegotiationDocs tblObj);

    public void deleteAllTblNegotiationDocs(Collection tblObj);

    public void updateTblNegotiationDocs(TblNegotiationDocs tblObj);

    public List<TblNegotiationDocs> getAllTblNegotiationDocs();

    public List<TblNegotiationDocs> findTblNegotiationDocs(Object... values) throws Exception;

    public List<TblNegotiationDocs> findByCountTblNegotiationDocs(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblNegotiationDocsCount();
}
