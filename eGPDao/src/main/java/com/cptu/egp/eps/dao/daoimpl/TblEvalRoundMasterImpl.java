package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblEvalRoundMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalRoundMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblEvalRoundMasterImpl extends AbcAbstractClass<TblEvalRoundMaster> implements TblEvalRoundMasterDao {

    @Override
    public void addTblEvalRoundMaster(TblEvalRoundMaster master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblEvalRoundMaster(TblEvalRoundMaster master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblEvalRoundMaster(TblEvalRoundMaster master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblEvalRoundMaster> getAllTblEvalRoundMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEvalRoundMaster> findTblEvalRoundMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEvalRoundMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalRoundMaster> findByCountTblEvalRoundMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
