package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblListBoxDetail;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblListBoxDetailDao extends GenericDao<TblListBoxDetail> {

    public void addTblListBoxDetail(TblListBoxDetail tblObj);

    public void deleteTblListBoxDetail(TblListBoxDetail tblObj);

    public void updateTblListBoxDetail(TblListBoxDetail tblObj);

    public List<TblListBoxDetail> getAllTblListBoxDetail();

    public List<TblListBoxDetail> findTblListBoxDetail(Object... values) throws Exception;

    public List<TblListBoxDetail> findByCountTblListBoxDetail(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblListBoxDetailCount();

    public void updateOrSaveAllListBoxDetail(List<TblListBoxDetail> list);
}
