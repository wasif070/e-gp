package com.cptu.egp.eps.dao.daoimpl;


import com.cptu.egp.eps.dao.daointerface.TblCmsSrvCcvariDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvCcvari;
import java.util.List;


public class TblCmsSrvCcvariImpl extends AbcAbstractClass<TblCmsSrvCcvari> implements TblCmsSrvCcvariDao {

	@Override 
	public void addTblCmsSrvCcvari(TblCmsSrvCcvari master){
		super.addEntity(master);
	}
	@Override 
	public void deleteTblCmsSrvCcvari(TblCmsSrvCcvari master) {
		super.deleteEntity(master);
	}
	@Override 
	public void updateTblCmsSrvCcvari(TblCmsSrvCcvari master) {
		super.updateEntity(master);
	}
	@Override 
	public List<TblCmsSrvCcvari> getAllTblCmsSrvCcvari() {
		return super.getAllEntity();
	}
	@Override 
	public List<TblCmsSrvCcvari> findTblCmsSrvCcvari(Object... values) throws Exception {
		return super.findEntity(values);
	}
	@Override 
	public long getTblCmsSrvCcvariCount() {
		return super.getEntityCount();
	}
	@Override 
	public List<TblCmsSrvCcvari> findByCountTblCmsSrvCcvari(int firstResult, int maxResult, Object... values) throws Exception {
		return super.findByCountEntity(firstResult, maxResult, values);
	}
	@Override 
	public void updateOrSaveTblCmsSrvCcvari(List<TblCmsSrvCcvari> tblObj) {
		super.updateAll(tblObj);
	}
}