/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblSubContractingDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblSubContracting;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblSubContractingImpl extends AbcAbstractClass<TblSubContracting> implements TblSubContractingDao{

    @Override
    public void addTblSubContracting(TblSubContracting tblSubContracting) {
        super.addEntity(tblSubContracting);
    }

    @Override
    public List<TblSubContracting> findTblSubContracting(Object... values) throws Exception {
        return super.findEntity(values);       
    }

    @Override
    public void deleteTblSubContracting(TblSubContracting tblSubContracting) {
        super.deleteEntity(tblSubContracting);       
    }

    @Override
    public void updateTblSubContracting(TblSubContracting tblSubContracting) {
        super.updateEntity(tblSubContracting);       
    }

    @Override
    public List<TblSubContracting> getAllTblSubContracting() {
        return super.getAllEntity();
    }

    @Override
    public long getTblSubContractingCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblSubContracting> findByCountTblSubContracting(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
