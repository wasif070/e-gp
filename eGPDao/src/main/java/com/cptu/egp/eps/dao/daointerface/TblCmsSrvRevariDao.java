package com.cptu.egp.eps.dao.daointerface;


import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvRevari;
import java.util.List;


	public interface TblCmsSrvRevariDao extends GenericDao<TblCmsSrvRevari> {

	public void addTblCmsSrvRevari(TblCmsSrvRevari tblObj);

	public void deleteTblCmsSrvRevari(TblCmsSrvRevari tblObj);

	public void updateTblCmsSrvRevari(TblCmsSrvRevari tblObj);

	public List<TblCmsSrvRevari> getAllTblCmsSrvRevari();

	public List<TblCmsSrvRevari> findTblCmsSrvRevari(Object... values) throws Exception;

	public List<TblCmsSrvRevari> findByCountTblCmsSrvRevari(int firstResult,int maxResult,Object... values) throws Exception;

	public long getTblCmsSrvRevariCount();

	public void updateOrSaveTblCmsSrvRevari(List<TblCmsSrvRevari> tblObj);
}