/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderPhasingDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderPhasing;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderPhasingImpl extends AbcAbstractClass<TblTenderPhasing> implements TblTenderPhasingDao {

    @Override
    public void addTblTenderPhasing(TblTenderPhasing tenderPhasing) {
        super.addEntity(tenderPhasing);
    }

    @Override
    public void deleteTblTenderPhasing(TblTenderPhasing tenderPhasing) {
        super.deleteEntity(tenderPhasing);
    }

    @Override
    public void updateTblTenderPhasing(TblTenderPhasing tenderPhasing) {
        super.updateEntity(tenderPhasing);
    }

    @Override
    public List<TblTenderPhasing> getAllTblTenderPhasing() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderPhasing> findTblTenderPhasing(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderPhasingCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderPhasing> findByCountTblTenderPhasing(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
