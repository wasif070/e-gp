/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTemplateFormulasDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTemplateFormulas;
import java.util.List;

/**
 *
 * @author yanki
 */
public class TblTemplateFormulasImpl extends AbcAbstractClass<TblTemplateFormulas> implements TblTemplateFormulasDao{

    @Override
    public void addTblTemplateFormulas(TblTemplateFormulas templateFormulas) {
        super.addEntity(templateFormulas);
    }

    @Override
    public void deleteTblTemplateFormulas(TblTemplateFormulas templateFormulas) {
        super.deleteEntity(templateFormulas);
    }

    @Override
    public void updateTblTemplateFormulas(TblTemplateFormulas templateFormulas) {
        super.updateEntity(templateFormulas);
    }

    @Override
    public List<TblTemplateFormulas> getAllTblTemplateFormulas() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTemplateFormulas> findTblTemplateFormulas(Object... values) throws Exception {
        return super.findEntity(values);
    }

}
