package com.cptu.egp.eps.dao.daointerface;


import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblPriTopicMaster;
import java.util.List;


	public interface TblPriTopicMasterDao extends GenericDao<TblPriTopicMaster> {

	public void addTblPriTopicMaster(TblPriTopicMaster tblObj);

	public void deleteTblPriTopicMaster(TblPriTopicMaster tblObj);

	public void updateTblPriTopicMaster(TblPriTopicMaster tblObj);

	public List<TblPriTopicMaster> getAllTblPriTopicMaster();

	public List<TblPriTopicMaster> findTblPriTopicMaster(Object... values) throws Exception;

	public List<TblPriTopicMaster> findByCountTblPriTopicMaster(int firstResult,int maxResult,Object... values) throws Exception;

	public long getTblPriTopicMasterCount();

	public void updateOrSaveTblPriTopicMaster(List<TblPriTopicMaster> tblObj);
}