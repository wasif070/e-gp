package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsPrDocumentDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsPrDocument;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsPrDocumentImpl extends AbcAbstractClass<TblCmsPrDocument> implements TblCmsPrDocumentDao {

    @Override
    public void addTblCmsPrDocument(TblCmsPrDocument cmsPrDocument){
        super.addEntity(cmsPrDocument);
    }

    @Override
    public void deleteTblCmsPrDocument(TblCmsPrDocument cmsPrDocument) {
        super.deleteEntity(cmsPrDocument);
    }

    @Override
    public void updateTblCmsPrDocument(TblCmsPrDocument cmsPrDocument) {
        super.updateEntity(cmsPrDocument);
    }

    @Override
    public List<TblCmsPrDocument> getAllTblCmsPrDocument() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsPrDocument> findTblCmsPrDocument(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsPrDocumentCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsPrDocument> findByCountTblCmsPrDocument(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
