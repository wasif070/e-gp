/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.jdbc.object.StoredProcedure;

/**
 * This class is helps for execution the procedure <b>p_search_SearchDebarredTenderers</b>.
 *
 * @author Sreenu
 */
public class SPgetDebarredTenderersInfo extends StoredProcedure {

    private static final Logger LOGGER =
            Logger.getLogger(SPgetDebarredTenderersInfo.class);

    /***
     * Construction for <b>SPgetDebarredTenderersInfo</b>
     * we inject this properties at DaoContext
     * @param dataSource
     * @param procName
     */
    public SPgetDebarredTenderersInfo(BasicDataSource dataSource, String procName) {

       this.setDataSource(dataSource);
       this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
       LOGGER.debug("Proc Name: "+ procName);
    }
    /***
     * This method executes the given procedure and returns the list of objects
     * Execute stored procedure: p_search_SearchDebarredTenderers
     * @return List<CommonDebarredTendererDetails>
     */
    public List<CommonDebarredTendererDetails> executeProcedure(){
        this.compile();
        List<CommonDebarredTendererDetails> debarredTendererDetails =
                new ArrayList<CommonDebarredTendererDetails>();
        Map inParams = new HashMap();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = 
                    (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonDebarredTendererDetails commonDebarredTendererDetails =
                        new CommonDebarredTendererDetails();
                commonDebarredTendererDetails.setDebarmentId((Integer)linkedHashMap.get("debarmentId"));
                commonDebarredTendererDetails.setCompanyName((String)linkedHashMap.get("companyName"));
                commonDebarredTendererDetails.setDebarmentDate((String)linkedHashMap.get("debarDate"));
                commonDebarredTendererDetails.setDebarmentPeriod((String)linkedHashMap.get("period"));
                commonDebarredTendererDetails.setDebarredAt((String)linkedHashMap.get("debarredAt"));
                commonDebarredTendererDetails.setDebarredBy((String)linkedHashMap.get("employeeName"));
                commonDebarredTendererDetails.setReason((String)linkedHashMap.get("comments"));
                commonDebarredTendererDetails.setOfficeId((Integer)linkedHashMap.get("officeId"));
                debarredTendererDetails.add(commonDebarredTendererDetails);
            }
        }catch (Exception e) {
            LOGGER.error("SPgetDebarredTenderersInfo : "+ e);
        }
      return debarredTendererDetails;
    }

}
