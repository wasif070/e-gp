package com.cptu.egp.eps.dao.daoimpl;


import com.cptu.egp.eps.dao.daointerface.TblTendercorriFormsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTendercorriForms;
import java.util.List;


public class TblTendercorriFormsImpl extends AbcAbstractClass<TblTendercorriForms> implements TblTendercorriFormsDao {

	@Override 
	public void addTblTendercorriForms(TblTendercorriForms master){
		super.addEntity(master);
	}
	@Override 
	public void deleteTblTendercorriForms(TblTendercorriForms master) {
		super.deleteEntity(master);
	}
	@Override 
	public void updateTblTendercorriForms(TblTendercorriForms master) {
		super.updateEntity(master);
	}
	@Override 
	public List<TblTendercorriForms> getAllTblTendercorriForms() {
		return super.getAllEntity();
	}
	@Override 
	public List<TblTendercorriForms> findTblTendercorriForms(Object... values) throws Exception {
		return super.findEntity(values);
	}
	@Override 
	public long getTblTendercorriFormsCount() {
		return super.getEntityCount();
	}
	@Override 
	public List<TblTendercorriForms> findByCountTblTendercorriForms(int firstResult, int maxResult, Object... values) throws Exception {
		return super.findByCountEntity(firstResult, maxResult, values);
	}
	@Override 
	public void updateOrSaveTblTendercorriForms(List<TblTendercorriForms> tblObj) {
		super.updateAll(tblObj);
	}
}