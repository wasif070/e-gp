/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.math.BigDecimal;

/**
 *
 * @author parag
 */
public class SPProjectDetailReturn {

  private String progName;
  private int progId;
  private int projectId;
  private String projectName;
  private String projectCode;
  private BigDecimal  projectCost;
  private String projectStartDate;
  private String projectEndDate;
  private String sourceOfFund;
  private int sBankDevelopId;
  private String sbDevelopName;

    public int getProgId() {
        return progId;
    }

    public void setProgId(int progId) {
        this.progId = progId;
    }

    public String getProgName() {
        return progName;
    }

    public void setProgName(String progName) {
        this.progName = progName;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public BigDecimal getProjectCost() {
        return projectCost;
    }

    public void setProjectCost(BigDecimal projectCost) {
        this.projectCost = projectCost;
    }

    public String getProjectStartDate() {
        return projectStartDate;
    }

    public void setProjectStartDate(String projectStartDate) {
        this.projectStartDate = projectStartDate;
    }


    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectEndDate() {
        return projectEndDate;
    }

    public void setProjectEndDate(String projectEndDate) {
        this.projectEndDate = projectEndDate;
    }

    public int getsBankDevelopId() {
        return sBankDevelopId;
    }

    public void setsBankDevelopId(int sBankDevelopId) {
        this.sBankDevelopId = sBankDevelopId;
    }

    


    public String getSbDevelopName() {
        return sbDevelopName;
    }

    public void setSbDevelopName(String sbDevelopName) {
        this.sbDevelopName = sbDevelopName;
    }

    public String getSourceOfFund() {
        return sourceOfFund;
    }

    public void setSourceOfFund(String sourceOfFund) {
        this.sourceOfFund = sourceOfFund;
    }



}
