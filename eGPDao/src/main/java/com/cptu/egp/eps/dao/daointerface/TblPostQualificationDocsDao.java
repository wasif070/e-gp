package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblPostQualificationDocs;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblPostQualificationDocsDao extends GenericDao<TblPostQualificationDocs> {

    public void addTblPostQualificationDocs(TblPostQualificationDocs tblPostQualificationDocsDao);

    public void deleteTblPostQualificationDocs(TblPostQualificationDocs tblPostQualificationDocsDao);

    public void deleteAllTblPostQualificationDocs(Collection tblPostQualificationDocsDao);

    public void updateTblPostQualificationDocs(TblPostQualificationDocs tblPostQualificationDocsDao);

    public List<TblPostQualificationDocs> getAllTblPostQualificationDocs();

    public List<TblPostQualificationDocs> findTblPostQualificationDocs(Object... values) throws Exception;

    public List<TblPostQualificationDocs> findByCountTblPostQualificationDocs(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblPostQualificationDocsCount();
}
