/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.model.table.TblCmsVendorReqWcc;
import java.util.List;

/**
 *
 * @author rikin.p
 */
public interface TblCmsVendorReqWccDao {
    
    public void addTblCmsVendorReqWcc(TblCmsVendorReqWcc cmsVendorReqWcc);

    public void deleteTblCmsVendorReqWcc(TblCmsVendorReqWcc cmsVendorReqWcc);

    public void updateTblCmsVendorReqWcc(TblCmsVendorReqWcc cmsVendorReqWcc);

    public List<TblCmsVendorReqWcc> getAllTblCmsVendorReqWcc();

    public List<TblCmsVendorReqWcc> findTblCmsVendorReqWcc(Object... values) throws Exception;

    public List<TblCmsVendorReqWcc> findByCountTblCmsVendorReqWcc(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsVendorReqWccCount();
}
