package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblNoaIssueDetails;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblNoaIssueDetailsDao extends GenericDao<TblNoaIssueDetails> {

    public void addTblNoaIssueDetails(TblNoaIssueDetails tblNoaIssueDetails);

    public void deleteTblNoaIssueDetails(TblNoaIssueDetails tblNoaIssueDetails);

    public void deleteAllTblNoaIssueDetails(Collection tblNoaIssueDetails);

    public void updateTblNoaIssueDetails(TblNoaIssueDetails tblNoaIssueDetails);

    public List<TblNoaIssueDetails> getAllTblNoaIssueDetails();

    public List<TblNoaIssueDetails> findTblNoaIssueDetails(Object... values) throws Exception;

    public List<TblNoaIssueDetails> findByCountTblNoaIssueDetails(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblNoaIssueDetailsCount();
}
