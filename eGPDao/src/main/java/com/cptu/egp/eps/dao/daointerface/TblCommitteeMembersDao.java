package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCommitteeMembers;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblCommitteeMembersDao extends GenericDao<TblCommitteeMembers> {

    public void addTblCommitteeMembers(TblCommitteeMembers tblObj);

    public void deleteTblCommitteeMembers(TblCommitteeMembers tblObj);

    public void updateTblCommitteeMembers(TblCommitteeMembers tblObj);

    public List<TblCommitteeMembers> getAllTblCommitteeMembers();

    public List<TblCommitteeMembers> findTblCommitteeMembers(Object... values) throws Exception;

    public List<TblCommitteeMembers> findByCountTblCommitteeMembers(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCommitteeMembersCount();
}
