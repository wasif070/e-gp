/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblDepartmentMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import java.util.List;

/**
 *
 * @author rishita
 */
public class TblDepartmentMasterImpl extends AbcAbstractClass<TblDepartmentMaster> implements TblDepartmentMasterDao{

    @Override
    public void addDepartmentMaster(TblDepartmentMaster tbldepartmentMaster) {
        super.addEntity(tbldepartmentMaster);
    }

    @Override
    public List<TblDepartmentMaster> findTblDepartmentMaster(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblDepartmentMaster(TblDepartmentMaster tbldepartmentMaster) {

        super.deleteEntity(tbldepartmentMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblDepartmentMaster(TblDepartmentMaster tbldepartmentMaster) {

        super.updateEntity(tbldepartmentMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblDepartmentMaster> getAllTblDepartmentMaster() {
        return super.getAllEntity();
    }

    @Override
    public long getTblDepartmentMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblDepartmentMaster> findByCountTblDepartmentMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
