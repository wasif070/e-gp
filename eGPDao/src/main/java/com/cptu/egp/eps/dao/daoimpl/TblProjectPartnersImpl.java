package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblProjectPartnersDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblProjectPartners;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblProjectPartnersImpl extends AbcAbstractClass<TblProjectPartners> implements TblProjectPartnersDao {

    @Override
    public void addTblProjectPartners(TblProjectPartners projectPartners){
        super.addEntity(projectPartners);
    }

    @Override
    public void deleteTblProjectPartners(TblProjectPartners projectPartners) {
        super.deleteEntity(projectPartners);
    }

    @Override
    public void updateTblProjectPartners(TblProjectPartners projectPartners) {
        super.updateEntity(projectPartners);
    }

    @Override
    public List<TblProjectPartners> getAllTblProjectPartners() {
        return super.getAllEntity();
    }

    @Override
    public List<TblProjectPartners> findTblProjectPartners(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblProjectPartnersCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblProjectPartners> findByCountTblProjectPartners(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
