/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderFormulaDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderFormula;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderFormulaImpl extends AbcAbstractClass<TblTenderFormula> implements TblTenderFormulaDao {

    @Override
    public void addTblTenderFormula(TblTenderFormula tenderFormula) {
        super.addEntity(tenderFormula);
    }

    @Override
    public void deleteTblTenderFormula(TblTenderFormula tenderFormula) {
        super.deleteEntity(tenderFormula);
    }

    @Override
    public void updateTblTenderFormula(TblTenderFormula tenderFormula) {
        super.updateEntity(tenderFormula);
    }

    @Override
    public List<TblTenderFormula> getAllTblTenderFormula() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderFormula> findTblTenderFormula(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderFormulaCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderFormula> findByCountTblTenderFormula(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
