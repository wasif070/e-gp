/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderEnvelopes;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderEnvelopesDao extends GenericDao<TblTenderEnvelopes>{

    public void addTblTenderEnvelopes(TblTenderEnvelopes tblObj);

    public void deleteTblTenderEnvelopes(TblTenderEnvelopes tblObj);

    public void updateTblTenderEnvelopes(TblTenderEnvelopes tblObj);

    public List<TblTenderEnvelopes> getAllTblTenderEnvelopes();

    public List<TblTenderEnvelopes> findTblTenderEnvelopes(Object... values) throws Exception;

    public List<TblTenderEnvelopes> findByCountTblTenderEnvelopes(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderEnvelopesCount();
}
