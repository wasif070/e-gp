/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblCmsTaxConfigDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsTaxConfig;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public class TblCmsTaxConfigDaoImpl extends AbcAbstractClass<TblCmsTaxConfig> implements TblCmsTaxConfigDao {

    @Override
    public void addTblCmsTaxConfig(TblCmsTaxConfig tblCmsTaxConfig) {
        super.addEntity(tblCmsTaxConfig);
    }

    @Override
    public void deleteTblCmsTaxConfig(TblCmsTaxConfig tblCmsTaxConfig) {
        super.deleteEntity(tblCmsTaxConfig);
    }

    @Override
    public void updateTblCmsTaxConfig(TblCmsTaxConfig tblCmsTaxConfig) {
        super.updateEntity(tblCmsTaxConfig);
    }

    @Override
    public List<TblCmsTaxConfig> getAllTblCmsTaxConfig() {
        super.setPersistentClass(TblCmsTaxConfig.class);
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsTaxConfig> findTblCmsTaxConfig(Object... values) throws Exception {
        super.setPersistentClass(TblCmsTaxConfig.class);
        return super.findEntity(values);
    }

    @Override
    public List<TblCmsTaxConfig> findByCountTblCmsTaxConfig(int firstResult, int maxResult, Object... values) throws Exception {
        super.setPersistentClass(TblCmsTaxConfig.class);
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblCmsTaxConfigCount() {
        super.setPersistentClass(TblCmsTaxConfig.class);
        return super.getEntityCount();
    }
}
