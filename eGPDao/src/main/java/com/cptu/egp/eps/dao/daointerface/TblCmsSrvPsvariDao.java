package com.cptu.egp.eps.dao.daointerface;


import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvPsvari;
import java.util.List;


	public interface TblCmsSrvPsvariDao extends GenericDao<TblCmsSrvPsvari> {

	public void addTblCmsSrvPsvari(TblCmsSrvPsvari tblObj);

	public void deleteTblCmsSrvPsvari(TblCmsSrvPsvari tblObj);

	public void updateTblCmsSrvPsvari(TblCmsSrvPsvari tblObj);

	public List<TblCmsSrvPsvari> getAllTblCmsSrvPsvari();

	public List<TblCmsSrvPsvari> findTblCmsSrvPsvari(Object... values) throws Exception;

	public List<TblCmsSrvPsvari> findByCountTblCmsSrvPsvari(int firstResult,int maxResult,Object... values) throws Exception;

	public long getTblCmsSrvPsvariCount();

	public void updateOrSaveTblCmsSrvPsvari(List<TblCmsSrvPsvari> tblObj);
}