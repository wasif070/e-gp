package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblTempTendererEsignatureDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTempTendererEsignature;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblTempTendererEsignatureImpl extends AbcAbstractClass<TblTempTendererEsignature> implements TblTempTendererEsignatureDao {

    @Override
    public void addTblTempTendererEsignature(TblTempTendererEsignature master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblTempTendererEsignature(TblTempTendererEsignature master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblTempTendererEsignature(TblTempTendererEsignature master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblTempTendererEsignature> getAllTblTempTendererEsignature() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTempTendererEsignature> findTblTempTendererEsignature(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTempTendererEsignatureCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTempTendererEsignature> findByCountTblTempTendererEsignature(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
