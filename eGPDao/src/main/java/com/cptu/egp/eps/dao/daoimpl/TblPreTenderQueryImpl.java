/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblPreTenderQueryDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblPreTenderQuery;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblPreTenderQueryImpl extends AbcAbstractClass<TblPreTenderQuery> implements TblPreTenderQueryDao {

    @Override
    public void addTblPreTenderQuery(TblPreTenderQuery admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblPreTenderQuery(TblPreTenderQuery admin) {
        super.deleteEntity(admin);    
    }

    @Override
    public void updateTblPreTenderQuery(TblPreTenderQuery admin) {
        super.updateEntity(admin);
    }

    @Override
    public List<TblPreTenderQuery> getAllTblPreTenderQuery() {
        return super.getAllEntity();
    }

    @Override
    public List<TblPreTenderQuery> findTblPreTenderQuery(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblPreTenderQueryCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblPreTenderQuery> findByCountTblPreTenderQuery(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
