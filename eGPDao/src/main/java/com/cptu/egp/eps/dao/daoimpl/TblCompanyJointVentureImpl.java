package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCompanyJointVentureDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCompanyJointVenture;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCompanyJointVentureImpl extends AbcAbstractClass<TblCompanyJointVenture> implements TblCompanyJointVentureDao {

    @Override
    public void addTblCompanyJointVenture(TblCompanyJointVenture companyJointVenture){
        super.addEntity(companyJointVenture);
    }

    @Override
    public void deleteTblCompanyJointVenture(TblCompanyJointVenture companyJointVenture) {
        super.deleteEntity(companyJointVenture);
    }

    @Override
    public void updateTblCompanyJointVenture(TblCompanyJointVenture companyJointVenture) {
        super.updateEntity(companyJointVenture);
    }

    @Override
    public List<TblCompanyJointVenture> getAllTblCompanyJointVenture() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCompanyJointVenture> findTblCompanyJointVenture(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCompanyJointVentureCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCompanyJointVenture> findByCountTblCompanyJointVenture(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
