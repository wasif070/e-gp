package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblAskProcurementDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblAskProcurement;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblAskProcurementImpl extends AbcAbstractClass<TblAskProcurement> implements TblAskProcurementDao {

    @Override
    public void addTblAskProcurement(TblAskProcurement askProcurement){
        super.addEntity(askProcurement);
    }

    @Override
    public void deleteTblAskProcurement(TblAskProcurement askProcurement) {
        super.deleteEntity(askProcurement);
    }

    @Override
    public void updateTblAskProcurement(TblAskProcurement askProcurement) {
        super.updateEntity(askProcurement);
    }

    @Override
    public List<TblAskProcurement> getAllTblAskProcurement() {
        return super.getAllEntity();
    }

    @Override
    public List<TblAskProcurement> findTblAskProcurement(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblAskProcurementCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblAskProcurement> findByCountTblAskProcurement(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
