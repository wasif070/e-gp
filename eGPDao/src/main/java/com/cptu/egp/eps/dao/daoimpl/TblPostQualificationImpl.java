/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblPostQualificationDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblPostQualification;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblPostQualificationImpl extends AbcAbstractClass<TblPostQualification> implements TblPostQualificationDao{

    @Override
    public void addTblPostQualification(TblPostQualification postQualification) {
        super.addEntity(postQualification);
    }

    @Override
    public List<TblPostQualification> findTblPostQualification(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblPostQualification(TblPostQualification postQualification) {

        super.deleteEntity(postQualification);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblPostQualification(Collection postQualification) {
        super.deleteAll(postQualification);
    }

    @Override
    public void updateTblPostQualification(TblPostQualification postQualification) {

        super.updateEntity(postQualification);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblPostQualification> getAllTblPostQualification() {
        return super.getAllEntity();
    }

    @Override
    public long getTblPostQualificationCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblPostQualification> findByCountTblPostQualification(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
