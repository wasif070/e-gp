package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblComplaintMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.*;
import com.cptu.egp.eps.dao.storedprocedure.SearchComplaintPaymentBean;
import com.cptu.egp.eps.dao.generic.ComplaintMgmtConstants;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import java.util.*;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.*;

/**
 * This is an Dao class for ComplaintMaster table.
 *
 * @version 9/6/2011
 * @author teja.ravali
 * @project e-GP-ComplaintManagement_Module
 * @section 01
 */
public class TblComplaintMasterDaoImpl extends AbcAbstractClass<TblComplaintMaster> implements TblComplaintMasterDao {

    final static Logger log = Logger.getLogger("TblComplaintMasterDaoImpl.class");

    /**
     * This is the method to add a complaint in the datebase
     * table ComplaintMaster by calling addEntity method of
     * super class i.e AbcAbstract class which uses
     * hibernate template
     * return type is entity
     */
    @Override
    public void addComplaint(TblComplaintMaster complaint) {
        super.addEntity(complaint);
    }

    /**
     * This is the method to get all the complaint details from the
     * database table ComplaintMaster by passing the
     * complaintId which is of Integer type.
     * return type is entity
     */
    public TblComplaintMaster getComplaintDetails(int complaintId) {
        return (TblComplaintMaster) super.getEntity(complaintId);
    }

    /**
     * This is the method to update a complaint in the datebase
     * table ComplaintMaster by calling updateEntity method of
     * super class i.e AbcAbstract class which uses
     * hibernate template
     * return type is entity
     */
    public void updateMaster(TblComplaintMaster master) {
        super.updateEntity(master);
    }

    /**
     * This is the method to get PE governmentUserId of
     * a particular tender from the table employeeTrasfer
     * table by passing the tenderId parameter and by calling
     * createQuery method of super class i.e AbcAbstract
     * class
     * return type is listObject
     */
    public List<Object[]> getGovtUserByTenderId(int tenderId) {
        log.debug("getGovtUserByTenderId>>>>>>>starts>");
        StringBuilder query = new StringBuilder();
        query.append("select te.govUserId from TblEmployeeMaster em,TblEmployeeTrasfer te, TblTenderMaster tm, TblAppMaster apm, TblTenderDetails td");
        query.append(" where em.tblLoginMaster.userId=te.tblLoginMaster.userId and tm.tenderId = td.tblTenderMaster.tenderId and te.employeeId = apm.tblEmployeeMaster.employeeId and te.isCurrent='yes' and tm.appId= apm.appId and te.employeeId = apm.tblEmployeeMaster.employeeId and tm.tenderId=" + tenderId);
        String queryStr = query.toString();
        return super.createQuery(queryStr);
    }

    /**
     * This is the method to search all the complaints
     * which are in pending state from the table
     * complaintMaster by passing reviewPanelId and by calling
     * findEntity method of super class i.e AbcAbstract
     * class
     * returns List<TblComplaintMaster>
     */
    @Override
    public List<TblComplaintMaster> getPendingComplaintsByReviewPanel(Object... values) throws Exception {
        return super.findEntity(values);
    }

    /**
     * This is the method to get the HOPE governmentUserId
     * from the employeeTrasfer table by passing officeId and
     * role as the two parameters
     * Native query is used to retrieve the governmentUserId
     * return type is listObject
     */
    @Override
    public List<Object> getGovtUserDetails(int officeId, String role) {
        String query = "select et.govUserId ";
        query = query + "from tbl_loginmaster lm, tbl_EmployeeTrasfer et,";
        query = query + "(selecT * from tbl_EmployeeRoles where employeeId in ";
        query = query + "(selecT employeeId from tbl_EmployeeMaster where employeeId in ";
        query = query + "(selecT employeeId from tbl_EmployeeOffices where officeId =" + officeId + " ))) a,";
        query = query + "tbl_EmployeeMaster em, tbl_ProcurementRole pr ";
        query = query + "where a.employeeId = em.employeeId and em.userId = lm.userId  and ";
        query = query + "pr.procurementRoleId = a.procurementRoleId and em.employeeId=et.employeeId and pr.procurementRole = '" + role + "'";
        return super.nativeSQLQuery(query);
    }

    /**
     * This is the method to search all the complaints for HOPE
     * by passing the parameters tenderId,complaintId,details,govUserId,
     * role,status,complaintFeePaid from complaintMaster table
     * returns List<TblComplaintMaster>
     */
    @Override
    public List<TblComplaintMaster> searchComplaintDetails(int tenderId, int complaintId, TblTenderDetails details, int govUserId, String role, String status, boolean complaintFeePaid, int panelId) throws Exception {
        StringBuilder query = new StringBuilder();
        log.error("Inside search complaint details");
        List<TblComplaintMaster> lstComplaintMaster = new ArrayList<TblComplaintMaster>();
        int rolId = 0;
        boolean flag = true;
        if ("PE".equalsIgnoreCase(role)) {
            rolId = 1;
        } else if ("HOPE".equalsIgnoreCase(role)) {
            rolId = 2;
        } else if ("Secretary".equalsIgnoreCase(role)) {
            rolId = 3;
        } else {
            rolId = 4;
        }

        query.append(" from  TblComplaintMaster master ");
        if (tenderId != 0 || complaintId != 0 || details != null || panelId!=-1) {
            query.append("where ");
            if (tenderId != 0 && complaintId != 0 && details != null) {
                query.append("master.tenderMaster.tenderId=").append(tenderId).append(" and master.complaintId=").append(complaintId).append(" and ").append(details.getTblTenderMaster().getTenderId()).append(" = master.tenderMaster.tenderId");
            } else {
                if (tenderId != 0 && complaintId != 0) {
                    query.append("  master.tenderMaster.tenderId=").append(tenderId).append(" and master.complaintId=").append(complaintId);
                } else if (tenderId != 0 && details != null) {
                    query.append(" master.tenderMaster.tenderId=").append(tenderId).append(" and ").append(details.getTblTenderMaster().getTenderId()).append(" = master.tenderId");
                } else if (complaintId != 0 && details != null) {
                    query.append(" master.complaintId=").append(complaintId).append(" and ").append(details.getTblTenderMaster().getTenderId()).append(" = master.tenderId");
                } else if (tenderId != 0) {
                    query.append(" master.tenderMaster.tenderId=").append(tenderId);
                } else if (complaintId != 0) {
                    query.append(" master.complaintId=").append(complaintId);
                } else if (details != null) {
                    query.append(details.getTblTenderMaster().getTenderId()).append(" = master.tenderMaster.tenderId");
                
                } else if (panelId != 0) {
                     flag = false;
                     query.append(" master.panelId!=0");
                }else{
                     flag = false;
                     query.append(" master.panelId=0");
                }
            }
            query.append(" and master.complaintLevel.complaintLevelId=").append(rolId);
            if (role.equals(ComplaintMgmtConstants.REVIEW_PANEL_ROLE)) {
                if (complaintFeePaid) {
                    query.append(" and master.paymentStatus='paid' ");
                }
//                if (panelId != 0) {
//                    if(flag)
//                    query.append(" and master.panelId=" + panelId);
//                }
            }
            query.append(" order by master.lastModificationDt desc");
        } else {
            log.error("in search query****");
            query.append(" where master.complaintLevel.complaintLevel='").append(role).append("'  and master.complaintStatus in ('Pending','Clarification') and master.employeeTrasfer.govUserId=").append(govUserId);
            if (role.equals("ComplaintMgmtConstants.REVIEW_PANEL_ROLE")) {
                if (complaintFeePaid) {
                    query.append(" and master.paymentStatus='paid' ");
                }
                if (panelId != 0) {
                    query.append(" and master.panelId!=" + panelId);
                }else{
                    query.append(" and master.panelId=" + panelId);
                }
            }
            query.append(" order by master.lastModificationDt desc");

        }
        log.error("Inside search complaint details: QUERY STRING: " + query.toString());
        List<Object> lstObjects = super.createQueryForObject(query.toString());
        TblComplaintMaster complaintMaster = new TblComplaintMaster();
        log.error("Inside search complaint details: lstObjects.size(): " + lstObjects.size());
        if (lstObjects.size() != 0) {
            for (int i = 0; i < lstObjects.size(); i++) {
                lstComplaintMaster.add((TblComplaintMaster) lstObjects.get(i));
            }
        }
        log.error("Inside search complaint details: lstComplaintMaster.size(): " + lstComplaintMaster.size());
        return lstComplaintMaster;
    }

    @Override
    public List<TblComplaintMaster> searchPendingComplaintDetails(String qq) throws Exception {
        StringBuilder query = new StringBuilder();
        log.error("Inside search complaint details");
        List<TblComplaintMaster> lstComplaintMaster = new ArrayList<TblComplaintMaster>();
        query.append("from TblComplaintMaster tt where tt.complaintLevel.complaintLevelId in (").append(qq).append(") and tt.complaintStatus='pending'");
        List<Object> lstObjects = super.createQueryForObject(query.toString());
        log.error("Inside search complaint details: lstObjects.size(): " + lstObjects.size());
        if (lstObjects != null && !lstObjects.isEmpty()) {
            for (int i = 0; i < lstObjects.size(); i++) {
                lstComplaintMaster.add((TblComplaintMaster) lstObjects.get(i));
            }
        }
        log.error("Inside search complaint details: lstComplaintMaster.size(): " + lstComplaintMaster.size());
        return lstComplaintMaster;
    }

    /**
     *  This is the method to retrieve the government secretaries
     *  from employeeTrasfer table
     *  by passing array of officeId's and role as parameters
     *   return type is listObject
     */
    public List<Object> getGovUserSecretaries(int[] officeIds, String role) throws Exception {
        String ids = " ";
        log.error("in dao impl>>>>>>>>>>>>>");
        for (int i = 0; i < officeIds.length; i++) {
            ids = ids + officeIds[i] + ",";
        }
        String officeHirarchyIds = ids.substring(0, ids.length() - 1);
        String query = "select et.govUserId from tbl_loginmaster lm, tbl_EmployeeTrasfer et,";
        query = query + "(selecT * from tbl_EmployeeRoles where employeeId in";
        query = query + "(selecT employeeId from tbl_EmployeeMaster where employeeId in";
        query = query + "(selecT employeeId from tbl_EmployeeOffices where officeId in";
        query = query + "(" + officeHirarchyIds + ")))) a, tbl_EmployeeMaster em, tbl_ProcurementRole pr";
        query = query + " where a.employeeId = em.employeeId and em.userId = lm.userId  and et.employeeId=em.employeeId and pr.procurementRoleId = a.procurementRoleId";
        query = query + " and pr.procurementRole='" + role + "'";
        return super.nativeSQLQuery(query);
    }

    /**
     * This method is used to retrieve the governmentUserId of the
     * user who has logged in the session
     * from employeeTrasfer table by passing the userId(retrieved from session)
     * return type is array listObject
     */
    public List<Object[]> getGovUserDetails(int userId) throws Exception {
        String query = "select et.govUserId from TblLoginMaster lm, TblEmployeeTrasfer et ";
        query = query + "where lm.userId=et.tblLoginMaster.userId  and lm.userId=" + userId;
        return super.createQuery(query);
    }

    /**
     * not used
     */
    public List<Object[]> getComplaintList(int tenderid, int complaintlevelId) throws Exception {
        StringBuilder query = new StringBuilder();
        query.append("FROM TblComplaintMaster WHERE tenderMaster.tenderId=" + tenderid + " and complaintLevel.complaintLevelId=" + complaintlevelId);
        String queryStr = query.toString();
        return super.createQuery(queryStr);
    }

    /**
     * not used
     */
    public List<Object[]> getSearchList(int complid, int tenderid, int refid) throws Exception {
        StringBuilder query = new StringBuilder();
        query.append("FROM TblComplaintMaster WHERE tenderId=" + tenderid + " or complaintLevelId=2 or complaintId=" + complid);
        String queryStr = query.toString();
        return super.createQuery(queryStr);
    }

    /**
     * This method is used to retrieve list of complaints
     * for DGCPTU from the table complaintMaster
     * returns List<TblComplaintMaster>
     */
    @Override
    public List<TblComplaintMaster> getAllComplaints() {
        log.error("table complaints dao implements class for getAllComplaints");
        return super.getAllEntity();
    }

    /**
     * not used
     */
    @Override
    public List<TblComplaintMaster> findComplaints(Object... values) throws Exception {
        return super.findEntity(values);
    }

    /**
     *
     */
    public List<Object[]> getTenderDetails(int complaintId) throws Exception {
        StringBuilder query = new StringBuilder();
        /*select b.reoiRfpRefNo, c.companyName
        from tbl_CMS_ComplaintMaster a, tbl_TenderDetails b, tbl_CompanyMaster c,
        tbl_TendererMaster d where a.complaintId = 85 and a.tenderId = b.tenderId
        and c.userId = d.userId and a.tendererId = d.tendererId
         */
        query.append("select details.reoiRfpRefNo,companyMaster.companyName from TblComplaintMaster master, TblTenderDetails details, TblCompanyMaster companyMaster, TblTenderMaster tendermaster,TblLoginMaster loginMaster");
        query.append(" where master.complaintId=" + complaintId + " and master.tenderMaster.tenderId = tendermaster.tenderId and companyMaster.tblLoginMaster.userId = loginMaster.userId ");
        String queryStr = query.toString();
        return super.createQuery(queryStr);
    }

    /**
     * This method is used to search for the complaints when branchMaker logsIn
     * by passing the params
     * String emailId, String verificationStatus, String paymentFromDate, String paymentToDate, int createdBy, String paymentFor
     * return List<TblComplaintPayments>
     */
    @Override
    public List<SearchComplaintPaymentBean> searchComplaintPayments(String emailId, String verificationStatus, String paymentFromDate, String paymentToDate, int createdBy, String paymentFor, boolean verifyFlag, int tenderId, String tenderer) throws Exception {
        String query = "";
        String join = "LEFT";

        List<SearchComplaintPaymentBean> lstSearchPayments = new ArrayList<SearchComplaintPaymentBean>();
        query = query + "select cm.complaintId , cm.complaintSubject, cm.tenderId, lm.emailId,  isNULL(cp.status,'pending') as status, isNULL(cp.isLive,'Yes') as isLive,isNULL(cp.isVerified,'No') as isVerified,";
        query = query + " (CONVERT(VARCHAR(20), cp.createdDate, 103) +' '+ CONVERT(VARCHAR(20), cp.createdDate, 24)) as dtOfAction, (CONVERT(VARCHAR(20), cm.complaintCreationDt, 103) +' '+ CONVERT(VARCHAR(20), cm.complaintCreationDt, 24)) as createdDate, isNULL(cp.complaintPaymentId,0) as complaintPaymentId, isNULL(cp.createdBy,0) as createdBy, cp.paymentFor, cm.paymentStatus from tbl_CMS_ComplaintMaster cm";
        query = query + " JOIN tbl_TendererMaster tm ON cm.tendererId=tm.tendererId";
        query = query + " JOIN tbl_TenderMaster tenderMaster ON tenderMaster.tenderId=cm.tenderId and cm.complaintLevelId=4";
        query = query + " JOIN tbl_LoginMaster lm ON  tm.userId=lm.userId ";

        if (emailId != null || verificationStatus != null || paymentFromDate != null || paymentToDate != null || tenderId != 0 || tenderer != null) {


            if (emailId != null && verificationStatus != null && paymentFromDate != null && paymentToDate != null && tenderId != 0 && tenderer != null) {

                log.error("in if 1 alllllllllllll");
                query = query + " and tenderMaster.tenderId=" + tenderId;
                query = query + " JOIN tbl_CompanyMaster company ON tm.companyId=company.companyId and company.companyName like '" + tenderer + "%'";
                query = query + " and lm.emailId like '" + emailId + "%'";
                query = query + " JOIN tbl_CMS_ComplaintPayment cp ON ";
                query = query + "  cp.isVerified='" + verificationStatus + "' and cp.dtOfAction > '" + paymentFromDate + "' and cp.dtOfAction < '" + paymentToDate + "' and";
            } else {
                if (emailId != null) {
                    query = query + " and lm.emailId like '" + emailId + "%'";
                }
                if (verifyFlag) {
                    query = query + " JOIN tbl_CMS_ComplaintPayment cp ON  cm.complaintId=cp.complaintId and";
                } else {
                    query = query + " LEFT JOIN tbl_CMS_ComplaintPayment cp ON  cm.complaintId=cp.complaintId and";
                }

                if (verificationStatus != null) {
                    if (verificationStatus.equalsIgnoreCase("Yes")) {
                        if (query.contains(join)) {
                            query = query.replace(join, "");
                        }
                    }
                    query = query + " cp.isVerified='" + verificationStatus + "' and";
                }

                if (paymentFromDate != null) {
                    if (query.contains(join)) {
                        query = query.replace(join, "");
                    }
                    query = query + " cp.dtOfAction > '" + paymentFromDate + "' and";
                }

                if (paymentToDate != null) {
                    if (query.contains(join)) {
                        query = query.replace(join, "");
                    }
                    query = query + " cp.dtOfAction < '" + paymentToDate + "' and";
                }

                if (tenderId != 0) {
                    if (query.contains(join)) {
                        query = query.replace(join, "");
                    }

                    query = query + " tenderMaster.tenderId=" + tenderId + " and";
                }

                if (tenderer != null) {

                    if (query.contains(join)) {
                        query = query.replace(join, "");
                    }

                    int queryLength = query.length();
                    String end = "and";
                    if (query.endsWith(end)) {
                        query = query.substring(0, queryLength - end.length());
                        query = query + " JOIN tbl_CompanyMaster company ON";
                        query = query + " tm.companyId=company.companyId and company.companyName like '" + tenderer + "%' and";
                    } else {
                        query = query + " JOIN tbl_CompanyMaster company ON";
                        query = query + " tm.companyId=company.companyId and company.companyName like '" + tenderer + "%' and";
                    }
                }

                query = query + " cp.paymentFor='" + paymentFor + "'";
            }
        } else {
            query = query + " JOIN tbl_CMS_ComplaintPayment cp ON ";
            query = query + " cm.complaintId=cp.complaintId and cp.paymentFor='" + paymentFor + "'";

        }
        if (verifyFlag && createdBy != 0) {
            query = query + " and cp.createdBy in (" + createdBy + ")";
            query = query + " and cp.status='paid'";
        }
        log.error("Inside search complaint details: QUERY STRING: " + query.toString());
        List<SearchComplaintPaymentBean> lstObjects = this.getPaymentSearchResult(query);
//TblComplaintMaster complaintMaster = new TblComplaintMaster(); 
        log.error("lstObjects.size()>>>>>>>>>>>>>" + lstObjects.size());
        if (lstObjects != null && lstObjects.size() != 0) {
            log.error("Inside search complaint details: lstObjects.size().size(): " + lstObjects.size());
            return lstObjects;
        }
        return null;
    }

    /**
     * Used to transform the query results to SearchComplaintPaymentBean  object
     */
    public List<SearchComplaintPaymentBean> getPaymentSearchResult(String query) {
        Query q = null;
        List<SearchComplaintPaymentBean> lstObj = new ArrayList<SearchComplaintPaymentBean>();
        try {
            q = getSession().createSQLQuery(query).setResultTransformer(Transformers.aliasToBean(SearchComplaintPaymentBean.class));
            log.debug("Query :: " + q.getQueryString());
        } catch (Exception e) {
            log.error("native SQL query :: " + e);
        }
        lstObj = q.list();
        return lstObj;
    }

    /**
     * This method is used to retrieve the complaints from
     * complaintMaster table by passing either the tenderId
     * or complaintId
     * returns List<TblComplaintMaster>
     */
    public List<TblComplaintMaster> getComplaintById(Object... values) throws Exception {
        return super.findEntity(values);
    }

    public List<Object[]> getComplaintTenderer(int tenderId, int userId) throws Exception {
        StringBuilder query = new StringBuilder();
        query.append("FROM TblComplaintMaster WHERE tenderMaster.tenderId=" + tenderId + " and tendererMaster.tblLoginMaster.userId= " + userId + " order by lastModificationDt desc");
        String queryStr = query.toString();
        return super.createQuery(queryStr);
    }

    public List<Object> getEmailId(int govUserId) throws Exception {
        String query = "select lm.emailId from tbl_LoginMaster lm, tbl_EmployeeTrasfer et where lm.userId in (select et1.userId from tbl_EmployeeTrasfer et1 where et1.govUserId=" + govUserId + ") and lm.userId=et.userId";
        return super.nativeSQLQuery(query);

    }

    public List<TblPartnerAdmin> getTblPartnerAdmin(int createdBy, boolean getAllFlag) throws Exception {

        SQLQuery q = null;
        String query = null;
        try {
            if (getAllFlag) {
                query = "select * from tbl_PartnerAdmin as partner where partner.sBankDevelopId in";
                query = query + "(select sBankDevelopId from tbl_PartnerAdminTransfer where isCurrent='Yes'and userId =" + createdBy + ") and isMakerChecker='BranchMaker'";
            } else {
                query = "select * from tbl_PartnerAdmin as partner where partner.sBankDevelopId in (select sBankDevelopId from tbl_PartnerAdminTransfer where isCurrent='Yes') and userId =" + createdBy;
            }
            log.error("In side native SQL query :getTblPartnerAdmin:111111111111111111111111111111111 " + query);
            q = getSession().createSQLQuery(query).addEntity("partner", TblPartnerAdmin.class);

        } catch (Exception e) {
            log.error("native SQL query :: " + e);
        }
        List<TblPartnerAdmin> lstPartnerAdmin = q.list();
        log.error("native SQL query :getTblPartnerAdmin: 222222222222222222222222lstPartnerAdmin.size() :" + lstPartnerAdmin.size());
        return lstPartnerAdmin;
    }

    public void updateComplaintsForPayment() throws Exception {
        String query = "(select cp1.complaintId from tbl_CMS_ComplaintPayment cp1 where cp1.paymentFor";
        query = query + "= 'Registration Fee' and status='paid' and isVerified='Yes')";
        query = query + " INTERSECT ";
        query = query + "(select cp2.complaintId from tbl_CMS_ComplaintPayment cp2 where cp2.paymentFor='Security Deposit Fee' and status='paid'  and isVerified='Yes')";
        log.error("QUERY FOR UPDATE:" + query);
        String ids = "";
        List<Object> lstObjects = super.nativeSQLQuery(query);

        if (lstObjects != null && lstObjects.size() > 0) {
            log.error("list of objects size *******************" + lstObjects.size());

            for (int i = 0; i < lstObjects.size(); i++) {
                ids = ids + (Integer) lstObjects.get(i) + ",";
            }
            ids = ids.substring(0, ids.lastIndexOf(","));
        }
        log.error("ids for update complaints for payment" + ids);
//Updating payment status
        String queryStr = "update TblComplaintMaster master set master.paymentStatus='paid' where master.complaintId in (" + ids + ")";
        log.error("QUERY FOR UPDATE:" + queryStr);
        int updated = super.updateDeleteQuery(queryStr);
        if (updated != 0) {
            log.error("COMPLAINTS UPDATED FOR PAYMENT STATUS");
        } else {
            log.error("COMPLAINTS NOT UPDATED");
        }
    }

    @Override
    public List<TblComplaintMaster> searchProcessedComplaintDetails(String qq) throws Exception {
        StringBuilder query = new StringBuilder();
        log.error("Inside search complaint details");
        List<TblComplaintMaster> lstComplaintMaster = new ArrayList<TblComplaintMaster>();
        query.append("from TblComplaintMaster tt where tt.complaintLevel.complaintLevelId in (").append(qq).append(") and tt.complaintStatus!='pending'");
        List<Object> lstObjects = super.createQueryForObject(query.toString());
        log.error("Inside search complaint details: lstObjects.size(): " + lstObjects.size());
        if (lstObjects != null && !lstObjects.isEmpty()) {
            for (int i = 0; i < lstObjects.size(); i++) {
                lstComplaintMaster.add((TblComplaintMaster) lstObjects.get(i));
            }
        }
        log.error("Inside search complaint details: lstComplaintMaster.size(): " + lstComplaintMaster.size());
        return lstComplaintMaster;
    }

    @Override
    public List<TblComplaintMaster> searchPendingComplaintDetailsForDGCPTU(int ii) throws Exception {
        StringBuilder query = new StringBuilder();
        log.error("Inside search complaint details");
        List<TblComplaintMaster> lstComplaintMaster = new ArrayList<TblComplaintMaster>();
        query.append("from TblComplaintMaster tt where tt.complaintLevel.complaintLevelId in (").append(ii).append(") and tt.paymentStatus='paid'");
        List<Object> lstObjects = super.createQueryForObject(query.toString());
        log.error("Inside search complaint details: lstObjects.size(): " + lstObjects.size());
        if (lstObjects != null && !lstObjects.isEmpty()) {
            for (int i = 0; i < lstObjects.size(); i++) {
                lstComplaintMaster.add((TblComplaintMaster) lstObjects.get(i));
            }
        }
        log.error("Inside search complaint details: lstComplaintMaster.size(): " + lstComplaintMaster.size());
        return lstComplaintMaster;
    }

    @Override
    public List<TblComplaintMaster> searchPendingComplaintDetailsRP(int ii) throws Exception {
        StringBuilder query = new StringBuilder();
        log.error("Inside search complaint details");
        List<TblComplaintMaster> lstComplaintMaster = new ArrayList<TblComplaintMaster>();
        query.append("from TblComplaintMaster tt where tt.complaintLevel.complaintLevelId in (").append(ii).append(") and tt.complaintStatus='pending' and tt.paymentStatus='paid' and tt.panelId!=0");
        List<Object> lstObjects = super.createQueryForObject(query.toString());
        log.error("Inside search complaint details: lstObjects.size(): " + lstObjects.size());
        if (lstObjects != null && !lstObjects.isEmpty()) {
            for (int i = 0; i < lstObjects.size(); i++) {
                lstComplaintMaster.add((TblComplaintMaster) lstObjects.get(i));
            }
        }
        log.error("Inside search complaint details: lstComplaintMaster.size(): " + lstComplaintMaster.size());
        return lstComplaintMaster;
    }

    @Override
    public List<TblComplaintMaster> searchProcessedComplaintDetailsRP(int ii) throws Exception {
        StringBuilder query = new StringBuilder();
        log.error("Inside search complaint details");
        List<TblComplaintMaster> lstComplaintMaster = new ArrayList<TblComplaintMaster>();
        query.append("from TblComplaintMaster tt where tt.complaintLevel.complaintLevelId in (").append(ii).append(") and tt.complaintStatus!='pending' and tt.paymentStatus='paid' and tt.panelId!=0");
        List<Object> lstObjects = super.createQueryForObject(query.toString());
        log.error("Inside search complaint details: lstObjects.size(): " + lstObjects.size());
        if (lstObjects != null && !lstObjects.isEmpty()) {
            for (int i = 0; i < lstObjects.size(); i++) {
                lstComplaintMaster.add((TblComplaintMaster) lstObjects.get(i));
            }
        }
        log.error("Inside search complaint details: lstComplaintMaster.size(): " + lstComplaintMaster.size());
        return lstComplaintMaster;
    }

    @Override
    public List<Object> getEmailIdFromRpId(int rpId) throws Exception {
        String query = "select tl.emailId from TblReviewPanel rp , TblLoginMaster tl where rp.userId = tl.userId and rp.reviewPanelId=" + rpId;
        return super.singleColQuery(query);
    }

    @Override
    public List<SearchComplaintPaymentBean> searchComplaintPendingPayments(String verificationStatus, String paymentFor) throws Exception {
        String query = "";
        String join = "LEFT";

        List<SearchComplaintPaymentBean> lstSearchPayments = new ArrayList<SearchComplaintPaymentBean>();
        query = query + "select cm.complaintId , cm.complaintSubject, cm.tenderId, lm.emailId,  isNULL(cp.status,'pending') as status, isNULL(cp.isLive,'Yes') as isLive,isNULL(cp.isVerified,'No') as isVerified,";
        query = query + " (CONVERT(VARCHAR(20), cp.dtOfAction, 103) +' '+ CONVERT(VARCHAR(20), cp.dtOfAction, 24)) as dtOfAction, (CONVERT(VARCHAR(20), cm.complaintCreationDt, 103) +' '+ CONVERT(VARCHAR(20), cm.complaintCreationDt, 24)) as createdDate, isNULL(cp.complaintPaymentId,0) as complaintPaymentId, isNULL(cp.createdBy,0) as createdBy, cp.paymentFor, cm.paymentStatus from tbl_CMS_ComplaintMaster cm";
        query = query + " JOIN tbl_TendererMaster tm ON cm.tendererId=tm.tendererId";
        query = query + " JOIN tbl_TenderMaster tenderMaster ON tenderMaster.tenderId=cm.tenderId and cm.complaintLevelId=4";
        query = query + " JOIN tbl_LoginMaster lm ON  tm.userId=lm.userId ";



        log.error("in if 1 alllllllllllll");
        query = query + " JOIN tbl_CompanyMaster company ON tm.companyId=company.companyId";
        query = query + " JOIN tbl_CMS_ComplaintPayment cp ON ";
        query = query + "  cp.isVerified='" + verificationStatus + "' ";
        query = query + " JOIN tbl_CMS_ComplaintPayment cp ON  cm.complaintId=cp.complaintId and";
        if (verificationStatus != null) {
            if (verificationStatus.equalsIgnoreCase("Yes")) {
                if (query.contains(join)) {
                    query = query.replace(join, "");
                }
            }
            query = query + " cp.isVerified='" + verificationStatus + "' and";
        }
        query = query + " cp.paymentFor='" + paymentFor + "'";

        query = query + " JOIN tbl_CMS_ComplaintPayment cp ON ";
        query = query + " cm.complaintId=cp.complaintId and cp.paymentFor='" + paymentFor + "'";
        log.error("Inside search complaint details: QUERY STRING: " + query.toString());
        List<SearchComplaintPaymentBean> lstObjects = this.getPaymentSearchResult(query);
//TblComplaintMaster complaintMaster = new TblComplaintMaster();
        log.error("lstObjects.size()>>>>>>>>>>>>>" + lstObjects.size());
        if (lstObjects != null && lstObjects.size() != 0) {
            log.error("Inside search complaint details: lstObjects.size().size(): " + lstObjects.size());
            return lstObjects;
        }
        return null;
    }
}
