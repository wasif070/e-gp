package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblDebarmentCommitteeDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblDebarmentCommittee;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblDebarmentCommitteeImpl extends AbcAbstractClass<TblDebarmentCommittee> implements TblDebarmentCommitteeDao {

    @Override
    public void addTblDebarmentCommittee(TblDebarmentCommittee master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblDebarmentCommittee(TblDebarmentCommittee master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblDebarmentCommittee(TblDebarmentCommittee master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblDebarmentCommittee> getAllTblDebarmentCommittee() {
        return super.getAllEntity();
    }

    @Override
    public List<TblDebarmentCommittee> findTblDebarmentCommittee(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblDebarmentCommitteeCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblDebarmentCommittee> findByCountTblDebarmentCommittee(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
