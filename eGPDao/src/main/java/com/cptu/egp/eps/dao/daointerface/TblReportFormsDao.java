package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblReportForms;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblReportFormsDao extends GenericDao<TblReportForms> {

    public void addTblReportForms(TblReportForms forms);

    public void deleteTblReportForms(TblReportForms forms);

    public void updateTblReportForms(TblReportForms forms);

    public List<TblReportForms> getAllTblReportForms();

    public List<TblReportForms> findTblReportForms(Object... values) throws Exception;

    public List<TblReportForms> findByCountTblReportForms(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblReportFormsCount();

    public void updateOrSaveAllRepForms(List<TblReportForms> Event);
}
