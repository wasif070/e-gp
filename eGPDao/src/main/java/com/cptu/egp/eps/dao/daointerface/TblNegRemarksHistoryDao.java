package com.cptu.egp.eps.dao.daointerface;


import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblNegRemarksHistory;
import java.util.List;


public interface TblNegRemarksHistoryDao extends GenericDao<TblNegRemarksHistory> {

    public void addTblNegRemarksHistory(TblNegRemarksHistory tblObj);

    public void deleteTblNegRemarksHistory(TblNegRemarksHistory tblObj);

    public void updateTblNegRemarksHistory(TblNegRemarksHistory tblObj);

    public List<TblNegRemarksHistory> getAllTblNegRemarksHistory();

    public List<TblNegRemarksHistory> findTblNegRemarksHistory(Object... values) throws Exception;

    public List<TblNegRemarksHistory> findByCountTblNegRemarksHistory(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblNegRemarksHistoryCount();

    public void updateOrSaveTblNegRemarksHistory(List<TblNegRemarksHistory> tblObj);
}