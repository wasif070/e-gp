package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvTcvariDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvTcvari;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsSrvTcvariImpl extends AbcAbstractClass<TblCmsSrvTcvari> implements TblCmsSrvTcvariDao {

    @Override
    public void addTblCmsSrvTcvari(TblCmsSrvTcvari cmsInvoiceDetails){
        super.addEntity(cmsInvoiceDetails);
    }

    @Override
    public void deleteTblCmsSrvTcvari(TblCmsSrvTcvari cmsInvoiceDetails) {
        super.deleteEntity(cmsInvoiceDetails);
    }

    @Override
    public void updateTblCmsSrvTcvari(TblCmsSrvTcvari cmsInvoiceDetails) {
        super.updateEntity(cmsInvoiceDetails);
    }

    @Override
    public List<TblCmsSrvTcvari> getAllTblCmsSrvTcvari() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvTcvari> findTblCmsSrvTcvari(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvTcvariCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvTcvari> findByCountTblCmsSrvTcvari(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }


    @Override
    public void updateOrSaveEstCost(List<TblCmsSrvTcvari> estCost) {
       super.updateAll(estCost);
    }
}
