package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import java.util.List;
import com.cptu.egp.eps.model.table.TblReviewPanel;
import com.cptu.egp.eps.dao.daointerface.TblReviewPanelDao;

/**
 *
 * @author Radha
 */
public class TblReviewPanelDaoImpl extends AbcAbstractClass<TblReviewPanel> implements TblReviewPanelDao {

 

    @Override
    public List<TblReviewPanel> getReviewPanels(){
    	return super.getAllEntity();
    }   
    @Override
    public void addReviewPanel(TblReviewPanel reviewPanel){
    	super.addEntity(reviewPanel);
    }
    @Override
    public void updateOrSaveReviewPanel(TblReviewPanel reviewPanel) {
        super.updateEntity(reviewPanel);
    }
      
}
