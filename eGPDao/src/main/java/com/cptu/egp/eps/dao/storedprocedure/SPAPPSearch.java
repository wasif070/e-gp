/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author rishita
 */
public class SPAPPSearch extends StoredProcedure {
    private static final Logger LOGGER = Logger.getLogger(SPAPPSearch.class);
    public SPAPPSearch(BasicDataSource dataSource, String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("@v_financialYear_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_departmentId_inN", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_stateId_inN", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_officeId_inN", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_Page_inN", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_RecordPerPage_inN", Types.INTEGER));
    }

    /**
     * Execute stored procedure : p_app_search
     * to performed search APP.
     * @param financialYear
     * @param departmentId
     * @param stateId
     * @param officeId
     * @param pageNo
     * @param recordPerPage
     * @return APPSearch Details as list of APPSearchDtBean object.
     */
    public List<APPSearchDtBean> executeProcedure(String financialYear,int departmentId,int stateId,int officeId,int pageNo,int recordPerPage) {
        Map inParams = new HashMap();
        inParams.put("@v_financialYear_inVc", financialYear);
        inParams.put("@v_departmentId_inN", departmentId);
        inParams.put("@v_stateId_inN", stateId);
        inParams.put("@v_officeId_inN", officeId);
        inParams.put("@v_Page_inN", pageNo);
        inParams.put("@v_RecordPerPage_inN", recordPerPage);
        
        
        this.compile();
        List<APPSearchDtBean> details = new ArrayList<APPSearchDtBean>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                APPSearchDtBean appDtBean = new APPSearchDtBean();

                appDtBean.setMinistry((String) linkedHashMap.get("Ministry"));
                appDtBean.setDivision((String) linkedHashMap.get("Division"));
                appDtBean.setOranisation((String) linkedHashMap.get("Oranisation"));
                appDtBean.setOfficeName((String) linkedHashMap.get("OfficeName"));
                appDtBean.setBudgetType((String) linkedHashMap.get("BudgetType"));
                appDtBean.setOfficeId((String) linkedHashMap.get("OfficeId"));
                appDtBean.setAppId((String) linkedHashMap.get("AppId"));
                appDtBean.setTotalRowCount((String) linkedHashMap.get("TotalRowCount"));
                appDtBean.setTotalPages((String) linkedHashMap.get("TotalPages"));
                details.add(appDtBean);
            }
        } catch (Exception e) {
            LOGGER.error("SPAPPSearch : "+ e);
        }
        return details;
    }
}
