/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblDesignationMaster;
import java.util.List;

/**
 * <b>Interface Description goes here</b>
 * $Revision: 1.1.1.1 $
 * @version <version-no.> <Date>
 * @author Administrator
 */
public interface TblDesignationMasterDao extends GenericDao<TblDesignationMaster>{

    public void addDesignation(TblDesignationMaster tblDesignationMaster);

    public void deleteDesignnation(TblDesignationMaster tblDesignationMaster);

    public void updateDesignantion(TblDesignationMaster tblDesignationMaster);

    public List<TblDesignationMaster> getAllDesinations();

    public List<TblDesignationMaster> findDesignantion(Object... values) throws Exception;

    public List<TblDesignationMaster> findByCountDesignation(int firstResult,int maxResult,Object... values) throws Exception;

    public long getDesignationCount();

}
