package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblEvalTernotifyMemberDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalTernotifyMember;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblEvalTernotifyMemberImpl extends AbcAbstractClass<TblEvalTernotifyMember> implements TblEvalTernotifyMemberDao {

    @Override
    public void addTblEvalTernotifyMember(TblEvalTernotifyMember evalTernotifyMember){
        super.addEntity(evalTernotifyMember);
    }

    @Override
    public void deleteTblEvalTernotifyMember(TblEvalTernotifyMember evalTernotifyMember) {
        super.deleteEntity(evalTernotifyMember);
    }

    @Override
    public void updateTblEvalTernotifyMember(TblEvalTernotifyMember evalTernotifyMember) {
        super.updateEntity(evalTernotifyMember);
    }

    @Override
    public List<TblEvalTernotifyMember> getAllTblEvalTernotifyMember() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEvalTernotifyMember> findTblEvalTernotifyMember(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEvalTernotifyMemberCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalTernotifyMember> findByCountTblEvalTernotifyMember(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
