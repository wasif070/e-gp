/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderBidFormDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderBidForm;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderBidFormImpl extends AbcAbstractClass<TblTenderBidForm> implements TblTenderBidFormDao {

    @Override
    public void addTblTenderBidForm(TblTenderBidForm admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblTenderBidForm(TblTenderBidForm admin) {
        super.deleteEntity(admin);        
    }

    @Override
    public void updateTblTenderBidForm(TblTenderBidForm admin) {
        super.updateEntity(admin);        
    }

    @Override
    public List<TblTenderBidForm> getAllTblTenderBidForm() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderBidForm> findTblTenderBidForm(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderBidFormCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderBidForm> findByCountTblTenderBidForm(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
