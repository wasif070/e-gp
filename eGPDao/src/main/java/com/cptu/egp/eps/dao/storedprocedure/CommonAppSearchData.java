/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.math.BigDecimal;

/**
 *
 * @author Ramesh.Janagondakuru
 */
public class CommonAppSearchData
{

    private Integer appId;
    private String appCode;
    private String departmentName;
    private String employeeName;
    private String stateName;
    private String procurementNature;
    private String packageNo;
    private String packageDesc;
    private String projectName;
    private String financialYear;
    private Integer departmentId;
    private Integer employeeId;
    private String cpvCode;
    private String procurementType;
    private BigDecimal estimatedCost;
    private BigDecimal pkgEstCost;
    private Integer officeId;
    private String budgetType;
    private String procurementMethod;
    private String officeName;
    private String status;
    private Integer totalRecords;
    private Integer packageId;
    private String workFlowStatus;

    public String getWorkFlowStatus() {
        return workFlowStatus;
    }

    public void setWorkFlowStatus(String workFlowStatus) {
        this.workFlowStatus = workFlowStatus;
    }





    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public Integer getPackageId()
    {
        return packageId;
    }

    public void setPackageId(Integer packageId)
    {
        this.packageId = packageId;
    }


    public String getAppCode()
    {
        return appCode;
    }

    public void setAppCode(String appCode)
    {
        this.appCode = appCode;
    }

    public Integer getAppId()
    {
        return appId;
    }

    public void setAppId(Integer appId)
    {
        this.appId = appId;
    }

    public String getBudgetType() {
        return budgetType;
    }

    public void setBudgetType(String budgetType) {
        this.budgetType = budgetType;
    }

    public String getCpvCode()
    {
        return cpvCode;
    }

    public void setCpvCode(String cpvCode)
    {
        this.cpvCode = cpvCode;
    }

    public Integer getDepartmentId()
    {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId)
    {
        this.departmentId = departmentId;
    }

    public String getDepartmentName()
    {
        return departmentName;
    }

    public void setDepartmentName(String departmentName)
    {
        this.departmentName = departmentName;
    }

    public Integer getEmployeeId()
    {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId)
    {
        this.employeeId = employeeId;
    }

    public String getEmployeeName()
    {
        return employeeName;
    }

    public void setEmployeeName(String employeeName)
    {
        this.employeeName = employeeName;
    }

    public BigDecimal getEstimatedCost()
    {
        return estimatedCost;
    }

    public void setEstimatedCost(BigDecimal estimatedCost)
    {
        this.estimatedCost = estimatedCost;
    }

    public String getFinancialYear()
    {
        return financialYear;
    }

    public void setFinancialYear(String financialYear)
    {
        this.financialYear = financialYear;
    }

    public Integer getOfficeId()
    {
        return officeId;
    }

    public void setOfficeId(Integer officeId)
    {
        this.officeId = officeId;
    }

    public String getOfficeName()
    {
        return officeName;
    }

    public void setOfficeName(String officeName)
    {
        this.officeName = officeName;
    }

    public String getPackageDesc()
    {
        return packageDesc;
    }

    public void setPackageDesc(String packageDesc)
    {
        this.packageDesc = packageDesc;
    }

    public String getPackageNo()
    {
        return packageNo;
    }

    public void setPackageNo(String packageNo)
    {
        this.packageNo = packageNo;
    }

    public BigDecimal getPkgEstCost()
    {
        return pkgEstCost;
    }

    public void setPkgEstCost(BigDecimal pkgEstCost)
    {
        this.pkgEstCost = pkgEstCost;
    }

    public String getProcurementMethod()
    {
        return procurementMethod;
    }

    public void setProcurementMethod(String procurementMethod)
    {
        this.procurementMethod = procurementMethod;
    }

    public String getProcurementNature()
    {
        return procurementNature;
    }

    public void setProcurementNature(String procurementNature)
    {
        this.procurementNature = procurementNature;
    }

    public String getProcurementType()
    {
        return procurementType;
    }

    public void setProcurementType(String procurementType)
    {
        this.procurementType = procurementType;
    }

    public String getProjectName()
    {
        return projectName;
    }

    public void setProjectName(String projectName)
    {
        this.projectName = projectName;
    }

    public String getStateName()
    {
        return stateName;
    }

    public void setStateName(String stateName)
    {
        this.stateName = stateName;
    }

    public Integer getTotalRecords()
    {
        return totalRecords;
    }

    public void setTotalRecords(Integer totalRecords)
    {
        this.totalRecords = totalRecords;
    }

}
