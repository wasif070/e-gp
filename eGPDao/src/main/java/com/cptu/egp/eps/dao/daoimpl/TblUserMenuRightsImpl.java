package com.cptu.egp.eps.dao.daoimpl;


import com.cptu.egp.eps.dao.daointerface.TblUserMenuRightsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblUserMenuRights;
import java.util.List;


public class TblUserMenuRightsImpl extends AbcAbstractClass<TblUserMenuRights> implements TblUserMenuRightsDao {

	@Override 
	public void addtblUserMenuRights(TblUserMenuRights master){
		super.addEntity(master);
	}
	@Override 
	public void deletetblUserMenuRights(TblUserMenuRights master) {
		super.deleteEntity(master);
	}
	@Override 
	public void updatetblUserMenuRights(TblUserMenuRights master) {
		super.updateEntity(master);
	}
	@Override 
	public List<TblUserMenuRights> getAlltblUserMenuRights() {
		return super.getAllEntity();
	}
	@Override 
	public List<TblUserMenuRights> findtblUserMenuRights(Object... values) throws Exception {
		return super.findEntity(values);
	}
	@Override 
	public long gettblUserMenuRightsCount() {
		return super.getEntityCount();
	}
	@Override 
	public List<TblUserMenuRights> findByCounttblUserMenuRights(int firstResult, int maxResult, Object... values) throws Exception {
		return super.findByCountEntity(firstResult, maxResult, values);
	}
	@Override 
	public void updateOrSavetblUserMenuRights(List<TblUserMenuRights> tblObj) {
		super.updateAll(tblObj);
	}
}