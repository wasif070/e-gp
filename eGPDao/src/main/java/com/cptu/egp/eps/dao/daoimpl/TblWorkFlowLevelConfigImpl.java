/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblWorkFlowLevelConfigDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public class TblWorkFlowLevelConfigImpl extends AbcAbstractClass<TblWorkFlowLevelConfig> implements TblWorkFlowLevelConfigDao {

    @Override
    public void addTblWorkFlowLevelConfig(TblWorkFlowLevelConfig workFlowLevelConfig) {

        super.addEntity(workFlowLevelConfig);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblWorkFlowLevelConfig(TblWorkFlowLevelConfig workFlowLevelConfig) {

        super.deleteEntity(workFlowLevelConfig);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblWorkFlowLevelConfig(TblWorkFlowLevelConfig workFlowLevelConfig) {

        super.updateEntity(workFlowLevelConfig);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblWorkFlowLevelConfig> getAllTblWorkFlowLevelConfig() {
        return super.getAllEntity();
    }

    @Override
    public List<TblWorkFlowLevelConfig> findTblWorkFlowLevelConfig(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblWorkFlowLevelConfigCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblWorkFlowLevelConfig> findByCountTblWorkFlowLevelConfig(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public int updateDeleteQuery(String query) {
        return super.updateDeleteQuery(query);
    }

    @Override
    public int updateDeleteSQLQuery(String query) {
        return super.updateDeleteSQLQuery(query);
    }
    

}
