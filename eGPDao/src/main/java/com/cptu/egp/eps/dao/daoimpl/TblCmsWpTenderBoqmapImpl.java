package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsWpTenderBoqmapDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsWptenderBoqmap;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsWpTenderBoqmapImpl extends AbcAbstractClass<TblCmsWptenderBoqmap> implements TblCmsWpTenderBoqmapDao {

    @Override
    public void addTblCmsWptenderBoqmap(TblCmsWptenderBoqmap cmsWptenderBoqmap){
        super.addEntity(cmsWptenderBoqmap);
    }

    @Override
    public void deleteTblCmsWptenderBoqmap(TblCmsWptenderBoqmap cmsWptenderBoqmap) {
        super.deleteEntity(cmsWptenderBoqmap);
    }

    @Override
    public void updateTblCmsWptenderBoqmap(TblCmsWptenderBoqmap cmsWptenderBoqmap) {
        super.updateEntity(cmsWptenderBoqmap);
    }

    @Override
    public List<TblCmsWptenderBoqmap> getAllTblCmsWptenderBoqmap() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsWptenderBoqmap> findTblCmsWptenderBoqmap(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsWptenderBoqmapCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsWptenderBoqmap> findByCountTblCmsWptenderBoqmap(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsWptenderBoqmap> estCost) {
         super.updateAll(estCost);
    }
}
