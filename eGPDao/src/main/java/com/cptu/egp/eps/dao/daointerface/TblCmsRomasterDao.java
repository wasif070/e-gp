package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsRomaster;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsRomasterDao extends GenericDao<TblCmsRomaster> {

    public void addTblCmsRomaster(TblCmsRomaster tblObj);

    public void deleteTblCmsRomaster(TblCmsRomaster tblObj);

    public void updateTblCmsRomaster(TblCmsRomaster tblObj);

    public List<TblCmsRomaster> getAllTblCmsRomaster();

    public List<TblCmsRomaster> findTblCmsRomaster(Object... values) throws Exception;

    public List<TblCmsRomaster> findByCountTblCmsRomaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsRomasterCount();
    
    public void updateInsertAllTblCmsRomaster(List<TblCmsRomaster> list);
}
