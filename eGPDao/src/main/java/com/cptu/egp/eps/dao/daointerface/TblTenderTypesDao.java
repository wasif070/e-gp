/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderTypes;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TblTenderTypesDao extends GenericDao<TblTenderTypes>{

    public void addTblTenderTypes(TblTenderTypes tblObj);

    public void deleteTblTenderTypes(TblTenderTypes tblObj);

    public void updateTblTenderTypes(TblTenderTypes tblObj);

    public List<TblTenderTypes> getAllTblTenderTypes();

    public List<TblTenderTypes> findTblTenderTypes(Object... values) throws Exception;

    public List<TblTenderTypes> findByCountTblTenderTypes(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderTypesCount();
}
