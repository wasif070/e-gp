/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblBidDocuments;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblBidDocumentsDao extends GenericDao<TblBidDocuments>{

    public void addTblBidDocuments(TblBidDocuments tblObj);

    public void deleteTblBidDocuments(TblBidDocuments tblObj);

    public void updateTblBidDocuments(TblBidDocuments tblObj);

    public List<TblBidDocuments> getAllTblBidDocuments();

    public List<TblBidDocuments> findTblBidDocuments(Object... values) throws Exception;

    public List<TblBidDocuments> findByCountTblBidDocuments(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblBidDocumentsCount();
}
