/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblDebarmentReqDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblDebarmentReq;
import java.util.List;

/**
 *
 * @author test
 */
public class TblDebarmentReqDaoImpl extends AbcAbstractClass<TblDebarmentReq> implements TblDebarmentReqDao {

    @Override
    public void addTblDebarmentReq(TblDebarmentReq tblDebarmentReq) {
        super.addEntity(tblDebarmentReq);
    }

    @Override
    public void deleteTblDebarmentReq(TblDebarmentReq tblDebarmentReq) {
        super.deleteEntity(tblDebarmentReq);
    }

    @Override
    public void updateTblDebarmentReq(TblDebarmentReq tblDebarmentReq) {
        super.updateEntity(tblDebarmentReq);
    }

    @Override
    public List<TblDebarmentReq> getAllTblDebarmentReq() {
        return super.getAllEntity();
    }

    @Override
    public List<TblDebarmentReq> findTblDebarmentReq(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public List<TblDebarmentReq> findByCountTblDebarmentReq(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblDebarmentReq() {
        return super.getEntityCount();
    }

    

}
