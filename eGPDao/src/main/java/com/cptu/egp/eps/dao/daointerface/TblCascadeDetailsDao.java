/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCascadeLinkDetails;
import java.util.List;

/**
 *
 * @author tejasree.goriparthi
 */
public interface TblCascadeDetailsDao extends GenericDao<TblCascadeLinkDetails> {

    public List<TblCascadeLinkDetails> getAllTblCascadeDetails();

    public List<TblCascadeLinkDetails> findTblCascadeDetails(Object... values) throws Exception;
}
