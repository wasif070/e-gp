/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEmployeeMaster;
import java.util.List;

/**
 * <b>Interface Description goes here</b>
 * $Revision: 1.1.1.1 $
 * @version <version-no.> <Date>
 * @author Administrator
 */
public interface TblEmployeeMasterDao extends GenericDao<TblEmployeeMaster>{

    public void addEmployee(TblEmployeeMaster tblEmployeeMaster);

    public void deleteEmployee(TblEmployeeMaster tblEmployeeMaster);

    public void updateEmployee(TblEmployeeMaster tblEmployeeMaster);

    public List<TblEmployeeMaster> getAllEmployees();

    public List<TblEmployeeMaster> findEmployee(Object... values) throws Exception;

    public List<TblEmployeeMaster> findByCountEployee(int firstResult,int maxResult,Object... values) throws Exception;

    public long getEmployeeCount();

}
