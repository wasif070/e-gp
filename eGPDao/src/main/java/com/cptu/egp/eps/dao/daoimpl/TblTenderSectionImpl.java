/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderSectionDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderSection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderSectionImpl extends AbcAbstractClass<TblTenderSection> implements TblTenderSectionDao {

    @Override
    public void addTblTenderSection(TblTenderSection admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblTenderSection(TblTenderSection admin) {
        super.deleteEntity(admin);        
    }

    @Override
    public void updateTblTenderSection(TblTenderSection admin) {
        super.updateEntity(admin);        
    }

    @Override
    public List<TblTenderSection> getAllTblTenderSection() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderSection> findTblTenderSection(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderSectionCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderSection> findByCountTblTenderSection(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
