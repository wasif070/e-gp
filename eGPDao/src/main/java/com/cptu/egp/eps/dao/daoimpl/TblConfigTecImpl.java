/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblConfigTecDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblConfigTec;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblConfigTecImpl extends AbcAbstractClass<TblConfigTec> implements TblConfigTecDao{

    @Override
    public void addTblConfigTec(TblConfigTec tblConfigTec) {
        super.addEntity(tblConfigTec);
    }

    @Override
    public List<TblConfigTec> findTblConfigTec(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblConfigTec(TblConfigTec tblConfigTec) {

        super.deleteEntity(tblConfigTec);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblConfigTec(TblConfigTec tblConfigTec) {

        super.updateEntity(tblConfigTec);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblConfigTec> getAllTblConfigTec() {
        return super.getAllEntity();
    }

    @Override
    public long getTblConfigTecCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblConfigTec> findByCountTblConfigTec(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
