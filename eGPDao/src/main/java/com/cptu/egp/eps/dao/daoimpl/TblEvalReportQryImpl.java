/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblEvalReportQryDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalReportQry;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblEvalReportQryImpl extends AbcAbstractClass<TblEvalReportQry> implements TblEvalReportQryDao{

    @Override
    public void addTblEvalReportQry(TblEvalReportQry tblEvalReportQry) {
        super.addEntity(tblEvalReportQry);
    }

    @Override
    public List<TblEvalReportQry> findTblEvalReportQry(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblEvalReportQry(TblEvalReportQry tblEvalReportQry) {

        super.deleteEntity(tblEvalReportQry);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblEvalReportQry(Collection tblEvalReportQry) {
        super.deleteAll(tblEvalReportQry);
    }

    @Override
    public void updateTblEvalReportQry(TblEvalReportQry tblEvalReportQry) {

        super.updateEntity(tblEvalReportQry);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblEvalReportQry> getAllTblEvalReportQry() {
        return super.getAllEntity();
    }

    @Override
    public long getTblEvalReportQryCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalReportQry> findByCountTblEvalReportQry(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
