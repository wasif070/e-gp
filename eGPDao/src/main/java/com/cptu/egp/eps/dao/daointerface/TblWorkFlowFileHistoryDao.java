/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblWorkFlowFileHistory;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public interface TblWorkFlowFileHistoryDao extends GenericDao<TblWorkFlowFileHistory> {

    public void addTblWorkFlowFileHistory(TblWorkFlowFileHistory admin);

    public void deleteTblWorkFlowFileHistory(TblWorkFlowFileHistory admin);

    public void updateTblWorkFlowFileHistory(TblWorkFlowFileHistory admin);

    public List<TblWorkFlowFileHistory> getAllTblWorkFlowFileHistory();

    public List<TblWorkFlowFileHistory> findTblWorkFlowFileHistory(Object... values) throws Exception;

    public List<TblWorkFlowFileHistory> findByCountTblWorkFlowFileHistory(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblWorkFlowFileHistoryCount();
}
