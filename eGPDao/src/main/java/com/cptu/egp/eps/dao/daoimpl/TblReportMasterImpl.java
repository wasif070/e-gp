/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblReportMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblReportMaster;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblReportMasterImpl extends AbcAbstractClass<TblReportMaster> implements TblReportMasterDao{

    @Override
    public void addTblReportMaster(TblReportMaster reportMaster) {
        super.addEntity(reportMaster);
    }

    @Override
    public List<TblReportMaster> findTblReportMaster(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblReportMaster(TblReportMaster reportMaster) {

        super.deleteEntity(reportMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblReportMaster(Collection event) {
        super.deleteAll(event);
    }

    @Override
    public void updateTblReportMaster(TblReportMaster reportMaster) {

        super.updateEntity(reportMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblReportMaster> getAllTblReportMaster() {
        return super.getAllEntity();
    }

    @Override
    public long getTblReportMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblReportMaster> findByCountTblReportMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
