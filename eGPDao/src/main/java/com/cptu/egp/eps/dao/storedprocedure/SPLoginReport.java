/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author rishita
 */
public class SPLoginReport extends StoredProcedure {
   private static final Logger LOGGER = Logger.getLogger(SPLoginReport.class);

   /**
    *
    * @param dataSource
    * @param procName
    */
   public SPLoginReport(BasicDataSource dataSource, String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("@v_Typeofuser_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_Searchfordate_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_DateFrom_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_DateTo_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_EmailId_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_UserId_inVc", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_UserTypeId_inVc", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_ipaddress_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_PageId_inVc", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_TotalPage_inVc", Types.INTEGER));
    }

   /**
    * Execute stored procedure : p_search_LoginReport
    * Use for get user login detail for generate login report.
    * @param typeOfUser
    * @param searchForDate
    * @param dateFrom
    * @param dateTo
    * @param emailId
    * @param userId
    * @param userTypeId
    * @param pageNo
    * @param totalPages
    * @param ipaddress
    * @return Login details as List of LoginReportDtBean Object
    */
    public List<LoginReportDtBean> executeProcedure(String typeOfUser, String searchForDate, String dateFrom, String dateTo, String emailId,Integer userId,Integer userTypeId, Integer pageNo, Integer totalPages,String ipaddress) {
        Map inParams = new HashMap();
        inParams.put("@v_Typeofuser_inVc", typeOfUser);
        inParams.put("@v_Searchfordate_inVc", searchForDate);
        inParams.put("@v_DateFrom_inVc", dateFrom);
        inParams.put("@v_DateTo_inVc", dateTo);
        inParams.put("@v_EmailId_inVc", emailId);
        inParams.put("@v_UserId_inVc", userId);
        inParams.put("@v_UserTypeId_inVc", userTypeId);
        inParams.put("@v_ipaddress_inVc", ipaddress);
        inParams.put("@v_PageId_inVc", pageNo);
        inParams.put("@v_TotalPage_inVc", totalPages);
        this.compile();
        List<LoginReportDtBean> details = new ArrayList<LoginReportDtBean>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                LoginReportDtBean loginReportDtBean = new LoginReportDtBean();
                loginReportDtBean.setRowNumber((Long) linkedHashMap.get("Rownumber"));
                loginReportDtBean.setUserId((Integer) linkedHashMap.get("userId"));
                loginReportDtBean.setEmailId((String) linkedHashMap.get("EmailId"));
                loginReportDtBean.setUserTyperId((Integer) linkedHashMap.get("userTyperId"));
                loginReportDtBean.setSessionStartDt((Date) linkedHashMap.get("sessionStartDt"));
                loginReportDtBean.setSessionEndDt((Date) linkedHashMap.get("sessionEndDt"));
                loginReportDtBean.setIpAddress((String) linkedHashMap.get("ipAddress"));
                loginReportDtBean.setTotalPages((Integer) linkedHashMap.get("TotalPages"));
                loginReportDtBean.setTotalRecords((Integer) linkedHashMap.get("TotalRecords"));
                details.add(loginReportDtBean);
            }
        } catch (Exception e) {
            LOGGER.error("SPLoginReport  : "+ e);
        }
        return details;
    }
}
