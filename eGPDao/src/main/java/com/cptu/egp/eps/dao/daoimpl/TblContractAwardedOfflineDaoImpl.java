/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblContractAwardedOfflineDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblContractAwardedOffline;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class TblContractAwardedOfflineDaoImpl extends AbcAbstractClass<TblContractAwardedOffline> implements TblContractAwardedOfflineDao{

    @Override
    public void addTblContractAwardedOffline(TblContractAwardedOffline tblObj) {
        super.addEntity(tblObj);
    }

    @Override
    public void deleteTblContractAwardedOffline(TblContractAwardedOffline tblObj) {
        super.deleteEntity(tblObj);
    }

    @Override
    public void updateTblContractAwardedOffline(TblContractAwardedOffline tblObj) {
        super.updateEntity(tblObj);
    }

    @Override
    public List<TblContractAwardedOffline> getAllTblContractAwardedOffline() {
       return super.getAllEntity();
    }

    @Override
    public List<TblContractAwardedOffline> findTblContractAwardedOffline(Object... values) throws Exception {
       return super.findEntity(values);
    }

    @Override
    public List<TblContractAwardedOffline> findByCountTblContractAwardedOffline(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblContractAwardedOfflineCount() {
        return super.getEntityCount();
    }

}
