package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalRptApprove;
import java.util.List;

/**
 *
 * @author rishita
 */
public interface TblEvalRptApproveDao extends GenericDao<TblEvalRptApprove> {

    public void addTblEvalRptApprove(TblEvalRptApprove tblEvalRptApprove);

    public void deleteTblEvalRptApprove(TblEvalRptApprove tblEvalRptApprove);

    public void updateTblEvalRptApprove(TblEvalRptApprove tblEvalRptApprove);

    public List<TblEvalRptApprove> getAllTblEvalRptApprove();

    public List<TblEvalRptApprove> findTblEvalRptApprove(Object... values) throws Exception;

    public List<TblEvalRptApprove> findByCountTblEvalRptApprove(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalRptApproveCount();
}
