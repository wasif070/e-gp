/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderLotSecurityDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderLotSecurity;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderLotSecurityImpl extends AbcAbstractClass<TblTenderLotSecurity> implements TblTenderLotSecurityDao {

    @Override
    public void addTblTenderLotSecurity(TblTenderLotSecurity tenderLotSecurity) {
        super.addEntity(tenderLotSecurity);
    }

    @Override
    public void deleteTblTenderLotSecurity(TblTenderLotSecurity tenderLotSecurity) {
        super.deleteEntity(tenderLotSecurity);
    }

    @Override
    public void updateTblTenderLotSecurity(TblTenderLotSecurity tenderLotSecurity) {
        super.updateEntity(tenderLotSecurity);
    }

    @Override
    public List<TblTenderLotSecurity> getAllTblTenderLotSecurity() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderLotSecurity> findTblTenderLotSecurity(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderLotSecurityCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderLotSecurity> findByCountTblTenderLotSecurity(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
