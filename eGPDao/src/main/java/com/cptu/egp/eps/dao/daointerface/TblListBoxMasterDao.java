package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblListBoxMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblListBoxMasterDao extends GenericDao<TblListBoxMaster> {

    public void addTblListBoxMaster(TblListBoxMaster tblObj);

    public void deleteTblListBoxMaster(TblListBoxMaster tblObj);

    public void updateTblListBoxMaster(TblListBoxMaster tblObj);

    public List<TblListBoxMaster> getAllTblListBoxMaster();

    public List<TblListBoxMaster> findTblListBoxMaster(Object... values) throws Exception;

    public List<TblListBoxMaster> findByCountTblListBoxMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblListBoxMasterCount();
}
