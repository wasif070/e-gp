package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblDebarmentComMembers;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblDebarmentComMembersDao extends GenericDao<TblDebarmentComMembers> {

    public void addTblDebarmentComMembers(TblDebarmentComMembers tblObj);

    public void deleteTblDebarmentComMembers(TblDebarmentComMembers tblObj);

    public void updateTblDebarmentComMembers(TblDebarmentComMembers tblObj);

    public List<TblDebarmentComMembers> getAllTblDebarmentComMembers();

    public List<TblDebarmentComMembers> findTblDebarmentComMembers(Object... values) throws Exception;

    public List<TblDebarmentComMembers> findByCountTblDebarmentComMembers(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblDebarmentComMembersCount();

    public void updateSaveAllDebarComMembers(List<TblDebarmentComMembers> comMembers);
}
