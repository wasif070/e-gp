/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTemplateTablesDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTemplateTables;
import java.util.List;

/**
 *
 * @author yanki
 */
public class TblTemplateTablesImpl extends AbcAbstractClass<TblTemplateTables> implements TblTemplateTablesDao{

    @Override
    public void addTblTemplateTables(TblTemplateTables templateTables) {
        super.addEntity(templateTables);
    }

    @Override
    public void deleteTblTemplateTables(TblTemplateTables templateTables) {
        super.deleteEntity(templateTables);
    }

    @Override
    public void updateTblTemplateTables(TblTemplateTables templateTables) {
        super.updateEntity(templateTables);
    }

    @Override
    public List<TblTemplateTables> getAllTblTemplateTables() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTemplateTables> findTblTemplateTables(Object... values) throws Exception {
        return super.findEntity(values);
    }

}
