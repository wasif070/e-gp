/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblEvalServiceCriteriaDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalServiceCriteria;
import java.util.List;



/**
 *
 * @author Administrator
 */
public class TblEvalServiceCriteriaImpl extends AbcAbstractClass<TblEvalServiceCriteria> implements TblEvalServiceCriteriaDao{


    @Override
    public void addTblEvalServiceCriteria(TblEvalServiceCriteria evalServiceCriteria){
        super.addEntity(evalServiceCriteria);
    }

    @Override
    public void deleteTblEvalServiceCriteria(TblEvalServiceCriteria evalServiceCriteria) {
        super.deleteEntity(evalServiceCriteria);
    }

    @Override
    public void updateTblEvalServiceCriteria(TblEvalServiceCriteria evalServiceCriteria) {
        super.updateEntity(evalServiceCriteria);
    }

    @Override
    public List<TblEvalServiceCriteria> getAllTblEvalServiceCriteria() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEvalServiceCriteria> findTblEvalServiceCriteria(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEvalServiceCriteriaCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalServiceCriteria> findByCountTblEvalServiceCriteria(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrInsertAll(List<TblEvalServiceCriteria> list) {
       super.updateAll(list);
    }

}
