package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEmployeeTrasfer;
import java.util.List;

/**
 *
 * @author rishita
 */
public interface TblEmployeeTrasferDao extends GenericDao<TblEmployeeTrasfer> {

    public void addTblEmployeeTrasfer(TblEmployeeTrasfer tblObj);

    public void deleteTblEmployeeTrasfer(TblEmployeeTrasfer tblObj);

    public void updateTblEmployeeTrasfer(TblEmployeeTrasfer tblObj);

    public List<TblEmployeeTrasfer> getAllTblEmployeeTrasfer();

    public List<TblEmployeeTrasfer> findTblEmployeeTrasfer(Object... values) throws Exception;

    public List<TblEmployeeTrasfer> findByCountTblEmployeeTrasfer(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEmployeeTrasferCount();
}
