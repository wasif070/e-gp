/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTdsHeaderDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTdsHeader;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTdsHeaderImpl extends AbcAbstractClass<TblTdsHeader> implements TblTdsHeaderDao{

    @Override
    public void addTblTdsHeader(TblTdsHeader tblTdsHeader) {
        super.addEntity(tblTdsHeader);
    }

    @Override
    public List<TblTdsHeader> findTblTdsHeader(Object... values) throws Exception {
        return super.findEntity(values);
        
    }

    @Override
    public void deleteTblTdsHeader(TblTdsHeader tblTdsHeader) {

        super.deleteEntity(tblTdsHeader);
       
    }

    @Override
    public void updateTblTdsHeader(TblTdsHeader tblTdsHeader) {

        super.updateEntity(tblTdsHeader);
        
    }

    @Override
    public List<TblTdsHeader> getAllTblTdsHeader() {
        return super.getAllEntity();
    }

    @Override
    public long getTblTdsHeaderCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTdsHeader> findByCountTblTdsHeader(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
