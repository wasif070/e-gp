package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderMegaHash;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderMegaHashDao extends GenericDao<TblTenderMegaHash> {

    public void addTblTenderMegaHash(TblTenderMegaHash tblTenderMegaHash);

    public void deleteTblTenderMegaHash(TblTenderMegaHash tblTenderMegaHash);

    public void deleteAllTblTenderMegaHash(Collection tblTenderMegaHash);

    public void updateTblTenderMegaHash(TblTenderMegaHash tblTenderMegaHash);

    public List<TblTenderMegaHash> getAllTblTenderMegaHash();

    public List<TblTenderMegaHash> findTblTenderMegaHash(Object... values) throws Exception;

    public List<TblTenderMegaHash> findByCountTblTenderMegaHash(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderMegaHashCount();
}
