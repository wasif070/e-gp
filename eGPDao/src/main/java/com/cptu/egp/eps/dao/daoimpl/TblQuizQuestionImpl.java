/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblQuizQuestionDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblQuizQuestion;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author feroz
 */
public class TblQuizQuestionImpl extends AbcAbstractClass<TblQuizQuestion> implements TblQuizQuestionDao{

    @Override
    public void addTblQuizQuestion(TblQuizQuestion tblQuizQuestion) {
        super.addEntity(tblQuizQuestion);
    }

    @Override
    public void deleteTblQuizQuestion(TblQuizQuestion tblQuizQuestion) {
        super.deleteEntity(tblQuizQuestion);
    }

    @Override
    public void updateTblQuizQuestion(TblQuizQuestion tblQuizQuestion) {
        super.updateEntity(tblQuizQuestion);
    }

    @Override
    public List<TblQuizQuestion> getAllTblQuizQuestion() {
        return super.getAllEntity();
    }

    @Override
    public List<TblQuizQuestion> findTblQuizQuestion(Object... values) {
        List<TblQuizQuestion> tblQuizQuestion = null;
        try {
            tblQuizQuestion = super.findEntity(values);
        } catch (Exception ex) {
            Logger.getLogger(TblQuizQuestionImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tblQuizQuestion;
    }
    
}
