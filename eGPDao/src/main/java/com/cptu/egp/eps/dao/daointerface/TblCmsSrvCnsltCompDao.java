package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvCnsltComp;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCmsSrvCnsltCompDao extends GenericDao<TblCmsSrvCnsltComp> {

    public void addTblCmsSrvCnsltComp(TblCmsSrvCnsltComp tblObj);

    public void deleteTblCmsSrvCnsltComp(TblCmsSrvCnsltComp tblObj);

    public void updateTblCmsSrvCnsltComp(TblCmsSrvCnsltComp tblObj);

    public List<TblCmsSrvCnsltComp> getAllTblCmsSrvCnsltComp();

    public List<TblCmsSrvCnsltComp> findTblCmsSrvCnsltComp(Object... values) throws Exception;

    public List<TblCmsSrvCnsltComp> findByCountTblCmsSrvCnsltComp(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsSrvCnsltCompCount();

    public void updateOrSaveEstCost(List<TblCmsSrvCnsltComp> estCost);
}
