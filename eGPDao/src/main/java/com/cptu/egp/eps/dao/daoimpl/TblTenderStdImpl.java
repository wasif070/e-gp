/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderStdDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderStd;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderStdImpl extends AbcAbstractClass<TblTenderStd> implements TblTenderStdDao {

    @Override
    public void addTblTenderStd(TblTenderStd tenderStd) {
        super.addEntity(tenderStd);
    }

    @Override
    public void deleteTblTenderStd(TblTenderStd tenderStd) {
        super.deleteEntity(tenderStd);
    }

    @Override
    public void updateTblTenderStd(TblTenderStd tenderStd) {
        super.updateEntity(tenderStd);
    }

    @Override
    public List<TblTenderStd> getAllTblTenderStd() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderStd> findTblTenderStd(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderStdCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderStd> findByCountTblTenderStd(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
