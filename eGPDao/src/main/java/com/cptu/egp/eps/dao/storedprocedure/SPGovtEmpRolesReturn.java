/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

/**
 *
 * @author parag
 */
public class SPGovtEmpRolesReturn {

    private int employeeId;
    private String employeeName;
    private int designationId;
    private String designationName;
    private int  userId;
    private String finPowerBy;

    private String employeeRoleId;
    private String procurementRoleId;
    private int departmentId;

    private String departmentname;
    private String procurementRole;

    private String officeName;
    private String officeId;

    public String getDesignationName() {
        return designationName;
    }

    public void setDesignationName(String designationName) {
        this.designationName = designationName;
    }

    

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }
    

    public String getDepartmentname() {
        return departmentname;
    }

    public void setDepartmentname(String departmentname) {
        this.departmentname = departmentname;
    }

    public int getDesignationId() {
        return designationId;
    }

    public void setDesignationId(int designationId) {
        this.designationId = designationId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeRoleId() {
        return employeeRoleId;
    }

    public void setEmployeeRoleId(String employeeRoleId) {
        this.employeeRoleId = employeeRoleId;
    }
    
    public String getFinPowerBy() {
        return finPowerBy;
    }

    public void setFinPowerBy(String finPowerBy) {
        this.finPowerBy = finPowerBy;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    public String getProcurementRole() {
        return procurementRole;
    }

    public void setProcurementRole(String procurementRole) {
        this.procurementRole = procurementRole;
    }

    public String getProcurementRoleId() {
        return procurementRoleId;
    }

    public void setProcurementRoleId(String procurementRoleId) {
        this.procurementRoleId = procurementRoleId;
    }

    

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    


}
