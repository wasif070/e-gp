package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this late, choose Tools | lates
 * and open the late in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblMessageInBox;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblMessageInBoxDao extends GenericDao<TblMessageInBox> {

    public void addTblMessageInBox(TblMessageInBox tblObj);

    public void deleteTblMessageInBox(TblMessageInBox tblObj);

    public void updateTblMessageInBox(TblMessageInBox tblObj);

    public List<TblMessageInBox> getAllTblMessageInBox();

    public List<TblMessageInBox> findTblMessageInBox(Object... values) throws Exception;

    public List<TblMessageInBox> findByCountTblMessageInBox(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblMessageInBoxCount();
}
