package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvPaymentSchDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsSrvPaymentSchImpl extends AbcAbstractClass<TblCmsSrvPaymentSch> implements TblCmsSrvPaymentSchDao {

    @Override
    public void addTblCmsSrvPaymentSch(TblCmsSrvPaymentSch cmsWpDetail){
        super.addEntity(cmsWpDetail);
    }

    @Override
    public void deleteTblCmsSrvPaymentSch(TblCmsSrvPaymentSch cmsWpDetail) {
        super.deleteEntity(cmsWpDetail);
    }

    @Override
    public void updateTblCmsSrvPaymentSch(TblCmsSrvPaymentSch cmsWpDetail) {
        super.updateEntity(cmsWpDetail);
    }

    @Override
    public List<TblCmsSrvPaymentSch> getAllTblCmsSrvPaymentSch() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvPaymentSch> findTblCmsSrvPaymentSch(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvPaymentSchCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvPaymentSch> findByCountTblCmsSrvPaymentSch(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsSrvPaymentSch> estCost) {
       super.updateAll(estCost);
    }
}
