/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.model.table.TblDebarmentReq;
import java.util.List;

/**
 *
 * @author test
 */
public interface TblDebarmentReqDao {

    public void addTblDebarmentReq(TblDebarmentReq tblDebarmentReq);

    public void deleteTblDebarmentReq(TblDebarmentReq tblDebarmentReq);

    public void updateTblDebarmentReq(TblDebarmentReq tblDebarmentReq);

    public List<TblDebarmentReq> getAllTblDebarmentReq();

    public List<TblDebarmentReq> findTblDebarmentReq(Object... values) throws Exception;

    public List<TblDebarmentReq> findByCountTblDebarmentReq(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblDebarmentReq();
}
