package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblNegotiation;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblNegotiationDao extends GenericDao<TblNegotiation> {

    public void addTblNegotiation(TblNegotiation tblNegotiation);

    public void deleteTblNegotiation(TblNegotiation tblNegotiation);

    public void deleteAllTblNegotiation(Collection tblNegotiation);

    public void updateTblNegotiation(TblNegotiation tblNegotiation);

    public List<TblNegotiation> getAllTblNegotiation();

    public List<TblNegotiation> findTblNegotiation(Object... values) throws Exception;

    public List<TblNegotiation> findByCountTblNegotiation(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblNegotiationCount();
}
