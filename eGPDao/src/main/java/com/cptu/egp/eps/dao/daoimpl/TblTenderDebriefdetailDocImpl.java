package com.cptu.egp.eps.dao.daoimpl;


import com.cptu.egp.eps.dao.daointerface.TblTenderDebriefdetailDocDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderDebriefdetailDoc;
import java.util.List;


public class TblTenderDebriefdetailDocImpl extends AbcAbstractClass<TblTenderDebriefdetailDoc> implements TblTenderDebriefdetailDocDao {

	@Override 
	public void addTblTenderDebriefdetailDoc(TblTenderDebriefdetailDoc master){
		super.addEntity(master);
	}
	@Override 
	public void deleteTblTenderDebriefdetailDoc(TblTenderDebriefdetailDoc master) {
		super.deleteEntity(master);
	}
	@Override 
	public void updateTblTenderDebriefdetailDoc(TblTenderDebriefdetailDoc master) {
		super.updateEntity(master);
	}
	@Override 
	public List<TblTenderDebriefdetailDoc> getAllTblTenderDebriefdetailDoc() {
		return super.getAllEntity();
	}
	@Override 
	public List<TblTenderDebriefdetailDoc> findTblTenderDebriefdetailDoc(Object... values) throws Exception {
		return super.findEntity(values);
	}
	@Override 
	public long getTblTenderDebriefdetailDocCount() {
		return super.getEntityCount();
	}
	@Override 
	public List<TblTenderDebriefdetailDoc> findByCountTblTenderDebriefdetailDoc(int firstResult, int maxResult, Object... values) throws Exception {
		return super.findByCountEntity(firstResult, maxResult, values);
	}
	@Override 
	public void updateOrSaveTblTenderDebriefdetailDoc(List<TblTenderDebriefdetailDoc> tblObj) {
		super.updateAll(tblObj);
	}
}