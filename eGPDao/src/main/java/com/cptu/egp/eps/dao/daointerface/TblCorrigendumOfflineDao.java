/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import java.util.List;
import com.cptu.egp.eps.model.table.TblCorrigendumDetailOffline;
import com.cptu.egp.eps.dao.generic.GenericDao;

/**
 *
 * @author salahuddin
 */
public interface TblCorrigendumOfflineDao extends GenericDao<TblCorrigendumDetailOffline>{

    public void addTblCorrigendumOfflineDetail(List<TblCorrigendumDetailOffline> tblCorrigendumDetailOffline);

    public List<TblCorrigendumDetailOffline> findTblCorrigendumDetailOffline(Object... values) throws Exception;

}
