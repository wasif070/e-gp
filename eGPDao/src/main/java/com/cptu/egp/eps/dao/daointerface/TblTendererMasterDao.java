package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTendererMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblTendererMasterDao extends GenericDao<TblTendererMaster> {

    public void addTblTendererMaster(TblTendererMaster tblObj);

    public void deleteTblTendererMaster(TblTendererMaster tblObj);

    public void updateTblTendererMaster(TblTendererMaster tblObj);

    public List<TblTendererMaster> getAllTblTendererMaster();

    public List<TblTendererMaster> findTblTendererMaster(Object... values) throws Exception;

    public List<TblTendererMaster> findByCountTblTendererMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTendererMasterCount();
}
