/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblWorkFlowRoleMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblWorkFlowRoleMaster;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public class TblWorkFlowRoleMasterImpl extends AbcAbstractClass<TblWorkFlowRoleMaster> implements TblWorkFlowRoleMasterDao {

    @Override
    public void addTblWorkFlowRoleMaster(TblWorkFlowRoleMaster workFlowRoleMaster) {

        super.addEntity(workFlowRoleMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblWorkFlowRoleMaster(TblWorkFlowRoleMaster workFlowRoleMaster) {

        super.deleteEntity(workFlowRoleMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblWorkFlowRoleMaster(TblWorkFlowRoleMaster workFlowRoleMaster) {

        super.updateEntity(workFlowRoleMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblWorkFlowRoleMaster> getAllTblWorkFlowRoleMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblWorkFlowRoleMaster> findTblWorkFlowRoleMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblWorkFlowRoleMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblWorkFlowRoleMaster> findByCountTblWorkFlowRoleMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
