package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalTerreport;
import java.util.List;

/**
 *
 * @author rishita
 */
public interface TblEvalTerreportDao extends GenericDao<TblEvalTerreport> {

    public void addTblEvalTerreport(TblEvalTerreport tblEvalTerreport);

    public void deleteTblEvalTerreport(TblEvalTerreport tblEvalTerreport);

    public void updateTblEvalTerreport(TblEvalTerreport tblEvalTerreport);

    public List<TblEvalTerreport> getAllTblEvalTerreport();

    public List<TblEvalTerreport> findTblEvalTerreport(Object... values) throws Exception;

    public List<TblEvalTerreport> findByCountTblEvalTerreport(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalTerreportCount();

    public void updateInsAllEvalTerreport(List<TblEvalTerreport> tblEvalTerreport);
}
