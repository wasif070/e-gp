/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderValAcceptance;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderValAcceptanceDao extends GenericDao<TblTenderValAcceptance>{

    public void addTblTenderValAcceptance(TblTenderValAcceptance tblObj);

    public void deleteTblTenderValAcceptance(TblTenderValAcceptance tblObj);

    public void updateTblTenderValAcceptance(TblTenderValAcceptance tblObj);

    public List<TblTenderValAcceptance> getAllTblTenderValAcceptance();

    public List<TblTenderValAcceptance> findTblTenderValAcceptance(Object... values) throws Exception;

    public List<TblTenderValAcceptance> findByCountTblTenderValAcceptance(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderValAcceptanceCount();
}
