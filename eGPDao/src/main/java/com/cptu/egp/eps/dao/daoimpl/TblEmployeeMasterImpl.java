/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblEmployeeMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEmployeeMaster;
import java.util.List;

/**
 * <b>Class Description goes here</b>
 * $Revision: 1.1.1.1 $
 * @version <version-no.> <Date>
 * @author Administrator
 */
public class TblEmployeeMasterImpl extends AbcAbstractClass<TblEmployeeMaster> implements TblEmployeeMasterDao {

    @Override
    public void addEmployee(TblEmployeeMaster tblEmployeeMaster)
    {
        super.addEntity(tblEmployeeMaster);
    }

    @Override
    public void deleteEmployee(TblEmployeeMaster tblEmployeeMaster)
    {
        super.deleteEntity(tblEmployeeMaster);
    }

    @Override
    public void updateEmployee(TblEmployeeMaster tblEmployeeMaster)
    {
        super.updateEntity(tblEmployeeMaster);
    }

    @Override
    public List<TblEmployeeMaster> getAllEmployees()
    {
        return super.getAllEntity();
    }

    @Override
    public List<TblEmployeeMaster> findEmployee(Object... values) throws Exception
    {
        return super.findEntity(values);
    }

    @Override
    public List<TblEmployeeMaster> findByCountEployee(int firstResult,
            int maxResult, Object... values) throws Exception
    {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getEmployeeCount()
    {
        return super.getEntityCount();
    }

    


}
