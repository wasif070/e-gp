package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblMessageInBoxDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblMessageInBox;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblMessageInBoxImpl extends AbcAbstractClass<TblMessageInBox> implements TblMessageInBoxDao {

    @Override
    public void addTblMessageInBox(TblMessageInBox master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblMessageInBox(TblMessageInBox master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblMessageInBox(TblMessageInBox master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblMessageInBox> getAllTblMessageInBox() {
        return super.getAllEntity();
    }

    @Override
    public List<TblMessageInBox> findTblMessageInBox(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblMessageInBoxCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblMessageInBox> findByCountTblMessageInBox(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
