package com.cptu.egp.eps.dao.daoimpl;


import com.cptu.egp.eps.dao.daointerface.TblGovUserAuditDetailsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblGovUserAuditDetails;
import java.util.List;


public class TblGovUserAuditDetailsImpl extends AbcAbstractClass<TblGovUserAuditDetails> implements TblGovUserAuditDetailsDao {

	@Override 
	public void addTblGovUserAuditDetails(TblGovUserAuditDetails master){
		super.addEntity(master);
	}
	@Override 
	public void deleteTblGovUserAuditDetails(TblGovUserAuditDetails master) {
		super.deleteEntity(master);
	}
	@Override 
	public void updateTblGovUserAuditDetails(TblGovUserAuditDetails master) {
		super.updateEntity(master);
	}
	@Override 
	public List<TblGovUserAuditDetails> getAllTblGovUserAuditDetails() {
		return super.getAllEntity();
	}
	@Override 
	public List<TblGovUserAuditDetails> findTblGovUserAuditDetails(Object... values) throws Exception {
		return super.findEntity(values);
	}
	@Override 
	public long getTblGovUserAuditDetailsCount() {
		return super.getEntityCount();
	}
	@Override 
	public List<TblGovUserAuditDetails> findByCountTblGovUserAuditDetails(int firstResult, int maxResult, Object... values) throws Exception {
		return super.findByCountEntity(firstResult, maxResult, values);
	}
	@Override 
	public void updateOrSaveTblGovUserAuditDetails(List<TblGovUserAuditDetails> tblObj) {
		super.updateAll(tblObj);
	}
}