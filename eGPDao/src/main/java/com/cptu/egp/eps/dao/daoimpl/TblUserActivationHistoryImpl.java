package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblUserActivationHistoryDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblUserActivationHistory;
import java.util.List;

/**
 *
 * @author rishita
 */
public class TblUserActivationHistoryImpl extends AbcAbstractClass<TblUserActivationHistory> implements TblUserActivationHistoryDao {

    @Override
    public void addTblUserActivationHistory(TblUserActivationHistory userActivationHistory){
        super.addEntity(userActivationHistory);
    }

    @Override
    public void deleteTblUserActivationHistory(TblUserActivationHistory userActivationHistory) {
        super.deleteEntity(userActivationHistory);
    }

    @Override
    public void updateTblUserActivationHistory(TblUserActivationHistory userActivationHistory) {
        super.updateEntity(userActivationHistory);
    }

    @Override
    public List<TblUserActivationHistory> getAllTblUserActivationHistory() {
        return super.getAllEntity();
    }

    @Override
    public List<TblUserActivationHistory> findTblUserActivationHistory(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblUserActivationHistoryCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblUserActivationHistory> findByCountTblUserActivationHistory(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
