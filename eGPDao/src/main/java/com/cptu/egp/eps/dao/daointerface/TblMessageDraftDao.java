package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this late, choose Tools | lates
 * and open the late in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblMessageDraft;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblMessageDraftDao extends GenericDao<TblMessageDraft> {

    public void addTblMessageDraft(TblMessageDraft tblObj);

    public void deleteTblMessageDraft(TblMessageDraft tblObj);

    public void updateTblMessageDraft(TblMessageDraft tblObj);

    public List<TblMessageDraft> getAllTblMessageDraft();

    public List<TblMessageDraft> findTblMessageDraft(Object... values) throws Exception;

    public List<TblMessageDraft> findByCountTblMessageDraft(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblMessageDraftCount();
}
