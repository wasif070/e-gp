package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblAppWatchListDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblAppWatchList;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblAppWatchListImpl extends AbcAbstractClass<TblAppWatchList> implements TblAppWatchListDao {

    @Override
    public void addTblAppWatchList(TblAppWatchList tblAppWatchList){
        super.addEntity(tblAppWatchList);
    }

    @Override
    public void deleteTblAppWatchList(TblAppWatchList tblAppWatchList) {
        super.deleteEntity(tblAppWatchList);
    }

    @Override
    public void updateTblAppWatchList(TblAppWatchList tblAppWatchList) {
        super.updateEntity(tblAppWatchList);
    }

    @Override
    public List<TblAppWatchList> getAllTblAppWatchList() {
        return super.getAllEntity();
    }

    @Override
    public List<TblAppWatchList> findTblAppWatchList(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblAppWatchListCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblAppWatchList> findByCountTblAppWatchList(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
