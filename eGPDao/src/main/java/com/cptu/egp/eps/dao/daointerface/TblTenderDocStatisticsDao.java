/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderDocStatistics;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TblTenderDocStatisticsDao extends GenericDao<TblTenderDocStatistics> {

    public void addTblTenderDocStatistics(TblTenderDocStatistics tenderDocStatistics);

    public void deleteTblTenderDocStatistics(TblTenderDocStatistics tenderDocStatistics);

    public void updateTblTenderDocStatistics(TblTenderDocStatistics tenderDocStatistics);

    public List<TblTenderDocStatistics> getAllTblTenderDocStatistics();

    public List<TblTenderDocStatistics> findTblTenderDocStatistics(Object... values) throws Exception;

    public List<TblTenderDocStatistics> findByCountTblTenderDocStatistics(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderDocStatisticsCount();
}
