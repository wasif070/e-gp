/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblWorkFlowFileOnHand;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public interface TblWorkFlowFileOnHandDao extends GenericDao<TblWorkFlowFileOnHand> {

    public void addTblWorkFlowFileOnHand(TblWorkFlowFileOnHand admin);

    public void deleteTblWorkFlowFileOnHand(TblWorkFlowFileOnHand admin);

    public void updateTblWorkFlowFileOnHand(TblWorkFlowFileOnHand admin);

    public List<TblWorkFlowFileOnHand> getAllTblWorkFlowFileOnHand();

    public List<TblWorkFlowFileOnHand> findTblWorkFlowFileOnHand(Object... values) throws Exception;

    public List<TblWorkFlowFileOnHand> findByCountTblWorkFlowFileOnHand(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblWorkFlowFileOnHandCount();
}
