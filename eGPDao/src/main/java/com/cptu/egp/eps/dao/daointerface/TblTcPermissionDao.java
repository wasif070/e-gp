/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;
import com.cptu.egp.eps.dao.generic.GenericDao;
import java.util.List;
import com.cptu.egp.eps.model.table.TblTcpermission;
/**
 *
 * @author Dohatec
 */
public interface TblTcPermissionDao extends GenericDao<TblTcpermission>{
     public void addTblTcpermission(TblTcpermission tblObj);

    public void deleteTblTcpermission(TblTcpermission tblObj);

    public void updateTblTcpermission(TblTcpermission tblObj);

    public List<TblTcpermission> getAllTblTcpermission();

    public List<TblTcpermission> findTblTcpermission(Object... values) throws Exception;

    public List<TblTcpermission> findByCountTblTcpermission(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblTcpermissionCount();

    public void updateOrSaveTblTcpermission(List<TblTcpermission> tblObj);
    
}
