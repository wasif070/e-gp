package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalTscDocs;
import java.util.List;

/**
 *
 * @author rishita
 */
public interface TblEvalTscDocsDao extends GenericDao<TblEvalTscDocs> {

    public void addTblEvalTscDocs(TblEvalTscDocs tblEvalTscDocs);

    public void deleteTblEvalTscDocs(TblEvalTscDocs tblEvalTscDocs);

    public void updateTblEvalTscDocs(TblEvalTscDocs tblEvalTscDocs);

    public List<TblEvalTscDocs> getAllTblEvalTscDocs();

    public List<TblEvalTscDocs> findTblEvalTscDocs(Object... values) throws Exception;

    public List<TblEvalTscDocs> findByCountTblEvalTscDocs(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalTscDocsCount();
}
