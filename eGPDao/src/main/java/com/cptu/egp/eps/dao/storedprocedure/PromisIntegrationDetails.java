/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

/**
 * This class is used to know the details of Promis Integration
 * This class is created for web-service(PromisIntegration)
 * @author Sreenu
 */
public class PromisIntegrationDetails {

    private int peId;
    private String peOfficeName;
    private String deptType;
    private String ministryName;
    private String divisionName;
    private String organizationName;
    private String districtName;

    public int getPEId() {
        return peId;
    }

    public void setPEId(int peId) {
        this.peId = peId;
    }

    public String getPEOfficeName() {
        return peOfficeName;
    }

    public void setPEOfficeName(String peOfficeName) {
        this.peOfficeName = peOfficeName;
    }

    public String getDeptType() {
        return deptType;
    }

    public void setDeptType(String deptType) {
        this.deptType = deptType;
    }

    public String getMinistryName() {
        return ministryName;
    }

    public void setMinistryName(String ministryName) {
        this.ministryName = ministryName;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }
}
