/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblPreTenderReply;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblPreTenderReplyDao extends GenericDao<TblPreTenderReply>{

    public void addTblPreTenderReply(TblPreTenderReply tblObj);

    public void deleteTblPreTenderReply(TblPreTenderReply tblObj);

    public void updateTblPreTenderReply(TblPreTenderReply tblObj);

    public List<TblPreTenderReply> getAllTblPreTenderReply();

    public List<TblPreTenderReply> findTblPreTenderReply(Object... values) throws Exception;

    public List<TblPreTenderReply> findByCountTblPreTenderReply(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblPreTenderReplyCount();
}
