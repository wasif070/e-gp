/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblPreTenderAuditTrailDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblPreTenderAuditTrail;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblPreTenderAuditTrailImpl extends AbcAbstractClass<TblPreTenderAuditTrail> implements TblPreTenderAuditTrailDao{

    @Override
    public void addTblPreTenderAuditTrail(TblPreTenderAuditTrail tblPreTenderAuditTrail) {
        super.addEntity(tblPreTenderAuditTrail);
    }

    @Override
    public List<TblPreTenderAuditTrail> findTblPreTenderAuditTrail(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblPreTenderAuditTrail(TblPreTenderAuditTrail tblPreTenderAuditTrail) {

        super.deleteEntity(tblPreTenderAuditTrail);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblPreTenderAuditTrail(Collection tblPreTenderAuditTrail) {
        super.deleteAll(tblPreTenderAuditTrail);
    }

    @Override
    public void updateTblPreTenderAuditTrail(TblPreTenderAuditTrail tblPreTenderAuditTrail) {

        super.updateEntity(tblPreTenderAuditTrail);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblPreTenderAuditTrail> getAllTblPreTenderAuditTrail() {
        return super.getAllEntity();
    }

    @Override
    public long getTblPreTenderAuditTrailCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblPreTenderAuditTrail> findByCountTblPreTenderAuditTrail(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
