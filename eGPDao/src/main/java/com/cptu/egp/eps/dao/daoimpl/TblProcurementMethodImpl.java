package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblProcurementMethodDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblProcurementMethod;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblProcurementMethodImpl extends AbcAbstractClass<TblProcurementMethod> implements TblProcurementMethodDao {

    @Override
    public void addTblProcurementMethod(TblProcurementMethod master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblProcurementMethod(TblProcurementMethod master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblProcurementMethod(TblProcurementMethod master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblProcurementMethod> getAllTblProcurementMethod() {
        return super.getAllEntity();
    }

    @Override
    public List<TblProcurementMethod> findTblProcurementMethod(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblProcurementMethodCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblProcurementMethod> findByCountTblProcurementMethod(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
