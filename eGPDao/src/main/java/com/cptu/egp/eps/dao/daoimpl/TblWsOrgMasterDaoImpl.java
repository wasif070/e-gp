/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblWsOrgMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblWsOrgMaster;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Sreenu
 */
public class TblWsOrgMasterDaoImpl extends AbcAbstractClass<TblWsOrgMaster> implements TblWsOrgMasterDao {

    @Override
    public void addTblWsOrgMaster(TblWsOrgMaster tblWsOrgMaster) {
        super.addEntity(tblWsOrgMaster);
    }

    @Override
    public void deleteTblWsOrgMaster(TblWsOrgMaster tblWsOrgMaster) {
        super.deleteEntity(tblWsOrgMaster);
    }

    @Override
    public void updateTblWsOrgMaster(TblWsOrgMaster tblWsOrgMaster) {
        super.updateEntity(tblWsOrgMaster);
    }

    @Override
    public List<TblWsOrgMaster> getAllTblWsOrgMaster() {
        super.setPersistentClass(TblWsOrgMaster.class);
        return super.getAllEntity();
    }

    @Override
    public List<TblWsOrgMaster> findTblWsOrgMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public List<TblWsOrgMaster> findByCountTblWsOrgMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getCountTblWsOrgMaster() {
        return super.getEntityCount();
    }

    @Override
    public List<TblWsOrgMaster> getRequiredTblWsOrgMasterList(String whereCondition) {
        List<TblWsOrgMaster> wsOrgMastersList = new ArrayList<TblWsOrgMaster>();
        List<Object[]> wsOrgMastersObjectList = null;
        StringBuffer queryBuffer = new StringBuffer();
        String query = "SELECT "
                + "wsOrgMaster.orgId, "
                + "wsOrgMaster.emailId, "
                + "wsOrgMaster.loginPass, "
                + "wsOrgMaster.orgNameEn, "
                + "wsOrgMaster.orgNameBg, "
                + "wsOrgMaster.orgDetail, "
                + "wsOrgMaster.contactPerson, "
                + "wsOrgMaster.mobileNo, "
                + "wsOrgMaster.wsRights, "
                + "wsOrgMaster.createdBy, "
                + "wsOrgMaster.createdDate, "
                + "wsOrgMaster.isDeleted "
                + "FROM TblWsOrgMaster wsOrgMaster ";
        //String Query1 = "SELECT * FROM TblWsOrgMaster";
        queryBuffer.append(query);
        queryBuffer.append(whereCondition);

        wsOrgMastersObjectList = super.createQuery(queryBuffer.toString());
        if (wsOrgMastersObjectList != null && !wsOrgMastersObjectList.isEmpty()) {
            for (int i = 0; i < wsOrgMastersObjectList.size(); i++) {
                Object[] tempObjectArray = wsOrgMastersObjectList.get(i);
                TblWsOrgMaster tempTblWsOrgMaster = new TblWsOrgMaster();
                tempTblWsOrgMaster.setOrgId((Integer) tempObjectArray[0]);
                tempTblWsOrgMaster.setEmailId((String) tempObjectArray[1]);
                tempTblWsOrgMaster.setLoginPass((String) tempObjectArray[2]);
                tempTblWsOrgMaster.setOrgNameEn((String) tempObjectArray[3]);
                tempTblWsOrgMaster.setOrgNameBg((String) tempObjectArray[4]);
                tempTblWsOrgMaster.setOrgDetail((String) tempObjectArray[5]);
                tempTblWsOrgMaster.setContactPerson((String) tempObjectArray[6]);
                tempTblWsOrgMaster.setMobileNo((String) tempObjectArray[7]);
                tempTblWsOrgMaster.setWsRights((String) tempObjectArray[8]);
                tempTblWsOrgMaster.setCreatedBy((Integer) tempObjectArray[9]);
                tempTblWsOrgMaster.setCreatedDate((Date) tempObjectArray[10]);
                tempTblWsOrgMaster.setIsDeleted((String) tempObjectArray[11]);
                wsOrgMastersList.add(tempTblWsOrgMaster);
            }
        }
        return wsOrgMastersList;
    }
}
