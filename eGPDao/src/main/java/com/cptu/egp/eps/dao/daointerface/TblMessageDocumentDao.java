package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblMessageDocument;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblMessageDocumentDao extends GenericDao<TblMessageDocument> {

    public void addTblMessageDocument(TblMessageDocument tblObj);

    public void deleteTblMessageDocument(TblMessageDocument tblObj);

    public void updateTblMessageDocument(TblMessageDocument tblObj);

    public List<TblMessageDocument> getAllTblMessageDocument();

    public List<TblMessageDocument> findTblMessageDocument(Object... values) throws Exception;

    public List<TblMessageDocument> findByCountTblMessageDocument(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblMessageDocumentCount();
}
