/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Administrator
 */
public class SPSearchAppReport extends StoredProcedure{
    private static final Logger LOGGER = Logger.getLogger(SPSearchAppReport.class);
    public SPSearchAppReport(BasicDataSource dataSource, String procName){
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        
        this.declareParameter(new SqlParameter("v_Action_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_DepartmentId_insInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_OfficeId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_FinancialYear_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_BudgetType_intInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_ProjectId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_ProcurementNature_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_PackageId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_AppId_inInt", Types.INTEGER));
        
    }

    /**
     * Execute stored procedure : p_search_appreports
     * Get App Package Report details.
     * @param action
     * @param deptId
     * @param officeId
     * @param financialYear
     * @param budgetType
     * @param prjId
     * @param prcNature
     * @param packageId
     * @param appId
     * @return App Package details as List of CommonAppPkgDetails.
     */
     public List<CommonAppPkgDetails> executeProcedure(String action,int deptId,int officeId,String financialYear,int budgetType,int prjId,String prcNature,int packageId,int appId){
        Map inParams = new HashMap();

        inParams.put("v_Action_inVc", action);
        inParams.put("v_DepartmentId_insInt", deptId);
        inParams.put("v_OfficeId_inInt", officeId);
        inParams.put("v_FinancialYear_inVc", financialYear);
        inParams.put("v_BudgetType_intInt", budgetType);
        inParams.put("v_ProjectId_inInt", prjId);
        inParams.put("v_ProcurementNature_inVc", prcNature);
        inParams.put("v_PackageId_inInt", packageId);
        inParams.put("v_AppId_inInt", appId);
        
        
        this.compile();
        List<CommonAppPkgDetails> details = new ArrayList<CommonAppPkgDetails>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            if(!list.isEmpty())
            {
                for (LinkedHashMap<String, Object> linkedHashMap : list)
                {
                    CommonAppPkgDetails cmmAppPkgDetails=new CommonAppPkgDetails();
                    
                    if("getLotInfo".equalsIgnoreCase(action))
                    {
                        cmmAppPkgDetails.setLotNo((String)linkedHashMap.get("lotNo"));
                        cmmAppPkgDetails.setLotDesc((String)linkedHashMap.get("lotDesc"));
                        cmmAppPkgDetails.setQuantity((BigDecimal)linkedHashMap.get("quantity"));
                        cmmAppPkgDetails.setUnit((String)linkedHashMap.get("unit"));
                    }
                    else if("getTopInfo".equalsIgnoreCase(action))
                    {
                        cmmAppPkgDetails.setMinistry((String)linkedHashMap.get("Ministry"));
                        cmmAppPkgDetails.setDivision((String)linkedHashMap.get("Division"));
                        cmmAppPkgDetails.setAgency((String)linkedHashMap.get("Agency"));
                        cmmAppPkgDetails.setPeCode((String)linkedHashMap.get("PECode"));
                        cmmAppPkgDetails.setOfficeName((String)linkedHashMap.get("OfficeName"));
                        cmmAppPkgDetails.setProjectName((String)linkedHashMap.get("ProjectName"));
                        cmmAppPkgDetails.setProjectCode((String)linkedHashMap.get("ProjectCode"));
                    }
                    else if("searchData".equalsIgnoreCase(action))
                    {
                        cmmAppPkgDetails.setAppId((Integer)linkedHashMap.get("appId"));
                        cmmAppPkgDetails.setProjectNameCode((String)linkedHashMap.get("projectnamecode"));
                        cmmAppPkgDetails.setMinistryDetails((String)linkedHashMap.get("MinistryDetails"));
                        cmmAppPkgDetails.setPackageId((Integer)linkedHashMap.get("packageId"));
                        cmmAppPkgDetails.setPackageNo((String)linkedHashMap.get("packageNo"));
                        cmmAppPkgDetails.setProcurementMethod((String)linkedHashMap.get("procurementMethod"));
                        cmmAppPkgDetails.setProcurementType((String)linkedHashMap.get("procurementType"));
                        cmmAppPkgDetails.setProcurementRole((String)linkedHashMap.get("procurementRole"));

                        cmmAppPkgDetails.setSourceOfFund((String)linkedHashMap.get("sourceOfFund"));
                        cmmAppPkgDetails.setBudgetType((String)linkedHashMap.get("BudgetType"));
                        cmmAppPkgDetails.setPackageDesc((String)linkedHashMap.get("packageDesc"));
                        cmmAppPkgDetails.setProcurementNature((String)linkedHashMap.get("procurementnature"));
                        cmmAppPkgDetails.setEstimatedCost((BigDecimal)linkedHashMap.get("estimatedCost"));

                        cmmAppPkgDetails.setAdvtDt((String)linkedHashMap.get("advtDt"));
                        cmmAppPkgDetails.setSubDt((String)linkedHashMap.get("subDt"));
                        cmmAppPkgDetails.setTenderAdvertDt((String)linkedHashMap.get("tenderAdvertDt"));
                        cmmAppPkgDetails.setTenderSubDt((String)linkedHashMap.get("tenderSubDt"));
                        cmmAppPkgDetails.setTenderOpenDt((String)linkedHashMap.get("tenderOpenDt"));
                        //Code by Proshanto Kumar Saha
                        cmmAppPkgDetails.setTenderLetterIntentDt((String)linkedHashMap.get("tenderLetterIntentDt"));
                       //End
                        cmmAppPkgDetails.setTenderNoaIssueDt((String)linkedHashMap.get("tenderNoaIssueDt"));
                        cmmAppPkgDetails.setTenderContractSignDt((String)linkedHashMap.get("tenderContractSignDt"));
                        cmmAppPkgDetails.setTenderContractCompDt((String)linkedHashMap.get("tenderContractCompDt"));
                        cmmAppPkgDetails.setTechSubCmtRptDt((String)linkedHashMap.get("techSubCmtRptDt"));
                        cmmAppPkgDetails.setTenderEvalRptDt((String)linkedHashMap.get("tenderEvalRptDt"));
                        cmmAppPkgDetails.setTenderEvalRptAppDt((String)linkedHashMap.get("tenderEvalRptAppDt"));
                        cmmAppPkgDetails.setTenderContractAppDt((String)linkedHashMap.get("tenderContractAppDt"));
                        cmmAppPkgDetails.setReoiReceiptDt((String)linkedHashMap.get("reoiReceiptDt"));
                        cmmAppPkgDetails.setRfpTechEvalDt((String)linkedHashMap.get("rfpTechEvalDt"));
                        cmmAppPkgDetails.setRfpFinancialOpenDt((String)linkedHashMap.get("rfpFinancialOpenDt"));
                        cmmAppPkgDetails.setRfpNegCompDt((String)linkedHashMap.get("rfpNegCompDt"));
                        cmmAppPkgDetails.setRfpContractAppDt((String)linkedHashMap.get("rfpContractAppDt"));
                        cmmAppPkgDetails.setRfaAdvertDt((String)linkedHashMap.get("rfaAdvertDt"));
                        cmmAppPkgDetails.setRfaReceiptDt((String)linkedHashMap.get("rfaReceiptDt"));
                        cmmAppPkgDetails.setRfaEvalDt((String)linkedHashMap.get("rfaEvalDt"));
                        cmmAppPkgDetails.setRfaInterviewDt((String)linkedHashMap.get("rfaInterviewDt"));
                        cmmAppPkgDetails.setRfaFinalSelDt((String)linkedHashMap.get("rfaFinalSelDt"));
                        cmmAppPkgDetails.setRfaEvalRptSubDt((String)linkedHashMap.get("rfaEvalRptSubDt"));
                        cmmAppPkgDetails.setRfaAppConsultantDt((String)linkedHashMap.get("rfaAppConsultantDt"));
                        cmmAppPkgDetails.setActSubDt((String)linkedHashMap.get("actSubDt"));
                        cmmAppPkgDetails.setActAdvtDt((String)linkedHashMap.get("actAdvtDt"));
                        cmmAppPkgDetails.setActEvalRptDt((String)linkedHashMap.get("actEvalRptDt"));
                        cmmAppPkgDetails.setActAppLstDt((String)linkedHashMap.get("actAppLstDt"));
                        cmmAppPkgDetails.setActTenderAdvertDt((String)linkedHashMap.get("actTenderAdvertDt"));
                        cmmAppPkgDetails.setActTenderSubDt((String)linkedHashMap.get("actTenderSubDt"));
                        cmmAppPkgDetails.setActTenderOpenDt((String)linkedHashMap.get("actTenderOpenDt"));
                        cmmAppPkgDetails.setTechSubCmtRptDt((String)linkedHashMap.get("actTechSubCmtRptDt"));
                        cmmAppPkgDetails.setActTenderEvalRptDt((String)linkedHashMap.get("actTenderEvalRptDt"));
                        cmmAppPkgDetails.setActtenderEvalRptAppDt((String)linkedHashMap.get("acttenderEvalRptAppDt"));
                        cmmAppPkgDetails.setActTenderContractAppDt((String)linkedHashMap.get("actTenderContractAppDt"));
                        cmmAppPkgDetails.setActTenderLetterIntentDt((String)linkedHashMap.get("actTenderLetterIntentDt"));  //nafiul
                        cmmAppPkgDetails.setActTenderNoaIssueDt((String)linkedHashMap.get("actTenderNoaIssueDt"));
                        cmmAppPkgDetails.setActTenderContractSignDt((String)linkedHashMap.get("actTenderContractSignDt"));
                        cmmAppPkgDetails.setActTenderContractCompDt((String)linkedHashMap.get("actTenderContractCompDt"));
                        cmmAppPkgDetails.setActReoiReceiptDt((String)linkedHashMap.get("actReoiReceiptDt"));
                        cmmAppPkgDetails.setActRfpTechEvalDt((String)linkedHashMap.get("actRfpTechEvalDt"));
                        cmmAppPkgDetails.setActRfpFinancialOpenDt((String)linkedHashMap.get("actRfpFinancialOpenDt"));
                        cmmAppPkgDetails.setActRfpNegComDt((String)linkedHashMap.get("actRfpNegComDt"));
                        cmmAppPkgDetails.setActRfpContractAppDt((String)linkedHashMap.get("actRfpContractAppDt"));
                        cmmAppPkgDetails.setActRfaAdvertDt((String)linkedHashMap.get("actRfaAdvertDt"));
                        cmmAppPkgDetails.setActRfaReceiptDt((String)linkedHashMap.get("actRfaReceiptDt"));
                        cmmAppPkgDetails.setActRfaEvalDt((String)linkedHashMap.get("actRfaEvalDt"));
                        cmmAppPkgDetails.setActRfaInterviewDt((String)linkedHashMap.get("actRfaInterviewDt"));
                        cmmAppPkgDetails.setActRfaFinalSelDt((String)linkedHashMap.get("actRfaFinalSelDt"));
                        cmmAppPkgDetails.setActRfaEvalRptSubDt((String)linkedHashMap.get("actRfaEvalRptSubDt"));
                        cmmAppPkgDetails.setActRfaAppConsultantDt((String)linkedHashMap.get("actRfaAppConsultantDt"));
                        cmmAppPkgDetails.setEvalRptDt((String)linkedHashMap.get("evalRptDt"));
                        cmmAppPkgDetails.setAppLstDt((String)linkedHashMap.get("appLstDt"));
                        cmmAppPkgDetails.setOpenDt((String)linkedHashMap.get("openDt"));
                        cmmAppPkgDetails.setIsPQRequired((String)linkedHashMap.get("isPQRequired"));
                        
                        cmmAppPkgDetails.setSubDays((Short)linkedHashMap.get("subDays"));
                        cmmAppPkgDetails.setAdvtDays((Short)linkedHashMap.get("advtDays"));
                        cmmAppPkgDetails.setOpenDays((Short)linkedHashMap.get("openDays"));
                        cmmAppPkgDetails.setEvalRptDays((Short)linkedHashMap.get("evalRptDays"));
                        cmmAppPkgDetails.setTenderAdvertDays((Short)linkedHashMap.get("tenderAdvertDays"));
                        cmmAppPkgDetails.setTenderSubDays((Short)linkedHashMap.get("tenderSubDays"));
                        cmmAppPkgDetails.setTenderOpenDays((Short)linkedHashMap.get("tenderOpenDays"));
                        cmmAppPkgDetails.setTechSubCmtRptDays((Short)linkedHashMap.get("techSubCmtRptDays"));
                        cmmAppPkgDetails.setTenderEvalRptdays((Short)linkedHashMap.get("tenderEvalRptdays"));
                        cmmAppPkgDetails.setTenderEvalRptAppDays((Short)linkedHashMap.get("tenderEvalRptAppDays"));
                        cmmAppPkgDetails.setTenderContractAppDays((Short)linkedHashMap.get("tenderContractAppDays"));
                        //Code by Proshanto Kumar Saha
                        cmmAppPkgDetails.setTnderLetterIntentDays((Short)linkedHashMap.get("tnderLetterIntentDays"));
                        //End
                        cmmAppPkgDetails.setTenderNoaIssueDays((Short)linkedHashMap.get("tenderNoaIssueDays"));
                        cmmAppPkgDetails.setTenderContractSignDays((Short)linkedHashMap.get("tenderContractSignDays"));
                        cmmAppPkgDetails.setRfaAdvertDays((Short)linkedHashMap.get("rfaAdvertDays"));
                        cmmAppPkgDetails.setRfaReceiptDays((Short)linkedHashMap.get("rfaReceiptDays"));
                        cmmAppPkgDetails.setRfaEvalDays((Short)linkedHashMap.get("rfaEvalDays"));
                        cmmAppPkgDetails.setRfaInterviewDays((Short)linkedHashMap.get("rfaInterviewDays"));
                        cmmAppPkgDetails.setRfaFinalSelDays((Short)linkedHashMap.get("rfaFinalSelDays"));
                        cmmAppPkgDetails.setRfaEvalRptSubDays((Short)linkedHashMap.get("rfaEvalRptSubDays"));
                        cmmAppPkgDetails.setRfpContractAppDays((Short)linkedHashMap.get("rfpContractAppDays"));
                        cmmAppPkgDetails.setRfpNegCompDays((Short)linkedHashMap.get("rfpNegCompDays"));
                        cmmAppPkgDetails.setRfpFinancialOpenDays((Short)linkedHashMap.get("rfpFinancialOpenDays"));
                        cmmAppPkgDetails.setRfpTechEvalDays((Short)linkedHashMap.get("rfpTechEvalDays"));
                        cmmAppPkgDetails.setReoiReceiptDays((Short)linkedHashMap.get("reoiReceiptDays"));
                        
                    }
                    details.add(cmmAppPkgDetails);
                }
            }else{
                LOGGER.debug("List is Empty");
            }
        } catch (Exception e) {
          LOGGER.error("SPSearchAppReport  : "+ e);
        }
        return details;
     }
}
