/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Administrator
 */
public class SPAppPkgDetails extends StoredProcedure
{
    private static final Logger LOGGER = Logger.getLogger(SPAppPkgDetails.class);
    public SPAppPkgDetails(BasicDataSource dataSource, String procName)
    {

        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("v_AppId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_PackageId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_PackageType_inVc", Types.VARCHAR));
    }

    /**
     * Execute stored procedure : p_get_app_packagedetails
     * To get app package details.
     * @param appId
     * @param pkgId
     * @param pkgType
     * @return List of CommonAppPkgDetails object.
     */
    public List<CommonAppPkgDetails> executeProcedure(int appId, int pkgId, String pkgType)
    {
        Map inParams = new HashMap();
        inParams.put("v_AppId_inInt", appId);
        inParams.put("v_PackageId_inInt", pkgId);
        inParams.put("v_PackageType_inVc", pkgType);
        this.compile();
        List<CommonAppPkgDetails> details = new ArrayList<CommonAppPkgDetails>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            if(!list.isEmpty()){
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonAppPkgDetails cmmAppPkgDetails=new CommonAppPkgDetails();
                cmmAppPkgDetails.setAppId((Integer)linkedHashMap.get("appId"));
                cmmAppPkgDetails.setAppCode((String)linkedHashMap.get("appCode"));
                cmmAppPkgDetails.setFinancialYear((String)linkedHashMap.get("financialYear"));
                cmmAppPkgDetails.setBudgetType((String)linkedHashMap.get("budgetType"));
                cmmAppPkgDetails.setProjectName((String) linkedHashMap.get("projectName"));
                cmmAppPkgDetails.setProcurementnature((String)linkedHashMap.get("procurementnature"));
                cmmAppPkgDetails.setServicesType((String)linkedHashMap.get("servicesType"));
                cmmAppPkgDetails.setPackageNo((String)linkedHashMap.get("packageNo"));
                cmmAppPkgDetails.setPackageDesc((String)linkedHashMap.get("packageDesc"));
             //aprojit-start
                cmmAppPkgDetails.setBidderCategory((String)linkedHashMap.get("bidderCategory"));
                cmmAppPkgDetails.setWorkCategory((String)linkedHashMap.get("workCategory"));
                cmmAppPkgDetails.setDepoplanWork((String)linkedHashMap.get("depoplanWork"));
                cmmAppPkgDetails.setEntrustingAgency((String)linkedHashMap.get("entrustingAgency"));
                cmmAppPkgDetails.setTimeFrame((String)linkedHashMap.get("timeFrame"));
             //aprojit-End
                cmmAppPkgDetails.setAllocateBudget((BigDecimal)linkedHashMap.get("allocateBudget"));
                cmmAppPkgDetails.setPkgEstCost((BigDecimal)linkedHashMap.get("pkgEstCost"));
                cmmAppPkgDetails.setIsPQRequired((String)linkedHashMap.get("isPQRequired"));
                cmmAppPkgDetails.setReoiRfaRequired((String)linkedHashMap.get("reoiRfaRequired"));
                cmmAppPkgDetails.setProcurementMethodId((Short)linkedHashMap.get("procurementMethodId"));
                cmmAppPkgDetails.setProcurementType((String)linkedHashMap.get("procurementType"));
                cmmAppPkgDetails.setSourceOfFund((String)linkedHashMap.get("sourceOfFund"));
                cmmAppPkgDetails.setPkgUrgency((String)linkedHashMap.get("pkgUrgency"));
                cmmAppPkgDetails.setCpvCode((String)linkedHashMap.get("cpvCode"));
                cmmAppPkgDetails.setPackageId((Integer)linkedHashMap.get("packageId"));
                cmmAppPkgDetails.setLotNo((String)linkedHashMap.get("lotNo"));
                cmmAppPkgDetails.setLotDesc((String)linkedHashMap.get("lotDesc"));
                cmmAppPkgDetails.setQuantity((BigDecimal)linkedHashMap.get("quantity"));
                cmmAppPkgDetails.setUnit((String)linkedHashMap.get("unit"));
                cmmAppPkgDetails.setLotEstCost((BigDecimal)linkedHashMap.get("lotEstCost"));
                cmmAppPkgDetails.setAaEmpId((Integer)linkedHashMap.get("aAEmployeeId"));
                cmmAppPkgDetails.setEmeType((String)linkedHashMap.get("pkgUrgency"));
                cmmAppPkgDetails.setCpvCode((String)linkedHashMap.get("cpvCode"));
                cmmAppPkgDetails.setPqDtId((Integer)linkedHashMap.get("pqDtId"));
                cmmAppPkgDetails.setProjectId((Integer)linkedHashMap.get("projectId"));
                                
                if(!"AppPackage".equalsIgnoreCase(pkgType)){
                    cmmAppPkgDetails.setPe((String)linkedHashMap.get("PE"));
                    cmmAppPkgDetails.setDistrict((String)linkedHashMap.get("district"));
                    cmmAppPkgDetails.setAdvtDt((String)linkedHashMap.get("advtDt"));
                    cmmAppPkgDetails.setSubDt((String)linkedHashMap.get("subDt"));
                    cmmAppPkgDetails.setTenderAdvertDt((String)linkedHashMap.get("tenderAdvertDt"));
                    cmmAppPkgDetails.setTenderSubDt((String)linkedHashMap.get("tenderSubDt"));
                    cmmAppPkgDetails.setTenderOpenDt((String)linkedHashMap.get("tenderOpenDt"));
                    //Proshanto
                    cmmAppPkgDetails.setTenderLetterIntentDt((String)linkedHashMap.get("tenderLetterIntentDt"));
                    //
                    cmmAppPkgDetails.setTenderNoaIssueDt((String)linkedHashMap.get("tenderNoaIssueDt"));
                    cmmAppPkgDetails.setTenderContractSignDt((String)linkedHashMap.get("tenderContractSignDt"));
                    cmmAppPkgDetails.setTenderContractCompDt((String)linkedHashMap.get("tenderContractCompDt"));
                    cmmAppPkgDetails.setTechSubCmtRptDt((String)linkedHashMap.get("techSubCmtRptDt"));
                    cmmAppPkgDetails.setTenderEvalRptDt((String)linkedHashMap.get("tenderEvalRptDt"));
                    cmmAppPkgDetails.setTenderEvalRptAppDt((String)linkedHashMap.get("tenderEvalRptAppDt"));
                    cmmAppPkgDetails.setTenderContractAppDt((String)linkedHashMap.get("tenderContractAppDt"));
                    cmmAppPkgDetails.setReoiReceiptDt((String)linkedHashMap.get("reoiReceiptDt"));
                    cmmAppPkgDetails.setRfpTechEvalDt((String)linkedHashMap.get("rfpTechEvalDt"));
                    cmmAppPkgDetails.setRfpFinancialOpenDt((String)linkedHashMap.get("rfpFinancialOpenDt"));
                    cmmAppPkgDetails.setRfpNegCompDt((String)linkedHashMap.get("rfpNegCompDt"));
                    cmmAppPkgDetails.setRfpContractAppDt((String)linkedHashMap.get("rfpContractAppDt"));
                    cmmAppPkgDetails.setRfaAdvertDt((String)linkedHashMap.get("rfaAdvertDt"));
                    cmmAppPkgDetails.setRfaReceiptDt((String)linkedHashMap.get("rfaReceiptDt"));
                    cmmAppPkgDetails.setRfaEvalDt((String)linkedHashMap.get("rfaEvalDt"));
                    cmmAppPkgDetails.setRfaInterviewDt((String)linkedHashMap.get("rfaInterviewDt"));
                    cmmAppPkgDetails.setRfaFinalSelDt((String)linkedHashMap.get("rfaFinalSelDt"));
                    cmmAppPkgDetails.setRfaEvalRptSubDt((String)linkedHashMap.get("rfaEvalRptSubDt"));
                    cmmAppPkgDetails.setRfaAppConsultantDt((String)linkedHashMap.get("rfaAppConsultantDt"));
                    cmmAppPkgDetails.setActSubDt((String)linkedHashMap.get("actSubDt"));
                    cmmAppPkgDetails.setActAdvtDt((String)linkedHashMap.get("actAdvtDt"));
                    cmmAppPkgDetails.setActEvalRptDt((String)linkedHashMap.get("actEvalRptDt"));
                    cmmAppPkgDetails.setActAppLstDt((String)linkedHashMap.get("actAppLstDt"));
                    cmmAppPkgDetails.setActTenderAdvertDt((String)linkedHashMap.get("actTenderAdvertDt"));
                    cmmAppPkgDetails.setActTenderSubDt((String)linkedHashMap.get("actTenderSubDt"));
                    cmmAppPkgDetails.setActTenderOpenDt((String)linkedHashMap.get("actTenderOpenDt"));
                    cmmAppPkgDetails.setTechSubCmtRptDt((String)linkedHashMap.get("actTechSubCmtRptDt"));
                    cmmAppPkgDetails.setActTenderEvalRptDt((String)linkedHashMap.get("actTenderEvalRptDt"));
                    cmmAppPkgDetails.setActtenderEvalRptAppDt((String)linkedHashMap.get("acttenderEvalRptAppDt"));
                    cmmAppPkgDetails.setActTenderContractAppDt((String)linkedHashMap.get("actTenderContractAppDt"));
                    cmmAppPkgDetails.setActTenderNoaIssueDt((String)linkedHashMap.get("actTenderNoaIssueDt"));
                    cmmAppPkgDetails.setActTenderContractSignDt((String)linkedHashMap.get("actTenderContractSignDt"));
                    cmmAppPkgDetails.setActTenderContractCompDt((String)linkedHashMap.get("actTenderContractCompDt"));
                    cmmAppPkgDetails.setActReoiReceiptDt((String)linkedHashMap.get("actReoiReceiptDt"));
                    cmmAppPkgDetails.setActRfpTechEvalDt((String)linkedHashMap.get("actRfpTechEvalDt"));
                    cmmAppPkgDetails.setActRfpFinancialOpenDt((String)linkedHashMap.get("actRfpFinancialOpenDt"));
                    cmmAppPkgDetails.setActRfpNegComDt((String)linkedHashMap.get("actRfpNegComDt"));
                    cmmAppPkgDetails.setActRfpContractAppDt((String)linkedHashMap.get("actRfpContractAppDt"));
                    cmmAppPkgDetails.setActRfaAdvertDt((String)linkedHashMap.get("actRfaAdvertDt"));
                    cmmAppPkgDetails.setActRfaReceiptDt((String)linkedHashMap.get("actRfaReceiptDt"));
                    cmmAppPkgDetails.setActRfaEvalDt((String)linkedHashMap.get("actRfaEvalDt"));
                    cmmAppPkgDetails.setActRfaInterviewDt((String)linkedHashMap.get("actRfaInterviewDt"));
                    cmmAppPkgDetails.setActRfaFinalSelDt((String)linkedHashMap.get("actRfaFinalSelDt"));
                    cmmAppPkgDetails.setActRfaEvalRptSubDt((String)linkedHashMap.get("actRfaEvalRptSubDt"));
                    cmmAppPkgDetails.setActRfaAppConsultantDt((String)linkedHashMap.get("actRfaAppConsultantDt"));
                    cmmAppPkgDetails.setEvalRptDt((String)linkedHashMap.get("evalRptDt"));

                    cmmAppPkgDetails.setAppLstDt((String)linkedHashMap.get("appLstDt"));
                    cmmAppPkgDetails.setOpenDt((String)linkedHashMap.get("openDt"));
                    cmmAppPkgDetails.setSubDays((Short)linkedHashMap.get("subDays"));
                    cmmAppPkgDetails.setAdvtDays((Short)linkedHashMap.get("advtDays"));
                    cmmAppPkgDetails.setOpenDays((Short)linkedHashMap.get("openDays"));
                    cmmAppPkgDetails.setEvalRptDays((Short)linkedHashMap.get("evalRptDays"));
                    cmmAppPkgDetails.setTenderAdvertDays((Short)linkedHashMap.get("tenderAdvertDays"));
                    cmmAppPkgDetails.setTenderSubDays((Short)linkedHashMap.get("tenderSubDays"));
                    cmmAppPkgDetails.setTenderOpenDays((Short)linkedHashMap.get("tenderOpenDays"));
                    cmmAppPkgDetails.setTechSubCmtRptDays((Short)linkedHashMap.get("techSubCmtRptDays"));
                    cmmAppPkgDetails.setTenderEvalRptdays((Short)linkedHashMap.get("tenderEvalRptdays"));
                    cmmAppPkgDetails.setTenderEvalRptAppDays((Short)linkedHashMap.get("tenderEvalRptAppDays"));
                    cmmAppPkgDetails.setTenderContractAppDays((Short)linkedHashMap.get("tenderContractAppDays"));
                    //Proshanto
                    cmmAppPkgDetails.setTnderLetterIntentDays((Short)linkedHashMap.get("tnderLetterIntentDays"));
                    //
                    cmmAppPkgDetails.setTenderNoaIssueDays((Short)linkedHashMap.get("tenderNoaIssueDays"));
                    cmmAppPkgDetails.setTenderContractSignDays((Short)linkedHashMap.get("tenderContractSignDays"));
                    cmmAppPkgDetails.setRfaAdvertDays((Short)linkedHashMap.get("rfaAdvertDays"));
                    cmmAppPkgDetails.setRfaReceiptDays((Short)linkedHashMap.get("rfaReceiptDays"));
                    cmmAppPkgDetails.setRfaEvalDays((Short)linkedHashMap.get("rfaEvalDays"));
                    cmmAppPkgDetails.setRfaInterviewDays((Short)linkedHashMap.get("rfaInterviewDays"));
                    cmmAppPkgDetails.setRfaFinalSelDays((Short)linkedHashMap.get("rfaFinalSelDays"));
                    cmmAppPkgDetails.setRfaEvalRptSubDays((Short)linkedHashMap.get("rfaEvalRptSubDays"));
                    cmmAppPkgDetails.setRfpContractAppDays((Short)linkedHashMap.get("rfpContractAppDays"));
                    cmmAppPkgDetails.setRfpNegCompDays((Short)linkedHashMap.get("rfpNegCompDays"));
                    cmmAppPkgDetails.setRfpFinancialOpenDays((Short)linkedHashMap.get("rfpFinancialOpenDays"));
                    cmmAppPkgDetails.setRfpTechEvalDays((Short)linkedHashMap.get("rfpTechEvalDays"));
                    cmmAppPkgDetails.setReoiReceiptDays((Short)linkedHashMap.get("reoiReceiptDays"));

                }
                details.add(cmmAppPkgDetails);
            }
            }else{
                LOGGER.debug("list is empty : ");
            }
        } catch (Exception e) {
            LOGGER.error("SPAppPkgDetails : "+ e);
        }
      return details;

    }
}
