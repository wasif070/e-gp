/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblCascadeDetailsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCascadeLinkDetails;
import java.util.List;

/**
 *
 * @author tejasree.goriparthi
 */
public class TblCascadeDetailsImpl extends AbcAbstractClass<TblCascadeLinkDetails> implements TblCascadeDetailsDao {

    @Override
    public List<TblCascadeLinkDetails> getAllTblCascadeDetails() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCascadeLinkDetails> findTblCascadeDetails(Object... values) throws Exception {
        return super.findEntity(values);
    }

   
}
