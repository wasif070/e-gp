/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblBidConfirmationDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblBidConfirmation;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblBidConfirmationImpl extends AbcAbstractClass<TblBidConfirmation> implements TblBidConfirmationDao {

    @Override
    public void addTblBidConfirmation(TblBidConfirmation admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblBidConfirmation(TblBidConfirmation admin) {
        super.deleteEntity(admin);        
    }

    @Override
    public void updateTblBidConfirmation(TblBidConfirmation admin) {
        super.updateEntity(admin);        
    }

    @Override
    public List<TblBidConfirmation> getAllTblBidConfirmation() {
        return super.getAllEntity();
    }

    @Override
    public List<TblBidConfirmation> findTblBidConfirmation(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblBidConfirmationCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblBidConfirmation> findByCountTblBidConfirmation(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
