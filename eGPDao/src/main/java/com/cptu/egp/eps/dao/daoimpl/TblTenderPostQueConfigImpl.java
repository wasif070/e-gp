/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderPostQueConfigDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderPostQueConfig;
import java.util.List;

/**
 *
 * @author dixit
 */
public class TblTenderPostQueConfigImpl extends AbcAbstractClass<TblTenderPostQueConfig> implements TblTenderPostQueConfigDao{

    @Override
    public void addTblTenderPostQueConfig(TblTenderPostQueConfig tblTenderPostQueConfig) {
        super.addEntity(tblTenderPostQueConfig);
    }

    @Override
    public void deleteTblTenderPostQueConfig(TblTenderPostQueConfig tblTenderPostQueConfig) {
         super.deleteEntity(tblTenderPostQueConfig);
    }

    @Override
    public void updateTblTenderPostQueConfig(TblTenderPostQueConfig tblTenderPostQueConfig) {
         super.updateEntity(tblTenderPostQueConfig);
    }

    @Override
    public List<TblTenderPostQueConfig> getAllTblTenderPostQueConfig() {
         return super.getAllEntity();
    }

    @Override
    public List<TblTenderPostQueConfig> findTblTenderPostQueConfig(Object... values) throws Exception {
         return super.findEntity(values);
    }

    @Override
    public List<TblTenderPostQueConfig> findByCountTblTenderPostQueConfig(int firstResult, int maxResult, Object... values) throws Exception {
         return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblTenderTablesCount() {
         return super.getEntityCount();
    }

}
