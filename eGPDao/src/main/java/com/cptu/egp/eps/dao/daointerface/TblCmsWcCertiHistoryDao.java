/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.model.table.TblCmsWcCertiHistory;
import java.util.List;

/**
 *
 * @author rikin.p
 */
public interface TblCmsWcCertiHistoryDao {
    
    public void addTblCmsWcCertiHistory(TblCmsWcCertiHistory event);

    public void deleteTblCmsWcCertiHistory(TblCmsWcCertiHistory event);

    public void updateTblCmsWcCertiHistory(TblCmsWcCertiHistory event);

    public List<TblCmsWcCertiHistory> getAllTblCmsWcCertiHistory();

    public List<TblCmsWcCertiHistory> findTblCmsWcCertiHistory(Object... values) throws Exception;

    public List<TblCmsWcCertiHistory> findByCountTblCmsWcCertiHistory(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsWcCertiHistoryCount();
}
