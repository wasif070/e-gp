package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalRoundMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblEvalRoundMasterDao extends GenericDao<TblEvalRoundMaster> {

    public void addTblEvalRoundMaster(TblEvalRoundMaster tblObj);

    public void deleteTblEvalRoundMaster(TblEvalRoundMaster tblObj);

    public void updateTblEvalRoundMaster(TblEvalRoundMaster tblObj);

    public List<TblEvalRoundMaster> getAllTblEvalRoundMaster();

    public List<TblEvalRoundMaster> findTblEvalRoundMaster(Object... values) throws Exception;

    public List<TblEvalRoundMaster> findByCountTblEvalRoundMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalRoundMasterCount();
}
