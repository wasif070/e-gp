package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvStaffDaysCntDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvStaffDaysCnt;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsSrvStaffDaysCntImpl extends AbcAbstractClass<TblCmsSrvStaffDaysCnt> implements TblCmsSrvStaffDaysCntDao {

    @Override
    public void addTblCmsSrvStaffDaysCnt(TblCmsSrvStaffDaysCnt cmsInvoiceDetails){
        super.addEntity(cmsInvoiceDetails);
    }

    @Override
    public void deleteTblCmsSrvStaffDaysCnt(TblCmsSrvStaffDaysCnt cmsInvoiceDetails) {
        super.deleteEntity(cmsInvoiceDetails);
    }

    @Override
    public void updateTblCmsSrvStaffDaysCnt(TblCmsSrvStaffDaysCnt cmsInvoiceDetails) {
        super.updateEntity(cmsInvoiceDetails);
    }

    @Override
    public List<TblCmsSrvStaffDaysCnt> getAllTblCmsSrvStaffDaysCnt() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvStaffDaysCnt> findTblCmsSrvStaffDaysCnt(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvStaffDaysCntCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvStaffDaysCnt> findByCountTblCmsSrvStaffDaysCnt(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }


    @Override
    public void updateOrSaveEstCost(List<TblCmsSrvStaffDaysCnt> estCost) {
       super.updateAll(estCost);
    }
}
