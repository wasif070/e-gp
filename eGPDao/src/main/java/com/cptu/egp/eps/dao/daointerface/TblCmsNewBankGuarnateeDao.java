/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsNewBankGuarnatee;
import java.util.List;

/**
 *
 * @author rikin.p
 */
public interface TblCmsNewBankGuarnateeDao extends GenericDao<TblCmsNewBankGuarnatee> {

    public void addTblCmsNewBankGuarnatee(TblCmsNewBankGuarnatee cmsNewBankGuarnatee);

    public void deleteTblCmsNewBankGuarnatee(TblCmsNewBankGuarnatee cmsNewBankGuarnatee);

    public void updateTblCmsNewBankGuarnatee(TblCmsNewBankGuarnatee cmsNewBankGuarnatee);

    public List<TblCmsNewBankGuarnatee> getAllTblCmsNewBankGuarnatee();

    public List<TblCmsNewBankGuarnatee> findTblCmsNewBankGuarnatee(Object... values) throws Exception;

    public List<TblCmsNewBankGuarnatee> findByCountTblCmsNewBankGuarnatee(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsNewBankGuarnateeCount();
}
