package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvCchistory;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsSrvCchistoryDao extends GenericDao<TblCmsSrvCchistory> {

    public void addTblCmsSrvCchistory(TblCmsSrvCchistory activity);

    public void deleteTblCmsSrvCchistory(TblCmsSrvCchistory activity);

    public void updateTblCmsSrvCchistory(TblCmsSrvCchistory activity);

    public List<TblCmsSrvCchistory> getAllTblCmsSrvCchistory();

    public List<TblCmsSrvCchistory> findTblCmsSrvCchistory(Object... values) throws Exception;

    public List<TblCmsSrvCchistory> findByCountTblCmsSrvCchistory(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsSrvCchistoryCount();

    public void updateOrSaveEstCost(List<TblCmsSrvCchistory> estCost);
}
