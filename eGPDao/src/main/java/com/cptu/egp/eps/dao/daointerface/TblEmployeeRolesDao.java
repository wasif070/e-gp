/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEmployeeRoles;
import java.util.List;

/**
 * <b>Interface Description goes here</b>
 * $Revision: 1.1.1.1 $
 * @version <version-no.> <Date>
 * @author Administrator
 */
public interface TblEmployeeRolesDao extends GenericDao<TblEmployeeRoles>{

    public void addEmployeeRole(TblEmployeeRoles tblEmployeeRoles);

    public void deleteEmployeeRole(TblEmployeeRoles tblEmployeeRoles);

    public void updateEmployeeRole(TblEmployeeRoles tblEmployeeRoles);

    public List<TblEmployeeRoles> getAllEmployeeRoles();

    public List<TblEmployeeRoles> findEmployeeRoles(Object... values) throws Exception;

}
