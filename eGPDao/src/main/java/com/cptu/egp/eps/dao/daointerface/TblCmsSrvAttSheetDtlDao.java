package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvAttSheetDtl;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsSrvAttSheetDtlDao extends GenericDao<TblCmsSrvAttSheetDtl> {

    public void addTblCmsSrvAttSheetDtl(TblCmsSrvAttSheetDtl tblObj);

    public void deleteTblCmsSrvAttSheetDtl(TblCmsSrvAttSheetDtl tblObj);

    public void updateTblCmsSrvAttSheetDtl(TblCmsSrvAttSheetDtl tblObj);

    public List<TblCmsSrvAttSheetDtl> getAllTblCmsSrvAttSheetDtl();

    public List<TblCmsSrvAttSheetDtl> findTblCmsSrvAttSheetDtl(Object... values) throws Exception;

    public List<TblCmsSrvAttSheetDtl> findByCountTblCmsSrvAttSheetDtl(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsSrvAttSheetDtlCount();
    
   public void updateOrSaveEstCost(List<TblCmsSrvAttSheetDtl> estCost);
}
