/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Ramesh.Janagondakuruba
 */
public class SPWfFileHistory extends StoredProcedure
{
    private static final Logger LOGGER = Logger.getLogger(SPWfFileHistory.class);
    public SPWfFileHistory(BasicDataSource dataSource, String procName)
    {

        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("v_ViewType_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_ActivityId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_ObjectId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_ChildId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_UserId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_Page_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_RecordPerPage_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_columnNameVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_orderTypeVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_ModuleName", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_EventName", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_ProcessedBy", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_ObjectId", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_FromProcessDate", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_FromProcessTo", Types.VARCHAR));

    }

    /**
     * Execute stored procedure : p_get_wffilehistory
     * Get Workflow file history details
     * @param viewType
     * @param activityId
     * @param objId
     * @param childId
     * @param userId
     * @param page
     * @param recordPerPage
     * @param columnName
     * @param order
     * @param moduleName
     * @param eventName
     * @param processedBy
     * @param objectId
     * @param fromProcessDate
     * @param fromProcessTo
     * @return Work flow file history details as list of CommonSPWfFileHistory object
     */
    public List<CommonSPWfFileHistory> executeProcedure(String viewType, Integer activityId, Integer objId, Integer childId, Integer userId, Integer page, Integer recordPerPage,String columnName,String order,String moduleName,String eventName,String processedBy,Integer objectId, String fromProcessDate, String fromProcessTo)
    {

        Map inParams = new HashMap();
        inParams.put("v_ViewType_inVc", viewType);
        inParams.put("v_ActivityId_inInt", activityId);
        inParams.put("v_ObjectId_inInt", objId);
        inParams.put("v_ChildId_inInt", childId);
        inParams.put("v_UserId_inInt", userId);
        inParams.put("v_Page_inInt", page);
        inParams.put("v_RecordPerPage_inInt", recordPerPage);
        inParams.put("v_columnNameVc", columnName);
        inParams.put("v_orderTypeVc", order);
        inParams.put("v_ModuleName", moduleName);
        inParams.put("v_EventName", eventName);
        inParams.put("v_ProcessedBy", processedBy);
        inParams.put("v_ObjectId", objectId);
        inParams.put("v_FromProcessDate", fromProcessDate);
        inParams.put("v_FromProcessTo", fromProcessTo);
        
        this.compile();
        List<CommonSPWfFileHistory> details = new ArrayList<CommonSPWfFileHistory>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {

                CommonSPWfFileHistory cmmSPWfFileHistory=new CommonSPWfFileHistory();
                
                cmmSPWfFileHistory.setFromUserId((Integer)linkedHashMap.get("fromUserId"));
                cmmSPWfFileHistory.setToUserId((Integer)linkedHashMap.get("toUserId"));
                cmmSPWfFileHistory.setFileSentFrom((String)linkedHashMap.get("fileSentFrom"));
                cmmSPWfFileHistory.setFileSentTo((String)linkedHashMap.get("fileSentTo"));
                cmmSPWfFileHistory.setAction((String)linkedHashMap.get("action"));
                cmmSPWfFileHistory.setRemarks((String)linkedHashMap.get("remarks"));
                cmmSPWfFileHistory.setProcessDate((Date)linkedHashMap.get("processDate"));
                cmmSPWfFileHistory.setModuleName((String)linkedHashMap.get("moduleName"));
                cmmSPWfFileHistory.setEventName((String)linkedHashMap.get("eventName"));
                cmmSPWfFileHistory.setTotalRecords((Integer)linkedHashMap.get("totalRecords"));
                cmmSPWfFileHistory.setRowNumber((String)linkedHashMap.get("RowNumber"));
                cmmSPWfFileHistory.setObjectiId((Integer)linkedHashMap.get("objectId"));
                cmmSPWfFileHistory.setChildId((Integer)linkedHashMap.get("childId"));
                cmmSPWfFileHistory.setEventId((Short)linkedHashMap.get("eventId"));
                cmmSPWfFileHistory.setActivityId((Short)linkedHashMap.get("activityId"));
                cmmSPWfFileHistory.setDocId((String)linkedHashMap.get("docId"));
                cmmSPWfFileHistory.setWfHistoryId((String)linkedHashMap.get("wfHistoryId"));
                details.add(cmmSPWfFileHistory);
            }
        }
        catch (Exception e) {
            LOGGER.error("SPWfFileHistory  : "+ e);
        }
        return details;

    }
}
