package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCountryMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCountryMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCountryMasterImpl extends AbcAbstractClass<TblCountryMaster> implements TblCountryMasterDao {
   
    @Override
    public List<TblCountryMaster> getAllTblCountryMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCountryMaster> findTblCountryMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCountryMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCountryMaster> findByCountTblCountryMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
