/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.math.BigDecimal;
import java.util.Date;
import net.sf.cglib.core.TinyBitSet;
/**
 *
 * @author Ahsan
 */
public class ContractAwardOfflineDetails {

    private Integer ContarctAIdOL;
    private String ministry;
    private String division;
    private String agency;
    private String peOfficeName;
    private String peCode;
    private String peDistrict;
    private String AwardForPNature;
    private String RefNo;
    private String procurementMethod;
    private String budgetType;
    private String sourceOfFund;
    private String devPartners;
    private String projectCode;
    private String projectName;
    private String packageNo;
    private String packageName;
    private Date DateofAdvertisement;
    private Date DateofNOA;
    private Date DateofContractSign;
    private Date DateofPCCompletion;
    private String Sold;
    private String Received;
    private String Response;
    private String DescriptionofContract;
    private BigDecimal ContractValue;
    private String NameofTenderer;
    private String AddressofTenderer;
    private String DeliveryPlace;
    private Short IsSamePersonNOA;
    private String NOAReason;
    private Short IsPSecurityDueTime;
    private String PSecurityReason;
    private Short IsSignedDueTime;
    private String SignedReason;
    private String OfficerName;
    private String OfficerDesignation;
    private Integer UserID;
    private Date Date;
    private String Status;

    /**
     * @return the ContarctAIdOL
     */
    public Integer getContarctAIdOL() {
        return ContarctAIdOL;
    }

    /**
     * @param ContarctAIdOL the ContarctAIdOL to set
     */
    public void setContarctAIdOL(Integer ContarctAIdOL) {
        this.ContarctAIdOL = ContarctAIdOL;
    }

    public String getMinistry() {
        return ministry;
    }

    public void setMinistry(String ministry) {
        this.ministry = ministry;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public String getPeOfficeName() {
        return peOfficeName;
    }

    public void setPeOfficeName(String peOfficeName) {
        this.peOfficeName = peOfficeName;
    }

    public String getPeCode() {
        return peCode;
    }

    public void setPeCode(String peCode) {
        this.peCode = peCode;
    }

    public String getPeDistrict() {
        return peDistrict;
    }

    public void setPeDistrict(String peDistrict) {
        this.peDistrict = peDistrict;
    }

    public String getAwardForPNature() {
        return AwardForPNature;
    }

    public void setAwardForPNature(String AwardForPNature) {
        this.AwardForPNature = AwardForPNature;
    }

    public String getRefNo() {
        return RefNo;
    }

    public void setRefNo(String RefNo) {
        this.RefNo = RefNo;
    }

    public String getProcurementMethod() {
        return procurementMethod;
    }

    public void setProcurementMethod(String procurementMethod) {
        this.procurementMethod = procurementMethod;
    }

    public String getBudgetType() {
        return budgetType;
    }

    public void setBudgetType(String budgetType) {
        this.budgetType = budgetType;
    }

    public String getSourceOfFund() {
        return sourceOfFund;
    }

    public void setSourceOfFund(String sourceOfFund) {
        this.sourceOfFund = sourceOfFund;
    }

    public String getDevPartners() {
        return devPartners;
    }

    public void setDevPartners(String devPartners) {
        this.devPartners = devPartners;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getPackageNo() {
        return packageNo;
    }

    public void setPackageNo(String packageNo) {
        this.packageNo = packageNo;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Date getDateofAdvertisement() {
        return DateofAdvertisement;
    }

    public void setDateofAdvertisement(Date DateofAdvertisement) {
        this.DateofAdvertisement = DateofAdvertisement;
    }

    public Date getDateofNOA() {
        return DateofNOA;
    }

    public void setDateofNOA(Date DateofNOA) {
        this.DateofNOA = DateofNOA;
    }

    public Date getDateofContractSign() {
        return DateofContractSign;
    }

    public void setDateofContractSign(Date DateofContractSign) {
        this.DateofContractSign = DateofContractSign;
    }

    public Date getDateofPCCompletion() {
        return DateofPCCompletion;
    }

    public void setDateofPCCompletion(Date DateofPCCompletion) {
        this.DateofPCCompletion = DateofPCCompletion;
    }

    public String getSold() {
        return Sold;
    }

    public void setSold(String Sold) {
        this.Sold = Sold;
    }

    public String getReceived() {
        return Received;
    }

    public void setReceived(String Received) {
        this.Received = Received;
    }

    public String getResponse() {
        return Response;
    }

    public void setResponse(String Response) {
        this.Response = Response;
    }

    public String getDescriptionofContract() {
        return DescriptionofContract;
    }

    public void setDescriptionofContract(String DescriptionofContract) {
        this.DescriptionofContract = DescriptionofContract;
    }

    public BigDecimal getContractValue() {
        return ContractValue;
    }

    public void setContractValue(BigDecimal ContractValue) {
        this.ContractValue = ContractValue;
    }

    public String getNameofTenderer() {
        return NameofTenderer;
    }

    public void setNameofTenderer(String NameofTenderer) {
        this.NameofTenderer = NameofTenderer;
    }

    public String getAddressofTenderer() {
        return AddressofTenderer;
    }

    public void setAddressofTenderer(String AddressofTenderer) {
        this.AddressofTenderer = AddressofTenderer;
    }

    public String getDeliveryPlace() {
        return DeliveryPlace;
    }

    public void setDeliveryPlace(String DeliveryPlace) {
        this.DeliveryPlace = DeliveryPlace;
    }

    public Short getIsSamePersonNOA() {
        return IsSamePersonNOA;
    }

    public void setIsSamePersonNOA(Short IsSamePersonNOA) {
        this.IsSamePersonNOA = IsSamePersonNOA;
    }

    public String getNOAReason() {
        return NOAReason;
    }

    public void setNOAReason(String NOAReason) {
        this.NOAReason = NOAReason;
    }

    public Short getIsPSecurityDueTime() {
        return IsPSecurityDueTime;
    }

    public void setIsPSecurityDueTime(Short IsPSecurityDueTime) {
        this.IsPSecurityDueTime = IsPSecurityDueTime;
    }

    public String getPSecurityReason() {
        return PSecurityReason;
    }

    public void setPSecurityReason(String PSecurityReason) {
        this.PSecurityReason = PSecurityReason;
    }

    public Short getIsSignedDueTime() {
        return IsSignedDueTime;
    }

    public void setIsSignedDueTime(Short IsSignedDueTime) {
        this.IsSignedDueTime = IsSignedDueTime;
    }

    public String getSignedReason() {
        return SignedReason;
    }

    public void setSignedReason(String SignedReason) {
        this.SignedReason = SignedReason;
    }

    public String getOfficerName() {
        return OfficerName;
    }

    public void setOfficerName(String OfficerName) {
        this.OfficerName = OfficerName;
    }

    public String getOfficerDesignation() {
        return OfficerDesignation;
    }

    public void setOfficerDesignation(String OfficerDesignation) {
        this.OfficerDesignation = OfficerDesignation;
    }

    public Integer getUserID() {
        return UserID;
    }

    public void setUserID(Integer UserID) {
        this.UserID = UserID;
    }

    public Date getDate() {
        return Date;
    }

    public void setDate(Date Date) {
        this.Date = Date;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }
   
}
