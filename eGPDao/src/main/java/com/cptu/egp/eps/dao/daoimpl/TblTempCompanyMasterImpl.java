package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblTempCompanyMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTempCompanyMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblTempCompanyMasterImpl extends AbcAbstractClass<TblTempCompanyMaster> implements TblTempCompanyMasterDao {

    @Override
    public void addTblTempCompanyMaster(TblTempCompanyMaster master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblTempCompanyMaster(TblTempCompanyMaster master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblTempCompanyMaster(TblTempCompanyMaster master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblTempCompanyMaster> getAllTblTempCompanyMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTempCompanyMaster> findTblTempCompanyMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTempCompanyMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTempCompanyMaster> findByCountTblTempCompanyMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
