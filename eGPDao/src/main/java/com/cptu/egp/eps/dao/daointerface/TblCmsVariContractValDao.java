package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsVariContractVal;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsVariContractValDao extends GenericDao<TblCmsVariContractVal> {

    public void addTblCmsVariContractVal(TblCmsVariContractVal event);

    public void deleteTblCmsVariContractVal(TblCmsVariContractVal event);

    public void updateTblCmsVariContractVal(TblCmsVariContractVal event);

    public List<TblCmsVariContractVal> getAllTblCmsVariContractVal();

    public List<TblCmsVariContractVal> findTblCmsVariContractVal(Object... values) throws Exception;

    public List<TblCmsVariContractVal> findByCountTblCmsVariContractVal(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsVariContractValCount();

    public void updateOrSaveEstCost(List<TblCmsVariContractVal> estCost);
}
