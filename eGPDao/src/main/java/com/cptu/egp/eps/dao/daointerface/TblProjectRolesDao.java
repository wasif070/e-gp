package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblProjectRoles;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblProjectRolesDao extends GenericDao<TblProjectRoles> {

    public void addTblProjectRoles(TblProjectRoles tblObj);

    public void deleteTblProjectRoles(TblProjectRoles tblObj);

    public void updateTblProjectRoles(TblProjectRoles tblObj);

    public List<TblProjectRoles> getAllTblProjectRoles();

    public List<TblProjectRoles> findTblProjectRoles(Object... values) throws Exception;

    public List<TblProjectRoles> findByCountTblProjectRoles(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblProjectRolesCount();
}
