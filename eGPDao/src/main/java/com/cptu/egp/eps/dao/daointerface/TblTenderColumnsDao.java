/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderColumns;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderColumnsDao extends GenericDao<TblTenderColumns>{

    public void addTblTenderColumns(TblTenderColumns tblObj);

    public void deleteTblTenderColumns(TblTenderColumns tblObj);

    public void updateTblTenderColumns(TblTenderColumns tblObj);

    public List<TblTenderColumns> getAllTblTenderColumns();

    public List<TblTenderColumns> findTblTenderColumns(Object... values) throws Exception;

    public List<TblTenderColumns> findByCountTblTenderColumns(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderColumnsCount();
}
