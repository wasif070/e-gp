package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvWorkPlan;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsSrvWorkPlanDao extends GenericDao<TblCmsSrvWorkPlan> {

    public void addTblCmsSrvWorkPlan(TblCmsSrvWorkPlan event);

    public void deleteTblCmsSrvWorkPlan(TblCmsSrvWorkPlan event);

    public void updateTblCmsSrvWorkPlan(TblCmsSrvWorkPlan event);

    public List<TblCmsSrvWorkPlan> getAllTblCmsSrvWorkPlan();

    public List<TblCmsSrvWorkPlan> findTblCmsSrvWorkPlan(Object... values) throws Exception;

    public List<TblCmsSrvWorkPlan> findByCountTblCmsSrvWorkPlan(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsSrvWorkPlanCount();

    public void updateOrSaveEstCost(List<TblCmsSrvWorkPlan> estCost);
}
