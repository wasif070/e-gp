package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsInvoiceMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCmsInvoiceMasterDao extends GenericDao<TblCmsInvoiceMaster> {

    public void addTblCmsInvoiceMaster(TblCmsInvoiceMaster event);

    public void deleteTblCmsInvoiceMaster(TblCmsInvoiceMaster event);

    public void updateTblCmsInvoiceMaster(TblCmsInvoiceMaster event);

    public List<TblCmsInvoiceMaster> getAllTblCmsInvoiceMaster();

    public List<TblCmsInvoiceMaster> findTblCmsInvoiceMaster(Object... values) throws Exception;

    public List<TblCmsInvoiceMaster> findByCountTblCmsInvoiceMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsInvoiceMasterCount();

    public void updateOrSaveEstCost(List<TblCmsInvoiceMaster> estCost);

}
