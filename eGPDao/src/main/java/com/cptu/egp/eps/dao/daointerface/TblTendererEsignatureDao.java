package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTendererEsignature;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblTendererEsignatureDao extends GenericDao<TblTendererEsignature> {

    public void addTblTendererEsignature(TblTendererEsignature esign);

    public void deleteTblTendererEsignature(TblTendererEsignature esign);

    public void updateTblTendererEsignature(TblTendererEsignature esign);

    public List<TblTendererEsignature> getAllTblTendererEsignature();

    public List<TblTendererEsignature> findTblTendererEsignature(Object... values) throws Exception;

    public List<TblTendererEsignature> findByCountTblTendererEsignature(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTendererEsignatureCount();
}
