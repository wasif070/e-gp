/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblNoaIssueDetailsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblNoaIssueDetails;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblNoaIssueDetailsImpl extends AbcAbstractClass<TblNoaIssueDetails> implements TblNoaIssueDetailsDao{

    @Override
    public void addTblNoaIssueDetails(TblNoaIssueDetails noaIssueDetails) {
        super.addEntity(noaIssueDetails);
    }

    @Override
    public List<TblNoaIssueDetails> findTblNoaIssueDetails(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblNoaIssueDetails(TblNoaIssueDetails department) {

        super.deleteEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblNoaIssueDetails(Collection event) {
        super.deleteAll(event);
    }

    @Override
    public void updateTblNoaIssueDetails(TblNoaIssueDetails department) {

        super.updateEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblNoaIssueDetails> getAllTblNoaIssueDetails() {
        return super.getAllEntity();
    }

    @Override
    public long getTblNoaIssueDetailsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblNoaIssueDetails> findByCountTblNoaIssueDetails(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
