/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderIttHeaderDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderIttHeader;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderIttHeaderImpl extends AbcAbstractClass<TblTenderIttHeader> implements TblTenderIttHeaderDao {

    @Override
    public void addTblTenderIttHeader(TblTenderIttHeader admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblTenderIttHeader(TblTenderIttHeader admin) {
        super.deleteEntity(admin);        
    }

    @Override
    public void updateTblTenderIttHeader(TblTenderIttHeader admin) {
        super.updateEntity(admin);        
    }

    @Override
    public List<TblTenderIttHeader> getAllTblTenderIttHeader() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderIttHeader> findTblTenderIttHeader(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderIttHeaderCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderIttHeader> findByCountTblTenderIttHeader(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
