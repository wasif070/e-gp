/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblWorkFlowDocuments;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public interface TblWorkFlowDocumentsDao extends GenericDao<TblWorkFlowDocuments> {

    public void addTblWorkFlowDocuments(TblWorkFlowDocuments admin);

    public void deleteTblWorkFlowDocuments(TblWorkFlowDocuments admin);

    public void updateTblWorkFlowDocuments(TblWorkFlowDocuments admin);

    public List<TblWorkFlowDocuments> getAllTblWorkFlowDocuments();

    public List<TblWorkFlowDocuments> findTblWorkFlowDocuments(Object... values) throws Exception;

    public List<TblWorkFlowDocuments> findByCountTblWorkFlowDocuments(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblWorkFlowDocumentsCount();
}
