package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblActivityMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblActivityMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblActivityMasterImpl extends AbcAbstractClass<TblActivityMaster> implements TblActivityMasterDao {

    @Override
    public void addTblActivityMaster(TblActivityMaster activityMaster){
        super.addEntity(activityMaster);
    }

    @Override
    public void deleteTblActivityMaster(TblActivityMaster activityMaster) {
        super.deleteEntity(activityMaster);
    }

    @Override
    public void updateTblActivityMaster(TblActivityMaster activityMaster) {
        super.updateEntity(activityMaster);
    }

    @Override
    public List<TblActivityMaster> getAllTblActivityMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblActivityMaster> findTblActivityMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblActivityMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblActivityMaster> findByCountTblActivityMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
