/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblAdvtConfigurationMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblAdvtConfigurationMaster;
import java.util.List;

/**
 *
 * @author spandana.vaddi
 */
public class TblAdvtConfigurationMasterImpl extends AbcAbstractClass<TblAdvtConfigurationMaster> implements TblAdvtConfigurationMasterDao {

    @Override
    public List<TblAdvtConfigurationMaster> getAllTblAdvtConfigurationMaster() {
        return super.getAllEntity();
    }

    @Override
    public void addTblAdvtConfigurationMaster(TblAdvtConfigurationMaster tblAdvtConfigurationMaster) {
        super.addEntity(tblAdvtConfigurationMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblAdvtConfigurationMaster(TblAdvtConfigurationMaster adConfigId) {
        super.deleteEntity(adConfigId);
    }

    @Override
    public void updateTblAdvtConfigurationMaster(TblAdvtConfigurationMaster tblAdvtConfigurationMaster) {
        super.updateEntity(tblAdvtConfigurationMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblAdvtConfigurationMaster> findTblAdvtConfigurationMaster(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblAdvtConfigurationMaster> findByCountTblAdvtConfigurationMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }
}
