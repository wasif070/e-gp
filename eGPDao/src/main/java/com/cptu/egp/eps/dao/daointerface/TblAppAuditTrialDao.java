package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblAppAuditTrial;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblAppAuditTrialDao extends GenericDao<TblAppAuditTrial> {

    public void addTblAppAuditTrial(TblAppAuditTrial tblObj);

    public void deleteTblAppAuditTrial(TblAppAuditTrial tblObj);

    public void updateTblAppAuditTrial(TblAppAuditTrial tblObj);

    public List<TblAppAuditTrial> getAllTblAppAuditTrial();

    public List<TblAppAuditTrial> findTblAppAuditTrial(Object... values) throws Exception;

    public List<TblAppAuditTrial> findByCountTblAppAuditTrial(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblAppAuditTrialCount();
}
