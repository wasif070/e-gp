package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvSalaryRe;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsSrvSalaryReDao extends GenericDao<TblCmsSrvSalaryRe> {

    public void addTblCmsSrvSalaryRe(TblCmsSrvSalaryRe event);

    public void deleteTblCmsSrvSalaryRe(TblCmsSrvSalaryRe event);

    public void updateTblCmsSrvSalaryRe(TblCmsSrvSalaryRe event);

    public List<TblCmsSrvSalaryRe> getAllTblCmsSrvSalaryRe();

    public List<TblCmsSrvSalaryRe> findTblCmsSrvSalaryRe(Object... values) throws Exception;

    public List<TblCmsSrvSalaryRe> findByCountTblCmsSrvSalaryRe(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsSrvSalaryReCount();

    public void updateOrSaveEstCost(List<TblCmsSrvSalaryRe> estCost);
}
