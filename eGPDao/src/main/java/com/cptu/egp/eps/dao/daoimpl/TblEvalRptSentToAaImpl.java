package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblEvalRptSentToAaDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalRptSentToAa;
import java.util.List;

/**
 *
 * @author rishita
 */
public class TblEvalRptSentToAaImpl extends AbcAbstractClass<TblEvalRptSentToAa> implements TblEvalRptSentToAaDao {

    @Override
    public void addTblEvalRptSentToAa(TblEvalRptSentToAa tblEvalRptSentToAa){
        super.addEntity(tblEvalRptSentToAa);
    }

    @Override
    public void deleteTblEvalRptSentToAa(TblEvalRptSentToAa tblEvalRptSentToAa) {
        super.deleteEntity(tblEvalRptSentToAa);
    }

    @Override
    public void updateTblEvalRptSentToAa(TblEvalRptSentToAa tblEvalRptSentToAa) {
        super.updateEntity(tblEvalRptSentToAa);
    }

    @Override
    public List<TblEvalRptSentToAa> getAllTblEvalRptSentToAa() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEvalRptSentToAa> findTblEvalRptSentToAa(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEvalRptSentToAaCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalRptSentToAa> findByCountTblEvalRptSentToAa(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
