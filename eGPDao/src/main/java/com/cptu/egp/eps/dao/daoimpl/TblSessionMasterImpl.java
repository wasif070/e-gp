/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblSessionMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblSessionMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblSessionMasterImpl extends AbcAbstractClass<TblSessionMaster> implements TblSessionMasterDao{

    @Override
    public void addTblSessionMaster(TblSessionMaster tblSessionMaster) {
        super.addEntity(tblSessionMaster);
    }

    @Override
    public List<TblSessionMaster> findTblSessionMaster(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblSessionMaster(TblSessionMaster tblSessionMaster) {

        super.deleteEntity(tblSessionMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblSessionMaster(TblSessionMaster tblSessionMaster) {

        super.updateEntity(tblSessionMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblSessionMaster> getAllTblSessionMaster() {
        return super.getAllEntity();
    }

    @Override
    public long getTblSessionMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblSessionMaster> findByCountTblSessionMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
