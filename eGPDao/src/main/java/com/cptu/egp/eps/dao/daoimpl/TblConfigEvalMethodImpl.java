package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblConfigEvalMethodDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblConfigEvalMethod;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblConfigEvalMethodImpl extends AbcAbstractClass<TblConfigEvalMethod> implements TblConfigEvalMethodDao {

    @Override
    public void addTblConfigEvalMethod(TblConfigEvalMethod configEvalMethod){
        super.addEntity(configEvalMethod);
    }

    @Override
    public void deleteTblConfigEvalMethod(TblConfigEvalMethod configEvalMethod) {
        super.deleteEntity(configEvalMethod);
    }

    @Override
    public void updateTblConfigEvalMethod(TblConfigEvalMethod configEvalMethod) {
        super.updateEntity(configEvalMethod);
    }

    @Override
    public List<TblConfigEvalMethod> getAllTblConfigEvalMethod() {
        return super.getAllEntity();
    }

    @Override
    public List<TblConfigEvalMethod> findTblConfigEvalMethod(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblConfigEvalMethodCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblConfigEvalMethod> findByCountTblConfigEvalMethod(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateInsertAllConfigEvalMethod(List<TblConfigEvalMethod> list) {
        super.updateAll(list);
    }
}
