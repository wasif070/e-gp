package com.cptu.egp.eps.dao.daoimpl;


import com.cptu.egp.eps.dao.daointerface.TblCmsSrvPsvariDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvPsvari;
import java.util.List;


public class TblCmsSrvPsvariImpl extends AbcAbstractClass<TblCmsSrvPsvari> implements TblCmsSrvPsvariDao {

	@Override 
	public void addTblCmsSrvPsvari(TblCmsSrvPsvari master){
		super.addEntity(master);
	}
	@Override 
	public void deleteTblCmsSrvPsvari(TblCmsSrvPsvari master) {
		super.deleteEntity(master);
	}
	@Override 
	public void updateTblCmsSrvPsvari(TblCmsSrvPsvari master) {
		super.updateEntity(master);
	}
	@Override 
	public List<TblCmsSrvPsvari> getAllTblCmsSrvPsvari() {
		return super.getAllEntity();
	}
	@Override 
	public List<TblCmsSrvPsvari> findTblCmsSrvPsvari(Object... values) throws Exception {
		return super.findEntity(values);
	}
	@Override 
	public long getTblCmsSrvPsvariCount() {
		return super.getEntityCount();
	}
	@Override 
	public List<TblCmsSrvPsvari> findByCountTblCmsSrvPsvari(int firstResult, int maxResult, Object... values) throws Exception {
		return super.findByCountEntity(firstResult, maxResult, values);
	}
	@Override 
	public void updateOrSaveTblCmsSrvPsvari(List<TblCmsSrvPsvari> tblObj) {
		super.updateAll(tblObj);
	}
}