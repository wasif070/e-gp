package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsWpDetailHistory;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsWpDetailHistoryrDao extends GenericDao<TblCmsWpDetailHistory> {

    public void addTblCmsWpDetailHistory(TblCmsWpDetailHistory activity);

    public void deleteTblCmsWpDetailHistory(TblCmsWpDetailHistory activity);

    public void updateTblCmsWpDetailHistory(TblCmsWpDetailHistory activity);

    public List<TblCmsWpDetailHistory> getAllTblCmsWpDetailHistory();

    public List<TblCmsWpDetailHistory> findTblCmsWpDetailHistory(Object... values) throws Exception;

    public List<TblCmsWpDetailHistory> findByCountTblCmsWpDetailHistory(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsWpDetailHistoryCount();

    public void updateOrSaveEstCost(List<TblCmsWpDetailHistory> estCost) throws Exception;
}
