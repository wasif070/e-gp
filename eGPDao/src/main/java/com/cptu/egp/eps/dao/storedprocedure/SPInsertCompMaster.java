/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Administrator
 */
public class SPInsertCompMaster extends StoredProcedure{
    private static final Logger LOGGER = Logger.getLogger(SPInsertCompMaster.class);
    SPInsertCompMaster(BasicDataSource dataSource, String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("@v_companyRegNumber_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_userId_inInt", Types.INTEGER));
    }

    /**
     * Execute stored procedure : p_gen_companyid
     * Add Company Master and fetch company id.
     * @param companyRegNumber
     * @param userId
     * @return BidDecimal object having company id value
     */
    public BigDecimal executeProcedure(String companyRegNumber,int userId) {
        Map inParams = new HashMap();
        inParams.put("@v_companyRegNumber_inVc", companyRegNumber);
        inParams.put("@v_userId_inInt", userId);
        this.compile();
        BigDecimal cmpId = null;
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                cmpId = ((BigDecimal) linkedHashMap.get("companyId"));
            }
        } catch (Exception e) {
            LOGGER.error("SPInsertCompMaster: "+ e);
        }
        return cmpId;
    }

}
