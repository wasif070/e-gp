/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
/**
 *
 * @author Kinjal Shah
 * Used for setting input output para
 */
public class SPReportCommon extends StoredProcedure {

    private static final Logger LOGGER = Logger.getLogger(SPReportCommon.class);
    /* Pass Input Paras to proc*/

    public SPReportCommon(BasicDataSource dataSource, String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);
        this.declareParameter(new SqlParameter("@v_formId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_userId_inInt", Types.INTEGER));

    }

    /**
     * Execute stored procedure : p_gen_individualreport/p_gen_comparativereport
     * Generate Individual report bidder wise.
     * @param formId
     * @param userId
     * @return list of SPReportCommonData.
     */
    public List<SPReportCommonData> executeProcedure(int formId, int userId) {
        Map inParams = new HashMap();
        inParams.put("@v_formId_inInt", formId);
        inParams.put("@v_userId_inInt", userId);

        this.compile();

        List<SPReportCommonData> reportData = new ArrayList<SPReportCommonData>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            if (list != null && !list.isEmpty()) {
                for (LinkedHashMap<String, Object> linkedHashMap : list) {
                    SPReportCommonData sPReportCommonData = new SPReportCommonData();
                    sPReportCommonData.setReportHtml((String) linkedHashMap.get("reportHtml"));
                    reportData.add(sPReportCommonData);
                }
            }
        } catch (Exception e) {
            if(userId == 0){
                System.out.println("Comparetive report");
            } else {
                System.out.println("INdividual report");
            }
            e.printStackTrace();
            LOGGER.error("SPReportCommon  : " + e);
        }
        return reportData;
    }
}
