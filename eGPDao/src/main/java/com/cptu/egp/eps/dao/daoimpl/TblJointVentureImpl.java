package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblJointVentureDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblJointVenture;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblJointVentureImpl extends AbcAbstractClass<TblJointVenture> implements TblJointVentureDao {

    @Override
    public void addTblJointVenture(TblJointVenture jointVenture){
        super.addEntity(jointVenture);
    }

    @Override
    public void deleteTblJointVenture(TblJointVenture jointVenture) {
        super.deleteEntity(jointVenture);
    }

    @Override
    public void updateTblJointVenture(TblJointVenture jointVenture) {
        super.updateEntity(jointVenture);
    }

    @Override
    public List<TblJointVenture> getAllTblJointVenture() {
        return super.getAllEntity();
    }

    @Override
    public List<TblJointVenture> findTblJointVenture(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblJointVentureCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblJointVenture> findByCountTblJointVenture(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
