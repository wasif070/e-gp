/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblPreTenderMetDocs;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblPreTenderMetDocsDao extends GenericDao<TblPreTenderMetDocs>{

    public void addTblPreTenderMetDocs(TblPreTenderMetDocs tblObj);

    public void deleteTblPreTenderMetDocs(TblPreTenderMetDocs tblObj);

    public void updateTblPreTenderMetDocs(TblPreTenderMetDocs tblObj);

    public List<TblPreTenderMetDocs> getAllTblPreTenderMetDocs();

    public List<TblPreTenderMetDocs> findTblPreTenderMetDocs(Object... values) throws Exception;

    public List<TblPreTenderMetDocs> findByCountTblPreTenderMetDocs(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblPreTenderMetDocsCount();
}
