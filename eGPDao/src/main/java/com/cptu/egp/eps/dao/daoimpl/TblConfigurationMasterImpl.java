/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;


import com.cptu.egp.eps.dao.daointerface.TblConfigurationMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class TblConfigurationMasterImpl extends AbcAbstractClass<TblConfigurationMaster> implements TblConfigurationMasterDao {

    @Override
    public List<TblConfigurationMaster> getAllTblConfigurationMaster() {
        return super.getAllEntity();
    }

    @Override
    public void addTblConfigurationMaster(TblConfigurationMaster tblConfigurationMaster) {
        super.addEntity(tblConfigurationMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblConfigurationMaster(TblConfigurationMaster tblConfigurationMaster) {
        super.deleteEntity(tblConfigurationMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblConfigurationMaster(TblConfigurationMaster tblConfigurationMaster) {
        super.updateEntity(tblConfigurationMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblConfigurationMaster> findTblConfigurationMaster(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblConfigurationMaster> findByCountTblConfigurationMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    /*@Override
    public long getTblConfigurationMasterCount() {
        throw new UnsupportedOperationException("Not supported yet.");
    }*/
}
