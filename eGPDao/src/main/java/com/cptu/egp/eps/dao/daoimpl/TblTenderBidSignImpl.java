/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderBidSignDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderBidSign;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderBidSignImpl extends AbcAbstractClass<TblTenderBidSign> implements TblTenderBidSignDao {

    @Override
    public void addTblTenderBidSign(TblTenderBidSign admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblTenderBidSign(TblTenderBidSign admin) {
        super.deleteEntity(admin);        
    }

    @Override
    public void updateTblTenderBidSign(TblTenderBidSign admin) {
        super.updateEntity(admin);        
    }

    @Override
    public List<TblTenderBidSign> getAllTblTenderBidSign() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderBidSign> findTblTenderBidSign(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderBidSignCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderBidSign> findByCountTblTenderBidSign(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
