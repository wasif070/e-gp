/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblWorkFlowFileOnHandDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblWorkFlowFileOnHand;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public class TblWorkFlowFileOnHandImpl extends AbcAbstractClass<TblWorkFlowFileOnHand> implements TblWorkFlowFileOnHandDao {

    @Override
    public void addTblWorkFlowFileOnHand(TblWorkFlowFileOnHand workFlowFileOnHand) {

        super.addEntity(workFlowFileOnHand);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblWorkFlowFileOnHand(TblWorkFlowFileOnHand workFlowFileOnHand) {

        super.deleteEntity(workFlowFileOnHand);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblWorkFlowFileOnHand(TblWorkFlowFileOnHand workFlowFileOnHand) {

        super.updateEntity(workFlowFileOnHand);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblWorkFlowFileOnHand> getAllTblWorkFlowFileOnHand() {
        return super.getAllEntity();
    }

    @Override
    public List<TblWorkFlowFileOnHand> findTblWorkFlowFileOnHand(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblWorkFlowFileOnHandCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblWorkFlowFileOnHand> findByCountTblWorkFlowFileOnHand(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
