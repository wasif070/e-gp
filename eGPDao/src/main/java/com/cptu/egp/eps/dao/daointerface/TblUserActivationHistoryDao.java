package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblUserActivationHistory;
import java.util.List;

/**
 *
 * @author rishita
 */
public interface TblUserActivationHistoryDao extends GenericDao<TblUserActivationHistory> {

    public void addTblUserActivationHistory(TblUserActivationHistory tblObj);

    public void deleteTblUserActivationHistory(TblUserActivationHistory tblObj);

    public void updateTblUserActivationHistory(TblUserActivationHistory tblObj);

    public List<TblUserActivationHistory> getAllTblUserActivationHistory();

    public List<TblUserActivationHistory> findTblUserActivationHistory(Object... values) throws Exception;

    public List<TblUserActivationHistory> findByCountTblUserActivationHistory(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblUserActivationHistoryCount();
}
