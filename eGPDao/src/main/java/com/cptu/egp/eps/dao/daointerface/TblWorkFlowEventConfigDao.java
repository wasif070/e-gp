/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblWorkFlowEventConfig;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public interface TblWorkFlowEventConfigDao extends GenericDao<TblWorkFlowEventConfig> {

    public void addTblWorkFlowEventConfig(TblWorkFlowEventConfig admin);

    public void deleteTblWorkFlowEventConfig(TblWorkFlowEventConfig admin);

    public void updateTblWorkFlowEventConfig(TblWorkFlowEventConfig admin);

    public List<TblWorkFlowEventConfig> getAllTblWorkFlowEventConfig();

    public List<TblWorkFlowEventConfig> findTblWorkFlowEventConfig(Object... values) throws Exception;

    public List<TblWorkFlowEventConfig> findByCountTblWorkFlowEventConfig(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblWorkFlowEventConfigCount();
}
