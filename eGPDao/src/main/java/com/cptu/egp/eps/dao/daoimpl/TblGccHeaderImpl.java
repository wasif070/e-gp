package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblGccHeaderDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblGccHeader;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblGccHeaderImpl extends AbcAbstractClass<TblGccHeader> implements TblGccHeaderDao {

    @Override
    public void addTblGccHeader(TblGccHeader gccHeader){
        super.addEntity(gccHeader);
    }

    @Override
    public void deleteTblGccHeader(TblGccHeader gccHeader) {
        super.deleteEntity(gccHeader);
    }

    @Override
    public void updateTblGccHeader(TblGccHeader gccHeader) {
        super.updateEntity(gccHeader);
    }

    @Override
    public List<TblGccHeader> getAllTblGccHeader() {
        return super.getAllEntity();
    }

    @Override
    public List<TblGccHeader> findTblGccHeader(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblGccHeaderCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblGccHeader> findByCountTblGccHeader(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
