package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblToDoList;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblToDoListDao extends GenericDao<TblToDoList> {

    public void addTblToDoList(TblToDoList tblObj);

    public void deleteTblToDoList(TblToDoList tblObj);

    public void updateTblToDoList(TblToDoList tblObj);

    public List<TblToDoList> getAllTblToDoList();

    public List<TblToDoList> findTblToDoList(Object... values) throws Exception;

    public List<TblToDoList> findByCountTblToDoList(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblToDoListCount();
}
