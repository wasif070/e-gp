package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvCchistoryDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvCchistory;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsSrvCchistoryImpl extends AbcAbstractClass<TblCmsSrvCchistory> implements TblCmsSrvCchistoryDao {

    @Override
    public void addTblCmsSrvCchistory(TblCmsSrvCchistory cmsInvoiceDetails){
        super.addEntity(cmsInvoiceDetails);
    }

    @Override
    public void deleteTblCmsSrvCchistory(TblCmsSrvCchistory cmsInvoiceDetails) {
        super.deleteEntity(cmsInvoiceDetails);
    }

    @Override
    public void updateTblCmsSrvCchistory(TblCmsSrvCchistory cmsInvoiceDetails) {
        super.updateEntity(cmsInvoiceDetails);
    }

    @Override
    public List<TblCmsSrvCchistory> getAllTblCmsSrvCchistory() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvCchistory> findTblCmsSrvCchistory(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvCchistoryCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvCchistory> findByCountTblCmsSrvCchistory(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }


    @Override
    public void updateOrSaveEstCost(List<TblCmsSrvCchistory> estCost) {
       super.updateAll(estCost);
    }
}
