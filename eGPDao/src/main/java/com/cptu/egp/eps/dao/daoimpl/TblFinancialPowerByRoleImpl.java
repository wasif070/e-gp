package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblFinancialPowerByRoleDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblFinancialPowerByRole;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblFinancialPowerByRoleImpl extends AbcAbstractClass<TblFinancialPowerByRole> implements TblFinancialPowerByRoleDao {

    @Override
    public void addTblFinancialPowerByRole(TblFinancialPowerByRole master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblFinancialPowerByRole(TblFinancialPowerByRole master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblFinancialPowerByRole(TblFinancialPowerByRole master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblFinancialPowerByRole> getAllTblFinancialPowerByRole() {
        return super.getAllEntity();
    }

    @Override
    public List<TblFinancialPowerByRole> findTblFinancialPowerByRole(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblFinancialPowerByRoleCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblFinancialPowerByRole> findByCountTblFinancialPowerByRole(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
