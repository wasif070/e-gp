/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsAitConfig;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public interface TblCmsAitConfigDao extends GenericDao<TblCmsAitConfig>{
    
    public void addTblCmsAitConfig(TblCmsAitConfig tblCmsAitConfig);

    public void deleteTblCmsAitConfig(TblCmsAitConfig tblCmsAitConfig);

    public void updateTblCmsAitConfig(TblCmsAitConfig tblCmsAitConfig);

    public List<TblCmsAitConfig> getAllTblCmsAitConfig();

    public List<TblCmsAitConfig> findTblCmsAitConfig(Object... values) throws Exception;

    public List<TblCmsAitConfig> findByCountTblCmsAitConfig(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsAitConfigCount();

}
