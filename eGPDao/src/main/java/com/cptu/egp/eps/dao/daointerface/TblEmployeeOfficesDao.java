package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEmployeeOffices;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblEmployeeOfficesDao extends GenericDao<TblEmployeeOffices> {

    public void addTblEmployeeOffices(TblEmployeeOffices empOffice);

    public void deleteTblEmployeeOffices(TblEmployeeOffices empOffice);

    public void updateTblEmployeeOffices(TblEmployeeOffices empOffice);

    public List<TblEmployeeOffices> getAllTblEmployeeOffices();

    public List<TblEmployeeOffices> findTblEmployeeOffices(Object... values) throws Exception;

    public List<TblEmployeeOffices> findByCountTblEmployeeOffices(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEmployeeOfficesCount();

    // Start GSS Change
    public List<Object> getOfficeHirarchies(int officeId,List<Object> offices)throws Exception;

    public List<Object> getOfficeByIdForHirarchy(int officeId)throws Exception;
    // End GSS Change
}
