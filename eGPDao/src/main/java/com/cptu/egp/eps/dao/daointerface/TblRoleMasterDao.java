package com.cptu.egp.eps.dao.daointerface;


import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblRoleMaster;
import java.util.List;


	public interface TblRoleMasterDao extends GenericDao<TblRoleMaster> {

	public void addTblRoleMaster(TblRoleMaster tblObj);

	public void deleteTblRoleMaster(TblRoleMaster tblObj);

	public void updateTblRoleMaster(TblRoleMaster tblObj);

	public List<TblRoleMaster> getAllTblRoleMaster();

	public List<TblRoleMaster> findTblRoleMaster(Object... values) throws Exception;

	public List<TblRoleMaster> findByCountTblRoleMaster(int firstResult,int maxResult,Object... values) throws Exception;

	public long getTblRoleMasterCount();

	public void updateOrSaveTblRoleMaster(List<TblRoleMaster> tblObj);
}