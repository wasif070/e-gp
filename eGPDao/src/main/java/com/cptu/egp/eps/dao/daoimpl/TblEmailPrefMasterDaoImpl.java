package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblEmailPrefMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEmailPrefMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblEmailPrefMasterDaoImpl extends AbcAbstractClass<TblEmailPrefMaster> implements TblEmailPrefMasterDao {

    @Override
    public void addTblEmailPrefMaster(TblEmailPrefMaster activityMaster){
        super.addEntity(activityMaster);
    }

    @Override
    public void deleteTblEmailPrefMaster(TblEmailPrefMaster activityMaster) {
        super.deleteEntity(activityMaster);
    }

    @Override
    public void updateTblEmailPrefMaster(TblEmailPrefMaster activityMaster) {
        super.updateEntity(activityMaster);
    }

    @Override
    public List<TblEmailPrefMaster> getAllTblEmailPrefMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEmailPrefMaster> findTblEmailPrefMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEmailPrefMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEmailPrefMaster> findByCountTblEmailPrefMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
