/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblLimitedTendersDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblLimitedTenders;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblLimitedTendersImpl extends AbcAbstractClass<TblLimitedTenders> implements TblLimitedTendersDao {

    @Override
    public void addTblLimitedTenders(TblLimitedTenders limitedTenders) {
        super.addEntity(limitedTenders);
    }

    @Override
    public void deleteTblLimitedTenders(TblLimitedTenders limitedTenders) {
        super.deleteEntity(limitedTenders);
    }

    @Override
    public void updateTblLimitedTenders(TblLimitedTenders limitedTenders) {
        super.updateEntity(limitedTenders);
    }

    @Override
    public List<TblLimitedTenders> getAllTblLimitedTenders() {
        return super.getAllEntity();
    }

    @Override
    public List<TblLimitedTenders> findTblLimitedTenders(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblLimitedTendersCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblLimitedTenders> findByCountTblLimitedTenders(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
