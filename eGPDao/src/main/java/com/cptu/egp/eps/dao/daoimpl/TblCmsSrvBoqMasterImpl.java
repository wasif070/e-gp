package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvBoqMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvBoqMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsSrvBoqMasterImpl extends AbcAbstractClass<TblCmsSrvBoqMaster> implements TblCmsSrvBoqMasterDao {

    @Override
    public void addTblCmsSrvBoqMaster(TblCmsSrvBoqMaster master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblCmsSrvBoqMaster(TblCmsSrvBoqMaster master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblCmsSrvBoqMaster(TblCmsSrvBoqMaster master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblCmsSrvBoqMaster> getAllTblCmsSrvBoqMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvBoqMaster> findTblCmsSrvBoqMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvBoqMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvBoqMaster> findByCountTblCmsSrvBoqMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
