package com.cptu.egp.eps.dao.daointerface;

import java.util.List;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblReviewPanel;


public interface TblReviewPanelDao extends GenericDao<TblReviewPanel> {

    public List<TblReviewPanel> getReviewPanels();

    public void addReviewPanel(TblReviewPanel reviewPanel);

    public void updateOrSaveReviewPanel(TblReviewPanel reviewPanel);
}
