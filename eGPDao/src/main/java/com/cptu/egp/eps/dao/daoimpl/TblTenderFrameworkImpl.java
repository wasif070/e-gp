/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderFrameWorkDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderFrameWork;
import java.util.List;

public class TblTenderFrameworkImpl extends AbcAbstractClass<TblTenderFrameWork> implements TblTenderFrameWorkDao{
    @Override
    public void addTblTenderFrameWork(TblTenderFrameWork master){
            super.addEntity(master);
    }
    @Override
    public void deleteTblTenderFrameWork(TblTenderFrameWork master) {
            super.deleteEntity(master);
    }
    @Override
    public void updateTblTenderFrameWork(TblTenderFrameWork master) {
            super.updateEntity(master);
    }
    @Override
    public List<TblTenderFrameWork> getAllTblTenderFrameWork() {
            return super.getAllEntity();
    }
    @Override
    public List<TblTenderFrameWork> findTblTenderFrameWork(Object... values) throws Exception {
            return super.findEntity(values);
    }
    @Override
    public long getTblTenderFrameWorkCount() {
            return super.getEntityCount();
    }
    @Override
    public List<TblTenderFrameWork> findByCountTblTenderFrameWork(int firstResult, int maxResult, Object... values) throws Exception {
            return super.findByCountEntity(firstResult, maxResult, values);
    }
    @Override
    public void updateOrSaveTblTenderFrameWork(List<TblTenderFrameWork> tblObj) {
            super.updateAll(tblObj);
    }
}
