/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblEvalServiceFormsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalServiceForms;
import java.util.List;



/**
 *
 * @author Administrator
 */
public class TblEvalServiceFormsImpl extends AbcAbstractClass<TblEvalServiceForms> implements TblEvalServiceFormsDao{


    @Override
    public void addTblEvalServiceForms(TblEvalServiceForms master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblEvalServiceForms(TblEvalServiceForms master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblEvalServiceForms(TblEvalServiceForms master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblEvalServiceForms> getAllTblEvalServiceForms() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEvalServiceForms> findTblEvalServiceForms(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEvalServiceFormsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalServiceForms> findByCountTblEvalServiceForms(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrInsertAll(List<TblEvalServiceForms> list) {
       super.updateAll(list);
    }

}
