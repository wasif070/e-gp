/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTdsSubClauseDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTdsSubClause;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTdsSubClauseImpl extends AbcAbstractClass<TblTdsSubClause> implements TblTdsSubClauseDao{

    @Override
    public void addTblTdsSubClause(TblTdsSubClause tblTdsSubClause) {
        super.addEntity(tblTdsSubClause);
    }

    @Override
    public List<TblTdsSubClause> findTblTdsSubClause(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblTdsSubClause(TblTdsSubClause tblTdsSubClause) {

        super.deleteEntity(tblTdsSubClause);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblTdsSubClause(TblTdsSubClause tblTdsSubClause) {

        super.updateEntity(tblTdsSubClause);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblTdsSubClause> getAllTblTdsSubClause() {
        return super.getAllEntity();
    }

    @Override
    public long getTblTdsSubClauseCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTdsSubClause> findByCountTblTdsSubClause(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
