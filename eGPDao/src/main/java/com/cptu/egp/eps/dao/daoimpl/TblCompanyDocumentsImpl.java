package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCompanyDocumentsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCompanyDocuments;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCompanyDocumentsImpl extends AbcAbstractClass<TblCompanyDocuments> implements TblCompanyDocumentsDao {

    @Override
    public void addTblCompanyDocuments(TblCompanyDocuments master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblCompanyDocuments(TblCompanyDocuments master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblCompanyDocuments(TblCompanyDocuments master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblCompanyDocuments> getAllTblCompanyDocuments() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCompanyDocuments> findTblCompanyDocuments(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCompanyDocumentsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCompanyDocuments> findByCountTblCompanyDocuments(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
