/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblMessageTrashDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblMessageTrash;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblMessageTrashImpl extends AbcAbstractClass<TblMessageTrash> implements TblMessageTrashDao{

    @Override
    public void addTblMessageTrash(TblMessageTrash tblMessageTrash) {
        super.addEntity(tblMessageTrash);
    }

    @Override
    public List<TblMessageTrash> findTblMessageTrash(Object... values) throws Exception {
        return super.findEntity(values);
        
    }

    @Override
    public void deleteTblMessageTrash(TblMessageTrash tblMessageTrash) {

        super.deleteEntity(tblMessageTrash);
        
    }

    @Override
    public void updateTblMessageTrash(TblMessageTrash tblMessageTrash) {

        super.updateEntity(tblMessageTrash);
        
    }

    @Override
    public List<TblMessageTrash> getAllTblMessageTrash() {
        return super.getAllEntity();
    }

    @Override
    public long getTblMessageTrashCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblMessageTrash> findByCountTblMessageTrash(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
