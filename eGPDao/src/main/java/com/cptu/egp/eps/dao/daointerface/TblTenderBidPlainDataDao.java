/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderBidPlainData;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderBidPlainDataDao extends GenericDao<TblTenderBidPlainData>{

    public void addTblTenderBidPlainData(TblTenderBidPlainData tblObj);

    public void deleteTblTenderBidPlainData(TblTenderBidPlainData tblObj);

    public void updateTblTenderBidPlainData(TblTenderBidPlainData tblObj);

    public List<TblTenderBidPlainData> getAllTblTenderBidPlainData();

    public List<TblTenderBidPlainData> findTblTenderBidPlainData(Object... values) throws Exception;

    public List<TblTenderBidPlainData> findByCountTblTenderBidPlainData(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderBidPlainDataCount();
}
