/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblProcurementTypes;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TblProcurementTypesDao extends GenericDao<TblProcurementTypes>{

    public void addTblProcurementTypes(TblProcurementTypes tblObj);

    public void deleteTblProcurementTypes(TblProcurementTypes tblObj);

    public void updateTblProcurementTypes(TblProcurementTypes tblObj);

    public List<TblProcurementTypes> getAllTblProcurementTypes();

    public List<TblProcurementTypes> findTblProcurementTypes(Object... values) throws Exception;

    public List<TblProcurementTypes> findByCountTblProcurementTypes(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblProcurementTypesCount();
}
