package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvFormMapDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvFormMap;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsSrvFormMapImpl extends AbcAbstractClass<TblCmsSrvFormMap> implements TblCmsSrvFormMapDao {

    @Override
    public void addTblCmsSrvFormMap(TblCmsSrvFormMap cmsWpMaster){
        super.addEntity(cmsWpMaster);
    }

    @Override
    public void deleteTblCmsSrvFormMap(TblCmsSrvFormMap cmsWpMaster) {
        super.deleteEntity(cmsWpMaster);
    }

    @Override
    public void updateTblCmsSrvFormMap(TblCmsSrvFormMap cmsWpMaster) {
        super.updateEntity(cmsWpMaster);
    }

    @Override
    public List<TblCmsSrvFormMap> getAllTblCmsSrvFormMap() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvFormMap> findTblCmsSrvFormMap(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvFormMapCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvFormMap> findByCountTblCmsSrvFormMap(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsSrvFormMap> estCost) {
         super.updateAll(estCost);
    }
}
