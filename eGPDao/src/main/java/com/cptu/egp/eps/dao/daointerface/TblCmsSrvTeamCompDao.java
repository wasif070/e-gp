package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvTeamComp;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsSrvTeamCompDao extends GenericDao<TblCmsSrvTeamComp> {

    public void addTblCmsSrvTeamComp(TblCmsSrvTeamComp event);

    public void deleteTblCmsSrvTeamComp(TblCmsSrvTeamComp event);

    public void updateTblCmsSrvTeamComp(TblCmsSrvTeamComp event);

    public List<TblCmsSrvTeamComp> getAllTblCmsSrvTeamComp();

    public List<TblCmsSrvTeamComp> findTblCmsSrvTeamComp(Object... values) throws Exception;

    public List<TblCmsSrvTeamComp> findByCountTblCmsSrvTeamComp(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsSrvTeamCompCount();

    public void updateOrSaveEstCost(List<TblCmsSrvTeamComp> estCost);
}
