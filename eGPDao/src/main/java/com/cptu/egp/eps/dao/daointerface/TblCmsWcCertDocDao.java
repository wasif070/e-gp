/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsWcCertDoc;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public interface TblCmsWcCertDocDao extends GenericDao<TblCmsWcCertDoc> {

    public void addTblCmsWcCertDoc(TblCmsWcCertDoc tblCmsWcCertDoc);

    public void deleteTblCmsWcCertDoc(TblCmsWcCertDoc tblCmsWcCertDoc);

    public void updateTblCmsWcCertDoc(TblCmsWcCertDoc tblCmsWcCertDoc);

    public List<TblCmsWcCertDoc> getAllTblCmsWcCertDoc();

    public List<TblCmsWcCertDoc> findTblCmsWcCertDoc(Object... values) throws Exception;

    public List<TblCmsWcCertDoc> findByCountTblCmsWcCertDoc(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsWcCertDocCount();
}
