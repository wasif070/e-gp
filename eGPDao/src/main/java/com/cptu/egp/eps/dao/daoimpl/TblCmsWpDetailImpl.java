package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsWpDetailDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsWpDetail;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsWpDetailImpl extends AbcAbstractClass<TblCmsWpDetail> implements TblCmsWpDetailDao {

    @Override
    public void addTblCmsWpDetail(TblCmsWpDetail cmsWpDetail){
        super.addEntity(cmsWpDetail);
    }

    @Override
    public void deleteTblCmsWpDetail(TblCmsWpDetail cmsWpDetail) {
        super.deleteEntity(cmsWpDetail);
    }

    @Override
    public void updateTblCmsWpDetail(TblCmsWpDetail cmsWpDetail) {
        super.updateEntity(cmsWpDetail);
    }

    @Override
    public List<TblCmsWpDetail> getAllTblCmsWpDetail() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsWpDetail> findTblCmsWpDetail(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsWpDetailCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsWpDetail> findByCountTblCmsWpDetail(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsWpDetail> estCost) throws Exception {
       super.updateAll(estCost);
    }
}
