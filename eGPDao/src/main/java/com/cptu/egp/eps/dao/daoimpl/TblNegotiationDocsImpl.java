/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblNegotiationDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblNegotiationDocs;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblNegotiationDocsImpl extends AbcAbstractClass<TblNegotiationDocs> implements TblNegotiationDocsDao{

    @Override
    public void addTblNegotiationDocs(TblNegotiationDocs negotiationDocs) {
        super.addEntity(negotiationDocs);
    }

    @Override
    public List<TblNegotiationDocs> findTblNegotiationDocs(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblNegotiationDocs(TblNegotiationDocs negotiationDocs) {

        super.deleteEntity(negotiationDocs);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblNegotiationDocs(Collection negotiationDocs) {
        super.deleteAll(negotiationDocs);
    }

    @Override
    public void updateTblNegotiationDocs(TblNegotiationDocs negotiationDocs) {

        super.updateEntity(negotiationDocs);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblNegotiationDocs> getAllTblNegotiationDocs() {
        return super.getAllEntity();
    }

    @Override
    public long getTblNegotiationDocsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblNegotiationDocs> findByCountTblNegotiationDocs(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
