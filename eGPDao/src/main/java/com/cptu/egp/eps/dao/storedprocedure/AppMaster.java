/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Ramesh.Janagondakuru
 */
public class AppMaster extends StoredProcedure {

private static final Logger LOGGER = Logger.getLogger(AppMaster.class);
public AppMaster(BasicDataSource dataSource, String procName){

     this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("v_FinancialYear_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_BudgetType_intInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_ProjectId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_ProjectName_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_OfficeId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_EmployeeId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_AAEmployeeId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_AppCode_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_CreatedBy_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_DepartmentId_insInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_AAProcurementRoleId_intInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_Action_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_App_inInt", Types.INTEGER));
        //Added by rased, 09-05-2015
        this.declareParameter(new SqlParameter("v_AppType_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_EntrustingAgency_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_RefAppId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_ReviseCount_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_LastRevisionDate_inDateTime", Types.DATE));
        this.declareParameter(new SqlParameter("v_ActivityName_inVc", Types.VARCHAR));
}

/**
 *
 * Execute stored procedure : p_ins_upd_appmaster
 * to performed insert,update,delete operation for APPMaster.
 * @param financialYear
 * @param budgetType
 * @param projectId
 * @param projectName
 * @param officeId
 * @param employeeId
 * @param aAEmployeeId
 * @param appCode
 * @param createdBy
 * @param departmentId
 * @param aAProcurementRoleId
 * @param action
 * @param app
 * @param appType
 * @param entrustingAgency
 * @param refAppId
 * @param revisionCount
 * @param lastRevisionDate
     * @param ActivityName
 * @return CommonSPReturn list contains flags indicate operation success status.
 */
 public List<CommonSPReturn> executeProcedure(String financialYear,Integer budgetType,Integer projectId,String projectName,Integer officeId,Integer employeeId,Integer aAEmployeeId,String appCode,Integer createdBy,Integer departmentId,Integer aAProcurementRoleId,String action,Integer app,
                    String appType, String entrustingAgency, Integer refAppId, int revisionCount, Date lastRevisionDate, String ActivityName){
//     java.sql.Date LastRevisionDate = new java.sql.Date(0);
//     if (lastRevisionDate!=null) {         
//        LastRevisionDate = new java.sql.Date(lastRevisionDate.getTime());
//     }
     Map inParams = new HashMap();
        inParams.put("v_FinancialYear_inVc", financialYear);
        inParams.put("v_BudgetType_intInt", budgetType);
        inParams.put("v_ProjectId_inInt", projectId);
        inParams.put("v_ProjectName_inVc", projectName);
        inParams.put("v_OfficeId_inInt", officeId);
        inParams.put("v_EmployeeId_inInt", employeeId);
        inParams.put("v_AAEmployeeId_inInt", aAEmployeeId);
        inParams.put("v_AppCode_inVc", appCode);
        inParams.put("v_CreatedBy_inInt", createdBy);
        inParams.put("v_DepartmentId_insInt", departmentId);
        inParams.put("v_AAProcurementRoleId_intInt", aAProcurementRoleId);
        inParams.put("v_Action_inVc", action);
        inParams.put("v_App_inInt", app);
        //added by rased
        inParams.put("v_AppType_inVc", appType);
        inParams.put("v_EntrustingAgency_inVc", entrustingAgency);
        inParams.put("v_RefAppId_inInt", refAppId);
        inParams.put("v_ReviseCount_inInt", revisionCount);
        inParams.put("v_ActivityName_inVc", ActivityName);
        inParams.put("v_LastRevisionDate_inDateTime", lastRevisionDate!=null? new java.sql.Date(lastRevisionDate.getTime()) : lastRevisionDate);

     this.compile();
        List<CommonSPReturn> details = new ArrayList<CommonSPReturn>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonSPReturn commonSPReturn = new CommonSPReturn();
                commonSPReturn.setId((BigDecimal)linkedHashMap.get("appId"));
                commonSPReturn.setFlag((Boolean)linkedHashMap.get("flag"));
                commonSPReturn.setMsg((String) linkedHashMap.get("Message"));
                details.add(commonSPReturn);           
            }
        } catch (Exception e) {
           LOGGER.error("AppMaster : " + e);
        }
        return details;
 }

}
