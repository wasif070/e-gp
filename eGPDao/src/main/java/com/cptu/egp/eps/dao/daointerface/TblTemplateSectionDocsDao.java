/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTemplateSectionDocs;
import java.util.List;

/**
 *
 * @author yanki
 */
public interface TblTemplateSectionDocsDao extends GenericDao<TblTemplateSectionDocs>{

    /**
     * Add Template Section Doc
     * @param templateSectionDocs
     */
    public void addTblTemplateSectionDocs(TblTemplateSectionDocs templateSectionDocs);

    /**
     * Delete entry from Template Section Doc
     * @param templateSectionDocs
     */
    public void deleteTblTemplateSectionDocs(TblTemplateSectionDocs templateSectionDocs);

    /**
     * Update entry for Template Section Doc
     * @param templateSectionDocs
     */
    public void updateTblTemplateSectionDocs(TblTemplateSectionDocs templateSectionDocs);

    /**
     * Get All Template Section Docs Details
     * @return = List of TblTemplateSectionDocs Objects
     */
    public List<TblTemplateSectionDocs> getAllTblTemplateSectionDocs();

    /**
     * Get Template Section Docs base on search criteria.
     * @param values = Object array having search criteria.
     * @return = List of TblTemplateSectionDocs Objects
     * @throws Exception
     */
    public List<TblTemplateSectionDocs> findTblTemplateSectionDocs(Object... values) throws Exception;

}
