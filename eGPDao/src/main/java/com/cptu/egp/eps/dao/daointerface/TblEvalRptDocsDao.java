package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalRptDocs;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblEvalRptDocsDao extends GenericDao<TblEvalRptDocs> {

    public void addTblEvalRptDocs(TblEvalRptDocs tblEvalRptDocs);

    public void deleteTblEvalRptDocs(TblEvalRptDocs tblEvalRptDocs);

    public void deleteAllTblEvalRptDocs(Collection tblEvalRptDocs);

    public void updateTblEvalRptDocs(TblEvalRptDocs tblEvalRptDocs);

    public List<TblEvalRptDocs> getAllTblEvalRptDocs();

    public List<TblEvalRptDocs> findTblEvalRptDocs(Object... values) throws Exception;

    public List<TblEvalRptDocs> findByCountTblEvalRptDocs(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalRptDocsCount();
}
