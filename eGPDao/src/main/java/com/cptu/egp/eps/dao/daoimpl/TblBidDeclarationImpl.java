package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblBidDeclarationDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblBidDeclaration;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblBidDeclarationImpl extends AbcAbstractClass<TblBidDeclaration> implements TblBidDeclarationDao {

    @Override
    public void addTblBidDeclaration(TblBidDeclaration master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblBidDeclaration(TblBidDeclaration master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblBidDeclaration(TblBidDeclaration master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblBidDeclaration> getAllTblBidDeclaration() {
        return super.getAllEntity();
    }

    @Override
    public List<TblBidDeclaration> findTblBidDeclaration(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblBidDeclarationCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblBidDeclaration> findByCountTblBidDeclaration(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
