/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Ramesh
 */
public class SPReTenderNotice extends StoredProcedure{
    private static final Logger LOGGER = Logger.getLogger(SPReTenderNotice.class);
    public SPReTenderNotice(BasicDataSource dataSource,String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("v_Action_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Tenderid_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_EventType_inVc", Types.VARCHAR));
    }

    /**
     * Execute stored procedure : p_add_retendernotice
     * To perform Insert/Update Tender Notice.
     * @param action
     * @param tenderId
     * @param eventType
     * @return CommonMsgChk list contains flags indicate operation successful or not.
     */
    public List<CommonMsgChk> exeProcedure(String action,Integer tenderId,String eventType ){
         Map inParams = new HashMap();
          inParams.put("v_Action_inVc",action);
           inParams.put("v_Tenderid_inInt",tenderId);
           inParams.put("v_EventType_inVc",eventType);
           this.compile();
          List<CommonMsgChk> details = new ArrayList<CommonMsgChk>();
           try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonMsgChk commonMsgChk = new CommonMsgChk();
                commonMsgChk.setFlag((Boolean) linkedHashMap.get("flag"));
                commonMsgChk.setMsg((String) linkedHashMap.get("msg"));
                details.add(commonMsgChk);
            }
        } catch (Exception e) {
        LOGGER.error("SPReTenderNotice  : "+ e);
        }
        return details;
    }

}
