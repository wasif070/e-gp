package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblConfigDocFees;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblConfigDocFeesDao extends GenericDao<TblConfigDocFees> {

    public void addTblConfigDocFees(TblConfigDocFees tblObj);

    public void deleteTblConfigDocFees(TblConfigDocFees tblObj);

    public void updateTblConfigDocFees(TblConfigDocFees tblObj);

    public List<TblConfigDocFees> getAllTblConfigDocFees();

    public List<TblConfigDocFees> findTblConfigDocFees(Object... values) throws Exception;

    public List<TblConfigDocFees> findByCountTblConfigDocFees(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblConfigDocFeesCount();

    public void updateInsAllConfigDocFees(List<TblConfigDocFees> list);

}
