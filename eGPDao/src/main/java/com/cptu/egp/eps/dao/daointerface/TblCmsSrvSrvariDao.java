package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvSrvari;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsSrvSrvariDao extends GenericDao<TblCmsSrvSrvari> {

    public void addTblCmsSrvSrvari(TblCmsSrvSrvari event);

    public void deleteTblCmsSrvSrvari(TblCmsSrvSrvari event);

    public void updateTblCmsSrvSrvari(TblCmsSrvSrvari event);

    public List<TblCmsSrvSrvari> getAllTblCmsSrvSrvari();

    public List<TblCmsSrvSrvari> findTblCmsSrvSrvari(Object... values) throws Exception;

    public List<TblCmsSrvSrvari> findByCountTblCmsSrvSrvari(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsSrvSrvariCount();

    public void updateOrSaveEstCost(List<TblCmsSrvSrvari> srvTcvaris);
}
