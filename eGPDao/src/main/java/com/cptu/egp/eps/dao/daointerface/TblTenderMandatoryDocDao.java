/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderMandatoryDoc;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TblTenderMandatoryDocDao extends GenericDao<TblTenderMandatoryDoc>{


    public void addTblTenderMandatoryDoc(TblTenderMandatoryDoc tblObj);

    public void deleteTblTenderMandatoryDoc(TblTenderMandatoryDoc tblObj);

    public void updateTblTenderMandatoryDoc(TblTenderMandatoryDoc tblObj);

    public List<TblTenderMandatoryDoc> getAllTblTenderMandatoryDoc();

    public List<TblTenderMandatoryDoc> findTblTenderMandatoryDoc(Object... values) throws Exception;

    public List<TblTenderMandatoryDoc> findByCountTblTenderMandatoryDoc(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderMandatoryDocCount();
    public void updateOrInsertAll(List<TblTenderMandatoryDoc> list);

}
