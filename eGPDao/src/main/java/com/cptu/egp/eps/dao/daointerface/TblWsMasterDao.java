/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblWsMaster;
import java.util.List;

/**
 *
 * @author Sreenu
 */
public interface TblWsMasterDao extends GenericDao<TblWsMaster>{

    public void addTblWsMaster(TblWsMaster tblWsMaster);

    public void deleteTblWsMaster(TblWsMaster tblWsMaster);

    public void updateTblWsMaster(TblWsMaster tblWsMaster);

    public List<TblWsMaster> getAllTblWsMaster();

    public List<TblWsMaster> findTblWsMaster(Object... values) throws Exception;

    public List<TblWsMaster> findByCountTblWsMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblWsMasterCount();

    public List<TblWsMaster> getRequiredTblWsMasterList(String whereCondition);

}
