/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.generic;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
/**
 * author taher
 */
public abstract class AbcAbstractClass<EntityType> extends HibernateDaoSupport implements GenericDao<EntityType> {

    private final Logger logger = Logger.getLogger(AbcAbstractClass.class);
    private Class<EntityType> persistentClass;

    public Class<EntityType> getPersistentClass() {
        return persistentClass;
    }

    public void setPersistentClass(Class<EntityType> persistentClass) {
        this.persistentClass = persistentClass;
    }

    public AbcAbstractClass() {
        super();
        this.persistentClass = (Class<EntityType>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public void addEntity(EntityType entity) {
        try{
        getHibernateTemplate().save(entity);
        }catch(Exception ex){
            logger.error("addEntity :"+ex);
    }
    }

    @Override
    public void deleteEntity(EntityType entity) {
        try{
        getHibernateTemplate().delete(entity);
        }catch(Exception ex){
            logger.error("deleteEntity :"+ex);
    }
    }


    @Override
    public void deleteAll(Collection entity){
        try{
        getHibernateTemplate().deleteAll(entity);
        }catch(Exception ex){
            logger.error("deleteAll :"+ex);
    }
    }

    @Override
    public void updateEntity(EntityType entity) {
        try{
        getHibernateTemplate().update(entity);
        }catch(Exception ex){
            logger.error("updateEntity :"+ex);
    }
    }

    @Override
    public void updateAll(Collection<EntityType> collection) {
        try {
            for(EntityType item:collection)
            {
                getHibernateTemplate().saveOrUpdate(item);
            }
        } catch (Exception e) {
            logger.error("updateAllIterator :"+e);
        }
    }

    @Override
    public List<EntityType> getAllEntity() {

        List<EntityType> lst = null;
        try {
             lst= getHibernateTemplate().loadAll(getPersistentClass());
        } catch (Exception e) {
            logger.error("updateAll :"+e);
        }
        //getHibernateTemplate().getSessionFactory().getCurrentSession().close();6666666666
        //getHibernateTemplate().getSessionFactory().close();
        return lst;
    }

    @Override
    public List<EntityType> findEntity(Object... values) throws Exception {

        HibernateTemplate hibernateTemplate = getHibernateTemplate();
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(persistentClass);
        List<EntityType> lst = new ArrayList<EntityType>();
            try{
        int len = values.length % 3;
        boolean ordered = false;

        if (len != 0) {
            throw new Exception("Arguments Must be triplet of [field, expression(e.g. eq,lt,gt...), value]");
        }

        len = values.length / 3;

        for (int i = 0, j = 0; i < len; i++, j += 3) {

            Operation_enum operation = (Operation_enum) values[j + 1];

            switch (operation) {
                case EQ:
                    detachedCriteria.add(Restrictions.eq(values[j].toString(), values[j + 2]));
                    break;
                case GE:
                    detachedCriteria.add(Restrictions.ge(values[j].toString(), values[j + 2]));
                    break;
                case GT:
                    detachedCriteria.add(Restrictions.gt(values[j].toString(), values[j + 2]));
                    break;
                case LE:
                    detachedCriteria.add(Restrictions.le(values[j].toString(), values[j + 2]));
                    break;
                case LT:
                    detachedCriteria.add(Restrictions.lt(values[j].toString(), values[j + 2]));
                    break;
                case NE:
                    detachedCriteria.add(Restrictions.ne(values[j].toString(), values[j + 2]));
                    break;
                case LIKE:
                    detachedCriteria.add(Restrictions.ilike(values[j].toString(), values[j + 2]));
                    break;
                case OR:
                    detachedCriteria.add(Restrictions.or(Restrictions.ilike(values[j].toString(), values[j + 2]), Restrictions.ilike(values[j + 3].toString(), values[j + 5])));
                    j = j + 3;
                    i++;
                    break;
                case IN:
                    detachedCriteria.add(Restrictions.in(values[j].toString(), (Object[]) values[j + 2]));
                    break;
                case ORDERBY:
                    ordered = true;
                    Operation_enum order = (Operation_enum) values[j + 2];
                    if (order == Operation_enum.ASC) {
                        detachedCriteria.addOrder(Order.asc(values[j].toString()));
                    } else if (order == Operation_enum.DESC) {
                        detachedCriteria.addOrder(Order.desc(values[j].toString()));
                    } else {
                        throw new Exception("Order by can be ASC or DESC only. use Enum for to specify this.");
                    }
                    break;
                        default:
                            logger.debug("findEntity(): Arguments Must be triplet of [field, expression(e.g. eq,lt,gt...), value]");
                            logger.error("findEntity(): Arguments Must be triplet of [field, expression(e.g. eq,lt,gt...), value]");
            }
        }

        if (!ordered) {
            if (!persistentClass.getSimpleName().startsWith("Vw")) {
                String firstFieldNameFull = persistentClass.getDeclaredFields()[0].toString();
                int intlastdot = firstFieldNameFull.lastIndexOf('.');
                String firstFieldName = firstFieldNameFull.substring(intlastdot + 1);

                        logger.debug("Sorted on = " + firstFieldName + " of " + persistentClass.getSimpleName() + " class");

                detachedCriteria.addOrder(Order.asc(firstFieldName));
            }
        }



            lst = (List<EntityType>) hibernateTemplate.findByCriteria(detachedCriteria);
        }catch(Exception ex){
            logger.error("findEntity :"+ex);
        }
        return lst;
    }

    @Override
    public List<EntityType> findByCountEntity(int firstResult,int maxResult,Object... values) throws Exception {

        HibernateTemplate hibernateTemplate = getHibernateTemplate();
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(persistentClass);
        List<EntityType> lst = new ArrayList<EntityType>();
            try{

        int len = values.length % 3;
        boolean ordered = false;

        if (len != 0) {
            throw new Exception("Arguments Must be triplet of [field, expression(e.g. eq,lt,gt...), value]");
        }

        len = values.length / 3;

        for (int i = 0, j = 0; i < len; i++, j += 3) {

            Operation_enum operation = (Operation_enum) values[j + 1];

            switch (operation) {
                case EQ:
                    detachedCriteria.add(Restrictions.eq(values[j].toString(), values[j + 2]));
                    break;
                case GE:
                    detachedCriteria.add(Restrictions.ge(values[j].toString(), values[j + 2]));
                    break;
                case GT:
                    detachedCriteria.add(Restrictions.gt(values[j].toString(), values[j + 2]));
                    break;
                case LE:
                    detachedCriteria.add(Restrictions.le(values[j].toString(), values[j + 2]));
                    break;
                case LT:
                    detachedCriteria.add(Restrictions.lt(values[j].toString(), values[j + 2]));
                    break;
                case NE:
                    detachedCriteria.add(Restrictions.ne(values[j].toString(), values[j + 2]));
                    break;
                case LIKE:
                    detachedCriteria.add(Restrictions.ilike(values[j].toString(), values[j + 2]));
                    break;
                case OR:
                    detachedCriteria.add(Restrictions.or(Restrictions.ilike(values[j].toString(), values[j + 2]), Restrictions.ilike(values[j + 3].toString(), values[j + 5])));
                    j = j + 3;
                    i++;
                    break;
                case IN:
                    detachedCriteria.add(Restrictions.in(values[j].toString(), (Object[]) values[j + 2]));
                    break;
                case ORDERBY:
                    ordered = true;
                    Operation_enum order = (Operation_enum) values[j + 2];
                    if (order == Operation_enum.ASC) {
                        detachedCriteria.addOrder(Order.asc(values[j].toString()));
                    } else if (order == Operation_enum.DESC) {
                        detachedCriteria.addOrder(Order.desc(values[j].toString()));
                    } else {
                        throw new Exception("Order by can be ASC or DESC only. use Enum for to specify this.");
                    }
                    break;
                            default:
                                logger.debug("findByCountEntity(): Arguments Must be triplet of [field, expression(e.g. eq,lt,gt...), value]");
                                logger.error("findByCountEntity(): Arguments Must be triplet of [field, expression(e.g. eq,lt,gt...), value]");
            }
        }

        if (!ordered) {
            if (!persistentClass.getSimpleName().startsWith("Vw")) {
                String firstFieldNameFull = persistentClass.getDeclaredFields()[0].toString();
                int intlastdot = firstFieldNameFull.lastIndexOf('.');
                String firstFieldName = firstFieldNameFull.substring(intlastdot + 1);

                            logger.debug("Sorted on = " + firstFieldName + " of " + persistentClass.getSimpleName() + " class");

                detachedCriteria.addOrder(Order.asc(firstFieldName));
            }
        }
            lst = (List<EntityType>) hibernateTemplate.findByCriteria(detachedCriteria,firstResult,maxResult);
        }catch(Exception ex){
            logger.error("findByCountEntity() :" + ex);
        }
        return lst;
    }

    /*
     * Below is to create Projection Ex. count, rowcount, max, min etc
     * See Projection.Ctrl+Space
     */
    @Override
    public long getEntityCount() {
        long count = 0;
        try {
        Criteria c = getSession().createCriteria(persistentClass).setProjection(Projections.rowCount());
            count = (Long) c.list().get(0);
            logger.debug("getEntityCount :: " + count);
        } catch (Exception e) {
            logger.error("getEntityCount :"+e);
        }

        return count;
    }

    @Override
    public List<Object[]> createQuery(String query) {
        Query q = null;
        try {
            q = getSession().createQuery(query);
            logger.debug("createQuery :: " + q.getQueryString());
        } catch (Exception e) {
            logger.error("createQuery :"+e);
    }
        return q==null?null:q.list();
    }

    @Override
    public List<Object[]> createByCountQuery(String query,int firstResult,int maxResult) {
        Query q = null;
        try {
            q = getSession().createQuery(query);
        q.setFirstResult(firstResult);
        q.setMaxResults(maxResult);
            logger.debug("Query By Count :: " + q.getQueryString());
        } catch (Exception e) {
            logger.error("createByCountQuery :"+e);
    }
        return q==null?null:q.list();
    }
    @Override
    public long countForQuery(String from,String where){
        Query q = null;
        try {
            q = getSession().createQuery("select count(*) as count from "+from+" where "+where);
            logger.debug("Query :: " + q.getQueryString());
        } catch (Exception e) {
            logger.error("countForQuery :: " + e);
    }
            return q==null?0:(Long)q.list().get(0);
    }

     @Override
    public List<Object> singleColQuery(String query) {
     Query q = null;
         try {
                q = getSession().createQuery(query);
                logger.debug("Query :: " + q.getQueryString());
         } catch (Exception e) {
             logger.error("singleColQuery :: " + e);
    }
        return q==null?null:q.list();
    }

     @Override
    public int updateDeleteQuery(String query) {
         Query q = null;
         try{
            q = getSession().createQuery(query);
            logger.debug("Query :: " + q.getQueryString());
         }catch(Exception e){
             logger.error("updateDeleteQuery :: " + e);
    }
        return q==null?0:q.executeUpdate();
    }

     @Override
    public int updateDeleteSQLQuery(String query) {
         Query q = null;
         try {
                q = getSession().createSQLQuery(query);
                logger.debug("Query :: " + q.getQueryString());
         } catch (Exception e) {
             logger.error("updateDeleteSQLQuery :: " + e);
    }

        return q==null?0:q.executeUpdate();
    }

    /*
     @Override
     public List<Object[]> runNativeSQLQry(String query) {
         Query q = null;
         List<Object[]> lstObj=new ArrayList<Object[]>();
         try {
                q = getSession().createSQLQuery(query);
                logger.debug("Query :: " + q.getQueryString());
             } catch (Exception e) {
             logger.error("native SQL query :: " + e);
         }

         lstObj=q.list();
        return lstObj;
    }
    */

    // START code added by GSS Team for Complaint Management
    public Object getEntity(int id){
    	 Query q = null;
    	 Object row=null;
         try {
        	 q = getSession().createQuery("from " + (persistentClass.getName())   + " where complaintId=" +id);
        	 for(Iterator it=q.iterate();it.hasNext();){
                  row = (Object) it.next();
        	 }
       logger.debug("Query :: " + q.getQueryString());
         } catch (Exception e) {
             logger.error("updateDeleteSQLQuery :: " + e);
         }
         return row;


     }

     public List<Object> createQueryForObject(String query) {
         Query q = null;
         try {
         	System.out.println("in abc start create query");
             q = getSession().createQuery(query);
             System.out.println("in abc abstract class list size is"+q.list().size());
             logger.debug("createQuery :: " + q.getQueryString());
         } catch (Exception e) {
             logger.error("createQuery :"+e);
         }
         return q==null?null:q.list();
     }


     public List<Object> nativeSQLQuery(String query) {
         Query q = null;
         List<Object> lstObj=new ArrayList<Object>();
         try {
                q = getSession().createSQLQuery(query);
                logger.debug("Query :: " + q.getQueryString());
             } catch (Exception e) {
             logger.error("native SQL query :: " + e);
         }

         lstObj=q.list();
        return lstObj;
    }
    // END code added by GSS Team for Complaint Management

	/**
     *
     * @param query
     * @param var
     * @return
     */
    @Override
    public List<Object[]> createSQLQuery(String query, Map<String, Object> var) {
        Query q = null;
        q = getSession().createSQLQuery(query);
        setQueryMap(q, var);
        return q == null ? null : q.list();
    }

             private void setQueryMap(Query q,Map<String, Object> var){
        if(var!=null){
            Set<String> varnames = var.keySet();
            for (String key : varnames) {
                Object val = var.get(key);
                if (val instanceof String) {
                    q.setString(key, val.toString());
                } else if (val instanceof Integer) {
                    q.setInteger(key, (Integer) val);
                } else if (val instanceof Date) {
                    q.setDate(key, (Date) val);
                }else if (val instanceof Long) {
                    q.setLong(key, (Long) val);
                }else if (val instanceof Object[]) {
                    q.setParameterList(key,(Object[]) val);
                }else if (val instanceof Collection) {
                    q.setParameterList(key,(Collection) val);
                }
            }
        }
    }
}
