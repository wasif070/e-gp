/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblQuizAnswer;
import java.util.List;

/**
 *
 * @author feroz
 */
public interface TblQuizAnswerDao extends GenericDao<TblQuizAnswer>{
    
    public void addTblQuizAnswer(TblQuizAnswer tblQuizAnswer);

    public void deleteTblQuizAnswer(TblQuizAnswer tblQuizAnswer);

    public void updateTblQuizAnswer(TblQuizAnswer tblQuizAnswer);

    public List<TblQuizAnswer> getAllTblQuizAnswer();
    
    /**
     *
     * @param values
     * @return
     */
    public List<TblQuizAnswer> findTblQuizAnswer(Object... values);
    
}
