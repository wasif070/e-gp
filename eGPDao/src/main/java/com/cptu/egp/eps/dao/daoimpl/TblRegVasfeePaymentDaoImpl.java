package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblRegVasfeePaymentDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblRegVasfeePayment;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblRegVasfeePaymentDaoImpl extends AbcAbstractClass<TblRegVasfeePayment> implements TblRegVasfeePaymentDao {

    @Override
    public void addTblRegVasfeePayment(TblRegVasfeePayment activityMaster){
        super.addEntity(activityMaster);
    }

    @Override
    public void deleteTblRegVasfeePayment(TblRegVasfeePayment activityMaster) {
        super.deleteEntity(activityMaster);
    }

    @Override
    public void updateTblRegVasfeePayment(TblRegVasfeePayment activityMaster) {
        super.updateEntity(activityMaster);
    }

    @Override
    public List<TblRegVasfeePayment> getAllTblRegVasfeePayment() {
        return super.getAllEntity();
    }

    @Override
    public List<TblRegVasfeePayment> findTblRegVasfeePayment(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblRegVasfeePaymentCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblRegVasfeePayment> findByCountTblRegVasfeePayment(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
