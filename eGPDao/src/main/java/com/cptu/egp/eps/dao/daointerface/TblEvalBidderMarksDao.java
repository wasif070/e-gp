package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalBidderMarks;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblEvalBidderMarksDao extends GenericDao<TblEvalBidderMarks> {

    public void addTblEvalBidderMarks(TblEvalBidderMarks tblEvalBidderMarks);

    public void deleteTblEvalBidderMarks(TblEvalBidderMarks tblEvalBidderMarks);

    public void updateTblEvalBidderMarks(TblEvalBidderMarks tblEvalBidderMarks);

    public List<TblEvalBidderMarks> getAllTblEvalBidderMarks();

    public List<TblEvalBidderMarks> findTblEvalBidderMarks(Object... values) throws Exception;

    public List<TblEvalBidderMarks> findByCountTblEvalBidderMarks(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalBidderMarksCount();

   public void updateInsAllEvalBidderMarks(List<TblEvalBidderMarks> tblEvalBidderMarks);

}
