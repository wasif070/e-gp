package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblMediaContentDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblMediaContent;
import java.util.List;

/**
 *
 * @author Ketan Prajapati
 */
public class TblMediaContentImpl extends AbcAbstractClass<TblMediaContent> implements TblMediaContentDao {

    @Override
    public void addTblMediaContent(TblMediaContent master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblMediaContent(TblMediaContent master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblMediaContent(TblMediaContent master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblMediaContent> getAllTblMediaContent() {
        return super.getAllEntity();
    }

    @Override
    public List<TblMediaContent> findTblMediaContent(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblMediaContentCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblMediaContent> findByCountTblMediaContent(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
