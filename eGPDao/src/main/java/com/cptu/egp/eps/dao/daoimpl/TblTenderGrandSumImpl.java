/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderGrandSumDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderGrandSum;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderGrandSumImpl extends AbcAbstractClass<TblTenderGrandSum> implements TblTenderGrandSumDao {

    @Override
    public void addTblTenderGrandSum(TblTenderGrandSum admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblTenderGrandSum(TblTenderGrandSum admin) {
        super.deleteEntity(admin);    
    }

    @Override
    public void updateTblTenderGrandSum(TblTenderGrandSum admin) {
        super.updateEntity(admin);
    }

    @Override
    public List<TblTenderGrandSum> getAllTblTenderGrandSum() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderGrandSum> findTblTenderGrandSum(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderGrandSumCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderGrandSum> findByCountTblTenderGrandSum(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
