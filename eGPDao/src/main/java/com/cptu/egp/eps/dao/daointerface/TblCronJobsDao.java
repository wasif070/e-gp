package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCronJobs;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCronJobsDao extends GenericDao<TblCronJobs> {

    public void addTblCronJobs(TblCronJobs tblObj);

    public void deleteTblCronJobs(TblCronJobs tblObj);

    public void updateTblCronJobs(TblCronJobs tblObj);

    public List<TblCronJobs> getAllTblCronJobs();

    public List<TblCronJobs> findTblCronJobs(Object... values) throws Exception;

    public List<TblCronJobs> findByCountTblCronJobs(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCronJobsCount();
}
