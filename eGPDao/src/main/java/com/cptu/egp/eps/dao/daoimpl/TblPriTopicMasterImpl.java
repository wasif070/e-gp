package com.cptu.egp.eps.dao.daoimpl;


import com.cptu.egp.eps.dao.daointerface.TblPriTopicMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblPriTopicMaster;
import java.util.List;


public class TblPriTopicMasterImpl extends AbcAbstractClass<TblPriTopicMaster> implements TblPriTopicMasterDao {

	@Override 
	public void addTblPriTopicMaster(TblPriTopicMaster master){
		super.addEntity(master);
	}
	@Override 
	public void deleteTblPriTopicMaster(TblPriTopicMaster master) {
		super.deleteEntity(master);
	}
	@Override 
	public void updateTblPriTopicMaster(TblPriTopicMaster master) {
		super.updateEntity(master);
	}
	@Override 
	public List<TblPriTopicMaster> getAllTblPriTopicMaster() {
		return super.getAllEntity();
	}
	@Override 
	public List<TblPriTopicMaster> findTblPriTopicMaster(Object... values) throws Exception {
		return super.findEntity(values);
	}
	@Override 
	public long getTblPriTopicMasterCount() {
		return super.getEntityCount();
	}
	@Override 
	public List<TblPriTopicMaster> findByCountTblPriTopicMaster(int firstResult, int maxResult, Object... values) throws Exception {
		return super.findByCountEntity(firstResult, maxResult, values);
	}
	@Override 
	public void updateOrSaveTblPriTopicMaster(List<TblPriTopicMaster> tblObj) {
		super.updateAll(tblObj);
	}
}