/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblPromisProcessDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblPromisProcess;
import java.util.List;

/**
 *
 * @author Sudhir Chavhah
 * 
 */
public class TblPromisProcessImpl extends AbcAbstractClass<TblPromisProcess> implements TblPromisProcessDao{

    @Override
    public void addTblPromisProcess(TblPromisProcess admin) {
        super.addEntity(admin);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblPromisProcess> getAllTblPromisProcess() {
         return super.getAllEntity();
        //throw new UnsupportedOperationException("Not supported yet.");
    }

}
