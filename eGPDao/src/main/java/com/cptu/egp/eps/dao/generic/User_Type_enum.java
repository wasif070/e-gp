package com.cptu.egp.eps.dao.generic;

/**
 *
 * @author Radha
 */
public enum User_Type_enum {

    EGPAdMIN(1), TENDERER(2), OFFICER(3), PEADMIN(4), ORGADMIN(5), DEVELOPMENT(6),
    SCHEDULEBANK(7), CONTENTADMIN(8), COMPANYADMIN(9), PROCUREMENTEXPERT(12), EXTERNALMEMBER(14), BRANCHADMIN(15), DGCPTU(16), REVIEWPANELMEMBER(17);

    private int code;

    private User_Type_enum(int c) {
      code = c;
    }

    public int getType() {
      return code;
    }
}
