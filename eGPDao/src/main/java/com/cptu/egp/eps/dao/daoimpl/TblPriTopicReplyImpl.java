package com.cptu.egp.eps.dao.daoimpl;


import com.cptu.egp.eps.dao.daointerface.TblPriTopicReplyDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblPriTopicReply;
import java.util.List;


public class TblPriTopicReplyImpl extends AbcAbstractClass<TblPriTopicReply> implements TblPriTopicReplyDao {

	@Override 
	public void addTblPriTopicReply(TblPriTopicReply master){
		super.addEntity(master);
	}
	@Override 
	public void deleteTblPriTopicReply(TblPriTopicReply master) {
		super.deleteEntity(master);
	}
	@Override 
	public void updateTblPriTopicReply(TblPriTopicReply master) {
		super.updateEntity(master);
	}
	@Override 
	public List<TblPriTopicReply> getAllTblPriTopicReply() {
		return super.getAllEntity();
	}
	@Override 
	public List<TblPriTopicReply> findTblPriTopicReply(Object... values) throws Exception {
		return super.findEntity(values);
	}
	@Override 
	public long getTblPriTopicReplyCount() {
		return super.getEntityCount();
	}
	@Override 
	public List<TblPriTopicReply> findByCountTblPriTopicReply(int firstResult, int maxResult, Object... values) throws Exception {
		return super.findByCountEntity(firstResult, maxResult, values);
	}
	@Override 
	public void updateOrSaveTblPriTopicReply(List<TblPriTopicReply> tblObj) {
		super.updateAll(tblObj);
	}
}