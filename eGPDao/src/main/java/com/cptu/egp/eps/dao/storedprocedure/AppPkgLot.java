/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Ramesh.Janagondakuru
 */
public class AppPkgLot extends StoredProcedure{
    private static final Logger LOGGER = Logger.getLogger(AppPkgLot.class);
    public AppPkgLot(BasicDataSource dataSource, String procName){
         this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("v_packageid_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_appId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_lotNo_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_lotDesc_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_quantity_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_unit_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Action_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_AppPkgLotId_inInt", Types.VARCHAR));
    }

    /**
     * Execute stored procedure : p_ins_upd_apppkglot
     * to performed insert,update,delete operation for APP Package Lot.
     * @param packageid
     * @param appId
     * @param lotNo
     * @param lotDesc
     * @param quantity
     * @param unit
     * @param action
     * @param appPkgLotId
     * @return CommonMsgChk list contains flags indicate operation successful or not.
     */
     public List<CommonMsgChk> executeProcedure(Integer packageid,Integer appId,Integer lotNo,String  lotDesc,Integer  quantity,String unit,String action,String appPkgLotId){
        Map inParams = new HashMap();
        inParams.put("v_packageid_inInt", packageid);
        inParams.put("v_appId_inInt", appId);
        inParams.put("v_lotNo_inInt", lotNo);
        inParams.put("v_lotDesc_inVc", lotDesc);
        inParams.put("v_quantity_inInt", quantity);
        inParams.put("v_unit_inVc", unit);
        inParams.put("v_Action_inVc", action);
        inParams.put("v_AppPkgLotId_inInt", appPkgLotId);
      this.compile();
        List<CommonMsgChk> details = new ArrayList<CommonMsgChk>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonMsgChk commonMsgChk = new CommonMsgChk();
                commonMsgChk.setFlag((Boolean) linkedHashMap.get("flag"));
                commonMsgChk.setMsg((String) linkedHashMap.get("msg"));
                details.add(commonMsgChk);
            }
        } catch (Exception e) {
            LOGGER.error("AppPackage : " + e);
        }
        return details;
     }

}
