/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblJvcapartners;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TblJvcapartnersDao extends GenericDao<TblJvcapartners> {

    public void addTblJvcapartners(TblJvcapartners tblObj);

    public void deleteTblJvcapartners(TblJvcapartners tblObj);

    public void updateTblJvcapartners(TblJvcapartners tblObj);

    public List<TblJvcapartners> getAllTblJvcapartners();

    public List<TblJvcapartners> findTblJvcapartners(Object... values) throws Exception;

    public List<TblJvcapartners> findByCountTblJvcapartners(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblJvcapartnersCount();
}
