package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvWorkPlanDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvWorkPlan;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsSrvWorkPlanImpl extends AbcAbstractClass<TblCmsSrvWorkPlan> implements TblCmsSrvWorkPlanDao {

    @Override
    public void addTblCmsSrvWorkPlan(TblCmsSrvWorkPlan cmsWpDetail){
        super.addEntity(cmsWpDetail);
    }

    @Override
    public void deleteTblCmsSrvWorkPlan(TblCmsSrvWorkPlan cmsWpDetail) {
        super.deleteEntity(cmsWpDetail);
    }

    @Override
    public void updateTblCmsSrvWorkPlan(TblCmsSrvWorkPlan cmsWpDetail) {
        super.updateEntity(cmsWpDetail);
    }

    @Override
    public List<TblCmsSrvWorkPlan> getAllTblCmsSrvWorkPlan() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvWorkPlan> findTblCmsSrvWorkPlan(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvWorkPlanCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvWorkPlan> findByCountTblCmsSrvWorkPlan(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsSrvWorkPlan> estCost) {
       super.updateAll(estCost);
    }
}
