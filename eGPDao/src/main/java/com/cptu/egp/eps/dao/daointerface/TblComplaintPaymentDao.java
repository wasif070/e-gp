/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblComplaintPayments;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TblComplaintPaymentDao extends GenericDao<TblComplaintPayments>{

    public void addComplaintPayment(TblComplaintPayments complaintPayment);

    public void updateComplaintPayment(TblComplaintPayments complaintPayment);
    
    public List<TblComplaintPayments>   findComplaintPayments(Object... values) throws Exception;
    
    public TblComplaintPayments getPaymentDetails(String feetype, Integer id, Integer compId) throws Exception;

    public int getGovUserId() throws Exception;
    
/*    public List<TblCompanyMaster> getAllTblCompanyMaster();

    public List<TblCompanyMaster> findTblCompanyMaster(Object... values) throws Exception;

    public List<TblCompanyMaster> findByCountTblCompanyMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCompanyMasterCount();
 *
*/}
