/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.math.BigDecimal;

import java.util.Date;

/**
 *
 * @author Istiak
 */
public class TenderDashboardOfflineDetails {

    private Integer tenderOfflineId;
    private String ministryOrDivision;
    private String agency;
    private String peOfficeName;
    private String peCode;
    private String peDistrict;
    private String eventType;
    private String invitationFor;
    private String reoiRfpFor;
    private String reoiRfpRefNo;
    private Date IssueDate;
    private String procurementMethod;
    private String procurementNature;
    private String procurementType;
    private String budgetType;
    private String sourceOfFund;
    private String devPartners;
    private String projectCode;
    private String projectName;
    private String packageNo;
    private String PackageName;
    private Date tenderPubDate;
    private Date preTenderREOIDate;
    private String PreTenderREOIPlace;
    private Date LastSellingDate;
    private Date ClosingDate;
    private Date OpeningDate;
    private String SellingAddPrinciple;
    private String SellingAddOthers;
    private String ReceivingAdd;
    private String OpeningAdd;
    private String EligibilityCriteria;
    private String BriefDescription;
    private String RelServicesOrDeliverables;
    private String OtherDetails;
    private String ForeignFirm;
    private BigDecimal DocumentPrice;
    private String TenderStatus;
    private String peName;
    private String peDesignation;
    private String peAddress;
    private String peContactDetails;
    private Integer UserID;
    private Date InsertUpdateDate;

    private boolean flag;
    private String msg;

    private String lotOrRefNo;
    private String lotIdentOrPhasingServ;
    private String location;
    private BigDecimal tenderSecurityAmt;
    private String startDateTime;
    private String completionDateTime;

    private String fieldName;
    private String oldValue;
    private String newValue;
    private String corrigendumStatus;
    private Integer numberOfCOR;

    public Integer getTenderOfflineId() {
        return tenderOfflineId;
    }

    public void setTenderOfflineId(Integer tenderOfflineId) {
        this.tenderOfflineId = tenderOfflineId;
    }

    public String getMinistryOrDivision() {
        return ministryOrDivision;
    }

    public void setMinistryOrDivision(String ministryOrDivision) {
        this.ministryOrDivision = ministryOrDivision;
    }

    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public String getPeOfficeName() {
        return peOfficeName;
    }

    public void setPeOfficeName(String peOfficeName) {
        this.peOfficeName = peOfficeName;
    }

    public String getPeCode() {
        return peCode;
    }

    public void setPeCode(String peCode) {
        this.peCode = peCode;
    }

    public String getPeDistrict() {
        return peDistrict;
    }

    public void setPeDistrict(String peDistrict) {
        this.peDistrict = peDistrict;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getInvitationFor() {
        return invitationFor;
    }

    public void setInvitationFor(String invitationFor) {
        this.invitationFor = invitationFor;
    }

    public String getReoiRfpFor() {
        return reoiRfpFor;
    }

    public void setReoiRfpFor(String reoiRfpFor) {
        this.reoiRfpFor = reoiRfpFor;
    }

    public String getReoiRfpRefNo() {
        return reoiRfpRefNo;
    }

    public void setReoiRfpRefNo(String reoiRfpRefNo) {
        this.reoiRfpRefNo = reoiRfpRefNo;
    }

    public Date getIssueDate() {
        return IssueDate;
    }

    public void setIssueDate(Date IssueDate) {
        this.IssueDate = IssueDate;
    }

    public String getProcurementMethod() {
        return procurementMethod;
    }

    public void setProcurementMethod(String procurementMethod) {
        this.procurementMethod = procurementMethod;
    }

    public String getProcurementNature() {
        return procurementNature;
    }

    public void setProcurementNature(String procurementNature) {
        this.procurementNature = procurementNature;
    }

    public String getProcurementType() {
        return procurementType;
    }

    public void setProcurementType(String procurementType) {
        this.procurementType = procurementType;
    }

    public String getBudgetType() {
        return budgetType;
    }

    public void setBudgetType(String budgetType) {
        this.budgetType = budgetType;
    }

    public String getSourceOfFund() {
        return sourceOfFund;
    }

    public void setSourceOfFund(String sourceOfFund) {
        this.sourceOfFund = sourceOfFund;
    }

    public String getDevPartners() {
        return devPartners;
    }

    public void setDevPartners(String devPartners) {
        this.devPartners = devPartners;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getPackageNo() {
        return packageNo;
    }

    public void setPackageNo(String packageNo) {
        this.packageNo = packageNo;
    }

    public String getPackageName() {
        return PackageName;
    }

    public void setPackageName(String PackageName) {
        this.PackageName = PackageName;
    }

    public Date getTenderPubDate() {
        return tenderPubDate;
    }

    public void setTenderPubDate(Date tenderPubDate) {
        this.tenderPubDate = tenderPubDate;
    }

    public Date getPreTenderREOIDate() {
        return preTenderREOIDate;
    }

    public void setPreTenderREOIDate(Date preTenderREOIDate) {
        this.preTenderREOIDate = preTenderREOIDate;
    }

    public String getPreTenderREOIPlace() {
        return PreTenderREOIPlace;
    }

    public void setPreTenderREOIPlace(String PreTenderREOIPlace) {
        this.PreTenderREOIPlace = PreTenderREOIPlace;
    }

    public Date getLastSellingDate() {
        return LastSellingDate;
    }

    public void setLastSellingDate(Date LastSellingDate) {
        this.LastSellingDate = LastSellingDate;
    }

    public Date getClosingDate() {
        return ClosingDate;
    }

    public void setClosingDate(Date ClosingDate) {
        this.ClosingDate = ClosingDate;
    }

    public Date getOpeningDate() {
        return OpeningDate;
    }

    public void setOpeningDate(Date OpeningDate) {
        this.OpeningDate = OpeningDate;
    }

    public String getSellingAddPrinciple() {
        return SellingAddPrinciple;
    }

    public void setSellingAddPrinciple(String SellingAddPrinciple) {
        this.SellingAddPrinciple = SellingAddPrinciple;
    }

    public String getSellingAddOthers() {
        return SellingAddOthers;
    }

    public void setSellingAddOthers(String SellingAddOthers) {
        this.SellingAddOthers = SellingAddOthers;
    }

    public String getReceivingAdd() {
        return ReceivingAdd;
    }

    public void setReceivingAdd(String ReceivingAdd) {
        this.ReceivingAdd = ReceivingAdd;
    }

    public String getOpeningAdd() {
        return OpeningAdd;
    }

    public void setOpeningAdd(String OpeningAdd) {
        this.OpeningAdd = OpeningAdd;
    }

    public String getEligibilityCriteria() {
        return EligibilityCriteria;
    }

    public void setEligibilityCriteria(String EligibilityCriteria) {
        this.EligibilityCriteria = EligibilityCriteria;
    }

    public String getBriefDescription() {
        return BriefDescription;
    }

    public void setBriefDescription(String BriefDescription) {
        this.BriefDescription = BriefDescription;
    }

    public String getRelServicesOrDeliverables() {
        return RelServicesOrDeliverables;
    }

    public void setRelServicesOrDeliverables(String RelServicesOrDeliverables) {
        this.RelServicesOrDeliverables = RelServicesOrDeliverables;
    }

    public String getOtherDetails() {
        return OtherDetails;
    }

    public void setOtherDetails(String OtherDetails) {
        this.OtherDetails = OtherDetails;
    }

    public String getForeignFirm() {
        return ForeignFirm;
    }

    public void setForeignFirm(String ForeignFirm) {
        this.ForeignFirm = ForeignFirm;
    }

    public BigDecimal getDocumentPrice() {
        return DocumentPrice;
    }

    public void setDocumentPrice(BigDecimal DocumentPrice) {
        this.DocumentPrice = DocumentPrice;
    }

    public String getTenderStatus() {
        return TenderStatus;
    }

    public void setTenderStatus(String TenderStatus) {
        this.TenderStatus = TenderStatus;
    }

    public String getPeName() {
        return peName;
    }

    public void setPeName(String peName) {
        this.peName = peName;
    }

    public String getPeDesignation() {
        return peDesignation;
    }

    public void setPeDesignation(String peDesignation) {
        this.peDesignation = peDesignation;
    }

    public String getPeAddress() {
        return peAddress;
    }

    public void setPeAddress(String peAddress) {
        this.peAddress = peAddress;
    }

    public String getPeContactDetails() {
        return peContactDetails;
    }

    public void setPeContactDetails(String peContactDetails) {
        this.peContactDetails = peContactDetails;
    }

    public Integer getUserID() {
        return UserID;
    }

    public void setUserID(Integer UserID) {
        this.UserID = UserID;
    }

    public Date getInsertUpdateDate() {
        return InsertUpdateDate;
    }

    public void setInsertUpdateDate(Date InsertUpdateDate) {
        this.InsertUpdateDate = InsertUpdateDate;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getLotOrRefNo() {
        return lotOrRefNo;
    }

    public void setLotOrRefNo(String lotOrRefNo) {
        this.lotOrRefNo = lotOrRefNo;
    }

    public String getLotIdentOrPhasingServ() {
        return lotIdentOrPhasingServ;
    }

    public void setLotIdentOrPhasingServ(String lotIdentOrPhasingServ) {
        this.lotIdentOrPhasingServ = lotIdentOrPhasingServ;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public BigDecimal getTenderSecurityAmt() {
        return tenderSecurityAmt;
    }

    public void setTenderSecurityAmt(BigDecimal tenderSecurityAmt) {
        this.tenderSecurityAmt = tenderSecurityAmt;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getCompletionDateTime() {
        return completionDateTime;
    }

    public void setCompletionDateTime(String completionDateTime) {
        this.completionDateTime = completionDateTime;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getCorrigendumStatus() {
        return corrigendumStatus;
    }

    public void setCorrigendumStatus(String corrigendumStatus) {
        this.corrigendumStatus = corrigendumStatus;
    }

    public Integer getNumberOfCOR() {
        return numberOfCOR;
    }

    public void setNumberOfCOR(Integer numberOfCOR) {
        this.numberOfCOR = numberOfCOR;
    }

}
