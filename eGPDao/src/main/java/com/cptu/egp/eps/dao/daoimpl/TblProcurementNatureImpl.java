package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblProcurementNatureDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblProcurementNature;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblProcurementNatureImpl extends AbcAbstractClass<TblProcurementNature> implements TblProcurementNatureDao {

    @Override
    public void addTblProcurementNature(TblProcurementNature procure){
        super.addEntity(procure);
    }

    @Override
    public void deleteTblProcurementNature(TblProcurementNature procure) {
        super.deleteEntity(procure);
    }

    @Override
    public void updateTblProcurementNature(TblProcurementNature procure) {
        super.updateEntity(procure);
    }

    @Override
    public List<TblProcurementNature> getAllTblProcurementNature() {
        return super.getAllEntity();
    }

    @Override
    public List<TblProcurementNature> findTblProcurementNature(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblProcurementNatureCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblProcurementNature> findByCountTblProcurementNature(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
