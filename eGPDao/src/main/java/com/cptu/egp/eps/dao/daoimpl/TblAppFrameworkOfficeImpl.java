/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblAppFrameworkOfficeDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblAppFrameworkOffice;
import java.util.List;

public class TblAppFrameworkOfficeImpl extends AbcAbstractClass<TblAppFrameworkOffice> implements TblAppFrameworkOfficeDao{
    @Override
    public void addTblAppFrameworkOffice(TblAppFrameworkOffice master){
            super.addEntity(master);
    }
    @Override
    public void deleteTblAppFrameworkOffice(TblAppFrameworkOffice master) {
            super.deleteEntity(master);
    }
    @Override
    public void updateTblAppFrameworkOffice(TblAppFrameworkOffice master) {
            super.updateEntity(master);
    }
    @Override
    public List<TblAppFrameworkOffice> getAllTblAppFrameworkOffice() {
            return super.getAllEntity();
    }
    @Override
    public List<TblAppFrameworkOffice> findTblAppFrameworkOffice(Object... values) throws Exception {
            return super.findEntity(values);
    }
    @Override
    public long getTblAppFrameworkOfficeCount() {
            return super.getEntityCount();
    }
    @Override
    public List<TblAppFrameworkOffice> findByCountTblAppFrameworkOffice(int firstResult, int maxResult, Object... values) throws Exception {
            return super.findByCountEntity(firstResult, maxResult, values);
    }
    @Override
    public void updateOrSaveTblAppFrameworkOffice(List<TblAppFrameworkOffice> tblObj) {
            super.updateAll(tblObj);
    }
}
