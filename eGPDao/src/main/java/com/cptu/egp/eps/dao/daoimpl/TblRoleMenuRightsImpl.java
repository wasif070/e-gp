package com.cptu.egp.eps.dao.daoimpl;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.daointerface.TblRoleMenuRightsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblRoleMenuRights;
import java.util.List;

/**
 *
 * @author dipal.shah
 */
public class TblRoleMenuRightsImpl extends AbcAbstractClass<TblRoleMenuRights> implements TblRoleMenuRightsDao {

	@Override
	public void addtblRoleMenuRights(TblRoleMenuRights master){
		super.addEntity(master);
	}
	@Override
	public void deletetblRoleMenuRights(TblRoleMenuRights master) {
		super.deleteEntity(master);
	}
	@Override
	public void updatetblRoleMenuRights(TblRoleMenuRights master) {
		super.updateEntity(master);
	}
	@Override
	public List<TblRoleMenuRights> getAlltblRoleMenuRights() {
		return super.getAllEntity();
	}
	@Override
	public List<TblRoleMenuRights> findtblRoleMenuRights(Object... values) throws Exception {
		return super.findEntity(values);
	}
	@Override
	public long gettblRoleMenuRightsCount() {
		return super.getEntityCount();
	}
	@Override
	public List<TblRoleMenuRights> findByCounttblRoleMenuRights(int firstResult, int maxResult, Object... values) throws Exception {
		return super.findByCountEntity(firstResult, maxResult, values);
	}
	@Override
	public void updateOrSavetblRoleMenuRights(List<TblRoleMenuRights> tblObj) {
		super.updateAll(tblObj);
	}
}