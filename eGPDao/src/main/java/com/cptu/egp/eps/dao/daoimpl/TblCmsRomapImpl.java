package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsRomapDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsRomap;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsRomapImpl extends AbcAbstractClass<TblCmsRomap> implements TblCmsRomapDao {

    @Override
    public void addTblCmsRomap(TblCmsRomap cmsInvoiceDetails){
        super.addEntity(cmsInvoiceDetails);
    }

    @Override
    public void deleteTblCmsRomap(TblCmsRomap cmsInvoiceDetails) {
        super.deleteEntity(cmsInvoiceDetails);
    }

    @Override
    public void updateTblCmsRomap(TblCmsRomap cmsInvoiceDetails) {
        super.updateEntity(cmsInvoiceDetails);
    }

    @Override
    public List<TblCmsRomap> getAllTblCmsRomap() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsRomap> findTblCmsRomap(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsRomapCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsRomap> findByCountTblCmsRomap(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }


    @Override
    public void updateInsertAllTblCmsRomap(List<TblCmsRomap> list) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
