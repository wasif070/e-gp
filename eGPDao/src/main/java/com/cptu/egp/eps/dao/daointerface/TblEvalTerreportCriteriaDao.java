package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalTerreportCriteria;
import java.util.List;

/**
 *
 * @author rishita
 */
public interface TblEvalTerreportCriteriaDao extends GenericDao<TblEvalTerreportCriteria> {

    public void addTblEvalTerreportCriteria(TblEvalTerreportCriteria tblObj);

    public void deleteTblEvalTerreportCriteria(TblEvalTerreportCriteria tblObj);

    public void updateTblEvalTerreportCriteria(TblEvalTerreportCriteria tblObj);

    public List<TblEvalTerreportCriteria> getAllTblEvalTerreportCriteria();

    public List<TblEvalTerreportCriteria> findTblEvalTerreportCriteria(Object... values) throws Exception;

    public List<TblEvalTerreportCriteria> findByCountTblEvalTerreportCriteria(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalTerreportCriteriaCount();
}
