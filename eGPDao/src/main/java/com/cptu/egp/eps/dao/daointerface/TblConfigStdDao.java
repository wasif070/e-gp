package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblConfigStd;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblConfigStdDao extends GenericDao<TblConfigStd> {

    public void addTblConfigStd(TblConfigStd tblObj);

    public void deleteTblConfigStd(TblConfigStd tblObj);

    public void deleteAllTblConfigStd(Collection entity);

    public void updateTblConfigStd(TblConfigStd tblObj);

    public List<TblConfigStd> getAllTblConfigStd();

    public List<TblConfigStd> findTblConfigStd(Object... values) throws Exception;

    public List<TblConfigStd> findByCountTblConfigStd(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblConfigStdCount();
}
