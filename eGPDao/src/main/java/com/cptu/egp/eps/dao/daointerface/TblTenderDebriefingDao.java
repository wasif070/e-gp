/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.model.table.TblTenderDebriefing;
import java.util.List;

/**
 *
 * @author dixit
 */
public interface TblTenderDebriefingDao {

    public void addTblTenderDebriefing(TblTenderDebriefing tblObj);

    public void deleteTblTenderDebriefing(TblTenderDebriefing tblObj);

    public void updateTblTenderDebriefing(TblTenderDebriefing tblObj);

    public List<TblTenderDebriefing> getAllTblTenderDebriefing();

    public List<TblTenderDebriefing> findTblTenderDebriefing(Object... values) throws Exception;

    public List<TblTenderDebriefing> findByCountTblTenderDebriefing(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderDetailsCount();

}
