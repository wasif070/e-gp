package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblIttGccSectionsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblIttGccSections;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblIttGccSectionsImpl extends AbcAbstractClass<TblIttGccSections> implements TblIttGccSectionsDao {

    @Override
    public void addTblIttGccSections(TblIttGccSections sections){
        super.addEntity(sections);
    }

    @Override
    public void deleteTblIttGccSections(TblIttGccSections sections) {
        super.deleteEntity(sections);
    }

    @Override
    public void updateTblIttGccSections(TblIttGccSections sections) {
        super.updateEntity(sections);
    }

    @Override
    public List<TblIttGccSections> getAllTblIttGccSections() {
        return super.getAllEntity();
    }

    @Override
    public List<TblIttGccSections> findTblIttGccSections(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblIttGccSectionsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblIttGccSections> findByCountTblIttGccSections(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
