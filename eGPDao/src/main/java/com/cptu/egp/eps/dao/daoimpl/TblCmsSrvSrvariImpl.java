package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvSrvariDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvSrvari;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsSrvSrvariImpl extends AbcAbstractClass<TblCmsSrvSrvari> implements TblCmsSrvSrvariDao {

    @Override
    public void addTblCmsSrvSrvari(TblCmsSrvSrvari cmsInvoiceDetails){
        super.addEntity(cmsInvoiceDetails);
    }

    @Override
    public void deleteTblCmsSrvSrvari(TblCmsSrvSrvari cmsInvoiceDetails) {
        super.deleteEntity(cmsInvoiceDetails);
    }

    @Override
    public void updateTblCmsSrvSrvari(TblCmsSrvSrvari cmsInvoiceDetails) {
        super.updateEntity(cmsInvoiceDetails);
    }

    @Override
    public List<TblCmsSrvSrvari> getAllTblCmsSrvSrvari() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvSrvari> findTblCmsSrvSrvari(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvSrvariCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvSrvari> findByCountTblCmsSrvSrvari(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }


    @Override
    public void updateOrSaveEstCost(List<TblCmsSrvSrvari> estCost) {
       super.updateAll(estCost);
    }
}
