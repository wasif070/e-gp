package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblEvalBidderStatusDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalBidderStatus;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblEvalBidderStatusImpl extends AbcAbstractClass<TblEvalBidderStatus> implements TblEvalBidderStatusDao {

    @Override
    public void addTblEvalBidderStatus(TblEvalBidderStatus evalBidderStatus){
        super.addEntity(evalBidderStatus);
    }

    @Override
    public void deleteTblEvalBidderStatus(TblEvalBidderStatus evalBidderStatus) {
        super.deleteEntity(evalBidderStatus);
    }

    @Override
    public void updateTblEvalBidderStatus(TblEvalBidderStatus evalBidderStatus) {
        super.updateEntity(evalBidderStatus);
    }

    @Override
    public List<TblEvalBidderStatus> getAllTblEvalBidderStatus() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEvalBidderStatus> findTblEvalBidderStatus(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEvalBidderStatusCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalBidderStatus> findByCountTblEvalBidderStatus(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
