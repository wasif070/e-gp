package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderPaymentStatus;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderPaymentStatusDao extends GenericDao<TblTenderPaymentStatus> {

    public void addTblTenderPaymentStatus(TblTenderPaymentStatus tblTenderPaymentStatus);

    public void deleteTblTenderPaymentStatus(TblTenderPaymentStatus tblTenderPaymentStatus);

    public void deleteAllTblTenderPaymentStatus(Collection tblTenderPaymentStatus);

    public void updateTblTenderPaymentStatus(TblTenderPaymentStatus tblTenderPaymentStatus);

    public List<TblTenderPaymentStatus> getAllTblTenderPaymentStatus();

    public List<TblTenderPaymentStatus> findTblTenderPaymentStatus(Object... values) throws Exception;

    public List<TblTenderPaymentStatus> findByCountTblTenderPaymentStatus(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderPaymentStatusCount();
}
