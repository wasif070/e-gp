/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblEvalRptDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalRptDocs;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblEvalRptDocsImpl extends AbcAbstractClass<TblEvalRptDocs> implements TblEvalRptDocsDao{

    @Override
    public void addTblEvalRptDocs(TblEvalRptDocs tblEvalRptDocs) {
        super.addEntity(tblEvalRptDocs);
    }

    @Override
    public List<TblEvalRptDocs> findTblEvalRptDocs(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblEvalRptDocs(TblEvalRptDocs tblEvalRptDocs) {

        super.deleteEntity(tblEvalRptDocs);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblEvalRptDocs(Collection tblEvalRptDocs) {
        super.deleteAll(tblEvalRptDocs);
    }

    @Override
    public void updateTblEvalRptDocs(TblEvalRptDocs tblEvalRptDocs) {

        super.updateEntity(tblEvalRptDocs);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblEvalRptDocs> getAllTblEvalRptDocs() {
        return super.getAllEntity();
    }

    @Override
    public long getTblEvalRptDocsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalRptDocs> findByCountTblEvalRptDocs(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
