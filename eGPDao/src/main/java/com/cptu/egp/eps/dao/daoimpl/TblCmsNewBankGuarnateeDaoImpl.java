/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblCmsNewBankGuarnateeDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsNewBankGuarnatee;
import java.util.List;

/**
 *
 * @author rikin.p
 */
public class TblCmsNewBankGuarnateeDaoImpl extends AbcAbstractClass<TblCmsNewBankGuarnatee> implements TblCmsNewBankGuarnateeDao {

    @Override
    public void addTblCmsNewBankGuarnatee(TblCmsNewBankGuarnatee cmsNewBankGuarnatee){
        super.addEntity(cmsNewBankGuarnatee);
    }

    @Override
    public void deleteTblCmsNewBankGuarnatee(TblCmsNewBankGuarnatee cmsNewBankGuarnatee) {
        super.deleteEntity(cmsNewBankGuarnatee);
    }

    @Override
    public void updateTblCmsNewBankGuarnatee(TblCmsNewBankGuarnatee cmsNewBankGuarnatee) {
        super.updateEntity(cmsNewBankGuarnatee);
    }

    @Override
    public List<TblCmsNewBankGuarnatee> getAllTblCmsNewBankGuarnatee() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsNewBankGuarnatee> findTblCmsNewBankGuarnatee(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsNewBankGuarnateeCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsNewBankGuarnatee> findByCountTblCmsNewBankGuarnatee(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}