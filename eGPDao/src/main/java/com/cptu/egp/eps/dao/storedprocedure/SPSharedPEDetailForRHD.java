/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.object.StoredProcedure;

/**
 * This class is helps for execution the procedure <b>p_get_PEOfficeDetailRHD</b>.
 * @author Sudhir Chavhan
 */
public class SPSharedPEDetailForRHD extends StoredProcedure {

    private static final Logger LOGGER = Logger.getLogger(SPSharedPEDetailForRHD.class);

    /***
     * Construction for <b>SPSharedPEDetailForRHD</b>
     * we inject this properties at DaoContext
     * @param dataSource
     * @param procName
     */
    public SPSharedPEDetailForRHD(BasicDataSource dataSource, String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        LOGGER.debug("Proc Name: " + procName);
    }
    /***
     * This method executes the given procedure and returns the list of objects
     * Execute procedure : p_get_PEOfficeDetailRHD
     * @return List<RHDIntegrationPEDetails>
     */
    public List<RHDIntegrationPEDetails> executeProcedure() {
        this.compile();
        List<RHDIntegrationPEDetails> rhdIntegrationDetailsList =
                new ArrayList<RHDIntegrationPEDetails>();
        Map inParams = new HashMap();
        try {
            ArrayList<LinkedHashMap<String, Object>> list =
                    (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                RHDIntegrationPEDetails rhdIntegrationDetails =
                        new RHDIntegrationPEDetails();
                rhdIntegrationDetails.setPEId((Integer) linkedHashMap.get("PEID"));
                rhdIntegrationDetails.setPEOfficeName((String) linkedHashMap.get("PEOfficeName"));
                rhdIntegrationDetails.setDeptType((String) linkedHashMap.get("DeptType"));
                rhdIntegrationDetails.setMinistryName((String) linkedHashMap.get("MinistryName"));
                rhdIntegrationDetails.setDivisionName((String) linkedHashMap.get("DivisionName"));
                rhdIntegrationDetails.setRHDOrgName((String) linkedHashMap.get("RHDOrgName"));
                rhdIntegrationDetails.setDistrictName((String) linkedHashMap.get("DistrictName"));
                rhdIntegrationDetails.setOfficeAddr((String) linkedHashMap.get("OfficeAddr"));
                rhdIntegrationDetailsList.add(rhdIntegrationDetails);
            }
        } catch (Exception e) {
            LOGGER.error("SPSharedPEDetailForRHD : " + e);
        }
        return rhdIntegrationDetailsList;
    }
}
