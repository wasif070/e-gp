package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCorrigendumMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblCorrigendumMasterDao extends GenericDao<TblCorrigendumMaster> {

    public void addTblCorrigendumMaster(TblCorrigendumMaster tblObj);

    public void deleteTblCorrigendumMaster(TblCorrigendumMaster tblObj);

    public void updateTblCorrigendumMaster(TblCorrigendumMaster tblObj);

    public List<TblCorrigendumMaster> getAllTblCorrigendumMaster();

    public List<TblCorrigendumMaster> findTblCorrigendumMaster(Object... values) throws Exception;

    public List<TblCorrigendumMaster> findByCountTblCorrigendumMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCorrigendumMasterCount();
}
