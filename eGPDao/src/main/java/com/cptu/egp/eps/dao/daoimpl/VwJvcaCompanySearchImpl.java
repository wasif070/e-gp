package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.VwJvcaCompanySearchDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.view.VwJvcaCompanySearch;
import java.util.List;

/**
 *
 * @author taher
 */
public class VwJvcaCompanySearchImpl extends AbcAbstractClass<VwJvcaCompanySearch> implements VwJvcaCompanySearchDao {

    @Override
    public List<VwJvcaCompanySearch> getAllJvcaCompanySearch() {
        return super.getAllEntity();
    }

    @Override
    public List<VwJvcaCompanySearch> findJvcaCompanySearch(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public List<VwJvcaCompanySearch> findByCountJvcaCompanySearch(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

}
