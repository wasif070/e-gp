package com.cptu.egp.eps.dao.daoimpl;


import com.cptu.egp.eps.dao.daointerface.TblContractSignDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblContractSignDocs;
import java.util.List;


public class TblContractSignDocsImpl extends AbcAbstractClass<TblContractSignDocs> implements TblContractSignDocsDao {

    @Override 
    public void addTblContractSignDocs(TblContractSignDocs master){
            super.addEntity(master);
    }
    @Override 
    public void deleteTblContractSignDocs(TblContractSignDocs master) {
            super.deleteEntity(master);
    }
    @Override 
    public void updateTblContractSignDocs(TblContractSignDocs master) {
            super.updateEntity(master);
    }
    @Override 
    public List<TblContractSignDocs> getAllTblContractSignDocs() {
            return super.getAllEntity();
    }
    @Override 
    public List<TblContractSignDocs> findTblContractSignDocs(Object... values) throws Exception {
            return super.findEntity(values);
    }
    @Override 
    public long getTblContractSignDocsCount() {
            return super.getEntityCount();
    }
    @Override 
    public List<TblContractSignDocs> findByCountTblContractSignDocs(int firstResult, int maxResult, Object... values) throws Exception {
            return super.findByCountEntity(firstResult, maxResult, values);
    }
    @Override 
    public void updateOrSaveTblContractSignDocs(List<TblContractSignDocs> tblObj) {
            super.updateAll(tblObj);
    }
}