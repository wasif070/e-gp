package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvReExpenseDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvReExpense;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsSrvReExpenseImpl extends AbcAbstractClass<TblCmsSrvReExpense> implements TblCmsSrvReExpenseDao {

    @Override
    public void addTblCmsSrvReExpense(TblCmsSrvReExpense cmsWpDetail){
        super.addEntity(cmsWpDetail);
    }

    @Override
    public void deleteTblCmsSrvReExpense(TblCmsSrvReExpense cmsWpDetail) {
        super.deleteEntity(cmsWpDetail);
    }

    @Override
    public void updateTblCmsSrvReExpense(TblCmsSrvReExpense cmsWpDetail) {
        super.updateEntity(cmsWpDetail);
    }

    @Override
    public List<TblCmsSrvReExpense> getAllTblCmsSrvReExpense() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvReExpense> findTblCmsSrvReExpense(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvReExpenseCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvReExpense> findByCountTblCmsSrvReExpense(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsSrvReExpense> estCost) {
       super.updateAll(estCost);
    }
}
