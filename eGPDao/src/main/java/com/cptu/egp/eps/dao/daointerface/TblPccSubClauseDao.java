package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblPccSubClause;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblPccSubClauseDao extends GenericDao<TblPccSubClause> {

    public void addTblPccSubClause(TblPccSubClause tblObj);

    public void deleteTblPccSubClause(TblPccSubClause tblObj);

    public void updateTblPccSubClause(TblPccSubClause tblObj);

    public List<TblPccSubClause> getAllTblPccSubClause();

    public List<TblPccSubClause> findTblPccSubClause(Object... values) throws Exception;

    public List<TblPccSubClause> findByCountTblPccSubClause(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblPccSubClauseCount();
}
