/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblCmsCtcertDocDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsCtcertDoc;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public class TblCmsCtcertDocDaoImpl extends AbcAbstractClass<TblCmsCtcertDoc> implements TblCmsCtcertDocDao {

    @Override
    public void addTblCmsCtCertDoc(TblCmsCtcertDoc tblCmsCtcertDoc) {
        super.addEntity(tblCmsCtcertDoc);
    }

    @Override
    public void deleteTblCmsCtCertDoc(TblCmsCtcertDoc tblCmsCtcertDoc) {
        super.deleteEntity(tblCmsCtcertDoc);
    }

    @Override
    public void updateTblCmsCtCertDoc(TblCmsCtcertDoc tblCmsCtcertDoc) {
        super.updateEntity(tblCmsCtcertDoc);
    }

    @Override
    public List<TblCmsCtcertDoc> getAllTblCmsCtCertDoc() {
        super.setPersistentClass(TblCmsCtcertDoc.class);
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsCtcertDoc> findTblCmsCtCertDoc(Object... values) throws Exception {
        super.setPersistentClass(TblCmsCtcertDoc.class);
        return super.findEntity(values);
    }

    @Override
    public List<TblCmsCtcertDoc> findByCountTblCmsCtCertDoc(int firstResult, int maxResult, Object... values) throws Exception {
        super.setPersistentClass(TblCmsCtcertDoc.class);
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblCmsCtCertDocCount() {
        super.setPersistentClass(TblCmsCtcertDoc.class);
        return super.getEntityCount();
    }
}
