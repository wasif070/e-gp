/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table. TblAdvertisement;
import java.util.List;

/**
 *
 * @author sambasiva.sugguna
 */

public interface TblAdvertisementDao extends GenericDao<TblAdvertisement> {

    public Integer addTblAdvertisement(TblAdvertisement tblAdvertisement);

    public void deleteTblAdvertisement(TblAdvertisement tblAdvertisement);

    public void updateTblAdvertisement(TblAdvertisement tblAdvertisement);

    public List<TblAdvertisement> getAllTblAdvertisement();

    public List<TblAdvertisement> findTblAdvertisement(Object... values) throws Exception;

    public List<TblAdvertisement> findByCountTblAdvertisement(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblAdvertisement(TblAdvertisement tblAdvertisement);
}

