package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsVariationOrderDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsVariationOrder;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsVariationOrderImpl extends AbcAbstractClass<TblCmsVariationOrder> implements TblCmsVariationOrderDao {

    @Override
    public void addTblCmsVariationOrder(TblCmsVariationOrder cmsVariationOrder){
        super.addEntity(cmsVariationOrder);
    }

    @Override
    public void deleteTblCmsVariationOrder(TblCmsVariationOrder cmsVariationOrder) {
        super.deleteEntity(cmsVariationOrder);
    }

    @Override
    public void updateTblCmsVariationOrder(TblCmsVariationOrder cmsVariationOrder) {
        super.updateEntity(cmsVariationOrder);
    }

    @Override
    public List<TblCmsVariationOrder> getAllTblCmsVariationOrder() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsVariationOrder> findTblCmsVariationOrder(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsVariationOrderCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsVariationOrder> findByCountTblCmsVariationOrder(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsVariationOrder> estCost) {
         super.updateAll(estCost);
    }
}
