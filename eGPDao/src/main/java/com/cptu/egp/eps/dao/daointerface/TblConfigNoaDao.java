package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblConfigNoa;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblConfigNoaDao extends GenericDao<TblConfigNoa> {

    public void addTblConfigNoa(TblConfigNoa tblConfigNoa);

    public void deleteTblConfigNoa(TblConfigNoa tblConfigNoa);

    public void deleteAllTblConfigNoa(Collection tblConfigNoa);

    public void updateTblConfigNoa(TblConfigNoa tblConfigNoa);

    public List<TblConfigNoa> getAllTblConfigNoa();

    public List<TblConfigNoa> findTblConfigNoa(Object... values) throws Exception;

    public List<TblConfigNoa> findByCountTblConfigNoa(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblConfigNoaCount();
}
