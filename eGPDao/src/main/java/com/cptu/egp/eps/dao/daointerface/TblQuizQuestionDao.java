/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblQuizQuestion;
import java.util.List;

/**
 *
 * @author feroz
 */
public interface TblQuizQuestionDao extends GenericDao<TblQuizQuestion>{
    
    public void addTblQuizQuestion(TblQuizQuestion tblQuizQuestion);

    public void deleteTblQuizQuestion(TblQuizQuestion tblQuizQuestion);

    public void updateTblQuizQuestion(TblQuizQuestion tblQuizQuestion);

    public List<TblQuizQuestion> getAllTblQuizQuestion();
    
    /**
     *
     * @param values
     * @return
     */
    public List<TblQuizQuestion> findTblQuizQuestion(Object... values);
}
