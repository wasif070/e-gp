package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalReportReq;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblEvalReportReqDao extends GenericDao<TblEvalReportReq> {

    public void addTblEvalReportReq(TblEvalReportReq tblEvalReportReq);

    public void deleteTblEvalReportReq(TblEvalReportReq tblEvalReportReq);

    public void deleteAllTblEvalReportReq(Collection tblEvalReportReq);

    public void updateTblEvalReportReq(TblEvalReportReq tblEvalReportReq);

    public List<TblEvalReportReq> getAllTblEvalReportReq();

    public List<TblEvalReportReq> findTblEvalReportReq(Object... values) throws Exception;

    public List<TblEvalReportReq> findByCountTblEvalReportReq(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalReportReqCount();
}
