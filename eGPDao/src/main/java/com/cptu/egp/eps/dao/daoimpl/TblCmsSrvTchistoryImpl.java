package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvTchistoryDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvTchistory;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsSrvTchistoryImpl extends AbcAbstractClass<TblCmsSrvTchistory> implements TblCmsSrvTchistoryDao {

    @Override
    public void addTblCmsSrvTchistory(TblCmsSrvTchistory activityMaster){
        super.addEntity(activityMaster);
    }

    @Override
    public void deleteTblCmsSrvTchistory(TblCmsSrvTchistory activityMaster) {
        super.deleteEntity(activityMaster);
    }

    @Override
    public void updateTblCmsSrvTchistory(TblCmsSrvTchistory activityMaster) {
        super.updateEntity(activityMaster);
    }

    @Override
    public List<TblCmsSrvTchistory> getAllTblCmsSrvTchistory() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvTchistory> findTblCmsSrvTchistory(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvTchistoryCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvTchistory> findByCountTblCmsSrvTchistory(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveAllCmsSrvTchistory(List<TblCmsSrvTchistory> tblObjList) {
        super.updateAll(tblObjList);
    }
}
