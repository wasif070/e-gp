package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblEmailVerificationCodeDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEmailVerificationCode;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblEmailVerificationCodeImpl extends AbcAbstractClass<TblEmailVerificationCode> implements TblEmailVerificationCodeDao {

    @Override
    public void addTblEmailVerificationCode(TblEmailVerificationCode master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblEmailVerificationCode(TblEmailVerificationCode master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblEmailVerificationCode(TblEmailVerificationCode master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblEmailVerificationCode> getAllTblEmailVerificationCode() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEmailVerificationCode> findTblEmailVerificationCode(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEmailVerificationCodeCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEmailVerificationCode> findByCountTblEmailVerificationCode(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
