package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsInvRemarks;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCmsInvRemarksDao extends GenericDao<TblCmsInvRemarks> {

    public void addTblCmsInvRemarks(TblCmsInvRemarks event);

    public void deleteTblCmsInvRemarks(TblCmsInvRemarks event);

    public void updateTblCmsInvRemarks(TblCmsInvRemarks event);

    public List<TblCmsInvRemarks> getAllTblCmsInvRemarks();

    public List<TblCmsInvRemarks> findTblCmsInvRemarks(Object... values) throws Exception;

    public List<TblCmsInvRemarks> findByCountTblCmsInvRemarks(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsInvRemarksCount();
}
