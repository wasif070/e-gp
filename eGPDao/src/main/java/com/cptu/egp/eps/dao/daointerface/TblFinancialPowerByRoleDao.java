/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblFinancialPowerByRole;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblFinancialPowerByRoleDao extends GenericDao<TblFinancialPowerByRole>{

    public void addTblFinancialPowerByRole(TblFinancialPowerByRole tblObj);

    public void deleteTblFinancialPowerByRole(TblFinancialPowerByRole tblObj);

    public void updateTblFinancialPowerByRole(TblFinancialPowerByRole tblObj);

    public List<TblFinancialPowerByRole> getAllTblFinancialPowerByRole();

    public List<TblFinancialPowerByRole> findTblFinancialPowerByRole(Object... values) throws Exception;

    public List<TblFinancialPowerByRole> findByCountTblFinancialPowerByRole(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblFinancialPowerByRoleCount();
}
