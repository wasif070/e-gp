/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblOfficeAdmin;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public interface TblOfficeAdminDao extends GenericDao<TblOfficeAdmin> {

    public void addTblOfficeAdmin(TblOfficeAdmin officeadmin);

    public void deleteTblOfficeAdmin(TblOfficeAdmin officeadmin);

    public void updateTblOfficeAdmin(TblOfficeAdmin officeadmin);

    public List<TblOfficeAdmin> getAllTblOfficeAdmin();

    public List<TblOfficeAdmin> findTblOfficeAdmin(Object... values) throws Exception;

    public List<TblOfficeAdmin> findByCountTblOfficeAdmin(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblOfficeAdminCount();
}
