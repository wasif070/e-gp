package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblNegBidFormDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblNegBidForm;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblNegBidFormImpl extends AbcAbstractClass<TblNegBidForm> implements TblNegBidFormDao {

    @Override
    public void addTblNegBidForm(TblNegBidForm tblNegBidForm){
        super.addEntity(tblNegBidForm);
    }

    @Override
    public void deleteTblNegBidForm(TblNegBidForm tblNegBidForm) {
        super.deleteEntity(tblNegBidForm);
    }

    @Override
    public void updateTblNegBidForm(TblNegBidForm tblNegBidForm) {
        super.updateEntity(tblNegBidForm);
    }

    @Override
    public List<TblNegBidForm> getAllTblNegBidForm() {
        return super.getAllEntity();
    }

    @Override
    public List<TblNegBidForm> findTblNegBidForm(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblNegBidFormCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblNegBidForm> findByCountTblNegBidForm(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
