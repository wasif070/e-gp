/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

/**
 *
 * @author rishita
 */
public class APPSearchDtBean {

    private String ministry;
    private String division;
    private String oranisation;
    private String officeName;
    private String budgetType;
    private String officeId;
    private String appId;
    private String totalRowCount;
    private String totalPages;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    public String getBudgetType() {
        return budgetType;
    }

    public void setBudgetType(String budgetType) {
        this.budgetType = budgetType;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getMinistry() {
        return ministry;
    }

    public void setMinistry(String ministry) {
        this.ministry = ministry;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    public String getOranisation() {
        return oranisation;
    }

    public void setOranisation(String oranisation) {
        this.oranisation = oranisation;
    }

    public String getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(String totalPages) {
        this.totalPages = totalPages;
    }

    public String getTotalRowCount() {
        return totalRowCount;
    }

    public void setTotalRowCount(String totalRowCount) {
        this.totalRowCount = totalRowCount;
    }
}
