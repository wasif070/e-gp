package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblAppPackages;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblAppPackagesDao extends GenericDao<TblAppPackages> {

    public void addTblAppPackages(TblAppPackages tblObj);

    public void deleteTblAppPackages(TblAppPackages tblObj);

    public void updateTblAppPackages(TblAppPackages tblObj);

    public List<TblAppPackages> getAllTblAppPackages();

    public List<TblAppPackages> findTblAppPackages(Object... values) throws Exception;

    public List<TblAppPackages> findByCountTblAppPackages(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblAppPackagesCount();
}
