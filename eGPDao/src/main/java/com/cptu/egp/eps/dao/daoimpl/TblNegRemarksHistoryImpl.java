package com.cptu.egp.eps.dao.daoimpl;


import com.cptu.egp.eps.dao.daointerface.TblNegRemarksHistoryDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblNegRemarksHistory;
import java.util.List;


public class TblNegRemarksHistoryImpl extends AbcAbstractClass<TblNegRemarksHistory> implements TblNegRemarksHistoryDao {

	@Override 
	public void addTblNegRemarksHistory(TblNegRemarksHistory master){
		super.addEntity(master);
	}
	@Override 
	public void deleteTblNegRemarksHistory(TblNegRemarksHistory master) {
		super.deleteEntity(master);
	}
	@Override 
	public void updateTblNegRemarksHistory(TblNegRemarksHistory master) {
		super.updateEntity(master);
	}
	@Override 
	public List<TblNegRemarksHistory> getAllTblNegRemarksHistory() {
		return super.getAllEntity();
	}
	@Override 
	public List<TblNegRemarksHistory> findTblNegRemarksHistory(Object... values) throws Exception {
		return super.findEntity(values);
	}
	@Override 
	public long getTblNegRemarksHistoryCount() {
		return super.getEntityCount();
	}
	@Override 
	public List<TblNegRemarksHistory> findByCountTblNegRemarksHistory(int firstResult, int maxResult, Object... values) throws Exception {
		return super.findByCountEntity(firstResult, maxResult, values);
	}
	@Override 
	public void updateOrSaveTblNegRemarksHistory(List<TblNegRemarksHistory> tblObj) {
		super.updateAll(tblObj);
	}
}