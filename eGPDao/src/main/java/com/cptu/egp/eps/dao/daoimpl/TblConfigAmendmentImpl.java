/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblConfigAmendmentDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblConfigAmendment;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblConfigAmendmentImpl extends AbcAbstractClass<TblConfigAmendment> implements TblConfigAmendmentDao{

    @Override
    public void addTblConfigAmendment(TblConfigAmendment configAmendment) {
        super.addEntity(configAmendment);
    }

    @Override
    public List<TblConfigAmendment> findTblConfigAmendment(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblConfigAmendment(TblConfigAmendment configAmendment) {

        super.deleteEntity(configAmendment);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblConfigAmendment(Collection event) {
        super.deleteAll(event);
    }

    @Override
    public void updateTblConfigAmendment(TblConfigAmendment configAmendment) {

        super.updateEntity(configAmendment);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblConfigAmendment> getAllTblConfigAmendment() {
        return super.getAllEntity();
    }

    @Override
    public long getTblConfigAmendmentCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblConfigAmendment> findByCountTblConfigAmendment(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
