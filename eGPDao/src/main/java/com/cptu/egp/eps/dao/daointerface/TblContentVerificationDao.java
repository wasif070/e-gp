package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblContentVerification;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblContentVerificationDao extends GenericDao<TblContentVerification> {

    public void addTblContentVerification(TblContentVerification tblContentVerification);

    public void deleteTblContentVerification(TblContentVerification tblContentVerification);

    public void deleteAllTblContentVerification(Collection tblContentVerification);

    public void updateTblContentVerification(TblContentVerification tblContentVerification);

    public List<TblContentVerification> getAllTblContentVerification();

    public List<TblContentVerification> findTblContentVerification(Object... values) throws Exception;

    public List<TblContentVerification> findByCountTblContentVerification(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblContentVerificationCount();
}
