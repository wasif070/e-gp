package com.cptu.egp.eps.dao.daointerface;


import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblPriTopicReply;
import java.util.List;


	public interface TblPriTopicReplyDao extends GenericDao<TblPriTopicReply> {

	public void addTblPriTopicReply(TblPriTopicReply tblObj);

	public void deleteTblPriTopicReply(TblPriTopicReply tblObj);

	public void updateTblPriTopicReply(TblPriTopicReply tblObj);

	public List<TblPriTopicReply> getAllTblPriTopicReply();

	public List<TblPriTopicReply> findTblPriTopicReply(Object... values) throws Exception;

	public List<TblPriTopicReply> findByCountTblPriTopicReply(int firstResult,int maxResult,Object... values) throws Exception;

	public long getTblPriTopicReplyCount();

	public void updateOrSaveTblPriTopicReply(List<TblPriTopicReply> tblObj);
}