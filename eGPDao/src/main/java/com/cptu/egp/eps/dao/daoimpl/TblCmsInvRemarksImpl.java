package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsInvRemarksDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsInvRemarks;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsInvRemarksImpl extends AbcAbstractClass<TblCmsInvRemarks> implements TblCmsInvRemarksDao {

    @Override
    public void addTblCmsInvRemarks(TblCmsInvRemarks master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblCmsInvRemarks(TblCmsInvRemarks master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblCmsInvRemarks(TblCmsInvRemarks master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblCmsInvRemarks> getAllTblCmsInvRemarks() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsInvRemarks> findTblCmsInvRemarks(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsInvRemarksCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsInvRemarks> findByCountTblCmsInvRemarks(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
