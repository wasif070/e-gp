package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblProcurementNature;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblProcurementNatureDao extends GenericDao<TblProcurementNature> {

    public void addTblProcurementNature(TblProcurementNature budget);

    public void deleteTblProcurementNature(TblProcurementNature budget);

    public void updateTblProcurementNature(TblProcurementNature budget);

    public List<TblProcurementNature> getAllTblProcurementNature();

    public List<TblProcurementNature> findTblProcurementNature(Object... values) throws Exception;

    public List<TblProcurementNature> findByCountTblProcurementNature(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblProcurementNatureCount();
}
