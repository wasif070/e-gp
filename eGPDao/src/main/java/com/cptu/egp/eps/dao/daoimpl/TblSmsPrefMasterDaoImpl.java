package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblSmsPrefMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblSmsPrefMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblSmsPrefMasterDaoImpl extends AbcAbstractClass<TblSmsPrefMaster> implements TblSmsPrefMasterDao {

    @Override
    public void addTblSmsPrefMaster(TblSmsPrefMaster activityMaster){
        super.addEntity(activityMaster);
    }

    @Override
    public void deleteTblSmsPrefMaster(TblSmsPrefMaster activityMaster) {
        super.deleteEntity(activityMaster);
    }

    @Override
    public void updateTblSmsPrefMaster(TblSmsPrefMaster activityMaster) {
        super.updateEntity(activityMaster);
    }

    @Override
    public List<TblSmsPrefMaster> getAllTblSmsPrefMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblSmsPrefMaster> findTblSmsPrefMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblSmsPrefMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblSmsPrefMaster> findByCountTblSmsPrefMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
