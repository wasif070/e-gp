/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderBidTable;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderBidTableDao extends GenericDao<TblTenderBidTable>{

    public void addTblTenderBidTable(TblTenderBidTable tblObj);

    public void deleteTblTenderBidTable(TblTenderBidTable tblObj);

    public void updateTblTenderBidTable(TblTenderBidTable tblObj);

    public List<TblTenderBidTable> getAllTblTenderBidTable();

    public List<TblTenderBidTable> findTblTenderBidTable(Object... values) throws Exception;

    public List<TblTenderBidTable> findByCountTblTenderBidTable(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderBidTableCount();
}
