/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblOfficeMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public class TblOfficeMasterImpl extends AbcAbstractClass<TblOfficeMaster> implements TblOfficeMasterDao {

    @Override
    public void addTblOfficeMaster(TblOfficeMaster office) {

        super.addEntity(office);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblOfficeMaster(TblOfficeMaster office) {

        super.deleteEntity(office);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblOfficeMaster(TblOfficeMaster office) {

        super.updateEntity(office);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblOfficeMaster> getAllTblOfficeMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblOfficeMaster> findTblOfficeMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblOfficeMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblOfficeMaster> findByCountTblOfficeMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
