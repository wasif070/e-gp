package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCompanyJointVenture;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCompanyJointVentureDao extends GenericDao<TblCompanyJointVenture> {

    public void addTblCompanyJointVenture(TblCompanyJointVenture login);

    public void deleteTblCompanyJointVenture(TblCompanyJointVenture login);

    public void updateTblCompanyJointVenture(TblCompanyJointVenture login);

    public List<TblCompanyJointVenture> getAllTblCompanyJointVenture();

    public List<TblCompanyJointVenture> findTblCompanyJointVenture(Object... values) throws Exception;

    public List<TblCompanyJointVenture> findByCountTblCompanyJointVenture(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCompanyJointVentureCount();
}
