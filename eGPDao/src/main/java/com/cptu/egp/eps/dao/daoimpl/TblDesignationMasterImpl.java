/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblDesignationMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblDesignationMaster;
import java.util.List;

/**
 * <b>Class Description goes here</b>
 * $Revision: 1.1.1.1 $
 * @version <version-no.> <Date>
 * @author Administrator
 */
public class TblDesignationMasterImpl extends AbcAbstractClass<TblDesignationMaster> implements TblDesignationMasterDao {

    @Override
    public void addDesignation(TblDesignationMaster tblDesignationMaster)
    {
        super.addEntity(tblDesignationMaster);
    }

    @Override
    public void deleteDesignnation(TblDesignationMaster tblDesignationMaster)
    {
        super.deleteEntity(tblDesignationMaster);
    }

    @Override
    public void updateDesignantion(TblDesignationMaster tblDesignationMaster)
    {
        super.updateEntity(tblDesignationMaster);
    }

    @Override
    public List<TblDesignationMaster> getAllDesinations()
    {
        return super.getAllEntity();
    }

    @Override
    public List<TblDesignationMaster> findDesignantion(Object... values) throws Exception
    {
        return super.findEntity(values);
    }

    @Override
    public List<TblDesignationMaster> findByCountDesignation(int firstResult, int maxResult, Object... values) throws Exception
    {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getDesignationCount()
    {
        return super.getEntityCount();
    }


}
