package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsTrackVariationDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsTrackVariation;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsTrackVariationImpl extends AbcAbstractClass<TblCmsTrackVariation> implements TblCmsTrackVariationDao {

    @Override
    public void addTblCmsTrackVariation(TblCmsTrackVariation cmsWpDetail){
        super.addEntity(cmsWpDetail);
    }

    @Override
    public void deleteTblCmsTrackVariation(TblCmsTrackVariation cmsWpDetail) {
        super.deleteEntity(cmsWpDetail);
    }

    @Override
    public void updateTblCmsTrackVariation(TblCmsTrackVariation cmsWpDetail) {
        super.updateEntity(cmsWpDetail);
    }

    @Override
    public List<TblCmsTrackVariation> getAllTblCmsTrackVariation() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsTrackVariation> findTblCmsTrackVariation(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsTrackVariationCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsTrackVariation> findByCountTblCmsTrackVariation(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsTrackVariation> estCost) {
       super.updateAll(estCost);
    }
}
