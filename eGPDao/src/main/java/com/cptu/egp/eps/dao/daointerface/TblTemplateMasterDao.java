/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTemplateMaster;
import java.util.List;

/**
 *
 * @author yanki
 */
public interface TblTemplateMasterDao extends GenericDao<TblTemplateMaster>{

    public void addTblTemplateMaster(TblTemplateMaster templateMaster);

    public void deleteTblTemplateMaster(TblTemplateMaster templateMaster);

    public void updateTblTemplateMaster(TblTemplateMaster templateMaster);

    public List<TblTemplateMaster> getAllTblTemplateMaster();

    public List<TblTemplateMaster> findTblTemplateMaster(Object... values) throws Exception;
    
}
