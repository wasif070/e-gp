/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblCorrigendumDetailDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCorrigendumDetail;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblCorrigendumDetailImpl extends AbcAbstractClass<TblCorrigendumDetail> implements TblCorrigendumDetailDao{

    @Override
    public void addTblCorrigendumDetail(TblCorrigendumDetail tblCorrigendumDetail) {
        super.addEntity(tblCorrigendumDetail);
    }

    @Override
    public List<TblCorrigendumDetail> findTblCorrigendumDetail(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblCorrigendumDetail(TblCorrigendumDetail tblCorrigendumDetail) {

        super.deleteEntity(tblCorrigendumDetail);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblCorrigendumDetail(TblCorrigendumDetail tblCorrigendumDetail) {

        super.updateEntity(tblCorrigendumDetail);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblCorrigendumDetail> getAllTblCorrigendumDetail() {
        return super.getAllEntity();
    }

    @Override
    public long getTblCorrigendumDetailCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCorrigendumDetail> findByCountTblCorrigendumDetail(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveAllCorriDetail(List<TblCorrigendumDetail> tblCorrigendumDetail) {
        super.updateAll(tblCorrigendumDetail);
    }
}
