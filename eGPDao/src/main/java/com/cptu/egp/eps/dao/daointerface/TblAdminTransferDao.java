package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblAdminTransfer;
import java.util.List;

/**
 *
 * @author rishita
 */
public interface TblAdminTransferDao extends GenericDao<TblAdminTransfer> {

    public void addTblAdminTransfer(TblAdminTransfer tblObj);

    public void deleteTblAdminTransfer(TblAdminTransfer tblObj);

    public void updateTblAdminTransfer(TblAdminTransfer tblObj);

    public List<TblAdminTransfer> getAllTblAdminTransfer();

    public List<TblAdminTransfer> findTblAdminTransfer(Object... values) throws Exception;

    public List<TblAdminTransfer> findByCountTblAdminTransfer(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblAdminTransferCount();
}
