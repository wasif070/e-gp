package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsWpDetailDocs;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCmsWpDetailDocsDao extends GenericDao<TblCmsWpDetailDocs> {

    public void addTblCmsWpDetailDocs(TblCmsWpDetailDocs tblObj);

    public void deleteTblCmsWpDetailDocs(TblCmsWpDetailDocs tblObj);

    public void updateTblCmsWpDetailDocs(TblCmsWpDetailDocs tblObj);

    public List<TblCmsWpDetailDocs> getAllTblCmsWpDetailDocs();

    public List<TblCmsWpDetailDocs> findTblCmsWpDetailDocs(Object... values) throws Exception;

    public List<TblCmsWpDetailDocs> findByCountTblCmsWpDetailDocs(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsWpDetailDocsCount();
}
