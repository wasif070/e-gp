package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblComplaintLevelDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblComplaintLevel;
import java.util.List;

/**
 *
 * @author Radha
 */
public class TblComplaintLevelDaoImpl extends AbcAbstractClass<TblComplaintLevel> implements TblComplaintLevelDao {

    @Override
    public void addTblComplaintLevel(TblComplaintLevel complaintLevel){
        super.addEntity(complaintLevel);
    }

 /*
    @Override
    public void deleteTblComplaintLevel(TblComplaintLevel complaintLevel) {
        super.deleteEntity(complaintLevel);
    }

    @Override
    public void updateTblComplaintLevel(TblComplaintLevel complaintLevel) {
        super.updateEntity(complaintLevel);
    }

    @Override
    public List<TblComplaintLevel> getAllComplaintLevels() {
        return super.getAllEntity();
    }

   */
    
    
    public List<TblComplaintLevel> findByLevel(Object... values) throws Exception{
    	
    	
    	return super.findEntity(values);
    }
}
