package com.cptu.egp.eps.dao.daoimpl;


import com.cptu.egp.eps.dao.daointerface.TblTendererAuditDetailsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTendererAuditDetails;
import java.util.List;


public class TblTendererAuditDetailsImpl extends AbcAbstractClass<TblTendererAuditDetails> implements TblTendererAuditDetailsDao {

	@Override 
	public void addTblTendererAuditDetails(TblTendererAuditDetails master){
		super.addEntity(master);
	}
	@Override 
	public void deleteTblTendererAuditDetails(TblTendererAuditDetails master) {
		super.deleteEntity(master);
	}
	@Override 
	public void updateTblTendererAuditDetails(TblTendererAuditDetails master) {
		super.updateEntity(master);
	}
	@Override 
	public List<TblTendererAuditDetails> getAllTblTendererAuditDetails() {
		return super.getAllEntity();
	}
	@Override 
	public List<TblTendererAuditDetails> findTblTendererAuditDetails(Object... values) throws Exception {
		return super.findEntity(values);
	}
	@Override 
	public long getTblTendererAuditDetailsCount() {
		return super.getEntityCount();
	}
	@Override 
	public List<TblTendererAuditDetails> findByCountTblTendererAuditDetails(int firstResult, int maxResult, Object... values) throws Exception {
		return super.findByCountEntity(firstResult, maxResult, values);
	}
	@Override 
	public void updateOrSaveTblTendererAuditDetails(List<TblTendererAuditDetails> tblObj) {
		super.updateAll(tblObj);
	}
}