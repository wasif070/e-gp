/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
/**
 *
 * @author dipal.shah
 */
public class CommonStoredProcedure extends StoredProcedure 
{
    private static final Logger LOGGER = Logger.getLogger(SPCommonSearch.class);
    
   
    public CommonStoredProcedure(BasicDataSource dataSource,String procName)
    {
        this.setDataSource(dataSource);
        this.setSql(procName);
        this.declareParameter(new SqlParameter("@v_fieldName1Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName2Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName3Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName4Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName5Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName6Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName7Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName8Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName9Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName10Vc", Types.VARCHAR));
    }

    
    /**
     * This method is use to execute stored procedure and get data from stored procedure. 
     * which stored procedure will executed that is depend on procName parameter of this class. which is stored procedure name.
     * Example : Execute stored procedure : p_dump_neg_forms_data [to dump tender bid data from tender bid tables to negotiation bid tables].
     * @param fieldName1
     * @param fieldName2
     * @param fieldName3
     * @param fieldName4
     * @param fieldName5
     * @param fieldName6
     * @param fieldName7
     * @param fieldName8
     * @param fieldName9
     * @param fieldName10
     * @return list of CommonStoredProcedureData object.
     */
    public List <CommonStoredProcedureData> executeProcedure (String fieldName1,String fieldName2, String fieldName3, String fieldName4, String fieldName5 ,String fieldName6, String fieldName7,String fieldName8, String fieldName9,String fieldName10)
    {
        
        Map inParams = new HashMap();
        inParams.put("@v_fieldName1Vc", fieldName1);
        inParams.put("@v_fieldName2Vc", fieldName2);
        inParams.put("@v_fieldName3Vc", fieldName3);
        inParams.put("@v_fieldName4Vc", fieldName4);
        inParams.put("@v_fieldName5Vc", fieldName5);
        inParams.put("@v_fieldName6Vc", fieldName6);
        inParams.put("@v_fieldName7Vc", fieldName7);
        inParams.put("@v_fieldName8Vc", fieldName8);
        inParams.put("@v_fieldName9Vc", fieldName9);
        inParams.put("@v_fieldName10Vc", fieldName10);

        this.compile();
         
        List<CommonStoredProcedureData> returnData = new ArrayList<CommonStoredProcedureData>();
        try
        {
             ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
             for (LinkedHashMap<String, Object> linkedHashMap: list )
             {
                CommonStoredProcedureData commonStoredProcedureData = new CommonStoredProcedureData();
                commonStoredProcedureData.setFieldName1((String)linkedHashMap.get("FieldValue1"));
                commonStoredProcedureData.setFieldName2((String)linkedHashMap.get("FieldValue2"));
                commonStoredProcedureData.setFieldName3((String)linkedHashMap.get("FieldValue3"));
                commonStoredProcedureData.setFieldName4((String)linkedHashMap.get("FieldValue4"));
                commonStoredProcedureData.setFieldName5((String)linkedHashMap.get("FieldValue5"));
                commonStoredProcedureData.setFieldName6((String)linkedHashMap.get("FieldValue6"));
                commonStoredProcedureData.setFieldName7((String)linkedHashMap.get("FieldValue7"));
                commonStoredProcedureData.setFieldName8((String)linkedHashMap.get("FieldValue8"));
                commonStoredProcedureData.setFieldName9((String)linkedHashMap.get("FieldValue9"));
                commonStoredProcedureData.setFieldName10((String)linkedHashMap.get("FieldValue10"));
                commonStoredProcedureData.setFieldName11((String)linkedHashMap.get("FieldValue11"));
                commonStoredProcedureData.setFieldName12((String)linkedHashMap.get("FieldValue12"));
                commonStoredProcedureData.setFieldName12((String)linkedHashMap.get("FieldValue13"));
                commonStoredProcedureData.setFieldName12((String)linkedHashMap.get("FieldValue14"));
                commonStoredProcedureData.setFieldName12((String)linkedHashMap.get("FieldValue15"));
                commonStoredProcedureData.setFieldName12((String)linkedHashMap.get("FieldValue16"));
                commonStoredProcedureData.setFieldName12((String)linkedHashMap.get("FieldValue17"));
                commonStoredProcedureData.setFieldName12((String)linkedHashMap.get("FieldValue18"));
                commonStoredProcedureData.setFieldName12((String)linkedHashMap.get("FieldValue19"));
                commonStoredProcedureData.setFieldName12((String)linkedHashMap.get("FieldValue20"));
                returnData.add(commonStoredProcedureData);
             }
        }
        catch(Exception e)
        {
         LOGGER.error("CommonStoredProcedure : "+ e);
            System.out.println("Erro in "+e.getMessage());
         e.printStackTrace();
        }
        return returnData;
    }

}