/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblEvalReportSignDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalReportSign;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblEvalReportSignImpl extends AbcAbstractClass<TblEvalReportSign> implements TblEvalReportSignDao{

    @Override
    public void addTblEvalReportSign(TblEvalReportSign evalReportSign) {
        super.addEntity(evalReportSign);
    }

    @Override
    public List<TblEvalReportSign> findTblEvalReportSign(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblEvalReportSign(TblEvalReportSign evalReportSign) {

        super.deleteEntity(evalReportSign);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblEvalReportSign(Collection evalReportSign) {
        super.deleteAll(evalReportSign);
    }

    @Override
    public void updateTblEvalReportSign(TblEvalReportSign evalReportSign) {

        super.updateEntity(evalReportSign);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblEvalReportSign> getAllTblEvalReportSign() {
        return super.getAllEntity();
    }

    @Override
    public long getTblEvalReportSignCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalReportSign> findByCountTblEvalReportSign(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
