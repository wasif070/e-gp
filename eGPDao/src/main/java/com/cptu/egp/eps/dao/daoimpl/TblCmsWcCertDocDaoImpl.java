/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblCmsWcCertDocDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsWcCertDoc;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public class TblCmsWcCertDocDaoImpl extends AbcAbstractClass<TblCmsWcCertDoc> implements TblCmsWcCertDocDao {

    @Override
    public void addTblCmsWcCertDoc(TblCmsWcCertDoc tblCmsWcCertDoc) {
        super.addEntity(tblCmsWcCertDoc);
    }

    @Override
    public void deleteTblCmsWcCertDoc(TblCmsWcCertDoc tblCmsWcCertDoc) {
        super.deleteEntity(tblCmsWcCertDoc);
    }

    @Override
    public void updateTblCmsWcCertDoc(TblCmsWcCertDoc tblCmsWcCertDoc) {
        super.updateEntity(tblCmsWcCertDoc);
    }

    @Override
    public List<TblCmsWcCertDoc> getAllTblCmsWcCertDoc() {
        super.setPersistentClass(TblCmsWcCertDoc.class);
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsWcCertDoc> findTblCmsWcCertDoc(Object... values) throws Exception {
        super.setPersistentClass(TblCmsWcCertDoc.class);
        return super.findEntity(values);
    }

    @Override
    public List<TblCmsWcCertDoc> findByCountTblCmsWcCertDoc(int firstResult, int maxResult, Object... values) throws Exception {
        super.setPersistentClass(TblCmsWcCertDoc.class);
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblCmsWcCertDocCount() {
        super.setPersistentClass(TblCmsWcCertDoc.class);
        return super.getEntityCount();
    }
}
