package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvTeamCompDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvTeamComp;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsSrvTeamCompImpl extends AbcAbstractClass<TblCmsSrvTeamComp> implements TblCmsSrvTeamCompDao {

    @Override
    public void addTblCmsSrvTeamComp(TblCmsSrvTeamComp cmsWpDetail){
        super.addEntity(cmsWpDetail);
    }

    @Override
    public void deleteTblCmsSrvTeamComp(TblCmsSrvTeamComp cmsWpDetail) {
        super.deleteEntity(cmsWpDetail);
    }

    @Override
    public void updateTblCmsSrvTeamComp(TblCmsSrvTeamComp cmsWpDetail) {
        super.updateEntity(cmsWpDetail);
    }

    @Override
    public List<TblCmsSrvTeamComp> getAllTblCmsSrvTeamComp() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvTeamComp> findTblCmsSrvTeamComp(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvTeamCompCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvTeamComp> findByCountTblCmsSrvTeamComp(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsSrvTeamComp> estCost) {
       super.updateAll(estCost);
    }
}
