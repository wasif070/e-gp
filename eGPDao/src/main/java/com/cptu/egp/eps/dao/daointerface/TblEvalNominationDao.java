package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalNomination;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblEvalNominationDao extends GenericDao<TblEvalNomination> {

    public void addTblEvalNomination(TblEvalNomination tblObj);

    public void deleteTblEvalNomination(TblEvalNomination tblObj);

    public void updateTblEvalNomination(TblEvalNomination tblObj);

    public List<TblEvalNomination> getAllTblEvalNomination();

    public List<TblEvalNomination> findTblEvalNomination(Object... values) throws Exception;

    public List<TblEvalNomination> findByCountTblEvalNomination(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalNominationCount();
}
