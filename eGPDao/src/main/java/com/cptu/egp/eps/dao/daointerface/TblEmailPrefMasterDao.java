/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEmailPrefMaster;
import java.util.List;

/**
 *
 * @author shreyansh.shah
 */
public interface TblEmailPrefMasterDao extends GenericDao<TblEmailPrefMaster> {

    public void addTblEmailPrefMaster(TblEmailPrefMaster tblObj);

    public void deleteTblEmailPrefMaster(TblEmailPrefMaster tblObj);

    public void updateTblEmailPrefMaster(TblEmailPrefMaster tblObj);

    public List<TblEmailPrefMaster> getAllTblEmailPrefMaster();

    public List<TblEmailPrefMaster> findTblEmailPrefMaster(Object... values) throws Exception;

    public List<TblEmailPrefMaster> findByCountTblEmailPrefMaster(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblEmailPrefMasterCount();
}
