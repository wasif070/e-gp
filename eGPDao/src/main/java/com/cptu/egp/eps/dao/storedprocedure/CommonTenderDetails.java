/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.math.BigDecimal;

import java.util.Date;

/**
 *
 * @author Administrator
 */
public class CommonTenderDetails {

    private Integer tenderDtlId;
    private Integer departmentId;
    private Integer tenderId;
    private String ministry;
    private String division;
    private String agency;
    private String peOfficeName;
    private String peCode;
    private String peDistrict;
    private String eventType;
    private String invitationFor;
    private String reoiRfpFor;
    private String contractType;
    private String reoiRfpRefNo;
    private String procurementMethod;
    private String budgetType;
    private String sourceOfFund;
    private String devPartners;
    private String projectCode;
    private String projectName;
    private String packageNo;
    private String packageDescription;
    private String cpvCode;
    private Date docEndDate;
    private Date preBidStartDt;
    private Date preBidEndDt;
    private Date submissionDt;
    private Date openingDt;
    private String eligibilityCriteria;
    private String tenderBrief;
    private String deliverables;
    private String otherDetails;
    private String foreignFirm;
    private String docAvlMethod;
    private String evalType;
    private String docFeesMethod;
    private String docFeesMode;
    private String docOfficeAdd;
    private Date securityLastDt;
    private String securitySubOff;
    private String procurementNature;
    private String procurementType;
    private String modeOfTender;
    private Integer reTenderId;
    private Integer pqTenderId;
    private Integer reoiTenderId;
    private String tenderStatus;
    private Date tenderPubDt;
    private String peName;
    private String peDesignation;
    private String peAddress;
    private String peContactDetails;
    private BigDecimal estCost;
    private Integer approvingAuthId;
    private Integer stdTemplateId;
    private String docAccess;
    private Integer tenderValDays;
    private Date tenderValidityDt;
    private Integer tenderSecurityDays;
    private Date tenderSecurityDt;
    private Integer procurementNatureId;
    private Integer procurementMethodId;
    private Integer officeId;
    private Integer budgetTypeId;
    private String workflowStatus;
    private BigDecimal pkgDocFees;
    private String phasingRefNo;
    private String phasingOfService;
    private String location;
    private Date indStartDt;
    private Date indEndDt;
    private Integer tenderLotSecId;
    private String lotNo;
    private String lotDesc;
    private String locationSec;
    private BigDecimal docFess;
    private BigDecimal tenderSecurityAmt;
    private String completionTime;
    private String startTime;
    private Integer totalRecords;
    private Integer tenderPhasingId;
    private Integer appPkgLotId;
    private Integer passingMarks;
    private String tenderevalstatus;
    private BigDecimal tenderSecurityAmtUSD;    // For ICT
    private BigDecimal pkgDocFeesUSD;
    private String bidSecurityType;// For ICT

    public String getTenderevalstatus() {
        return tenderevalstatus;
    }

    public void setTenderevalstatus(String tenderevalstatus) {
        this.tenderevalstatus = tenderevalstatus;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public Integer getPassingMarks() {
        return passingMarks;
    }

    public void setPassingMarks(Integer passingMarks) {
        this.passingMarks = passingMarks;
    }

    public Integer getAppPkgLotId() {
        return appPkgLotId;
    }

    public void setAppPkgLotId(Integer appPkgLotId) {
        this.appPkgLotId = appPkgLotId;
    }

    public Integer getTenderPhasingId() {
        return tenderPhasingId;
    }

    public void setTenderPhasingId(Integer tenderPhasingId) {
        this.tenderPhasingId = tenderPhasingId;
    }

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }

    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public Integer getApprovingAuthId() {
        return approvingAuthId;
    }

    public void setApprovingAuthId(Integer approvingAuthId) {
        this.approvingAuthId = approvingAuthId;
    }

    public String getBudgetType() {
        return budgetType;
    }

    public void setBudgetType(String budgetType) {
        this.budgetType = budgetType;
    }

    public Integer getBudgetTypeId() {
        return budgetTypeId;
    }

    public void setBudgetTypeId(Integer budgetTypeId) {
        this.budgetTypeId = budgetTypeId;
    }

    public String getCompletionTime() {
        return completionTime;
    }

    public void setCompletionTime(String completionTime) {
        this.completionTime = completionTime;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getCpvCode() {
        return cpvCode;
    }

    public void setCpvCode(String cpvCode) {
        this.cpvCode = cpvCode;
    }

    public String getDeliverables() {
        return deliverables;
    }

    public void setDeliverables(String deliverables) {
        this.deliverables = deliverables;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getDevPartners() {
        return devPartners;
    }

    public void setDevPartners(String devPartners) {
        this.devPartners = devPartners;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getDocAccess() {
        return docAccess;
    }

    public void setDocAccess(String docAccess) {
        this.docAccess = docAccess;
    }

    public String getDocAvlMethod() {
        return docAvlMethod;
    }

    public void setDocAvlMethod(String docAvlMethod) {
        this.docAvlMethod = docAvlMethod;
    }

    public Date getDocEndDate() {
        return docEndDate;
    }

    public void setDocEndDate(Date docEndDate) {
        this.docEndDate = docEndDate;
    }

    public String getDocFeesMethod() {
        return docFeesMethod;
    }

    public void setDocFeesMethod(String docFeesMethod) {
        this.docFeesMethod = docFeesMethod;
    }

    public String getDocFeesMode() {
        return docFeesMode;
    }

    public void setDocFeesMode(String docFeesMode) {
        this.docFeesMode = docFeesMode;
    }

    public BigDecimal getDocFess() {
        return docFess;
    }

    public void setDocFess(BigDecimal docFess) {
        this.docFess = docFess;
    }

    public String getDocOfficeAdd() {
        return docOfficeAdd;
    }

    public void setDocOfficeAdd(String docOfficeAdd) {
        this.docOfficeAdd = docOfficeAdd;
    }

    public String getEligibilityCriteria() {
        return eligibilityCriteria;
    }

    public void setEligibilityCriteria(String eligibilityCriteria) {
        this.eligibilityCriteria = eligibilityCriteria;
    }

    public BigDecimal getEstCost() {
        return estCost;
    }

    public void setEstCost(BigDecimal estCost) {
        this.estCost = estCost;
    }

    public String getEvalType() {
        return evalType;
    }

    public void setEvalType(String evalType) {
        this.evalType = evalType;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getForeignFirm() {
        return foreignFirm;
    }

    public void setForeignFirm(String foreignFirm) {
        this.foreignFirm = foreignFirm;
    }

    public Date getIndEndDt() {
        return indEndDt;
    }

    public void setIndEndDt(Date indEndDt) {
        this.indEndDt = indEndDt;
    }

    public Date getIndStartDt() {
        return indStartDt;
    }

    public void setIndStartDt(Date indStartDt) {
        this.indStartDt = indStartDt;
    }

    public String getInvitationFor() {
        return invitationFor;
    }

    public void setInvitationFor(String invitationFor) {
        this.invitationFor = invitationFor;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationSec() {
        return locationSec;
    }

    public void setLocationSec(String locationSec) {
        this.locationSec = locationSec;
    }

    public String getLotDesc() {
        return lotDesc;
    }

    public void setLotDesc(String lotDesc) {
        this.lotDesc = lotDesc;
    }

    public String getLotNo() {
        return lotNo;
    }

    public void setLotNo(String lotNo) {
        this.lotNo = lotNo;
    }

    public String getMinistry() {
        return ministry;
    }

    public void setMinistry(String ministry) {
        this.ministry = ministry;
    }

    public String getModeOfTender() {
        return modeOfTender;
    }

    public void setModeOfTender(String modeOfTender) {
        this.modeOfTender = modeOfTender;
    }

    public Integer getOfficeId() {
        return officeId;
    }

    public void setOfficeId(Integer officeId) {
        this.officeId = officeId;
    }

    public Date getOpeningDt() {
        return openingDt;
    }

    public void setOpeningDt(Date openingDt) {
        this.openingDt = openingDt;
    }

    public String getOtherDetails() {
        return otherDetails;
    }

    public void setOtherDetails(String otherDetails) {
        this.otherDetails = otherDetails;
    }

    public String getPackageDescription() {
        return packageDescription;
    }

    public void setPackageDescription(String packageDescription) {
        this.packageDescription = packageDescription;
    }

    public String getPackageNo() {
        return packageNo;
    }

    public void setPackageNo(String packageNo) {
        this.packageNo = packageNo;
    }

    public String getPeAddress() {
        return peAddress;
    }

    public void setPeAddress(String peAddress) {
        this.peAddress = peAddress;
    }

    public String getPeCode() {
        return peCode;
    }

    public void setPeCode(String peCode) {
        this.peCode = peCode;
    }

    public String getPeContactDetails() {
        return peContactDetails;
    }

    public void setPeContactDetails(String peContactDetails) {
        this.peContactDetails = peContactDetails;
    }

    public String getPeDesignation() {
        return peDesignation;
    }

    public void setPeDesignation(String peDesignation) {
        this.peDesignation = peDesignation;
    }

    public String getPeDistrict() {
        return peDistrict;
    }

    public void setPeDistrict(String peDistrict) {
        this.peDistrict = peDistrict;
    }

    public String getPeName() {
        return peName;
    }

    public void setPeName(String peName) {
        this.peName = peName;
    }

    public String getPeOfficeName() {
        return peOfficeName;
    }

    public void setPeOfficeName(String peOfficeName) {
        this.peOfficeName = peOfficeName;
    }

    public String getPhasingOfService() {
        return phasingOfService;
    }

    public void setPhasingOfService(String phasingOfService) {
        this.phasingOfService = phasingOfService;
    }

    public String getPhasingRefNo() {
        return phasingRefNo;
    }

    public void setPhasingRefNo(String phasingRefNo) {
        this.phasingRefNo = phasingRefNo;
    }

    public BigDecimal getPkgDocFees() {
        return pkgDocFees;
    }

    public void setPkgDocFees(BigDecimal pkgDocFees) {
        this.pkgDocFees = pkgDocFees;
    }

    public Integer getPqTenderId() {
        return pqTenderId;
    }

    public void setPqTenderId(Integer pqTenderId) {
        this.pqTenderId = pqTenderId;
    }

    public Date getPreBidEndDt() {
        return preBidEndDt;
    }

    public void setPreBidEndDt(Date preBidEndDt) {
        this.preBidEndDt = preBidEndDt;
    }

    public Date getPreBidStartDt() {
        return preBidStartDt;
    }

    public void setPreBidStartDt(Date preBidStartDt) {
        this.preBidStartDt = preBidStartDt;
    }

    public String getProcurementMethod() {
        return procurementMethod;
    }

    public void setProcurementMethod(String procurementMethod) {
        this.procurementMethod = procurementMethod;
    }

    public Integer getProcurementMethodId() {
        return procurementMethodId;
    }

    public void setProcurementMethodId(Integer procurementMethodId) {
        this.procurementMethodId = procurementMethodId;
    }

    public String getProcurementNature() {
        return procurementNature;
    }

    public void setProcurementNature(String procurementNature) {
        this.procurementNature = procurementNature;
    }

    public Integer getProcurementNatureId() {
        return procurementNatureId;
    }

    public void setProcurementNatureId(Integer procurementNatureId) {
        this.procurementNatureId = procurementNatureId;
    }

    public String getProcurementType() {
        return procurementType;
    }

    public void setProcurementType(String procurementType) {
        this.procurementType = procurementType;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Integer getReTenderId() {
        return reTenderId;
    }

    public void setReTenderId(Integer reTenderId) {
        this.reTenderId = reTenderId;
    }

    public String getReoiRfpFor() {
        return reoiRfpFor;
    }

    public void setReoiRfpFor(String reoiRfpFor) {
        this.reoiRfpFor = reoiRfpFor;
    }

    public String getReoiRfpRefNo() {
        return reoiRfpRefNo;
    }

    public void setReoiRfpRefNo(String reoiRfpRefNo) {
        this.reoiRfpRefNo = reoiRfpRefNo;
    }

    public Integer getReoiTenderId() {
        return reoiTenderId;
    }

    public void setReoiTenderId(Integer reoiTenderId) {
        this.reoiTenderId = reoiTenderId;
    }

    public Date getSecurityLastDt() {
        return securityLastDt;
    }

    public void setSecurityLastDt(Date securityLastDt) {
        this.securityLastDt = securityLastDt;
    }

    public String getSecuritySubOff() {
        return securitySubOff;
    }

    public void setSecuritySubOff(String securitySubOff) {
        this.securitySubOff = securitySubOff;
    }

    public String getSourceOfFund() {
        return sourceOfFund;
    }

    public void setSourceOfFund(String sourceOfFund) {
        this.sourceOfFund = sourceOfFund;
    }

    public Integer getStdTemplateId() {
        return stdTemplateId;
    }

    public void setStdTemplateId(Integer stdTemplateId) {
        this.stdTemplateId = stdTemplateId;
    }

    public Date getSubmissionDt() {
        return submissionDt;
    }

    public void setSubmissionDt(Date submissionDt) {
        this.submissionDt = submissionDt;
    }

    public String getTenderBrief() {
        return tenderBrief;
    }

    public void setTenderBrief(String tenderBrief) {
        this.tenderBrief = tenderBrief;
    }

    public Integer getTenderDtlId() {
        return tenderDtlId;
    }

    public void setTenderDtlId(Integer tenderDtlId) {
        this.tenderDtlId = tenderDtlId;
    }

    public Integer getTenderId() {
        return tenderId;
    }

    public void setTenderId(Integer tenderId) {
        this.tenderId = tenderId;
    }

    public Integer getTenderLotSecId() {
        return tenderLotSecId;
    }

    public void setTenderLotSecId(Integer tenderLotSecId) {
        this.tenderLotSecId = tenderLotSecId;
    }

    public Date getTenderPubDt() {
        return tenderPubDt;
    }

    public void setTenderPubDt(Date tenderPubDt) {
        this.tenderPubDt = tenderPubDt;
    }

    public BigDecimal getTenderSecurityAmt() {
        return tenderSecurityAmt;
    }

    public void setTenderSecurityAmt(BigDecimal tenderSecurityAmt) {
        this.tenderSecurityAmt = tenderSecurityAmt;
    }

    public Integer getTenderSecurityDays() {
        return tenderSecurityDays;
    }

    public void setTenderSecurityDays(Integer tenderSecurityDays) {
        this.tenderSecurityDays = tenderSecurityDays;
    }

    public Date getTenderSecurityDt() {
        return tenderSecurityDt;
    }

    public void setTenderSecurityDt(Date tenderSecurityDt) {
        this.tenderSecurityDt = tenderSecurityDt;
    }

    public String getTenderStatus() {
        return tenderStatus;
    }

    public void setTenderStatus(String tenderStatus) {
        this.tenderStatus = tenderStatus;
    }

    public Integer getTenderValDays() {
        return tenderValDays;
    }

    public void setTenderValDays(Integer tenderValDays) {
        this.tenderValDays = tenderValDays;
    }

    public Date getTenderValidityDt() {
        return tenderValidityDt;
    }

    public void setTenderValidityDt(Date tenderValidityDt) {
        this.tenderValidityDt = tenderValidityDt;
    }

    public String getWorkflowStatus() {
        return workflowStatus;
    }

    public void setWorkflowStatus(String workflowStatus) {
        this.workflowStatus = workflowStatus;
    }

    // Start ICT Dohatec
    public BigDecimal getTenderSecurityAmtUSD() {
        return tenderSecurityAmtUSD;
    }

    public void setTenderSecurityAmtUSD(BigDecimal tenderSecurityAmtUSD) {
        this.tenderSecurityAmtUSD = tenderSecurityAmtUSD;
    }

    public BigDecimal getPkgDocFeesUSD() {
        return pkgDocFeesUSD;
    }

    public void setPkgDocFeesUSD(BigDecimal pkgDocFeesUSD) {
        this.pkgDocFeesUSD = pkgDocFeesUSD;
    }
    // End ICT Dohatec

    /**
     * @return the bidSecurityType
     */
    public String getBidSecurityType() {
        return bidSecurityType;
    }

    /**
     * @param bidSecurityType the bidSecurityType to set
     */
    public void setBidSecurityType(String bidSecurityType) {
        this.bidSecurityType = bidSecurityType;
    }
}
