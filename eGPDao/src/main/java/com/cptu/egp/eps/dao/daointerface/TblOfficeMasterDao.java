/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TblOfficeMasterDao extends GenericDao<TblOfficeMaster>{

    public void addTblOfficeMaster(TblOfficeMaster office);

    public void deleteTblOfficeMaster(TblOfficeMaster office);

    public void updateTblOfficeMaster(TblOfficeMaster office);

    public List<TblOfficeMaster> getAllTblOfficeMaster();

    public List<TblOfficeMaster> findTblOfficeMaster(Object... values) throws Exception;

    public List<TblOfficeMaster> findByCountTblOfficeMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblOfficeMasterCount();
}
