/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.math.BigDecimal;
import java.util.Date;

/**
 * This class is created for web-service(RHD upto CMS data Integration)
 * @author Sudhir Chavhan
 */
public class RHDIntegrationCMSDetails {

    private int PE_Id;
    private String PE_Name;
    private String PE_Code;
    private String NoA_Name;
    private int Tender_Id;
    private String Proc_Nature;
    private String Proc_Method;
    private String Proc_Type;
    private String Budget_Type;
    private String Source_Of_Funds;
    private String Project_Code;
    private String Project_Name;
    private String Tender_RefNo;
    private String Tender_InvitationFor;
    private String Tender_PkgNo;
    private String Tender_PkgDesc;
    private String Tender_Category;
    private Date Tender_PubDt;
    private Date Tender_DocLstSelD;
    private Date Tender_PreMeetingStrtDt;
    private Date Tender_PreMeetingEndDt;
    private Date Tender_ClosingDt;
    private Date Tender_OpeningDt;
    private Date Tender_SecLstSubDt;
    private String Tender_BriefDesc;
    private String Tender_EvaluationType;
    private String Tender_DocAvailable;
    private String Tender_DocFees;
    private BigDecimal Tender_DocPrice;
    private BigDecimal Tender_SecAmt;
    private String Tender_SecPayMode;
    private Date Tender_SecValidUpToDt;
    private String Tender_ValidUpToDt;
    private String Per_SecValidUpToDt;
    private String Per_SecBankName;
    private String Per_SecBankBranchName;
    private BigDecimal Per_PayAmount;
    private Date Per_PayPaidDt;
    private String Per_SecPayMode;
    private String Per_PayInstrumentNo;
    private String Per_PayIssuingBank;
    private String Per_PayIssuingBranch;
    private Date Per_PayIssuanceDt;
    private String Contract_No;
    private String Contract_LotNo;
    private String Contract_DescOfLot;
    private String Contract_StartDt;
    private String Contract_CompletionDt;
    private BigDecimal Contract_Value;
    private Date NoA_Dt;
    private String NoA_PEName;

    public String getBudget_Type() {
        return Budget_Type;
    }

    public void setBudget_Type(String Budget_Type) {
        this.Budget_Type = Budget_Type;
    }

    public String getContract_CompletionDt() {
        return Contract_CompletionDt;
    }

    public void setContract_CompletionDt(String Contract_CompletionDt) {
        this.Contract_CompletionDt = Contract_CompletionDt;
    }

    public String getContract_DescOfLot() {
        return Contract_DescOfLot;
    }

    public void setContract_DescOfLot(String Contract_DescOfLot) {
        this.Contract_DescOfLot = Contract_DescOfLot;
    }

    public String getContract_LotNo() {
        return Contract_LotNo;
    }

    public void setContract_LotNo(String Contract_LotNo) {
        this.Contract_LotNo = Contract_LotNo;
    }

    public String getContract_No() {
        return Contract_No;
    }

    public void setContract_No(String Contract_No) {
        this.Contract_No = Contract_No;
    }

    public String getContract_StartDt() {
        return Contract_StartDt;
    }

    public void setContract_StartDt(String Contract_StartDt) {
        this.Contract_StartDt = Contract_StartDt;
    }

    public BigDecimal getContract_Value() {
        return Contract_Value;
    }

    public void setContract_Value(BigDecimal Contract_Value) {
        this.Contract_Value = Contract_Value;
    }

    public Date getNoA_Dt() {
        return NoA_Dt;
    }

    public void setNoA_Dt(Date NoA_Dt) {
        this.NoA_Dt = NoA_Dt;
    }

    public String getNoA_Name() {
        return NoA_Name;
    }

    public void setNoA_Name(String NoA_Name) {
        this.NoA_Name = NoA_Name;
    }

    public String getNoA_PEName() {
        return NoA_PEName;
    }

    public void setNoA_PEName(String NoA_PEName) {
        this.NoA_PEName = NoA_PEName;
    }

    public String getPE_Code() {
        return PE_Code;
    }

    public void setPE_Code(String PE_Code) {
        this.PE_Code = PE_Code;
    }

    public int getPE_Id() {
        return PE_Id;
    }

    public void setPE_Id(int PE_Id) {
        this.PE_Id = PE_Id;
    }

    public String getPE_Name() {
        return PE_Name;
    }

    public void setPE_Name(String PE_Name) {
        this.PE_Name = PE_Name;
    }

    public BigDecimal getPer_PayAmount() {
        return Per_PayAmount;
    }

    public void setPer_PayAmount(BigDecimal Per_PayAmount) {
        this.Per_PayAmount = Per_PayAmount;
    }

    public String getPer_PayInstrumentNo() {
        return Per_PayInstrumentNo;
    }

    public void setPer_PayInstrumentNo(String Per_PayInstrumentNo) {
        this.Per_PayInstrumentNo = Per_PayInstrumentNo;
    }

    public Date getPer_PayIssuanceDt() {
        return Per_PayIssuanceDt;
    }

    public void setPer_PayIssuanceDt(Date Per_PayIssuanceDt) {
        this.Per_PayIssuanceDt = Per_PayIssuanceDt;
    }

    public String getPer_PayIssuingBank() {
        return Per_PayIssuingBank;
    }

    public void setPer_PayIssuingBank(String Per_PayIssuingBank) {
        this.Per_PayIssuingBank = Per_PayIssuingBank;
    }

    public String getPer_PayIssuingBranch() {
        return Per_PayIssuingBranch;
    }

    public void setPer_PayIssuingBranch(String Per_PayIssuingBranch) {
        this.Per_PayIssuingBranch = Per_PayIssuingBranch;
    }

    public Date getPer_PayPaidDt() {
        return Per_PayPaidDt;
    }

    public void setPer_PayPaidDt(Date Per_PayPaidDt) {
        this.Per_PayPaidDt = Per_PayPaidDt;
    }

    public String getPer_SecBankBranchName() {
        return Per_SecBankBranchName;
    }

    public void setPer_SecBankBranchName(String Per_SecBankBranchName) {
        this.Per_SecBankBranchName = Per_SecBankBranchName;
    }

    public String getPer_SecBankName() {
        return Per_SecBankName;
    }

    public void setPer_SecBankName(String Per_SecBankName) {
        this.Per_SecBankName = Per_SecBankName;
    }

    public String getPer_SecPayMode() {
        return Per_SecPayMode;
    }

    public void setPer_SecPayMode(String Per_SecPayMode) {
        this.Per_SecPayMode = Per_SecPayMode;
    }

    public String getPer_SecValidUpToDt() {
        return Per_SecValidUpToDt;
    }

    public void setPer_SecValidUpToDt(String Per_SecValidUpToDt) {
        this.Per_SecValidUpToDt = Per_SecValidUpToDt;
    }

    public String getProc_Method() {
        return Proc_Method;
    }

    public void setProc_Method(String Proc_Method) {
        this.Proc_Method = Proc_Method;
    }

    public String getProc_Nature() {
        return Proc_Nature;
    }

    public void setProc_Nature(String Proc_Nature) {
        this.Proc_Nature = Proc_Nature;
    }

    public String getProc_Type() {
        return Proc_Type;
    }

    public void setProc_Type(String Proc_Type) {
        this.Proc_Type = Proc_Type;
    }

    public String getProject_Code() {
        return Project_Code;
    }

    public void setProject_Code(String Project_Code) {
        this.Project_Code = Project_Code;
    }

    public String getProject_Name() {
        return Project_Name;
    }

    public void setProject_Name(String Project_Name) {
        this.Project_Name = Project_Name;
    }

    public String getSource_Of_Funds() {
        return Source_Of_Funds;
    }

    public void setSource_Of_Funds(String Source_Of_Funds) {
        this.Source_Of_Funds = Source_Of_Funds;
    }

    public String getTender_BriefDesc() {
        return Tender_BriefDesc;
    }

    public void setTender_BriefDesc(String Tender_BriefDesc) {
        this.Tender_BriefDesc = Tender_BriefDesc;
    }

    public String getTender_Category() {
        return Tender_Category;
    }

    public void setTender_Category(String Tender_Category) {
        this.Tender_Category = Tender_Category;
    }

    public Date getTender_ClosingDt() {
        return Tender_ClosingDt;
    }

    public void setTender_ClosingDt(Date Tender_ClosingDt) {
        this.Tender_ClosingDt = Tender_ClosingDt;
    }

    public String getTender_DocAvailable() {
        return Tender_DocAvailable;
    }

    public void setTender_DocAvailable(String Tender_DocAvailable) {
        this.Tender_DocAvailable = Tender_DocAvailable;
    }

    public String getTender_DocFees() {
        return Tender_DocFees;
    }

    public void setTender_DocFees(String Tender_DocFees) {
        this.Tender_DocFees = Tender_DocFees;
    }

    public Date getTender_DocLstSelD() {
        return Tender_DocLstSelD;
    }

    public void setTender_DocLstSelD(Date Tender_DocLstSelD) {
        this.Tender_DocLstSelD = Tender_DocLstSelD;
    }

    public BigDecimal getTender_DocPrice() {
        return Tender_DocPrice;
    }

    public void setTender_DocPrice(BigDecimal Tender_DocPrice) {
        this.Tender_DocPrice = Tender_DocPrice;
    }

    public String getTender_EvaluationType() {
        return Tender_EvaluationType;
    }

    public void setTender_EvaluationType(String Tender_EvaluationType) {
        this.Tender_EvaluationType = Tender_EvaluationType;
    }

    public int getTender_Id() {
        return Tender_Id;
    }

    public void setTender_Id(int Tender_Id) {
        this.Tender_Id = Tender_Id;
    }

    public String getTender_InvitationFor() {
        return Tender_InvitationFor;
    }

    public void setTender_InvitationFor(String Tender_InvitationFor) {
        this.Tender_InvitationFor = Tender_InvitationFor;
    }

    public Date getTender_OpeningDt() {
        return Tender_OpeningDt;
    }

    public void setTender_OpeningDt(Date Tender_OpeningDt) {
        this.Tender_OpeningDt = Tender_OpeningDt;
    }

    public String getTender_PkgDesc() {
        return Tender_PkgDesc;
    }

    public void setTender_PkgDesc(String Tender_PkgDesc) {
        this.Tender_PkgDesc = Tender_PkgDesc;
    }

    public String getTender_PkgNo() {
        return Tender_PkgNo;
    }

    public void setTender_PkgNo(String Tender_PkgNo) {
        this.Tender_PkgNo = Tender_PkgNo;
    }

    public Date getTender_PreMeetingEndDt() {
        return Tender_PreMeetingEndDt;
    }

    public void setTender_PreMeetingEndDt(Date Tender_PreMeetingEndDt) {
        this.Tender_PreMeetingEndDt = Tender_PreMeetingEndDt;
    }

    public Date getTender_PreMeetingStrtDt() {
        return Tender_PreMeetingStrtDt;
    }

    public void setTender_PreMeetingStrtDt(Date Tender_PreMeetingStrtDt) {
        this.Tender_PreMeetingStrtDt = Tender_PreMeetingStrtDt;
    }

    public Date getTender_PubDt() {
        return Tender_PubDt;
    }

    public void setTender_PubDt(Date Tender_PubDt) {
        this.Tender_PubDt = Tender_PubDt;
    }

    public String getTender_RefNo() {
        return Tender_RefNo;
    }

    public void setTender_RefNo(String Tender_RefNo) {
        this.Tender_RefNo = Tender_RefNo;
    }

    public BigDecimal getTender_SecAmt() {
        return Tender_SecAmt;
    }

    public void setTender_SecAmt(BigDecimal Tender_SecAmt) {
        this.Tender_SecAmt = Tender_SecAmt;
    }

    public Date getTender_SecLstSubDt() {
        return Tender_SecLstSubDt;
    }

    public void setTender_SecLstSubDt(Date Tender_SecLstSubDt) {
        this.Tender_SecLstSubDt = Tender_SecLstSubDt;
    }

    public String getTender_SecPayMode() {
        return Tender_SecPayMode;
    }

    public void setTender_SecPayMode(String Tender_SecPayMode) {
        this.Tender_SecPayMode = Tender_SecPayMode;
    }

    public Date getTender_SecValidUpToDt() {
        return Tender_SecValidUpToDt;
    }

    public void setTender_SecValidUpToDt(Date Tender_SecValidUpToDt) {
        this.Tender_SecValidUpToDt = Tender_SecValidUpToDt;
    }

    public String getTender_ValidUpToDt() {
        return Tender_ValidUpToDt;
    }

    public void setTender_ValidUpToDt(String Tender_ValidUpToDt) {
        this.Tender_ValidUpToDt = Tender_ValidUpToDt;
    }
}
