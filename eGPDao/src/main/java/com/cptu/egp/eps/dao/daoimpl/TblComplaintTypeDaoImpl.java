package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblComplaintTypeDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblComplaintType;
import java.util.List;
import org.apache.log4j.Logger;
import com.cptu.egp.eps.model.table.TblComplaintMaster;
/**
 *
 * @author Radha
 */
public class TblComplaintTypeDaoImpl extends AbcAbstractClass<TblComplaintType> implements TblComplaintTypeDao {
 
	final static Logger log=Logger.getLogger("TblComplaintTypeDaoImpl.class");
    @Override
    public void addTblComplaintType(TblComplaintType complaintType){
    	log.info("in dao impl for adding complaint into database");
        super.addEntity(complaintType);
    }

 /*
    @Override
    public void deleteTblComplaintType(TblComplaintType complaintType) {
        super.deleteEntity(complaintType);
    }

    @Override
    public void updateTblComplaintType(TblComplaintType complaintType) {
        super.updateEntity(complaintType);
    }
*/
    @Override
    public List<TblComplaintType> getComplaintTypes() {
    	log.info("in dao implementation for complaint types list");
        return super.getAllEntity();
    }
    
    @Override
    public TblComplaintMaster getComplaintDetails(int complaintId){
    	return (TblComplaintMaster)super.getEntity(complaintId);
    } 
    
    @Override
    public List<TblComplaintType> getComplaintTypeById(Object... values) throws Exception{
    	
    	return super.findEntity(values);
    }

   
}
