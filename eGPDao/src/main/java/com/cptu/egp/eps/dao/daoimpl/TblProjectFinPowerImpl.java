/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblProjectFinPowerDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblProjectFinPower;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblProjectFinPowerImpl extends AbcAbstractClass<TblProjectFinPower> implements TblProjectFinPowerDao{

    @Override
    public void addTblProjectFinPower(TblProjectFinPower tblProjectFinPower) {
        super.addEntity(tblProjectFinPower);
    }

    @Override
    public List<TblProjectFinPower> findTblProjectFinPower(Object... values) throws Exception {
        return super.findEntity(values);        
    }

    @Override
    public void deleteTblProjectFinPower(TblProjectFinPower tblProjectFinPower) {
        super.deleteEntity(tblProjectFinPower);       
    }

    @Override
    public void updateTblProjectFinPower(TblProjectFinPower tblProjectFinPower) {
        super.updateEntity(tblProjectFinPower);       
    }

    @Override
    public List<TblProjectFinPower> getAllTblProjectFinPower() {
        return super.getAllEntity();
    }

    @Override
    public long getTblProjectFinPowerCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblProjectFinPower> findByCountTblProjectFinPower(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
