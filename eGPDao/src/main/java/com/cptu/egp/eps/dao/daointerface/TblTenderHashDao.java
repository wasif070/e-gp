package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderHash;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblTenderHashDao extends GenericDao<TblTenderHash> {

    public void addTblTenderHash(TblTenderHash tblObj);

    public void deleteTblTenderHash(TblTenderHash tblObj);

    public void updateTblTenderHash(TblTenderHash tblObj);

    public List<TblTenderHash> getAllTblTenderHash();

    public List<TblTenderHash> findTblTenderHash(Object... values) throws Exception;

    public List<TblTenderHash> findByCountTblTenderHash(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderHashCount();
}
