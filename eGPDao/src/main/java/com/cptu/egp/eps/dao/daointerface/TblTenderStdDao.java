/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderStd;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderStdDao extends GenericDao<TblTenderStd>{

    public void addTblTenderStd(TblTenderStd tblObj);

    public void deleteTblTenderStd(TblTenderStd tblObj);

    public void updateTblTenderStd(TblTenderStd tblObj);

    public List<TblTenderStd> getAllTblTenderStd();

    public List<TblTenderStd> findTblTenderStd(Object... values) throws Exception;

    public List<TblTenderStd> findByCountTblTenderStd(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderStdCount();
}
