/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.math.BigDecimal;

/**
 *
 * @author Administrator
 */
public class CommitteDtBean {

    private BigDecimal minTenderVal;
    private BigDecimal maxTenderVal;
    private int minMemReq;
    private int maxMemReq;
    private int minMemOutSidePe;
    private int minMemFromPe;
    private int minMemFromTec;
    private String pNature;
    private String committeeId;

    public String getCommitteeId() {
        return committeeId;
    }

    public void setCommitteeId(String committeeId) {
        this.committeeId = committeeId;
    }

    public String getpNature() {
        return pNature;
    }

    public void setpNature(String pNature) {
        this.pNature = pNature;
    }

    public int getMaxMemReq() {
        return maxMemReq;
    }

    public void setMaxMemReq(int maxMemReq) {
        this.maxMemReq = maxMemReq;
    }   

    public int getMinMemFromPe() {
        return minMemFromPe;
    }

    public void setMinMemFromPe(int minMemFromPe) {
        this.minMemFromPe = minMemFromPe;
    }

    public int getMinMemFromTec() {
        return minMemFromTec;
    }

    public void setMinMemFromTec(int minMemFromTec) {
        this.minMemFromTec = minMemFromTec;
    }

    public int getMinMemOutSidePe() {
        return minMemOutSidePe;
    }

    public void setMinMemOutSidePe(int minMemOutSidePe) {
        this.minMemOutSidePe = minMemOutSidePe;
    }

    public int getMinMemReq() {
        return minMemReq;
    }

    public void setMinMemReq(int minMemReq) {
        this.minMemReq = minMemReq;
    }

    public BigDecimal getMaxTenderVal() {
        return maxTenderVal;
    }

    public void setMaxTenderVal(BigDecimal maxTenderVal) {
        this.maxTenderVal = maxTenderVal;
    }

    public BigDecimal getMinTenderVal() {
        return minTenderVal;
    }

    public void setMinTenderVal(BigDecimal minTenderVal) {
        this.minTenderVal = minTenderVal;
    }
}
