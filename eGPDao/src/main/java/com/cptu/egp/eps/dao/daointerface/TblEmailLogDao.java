/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.model.table.TblEmailLog;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TblEmailLogDao {

    public void addTblEmailLog(TblEmailLog tblObj);

    public void deleteTblEmailLog(TblEmailLog tblObj);

    public void updateTblEmailLog(TblEmailLog tblObj);

    public List<TblEmailLog> getAllTblEmailLog();

    public List<TblEmailLog> findTblEmailLog(Object... values) throws Exception;

    public List<TblEmailLog> findByCountTblEmailLog(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEmailLogCount();
}
