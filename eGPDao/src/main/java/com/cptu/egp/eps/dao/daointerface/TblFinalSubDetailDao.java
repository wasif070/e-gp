package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblFinalSubDetail;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblFinalSubDetailDao extends GenericDao<TblFinalSubDetail> {

    public void addTblFinalSubDetail(TblFinalSubDetail tblObj);

    public void deleteTblFinalSubDetail(TblFinalSubDetail tblObj);

    public void updateTblFinalSubDetail(TblFinalSubDetail tblObj);

    public List<TblFinalSubDetail> getAllTblFinalSubDetail();

    public List<TblFinalSubDetail> findTblFinalSubDetail(Object... values) throws Exception;

    public List<TblFinalSubDetail> findByCountTblFinalSubDetail(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblFinalSubDetailCount();
}
