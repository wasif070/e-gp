package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblReportLotsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblReportLots;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblReportLotsImpl extends AbcAbstractClass<TblReportLots> implements TblReportLotsDao {

    @Override
    public void addTblReportLots(TblReportLots master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblReportLots(TblReportLots master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblReportLots(TblReportLots master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblReportLots> getAllTblReportLots() {
        return super.getAllEntity();
    }

    @Override
    public List<TblReportLots> findTblReportLots(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblReportLotsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblReportLots> findByCountTblReportLots(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateInsertAllReportLots(List<TblReportLots> list) {
        super.updateAll(list);
    }
}
