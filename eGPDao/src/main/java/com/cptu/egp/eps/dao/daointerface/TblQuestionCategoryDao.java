package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblQuestionCategory;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblQuestionCategoryDao extends GenericDao<TblQuestionCategory> {

    public void addTblQuestionCategory(TblQuestionCategory tblObj);

    public void deleteTblQuestionCategory(TblQuestionCategory tblObj);

    public void updateTblQuestionCategory(TblQuestionCategory tblObj);

    public List<TblQuestionCategory> getAllTblQuestionCategory();

    public List<TblQuestionCategory> findTblQuestionCategory(Object... values) throws Exception;

    public List<TblQuestionCategory> findByCountTblQuestionCategory(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblQuestionCategoryCount();
}
