package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvSsvariDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvSsvari;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsSrvSsvariImpl extends AbcAbstractClass<TblCmsSrvSsvari> implements TblCmsSrvSsvariDao {

    @Override
    public void addTblCmsSrvSsvari(TblCmsSrvSsvari cmsInvoiceDetails){
        super.addEntity(cmsInvoiceDetails);
    }

    @Override
    public void deleteTblCmsSrvSsvari(TblCmsSrvSsvari cmsInvoiceDetails) {
        super.deleteEntity(cmsInvoiceDetails);
    }

    @Override
    public void updateTblCmsSrvSsvari(TblCmsSrvSsvari cmsInvoiceDetails) {
        super.updateEntity(cmsInvoiceDetails);
    }

    @Override
    public List<TblCmsSrvSsvari> getAllTblCmsSrvSsvari() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvSsvari> findTblCmsSrvSsvari(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvSsvariCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvSsvari> findByCountTblCmsSrvSsvari(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }


    @Override
    public void updateOrSaveEstCost(List<TblCmsSrvSsvari> estCost) {
       super.updateAll(estCost);
    }
}
