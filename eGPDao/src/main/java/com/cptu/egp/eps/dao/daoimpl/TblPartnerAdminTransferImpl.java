package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblPartnerAdminTransferDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblPartnerAdminTransfer;
import java.util.List;

/**
 *
 * @author rishita
 */
public class TblPartnerAdminTransferImpl extends AbcAbstractClass<TblPartnerAdminTransfer> implements TblPartnerAdminTransferDao {

    @Override
    public void addTblPartnerAdminTransfer(TblPartnerAdminTransfer partnerAdminTransfer){
        super.addEntity(partnerAdminTransfer);
    }

    @Override
    public void deleteTblPartnerAdminTransfer(TblPartnerAdminTransfer partnerAdminTransfer) {
        super.deleteEntity(partnerAdminTransfer);
    }

    @Override
    public void updateTblPartnerAdminTransfer(TblPartnerAdminTransfer partnerAdminTransfer) {
        super.updateEntity(partnerAdminTransfer);
    }

    @Override
    public List<TblPartnerAdminTransfer> getAllTblPartnerAdminTransfer() {
        return super.getAllEntity();
    }

    @Override
    public List<TblPartnerAdminTransfer> findTblPartnerAdminTransfer(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblPartnerAdminTransferCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblPartnerAdminTransfer> findByCountTblPartnerAdminTransfer(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
