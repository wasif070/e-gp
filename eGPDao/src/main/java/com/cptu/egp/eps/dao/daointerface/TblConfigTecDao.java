package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblConfigTec;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblConfigTecDao extends GenericDao<TblConfigTec> {

    public void addTblConfigTec(TblConfigTec tblObj);

    public void deleteTblConfigTec(TblConfigTec tblObj);

    public void updateTblConfigTec(TblConfigTec tblObj);

    public List<TblConfigTec> getAllTblConfigTec();

    public List<TblConfigTec> findTblConfigTec(Object... values) throws Exception;

    public List<TblConfigTec> findByCountTblConfigTec(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblConfigTecCount();
}
