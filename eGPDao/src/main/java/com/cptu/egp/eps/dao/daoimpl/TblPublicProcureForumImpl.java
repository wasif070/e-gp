package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblPublicProcureForumDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblPublicProcureForum;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblPublicProcureForumImpl extends AbcAbstractClass<TblPublicProcureForum> implements TblPublicProcureForumDao {

    @Override
    public void addTblPublicProcureForum(TblPublicProcureForum master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblPublicProcureForum(TblPublicProcureForum master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblPublicProcureForum(TblPublicProcureForum master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblPublicProcureForum> getAllTblPublicProcureForum() {
        return super.getAllEntity();
    }

    @Override
    public List<TblPublicProcureForum> findTblPublicProcureForum(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblPublicProcureForumCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblPublicProcureForum> findByCountTblPublicProcureForum(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
