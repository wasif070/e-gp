/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblCascadeMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCascadeLinkMaster;
import java.util.List;

/**
 *
 * @author tejasree.goriparthi
 */
public class TblCascadeMasterImpl extends AbcAbstractClass<TblCascadeLinkMaster> implements TblCascadeMasterDao {

    @Override
    public List<TblCascadeLinkMaster> getAllTblCascadeMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<Object> findTblCascadeMaster(String query) throws Exception {
        return super.createQueryForObject(query);
    }

}
