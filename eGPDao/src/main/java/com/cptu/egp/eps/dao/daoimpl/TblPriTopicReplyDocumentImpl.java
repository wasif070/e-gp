package com.cptu.egp.eps.dao.daoimpl;


import com.cptu.egp.eps.dao.daointerface.TblPriTopicReplyDocumentDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblPriTopicReplyDocument;
import java.util.List;


public class TblPriTopicReplyDocumentImpl extends AbcAbstractClass<TblPriTopicReplyDocument> implements TblPriTopicReplyDocumentDao {

	@Override 
	public void addTblPriTopicReplyDocument(TblPriTopicReplyDocument master){
		super.addEntity(master);
	}
	@Override 
	public void deleteTblPriTopicReplyDocument(TblPriTopicReplyDocument master) {
		super.deleteEntity(master);
	}
	@Override 
	public void updateTblPriTopicReplyDocument(TblPriTopicReplyDocument master) {
		super.updateEntity(master);
	}
	@Override 
	public List<TblPriTopicReplyDocument> getAllTblPriTopicReplyDocument() {
		return super.getAllEntity();
	}
	@Override 
	public List<TblPriTopicReplyDocument> findTblPriTopicReplyDocument(Object... values) throws Exception {
		return super.findEntity(values);
	}
	@Override 
	public long getTblPriTopicReplyDocumentCount() {
		return super.getEntityCount();
	}
	@Override 
	public List<TblPriTopicReplyDocument> findByCountTblPriTopicReplyDocument(int firstResult, int maxResult, Object... values) throws Exception {
		return super.findByCountEntity(firstResult, maxResult, values);
	}
	@Override 
	public void updateOrSaveTblPriTopicReplyDocument(List<TblPriTopicReplyDocument> tblObj) {
		super.updateAll(tblObj);
	}
}