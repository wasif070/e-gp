/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderForms;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderFormsDao extends GenericDao<TblTenderForms>{

    public void addTblTenderForms(TblTenderForms tblObj);

    public void deleteTblTenderForms(TblTenderForms tblObj);

    public void updateTblTenderForms(TblTenderForms tblObj);

    public List<TblTenderForms> getAllTblTenderForms();

    public List<TblTenderForms> findTblTenderForms(Object... values) throws Exception;

    public List<TblTenderForms> findByCountTblTenderForms(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderFormsCount();
}
