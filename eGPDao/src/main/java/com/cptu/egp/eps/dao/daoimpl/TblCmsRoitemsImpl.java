package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsRoitemsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsRoitems;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsRoitemsImpl extends AbcAbstractClass<TblCmsRoitems> implements TblCmsRoitemsDao {

    @Override
    public void addTblCmsRoitems(TblCmsRoitems cmsWpDetail){
        super.addEntity(cmsWpDetail);
    }

    @Override
    public void deleteTblCmsRoitems(TblCmsRoitems cmsWpDetail) {
        super.deleteEntity(cmsWpDetail);
    }

    @Override
    public void updateTblCmsRoitems(TblCmsRoitems cmsWpDetail) {
        super.updateEntity(cmsWpDetail);
    }

    @Override
    public List<TblCmsRoitems> getAllTblCmsRoitems() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsRoitems> findTblCmsRoitems(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsRoitemsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsRoitems> findByCountTblCmsRoitems(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsRoitems> estCost) {
       super.updateAll(estCost);
    }
}
