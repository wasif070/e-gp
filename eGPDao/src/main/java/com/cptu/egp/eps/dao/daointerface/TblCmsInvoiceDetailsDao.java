package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsInvoiceDetails;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCmsInvoiceDetailsDao extends GenericDao<TblCmsInvoiceDetails> {

    public void addTblCmsInvoiceDetails(TblCmsInvoiceDetails event);

    public void deleteTblCmsInvoiceDetails(TblCmsInvoiceDetails event);

    public void updateTblCmsInvoiceDetails(TblCmsInvoiceDetails event);

    public List<TblCmsInvoiceDetails> getAllTblCmsInvoiceDetails();

    public List<TblCmsInvoiceDetails> findTblCmsInvoiceDetails(Object... values) throws Exception;

    public List<TblCmsInvoiceDetails> findByCountTblCmsInvoiceDetails(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsInvoiceDetailsCount();

    public void updateOrSaveEstCost(List<TblCmsInvoiceDetails> estCost);
}
