/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.math.BigDecimal;
import java.util.List;
import java.util.Date;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
/**
 *
 * @author Ahsan
 */
public class SPGetUpdateTenderInfoForICTGoods extends StoredProcedure {

        private static final Logger LOGGER = Logger.getLogger(SPGetUpdateTenderInfoForICTGoods.class);


        public SPGetUpdateTenderInfoForICTGoods(BasicDataSource dataSource, String procName){

       this.setDataSource(dataSource);
       this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
       LOGGER.debug("Proc Name: "+ procName);

       this.declareParameter(new SqlParameter("v_actionName_inVC", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_tenderId_inInt", Types.INTEGER));


    }

    public List<Object[]> executeProcedure(String actionName,int tenderId)
    {
        Map inParams = new HashMap();
        inParams.put("v_actionName_inVC", actionName);
        inParams.put("v_tenderId_inInt", tenderId);


        this.compile();
        List<Object[]> details = new ArrayList<Object[]>();
        try{
             ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
             for (LinkedHashMap<String, Object> linkedHashMap : list) {
                 Object[] objList = new Object[1];
                 System.out.print(list);
                // objList[0] = linkedHashMap.get("bidTableId");
                 objList[0] = linkedHashMap.get("tenderFormId");
                // objList[2] = linkedHashMap.get("tenderTableId");
                // details.
                 details.add(objList);
                
            }
        }

        catch(Exception e){
        LOGGER.error("SPGetUpdateTenderInfoForICTGoods : "+ e);
        }

        return details;
    }




}