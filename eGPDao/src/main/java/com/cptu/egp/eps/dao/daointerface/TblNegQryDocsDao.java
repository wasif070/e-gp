package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblNegQryDocs;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblNegQryDocsDao extends GenericDao<TblNegQryDocs> {

    public void addTblNegQryDocs(TblNegQryDocs tblNegQryDocs);

    public void deleteTblNegQryDocs(TblNegQryDocs tblNegQryDocs);

    public void deleteAllTblNegQryDocs(Collection tblNegQryDocs);

    public void updateTblNegQryDocs(TblNegQryDocs tblNegQryDocs);

    public List<TblNegQryDocs> getAllTblNegQryDocs();

    public List<TblNegQryDocs> findTblNegQryDocs(Object... values) throws Exception;

    public List<TblNegQryDocs> findByCountTblNegQryDocs(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblNegQryDocsCount();
}
