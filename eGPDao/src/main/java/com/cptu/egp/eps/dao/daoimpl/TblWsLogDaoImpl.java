/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblWsLogDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblWsLog;
import java.util.List;

/**
 *
 * @author Sreenu
 */
public class TblWsLogDaoImpl extends AbcAbstractClass<TblWsLog> implements TblWsLogDao {

    @Override
    public void addTblWsLog(TblWsLog tblWsLog) {
        super.addEntity(tblWsLog);
    }

    @Override
    public void deleteTblWsLog(TblWsLog tblWsLog) {
        super.deleteEntity(tblWsLog);
    }

    @Override
    public void updateTblWsLog(TblWsLog tblWsLog) {
        super.updateEntity(tblWsLog);
    }

    @Override
    public List<TblWsLog> getAllTblWsLogs() {
        return super.getAllEntity();
    }

    @Override
    public List<TblWsLog> findTblWsLog(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public List<TblWsLog> findByCountTblWsLog(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblWsLogCount() {
        return super.getEntityCount();
    }
}
