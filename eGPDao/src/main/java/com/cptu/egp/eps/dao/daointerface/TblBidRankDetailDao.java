package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblBidRankDetail;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblBidRankDetailDao extends GenericDao<TblBidRankDetail> {

    public void addTblBidRankDetail(TblBidRankDetail tblObj);

    public void deleteTblBidRankDetail(TblBidRankDetail tblObj);

    public void updateTblBidRankDetail(TblBidRankDetail tblObj);

    public List<TblBidRankDetail> getAllTblBidRankDetail();

    public List<TblBidRankDetail> findTblBidRankDetail(Object... values) throws Exception;

    public List<TblBidRankDetail> findByCountTblBidRankDetail(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblBidRankDetailCount();

     public void updateOrSaveBidRankDetail(List<TblBidRankDetail> bidRankDetails);
}
