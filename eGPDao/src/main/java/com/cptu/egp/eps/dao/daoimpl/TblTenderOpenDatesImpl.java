/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderOpenDatesDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderOpenDates;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderOpenDatesImpl extends AbcAbstractClass<TblTenderOpenDates> implements TblTenderOpenDatesDao{

    @Override
    public void addTblTenderOpenDates(TblTenderOpenDates tenderOpenDates) {
        super.addEntity(tenderOpenDates);
    }

    @Override
    public List<TblTenderOpenDates> findTblTenderOpenDates(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblTenderOpenDates(TblTenderOpenDates tenderOpenDates) {

        super.deleteEntity(tenderOpenDates);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblTenderOpenDates(Collection event) {
        super.deleteAll(event);
    }

    @Override
    public void updateTblTenderOpenDates(TblTenderOpenDates tenderOpenDates) {

        super.updateEntity(tenderOpenDates);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblTenderOpenDates> getAllTblTenderOpenDates() {
        return super.getAllEntity();
    }

    @Override
    public long getTblTenderOpenDatesCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderOpenDates> findByCountTblTenderOpenDates(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
