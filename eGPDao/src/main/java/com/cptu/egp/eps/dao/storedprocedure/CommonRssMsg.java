/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

/**
 *Pojo for SPRss
 * @author Administrator
 */
public class CommonRssMsg {

    private String rssXml;

    public String getRssXml() {
        return rssXml;
    }

    public void setRssXml(String rssXml) {
        this.rssXml = rssXml;
    }

}
