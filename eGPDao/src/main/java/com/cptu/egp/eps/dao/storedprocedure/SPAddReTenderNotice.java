/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author TaherT
 */
public class SPAddReTenderNotice extends StoredProcedure {
    private static final Logger LOGGER = Logger.getLogger(SPAddReTenderNotice.class);
    public SPAddReTenderNotice(BasicDataSource dataSource, String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("@v_Action_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_Tenderid_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_EventType_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_userIds_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_loginUserName", Types.VARCHAR));
    }

    /**
     * Execute stored procedure : p_add_retendernotice
     * To perform Insert/Update Tender Notice.
     * @param tenderId
     * @param action
     * @param eventType
     * @param userIds -- FOR REOI 7 TENDERER MAPPING
     * @param strLoginUserID -- FOR RE-TENDER ACTUAL Logged PENAME
     * @return CommonMsgChk object contains flags indicate operation success status.
     */
    public CommonMsgChk executeProcedure(int tenderId, String action, String eventType,String userIds,String strLoginUserID) {
        Map inParams = new HashMap();
        inParams.put("@v_Action_inVc", action);
        inParams.put("@v_Tenderid_inInt", tenderId);
        inParams.put("@v_EventType_inVc", eventType);
        inParams.put("@v_userIds_inVc", userIds);
        inParams.put("@v_loginUserName", strLoginUserID);
        this.compile();
        CommonMsgChk msg = new CommonMsgChk();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                msg.setFlag((Boolean) linkedHashMap.get("flag"));
                msg.setMsg((String) linkedHashMap.get("Msg"));
                msg.setId((Integer) linkedHashMap.get("Id"));
            }
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            LOGGER.error("SPAddReTenderNotice : "+ sw.toString());
        }
        return msg;
    }
}
