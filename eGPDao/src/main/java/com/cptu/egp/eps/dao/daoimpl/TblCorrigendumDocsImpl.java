/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblCorrigendumDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCorrigendumDocs;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblCorrigendumDocsImpl extends AbcAbstractClass<TblCorrigendumDocs> implements TblCorrigendumDocsDao{

    @Override
    public void addTblCorrigendumDocs(TblCorrigendumDocs tblCorrigendumDocs) {
        super.addEntity(tblCorrigendumDocs);
    }

    @Override
    public List<TblCorrigendumDocs> findTblCorrigendumDocs(Object... values) throws Exception {
        return super.findEntity(values);       
    }

    @Override
    public void deleteTblCorrigendumDocs(TblCorrigendumDocs tblCorrigendumDocs) {
        super.deleteEntity(tblCorrigendumDocs);
    }

    @Override
    public void updateTblCorrigendumDocs(TblCorrigendumDocs tblCorrigendumDocs) {
        super.updateEntity(tblCorrigendumDocs);        
    }

    @Override
    public List<TblCorrigendumDocs> getAllTblCorrigendumDocs() {
        return super.getAllEntity();
    }

    @Override
    public long getTblCorrigendumDocsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCorrigendumDocs> findByCountTblCorrigendumDocs(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
