/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderAdvertisementDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderAdvertisement;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderAdvertisementImpl extends AbcAbstractClass<TblTenderAdvertisement> implements TblTenderAdvertisementDao {

    @Override
    public void addTblTenderAdvertisement(TblTenderAdvertisement tenderAdvertisement) {
        super.addEntity(tenderAdvertisement);
    }

    @Override
    public void deleteTblTenderAdvertisement(TblTenderAdvertisement tenderAdvertisement) {
        super.deleteEntity(tenderAdvertisement);
    }

    @Override
    public void updateTblTenderAdvertisement(TblTenderAdvertisement tenderAdvertisement) {
        super.updateEntity(tenderAdvertisement);
    }

    @Override
    public List<TblTenderAdvertisement> getAllTblTenderAdvertisement() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderAdvertisement> findTblTenderAdvertisement(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderAdvertisementCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderAdvertisement> findByCountTblTenderAdvertisement(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
