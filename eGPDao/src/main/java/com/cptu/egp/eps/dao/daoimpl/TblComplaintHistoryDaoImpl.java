package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblComplaintHistoryDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblComplaintHistory;
import java.util.List;
import org.hibernate.Query;
import org.apache.log4j.Logger;

/**
 *
 * @author Radha
 */
public class TblComplaintHistoryDaoImpl extends AbcAbstractClass<TblComplaintHistory> implements TblComplaintHistoryDao {

	final static Logger log=Logger.getLogger(TblComplaintHistoryDaoImpl.class);
    @Override
    public void addHistory(TblComplaintHistory complaintHistory){
        super.addEntity(complaintHistory);
    }
    
     
    public List<Object[]> getComplaintHistory(int id,int complaintLevelId,int userTypeId){ 
    	
    	log.error("complaint his dao");
    	
    	StringBuilder query=new StringBuilder();
    	query.append("from TblComplaintHistory where complaintId="+id+"and complaintLevelId="+complaintLevelId + "and userTypeId="+userTypeId +" order by  commentsdt desc");
        String queryStr=query.toString();
        log.error("in dao after query creation size of historyy list is"); 
        
        System.out.println("query is"+queryStr);
    	
    	
    	return super.createQuery(queryStr);    
    }

    public List<Object[]> getComplaintHistoryDetails(int id,int complaintLevelId) throws Exception{
            log.error("complaint hisdet dao");
    	
    	StringBuilder query=new StringBuilder();
    	query.append("from TblComplaintHistory where complaintId="+id+"and complaintLevelId="+complaintLevelId +" order by  commentsdt asc"); 
        String queryStr=query.toString();
        log.error("in dao after query creation size of historyy list is"); 
        
        System.out.println("query is"+queryStr);
    	
    	
    	return super.createQuery(queryStr);  
    	
    	
    }

    
    //for pe 
    public List<Object[]> getComplaintHistoryPe(int tenderId) throws Exception{
        log.error("complaint hisdet dao");
	
	String queryStr="FROM TblComplaintHistory history WHERE history.complaintHistId =(SELECT Max(history1.complaintHistId) from TblComplaintHistory history1 ,TblComplaintMaster master WHERE history1.complaint.complaintId=history.complaint.complaintId and history1.complaint.complaintId=master.complaintId and history1.complaintLevel.complaintLevelId=1 and master.tenderMaster.tenderId="+tenderId+") order by history.commentsdt desc";
	 
   
    log.error("in dao after query creation size of historyy list is"); 
    
    System.out.println("query is"+queryStr);
	
	
	return super.createQuery(queryStr);   
	
	
}
    public List<Object[]> getComplaintHistoryRP(int userId) throws Exception{
        log.error("complaint hisdet dao");

	String queryStr="from TblComplaintMaster tc where"
                + " tc.panelId in (Select rp.reviewPanelId from TblReviewPanel rp where rp.userId="+userId+")";


    log.error("in dao after query creation size of historyy list is");

    System.out.println("query is"+queryStr);


	return super.createQuery(queryStr);


}
      
    
    
 /*
    @Override
    public void deleteTblComplaintHistory(TblComplaintHistory complaintHistory) {
        super.deleteEntity(complaintHistory);
    }

    @Override
    public void updateTblComplaintHistory(TblComplaintHistory complaintHistory) {
        super.updateEntity(complaintHistory);
    }

    @Override
    public List<TblComplaintHistory> getAllComplaintHistories() {
        return super.getAllEntity();
    }

   */
}
