package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblEmployeeTrasferDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEmployeeTrasfer;
import java.util.List;

/**
 *
 * @author rishita
 */
public class TblEmployeeTrasferImpl extends AbcAbstractClass<TblEmployeeTrasfer> implements TblEmployeeTrasferDao {

    @Override
    public void addTblEmployeeTrasfer(TblEmployeeTrasfer master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblEmployeeTrasfer(TblEmployeeTrasfer master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblEmployeeTrasfer(TblEmployeeTrasfer master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblEmployeeTrasfer> getAllTblEmployeeTrasfer() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEmployeeTrasfer> findTblEmployeeTrasfer(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEmployeeTrasferCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEmployeeTrasfer> findByCountTblEmployeeTrasfer(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
