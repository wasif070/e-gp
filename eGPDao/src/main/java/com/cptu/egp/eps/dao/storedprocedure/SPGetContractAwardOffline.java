/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.math.BigDecimal;
import java.util.List;
import java.util.Date;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
/**
 *
 * @author Ahsan
 */
public class SPGetContractAwardOffline extends StoredProcedure {

        private static final Logger LOGGER = Logger.getLogger(SPGetContractAwardOffline.class);

        
        public SPGetContractAwardOffline(BasicDataSource dataSource, String procName){

       this.setDataSource(dataSource);
       this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
       LOGGER.debug("Proc Name: "+ procName);

       this.declareParameter(new SqlParameter("searchFlag", Types.VARCHAR));
       this.declareParameter(new SqlParameter("status", Types.VARCHAR));
       this.declareParameter(new SqlParameter("procNature", Types.VARCHAR));
       this.declareParameter(new SqlParameter("procMethod", Types.VARCHAR));      
       this.declareParameter(new SqlParameter("value", Types.VARCHAR));
       this.declareParameter(new SqlParameter("refNo", Types.VARCHAR));
       this.declareParameter(new SqlParameter("advDateFrom", Types.VARCHAR));
       this.declareParameter(new SqlParameter("advDateTo", Types.VARCHAR));
       this.declareParameter(new SqlParameter("ministry", Types.VARCHAR));
       this.declareParameter(new SqlParameter("action", Types.VARCHAR));
        this.declareParameter(new SqlParameter("comment", Types.VARCHAR));

       /*this.declareParameter(new SqlParameter("v_ViewType_inVc", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_TenderId_Int", Types.INTEGER));
       this.declareParameter(new SqlParameter("v_AppId_inInt", Types.INTEGER));

       this.declareParameter(new SqlParameter("v_UserId_inInt", Types.INTEGER));
       this.declareParameter(new SqlParameter("v_procurementNatureId_inInt", Types.INTEGER));
       this.declareParameter(new SqlParameter("v_procurementType_inVc", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_procurementMethod_inInt", Types.INTEGER));
       this.declareParameter(new SqlParameter("v_reoiRfpRefNo_inVc", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_tenderPubDtFrom_inVc", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_tenderPubDtTo_inVc", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_Status_inVc", Types.VARCHAR));

       this.declareParameter(new SqlParameter("v_Ministry_inVc", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_Division_inVc", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_Agency_inVc", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_PeOfficeName", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_SubmissionDtFrom_inVc", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_SubmissionDtTo_inVc", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_Page_inInt", Types.INTEGER));
       this.declareParameter(new SqlParameter("v_RecordPerPage_inInt", Types.INTEGER)); */
    }
        
    public List<ContractAwardOfflineDetails> executeProcedure(String searchFlag,String status,String procNature,String procMethod,String refNo,String value,String advDateFrom,String advDateTo,String ministry,String action,String comment)
   // public List<ContractAwardOfflineDetails> executeProcedure(String searchFlag,String status,String procNature,String procMethod)
    {
        Map inParams = new HashMap();
        inParams.put("searchFlag", searchFlag);
        inParams.put("status", status);
        inParams.put("procNature", procNature);
        inParams.put("procMethod", procMethod);
        inParams.put("value", value);
        inParams.put("refNo", refNo);
        inParams.put("advDateFrom", advDateFrom);
        inParams.put("advDateTo", advDateTo);
        inParams.put("ministry", ministry);
        inParams.put("action", action);
        inParams.put("comment", comment);
        
        this.compile();
        List<ContractAwardOfflineDetails> details = new ArrayList<ContractAwardOfflineDetails>();
        try{
             ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
             for (LinkedHashMap<String, Object> linkedHashMap : list) {
                ContractAwardOfflineDetails offlineDetails =new ContractAwardOfflineDetails();

                offlineDetails.setContarctAIdOL((Integer)linkedHashMap.get("conAwardOfflineId"));
                offlineDetails.setMinistry((String)linkedHashMap.get("ministry"));
                offlineDetails.setDivision((String)linkedHashMap.get("division"));
                offlineDetails.setAgency((String)linkedHashMap.get("agency"));
                offlineDetails.setPeOfficeName((String)linkedHashMap.get("peOfficeName"));
                offlineDetails.setPeCode((String)linkedHashMap.get("peCode"));
                offlineDetails.setPeDistrict((String)linkedHashMap.get("peDistrict"));
                offlineDetails.setAwardForPNature((String)linkedHashMap.get("AwardForPNature"));
                offlineDetails.setRefNo((String)linkedHashMap.get("RefNo"));
                offlineDetails.setProcurementMethod((String)linkedHashMap.get("procurementMethod"));
                offlineDetails.setBudgetType((String)linkedHashMap.get("budgetType"));
                offlineDetails.setSourceOfFund((String)linkedHashMap.get("sourceOfFund"));
                offlineDetails.setDevPartners((String)linkedHashMap.get("devPartners"));
                offlineDetails.setProjectCode((String)linkedHashMap.get("projectCode"));
                offlineDetails.setProjectName((String)linkedHashMap.get("projectName"));
                offlineDetails.setPackageNo((String)linkedHashMap.get("packageNo"));
                offlineDetails.setPackageName((String)linkedHashMap.get("packageName"));
                offlineDetails.setDateofAdvertisement((Date)linkedHashMap.get("DateofAdvertisement"));
                offlineDetails.setDateofNOA((Date)linkedHashMap.get("DateofNOA"));
                offlineDetails.setDateofContractSign((Date)linkedHashMap.get("DateofContractSign"));
                offlineDetails.setDateofPCCompletion((Date)linkedHashMap.get("DateofPCCompletion"));
                offlineDetails.setSold((String)linkedHashMap.get("Sold"));
                offlineDetails.setReceived((String)linkedHashMap.get("Received"));
                offlineDetails.setResponse((String)linkedHashMap.get("Response"));
                offlineDetails.setDescriptionofContract((String)linkedHashMap.get("DescriptionofContract"));
                offlineDetails.setContractValue((BigDecimal)linkedHashMap.get("ContractValue"));
                offlineDetails.setNameofTenderer((String)linkedHashMap.get("NameofTenderer"));
                offlineDetails.setAddressofTenderer((String)linkedHashMap.get("AddressofTenderer"));
                offlineDetails.setDeliveryPlace((String)linkedHashMap.get("DeliveryPlace"));
                offlineDetails.setIsSamePersonNOA((Short)linkedHashMap.get("IsSamePersonNOA"));
                offlineDetails.setNOAReason((String)linkedHashMap.get("NOAReason"));
                offlineDetails.setIsPSecurityDueTime((Short)linkedHashMap.get("IsPSecurityDueTime"));
                offlineDetails.setPSecurityReason((String)linkedHashMap.get("PSecurityReason"));
                offlineDetails.setIsSignedDueTime((Short)linkedHashMap.get("IsSignedDueTime"));
                offlineDetails.setSignedReason((String)linkedHashMap.get("SignedReason"));
                offlineDetails.setOfficerName((String)linkedHashMap.get("OfficerName"));
                offlineDetails.setOfficerDesignation((String)linkedHashMap.get("OfficerDesignation"));
                offlineDetails.setStatus((String)linkedHashMap.get("Status"));
                offlineDetails.setUserID((Integer)linkedHashMap.get("UserID"));
                offlineDetails.setDate((Date)linkedHashMap.get("Date"));

                details.add(offlineDetails);
            }
        }

        catch(Exception e){
        LOGGER.error("SPGetContractAwardOffline : "+ e);
        }

        return details;
    }

    public List<CommonSPReturn> executeProcedure(String refNo)
    {
        boolean isDeleted = false;
        Map inParams = new HashMap();
        inParams.put("searchFlag", "");
        inParams.put("status", "");
        inParams.put("procNature", "");
        inParams.put("procMethod", "");
        inParams.put("value", "");
        inParams.put("refNo", refNo);
        inParams.put("advDateFrom", "");
        inParams.put("advDateTo", "");
        inParams.put("ministry", "");
        inParams.put("action", "delete");
        inParams.put("comment", "");
        this.compile();
        List<CommonSPReturn> details = new ArrayList<CommonSPReturn>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonSPReturn commonSPReturn = new CommonSPReturn();
                //commonSPReturn.setId(new BigDecimal(Integer.toString((Integer)linkedHashMap.get("PackageId"))));
                commonSPReturn.setFlag((Boolean)linkedHashMap.get("flag"));
                commonSPReturn.setMsg((String) linkedHashMap.get("Message"));
                details.add(commonSPReturn);
            }
        } catch (Exception e) {
           LOGGER.error("SPGetContractAwardOffline : " + e);
        }
        return details;
    }


}
