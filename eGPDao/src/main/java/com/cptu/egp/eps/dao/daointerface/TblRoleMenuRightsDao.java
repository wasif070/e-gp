package com.cptu.egp.eps.dao.daointerface;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblRoleMenuRights;
import java.util.List;

/**
 *
 * @author dipal.shah
 */

	public interface TblRoleMenuRightsDao extends GenericDao<TblRoleMenuRights> {

	public void addtblRoleMenuRights(TblRoleMenuRights tblObj);

	public void deletetblRoleMenuRights(TblRoleMenuRights tblObj);

	public void updatetblRoleMenuRights(TblRoleMenuRights tblObj);

	public List<TblRoleMenuRights> getAlltblRoleMenuRights();

	public List<TblRoleMenuRights> findtblRoleMenuRights(Object... values) throws Exception;

	public List<TblRoleMenuRights> findByCounttblRoleMenuRights(int firstResult,int maxResult,Object... values) throws Exception;

	public long gettblRoleMenuRightsCount();

	public void updateOrSavetblRoleMenuRights(List<TblRoleMenuRights> tblObj);
}
