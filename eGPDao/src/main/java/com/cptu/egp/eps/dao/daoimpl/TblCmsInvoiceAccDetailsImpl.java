package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsInvoiceAccDetailsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsInvoiceAccDetails;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsInvoiceAccDetailsImpl extends AbcAbstractClass<TblCmsInvoiceAccDetails> implements TblCmsInvoiceAccDetailsDao {

    @Override
    public void addTblCmsInvoiceAccDetails(TblCmsInvoiceAccDetails master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblCmsInvoiceAccDetails(TblCmsInvoiceAccDetails master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblCmsInvoiceAccDetails(TblCmsInvoiceAccDetails master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblCmsInvoiceAccDetails> getAllTblCmsInvoiceAccDetails() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsInvoiceAccDetails> findTblCmsInvoiceAccDetails(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsInvoiceAccDetailsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsInvoiceAccDetails> findByCountTblCmsInvoiceAccDetails(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
