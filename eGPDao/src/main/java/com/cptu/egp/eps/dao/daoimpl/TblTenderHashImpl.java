package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblTenderHashDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderHash;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblTenderHashImpl extends AbcAbstractClass<TblTenderHash> implements TblTenderHashDao {

    @Override
    public void addTblTenderHash(TblTenderHash tenderHash){
        super.addEntity(tenderHash);
    }

    @Override
    public void deleteTblTenderHash(TblTenderHash tenderHash) {
        super.deleteEntity(tenderHash);
    }

    @Override
    public void updateTblTenderHash(TblTenderHash tenderHash) {
        super.updateEntity(tenderHash);
    }

    @Override
    public List<TblTenderHash> getAllTblTenderHash() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderHash> findTblTenderHash(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderHashCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderHash> findByCountTblTenderHash(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
