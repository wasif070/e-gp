/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderIttSubClause;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderIttSubClauseDao extends GenericDao<TblTenderIttSubClause>{

    public void addTblTenderIttSubClause(TblTenderIttSubClause tblObj);

    public void deleteTblTenderIttSubClause(TblTenderIttSubClause tblObj);

    public void updateTblTenderIttSubClause(TblTenderIttSubClause tblObj);

    public List<TblTenderIttSubClause> getAllTblTenderIttSubClause();

    public List<TblTenderIttSubClause> findTblTenderIttSubClause(Object... values) throws Exception;

    public List<TblTenderIttSubClause> findByCountTblTenderIttSubClause(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderIttSubClauseCount();
}
