package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblAppPackagesDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblAppPackages;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblAppPackagesImpl extends AbcAbstractClass<TblAppPackages> implements TblAppPackagesDao {

    @Override
    public void addTblAppPackages(TblAppPackages appPackages){
        super.addEntity(appPackages);
    }

    @Override
    public void deleteTblAppPackages(TblAppPackages appPackages) {
        super.deleteEntity(appPackages);
    }

    @Override
    public void updateTblAppPackages(TblAppPackages appPackages) {
        super.updateEntity(appPackages);
    }

    @Override
    public List<TblAppPackages> getAllTblAppPackages() {
        return super.getAllEntity();
    }

    @Override
    public List<TblAppPackages> findTblAppPackages(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblAppPackagesCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblAppPackages> findByCountTblAppPackages(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
