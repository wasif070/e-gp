package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTempTendererMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblTempTendererMasterDao extends GenericDao<TblTempTendererMaster> {

    public void addTblTempTendererMaster(TblTempTendererMaster tblObj);

    public void deleteTblTempTendererMaster(TblTempTendererMaster tblObj);

    public void updateTblTempTendererMaster(TblTempTendererMaster tblObj);

    public List<TblTempTendererMaster> getAllTblTempTendererMaster();

    public List<TblTempTendererMaster> findTblTempTendererMaster(Object... values) throws Exception;

    public List<TblTempTendererMaster> findByCountTblTempTendererMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTempTendererMasterCount();
}
