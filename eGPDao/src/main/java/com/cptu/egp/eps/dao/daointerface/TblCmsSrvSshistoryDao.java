package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvSshistory;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsSrvSshistoryDao extends GenericDao<TblCmsSrvSshistory> {

    public void addTblCmsSrvSshistory(TblCmsSrvSshistory event);

    public void deleteTblCmsSrvSshistory(TblCmsSrvSshistory event);

    public void updateTblCmsSrvSshistory(TblCmsSrvSshistory event);

    public List<TblCmsSrvSshistory> getAllTblCmsSrvSshistory();

    public List<TblCmsSrvSshistory> findTblCmsSrvSshistory(Object... values) throws Exception;

    public List<TblCmsSrvSshistory> findByCountTblCmsSrvSshistory(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsSrvSshistoryCount();

    public void updateOrSaveEstCost(List<TblCmsSrvSshistory> estCost);
}
