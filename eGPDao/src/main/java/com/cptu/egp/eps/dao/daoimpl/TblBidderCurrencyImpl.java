/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblBidderCurrencyDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblBidderCurrency;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblBidderCurrencyImpl extends AbcAbstractClass<TblBidderCurrency> implements TblBidderCurrencyDao {

    @Override
    public void addTblBidderCurrency(TblBidderCurrency bidderCurrency) {
        super.addEntity(bidderCurrency);
    }

    @Override
    public void deleteTblBidderCurrency(TblBidderCurrency bidderCurrency) {
        super.deleteEntity(bidderCurrency);
    }

    @Override
    public void updateTblBidderCurrency(TblBidderCurrency bidderCurrency) {
        super.updateEntity(bidderCurrency);
    }

    @Override
    public List<TblBidderCurrency> getAllTblBidderCurrency() {
        return super.getAllEntity();
    }

    @Override
    public List<TblBidderCurrency> findTblBidderCurrency(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblBidderCurrencyCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblBidderCurrency> findByCountTblBidderCurrency(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
