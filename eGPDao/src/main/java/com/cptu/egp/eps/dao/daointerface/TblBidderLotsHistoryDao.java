package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblBidderLotsHistory;
import java.util.List;

/**
 *
 * @author Nishith
 */
public interface TblBidderLotsHistoryDao extends GenericDao<TblBidderLotsHistory> {

    public void addTblBidderLotsHistory(TblBidderLotsHistory tblObj);

    public void deleteTblBidderLotsHistory(TblBidderLotsHistory tblObj);

    public void updateTblBidderLotsHistory(TblBidderLotsHistory tblObj);

    public List<TblBidderLotsHistory> getAllTblBidderLotsHistory();

    public List<TblBidderLotsHistory> findTblBidderLotsHistory(Object... values) throws Exception;

    public List<TblBidderLotsHistory> findByCountTblBidderLotsHistory(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblBidderLotsHistoryCount();
    
    public void updateInsertAllTblBidderLotsHistory(List<TblBidderLotsHistory> list);
}
