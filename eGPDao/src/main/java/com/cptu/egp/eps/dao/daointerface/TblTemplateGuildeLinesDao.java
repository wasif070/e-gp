package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTemplateGuildeLines;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblTemplateGuildeLinesDao extends GenericDao<TblTemplateGuildeLines> {

    /**
     * Add Template Guide Line
     * @param tblObj = Object of TblTemplateGuildeLines
     */
    public void addTblTemplateGuildeLines(TblTemplateGuildeLines tblObj);

    /**
     * Delete Template Guide Line
     * @param tblObj = Object of TblTemplateGuildeLines
     */
    public void deleteTblTemplateGuildeLines(TblTemplateGuildeLines tblObj);

    /**
     * Update Template Guide Line
     * @param tblObj = Object of TblTemplateGuildeLines
     */
    public void updateTblTemplateGuildeLines(TblTemplateGuildeLines tblObj);

    /**
     * Get list of All Template Guide Lines.
     * @return = List of TblTemplateGuideLInes Objects.
     */
    public List<TblTemplateGuildeLines> getAllTblTemplateGuildeLines();

    /**
     * Get list of Specific Template Guide Lines base on search condition.
     * @param values = Array of Object having search criteria.
     * @return = List of TblTemplateGuideLInes Objects.
     * @throws Exception
     */
    public List<TblTemplateGuildeLines> findTblTemplateGuildeLines(Object... values) throws Exception;

    /**
     * Get list of Specific Template Guide Lines base on search condition range wise. like list of data starting from 5 to 10, etc.
     * @param firstResult = Start value for range
     * @param maxResult = End value for range
     * @param values = Array of Object having search criteria.
     * @return = List of TblTemplateGuideLInes Objects.
     * @throws Exception
     */
    public List<TblTemplateGuildeLines> findByCountTblTemplateGuildeLines(int firstResult,int maxResult,Object... values) throws Exception;

    /**
     * Get Template GuideLines Count.
     * @return = long value having Template GuideLines Count.
     */
    public long getTblTemplateGuildeLinesCount();
}
