package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCommitteeMembersDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCommitteeMembers;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblCommitteeMembersImpl extends AbcAbstractClass<TblCommitteeMembers> implements TblCommitteeMembersDao {

    @Override
    public void addTblCommitteeMembers(TblCommitteeMembers committeeMembers){
        super.addEntity(committeeMembers);
    }

    @Override
    public void deleteTblCommitteeMembers(TblCommitteeMembers committeeMembers) {
        super.deleteEntity(committeeMembers);
    }

    @Override
    public void updateTblCommitteeMembers(TblCommitteeMembers committeeMembers) {
        super.updateEntity(committeeMembers);
    }

    @Override
    public List<TblCommitteeMembers> getAllTblCommitteeMembers() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCommitteeMembers> findTblCommitteeMembers(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCommitteeMembersCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCommitteeMembers> findByCountTblCommitteeMembers(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
