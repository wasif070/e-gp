/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTemplateAuditTrialDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTemplateAuditTrial;
import java.util.List;

/**
 *
 * @author yanki
 */
public class TblTemplateAuditTrialImpl extends AbcAbstractClass<TblTemplateAuditTrial> implements TblTemplateAuditTrialDao{

    @Override
    public void addTblTemplateAuditTrial(TblTemplateAuditTrial templateAuditTrail) {
        super.addEntity(templateAuditTrail);
    }

    @Override
    public void deleteTblTemplateAuditTrial(TblTemplateAuditTrial templateAuditTrail) {
        super.deleteEntity(templateAuditTrail);
    }

    @Override
    public void updateTblTemplateAuditTrial(TblTemplateAuditTrial templateAuditTrail) {
        super.updateEntity(templateAuditTrail);
    }

    @Override
    public List<TblTemplateAuditTrial> getAllTblTemplateAuditTrial() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTemplateAuditTrial> findTblTemplateAuditTrial(Object... values) throws Exception {
        return super.findEntity(values);
    }

}
