/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderOpenExtDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderOpenExt;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderOpenExtImpl extends AbcAbstractClass<TblTenderOpenExt> implements TblTenderOpenExtDao{

    @Override
    public void addTblTenderOpenExt(TblTenderOpenExt tblTenderOpenExt) {
        super.addEntity(tblTenderOpenExt);
    }

    @Override
    public List<TblTenderOpenExt> findTblTenderOpenExt(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblTenderOpenExt(TblTenderOpenExt tblTenderOpenExt) {

        super.deleteEntity(tblTenderOpenExt);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblTenderOpenExt(Collection tblTenderOpenExt) {
        super.deleteAll(tblTenderOpenExt);
    }

    @Override
    public void updateTblTenderOpenExt(TblTenderOpenExt tblTenderOpenExt) {

        super.updateEntity(tblTenderOpenExt);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblTenderOpenExt> getAllTblTenderOpenExt() {
        return super.getAllEntity();
    }

    @Override
    public long getTblTenderOpenExtCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderOpenExt> findByCountTblTenderOpenExt(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
