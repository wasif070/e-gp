package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblDebarmentDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblDebarmentDocs;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblDebarmentDocsImpl extends AbcAbstractClass<TblDebarmentDocs> implements TblDebarmentDocsDao {

    @Override
    public void addTblDebarmentDocs(TblDebarmentDocs debarmentDocs){
        super.addEntity(debarmentDocs);
    }

    @Override
    public void deleteTblDebarmentDocs(TblDebarmentDocs debarmentDocs) {
        super.deleteEntity(debarmentDocs);
    }

    @Override
    public void updateTblDebarmentDocs(TblDebarmentDocs debarmentDocs) {
        super.updateEntity(debarmentDocs);
    }

    @Override
    public List<TblDebarmentDocs> getAllTblDebarmentDocs() {
        return super.getAllEntity();
    }

    @Override
    public List<TblDebarmentDocs> findTblDebarmentDocs(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblDebarmentDocsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblDebarmentDocs> findByCountTblDebarmentDocs(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
