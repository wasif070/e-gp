package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblSessionMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblSessionMasterDao extends GenericDao<TblSessionMaster> {

    public void addTblSessionMaster(TblSessionMaster tblObj);

    public void deleteTblSessionMaster(TblSessionMaster tblObj);

    public void updateTblSessionMaster(TblSessionMaster tblObj);

    public List<TblSessionMaster> getAllTblSessionMaster();

    public List<TblSessionMaster> findTblSessionMaster(Object... values) throws Exception;

    public List<TblSessionMaster> findByCountTblSessionMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblSessionMasterCount();
}
