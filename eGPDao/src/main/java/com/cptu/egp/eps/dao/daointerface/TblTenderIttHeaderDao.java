/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderIttHeader;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderIttHeaderDao extends GenericDao<TblTenderIttHeader>{

    public void addTblTenderIttHeader(TblTenderIttHeader tblObj);

    public void deleteTblTenderIttHeader(TblTenderIttHeader tblObj);

    public void updateTblTenderIttHeader(TblTenderIttHeader tblObj);

    public List<TblTenderIttHeader> getAllTblTenderIttHeader();

    public List<TblTenderIttHeader> findTblTenderIttHeader(Object... values) throws Exception;

    public List<TblTenderIttHeader> findByCountTblTenderIttHeader(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderIttHeaderCount();
}
