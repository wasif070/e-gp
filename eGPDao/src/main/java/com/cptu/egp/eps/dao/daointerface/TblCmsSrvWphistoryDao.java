package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvWphistory;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsSrvWphistoryDao extends GenericDao<TblCmsSrvWphistory> {

    public void addTblCmsSrvWphistory(TblCmsSrvWphistory event);

    public void deleteTblCmsSrvWphistory(TblCmsSrvWphistory event);

    public void updateTblCmsSrvWphistory(TblCmsSrvWphistory event);

    public List<TblCmsSrvWphistory> getAllTblCmsSrvWphistory();

    public List<TblCmsSrvWphistory> findTblCmsSrvWphistory(Object... values) throws Exception;

    public List<TblCmsSrvWphistory> findByCountTblCmsSrvWphistory(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsSrvWphistoryCount();

    public void updateOrSaveEstCost(List<TblCmsSrvWphistory> estCost);
}
