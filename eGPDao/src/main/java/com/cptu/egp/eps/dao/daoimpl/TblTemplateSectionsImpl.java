/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTemplateSectionsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTemplateSections;
import java.util.List;

/**
 *
 * @author yanki
 */
public class TblTemplateSectionsImpl extends AbcAbstractClass<TblTemplateSections> implements TblTemplateSectionsDao{

    @Override
    public void addTblTemplateSections(TblTemplateSections templateSection) {
        super.addEntity(templateSection);
    }

    @Override
    public void deleteTblTemplateSections(TblTemplateSections templateSection) {
        super.deleteEntity(templateSection);
    }

    @Override
    public void updateTblTemplateSections(TblTemplateSections templateSection) {
        super.updateEntity(templateSection);
    }

    @Override
    public List<TblTemplateSections> getAllTblTemplateSections() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTemplateSections> findTblTemplateSections(Object... values) throws Exception {
        return super.findEntity(values);
    }

}
