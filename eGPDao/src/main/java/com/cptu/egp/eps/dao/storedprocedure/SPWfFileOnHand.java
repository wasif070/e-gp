/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Ramesh
 */
public class SPWfFileOnHand extends StoredProcedure{
    private static final Logger LOGGER = Logger.getLogger(SPWfFileOnHand.class);
    public SPWfFileOnHand(BasicDataSource dataSource,String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("v_activityId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_objectId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_childId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_fromUserId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_action_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_remarks_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_levelid_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_wftrigger_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_pubDt_inDt", Types.DATE));
        this.declareParameter(new SqlParameter("v_documentId_inVc", Types.VARCHAR));
    }

    /**
     * Execute stored procedure : p_add_upd_wffile
     * Insert/Update Workflow files.
     * @param activityId
     * @param objectId
     * @param childId
     * @param fromUserId
     * @param action
     * @param remarks
     * @param levelid
     * @param wftrigger
     * @param pubDt
     * @param documentId
     * @return List of CommonMsgChk having flags indicate operation perform successfully or not.
     */
    public List<CommonMsgChk> exeProcedure(Integer activityId,Integer objectId,Integer childId,Integer fromUserId,String  action,String remarks,Integer levelid,String wftrigger,Date pubDt,String documentId ){
         Map inParams = new HashMap();
          inParams.put("v_activityId_inInt",activityId);
           inParams.put("v_objectId_inInt",objectId);
           inParams.put("v_childId_inInt",childId);
           inParams.put("v_fromUserId_inInt",fromUserId);
           inParams.put("v_action_inVc",action);
           inParams.put("v_remarks_inVc",remarks);
           inParams.put("v_levelid_inInt",levelid);
           inParams.put("v_wftrigger_inVc",wftrigger);
           inParams.put("v_pubDt_inDt",pubDt);
           inParams.put("v_documentId_inVc",documentId);
           this.compile();
          List<CommonMsgChk> details = new ArrayList<CommonMsgChk>();
           try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonMsgChk commonMsgChk = new CommonMsgChk();
                commonMsgChk.setFlag((Boolean) linkedHashMap.get("flag"));
                commonMsgChk.setMsg((String) linkedHashMap.get("Message"));
                details.add(commonMsgChk);
            }
        } catch (Exception e) {
            LOGGER.error("SPWfFileOnHand  : "+ e);
        }
        return details;
    }
    
}
