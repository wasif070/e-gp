package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblEvalNominationDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalNomination;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblEvalNominationImpl extends AbcAbstractClass<TblEvalNomination> implements TblEvalNominationDao {

    @Override
    public void addTblEvalNomination(TblEvalNomination master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblEvalNomination(TblEvalNomination master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblEvalNomination(TblEvalNomination master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblEvalNomination> getAllTblEvalNomination() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEvalNomination> findTblEvalNomination(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEvalNominationCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalNomination> findByCountTblEvalNomination(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
