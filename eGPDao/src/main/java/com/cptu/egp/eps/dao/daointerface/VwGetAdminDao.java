package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.view.VwGetAdmin;
import java.util.List;

/**
 *
 * @author taher
 */
public interface VwGetAdminDao extends GenericDao<VwGetAdmin> {

    public List<VwGetAdmin> getAllVwGetAdmin();

    public List<VwGetAdmin> findVwGetAdmin(Object... values) throws Exception;

    public List<VwGetAdmin> findByVwGetAdmin(int firstResult,int maxResult,Object... values) throws Exception;

}
