/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderResultSharingDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderResultSharing;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderResultSharingImpl extends AbcAbstractClass<TblTenderResultSharing> implements TblTenderResultSharingDao {

    @Override
    public void addTblTenderResultSharing(TblTenderResultSharing tblTenderResultSharing) {
        super.addEntity(tblTenderResultSharing);
    }

    @Override
    public void deleteTblTenderResultSharing(TblTenderResultSharing tblTenderResultSharing) {
        super.deleteEntity(tblTenderResultSharing);
    }

    @Override
    public void updateTblTenderResultSharing(TblTenderResultSharing tblTenderResultSharing) {
        super.updateEntity(tblTenderResultSharing);
    }

    @Override
    public List<TblTenderResultSharing> getAllTblTenderResultSharing() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderResultSharing> findTblTenderResultSharing(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderResultSharingCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderResultSharing> findByCountTblTenderResultSharing(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
