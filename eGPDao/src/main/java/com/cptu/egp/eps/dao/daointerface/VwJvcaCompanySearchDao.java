package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.view.VwJvcaCompanySearch;
import java.util.List;

/**
 *
 * @author taher
 */
public interface VwJvcaCompanySearchDao extends GenericDao<VwJvcaCompanySearch>{

    public List<VwJvcaCompanySearch> getAllJvcaCompanySearch();

    public List<VwJvcaCompanySearch> findJvcaCompanySearch(Object... values) throws Exception;

    public List<VwJvcaCompanySearch> findByCountJvcaCompanySearch(int firstResult,int maxResult,Object... values) throws Exception;

}
