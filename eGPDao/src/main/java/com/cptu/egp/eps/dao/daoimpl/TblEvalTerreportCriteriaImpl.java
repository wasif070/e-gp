package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblEvalTerreportCriteriaDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalTerreportCriteria;
import java.util.List;

/**
 *
 * @author rishita
 */
public class TblEvalTerreportCriteriaImpl extends AbcAbstractClass<TblEvalTerreportCriteria> implements TblEvalTerreportCriteriaDao {

    @Override
    public void addTblEvalTerreportCriteria(TblEvalTerreportCriteria master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblEvalTerreportCriteria(TblEvalTerreportCriteria master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblEvalTerreportCriteria(TblEvalTerreportCriteria master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblEvalTerreportCriteria> getAllTblEvalTerreportCriteria() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEvalTerreportCriteria> findTblEvalTerreportCriteria(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEvalTerreportCriteriaCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalTerreportCriteria> findByCountTblEvalTerreportCriteria(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
