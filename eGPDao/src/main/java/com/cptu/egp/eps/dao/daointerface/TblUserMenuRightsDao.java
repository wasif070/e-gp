package com.cptu.egp.eps.dao.daointerface;


import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblUserMenuRights;
import java.util.List;


	public interface TblUserMenuRightsDao extends GenericDao<TblUserMenuRights> {

	public void addtblUserMenuRights(TblUserMenuRights tblObj);

	public void deletetblUserMenuRights(TblUserMenuRights tblObj);

	public void updatetblUserMenuRights(TblUserMenuRights tblObj);

	public List<TblUserMenuRights> getAlltblUserMenuRights();

	public List<TblUserMenuRights> findtblUserMenuRights(Object... values) throws Exception;

	public List<TblUserMenuRights> findByCounttblUserMenuRights(int firstResult,int maxResult,Object... values) throws Exception;

	public long gettblUserMenuRightsCount();

	public void updateOrSavetblUserMenuRights(List<TblUserMenuRights> tblObj);
}