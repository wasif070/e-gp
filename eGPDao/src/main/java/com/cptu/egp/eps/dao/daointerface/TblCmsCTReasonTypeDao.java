/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsCTReasonType;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public interface TblCmsCTReasonTypeDao extends GenericDao<TblCmsCTReasonType> {

    public void addTblCmsCTReasonType(TblCmsCTReasonType tblCmsCTReasonType);

    public void deleteTblCmsCTReasonType(TblCmsCTReasonType tblCmsCTReasonType);

    public void updateTblCmsCTReasonType(TblCmsCTReasonType tblCmsCTReasonType);

    public List<TblCmsCTReasonType> getAllTblCmsCTReasonType();

    public List<TblCmsCTReasonType> findTblCmsCTReasonType(Object... values) throws Exception;

    public List<TblCmsCTReasonType> findByCountTblCmsCTReasonType(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsCTReasonTypeCount();
}
