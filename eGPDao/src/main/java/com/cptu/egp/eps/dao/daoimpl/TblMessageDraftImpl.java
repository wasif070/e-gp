package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblMessageDraftDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblMessageDraft;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblMessageDraftImpl extends AbcAbstractClass<TblMessageDraft> implements TblMessageDraftDao {

    @Override
    public void addTblMessageDraft(TblMessageDraft master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblMessageDraft(TblMessageDraft master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblMessageDraft(TblMessageDraft master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblMessageDraft> getAllTblMessageDraft() {
        return super.getAllEntity();
    }

    @Override
    public List<TblMessageDraft> findTblMessageDraft(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblMessageDraftCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblMessageDraft> findByCountTblMessageDraft(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
