/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;
import com.cptu.egp.eps.dao.generic.GenericDao;
import java.util.List;
import com.cptu.egp.eps.model.table.TblAppreviseConfiguration;
/**
 *
 * @author HP
 */
public interface TblAppreviseConfigurationDao extends GenericDao<TblAppreviseConfiguration> {
    public void addTblAppreviseConfiguration(TblAppreviseConfiguration tblObj);

    public void deleteTblAppreviseConfiguration(TblAppreviseConfiguration tblObj);

    public void updateTblAppreviseConfiguration(TblAppreviseConfiguration tblObj);

    public List<TblAppreviseConfiguration> getAllTblAppreviseConfiguration();

    public List<TblAppreviseConfiguration> findTblAppreviseConfiguration(Object... values) throws Exception;

    public List<TblAppreviseConfiguration> findByCountTblAppreviseConfiguration(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblAppreviseConfigurationCount();

    public void updateOrSaveTblAppreviseConfiguration(List<TblAppreviseConfiguration> tblObj);
}
