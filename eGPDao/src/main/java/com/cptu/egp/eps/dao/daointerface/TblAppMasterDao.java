package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblAppMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblAppMasterDao extends GenericDao<TblAppMaster> {

    public void addTblAppMaster(TblAppMaster tblObj);

    public void deleteTblAppMaster(TblAppMaster tblObj);

    public void updateTblAppMaster(TblAppMaster tblObj);

    public List<TblAppMaster> getAllTblAppMaster();

    public List<TblAppMaster> findTblAppMaster(Object... values) throws Exception;

    public List<TblAppMaster> findByCountTblAppMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblAppMasterCount();
}
