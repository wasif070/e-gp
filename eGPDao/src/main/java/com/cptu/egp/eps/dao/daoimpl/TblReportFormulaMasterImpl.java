/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblReportFormulaMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblReportFormulaMaster;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblReportFormulaMasterImpl extends AbcAbstractClass<TblReportFormulaMaster> implements TblReportFormulaMasterDao{

    @Override
    public void addTblReportFormulaMaster(TblReportFormulaMaster reportFormulaMaster) {
        super.addEntity(reportFormulaMaster);
    }

    @Override
    public List<TblReportFormulaMaster> findTblReportFormulaMaster(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblReportFormulaMaster(TblReportFormulaMaster department) {

        super.deleteEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblReportFormulaMaster(Collection reportFormulaMaster) {
        super.deleteAll(reportFormulaMaster);
    }

    @Override
    public void updateTblReportFormulaMaster(TblReportFormulaMaster department) {

        super.updateEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblReportFormulaMaster> getAllTblReportFormulaMaster() {
        return super.getAllEntity();
    }

    @Override
    public long getTblReportFormulaMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblReportFormulaMaster> findByCountTblReportFormulaMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveAllRepFormula(List<TblReportFormulaMaster> event) {
        super.updateAll(event);
    }
}
