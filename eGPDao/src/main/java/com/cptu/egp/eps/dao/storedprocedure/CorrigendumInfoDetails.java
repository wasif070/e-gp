/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

/**
 * This class is used to share the details of Corrigendum Details.
 * This class is created for web-service(Amendments)
 * @author Sreenu
 */
public class CorrigendumInfoDetails {
    private int corriDetailId;
    private int corrigendumId;
    private int tenderId;
    private String fieldName;
    private String  oldValue;
    private String  newValue;
    private String  amendmentText;

    public String getAmendmentText() {
        return amendmentText;
    }

    public void setAmendmentText(String amendmentText) {
        this.amendmentText = amendmentText;
    }

    public int getCorriDetailId() {
        return corriDetailId;
    }

    public void setCorriDetailId(int corriDetailId) {
        this.corriDetailId = corriDetailId;
    }

    public int getCorrigendumId() {
        return corrigendumId;
    }

    public void setCorrigendumId(int corrigendumId) {
        this.corrigendumId = corrigendumId;
    }

    public String getFiledName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public int getTenderId() {
        return tenderId;
    }

    public void setTenderId(int tenderId) {
        this.tenderId = tenderId;
    }

}
