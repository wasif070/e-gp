/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblPostQualificationDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblPostQualificationDocs;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblPostQualificationDocsImpl extends AbcAbstractClass<TblPostQualificationDocs> implements TblPostQualificationDocsDao{

    @Override
    public void addTblPostQualificationDocs(TblPostQualificationDocs postQualificationDocs) {
        super.addEntity(postQualificationDocs);
    }

    @Override
    public List<TblPostQualificationDocs> findTblPostQualificationDocs(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblPostQualificationDocs(TblPostQualificationDocs department) {

        super.deleteEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblPostQualificationDocs(Collection event) {
        super.deleteAll(event);
    }

    @Override
    public void updateTblPostQualificationDocs(TblPostQualificationDocs department) {

        super.updateEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblPostQualificationDocs> getAllTblPostQualificationDocs() {
        return super.getAllEntity();
    }

    @Override
    public long getTblPostQualificationDocsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblPostQualificationDocs> findByCountTblPostQualificationDocs(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
