/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.model.table.TblCommittee;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblCommitteeDao {



    public void addTblCommittee(TblCommittee tblObj);

    public void deleteTblCommittee(TblCommittee tblObj);

    public void updateTblCommittee(TblCommittee tblObj);

    public List<TblCommittee> getAllTblCommittee();

    public List<TblCommittee> findTblCommittee(Object... values) throws Exception;

    public List<TblCommittee> findByCountTblCommittee(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCommitteeCount();

}
