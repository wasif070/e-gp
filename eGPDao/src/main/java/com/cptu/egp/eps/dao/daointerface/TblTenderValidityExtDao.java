/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderValidityExt;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderValidityExtDao extends GenericDao<TblTenderValidityExt>{

    public void addTblTenderValidityExt(TblTenderValidityExt validityExt);

    public void deleteTblTenderValidityExt(TblTenderValidityExt validityExt);

    public void updateTblTenderValidityExt(TblTenderValidityExt validityExt);

    public List<TblTenderValidityExt> getAllTblTenderValidityExt();

    public List<TblTenderValidityExt> findTblTenderValidityExt(Object... values) throws Exception;

    public List<TblTenderValidityExt> findByCountTblTenderValidityExt(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderValidityExtCount();
}
