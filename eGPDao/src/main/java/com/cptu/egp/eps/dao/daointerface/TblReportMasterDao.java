package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblReportMaster;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblReportMasterDao extends GenericDao<TblReportMaster> {

    public void addTblReportMaster(TblReportMaster tblReportMaster);

    public void deleteTblReportMaster(TblReportMaster tblReportMaster);

    public void deleteAllTblReportMaster(Collection tblReportMaster);

    public void updateTblReportMaster(TblReportMaster tblReportMaster);

    public List<TblReportMaster> getAllTblReportMaster();

    public List<TblReportMaster> findTblReportMaster(Object... values) throws Exception;

    public List<TblReportMaster> findByCountTblReportMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblReportMasterCount();
}
