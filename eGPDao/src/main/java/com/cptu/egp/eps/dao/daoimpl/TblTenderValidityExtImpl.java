/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderValidityExtDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderValidityExt;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderValidityExtImpl extends AbcAbstractClass<TblTenderValidityExt> implements TblTenderValidityExtDao {

    @Override
    public void addTblTenderValidityExt(TblTenderValidityExt admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblTenderValidityExt(TblTenderValidityExt admin) {
        super.deleteEntity(admin);        
    }

    @Override
    public void updateTblTenderValidityExt(TblTenderValidityExt admin) {
        super.updateEntity(admin);        
    }

    @Override
    public List<TblTenderValidityExt> getAllTblTenderValidityExt() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderValidityExt> findTblTenderValidityExt(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderValidityExtCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderValidityExt> findByCountTblTenderValidityExt(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
