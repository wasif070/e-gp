/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderPaymentDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderPaymentDocs;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderPaymentDocsImpl extends AbcAbstractClass<TblTenderPaymentDocs> implements TblTenderPaymentDocsDao {

    @Override
    public void addTblTenderPaymentDocs(TblTenderPaymentDocs admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblTenderPaymentDocs(TblTenderPaymentDocs admin) {
        super.deleteEntity(admin);        
    }

    @Override
    public void updateTblTenderPaymentDocs(TblTenderPaymentDocs admin) {
        super.updateEntity(admin);        
    }

    @Override
    public List<TblTenderPaymentDocs> getAllTblTenderPaymentDocs() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderPaymentDocs> findTblTenderPaymentDocs(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderPaymentDocsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderPaymentDocs> findByCountTblTenderPaymentDocs(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
