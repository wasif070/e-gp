/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderSection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderSectionDao extends GenericDao<TblTenderSection>{

    public void addTblTenderSection(TblTenderSection tblObj);

    public void deleteTblTenderSection(TblTenderSection tblObj);

    public void updateTblTenderSection(TblTenderSection tblObj);

    public List<TblTenderSection> getAllTblTenderSection();

    public List<TblTenderSection> findTblTenderSection(Object... values) throws Exception;

    public List<TblTenderSection> findByCountTblTenderSection(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderSectionCount();
}
