/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblNegBidTable;
import java.util.List;
/**
 *
 * @author dipal.shah
 */
public interface TblNegBidTableDao extends GenericDao<TblNegBidTable>
{
    public void addTblNegBidTable(TblNegBidTable tblObj);

    public void deleteTblNegBidTable(TblNegBidTable tblObj);

    public void updateTblNegBidTable(TblNegBidTable tblObj);
}
