/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.Date;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Administrator
 */
public class SPAddUpdTendernotice extends StoredProcedure {
    private static final Logger LOGGER = Logger.getLogger(SPAddUpdTendernotice.class);
    public SPAddUpdTendernotice(BasicDataSource dataSource, String procName) {

        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.

        this.declareParameter(new SqlParameter("v_bidCategory_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_W1_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_W2_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_W3_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_W4_inVc", Types.VARCHAR));
        
        this.declareParameter(new SqlParameter("v_AppId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_PackageId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_TenderType_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_CreatedBy_inVc", Types.VARCHAR));

        this.declareParameter(new SqlParameter("v_ReoiRfpRefNo_Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_DocEndDate_sDt", Types.TIMESTAMP));
        this.declareParameter(new SqlParameter("v_PreBidStartDt_sDt", Types.TIMESTAMP));
        this.declareParameter(new SqlParameter("v_PreBidEndDt_sDt", Types.TIMESTAMP));
        this.declareParameter(new SqlParameter("v_SubmissionDt_sDt", Types.TIMESTAMP));
        this.declareParameter(new SqlParameter("v_OpeningDt_sDt", Types.TIMESTAMP));
        this.declareParameter(new SqlParameter("v_EligibilityCriteria_Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_TenderBrief_Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Deliverables_Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_OtherDetails_Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_ForeignFirm_Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_DocAvlMethod_Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_EvalType_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_DocFeesMethod_Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_DocFeesMode_Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_DocOfficeAdd_Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_SecurityLastDt_inDt", Types.TIMESTAMP));
        this.declareParameter(new SqlParameter("v_SecuritySubOff_inVc", Types.VARCHAR));

        this.declareParameter(new SqlParameter("v_Location_Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_DocFess_M", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_TenderSecurityAmt_M", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_CompletionTime", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Action_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Tenderid_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_Corrid_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_PhasingRefNo_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_PhasingOfService_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_PhasingLocation_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_IndStartDt_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_IndEndDt_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_ESignature_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_DigitalSignature_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_TenderLotSecId_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_ContractTypeNew_Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_tenderPubDt_Vc", Types.TIMESTAMP));
        this.declareParameter(new SqlParameter("v_PkgDocFees_Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_EstCost_inM", Types.DECIMAL));
        this.declareParameter(new SqlParameter("v_PassingMarks_Int", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_StartTime_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_PkgDocFeesUSD_Vc", Types.VARCHAR));    // For ICT Dohatec
        this.declareParameter(new SqlParameter("v_TenderSecurityAmt_M_USD", Types.VARCHAR));    // For ICT Dohatec
        this.declareParameter(new SqlParameter("v_ProcurementMethodParam_Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_BidSecurityType_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_eventTypeVc", Types.VARCHAR));
    }

    /**
     * Execute stored procedure : p_add_retendernotice
     * To perform Add Retender Notice operation.
     * @param bidCategory
     * @param W1
     * @param W2
     * @param W3
     * @param W4
     * @param appIdinInt
     * @param packageIdinInt
     * @param tenderTypeinVc
     * @param createdByinVc
     * @param reoiRfpRefNoVc
     * @param docEndDatesDt
     * @param preBidStartDtsDt
     * @param preBidEndDtsDt
     * @param submissionDtsDt
     * @param openingDtsDt
     * @param eligibilityCriteriaVc
     * @param tenderBriefVc
     * @param deliverablesVc
     * @param otherDetailsVc
     * @param foreignFirmVc
     * @param docAvlMethodVc
     * @param evalTypeVc
     * @param docFeesMethodVc
     * @param docFeesModeVc
     * @param docOfficeAddVc
     * @param securityLastDtDt
     * @param securitySubOffVc
     * @param locationVc
     * @param docFessM
     * @param tenderSecurityAmtM
     * @param tenderSecurityAmtMUSD
     * @param completionTime
     * @param startTime
     * @param actioninVc
     * @param tenderidinInt
     * @param corridinInt
     * @param phasingRefNoinVc
     * @param phasingOfServiceinVc
     * @param phasingLocationinVc
     * @param indStartDtinVc
     * @param indEndDtinVc
     * @param eSignatureinVc
     * @param digitalSignatureinVc
     * @param tenderLotSecIdinVc
     * @param contractTypeNewVc
     * @param tenderPubDtVc
     * @param pkgDocFeesVc
     * @param estCostinM
     * @param pkgDocFeesVcUSD
     * @param passingMarks
     * @param bidSecurityType
     * @param ProcurementMethod
     * @return CommonSPReturn list contains flags indicate operation success status.
     */
    public List<CommonSPReturn> executeProcedure(
            String bidCategory,
            String W1,
            String W2,
            String W3,
            String W4,
            Integer appIdinInt,
            Integer packageIdinInt,
            String tenderTypeinVc,
            String createdByinVc,
            String reoiRfpRefNoVc,
            Date docEndDatesDt,
            Date preBidStartDtsDt,
            Date preBidEndDtsDt,
            Date submissionDtsDt,
            Date openingDtsDt,
            String eligibilityCriteriaVc,
            String tenderBriefVc,
            String deliverablesVc,
            String otherDetailsVc,
            String foreignFirmVc,
            String docAvlMethodVc,
            String evalTypeVc,
            String docFeesMethodVc,
            String docFeesModeVc,
            String docOfficeAddVc,
            Date securityLastDtDt,
            String securitySubOffVc,
            String locationVc,
            String docFessM,
            String tenderSecurityAmtM,
            String tenderSecurityAmtMUSD,    // For ICT Dohatec
            String completionTime,
            String startTime,
            String actioninVc,
            Integer tenderidinInt,
            Integer corridinInt,
            String phasingRefNoinVc,
            String phasingOfServiceinVc,
            String phasingLocationinVc,
            String indStartDtinVc,
            String indEndDtinVc,
            String eSignatureinVc,
            String digitalSignatureinVc,
            String tenderLotSecIdinVc,
            String contractTypeNewVc,
            Date tenderPubDtVc,
            String pkgDocFeesVc,
            String pkgDocFeesVcUSD,    // For ICT Dohatec
            BigDecimal estCostinM,
            Integer passingMarks,
            String ProcurementMethod,
            String bidSecurityType,
            String eventType) {
        Map inParams = new HashMap();

        inParams.put("v_bidCategory_inVc", bidCategory);
        inParams.put("v_W1_inVc", W1);
        inParams.put("v_W2_inVc", W2);
        inParams.put("v_W3_inVc", W3);
        inParams.put("v_W4_inVc", W4);
        
        inParams.put("v_AppId_inInt", appIdinInt);
        inParams.put("v_PackageId_inInt", packageIdinInt);
        inParams.put("v_TenderType_inVc", tenderTypeinVc);
        inParams.put("v_CreatedBy_inVc", createdByinVc);

        inParams.put("v_ReoiRfpRefNo_Vc", reoiRfpRefNoVc);
        inParams.put("v_DocEndDate_sDt", docEndDatesDt);
        inParams.put("v_PreBidStartDt_sDt", preBidStartDtsDt);
        inParams.put("v_PreBidEndDt_sDt", preBidEndDtsDt);
        inParams.put("v_SubmissionDt_sDt", submissionDtsDt);
        inParams.put("v_OpeningDt_sDt", openingDtsDt);
        inParams.put("v_EligibilityCriteria_Vc", eligibilityCriteriaVc);
        inParams.put("v_TenderBrief_Vc", tenderBriefVc);
        inParams.put("v_Deliverables_Vc", deliverablesVc);
        inParams.put("v_OtherDetails_Vc", otherDetailsVc);
        inParams.put("v_ForeignFirm_Vc", foreignFirmVc);
        inParams.put("v_DocAvlMethod_Vc", docAvlMethodVc);
        inParams.put("v_EvalType_inVc", evalTypeVc);
        inParams.put("v_DocFeesMethod_Vc", docFeesMethodVc);
        inParams.put("v_DocFeesMode_Vc", docFeesModeVc);
        inParams.put("v_DocOfficeAdd_Vc", docOfficeAddVc);
        inParams.put("v_SecurityLastDt_inDt", securityLastDtDt);
        inParams.put("v_SecuritySubOff_inVc", securitySubOffVc);
        inParams.put("v_Action_inVc", actioninVc);
        inParams.put("v_Tenderid_inInt", tenderidinInt);

        inParams.put("v_Location_Vc", locationVc);
        inParams.put("v_DocFess_M", docFessM);
        inParams.put("v_TenderSecurityAmt_M", tenderSecurityAmtM);
        inParams.put("v_CompletionTime", completionTime);
        inParams.put("v_Corrid_inInt", corridinInt);
        inParams.put("v_PhasingRefNo_inVc", phasingRefNoinVc);
        inParams.put("v_PhasingOfService_inVc", phasingOfServiceinVc);
        inParams.put("v_PhasingLocation_inVc", phasingLocationinVc);
        inParams.put("v_IndStartDt_inVc", indStartDtinVc);
        inParams.put("v_IndEndDt_inVc", indEndDtinVc);
        inParams.put("v_ESignature_inVc", eSignatureinVc);
        inParams.put("v_DigitalSignature_inVc", digitalSignatureinVc);
        inParams.put("v_TenderLotSecId_inVc", tenderLotSecIdinVc);
        inParams.put("v_ContractTypeNew_Vc", contractTypeNewVc);
        inParams.put("v_tenderPubDt_Vc", tenderPubDtVc);
        inParams.put("v_PkgDocFees_Vc", pkgDocFeesVc);
        inParams.put("v_EstCost_inM", estCostinM);
        inParams.put("v_PassingMarks_Int", passingMarks);
        inParams.put("v_StartTime_inVc", startTime);
        inParams.put("v_PkgDocFeesUSD_Vc", pkgDocFeesVcUSD);    // For ICT Dohatec
        inParams.put("v_TenderSecurityAmt_M_USD", tenderSecurityAmtMUSD);    // For ICT Dohatec
        inParams.put("v_ProcurementMethodParam_Vc", ProcurementMethod);
        inParams.put("v_BidSecurityType_inVc", bidSecurityType);
        inParams.put("v_eventTypeVc", eventType);



        this.compile();
        List<CommonSPReturn> details = new ArrayList<CommonSPReturn>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonSPReturn commonSPReturn = new CommonSPReturn();
                commonSPReturn.setMsg((String) linkedHashMap.get("Msg"));
                commonSPReturn.setId(new BigDecimal(Integer.toString((Integer) linkedHashMap.get("Id"))));
                //commonSPReturn.setId((BigDecimal) linkedHashMap.get("Id"));
                commonSPReturn.setFlag((Boolean) linkedHashMap.get("flag"));


                details.add(commonSPReturn);
            }
        } catch (Exception e) {
            LOGGER.error("SPAddUpdTendernotice : "+ e);
            e.printStackTrace();
        }
        return details;

    }
}
