package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblModuleMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblModuleMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblModuleMasterImpl extends AbcAbstractClass<TblModuleMaster> implements TblModuleMasterDao {

    @Override
    public void addTblModuleMaster(TblModuleMaster master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblModuleMaster(TblModuleMaster master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblModuleMaster(TblModuleMaster master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblModuleMaster> getAllTblModuleMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblModuleMaster> findTblModuleMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblModuleMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblModuleMaster> findByCountTblModuleMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
