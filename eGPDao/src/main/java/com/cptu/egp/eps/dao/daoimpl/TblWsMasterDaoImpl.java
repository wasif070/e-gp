/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblWsMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblWsMaster;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Sreenu
 */
public class TblWsMasterDaoImpl extends AbcAbstractClass<TblWsMaster> implements TblWsMasterDao {

    @Override
    public void addTblWsMaster(TblWsMaster tblWsMaster) {
        super.addEntity(tblWsMaster);
    }

    @Override
    public void deleteTblWsMaster(TblWsMaster tblWsMaster) {
        super.deleteEntity(tblWsMaster);
    }

    @Override
    public void updateTblWsMaster(TblWsMaster tblWsMaster) {
        super.updateEntity(tblWsMaster);
    }

    @Override
    public List<TblWsMaster> getAllTblWsMaster() {
        super.setPersistentClass(TblWsMaster.class);
        return super.getAllEntity();
//        List<TblWsMaster> wsMastersList = new ArrayList<TblWsMaster>();
//        List<Object[]> wsMastersObjectList = new ArrayList<Object[]>();
//        String query = "SELECT "
//                + "twm.wsId, "
//                + "twm.wsName, "
//                + "twm.wsDesc, "
//                + "twm.isAuthenticate, "
//                + "twm.createdBy, "
//                + "twm.createdDate, "
//                + "twm.isActive "
//                + "FROM TblWsMaster twm";
//        wsMastersObjectList = super.createQuery(query);
//        if (wsMastersObjectList != null && !wsMastersObjectList.isEmpty()) {
//            for (int i = 0; i < wsMastersObjectList.size(); i++) {
//                Object[] tempObjectArray = wsMastersObjectList.get(i);
//                TblWsMaster tempTblWsMaster = new TblWsMaster();
//                tempTblWsMaster.setWsId((Integer) tempObjectArray[0]);
//                tempTblWsMaster.setWsName((String) tempObjectArray[1]);
//                tempTblWsMaster.setWsDesc((String) tempObjectArray[2]);
//                tempTblWsMaster.setIsAuthenticate((String) tempObjectArray[3]);
//                tempTblWsMaster.setCreatedBy((Integer) tempObjectArray[4]);
//                tempTblWsMaster.setCreatedDate((Date) tempObjectArray[5]);
//                tempTblWsMaster.setIsActive((String) tempObjectArray[6]);
//                wsMastersList.add(tempTblWsMaster);
//            }
//        }
//    return wsMastersList;
    }

    @Override
    public List<TblWsMaster> findTblWsMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public List<TblWsMaster> findByCountTblWsMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblWsMasterCount() {
        return super.getEntityCount();
    }

    /***
     * This method gets the required objects from the database.
     * The object name used in the condition is <b>wsMaster</b>.
     * Just pass the query condition to this method.
     * <b>Need to append the WHERE keyword to the above condition</b>
     * @param String whereCondition
     * @return List<TblWsMaster>
     */
    @Override
    public List<TblWsMaster> getRequiredTblWsMasterList(String whereCondition){
        List<TblWsMaster> wsMastersList = new ArrayList<TblWsMaster>();
        List<Object[]> wsMastersObjectList = new ArrayList<Object[]>();
        StringBuffer queryBuffer = new StringBuffer();
        String queryString = "SELECT "
                + "wsMaster.wsId, "
                + "wsMaster.wsName, "
                + "wsMaster.wsDesc, "
                + "wsMaster.isAuthenticate, "
                + "wsMaster.createdBy, "
                + "wsMaster.createdDate, "
                + "wsMaster.isActive "
                + "FROM TblWsMaster wsMaster ";
        queryBuffer.append(queryString);
        queryBuffer.append(whereCondition);
        wsMastersObjectList = super.createQuery(queryBuffer.toString());
        if (wsMastersObjectList != null && !wsMastersObjectList.isEmpty()) {
            for (int i = 0; i < wsMastersObjectList.size(); i++) {
                Object[] tempObjectArray = wsMastersObjectList.get(i);
                TblWsMaster tempTblWsMaster = new TblWsMaster();
                tempTblWsMaster.setWsId((Integer) tempObjectArray[0]);
                tempTblWsMaster.setWsName((String) tempObjectArray[1]);
                tempTblWsMaster.setWsDesc((String) tempObjectArray[2]);
                tempTblWsMaster.setIsAuthenticate((String) tempObjectArray[3]);
                tempTblWsMaster.setCreatedBy((Integer) tempObjectArray[4]);
                tempTblWsMaster.setCreatedDate((Date) tempObjectArray[5]);
                tempTblWsMaster.setIsActive((String) tempObjectArray[6]);
                wsMastersList.add(tempTblWsMaster);
            }
        }
        return wsMastersList;
    }
}
