package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblNegotiationForms;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblNegotiationFormsDao extends GenericDao<TblNegotiationForms> {

    public void addTblNegotiationForms(TblNegotiationForms tblObj);

    public void deleteTblNegotiationForms(TblNegotiationForms tblObj);

    public void updateTblNegotiationForms(TblNegotiationForms tblObj);

    public List<TblNegotiationForms> getAllTblNegotiationForms();

    public List<TblNegotiationForms> findTblNegotiationForms(Object... values) throws Exception;

    public List<TblNegotiationForms> findByCountTblNegotiationForms(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblNegotiationFormsCount();
}
