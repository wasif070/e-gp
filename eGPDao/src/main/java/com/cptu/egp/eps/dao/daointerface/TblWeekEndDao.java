package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblWeekEnd;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblWeekEndDao extends GenericDao<TblWeekEnd> {

    public void addTblWeekEnd(TblWeekEnd tblObj);

    public void deleteTblWeekEnd(TblWeekEnd tblObj);

    public void updateTblWeekEnd(TblWeekEnd tblObj);

    public List<TblWeekEnd> getAllTblWeekEnd();

    public List<TblWeekEnd> findTblWeekEnd(Object... values) throws Exception;

    public List<TblWeekEnd> findByCountTblWeekEnd(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblWeekEndCount();
}
