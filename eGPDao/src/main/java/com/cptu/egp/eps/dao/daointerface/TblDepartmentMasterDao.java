/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import java.util.List;

/**
 *
 * @author rishita
 */
public interface TblDepartmentMasterDao extends GenericDao<TblDepartmentMaster> {

    public void addDepartmentMaster(TblDepartmentMaster departmentMaster);

    public List<TblDepartmentMaster> findTblDepartmentMaster(Object... values) throws Exception;

    public void deleteTblDepartmentMaster(TblDepartmentMaster Department);

    public void updateTblDepartmentMaster(TblDepartmentMaster Department);

    public List<TblDepartmentMaster> getAllTblDepartmentMaster();

    public List<TblDepartmentMaster> findByCountTblDepartmentMaster(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblDepartmentMasterCount();
}
