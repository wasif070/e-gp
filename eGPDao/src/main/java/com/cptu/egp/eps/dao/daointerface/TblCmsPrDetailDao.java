package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsPrDetail;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsPrDetailDao extends GenericDao<TblCmsPrDetail> {

    public void addTblCmsPrDetail(TblCmsPrDetail event);

    public void deleteTblCmsPrDetail(TblCmsPrDetail event);

    public void updateTblCmsPrDetail(TblCmsPrDetail event);

    public List<TblCmsPrDetail> getAllTblCmsPrDetail();

    public List<TblCmsPrDetail> findTblCmsPrDetail(Object... values) throws Exception;

    public List<TblCmsPrDetail> findByCountTblCmsPrDetail(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsPrDetailCount();

    public void updateOrSaveEstCost(List<TblCmsPrDetail> estCost);
}
