package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderBidEncrypt;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderBidEncryptDao extends GenericDao<TblTenderBidEncrypt> {

    public void addTblTenderBidEncrypt(TblTenderBidEncrypt tblObj);

    public void deleteTblTenderBidEncrypt(TblTenderBidEncrypt tblObj);

    public void deleteAllTblTenderBidEncrypt(Collection tblObj);

    public void updateTblTenderBidEncrypt(TblTenderBidEncrypt tblObj);

    public List<TblTenderBidEncrypt> getAllTblTenderBidEncrypt();

    public List<TblTenderBidEncrypt> findTblTenderBidEncrypt(Object... values) throws Exception;

    public List<TblTenderBidEncrypt> findByCountTblTenderBidEncrypt(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderBidEncryptCount();
}
