package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblPreTenderAuditTrail;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblPreTenderAuditTrailDao extends GenericDao<TblPreTenderAuditTrail> {

    public void addTblPreTenderAuditTrail(TblPreTenderAuditTrail tblPreTenderAuditTrail);

    public void deleteTblPreTenderAuditTrail(TblPreTenderAuditTrail tblPreTenderAuditTrail);

    public void deleteAllTblPreTenderAuditTrail(Collection tblPreTenderAuditTrail);

    public void updateTblPreTenderAuditTrail(TblPreTenderAuditTrail tblPreTenderAuditTrail);

    public List<TblPreTenderAuditTrail> getAllTblPreTenderAuditTrail();

    public List<TblPreTenderAuditTrail> findTblPreTenderAuditTrail(Object... values) throws Exception;

    public List<TblPreTenderAuditTrail> findByCountTblPreTenderAuditTrail(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblPreTenderAuditTrailCount();
}
