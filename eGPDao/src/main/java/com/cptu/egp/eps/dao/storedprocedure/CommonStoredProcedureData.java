/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

/**
 *
 * @author dipal.shah
 */
public class CommonStoredProcedureData
{
     private String fieldName1;
    private String fieldName2;
    private String fieldName3;
    private String fieldName4;
    private String fieldName5;
    private String fieldName6;
    private String fieldName7;
    private String fieldName8;
    private String fieldName9;
    private String fieldName10;
    private String fieldName11;
    private String fieldName12;
    private String fieldName13;
    private String fieldName14;
    private String fieldName15;
    private String fieldName16;
    private String fieldName17;
    private String fieldName18;
    private String fieldName19;
    private String fieldName20;

    public String getFieldName1()
    {
        return fieldName1;
    }

    public void setFieldName1(String fieldName1)
    {
        this.fieldName1 = fieldName1;
    }

    public String getFieldName10()
    {
        return fieldName10;
    }

    public void setFieldName10(String fieldName10)
    {
        this.fieldName10 = fieldName10;
    }

    public String getFieldName11()
    {
        return fieldName11;
    }

    public void setFieldName11(String fieldName11)
    {
        this.fieldName11 = fieldName11;
    }

    public String getFieldName12()
    {
        return fieldName12;
    }

    public void setFieldName12(String fieldName12)
    {
        this.fieldName12 = fieldName12;
    }

    public String getFieldName13()
    {
        return fieldName13;
    }

    public void setFieldName13(String fieldName13)
    {
        this.fieldName13 = fieldName13;
    }

    public String getFieldName14()
    {
        return fieldName14;
    }

    public void setFieldName14(String fieldName14)
    {
        this.fieldName14 = fieldName14;
    }

    public String getFieldName15()
    {
        return fieldName15;
    }

    public void setFieldName15(String fieldName15)
    {
        this.fieldName15 = fieldName15;
    }

    public String getFieldName16()
    {
        return fieldName16;
    }

    public void setFieldName16(String fieldName16)
    {
        this.fieldName16 = fieldName16;
    }

    public String getFieldName17()
    {
        return fieldName17;
    }

    public void setFieldName17(String fieldName17)
    {
        this.fieldName17 = fieldName17;
    }

    public String getFieldName18()
    {
        return fieldName18;
    }

    public void setFieldName18(String fieldName18)
    {
        this.fieldName18 = fieldName18;
    }

    public String getFieldName19()
    {
        return fieldName19;
    }

    public void setFieldName19(String fieldName19)
    {
        this.fieldName19 = fieldName19;
    }

    public String getFieldName2()
    {
        return fieldName2;
    }

    public void setFieldName2(String fieldName2)
    {
        this.fieldName2 = fieldName2;
    }

    public String getFieldName20()
    {
        return fieldName20;
    }

    public void setFieldName20(String fieldName20)
    {
        this.fieldName20 = fieldName20;
    }

    public String getFieldName3()
    {
        return fieldName3;
    }

    public void setFieldName3(String fieldName3)
    {
        this.fieldName3 = fieldName3;
    }

    public String getFieldName4()
    {
        return fieldName4;
    }

    public void setFieldName4(String fieldName4)
    {
        this.fieldName4 = fieldName4;
    }

    public String getFieldName5()
    {
        return fieldName5;
    }

    public void setFieldName5(String fieldName5)
    {
        this.fieldName5 = fieldName5;
    }

    public String getFieldName6()
    {
        return fieldName6;
    }

    public void setFieldName6(String fieldName6)
    {
        this.fieldName6 = fieldName6;
    }

    public String getFieldName7()
    {
        return fieldName7;
    }

    public void setFieldName7(String fieldName7)
    {
        this.fieldName7 = fieldName7;
    }

    public String getFieldName8()
    {
        return fieldName8;
    }

    public void setFieldName8(String fieldName8)
    {
        this.fieldName8 = fieldName8;
    }

    public String getFieldName9()
    {
        return fieldName9;
    }

    public void setFieldName9(String fieldName9)
    {
        this.fieldName9 = fieldName9;
    }
    
    
    
}
