/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblWorkFlowRuleEngineHistDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblWorkFlowRuleEngineHist;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public class TblWorkFlowRuleEngineHistImpl extends AbcAbstractClass<TblWorkFlowRuleEngineHist> implements TblWorkFlowRuleEngineHistDao {

    @Override
    public void addTblWorkflowRuleEngineHist(TblWorkFlowRuleEngineHist workFlowRuleEngineHist) {
        super.addEntity(workFlowRuleEngineHist);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblWorkflowRuleEngineHist(TblWorkFlowRuleEngineHist workFlowRuleEngineHist) {
        super.deleteEntity(workFlowRuleEngineHist);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblWorkflowRuleEngineHist(TblWorkFlowRuleEngineHist workFlowRuleEngineHist) {
        super.updateEntity(workFlowRuleEngineHist);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblWorkFlowRuleEngineHist> getAllTblWorkflowRuleEngineHist() {
        return super.getAllEntity();
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblWorkFlowRuleEngineHist> findTblWorkflowRuleEngineHist(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblWorkFlowRuleEngineHist> findByCountTblWorkflowRuleEngineHist(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public long getTblWorkflowRuleEngineHistCount() {
        return super.getEntityCount();
        //throw new UnsupportedOperationException("Not supported yet.");
    }

   
}
