/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderResultSharing;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderResultSharingDao extends GenericDao<TblTenderResultSharing>{

    public void addTblTenderResultSharing(TblTenderResultSharing tblObj);

    public void deleteTblTenderResultSharing(TblTenderResultSharing tblObj);

    public void updateTblTenderResultSharing(TblTenderResultSharing tblObj);

    public List<TblTenderResultSharing> getAllTblTenderResultSharing();

    public List<TblTenderResultSharing> findTblTenderResultSharing(Object... values) throws Exception;

    public List<TblTenderResultSharing> findByCountTblTenderResultSharing(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderResultSharingCount();
}
