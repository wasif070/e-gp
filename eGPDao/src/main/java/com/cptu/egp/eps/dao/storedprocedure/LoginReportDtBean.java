/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.util.Date;

/**
 *
 * @author rishita
 */
public class LoginReportDtBean {

    private Long rowNumber;
    private Integer userId;
    private String emailId;
    private Integer userTyperId;
    private Date sessionStartDt;
    private Date sessionEndDt;
    private String ipAddress;
    private Integer totalPages;
    private Integer totalRecords;

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Date getSessionEndDt() {
        return sessionEndDt;
    }

    public void setSessionEndDt(Date sessionEndDt) {
        this.sessionEndDt = sessionEndDt;
    }

    public Date getSessionStartDt() {
        return sessionStartDt;
    }

    public void setSessionStartDt(Date sessionStartDt) {
        this.sessionStartDt = sessionStartDt;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getUserTyperId() {
        return userTyperId;
    }

    public void setUserTyperId(Integer userTyperId) {
        this.userTyperId = userTyperId;
    }

    public Long getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(Long rowNumber) {
        this.rowNumber = rowNumber;
    }
}
