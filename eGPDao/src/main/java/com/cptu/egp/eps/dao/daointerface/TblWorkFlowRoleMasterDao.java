/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblWorkFlowRoleMaster;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public interface TblWorkFlowRoleMasterDao extends GenericDao<TblWorkFlowRoleMaster> {

    public void addTblWorkFlowRoleMaster(TblWorkFlowRoleMaster admin);

    public void deleteTblWorkFlowRoleMaster(TblWorkFlowRoleMaster admin);

    public void updateTblWorkFlowRoleMaster(TblWorkFlowRoleMaster admin);

    public List<TblWorkFlowRoleMaster> getAllTblWorkFlowRoleMaster();

    public List<TblWorkFlowRoleMaster> findTblWorkFlowRoleMaster(Object... values) throws Exception;

    public List<TblWorkFlowRoleMaster> findByCountTblWorkFlowRoleMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblWorkFlowRoleMasterCount();
}
