package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this late, choose Tools | lates
 * and open the late in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblMessageSent;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblMessageSentDao extends GenericDao<TblMessageSent> {

    public void addTblMessageSent(TblMessageSent tblObj);

    public void deleteTblMessageSent(TblMessageSent tblObj);

    public void updateTblMessageSent(TblMessageSent tblObj);

    public List<TblMessageSent> getAllTblMessageSent();

    public List<TblMessageSent> findTblMessageSent(Object... values) throws Exception;

    public List<TblMessageSent> findByCountTblMessageSent(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblMessageSentCount();
}
