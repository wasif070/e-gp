/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCascadeLinkMaster;
import java.util.List;

/**
 *
 * @author tejasree.goriparthi
 */
public interface TblCascadeMasterDao extends GenericDao<TblCascadeLinkMaster> {

    public List<TblCascadeLinkMaster> getAllTblCascadeMaster();

    public List<Object> findTblCascadeMaster(String query) throws Exception;
}
