/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTemplateColumns;
import java.util.List;

/**
 *
 * @author yanki
 */
public interface TblTemplateColumnsDao extends GenericDao<TblTemplateColumns>{

    public void addTblTemplateColumns(TblTemplateColumns templateColumns);

    public void deleteTblTemplateColumns(TblTemplateColumns templateColumns);

    public void updateTblTemplateColumns(TblTemplateColumns templateColumns);

    public List<TblTemplateColumns> getAllTblTemplateColumns();

    public List<TblTemplateColumns> findTblTemplateColumns(Object... values) throws Exception;

}
