/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblNegQueryDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblNegQuery;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblNegQueryImpl extends AbcAbstractClass<TblNegQuery> implements TblNegQueryDao{

    @Override
    public void addTblNegQuery(TblNegQuery negQuery) {
        super.addEntity(negQuery);
    }

    @Override
    public List<TblNegQuery> findTblNegQuery(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblNegQuery(TblNegQuery negQuery) {

        super.deleteEntity(negQuery);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblNegQuery(Collection negQuery) {
        super.deleteAll(negQuery);
    }

    @Override
    public void updateTblNegQuery(TblNegQuery negQuery) {

        super.updateEntity(negQuery);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblNegQuery> getAllTblNegQuery() {
        return super.getAllEntity();
    }

    @Override
    public long getTblNegQueryCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblNegQuery> findByCountTblNegQuery(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
