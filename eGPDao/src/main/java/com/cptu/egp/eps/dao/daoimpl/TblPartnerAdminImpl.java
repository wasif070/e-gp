package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblPartnerAdminDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblPartnerAdmin;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblPartnerAdminImpl extends AbcAbstractClass<TblPartnerAdmin> implements TblPartnerAdminDao {

    @Override
    public void addTblPartnerAdmin(TblPartnerAdmin admin){
        super.addEntity(admin);
    }

    @Override
    public void deleteTblPartnerAdmin(TblPartnerAdmin admin) {
        super.deleteEntity(admin);
    }

    @Override
    public void updateTblPartnerAdmin(TblPartnerAdmin admin) {
        super.updateEntity(admin);
    }

    @Override
    public List<TblPartnerAdmin> getAllTblPartnerAdmin() {
        return super.getAllEntity();
    }

    @Override
    public List<TblPartnerAdmin> findTblPartnerAdmin(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblPartnerAdminCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblPartnerAdmin> findByCountTblPartnerAdmin(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
