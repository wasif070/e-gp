/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderMandatoryDocDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderMandatoryDoc;
import java.util.List;



/**
 *
 * @author Administrator
 */
public class TblTenderMandatoryDocImpl extends AbcAbstractClass<TblTenderMandatoryDoc> implements TblTenderMandatoryDocDao{


    @Override
    public void addTblTenderMandatoryDoc(TblTenderMandatoryDoc tenderMandatoryDoc){
        super.addEntity(tenderMandatoryDoc);
    }

    @Override
    public void deleteTblTenderMandatoryDoc(TblTenderMandatoryDoc tenderMandatoryDoc) {
        super.deleteEntity(tenderMandatoryDoc);
    }

    @Override
    public void updateTblTenderMandatoryDoc(TblTenderMandatoryDoc tenderMandatoryDoc) {
        super.updateEntity(tenderMandatoryDoc);
    }

    @Override
    public List<TblTenderMandatoryDoc> getAllTblTenderMandatoryDoc() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderMandatoryDoc> findTblTenderMandatoryDoc(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderMandatoryDocCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderMandatoryDoc> findByCountTblTenderMandatoryDoc(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrInsertAll(List<TblTenderMandatoryDoc> list) {
       super.updateAll(list);
    }

}
