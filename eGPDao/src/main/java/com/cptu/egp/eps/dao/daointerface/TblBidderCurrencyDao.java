/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblBidderCurrency;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblBidderCurrencyDao extends GenericDao<TblBidderCurrency>{

    public void addTblBidderCurrency(TblBidderCurrency tblObj);

    public void deleteTblBidderCurrency(TblBidderCurrency tblObj);

    public void updateTblBidderCurrency(TblBidderCurrency tblObj);

    public List<TblBidderCurrency> getAllTblBidderCurrency();

    public List<TblBidderCurrency> findTblBidderCurrency(Object... values) throws Exception;

    public List<TblBidderCurrency> findByCountTblBidderCurrency(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblBidderCurrencyCount();
}
