package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsRoitems;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsRoitemsDao extends GenericDao<TblCmsRoitems> {

    public void addTblCmsRoitems(TblCmsRoitems event);

    public void deleteTblCmsRoitems(TblCmsRoitems event);

    public void updateTblCmsRoitems(TblCmsRoitems event);

    public List<TblCmsRoitems> getAllTblCmsRoitems();

    public List<TblCmsRoitems> findTblCmsRoitems(Object... values) throws Exception;

    public List<TblCmsRoitems> findByCountTblCmsRoitems(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsRoitemsCount();

    public void updateOrSaveEstCost(List<TblCmsRoitems> estCost);
}
