/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblPreTenderReplyDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblPreTenderReplyDocs;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblPreTenderReplyDocsImpl extends AbcAbstractClass<TblPreTenderReplyDocs> implements TblPreTenderReplyDocsDao {

    @Override
    public void addTblPreTenderReplyDocs(TblPreTenderReplyDocs preTenderReplyDocs) {
        super.addEntity(preTenderReplyDocs);
    }

    @Override
    public void deleteTblPreTenderReplyDocs(TblPreTenderReplyDocs preTenderReplyDocs) {
        super.deleteEntity(preTenderReplyDocs);
    }

    @Override
    public void updateTblPreTenderReplyDocs(TblPreTenderReplyDocs preTenderReplyDocs) {
        super.updateEntity(preTenderReplyDocs);
    }

    @Override
    public List<TblPreTenderReplyDocs> getAllTblPreTenderReplyDocs() {
        return super.getAllEntity();
    }

    @Override
    public List<TblPreTenderReplyDocs> findTblPreTenderReplyDocs(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblPreTenderReplyDocsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblPreTenderReplyDocs> findByCountTblPreTenderReplyDocs(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
