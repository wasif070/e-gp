/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTemplateMandatoryDocDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTemplateMandatoryDoc;
import java.util.List;



/**
 *
 * @author Administrator
 */
public class TblTemplateMandatoryDocImpl extends AbcAbstractClass<TblTemplateMandatoryDoc> implements TblTemplateMandatoryDocDao{


    @Override
    public void addTblTemplateMandatoryDoc(TblTemplateMandatoryDoc master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblTemplateMandatoryDoc(TblTemplateMandatoryDoc master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblTemplateMandatoryDoc(TblTemplateMandatoryDoc master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblTemplateMandatoryDoc> getAllTblTemplateMandatoryDoc() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTemplateMandatoryDoc> findTblTemplateMandatoryDoc(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTemplateMandatoryDocCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTemplateMandatoryDoc> findByCountTblTemplateMandatoryDoc(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrInsertAll(List<TblTemplateMandatoryDoc> list) {
       super.updateAll(list);
    }

}
