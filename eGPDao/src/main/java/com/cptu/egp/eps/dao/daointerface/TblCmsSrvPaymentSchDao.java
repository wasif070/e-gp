package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsSrvPaymentSchDao extends GenericDao<TblCmsSrvPaymentSch> {

    public void addTblCmsSrvPaymentSch(TblCmsSrvPaymentSch event);

    public void deleteTblCmsSrvPaymentSch(TblCmsSrvPaymentSch event);

    public void updateTblCmsSrvPaymentSch(TblCmsSrvPaymentSch event);

    public List<TblCmsSrvPaymentSch> getAllTblCmsSrvPaymentSch();

    public List<TblCmsSrvPaymentSch> findTblCmsSrvPaymentSch(Object... values) throws Exception;

    public List<TblCmsSrvPaymentSch> findByCountTblCmsSrvPaymentSch(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsSrvPaymentSchCount();

    public void updateOrSaveEstCost(List<TblCmsSrvPaymentSch> estCost);
}
