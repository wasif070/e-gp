/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.object.StoredProcedure;

/**
 * This class is helps for execution the procedure <b>p_get_DataUpToCMSForRHD</b>.
 * @author Sudhir Chavhan
 */
public class SPSharedCMSDetailForRHD extends StoredProcedure {

    private static final Logger LOGGER = Logger.getLogger(SPSharedCMSDetailForRHD.class);
    /***
     * Construction for <b>SPSharedCMSDetailForRHD</b>
     * we inject this properties at DaoContext
     * @param dataSource
     * @param procName
     */
    public SPSharedCMSDetailForRHD(BasicDataSource dataSource, String procName) {
    this.setDataSource(dataSource);
    this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
    LOGGER.debug("Proc Name: " + procName);
    }
    /***
     * This method executes the given procedure and returns the list of objects
     * Execute Procedure : p_get_DataUpToCMSForRHD
     * @return List<RHDIntegrationCMSDetails>
     */
    public List<RHDIntegrationCMSDetails> executeProcedure() {
        this.compile();
        List<RHDIntegrationCMSDetails> RHDIntegrationDetailsList =
                new ArrayList<RHDIntegrationCMSDetails>();
        Map inParams = new HashMap();
        try {
            ArrayList<LinkedHashMap<String, Object>> list =
                    (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            //System.out.println("SPSharedCMSDetailForRHD : 1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                RHDIntegrationCMSDetails RHDIntegrationDetails =
                        new RHDIntegrationCMSDetails();

                RHDIntegrationDetails.setPE_Id((Integer) linkedHashMap.get("officeId"));
                //System.out.println("SPSharedCMSDetailForRHD : 4");
                RHDIntegrationDetails.setPE_Name((String) linkedHashMap.get("PE_Name"));
                RHDIntegrationDetails.setPE_Code((String) linkedHashMap.get("PE_Code"));
                RHDIntegrationDetails.setNoA_Name((String) linkedHashMap.get("NoA_Name"));
                RHDIntegrationDetails.setTender_Id((Integer) linkedHashMap.get("Tender_Id"));
                RHDIntegrationDetails.setProc_Nature((String) linkedHashMap.get("Proc_Nature"));
                RHDIntegrationDetails.setProc_Method((String) linkedHashMap.get("Proc_Method"));
                RHDIntegrationDetails.setProc_Type((String) linkedHashMap.get("Proc_Type"));
                RHDIntegrationDetails.setBudget_Type((String) linkedHashMap.get("Budget_Type"));
                RHDIntegrationDetails.setSource_Of_Funds((String) linkedHashMap.get("Source_Of_Funds"));
                RHDIntegrationDetails.setProject_Code((String) linkedHashMap.get("Project_Code"));
                RHDIntegrationDetails.setProject_Name((String) linkedHashMap.get("Project_Name"));
                RHDIntegrationDetails.setTender_RefNo((String) linkedHashMap.get("Tender_RefNo"));
                RHDIntegrationDetails.setTender_InvitationFor((String) linkedHashMap.get("Tender_InvitationFor"));
                RHDIntegrationDetails.setTender_PkgNo((String) linkedHashMap.get("Tender_PkgNo"));
                RHDIntegrationDetails.setTender_PkgDesc((String) linkedHashMap.get("Tender_PkgDesc"));
                RHDIntegrationDetails.setTender_Category((String) linkedHashMap.get("Tender_Category"));
                RHDIntegrationDetails.setTender_PubDt((Date) linkedHashMap.get("Tender_PubDt"));
                //System.out.println("SPSharedCMSDetailForRHD : 5");
                RHDIntegrationDetails.setTender_DocLstSelD((Date) linkedHashMap.get("Tender_DocLstSelD"));
                RHDIntegrationDetails.setTender_PreMeetingStrtDt((Date) linkedHashMap.get("Tender_PreMeetingStrtDt"));
                RHDIntegrationDetails.setTender_PreMeetingEndDt((Date) linkedHashMap.get("Tender_PreMeetingEndDt"));
                RHDIntegrationDetails.setTender_ClosingDt((Date) linkedHashMap.get("Tender_ClosingDt"));
                RHDIntegrationDetails.setTender_OpeningDt((Date) linkedHashMap.get("Tender_OpeningDt"));
                //System.out.println("SPSharedCMSDetailForRHD : 51");
                RHDIntegrationDetails.setTender_SecLstSubDt((Date) linkedHashMap.get("Tender_SecLstSubDt"));
                RHDIntegrationDetails.setTender_BriefDesc((String) linkedHashMap.get("Tender_BriefDesc"));
                RHDIntegrationDetails.setTender_EvaluationType((String) linkedHashMap.get("Tender_EvaluationType"));
                RHDIntegrationDetails.setTender_DocAvailable((String) linkedHashMap.get("Tender_DocAvailable"));
                RHDIntegrationDetails.setTender_DocFees((String) linkedHashMap.get("Tender_DocFees"));
                //System.out.println("SPSharedCMSDetailForRHD : 6");
                RHDIntegrationDetails.setTender_DocPrice((BigDecimal) linkedHashMap.get("Tender_DocPrice"));
                RHDIntegrationDetails.setTender_SecAmt((BigDecimal) linkedHashMap.get("Tender_SecAmt"));
                RHDIntegrationDetails.setTender_SecPayMode((String) linkedHashMap.get("Tender_SecPayMode"));
                RHDIntegrationDetails.setTender_SecValidUpToDt((Date) linkedHashMap.get("Tender_SecValidUpToDt"));
                //System.out.println("SPSharedCMSDetailForRHD : 61");
                RHDIntegrationDetails.setTender_ValidUpToDt((String) linkedHashMap.get("Tender_ValidUpToDt"));
                RHDIntegrationDetails.setPer_SecValidUpToDt((String) linkedHashMap.get("Per_SecValidUpToDt"));
                RHDIntegrationDetails.setPer_SecBankName((String) linkedHashMap.get("Per_SecBankName"));
                RHDIntegrationDetails.setPer_SecBankBranchName((String) linkedHashMap.get("Per_SecBankBranchName"));
                //System.out.println("SPSharedCMSDetailForRHD : 7");
                RHDIntegrationDetails.setPer_PayAmount((BigDecimal) linkedHashMap.get("Per_PayAmount"));
                RHDIntegrationDetails.setPer_PayPaidDt((Date) linkedHashMap.get("Per_PayPaidDt"));
                RHDIntegrationDetails.setPer_SecPayMode((String) linkedHashMap.get("Per_SecPayMode"));
                RHDIntegrationDetails.setPer_PayInstrumentNo((String) linkedHashMap.get("Per_PayInstrumentNo"));
                RHDIntegrationDetails.setPer_PayIssuingBank((String) linkedHashMap.get("Per_PayIssuingBank"));
                RHDIntegrationDetails.setPer_PayIssuingBranch((String) linkedHashMap.get("Per_PayIssuingBranch"));
                RHDIntegrationDetails.setPer_PayIssuanceDt((Date) linkedHashMap.get("Per_PayIssuanceDt"));
                RHDIntegrationDetails.setContract_No((String) linkedHashMap.get("Contract_No"));
                RHDIntegrationDetails.setContract_LotNo((String) linkedHashMap.get("Contract_LotNo"));
                //System.out.println("SPSharedCMSDetailForRHD : 8");
                RHDIntegrationDetails.setContract_DescOfLot((String) linkedHashMap.get("Contract_DescOfLot"));
                RHDIntegrationDetails.setContract_StartDt((String) linkedHashMap.get("Contract_StartDt"));
                //System.out.println("SPSharedCMSDetailForRHD : 81");
                RHDIntegrationDetails.setContract_CompletionDt((String) linkedHashMap.get("Contract_CompletionDt"));
                //System.out.println("SPSharedCMSDetailForRHD : 82");
                RHDIntegrationDetails.setContract_Value((BigDecimal) linkedHashMap.get("Contract_Value"));
                RHDIntegrationDetails.setNoA_Dt((Date) linkedHashMap.get("NoA_Dt"));
                RHDIntegrationDetails.setNoA_PEName((String) linkedHashMap.get("NoA_PEName"));

                RHDIntegrationDetailsList.add(RHDIntegrationDetails);
                //System.out.println("SPSharedCMSDetailForRHD : 5"+RHDIntegrationDetailsList);
            }
        } catch (Exception e) {
            LOGGER.error("SPSharedCMSDetailForRHD : " + e.toString());
            System.out.println("Exception SPSharedCMSDetailForRHD : " + e.toString());
        }
        return RHDIntegrationDetailsList;
    }

}
