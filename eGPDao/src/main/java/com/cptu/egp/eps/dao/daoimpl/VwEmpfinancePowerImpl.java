package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.VwEmpfinancePowerDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.view.VwEmpfinancePower;
import java.util.List;

/**
 *
 * @author taher
 */
public class VwEmpfinancePowerImpl extends AbcAbstractClass<VwEmpfinancePower> implements VwEmpfinancePowerDao {

    @Override
    public List<VwEmpfinancePower> getAllEmpfinancePower() {
        return super.getAllEntity();
    }

    @Override
    public List<VwEmpfinancePower> findEmpfinancePower(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public List<VwEmpfinancePower> findByCountEmpfinancePower(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

}
