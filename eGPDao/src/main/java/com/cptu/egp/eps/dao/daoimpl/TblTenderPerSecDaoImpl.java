package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblTenderPerSecDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderPerfSecurity;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblTenderPerSecDaoImpl extends AbcAbstractClass<TblTenderPerfSecurity> implements TblTenderPerSecDao {

    @Override
    public void addTblTenderPerfSecurity(TblTenderPerfSecurity tblTenderPerfSecurity){
        super.addEntity(tblTenderPerfSecurity);
    }

    @Override
    public void deleteTblTenderPerfSecurity(TblTenderPerfSecurity tblTenderPerfSecurity) {
        super.deleteEntity(tblTenderPerfSecurity);
    }

    @Override
    public void updateTblTenderPerfSecurity(TblTenderPerfSecurity tblTenderPerfSecurity) {
        super.updateEntity(tblTenderPerfSecurity);
    }

    @Override
    public List<TblTenderPerfSecurity> getAllTblTenderPerfSecurity() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderPerfSecurity> findTblTenderPerfSecurity(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderPerfSecurityCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderPerfSecurity> findByCountTblTenderPerfSecurity(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSavePerSec(List<TblTenderPerfSecurity> tblTenderPerfSecurity) {
        super.updateAll(tblTenderPerfSecurity);
    }
}
