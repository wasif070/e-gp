/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblConfigNoaDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblConfigNoa;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblConfigNoaImpl extends AbcAbstractClass<TblConfigNoa> implements TblConfigNoaDao{

    @Override
    public void addTblConfigNoa(TblConfigNoa configNoa) {
        super.addEntity(configNoa);
    }

    @Override
    public List<TblConfigNoa> findTblConfigNoa(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblConfigNoa(TblConfigNoa department) {

        super.deleteEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblConfigNoa(Collection event) {
        super.deleteAll(event);
    }

    @Override
    public void updateTblConfigNoa(TblConfigNoa department) {

        super.updateEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblConfigNoa> getAllTblConfigNoa() {
        return super.getAllEntity();
    }

    @Override
    public long getTblConfigNoaCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblConfigNoa> findByCountTblConfigNoa(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
