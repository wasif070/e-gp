/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblWorkFlowDonorConcDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblWorkFlowDonorConc;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public class TblWorkFlowDonorConcImpl extends AbcAbstractClass<TblWorkFlowDonorConc> implements TblWorkFlowDonorConcDao {

    @Override
    public void addTblWorkFlowDonorConc(TblWorkFlowDonorConc workflowDonorConc) {

        super.addEntity(workflowDonorConc);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblWorkFlowDonorConc(TblWorkFlowDonorConc workflowDonorConc) {

        super.deleteEntity(workflowDonorConc);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblWorkFlowDonorConc(TblWorkFlowDonorConc workflowDonorConc) {

        super.updateEntity(workflowDonorConc);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblWorkFlowDonorConc> getAllTblWorkFlowDonorConc() {
        return super.getAllEntity();
    }

    @Override
    public List<TblWorkFlowDonorConc> findTblWorkFlowDonorConc(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblWorkFlowDonorConcCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblWorkFlowDonorConc> findByCountTblWorkFlowDonorConc(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
