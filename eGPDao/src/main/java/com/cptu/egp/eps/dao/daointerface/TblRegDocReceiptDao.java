package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblRegDocReceipt;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblRegDocReceiptDao extends GenericDao<TblRegDocReceipt> {

    public void addTblRegDocReceipt(TblRegDocReceipt tblObj);

    public void deleteTblRegDocReceipt(TblRegDocReceipt tblObj);

    public void updateTblRegDocReceipt(TblRegDocReceipt tblObj);

    public List<TblRegDocReceipt> getAllTblRegDocReceipt();

    public List<TblRegDocReceipt> findTblRegDocReceipt(Object... values) throws Exception;

    public List<TblRegDocReceipt> findByCountTblRegDocReceipt(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblRegDocReceiptCount();
}
