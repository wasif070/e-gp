/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblPromisProcess;
import java.util.List;

/**
 *
 * @author Sudhir Chavhan
 */
public interface TblPromisProcessDao extends GenericDao<TblPromisProcess>{

   public void addTblPromisProcess(TblPromisProcess admin);

   public List<TblPromisProcess> getAllTblPromisProcess();

/* public void deleteTblPromisProcess(TblPromisProcess admin);

   public void updateTblPromisProcess(TblPromisProcess admin);

   public List<TblPromisProcess> findTblPromisProcess(Object... values) throws Exception;

   public List<TblPromisProcess> findByCountTblPromisProcess(int firstResult,int maxResult,Object... values) throws Exception;

   public long getTblPromisProcessCount();   */
   
}
