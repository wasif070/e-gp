/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderDetailsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderDetailsImpl extends AbcAbstractClass<TblTenderDetails> implements TblTenderDetailsDao {

    @Override
    public void addTblTenderDetails(TblTenderDetails tenderDetails) {
        super.addEntity(tenderDetails);
    }

    @Override
    public void deleteTblTenderDetails(TblTenderDetails tenderDetails) {
        super.deleteEntity(tenderDetails);
    }

    @Override
    public void updateTblTenderDetails(TblTenderDetails tenderDetails) {
        super.updateEntity(tenderDetails);
    }

    @Override
    public List<TblTenderDetails> getAllTblTenderDetails() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderDetails> findTblTenderDetails(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderDetailsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderDetails> findByCountTblTenderDetails(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
