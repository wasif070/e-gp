package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblContractSign;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblContractSignDao extends GenericDao<TblContractSign> {

    public void addTblContractSign(TblContractSign tblObj);

    public void deleteTblContractSign(TblContractSign tblObj);

    public void updateTblContractSign(TblContractSign tblObj);

    public List<TblContractSign> getAllTblContractSign();

    public List<TblContractSign> findTblContractSign(Object... values) throws Exception;

    public List<TblContractSign> findByCountTblContractSign(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblContractSignCount();
}
