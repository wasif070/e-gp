package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.dao.storedprocedure.SearchComplaintPaymentBean;
import com.cptu.egp.eps.model.table.*;
import java.util.List;
import java.util.Date;
public interface TblComplaintMasterDao extends GenericDao<TblComplaintMaster> {

	public void addComplaint(TblComplaintMaster complaint);

   // public void deleteTblComplaintMaster(TblComplaintMaster complaint) throws DataAccessException;

   // public void updateTblComplaintMaster(TblComplaintMaster complaint);

   public List<TblComplaintMaster> getAllComplaints();

    public List<TblComplaintMaster> findComplaints(Object... values) throws Exception;

   // public List<TblComplaintMaster> findByCountComplaints(int firstResult,int maxResult,Object... values) throws Exception;

   // public long getTblComplaintMasterCount();
	public List<Object[]> getGovtUserByTenderId(int tenderId);
	
	// public List<TblComplaintType> findComplaintType(Object... values) throws Exception;
	public TblComplaintMaster getComplaintDetails(int complaintId); 
	
	public List<TblComplaintMaster>   getComplaintById(Object... values) throws Exception;
	
	 public void updateMaster(TblComplaintMaster master);
	 public List<Object[]>  getComplaintList(int tenderid,int complaintlevelId)throws Exception;
	 public List<Object[]>  getSearchList(int complid,int tenderid,int refid) throws Exception;
	 public List<TblComplaintMaster> getPendingComplaintsByReviewPanel(Object... values)throws Exception;
	 public List<Object> getGovtUserDetails(int officeId, String role);
	 public List<TblComplaintMaster> searchComplaintDetails(int tenderId, int complaintId, TblTenderDetails details,int govUserId, String role, String status, boolean complaintFeePaid,int panelId)throws Exception;	
 	 public List<Object>  getGovUserSecretaries(int[] officeIds, String role) throws Exception;
 	 public List<Object[]>  getGovUserDetails(int userId) throws Exception;
 	 public List<Object[]> getTenderDetails(int complaintId)throws Exception;
 	 public List<SearchComplaintPaymentBean> searchComplaintPayments(String emailId, String verificationStatus, String paymentFromDate, String paymentToDate, int createdBy,String paymentFor, boolean verifyFlag, int tenderId, String tenderer)throws Exception;
 	 public List<Object[]> getComplaintTenderer(int tenderId, int userId) throws Exception;
 	 public List<Object> getEmailId(int govUserId) throws Exception;
 	 public List<TblPartnerAdmin> getTblPartnerAdmin(int userId, boolean getAllFlag) throws Exception;
 	 public void updateComplaintsForPayment()throws Exception;

    public List<TblComplaintMaster> searchPendingComplaintDetails(String query) throws Exception;

    public List<TblComplaintMaster> searchProcessedComplaintDetails(String query) throws Exception;

    public List<TblComplaintMaster> searchPendingComplaintDetailsForDGCPTU(int i) throws Exception;

    public List<TblComplaintMaster> searchPendingComplaintDetailsRP(int i) throws Exception;

    public List<TblComplaintMaster> searchProcessedComplaintDetailsRP(int i) throws Exception;

    public List<Object> getEmailIdFromRpId(int rpId) throws Exception;

    public List<SearchComplaintPaymentBean> searchComplaintPendingPayments(String verificationStatus, String paymentFor) throws Exception;
}
