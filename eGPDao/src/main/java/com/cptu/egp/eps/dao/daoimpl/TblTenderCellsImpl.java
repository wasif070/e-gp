/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderCellsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderCells;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderCellsImpl extends AbcAbstractClass<TblTenderCells> implements TblTenderCellsDao {

    @Override
    public void addTblTenderCells(TblTenderCells tenderCells) {
        super.addEntity(tenderCells);
    }

    @Override
    public void deleteTblTenderCells(TblTenderCells tenderCells) {
        super.deleteEntity(tenderCells);
    }

    @Override
    public void updateTblTenderCells(TblTenderCells tenderCells) {
        super.updateEntity(tenderCells);
    }

    @Override
    public List<TblTenderCells> getAllTblTenderCells() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderCells> findTblTenderCells(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderCellsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderCells> findByCountTblTenderCells(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
