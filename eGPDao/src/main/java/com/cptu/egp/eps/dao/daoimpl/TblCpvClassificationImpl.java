package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCpvClassificationDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCpvClassification;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCpvClassificationImpl extends AbcAbstractClass<TblCpvClassification> implements TblCpvClassificationDao {

    @Override
    public void addCpvClassification(TblCpvClassification cpvClassification){
        super.addEntity(cpvClassification);
    }

    @Override
    public void deleteCpvClassification(TblCpvClassification cpvClassification) {
        super.deleteEntity(cpvClassification);
    }

    @Override
    public void updateCpvClassification(TblCpvClassification cpvClassification) {
        super.updateEntity(cpvClassification);
    }

    @Override
    public List<TblCpvClassification> getAllCpvClassification() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCpvClassification> findCpvClassification(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getCpvClassificationCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCpvClassification> findByCountCpvClassification(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
