/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderPaymentDocs;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderPaymentDocsDao extends GenericDao<TblTenderPaymentDocs>{

    public void addTblTenderPaymentDocs(TblTenderPaymentDocs tblObj);

    public void deleteTblTenderPaymentDocs(TblTenderPaymentDocs tblObj);

    public void updateTblTenderPaymentDocs(TblTenderPaymentDocs tblObj);

    public List<TblTenderPaymentDocs> getAllTblTenderPaymentDocs();

    public List<TblTenderPaymentDocs> findTblTenderPaymentDocs(Object... values) throws Exception;

    public List<TblTenderPaymentDocs> findByCountTblTenderPaymentDocs(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderPaymentDocsCount();
}
