/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderValAcceptanceDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderValAcceptance;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderValAcceptanceImpl extends AbcAbstractClass<TblTenderValAcceptance> implements TblTenderValAcceptanceDao {

    @Override
    public void addTblTenderValAcceptance(TblTenderValAcceptance tenderValAcceptance) {
        super.addEntity(tenderValAcceptance);
    }

    @Override
    public void deleteTblTenderValAcceptance(TblTenderValAcceptance tenderValAcceptance) {
        super.deleteEntity(tenderValAcceptance);
    }

    @Override
    public void updateTblTenderValAcceptance(TblTenderValAcceptance tenderValAcceptance) {
        super.updateEntity(tenderValAcceptance);
    }

    @Override
    public List<TblTenderValAcceptance> getAllTblTenderValAcceptance() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderValAcceptance> findTblTenderValAcceptance(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderValAcceptanceCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderValAcceptance> findByCountTblTenderValAcceptance(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
