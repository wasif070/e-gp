/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderBidDetailDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderBidDetail;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderBidDetailImpl extends AbcAbstractClass<TblTenderBidDetail> implements TblTenderBidDetailDao {

    @Override
    public void addTblTenderBidDetail(TblTenderBidDetail tenderBidDetail) {
        super.addEntity(tenderBidDetail);
    }

    @Override
    public void deleteTblTenderBidDetail(TblTenderBidDetail tenderBidDetail) {
        super.deleteEntity(tenderBidDetail);
    }

    @Override
    public void updateTblTenderBidDetail(TblTenderBidDetail tenderBidDetail) {
        super.updateEntity(tenderBidDetail);
    }

    @Override
    public List<TblTenderBidDetail> getAllTblTenderBidDetail() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderBidDetail> findTblTenderBidDetail(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderBidDetailCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderBidDetail> findByCountTblTenderBidDetail(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
