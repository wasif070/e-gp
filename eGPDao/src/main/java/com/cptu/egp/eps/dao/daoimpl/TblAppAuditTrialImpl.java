package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblAppAuditTrialDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblAppAuditTrial;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblAppAuditTrialImpl extends AbcAbstractClass<TblAppAuditTrial> implements TblAppAuditTrialDao {

    @Override
    public void addTblAppAuditTrial(TblAppAuditTrial appAuditTrialc){
        super.addEntity(appAuditTrialc);
    }

    @Override
    public void deleteTblAppAuditTrial(TblAppAuditTrial appAuditTrialc) {
        super.deleteEntity(appAuditTrialc);
    }

    @Override
    public void updateTblAppAuditTrial(TblAppAuditTrial appAuditTrialc) {
        super.updateEntity(appAuditTrialc);
    }

    @Override
    public List<TblAppAuditTrial> getAllTblAppAuditTrial() {
        return super.getAllEntity();
    }

    @Override
    public List<TblAppAuditTrial> findTblAppAuditTrial(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblAppAuditTrialCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblAppAuditTrial> findByCountTblAppAuditTrial(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
