package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblProgrammeMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblProgrammeMasterDao extends GenericDao<TblProgrammeMaster> {

    public void addTblProgrammeMaster(TblProgrammeMaster tblObj);

    public void deleteTblProgrammeMaster(TblProgrammeMaster tblObj);

    public void updateTblProgrammeMaster(TblProgrammeMaster tblObj);

    public List<TblProgrammeMaster> getAllTblProgrammeMaster();

    public List<TblProgrammeMaster> findTblProgrammeMaster(Object... values) throws Exception;

    public List<TblProgrammeMaster> findByCountTblProgrammeMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblProgrammeMasterCount();
}
