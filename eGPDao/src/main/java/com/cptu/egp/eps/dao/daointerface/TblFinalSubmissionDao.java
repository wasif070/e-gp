package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblFinalSubmission;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblFinalSubmissionDao extends GenericDao<TblFinalSubmission> {

    public void addTblFinalSubmission(TblFinalSubmission tblObj);

    public void deleteTblFinalSubmission(TblFinalSubmission tblObj);

    public void updateTblFinalSubmission(TblFinalSubmission tblObj);

    public List<TblFinalSubmission> getAllTblFinalSubmission();

    public List<TblFinalSubmission> findTblFinalSubmission(Object... values) throws Exception;

    public List<TblFinalSubmission> findByCountTblFinalSubmission(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblFinalSubmissionCount();
}
