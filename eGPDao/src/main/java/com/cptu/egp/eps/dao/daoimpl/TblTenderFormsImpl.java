/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderFormsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderForms;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderFormsImpl extends AbcAbstractClass<TblTenderForms> implements TblTenderFormsDao {

    @Override
    public void addTblTenderForms(TblTenderForms admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblTenderForms(TblTenderForms admin) {
        super.deleteEntity(admin);        
    }

    @Override
    public void updateTblTenderForms(TblTenderForms admin) {
        super.updateEntity(admin);        
    }

    @Override
    public List<TblTenderForms> getAllTblTenderForms() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderForms> findTblTenderForms(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderFormsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderForms> findByCountTblTenderForms(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
