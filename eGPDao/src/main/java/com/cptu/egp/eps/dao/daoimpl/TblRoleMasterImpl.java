package com.cptu.egp.eps.dao.daoimpl;


import com.cptu.egp.eps.dao.daointerface.TblRoleMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblRoleMaster;
import java.util.List;


public class TblRoleMasterImpl extends AbcAbstractClass<TblRoleMaster> implements TblRoleMasterDao {

	@Override 
	public void addTblRoleMaster(TblRoleMaster master){
		super.addEntity(master);
	}
	@Override 
	public void deleteTblRoleMaster(TblRoleMaster master) {
		super.deleteEntity(master);
	}
	@Override 
	public void updateTblRoleMaster(TblRoleMaster master) {
		super.updateEntity(master);
	}
	@Override 
	public List<TblRoleMaster> getAllTblRoleMaster() {
		return super.getAllEntity();
	}
	@Override 
	public List<TblRoleMaster> findTblRoleMaster(Object... values) throws Exception {
		return super.findEntity(values);
	}
	@Override 
	public long getTblRoleMasterCount() {
		return super.getEntityCount();
	}
	@Override 
	public List<TblRoleMaster> findByCountTblRoleMaster(int firstResult, int maxResult, Object... values) throws Exception {
		return super.findByCountEntity(firstResult, maxResult, values);
	}
	@Override 
	public void updateOrSaveTblRoleMaster(List<TblRoleMaster> tblObj) {
		super.updateAll(tblObj);
	}
}