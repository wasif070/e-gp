/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Administrator
 */
public class SPPageNavigate extends StoredProcedure{
    private static final Logger LOGGER = Logger.getLogger(SPPageNavigate.class);
    SPPageNavigate(BasicDataSource dataSource, String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("@v_Userid_inInt", Types.INTEGER));
    }

    /**
     * Execute stored procedure : p_get_pagenavigate_info
     * Use for Select information for page navigation
     * @param userId
     * @return String value 
     */
    public String executeProcedure(int userId) {
        Map inParams = new HashMap();
        inParams.put("@v_Userid_inInt", userId);
        this.compile();
        String pageName="";
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {                
                pageName = ((String) linkedHashMap.get("PageString"));
            }
        } catch (Exception e) {
           LOGGER.error("SPPageNavigate  : "+ e);
        }
        return pageName;
    }

}
