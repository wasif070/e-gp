package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsInvAccHistoryDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsInvAccHistory;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsInvAccHistoryImpl extends AbcAbstractClass<TblCmsInvAccHistory> implements TblCmsInvAccHistoryDao {

    @Override
    public void addTblCmsInvAccHistory(TblCmsInvAccHistory cmsInvAccHistory){
        super.addEntity(cmsInvAccHistory);
    }

    @Override
    public void deleteTblCmsInvAccHistory(TblCmsInvAccHistory cmsInvAccHistory) {
        super.deleteEntity(cmsInvAccHistory);
    }

    @Override
    public void updateTblCmsInvAccHistory(TblCmsInvAccHistory cmsInvAccHistory) {
        super.updateEntity(cmsInvAccHistory);
    }

    @Override
    public List<TblCmsInvAccHistory> getAllTblCmsInvAccHistory() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsInvAccHistory> findTblCmsInvAccHistory(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsInvAccHistoryCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsInvAccHistory> findByCountTblCmsInvAccHistory(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
