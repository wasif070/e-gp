package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblAuditTrailMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblAuditTrailMasterDao extends GenericDao<TblAuditTrailMaster> {

    public void addTblAuditTrailMaster(TblAuditTrailMaster auditTrailMaster);

    public void deleteTblAuditTrailMaster(TblAuditTrailMaster auditTrailMaster);

    public void updateTblAuditTrailMaster(TblAuditTrailMaster auditTrailMaster);

    public List<TblAuditTrailMaster> getAllTblAuditTrailMaster();

    public List<TblAuditTrailMaster> findTblAuditTrailMaster(Object... values) throws Exception;

    public List<TblAuditTrailMaster> findByCountTblAuditTrailMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblAuditTrailMasterCount();
}
