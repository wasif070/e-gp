package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalReportDocs;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblEvalReportDocsDao extends GenericDao<TblEvalReportDocs> {

    public void addTblEvalReportDocs(TblEvalReportDocs tblEvalReportDocs);

    public void deleteTblEvalReportDocs(TblEvalReportDocs tblEvalReportDocs);

    public void deleteAllTblEvalReportDocs(Collection tblEvalReportDocs);

    public void updateTblEvalReportDocs(TblEvalReportDocs tblEvalReportDocs);

    public List<TblEvalReportDocs> getAllTblEvalReportDocs();

    public List<TblEvalReportDocs> findTblEvalReportDocs(Object... values) throws Exception;

    public List<TblEvalReportDocs> findByCountTblEvalReportDocs(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalReportDocsCount();
}
