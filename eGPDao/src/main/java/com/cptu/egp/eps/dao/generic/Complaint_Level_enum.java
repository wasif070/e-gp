package com.cptu.egp.eps.dao.generic;

/**
 *
 * @author Radha
 */
public enum Complaint_Level_enum {

    PE(1), HOPE(2), SECRETARY(3), REVIEW_PANEL(4);

    private int code;

    private Complaint_Level_enum(int c) {
      code = c;
    }

    public int getLevel() {
      return code;
    }
}
