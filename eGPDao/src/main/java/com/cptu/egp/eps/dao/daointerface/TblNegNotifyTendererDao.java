package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblNegNotifyTenderer;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblNegNotifyTendererDao extends GenericDao<TblNegNotifyTenderer> {

    public void addTblNegNotifyTenderer(TblNegNotifyTenderer tblObj);

    public void deleteTblNegNotifyTenderer(TblNegNotifyTenderer tblObj);

    public void deleteAllTblNegNotifyTenderer(Collection tblObj);

    public void updateTblNegNotifyTenderer(TblNegNotifyTenderer tblObj);

    public List<TblNegNotifyTenderer> getAllTblNegNotifyTenderer();

    public List<TblNegNotifyTenderer> findTblNegNotifyTenderer(Object... values) throws Exception;

    public List<TblNegNotifyTenderer> findByCountTblNegNotifyTenderer(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblNegNotifyTendererCount();
}
