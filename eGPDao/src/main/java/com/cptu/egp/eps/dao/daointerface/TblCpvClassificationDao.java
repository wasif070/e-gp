package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCpvClassification;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCpvClassificationDao extends GenericDao<TblCpvClassification> {

    public void addCpvClassification(TblCpvClassification cpv);

    public void deleteCpvClassification(TblCpvClassification cpv);

    public void updateCpvClassification(TblCpvClassification cpv);

    public List<TblCpvClassification> getAllCpvClassification();

    public List<TblCpvClassification> findCpvClassification(Object... values) throws Exception;

    public List<TblCpvClassification> findByCountCpvClassification(int firstResult,int maxResult,Object... values) throws Exception;

    public long getCpvClassificationCount();
}
