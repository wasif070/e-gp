/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderMegaHashDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderMegaHash;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderMegaHashImpl extends AbcAbstractClass<TblTenderMegaHash> implements TblTenderMegaHashDao{

    @Override
    public void addTblTenderMegaHash(TblTenderMegaHash tenderMegaHash) {
        super.addEntity(tenderMegaHash);
    }

    @Override
    public List<TblTenderMegaHash> findTblTenderMegaHash(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblTenderMegaHash(TblTenderMegaHash department) {

        super.deleteEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblTenderMegaHash(Collection event) {
        super.deleteAll(event);
    }

    @Override
    public void updateTblTenderMegaHash(TblTenderMegaHash department) {

        super.updateEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblTenderMegaHash> getAllTblTenderMegaHash() {
        return super.getAllEntity();
    }

    @Override
    public long getTblTenderMegaHashCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderMegaHash> findByCountTblTenderMegaHash(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
