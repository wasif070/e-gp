package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblDebarredBiddingPermissionDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblDebarredBiddingPermission;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblDebarredBiddingPermissionImpl extends AbcAbstractClass<TblDebarredBiddingPermission> implements TblDebarredBiddingPermissionDao {

    @Override
    public void addTblDebarredBiddingPermission(TblDebarredBiddingPermission master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblDebarredBiddingPermission(TblDebarredBiddingPermission master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblDebarredBiddingPermission(TblDebarredBiddingPermission master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblDebarredBiddingPermission> getAllTblDebarredBiddingPermission() {
        return super.getAllEntity();
    }

    @Override
    public List<TblDebarredBiddingPermission> findTblDebarredBiddingPermission(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblDebarredBiddingPermissionCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblDebarredBiddingPermission> findByCountTblDebarredBiddingPermission(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
