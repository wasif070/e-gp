/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Sanjay Prajapati
 * This class used for setting Input / Output parameters
 */
public class SPTenderFormDtl extends StoredProcedure{
    private static final Logger LOGGER = Logger.getLogger(SPTenderFormDtl.class);
    /* Pass Input Paras to proc*/
    public SPTenderFormDtl(BasicDataSource dataSource, String procName )
    {
        this.setDataSource(dataSource);
        this.setSql(procName);
        this.declareParameter(new SqlParameter("@v_tableId", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_formId", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_columnId", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_userId", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_bidId", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_tenderId", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_funcNameinVc", Types.VARCHAR));
    }

    /**
     * Execute stored procedure : p_getformdata
     * Get Tender common form data.
     * @param tableId
     * @param formId
     * @param columnId
     * @param userId
     * @param bidId
     * @param tenderId
     * @param funName
     * @return Tender form data as list of CommonFormData object
     */
    public List <CommonFormData> executeProcedure (Integer tableId,Integer formId,Integer columnId,Integer userId,Integer bidId,Integer tenderId,String funName)
    {
        Map inParams = new HashMap();
        inParams.put("@v_tableId", tableId);
        inParams.put("@v_formId", formId);
        inParams.put("@v_columnId", columnId);
        inParams.put("@v_userId", userId);
        inParams.put("@v_bidId", bidId);
        inParams.put("@v_tenderId", tenderId);
        inParams.put("@v_funcNameinVc", funName);

        this.compile();

        List<CommonFormData> formDataList = new ArrayList<CommonFormData>();
        try
        {
             ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
             for (LinkedHashMap<String, Object> linkedHashMap: list )
             {
                CommonFormData formData = new CommonFormData();
                formData.setFormName((String)linkedHashMap.get("formName"));
                formData.setFormHeader((String)linkedHashMap.get("formHeader"));
                formData.setFormFooter((String)linkedHashMap.get("formFooter"));
                formData.setIsMultipleFormFeeling((String)linkedHashMap.get("isMultipleFormFeeling"));
                formData.setIsEncryption((String)linkedHashMap.get("isEncryption"));
                formData.setIsPriceBid((String)linkedHashMap.get("isPriceBid"));
                formData.setIsMandatory((String)linkedHashMap.get("isMandatory"));

                formData.setTableId((Integer)linkedHashMap.get("tenderTableId"));
                formData.setTableName((String)linkedHashMap.get("tableName"));
                formData.setTableHeader((String)linkedHashMap.get("tableHeader"));
                formData.setTableFooter((String)linkedHashMap.get("tablefooter"));
                formData.setNoOfCols((Short)linkedHashMap.get("noOfCols"));
                formData.setNoOfRows((Short)linkedHashMap.get("noOfRows"));
                formData.setIsMultipleFilling((String)linkedHashMap.get("isMultipleFilling"));

                formData.setColumnId((Short)linkedHashMap.get("columnId"));
                formData.setColumnHeader((String)linkedHashMap.get("columnHeader"));
                formData.setDataType((Short)linkedHashMap.get("dataType"));
                formData.setFillBy((Short)linkedHashMap.get("filledBy"));
                formData.setColumnType((String)linkedHashMap.get("columnType"));
                formData.setShowOrHide((String)linkedHashMap.get("showorhide"));

                formData.setRowId((Integer)linkedHashMap.get("rowId"));
                formData.setCellDataType((Short)linkedHashMap.get("cellDatatype"));
                formData.setCellValue((String)linkedHashMap.get("cellValue"));
                formData.setCellId((Integer)linkedHashMap.get("cellId"));
                
                formData.setFormula((String)linkedHashMap.get("formula"));
                formData.setIsGrandTotal((String)linkedHashMap.get("isGrandTotal"));

                formData.setCount((Integer)linkedHashMap.get("cnt"));

                formData.setBidTableId((Integer)linkedHashMap.get("bidTableId"));

                formDataList.add(formData);
             }
        }
        catch(Exception e)
        {
         LOGGER.error("SPTenderFormDtl  : "+ e);
        }
    return formDataList;
    }
}
