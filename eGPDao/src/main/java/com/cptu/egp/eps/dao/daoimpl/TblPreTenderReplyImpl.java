/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblPreTenderReplyDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblPreTenderReply;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblPreTenderReplyImpl extends AbcAbstractClass<TblPreTenderReply> implements TblPreTenderReplyDao {

    @Override
    public void addTblPreTenderReply(TblPreTenderReply preTenderReply) {
        super.addEntity(preTenderReply);
    }

    @Override
    public void deleteTblPreTenderReply(TblPreTenderReply preTenderReply) {
        super.deleteEntity(preTenderReply);
    }

    @Override
    public void updateTblPreTenderReply(TblPreTenderReply preTenderReply) {
        super.updateEntity(preTenderReply);
    }

    @Override
    public List<TblPreTenderReply> getAllTblPreTenderReply() {
        return super.getAllEntity();
    }

    @Override
    public List<TblPreTenderReply> findTblPreTenderReply(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblPreTenderReplyCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblPreTenderReply> findByCountTblPreTenderReply(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
