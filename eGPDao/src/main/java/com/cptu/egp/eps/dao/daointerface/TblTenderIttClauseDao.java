/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderIttClause;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderIttClauseDao extends GenericDao<TblTenderIttClause>{

    public void addTblTenderIttClause(TblTenderIttClause tblObj);

    public void deleteTblTenderIttClause(TblTenderIttClause tblObj);

    public void updateTblTenderIttClause(TblTenderIttClause tblObj);

    public List<TblTenderIttClause> getAllTblTenderIttClause();

    public List<TblTenderIttClause> findTblTenderIttClause(Object... values) throws Exception;

    public List<TblTenderIttClause> findByCountTblTenderIttClause(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderIttClauseCount();
}
