package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvStaffSchDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvStaffSch;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsSrvStaffSchImpl extends AbcAbstractClass<TblCmsSrvStaffSch> implements TblCmsSrvStaffSchDao {

    @Override
    public void addTblCmsSrvStaffSch(TblCmsSrvStaffSch cmsWpDetail){
        super.addEntity(cmsWpDetail);
    }

    @Override
    public void deleteTblCmsSrvStaffSch(TblCmsSrvStaffSch cmsWpDetail) {
        super.deleteEntity(cmsWpDetail);
    }

    @Override
    public void updateTblCmsSrvStaffSch(TblCmsSrvStaffSch cmsWpDetail) {
        super.updateEntity(cmsWpDetail);
    }

    @Override
    public List<TblCmsSrvStaffSch> getAllTblCmsSrvStaffSch() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvStaffSch> findTblCmsSrvStaffSch(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvStaffSchCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvStaffSch> findByCountTblCmsSrvStaffSch(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsSrvStaffSch> estCost) {
       super.updateAll(estCost);
    }
}
