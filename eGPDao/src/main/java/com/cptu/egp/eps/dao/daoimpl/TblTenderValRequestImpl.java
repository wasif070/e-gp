/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderValRequestDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderValRequest;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderValRequestImpl extends AbcAbstractClass<TblTenderValRequest> implements TblTenderValRequestDao{

    @Override
    public void addTblTenderValRequest(TblTenderValRequest tenderValRequest) {
        super.addEntity(tenderValRequest);
    }

    @Override
    public List<TblTenderValRequest> findTblTenderValRequest(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblTenderValRequest(TblTenderValRequest department) {

        super.deleteEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblTenderValRequest(Collection event) {
        super.deleteAll(event);
    }

    @Override
    public void updateTblTenderValRequest(TblTenderValRequest department) {

        super.updateEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblTenderValRequest> getAllTblTenderValRequest() {
        return super.getAllEntity();
    }

    @Override
    public long getTblTenderValRequestCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderValRequest> findByCountTblTenderValRequest(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
