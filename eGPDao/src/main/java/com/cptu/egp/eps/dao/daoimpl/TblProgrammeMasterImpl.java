package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblProgrammeMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblProgrammeMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblProgrammeMasterImpl extends AbcAbstractClass<TblProgrammeMaster> implements TblProgrammeMasterDao {

    @Override
    public void addTblProgrammeMaster(TblProgrammeMaster master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblProgrammeMaster(TblProgrammeMaster master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblProgrammeMaster(TblProgrammeMaster master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblProgrammeMaster> getAllTblProgrammeMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblProgrammeMaster> findTblProgrammeMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblProgrammeMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblProgrammeMaster> findByCountTblProgrammeMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
