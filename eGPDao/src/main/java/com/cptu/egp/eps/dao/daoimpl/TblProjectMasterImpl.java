/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblProjectMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblProjectMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblProjectMasterImpl extends AbcAbstractClass<TblProjectMaster> implements TblProjectMasterDao{

    @Override
    public void addTblProjectMaster(TblProjectMaster tblProjectMaster) {
        super.addEntity(tblProjectMaster);
    }

    @Override
    public List<TblProjectMaster> findTblProjectMaster(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblProjectMaster(TblProjectMaster tblProjectMaster) {

        super.deleteEntity(tblProjectMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblProjectMaster(TblProjectMaster tblProjectMaster) {

        super.updateEntity(tblProjectMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblProjectMaster> getAllTblProjectMaster() {
        return super.getAllEntity();
    }

    @Override
    public long getTblProjectMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblProjectMaster> findByCountTblProjectMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
