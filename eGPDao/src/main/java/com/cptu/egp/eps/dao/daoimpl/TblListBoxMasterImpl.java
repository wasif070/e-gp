/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblListBoxMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblListBoxMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblListBoxMasterImpl extends AbcAbstractClass<TblListBoxMaster> implements TblListBoxMasterDao{

    @Override
    public void addTblListBoxMaster(TblListBoxMaster tblListBoxMaster) {
        super.addEntity(tblListBoxMaster);
    }

    @Override
    public List<TblListBoxMaster> findTblListBoxMaster(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblListBoxMaster(TblListBoxMaster tblListBoxMaster) {

        super.deleteEntity(tblListBoxMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblListBoxMaster(TblListBoxMaster tblListBoxMaster) {

        super.updateEntity(tblListBoxMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblListBoxMaster> getAllTblListBoxMaster() {
        return super.getAllEntity();
    }

    @Override
    public long getTblListBoxMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblListBoxMaster> findByCountTblListBoxMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
