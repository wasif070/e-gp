/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblCorrigendumMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCorrigendumMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblCorrigendumMasterImpl extends AbcAbstractClass<TblCorrigendumMaster> implements TblCorrigendumMasterDao{

    @Override
    public void addTblCorrigendumMaster(TblCorrigendumMaster tblCorrigendumMaster) {
        super.addEntity(tblCorrigendumMaster);
    }

    @Override
    public List<TblCorrigendumMaster> findTblCorrigendumMaster(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblCorrigendumMaster(TblCorrigendumMaster tblCorrigendumMaster) {

        super.deleteEntity(tblCorrigendumMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblCorrigendumMaster(TblCorrigendumMaster tblCorrigendumMaster) {

        super.updateEntity(tblCorrigendumMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblCorrigendumMaster> getAllTblCorrigendumMaster() {
        return super.getAllEntity();
    }

    @Override
    public long getTblCorrigendumMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCorrigendumMaster> findByCountTblCorrigendumMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
