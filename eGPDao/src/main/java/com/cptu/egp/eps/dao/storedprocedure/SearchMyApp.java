/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Ramesh.Janagondakuru
 */
public class SearchMyApp extends StoredProcedure {
    private static final Logger LOGGER = Logger.getLogger(SearchMyApp.class);
    public SearchMyApp(BasicDataSource dataSource, String procName) {

       this.setDataSource(dataSource);
       this.setSql(procName);//Stored Procedure name has to be set from DaoContext.

       this.declareParameter(new SqlParameter("v_financialYear_inVc", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_budgetType_tint", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_projectName_inVc", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_activityName_inVc", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_status_inVc", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_Page_inN", Types.INTEGER));
       this.declareParameter(new SqlParameter("v_RecordPerPage_inN", Types.INTEGER));
       this.declareParameter(new SqlParameter("v_UserId_inN", Types.INTEGER));
       this.declareParameter(new SqlParameter("v_AppId_inN", Types.INTEGER));
       this.declareParameter(new SqlParameter("v_AppCode_inN", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_CreatedBy_inN", Types.INTEGER));
       this.declareParameter(new SqlParameter("v_SortingColumn_InVc", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_SortingType_InVc", Types.VARCHAR));
    }

   /**
    * Execute stored procedure : p_search_myapp
    * Get My APP Search data
    * @param financialYear
    * @param budgetType
    * @param projectName
    * @param status
    * @param page
    * @param recordPerPage
    * @param userId
    * @param appId
    * @param appCode
    * @param createdBy
    * @param sortColumn
    * @param sortType
    * @return App Data as list of SearchMyAppData Object
    */
   public List<SearchMyAppData> executeProcedure(String financialYear,String budgetType,String projectName,String activityName,String status,Integer page,Integer recordPerPage,Integer userId,Integer appId,String appCode,Integer createdBy,String sortColumn,String sortType){
        Map inParams = new HashMap();

        inParams.put("v_financialYear_inVc", financialYear);
        inParams.put("v_budgetType_tint", budgetType);
        inParams.put("v_projectName_inVc", projectName);
        inParams.put("v_activityName_inVc", activityName);
        inParams.put("v_status_inVc", status);
        inParams.put("v_Page_inN", page);
        inParams.put("v_RecordPerPage_inN", recordPerPage);
        inParams.put("v_UserId_inN", userId);
        inParams.put("v_AppId_inN", appId);
        inParams.put("v_AppCode_inN", appCode);
        inParams.put("v_CreatedBy_inN", createdBy);
        inParams.put("v_SortingColumn_InVc", sortColumn);
        inParams.put("v_SortingType_InVc", sortType);

         this.compile();
         List<SearchMyAppData> details = new ArrayList<SearchMyAppData>();
         try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            if (list != null && !list.isEmpty()) {
                for (LinkedHashMap<String, Object> linkedHashMap : list) {
                    SearchMyAppData searchMyAppData = new SearchMyAppData();
                    searchMyAppData.setAppId((Integer) linkedHashMap.get("appId"));
                    searchMyAppData.setAppCode((String) linkedHashMap.get("appCode"));
                    searchMyAppData.setBudgetType((String) linkedHashMap.get("budgetType"));
                    searchMyAppData.setPrjName((String) linkedHashMap.get("projectName"));
                    searchMyAppData.setactivityName((String) linkedHashMap.get("activityName"));
                    searchMyAppData.setTotalPage((Integer) linkedHashMap.get("TotalPages"));
                    searchMyAppData.setTotalRec((Integer) linkedHashMap.get("TotalRecords"));
                    details.add(searchMyAppData);
                }
            }
            LOGGER.debug("SearchMyApp : No data found ");
        } catch (Exception e) {
            LOGGER.error("SearchMyApp : "+ e);
            e.printStackTrace();
        }
      return details;

    }
}
