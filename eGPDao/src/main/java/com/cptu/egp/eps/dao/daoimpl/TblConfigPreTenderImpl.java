package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblConfigPreTenderDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblConfigPreTender;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblConfigPreTenderImpl extends AbcAbstractClass<TblConfigPreTender> implements TblConfigPreTenderDao {

    @Override
    public void addTblConfigPreTender(TblConfigPreTender configPreTender){
        super.addEntity(configPreTender);
    }

    @Override
    public void deleteTblConfigPreTender(TblConfigPreTender configPreTender) {
        super.deleteEntity(configPreTender);
    }

    @Override
    public void deleteAllTblConfigPreTender(Collection entity) {
        super.deleteAll(entity);
    }

    @Override
    public void updateTblConfigPreTender(TblConfigPreTender configPreTender) {
        super.updateEntity(configPreTender);
    }

    @Override
    public List<TblConfigPreTender> getAllTblConfigPreTender() {
        return super.getAllEntity();
    }

    @Override
    public List<TblConfigPreTender> findTblConfigPreTender(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblConfigPreTenderCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblConfigPreTender> findByCountTblConfigPreTender(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
