/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblNoaDocumentsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblNoaDocuments;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblNoaDocumentsImpl extends AbcAbstractClass<TblNoaDocuments> implements TblNoaDocumentsDao{

    @Override
    public void addTblNoaDocuments(TblNoaDocuments tblNoaDocuments) {
        super.addEntity(tblNoaDocuments);
    }

    @Override
    public List<TblNoaDocuments> findTblNoaDocuments(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblNoaDocuments(TblNoaDocuments tblNoaDocuments) {

        super.deleteEntity(tblNoaDocuments);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblNoaDocuments(Collection tblNoaDocuments) {
        super.deleteAll(tblNoaDocuments);
    }

    @Override
    public void updateTblNoaDocuments(TblNoaDocuments tblNoaDocuments) {

        super.updateEntity(tblNoaDocuments);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblNoaDocuments> getAllTblNoaDocuments() {
        return super.getAllEntity();
    }

    @Override
    public long getTblNoaDocumentsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblNoaDocuments> findByCountTblNoaDocuments(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
