/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderEnvelopesDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderEnvelopes;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderEnvelopesImpl extends AbcAbstractClass<TblTenderEnvelopes> implements TblTenderEnvelopesDao {

    @Override
    public void addTblTenderEnvelopes(TblTenderEnvelopes admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblTenderEnvelopes(TblTenderEnvelopes admin) {
        super.deleteEntity(admin);    
    }

    @Override
    public void updateTblTenderEnvelopes(TblTenderEnvelopes admin) {
        super.updateEntity(admin);
    }

    @Override
    public List<TblTenderEnvelopes> getAllTblTenderEnvelopes() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderEnvelopes> findTblTenderEnvelopes(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderEnvelopesCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderEnvelopes> findByCountTblTenderEnvelopes(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
