/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblWsOrgMaster;
import java.util.List;

/**
 *
 * @author Sreenu
 */
public interface TblWsOrgMasterDao extends GenericDao<TblWsOrgMaster>{

    public void addTblWsOrgMaster(TblWsOrgMaster tblWsOrgMaster);

    public void deleteTblWsOrgMaster(TblWsOrgMaster tblWsOrgMaster);

    public void updateTblWsOrgMaster(TblWsOrgMaster tblWsOrgMaster);
    
    public List<TblWsOrgMaster> getAllTblWsOrgMaster();

    public List<TblWsOrgMaster> findTblWsOrgMaster(Object... values) throws Exception;

    public List<TblWsOrgMaster> findByCountTblWsOrgMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getCountTblWsOrgMaster();

    public List<TblWsOrgMaster> getRequiredTblWsOrgMasterList(String whereCondition);

}
