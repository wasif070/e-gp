package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblEmployeeOfficesDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEmployeeOffices;
import java.util.List;
import java.util.ArrayList;
import org.apache.log4j.Logger;
/**
 *
 * @author taher
 */
public class TblEmployeeOfficesImpl extends AbcAbstractClass<TblEmployeeOffices> implements TblEmployeeOfficesDao {
        final static Logger log=Logger.getLogger(TblEmployeeOfficesImpl.class);
    @Override
    public void addTblEmployeeOffices(TblEmployeeOffices empOffice){
        super.addEntity(empOffice);
    }

    @Override
    public void deleteTblEmployeeOffices(TblEmployeeOffices empOffice) {
        super.deleteEntity(empOffice);
    }

    @Override
    public void updateTblEmployeeOffices(TblEmployeeOffices empOffice) {
        super.updateEntity(empOffice);
    }

    @Override
    public List<TblEmployeeOffices> getAllTblEmployeeOffices() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEmployeeOffices> findTblEmployeeOffices(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEmployeeOfficesCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEmployeeOffices> findByCountTblEmployeeOffices(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    // Start GSS Change
    public List<Object> getOfficeHirarchies(int officeId, List<Object> offices)throws Exception {
    	 List<Object> officesHirarchy;
    	 if(offices!=null && offices.size()!=0){
    		 officesHirarchy=offices;
      	 }
    	 else officesHirarchy=new ArrayList<Object>();
    	List<Object> lstOffices=getOfficeByIdForHirarchy(officeId);
    	log.error("In getOfficeHirarchies >>>>>>>>>11111111111"+lstOffices.size());
    	if(lstOffices.size()!=0){
    		officesHirarchy.add(lstOffices.get(0));
    		log.error("In getOfficeHirarchies >>>>>>>>>22222222222"+((Integer)lstOffices.get(0)).intValue() );
    		this.getOfficeHirarchies(((Integer)lstOffices.get(0)).intValue(),officesHirarchy);
    	}
        return officesHirarchy;
    }
    @Override
     public List<Object> getOfficeByIdForHirarchy(int officeId)throws Exception {
    	log.error("list size in dao emp officesss in query");
    	String query="select office.officeId from tbl_OfficeMaster office where office.departmentId in";
    	query=query+" (select  dept.parentDepartmentId from tbl_DepartmentMaster dept ,tbl_OfficeMaster officeMaster where officeMaster.officeId ="+officeId+" and dept.departmentId = officeMaster.departmentId)";
        return super.nativeSQLQuery(query);
    }
    // End GSS change
}
