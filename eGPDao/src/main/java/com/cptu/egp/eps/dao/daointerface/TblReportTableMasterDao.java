package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblReportTableMaster;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblReportTableMasterDao extends GenericDao<TblReportTableMaster> {

    public void addTblReportTableMaster(TblReportTableMaster tblReportTableMaster);

    public void deleteTblReportTableMaster(TblReportTableMaster tblReportTableMaster);

    public void deleteAllTblReportTableMaster(Collection tblReportTableMaster);

    public void updateTblReportTableMaster(TblReportTableMaster tblReportTableMaster);

    public List<TblReportTableMaster> getAllTblReportTableMaster();

    public List<TblReportTableMaster> findTblReportTableMaster(Object... values) throws Exception;

    public List<TblReportTableMaster> findByCountTblReportTableMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblReportTableMasterCount();
}
