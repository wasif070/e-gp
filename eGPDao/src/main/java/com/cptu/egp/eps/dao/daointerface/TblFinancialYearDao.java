package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblFinancialYear;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblFinancialYearDao extends GenericDao<TblFinancialYear> {

    public void addTblFinancialYear(TblFinancialYear tblObj);

    public void deleteTblFinancialYear(TblFinancialYear tblObj);

    public void updateTblFinancialYear(TblFinancialYear tblObj);

    public List<TblFinancialYear> getAllTblFinancialYear();

    public List<TblFinancialYear> findTblFinancialYear(Object... values) throws Exception;

    public List<TblFinancialYear> findByCountTblFinancialYear(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblFinancialYearCount();
}
