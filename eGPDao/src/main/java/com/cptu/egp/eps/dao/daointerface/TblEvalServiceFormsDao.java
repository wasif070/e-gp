/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalServiceForms;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TblEvalServiceFormsDao extends GenericDao<TblEvalServiceForms>{


    public void addTblEvalServiceForms(TblEvalServiceForms tblObj);

    public void deleteTblEvalServiceForms(TblEvalServiceForms tblObj);

    public void updateTblEvalServiceForms(TblEvalServiceForms tblObj);

    public List<TblEvalServiceForms> getAllTblEvalServiceForms();

    public List<TblEvalServiceForms> findTblEvalServiceForms(Object... values) throws Exception;

    public List<TblEvalServiceForms> findByCountTblEvalServiceForms(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalServiceFormsCount();

    public void updateOrInsertAll(List<TblEvalServiceForms> list);

}
