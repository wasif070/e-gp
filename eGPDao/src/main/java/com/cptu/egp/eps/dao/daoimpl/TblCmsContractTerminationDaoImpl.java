/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblCmsContractTerminationDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsContractTermination;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public class TblCmsContractTerminationDaoImpl extends AbcAbstractClass<TblCmsContractTermination> implements TblCmsContractTerminationDao {

    @Override
    public void addTblCmsContractTermination(TblCmsContractTermination tblCmsContractTermination) {
        super.addEntity(tblCmsContractTermination);
    }

    @Override
    public void deleteTblCmsContractTermination(TblCmsContractTermination tblCmsContractTermination) {
        super.deleteEntity(tblCmsContractTermination);
    }

    @Override
    public void updateTblCmsContractTermination(TblCmsContractTermination tblCmsContractTermination) {
        super.updateEntity(tblCmsContractTermination);
    }

    @Override
    public List<TblCmsContractTermination> getAllTblCmsContractTermination() {
        super.setPersistentClass(TblCmsContractTermination.class);
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsContractTermination> findTblCmsContractTermination(Object... values) throws Exception {
        super.setPersistentClass(TblCmsContractTermination.class);
        return super.findEntity(values);
    }

    @Override
    public List<TblCmsContractTermination> findByCountTblCmsContractTermination(int firstResult, int maxResult, Object... values) throws Exception {
        super.setPersistentClass(TblCmsContractTermination.class);
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblCmsContractTerminationCount() {
        super.setPersistentClass(TblCmsContractTermination.class);
        return super.getEntityCount();
    }
}
