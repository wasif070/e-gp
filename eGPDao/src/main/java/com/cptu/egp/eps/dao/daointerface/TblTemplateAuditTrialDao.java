/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTemplateAuditTrial;
import java.util.List;

/**
 *
 * @author yanki
 */
public interface TblTemplateAuditTrialDao extends GenericDao<TblTemplateAuditTrial>{

    public void addTblTemplateAuditTrial(TblTemplateAuditTrial templateAuditTrail);

    public void deleteTblTemplateAuditTrial(TblTemplateAuditTrial templateAuditTrail);

    public void updateTblTemplateAuditTrial(TblTemplateAuditTrial templateAuditTrail);

    public List<TblTemplateAuditTrial> getAllTblTemplateAuditTrial();

    public List<TblTemplateAuditTrial> findTblTemplateAuditTrial(Object... values) throws Exception;

}
