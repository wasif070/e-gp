/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;
import com.cptu.egp.eps.dao.generic.GenericDao;
import java.util.List;
import com.cptu.egp.eps.model.table.TblAppPermission;
/**
 *
 * @author HP
 */
public interface TblAppPermissionDao extends GenericDao<TblAppPermission> {
    public void addTblAppPermission(TblAppPermission tblObj);

    public void deleteTblAppPermission(TblAppPermission tblObj);

    public void updateTblAppPermission(TblAppPermission tblObj);

    public List<TblAppPermission> getAllTblAppPermission();

    public List<TblAppPermission> findTblAppPermission(Object... values) throws Exception;

    public List<TblAppPermission> findByCountTblAppPermission(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblAppPermissionCount();

    public void updateOrSaveTblAppPermission(List<TblAppPermission> tblObj);
}
