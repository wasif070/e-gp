package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTempCompanyMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblTempCompanyMasterDao extends GenericDao<TblTempCompanyMaster>{

    public void addTblTempCompanyMaster(TblTempCompanyMaster master);

    public void deleteTblTempCompanyMaster(TblTempCompanyMaster master);

    public void updateTblTempCompanyMaster(TblTempCompanyMaster master);

    public List<TblTempCompanyMaster> getAllTblTempCompanyMaster();

    public List<TblTempCompanyMaster> findTblTempCompanyMaster(Object... values) throws Exception;

    public long getTblTempCompanyMasterCount();

    public List<TblTempCompanyMaster> findByCountTblTempCompanyMaster(int firstResult, int maxResult, Object... values) throws Exception;
}
