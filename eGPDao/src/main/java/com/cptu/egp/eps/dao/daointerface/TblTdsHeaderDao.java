package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTdsHeader;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTdsHeaderDao extends GenericDao<TblTdsHeader> {

    public void addTblTdsHeader(TblTdsHeader tblObj);

    public void deleteTblTdsHeader(TblTdsHeader tblObj);

    public void updateTblTdsHeader(TblTdsHeader tblObj);

    public List<TblTdsHeader> getAllTblTdsHeader();

    public List<TblTdsHeader> findTblTdsHeader(Object... values) throws Exception;

    public List<TblTdsHeader> findByCountTblTdsHeader(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTdsHeaderCount();
}
