package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsTemplateSrvBoqDetailDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsTemplateSrvBoqDetail;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsTemplateSrvBoqDetialImpl extends AbcAbstractClass<TblCmsTemplateSrvBoqDetail> implements TblCmsTemplateSrvBoqDetailDao{


    @Override
    public void addTblCmsTemplateSrvBoqDetail(TblCmsTemplateSrvBoqDetail cmsTemplateSrvBoqDetail) {
        super.addEntity(cmsTemplateSrvBoqDetail);
    }

    @Override
    public void deleteTblCmsTemplateSrvBoqDetail(TblCmsTemplateSrvBoqDetail cmsTemplateSrvBoqDetail) {
        super.deleteEntity(cmsTemplateSrvBoqDetail);
    }

    @Override
    public void updateTblCmsTemplateSrvBoqDetail(TblCmsTemplateSrvBoqDetail cmsTemplateSrvBoqDetail) {
        super.updateEntity(cmsTemplateSrvBoqDetail);
    }

    @Override
    public List<TblCmsTemplateSrvBoqDetail> getAllTblCmsTemplateSrvBoqDetail() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsTemplateSrvBoqDetail> findTblCmsTemplateSrvBoqDetail(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public List<TblCmsTemplateSrvBoqDetail> findByCountTblCmsTemplateSrvBoqDetail(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblCmsTemplateSrvBoqDetailCount() {
        return super.getEntityCount();
    }
}
