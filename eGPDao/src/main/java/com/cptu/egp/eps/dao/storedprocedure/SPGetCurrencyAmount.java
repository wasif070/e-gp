/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

//import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author salahuddin
 */
public class SPGetCurrencyAmount  extends StoredProcedure   {

    private static final Logger LOGGER = Logger.getLogger(SPGetCurrencyAmount.class);

    public SPGetCurrencyAmount(BasicDataSource dataSource, String procName)
    {
       this.setDataSource(dataSource);
       this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
       LOGGER.debug("Proc Name: "+ procName);       

       this.declareParameter(new SqlParameter("tenderID", Types.INTEGER));
       this.declareParameter(new SqlParameter("roundID", Types.INTEGER));

    }


    public List<GetCurrencyAmount> executeProcedure(int tenderID, int roundID)
    {
        //boolean isAdded = false;
        Map inParams = new HashMap();
        inParams.put("tenderID", tenderID);
        inParams.put("roundID", roundID);
        this.compile();


        List<GetCurrencyAmount> details = new ArrayList<GetCurrencyAmount>();
        try{
             ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
             for (LinkedHashMap<String, Object> linkedHashMap : list) {
                GetCurrencyAmount currencyDetails =new GetCurrencyAmount();

                currencyDetails.setValue1((Double)linkedHashMap.get("value1"));
                currencyDetails.setCurrencyName1((String)linkedHashMap.get("currencyName1"));
                currencyDetails.setValue2((Double)linkedHashMap.get("value2"));
                currencyDetails.setCurrencyName2((String)linkedHashMap.get("currencyName2"));
                currencyDetails.setValue3((Double)linkedHashMap.get("value3"));
                currencyDetails.setCurrencyName3((String)linkedHashMap.get("currencyName3"));
                currencyDetails.setValue4((Double)linkedHashMap.get("value4"));
                currencyDetails.setCurrencyName4((String)linkedHashMap.get("currencyName4"));
                
                currencyDetails.setCurrencyID1((Integer)linkedHashMap.get("currencyID1"));
                currencyDetails.setCurrencyID2((Integer)linkedHashMap.get("currencyID2"));
                currencyDetails.setCurrencyID3((Integer)linkedHashMap.get("currencyID3"));
                currencyDetails.setCurrencyID4((Integer)linkedHashMap.get("currencyID4"));

                details.add(currencyDetails);
            }
        }

        catch(Exception e){
        LOGGER.error("SPGetCurrencyAmount : "+ e);
        }

        return details;
    }

}
