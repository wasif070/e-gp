package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblMultiLangContent;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblMultiLangContentDao extends GenericDao<TblMultiLangContent> {

    public void addTblMultiLangContent(TblMultiLangContent tblMultiLangContent);

    public void deleteTblMultiLangContent(TblMultiLangContent tblMultiLangContent);

    public void deleteAllTblMultiLangContent(Collection tblMultiLangContent);

    public void updateTblMultiLangContent(TblMultiLangContent tblMultiLangContent);

    public List<TblMultiLangContent> getAllTblMultiLangContent();

    public List<TblMultiLangContent> findTblMultiLangContent(Object... values) throws Exception;

    public List<TblMultiLangContent> findByCountTblMultiLangContent(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblMultiLangContentCount();
}
