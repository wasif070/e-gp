package com.cptu.egp.eps.dao.daointerface;


import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblReportDocs;
import java.util.List;


	public interface TblReportDocsDao extends GenericDao<TblReportDocs> {

	public void addTblReportDocs(TblReportDocs tblObj);

	public void deleteTblReportDocs(TblReportDocs tblObj);

	public void updateTblReportDocs(TblReportDocs tblObj);

	public List<TblReportDocs> getAllTblReportDocs();

	public List<TblReportDocs> findTblReportDocs(Object... values) throws Exception;

	public List<TblReportDocs> findByCountTblReportDocs(int firstResult,int maxResult,Object... values) throws Exception;

	public long getTblReportDocsCount();

	public void updateOrSaveTblReportDocs(List<TblReportDocs> tblObj);
}
