package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblPartnerAdmin;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblPartnerAdminDao extends GenericDao<TblPartnerAdmin> {

    public void addTblPartnerAdmin(TblPartnerAdmin admin);

    public void deleteTblPartnerAdmin(TblPartnerAdmin admin);

    public void updateTblPartnerAdmin(TblPartnerAdmin admin);

    public List<TblPartnerAdmin> getAllTblPartnerAdmin();

    public List<TblPartnerAdmin> findTblPartnerAdmin(Object... values) throws Exception;

    public List<TblPartnerAdmin> findByCountTblPartnerAdmin(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblPartnerAdminCount();
}
