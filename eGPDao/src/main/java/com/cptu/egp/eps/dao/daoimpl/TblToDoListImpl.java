package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblToDoListDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblToDoList;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblToDoListImpl extends AbcAbstractClass<TblToDoList> implements TblToDoListDao {

    @Override
    public void addTblToDoList(TblToDoList master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblToDoList(TblToDoList master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblToDoList(TblToDoList master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblToDoList> getAllTblToDoList() {
        return super.getAllEntity();
    }

    @Override
    public List<TblToDoList> findTblToDoList(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblToDoListCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblToDoList> findByCountTblToDoList(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
