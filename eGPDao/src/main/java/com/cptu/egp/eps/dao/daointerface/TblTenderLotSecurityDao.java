/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderLotSecurity;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderLotSecurityDao extends GenericDao<TblTenderLotSecurity>{

    public void addTblTenderLotSecurity(TblTenderLotSecurity tblObj);

    public void deleteTblTenderLotSecurity(TblTenderLotSecurity tblObj);

    public void updateTblTenderLotSecurity(TblTenderLotSecurity tblObj);

    public List<TblTenderLotSecurity> getAllTblTenderLotSecurity();

    public List<TblTenderLotSecurity> findTblTenderLotSecurity(Object... values) throws Exception;

    public List<TblTenderLotSecurity> findByCountTblTenderLotSecurity(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderLotSecurityCount();
}
