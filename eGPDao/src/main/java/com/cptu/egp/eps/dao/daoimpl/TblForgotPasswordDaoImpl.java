/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblForgotPasswordDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblForgotPassword;
import java.util.List;

/**
 *
 * @author dixit
 */
public class TblForgotPasswordDaoImpl extends AbcAbstractClass<TblForgotPassword> implements TblForgotPasswordDao {

    @Override
    public void addTblForgotPassword(TblForgotPassword tblForgotPassword) {
        super.addEntity(tblForgotPassword);
    }

    @Override
    public void deleteTblForgotPassword(TblForgotPassword tblForgotPassword) {
        super.deleteEntity(tblForgotPassword);
    }

    @Override
    public void updateTblForgotPassword(TblForgotPassword tblForgotPassword) {
        super.updateEntity(tblForgotPassword);
    }

    @Override
    public List<TblForgotPassword> getAllTblForgotPassword() {
        return super.getAllEntity();
    }

    @Override
    public List<TblForgotPassword> findTblForgotPassword(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public List<TblForgotPassword> findByCountTblForgotPassword(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblForgotPasswordCount() {
       return super.getEntityCount();
    }

}
