/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 * This class is helps for execution the procedure <b>p_get_DataUpToCMSForRHD_BoQ</b>.
 * @author Sudhir Chavhan
 */
public class SPSharedCMSDetailBoQForRHD extends StoredProcedure {

    private static final Logger LOGGER = Logger.getLogger(SPSharedCMSDetailBoQForRHD.class);
    /***
     * Construction for <b>SPSharedCMSDetailBoQForRHD</b>
     * we inject this properties at DaoContext
     * @param dataSource
     * @param procName
     */
    public SPSharedCMSDetailBoQForRHD(BasicDataSource dataSource, String procName) {
    this.setDataSource(dataSource);
    this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
    this.declareParameter(new SqlParameter("@v_tenderId_inN",Types.VARCHAR));
    LOGGER.debug("Proc Name: " + procName);
    }
    /***
     * This method executes the given procedure and returns the list of objects
     * Execute procedure : p_get_DataUpToCMSForRHD_BoQ
     * @return List<SPSharedCMSDetailBoQForRHD>
     */
    public List<RHDIntegrationCMSBoQDetails> executeProcedure(String tenderId) {

        Map inParams = new HashMap();
        inParams.put("@v_tenderId_inN", tenderId);

        this.compile();
        List<RHDIntegrationCMSBoQDetails> RHDIntegrationDetailsList =
                new ArrayList<RHDIntegrationCMSBoQDetails>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list =
                    (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");

            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                RHDIntegrationCMSBoQDetails RHDIntegrationBoQDetails =
                        new RHDIntegrationCMSBoQDetails();

                RHDIntegrationBoQDetails.setBoQ_ItemGroup((String) linkedHashMap.get("groupId"));
                RHDIntegrationBoQDetails.setBoQ_ItemSrNo((String) linkedHashMap.get("wpSrNo"));
                RHDIntegrationBoQDetails.setBoQ_ItemDesc((String) linkedHashMap.get("wpDescription"));
                RHDIntegrationBoQDetails.setBoQ_UnitofMeasur((String) linkedHashMap.get("wpUom"));
                RHDIntegrationBoQDetails.setBoQ_ItemQnty((BigDecimal) linkedHashMap.get("wpQty"));
                RHDIntegrationBoQDetails.setBoQ_WPstartDt((Date) linkedHashMap.get("wpStartDate"));
                RHDIntegrationBoQDetails.setBoQ_WPEndDt((Date) linkedHashMap.get("wpEndDate"));
                RHDIntegrationBoQDetails.setBoQ_WPNoOfDays((Integer) linkedHashMap.get("wpNoOfDays"));
                RHDIntegrationBoQDetails.setBoQ_UnitRate((BigDecimal) linkedHashMap.get("wpRate"));
                RHDIntegrationDetailsList.add(RHDIntegrationBoQDetails);
            }
        } catch (Exception e) {
            LOGGER.error("SPSharedCMSDetailBoQForRHD : " + e.toString());
            System.out.println("Exception SPSharedCMSDetailBoQForRHD : " + e.toString());
        }
        return RHDIntegrationDetailsList;
    }

}

