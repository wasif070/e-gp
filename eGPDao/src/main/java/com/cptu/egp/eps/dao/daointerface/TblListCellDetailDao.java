package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblListCellDetail;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblListCellDetailDao extends GenericDao<TblListCellDetail> {

    public void addTblListCellDetail(TblListCellDetail tblObj);

    public void deleteTblListCellDetail(TblListCellDetail tblObj);

    public void updateTblListCellDetail(TblListCellDetail tblObj);

    public List<TblListCellDetail> getAllTblListCellDetail();

    public List<TblListCellDetail> findTblListCellDetail(Object... values) throws Exception;

    public List<TblListCellDetail> findByCountTblListCellDetail(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblListCellDetailCount();
}
