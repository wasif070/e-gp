package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblTendererEsignatureDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTendererEsignature;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblTendererEsignatureImpl extends AbcAbstractClass<TblTendererEsignature> implements TblTendererEsignatureDao {

    @Override
    public void addTblTendererEsignature(TblTendererEsignature tendererEsignature){
        super.addEntity(tendererEsignature);
    }

    @Override
    public void deleteTblTendererEsignature(TblTendererEsignature tendererEsignature) {
        super.deleteEntity(tendererEsignature);
    }

    @Override
    public void updateTblTendererEsignature(TblTendererEsignature tendererEsignature) {
        super.updateEntity(tendererEsignature);
    }

    @Override
    public List<TblTendererEsignature> getAllTblTendererEsignature() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTendererEsignature> findTblTendererEsignature(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTendererEsignatureCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTendererEsignature> findByCountTblTendererEsignature(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
