/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblQuestionCategoryDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblQuestionCategory;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public class TblQuestionCategoryImpl extends AbcAbstractClass<TblQuestionCategory> implements TblQuestionCategoryDao {

    @Override
    public void addTblQuestionCategory(TblQuestionCategory questionCategory) {

        super.addEntity(questionCategory);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblQuestionCategory(TblQuestionCategory questionCategory) {

        super.deleteEntity(questionCategory);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblQuestionCategory(TblQuestionCategory questionCategory) {

        super.updateEntity(questionCategory);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblQuestionCategory> getAllTblQuestionCategory() {
        return super.getAllEntity();
    }

    @Override
    public List<TblQuestionCategory> findTblQuestionCategory(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblQuestionCategoryCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblQuestionCategory> findByCountTblQuestionCategory(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
