package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblPublicProcureForum;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblPublicProcureForumDao extends GenericDao<TblPublicProcureForum> {

    public void addTblPublicProcureForum(TblPublicProcureForum tblObj);

    public void deleteTblPublicProcureForum(TblPublicProcureForum tblObj);

    public void updateTblPublicProcureForum(TblPublicProcureForum tblObj);

    public List<TblPublicProcureForum> getAllTblPublicProcureForum();

    public List<TblPublicProcureForum> findTblPublicProcureForum(Object... values) throws Exception;

    public List<TblPublicProcureForum> findByCountTblPublicProcureForum(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblPublicProcureForumCount();
}
