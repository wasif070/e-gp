/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblUserPrefrenceDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblUserPrefrence;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public class TblUserPrefrenceImpl extends AbcAbstractClass<TblUserPrefrence> implements TblUserPrefrenceDao {

    @Override
    public void addTblUserPrefrence(TblUserPrefrence user) {

        super.addEntity(user);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblUserPrefrence(TblUserPrefrence user) {

        super.deleteEntity(user);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblUserPrefrence(TblUserPrefrence user) {

        super.updateEntity(user);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblUserPrefrence> getAllTblUserPrefrence() {
        return super.getAllEntity();
    }

    @Override
    public List<TblUserPrefrence> findTblUserPrefrence(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblUserPrefrenceCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblUserPrefrence> findByCountTblUserPrefrence(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
