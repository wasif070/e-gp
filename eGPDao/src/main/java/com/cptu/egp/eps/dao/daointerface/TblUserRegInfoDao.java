package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblUserRegInfo;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblUserRegInfoDao extends GenericDao<TblUserRegInfo> {

    public void addTblUserRegInfo(TblUserRegInfo tblUserRegInfo);

    public void deleteTblUserRegInfo(TblUserRegInfo tblUserRegInfo);

    public void updateTblUserRegInfo(TblUserRegInfo tblUserRegInfo);

    public List<TblUserRegInfo> getAllTblUserRegInfo();

    public List<TblUserRegInfo> findTblUserRegInfo(Object... values) throws Exception;

    public List<TblUserRegInfo> findByCountTblUserRegInfo(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblUserRegInfoCount();
}
