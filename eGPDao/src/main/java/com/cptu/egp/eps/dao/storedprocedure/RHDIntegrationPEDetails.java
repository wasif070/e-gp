/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

/**
 * This class is created for web-service(RHD PE Integration)
 * @author Sudhir Chavhan
 */
public class RHDIntegrationPEDetails {

    private int peId;
    private String peOfficeName;
    private String deptType;
    private String ministryName;
    private String divisionName;
    private String rHDorgName;
    private String districtName;
    private String officeAddr;

    public int getPEId() {
        return peId;
    }

    public void setPEId(int peId) {
        this.peId = peId;
    }

    public String getPEOfficeName() {
        return peOfficeName;
    }

    public void setPEOfficeName(String peOfficeName) {
        this.peOfficeName = peOfficeName;
    }

    public String getDeptType() {
        return deptType;
    }

    public void setDeptType(String deptType) {
        this.deptType = deptType;
    }

    public String getMinistryName() {
        return ministryName;
    }

    public void setMinistryName(String ministryName) {
        this.ministryName = ministryName;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public String getRHDOrgName() {
        return rHDorgName;
    }

    public void setRHDOrgName(String rHDorgName) {
        this.rHDorgName = rHDorgName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }
    public String getOfficeAddr() {
        return officeAddr;
    }

    public void setOfficeAddr(String officeAddr) {
        this.officeAddr = officeAddr;
    }
}
