/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblAdminMaster;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public interface TblAdminMasterDao extends GenericDao<TblAdminMaster> {

    public void addTblAdminMaster(TblAdminMaster admin);

    public void deleteTblAdminMaster(TblAdminMaster admin);

    public void updateTblAdminMaster(TblAdminMaster admin);

    public List<TblAdminMaster> getAllTblAdminMaster();

    public List<TblAdminMaster> findTblAdminMaster(Object... values) throws Exception;

    public List<TblAdminMaster> findByCountTblAdminMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblAdminMasterCount();
}
