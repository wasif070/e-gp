package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalBidderStatus;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblEvalBidderStatusDao extends GenericDao<TblEvalBidderStatus> {

    public void addTblEvalBidderStatus(TblEvalBidderStatus tblObj);

    public void deleteTblEvalBidderStatus(TblEvalBidderStatus tblObj);

    public void updateTblEvalBidderStatus(TblEvalBidderStatus tblObj);

    public List<TblEvalBidderStatus> getAllTblEvalBidderStatus();

    public List<TblEvalBidderStatus> findTblEvalBidderStatus(Object... values) throws Exception;

    public List<TblEvalBidderStatus> findByCountTblEvalBidderStatus(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalBidderStatusCount();
}
