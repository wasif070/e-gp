package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblMarqueeMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblMarqueeMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblMarqueeMasterImpl extends AbcAbstractClass<TblMarqueeMaster> implements TblMarqueeMasterDao {

    @Override
    public void addTblMarqueeMaster(TblMarqueeMaster marqueeMaster){
        super.addEntity(marqueeMaster);
    }

    @Override
    public void deleteTblMarqueeMaster(TblMarqueeMaster marqueeMaster) {
        super.deleteEntity(marqueeMaster);
    }

    @Override
    public void updateTblMarqueeMaster(TblMarqueeMaster marqueeMaster) {
        super.updateEntity(marqueeMaster);
    }

    @Override
    public List<TblMarqueeMaster> getAllTblMarqueeMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblMarqueeMaster> findTblMarqueeMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblMarqueeMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblMarqueeMaster> findByCountTblMarqueeMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
