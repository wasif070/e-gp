/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblDebarmentRespDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblDebarmentResp;
import java.util.List;

/**
 *
 * @author test
 */
public class TblDebarmentRespDaoImpl extends AbcAbstractClass<TblDebarmentResp> implements TblDebarmentRespDao {

    @Override
    public void addTblDebarmentResp(TblDebarmentResp tblDebarmentResp) {
        super.addEntity(tblDebarmentResp);
    }

    @Override
    public void deleteTblDebarmentResp(TblDebarmentResp tblDebarmentResp) {
        super.deleteEntity(tblDebarmentResp);
    }

    @Override
    public void updateTblDebarmentResp(TblDebarmentResp tblDebarmentResp) {
        super.updateEntity(tblDebarmentResp);
    }

    @Override
    public List<TblDebarmentResp> getAllTblDebarmentResp() {
        return super.getAllEntity();
    }

    @Override
    public List<TblDebarmentResp> findTblTblDebarmentResp(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public List<TblDebarmentResp> findByCountTblDebarmentResp(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblTblDebarmentResp() {
        return super.getEntityCount();
    }
    
}
