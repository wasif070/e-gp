package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTosDocs;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTosDocsDao extends GenericDao<TblTosDocs> {

    public void addTblTosDocs(TblTosDocs tblObj);

    public void deleteTblTosDocs(TblTosDocs tblObj);

    public void updateTblTosDocs(TblTosDocs tblObj);

    public List<TblTosDocs> getAllTblTosDocs();

    public List<TblTosDocs> findTblTosDocs(Object... values) throws Exception;

    public List<TblTosDocs> findByCountTblTosDocs(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTosDocsCount();
}
