package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalRptSentToAa;
import java.util.List;

/**
 *
 * @author rishita
 */
public interface TblEvalRptSentToAaDao extends GenericDao<TblEvalRptSentToAa> {

    public void addTblEvalRptSentToAa(TblEvalRptSentToAa tblEvalRptSentToAa);

    public void deleteTblEvalRptSentToAa(TblEvalRptSentToAa tblEvalRptSentToAa);

    public void updateTblEvalRptSentToAa(TblEvalRptSentToAa tblEvalRptSentToAa);

    public List<TblEvalRptSentToAa> getAllTblEvalRptSentToAa();

    public List<TblEvalRptSentToAa> findTblEvalRptSentToAa(Object... values) throws Exception;

    public List<TblEvalRptSentToAa> findByCountTblEvalRptSentToAa(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalRptSentToAaCount();
}
