package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblHintQuestionMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblHintQuestionMasterDao extends GenericDao<TblHintQuestionMaster> {

    public List<TblHintQuestionMaster> getAllTblHintQuestionMaster();

    public List<TblHintQuestionMaster> findTblHintQuestionMaster(Object... values) throws Exception;

    public List<TblHintQuestionMaster> findByCountTblHintQuestionMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblHintQuestionMasterCount();
}
