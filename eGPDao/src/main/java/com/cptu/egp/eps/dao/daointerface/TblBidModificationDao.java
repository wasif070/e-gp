/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblBidModification;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblBidModificationDao extends GenericDao<TblBidModification>{

    public void addTblBidModification(TblBidModification tblObj);

    public void deleteTblBidModification(TblBidModification tblObj);

    public void updateTblBidModification(TblBidModification tblObj);

    public List<TblBidModification> getAllTblBidModification();

    public List<TblBidModification> findTblBidModification(Object... values) throws Exception;

    public List<TblBidModification> findByCountTblBidModification(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblBidModificationCount();
}
