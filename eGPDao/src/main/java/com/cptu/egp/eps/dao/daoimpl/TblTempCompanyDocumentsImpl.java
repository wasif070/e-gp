package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblTempCompanyDocumentsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTempCompanyDocuments;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblTempCompanyDocumentsImpl extends AbcAbstractClass<TblTempCompanyDocuments> implements TblTempCompanyDocumentsDao {

    @Override
    public void addTblTempCompanyDocuments(TblTempCompanyDocuments tempCompanyDocuments){
        super.addEntity(tempCompanyDocuments);
    }

    @Override
    public void deleteTblTempCompanyDocuments(TblTempCompanyDocuments tempCompanyDocuments) {
        super.deleteEntity(tempCompanyDocuments);
    }

    @Override
    public void updateTblTempCompanyDocuments(TblTempCompanyDocuments tempCompanyDocuments) {
        super.updateEntity(tempCompanyDocuments);
    }

    @Override
    public List<TblTempCompanyDocuments> getAllTblTempCompanyDocuments() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTempCompanyDocuments> findTblTempCompanyDocuments(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTempCompanyDocumentsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTempCompanyDocuments> findByCountTblTempCompanyDocuments(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
