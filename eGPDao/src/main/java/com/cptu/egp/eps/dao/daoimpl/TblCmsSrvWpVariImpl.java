package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvWpVariDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvWpvari;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsSrvWpVariImpl extends AbcAbstractClass<TblCmsSrvWpvari> implements TblCmsSrvWpVariDao {

    @Override
    public void addTblCmsSrvWpvari(TblCmsSrvWpvari cmsWpVaryDetail){
        super.addEntity(cmsWpVaryDetail);
    }

    @Override
    public void deleteTblCmsSrvWpvari(TblCmsSrvWpvari cmsWpVaryDetail) {
        super.deleteEntity(cmsWpVaryDetail);
    }

    @Override
    public void updateTblCmsSrvWpvari(TblCmsSrvWpvari cmsWpVaryDetail) {
        super.updateEntity(cmsWpVaryDetail);
    }

    @Override
    public List<TblCmsSrvWpvari> getAllTblCmsSrvWpvari() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvWpvari> findTblCmsSrvWpvari(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvWpvariCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvWpvari> findByCountTblCmsSrvWpvari(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsSrvWpvari> estCost) {
       super.updateAll(estCost);
    }
}
