/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblReportTableMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblReportTableMaster;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblReportTableMasterImpl extends AbcAbstractClass<TblReportTableMaster> implements TblReportTableMasterDao{

    @Override
    public void addTblReportTableMaster(TblReportTableMaster tblReportTableMaster) {
        super.addEntity(tblReportTableMaster);
    }

    @Override
    public List<TblReportTableMaster> findTblReportTableMaster(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblReportTableMaster(TblReportTableMaster tblReportTableMaster) {

        super.deleteEntity(tblReportTableMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblReportTableMaster(Collection tblReportTableMaster) {
        super.deleteAll(tblReportTableMaster);
    }

    @Override
    public void updateTblReportTableMaster(TblReportTableMaster tblReportTableMaster) {

        super.updateEntity(tblReportTableMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblReportTableMaster> getAllTblReportTableMaster() {
        return super.getAllEntity();
    }

    @Override
    public long getTblReportTableMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblReportTableMaster> findByCountTblReportTableMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
