package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblIttSubClause;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblIttSubClauseDao extends GenericDao<TblIttSubClause> {

    public void addTblIttSubClause(TblIttSubClause tblObj);

    public void deleteTblIttSubClause(TblIttSubClause tblObj);

    public void updateTblIttSubClause(TblIttSubClause tblObj);

    public List<TblIttSubClause> getAllTblIttSubClause();

    public List<TblIttSubClause> findTblIttSubClause(Object... values) throws Exception;

    public List<TblIttSubClause> findByCountTblIttSubClause(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblIttSubClauseCount();
}
