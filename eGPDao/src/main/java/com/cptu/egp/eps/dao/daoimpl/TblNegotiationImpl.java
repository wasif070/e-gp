/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblNegotiationDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblNegotiation;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblNegotiationImpl extends AbcAbstractClass<TblNegotiation> implements TblNegotiationDao{

    @Override
    public void addTblNegotiation(TblNegotiation tblNegotiation) {
        super.addEntity(tblNegotiation);
    }

    @Override
    public List<TblNegotiation> findTblNegotiation(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblNegotiation(TblNegotiation tblNegotiation) {

        super.deleteEntity(tblNegotiation);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblNegotiation(Collection tblNegotiation) {
        super.deleteAll(tblNegotiation);
    }

    @Override
    public void updateTblNegotiation(TblNegotiation tblNegotiation) {

        super.updateEntity(tblNegotiation);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblNegotiation> getAllTblNegotiation() {
        return super.getAllEntity();
    }

    @Override
    public long getTblNegotiationCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblNegotiation> findByCountTblNegotiation(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
