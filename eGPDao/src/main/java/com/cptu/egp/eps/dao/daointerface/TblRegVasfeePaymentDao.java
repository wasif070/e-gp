package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblRegVasfeePayment;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblRegVasfeePaymentDao extends GenericDao<TblRegVasfeePayment> {

    public void addTblRegVasfeePayment(TblRegVasfeePayment tblObj);

    public void deleteTblRegVasfeePayment(TblRegVasfeePayment tblObj);

    public void updateTblRegVasfeePayment(TblRegVasfeePayment tblObj);

    public List<TblRegVasfeePayment> getAllTblRegVasfeePayment();

    public List<TblRegVasfeePayment> findTblRegVasfeePayment(Object... values) throws Exception;

    public List<TblRegVasfeePayment> findByCountTblRegVasfeePayment(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblRegVasfeePaymentCount();
}
