package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvTcvari;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsSrvTcvariDao extends GenericDao<TblCmsSrvTcvari> {

    public void addTblCmsSrvTcvari(TblCmsSrvTcvari event);

    public void deleteTblCmsSrvTcvari(TblCmsSrvTcvari event);

    public void updateTblCmsSrvTcvari(TblCmsSrvTcvari event);

    public List<TblCmsSrvTcvari> getAllTblCmsSrvTcvari();

    public List<TblCmsSrvTcvari> findTblCmsSrvTcvari(Object... values) throws Exception;

    public List<TblCmsSrvTcvari> findByCountTblCmsSrvTcvari(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsSrvTcvariCount();

    public void updateOrSaveEstCost(List<TblCmsSrvTcvari> srvTcvaris);
}
