/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

/**
 *
 * @author Ramesh.Janagondakuru
 */
public class CommonAppData {
    private String fieldName1;
    private String fieldName2;
    private String fieldName3;
    private String fieldName4;
    private String fieldName5;
    private String fieldName6;
    private String fieldName7;
    private String fieldName8;
    private String fieldName9;

    public String getFieldName1() {
        return fieldName1;
    }

    public void setFieldName1(String fieldName1) {
        this.fieldName1 = fieldName1;
    }

    public String getFieldName2() {
        return fieldName2;
    }

    public void setFieldName2(String fieldName2) {
        this.fieldName2 = fieldName2;
    }

    public String getFieldName3() {
        return fieldName3;
    }

    public void setFieldName3(String fieldName3) {
        this.fieldName3 = fieldName3;
    }

    public String getFieldName4() {
        return fieldName4;
    }

    public void setFieldName4(String fieldName4) {
        this.fieldName4 = fieldName4;
    }

    public String getFieldName5() {
        return fieldName5;
    }

    public void setFieldName5(String fieldName5) {
        this.fieldName5 = fieldName5;
    }

    public String getFieldName6() {
        return fieldName6;
    }

    public void setFieldName6(String fieldName6) {
        this.fieldName6 = fieldName6;
    }
    
    public String getFieldName7() {
        return fieldName7;
    }
    public void setFieldName7(String fieldName7) {
        this.fieldName7 = fieldName7;
    }
    
     public String getFieldName8() {
        return fieldName8;
    }
    public void setFieldName8(String fieldName8) {
        this.fieldName8 = fieldName8;
    }
    
     public String getFieldName9() {
        return fieldName9;
    }
    public void setFieldName9(String fieldName9) {
        this.fieldName9 = fieldName9;
    }





}
