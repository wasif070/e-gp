/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTrackLoginDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTrackLogin;

/**
 *
 * @author Administrator
 */
public class TblTrackLoginImpl extends AbcAbstractClass<TblTrackLogin> implements TblTrackLoginDao {

    @Override
    public void addTblTrackLogin(TblTrackLogin trackLogin) {
        super.addEntity(trackLogin);
    }

}
