package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCurrencyMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCurrencyMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblCurrencyMasterImpl extends AbcAbstractClass<TblCurrencyMaster> implements TblCurrencyMasterDao {

    @Override
    public void addTblCurrencyMaster(TblCurrencyMaster master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblCurrencyMaster(TblCurrencyMaster master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblCurrencyMaster(TblCurrencyMaster master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblCurrencyMaster> getAllTblCurrencyMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCurrencyMaster> findTblCurrencyMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCurrencyMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCurrencyMaster> findByCountTblCurrencyMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
