package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsWptenderBoqmap;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCmsWpTenderBoqmapDao extends GenericDao<TblCmsWptenderBoqmap> {

    public void addTblCmsWptenderBoqmap(TblCmsWptenderBoqmap event);

    public void deleteTblCmsWptenderBoqmap(TblCmsWptenderBoqmap event);

    public void updateTblCmsWptenderBoqmap(TblCmsWptenderBoqmap event);

    public List<TblCmsWptenderBoqmap> getAllTblCmsWptenderBoqmap();

    public List<TblCmsWptenderBoqmap> findTblCmsWptenderBoqmap(Object... values) throws Exception;

    public List<TblCmsWptenderBoqmap> findByCountTblCmsWptenderBoqmap(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsWptenderBoqmapCount();

    public void updateOrSaveEstCost(List<TblCmsWptenderBoqmap> estCost);
}
