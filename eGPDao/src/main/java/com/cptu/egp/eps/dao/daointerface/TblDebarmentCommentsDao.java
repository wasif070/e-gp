package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblDebarmentComments;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblDebarmentCommentsDao extends GenericDao<TblDebarmentComments> {

    public void addTblDebarmentComments(TblDebarmentComments tblObj);

    public void deleteTblDebarmentComments(TblDebarmentComments tblObj);

    public void updateTblDebarmentComments(TblDebarmentComments tblObj);

    public List<TblDebarmentComments> getAllTblDebarmentComments();

    public List<TblDebarmentComments> findTblDebarmentComments(Object... values) throws Exception;

    public List<TblDebarmentComments> findByCountTblDebarmentComments(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblDebarmentCommentsCount();
}
