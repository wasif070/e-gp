package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEventMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblEventMasterDao extends GenericDao<TblEventMaster> {

    public void addTblEventMaster(TblEventMaster tblObj);

    public void deleteTblEventMaster(TblEventMaster tblObj);

    public void updateTblEventMaster(TblEventMaster tblObj);

    public List<TblEventMaster> getAllTblEventMaster();

    public List<TblEventMaster> findTblEventMaster(Object... values) throws Exception;

    public List<TblEventMaster> findByCountTblEventMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEventMasterCount();
}
