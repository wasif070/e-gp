/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Sanjay Prajapati  08th Feb, 2011
 */
public class SPGenerateMegaHash extends StoredProcedure {

private static final Logger LOGGER = Logger.getLogger(SPGenerateMegaHash.class);
public SPGenerateMegaHash(BasicDataSource dataSource, String procName){
     this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("v_status_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_tenderId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_userId_inInt", Types.INTEGER));
        
}

/**
 * Execute stored procedure : p_megahash_fortender
 * To get Megha hash for tender.
 * @param status
 * @param tenderId
 * @param userId
 * @return list of CommonSPReturn which has flag indicate operation performed successfully or not.
 */
 public List<CommonSPReturn> executeProcedure(Integer status,Integer tenderId,Integer userId){

     Map inParams = new HashMap();
        inParams.put("v_status_inInt", status);
        inParams.put("v_tenderId_inInt", tenderId);
        inParams.put("v_userId_inInt", userId);
        
     this.compile();
        List<CommonSPReturn> details = new ArrayList<CommonSPReturn>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonSPReturn commonSPReturn = new CommonSPReturn();
                commonSPReturn.setId( new BigDecimal(linkedHashMap.get("Id").toString()));
                commonSPReturn.setFlag((Boolean)linkedHashMap.get("flag"));
                commonSPReturn.setMsg((String) linkedHashMap.get("Message"));
                details.add(commonSPReturn);           
            }
        } catch (Exception e) {
            LOGGER.error("SPGenerateMegaHash : "+ e);
        }
        return details;
 }

}
