/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalRptForwardToAa;
import java.util.List;

/**
 *
 * @author darshan
 */
public interface TblEvalRptForwardToAaDao extends GenericDao<TblEvalRptForwardToAa> {

    public void addTblEvalRptForwardToAa(TblEvalRptForwardToAa tblEvalRptSentToAa);

    public void deleteTblEvalRptForwardToAa(TblEvalRptForwardToAa tblEvalRptSentToAa);

    public void updateTblEvalRptForwardToAa(TblEvalRptForwardToAa tblEvalRptSentToAa);

    public List<TblEvalRptForwardToAa> getAllTblEvalRptForwardToAa();

    public List<TblEvalRptForwardToAa> findTblEvalRptForwardToAa(Object... values) throws Exception;

    public List<TblEvalRptForwardToAa> findByCountTblEvalRptForwardToAa(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalRptForwardToAaCount();
}
