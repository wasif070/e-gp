/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderBidEncryptDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderBidEncrypt;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderBidEncryptImpl extends AbcAbstractClass<TblTenderBidEncrypt> implements TblTenderBidEncryptDao{

    @Override
    public void addTblTenderBidEncrypt(TblTenderBidEncrypt tenderBidEncrypt) {
        super.addEntity(tenderBidEncrypt);
    }

    @Override
    public List<TblTenderBidEncrypt> findTblTenderBidEncrypt(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblTenderBidEncrypt(TblTenderBidEncrypt department) {

        super.deleteEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblTenderBidEncrypt(Collection event) {
        super.deleteAll(event);
    }

    @Override
    public void updateTblTenderBidEncrypt(TblTenderBidEncrypt department) {

        super.updateEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblTenderBidEncrypt> getAllTblTenderBidEncrypt() {
        return super.getAllEntity();
    }

    @Override
    public long getTblTenderBidEncryptCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderBidEncrypt> findByCountTblTenderBidEncrypt(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
