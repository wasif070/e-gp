/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderDetailsDao extends GenericDao<TblTenderDetails>{

    public void addTblTenderDetails(TblTenderDetails tblObj);

    public void deleteTblTenderDetails(TblTenderDetails tblObj);

    public void updateTblTenderDetails(TblTenderDetails tblObj);

    public List<TblTenderDetails> getAllTblTenderDetails();

    public List<TblTenderDetails> findTblTenderDetails(Object... values) throws Exception;

    public List<TblTenderDetails> findByCountTblTenderDetails(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderDetailsCount();
}
