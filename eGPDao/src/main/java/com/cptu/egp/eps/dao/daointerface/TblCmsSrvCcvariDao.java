package com.cptu.egp.eps.dao.daointerface;


import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvCcvari;
import java.util.List;


	public interface TblCmsSrvCcvariDao extends GenericDao<TblCmsSrvCcvari> {

	public void addTblCmsSrvCcvari(TblCmsSrvCcvari tblObj);

	public void deleteTblCmsSrvCcvari(TblCmsSrvCcvari tblObj);

	public void updateTblCmsSrvCcvari(TblCmsSrvCcvari tblObj);

	public List<TblCmsSrvCcvari> getAllTblCmsSrvCcvari();

	public List<TblCmsSrvCcvari> findTblCmsSrvCcvari(Object... values) throws Exception;

	public List<TblCmsSrvCcvari> findByCountTblCmsSrvCcvari(int firstResult,int maxResult,Object... values) throws Exception;

	public long getTblCmsSrvCcvariCount();

	public void updateOrSaveTblCmsSrvCcvari(List<TblCmsSrvCcvari> tblObj);
}