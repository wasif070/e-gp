package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderOpenExt;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderOpenExtDao extends GenericDao<TblTenderOpenExt> {

    public void addTblTenderOpenExt(TblTenderOpenExt tblTenderOpenExt);

    public void deleteTblTenderOpenExt(TblTenderOpenExt tblTenderOpenExt);

    public void deleteAllTblTenderOpenExt(Collection tblTenderOpenExt);

    public void updateTblTenderOpenExt(TblTenderOpenExt tblTenderOpenExt);

    public List<TblTenderOpenExt> getAllTblTenderOpenExt();

    public List<TblTenderOpenExt> findTblTenderOpenExt(Object... values) throws Exception;

    public List<TblTenderOpenExt> findByCountTblTenderOpenExt(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderOpenExtCount();
}
