package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import java.util.List;
import com.cptu.egp.eps.model.table.TblComplaintLevel;

public interface TblComplaintLevelDao extends GenericDao<TblComplaintLevel> {

    public void addTblComplaintLevel(TblComplaintLevel complaintLevel);

   // public void deleteTblComplaintLevel(TblComplaintLevel complaintLevel) throws DataAccessException;

   // public void updateTblComplaintLevel(TblComplaintLevel complaintLevel);

   // public List<TblComplaintLevel> getAllComplaintLevels();

    public List<TblComplaintLevel> findByLevel(Object... values) throws Exception;

   // public List<TblComplaintLevel> findByCountComplaintLevels(int firstResult,int maxResult,Object... values) throws Exception;

   // public long getTblComplaintLevelCount();
}
