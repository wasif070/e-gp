package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCancelTenderRequest;
import java.util.List;

/**
 *
 * @author rishita
 */
public interface TblCancelTenderRequestDao extends GenericDao<TblCancelTenderRequest> {

    public void addTblCancelTenderRequest(TblCancelTenderRequest tblObj);

    public void deleteTblCancelTenderRequest(TblCancelTenderRequest tblObj);

    public void updateTblCancelTenderRequest(TblCancelTenderRequest tblObj);

    public List<TblCancelTenderRequest> getAllTblCancelTenderRequest();

    public List<TblCancelTenderRequest> findTblCancelTenderRequest(Object... values) throws Exception;

    public List<TblCancelTenderRequest> findByCountTblCancelTenderRequest(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCancelTenderRequestCount();
}
