/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblActivityMasterDao;
import com.cptu.egp.eps.dao.daointerface.TblFaqBhutanDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblActivityMaster;
import com.cptu.egp.eps.model.table.TblFaqBhutan;
import java.util.List;

/**
 *
 * @author Nitish Oritro
 */
public class TblFaqBhutanImpl extends AbcAbstractClass<TblFaqBhutan> implements TblFaqBhutanDao
{

    @Override
    public void addTblFaqBhutan(TblFaqBhutan faqBhutan){
        super.addEntity(faqBhutan);
    }

    @Override
    public void deleteTblFaqBhutan(TblFaqBhutan faqBhutan) {
        super.deleteEntity(faqBhutan);
    }

    @Override
    public void updateTblFaqBhutan(TblFaqBhutan faqBhutan) {
        super.updateEntity(faqBhutan);
    }

    @Override
    public List<TblFaqBhutan> getAllTblFaqBhutan() {
        return super.getAllEntity();
    }

    @Override
    public List<TblFaqBhutan> findTblFaqBhutan(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblFaqBhutanCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblFaqBhutan> findByCountTblFaqBhutan(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

}
