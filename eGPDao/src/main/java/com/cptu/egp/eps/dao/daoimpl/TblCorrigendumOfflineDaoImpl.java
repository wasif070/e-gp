/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCorrigendumDetailOffline;
import com.cptu.egp.eps.dao.daointerface.TblCorrigendumOfflineDao;
import java.util.List;

/**
 *
 * @author salahuddin
 */

public class TblCorrigendumOfflineDaoImpl extends AbcAbstractClass<TblCorrigendumDetailOffline> implements TblCorrigendumOfflineDao {

    @Override
    public void addTblCorrigendumOfflineDetail(List<TblCorrigendumDetailOffline> tblCorrigendumDetailOffline) {
        
        super.updateAll(tblCorrigendumDetailOffline);
    }

    @Override
    public List<TblCorrigendumDetailOffline> findTblCorrigendumDetailOffline(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }
    

    /*public List<Object[]> getCurrencyTenderwise(int tenderId, int userId) {

        List<Object[]> listCurrency = new ArrayList<Object[]>();
        try{
            String sqlQuery = "select tc.currencyName, ttc.exchangeRate "
                               + "from TblTenderCurrency ttc, TblCurrencyMaster tc "
                             + "where ttc.currencyId = tc.currencyId and ttc.tenderId = "+tenderId+" and createdBy = "+userId
                             + "order by tc.currencyId asc";

            listCurrency = hibernateQueryDao.createQuery(sqlQuery);

        }catch(Exception ex){
              logger.debug("getCurrencyTenderwise : " +ex);
        }

        return listCurrency;
    }*/

}
