package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalSerFormDetail;
import java.util.List;

/**
 *
 * @author rishita
 */
public interface TblEvalSerFormDetailDao extends GenericDao<TblEvalSerFormDetail> {

    public void addTblEvalSerFormDetail(TblEvalSerFormDetail tblObj);

    public void deleteTblEvalSerFormDetail(TblEvalSerFormDetail tblObj);

    public void updateTblEvalSerFormDetail(TblEvalSerFormDetail tblObj);

    public List<TblEvalSerFormDetail> getAllTblEvalSerFormDetail();

    public List<TblEvalSerFormDetail> findTblEvalSerFormDetail(Object... values) throws Exception;

    public List<TblEvalSerFormDetail> findByCountTblEvalSerFormDetail(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalSerFormDetailCount();

    public void updateInsertAllEvalSerFormDetail(List<TblEvalSerFormDetail> list);
}
