/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderBidPlainDataDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderBidPlainData;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderBidPlainDataImpl extends AbcAbstractClass<TblTenderBidPlainData> implements TblTenderBidPlainDataDao {

    @Override
    public void addTblTenderBidPlainData(TblTenderBidPlainData tenderBidPlainData) {
        super.addEntity(tenderBidPlainData);
    }

    @Override
    public void deleteTblTenderBidPlainData(TblTenderBidPlainData tenderBidPlainData) {
        super.deleteEntity(tenderBidPlainData);
    }

    @Override
    public void updateTblTenderBidPlainData(TblTenderBidPlainData tenderBidPlainData) {
        super.updateEntity(tenderBidPlainData);
    }

    @Override
    public List<TblTenderBidPlainData> getAllTblTenderBidPlainData() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderBidPlainData> findTblTenderBidPlainData(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderBidPlainDataCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderBidPlainData> findByCountTblTenderBidPlainData(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
