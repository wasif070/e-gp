package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvWphistoryDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvWphistory;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsSrvWphistoryImpl extends AbcAbstractClass<TblCmsSrvWphistory> implements TblCmsSrvWphistoryDao {

    @Override
    public void addTblCmsSrvWphistory(TblCmsSrvWphistory cmsWpDetail){
        super.addEntity(cmsWpDetail);
    }

    @Override
    public void deleteTblCmsSrvWphistory(TblCmsSrvWphistory cmsWpDetail) {
        super.deleteEntity(cmsWpDetail);
    }

    @Override
    public void updateTblCmsSrvWphistory(TblCmsSrvWphistory cmsWpDetail) {
        super.updateEntity(cmsWpDetail);
    }

    @Override
    public List<TblCmsSrvWphistory> getAllTblCmsSrvWphistory() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvWphistory> findTblCmsSrvWphistory(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvWphistoryCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvWphistory> findByCountTblCmsSrvWphistory(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsSrvWphistory> estCost) {
       super.updateAll(estCost);
    }
}
