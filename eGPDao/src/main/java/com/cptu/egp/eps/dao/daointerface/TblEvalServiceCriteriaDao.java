/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalServiceCriteria;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TblEvalServiceCriteriaDao extends GenericDao<TblEvalServiceCriteria>{


    public void addTblEvalServiceCriteria(TblEvalServiceCriteria tblObj);

    public void deleteTblEvalServiceCriteria(TblEvalServiceCriteria tblObj);

    public void updateTblEvalServiceCriteria(TblEvalServiceCriteria tblObj);

    public List<TblEvalServiceCriteria> getAllTblEvalServiceCriteria();

    public List<TblEvalServiceCriteria> findTblEvalServiceCriteria(Object... values) throws Exception;

    public List<TblEvalServiceCriteria> findByCountTblEvalServiceCriteria(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalServiceCriteriaCount();
    public void updateOrInsertAll(List<TblEvalServiceCriteria> list);

}
