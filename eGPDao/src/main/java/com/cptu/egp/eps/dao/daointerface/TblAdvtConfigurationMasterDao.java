/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.model.table.TblAdvtConfigurationMaster;
import java.util.List;

/**
 *
 * @author spandana.vaddi
 */
public interface TblAdvtConfigurationMasterDao {

    public void addTblAdvtConfigurationMaster(TblAdvtConfigurationMaster tblAdvtConfigurationMaster);

    public void deleteTblAdvtConfigurationMaster(TblAdvtConfigurationMaster adConfigId);

    public void updateTblAdvtConfigurationMaster(TblAdvtConfigurationMaster tblAdvtConfigurationMaster);

    public List<TblAdvtConfigurationMaster> getAllTblAdvtConfigurationMaster();

    public List<TblAdvtConfigurationMaster> findTblAdvtConfigurationMaster(Object... values) throws Exception;

    public List<TblAdvtConfigurationMaster> findByCountTblAdvtConfigurationMaster(int firstResult, int maxResult, Object... values) throws Exception;
    //public long getTblConfigurationMasterCount();
}
