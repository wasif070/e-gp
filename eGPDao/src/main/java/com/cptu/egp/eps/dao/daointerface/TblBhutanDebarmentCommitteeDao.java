/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblBhutanDebarmentCommittee;
import java.util.List;

/**
 *
 * @author feroz
 */
public interface TblBhutanDebarmentCommitteeDao extends GenericDao<TblBhutanDebarmentCommittee>{
    
    public void addTblBhutanDebarmentCommittee(TblBhutanDebarmentCommittee tblBhutanDebarmentCommittee);

    public void deleteTblBhutanDebarmentCommittee(TblBhutanDebarmentCommittee tblBhutanDebarmentCommittee);

    public void updateTblBhutanDebarmentCommittee(TblBhutanDebarmentCommittee tblBhutanDebarmentCommittee);

    public List<TblBhutanDebarmentCommittee> getAllTblBhutanDebarmentCommittee();
   
    public List<TblBhutanDebarmentCommittee> findTblBhutanDebarmentCommittee(Object... values);
    
}
