/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblEmailLogDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEmailLog;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class TblEmailLogImpl extends AbcAbstractClass<TblEmailLog> implements TblEmailLogDao {

    @Override
    public void addTblEmailLog(TblEmailLog tblEmailLog){
        super.addEntity(tblEmailLog);
    }

    @Override
    public void deleteTblEmailLog(TblEmailLog tblEmailLog) {
        super.deleteEntity(tblEmailLog);
    }

    @Override
    public void updateTblEmailLog(TblEmailLog tblEmailLog) {
        super.updateEntity(tblEmailLog);
    }

    @Override
    public List<TblEmailLog> getAllTblEmailLog() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEmailLog> findTblEmailLog(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEmailLogCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEmailLog> findByCountTblEmailLog(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
