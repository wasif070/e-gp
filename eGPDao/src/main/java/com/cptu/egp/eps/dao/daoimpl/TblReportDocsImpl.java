/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;


import com.cptu.egp.eps.dao.daointerface.TblReportDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblReportDocs;
import java.util.List;


public class TblReportDocsImpl extends AbcAbstractClass<TblReportDocs> implements TblReportDocsDao {

    @Override
    public void addTblReportDocs(TblReportDocs master){
            super.addEntity(master);
    }
    @Override
    public void deleteTblReportDocs(TblReportDocs master) {
            super.deleteEntity(master);
    }
    @Override
    public void updateTblReportDocs(TblReportDocs master) {
            super.updateEntity(master);
    }
    @Override
    public List<TblReportDocs> getAllTblReportDocs() {
            return super.getAllEntity();
    }
    @Override
    public List<TblReportDocs> findTblReportDocs(Object... values) throws Exception {
            return super.findEntity(values);
    }
    @Override
    public long getTblReportDocsCount() {
            return super.getEntityCount();
    }
    @Override
    public List<TblReportDocs> findByCountTblReportDocs(int firstResult, int maxResult, Object... values) throws Exception {
            return super.findByCountEntity(firstResult, maxResult, values);
    }
    @Override
    public void updateOrSaveTblReportDocs(List<TblReportDocs> tblObj) {
            super.updateAll(tblObj);
    }
}


