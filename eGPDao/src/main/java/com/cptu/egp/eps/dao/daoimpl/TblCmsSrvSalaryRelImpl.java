package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvSalaryReDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvSalaryRe;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsSrvSalaryRelImpl extends AbcAbstractClass<TblCmsSrvSalaryRe> implements TblCmsSrvSalaryReDao {

    @Override
    public void addTblCmsSrvSalaryRe(TblCmsSrvSalaryRe cmsWpDetail){
        super.addEntity(cmsWpDetail);
    }

    @Override
    public void deleteTblCmsSrvSalaryRe(TblCmsSrvSalaryRe cmsWpDetail) {
        super.deleteEntity(cmsWpDetail);
    }

    @Override
    public void updateTblCmsSrvSalaryRe(TblCmsSrvSalaryRe cmsWpDetail) {
        super.updateEntity(cmsWpDetail);
    }

    @Override
    public List<TblCmsSrvSalaryRe> getAllTblCmsSrvSalaryRe() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvSalaryRe> findTblCmsSrvSalaryRe(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvSalaryReCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvSalaryRe> findByCountTblCmsSrvSalaryRe(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsSrvSalaryRe> estCost) {
       super.updateAll(estCost);
    }
}
