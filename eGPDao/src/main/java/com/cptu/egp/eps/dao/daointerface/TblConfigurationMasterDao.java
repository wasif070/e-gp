/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TblConfigurationMasterDao {



    public void addTblConfigurationMaster(TblConfigurationMaster tblObj);

    public void deleteTblConfigurationMaster(TblConfigurationMaster tblObj);

    public void updateTblConfigurationMaster(TblConfigurationMaster tblObj);

    public List<TblConfigurationMaster> getAllTblConfigurationMaster();

    public List<TblConfigurationMaster> findTblConfigurationMaster(Object... values) throws Exception;

    public List<TblConfigurationMaster> findByCountTblConfigurationMaster(int firstResult,int maxResult,Object... values) throws Exception;

    //public long getTblConfigurationMasterCount();

}
