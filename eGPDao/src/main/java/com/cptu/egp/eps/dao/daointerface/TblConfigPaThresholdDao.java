/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblConfigPaThreshold;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author feroz
 */
public interface TblConfigPaThresholdDao extends GenericDao<TblConfigPaThreshold> {

    public void addTblConfigPaThreshold(TblConfigPaThreshold tblConfigPaThreshold);

    public void deleteTblConfigPaThreshold(TblConfigPaThreshold tblConfigPaThreshold);

    public void updateTblConfigPaThreshold(TblConfigPaThreshold tblConfigPaThreshold);

    public List<TblConfigPaThreshold> getAllTblConfigPaThreshold();
    
    /**
     *
     * @param values
     * @return
     */
    public List<TblConfigPaThreshold> findTblConfigPaThreshold(Object... values);

}

