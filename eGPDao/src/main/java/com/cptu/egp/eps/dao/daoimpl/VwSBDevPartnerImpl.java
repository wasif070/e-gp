package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.VwSBDevPartnerDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.view.VwGetSbDevPartner;
import java.util.List;

/**
 *
 * @author taher
 */
public class VwSBDevPartnerImpl extends AbcAbstractClass<VwGetSbDevPartner> implements VwSBDevPartnerDao {

    @Override
    public List<VwGetSbDevPartner> getAllVwSBDevPartner() {
        return super.getAllEntity();
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<VwGetSbDevPartner> findVwSBDevPartner(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<VwGetSbDevPartner> findByCountVwSBDevPartner(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

}
