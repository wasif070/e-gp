/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblNegBidForm;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public interface TblNegBidFormDao extends GenericDao<TblNegBidForm> {

    public void addTblNegBidForm(TblNegBidForm tblNegBidForm);

    public void deleteTblNegBidForm(TblNegBidForm tblNegBidForm);

    public void updateTblNegBidForm(TblNegBidForm tblNegBidForm);

    public List<TblNegBidForm> getAllTblNegBidForm();

    public List<TblNegBidForm> findTblNegBidForm(Object... values) throws Exception;

    public List<TblNegBidForm> findByCountTblNegBidForm(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblNegBidFormCount();
}
