package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblConfigEvalMethod;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblConfigEvalMethodDao extends GenericDao<TblConfigEvalMethod> {

    public void addTblConfigEvalMethod(TblConfigEvalMethod tblObj);

    public void deleteTblConfigEvalMethod(TblConfigEvalMethod tblObj);

    public void updateTblConfigEvalMethod(TblConfigEvalMethod tblObj);

    public List<TblConfigEvalMethod> getAllTblConfigEvalMethod();

    public List<TblConfigEvalMethod> findTblConfigEvalMethod(Object... values) throws Exception;

    public List<TblConfigEvalMethod> findByCountTblConfigEvalMethod(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblConfigEvalMethodCount();

   public void updateInsertAllConfigEvalMethod(List<TblConfigEvalMethod> list);
}
