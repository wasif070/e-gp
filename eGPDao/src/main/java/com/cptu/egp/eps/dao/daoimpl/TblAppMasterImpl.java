package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblAppMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblAppMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblAppMasterImpl extends AbcAbstractClass<TblAppMaster> implements TblAppMasterDao {

    @Override
    public void addTblAppMaster(TblAppMaster master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblAppMaster(TblAppMaster master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblAppMaster(TblAppMaster master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblAppMaster> getAllTblAppMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblAppMaster> findTblAppMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblAppMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblAppMaster> findByCountTblAppMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
