package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblMessageFolderDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblMessageFolder;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblMessageFolderImpl extends AbcAbstractClass<TblMessageFolder> implements TblMessageFolderDao {

    @Override
    public void addTblMessageFolder(TblMessageFolder master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblMessageFolder(TblMessageFolder master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblMessageFolder(TblMessageFolder master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblMessageFolder> getAllTblMessageFolder() {
        return super.getAllEntity();
    }

    @Override
    public List<TblMessageFolder> findTblMessageFolder(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblMessageFolderCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblMessageFolder> findByCountTblMessageFolder(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
