/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.math.BigDecimal;
import java.util.List;
import java.util.Date;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author SRISTY
 */
public class SPgetTendererDashboardInfo extends StoredProcedure
{
private static final Logger LOGGER = Logger.getLogger(SPgetTenderInfo.class);
    public SPgetTendererDashboardInfo(BasicDataSource dataSource, String procName){

       this.setDataSource(dataSource);
       this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
       LOGGER.debug("Proc Name: "+ procName);

       this.declareParameter(new SqlParameter("v_fieldName1Vc", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_fieldName2Vc", Types.VARCHAR));
       //this.declareParameter(new SqlParameter("v_TenderId_Int", Types.INTEGER));
       //this.declareParameter(new SqlParameter("v_AppId_inInt", Types.INTEGER));

       this.declareParameter(new SqlParameter("v_fieldName3Vc", Types.INTEGER));
       //this.declareParameter(new SqlParameter("v_procurementNatureId_inInt", Types.INTEGER));
       //this.declareParameter(new SqlParameter("v_procurementType_inVc", Types.VARCHAR));
       //this.declareParameter(new SqlParameter("v_procurementMethod_inInt", Types.INTEGER));
       //this.declareParameter(new SqlParameter("v_reoiRfpRefNo_inVc", Types.VARCHAR));
       //this.declareParameter(new SqlParameter("v_tenderPubDtFrom_inVc", Types.VARCHAR));
       //this.declareParameter(new SqlParameter("v_tenderPubDtTo_inVc", Types.VARCHAR));
       //this.declareParameter(new SqlParameter("v_Status_inVc", Types.VARCHAR));

       //this.declareParameter(new SqlParameter("v_Ministry_inVc", Types.VARCHAR));
       //this.declareParameter(new SqlParameter("v_Division_inVc", Types.VARCHAR));
       //this.declareParameter(new SqlParameter("v_Agency_inVc", Types.VARCHAR));
       //this.declareParameter(new SqlParameter("v_PeOfficeName", Types.VARCHAR));
       //this.declareParameter(new SqlParameter("v_SubmissionDtFrom_inVc", Types.VARCHAR));
       //this.declareParameter(new SqlParameter("v_SubmissionDtTo_inVc", Types.VARCHAR));
       //this.declareParameter(new SqlParameter("v_Page_inInt", Types.INTEGER));
       //this.declareParameter(new SqlParameter("v_RecordPerPage_inInt", Types.INTEGER));
    }

    /**
     * Execute stored procedure : p_get_evalcommondata
     * Get Tender Information
     * @param actioninVc
     * @param viewTypeinVc
     * @param tenderIdInt
     * @param appIdinInt
     * @param userIdinInt
     * @param procurementNatureIdinInt
     * @param procurementTypeinVc
     * @param procurementMethodinInt
     * @param reoiRfpRefNoinVc
     * @param tenderPubDtFrominVc
     * @param tenderPubDtToinVc
     * @param statusinVc
     * @param ministryinVc
     * @param divisioninVc
     * @param agencyinVc
     * @param peOfficeName
     * @param submissionDtFrominVc
     * @param submissionDtToinVc
     * @param pageinInt
     * @param recordPerPageinInt
     * @return Get tender information as list of CommonTenderDetails object
     */
    public List<CommonTenderDetails> executeProcedure(String actioninVc,
                                                      String viewTypeinVc,
                                                      Integer userIdinInt){
                                                      //Integer tenderIdInt,
                                                      //Integer appIdinInt,
                                                      //Integer userIdinInt,
                                                     // Integer procurementNatureIdinInt,
                                                      //String procurementTypeinVc,
                                                      //Integer procurementMethodinInt,
                                                     // String reoiRfpRefNoinVc,
                                                     // String tenderPubDtFrominVc,
                                                     // String tenderPubDtToinVc,
                                                     // String statusinVc,
                                                      //String ministryinVc,
                                                      //String divisioninVc,
                                                      //String agencyinVc,
                                                      //String peOfficeName,
                                                      //String submissionDtFrominVc,
                                                      //String submissionDtToinVc,
                                                      //Integer pageinInt,
                                                      //Integer recordPerPageinInt){
        Map inParams = new HashMap();

        inParams.put("v_fieldName1Vc", actioninVc);
        inParams.put("v_fieldName2Vc", viewTypeinVc);
        //inParams.put("v_TenderId_Int", tenderIdInt);
        //inParams.put("v_AppId_inInt", appIdinInt);

        inParams.put("v_fieldName3Vc", userIdinInt);
        //inParams.put("v_procurementNatureId_inInt", procurementNatureIdinInt);
        //inParams.put("v_procurementType_inVc", procurementTypeinVc);
        //inParams.put("v_procurementMethod_inInt", procurementMethodinInt);
        //inParams.put("v_reoiRfpRefNo_inVc", reoiRfpRefNoinVc);
        //inParams.put("v_tenderPubDtFrom_inVc", tenderPubDtFrominVc);
        //inParams.put("v_tenderPubDtTo_inVc", tenderPubDtToinVc);
        //inParams.put("v_Status_inVc", statusinVc);

        //inParams.put("v_Ministry_inVc", ministryinVc);
        //inParams.put("v_Division_inVc", divisioninVc);
        //inParams.put("v_Agency_inVc", agencyinVc);
        //inParams.put("v_PeOfficeName", peOfficeName);
        //inParams.put("v_SubmissionDtFrom_inVc", submissionDtFrominVc);
        //inParams.put("v_SubmissionDtTo_inVc", submissionDtToinVc);
        //inParams.put("v_Page_inInt", pageinInt);
        //inParams.put("v_RecordPerPage_inInt", recordPerPageinInt);

         this.compile();
         List<CommonTenderDetails> details = new ArrayList<CommonTenderDetails>();
         try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            if (list != null && !list.isEmpty()) {
                for (LinkedHashMap<String, Object> linkedHashMap : list) {
                    CommonTenderDetails commonTenderDetails=new CommonTenderDetails();
                    commonTenderDetails.setTenderPhasingId((Integer)linkedHashMap.get("tenderPhasingId"));
                    commonTenderDetails.setTenderDtlId((Integer)linkedHashMap.get("tenderDtlId"));
                    commonTenderDetails.setDepartmentId((Integer)linkedHashMap.get("departmentId"));
                    commonTenderDetails.setPqTenderId((Integer)linkedHashMap.get("pqTenderId"));
                    commonTenderDetails.setTenderId((Integer)linkedHashMap.get("tenderId"));
                    commonTenderDetails.setMinistry((String)linkedHashMap.get("ministry"));
                    commonTenderDetails.setDivision((String)linkedHashMap.get("division"));
                    commonTenderDetails.setAgency((String)linkedHashMap.get("agency"));
                    commonTenderDetails.setPeOfficeName((String)linkedHashMap.get("peOfficeName"));
                    commonTenderDetails.setPeCode((String)linkedHashMap.get("peCode"));
                    commonTenderDetails.setPeDistrict((String)linkedHashMap.get("peDistrict"));
                    commonTenderDetails.setEventType((String)linkedHashMap.get("eventType"));
                    commonTenderDetails.setInvitationFor((String)linkedHashMap.get("invitationFor"));
                    commonTenderDetails.setReoiRfpFor((String)linkedHashMap.get("reoiRfpFor"));

                commonTenderDetails.setContractType((String)linkedHashMap.get("contractType"));
                commonTenderDetails.setReoiRfpRefNo((String)linkedHashMap.get("reoiRfpRefNo"));
                commonTenderDetails.setProcurementMethod((String)linkedHashMap.get("procurementMethod"));
                commonTenderDetails.setBudgetType((String)linkedHashMap.get("budgetType"));
                commonTenderDetails.setSourceOfFund((String)linkedHashMap.get("sourceOfFund"));
                commonTenderDetails.setDevPartners((String)linkedHashMap.get("devPartners"));
                commonTenderDetails.setProjectCode((String)linkedHashMap.get("projectCode"));
                commonTenderDetails.setProjectName((String)linkedHashMap.get("projectName"));
                commonTenderDetails.setPackageNo((String)linkedHashMap.get("packageNo"));
                commonTenderDetails.setPackageDescription((String)linkedHashMap.get("packageDescription"));
                commonTenderDetails.setCpvCode((String)linkedHashMap.get("cpvCode"));
                commonTenderDetails.setDocEndDate((Date)linkedHashMap.get("docEndDate"));

                commonTenderDetails.setPreBidStartDt((Date)linkedHashMap.get("preBidStartDt"));
                commonTenderDetails.setPreBidEndDt((Date)linkedHashMap.get("preBidEndDt"));
                commonTenderDetails.setSubmissionDt((Date)linkedHashMap.get("submissionDt"));
                commonTenderDetails.setOpeningDt((Date)linkedHashMap.get("openingDt"));
                commonTenderDetails.setEligibilityCriteria((String)linkedHashMap.get("eligibilityCriteria"));
                commonTenderDetails.setTenderBrief((String)linkedHashMap.get("tenderBrief"));
                commonTenderDetails.setDeliverables((String)linkedHashMap.get("deliverables"));
                commonTenderDetails.setOtherDetails((String)linkedHashMap.get("otherDetails"));
                commonTenderDetails.setForeignFirm((String)linkedHashMap.get("foreignFirm"));
                commonTenderDetails.setDocAvlMethod((String)linkedHashMap.get("docAvlMethod"));
                commonTenderDetails.setEvalType((String)linkedHashMap.get("evalType"));
                commonTenderDetails.setDocFeesMethod((String)linkedHashMap.get("docFeesMethod"));
                commonTenderDetails.setDocFeesMode((String)linkedHashMap.get("docFeesMode"));
                commonTenderDetails.setDocOfficeAdd((String)linkedHashMap.get("docOfficeAdd"));

                commonTenderDetails.setSecurityLastDt((Date)linkedHashMap.get("securityLastDt"));
                commonTenderDetails.setSecuritySubOff((String)linkedHashMap.get("securitySubOff"));
                commonTenderDetails.setProcurementNature((String)linkedHashMap.get("procurementNature"));
                commonTenderDetails.setProcurementType((String)linkedHashMap.get("procurementType"));
                commonTenderDetails.setModeOfTender((String)linkedHashMap.get("modeOfTender"));
                commonTenderDetails.setReTenderId((Integer)linkedHashMap.get("reTenderId"));
                commonTenderDetails.setPqTenderId((Integer)linkedHashMap.get("pqTenderId"));
                commonTenderDetails.setReoiTenderId((Integer)linkedHashMap.get("reoiTenderId"));
                commonTenderDetails.setTenderStatus((String)linkedHashMap.get("tenderStatus"));
                commonTenderDetails.setTenderPubDt((Date)linkedHashMap.get("tenderPubDt"));
                commonTenderDetails.setPeName((String)linkedHashMap.get("peName"));
                commonTenderDetails.setPeDesignation((String)linkedHashMap.get("peDesignation"));
                commonTenderDetails.setPeAddress((String)linkedHashMap.get("peAddress"));
                commonTenderDetails.setPeContactDetails((String)linkedHashMap.get("peContactDetails"));

                commonTenderDetails.setEstCost((BigDecimal)linkedHashMap.get("estCost"));
                commonTenderDetails.setApprovingAuthId((Integer)linkedHashMap.get("approvingAuthId"));
                commonTenderDetails.setStdTemplateId((Integer)linkedHashMap.get("stdTemplateId"));
                commonTenderDetails.setDocAccess((String)linkedHashMap.get("docAccess"));
                commonTenderDetails.setTenderValDays((Integer)linkedHashMap.get("tenderValDays"));
                commonTenderDetails.setTenderValidityDt((Date)linkedHashMap.get("tenderValidityDt"));
                commonTenderDetails.setTenderSecurityDays((Integer)linkedHashMap.get("tenderSecurityDays"));
                commonTenderDetails.setTenderSecurityDt((Date)linkedHashMap.get("tenderSecurityDt"));
                commonTenderDetails.setProcurementNatureId((Integer)linkedHashMap.get("procurementNatureId"));
                commonTenderDetails.setProcurementMethodId((Integer)linkedHashMap.get("procurementMethodId"));
                commonTenderDetails.setOfficeId((Integer)linkedHashMap.get("officeId"));
                commonTenderDetails.setBudgetTypeId((Integer)linkedHashMap.get("budgetTypeId"));
                commonTenderDetails.setWorkflowStatus((String)linkedHashMap.get("workflowStatus"));
                commonTenderDetails.setPkgDocFees((BigDecimal)linkedHashMap.get("pkgDocFees"));

                    commonTenderDetails.setPhasingRefNo((String)linkedHashMap.get("phasingRefNo"));
                    commonTenderDetails.setPhasingOfService((String)linkedHashMap.get("phasingOfService"));
                    commonTenderDetails.setLocation((String)linkedHashMap.get("location"));
                    commonTenderDetails.setIndStartDt((Date)linkedHashMap.get("indStartDt"));
                    commonTenderDetails.setIndEndDt((Date)linkedHashMap.get("indEndDt"));
                    commonTenderDetails.setTenderLotSecId((Integer)linkedHashMap.get("tenderLotSecId"));
                    commonTenderDetails.setLotNo((String)linkedHashMap.get("lotNo"));
                    commonTenderDetails.setLotDesc((String)linkedHashMap.get("lotDesc"));
                    commonTenderDetails.setLocationSec((String)linkedHashMap.get("locationSec"));
                    commonTenderDetails.setDocFess((BigDecimal)linkedHashMap.get("docFess"));
                    commonTenderDetails.setTenderSecurityAmt((BigDecimal)linkedHashMap.get("tenderSecurityAmt"));
                    commonTenderDetails.setCompletionTime((String)linkedHashMap.get("completionTime"));
                    commonTenderDetails.setStartTime((String)linkedHashMap.get("starttime"));
                    commonTenderDetails.setTotalRecords((Integer)linkedHashMap.get("totalRecords"));
                    commonTenderDetails.setAppPkgLotId((Integer)linkedHashMap.get("appPkgLotId"));
                    commonTenderDetails.setPassingMarks((Integer)linkedHashMap.get("passingMarks"));
                    commonTenderDetails.setTenderevalstatus((String)linkedHashMap.get("tenderevalstatus"));
                    commonTenderDetails.setTenderSecurityAmtUSD((BigDecimal)linkedHashMap.get("tenderSecurityAmtUSD"));    // For ICT Dohatec
                    commonTenderDetails.setPkgDocFeesUSD((BigDecimal)linkedHashMap.get("pkgDocFeesUSD"));    // For ICT Dohatec
                    details.add(commonTenderDetails);
                }
            }
            LOGGER.debug("SPgetTenderDashboardInfo : No data found for  actioninVc  "+actioninVc);
        } catch (Exception e) {
            LOGGER.error("SPgetTenderDashboardInfo : "+ e+ " actioninVc "+actioninVc);
        }
      return details;
    }
}
