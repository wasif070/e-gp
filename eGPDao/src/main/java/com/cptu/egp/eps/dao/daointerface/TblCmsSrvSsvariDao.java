package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvSsvari;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsSrvSsvariDao extends GenericDao<TblCmsSrvSsvari> {

    public void addTblCmsSrvSsvari(TblCmsSrvSsvari event);

    public void deleteTblCmsSrvSsvari(TblCmsSrvSsvari event);

    public void updateTblCmsSrvSsvari(TblCmsSrvSsvari event);

    public List<TblCmsSrvSsvari> getAllTblCmsSrvSsvari();

    public List<TblCmsSrvSsvari> findTblCmsSrvSsvari(Object... values) throws Exception;

    public List<TblCmsSrvSsvari> findByCountTblCmsSrvSsvari(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsSrvSsvariCount();

    public void updateOrSaveEstCost(List<TblCmsSrvSsvari> srvTcvaris);
}
