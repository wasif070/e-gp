/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblCmsAitConfigDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsAitConfig;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public class TblCmsAitConfigDaoImpl extends AbcAbstractClass<TblCmsAitConfig> implements TblCmsAitConfigDao{

    @Override
    public void addTblCmsAitConfig(TblCmsAitConfig tblCmsAitConfig) {
        super.addEntity(tblCmsAitConfig);
    }

    @Override
    public void deleteTblCmsAitConfig(TblCmsAitConfig tblCmsAitConfig) {
       super.deleteEntity(tblCmsAitConfig);
    }

    @Override
    public void updateTblCmsAitConfig(TblCmsAitConfig tblCmsAitConfig) {
        super.updateEntity(tblCmsAitConfig);
    }

    @Override
    public List<TblCmsAitConfig> getAllTblCmsAitConfig() {
        super.setPersistentClass(TblCmsAitConfig.class);
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsAitConfig> findTblCmsAitConfig(Object... values) throws Exception {
        super.setPersistentClass(TblCmsAitConfig.class);
        return super.findEntity(values);
    }

    @Override
    public List<TblCmsAitConfig> findByCountTblCmsAitConfig(int firstResult, int maxResult, Object... values) throws Exception {
        super.setPersistentClass(TblCmsAitConfig.class);
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblCmsAitConfigCount() {
        super.setPersistentClass(TblCmsAitConfig.class);
        return super.getEntityCount();
    }

}
