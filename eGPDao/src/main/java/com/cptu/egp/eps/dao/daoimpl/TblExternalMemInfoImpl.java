/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblExternalMemInfoDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblExternalMemInfo;
import java.util.List;

/**
 *
 * @author Rishita
 */
public class TblExternalMemInfoImpl extends AbcAbstractClass<TblExternalMemInfo> implements TblExternalMemInfoDao {

    @Override
    public void addTblExternalMemInfo(TblExternalMemInfo event) {
        super.addEntity(event);
    }

    @Override
    public void deleteTblExternalMemInfo(TblExternalMemInfo event) {
        super.deleteEntity(event);
    }

    @Override
    public void updateTblExternalMemInfo(TblExternalMemInfo event) {
        super.updateEntity(event);
    }

    @Override
    public List<TblExternalMemInfo> getAllTblExternalMemInfo() {
        return super.getAllEntity();
    }

    @Override
    public List<TblExternalMemInfo> findTblExternalMemInfo(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblExternalMemInfoCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblExternalMemInfo> findByCountTblExternalMemInfo(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
