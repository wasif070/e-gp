/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderGrandSum;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderGrandSumDao extends GenericDao<TblTenderGrandSum>{

    public void addTblTenderGrandSum(TblTenderGrandSum tblObj);

    public void deleteTblTenderGrandSum(TblTenderGrandSum tblObj);

    public void updateTblTenderGrandSum(TblTenderGrandSum tblObj);

    public List<TblTenderGrandSum> getAllTblTenderGrandSum();

    public List<TblTenderGrandSum> findTblTenderGrandSum(Object... values) throws Exception;

    public List<TblTenderGrandSum> findByCountTblTenderGrandSum(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderGrandSumCount();
}
