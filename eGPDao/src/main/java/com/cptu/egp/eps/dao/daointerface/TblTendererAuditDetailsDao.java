package com.cptu.egp.eps.dao.daointerface;


import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTendererAuditDetails;
import java.util.List;


	public interface TblTendererAuditDetailsDao extends GenericDao<TblTendererAuditDetails> {

	public void addTblTendererAuditDetails(TblTendererAuditDetails tblObj);

	public void deleteTblTendererAuditDetails(TblTendererAuditDetails tblObj);

	public void updateTblTendererAuditDetails(TblTendererAuditDetails tblObj);

	public List<TblTendererAuditDetails> getAllTblTendererAuditDetails();

	public List<TblTendererAuditDetails> findTblTendererAuditDetails(Object... values) throws Exception;

	public List<TblTendererAuditDetails> findByCountTblTendererAuditDetails(int firstResult,int maxResult,Object... values) throws Exception;

	public long getTblTendererAuditDetailsCount();

	public void updateOrSaveTblTendererAuditDetails(List<TblTendererAuditDetails> tblObj);
}