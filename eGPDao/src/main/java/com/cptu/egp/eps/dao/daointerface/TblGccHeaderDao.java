package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblGccHeader;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblGccHeaderDao extends GenericDao<TblGccHeader> {

    public void addTblGccHeader(TblGccHeader tblObj);

    public void deleteTblGccHeader(TblGccHeader tblObj);

    public void updateTblGccHeader(TblGccHeader tblObj);

    public List<TblGccHeader> getAllTblGccHeader();

    public List<TblGccHeader> findTblGccHeader(Object... values) throws Exception;

    public List<TblGccHeader> findByCountTblGccHeader(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblGccHeaderCount();
}
