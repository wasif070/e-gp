/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.dao.daointerface.TblTenderRightsDao;
import com.cptu.egp.eps.model.table.TblTenderRights;
import java.util.List;

/**
 *
 * @author Ketan
 */
public class TblTenderRightsDaoImpl extends AbcAbstractClass<TblTenderRights> implements TblTenderRightsDao {

    @Override
    public List<Object[]> createNewQuery(String query) {
        return super.createQuery(query);
    }    

    @Override
    public List<Object[]> createByCountNewQuery(String query, int firstResult, int maxResult) {
        return super.createByCountQuery(query, firstResult, maxResult);
    }

    @Override
    public long countForNewQuery(String from, String where) throws Exception {
        return super.countForQuery(from, where);
    }

    @Override
    public List<Object> getSingleColQuery(String query) {
        return super.singleColQuery(query);
    }

    @Override
    public int updateDeleteNewQuery(String query) {
        return super.updateDeleteQuery(query);
    }

    @Override
    public int updateDeleteSQLNewQuery(String query){
        return super.updateDeleteSQLQuery(query);
    }
}
