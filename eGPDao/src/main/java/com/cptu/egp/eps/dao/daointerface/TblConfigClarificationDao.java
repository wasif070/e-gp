package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblConfigClarification;
import java.util.List;

/**
 *
 * @author MD. Emtazul Haque
 */
public interface TblConfigClarificationDao extends GenericDao<TblConfigClarification> {

    public void addTblConfigClarification(TblConfigClarification tblObj);

    public void deleteTblConfigClarification(TblConfigClarification tblObj);

    public void updateTblConfigClarification(TblConfigClarification tblObj);

    public List<TblConfigClarification> getAllTblConfigClarification();

    public List<TblConfigClarification> findTblConfigClarification(Object... values) throws Exception;

    public List<TblConfigClarification> findByCountTblConfigClarification(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblConfigClarificationCount();

   public void updateInsertAllConfigClarification(List<TblConfigClarification> list);
}
