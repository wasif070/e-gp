package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTdsSubClause;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTdsSubClauseDao extends GenericDao<TblTdsSubClause> {

    public void addTblTdsSubClause(TblTdsSubClause tblObj);

    public void deleteTblTdsSubClause(TblTdsSubClause tblObj);

    public void updateTblTdsSubClause(TblTdsSubClause tblObj);

    public List<TblTdsSubClause> getAllTblTdsSubClause();

    public List<TblTdsSubClause> findTblTdsSubClause(Object... values) throws Exception;

    public List<TblTdsSubClause> findByCountTblTdsSubClause(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTdsSubClauseCount();
}
