package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblBidDeclaration;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblBidDeclarationDao extends GenericDao<TblBidDeclaration> {

    public void addTblBidDeclaration(TblBidDeclaration tblObj);

    public void deleteTblBidDeclaration(TblBidDeclaration tblObj);

    public void updateTblBidDeclaration(TblBidDeclaration tblObj);

    public List<TblBidDeclaration> getAllTblBidDeclaration();

    public List<TblBidDeclaration> findTblBidDeclaration(Object... values) throws Exception;

    public List<TblBidDeclaration> findByCountTblBidDeclaration(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblBidDeclarationCount();
}
