package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblMandatoryDoc;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblMandatoryDocDao extends GenericDao<TblMandatoryDoc> {

    public void addTblMandatoryDoc(TblMandatoryDoc tblObj);

    public void deleteTblMandatoryDoc(TblMandatoryDoc tblObj);

    public void updateTblMandatoryDoc(TblMandatoryDoc tblObj);

    public List<TblMandatoryDoc> getAllTblMandatoryDoc();

    public List<TblMandatoryDoc> findTblMandatoryDoc(Object... values) throws Exception;

    public List<TblMandatoryDoc> findByCountTblMandatoryDoc(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblMandatoryDocCount();
}
