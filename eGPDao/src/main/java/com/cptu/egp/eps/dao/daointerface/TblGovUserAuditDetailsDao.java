package com.cptu.egp.eps.dao.daointerface;


import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblGovUserAuditDetails;
import java.util.List;


	public interface TblGovUserAuditDetailsDao extends GenericDao<TblGovUserAuditDetails> {

	public void addTblGovUserAuditDetails(TblGovUserAuditDetails tblObj);

	public void deleteTblGovUserAuditDetails(TblGovUserAuditDetails tblObj);

	public void updateTblGovUserAuditDetails(TblGovUserAuditDetails tblObj);

	public List<TblGovUserAuditDetails> getAllTblGovUserAuditDetails();

	public List<TblGovUserAuditDetails> findTblGovUserAuditDetails(Object... values) throws Exception;

	public List<TblGovUserAuditDetails> findByCountTblGovUserAuditDetails(int firstResult,int maxResult,Object... values) throws Exception;

	public long getTblGovUserAuditDetailsCount();

	public void updateOrSaveTblGovUserAuditDetails(List<TblGovUserAuditDetails> tblObj);
}