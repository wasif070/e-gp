package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEmployeeEsignature;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblEmployeeEsignatureDao extends GenericDao<TblEmployeeEsignature> {

    public void addTblEmployeeEsignature(TblEmployeeEsignature tblObj);

    public void deleteTblEmployeeEsignature(TblEmployeeEsignature tblObj);

    public void updateTblEmployeeEsignature(TblEmployeeEsignature tblObj);

    public List<TblEmployeeEsignature> getAllTblEmployeeEsignature();

    public List<TblEmployeeEsignature> findTblEmployeeEsignature(Object... values) throws Exception;

    public List<TblEmployeeEsignature> findByCountTblEmployeeEsignature(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEmployeeEsignatureCount();
}
