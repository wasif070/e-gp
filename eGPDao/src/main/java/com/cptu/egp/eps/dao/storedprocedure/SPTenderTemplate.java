/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Ramesh
 */
public class SPTenderTemplate extends StoredProcedure{
    private static final Logger LOGGER = Logger.getLogger(SPTenderTemplate.class);
    public SPTenderTemplate(BasicDataSource dataSource,String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("v_Action_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_TenderId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_TemplateId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_CreatedBy_inInt", Types.INTEGER));
        
    }

    /**
     * Execute stored procedure : p_add_upd_tendertemplate
     * Performed insert/update operation for tender template 
     * @param action
     * @param tenderId
     * @param templateId
     * @param createdBy
     * @return CommonMsgChk list contains flags indicate operation successful or not.
     */
    public List<CommonMsgChk> exeProcedure(String action,Integer tenderId,Integer templateId,Integer createdBy){
         Map inParams = new HashMap();
          inParams.put("v_Action_inVc",action);
           inParams.put("v_TenderId_inInt",tenderId);
           inParams.put("v_TemplateId_inInt",templateId);
           inParams.put("v_CreatedBy_inInt",createdBy);
          
           this.compile();
          List<CommonMsgChk> details = new ArrayList<CommonMsgChk>();
           try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            if(list!=null && !list.isEmpty()){
                for (LinkedHashMap<String, Object> linkedHashMap : list) {
                    CommonMsgChk commonMsgChk = new CommonMsgChk();
                    commonMsgChk.setFlag((Boolean) linkedHashMap.get("flag"));
                    commonMsgChk.setMsg((String) linkedHashMap.get("msg"));
                    details.add(commonMsgChk);
                }
            }
            LOGGER.debug("SPTenderTemplate  : No data found for ACTION  " + action);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            LOGGER.error("SPTenderTemplate  : "+ e+" ACTION  " + action);
        }
        return details;
    }
    
}
