/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderTypesDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderTypes;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public class TblTenderTypesImpl extends AbcAbstractClass<TblTenderTypes> implements TblTenderTypesDao {

    @Override
    public void addTblTenderTypes(TblTenderTypes admin) {

        super.addEntity(admin);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblTenderTypes(TblTenderTypes admin) {

        super.deleteEntity(admin);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblTenderTypes(TblTenderTypes admin) {

        super.updateEntity(admin);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblTenderTypes> getAllTblTenderTypes() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderTypes> findTblTenderTypes(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderTypesCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderTypes> findByCountTblTenderTypes(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
