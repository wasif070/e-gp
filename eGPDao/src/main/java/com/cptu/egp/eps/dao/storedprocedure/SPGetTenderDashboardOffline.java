/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.math.BigDecimal;
import java.util.List;
import java.util.Date;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Istiak
 */
public class SPGetTenderDashboardOffline extends StoredProcedure {

    private static final Logger LOGGER = Logger.getLogger(SPGetTenderDashboardOffline.class);

    public SPGetTenderDashboardOffline(BasicDataSource dataSource, String procName){

        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        LOGGER.debug("Proc Name: "+ procName);

        this.declareParameter(new SqlParameter("moduleFlag", Types.VARCHAR));
        this.declareParameter(new SqlParameter("searchFlag", Types.VARCHAR));
        this.declareParameter(new SqlParameter("procNature", Types.VARCHAR));
        this.declareParameter(new SqlParameter("procType", Types.VARCHAR));
        this.declareParameter(new SqlParameter("procMethod", Types.VARCHAR));
        this.declareParameter(new SqlParameter("id", Types.VARCHAR));
        this.declareParameter(new SqlParameter("status", Types.VARCHAR));
        this.declareParameter(new SqlParameter("refNo", Types.VARCHAR));
        this.declareParameter(new SqlParameter("pubDateFrom", Types.VARCHAR));
        this.declareParameter(new SqlParameter("pubDateTo", Types.VARCHAR));
        this.declareParameter(new SqlParameter("comments", Types.VARCHAR));

    }

    public List<TenderDashboardOfflineDetails> executeProcedure(String moduleFlag, String searchFlag, String procNature, String procType, String procMethod, String id, String status, String refNo, String pubDateFrom, String pubDateTo)
    {
        Map inParams = new HashMap();

        inParams.put("moduleFlag", moduleFlag);
        inParams.put("searchFlag", searchFlag);
        inParams.put("procNature", procNature);
        inParams.put("procType", procType);
        inParams.put("procMethod", procMethod);
        inParams.put("id", id);
        inParams.put("status", status);
        inParams.put("refNo", refNo);
        inParams.put("pubDateFrom", pubDateFrom);
        inParams.put("pubDateTo", pubDateTo);
        inParams.put("comments", "");

        this.compile();
        List<TenderDashboardOfflineDetails> details = new ArrayList<TenderDashboardOfflineDetails>();
        try{
             ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
             for (LinkedHashMap<String, Object> linkedHashMap : list) {

                TenderDashboardOfflineDetails offlineDetails =new TenderDashboardOfflineDetails();

                offlineDetails.setTenderOfflineId((Integer)linkedHashMap.get("tenderOfflineId"));
                offlineDetails.setMinistryOrDivision((String)linkedHashMap.get("ministryOrDivision"));
                offlineDetails.setPeOfficeName((String)linkedHashMap.get("peOfficeName"));
                offlineDetails.setAgency((String)linkedHashMap.get("agency"));
                offlineDetails.setReoiRfpRefNo((String)linkedHashMap.get("reoiRfpRefNo"));
                offlineDetails.setTenderPubDate((Date)linkedHashMap.get("tenderPubDate"));
                offlineDetails.setProcurementMethod((String)linkedHashMap.get("procurementMethod"));
                offlineDetails.setProcurementNature((String)linkedHashMap.get("procurementNature"));
                offlineDetails.setProcurementType((String)linkedHashMap.get("procurementType"));
                offlineDetails.setClosingDate((Date)linkedHashMap.get("ClosingDate"));
                offlineDetails.setBriefDescription((String)linkedHashMap.get("BriefDescription"));
                offlineDetails.setTenderStatus((String)linkedHashMap.get("TenderStatus"));
                offlineDetails.setEventType((String)linkedHashMap.get("eventType"));
                offlineDetails.setPackageName((String)linkedHashMap.get("PackageName"));

                details.add(offlineDetails);
 
            }
        }

        catch(Exception e){
            LOGGER.error("SPGetTenderDashboardOffline : "+ e);
        }

        return details;
    }

    public List<TenderDashboardOfflineDetails> executeProcedure(String moduleFlag, String id)
    {
        Map inParams = new HashMap();

        inParams.put("moduleFlag", moduleFlag);
        inParams.put("searchFlag", "");
        inParams.put("procNature", "");
        inParams.put("procType", "");
        inParams.put("procMethod", "");
        inParams.put("id", id);
        inParams.put("status", "");
        inParams.put("refNo", "");
        inParams.put("pubDateFrom", "");
        inParams.put("pubDateTo", "");
        inParams.put("comments", "");

        this.compile();
        List<TenderDashboardOfflineDetails> details = new ArrayList<TenderDashboardOfflineDetails>();
        try{
             ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
             for (LinkedHashMap<String, Object> linkedHashMap : list) {
                TenderDashboardOfflineDetails offlineDetails = new TenderDashboardOfflineDetails();

                if(moduleFlag.equals("viewlot"))
                {
                    offlineDetails.setLotOrRefNo((String)linkedHashMap.get("lotOrRefNo"));
                    offlineDetails.setLotIdentOrPhasingServ((String)linkedHashMap.get("lotIdentOrPhasingServ"));
                    offlineDetails.setLocation((String)linkedHashMap.get("location"));
                    offlineDetails.setTenderSecurityAmt((BigDecimal)linkedHashMap.get("tenderSecurityAmt"));
                    offlineDetails.setStartDateTime((String)linkedHashMap.get("startDateTime"));
                    offlineDetails.setCompletionDateTime((String)linkedHashMap.get("completionDateTime"));
                }
                else if(moduleFlag.equals("viewcorrigendum"))
                {
                    offlineDetails.setFieldName((String)linkedHashMap.get("fieldName"));
                    offlineDetails.setOldValue((String)linkedHashMap.get("oldValue"));
                    offlineDetails.setNewValue((String)linkedHashMap.get("newValue"));
                    offlineDetails.setCorrigendumStatus((String)linkedHashMap.get("CorrigendumStatus"));
                    offlineDetails.setNumberOfCOR((Integer)linkedHashMap.get("CorNo"));
                }
                else if(moduleFlag.equals("corrigendum"))
                {
                    offlineDetails.setFlag((Boolean)linkedHashMap.get("flag"));
                    offlineDetails.setMsg((String)linkedHashMap.get("Message"));
                    offlineDetails.setNumberOfCOR((Integer)linkedHashMap.get("CorNo"));
                }
                else
                {
                    offlineDetails.setFlag((Boolean)linkedHashMap.get("flag"));
                    offlineDetails.setMsg((String)linkedHashMap.get("Message"));
                }

                details.add(offlineDetails);
            }
        }

        catch(Exception e){
            LOGGER.error("SPGetTenderDashboardOffline : "+ e);
        }

        return details;
    }

    public List<TenderDashboardOfflineDetails> executeProcedure(String moduleFlag, String id, String comments)
    {
        Map inParams = new HashMap();

        inParams.put("moduleFlag", moduleFlag);
        inParams.put("searchFlag", "");
        inParams.put("procNature", "");
        inParams.put("procType", "");
        inParams.put("procMethod", "");
        inParams.put("id", id);
        inParams.put("status", "");
        inParams.put("refNo", "");
        inParams.put("pubDateFrom", "");
        inParams.put("pubDateTo", "");
        inParams.put("pubDateTo", "");
        inParams.put("comments", comments);

        this.compile();
        List<TenderDashboardOfflineDetails> details = new ArrayList<TenderDashboardOfflineDetails>();
        try{
             ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
             for (LinkedHashMap<String, Object> linkedHashMap : list) {
                TenderDashboardOfflineDetails offlineDetails = new TenderDashboardOfflineDetails();

                offlineDetails.setFlag((Boolean)linkedHashMap.get("flag"));
                offlineDetails.setMsg((String)linkedHashMap.get("Message"));

                details.add(offlineDetails);
            }
        }

        catch(Exception e){
            LOGGER.error("SPGetTenderDashboardOffline : "+ e);
        }

        return details;
    }

}
