package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this late, choose Tools | lates
 * and open the late in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblMessageFolder;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblMessageFolderDao extends GenericDao<TblMessageFolder> {

    public void addTblMessageFolder(TblMessageFolder tblObj);

    public void deleteTblMessageFolder(TblMessageFolder tblObj);

    public void updateTblMessageFolder(TblMessageFolder tblObj);

    public List<TblMessageFolder> getAllTblMessageFolder();

    public List<TblMessageFolder> findTblMessageFolder(Object... values) throws Exception;

    public List<TblMessageFolder> findByCountTblMessageFolder(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblMessageFolderCount();
}
