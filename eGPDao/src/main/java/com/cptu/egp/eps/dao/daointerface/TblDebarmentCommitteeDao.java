package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblDebarmentCommittee;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblDebarmentCommitteeDao extends GenericDao<TblDebarmentCommittee> {

    public void addTblDebarmentCommittee(TblDebarmentCommittee tblDebarmentCommittee);

    public void deleteTblDebarmentCommittee(TblDebarmentCommittee tblDebarmentCommittee);

    public void updateTblDebarmentCommittee(TblDebarmentCommittee tblDebarmentCommittee);

    public List<TblDebarmentCommittee> getAllTblDebarmentCommittee();

    public List<TblDebarmentCommittee> findTblDebarmentCommittee(Object... values) throws Exception;

    public List<TblDebarmentCommittee> findByCountTblDebarmentCommittee(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblDebarmentCommitteeCount();
}
