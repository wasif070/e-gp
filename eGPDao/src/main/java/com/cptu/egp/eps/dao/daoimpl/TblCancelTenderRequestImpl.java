/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblCancelTenderRequestDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCancelTenderRequest;
import java.util.List;

/**
 *
 * @author rishita
 */
public class TblCancelTenderRequestImpl extends AbcAbstractClass<TblCancelTenderRequest> implements TblCancelTenderRequestDao {

    @Override
    public void addTblCancelTenderRequest(TblCancelTenderRequest tblCancelTenderRequest) {

        super.addEntity(tblCancelTenderRequest);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblCancelTenderRequest(TblCancelTenderRequest tblCancelTenderRequest) {

        super.deleteEntity(tblCancelTenderRequest);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblCancelTenderRequest(TblCancelTenderRequest tblCancelTenderRequest) {

        super.updateEntity(tblCancelTenderRequest);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblCancelTenderRequest> getAllTblCancelTenderRequest() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCancelTenderRequest> findTblCancelTenderRequest(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCancelTenderRequestCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCancelTenderRequest> findByCountTblCancelTenderRequest(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
