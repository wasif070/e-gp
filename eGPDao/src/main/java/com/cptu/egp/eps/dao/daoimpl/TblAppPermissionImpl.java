/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblAppPermissionDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblAppPermission;
import java.util.List;

public class TblAppPermissionImpl extends AbcAbstractClass<TblAppPermission> implements TblAppPermissionDao{
    @Override
    public void addTblAppPermission(TblAppPermission master){
            super.addEntity(master);
    }
    @Override
    public void deleteTblAppPermission(TblAppPermission master) {
            super.deleteEntity(master);
    }
    @Override
    public void updateTblAppPermission(TblAppPermission master) {
            super.updateEntity(master);
    }
    @Override
    public List<TblAppPermission> getAllTblAppPermission() {
            return super.getAllEntity();
    }
    @Override
    public List<TblAppPermission> findTblAppPermission(Object... values) throws Exception {
            return super.findEntity(values);
    }
    @Override
    public long getTblAppPermissionCount() {
            return super.getEntityCount();
    }
    @Override
    public List<TblAppPermission> findByCountTblAppPermission(int firstResult, int maxResult, Object... values) throws Exception {
            return super.findByCountEntity(firstResult, maxResult, values);
    }
    @Override
    public void updateOrSaveTblAppPermission(List<TblAppPermission> tblObj) {
            super.updateAll(tblObj);
    }
}
