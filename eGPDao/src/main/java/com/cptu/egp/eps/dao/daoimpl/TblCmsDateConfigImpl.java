package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsDateConfigDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsDateConfig;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsDateConfigImpl extends AbcAbstractClass<TblCmsDateConfig> implements TblCmsDateConfigDao {

    @Override
    public void addTblCmsDateConfig(TblCmsDateConfig master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblCmsDateConfig(TblCmsDateConfig master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblCmsDateConfig(TblCmsDateConfig master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblCmsDateConfig> getAllTblCmsDateConfig() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsDateConfig> findTblCmsDateConfig(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsDateConfigCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsDateConfig> findByCountTblCmsDateConfig(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
