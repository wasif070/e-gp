/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblWorkFlowEventConfigHist;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public interface TblWorkFlowEventConfigHistDao extends GenericDao<TblWorkFlowEventConfigHist> {

    public void addTblWorkFlowEventConfigHist(TblWorkFlowEventConfigHist admin);

    public void deleteTblWorkFlowEventConfigHist(TblWorkFlowEventConfigHist admin);

    public void updateTblWorkFlowEventConfigHist(TblWorkFlowEventConfigHist admin);

    public List<TblWorkFlowEventConfigHist> getAllTblWorkFlowEventConfigHist();

    public List<TblWorkFlowEventConfigHist> findTblWorkFlowEventConfigHist(Object... values) throws Exception;

    public List<TblWorkFlowEventConfigHist> findByCountTblWorkFlowEventConfigHist(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblWorkFlowEventConfigHistCount();
}
