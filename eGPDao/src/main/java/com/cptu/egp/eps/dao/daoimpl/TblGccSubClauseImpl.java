package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblGccClauseDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblGccClause;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblGccSubClauseImpl extends AbcAbstractClass<TblGccClause> implements TblGccClauseDao {

    @Override
    public void addTblGccClause(TblGccClause gccClause){
        super.addEntity(gccClause);
    }

    @Override
    public void deleteTblGccClause(TblGccClause gccClause) {
        super.deleteEntity(gccClause);
    }

    @Override
    public void updateTblGccClause(TblGccClause gccClause) {
        super.updateEntity(gccClause);
    }

    @Override
    public List<TblGccClause> getAllTblGccClause() {
        return super.getAllEntity();
    }

    @Override
    public List<TblGccClause> findTblGccClause(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblGccClauseCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblGccClause> findByCountTblGccClause(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
