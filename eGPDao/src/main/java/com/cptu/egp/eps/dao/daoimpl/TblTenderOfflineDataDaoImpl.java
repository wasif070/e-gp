/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderOfflineDataDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderDetailsOffline;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class TblTenderOfflineDataDaoImpl extends AbcAbstractClass<TblTenderDetailsOffline> implements TblTenderOfflineDataDao{

    @Override
    public void addTblTenderDetailOfflineData(TblTenderDetailsOffline tenderDetailOfflineData){
        super.addEntity(tenderDetailOfflineData);
    }

    @Override
    public void deleteTblTenderDetailOfflineData(TblTenderDetailsOffline tenderDetailOfflineData) {
        super.deleteEntity(tenderDetailOfflineData);
    }

    @Override
    public void updateTblTenderDetailOfflineData(TblTenderDetailsOffline tenderDetailOfflineData) {
        super.updateEntity(tenderDetailOfflineData);
    }

    @Override
    public List<TblTenderDetailsOffline> getAllTblTenderDetailOfflineData() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderDetailsOffline> findTblTenderDetailOfflineData(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public List<TblTenderDetailsOffline> findByCountTblTenderDetailOfflineData(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblTenderDetailOfflineDataCount() {
       return super.getEntityCount();
    }
}
