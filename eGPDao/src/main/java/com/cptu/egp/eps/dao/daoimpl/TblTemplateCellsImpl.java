/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTemplateCellsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTemplateCells;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTemplateCellsImpl extends AbcAbstractClass<TblTemplateCells> implements TblTemplateCellsDao{

    @Override
    public void addTblTemplateCells(TblTemplateCells tblTemplateCells) {
        super.addEntity(tblTemplateCells);
    }

    @Override
    public List<TblTemplateCells> findTblTemplateCells(Object... values) throws Exception {
        return super.findEntity(values);
        
    }

    @Override
    public void deleteTblTemplateCells(TblTemplateCells tblTemplateCells) {

        super.deleteEntity(tblTemplateCells);
       
    }

    @Override
    public void updateTblTemplateCells(TblTemplateCells tblTemplateCells) {

        super.updateEntity(tblTemplateCells);
       
    }

    @Override
    public List<TblTemplateCells> getAllTblTemplateCells() {
        return super.getAllEntity();
    }

    @Override
    public long getTblTemplateCellsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTemplateCells> findByCountTblTemplateCells(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
