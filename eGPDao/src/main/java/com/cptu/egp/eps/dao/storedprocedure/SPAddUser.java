/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Administrator
 */
public class SPAddUser extends StoredProcedure{
    private static final Logger LOGGER = Logger.getLogger(SPAddUser.class);
    SPAddUser(BasicDataSource dataSource, String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("@v_UserId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("@v_Action", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_Query", Types.VARCHAR));
    }

    /**
     * Execute stored procedure : p_add_user
     * To perform Add User Operation.
     * @param userId
     * @param action
     * @param query
     * @return CommonMsgChk list contains flags indicate operation success status.
     */
    public List<CommonMsgChk> executeProcedure(int userId, String action, String query) {
        Map inParams = new HashMap();
        inParams.put("@v_UserId_inInt", userId);
        inParams.put("@v_Action", action);
        inParams.put("@v_Query", query);
        this.compile();
        List<CommonMsgChk> details = new ArrayList<CommonMsgChk>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonMsgChk commonMsgChk = new CommonMsgChk();
                commonMsgChk.setFlag((Boolean) linkedHashMap.get("flag"));
                commonMsgChk.setMsg((String) linkedHashMap.get("Message"));
                details.add(commonMsgChk);
            }
        } catch (Exception e) {
            LOGGER.error("SPAddUser : "+ e);
        }
        return details;
    }

    
}
