package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsDateConfigHistory;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCmsDateConfigHistoryDao extends GenericDao<TblCmsDateConfigHistory> {

    public void addTblCmsDateConfigHistory(TblCmsDateConfigHistory Activity);

    public void deleteTblCmsDateConfigHistory(TblCmsDateConfigHistory Activity);

    public void updateTblCmsDateConfigHistory(TblCmsDateConfigHistory Activity);

    public List<TblCmsDateConfigHistory> getAllTblCmsDateConfigHistory();

    public List<TblCmsDateConfigHistory> findTblCmsDateConfigHistory(Object... values) throws Exception;

    public List<TblCmsDateConfigHistory> findByCountTblCmsDateConfigHistory(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsDateConfigHistoryCount();
}
