/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTemplateSectionFormDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTemplateSectionForm;
import java.util.List;

/**
 *
 * @author yanki
 */
public class TblTemplateSectionFormImpl extends AbcAbstractClass<TblTemplateSectionForm> implements TblTemplateSectionFormDao{

    @Override
    public void addTblTemplateForm(TblTemplateSectionForm templateSectionForm) {
        super.addEntity(templateSectionForm);
    }

    @Override
    public void deleteTblTemplateForm(TblTemplateSectionForm templateSectionForm) {
        super.deleteEntity(templateSectionForm);
    }

    @Override
    public void updateTblTemplateForm(TblTemplateSectionForm templateSectionForm) {
        super.updateEntity(templateSectionForm);
    }

    @Override
    public List<TblTemplateSectionForm> getAllTblTemplateForm() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTemplateSectionForm> findTblTemplateForm(Object... values) throws Exception {
        return super.findEntity(values);
    }

}
