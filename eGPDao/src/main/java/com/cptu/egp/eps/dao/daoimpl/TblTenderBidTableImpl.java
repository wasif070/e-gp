/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderBidTableDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderBidTable;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderBidTableImpl extends AbcAbstractClass<TblTenderBidTable> implements TblTenderBidTableDao {

    @Override
    public void addTblTenderBidTable(TblTenderBidTable admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblTenderBidTable(TblTenderBidTable admin) {
        super.deleteEntity(admin);        
    }

    @Override
    public void updateTblTenderBidTable(TblTenderBidTable admin) {
        super.updateEntity(admin);        
    }

    @Override
    public List<TblTenderBidTable> getAllTblTenderBidTable() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderBidTable> findTblTenderBidTable(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderBidTableCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderBidTable> findByCountTblTenderBidTable(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
