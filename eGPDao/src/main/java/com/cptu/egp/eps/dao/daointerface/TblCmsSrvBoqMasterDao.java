package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvBoqMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCmsSrvBoqMasterDao extends GenericDao<TblCmsSrvBoqMaster> {

    public void addTblCmsSrvBoqMaster(TblCmsSrvBoqMaster event);

    public void deleteTblCmsSrvBoqMaster(TblCmsSrvBoqMaster event);

    public void updateTblCmsSrvBoqMaster(TblCmsSrvBoqMaster event);

    public List<TblCmsSrvBoqMaster> getAllTblCmsSrvBoqMaster();

    public List<TblCmsSrvBoqMaster> findTblCmsSrvBoqMaster(Object... values) throws Exception;

    public List<TblCmsSrvBoqMaster> findByCountTblCmsSrvBoqMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsSrvBoqMasterCount();
}
