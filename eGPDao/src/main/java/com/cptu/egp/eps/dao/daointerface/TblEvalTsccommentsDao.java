package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalTsccomments;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblEvalTsccommentsDao extends GenericDao<TblEvalTsccomments> {

    public void addTblEvalTsccomments(TblEvalTsccomments tblObj);

    public void deleteTblEvalTsccomments(TblEvalTsccomments tblObj);

    public void updateTblEvalTsccomments(TblEvalTsccomments tblObj);

    public List<TblEvalTsccomments> getAllTblEvalTsccomments();

    public List<TblEvalTsccomments> findTblEvalTsccomments(Object... values) throws Exception;

    public List<TblEvalTsccomments> findByCountTblEvalTsccomments(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalTsccommentsCount();
}
