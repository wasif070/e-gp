package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblLocationMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblLocationMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblLocationMasterImpl extends AbcAbstractClass<TblLocationMaster> implements TblLocationMasterDao {

    @Override
    public void addTblLocationMaster(TblLocationMaster locationMaster){
        super.addEntity(locationMaster);
    }

    @Override
    public void deleteTblLocationMaster(TblLocationMaster locationMaster) {
        super.deleteEntity(locationMaster);
    }

    @Override
    public void updateTblLocationMaster(TblLocationMaster locationMaster) {
        super.updateEntity(locationMaster);
    }

    @Override
    public List<TblLocationMaster> getAllTblLocationMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblLocationMaster> findTblLocationMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblLocationMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblLocationMaster> findByCountTblLocationMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
