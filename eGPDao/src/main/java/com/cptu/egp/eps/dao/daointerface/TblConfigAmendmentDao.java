package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblConfigAmendment;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblConfigAmendmentDao extends GenericDao<TblConfigAmendment> {

    public void addTblConfigAmendment(TblConfigAmendment tblConfigAmendment);

    public void deleteTblConfigAmendment(TblConfigAmendment tblConfigAmendment);

    public void deleteAllTblConfigAmendment(Collection tblConfigAmendment);

    public void updateTblConfigAmendment(TblConfigAmendment tblConfigAmendment);

    public List<TblConfigAmendment> getAllTblConfigAmendment();

    public List<TblConfigAmendment> findTblConfigAmendment(Object... values) throws Exception;

    public List<TblConfigAmendment> findByCountTblConfigAmendment(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblConfigAmendmentCount();
}
