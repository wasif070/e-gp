/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblPreTenderReplyDocs;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblPreTenderReplyDocsDao extends GenericDao<TblPreTenderReplyDocs>{

    public void addTblPreTenderReplyDocs(TblPreTenderReplyDocs tblObj);

    public void deleteTblPreTenderReplyDocs(TblPreTenderReplyDocs tblObj);

    public void updateTblPreTenderReplyDocs(TblPreTenderReplyDocs tblObj);

    public List<TblPreTenderReplyDocs> getAllTblPreTenderReplyDocs();

    public List<TblPreTenderReplyDocs> findTblPreTenderReplyDocs(Object... values) throws Exception;

    public List<TblPreTenderReplyDocs> findByCountTblPreTenderReplyDocs(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblPreTenderReplyDocsCount();
}
