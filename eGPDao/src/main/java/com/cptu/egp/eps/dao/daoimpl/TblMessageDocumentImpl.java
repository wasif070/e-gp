package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblMessageDocumentDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblMessageDocument;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblMessageDocumentImpl extends AbcAbstractClass<TblMessageDocument> implements TblMessageDocumentDao {

    @Override
    public void addTblMessageDocument(TblMessageDocument mdebarmentTypes){
        super.addEntity(mdebarmentTypes);
    }

    @Override
    public void deleteTblMessageDocument(TblMessageDocument mdebarmentTypes) {
        super.deleteEntity(mdebarmentTypes);
    }

    @Override
    public void updateTblMessageDocument(TblMessageDocument mdebarmentTypes) {
        super.updateEntity(mdebarmentTypes);
    }

    @Override
    public List<TblMessageDocument> getAllTblMessageDocument() {
        return super.getAllEntity();
    }

    @Override
    public List<TblMessageDocument> findTblMessageDocument(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblMessageDocumentCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblMessageDocument> findByCountTblMessageDocument(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
