/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderEngEstDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderEngEstDocs;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderEngEstDocsImpl extends AbcAbstractClass<TblTenderEngEstDocs> implements TblTenderEngEstDocsDao {

    @Override
    public void addTblTenderEngEstDocs(TblTenderEngEstDocs tenderEngEstDocs) {
        super.addEntity(tenderEngEstDocs);
    }

    @Override
    public void deleteTblTenderEngEstDocs(TblTenderEngEstDocs tenderEngEstDocs) {
        super.deleteEntity(tenderEngEstDocs);
    }

    @Override
    public void updateTblTenderEngEstDocs(TblTenderEngEstDocs tenderEngEstDocs) {
        super.updateEntity(tenderEngEstDocs);
    }

    @Override
    public List<TblTenderEngEstDocs> getAllTblTenderEngEstDocs() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderEngEstDocs> findTblTenderEngEstDocs(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderEngEstDocsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderEngEstDocs> findByCountTblTenderEngEstDocs(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
