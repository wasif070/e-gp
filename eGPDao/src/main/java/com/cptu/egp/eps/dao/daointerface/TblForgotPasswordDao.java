/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblForgotPassword;
import java.util.List;

/**
 *
 * @author dixit
 */
public interface TblForgotPasswordDao extends GenericDao<TblForgotPassword> {

    public void addTblForgotPassword(TblForgotPassword tblObj);

    public void deleteTblForgotPassword(TblForgotPassword tblObj);

    public void updateTblForgotPassword(TblForgotPassword tblObj);

    public List<TblForgotPassword> getAllTblForgotPassword();

    public List<TblForgotPassword> findTblForgotPassword(Object... values) throws Exception;

    public List<TblForgotPassword> findByCountTblForgotPassword(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblForgotPasswordCount();
}
