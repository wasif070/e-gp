/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.model.table.TblSmslog;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TblSmslogDao {

    public void addTblSmslog(TblSmslog smslog);

    public void deleteTblSmslog(TblSmslog smslog);

    public void updateTblSmslog(TblSmslog smslog);

    public List<TblSmslog> getAllTblSmslog();

    public List<TblSmslog> findTblSmslog(Object... values) throws Exception;

    public List<TblSmslog> findByCountTblSmslog(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblSmslogCount();
}
