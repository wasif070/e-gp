/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsTaxConfig;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public interface TblCmsTaxConfigDao extends GenericDao<TblCmsTaxConfig>{

    public void addTblCmsTaxConfig(TblCmsTaxConfig tblCmsTaxConfig);

    public void deleteTblCmsTaxConfig(TblCmsTaxConfig tblCmsTaxConfig);

    public void updateTblCmsTaxConfig(TblCmsTaxConfig tblCmsTaxConfig);

    public List<TblCmsTaxConfig> getAllTblCmsTaxConfig();

    public List<TblCmsTaxConfig> findTblCmsTaxConfig(Object... values) throws Exception;

    public List<TblCmsTaxConfig> findByCountTblCmsTaxConfig(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsTaxConfigCount();
    
}
