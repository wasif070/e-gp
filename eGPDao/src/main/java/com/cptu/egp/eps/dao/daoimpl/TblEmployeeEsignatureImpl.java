/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblEmployeeEsignatureDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEmployeeEsignature;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblEmployeeEsignatureImpl extends AbcAbstractClass<TblEmployeeEsignature> implements TblEmployeeEsignatureDao{

    @Override
    public void addTblEmployeeEsignature(TblEmployeeEsignature tblEmployeeEsignature) {
        super.addEntity(tblEmployeeEsignature);
    }

    @Override
    public List<TblEmployeeEsignature> findTblEmployeeEsignature(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblEmployeeEsignature(TblEmployeeEsignature tblEmployeeEsignature) {

        super.deleteEntity(tblEmployeeEsignature);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblEmployeeEsignature(TblEmployeeEsignature tblEmployeeEsignature) {

        super.updateEntity(tblEmployeeEsignature);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblEmployeeEsignature> getAllTblEmployeeEsignature() {
        return super.getAllEntity();
    }

    @Override
    public long getTblEmployeeEsignatureCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEmployeeEsignature> findByCountTblEmployeeEsignature(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
