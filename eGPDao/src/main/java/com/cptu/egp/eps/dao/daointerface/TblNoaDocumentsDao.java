package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblNoaDocuments;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblNoaDocumentsDao extends GenericDao<TblNoaDocuments> {

    public void addTblNoaDocuments(TblNoaDocuments tblNoaDocuments);

    public void deleteTblNoaDocuments(TblNoaDocuments tblNoaDocuments);

    public void deleteAllTblNoaDocuments(Collection tblNoaDocuments);

    public void updateTblNoaDocuments(TblNoaDocuments tblNoaDocuments);

    public List<TblNoaDocuments> getAllTblNoaDocuments();

    public List<TblNoaDocuments> findTblNoaDocuments(Object... values) throws Exception;

    public List<TblNoaDocuments> findByCountTblNoaDocuments(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblNoaDocumentsCount();
}
