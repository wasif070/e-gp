package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsPrDocument;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCmsPrDocumentDao extends GenericDao<TblCmsPrDocument> {

    public void addTblCmsPrDocument(TblCmsPrDocument event);

    public void deleteTblCmsPrDocument(TblCmsPrDocument event);

    public void updateTblCmsPrDocument(TblCmsPrDocument event);

    public List<TblCmsPrDocument> getAllTblCmsPrDocument();

    public List<TblCmsPrDocument> findTblCmsPrDocument(Object... values) throws Exception;

    public List<TblCmsPrDocument> findByCountTblCmsPrDocument(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsPrDocumentCount();
}
