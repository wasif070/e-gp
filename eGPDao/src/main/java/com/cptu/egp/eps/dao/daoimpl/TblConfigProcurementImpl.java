/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblConfigProcurementDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblConfigProcurement;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblConfigProcurementImpl extends AbcAbstractClass<TblConfigProcurement> implements TblConfigProcurementDao{

    @Override
    public void addTblConfigProcurement(TblConfigProcurement configProcurement) {
        super.addEntity(configProcurement);
    }

    @Override
    public List<TblConfigProcurement> findTblConfigProcurement(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblConfigProcurement(TblConfigProcurement configProcurement) {

        super.deleteEntity(configProcurement);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblConfigProcurement(Collection event) {
        super.deleteAll(event);
    }

    @Override
    public void updateTblConfigProcurement(TblConfigProcurement configProcurement) {

        super.updateEntity(configProcurement);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblConfigProcurement> getAllTblConfigProcurement() {
        return super.getAllEntity();
    }

    @Override
    public long getTblConfigProcurementCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblConfigProcurement> findByCountTblConfigProcurement(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
