/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblNegNotifyTendererDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblNegNotifyTenderer;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblNegNotifyTendererImpl extends AbcAbstractClass<TblNegNotifyTenderer> implements TblNegNotifyTendererDao{

    @Override
    public void addTblNegNotifyTenderer(TblNegNotifyTenderer negNotifyTenderer) {
        super.addEntity(negNotifyTenderer);
    }

    @Override
    public List<TblNegNotifyTenderer> findTblNegNotifyTenderer(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblNegNotifyTenderer(TblNegNotifyTenderer department) {

        super.deleteEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblNegNotifyTenderer(Collection event) {
        super.deleteAll(event);
    }

    @Override
    public void updateTblNegNotifyTenderer(TblNegNotifyTenderer department) {

        super.updateEntity(department);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblNegNotifyTenderer> getAllTblNegNotifyTenderer() {
        return super.getAllEntity();
    }

    @Override
    public long getTblNegNotifyTendererCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblNegNotifyTenderer> findByCountTblNegNotifyTenderer(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
