package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblScBankDevPartnerMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblScBankDevPartnerMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblScBankDevPartnerMasterImpl extends AbcAbstractClass<TblScBankDevPartnerMaster> implements TblScBankDevPartnerMasterDao {

    @Override
    public void addTblScBankDevPartnerMaster(TblScBankDevPartnerMaster master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblScBankDevPartnerMaster(TblScBankDevPartnerMaster master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblScBankDevPartnerMaster(TblScBankDevPartnerMaster master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblScBankDevPartnerMaster> getAllTblScBankDevPartnerMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblScBankDevPartnerMaster> findTblScBankDevPartnerMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblScBankDevPartnerMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblScBankDevPartnerMaster> findByCountTblScBankDevPartnerMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
