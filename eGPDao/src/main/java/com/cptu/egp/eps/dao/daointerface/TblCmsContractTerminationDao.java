/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsContractTermination;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public interface TblCmsContractTerminationDao extends GenericDao<TblCmsContractTermination> {

    public void addTblCmsContractTermination(TblCmsContractTermination tblCmsContractTermination);

    public void deleteTblCmsContractTermination(TblCmsContractTermination tblCmsContractTermination);

    public void updateTblCmsContractTermination(TblCmsContractTermination tblCmsContractTermination);

    public List<TblCmsContractTermination> getAllTblCmsContractTermination();

    public List<TblCmsContractTermination> findTblCmsContractTermination(Object... values) throws Exception;

    public List<TblCmsContractTermination> findByCountTblCmsContractTermination(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsContractTerminationCount();
}
