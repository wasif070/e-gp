package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblFinalSubmissionDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblFinalSubmission;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblFinalSubmissionImpl extends AbcAbstractClass<TblFinalSubmission> implements TblFinalSubmissionDao {

    @Override
    public void addTblFinalSubmission(TblFinalSubmission finalSubmission){
        super.addEntity(finalSubmission);
    }

    @Override
    public void deleteTblFinalSubmission(TblFinalSubmission finalSubmission) {
        super.deleteEntity(finalSubmission);
    }

    @Override
    public void updateTblFinalSubmission(TblFinalSubmission finalSubmission) {
        super.updateEntity(finalSubmission);
    }

    @Override
    public List<TblFinalSubmission> getAllTblFinalSubmission() {
        return super.getAllEntity();
    }

    @Override
    public List<TblFinalSubmission> findTblFinalSubmission(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblFinalSubmissionCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblFinalSubmission> findByCountTblFinalSubmission(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
