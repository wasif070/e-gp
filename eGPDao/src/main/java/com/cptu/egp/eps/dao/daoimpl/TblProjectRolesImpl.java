/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblProjectRolesDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblProjectRoles;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblProjectRolesImpl extends AbcAbstractClass<TblProjectRoles> implements TblProjectRolesDao{

    @Override
    public void addTblProjectRoles(TblProjectRoles tblProjectRoles) {
        super.addEntity(tblProjectRoles);
    }

    @Override
    public List<TblProjectRoles> findTblProjectRoles(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblProjectRoles(TblProjectRoles tblProjectRoles) {

        super.deleteEntity(tblProjectRoles);
        
    }

    @Override
    public void updateTblProjectRoles(TblProjectRoles tblProjectRoles) {

        super.updateEntity(tblProjectRoles);
       
    }

    @Override
    public List<TblProjectRoles> getAllTblProjectRoles() {
        return super.getAllEntity();
    }

    @Override
    public long getTblProjectRolesCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblProjectRoles> findByCountTblProjectRoles(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
