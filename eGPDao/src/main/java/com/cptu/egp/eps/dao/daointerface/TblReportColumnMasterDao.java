package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblReportColumnMaster;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblReportColumnMasterDao extends GenericDao<TblReportColumnMaster> {

    public void addTblReportColumnMaster(TblReportColumnMaster tblReportColumnMaster);

    public void deleteTblReportColumnMaster(TblReportColumnMaster tblReportColumnMaster);

    public void deleteAllTblReportColumnMaster(Collection tblReportColumnMaster);

    public void updateTblReportColumnMaster(TblReportColumnMaster tblReportColumnMaster);

    public List<TblReportColumnMaster> getAllTblReportColumnMaster();

    public List<TblReportColumnMaster> findTblReportColumnMaster(Object... values) throws Exception;

    public List<TblReportColumnMaster> findByCountTblReportColumnMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblReportColumnMasterCount();

    public void updateOrSaveReportColumn(List<TblReportColumnMaster> tblReportColumnMaster);
}
