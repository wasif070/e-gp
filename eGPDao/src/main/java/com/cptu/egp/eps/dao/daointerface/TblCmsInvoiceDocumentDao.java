package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsInvoiceDocument;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCmsInvoiceDocumentDao extends GenericDao<TblCmsInvoiceDocument> {

    public void addTblCmsInvoiceDocument(TblCmsInvoiceDocument event);

    public void deleteTblCmsInvoiceDocument(TblCmsInvoiceDocument event);

    public void updateTblCmsInvoiceDocument(TblCmsInvoiceDocument event);

    public List<TblCmsInvoiceDocument> getAllTblCmsInvoiceDocument();

    public List<TblCmsInvoiceDocument> findTblCmsInvoiceDocument(Object... values) throws Exception;

    public List<TblCmsInvoiceDocument> findByCountTblCmsInvoiceDocument(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsInvoiceDocumentCount();
}
