/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblWorkFlowDocumentsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblWorkFlowDocuments;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public class TblWorkFlowDocumentsImpl extends AbcAbstractClass<TblWorkFlowDocuments> implements TblWorkFlowDocumentsDao {

    @Override
    public void addTblWorkFlowDocuments(TblWorkFlowDocuments workflowDoc) {

        super.addEntity(workflowDoc);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblWorkFlowDocuments(TblWorkFlowDocuments workflowDoc) {

        super.deleteEntity(workflowDoc);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblWorkFlowDocuments(TblWorkFlowDocuments workflowDoc) {

        super.updateEntity(workflowDoc);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblWorkFlowDocuments> getAllTblWorkFlowDocuments() {
        return super.getAllEntity();
    }

    @Override
    public List<TblWorkFlowDocuments> findTblWorkFlowDocuments(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblWorkFlowDocumentsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblWorkFlowDocuments> findByCountTblWorkFlowDocuments(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
