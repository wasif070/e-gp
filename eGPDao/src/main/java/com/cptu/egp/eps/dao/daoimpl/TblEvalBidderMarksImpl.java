package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblEvalBidderMarksDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalBidderMarks;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblEvalBidderMarksImpl extends AbcAbstractClass<TblEvalBidderMarks> implements TblEvalBidderMarksDao {

    @Override
    public void addTblEvalBidderMarks(TblEvalBidderMarks tblEvalBidderMarks){
        super.addEntity(tblEvalBidderMarks);
    }

    @Override
    public void deleteTblEvalBidderMarks(TblEvalBidderMarks tblEvalBidderMarks) {
        super.deleteEntity(tblEvalBidderMarks);
    }

    @Override
    public void updateTblEvalBidderMarks(TblEvalBidderMarks tblEvalBidderMarks) {
        super.updateEntity(tblEvalBidderMarks);
    }

    @Override
    public List<TblEvalBidderMarks> getAllTblEvalBidderMarks() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEvalBidderMarks> findTblEvalBidderMarks(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEvalBidderMarksCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalBidderMarks> findByCountTblEvalBidderMarks(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateInsAllEvalBidderMarks(List<TblEvalBidderMarks> tblEvalBidderMarks) {
        super.updateAll(tblEvalBidderMarks);
    }
}
