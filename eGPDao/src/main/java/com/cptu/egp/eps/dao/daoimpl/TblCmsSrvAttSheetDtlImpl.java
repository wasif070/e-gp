package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvAttSheetDtlDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvAttSheetDtl;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsSrvAttSheetDtlImpl extends AbcAbstractClass<TblCmsSrvAttSheetDtl> implements TblCmsSrvAttSheetDtlDao {

    @Override
    public void addTblCmsSrvAttSheetDtl(TblCmsSrvAttSheetDtl cmsInvoiceDetails){
        super.addEntity(cmsInvoiceDetails);
    }

    @Override
    public void deleteTblCmsSrvAttSheetDtl(TblCmsSrvAttSheetDtl cmsInvoiceDetails) {
        super.deleteEntity(cmsInvoiceDetails);
    }

    @Override
    public void updateTblCmsSrvAttSheetDtl(TblCmsSrvAttSheetDtl cmsInvoiceDetails) {
        super.updateEntity(cmsInvoiceDetails);
    }

    @Override
    public List<TblCmsSrvAttSheetDtl> getAllTblCmsSrvAttSheetDtl() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvAttSheetDtl> findTblCmsSrvAttSheetDtl(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvAttSheetDtlCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvAttSheetDtl> findByCountTblCmsSrvAttSheetDtl(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }


    @Override
    public void updateOrSaveEstCost(List<TblCmsSrvAttSheetDtl> estCost) {
       super.updateAll(estCost);
    }
}
