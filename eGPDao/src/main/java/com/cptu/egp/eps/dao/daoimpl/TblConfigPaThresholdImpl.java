/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;
import com.cptu.egp.eps.dao.daointerface.TblConfigPaThresholdDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblConfigPaThreshold;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author feroz
 */
public class TblConfigPaThresholdImpl extends AbcAbstractClass<TblConfigPaThreshold> implements TblConfigPaThresholdDao{
    
    @Override
    public void addTblConfigPaThreshold(TblConfigPaThreshold configPaThreshold) {
        super.addEntity(configPaThreshold);
    }
    
    @Override
    public void deleteTblConfigPaThreshold(TblConfigPaThreshold configPaThreshold) {

        super.deleteEntity(configPaThreshold);
        //throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public void updateTblConfigPaThreshold(TblConfigPaThreshold configPaThreshold) {

        super.updateEntity(configPaThreshold);
        //throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public List<TblConfigPaThreshold> getAllTblConfigPaThreshold() {
        return super.getAllEntity();
    }
    
    @Override
    public List<TblConfigPaThreshold> findTblConfigPaThreshold(Object... values) {
        List<TblConfigPaThreshold> tblConfigPaThreshold = null;
        try {
            tblConfigPaThreshold = super.findEntity(values);
        } catch (Exception ex) {
            Logger.getLogger(TblConfigPaThresholdImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tblConfigPaThreshold;
    }
    
}
