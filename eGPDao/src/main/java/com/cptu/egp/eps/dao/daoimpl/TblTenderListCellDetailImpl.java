/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderListCellDetailDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderListCellDetail;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class TblTenderListCellDetailImpl extends AbcAbstractClass<TblTenderListCellDetail> implements TblTenderListCellDetailDao{

    @Override
    public void addTblTenderListCellDetail(TblTenderListCellDetail tblTenderListCellDetail) {
        super.addEntity(tblTenderListCellDetail);
    }

    @Override
    public List<TblTenderListCellDetail> findTblTenderListCellDetail(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblTenderListCellDetail(TblTenderListCellDetail tblTenderListCellDetail) {

        super.deleteEntity(tblTenderListCellDetail);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblTenderListCellDetail(TblTenderListCellDetail tblTenderListCellDetail) {

        super.updateEntity(tblTenderListCellDetail);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblTenderListCellDetail> getAllTblTenderListCellDetail() {
        return super.getAllEntity();
    }

    @Override
    public long getTblTenderListCellDetailCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderListCellDetail> findByCountTblTenderListCellDetail(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveAllTblTenderListCellDetail(List<TblTenderListCellDetail> list) {
        super.updateAll(list);
    }
}