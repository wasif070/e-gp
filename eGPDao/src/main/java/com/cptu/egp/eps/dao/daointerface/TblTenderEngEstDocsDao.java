/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderEngEstDocs;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderEngEstDocsDao extends GenericDao<TblTenderEngEstDocs>{

    public void addTblTenderEngEstDocs(TblTenderEngEstDocs tblObj);

    public void deleteTblTenderEngEstDocs(TblTenderEngEstDocs tblObj);

    public void updateTblTenderEngEstDocs(TblTenderEngEstDocs tblObj);

    public List<TblTenderEngEstDocs> getAllTblTenderEngEstDocs();

    public List<TblTenderEngEstDocs> findTblTenderEngEstDocs(Object... values) throws Exception;

    public List<TblTenderEngEstDocs> findByCountTblTenderEngEstDocs(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderEngEstDocsCount();
}
