package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblJvcapartnersDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblJvcapartners;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblJvcapartnersImpl extends AbcAbstractClass<TblJvcapartners> implements TblJvcapartnersDao {

    @Override
    public void addTblJvcapartners(TblJvcapartners tblJvcapartners){
        super.addEntity(tblJvcapartners);
    }

    @Override
    public void deleteTblJvcapartners(TblJvcapartners tblJvcapartners) {
        super.deleteEntity(tblJvcapartners);
    }

    @Override
    public void updateTblJvcapartners(TblJvcapartners tblJvcapartners) {
        super.updateEntity(tblJvcapartners);
    }

    @Override
    public List<TblJvcapartners> getAllTblJvcapartners() {
        return super.getAllEntity();
    }

    @Override
    public List<TblJvcapartners> findTblJvcapartners(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblJvcapartnersCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblJvcapartners> findByCountTblJvcapartners(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
