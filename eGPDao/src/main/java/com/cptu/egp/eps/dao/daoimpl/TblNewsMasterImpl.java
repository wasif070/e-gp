package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblNewsMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblNewsMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblNewsMasterImpl extends AbcAbstractClass<TblNewsMaster> implements TblNewsMasterDao {

    @Override
    public void addTblNewsMaster(TblNewsMaster master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblNewsMaster(TblNewsMaster master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblNewsMaster(TblNewsMaster master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblNewsMaster> getAllTblNewsMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblNewsMaster> findTblNewsMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblNewsMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblNewsMaster> findByCountTblNewsMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
