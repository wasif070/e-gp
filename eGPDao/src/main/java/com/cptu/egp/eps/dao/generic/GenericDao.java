/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.generic;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * author taher
 */
public interface GenericDao<T> {

    public void addEntity(T entity);

    public void deleteEntity(T entity);

    public void deleteAll(Collection<T> collection);

    public void updateEntity(T entity);

    public void updateAll(Collection<T> collection);

    public List<T> getAllEntity();

    public List<T> findEntity(Object... values) throws Exception;

    public List<T> findByCountEntity(int firstResult,int maxResult,Object... values) throws Exception;

    public long getEntityCount();

    public List<Object[]> createQuery(String query);

    public List<Object[]> createByCountQuery(String query,int firstResult,int maxResult);

    public long countForQuery(String from,String where)throws Exception;

    public List<Object> singleColQuery(String query);

    public int updateDeleteQuery(String query);

    public int updateDeleteSQLQuery(String query);
    
    List<Object[]> createSQLQuery(String query,Map<String,Object> var);

    //public List<Object[]> runNativeSQLQry(String query);
}
