package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.view.VwGetSbDevPartner;
import java.util.List;

/**
 *
 * @author taher
 */
public interface VwSBDevPartnerDao extends GenericDao<VwGetSbDevPartner> {

    public List<VwGetSbDevPartner> getAllVwSBDevPartner();

    public List<VwGetSbDevPartner> findVwSBDevPartner(Object... values) throws Exception;

    public List<VwGetSbDevPartner> findByCountVwSBDevPartner(int firstResult,int maxResult,Object... values) throws Exception;

}
