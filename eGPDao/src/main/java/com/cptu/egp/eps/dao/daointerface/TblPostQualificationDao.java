package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblPostQualification;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblPostQualificationDao extends GenericDao<TblPostQualification> {

    public void addTblPostQualification(TblPostQualification tblPostQualification);

    public void deleteTblPostQualification(TblPostQualification tblPostQualification);

    public void deleteAllTblPostQualification(Collection tblPostQualification);

    public void updateTblPostQualification(TblPostQualification tblPostQualification);

    public List<TblPostQualification> getAllTblPostQualification();

    public List<TblPostQualification> findTblPostQualification(Object... values) throws Exception;

    public List<TblPostQualification> findByCountTblPostQualification(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblPostQualificationCount();
}
