package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblFinalSubDetailDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblFinalSubDetail;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblFinalSubDetailImpl extends AbcAbstractClass<TblFinalSubDetail> implements TblFinalSubDetailDao {

    @Override
    public void addTblFinalSubDetail(TblFinalSubDetail finalSubDetail){
        super.addEntity(finalSubDetail);
    }

    @Override
    public void deleteTblFinalSubDetail(TblFinalSubDetail finalSubDetail) {
        super.deleteEntity(finalSubDetail);
    }

    @Override
    public void updateTblFinalSubDetail(TblFinalSubDetail finalSubDetail) {
        super.updateEntity(finalSubDetail);
    }

    @Override
    public List<TblFinalSubDetail> getAllTblFinalSubDetail() {
        return super.getAllEntity();
    }

    @Override
    public List<TblFinalSubDetail> findTblFinalSubDetail(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblFinalSubDetailCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblFinalSubDetail> findByCountTblFinalSubDetail(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
