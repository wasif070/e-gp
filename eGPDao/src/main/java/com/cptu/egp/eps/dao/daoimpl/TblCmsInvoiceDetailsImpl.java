package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsInvoiceDetailsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsInvoiceDetails;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsInvoiceDetailsImpl extends AbcAbstractClass<TblCmsInvoiceDetails> implements TblCmsInvoiceDetailsDao {

    @Override
    public void addTblCmsInvoiceDetails(TblCmsInvoiceDetails cmsInvoiceDetails){
        super.addEntity(cmsInvoiceDetails);
    }

    @Override
    public void deleteTblCmsInvoiceDetails(TblCmsInvoiceDetails cmsInvoiceDetails) {
        super.deleteEntity(cmsInvoiceDetails);
    }

    @Override
    public void updateTblCmsInvoiceDetails(TblCmsInvoiceDetails cmsInvoiceDetails) {
        super.updateEntity(cmsInvoiceDetails);
    }

    @Override
    public List<TblCmsInvoiceDetails> getAllTblCmsInvoiceDetails() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsInvoiceDetails> findTblCmsInvoiceDetails(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsInvoiceDetailsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsInvoiceDetails> findByCountTblCmsInvoiceDetails(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsInvoiceDetails> estCost) {
         super.updateAll(estCost);
    }
}
