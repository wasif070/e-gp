package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblExternalMemInfo;
import java.util.List;

/**
 *
 * @author Rishita
 */
public interface TblExternalMemInfoDao extends GenericDao<TblExternalMemInfo> {

    public void addTblExternalMemInfo(TblExternalMemInfo tblObj);

    public void deleteTblExternalMemInfo(TblExternalMemInfo tblObj);

    public void updateTblExternalMemInfo(TblExternalMemInfo tblObj);

    public List<TblExternalMemInfo> getAllTblExternalMemInfo();

    public List<TblExternalMemInfo> findTblExternalMemInfo(Object... values) throws Exception;

    public List<TblExternalMemInfo> findByCountTblExternalMemInfo(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblExternalMemInfoCount();
}
