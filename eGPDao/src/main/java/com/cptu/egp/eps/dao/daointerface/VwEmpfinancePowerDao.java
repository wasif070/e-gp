package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.view.VwEmpfinancePower;
import java.util.List;

/**
 *
 * @author taher
 */
public interface VwEmpfinancePowerDao extends GenericDao<VwEmpfinancePower>{

    public List<VwEmpfinancePower> getAllEmpfinancePower();

    public List<VwEmpfinancePower> findEmpfinancePower(Object... values) throws Exception;

    public List<VwEmpfinancePower> findByCountEmpfinancePower(int firstResult,int maxResult,Object... values) throws Exception;

}
