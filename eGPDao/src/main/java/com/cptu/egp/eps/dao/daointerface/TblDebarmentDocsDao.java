package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblDebarmentDocs;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblDebarmentDocsDao extends GenericDao<TblDebarmentDocs> {

    public void addTblDebarmentDocs(TblDebarmentDocs debar);

    public void deleteTblDebarmentDocs(TblDebarmentDocs debar);

    public void updateTblDebarmentDocs(TblDebarmentDocs debar);

    public List<TblDebarmentDocs> getAllTblDebarmentDocs();

    public List<TblDebarmentDocs> findTblDebarmentDocs(Object... values) throws Exception;

    public List<TblDebarmentDocs> findByCountTblDebarmentDocs(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblDebarmentDocsCount();
}
