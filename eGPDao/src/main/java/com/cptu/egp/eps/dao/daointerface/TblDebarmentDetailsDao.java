package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblDebarmentDetails;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblDebarmentDetailsDao extends GenericDao<TblDebarmentDetails> {

    public void addTblDebarmentDetails(TblDebarmentDetails tblObj);

    public void deleteTblDebarmentDetails(TblDebarmentDetails tblObj);

    public void updateTblDebarmentDetails(TblDebarmentDetails tblObj);

    public List<TblDebarmentDetails> getAllTblDebarmentDetails();

    public List<TblDebarmentDetails> findTblDebarmentDetails(Object... values) throws Exception;

    public List<TblDebarmentDetails> findByCountTblDebarmentDetails(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblDebarmentDetailsCount();
}
