package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCronJobsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCronJobs;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCronJobsImpl extends AbcAbstractClass<TblCronJobs> implements TblCronJobsDao {

    @Override
    public void addTblCronJobs(TblCronJobs cronJobs){
        super.addEntity(cronJobs);
    }

    @Override
    public void deleteTblCronJobs(TblCronJobs cronJobs) {
        super.deleteEntity(cronJobs);
    }

    @Override
    public void updateTblCronJobs(TblCronJobs cronJobs) {
        super.updateEntity(cronJobs);
    }

    @Override
    public List<TblCronJobs> getAllTblCronJobs() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCronJobs> findTblCronJobs(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCronJobsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCronJobs> findByCountTblCronJobs(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
