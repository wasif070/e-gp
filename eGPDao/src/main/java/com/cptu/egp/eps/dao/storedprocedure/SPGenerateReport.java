/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Ramesh
 */
public class SPGenerateReport extends StoredProcedure{
    private static final Logger LOGGER = Logger.getLogger(SPGenerateReport.class);
    public SPGenerateReport(BasicDataSource dataSource,String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("v_rptTableId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_columnId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_userId_inInt", Types.INTEGER));
    }

    /**
     * Execute stored procedure : p_generatereport
     * To generate data for tender bid report.
     * @param rptTableId
     * @param columnId
     * @param userId
     * @return String value having report details.
     */
    public String exeProcedure(Integer rptTableId,Integer columnId,Integer userId ){
         Map inParams = new HashMap();
          inParams.put("v_rptTableId_inInt",rptTableId);
           inParams.put("v_columnId_inInt",columnId);
           inParams.put("v_userId_inInt",userId);
           this.compile();
           String result=null;
           try {           
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            if (list != null && !list.isEmpty()) {
                for (LinkedHashMap<String, Object> linkedHashMap : list) {
                    result = (String) linkedHashMap.get("result");
                }
            }
            LOGGER.debug("No data found ");
        } catch (Exception e) {
            LOGGER.error("SPGenerateReport : "+ e);
        }
        return result;
    }

}
