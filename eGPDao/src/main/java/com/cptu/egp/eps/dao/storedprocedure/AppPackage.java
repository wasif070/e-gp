/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Ramesh.Janagondakuru
 */
public class AppPackage extends StoredProcedure {
    private static final Logger LOGGER = Logger.getLogger(AppPackage.class);
    public AppPackage(BasicDataSource dataSource, String procName){
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("v_AppId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_Procurementnature_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_ServicesType_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_PackageNo_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_PackageDesc_inVc", Types.VARCHAR));        
        this.declareParameter(new SqlParameter("v_AllocateBudget_inM", Types.DECIMAL));
        this.declareParameter(new SqlParameter("v_EstimatedCost_inM", Types.DECIMAL));
        this.declareParameter(new SqlParameter("v_CpvCode_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_PkgEstCode_inM", Types.DECIMAL));
        this.declareParameter(new SqlParameter("v_ApprovingAuthEmpId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_IsPQRequired_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_ReoiRfaRequired_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_ProcurementMethodId_intInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_ProcurementType_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_SourceOfFund_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_AppStatus_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_WorkflowStatus_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_NoOfStages_intInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_Action_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_PackageId_inInt", Types.INTEGER));                
        //  Lot InParams --- Start ---
        this.declareParameter(new SqlParameter("v_LotNo_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_LotDesc_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Quantity_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_Unit_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_LotEstCost_inVc", Types.VARCHAR));
        //this.declareParameter(new SqlParameter("v_LotEstCost_inM", Types.DECIMAL));
        //  Lot InParams --- End ---
        this.declareParameter(new SqlParameter("v_pkgUrgency_inVc", Types.VARCHAR));
        //aprojit-Start
        this.declareParameter(new SqlParameter("v_bidderCategory_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_workCategory_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_depoplanWork_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_entrustingAgency_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_timeFrame_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_w1", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_w2", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_w3", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_w4", Types.VARCHAR));
        //aprojit-End
    }

    /**
     * Execute stored procedure : p_ins_upd_apppackage
     * to performed insert,update,delete operation for APP Package.
     * @param appId
     * @param procurementnature
     * @param servicesType
     * @param packageNo
     * @param packageDesc
     * @param allocateBudget
     * @param estimatedCost
     * @param cpvCode
     * @param pkgEstCode
     * @param approvingAuthEmpId
     * @param isPQRequired
     * @param reoiRfaRequired
     * @param procurementMethodId
     * @param procurementType
     * @param sourceOfFund
     * @param appStatus
     * @param workflowStatus
     * @param noOfStages
     * @param action
     * @param packageId
     * @param lotNo
     * @param lotDesc
     * @param qty
     * @param unit
     * @param lotEstCost
     * @param pkgUrgency
     * @return CommonSPReturn list contains flags indicate operation success status.
     */
     public List<CommonSPReturn> executeProcedure(Integer appId,String procurementnature,String  servicesType,String packageNo,String packageDesc, String depoplanWork, String entrustingAgency, String timeFrame, String bidderCategory, String workCategory, BigDecimal allocateBudget,BigDecimal estimatedCost,String cpvCode,BigDecimal pkgEstCode,Integer approvingAuthEmpId,String isPQRequired,String reoiRfaRequired,Integer procurementMethodId,String procurementType,String sourceOfFund,String appStatus,String workflowStatus,Integer noOfStages,String action,Integer packageId,String lotNo,String lotDesc,String qty,String unit,String lotEstCost,String pkgUrgency,String w1, String w2, String w3, String w4){
        Map inParams = new HashMap();
        inParams.put("v_AppId_inInt", appId);
        inParams.put("v_Procurementnature_inVc", procurementnature);
        inParams.put("v_ServicesType_inVc", servicesType);
        inParams.put("v_PackageNo_inVc", packageNo); 
        inParams.put("v_PackageDesc_inVc", packageDesc);
        inParams.put("v_AllocateBudget_inM", allocateBudget);
        inParams.put("v_EstimatedCost_inM", estimatedCost);
        inParams.put("v_CpvCode_inVc", cpvCode);
        inParams.put("v_PkgEstCode_inM", pkgEstCode);
        inParams.put("v_ApprovingAuthEmpId_inInt", approvingAuthEmpId);
        inParams.put("v_IsPQRequired_inVc", isPQRequired);
        inParams.put("v_ReoiRfaRequired_inVc", reoiRfaRequired);
        inParams.put("v_ProcurementMethodId_intInt", procurementMethodId);
        inParams.put("v_ProcurementType_inVc", procurementType);
        inParams.put("v_SourceOfFund_inVc", sourceOfFund);
        inParams.put("v_AppStatus_inVc", appStatus);
        inParams.put("v_WorkflowStatus_inVc", workflowStatus);
        inParams.put("v_NoOfStages_intInt", noOfStages);
        inParams.put("v_Action_inVc", action);
        inParams.put("v_PackageId_inInt", packageId);
        //  Lot InParams --- Start ---
        inParams.put("v_LotNo_inVc", lotNo);
        inParams.put("v_LotDesc_inVc", lotDesc);
        inParams.put("v_Quantity_inVc", qty);
        inParams.put("v_Unit_inVc", unit);
        inParams.put("v_LotEstCost_inVc", lotEstCost);
        /*inParams.put("v_LotEstCost_inM", lotEstCost);*/
        //  Lot InParams --- End ---
        inParams.put("v_pkgUrgency_inVc", pkgUrgency);
      //aprojit-Start
        inParams.put("v_bidderCategory_inVc", bidderCategory);
        inParams.put("v_workCategory_inVc", workCategory);
        inParams.put("v_depoplanWork_inVc", depoplanWork);
        inParams.put("v_entrustingAgency_inVc", entrustingAgency);
        inParams.put("v_timeFrame_inVc", timeFrame);
        inParams.put("v_w1",w1);
        inParams.put("v_w2",w2);
        inParams.put("v_w3",w3);
        inParams.put("v_w4",w4);
      //aprojit-End
        this.compile();
        List<CommonSPReturn> details = new ArrayList<CommonSPReturn>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonSPReturn commonSPReturn = new CommonSPReturn();
                commonSPReturn.setId(new BigDecimal(Integer.toString((Integer)linkedHashMap.get("PackageId"))));
                commonSPReturn.setFlag((Boolean)linkedHashMap.get("flag"));
                commonSPReturn.setMsg((String) linkedHashMap.get("Message"));
                details.add(commonSPReturn);
            }
        } catch (Exception e) {
           LOGGER.error("AppPackage : " + e);
        }
        return details;
     }

}
