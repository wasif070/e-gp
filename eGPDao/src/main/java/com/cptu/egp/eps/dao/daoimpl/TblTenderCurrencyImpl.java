/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderCurrencyDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderCurrency;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderCurrencyImpl extends AbcAbstractClass<TblTenderCurrency> implements TblTenderCurrencyDao {

    @Override
    public void addTblTenderCurrency(TblTenderCurrency admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblTenderCurrency(TblTenderCurrency admin) {
        super.deleteEntity(admin);        
    }

    @Override
    public void updateTblTenderCurrency(TblTenderCurrency admin) {
        super.updateEntity(admin);        
    }

    @Override
    public List<TblTenderCurrency> getAllTblTenderCurrency() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderCurrency> findTblTenderCurrency(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderCurrencyCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderCurrency> findByCountTblTenderCurrency(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
