package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblTemplateGuildeLinesDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTemplateGuildeLines;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblTemplateGuideLinesImpl extends AbcAbstractClass<TblTemplateGuildeLines> implements TblTemplateGuildeLinesDao {

    @Override
    public void addTblTemplateGuildeLines(TblTemplateGuildeLines templateGuildeLines){
        super.addEntity(templateGuildeLines);
    }

    @Override
    public void deleteTblTemplateGuildeLines(TblTemplateGuildeLines templateGuildeLines) {
        super.deleteEntity(templateGuildeLines);
    }

    @Override
    public void updateTblTemplateGuildeLines(TblTemplateGuildeLines templateGuildeLines) {
        super.updateEntity(templateGuildeLines);
    }

    @Override
    public List<TblTemplateGuildeLines> getAllTblTemplateGuildeLines() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTemplateGuildeLines> findTblTemplateGuildeLines(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTemplateGuildeLinesCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTemplateGuildeLines> findByCountTblTemplateGuildeLines(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
