/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblCmsVendorRatingMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsVendorRatingMaster;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public class TblCmsVendorRatingMasterDaoImpl extends AbcAbstractClass<TblCmsVendorRatingMaster> implements TblCmsVendorRatingMasterDao {

    @Override
    public void addTblCmsVendorRatingMaster(TblCmsVendorRatingMaster tblCmsVendorRatingMaster) {
        super.addEntity(tblCmsVendorRatingMaster);
    }

    @Override
    public void deleteTblCmsVendorRatingMaster(TblCmsVendorRatingMaster tblCmsVendorRatingMaster) {
        super.deleteEntity(tblCmsVendorRatingMaster);
    }

    @Override
    public void updateTblCmsVendorRatingMaster(TblCmsVendorRatingMaster tblCmsVendorRatingMaster) {
        super.updateEntity(tblCmsVendorRatingMaster);
    }

    @Override
    public List<TblCmsVendorRatingMaster> getAllTblCmsVendorRatingMaster() {
        super.setPersistentClass(TblCmsVendorRatingMaster.class);
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsVendorRatingMaster> findTblCmsVendorRatingMaster(Object... values) throws Exception {
        super.setPersistentClass(TblCmsVendorRatingMaster.class);
        return super.findEntity(values);
    }

    @Override
    public List<TblCmsVendorRatingMaster> findByCountTblCmsVendorRatingMaster(int firstResult, int maxResult, Object... values) throws Exception {
        super.setPersistentClass(TblCmsVendorRatingMaster.class);
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblCmsVendorRatingMasterCount() {
        super.setPersistentClass(TblCmsVendorRatingMaster.class);
        return super.getEntityCount();
    }
}
