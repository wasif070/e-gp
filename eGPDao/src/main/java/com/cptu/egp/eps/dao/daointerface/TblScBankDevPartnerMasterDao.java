package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblScBankDevPartnerMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblScBankDevPartnerMasterDao extends GenericDao<TblScBankDevPartnerMaster> {

    public void addTblScBankDevPartnerMaster(TblScBankDevPartnerMaster tblObj);

    public void deleteTblScBankDevPartnerMaster(TblScBankDevPartnerMaster tblObj);

    public void updateTblScBankDevPartnerMaster(TblScBankDevPartnerMaster tblObj);

    public List<TblScBankDevPartnerMaster> getAllTblScBankDevPartnerMaster();

    public List<TblScBankDevPartnerMaster> findTblScBankDevPartnerMaster(Object... values) throws Exception;

    public List<TblScBankDevPartnerMaster> findByCountTblScBankDevPartnerMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblScBankDevPartnerMasterCount();
}
