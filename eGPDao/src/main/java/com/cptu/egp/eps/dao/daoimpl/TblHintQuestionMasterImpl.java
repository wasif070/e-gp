package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblHintQuestionMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblHintQuestionMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblHintQuestionMasterImpl extends AbcAbstractClass<TblHintQuestionMaster> implements TblHintQuestionMasterDao {
   
    @Override
    public List<TblHintQuestionMaster> getAllTblHintQuestionMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblHintQuestionMaster> findTblHintQuestionMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblHintQuestionMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblHintQuestionMaster> findByCountTblHintQuestionMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
