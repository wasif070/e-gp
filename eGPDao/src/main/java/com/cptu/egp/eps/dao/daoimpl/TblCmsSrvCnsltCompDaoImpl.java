package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvCnsltCompDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvCnsltComp;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsSrvCnsltCompDaoImpl extends AbcAbstractClass<TblCmsSrvCnsltComp> implements TblCmsSrvCnsltCompDao {

    @Override
    public void addTblCmsSrvCnsltComp(TblCmsSrvCnsltComp activityMaster){
        super.addEntity(activityMaster);
    }

    @Override
    public void deleteTblCmsSrvCnsltComp(TblCmsSrvCnsltComp activityMaster) {
        super.deleteEntity(activityMaster);
    }

    @Override
    public void updateTblCmsSrvCnsltComp(TblCmsSrvCnsltComp activityMaster) {
        super.updateEntity(activityMaster);
    }

    @Override
    public List<TblCmsSrvCnsltComp> getAllTblCmsSrvCnsltComp() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvCnsltComp> findTblCmsSrvCnsltComp(Object... values) throws Exception {
        return super.findEntity(values);
    }

        @Override
    public long getTblCmsSrvCnsltCompCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvCnsltComp> findByCountTblCmsSrvCnsltComp(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsSrvCnsltComp> estCost) {
       super.updateAll(estCost);
    }
}
