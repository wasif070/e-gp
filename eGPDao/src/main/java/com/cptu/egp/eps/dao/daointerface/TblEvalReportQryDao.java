package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalReportQry;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblEvalReportQryDao extends GenericDao<TblEvalReportQry> {

    public void addTblEvalReportQry(TblEvalReportQry tblEvalReportQry);

    public void deleteTblEvalReportQry(TblEvalReportQry tblEvalReportQry);

    public void deleteAllTblEvalReportQry(Collection tblEvalReportQry);

    public void updateTblEvalReportQry(TblEvalReportQry tblEvalReportQry);

    public List<TblEvalReportQry> getAllTblEvalReportQry();

    public List<TblEvalReportQry> findTblEvalReportQry(Object... values) throws Exception;

    public List<TblEvalReportQry> findByCountTblEvalReportQry(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalReportQryCount();
}
