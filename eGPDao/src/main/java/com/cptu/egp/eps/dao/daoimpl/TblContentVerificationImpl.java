/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblContentVerificationDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblContentVerification;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblContentVerificationImpl extends AbcAbstractClass<TblContentVerification> implements TblContentVerificationDao{

    @Override
    public void addTblContentVerification(TblContentVerification tblContentVerification) {
        super.addEntity(tblContentVerification);
    }

    @Override
    public List<TblContentVerification> findTblContentVerification(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblContentVerification(TblContentVerification tblContentVerification) {

        super.deleteEntity(tblContentVerification);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblContentVerification(Collection tblContentVerification) {
        super.deleteAll(tblContentVerification);
    }

    @Override
    public void updateTblContentVerification(TblContentVerification tblContentVerification) {

        super.updateEntity(tblContentVerification);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblContentVerification> getAllTblContentVerification() {
        return super.getAllEntity();
    }

    @Override
    public long getTblContentVerificationCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblContentVerification> findByCountTblContentVerification(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
