/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

/**
 *
 * @author Administrator
 */
public class CommonFormData {

    private String formName;
    private String formHeader;
    private String formFooter;
    private String isMultipleFormFeeling;
    private String isEncryption;
    private String isPriceBid;
    private String isMandatory;

    private Integer tableId;
    private String tableName;
    private String tableHeader;
    private String tableFooter;
    private Short noOfCols;
    private Short noOfRows;
    private String isMultipleFilling;

    private Short columnId;
    private String columnHeader;
    private Short fillBy;
    private Short dataType;
    private String showOrHide;
    private String columnType;

    private Integer cellId;
    private String cellValue;
    private Integer rowId;
    private Short cellDataType;

    private String formula;
    private String isGrandTotal;

    private Integer count;

    private Integer bidTableId;

    public String getFormFooter() {
        return formFooter;
    }

    public void setFormFooter(String formFooter) {
        this.formFooter = formFooter;
    }

    public String getFormHeader() {
        return formHeader;
    }

    public void setFormHeader(String formHeader) {
        this.formHeader = formHeader;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getIsEncryption() {
        return isEncryption;
    }

    public void setIsEncryption(String isEncryption) {
        this.isEncryption = isEncryption;
    }

    public String getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(String isMandatory) {
        this.isMandatory = isMandatory;
    }

    public String getIsMultipleFormFeeling() {
        return isMultipleFormFeeling;
    }

    public void setIsMultipleFormFeeling(String isMultipleFormFeeling) {
        this.isMultipleFormFeeling = isMultipleFormFeeling;
    }

    public String getIsPriceBid() {
        return isPriceBid;
    }

    public void setIsPriceBid(String isPriceBid) {
        this.isPriceBid = isPriceBid;
    }

    public Integer getBidTableId() {
        return bidTableId;
    }

    public void setBidTableId(Integer bidTableId) {
        this.bidTableId = bidTableId;
    }

    
    public Short getCellDataType() {
        return cellDataType;
    }

    public void setCellDataType(Short cellDataType) {
        this.cellDataType = cellDataType;
    }

    public Integer getCellId() {
        return cellId;
    }

    public void setCellId(Integer cellId) {
        this.cellId = cellId;
    }

    public String getCellValue() {
        return cellValue;
    }

    public void setCellValue(String cellValue) {
        this.cellValue = cellValue;
    }

    public String getColumnHeader() {
        return columnHeader;
    }

    public void setColumnHeader(String columnHeader) {
        this.columnHeader = columnHeader;
    }

    public Short getColumnId() {
        return columnId;
    }

    public void setColumnId(Short columnId) {
        this.columnId = columnId;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public Short getDataType() {
        return dataType;
    }

    public void setDataType(Short dataType) {
        this.dataType = dataType;
    }

    public Short getFillBy() {
        return fillBy;
    }

    public void setFillBy(Short fillBy) {
        this.fillBy = fillBy;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public String getIsGrandTotal() {
        return isGrandTotal;
    }

    public void setIsGrandTotal(String isGrandTotal) {
        this.isGrandTotal = isGrandTotal;
    }

    public Integer getRowId() {
        return rowId;
    }

    public void setRowId(Integer rowId) {
        this.rowId = rowId;
    }

    public String getShowOrHide() {
        return showOrHide;
    }

    public void setShowOrHide(String showOrHide) {
        this.showOrHide = showOrHide;
    }

    public String getTableFooter() {
        return tableFooter;
    }

    public void setTableFooter(String tableFooter) {
        this.tableFooter = tableFooter;
    }

    public String getTableHeader() {
        return tableHeader;
    }

    public void setTableHeader(String tableHeader) {
        this.tableHeader = tableHeader;
    }

    public Integer getTableId() {
        return tableId;
    }

    public void setTableId(Integer tableId) {
        this.tableId = tableId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getIsMultipleFilling() {
        return isMultipleFilling;
    }

    public void setIsMultipleFilling(String isMultipleFilling) {
        this.isMultipleFilling = isMultipleFilling;
    }

    public Short getNoOfCols() {
        return noOfCols;
    }

    public void setNoOfCols(Short noOfCols) {
        this.noOfCols = noOfCols;
    }

    public Short getNoOfRows() {
        return noOfRows;
    }

    public void setNoOfRows(Short noOfRows) {
        this.noOfRows = noOfRows;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
    
}
