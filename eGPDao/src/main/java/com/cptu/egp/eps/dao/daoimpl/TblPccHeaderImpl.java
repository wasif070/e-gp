/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblPccHeaderDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblPccHeader;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblPccHeaderImpl extends AbcAbstractClass<TblPccHeader> implements TblPccHeaderDao{

    @Override
    public void addTblPccHeader(TblPccHeader tblPccHeader) {
        super.addEntity(tblPccHeader);
    }

    @Override
    public List<TblPccHeader> findTblPccHeader(Object... values) throws Exception {
        return super.findEntity(values);
       
    }

    @Override
    public void deleteTblPccHeader(TblPccHeader tblPccHeader) {

        super.deleteEntity(tblPccHeader);
      
    }

    @Override
    public void updateTblPccHeader(TblPccHeader tblPccHeader) {

        super.updateEntity(tblPccHeader);
        
    }

    @Override
    public List<TblPccHeader> getAllTblPccHeader() {
        return super.getAllEntity();
    }

    @Override
    public long getTblPccHeaderCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblPccHeader> findByCountTblPccHeader(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
