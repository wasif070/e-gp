/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderTdsSubClauseDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderTdsSubClause;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderTdsSubClauseImpl extends AbcAbstractClass<TblTenderTdsSubClause> implements TblTenderTdsSubClauseDao {

    @Override
    public void addTblTenderTdsSubClause(TblTenderTdsSubClause admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblTenderTdsSubClause(TblTenderTdsSubClause admin) {
        super.deleteEntity(admin);        
    }

    @Override
    public void updateTblTenderTdsSubClause(TblTenderTdsSubClause admin) {
        super.updateEntity(admin);        
    }

    @Override
    public List<TblTenderTdsSubClause> getAllTblTenderTdsSubClause() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderTdsSubClause> findTblTenderTdsSubClause(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderTdsSubClauseCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderTdsSubClause> findByCountTblTenderTdsSubClause(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
