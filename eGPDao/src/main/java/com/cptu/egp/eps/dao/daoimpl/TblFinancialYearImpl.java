package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblFinancialYearDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblFinancialYear;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblFinancialYearImpl extends AbcAbstractClass<TblFinancialYear> implements TblFinancialYearDao {

    @Override
    public void addTblFinancialYear(TblFinancialYear financialYear){
        super.addEntity(financialYear);
    }

    @Override
    public void deleteTblFinancialYear(TblFinancialYear financialYear) {
        super.deleteEntity(financialYear);
    }

    @Override
    public void updateTblFinancialYear(TblFinancialYear financialYear) {
        super.updateEntity(financialYear);
    }

    @Override
    public List<TblFinancialYear> getAllTblFinancialYear() {
        return super.getAllEntity();
    }

    @Override
    public List<TblFinancialYear> findTblFinancialYear(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblFinancialYearCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblFinancialYear> findByCountTblFinancialYear(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
