/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderPaymentStatusDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderPaymentStatus;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderPaymentStatusImpl extends AbcAbstractClass<TblTenderPaymentStatus> implements TblTenderPaymentStatusDao{

    @Override
    public void addTblTenderPaymentStatus(TblTenderPaymentStatus tblTenderPaymentStatus) {
        super.addEntity(tblTenderPaymentStatus);
    }

    @Override
    public List<TblTenderPaymentStatus> findTblTenderPaymentStatus(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblTenderPaymentStatus(TblTenderPaymentStatus tblTenderPaymentStatus) {

        super.deleteEntity(tblTenderPaymentStatus);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteAllTblTenderPaymentStatus(Collection tblTenderPaymentStatus) {
        super.deleteAll(tblTenderPaymentStatus);
    }

    @Override
    public void updateTblTenderPaymentStatus(TblTenderPaymentStatus tblTenderPaymentStatus) {

        super.updateEntity(tblTenderPaymentStatus);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblTenderPaymentStatus> getAllTblTenderPaymentStatus() {
        return super.getAllEntity();
    }

    @Override
    public long getTblTenderPaymentStatusCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderPaymentStatus> findByCountTblTenderPaymentStatus(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
