package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblBidderRank;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblBidderRankDao extends GenericDao<TblBidderRank> {

    public void addTblBidderRank(TblBidderRank tblBidderRank);

    public void deleteTblBidderRank(TblBidderRank tblBidderRank);

    public void deleteAllTblBidderRank(Collection tblBidderRank);

    public void updateTblBidderRank(TblBidderRank tblBidderRank);

    public List<TblBidderRank> getAllTblBidderRank();

    public List<TblBidderRank> findTblBidderRank(Object... values) throws Exception;

    public List<TblBidderRank> findByCountTblBidderRank(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblBidderRankCount();

    public void updateOrSaveBidderRank(List<TblBidderRank> bidderRanks);
}
