package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblTempTendererMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTempTendererMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblTempTendererMasterImpl extends AbcAbstractClass<TblTempTendererMaster> implements TblTempTendererMasterDao {

    @Override
    public void addTblTempTendererMaster(TblTempTendererMaster master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblTempTendererMaster(TblTempTendererMaster master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblTempTendererMaster(TblTempTendererMaster master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblTempTendererMaster> getAllTblTempTendererMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTempTendererMaster> findTblTempTendererMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTempTendererMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTempTendererMaster> findByCountTblTempTendererMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
