/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblUserPrefrence;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public interface TblUserPrefrenceDao extends GenericDao<TblUserPrefrence> {

    public void addTblUserPrefrence(TblUserPrefrence user);

    public void deleteTblUserPrefrence(TblUserPrefrence user);

    public void updateTblUserPrefrence(TblUserPrefrence user);

    public List<TblUserPrefrence> getAllTblUserPrefrence();

    public List<TblUserPrefrence> findTblUserPrefrence(Object... values) throws Exception;

    public List<TblUserPrefrence> findByCountTblUserPrefrence(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblUserPrefrenceCount();
}
