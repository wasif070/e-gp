/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblPreTenderQuery;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblPreTenderQueryDao extends GenericDao<TblPreTenderQuery>{

    public void addTblPreTenderQuery(TblPreTenderQuery tblObj);

    public void deleteTblPreTenderQuery(TblPreTenderQuery tblObj);

    public void updateTblPreTenderQuery(TblPreTenderQuery tblObj);

    public List<TblPreTenderQuery> getAllTblPreTenderQuery();

    public List<TblPreTenderQuery> findTblPreTenderQuery(Object... values) throws Exception;

    public List<TblPreTenderQuery> findByCountTblPreTenderQuery(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblPreTenderQueryCount();
}
