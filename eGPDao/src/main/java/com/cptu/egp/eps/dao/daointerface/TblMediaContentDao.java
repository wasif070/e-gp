package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblMediaContent;
import java.util.List;

/**
 *
 * @author Ketan Prajapati
 */
public interface TblMediaContentDao extends GenericDao<TblMediaContent> {

    public void addTblMediaContent(TblMediaContent tblMediaContent);

    public void deleteTblMediaContent(TblMediaContent tblMediaContent);

    public void updateTblMediaContent(TblMediaContent tblMediaContent);

    public List<TblMediaContent> getAllTblMediaContent();

    public List<TblMediaContent> findTblMediaContent(Object... values) throws Exception;

    public List<TblMediaContent> findByCountTblMediaContent(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblMediaContentCount();
}
