package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsRomasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsRomaster;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsRomasterImpl extends AbcAbstractClass<TblCmsRomaster> implements TblCmsRomasterDao {

    @Override
    public void addTblCmsRomaster(TblCmsRomaster cmsInvoiceDetails){
        super.addEntity(cmsInvoiceDetails);
    }

    @Override
    public void deleteTblCmsRomaster(TblCmsRomaster cmsInvoiceDetails) {
        super.deleteEntity(cmsInvoiceDetails);
    }

    @Override
    public void updateTblCmsRomaster(TblCmsRomaster cmsInvoiceDetails) {
        super.updateEntity(cmsInvoiceDetails);
    }

    @Override
    public List<TblCmsRomaster> getAllTblCmsRomaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsRomaster> findTblCmsRomaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsRomasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsRomaster> findByCountTblCmsRomaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }


    @Override
    public void updateInsertAllTblCmsRomaster(List<TblCmsRomaster> list) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
