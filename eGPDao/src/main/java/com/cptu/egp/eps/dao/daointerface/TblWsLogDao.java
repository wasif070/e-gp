/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblWsLog;
import java.util.List;

/**
 *
 * @author sreenu
 */
public interface TblWsLogDao extends GenericDao<TblWsLog>{

    public void addTblWsLog(TblWsLog tblWsLog);

    public void deleteTblWsLog(TblWsLog tblWsLog);

    public void updateTblWsLog(TblWsLog tblWsLog);

    public List<TblWsLog> getAllTblWsLogs();

    public List<TblWsLog> findTblWsLog(Object... values) throws Exception;

    public List<TblWsLog> findByCountTblWsLog(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblWsLogCount();

}
