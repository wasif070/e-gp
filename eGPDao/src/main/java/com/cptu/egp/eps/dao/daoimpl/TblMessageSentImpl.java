package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblMessageSentDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblMessageSent;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblMessageSentImpl extends AbcAbstractClass<TblMessageSent> implements TblMessageSentDao {

    @Override
    public void addTblMessageSent(TblMessageSent master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblMessageSent(TblMessageSent master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblMessageSent(TblMessageSent master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblMessageSent> getAllTblMessageSent() {
        return super.getAllEntity();
    }

    @Override
    public List<TblMessageSent> findTblMessageSent(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblMessageSentCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblMessageSent> findByCountTblMessageSent(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
