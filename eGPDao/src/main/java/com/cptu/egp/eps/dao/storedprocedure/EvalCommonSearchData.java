/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.util.Date;

/**
 *
 * @author Istiak (Dohatec) - 21.Apr.15
 */
public class EvalCommonSearchData {

    private String fieldName1;
    private String fieldName2;
    private String fieldName3;
    private String fieldName4;
    private String fieldName5;
    private String fieldName6;
    private String fieldName7;
    private String fieldName8;
    private String fieldName9;
    private String fieldName10;
    private String fieldName11;
    private String fieldName12;
    private String fieldName13;
    private String fieldName14;
    private String fieldName15;
   // private Date fieldName16;

    /**
     * @return the fieldName1
     */
    public String getFieldName1() {
        return fieldName1;
    }

    /**
     * @param fieldName1 the fieldName1 to set
     */
    public void setFieldName1(String fieldName1) {
        this.fieldName1 = fieldName1;
    }

    /**
     * @return the fieldName2
     */
    public String getFieldName2() {
        return fieldName2;
    }

    /**
     * @param fieldName2 the fieldName2 to set
     */
    public void setFieldName2(String fieldName2) {
        this.fieldName2 = fieldName2;
    }
// //add by sristy set data type date
    /**
     * @return the fieldName16
     */
   /* public Date getFieldName16() {
        return fieldName16;
    }

    /**
     * @param fieldName16 the fieldName16 to set
     */
  /*  public void setFieldName16(Date fieldName16) {
        this.fieldName16 = fieldName16;
    }*/
//end by sristy
    /**
     * @return the fieldName3
     */
    public String getFieldName3() {
        return fieldName3;
    }

    /**
     * @param fieldName3 the fieldName3 to set
     */
    public void setFieldName3(String fieldName3) {
        this.fieldName3 = fieldName3;
    }

    /**
     * @return the fieldName4
     */
    public String getFieldName4() {
        return fieldName4;
    }

    /**
     * @param fieldName4 the fieldName4 to set
     */
    public void setFieldName4(String fieldName4) {
        this.fieldName4 = fieldName4;
    }

    /**
     * @return the fieldName5
     */
    public String getFieldName5() {
        return fieldName5;
    }

    /**
     * @param fieldName5 the fieldName5 to set
     */
    public void setFieldName5(String fieldName5) {
        this.fieldName5 = fieldName5;
    }

    /**
     * @return the fieldName6
     */
    public String getFieldName6() {
        return fieldName6;
    }

    /**
     * @param fieldName6 the fieldName6 to set
     */
    public void setFieldName6(String fieldName6) {
        this.fieldName6 = fieldName6;
    }

    /**
     * @return the fieldName7
     */
    public String getFieldName7() {
        return fieldName7;
    }

    /**
     * @param fieldName7 the fieldName7 to set
     */
    public void setFieldName7(String fieldName7) {
        this.fieldName7 = fieldName7;
    }

    /**
     * @return the fieldName8
     */
    public String getFieldName8() {
        return fieldName8;
    }

    /**
     * @param fieldName8 the fieldName8 to set
     */
    public void setFieldName8(String fieldName8) {
        this.fieldName8 = fieldName8;
    }

    /**
     * @return the fieldName9
     */
    public String getFieldName9() {
        return fieldName9;
    }

    /**
     * @param fieldName9 the fieldName9 to set
     */
    public void setFieldName9(String fieldName9) {
        this.fieldName9 = fieldName9;
    }

    /**
     * @return the fieldName10
     */
    public String getFieldName10() {
        return fieldName10;
    }

    /**
     * @param fieldName10 the fieldName10 to set
     */
    public void setFieldName10(String fieldName10) {
        this.fieldName10 = fieldName10;
    }

    /**
     * @return the fieldName11
     */
    public String getFieldName11() {
        return fieldName11;
    }

    /**
     * @param fieldName11 the fieldName11 to set
     */
    public void setFieldName11(String fieldName11) {
        this.fieldName11 = fieldName11;
    }

    /**
     * @return the fieldName12
     */
    public String getFieldName12() {
        return fieldName12;
    }

    /**
     * @param fieldName12 the fieldName12 to set
     */
    public void setFieldName12(String fieldName12) {
        this.fieldName12 = fieldName12;
    }

    /**
     * @return the fieldName13
     */
    public String getFieldName13() {
        return fieldName13;
    }

    /**
     * @param fieldName13 the fieldName13 to set
     */
    public void setFieldName13(String fieldName13) {
        this.fieldName13 = fieldName13;
    }

    /**
     * @return the fieldName14
     */
    public String getFieldName14() {
        return fieldName14;
    }

    /**
     * @param fieldName14 the fieldName14 to set
     */
    public void setFieldName14(String fieldName14) {
        this.fieldName14 = fieldName14;
    }

    /**
     * @return the fieldName15
     */
    public String getFieldName15() {
        return fieldName15;
    }

    /**
     * @param fieldName15 the fieldName15 to set
     */
    public void setFieldName15(String fieldName15) {
        this.fieldName15 = fieldName15;
    }
}
