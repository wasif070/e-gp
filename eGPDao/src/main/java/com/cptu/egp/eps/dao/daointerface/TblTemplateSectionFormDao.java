/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTemplateSectionForm;
import java.util.List;

/**
 *
 * @author yanki
 */
public interface TblTemplateSectionFormDao extends  GenericDao<TblTemplateSectionForm>{

    public void addTblTemplateForm(TblTemplateSectionForm templateSectionForm);

    public void deleteTblTemplateForm(TblTemplateSectionForm templateSectionForm);

    public void updateTblTemplateForm(TblTemplateSectionForm templateSectionForm);

    public List<TblTemplateSectionForm> getAllTblTemplateForm();

    public List<TblTemplateSectionForm> findTblTemplateForm(Object... values) throws Exception;

}
