package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblUserTypeMaster;
import java.util.List;
/**
 *
 * @author taher
 */
public interface TblUserTypeMasterDao extends GenericDao<TblUserTypeMaster> {

    public void addTblUserTypeMaster(TblUserTypeMaster tblusermaster);

    public void deleteTblUserTypeMaster(TblUserTypeMaster tblusermaster);

    public void updateTblUserTypeMaster(TblUserTypeMaster tblusermaster);

    public List<TblUserTypeMaster> getAllTblUserTypeMaster();

    public List<TblUserTypeMaster> findTblUserTypeMaster(Object... values) throws Exception;

    public List<TblUserTypeMaster> findByCountTblUserTypeMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblUserTypeMasterCount();
}
