package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderEstCost;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblTenderEstCostDao extends GenericDao<TblTenderEstCost> {

    public void addTblTenderEstCost(TblTenderEstCost tblObj);

    public void deleteTblTenderEstCost(TblTenderEstCost tblObj);

    public void updateTblTenderEstCost(TblTenderEstCost tblObj);

    public List<TblTenderEstCost> getAllTblTenderEstCost();

    public List<TblTenderEstCost> findTblTenderEstCost(Object... values) throws Exception;

    public List<TblTenderEstCost> findByCountTblTenderEstCost(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderEstCostCount();

    public void updateOrSaveEstCost(List<TblTenderEstCost> tblObj);
}
