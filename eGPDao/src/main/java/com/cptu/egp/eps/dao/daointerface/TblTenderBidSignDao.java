/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderBidSign;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderBidSignDao extends GenericDao<TblTenderBidSign>{

    public void addTblTenderBidSign(TblTenderBidSign tblObj);

    public void deleteTblTenderBidSign(TblTenderBidSign tblObj);

    public void updateTblTenderBidSign(TblTenderBidSign tblObj);

    public List<TblTenderBidSign> getAllTblTenderBidSign();

    public List<TblTenderBidSign> findTblTenderBidSign(Object... values) throws Exception;

    public List<TblTenderBidSign> findByCountTblTenderBidSign(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderBidSignCount();
}
