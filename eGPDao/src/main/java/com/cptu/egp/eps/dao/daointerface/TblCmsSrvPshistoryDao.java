package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvPshistory;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsSrvPshistoryDao extends GenericDao<TblCmsSrvPshistory> {

    public void addTblCmsSrvPshistory(TblCmsSrvPshistory event);

    public void deleteTblCmsSrvPshistory(TblCmsSrvPshistory event);

    public void updateTblCmsSrvPshistory(TblCmsSrvPshistory event);

    public List<TblCmsSrvPshistory> getAllTblCmsSrvPshistory();

    public List<TblCmsSrvPshistory> findTblCmsSrvPshistory(Object... values) throws Exception;

    public List<TblCmsSrvPshistory> findByCountTblCmsSrvPshistory(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsSrvPshistoryCount();

    public void updateOrSaveEstCost(List<TblCmsSrvPshistory> estCost);
}
