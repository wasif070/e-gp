/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblWorkFlowFileHistoryDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblWorkFlowFileHistory;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public class TblWorkFlowFileHistoryImpl extends AbcAbstractClass<TblWorkFlowFileHistory> implements TblWorkFlowFileHistoryDao {

    @Override
    public void addTblWorkFlowFileHistory(TblWorkFlowFileHistory workFlowFileHistory) {

        super.addEntity(workFlowFileHistory);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblWorkFlowFileHistory(TblWorkFlowFileHistory workFlowFileHistory) {

        super.deleteEntity(workFlowFileHistory);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblWorkFlowFileHistory(TblWorkFlowFileHistory workFlowFileHistory) {

        super.updateEntity(workFlowFileHistory);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblWorkFlowFileHistory> getAllTblWorkFlowFileHistory() {
        return super.getAllEntity();
    }

    @Override
    public List<TblWorkFlowFileHistory> findTblWorkFlowFileHistory(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblWorkFlowFileHistoryCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblWorkFlowFileHistory> findByCountTblWorkFlowFileHistory(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
