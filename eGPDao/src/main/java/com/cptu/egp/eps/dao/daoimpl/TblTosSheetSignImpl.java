/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTosSheetSignDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTosSheetSign;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTosSheetSignImpl extends AbcAbstractClass<TblTosSheetSign> implements TblTosSheetSignDao{

    @Override
    public void addTblTosSheetSign(TblTosSheetSign tblTosSheetSign) {
        super.addEntity(tblTosSheetSign);
    }

    @Override
    public List<TblTosSheetSign> findTblTosSheetSign(Object... values) throws Exception {
        return super.findEntity(values);
        
    }

    @Override
    public void deleteTblTosSheetSign(TblTosSheetSign tblTosSheetSign) {

        super.deleteEntity(tblTosSheetSign);
       
    }

    @Override
    public void updateTblTosSheetSign(TblTosSheetSign tblTosSheetSign) {

        super.updateEntity(tblTosSheetSign);
        
    }

    @Override
    public List<TblTosSheetSign> getAllTblTosSheetSign() {
        return super.getAllEntity();
    }

    @Override
    public long getTblTosSheetSignCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTosSheetSign> findByCountTblTosSheetSign(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
