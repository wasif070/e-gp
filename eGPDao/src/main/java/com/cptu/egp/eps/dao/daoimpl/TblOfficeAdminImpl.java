/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblOfficeAdminDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblOfficeAdmin;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class TblOfficeAdminImpl extends AbcAbstractClass<TblOfficeAdmin> implements TblOfficeAdminDao {

    @Override
    public void addTblOfficeAdmin(TblOfficeAdmin officeadmin) {

        super.addEntity(officeadmin);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblOfficeAdmin(TblOfficeAdmin officeadmin) {

        super.deleteEntity(officeadmin);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblOfficeAdmin(TblOfficeAdmin officeadmin) {

        super.updateEntity(officeadmin);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblOfficeAdmin> getAllTblOfficeAdmin() {
        return super.getAllEntity();
    }

    @Override
    public List<TblOfficeAdmin> findTblOfficeAdmin(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblOfficeAdminCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblOfficeAdmin> findByCountTblOfficeAdmin(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
