/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderTables;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderTablesDao extends GenericDao<TblTenderTables>{

    public void addTblTenderTables(TblTenderTables tblObj);

    public void deleteTblTenderTables(TblTenderTables tblObj);

    public void updateTblTenderTables(TblTenderTables tblObj);

    public List<TblTenderTables> getAllTblTenderTables();

    public List<TblTenderTables> findTblTenderTables(Object... values) throws Exception;

    public List<TblTenderTables> findByCountTblTenderTables(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderTablesCount();
}
