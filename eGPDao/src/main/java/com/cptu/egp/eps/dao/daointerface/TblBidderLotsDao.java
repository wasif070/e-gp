package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblBidderLots;
import java.util.List;

/**
 *
 * @author Nishith
 */
public interface TblBidderLotsDao extends GenericDao<TblBidderLots> {

    public void addTblBidderLots(TblBidderLots tblObj);

    public void deleteTblBidderLots(TblBidderLots tblObj);

    public void updateTblBidderLots(TblBidderLots tblObj);

    public List<TblBidderLots> getAllTblBidderLots();

    public List<TblBidderLots> findTblBidderLots(Object... values) throws Exception;

    public List<TblBidderLots> findByCountTblBidderLots(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblBidderLotsCount();
    public void updateInsertAllTblBidderLots(List<TblBidderLots> list);
}
