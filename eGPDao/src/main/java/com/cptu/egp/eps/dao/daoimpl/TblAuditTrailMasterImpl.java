package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblAuditTrailMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblAuditTrailMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblAuditTrailMasterImpl extends AbcAbstractClass<TblAuditTrailMaster> implements TblAuditTrailMasterDao {

    @Override
    public void addTblAuditTrailMaster(TblAuditTrailMaster master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblAuditTrailMaster(TblAuditTrailMaster master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblAuditTrailMaster(TblAuditTrailMaster master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblAuditTrailMaster> getAllTblAuditTrailMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblAuditTrailMaster> findTblAuditTrailMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblAuditTrailMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblAuditTrailMaster> findByCountTblAuditTrailMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
