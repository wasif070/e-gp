package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalReportSign;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblEvalReportSignDao extends GenericDao<TblEvalReportSign> {

    public void addTblEvalReportSign(TblEvalReportSign tblObj);

    public void deleteTblEvalReportSign(TblEvalReportSign tblObj);

    public void deleteAllTblEvalReportSign(Collection tblObj);

    public void updateTblEvalReportSign(TblEvalReportSign tblObj);

    public List<TblEvalReportSign> getAllTblEvalReportSign();

    public List<TblEvalReportSign> findTblEvalReportSign(Object... values) throws Exception;

    public List<TblEvalReportSign> findByCountTblEvalReportSign(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalReportSignCount();
}
