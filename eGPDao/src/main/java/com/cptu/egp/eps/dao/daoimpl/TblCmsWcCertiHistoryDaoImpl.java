/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblCmsWcCertiHistoryDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsWcCertiHistory;
import java.util.List;

/**
 *
 * @author rikin.p
 */
public class TblCmsWcCertiHistoryDaoImpl extends AbcAbstractClass<TblCmsWcCertiHistory> implements TblCmsWcCertiHistoryDao{

    @Override
    public void addTblCmsWcCertiHistory(TblCmsWcCertiHistory appEngEstDoc) {
        super.addEntity(appEngEstDoc);
    }

    @Override
    public List<TblCmsWcCertiHistory> findTblCmsWcCertiHistory(Object... values) throws Exception {
        return super.findEntity(values);       
    }

    @Override
    public void deleteTblCmsWcCertiHistory(TblCmsWcCertiHistory appEngEstDoc) {
        super.deleteEntity(appEngEstDoc);
    }

    @Override
    public void updateTblCmsWcCertiHistory(TblCmsWcCertiHistory appEngEstDoc) {
        super.updateEntity(appEngEstDoc);
    }

    @Override
    public List<TblCmsWcCertiHistory> getAllTblCmsWcCertiHistory() {
        return super.getAllEntity();
    }

    @Override
    public long getTblCmsWcCertiHistoryCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsWcCertiHistory> findByCountTblCmsWcCertiHistory(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}