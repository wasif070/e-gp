package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblTscstatusDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTscstatus;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblTscstatusImpl extends AbcAbstractClass<TblTscstatus> implements TblTscstatusDao {

    @Override
    public void addTblTscstatus(TblTscstatus master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblTscstatus(TblTscstatus master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblTscstatus(TblTscstatus master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblTscstatus> getAllTblTscstatus() {
        return super.getAllEntity();
    }

    @Override
    public long getTblTscstatusCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTscstatus> findTblTscstatus(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public List<TblTscstatus> findByCountTblTscstatus(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
