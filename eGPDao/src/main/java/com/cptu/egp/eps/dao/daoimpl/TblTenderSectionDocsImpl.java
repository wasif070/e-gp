/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderSectionDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderSectionDocs;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderSectionDocsImpl extends AbcAbstractClass<TblTenderSectionDocs> implements TblTenderSectionDocsDao {

    @Override
    public void addTblTenderSectionDocs(TblTenderSectionDocs tenderSectionDocs) {
        super.addEntity(tenderSectionDocs);
    }

    @Override
    public void deleteTblTenderSectionDocs(TblTenderSectionDocs tenderSectionDocs) {
        super.deleteEntity(tenderSectionDocs);
    }

    @Override
    public void updateTblTenderSectionDocs(TblTenderSectionDocs tenderSectionDocs) {
        super.updateEntity(tenderSectionDocs);
    }

    @Override
    public List<TblTenderSectionDocs> getAllTblTenderSectionDocs() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderSectionDocs> findTblTenderSectionDocs(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderSectionDocsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderSectionDocs> findByCountTblTenderSectionDocs(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
