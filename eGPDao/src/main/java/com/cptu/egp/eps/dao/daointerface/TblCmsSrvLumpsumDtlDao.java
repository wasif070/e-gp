package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvLumpsumDtl;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsSrvLumpsumDtlDao extends GenericDao<TblCmsSrvLumpsumDtl> {

    public void addTblCmsSrvLumpsumDtl(TblCmsSrvLumpsumDtl srvLumpsum);

    public void deleteTblCmsSrvLumpsumDtl(TblCmsSrvLumpsumDtl srvLumpsum);

    public void updateTblCmsSrvLumpsumDtl(TblCmsSrvLumpsumDtl srvLumpsum);

    public List<TblCmsSrvLumpsumDtl> getAllTblCmsSrvLumpsumDtl();

    public List<TblCmsSrvLumpsumDtl> findTblCmsSrvLumpsumDtl(Object... values) throws Exception;

    public List<TblCmsSrvLumpsumDtl> findByCountTblCmsSrvLumpsumDtl(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsSrvLumpsumDtlCount();

    public void updateOrSaveEstCost(List<TblCmsSrvLumpsumDtl> estCost);
}
