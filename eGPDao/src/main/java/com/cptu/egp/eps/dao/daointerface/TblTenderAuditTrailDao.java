/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderAuditTrail;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderAuditTrailDao extends GenericDao<TblTenderAuditTrail>{

    public void addTblTenderAuditTrail(TblTenderAuditTrail tblObj);

    public void deleteTblTenderAuditTrail(TblTenderAuditTrail tblObj);

    public void updateTblTenderAuditTrail(TblTenderAuditTrail tblObj);

    public List<TblTenderAuditTrail> getAllTblTenderAuditTrail();

    public List<TblTenderAuditTrail> findTblTenderAuditTrail(Object... values) throws Exception;

    public List<TblTenderAuditTrail> findByCountTblTenderAuditTrail(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderAuditTrailCount();
}
