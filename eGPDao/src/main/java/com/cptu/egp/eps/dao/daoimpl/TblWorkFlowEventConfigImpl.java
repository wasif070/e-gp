/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblWorkFlowEventConfigDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblWorkFlowEventConfig;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public class TblWorkFlowEventConfigImpl extends AbcAbstractClass<TblWorkFlowEventConfig> implements TblWorkFlowEventConfigDao {

    @Override
    public void addTblWorkFlowEventConfig(TblWorkFlowEventConfig workFlowEventConfig) {

        super.addEntity(workFlowEventConfig);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblWorkFlowEventConfig(TblWorkFlowEventConfig workFlowEventConfig) {

        super.deleteEntity(workFlowEventConfig);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblWorkFlowEventConfig(TblWorkFlowEventConfig workFlowEventConfig) {

        super.updateEntity(workFlowEventConfig);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblWorkFlowEventConfig> getAllTblWorkFlowEventConfig() {
        return super.getAllEntity();
    }

    @Override
    public List<TblWorkFlowEventConfig> findTblWorkFlowEventConfig(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblWorkFlowEventConfigCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblWorkFlowEventConfig> findByCountTblWorkFlowEventConfig(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
