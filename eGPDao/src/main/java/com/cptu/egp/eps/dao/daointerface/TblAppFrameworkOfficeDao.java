/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;
import com.cptu.egp.eps.dao.generic.GenericDao;
import java.util.List;
import com.cptu.egp.eps.model.table.TblAppFrameworkOffice;
/**
 *
 * @author HP
 */
public interface TblAppFrameworkOfficeDao extends GenericDao<TblAppFrameworkOffice> {
    public void addTblAppFrameworkOffice(TblAppFrameworkOffice tblObj);

    public void deleteTblAppFrameworkOffice(TblAppFrameworkOffice tblObj);

    public void updateTblAppFrameworkOffice(TblAppFrameworkOffice tblObj);

    public List<TblAppFrameworkOffice> getAllTblAppFrameworkOffice();

    public List<TblAppFrameworkOffice> findTblAppFrameworkOffice(Object... values) throws Exception;

    public List<TblAppFrameworkOffice> findByCountTblAppFrameworkOffice(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblAppFrameworkOfficeCount();

    public void updateOrSaveTblAppFrameworkOffice(List<TblAppFrameworkOffice> tblObj);
}
