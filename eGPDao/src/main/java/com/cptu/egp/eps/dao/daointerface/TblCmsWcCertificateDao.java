/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsWcCertificate;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public interface TblCmsWcCertificateDao  extends GenericDao<TblCmsWcCertificate>{
    
    public void addTblCmsWcCertificate(TblCmsWcCertificate tblCmsWcCertificate);

    public void deleteTblCmsWcCertificate(TblCmsWcCertificate tblCmsWcCertificate);

    public void updateTblCmsWcCertificate(TblCmsWcCertificate tblCmsWcCertificate);

    public List<TblCmsWcCertificate> getAllTblCmsWcCertificate();

    public List<TblCmsWcCertificate> findTblCmsWcCertificate(Object... values) throws Exception;

    public List<TblCmsWcCertificate> findByCountTblCmsWcCertificate(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsWcCertificateCount();

}
