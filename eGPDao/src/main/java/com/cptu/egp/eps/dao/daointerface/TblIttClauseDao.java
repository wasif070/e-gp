/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblIttClause;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public interface TblIttClauseDao extends GenericDao<TblIttClause> {

    public void addTblIttClause(TblIttClause admin);

    public void deleteTblIttClause(TblIttClause admin);

    public void updateTblIttClause(TblIttClause admin);

    public List<TblIttClause> getAllTblIttClause();

    public List<TblIttClause> findTblIttClause(Object... values) throws Exception;

    public List<TblIttClause> findByCountTblIttClause(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblIttClauseCount();
}
