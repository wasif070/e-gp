/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblBidDocumentsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblBidDocuments;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblBidDocumentsImpl extends AbcAbstractClass<TblBidDocuments> implements TblBidDocumentsDao {

    @Override
    public void addTblBidDocuments(TblBidDocuments admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblBidDocuments(TblBidDocuments admin) {
        super.deleteEntity(admin);        
    }

    @Override
    public void updateTblBidDocuments(TblBidDocuments admin) {
        super.updateEntity(admin);        
    }

    @Override
    public List<TblBidDocuments> getAllTblBidDocuments() {
        return super.getAllEntity();
    }

    @Override
    public List<TblBidDocuments> findTblBidDocuments(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblBidDocumentsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblBidDocuments> findByCountTblBidDocuments(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
