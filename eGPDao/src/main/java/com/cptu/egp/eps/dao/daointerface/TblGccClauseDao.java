package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblGccClause;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblGccClauseDao extends GenericDao<TblGccClause> {

    public void addTblGccClause(TblGccClause tblObj);

    public void deleteTblGccClause(TblGccClause tblObj);

    public void updateTblGccClause(TblGccClause tblObj);

    public List<TblGccClause> getAllTblGccClause();

    public List<TblGccClause> findTblGccClause(Object... values) throws Exception;

    public List<TblGccClause> findByCountTblGccClause(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblGccClauseCount();
}
