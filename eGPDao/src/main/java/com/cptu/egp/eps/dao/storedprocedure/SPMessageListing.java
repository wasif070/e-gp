/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Administrator
 */
public class SPMessageListing extends StoredProcedure {
   private static final Logger LOGGER = Logger.getLogger(SPMessageListing.class);
   public  SPMessageListing(BasicDataSource dataSource, String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("v_MsgBoxTypeVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_UserIdN", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_FolderIdN", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_msgStatusVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_PagePerRecordN", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_PageN", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_TotalPages", Types.INTEGER));
         this.declareParameter(new SqlParameter("v_viewMsgVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_searchInVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_keywordsVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_emailIdVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_dateFromVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_dateToVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_msgidN", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_columnNameVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_orderTypeVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_searchOptVc", Types.VARCHAR));

    }

   /**
    * Execute stored procedure : p_view_mailmessagetest
    * @param msgBoxType
    * @param fromEmailId
    * @param folderId
    * @param msgType
    * @param noOfRecords
    * @param pageNo
    * @param totalPages
    * @param searchIn
    * @param keywords
    * @param emailId
    * @param dateFrom
    * @param dateTo
    * @param viewMsg
    * @param msgId
    * @param columnName
    * @param order
    * @param sortOpt
    * @return list of mail message 
    */
    public List<MsgListing> executeProcedure(String msgBoxType, Integer fromEmailId, Integer folderId, String msgType, Integer noOfRecords, Integer pageNo, Integer totalPages,String searchIn,String keywords,String emailId,String dateFrom,String dateTo,String viewMsg,Integer msgId,String columnName,String order,String sortOpt) {
        Map inParams = new HashMap();
        inParams.put("v_MsgBoxTypeVc", msgBoxType);
        inParams.put("v_UserIdN", fromEmailId);
        inParams.put("v_FolderIdN", folderId);
        inParams.put("v_msgStatusVc", msgType);
        inParams.put("v_PagePerRecordN", noOfRecords);
        inParams.put("v_PageN", pageNo);
        inParams.put("v_TotalPages", totalPages);
         inParams.put("v_viewMsgVc", viewMsg);
        inParams.put("v_searchInVc", searchIn);
        inParams.put("v_keywordsVc", keywords);
        inParams.put("v_emailIdVc", emailId);
        inParams.put("v_dateFromVc", dateFrom);
        inParams.put("v_dateToVc", dateTo);
        inParams.put("v_msgidN", msgId);
        inParams.put("v_columnNameVc", columnName);
        inParams.put("v_orderTypeVc", order);
        inParams.put("v_searchOptVc", sortOpt);
        this.compile();
        List<MsgListing> details = new ArrayList<MsgListing>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                MsgListing msgTesting = new MsgListing();
                msgTesting.setMsgBoxId((Integer) linkedHashMap.get("msgBoxId"));
                msgTesting.setFolderId((Integer) linkedHashMap.get("folderId"));
                msgTesting.setMsgFromUserId((Integer) linkedHashMap.get("msgFromUserId"));
                msgTesting.setMsgToUserId((Integer) linkedHashMap.get("msgToUserId"));
                msgTesting.setMsgId((Integer) linkedHashMap.get("msgId"));
                msgTesting.setSubject((String) linkedHashMap.get("subject"));
                msgTesting.setCreationDate((Date) linkedHashMap.get("creationDate"));
                msgTesting.setIsDocUploaded((String) linkedHashMap.get("isDocUploaded"));
                msgTesting.setIsMsgRead((String) linkedHashMap.get("isMsgRead"));
                msgTesting.setIsPriority((String) linkedHashMap.get("isPriority"));
                msgTesting.setMsgStatus((String) linkedHashMap.get("msgStatus"));
                msgTesting.setMsgText((String) linkedHashMap.get("msgText"));
                msgTesting.setMsgToCCReply((String) linkedHashMap.get("msgToCCReply"));
                msgTesting.setProcessUrl((String) linkedHashMap.get("processUrl"));
                msgTesting.setFromMailId((String) linkedHashMap.get("FromEmail"));
                msgTesting.setToMailId((String) linkedHashMap.get("ToEmail"));
                msgTesting.setTotalRecords((Integer) linkedHashMap.get("totalRecords"));
               
                details.add(msgTesting);
            }
        } catch (Exception e) {
            LOGGER.error("SPMessageListing  : "+ e);
        }
        return details;
    }
}
