package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblMandatoryDocDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblMandatoryDoc;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblMandatoryDocImpl extends AbcAbstractClass<TblMandatoryDoc> implements TblMandatoryDocDao {

    @Override
    public void addTblMandatoryDoc(TblMandatoryDoc master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblMandatoryDoc(TblMandatoryDoc master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblMandatoryDoc(TblMandatoryDoc master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblMandatoryDoc> getAllTblMandatoryDoc() {
        return super.getAllEntity();
    }

    @Override
    public List<TblMandatoryDoc> findTblMandatoryDoc(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblMandatoryDocCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblMandatoryDoc> findByCountTblMandatoryDoc(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
