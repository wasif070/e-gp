/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author parag
 */
public class SPXMLCommon extends StoredProcedure {
    private static final Logger LOGGER = Logger.getLogger(SPXMLCommon.class);
    public SPXMLCommon(BasicDataSource dataSource, String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("v_action_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_tablename_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_xml_text_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_conditions_inVc", Types.VARCHAR));
    }

    /**
     * Method to execute Stored procedure to perform data base operation using xml string for table.
     * Execute Stored Procedure : p_ins_upd_templatexml
     * @param action
     * @param tablename
     * @param xmlData
     * @param conditions
     * @return Common Message Details.
     */
    public List<CommonMsgChk> executeProcedure(String action, String tablename, String xmlData, String conditions) {
        Map inParams = new HashMap();
        inParams.put("v_action_inVc", action);
        inParams.put("v_tablename_inVc", tablename);
        inParams.put("v_xml_text_inVc", xmlData);
        inParams.put("v_conditions_inVc", conditions);

        this.compile();

        List<CommonMsgChk> details = new ArrayList<CommonMsgChk>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            if (list != null && !list.isEmpty()) {
                for (LinkedHashMap<String, Object> linkedHashMap : list) {
                    CommonMsgChk commonMsgChk = new CommonMsgChk();

                    commonMsgChk.setFlag((Boolean) linkedHashMap.get("flag"));
                    commonMsgChk.setMsg((String) linkedHashMap.get("Message"));
                    commonMsgChk.setId((Integer) linkedHashMap.get("Id"));
                    details.add(commonMsgChk);
                }
            }
            LOGGER.debug("SPXMLCommon : No data found for  ACTION  "+action);
        } catch (Exception e) {
            LOGGER.error("SPXMLCommon  : "+ e + " ACTION  "+action);
        }
        return details;
    }

    /**
     * Handle the special character before storing into database.
     * @param str
     * @return String 
     */
    public static String handleSpecialChar(String str) {
        str = str.replaceAll("&", "&amp;");
        str = str.replaceAll("'", "&rsquo;");
        str = str.replaceAll("\"", "&rsquo;");
        str = str.replaceAll(">", "&gt;");
        str = str.replaceAll("<", "&lt;");
        
        return str;
    }
}
