package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblContractSignDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblContractSign;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblContractSignImpl extends AbcAbstractClass<TblContractSign> implements TblContractSignDao {

    @Override
    public void addTblContractSign(TblContractSign contractSign){
        super.addEntity(contractSign);
    }

    @Override
    public void deleteTblContractSign(TblContractSign contractSign) {
        super.deleteEntity(contractSign);
    }

    @Override
    public void updateTblContractSign(TblContractSign contractSign) {
        super.updateEntity(contractSign);
    }

    @Override
    public List<TblContractSign> getAllTblContractSign() {
        return super.getAllEntity();
    }

    @Override
    public List<TblContractSign> findTblContractSign(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblContractSignCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblContractSign> findByCountTblContractSign(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
