package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblGradeMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblGradeMasterDao extends GenericDao<TblGradeMaster> {

    public void addTblGradeMaster(TblGradeMaster budget);

    public void deleteTblGradeMaster(TblGradeMaster budget);

    public void updateTblGradeMaster(TblGradeMaster budget);

    public List<TblGradeMaster> getAllTblGradeMaster();

    public List<TblGradeMaster> findTblGradeMaster(Object... values) throws Exception;

    public List<TblGradeMaster> findByCountTblGradeMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblGradeMasterCount();
}
