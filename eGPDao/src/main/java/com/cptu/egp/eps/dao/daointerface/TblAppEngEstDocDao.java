package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblAppEngEstDoc;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblAppEngEstDocDao extends GenericDao<TblAppEngEstDoc> {

    public void addTblAppEngEstDoc(TblAppEngEstDoc tblObj);

    public void deleteTblAppEngEstDoc(TblAppEngEstDoc tblObj);

    public void updateTblAppEngEstDoc(TblAppEngEstDoc tblObj);

    public List<TblAppEngEstDoc> getAllTblAppEngEstDoc();

    public List<TblAppEngEstDoc> findTblAppEngEstDoc(Object... values) throws Exception;

    public List<TblAppEngEstDoc> findByCountTblAppEngEstDoc(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblAppEngEstDocCount();
}
