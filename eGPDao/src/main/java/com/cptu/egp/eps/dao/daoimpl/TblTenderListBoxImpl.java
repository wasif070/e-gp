/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderListBoxDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderListBox;
import java.util.List;


/**
 *
 * @author parag
 */
public class TblTenderListBoxImpl extends AbcAbstractClass<TblTenderListBox> implements TblTenderListBoxDao{

    @Override
    public void addTblTenderListBox(TblTenderListBox tblListBoxMaster) {
        super.addEntity(tblListBoxMaster);
    }

    @Override
    public List<TblTenderListBox> findTblTenderListBox(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblTenderListBox(TblTenderListBox tblListBoxMaster) {

        super.deleteEntity(tblListBoxMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblTenderListBox(TblTenderListBox tblListBoxMaster) {

        super.updateEntity(tblListBoxMaster);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblTenderListBox> getAllTblTenderListBox() {
        return super.getAllEntity();
    }

    @Override
    public long getTblTenderListBoxCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderListBox> findByCountTblTenderListBox(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}

