package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTemplateCells;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTemplateCellsDao extends GenericDao<TblTemplateCells> {

    public void addTblTemplateCells(TblTemplateCells tblObj);

    public void deleteTblTemplateCells(TblTemplateCells tblObj);

    public void updateTblTemplateCells(TblTemplateCells tblObj);

    public List<TblTemplateCells> getAllTblTemplateCells();

    public List<TblTemplateCells> findTblTemplateCells(Object... values) throws Exception;

    public List<TblTemplateCells> findByCountTblTemplateCells(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTemplateCellsCount();
}
