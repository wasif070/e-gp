package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblDebarmentTypesDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblDebarmentTypes;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblDebarmentTypesImpl extends AbcAbstractClass<TblDebarmentTypes> implements TblDebarmentTypesDao {

    @Override
    public void addTblDebarmentTypes(TblDebarmentTypes debarmentTypes){
        super.addEntity(debarmentTypes);
    }

    @Override
    public void deleteTblDebarmentTypes(TblDebarmentTypes debarmentTypes) {
        super.deleteEntity(debarmentTypes);
    }

    @Override
    public void updateTblDebarmentTypes(TblDebarmentTypes debarmentTypes) {
        super.updateEntity(debarmentTypes);
    }

    @Override
    public List<TblDebarmentTypes> getAllTblDebarmentTypes() {
        return super.getAllEntity();
    }

    @Override
    public List<TblDebarmentTypes> findTblDebarmentTypes(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblDebarmentTypesCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblDebarmentTypes> findByCountTblDebarmentTypes(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
