/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderTdsSubClause;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderTdsSubClauseDao extends GenericDao<TblTenderTdsSubClause>{

    public void addTblTenderTdsSubClause(TblTenderTdsSubClause tblObj);

    public void deleteTblTenderTdsSubClause(TblTenderTdsSubClause tblObj);

    public void updateTblTenderTdsSubClause(TblTenderTdsSubClause tblObj);

    public List<TblTenderTdsSubClause> getAllTblTenderTdsSubClause();

    public List<TblTenderTdsSubClause> findTblTenderTdsSubClause(Object... values) throws Exception;

    public List<TblTenderTdsSubClause> findByCountTblTenderTdsSubClause(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderTdsSubClauseCount();
}
