/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.util.Date;

/**
 *
 * @author Ramesh.Janagondakuru
 */
public class CommonSPWfFileHistory {

    //Properties
    private Integer fromUserId;
    private Integer toUserId;
    private String fileSentFrom;
    private String fileSentTo;
    private String action;
    private String remarks;
    private Date processDate;
    private String moduleName;
    private String eventName;
    private Integer totalRecords;
    private String rowNumber;
    private Integer objectiId;
    private Integer childId;
    private Short activityId;
    private Short eventId;
    private String docId;
    private String wfHistoryId;

     //Setters & Getters
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getFileSentFrom() {
        return fileSentFrom;
    }

    public void setFileSentFrom(String fileSentFrom) {
        this.fileSentFrom = fileSentFrom;
    }

    public String getFileSentTo() {
        return fileSentTo;
    }

    public void setFileSentTo(String fileSentTo) {
        this.fileSentTo = fileSentTo;
    }

    public Integer getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Integer fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public Date getProcessDate() {
        return processDate;
    }

    public void setProcessDate(Date processDate) {
        this.processDate = processDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getToUserId() {
        return toUserId;
    }

    public void setToUserId(Integer toUserId) {
        this.toUserId = toUserId;
    }

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }

    /**
     * @return the rowNumber
     */
    public String getRowNumber() {
        return rowNumber;
    }

    /**
     * @param rowNumber the rowNumber to set
     */
    public void setRowNumber(String rowNumber) {
        this.rowNumber = rowNumber;
    }

    /**
     * @return the objectiId
     */
    public Integer getObjectiId() {
        return objectiId;
    }

    /**
     * @param objectiId the objectiId to set
     */
    public void setObjectiId(Integer objectiId) {
        this.objectiId = objectiId;
    }

    /**
     * @return the childId
     */
    public Integer getChildId() {
        return childId;
    }

    /**
     * @param childId the childId to set
     */
    public void setChildId(Integer childId) {
        this.childId = childId;
    }

   

    /**
     * @return the eventId
     */
    public Short getEventId() {
        return eventId;
    }

    /**
     * @param eventId the eventId to set
     */
    public void setEventId(Short eventId) {
        this.eventId = eventId;
    }

    /**
     * @return the activityId
     */
    public Short getActivityId() {
        return activityId;
    }

    /**
     * @param activityId the activityId to set
     */
    public void setActivityId(Short activityId) {
        this.activityId = activityId;
    }

    /**
     * @return the docId
     */
    public String getDocId() {
        return docId;
    }

    /**
     * @param docId the docId to set
     */
    public void setDocId(String docId) {
        this.docId = docId;
    }

    /**
     * @return the wfHistoryId
     */
    public String getWfHistoryId() {
        return wfHistoryId;
    }

    /**
     * @param wfHistoryId the wfHistoryId to set
     */
    public void setWfHistoryId(String wfHistoryId) {
        this.wfHistoryId = wfHistoryId;
    }

   


}
