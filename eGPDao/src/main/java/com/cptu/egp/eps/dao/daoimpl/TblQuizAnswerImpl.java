/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblQuizAnswerDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblQuizAnswer;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author feroz
 */
public class TblQuizAnswerImpl extends AbcAbstractClass<TblQuizAnswer> implements TblQuizAnswerDao{

    @Override
    public void addTblQuizAnswer(TblQuizAnswer tblQuizAnswer) {
        super.addEntity(tblQuizAnswer);
    }

    @Override
    public void deleteTblQuizAnswer(TblQuizAnswer tblQuizAnswer) {
        super.deleteEntity(tblQuizAnswer);
    }

    @Override
    public void updateTblQuizAnswer(TblQuizAnswer tblQuizAnswer) {
        super.updateEntity(tblQuizAnswer);
    }

    @Override
    public List<TblQuizAnswer> getAllTblQuizAnswer() {
        return super.getAllEntity();
    }

    @Override
    public List<TblQuizAnswer> findTblQuizAnswer(Object... values) {
        List<TblQuizAnswer> tblQuizAnswer = null;
        try {
            tblQuizAnswer = super.findEntity(values);
        } catch (Exception ex) {
            Logger.getLogger(TblQuizAnswerImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tblQuizAnswer;
    }
    
}
