package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsWpDetailHistoryrDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsWpDetailHistory;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsWpDetailHistoryImpl extends AbcAbstractClass<TblCmsWpDetailHistory> implements TblCmsWpDetailHistoryrDao {

    @Override
    public void addTblCmsWpDetailHistory(TblCmsWpDetailHistory cmsWpDetailHistory){
        super.addEntity(cmsWpDetailHistory);
    }

    @Override
    public void deleteTblCmsWpDetailHistory(TblCmsWpDetailHistory cmsWpDetailHistory) {
        super.deleteEntity(cmsWpDetailHistory);
    }

    @Override
    public void updateTblCmsWpDetailHistory(TblCmsWpDetailHistory cmsWpDetailHistory) {
        super.updateEntity(cmsWpDetailHistory);
    }

    @Override
    public List<TblCmsWpDetailHistory> getAllTblCmsWpDetailHistory() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsWpDetailHistory> findTblCmsWpDetailHistory(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsWpDetailHistoryCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsWpDetailHistory> findByCountTblCmsWpDetailHistory(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsWpDetailHistory> estCost) throws Exception {
         super.updateAll(estCost);
    }
}
