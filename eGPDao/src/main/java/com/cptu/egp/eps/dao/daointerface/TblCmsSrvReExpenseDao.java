package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvReExpense;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsSrvReExpenseDao extends GenericDao<TblCmsSrvReExpense>  {

    public void addTblCmsSrvReExpense(TblCmsSrvReExpense event) throws Exception;

    public void deleteTblCmsSrvReExpense(TblCmsSrvReExpense event) throws Exception;

    public void updateTblCmsSrvReExpense(TblCmsSrvReExpense event) throws Exception;

    public List<TblCmsSrvReExpense> getAllTblCmsSrvReExpense() throws Exception;

    public List<TblCmsSrvReExpense> findTblCmsSrvReExpense(Object... values) throws Exception;

    public List<TblCmsSrvReExpense> findByCountTblCmsSrvReExpense(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsSrvReExpenseCount() throws Exception;

    public void updateOrSaveEstCost(List<TblCmsSrvReExpense> estCost) throws Exception;
}
