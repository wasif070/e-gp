package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblTenderDomesticPrefDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderDomesticPref;
import java.util.List;

/**
 *
 * @author rishita
 */
public class TblTenderDomesticPrefImpl extends AbcAbstractClass<TblTenderDomesticPref> implements TblTenderDomesticPrefDao {

    @Override
    public void addTblTenderDomesticPref(TblTenderDomesticPref tenderDomesticPref) {
        super.addEntity(tenderDomesticPref);
    }

    @Override
    public void deleteTblTenderDomesticPref(TblTenderDomesticPref tenderDomesticPref) {
        super.deleteEntity(tenderDomesticPref);
    }

    @Override
    public void updateTblTenderDomesticPref(TblTenderDomesticPref tenderDomesticPref) {
        super.updateEntity(tenderDomesticPref);
    }

    @Override
    public List<TblTenderDomesticPref> getAllTblTenderDomesticPref() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderDomesticPref> findTblTenderDomesticPref(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderDomesticPrefCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderDomesticPref> findByCountTblTenderDomesticPref(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveTenderDomesticPref(List<TblTenderDomesticPref> tenderDomesticPref) {
        super.updateAll(tenderDomesticPref);
    }
}
