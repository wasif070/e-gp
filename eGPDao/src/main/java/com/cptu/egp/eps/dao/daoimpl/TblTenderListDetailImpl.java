/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderListDetailDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderListDetail;
import java.util.List;

/**
 *
 * @author parag
 */
public class TblTenderListDetailImpl extends AbcAbstractClass<TblTenderListDetail> implements TblTenderListDetailDao{

    @Override
    public void addTblTenderListDetail(TblTenderListDetail tblTenderListDetail) {
        super.addEntity(tblTenderListDetail);
    }

    @Override
    public List<TblTenderListDetail> findTblTenderListDetail(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblTenderListDetail(TblTenderListDetail tblTenderListDetail) {

        super.deleteEntity(tblTenderListDetail);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblTenderListDetail(TblTenderListDetail tblTenderListDetail) {

        super.updateEntity(tblTenderListDetail);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblTenderListDetail> getAllTblTenderListDetail() {
        return super.getAllEntity();
    }

    @Override
    public long getTblTenderListDetailCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderListDetail> findByCountTblTenderListDetail(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveAllTblTenderListDetail(List<TblTenderListDetail> list) {
        super.updateAll(list);
    }
}

