/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderPostQueConfig;
import java.util.List;

/**
 *
 * @author dixit
 */
public interface TblTenderPostQueConfigDao extends GenericDao<TblTenderPostQueConfig>{

    public void addTblTenderPostQueConfig(TblTenderPostQueConfig tblObj);

    public void deleteTblTenderPostQueConfig(TblTenderPostQueConfig tblObj);

    public void updateTblTenderPostQueConfig(TblTenderPostQueConfig tblObj);

    public List<TblTenderPostQueConfig> getAllTblTenderPostQueConfig();

    public List<TblTenderPostQueConfig> findTblTenderPostQueConfig(Object... values) throws Exception;

    public List<TblTenderPostQueConfig> findByCountTblTenderPostQueConfig(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderTablesCount();

}
