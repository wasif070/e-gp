package com.cptu.egp.eps.dao.daoimpl;


import com.cptu.egp.eps.dao.daointerface.TblCmsSrvWrkSchDocDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvWrkSchDoc;
import java.util.List;


public class TblCmsSrvWrkSchDocImpl extends AbcAbstractClass<TblCmsSrvWrkSchDoc> implements TblCmsSrvWrkSchDocDao {

	@Override 
	public void addTblCmsSrvWrkSchDoc(TblCmsSrvWrkSchDoc master){
		super.addEntity(master);
	}
	@Override 
	public void deleteTblCmsSrvWrkSchDoc(TblCmsSrvWrkSchDoc master) {
		super.deleteEntity(master);
	}
	@Override 
	public void updateTblCmsSrvWrkSchDoc(TblCmsSrvWrkSchDoc master) {
		super.updateEntity(master);
	}
	@Override 
	public List<TblCmsSrvWrkSchDoc> getAllTblCmsSrvWrkSchDoc() {
		return super.getAllEntity();
	}
	@Override 
	public List<TblCmsSrvWrkSchDoc> findTblCmsSrvWrkSchDoc(Object... values) throws Exception {
		return super.findEntity(values);
	}
	@Override 
	public long getTblCmsSrvWrkSchDocCount() {
		return super.getEntityCount();
	}
	@Override 
	public List<TblCmsSrvWrkSchDoc> findByCountTblCmsSrvWrkSchDoc(int firstResult, int maxResult, Object... values) throws Exception {
		return super.findByCountEntity(firstResult, maxResult, values);
	}
	@Override 
	public void updateOrSaveTblCmsSrvWrkSchDoc(List<TblCmsSrvWrkSchDoc> tblObj) {
		super.updateAll(tblObj);
	}
}