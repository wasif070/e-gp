/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblQuestionModule;
import java.util.List;

/**
 *
 * @author feroz
 */
public interface TblQuestionModuleDao extends GenericDao<TblQuestionModule>{
    
    public void addTblQuestionModule(TblQuestionModule tblQuestionModule);

    public void deleteTblQuestionModule(TblQuestionModule tblQuestionModule);

    public void updateTblQuestionModule(TblQuestionModule tblQuestionModule);

    public List<TblQuestionModule> getAllTblQuestionModule();
    
    /**
     *
     * @param values
     * @return
     */
    public List<TblQuestionModule> findTblQuestionModule(Object... values);

    
}
