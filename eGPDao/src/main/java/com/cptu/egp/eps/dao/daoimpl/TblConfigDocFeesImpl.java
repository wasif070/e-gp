/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblConfigDocFeesDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblConfigDocFees;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblConfigDocFeesImpl extends AbcAbstractClass<TblConfigDocFees> implements TblConfigDocFeesDao{

    @Override
    public void addTblConfigDocFees(TblConfigDocFees tblConfigDocFees) {
        super.addEntity(tblConfigDocFees);
    }

    @Override
    public List<TblConfigDocFees> findTblConfigDocFees(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblConfigDocFees(TblConfigDocFees tblConfigDocFees) {

        super.deleteEntity(tblConfigDocFees);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblConfigDocFees(TblConfigDocFees tblConfigDocFees) {

        super.updateEntity(tblConfigDocFees);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblConfigDocFees> getAllTblConfigDocFees() {
        return super.getAllEntity();
    }

    @Override
    public long getTblConfigDocFeesCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblConfigDocFees> findByCountTblConfigDocFees(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateInsAllConfigDocFees(List<TblConfigDocFees> tblConfigDocFees) {
        super.updateAll(tblConfigDocFees);
    }
}
