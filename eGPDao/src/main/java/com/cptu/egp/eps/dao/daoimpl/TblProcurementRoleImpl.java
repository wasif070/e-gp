package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblProcurementRoleDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblProcurementRole;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblProcurementRoleImpl extends AbcAbstractClass<TblProcurementRole> implements TblProcurementRoleDao {

    @Override
    public void addTblProcurementRole(TblProcurementRole procure){
        super.addEntity(procure);
    }

    @Override
    public void deleteTblProcurementRole(TblProcurementRole procure) {
        super.deleteEntity(procure);
    }

    @Override
    public void updateTblProcurementRole(TblProcurementRole procure) {
        super.updateEntity(procure);
    }

    @Override
    public List<TblProcurementRole> getAllTblProcurementRole() {
        return super.getAllEntity();
    }

    @Override
    public List<TblProcurementRole> findTblProcurementRole(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblProcurementRoleCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblProcurementRole> findByCountTblProcurementRole(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
