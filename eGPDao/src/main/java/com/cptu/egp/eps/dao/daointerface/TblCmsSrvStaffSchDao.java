package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsSrvStaffSch;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsSrvStaffSchDao extends GenericDao<TblCmsSrvStaffSch> {

    public void addTblCmsSrvStaffSch(TblCmsSrvStaffSch event);

    public void deleteTblCmsSrvStaffSch(TblCmsSrvStaffSch event);

    public void updateTblCmsSrvStaffSch(TblCmsSrvStaffSch event);

    public List<TblCmsSrvStaffSch> getAllTblCmsSrvStaffSch();

    public List<TblCmsSrvStaffSch> findTblCmsSrvStaffSch(Object... values) throws Exception;

    public List<TblCmsSrvStaffSch> findByCountTblCmsSrvStaffSch(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblCmsSrvStaffSchCount();

    public void updateOrSaveEstCost(List<TblCmsSrvStaffSch> estCost);
}
