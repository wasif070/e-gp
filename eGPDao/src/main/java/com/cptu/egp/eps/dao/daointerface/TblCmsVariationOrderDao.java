package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsVariationOrder;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsVariationOrderDao extends GenericDao<TblCmsVariationOrder> {

    public void addTblCmsVariationOrder(TblCmsVariationOrder activity);

    public void deleteTblCmsVariationOrder(TblCmsVariationOrder activity);

    public void updateTblCmsVariationOrder(TblCmsVariationOrder activity);

    public List<TblCmsVariationOrder> getAllTblCmsVariationOrder();

    public List<TblCmsVariationOrder> findTblCmsVariationOrder(Object... values) throws Exception;

    public List<TblCmsVariationOrder> findByCountTblCmsVariationOrder(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsVariationOrderCount();

    public void updateOrSaveEstCost(List<TblCmsVariationOrder> estCost);
}
