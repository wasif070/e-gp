package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblProcurementRole;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblProcurementRoleDao extends GenericDao<TblProcurementRole> {

    public void addTblProcurementRole(TblProcurementRole procure);

    public void deleteTblProcurementRole(TblProcurementRole procure);

    public void updateTblProcurementRole(TblProcurementRole procure);

    public List<TblProcurementRole> getAllTblProcurementRole();

    public List<TblProcurementRole> findTblProcurementRole(Object... values) throws Exception;

    public List<TblProcurementRole> findByCountTblProcurementRole(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblProcurementRoleCount();
}
