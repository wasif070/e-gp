package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalReportClarification;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblEvalReportClarificationDao extends GenericDao<TblEvalReportClarification> {

    public void addTblEvalReportClarification(TblEvalReportClarification tblEvalReportClarification);

    public void deleteTblEvalReportClarification(TblEvalReportClarification tblEvalReportClarification);

    public void updateTblEvalReportClarification(TblEvalReportClarification tblEvalReportClarification);

    public List<TblEvalReportClarification> getAllTblEvalReportClarification();

    public List<TblEvalReportClarification> findTblEvalReportClarification(Object... values) throws Exception;

    public List<TblEvalReportClarification> findByCountTblEvalReportClarification(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalReportClarificationCount();
}
