package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblMessageDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblMessage;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblMessageImpl extends AbcAbstractClass<TblMessage> implements TblMessageDao {

    @Override
    public void addTblMessage(TblMessage message){
        super.addEntity(message);
    }

    @Override
    public void deleteTblMessage(TblMessage message) {
        super.deleteEntity(message);
    }

    @Override
    public void updateTblMessage(TblMessage message) {
        super.updateEntity(message);
    }

    @Override
    public List<TblMessage> getAllTblMessage() {
        return super.getAllEntity();
    }

    @Override
    public List<TblMessage> findTblMessage(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblMessageCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblMessage> findByCountTblMessage(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
