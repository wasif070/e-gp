/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderAdvtDetailsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderAdvtDetails;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderAdvtDetailsImpl extends AbcAbstractClass<TblTenderAdvtDetails> implements TblTenderAdvtDetailsDao {

    @Override
    public void addTblTenderAdvtDetails(TblTenderAdvtDetails admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblTenderAdvtDetails(TblTenderAdvtDetails admin) {
        super.deleteEntity(admin);        
    }

    @Override
    public void updateTblTenderAdvtDetails(TblTenderAdvtDetails admin) {
        super.updateEntity(admin);        
    }

    @Override
    public List<TblTenderAdvtDetails> getAllTblTenderAdvtDetails() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderAdvtDetails> findTblTenderAdvtDetails(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderAdvtDetailsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderAdvtDetails> findByCountTblTenderAdvtDetails(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
