package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblComplaintDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblComplaintDocs;
import java.util.List;

/**
 *
 * @author Radha
 */
public class TblComplaintDocsDaoImpl extends AbcAbstractClass<TblComplaintDocs> implements TblComplaintDocsDao {

    @Override
    public void saveDoc(TblComplaintDocs complaintDoc)throws Exception{
        super.addEntity(complaintDoc);
    }


    @Override
    public void deleteTblComplaintDocs(TblComplaintDocs complaintDoc) throws Exception{
        super.deleteEntity(complaintDoc);
    }
    /*
    @Override
    public void updateTblComplaintDocs(TblComplaintDocs complaintDoc) {
        super.updateEntity(complaintDoc);
    }*/

    @Override
    public List<TblComplaintDocs> getComplaintDocs(Object... values    ) throws Exception{
        return super.findEntity(values); 
    }
   /* @Override
    public List<TblComplaintDocs> findComplaintDocs(int id) throws Exception{
    	
    	return super.findEntity();
    }*/
    
    public  List<Object[]> getDocs(int complaintId,int complaintLevelId) throws Exception{
    	
    	 StringBuilder query = new StringBuilder(); 
  	   query.append("from TblComplaintDocs where complaintId="+complaintId+" and complaintLevelId="+complaintLevelId);   
  	     String queryStr=query.toString();
      	
      return	super.createQuery(queryStr); 
    }
    
    public List<Object[]>  getDocsProcessList(int id,int levelId,int uploadedBy) throws Exception{
    	
    	
    	StringBuilder query = new StringBuilder(); 
   	   query.append("from TblComplaintDocs where complaintId="+id+" and uploadedBy="+uploadedBy+" and complaintLevelId="+levelId);
   	     String queryStr=query.toString();
       	
       return	super.createQuery(queryStr);
    	
    	
    	
    	
    }

    public  List<Object[]> getDocsByComplaintId(int complaintId) throws Exception{

    	 StringBuilder query = new StringBuilder();
  	   query.append("from TblComplaintDocs where complaintId="+complaintId);
  	     String queryStr=query.toString();

      return	super.createQuery(queryStr);
    }
    
    
      
}
