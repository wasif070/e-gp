/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblSmsPrefMaster;
import java.util.List;

/**
 *
 * @author shreyansh.shah
 */
public interface TblSmsPrefMasterDao extends GenericDao<TblSmsPrefMaster>{
public void addTblSmsPrefMaster(TblSmsPrefMaster tblObj);

    public void deleteTblSmsPrefMaster(TblSmsPrefMaster tblObj);

    public void updateTblSmsPrefMaster(TblSmsPrefMaster tblObj);

    public List<TblSmsPrefMaster> getAllTblSmsPrefMaster();

    public List<TblSmsPrefMaster> findTblSmsPrefMaster(Object... values) throws Exception;

    public List<TblSmsPrefMaster> findByCountTblSmsPrefMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblSmsPrefMasterCount();
}
