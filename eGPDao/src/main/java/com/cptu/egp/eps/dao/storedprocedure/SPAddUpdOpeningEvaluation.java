/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author rokib
 */
public class SPAddUpdOpeningEvaluation extends StoredProcedure {
    private static final Logger LOGGER = Logger.getLogger(SpgetCommonSearchDataMore.class);
      public SPAddUpdOpeningEvaluation(BasicDataSource dataSource, String procName) {

        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        LOGGER.debug("Proc Name: " + procName);
        this.declareParameter(new SqlParameter("@v_fieldName1Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName2Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName3Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName4Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName5Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName6Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName7Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName8Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName9Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName10Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName11Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName12Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName13Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName14Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName15Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName16Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName17Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName18Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName19Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName20Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName21Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName22Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName23Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName24Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName25Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName26Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName27Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName28Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName29Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName30Vc", Types.VARCHAR));
    }

    /**
     * Execute stored procedure : p_add_upd_openingEvaluation
     * Add Update Opening Evaluation.
     * @param fieldName1
     * @param fieldName2
     * @param fieldName3
     * @param fieldName4
     * @param fieldName5
     * @param fieldName6
     * @param fieldName7
     * @param fieldName8
     * @param fieldName9
     * @param fieldName10
     * @param fieldName11
     * @param fieldName12
     * @param fieldName13
     * @param fieldName14
     * @param fieldName15
     * @param fieldName16
     * @param fieldName17
     * @param fieldName18
     * @param fieldName19
     * @param fieldName20
     * @return Result data as list of CommonMsgChk object
     */
    public List<CommonMsgChk> executeProcedure(String fieldName1, String fieldName2, String fieldName3, String fieldName4, String fieldName5, String fieldName6, String fieldName7, String fieldName8, String fieldName9, String fieldName10, String fieldName11, String fieldName12, String fieldName13, String fieldName14, String fieldName15, String fieldName16, String fieldName17, String fieldName18, String fieldName19, String fieldName20, String fieldName21, String fieldName22, String fieldName23, String fieldName24, String fieldName25, String fieldName26, String fieldName27, String fieldName28, String fieldName29, String fieldName30) {
        Map inParams = new HashMap();
        inParams.put("@v_fieldName1Vc", fieldName1);
        inParams.put("@v_fieldName2Vc", fieldName2);
        inParams.put("@v_fieldName3Vc", fieldName3);
        inParams.put("@v_fieldName4Vc", fieldName4);
        inParams.put("@v_fieldName5Vc", fieldName5);
        inParams.put("@v_fieldName6Vc", fieldName6);
        inParams.put("@v_fieldName7Vc", fieldName7);
        inParams.put("@v_fieldName8Vc", fieldName8);
        inParams.put("@v_fieldName9Vc", fieldName9);
        inParams.put("@v_fieldName10Vc", fieldName10);
        inParams.put("@v_fieldName11Vc", fieldName11);
        inParams.put("@v_fieldName12Vc", fieldName12);
        inParams.put("@v_fieldName13Vc", fieldName13);
        inParams.put("@v_fieldName14Vc", fieldName14);
        inParams.put("@v_fieldName15Vc", fieldName15);
        inParams.put("@v_fieldName16Vc", fieldName16);
        inParams.put("@v_fieldName17Vc", fieldName17);
        inParams.put("@v_fieldName18Vc", fieldName18);
        inParams.put("@v_fieldName19Vc", fieldName19);
        inParams.put("@v_fieldName20Vc", fieldName20);
        inParams.put("@v_fieldName21Vc", fieldName21);
        inParams.put("@v_fieldName22Vc", fieldName22);
        inParams.put("@v_fieldName23Vc", fieldName23);
        inParams.put("@v_fieldName24Vc", fieldName24);
        inParams.put("@v_fieldName25Vc", fieldName25);
        inParams.put("@v_fieldName26Vc", fieldName26);
        inParams.put("@v_fieldName27Vc", fieldName27);
        inParams.put("@v_fieldName28Vc", fieldName28);
        inParams.put("@v_fieldName29Vc", fieldName29);
        inParams.put("@v_fieldName30Vc", fieldName30);

        this.compile();

        List<CommonMsgChk> details = new ArrayList<CommonMsgChk>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            if (list != null && !list.isEmpty()) {
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonMsgChk commonMsgChk = new CommonMsgChk();
                commonMsgChk.setFlag((Boolean) linkedHashMap.get("flag"));
                commonMsgChk.setMsg((String) linkedHashMap.get("Message"));
                commonMsgChk.setId((Integer) linkedHashMap.get("Id"));
                details.add(commonMsgChk);

            }
            }
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            LOGGER.error("SPAddUpdOpeningEvaluation: " + sw.toString() + " FUNCTION :" + fieldName1);
        }
        return details;
    }

}
