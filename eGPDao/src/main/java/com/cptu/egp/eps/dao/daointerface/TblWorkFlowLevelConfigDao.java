/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public interface TblWorkFlowLevelConfigDao extends GenericDao<TblWorkFlowLevelConfig> {

    public void addTblWorkFlowLevelConfig(TblWorkFlowLevelConfig admin);

    public void deleteTblWorkFlowLevelConfig(TblWorkFlowLevelConfig admin);

    public void updateTblWorkFlowLevelConfig(TblWorkFlowLevelConfig admin);

    public List<TblWorkFlowLevelConfig> getAllTblWorkFlowLevelConfig();

    public List<TblWorkFlowLevelConfig> findTblWorkFlowLevelConfig(Object... values) throws Exception;

    public List<TblWorkFlowLevelConfig> findByCountTblWorkFlowLevelConfig(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblWorkFlowLevelConfigCount();
}
