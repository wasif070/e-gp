/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblListCellDetailDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblListCellDetail;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblListCellDetailImpl extends AbcAbstractClass<TblListCellDetail> implements TblListCellDetailDao{

    @Override
    public void addTblListCellDetail(TblListCellDetail tblListCellDetail) {
        super.addEntity(tblListCellDetail);
    }

    @Override
    public List<TblListCellDetail> findTblListCellDetail(Object... values) throws Exception {
        return super.findEntity(values);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblListCellDetail(TblListCellDetail tblListCellDetail) {

        super.deleteEntity(tblListCellDetail);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblListCellDetail(TblListCellDetail tblListCellDetail) {

        super.updateEntity(tblListCellDetail);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblListCellDetail> getAllTblListCellDetail() {
        return super.getAllEntity();
    }

    @Override
    public long getTblListCellDetailCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblListCellDetail> findByCountTblListCellDetail(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
