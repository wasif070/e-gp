package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsDateConfig;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblCmsDateConfigDao extends GenericDao<TblCmsDateConfig> {

    public void addTblCmsDateConfig(TblCmsDateConfig event);

    public void deleteTblCmsDateConfig(TblCmsDateConfig event);

    public void updateTblCmsDateConfig(TblCmsDateConfig event);

    public List<TblCmsDateConfig> getAllTblCmsDateConfig();

    public List<TblCmsDateConfig> findTblCmsDateConfig(Object... values) throws Exception;

    public List<TblCmsDateConfig> findByCountTblCmsDateConfig(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsDateConfigCount();
}
