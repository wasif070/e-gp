package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblNegotiatedBid;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblNegotiatedBidDao extends GenericDao<TblNegotiatedBid> {

    public void addTblNegotiatedBid(TblNegotiatedBid tblObj);

    public void deleteTblNegotiatedBid(TblNegotiatedBid tblObj);

    public void updateTblNegotiatedBid(TblNegotiatedBid tblObj);

    public List<TblNegotiatedBid> getAllTblNegotiatedBid();

    public List<TblNegotiatedBid> findTblNegotiatedBid(Object... values) throws Exception;

    public List<TblNegotiatedBid> findByCountTblNegotiatedBid(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblNegotiatedBidCount();
}
