/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTemplateTables;
import java.util.List;

/**
 *
 * @author yanki
 */
public interface TblTemplateTablesDao extends GenericDao<TblTemplateTables>{

    public void addTblTemplateTables(TblTemplateTables templateTables);

    public void deleteTblTemplateTables(TblTemplateTables templateTables);

    public void updateTblTemplateTables(TblTemplateTables templateTables);

    public List<TblTemplateTables> getAllTblTemplateTables();

    public List<TblTemplateTables> findTblTemplateTables(Object... values) throws Exception;
    
}
