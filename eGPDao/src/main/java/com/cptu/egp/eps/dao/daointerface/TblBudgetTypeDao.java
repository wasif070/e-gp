package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblBudgetType;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblBudgetTypeDao extends GenericDao<TblBudgetType> {

    public void addTblBudgetType(TblBudgetType budget);

    public void deleteTblBudgetType(TblBudgetType budget);

    public void updateTblBudgetType(TblBudgetType budget);

    public List<TblBudgetType> getAllTblBudgetType();

    public List<TblBudgetType> findTblBudgetType(Object... values) throws Exception;

    public List<TblBudgetType> findByCountTblBudgetType(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblBudgetTypeCount();
}
