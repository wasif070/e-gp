/*
 * To change this template choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

import java.util.Date;

/**
 *
 * @author Administrator
 */
public class MsgInbox {

    private Integer msgBoxId;
    private Integer msgToUserId;
    private Integer msgFromUserId;
    private Integer msgId;
    private String msgToCCReply;
    private String msgStatus;
    private Date creationDate;
    private Integer folderId;
    private String isPriority;
    private String isMsgRead;
    private String isDocUploaded;
    private String processUrl;
    private String subject;
    private String msgText;

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getFolderId() {
        return folderId;
    }

    public void setFolderId(Integer folderId) {
        this.folderId = folderId;
    }

    public String getIsDocUploaded() {
        return isDocUploaded;
    }

    public void setIsDocUploaded(String isDocUploaded) {
        this.isDocUploaded = isDocUploaded;
    }

    public String getIsMsgRead() {
        return isMsgRead;
    }

    public void setIsMsgRead(String isMsgRead) {
        this.isMsgRead = isMsgRead;
    }

    public String getIsPriority() {
        return isPriority;
    }

    public void setIsPriority(String isPriority) {
        this.isPriority = isPriority;
    }

    public Integer getMsgBoxId() {
        return msgBoxId;
    }

    public void setMsgBoxId(Integer msgBoxId) {
        this.msgBoxId = msgBoxId;
    }

    public Integer getMsgFromUserId() {
        return msgFromUserId;
    }

    public void setMsgFromUserId(Integer msgFromUserId) {
        this.msgFromUserId = msgFromUserId;
    }

    public Integer getMsgId() {
        return msgId;
    }

    public void setMsgId(Integer msgId) {
        this.msgId = msgId;
    }

    public String getMsgStatus() {
        return msgStatus;
    }

    public void setMsgStatus(String msgStatus) {
        this.msgStatus = msgStatus;
    }

    public String getMsgText() {
        return msgText;
    }

    public void setMsgText(String msgText) {
        this.msgText = msgText;
    }

    public String getMsgToCCReply() {
        return msgToCCReply;
    }

    public void setMsgToCCReply(String msgToCCReply) {
        this.msgToCCReply = msgToCCReply;
    }

    public Integer getMsgToUserId() {
        return msgToUserId;
    }

    public void setMsgToUserId(Integer msgToUserId) {
        this.msgToUserId = msgToUserId;
    }

    public String getProcessUrl() {
        return processUrl;
    }

    public void setProcessUrl(String processUrl) {
        this.processUrl = processUrl;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
