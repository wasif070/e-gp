package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblEvalTerreportDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalTerreport;
import java.util.List;

/**
 *
 * @author rishita
 */
public class TblEvalTerreportImpl extends AbcAbstractClass<TblEvalTerreport> implements TblEvalTerreportDao {

    @Override
    public void addTblEvalTerreport(TblEvalTerreport tblEvalTerreport){
        super.addEntity(tblEvalTerreport);
    }

    @Override
    public void deleteTblEvalTerreport(TblEvalTerreport tblEvalTerreport) {
        super.deleteEntity(tblEvalTerreport);
    }

    @Override
    public void updateTblEvalTerreport(TblEvalTerreport tblEvalTerreport) {
        super.updateEntity(tblEvalTerreport);
    }

    @Override
    public List<TblEvalTerreport> getAllTblEvalTerreport() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEvalTerreport> findTblEvalTerreport(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEvalTerreportCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalTerreport> findByCountTblEvalTerreport(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateInsAllEvalTerreport(List<TblEvalTerreport> tblEvalTerreport) {
        super.updateAll(tblEvalTerreport);
    }
}
