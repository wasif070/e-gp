/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderValidityExtDate;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderValidityExtDateDao extends GenericDao<TblTenderValidityExtDate>{

    public void addTblTenderValidityExtDate(TblTenderValidityExtDate tblObj);

    public void deleteTblTenderValidityExtDate(TblTenderValidityExtDate tblObj);

    public void updateTblTenderValidityExtDate(TblTenderValidityExtDate tblObj);

    public List<TblTenderValidityExtDate> getAllTblTenderValidityExtDate();

    public List<TblTenderValidityExtDate> findTblTenderValidityExtDate(Object... values) throws Exception;

    public List<TblTenderValidityExtDate> findByCountTblTenderValidityExtDate(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderValidityExtDateCount();
}
