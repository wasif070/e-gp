/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Administrator
 */
public class SPWeekendsHolidays extends StoredProcedure{
     private static final Logger LOGGER = Logger.getLogger(SPWeekendsHolidays.class);
     public SPWeekendsHolidays(BasicDataSource dataSource, String procName)
     {

       this.setDataSource(dataSource);
       this.setSql(procName);//Stored Procedure name has to be set from DaoContext.

       this.declareParameter(new SqlParameter("v_Getdated", Types.DATE));
       this.declareParameter(new SqlParameter("v_DaysN", Types.INTEGER));
       
    }

     /**
      * Execute stored procedure : p_find_weekends_holidays
      * Get Weekends and Holiday details
      * @param getDated
      * @param daysN
      * @return Weekedns and Holidays details as List of CommonWeekendsHolidays object
      */
     public List<CommonWeekendsHolidays> executeProcedure(Date getDated,Integer daysN){
        Map inParams = new HashMap();

        inParams.put("v_Getdated", getDated);
        inParams.put("v_DaysN", daysN);
        

         this.compile();
         List<CommonWeekendsHolidays> details = new ArrayList<CommonWeekendsHolidays>();
         try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonWeekendsHolidays commonWeekendsHolidays=new CommonWeekendsHolidays();
                commonWeekendsHolidays.setFlag((Boolean)linkedHashMap.get("flag"));
                commonWeekendsHolidays.setFlagMessage((String)linkedHashMap.get("flgMessage"));
                commonWeekendsHolidays.setHolidayDate((Date)linkedHashMap.get("holidayDate"));
               
                details.add(commonWeekendsHolidays);
            }
        } catch (Exception e) {
            LOGGER.error("SPWeekendsHolidays  : "+ e);
        }
      return details;

    }

}
