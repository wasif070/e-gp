package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblNationalityMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblNationalityMasterDao extends GenericDao<TblNationalityMaster> {

    public List<TblNationalityMaster> getAllTblNationalityMaster();

    public List<TblNationalityMaster> findTblNationalityMaster(Object... values) throws Exception;

    public List<TblNationalityMaster> findByCountTblNationalityMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblNationalityMasterCount();
}
