package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCmsPrMaster;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public interface TblCmsPrMasterDao extends GenericDao<TblCmsPrMaster> {

    public void addTblCmsPrMaster(TblCmsPrMaster event);

    public void deleteTblCmsPrMaster(TblCmsPrMaster event);

    public void updateTblCmsPrMaster(TblCmsPrMaster event);

    public List<TblCmsPrMaster> getAllTblCmsPrMaster();

    public List<TblCmsPrMaster> findTblCmsPrMaster(Object... values) throws Exception;

    public List<TblCmsPrMaster> findByCountTblCmsPrMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCmsPrMasterCount();

    public void updateOrSaveEstCost(List<TblCmsPrMaster> estCost);
}
