/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Istiak (Dohatec) - 21.Apr.15
 */
public class SPEvalCommonSearchData extends StoredProcedure {

    private static final Logger LOGGER = Logger.getLogger(SPEvalCommonSearchData.class);

    public SPEvalCommonSearchData(BasicDataSource dataSource, String procName) {
        this.setDataSource(dataSource);
        this.setSql(procName);
        this.declareParameter(new SqlParameter("@v_fieldName1Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName2Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName3Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName4Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName5Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName6Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName7Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName8Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName9Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName10Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName11Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName12Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName13Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName14Vc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@v_fieldName15Vc", Types.VARCHAR));
    }

    /**
     * Execute stored procedure : p_get_evalcommondata
     * To perform Common search operation for whole application base on function name passed in fieldName1 parameter.
     * @param fieldName1 function name to perform search operation.
     * @param fieldName2
     * @param fieldName3
     * @param fieldName4
     * @param fieldName5
     * @param fieldName6
     * @param fieldName7
     * @param fieldName8
     * @param fieldName9
     * @param fieldName10
     * @param fieldName11
     * @param fieldName12
     * @param fieldName13
     * @param fieldName14
     * @param fieldName15
     * @return list of EvalSPCommonSearchData.
     */
    
    public List<EvalCommonSearchData> executeProcedure(String fieldName1, String fieldName2, String fieldName3, String fieldName4, String fieldName5, String fieldName6, String fieldName7, String fieldName8, String fieldName9, String fieldName10, String fieldName11, String fieldName12, String fieldName13, String fieldName14, String fieldName15) {
        Map inParams = new HashMap();
        inParams.put("@v_fieldName1Vc", fieldName1);
        inParams.put("@v_fieldName2Vc", fieldName2);
        inParams.put("@v_fieldName3Vc", fieldName3);
        inParams.put("@v_fieldName4Vc", fieldName4);
        inParams.put("@v_fieldName5Vc", fieldName5);
        inParams.put("@v_fieldName6Vc", fieldName6);
        inParams.put("@v_fieldName7Vc", fieldName7);
        inParams.put("@v_fieldName8Vc", fieldName8);
        inParams.put("@v_fieldName9Vc", fieldName9);
        inParams.put("@v_fieldName10Vc", fieldName10);
        inParams.put("@v_fieldName11Vc", fieldName11);
        inParams.put("@v_fieldName12Vc", fieldName12);
        inParams.put("@v_fieldName13Vc", fieldName13);
        inParams.put("@v_fieldName14Vc", fieldName14);
        inParams.put("@v_fieldName15Vc", fieldName15);

        this.compile();

        List<EvalCommonSearchData> searchData = new ArrayList<EvalCommonSearchData>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            if(list!=null && !list.isEmpty()){
                for (LinkedHashMap<String, Object> linkedHashMap : list) {
                    EvalCommonSearchData evalCommonSearchData = new EvalCommonSearchData();
                    evalCommonSearchData.setFieldName1((String) linkedHashMap.get("FieldValue1"));
                    evalCommonSearchData.setFieldName2((String) linkedHashMap.get("FieldValue2"));
                    evalCommonSearchData.setFieldName3((String) linkedHashMap.get("FieldValue3"));
                    evalCommonSearchData.setFieldName4((String) linkedHashMap.get("FieldValue4"));
                    evalCommonSearchData.setFieldName5((String) linkedHashMap.get("FieldValue5"));
                    evalCommonSearchData.setFieldName6((String) linkedHashMap.get("FieldValue6"));
                    evalCommonSearchData.setFieldName7((String) linkedHashMap.get("FieldValue7"));
                    evalCommonSearchData.setFieldName8((String) linkedHashMap.get("FieldValue8"));
                    evalCommonSearchData.setFieldName9((String) linkedHashMap.get("FieldValue9"));
                    evalCommonSearchData.setFieldName10((String) linkedHashMap.get("FieldValue10"));
                    evalCommonSearchData.setFieldName11((String) linkedHashMap.get("FieldValue11"));
                    evalCommonSearchData.setFieldName12((String) linkedHashMap.get("FieldValue12"));
                    evalCommonSearchData.setFieldName13((String) linkedHashMap.get("FieldValue13"));
                    evalCommonSearchData.setFieldName14((String) linkedHashMap.get("FieldValue14"));
                    evalCommonSearchData.setFieldName15((String) linkedHashMap.get("FieldValue15"));
                    //add by sristy set data type date
                   // evalCommonSearchData.setFieldName16((Date)linkedHashMap.get("FieldValue16"));
                    //end 

                    searchData.add(evalCommonSearchData);
                }
            }
            LOGGER.debug("SPEvalCommonSearchData : No data found for FUNCTION  " + fieldName1);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            LOGGER.error("SPEvalCommonSearchData : " + sw.toString() + " FUNCTION  " + fieldName1);
            System.out.println(""+ e);
        }
        return searchData;
    }
}
