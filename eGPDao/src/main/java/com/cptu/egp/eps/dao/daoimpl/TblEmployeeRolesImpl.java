/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblEmployeeRolesDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEmployeeRoles;
import java.util.List;

/**
 * <b>Class Description goes here</b>
 * $Revision: 1.1.1.1 $
 * @version <version-no.> <Date>
 * @author Administrator
 */
public class TblEmployeeRolesImpl extends AbcAbstractClass<TblEmployeeRoles> implements TblEmployeeRolesDao{

    @Override
    public void addEmployeeRole(TblEmployeeRoles tblEmployeeRoles)
    {
        super.addEntity(tblEmployeeRoles);
    }

    @Override
    public void deleteEmployeeRole(TblEmployeeRoles tblEmployeeRoles)
    {
        super.deleteEntity(tblEmployeeRoles);
    }

    @Override
    public void updateEmployeeRole(TblEmployeeRoles tblEmployeeRoles)
    {
        super.updateEntity(tblEmployeeRoles);
    }

    @Override
    public List<TblEmployeeRoles> getAllEmployeeRoles()
    {
        return super.getAllEntity();
    }

    @Override
    public List<TblEmployeeRoles> findEmployeeRoles(Object... values) throws Exception
    {
        return super.findEntity(values);
    }

}