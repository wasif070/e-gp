package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblGradeMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblGradeMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblGradeMasterImpl extends AbcAbstractClass<TblGradeMaster> implements TblGradeMasterDao {

    @Override
    public void addTblGradeMaster(TblGradeMaster master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblGradeMaster(TblGradeMaster master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblGradeMaster(TblGradeMaster master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblGradeMaster> getAllTblGradeMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblGradeMaster> findTblGradeMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblGradeMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblGradeMaster> findByCountTblGradeMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
