/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderBidForm;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderBidFormDao extends GenericDao<TblTenderBidForm>{

    public void addTblTenderBidForm(TblTenderBidForm tblObj);

    public void deleteTblTenderBidForm(TblTenderBidForm tblObj);

    public void updateTblTenderBidForm(TblTenderBidForm tblObj);

    public List<TblTenderBidForm> getAllTblTenderBidForm();

    public List<TblTenderBidForm> findTblTenderBidForm(Object... values) throws Exception;

    public List<TblTenderBidForm> findByCountTblTenderBidForm(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderBidFormCount();
}
