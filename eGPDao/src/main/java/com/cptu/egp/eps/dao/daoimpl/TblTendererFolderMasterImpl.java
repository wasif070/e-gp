package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblTendererFolderMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTendererFolderMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblTendererFolderMasterImpl extends AbcAbstractClass<TblTendererFolderMaster> implements TblTendererFolderMasterDao {

    @Override
    public void addTblTendererFolderMaster(TblTendererFolderMaster master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblTendererFolderMaster(TblTendererFolderMaster master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblTendererFolderMaster(TblTendererFolderMaster master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblTendererFolderMaster> getAllTblTendererFolderMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTendererFolderMaster> findTblTendererFolderMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTendererFolderMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTendererFolderMaster> findByCountTblTendererFolderMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
