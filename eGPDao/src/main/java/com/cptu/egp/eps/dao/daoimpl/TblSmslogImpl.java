package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblSmslogDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblSmslog;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblSmslogImpl extends AbcAbstractClass<TblSmslog> implements TblSmslogDao {

    @Override
    public void addTblSmslog(TblSmslog tblSmslog){
        super.addEntity(tblSmslog);
    }

    @Override
    public void deleteTblSmslog(TblSmslog tblSmslog) {
        super.deleteEntity(tblSmslog);
    }

    @Override
    public void updateTblSmslog(TblSmslog tblSmslog) {
        super.updateEntity(tblSmslog);
    }

    @Override
    public List<TblSmslog> getAllTblSmslog() {
        return super.getAllEntity();
    }

    @Override
    public List<TblSmslog> findTblSmslog(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblSmslogCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblSmslog> findByCountTblSmslog(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
