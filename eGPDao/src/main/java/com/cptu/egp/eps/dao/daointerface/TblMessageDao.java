package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this late, choose Tools | lates
 * and open the late in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblMessage;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblMessageDao extends GenericDao<TblMessage> {

    public void addTblMessage(TblMessage message);

    public void deleteTblMessage(TblMessage message);

    public void updateTblMessage(TblMessage message);

    public List<TblMessage> getAllTblMessage();

    public List<TblMessage> findTblMessage(Object... values) throws Exception;

    public List<TblMessage> findByCountTblMessage(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblMessageCount();
}
