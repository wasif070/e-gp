package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblMessageDocDetails;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblMessageDocDetailsDao extends GenericDao<TblMessageDocDetails> {

    public void addTblMessageDocDetails(TblMessageDocDetails tblObj);

    public void deleteTblMessageDocDetails(TblMessageDocDetails tblObj);

    public void updateTblMessageDocDetails(TblMessageDocDetails tblObj);

    public List<TblMessageDocDetails> getAllTblMessageDocDetails();

    public List<TblMessageDocDetails> findTblMessageDocDetails(Object... values) throws Exception;

    public List<TblMessageDocDetails> findByCountTblMessageDocDetails(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblMessageDocDetailsCount();
}
