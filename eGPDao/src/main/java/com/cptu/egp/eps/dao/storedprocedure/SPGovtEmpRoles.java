/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author parag
 */
public class SPGovtEmpRoles extends StoredProcedure  {
    private static final Logger LOGGER = Logger.getLogger(SPGovtEmpRoles.class);
    public SPGovtEmpRoles(BasicDataSource dataSource, String procName){
     this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("v_employeeId_inInt", Types.INTEGER));
    }

    /**
     * Execute stored procedure : p_select_govtemployee
     * To get employee details.
     * @param empId
     * @return List of SPGovtEmpRolesReturn object having detail of Gove. employee details
     */
    public List<SPGovtEmpRolesReturn> executeGovtEmpProcedure(int empId) {

        Map inParams = new HashMap();
        inParams.put("v_employeeId_inInt", empId);

        this.compile();

        List<SPGovtEmpRolesReturn> details = new ArrayList<SPGovtEmpRolesReturn>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                SPGovtEmpRolesReturn  pGovtEmpRolesReturn  = new SPGovtEmpRolesReturn();
                 pGovtEmpRolesReturn.setEmployeeName((String) linkedHashMap.get("employeeName"));
                 pGovtEmpRolesReturn.setDesignationName((String) linkedHashMap.get("designationName"));
                 pGovtEmpRolesReturn.setUserId((Integer)linkedHashMap.get("userId"));
                 pGovtEmpRolesReturn.setFinPowerBy((String) linkedHashMap.get("finPowerBy"));
                 pGovtEmpRolesReturn.setDepartmentId((Integer)linkedHashMap.get("departmentId"));
                 pGovtEmpRolesReturn.setDepartmentname((String) linkedHashMap.get("departmentname"));
                 pGovtEmpRolesReturn.setProcurementRoleId((String)linkedHashMap.get("procurementRoleId"));
                 pGovtEmpRolesReturn.setProcurementRole((String) linkedHashMap.get("procurementRole"));
                 pGovtEmpRolesReturn.setOfficeName((String) linkedHashMap.get("officeName"));
                 pGovtEmpRolesReturn.setEmployeeRoleId((String) linkedHashMap.get("employeeRoleId"));
                 pGovtEmpRolesReturn.setOfficeId((String) linkedHashMap.get("officeid"));

                details.add(pGovtEmpRolesReturn);
            }
        } catch (Exception e) {
            LOGGER.error("SPGovtEmpRoles : "+ e);
        }
        return details;
    }

}
