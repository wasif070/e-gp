/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblAppPqTenderDates;
import java.util.List;

/**
 * <b>Interface Description goes here</b>
 * $Revision: 1.1.1.1 $
 * @version <version-no.> <Date>
 * @author Ramesh
 */
public interface TblAppPqTenderDatesDao extends GenericDao<TblAppPqTenderDates>{

    public void addAppPqTenderDates(TblAppPqTenderDates tblAppPqTenderDates);

    public void deleteDesignnation(TblAppPqTenderDates tblAppPqTenderDates);

    public void updateDesignantion(TblAppPqTenderDates tblAppPqTenderDates);

    public List<TblAppPqTenderDates> getAllDesinations();

    public List<TblAppPqTenderDates> findDesignantion(Object... values) throws Exception;

    public List<TblAppPqTenderDates> findByCountAppPqTenderDates(int firstResult,int maxResult,Object... values) throws Exception;

    public long getAppPqTenderDatesCount();

}
