package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblCorrigendumDocs;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblCorrigendumDocsDao extends GenericDao<TblCorrigendumDocs> {

    public void addTblCorrigendumDocs(TblCorrigendumDocs tblObj);

    public void deleteTblCorrigendumDocs(TblCorrigendumDocs tblObj);

    public void updateTblCorrigendumDocs(TblCorrigendumDocs tblObj);

    public List<TblCorrigendumDocs> getAllTblCorrigendumDocs();

    public List<TblCorrigendumDocs> findTblCorrigendumDocs(Object... values) throws Exception;

    public List<TblCorrigendumDocs> findByCountTblCorrigendumDocs(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblCorrigendumDocsCount();
}
