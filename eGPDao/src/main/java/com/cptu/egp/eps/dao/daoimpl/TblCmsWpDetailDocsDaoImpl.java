package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsWpDetailDocsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsWpDetailDocs;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsWpDetailDocsDaoImpl extends AbcAbstractClass<TblCmsWpDetailDocs> implements TblCmsWpDetailDocsDao {

    @Override
    public void addTblCmsWpDetailDocs(TblCmsWpDetailDocs Docsobj){
        super.addEntity(Docsobj);
    }

    @Override
    public void deleteTblCmsWpDetailDocs(TblCmsWpDetailDocs Docsobj) {
        super.deleteEntity(Docsobj);
    }

    @Override
    public void updateTblCmsWpDetailDocs(TblCmsWpDetailDocs Docsobj) {
        super.updateEntity(Docsobj);
    }

    @Override
    public List<TblCmsWpDetailDocs> getAllTblCmsWpDetailDocs() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsWpDetailDocs> findTblCmsWpDetailDocs(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsWpDetailDocsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsWpDetailDocs> findByCountTblCmsWpDetailDocs(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
