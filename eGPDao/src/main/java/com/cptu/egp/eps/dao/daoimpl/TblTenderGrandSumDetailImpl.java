/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderGrandSumDetailDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderGrandSumDetail;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderGrandSumDetailImpl extends AbcAbstractClass<TblTenderGrandSumDetail> implements TblTenderGrandSumDetailDao {

    @Override
    public void addTblTenderGrandSumDetail(TblTenderGrandSumDetail tenderGrandSumDetail) {
        super.addEntity(tenderGrandSumDetail);
    }

    @Override
    public void deleteTblTenderGrandSumDetail(TblTenderGrandSumDetail tenderGrandSumDetail) {
        super.deleteEntity(tenderGrandSumDetail);
    }

    @Override
    public void updateTblTenderGrandSumDetail(TblTenderGrandSumDetail tenderGrandSumDetail) {
        super.updateEntity(tenderGrandSumDetail);
    }

    @Override
    public List<TblTenderGrandSumDetail> getAllTblTenderGrandSumDetail() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderGrandSumDetail> findTblTenderGrandSumDetail(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderGrandSumDetailCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderGrandSumDetail> findByCountTblTenderGrandSumDetail(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateSaveAllTenderGrandSumDetail(List<TblTenderGrandSumDetail> lstTenderGrandSumDetail)
    {
         super.updateAll(lstTenderGrandSumDetail);
    }
}
