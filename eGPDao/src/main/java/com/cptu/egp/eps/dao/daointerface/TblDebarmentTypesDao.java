package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblDebarmentTypes;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblDebarmentTypesDao extends GenericDao<TblDebarmentTypes> {

    public void addTblDebarmentTypes(TblDebarmentTypes tblObj);

    public void deleteTblDebarmentTypes(TblDebarmentTypes tblObj);

    public void updateTblDebarmentTypes(TblDebarmentTypes tblObj);

    public List<TblDebarmentTypes> getAllTblDebarmentTypes();

    public List<TblDebarmentTypes> findTblDebarmentTypes(Object... values) throws Exception;

    public List<TblDebarmentTypes> findByCountTblDebarmentTypes(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblDebarmentTypesCount();
}
