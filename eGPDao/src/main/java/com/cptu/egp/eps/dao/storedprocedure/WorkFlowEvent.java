/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Ramesh.Janagondakuru
 */
public class WorkFlowEvent extends StoredProcedure{
    private static final Logger LOGGER = Logger.getLogger(WorkFlowEvent.class);
    public WorkFlowEvent(BasicDataSource dataSource, String procName){
         this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
        this.declareParameter(new SqlParameter("v_noOfReviewer_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_noOfDays_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_isDonorConReq_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_eventid_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_objectId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_WfRoleId_inInt", Types.INTEGER));
        this.declareParameter(new SqlParameter("v_Action_inVc", Types.VARCHAR));
        this.declareParameter(new SqlParameter("v_wfEvtConfId_inInt", Types.INTEGER));
         this.declareParameter(new SqlParameter("v_actiondate_inDt", Types.DATE));
        this.declareParameter(new SqlParameter("v_actionby_inInt", Types.INTEGER));

    }

    /**
     * Execute stored procedure : p_ins_upd_workfloweventconfig
     * Perform Insert/Update work flow event config.
     * @param noOfReviewer
     * @param noOfDays
     * @param isDonorConReq
     * @param eventid
     * @param objectId
     * @param WfRoleId
     * @param action
     * @param wfEvtConfId
     * @param actiondate
     * @param actionby
     * @return CommonMsgChk list contains flags indicate operation successful or not.
     */
 public List<CommonMsgChk> executeProcedure(Integer noOfReviewer,Integer noOfDays,String isDonorConReq,Integer eventid,Integer  objectId,Integer WfRoleId,String action,Integer wfEvtConfId,Date actiondate,Integer actionby  ){
     Map inParams = new HashMap();
        inParams.put("v_noOfReviewer_inInt", noOfReviewer);
        inParams.put("v_noOfDays_inInt", noOfDays);
        inParams.put("v_isDonorConReq_inVc", isDonorConReq);
        inParams.put("v_eventid_inInt", eventid);
        inParams.put("v_objectId_inInt", objectId);
        inParams.put("v_WfRoleId_inInt", WfRoleId);
        inParams.put("v_Action_inVc", action);
        inParams.put("v_wfEvtConfId_inInt", wfEvtConfId);
        inParams.put("v_actiondate_inDt", actiondate);
        inParams.put("v_actionby_inInt", actionby);
       this.compile();
        List<CommonMsgChk> details = new ArrayList<CommonMsgChk>();
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonMsgChk commonMsgChk = new CommonMsgChk();
                commonMsgChk.setFlag((Boolean) linkedHashMap.get("flag"));
                commonMsgChk.setMsg((String) linkedHashMap.get("msg"));
                details.add(commonMsgChk);
            }
        } catch (Exception e) {
            LOGGER.error("WorkFlowEvent  : "+ e);
        }
        return details;
 }
}
