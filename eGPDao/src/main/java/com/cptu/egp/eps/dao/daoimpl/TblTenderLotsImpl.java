/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderLotsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderLots;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderLotsImpl extends AbcAbstractClass<TblTenderLots> implements TblTenderLotsDao {

    @Override
    public void addTblTenderLots(TblTenderLots admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblTenderLots(TblTenderLots admin) {
        super.deleteEntity(admin);        
    }

    @Override
    public void updateTblTenderLots(TblTenderLots admin) {
        super.updateEntity(admin);        
    }

    @Override
    public List<TblTenderLots> getAllTblTenderLots() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderLots> findTblTenderLots(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderLotsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderLots> findByCountTblTenderLots(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
