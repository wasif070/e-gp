package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblRegDocReceiptDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblRegDocReceipt;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblRegDocReceiptImpl extends AbcAbstractClass<TblRegDocReceipt> implements TblRegDocReceiptDao {

    @Override
    public void addTblRegDocReceipt(TblRegDocReceipt regDocReceipt){
        super.addEntity(regDocReceipt);
    }

    @Override
    public void deleteTblRegDocReceipt(TblRegDocReceipt regDocReceipt) {
        super.deleteEntity(regDocReceipt);
    }

    @Override
    public void updateTblRegDocReceipt(TblRegDocReceipt regDocReceipt) {
        super.updateEntity(regDocReceipt);
    }

    @Override
    public List<TblRegDocReceipt> getAllTblRegDocReceipt() {
        return super.getAllEntity();
    }

    @Override
    public List<TblRegDocReceipt> findTblRegDocReceipt(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblRegDocReceiptCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblRegDocReceipt> findByCountTblRegDocReceipt(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
