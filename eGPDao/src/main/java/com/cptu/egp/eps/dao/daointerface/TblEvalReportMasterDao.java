package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEvalReportMaster;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblEvalReportMasterDao extends GenericDao<TblEvalReportMaster> {

    public void addTblEvalReportMaster(TblEvalReportMaster tblEvalReportMaster);

    public void deleteTblEvalReportMaster(TblEvalReportMaster tblEvalReportMaster);

    public void deleteAllTblEvalReportMaster(Collection tblEvalReportMaster);

    public void updateTblEvalReportMaster(TblEvalReportMaster tblEvalReportMaster);

    public List<TblEvalReportMaster> getAllTblEvalReportMaster();

    public List<TblEvalReportMaster> findTblEvalReportMaster(Object... values) throws Exception;

    public List<TblEvalReportMaster> findByCountTblEvalReportMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblEvalReportMasterCount();
}
