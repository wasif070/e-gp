/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTemplateSections;
import java.util.List;

/**
 *
 * @author yanki
 */
public interface TblTemplateSectionsDao extends GenericDao<TblTemplateSections>{

    public void addTblTemplateSections(TblTemplateSections templateSection);

    public void deleteTblTemplateSections(TblTemplateSections templateSection);

    public void updateTblTemplateSections(TblTemplateSections templateSection);

    public List<TblTemplateSections> getAllTblTemplateSections();

    public List<TblTemplateSections> findTblTemplateSections(Object... values) throws Exception;

}
