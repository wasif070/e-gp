/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.model.table.TblDebarmentResp;
import java.util.List;

/**
 *
 * @author test
 */
public interface TblDebarmentRespDao {

    public void addTblDebarmentResp(TblDebarmentResp tblDebarmentResp);

    public void deleteTblDebarmentResp(TblDebarmentResp tblDebarmentResp);

    public void updateTblDebarmentResp(TblDebarmentResp tblDebarmentResp);

    public List<TblDebarmentResp> getAllTblDebarmentResp();

    public List<TblDebarmentResp> findTblTblDebarmentResp(Object... values) throws Exception;

    public List<TblDebarmentResp> findByCountTblDebarmentResp(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTblDebarmentResp();
}
