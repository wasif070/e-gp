/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblBidModificationDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblBidModification;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblBidModificationImpl extends AbcAbstractClass<TblBidModification> implements TblBidModificationDao {

    @Override
    public void addTblBidModification(TblBidModification admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblBidModification(TblBidModification admin) {
        super.deleteEntity(admin);        
    }

    @Override
    public void updateTblBidModification(TblBidModification admin) {
        super.updateEntity(admin);        
    }

    @Override
    public List<TblBidModification> getAllTblBidModification() {
        return super.getAllEntity();
    }

    @Override
    public List<TblBidModification> findTblBidModification(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblBidModificationCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblBidModification> findByCountTblBidModification(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
