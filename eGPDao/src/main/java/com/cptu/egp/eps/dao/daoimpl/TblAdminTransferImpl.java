package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblAdminTransferDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblAdminTransfer;
import java.util.List;

/**
 *
 * @author rishita
 */
public class TblAdminTransferImpl extends AbcAbstractClass<TblAdminTransfer> implements TblAdminTransferDao {

    @Override
    public void addTblAdminTransfer(TblAdminTransfer AdminTransfer){
        super.addEntity(AdminTransfer);
    }

    @Override
    public void deleteTblAdminTransfer(TblAdminTransfer AdminTransfer) {
        super.deleteEntity(AdminTransfer);
    }

    @Override
    public void updateTblAdminTransfer(TblAdminTransfer AdminTransfer) {
        super.updateEntity(AdminTransfer);
    }

    @Override
    public List<TblAdminTransfer> getAllTblAdminTransfer() {
        return super.getAllEntity();
    }

    @Override
    public List<TblAdminTransfer> findTblAdminTransfer(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblAdminTransferCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblAdminTransfer> findByCountTblAdminTransfer(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
