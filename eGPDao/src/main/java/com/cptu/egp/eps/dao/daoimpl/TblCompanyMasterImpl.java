/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblCompanyMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCompanyMaster;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class TblCompanyMasterImpl extends AbcAbstractClass<TblCompanyMaster> implements TblCompanyMasterDao {

    @Override
    public void addTblCompanyMaster(TblCompanyMaster master){
        super.addEntity(master);
}

    @Override
    public void deleteTblCompanyMaster(TblCompanyMaster master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblCompanyMaster(TblCompanyMaster master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblCompanyMaster> getAllTblCompanyMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCompanyMaster> findTblCompanyMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCompanyMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCompanyMaster> findByCountTblCompanyMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
