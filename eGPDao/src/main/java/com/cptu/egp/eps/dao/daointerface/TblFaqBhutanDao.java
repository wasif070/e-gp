/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblActivityMaster;
import com.cptu.egp.eps.model.table.TblFaqBhutan;
import java.util.List;

/**
 *
 * @author Nitish Oritro
 */
public interface TblFaqBhutanDao extends GenericDao<TblFaqBhutan>
{
    public void addTblFaqBhutan(TblFaqBhutan tblObj);

    public void deleteTblFaqBhutan(TblFaqBhutan tblObj);

    public void updateTblFaqBhutan(TblFaqBhutan tblObj);

    public List<TblFaqBhutan> getAllTblFaqBhutan();

    public List<TblFaqBhutan> findTblFaqBhutan(Object... values) throws Exception;

    public List<TblFaqBhutan> findByCountTblFaqBhutan(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblFaqBhutanCount();
}
