/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblProjectOfficeDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblProjectOffice;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblProjectOfficeImpl extends AbcAbstractClass<TblProjectOffice> implements TblProjectOfficeDao{

    @Override
    public void addTblProjectOffice(TblProjectOffice tblProjectOffice) {
        super.addEntity(tblProjectOffice);
    }

    @Override
    public List<TblProjectOffice> findTblProjectOffice(Object... values) throws Exception {
        return super.findEntity(values);
        
    }

    @Override
    public void deleteTblProjectOffice(TblProjectOffice tblProjectOffice) {

        super.deleteEntity(tblProjectOffice);
       
    }

    @Override
    public void updateTblProjectOffice(TblProjectOffice tblProjectOffice) {

        super.updateEntity(tblProjectOffice);
        
    }

    @Override
    public List<TblProjectOffice> getAllTblProjectOffice() {
        return super.getAllEntity();
    }

    @Override
    public long getTblProjectOfficeCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblProjectOffice> findByCountTblProjectOffice(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
