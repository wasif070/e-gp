/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.storedprocedure;

/**
 *
 * @author rishita
 */
public class UserStatisticsDtBean {

    private long countMinistry;
    private long countDivision;
    private long countOrganization;
    private long countPEOfficeMinistry;
    private long countPEOfficeDivision;
    private long countPEOfficeOrganization;
    private long countPEAdminUserMinistry;
    private long countPEAdminUserDivsion;
    private long countPEAdminUserOrganization;
    private long countGovtUserMinistry;
    private long countGovtUserDivsion;
    private long countGovtUserOrganization;
    private long countDevelopmentPartner;
    private long countDevelopmentPartnerRCOffices;
    private long countDevelopmentPartnerAdminUser;
    private long countDevelopmentPartnerUser;
    private long countScheduleBank;
    private long countScheduleBankBranches;
    private long countScheduleBankAdminUsers;
    private long countScheduleBankUsers;

    public long getCountDevelopmentPartner() {
        return countDevelopmentPartner;
    }

    public void setCountDevelopmentPartner(long countDevelopmentPartner) {
        this.countDevelopmentPartner = countDevelopmentPartner;
    }

    public long getCountDevelopmentPartnerAdminUser() {
        return countDevelopmentPartnerAdminUser;
    }

    public void setCountDevelopmentPartnerAdminUser(long countDevelopmentPartnerAdminUser) {
        this.countDevelopmentPartnerAdminUser = countDevelopmentPartnerAdminUser;
    }

    public long getCountDevelopmentPartnerRCOffices() {
        return countDevelopmentPartnerRCOffices;
    }

    public void setCountDevelopmentPartnerRCOffices(long countDevelopmentPartnerRCOffices) {
        this.countDevelopmentPartnerRCOffices = countDevelopmentPartnerRCOffices;
    }

    public long getCountDevelopmentPartnerUser() {
        return countDevelopmentPartnerUser;
    }

    public void setCountDevelopmentPartnerUser(long countDevelopmentPartnerUser) {
        this.countDevelopmentPartnerUser = countDevelopmentPartnerUser;
    }

    public long getCountGovtUserDivsion() {
        return countGovtUserDivsion;
    }

    public void setCountGovtUserDivsion(long countGovtUserDivsion) {
        this.countGovtUserDivsion = countGovtUserDivsion;
    }

    public long getCountGovtUserMinistry() {
        return countGovtUserMinistry;
    }

    public void setCountGovtUserMinistry(long countGovtUserMinistry) {
        this.countGovtUserMinistry = countGovtUserMinistry;
    }

    public long getCountGovtUserOrganization() {
        return countGovtUserOrganization;
    }

    public void setCountGovtUserOrganization(long countGovtUserOrganization) {
        this.countGovtUserOrganization = countGovtUserOrganization;
    }

    public long getCountPEAdminUserDivsion() {
        return countPEAdminUserDivsion;
    }

    public void setCountPEAdminUserDivsion(long countPEAdminUserDivsion) {
        this.countPEAdminUserDivsion = countPEAdminUserDivsion;
    }

    public long getCountPEAdminUserMinistry() {
        return countPEAdminUserMinistry;
    }

    public void setCountPEAdminUserMinistry(long countPEAdminUserMinistry) {
        this.countPEAdminUserMinistry = countPEAdminUserMinistry;
    }

    public long getCountPEAdminUserOrganization() {
        return countPEAdminUserOrganization;
    }

    public void setCountPEAdminUserOrganization(long countPEAdminUserOrganization) {
        this.countPEAdminUserOrganization = countPEAdminUserOrganization;
    }

    public long getCountScheduleBank() {
        return countScheduleBank;
    }

    public void setCountScheduleBank(long countScheduleBank) {
        this.countScheduleBank = countScheduleBank;
    }

    public long getCountScheduleBankAdminUsers() {
        return countScheduleBankAdminUsers;
    }

    public void setCountScheduleBankAdminUsers(long countScheduleBankAdminUsers) {
        this.countScheduleBankAdminUsers = countScheduleBankAdminUsers;
    }

    public long getCountScheduleBankBranches() {
        return countScheduleBankBranches;
    }

    public void setCountScheduleBankBranches(long countScheduleBankBranches) {
        this.countScheduleBankBranches = countScheduleBankBranches;
    }

    public long getCountScheduleBankUsers() {
        return countScheduleBankUsers;
    }

    public void setCountScheduleBankUsers(long countScheduleBankUsers) {
        this.countScheduleBankUsers = countScheduleBankUsers;
    }

    public long getCountDivision() {
        return countDivision;
    }

    public void setCountDivision(long countDivision) {
        this.countDivision = countDivision;
    }

    public long getCountMinistry() {
        return countMinistry;
    }

    public void setCountMinistry(long countMinistry) {
        this.countMinistry = countMinistry;
    }

    public long getCountOrganization() {
        return countOrganization;
    }

    public void setCountOrganization(long countOrganization) {
        this.countOrganization = countOrganization;
    }

    public long getCountPEOfficeDivision() {
        return countPEOfficeDivision;
    }

    public void setCountPEOfficeDivision(long countPEOfficeDivision) {
        this.countPEOfficeDivision = countPEOfficeDivision;
    }

    public long getCountPEOfficeMinistry() {
        return countPEOfficeMinistry;
    }

    public void setCountPEOfficeMinistry(long countPEOfficeMinistry) {
        this.countPEOfficeMinistry = countPEOfficeMinistry;
    }

    public long getCountPEOfficeOrganization() {
        return countPEOfficeOrganization;
    }

    public void setCountPEOfficeOrganization(long countPEOfficeOrganization) {
        this.countPEOfficeOrganization = countPEOfficeOrganization;
    }
}
