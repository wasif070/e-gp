package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblNationalityMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblNationalityMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblNationalityMasterImpl extends AbcAbstractClass<TblNationalityMaster> implements TblNationalityMasterDao {
   
    @Override
    public List<TblNationalityMaster> getAllTblNationalityMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblNationalityMaster> findTblNationalityMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblNationalityMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblNationalityMaster> findByCountTblNationalityMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
