package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTempCompanyDocuments;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblTempCompanyDocumentsDao extends GenericDao<TblTempCompanyDocuments> {

    public void addTblTempCompanyDocuments(TblTempCompanyDocuments document);

    public void deleteTblTempCompanyDocuments(TblTempCompanyDocuments document);

    public void updateTblTempCompanyDocuments(TblTempCompanyDocuments document);

    public List<TblTempCompanyDocuments> getAllTblTempCompanyDocuments();

    public List<TblTempCompanyDocuments> findTblTempCompanyDocuments(Object... values) throws Exception;

    public List<TblTempCompanyDocuments> findByCountTblTempCompanyDocuments(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTempCompanyDocumentsCount();
}
