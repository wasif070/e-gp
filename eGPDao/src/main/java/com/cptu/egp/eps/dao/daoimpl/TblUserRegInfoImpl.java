package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblUserRegInfoDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblUserRegInfo;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblUserRegInfoImpl extends AbcAbstractClass<TblUserRegInfo> implements TblUserRegInfoDao {

    @Override
    public void addTblUserRegInfo(TblUserRegInfo tblUserRegInfo){
        super.addEntity(tblUserRegInfo);
    }

    @Override
    public void deleteTblUserRegInfo(TblUserRegInfo tblUserRegInfo) {
        super.deleteEntity(tblUserRegInfo);
    }

    @Override
    public void updateTblUserRegInfo(TblUserRegInfo tblUserRegInfo) {
        super.updateEntity(tblUserRegInfo);
    }

    @Override
    public List<TblUserRegInfo> getAllTblUserRegInfo() {
        return super.getAllEntity();
    }

    @Override
    public List<TblUserRegInfo> findTblUserRegInfo(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblUserRegInfoCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblUserRegInfo> findByCountTblUserRegInfo(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
