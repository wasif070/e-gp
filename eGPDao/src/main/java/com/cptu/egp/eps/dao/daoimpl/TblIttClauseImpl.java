/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblIttClauseDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblIttClause;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public class TblIttClauseImpl extends AbcAbstractClass<TblIttClause> implements TblIttClauseDao {

    @Override
    public void addTblIttClause(TblIttClause ittClause) {

        super.addEntity(ittClause);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblIttClause(TblIttClause ittClause) {

        super.deleteEntity(ittClause);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblIttClause(TblIttClause ittClause) {

        super.updateEntity(ittClause);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblIttClause> getAllTblIttClause() {
        return super.getAllEntity();
    }

    @Override
    public List<TblIttClause> findTblIttClause(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblIttClauseCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblIttClause> findByCountTblIttClause(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
