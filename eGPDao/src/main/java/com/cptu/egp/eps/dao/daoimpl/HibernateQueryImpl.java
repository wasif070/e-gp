/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Administrator
 */
public class HibernateQueryImpl extends AbcAbstractClass<Object> implements HibernateQueryDao {

    @Override
    public List<Object[]> createNewQuery(String query) {
        return super.createQuery(query);
    }    

    @Override
    public List<Object[]> createByCountNewQuery(String query, int firstResult, int maxResult) {
        return super.createByCountQuery(query, firstResult, maxResult);
    }

    @Override
    public long countForNewQuery(String from, String where) throws Exception {
        return super.countForQuery(from, where);
    }

    @Override
    public List<Object> getSingleColQuery(String query) {
        return super.singleColQuery(query);
    }

    @Override
    public int updateDeleteNewQuery(String query) {
        return super.updateDeleteQuery(query);
    }

    @Override
    public int updateDeleteSQLNewQuery(String query){
        return super.updateDeleteSQLQuery(query);
    }
    
    @Override
    public List<Object[]> nativeSQLQuery(String query, Map<String, Object> var) {
        return super.createSQLQuery(query,var);
    }
}
