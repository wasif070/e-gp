package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblConfigClarificationDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblConfigClarification;
import java.util.List;

/**
 *
 * @author MD. Emtazul Haque
 */
public class TblConfigClarificationImpl extends AbcAbstractClass<TblConfigClarification> implements TblConfigClarificationDao {

    @Override
    public void addTblConfigClarification(TblConfigClarification configClarification){
        super.addEntity(configClarification);
    }

    @Override
    public void deleteTblConfigClarification(TblConfigClarification configClarification) {
        super.deleteEntity(configClarification);
    }

    @Override
    public void updateTblConfigClarification(TblConfigClarification configClarification) {
        super.updateEntity(configClarification);
    }

    @Override
    public List<TblConfigClarification> getAllTblConfigClarification() {
        return super.getAllEntity();
    }

    @Override
    public List<TblConfigClarification> findTblConfigClarification(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblConfigClarificationCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblConfigClarification> findByCountTblConfigClarification(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateInsertAllConfigClarification(List<TblConfigClarification> list) {
        super.updateAll(list);
    }
}
