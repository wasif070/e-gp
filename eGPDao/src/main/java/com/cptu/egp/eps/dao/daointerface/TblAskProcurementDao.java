package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblAskProcurement;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblAskProcurementDao extends GenericDao<TblAskProcurement> {

    public void addTblAskProcurement(TblAskProcurement tblObj);

    public void deleteTblAskProcurement(TblAskProcurement tblObj);

    public void updateTblAskProcurement(TblAskProcurement tblObj);

    public List<TblAskProcurement> getAllTblAskProcurement();

    public List<TblAskProcurement> findTblAskProcurement(Object... values) throws Exception;

    public List<TblAskProcurement> findByCountTblAskProcurement(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblAskProcurementCount();
}
