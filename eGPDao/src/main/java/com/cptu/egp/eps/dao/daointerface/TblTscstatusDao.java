package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTscstatus;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblTscstatusDao extends GenericDao<TblTscstatus> {

    public void addTblTscstatus(TblTscstatus tblObj);

    public void deleteTblTscstatus(TblTscstatus tblObj);

    public void updateTblTscstatus(TblTscstatus tblObj);

    public List<TblTscstatus> getAllTblTscstatus();

    public List<TblTscstatus> findTblTscstatus(Object... values) throws Exception;

    public List<TblTscstatus> findByCountTblTscstatus(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTscstatusCount();
}
