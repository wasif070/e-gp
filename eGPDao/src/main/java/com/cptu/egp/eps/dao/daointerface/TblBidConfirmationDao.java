/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblBidConfirmation;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblBidConfirmationDao extends GenericDao<TblBidConfirmation>{

    public void addTblBidConfirmation(TblBidConfirmation tblObj);

    public void deleteTblBidConfirmation(TblBidConfirmation tblObj);

    public void updateTblBidConfirmation(TblBidConfirmation tblObj);

    public List<TblBidConfirmation> getAllTblBidConfirmation();

    public List<TblBidConfirmation> findTblBidConfirmation(Object... values) throws Exception;

    public List<TblBidConfirmation> findByCountTblBidConfirmation(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblBidConfirmationCount();
}
