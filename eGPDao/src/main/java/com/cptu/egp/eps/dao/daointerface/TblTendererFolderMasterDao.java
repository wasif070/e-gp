package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTendererFolderMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblTendererFolderMasterDao extends GenericDao<TblTendererFolderMaster> {

    public void addTblTendererFolderMaster(TblTendererFolderMaster folder);

    public void deleteTblTendererFolderMaster(TblTendererFolderMaster folder);

    public void updateTblTendererFolderMaster(TblTendererFolderMaster folder);

    public List<TblTendererFolderMaster> getAllTblTendererFolderMaster();

    public List<TblTendererFolderMaster> findTblTendererFolderMaster(Object... values) throws Exception;

    public List<TblTendererFolderMaster> findByCountTblTendererFolderMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTendererFolderMasterCount();
}
