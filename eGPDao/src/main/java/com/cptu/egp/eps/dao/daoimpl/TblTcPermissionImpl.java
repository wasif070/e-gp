/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;
import com.cptu.egp.eps.dao.daointerface.TblTcPermissionDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTcpermission;
import java.util.List;
/**
 *
 * @author Dohatec
 */
public class TblTcPermissionImpl extends AbcAbstractClass<TblTcpermission> implements TblTcPermissionDao{
    @Override
    public void addTblTcpermission(TblTcpermission tblObj){
            super.addEntity(tblObj);
    }
    @Override
    public void deleteTblTcpermission(TblTcpermission tblObj) {
            super.deleteEntity(tblObj);
    }
    @Override
     public void updateTblTcpermission(TblTcpermission tblObj) {
            super.updateEntity(tblObj);
    }
    @Override
     public List<TblTcpermission> getAllTblTcpermission() {
            return super.getAllEntity();
    }
    @Override
    public List<TblTcpermission> findTblTcpermission(Object... values) throws Exception {
            return super.findEntity(values);
    }
    @Override
    public long getTblTcpermissionCount() {
            return super.getEntityCount();
    }
    @Override
    public List<TblTcpermission> findByCountTblTcpermission(int firstResult, int maxResult, Object... values) throws Exception {
            return super.findByCountEntity(firstResult, maxResult, values);
    }
    @Override
    public void updateOrSaveTblTcpermission(List<TblTcpermission> tblObj) {
            super.updateAll(tblObj);
    }
}
