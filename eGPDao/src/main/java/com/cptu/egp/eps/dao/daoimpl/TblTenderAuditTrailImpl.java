/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderAuditTrailDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderAuditTrail;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderAuditTrailImpl extends AbcAbstractClass<TblTenderAuditTrail> implements TblTenderAuditTrailDao {

    @Override
    public void addTblTenderAuditTrail(TblTenderAuditTrail admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblTenderAuditTrail(TblTenderAuditTrail admin) {
        super.deleteEntity(admin);    
    }

    @Override
    public void updateTblTenderAuditTrail(TblTenderAuditTrail admin) {
        super.updateEntity(admin);
    }

    @Override
    public List<TblTenderAuditTrail> getAllTblTenderAuditTrail() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderAuditTrail> findTblTenderAuditTrail(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderAuditTrailCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderAuditTrail> findByCountTblTenderAuditTrail(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
