/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderCurrency;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderCurrencyDao extends GenericDao<TblTenderCurrency>{

    public void addTblTenderCurrency(TblTenderCurrency tblObj);

    public void deleteTblTenderCurrency(TblTenderCurrency tblObj);

    public void updateTblTenderCurrency(TblTenderCurrency tblObj);

    public List<TblTenderCurrency> getAllTblTenderCurrency();

    public List<TblTenderCurrency> findTblTenderCurrency(Object... values) throws Exception;

    public List<TblTenderCurrency> findByCountTblTenderCurrency(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderCurrencyCount();
}
