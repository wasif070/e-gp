package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblUserTypeMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblUserTypeMaster;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblUserTypeMasterImpl extends AbcAbstractClass<TblUserTypeMaster> implements TblUserTypeMasterDao {
   
    @Override
    public List<TblUserTypeMaster> getAllTblUserTypeMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblUserTypeMaster> findTblUserTypeMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblUserTypeMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblUserTypeMaster> findByCountTblUserTypeMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void addTblUserTypeMaster(TblUserTypeMaster tblusermaster) {
        super.addEntity(tblusermaster);
    }

    @Override
    public void deleteTblUserTypeMaster(TblUserTypeMaster tblusermaster) {
        super.deleteEntity(tblusermaster);
    }

    @Override
    public void updateTblUserTypeMaster(TblUserTypeMaster tblusermaster) {
        super.updateEntity(tblusermaster);
    }
}
