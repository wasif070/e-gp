/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderMasterImpl extends AbcAbstractClass<TblTenderMaster> implements TblTenderMasterDao {

    @Override
    public void addTblTenderMaster(TblTenderMaster admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblTenderMaster(TblTenderMaster admin) {
        super.deleteEntity(admin);    
    }

    @Override
    public void updateTblTenderMaster(TblTenderMaster admin) {
        super.updateEntity(admin);
    }

    @Override
    public List<TblTenderMaster> getAllTblTenderMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderMaster> findTblTenderMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderMaster> findByCountTblTenderMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
