/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderColumnsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderColumns;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public class TblTenderColumnsImpl extends AbcAbstractClass<TblTenderColumns> implements TblTenderColumnsDao {

    @Override
    public void addTblTenderColumns(TblTenderColumns admin) {
        super.addEntity(admin);        
    }

    @Override
    public void deleteTblTenderColumns(TblTenderColumns admin) {
        super.deleteEntity(admin);        
    }

    @Override
    public void updateTblTenderColumns(TblTenderColumns admin) {
        super.updateEntity(admin);        
    }

    @Override
    public List<TblTenderColumns> getAllTblTenderColumns() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderColumns> findTblTenderColumns(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblTenderColumnsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblTenderColumns> findByCountTblTenderColumns(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
