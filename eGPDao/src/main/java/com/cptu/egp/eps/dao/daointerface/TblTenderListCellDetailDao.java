/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.model.table.TblTenderListCellDetail;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TblTenderListCellDetailDao {

    public void addTblTenderListCellDetail(TblTenderListCellDetail tblObj);

    public void deleteTblTenderListCellDetail(TblTenderListCellDetail tblObj);

    public void updateTblTenderListCellDetail(TblTenderListCellDetail tblObj);

    public List<TblTenderListCellDetail> getAllTblTenderListCellDetail();

    public List<TblTenderListCellDetail> findTblTenderListCellDetail(Object... values) throws Exception;

    public List<TblTenderListCellDetail> findByCountTblTenderListCellDetail(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderListCellDetailCount();

    public void updateOrSaveAllTblTenderListCellDetail(List<TblTenderListCellDetail> list);
}
