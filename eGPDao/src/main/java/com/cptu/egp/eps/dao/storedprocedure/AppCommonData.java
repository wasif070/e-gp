/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *
 * @author Ramesh.Janagondakuru
 */
public class AppCommonData extends StoredProcedure{
   private static final Logger LOGGER = Logger.getLogger(AppCommonData.class);
   public AppCommonData(BasicDataSource dataSource, String procName){

        this.setDataSource(dataSource);
        this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
       this.declareParameter(new SqlParameter("v_fieldName1Vc", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_fieldName2Vc", Types.VARCHAR));
       this.declareParameter(new SqlParameter("v_fieldName3Vc", Types.VARCHAR));
    }
   public List<CommonAppData> executeProcedure(String fieldName1,String fieldName2,String fieldName3){
        Map inParams = new HashMap();
        

        inParams.put("v_fieldName1Vc", fieldName1);
        inParams.put("v_fieldName2Vc", fieldName2);
        inParams.put("v_fieldName3Vc", fieldName3);

         this.compile();
         List<CommonAppData>   details = new ArrayList<CommonAppData>();
         try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            for (LinkedHashMap<String, Object> linkedHashMap : list) {
                CommonAppData cmmAppData=new CommonAppData();
                cmmAppData.setFieldName1((String)linkedHashMap.get("FieldValue1"));
                cmmAppData.setFieldName2((String)linkedHashMap.get("FieldValue2"));
                cmmAppData.setFieldName3((String)linkedHashMap.get("FieldValue3"));
                cmmAppData.setFieldName4((String)linkedHashMap.get("FieldValue4"));
                cmmAppData.setFieldName5((String)linkedHashMap.get("FieldValue5"));
                cmmAppData.setFieldName6((String)linkedHashMap.get("FieldValue6"));
                cmmAppData.setFieldName7((String)linkedHashMap.get("FieldValue7")); 
                cmmAppData.setFieldName8((String)linkedHashMap.get("FieldValue8"));
                cmmAppData.setFieldName9((String)linkedHashMap.get("FieldValue9"));       

                details.add(cmmAppData);
            }
        } catch (Exception e) {
            LOGGER.error("AppCommonData : " + e);
        }
      return details;

    }
}
