/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblWorkflowRuleEngineDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblWorkflowRuleEngine;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public class TblWorkFlowRuleEngineImpl extends AbcAbstractClass<TblWorkflowRuleEngine> implements TblWorkflowRuleEngineDao {

    @Override
    public void addTblWorkflowRuleEngine(TblWorkflowRuleEngine workflowRuleEngine) {

        super.addEntity(workflowRuleEngine);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteTblWorkflowRuleEngine(TblWorkflowRuleEngine workflowRuleEngine) {

        super.deleteEntity(workflowRuleEngine);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateTblWorkflowRuleEngine(TblWorkflowRuleEngine workflowRuleEngine) {

        super.updateEntity(workflowRuleEngine);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TblWorkflowRuleEngine> getAllTblWorkflowRuleEngine() {
        return super.getAllEntity();
    }

    @Override
    public List<TblWorkflowRuleEngine> findTblWorkflowRuleEngine(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblWorkflowRuleEngineCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblWorkflowRuleEngine> findByCountTblWorkflowRuleEngine(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
