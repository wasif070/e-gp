package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvLumpsumDtlDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvLumpsumDtl;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsSrvLumpsumDtlImpl extends AbcAbstractClass<TblCmsSrvLumpsumDtl> implements TblCmsSrvLumpsumDtlDao {

    @Override
    public void addTblCmsSrvLumpsumDtl(TblCmsSrvLumpsumDtl srvLumpsum){
        super.addEntity(srvLumpsum);
    }

    @Override
    public void deleteTblCmsSrvLumpsumDtl(TblCmsSrvLumpsumDtl srvLumpsum) {
        super.deleteEntity(srvLumpsum);
    }

    @Override
    public void updateTblCmsSrvLumpsumDtl(TblCmsSrvLumpsumDtl srvLumpsum) {
        super.updateEntity(srvLumpsum);
    }

    @Override
    public List<TblCmsSrvLumpsumDtl> getAllTblCmsSrvLumpsumDtl() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvLumpsumDtl> findTblCmsSrvLumpsumDtl(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvLumpsumDtlCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvLumpsumDtl> findByCountTblCmsSrvLumpsumDtl(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsSrvLumpsumDtl> estCost) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
