/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;
import com.cptu.egp.eps.dao.generic.GenericDao;
import java.util.List;
import com.cptu.egp.eps.model.table.TblTempBiddingPermission;
/**
 *
 * @author HP
 */
public interface TblTempBiddingPermissionDao extends GenericDao<TblTempBiddingPermission> {
    public void addTblTempBiddingPermission(TblTempBiddingPermission tblObj);

    public void deleteTblTempBiddingPermission(TblTempBiddingPermission tblObj);

    public void updateTblTempBiddingPermission(TblTempBiddingPermission tblObj);

    public List<TblTempBiddingPermission> getAllTblTempBiddingPermission();

    public List<TblTempBiddingPermission> findTblTempBiddingPermission(Object... values) throws Exception;

    public List<TblTempBiddingPermission> findByCountTblTempBiddingPermission(int firstResult, int maxResult, Object... values) throws Exception;

    public long getTblTempBiddingPermissionCount();

    public void updateOrSaveTblTempBiddingPermission(List<TblTempBiddingPermission> tblObj);
}
