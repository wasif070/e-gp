/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
/**
 *
 * @author Shreyansh Shah
 * This class used for setting Input / Output parameters for tender Search
 */
public class SPPromiseReport extends StoredProcedure {
    private static final Logger LOGGER = Logger.getLogger(SPPromiseReport.class);
    public SPPromiseReport(BasicDataSource dataSource, String procName )
    {
        this.setDataSource(dataSource);
        this.setSql(procName);
        this.declareParameter(new SqlParameter("@yearname", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@ptype", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@deptid", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@officeid", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@PEId", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@noofpage", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@records", Types.VARCHAR));
        this.declareParameter(new SqlParameter("@districtId",Types.VARCHAR));
        this.declareParameter(new SqlParameter("@quater",Types.VARCHAR));
        this.declareParameter(new SqlParameter("@projectId",Types.VARCHAR));
//        this.declareParameter(new SqlParameter("@v_fieldName8Vc", Types.VARCHAR));
//        this.declareParameter(new SqlParameter("@v_fieldName9Vc", Types.VARCHAR));
//        this.declareParameter(new SqlParameter("@v_fieldName10Vc", Types.VARCHAR));
    }

     /* Generate and create list of Outpara's value */
//    public List <SPPromiseReportData> executeProcedure (String fieldName1,String fieldName2, String fieldName3, String fieldName4, String fieldName5 ,String fieldName6, String fieldName7,String fieldName8, String fieldName9,String fieldName10)
    /**
     * Execute stored procedure : p_get_promisereport
     * Use for Promise Report
     * @param yearname
     * @param ptype
     * @param deptid
     * @param officeid
     * @param peid
     * @param noofpage
     * @param records
     * @return Promise Report data as List of SPPromiseReportData object
     */
    public List <SPPromiseReportData> executeProcedure (String yearname,String ptype, String deptid, String officeid,String peid, String noofpage ,String records,String districtId,String quater,String projectId)
    {
    Map inParams = new HashMap();
    inParams.put("@yearname", yearname);
    inParams.put("@ptype", ptype);
    inParams.put("@deptid", deptid);
    inParams.put("@officeid", officeid);
    inParams.put("@PEId", peid);
    inParams.put("@noofpage", noofpage);
    inParams.put("@records", records);
    inParams.put("@districtId",districtId);
    inParams.put("@quater",quater);
    inParams.put("@projectId",projectId);
//    inParams.put("@v_fieldName7Vc", fieldName7);
//    inParams.put("@v_fieldName8Vc", fieldName8);
//    inParams.put("@v_fieldName9Vc", fieldName9);
//    inParams.put("@v_fieldName10Vc", fieldName10);

    this.compile();

    List<SPPromiseReportData> searchData = new ArrayList<SPPromiseReportData>();
    try
    {
         ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
         for (LinkedHashMap<String, Object> linkedHashMap: list )
         {
            SPPromiseReportData SPPromiseReportData = new SPPromiseReportData();
            SPPromiseReportData.setFieldName1((String)linkedHashMap.get("FieldValue1"));
            SPPromiseReportData.setFieldName2((String)linkedHashMap.get("FieldValue2"));
            SPPromiseReportData.setFieldName3((String)linkedHashMap.get("FieldValue3"));
            SPPromiseReportData.setFieldName4((String)linkedHashMap.get("FieldValue4"));
            SPPromiseReportData.setFieldName5((String)linkedHashMap.get("FieldValue5"));
            SPPromiseReportData.setFieldName6((String)linkedHashMap.get("FieldValue6"));
            SPPromiseReportData.setFieldName7((String)linkedHashMap.get("FieldValue7"));
            SPPromiseReportData.setFieldName8((String)linkedHashMap.get("FieldValue8"));
            SPPromiseReportData.setFieldName9((String)linkedHashMap.get("FieldValue9"));
            SPPromiseReportData.setFieldName10((String)linkedHashMap.get("FieldValue10"));
            SPPromiseReportData.setFieldName11((String)linkedHashMap.get("FieldValue11"));
            SPPromiseReportData.setFieldName12((String)linkedHashMap.get("FieldValue12"));

            searchData.add(SPPromiseReportData);
         }

    }
    catch(Exception e)
    {
        System.out.println("PromiseReport exception :"+e);
     LOGGER.error("SPCommonSearch : "+ e);
    }
    return searchData;
    }

}
