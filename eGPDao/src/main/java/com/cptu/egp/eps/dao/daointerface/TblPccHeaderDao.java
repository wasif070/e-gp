package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblPccHeader;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblPccHeaderDao extends GenericDao<TblPccHeader> {

    public void addTblPccHeader(TblPccHeader tblObj);

    public void deleteTblPccHeader(TblPccHeader tblObj);

    public void updateTblPccHeader(TblPccHeader tblObj);

    public List<TblPccHeader> getAllTblPccHeader();

    public List<TblPccHeader> findTblPccHeader(Object... values) throws Exception;

    public List<TblPccHeader> findByCountTblPccHeader(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblPccHeaderCount();
}
