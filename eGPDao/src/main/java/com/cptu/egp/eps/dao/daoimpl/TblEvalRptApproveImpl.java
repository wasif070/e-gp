package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblEvalRptApproveDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalRptApprove;
import java.util.List;

/**
 *
 * @author rishita
 */
public class TblEvalRptApproveImpl extends AbcAbstractClass<TblEvalRptApprove> implements TblEvalRptApproveDao {

    @Override
    public void addTblEvalRptApprove(TblEvalRptApprove tblEvalRptApprove){
        super.addEntity(tblEvalRptApprove);
    }

    @Override
    public void deleteTblEvalRptApprove(TblEvalRptApprove tblEvalRptApprove) {
        super.deleteEntity(tblEvalRptApprove);
    }

    @Override
    public void updateTblEvalRptApprove(TblEvalRptApprove tblEvalRptApprove) {
        super.updateEntity(tblEvalRptApprove);
    }

    @Override
    public List<TblEvalRptApprove> getAllTblEvalRptApprove() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEvalRptApprove> findTblEvalRptApprove(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEvalRptApproveCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalRptApprove> findByCountTblEvalRptApprove(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
