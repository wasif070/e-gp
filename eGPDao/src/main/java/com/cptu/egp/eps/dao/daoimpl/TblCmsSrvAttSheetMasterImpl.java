package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvAttSheetMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvAttSheetMaster;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsSrvAttSheetMasterImpl extends AbcAbstractClass<TblCmsSrvAttSheetMaster> implements TblCmsSrvAttSheetMasterDao {

    @Override
    public void addTblCmsSrvAttSheetMaster(TblCmsSrvAttSheetMaster cmsInvoiceDetails){
        super.addEntity(cmsInvoiceDetails);
    }

    @Override
    public void deleteTblCmsSrvAttSheetMaster(TblCmsSrvAttSheetMaster cmsInvoiceDetails) {
        super.deleteEntity(cmsInvoiceDetails);
    }

    @Override
    public void updateTblCmsSrvAttSheetMaster(TblCmsSrvAttSheetMaster cmsInvoiceDetails) {
        super.updateEntity(cmsInvoiceDetails);
    }

    @Override
    public List<TblCmsSrvAttSheetMaster> getAllTblCmsSrvAttSheetMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvAttSheetMaster> findTblCmsSrvAttSheetMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvAttSheetMasterCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvAttSheetMaster> findByCountTblCmsSrvAttSheetMaster(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }


    @Override
    public void updateInsertAllTblCmsSrvAttSheetMaster(List<TblCmsSrvAttSheetMaster> list) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
