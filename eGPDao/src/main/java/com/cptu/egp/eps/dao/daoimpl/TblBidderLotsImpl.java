package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblBidderLotsDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblBidderLots;
import java.util.List;

/**
 *
 * @author Nishith
 */
public class TblBidderLotsImpl extends AbcAbstractClass<TblBidderLots> implements TblBidderLotsDao {

    @Override
    public void addTblBidderLots(TblBidderLots master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblBidderLots(TblBidderLots master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblBidderLots(TblBidderLots master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblBidderLots> getAllTblBidderLots() {
        return super.getAllEntity();
    }

    @Override
    public List<TblBidderLots> findTblBidderLots(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblBidderLotsCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblBidderLots> findByCountTblBidderLots(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateInsertAllTblBidderLots(List<TblBidderLots> list) {
        super.updateAll(list);
    }
}
