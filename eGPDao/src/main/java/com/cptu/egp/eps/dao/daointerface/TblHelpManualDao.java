package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblHelpManual;
import java.util.List;

/**
 *
 * @author taher
 */
public interface TblHelpManualDao extends GenericDao<TblHelpManual> {

    public void addTblHelpManual(TblHelpManual helpManual);

    public void deleteTblHelpManual(TblHelpManual helpManual);

    public void updateTblHelpManual(TblHelpManual helpManual);

    public List<TblHelpManual> getAllTblHelpManual();

    public List<TblHelpManual> findTblHelpManual(Object... values) throws Exception;

    public List<TblHelpManual> findByCountTblHelpManual(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblHelpManualCount();
}
