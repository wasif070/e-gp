/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblEmployeeFinancialPower;
import java.util.List;

/**
 * <b>Interface Description goes here</b>
 * $Revision: 1.1.1.1 $
 * @version <version-no.> <Date>
 * @author Administrator
 */
public interface TblEmployeeFinancialPowerDao extends GenericDao<TblEmployeeFinancialPower>{

    public void addEmployeeFinancialPower(TblEmployeeFinancialPower tblEmployeeFinancialPower);

    public void deleteEmployeeFinancialPower(TblEmployeeFinancialPower tblEmployeeFinancialPower);

    public void updateEmployeeFinancialPower(TblEmployeeFinancialPower tblEmployeeFinancialPower);

    public List<TblEmployeeFinancialPower> findEmployeeFinancialPower(Object... values) throws Exception;

}
