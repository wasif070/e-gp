/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.model.table.TblTenderListDetail;
import java.util.List;

/**
 *
 * @author parag
 */
public interface TblTenderListDetailDao {

    public void addTblTenderListDetail(TblTenderListDetail tblObj);

    public void deleteTblTenderListDetail(TblTenderListDetail tblObj);

    public void updateTblTenderListDetail(TblTenderListDetail tblObj);

    public List<TblTenderListDetail> getAllTblTenderListDetail();

    public List<TblTenderListDetail> findTblTenderListDetail(Object... values) throws Exception;

    public List<TblTenderListDetail> findByCountTblTenderListDetail(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderListDetailCount();

    public void updateOrSaveAllTblTenderListDetail(List<TblTenderListDetail> list);
}
