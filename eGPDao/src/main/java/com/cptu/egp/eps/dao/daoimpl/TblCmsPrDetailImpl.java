package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsPrDetailDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsPrDetail;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblCmsPrDetailImpl extends AbcAbstractClass<TblCmsPrDetail> implements TblCmsPrDetailDao {

    @Override
    public void addTblCmsPrDetail(TblCmsPrDetail cmsPrDetail){
        super.addEntity(cmsPrDetail);
    }

    @Override
    public void deleteTblCmsPrDetail(TblCmsPrDetail cmsPrDetail) {
        super.deleteEntity(cmsPrDetail);
    }

    @Override
    public void updateTblCmsPrDetail(TblCmsPrDetail cmsPrDetail) {
        super.updateEntity(cmsPrDetail);
    }

    @Override
    public List<TblCmsPrDetail> getAllTblCmsPrDetail() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsPrDetail> findTblCmsPrDetail(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsPrDetailCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsPrDetail> findByCountTblCmsPrDetail(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsPrDetail> estCost) {
         super.updateAll(estCost);
    }
}
