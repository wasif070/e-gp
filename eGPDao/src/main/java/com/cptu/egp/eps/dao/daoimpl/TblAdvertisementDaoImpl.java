/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;
import com.cptu.egp.eps.model.table.TblAdvertisement;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.dao.daointerface.TblAdvertisementDao;
import java.util.List;

/**
 *
 * @author sambasiva.sugguna
 */

public class TblAdvertisementDaoImpl extends AbcAbstractClass<TblAdvertisement> implements TblAdvertisementDao{

    @Override
    public Integer addTblAdvertisement(TblAdvertisement tblAdvertisement) {
          return ((Integer) getHibernateTemplate().save(tblAdvertisement)).intValue();
         //super.addEntity(tblAdvertisement);
    }

    @Override
    public void deleteTblAdvertisement(TblAdvertisement tblAdvertisement) {
        super.deleteEntity(tblAdvertisement);
    }

    @Override
    public void updateTblAdvertisement(TblAdvertisement tblAdvertisement) {
         super.updateEntity(tblAdvertisement);
    }

    @Override
    public List<TblAdvertisement> getAllTblAdvertisement() {
        return super.getAllEntity();
    }

    @Override
    public List<TblAdvertisement> findTblAdvertisement(Object... values) throws Exception {
         return super.findEntity(values);
    }

    @Override
    public List<TblAdvertisement> findByCountTblAdvertisement(int firstResult, int maxResult, Object... values) throws Exception {
         return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblAdvertisement(TblAdvertisement tblAdvertisement) {
               return super.getEntityCount();

    }

}
