package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblCmsSrvReExHistoryDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblCmsSrvReExHistory;
import java.util.List;

/**
 *
 * @author shreyansh
 */
public class TblCmsSrvReExHistoryImpl extends AbcAbstractClass<TblCmsSrvReExHistory> implements TblCmsSrvReExHistoryDao {

    @Override
    public void addTblCmsSrvReExHistory(TblCmsSrvReExHistory cmsWpDetail){
        super.addEntity(cmsWpDetail);
    }

    @Override
    public void deleteTblCmsSrvReExHistory(TblCmsSrvReExHistory cmsWpDetail) {
        super.deleteEntity(cmsWpDetail);
    }

    @Override
    public void updateTblCmsSrvReExHistory(TblCmsSrvReExHistory cmsWpDetail) {
        super.updateEntity(cmsWpDetail);
    }

    @Override
    public List<TblCmsSrvReExHistory> getAllTblCmsSrvReExHistory() {
        return super.getAllEntity();
    }

    @Override
    public List<TblCmsSrvReExHistory> findTblCmsSrvReExHistory(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblCmsSrvReExHistoryCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblCmsSrvReExHistory> findByCountTblCmsSrvReExHistory(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateOrSaveEstCost(List<TblCmsSrvReExHistory> estCost) {
       super.updateAll(estCost);
    }
}
