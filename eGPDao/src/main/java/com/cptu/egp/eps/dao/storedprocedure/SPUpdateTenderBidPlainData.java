/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.storedprocedure;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.math.BigDecimal;
import java.util.List;
import java.util.Date;
import java.util.Map;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
/**
 *
 * @author Ahsan
 */
public class SPUpdateTenderBidPlainData extends StoredProcedure{

    private static final Logger LOGGER = Logger.getLogger(SPUpdateTenderBidPlainData.class);

    /**
     *
     * @param dataSource
     * @param procName
     */
    public SPUpdateTenderBidPlainData(BasicDataSource dataSource, String procName) {

       this.setDataSource(dataSource);
       this.setSql(procName);//Stored Procedure name has to be set from DaoContext.
       LOGGER.debug("Proc Name: "+ procName);

        this.declareParameter(new SqlParameter("CellValueList", Types.VARCHAR));
        this.declareParameter(new SqlParameter("ColumnIdList", Types.VARCHAR));
        this.declareParameter(new SqlParameter("BidTableIdList", Types.VARCHAR));
        this.declareParameter(new SqlParameter("RowIdList", Types.VARCHAR));
        this.declareParameter(new SqlParameter("TenderFormId", Types.INTEGER));
    }

    /**
     *
     * @param cellValueList
     * @param columnIdList
     * @param bidTableIdList
     * @param rowIdList
     * @param tenderFormId
     * @return
     */

    public String executeProcedure(String cellValueList, String columnIdList, String bidTableIdList, String rowIdList, int tenderFormId) {
        Map inParams = new HashMap();
        inParams.put("CellValueList", cellValueList);
        inParams.put("ColumnIdList", columnIdList);
        inParams.put("BidTableIdList", bidTableIdList);
        inParams.put("RowIdList", rowIdList);
        inParams.put("TenderFormId", tenderFormId);


        this.compile();
        String updateStatus = "";
        try {
            ArrayList<LinkedHashMap<String, Object>> list = (ArrayList<LinkedHashMap<String, Object>>) execute(inParams).get("#result-set-1");
            if (list != null && !list.isEmpty()) {
                for (LinkedHashMap<String, Object> linkedHashMap : list) {
                    System.out.println(list);
                    updateStatus = (String) linkedHashMap.get("UpdateStatus");
                    System.out.println(updateStatus);
                }
            }
            LOGGER.debug("SPUpdateTenderBidPlainData : No data found ");
        } catch (Exception e) {
            LOGGER.error("SPUpdateTenderBidPlainData : " + e);
            System.out.println("SPUpdateTenderBidPlainData" + e);
            e.printStackTrace();
        }
        return updateStatus;
    }
}
