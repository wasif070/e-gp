/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderFormula;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderFormulaDao extends GenericDao<TblTenderFormula>{

    public void addTblTenderFormula(TblTenderFormula tblObj);

    public void deleteTblTenderFormula(TblTenderFormula tblObj);

    public void updateTblTenderFormula(TblTenderFormula tblObj);

    public List<TblTenderFormula> getAllTblTenderFormula();

    public List<TblTenderFormula> findTblTenderFormula(Object... values) throws Exception;

    public List<TblTenderFormula> findByCountTblTenderFormula(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderFormulaCount();
}
