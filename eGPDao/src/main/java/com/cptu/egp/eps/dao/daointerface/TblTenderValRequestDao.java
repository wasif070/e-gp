package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderValRequest;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderValRequestDao extends GenericDao<TblTenderValRequest> {

    public void addTblTenderValRequest(TblTenderValRequest tblTenderValRequest);

    public void deleteTblTenderValRequest(TblTenderValRequest tblTenderValRequest);

    public void deleteAllTblTenderValRequest(Collection tblTenderValRequest);

    public void updateTblTenderValRequest(TblTenderValRequest tblTenderValRequest);

    public List<TblTenderValRequest> getAllTblTenderValRequest();

    public List<TblTenderValRequest> findTblTenderValRequest(Object... values) throws Exception;

    public List<TblTenderValRequest> findByCountTblTenderValRequest(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderValRequestCount();
}
