/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTemplateMasterDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTemplateMaster;
import java.util.List;

/**
 *
 * @author yanki
 */
public class TblTemplateMasterImpl extends AbcAbstractClass<TblTemplateMaster> implements TblTemplateMasterDao {
    @Override
    public void addTblTemplateMaster(TblTemplateMaster templateMaster) {
        super.addEntity(templateMaster);
    }

    @Override
    public void deleteTblTemplateMaster(TblTemplateMaster templateMaster) {
        super.deleteEntity(templateMaster);
    }

    @Override
    public void updateTblTemplateMaster(TblTemplateMaster templateMaster) {
        super.updateEntity(templateMaster);
    }

    @Override
    public List<TblTemplateMaster> getAllTblTemplateMaster() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTemplateMaster> findTblTemplateMaster(Object... values) throws Exception {
        return super.findEntity(values);
    }

}
