/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblAppreviseConfigurationDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblAppreviseConfiguration;
import java.util.List;

public class TblAppreviseConfigurationImpl extends AbcAbstractClass<TblAppreviseConfiguration> implements TblAppreviseConfigurationDao{
    @Override
    public void addTblAppreviseConfiguration(TblAppreviseConfiguration master){
            super.addEntity(master);
    }
    @Override
    public void deleteTblAppreviseConfiguration(TblAppreviseConfiguration master) {
            super.deleteEntity(master);
    }
    @Override
    public void updateTblAppreviseConfiguration(TblAppreviseConfiguration master) {
            super.updateEntity(master);
    }
    @Override
    public List<TblAppreviseConfiguration> getAllTblAppreviseConfiguration() {
            return super.getAllEntity();
    }
    @Override
    public List<TblAppreviseConfiguration> findTblAppreviseConfiguration(Object... values) throws Exception {
            return super.findEntity(values);
    }
    @Override
    public long getTblAppreviseConfigurationCount() {
            return super.getEntityCount();
    }
    @Override
    public List<TblAppreviseConfiguration> findByCountTblAppreviseConfiguration(int firstResult, int maxResult, Object... values) throws Exception {
            return super.findByCountEntity(firstResult, maxResult, values);
    }
    @Override
    public void updateOrSaveTblAppreviseConfiguration(List<TblAppreviseConfiguration> tblObj) {
            super.updateAll(tblObj);
    }
}
