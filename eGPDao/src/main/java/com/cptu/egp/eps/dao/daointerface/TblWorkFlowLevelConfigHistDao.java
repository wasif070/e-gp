/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblWorkFlowLevelConfigHist;
import java.util.List;

/**
 *
 * @author Sanjay
 */
public interface TblWorkFlowLevelConfigHistDao extends GenericDao<TblWorkFlowLevelConfigHist> {

    public void addTblWorkFlowLevelConfigHist(TblWorkFlowLevelConfigHist admin);

    public void deleteTblWorkFlowLevelConfigHist(TblWorkFlowLevelConfigHist admin);

    public void updateTblWorkFlowLevelConfigHist(TblWorkFlowLevelConfigHist admin);

    public List<TblWorkFlowLevelConfigHist> getAllTblWorkFlowLevelConfigHist();

    public List<TblWorkFlowLevelConfigHist> findTblWorkFlowLevelConfigHist(Object... values) throws Exception;

    public List<TblWorkFlowLevelConfigHist> findByCountTblWorkFlowLevelConfigHist(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblWorkFlowLevelConfigHistCount();
}
