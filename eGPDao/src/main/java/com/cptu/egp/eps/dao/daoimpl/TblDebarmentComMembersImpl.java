package com.cptu.egp.eps.dao.daoimpl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.cptu.egp.eps.dao.daointerface.TblDebarmentComMembersDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblDebarmentComMembers;
import java.util.List;

/**
 *
 * @author taher
 */
public class TblDebarmentComMembersImpl extends AbcAbstractClass<TblDebarmentComMembers> implements TblDebarmentComMembersDao {

    @Override
    public void addTblDebarmentComMembers(TblDebarmentComMembers master){
        super.addEntity(master);
    }

    @Override
    public void deleteTblDebarmentComMembers(TblDebarmentComMembers master) {
        super.deleteEntity(master);
    }

    @Override
    public void updateTblDebarmentComMembers(TblDebarmentComMembers master) {
        super.updateEntity(master);
    }

    @Override
    public List<TblDebarmentComMembers> getAllTblDebarmentComMembers() {
        return super.getAllEntity();
    }

    @Override
    public List<TblDebarmentComMembers> findTblDebarmentComMembers(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblDebarmentComMembersCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblDebarmentComMembers> findByCountTblDebarmentComMembers(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public void updateSaveAllDebarComMembers(List<TblDebarmentComMembers> comMembers) {
        super.updateAll(comMembers);
    }
}
