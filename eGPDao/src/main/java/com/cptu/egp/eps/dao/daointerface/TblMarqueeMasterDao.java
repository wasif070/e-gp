package com.cptu.egp.eps.dao.daointerface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblMarqueeMaster;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblMarqueeMasterDao extends GenericDao<TblMarqueeMaster> {

    public void addTblMarqueeMaster(TblMarqueeMaster tblObj);

    public void deleteTblMarqueeMaster(TblMarqueeMaster tblObj);

    public void updateTblMarqueeMaster(TblMarqueeMaster tblObj);

    public List<TblMarqueeMaster> getAllTblMarqueeMaster();

    public List<TblMarqueeMaster> findTblMarqueeMaster(Object... values) throws Exception;

    public List<TblMarqueeMaster> findByCountTblMarqueeMaster(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblMarqueeMasterCount();
}
