/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblEvalRptForwardToAaDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblEvalRptForwardToAa;
import java.util.List;
/**
 *
 * @author darshan
 */
public class TblEvalRptForwardToAaImpl extends AbcAbstractClass<TblEvalRptForwardToAa> implements TblEvalRptForwardToAaDao {

    @Override
    public void addTblEvalRptForwardToAa(TblEvalRptForwardToAa tblEvalRptForwardToAa){
        super.addEntity(tblEvalRptForwardToAa);
    }

    @Override
    public void deleteTblEvalRptForwardToAa(TblEvalRptForwardToAa tblEvalRptForwardToAa) {
        super.deleteEntity(tblEvalRptForwardToAa);
    }

    @Override
    public void updateTblEvalRptForwardToAa(TblEvalRptForwardToAa tblEvalRptForwardToAa) {
        super.updateEntity(tblEvalRptForwardToAa);
    }

    @Override
    public List<TblEvalRptForwardToAa> getAllTblEvalRptForwardToAa() {
        return super.getAllEntity();
    }

    @Override
    public List<TblEvalRptForwardToAa> findTblEvalRptForwardToAa(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public long getTblEvalRptForwardToAaCount() {
        return super.getEntityCount();
    }

    @Override
    public List<TblEvalRptForwardToAa> findByCountTblEvalRptForwardToAa(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }
}
