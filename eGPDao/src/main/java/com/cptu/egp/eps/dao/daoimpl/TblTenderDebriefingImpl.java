/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daoimpl;

import com.cptu.egp.eps.dao.daointerface.TblTenderDebriefingDao;
import com.cptu.egp.eps.dao.generic.AbcAbstractClass;
import com.cptu.egp.eps.model.table.TblTenderDebriefing;
import java.util.List;

/**
 *
 * @author dixit
 */
public class TblTenderDebriefingImpl extends AbcAbstractClass<TblTenderDebriefing> implements TblTenderDebriefingDao {

    @Override
    public void addTblTenderDebriefing(TblTenderDebriefing tblTenderDebriefing) {
        super.addEntity(tblTenderDebriefing);
    }

    @Override
    public void deleteTblTenderDebriefing(TblTenderDebriefing tblTenderDebriefing) {
        super.deleteEntity(tblTenderDebriefing);
    }

    @Override
    public void updateTblTenderDebriefing(TblTenderDebriefing tblTenderDebriefing) {
        super.updateEntity(tblTenderDebriefing);
    }

    @Override
    public List<TblTenderDebriefing> getAllTblTenderDebriefing() {
        return super.getAllEntity();
    }

    @Override
    public List<TblTenderDebriefing> findTblTenderDebriefing(Object... values) throws Exception {
        return super.findEntity(values);
    }

    @Override
    public List<TblTenderDebriefing> findByCountTblTenderDebriefing(int firstResult, int maxResult, Object... values) throws Exception {
        return super.findByCountEntity(firstResult, maxResult, values);
    }

    @Override
    public long getTblTenderDetailsCount() {
        return super.getEntityCount();
    }

}
