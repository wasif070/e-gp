/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.dao.daointerface;

import com.cptu.egp.eps.dao.generic.GenericDao;
import com.cptu.egp.eps.model.table.TblTenderGrandSumDetail;
import java.util.List;

/**
 *
 * @author Ramesh
 */
public interface TblTenderGrandSumDetailDao extends GenericDao<TblTenderGrandSumDetail>{

    public void addTblTenderGrandSumDetail(TblTenderGrandSumDetail tblObj);

    public void deleteTblTenderGrandSumDetail(TblTenderGrandSumDetail tblObj);

    public void updateTblTenderGrandSumDetail(TblTenderGrandSumDetail tblObj);

    public List<TblTenderGrandSumDetail> getAllTblTenderGrandSumDetail();

    public List<TblTenderGrandSumDetail> findTblTenderGrandSumDetail(Object... values) throws Exception;

    public List<TblTenderGrandSumDetail> findByCountTblTenderGrandSumDetail(int firstResult,int maxResult,Object... values) throws Exception;

    public long getTblTenderGrandSumDetailCount();
    
    public void updateSaveAllTenderGrandSumDetail(List<TblTenderGrandSumDetail> lstTenderGrandSumDetail);
    
}
