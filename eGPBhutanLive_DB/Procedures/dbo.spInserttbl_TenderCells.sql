SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spInserttbl_TenderCells]
	@tenderTableId int,
	@rowId int,
	@cellDatatype Varchar(100),
	@cellvalue varchar(max),
	@columnId smallint,
	@templateTableId int,
	@templateColumnId smallint,
	@cellId int
As
Begin

	Insert Into tbl_TenderCells	(tenderTableId, rowId, cellDatatype, cellvalue, 
		columnId, templateTableId, templateColumnId, cellId)
	Values(@tenderTableId, @rowId, @cellDatatype, @cellvalue, 
		@columnId, @templateTableId, @templateColumnId, @cellId)

End


GO
