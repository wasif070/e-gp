SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kinjal Shah
-- alter date: 11-12-2010
-- Description:	To get TODO list
-- =============================================
-- exec [p_search_todolist] '24/11/2010', null, null

-- SP Name	:   p_search_todolist
-- Module	:	Message Box
-- Funftion	:	N.A.

CREATE PROCEDURE [dbo].[p_search_todolist]
	-- Add the parameters for the stored procedure here
	 @v_datefrom_inVc Varchar(20)=NULL,
	 @v_dateto_inVc varchar(20) =NULL,	 
	 @v_status_inVc Varchar(150)=NULL,
	 @v_Page_inN Int =1,
	 @v_RecordPerPage_inN INT =10
	 
AS
BEGIN
	SET NOCOUNT ON;   
	/**** Para Declaration ***/
	
	DECLARE @v_Reccountf Int, @v_TotalPagef Int, @v_finalstatus_Vc Varchar(100)=null
	
	if @v_status_inVc is not null	
		select @v_finalstatus_Vc = case when @v_status_inVc = 'pending' then 'No' else 'yes' end
	else
		set @v_finalstatus_Vc = null
	
	print @v_finalstatus_Vc
	SELECT @v_Reccountf = Count(Rownumber) From 
	(
		SELECT Rownumber From 
		(
			SELECT ROW_NUMBER() OVER (order by taskid desc) As Rownumber,taskid  
			FROM [tbl_ToDoList] 
			WHERE  				
				case 
					when @v_finalstatus_Vc is not null then
					case when isComplete = @v_finalstatus_Vc  then 1 else 0 end
					else 1
				end = 1					
				AND 
				case 
					when @v_datefrom_inVc is not null and @v_dateto_inVc is not null then 
						case when ((CONVERT(VARCHAR(10),[startdate],103) >= CONVERT(VARCHAR(10),@v_datefrom_inVc,103)
						AND CONVERT(VARCHAR(10),[Enddate],103) <= CONVERT(VARCHAR(10),@v_dateto_inVc,103)))
						then 1 else 0 end 
					when @v_datefrom_inVc is not null and @v_dateto_inVc is null then
						case when CONVERT(VARCHAR(10),[startdate],103) >= CONVERT(VARCHAR(10),@v_datefrom_inVc,103)					
						then 1 else 0 end
					when @v_datefrom_inVc is null and @v_dateto_inVc is not null then
						case when CONVERT(VARCHAR(10),[Enddate],103) <= CONVERT(VARCHAR(10),@v_dateto_inVc,103)					
						then 1 else 0 end  
					else 1
					end = 1 
				 
		) AS DATA
	) AS TTT
	
	SET @v_TotalPagef =CEILING(@v_Reccountf/+cast(@v_RecordPerPage_inN as varchar(10)))
	
	SELECT Rownumber,taskid,[taskBrief],[taskDetails],[priority],[startDate],[endDate],@v_TotalPagef as TotalPages,@v_Reccountf as TotalRecords From 
	(
		SELECT ROW_NUMBER() OVER (order by taskid desc) As Rownumber,taskid,[taskBrief],[taskDetails],		
												[priority],[startDate],[endDate]
		FROM [tbl_ToDoList] 
		WHERE  
				case 
					when @v_finalstatus_Vc is not null then
					case when isComplete = @v_finalstatus_Vc  then 1 else 0 end
					else 1
				end = 1	
				 AND 
				case 
					when @v_datefrom_inVc is not null and @v_dateto_inVc is not null then 
						case when ((CONVERT(VARCHAR(10),[startdate],103) >= CONVERT(VARCHAR(10),@v_datefrom_inVc,103)
						AND CONVERT(VARCHAR(10),[Enddate],103) <= CONVERT(VARCHAR(10),@v_dateto_inVc,103)))
						then 1 else 0 end 
					when @v_datefrom_inVc is not null and @v_dateto_inVc is null then
						case when CONVERT(VARCHAR(10),[startdate],103) >= CONVERT(VARCHAR(10),@v_datefrom_inVc,103)					
						then 1 else 0 end
					when @v_datefrom_inVc is null and @v_dateto_inVc is not null then
						case when CONVERT(VARCHAR(10),[Enddate],103) <= CONVERT(VARCHAR(10),@v_dateto_inVc,103)					
						then 1 else 0 end  
					else 1
					end = 1 
	) AS DATA 
	where Rownumber between cast(((@v_Page_inN - 1) * @v_RecordPerPage_inN + 1) as varchar(10)) 
	AND cast((@v_Page_inN * @v_RecordPerPage_inN) as varchar(10)) Order By taskId desc
	
	
	
	
	
	
	/*********************
	
	DECLARE @v_TempQuery_Vc Varchar(max),@v_IntialQuery_Vc Varchar(1000), @v_ExecutingQuery_Vc as varchar(max), @v_finalstatus_Vc Varchar(100)
		
		/** This condition is used to make search with status parameter ***/
	IF @v_status_inVc IS NULL OR @v_status_inVc =''	
		BEGIN
			SET @v_TempQuery_Vc =  ''				
		END
	ELSE
		BEGIN
			select @v_finalstatus_Vc = case when @v_status_inVc = 'pending' then 'No' else 'yes' end
			 
			SET @v_TempQuery_Vc =  ' isComplete = ''' + @v_finalstatus_Vc +''''
		END


IF (@v_datefrom_inVc is NULL OR @v_datefrom_inVc='') and (@v_dateto_inVc is NULL OR @v_dateto_inVc='')
	begin
		--	print 'BOTH NULL'
			SET @v_TempQuery_Vc=@v_TempQuery_Vc
	end
	
	
ELSE IF ((@v_datefrom_inVc is NOT NULL OR @v_datefrom_inVc<>'') and (@v_dateto_inVc is not NULL OR @v_dateto_inVc <> ''))
	BEGIN
	--print 'BOTH NOT NULL'
		if (@v_TempQuery_Vc='')
			BEGIN
				SET @v_TempQuery_Vc=' ((CONVERT(VARCHAR(10),[startdate],103) >= '''+CONVERT(VARCHAR(10),@v_datefrom_inVc,103)+''' 
				AND CONVERT(VARCHAR(10),[enddate],103) <= '''+CONVERT(VARCHAR(10),@v_dateto_inVc,103)+''' ))'	
			END
		ELSE
			BEGIN
				SET @v_TempQuery_Vc=@v_TempQuery_Vc + ' AND ((CONVERT(VARCHAR(10),[startdate],103) >= '''+CONVERT(VARCHAR(10),@v_datefrom_inVc,103)+''' 
				AND CONVERT(VARCHAR(10),[Enddate],103) <= '''+CONVERT(VARCHAR(10),@v_dateto_inVc,103)+''' ))'
			END
	END
ELSE IF ((@v_datefrom_inVc is NOT NULL OR @v_datefrom_inVc<>'') and (@v_dateto_inVc is NULL OR @v_dateto_inVc= ''))
BEGIN
	
	--print 'Datefrom NOT NULL'
	if (@v_TempQuery_Vc='')
			BEGIN
				SET @v_TempQuery_Vc=' CONVERT(VARCHAR(10),[startdate],103) >= '''+CONVERT(VARCHAR(10),@v_datefrom_inVc,103)+''''
					
			END
		ELSE
			BEGIN
				SET @v_TempQuery_Vc=@v_TempQuery_Vc + ' AND CONVERT(VARCHAR(10),[startdate],103) >= '''+CONVERT(VARCHAR(10),@v_datefrom_inVc,103)+''''
			END
END


ELSE IF ((@v_datefrom_inVc is NULL OR @v_datefrom_inVc = '') and (@v_dateto_inVc is Not NULL OR @v_dateto_inVc <> ''))
BEGIN	
	-- print 'DATE TO NOT NULL'
	if (@v_TempQuery_Vc='')
			BEGIN
				SET @v_TempQuery_Vc=' CONVERT(VARCHAR(10),[enddate],103) <= '''+CONVERT(VARCHAR(10),@v_dateto_inVc,103)+''' '	
			END
		ELSE
			BEGIN
				SET @v_TempQuery_Vc=@v_TempQuery_Vc + ' AND CONVERT(VARCHAR(10),[Enddate],103) <= '''+CONVERT(VARCHAR(10),@v_dateto_inVc,103)+''''
			END		
END
		
	IF @v_TempQuery_Vc=''
		BEGIN
			SET @v_IntialQuery_Vc =' FROM [tbl_ToDoList]'		
		END
	ELSE
		BEGIN
			SET @v_IntialQuery_Vc= ' FROM [tbl_ToDoList] WHERE '
		END
	
   
    set @v_ExecutingQuery_Vc='DECLARE @v_Reccountf Int, @v_TotalPagef Int
	SELECT @v_Reccountf = Count(Rownumber) From (
	SELECT Rownumber From (SELECT ROW_NUMBER() OVER (order by taskid desc) As Rownumber,taskid '+@v_IntialQuery_Vc+''+@v_TempQuery_Vc+') AS DATA) AS TTT
	
	SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_RecordPerPage_inN as varchar(10))+')
	SELECT Rownumber,taskid,[taskBrief],[taskDetails],[priority],[startDate],[endDate],@v_TotalPagef as TotalPages,@v_Reccountf as TotalRecords From (SELECT ROW_NUMBER() OVER (order by taskid desc) As Rownumber,taskid,[taskBrief],[taskDetails],[priority],[startDate],[endDate]
	'+@v_IntialQuery_Vc+''+@v_TempQuery_Vc+') AS DATA where Rownumber between '+cast(((@v_Page_inN - 1) * @v_RecordPerPage_inN + 1) as varchar(10))+' 
	AND '+cast((@v_Page_inN * @v_RecordPerPage_inN) as varchar(10))+'' + ' Order By taskId desc'
	
	print @v_ExecutingQuery_Vc
	Exec(@v_ExecutingQuery_Vc)
	
 *****************/
END


GO
