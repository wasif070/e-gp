SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetLowestTendererForICT]
@tenderId int
--@pkgLotId int

as

Declare @amount MONEY
Declare @userId int 
Declare @Count int
Declare @maxRowId int
Declare @sum MONEY
Declare @bidTableId int 
Declare @tenderFormId int 
Declare @laborCostValue varchar(100)
--Declare @amount money
Declare @flag bit
Declare @min money
Declare @calc money
Declare @local money
Declare @inter money
Set @min = 0

--Set @amount = 8056578.610

--Cursor Start

DECLARE CursorDomPref CURSOR FOR  

select distinct tbl_BidderRank.userId,tbl_BidderRank.amount from tbl_BidderRank inner join tbl_TenderDomesticPref 
on tbl_BidderRank.userId = tbl_TenderDomesticPref.userId
where tbl_BidderRank.tenderId = @tenderId 

OPEN CursorDomPref   
FETCH NEXT FROM CursorDomPref INTO @userId, @amount    

WHILE @@FETCH_STATUS = 0   
BEGIN   
	if(@min = 0)
		Set @min = @amount
	if(@min>@amount)
		Set @min = @amount
--select @tenderFormId =  tbl_tenderBidForm.tenderFormId from tbl_tenderBidForm inner join tbl_tenderForms 
--on tbl_tenderBidForm.tenderFormId = tbl_tenderForms.tenderFormId
--where tbl_tenderBidForm.userId = @userId and tbl_tenderBidForm.tenderId = @tenderId and tbl_tenderForms.pkgLotId  = @pkgLotId and tbl_tenderForms.FormType = 'manufactured'

--if(@tenderFormId > 0)
--	BEGIN
--		select @bidTableId = bidTableId from tbl_tenderBidTable where 
--				bidId = (select bidId from tbl_tenderBidForm where tenderFormId = @tenderFormId and userId = @userId)

--		select @maxRowId = MAX(rowId) from tbl_TenderBidPlainData where bidTableId  = @bidTableId and tenderFormId = @tenderFormId
--		set @Count = 1
--		set @sum = 0
--			While @Count <  @maxRowId 
--				Begin
--					select @laborCostValue = cellValue from	tbl_TenderBidPlainData where bidTableId  = @bidTableId and tenderFormId = @tenderFormId and rowId = @Count and tenderColId = 12	
--					select @sum = @sum + Convert(MONEY,@laborCostValue)
--					Set @Count = @Count + 1
--				End --End While
--				PRINT @sum
--		Set @calc = (@sum * 100)/@amount
--		If (@calc > 30)
--			BEGIN
--				IF(@min >= @calc)
--					Set @min = @calc
				 
--				SET @flag = 1	
--			END
				
--		ELSE	
--			SET @flag = 0
--	END
--else
--	SET @flag = 0
	
--Select @flag as DomPref
	
	 
	FETCH NEXT FROM CursorDomPref INTO @userId, @amount
    END   

	CLOSE CursorDomPref   
	DEALLOCATE CursorDomPref	
	Set @local = @min
		
--	Update tbl_TenderBidPlainData SET cellValue = @sum where bidTableId = 2216 and tenderColId = @updateColumnId and rowId = @maxRowId
	
	
				 		
SET @inter = (select MIN(amount) from  tbl_BidderRank where  userId not in (select distinct tbl_BidderRank.userId from tbl_BidderRank inner join tbl_TenderDomesticPref 
on tbl_BidderRank.userId = tbl_TenderDomesticPref.userId
where tbl_BidderRank.tenderId = 947 ) and tenderId=947)

select @local as localTender,@inter as interTender


GO
