SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE   proc [dbo].[p_fullbackup]
as
set nocount on
DECLARE @DBName varchar(255)
DECLARE @DS VARCHAR(50)
DECLARE @Path VARCHAR(255)
DECLARE Full_Backup 
CURSOR FOR SELECT name from sys.databases WHERE name  IN ('egp_stg')AND state = 0 --Exclude offline databases, they won't backup if they offline anywayAND Source_database_id is null -- Removes snapshots from the databases returned, these can't be backed up eith
OPEN Full_Backup 
FETCH NEXT FROM Full_Backup
INTO @DBName-- Check @@FETCH_STATUS to see if there are any more rows to fetch.
WHILE @@FETCH_STATUS = 0
BEGIN--Set the filename values of the backup files
SET @DS = REPLACE(CONVERT(VARCHAR(10), GETDATE(), 111), '/', '') + '_'    + REPLACE(CONVERT(VARCHAR(8), GETDATE(), 108), ':', '')
SET @Path = 'D:\backup\\egp_stgbk\'SET @Path = @path + @DBNAME + '_' + @DS + '.Fbak'--Take the backup
BACKUP DATABASE @DBNAME TO DISK = @Path  WITH    FORMAT, INIT,  SKIP, NOREWIND,    NOUNLOAD, STATS = 10    
FETCH NEXT FROM Full_Backup    INTO @DBName
END
CLOSE Full_Backup
DEALLOCATE Full_Backup

---exec p_fullbackup


GO
