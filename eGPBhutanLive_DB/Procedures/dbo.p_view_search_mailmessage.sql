SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: View Message Box.
--
--
-- Author: Rajesh Singh
-- Date: 25-10-2010
--
-- Last Modified:Kinjal
-- Modified By:
-- Date 27-10-2010
-- Modification:

-- SP Name:  p_view_search_mailmessage
-- Module:   Message Box
-- Function: Store Procedure is use for Search Message list on the basis of given search criteria.
--------------------------------------------------------------------------------
-- Inbox:	Search Messages for Message Type is Inbox.
-- Sent:	Search Messages for Message Type is Sent.
-- Draft:	Search Messages for Message Type is Draft.
-- Trash:	Search Messages for Message Type is Trash.

-- subject:	Search Messages on the basis of given subject details.
-- message:	Search Messages on the basis of given message details.
--------------------------------------------------------------------------------

--[p_view_search_mailmessage] 'Inbox','1','0','Active','10','1','','no','subject','Hello','taher1@t.com','25/10/2010','27/10/2010','','subject','desc'

--[p_view_search_mailmessage]'Inbox',97,0,'Live',10,1,0,'no',' ',' ',' ','15/11/2010','15/11/2010',0,'creationdate','asc'


CREATE PROCEDURE [dbo].[p_view_search_mailmessage]
	-- Add the parameters for the stored procedure here
		@v_MsgBoxTypeVc varchar(20),
		@v_UserIdN int,
		@v_FolderIdN int,
                @v_msgStatusVc varchar(10),  --(archive,Live,trash)
		@v_PagePerRecordN int = 10,
		@v_PageN int = 1,
		@v_TotalPages int,
		@v_viewMsgVc varchar(10),
		@v_searchInVc varchar(10) = '',
		@v_keywordsVc varchar(20) = '',
		@v_emailIdVc varchar(100) = '',
		@v_dateFromVc varchar(20) = '',
		@v_dateToVc varchar(20) = '',
		@v_msgidN int = 0,
		@v_columnNameVc varchar(20),
		@v_orderTypeVc varchar(10),
		@v_searchOptVc varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @v_TempVariableVc VARCHAR(1000),
			@v_TeampQueryVc Varchar(max)
	DECLARE @v_Reccountf int
	DECLARE @v_TotalPagef int

	IF (@v_emailIdVc IS NULL OR @v_emailIdVc ='')
	BEGIN
		SET @v_TempVariableVc=''
	END
	ELSE
	BEGIN
		IF @v_MsgBoxTypeVc='Inbox'
		BEGIN
			IF @v_searchOptVc = 'eq'
			BEGIN
				set @v_TempVariableVc='mi.msgfromuserid in (select userid  from tbl_LoginMaster where emailId = '''+@v_emailIdVc+''')'
			END
			ELSE
			BEGIN
				SET @v_TempVariableVc='mi.msgfromuserid in (select userid  from tbl_LoginMaster where emailId LIKE ''%'+@v_emailIdVc+'%'')'
			END
		END
		ELSE
		BEGIN
			SET @v_TempVariableVc=' mi.msgtouserid in (select userid  from tbl_LoginMaster where emailId LIKE ''%'+@v_emailIdVc+'%'')  '
		END
	END

	IF @v_searchInVc='subject'
	BEGIN
		IF (@v_keywordsVc is null OR @v_keywordsVc='')
		BEGIN
				SET @v_TempVariableVc=@v_TempVariableVc
		END
		ELSE
		BEGIN
			IF (@v_TempVariableVc='')
			BEGIN
				SET @v_TempVariableVc=' m.[subject] LIKE ''%'+@v_keywordsVc+'%'''
			END
			ELSE
			BEGIN
				SET @v_TempVariableVc=@v_TempVariableVc + ' AND m.[subject] LIKE ''%'+@v_keywordsVc+'%'''
			END
		END
	END
	ELSE IF @v_searchInVc='message'
	BEGIN
		IF (@v_keywordsVc is null OR @v_keywordsVc='')
		BEGIN
			SET @v_TempVariableVc=@v_TempVariableVc
		END
		ELSE
		BEGIN
			IF (@v_TempVariableVc='')
			BEGIN
				SET @v_TempVariableVc=' m.[msgText] LIKE ''%'+@v_keywordsVc+'%'''
			END
			ELSE
			BEGIN
				SET @v_TempVariableVc=@v_TempVariableVc + ' AND m.[msgText] LIKE ''%'+@v_keywordsVc+'%'''
			END
		END
	END

	IF (@v_dateFromVc is NULL OR @v_dateFromVc='') and (@v_dateToVc is NULL OR @v_dateToVc='')
	BEGIN
		SET @v_TempVariableVc=@v_TempVariableVc
	END
	ELSE
	BEGIN
		IF (@v_TempVariableVc='')
		BEGIN
			SET @v_TempVariableVc=' (( CAST(floor(cast(mi.[creationDate] as float))as datetime) >= '''+convert(varchar(30),CAST(floor(cast(convert(datetime,@v_dateFromVc,105) as float))as datetime),120)+'''
			AND CAST(floor(cast(mi.[creationDate] as float))as datetime) <= '''+convert(varchar(30),CAST(floor(cast(convert(datetime,@v_dateToVc,105) as float))as datetime),120)+''' ))'
		END
		ELSE
		BEGIN
			SET @v_TempVariableVc=@v_TempVariableVc + ' AND ((CAST(floor(cast(mi.[creationDate] as float))as datetime) >= '''+convert(varchar(30),CAST(floor(cast(convert(datetime,@v_dateFromVc,105) as float))as datetime),120)+''' AND CAST(floor(cast(mi.[creationDate] as float))as datetime) <= '''+convert(varchar(30),CAST(floor(cast(convert(datetime,@v_dateToVc,105) as float))as datetime),120)+''' ))'
		END
	END


	IF @v_viewMsgVc='no'  --if View Message is password = 'no'
	BEGIN
		IF @v_searchInVc='subject'	--Where search is made in SUBJECT
		BEGIN
			IF @v_FolderIdN=0   --When FolderId = 0 and search is made in SUBJECT
			BEGIN
				If @v_TempVariableVc =''
				BEGIN
					SET @v_TempVariableVc=' 0=0 '
				END

				IF @v_MsgBoxTypeVc='Inbox' --if MessageBox Type is Inbox and Email id= mi.[msgFromUserId]
				BEGIN
					set @v_TeampQueryVc=
					'DECLARE @v_Reccountf int
					DECLARE @v_TotalPagef int
					SELECT	@v_Reccountf = Count(*) From (
							SELECT	* From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
							FROM tbl_Message m
									INNER JOIN tbl_MessageInBox mi ON mi.msgId=m.msgId
												AND mi.msgStatus='''+@v_msgStatusVc+'''
												AND mi.msgStatus<>''trash''
									INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
									INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
							WHERE msgToUserId='+cast(@v_UserIdN as varchar(10))+'
									AND folderId= 0  AND
							'+@v_TempVariableVc+') AS DATA) AS TTT

					SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
					SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
							,m.[msgText],mi.[msgBoxId],mi.[msgToUserId],mi.[msgFromUserId]
							--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
							--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
							,fromLogin.emailid as FromEmail
							,toLogin.emailid as ToEmail
							,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
							,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded],mi.[processUrl]
					FROM	tbl_Message m
							INNER JOIN tbl_MessageInBox mi ON mi.msgId=m.msgId
										AND mi.msgStatus='''+@v_msgStatusVc+'''
										AND mi.msgStatus<>''trash''
							INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
							INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE	msgToUserId='+cast(@v_UserIdN as varchar(10))+' AND folderId= 0  AND
					'+@v_TempVariableVc+') AS DATA where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
							AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''

					print '1'+@v_TeampQueryVc
					exec(@v_TeampQueryVc)
				END
				ElSE IF @v_MsgBoxTypeVc='Sent' --if MessageBox Type is Sent and Email id= mi.[msgToUserId]
				BEGIN
					set @v_TeampQueryVc=
					'DECLARE @v_Reccountf int
					DECLARE @v_TotalPagef int
					SELECT @v_Reccountf = Count(*) From (
					SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
					FROM tbl_Message m
						 INNER JOIN tbl_MessageSent mi ON mi.msgId=m.msgId
									 AND mi.msgStatus='''+@v_msgStatusVc+'''
									 AND mi.msgStatus<>''trash''
						 INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						 INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE	msgToCCReply=''to''
							AND msgFromUserId='+cast(@v_UserIdN as varchar(10))+'
							AND folderId= 0
							AND '+@v_TempVariableVc+'
					) AS DATA) AS TTT

					SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
					SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
							,m.[msgText],mi.[msgSentId] msgBoxId,mi.[msgToUserId] msgToUserId,mi.[msgFromUserId]
							--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
							--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
							,fromLogin.emailid as FromEmail
							,toLogin.emailid as ToEmail
							,mi.[msgToCCReply],
							mi.[msgStatus],
							mi.[creationDate]
							,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded]
					FROM tbl_Message m
						INNER JOIN tbl_MessageSent mi ON mi.msgId=m.msgId
								AND mi.msgStatus='''+@v_msgStatusVc+'''
								AND mi.msgStatus<>''trash''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE	msgToCCReply=''to''
							AND msgFromUserId='+cast(@v_UserIdN as varchar(10))+'
							AND folderId= 0

					AND
					'+@v_TempVariableVc+'
					 ) AS DATA where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
							AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''

					print '2'+@v_TeampQueryVc
					exec(@v_TeampQueryVc)
				END
				ELSE IF @v_MsgBoxTypeVc='Draft' --if MessageBox Type is Draft and Email id= mi.[msgFromUserId]
				BEGIN
					SET @v_TeampQueryVc=
					'DECLARE @v_Reccountf int
					DECLARE @v_TotalPagef int
					SELECT @v_Reccountf = Count(*) From (
					SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
					FROM tbl_Message m
						INNER JOIN tbl_MessageDraft mi ON mi.msgId=m.msgId
									AND mi.msgStatus='''+@v_msgStatusVc+'''
									AND mi.msgStatus<>''trash''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE msgToUserId='+cast(@v_UserIdN as varchar(10))+' AND folderId= 0 AND
					'+@v_TempVariableVc+') AS DATA) AS TTT

					SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
					SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
							,m.[msgText],mi.[msgDraftId] msgBoxId,mi.[msgToUserId],mi.[msgFromUserId]
							--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
							--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
							,fromLogin.emailid as FromEmail
							,toLogin.emailid as ToEmail
							,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
							,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded]
					FROM tbl_Message m
							INNER JOIN tbl_MessageDraft mi ON mi.msgId=m.msgId
										AND mi.msgStatus='''+@v_msgStatusVc+'''
										AND mi.msgStatus<>''trash''
							INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
							INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE	msgToUserId='+cast(@v_UserIdN as varchar(10))+'
							AND folderId= 0
							AND '+@v_TempVariableVc+') AS DATA
				where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
							AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''

					print '3'+@v_TeampQueryVc
					exec(@v_TeampQueryVc)

				END
				ELSE IF @v_MsgBoxTypeVc='Trash' ----if MessageBox Type is Trash and Email id= mi.[msgFromUserId]
				BEGIN
					--AND folderId= 0
					set @v_TeampQueryVc=
					'DECLARE @v_Reccountf int
					DECLARE @v_TotalPagef int
					SELECT @v_Reccountf = Count(*) From (
					SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
					FROM tbl_Message m
						INNER JOIN tbl_MessageTrash mi ON mi.msgId=m.msgId
									AND mi.msgStatus='''+@v_msgStatusVc+'''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE msgToUserId='+cast(@v_UserIdN as varchar(10))+'
							AND '+@v_TempVariableVc+'
				) AS DATA) AS TTT

					SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
					SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
							,m.[msgText],mi.[msgTrashId] msgBoxId,mi.[msgToUserId],mi.[msgFromUserId]
							--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
							--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
							,fromLogin.emailid as FromEmail
							,toLogin.emailid as ToEmail
							,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
							,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded],mi.[processUrl]
					FROM tbl_Message m
						INNER JOIN tbl_MessageTrash mi ON mi.msgId=m.msgId
									AND mi.msgStatus='''+@v_msgStatusVc+'''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE msgToUserId='+cast(@v_UserIdN as varchar(10))+'
						 AND '+@v_TempVariableVc+') AS DATA
				where	Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
						AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
					--AND folderId= 0
					print '4'+@v_TeampQueryVc
					exec(@v_TeampQueryVc)
				END
			END
			--When FolderId is not equal to 0 and search is made in SUBJECT
			ELSE
			BEGIN
				SET @v_TeampQueryVc=
				'DECLARE @v_Reccountf int
				DECLARE @v_TotalPagef int
				SELECT @v_Reccountf = Count(*) From (
				SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
				FROM tbl_Message m
					INNER JOIN tbl_MessageInBox mi ON mi.msgId=m.msgId
								AND mi.msgStatus='''+@v_msgStatusVc+'''
								AND mi.[folderId]='+cast(@v_FolderIdN as varchar(10))+'
								AND mi.msgStatus<>''trash''
					INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
					INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
				WHERE msgToUserId='+cast(@v_UserIdN as varchar(10))+'  AND
				'+@v_TempVariableVc+') AS DATA) AS TTT
				SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
				SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
						,m.[msgText],mi.[msgBoxId] ,mi.[msgToUserId],mi.[msgFromUserId]
						--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
						--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
						,fromLogin.emailid as FromEmail
						,toLogin.emailid as ToEmail
						,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
						,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded],mi.[processUrl]
				FROM tbl_Message m
						INNER JOIN tbl_MessageInBox mi ON mi.msgId=m.msgId
									AND mi.msgStatus='''+@v_msgStatusVc+'''
									AND mi.[folderId]='+cast(@v_FolderIdN as varchar(10))+'
									AND mi.msgStatus<>''trash''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
				WHERE	msgToUserId='+cast(@v_UserIdN as varchar(10))+'
						AND '+@v_TempVariableVc+') AS DATA
				where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
						AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''

				print '5'+@v_TeampQueryVc
				exec(@v_TeampQueryVc)
			END
		END
		ELSE IF @v_searchInVc='message'  --Where search is made in MESSAGE
		BEGIN
			IF @v_FolderIdN=0	--When FolderId = 0 and search is made in MESSAGE
			BEGIN
				IF @v_MsgBoxTypeVc='Inbox'  --if MessageBox Type is Inbox and Email id= mi.[msgFromUserId]
				BEGIN
					set @v_TeampQueryVc=
					'DECLARE @v_Reccountf int
					DECLARE @v_TotalPagef int
					SELECT @v_Reccountf = Count(*) From (
					SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
					FROM tbl_Message m
						INNER JOIN tbl_MessageInBox mi ON mi.msgId=m.msgId
								AND mi.msgStatus<>''trash''
								AND mi.msgStatus='''+@v_msgStatusVc+'''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE msgToUserId='+cast(@v_UserIdN as varchar(10))+'
							AND folderId= 0
							AND '+@v_TempVariableVc+') AS DATA) AS TTT

					SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
					SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
							,m.[msgText],mi.[msgBoxId],mi.[msgToUserId],mi.[msgFromUserId]
							--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
							--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
							,fromLogin.emailid as FromEmail
							,toLogin.emailid as ToEmail
							,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
							,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded],mi.[processUrl]
					FROM tbl_Message m
						INNER JOIN tbl_MessageInBox mi ON mi.msgId=m.msgId
									AND mi.msgStatus='''+@v_msgStatusVc+'''
									AND mi.msgStatus<>''trash''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE msgToUserId='+cast(@v_UserIdN as varchar(10))+'
						AND folderId= 0
						AND '+@v_TempVariableVc+') AS DATA
				where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
							AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''

					print '6'+@v_TeampQueryVc
					exec(@v_TeampQueryVc)

				END
				ElSE IF @v_MsgBoxTypeVc='Sent'  --if MessageBox Type is Sent and Email id= mi.[msgToUserId]
				BEGIN
					set @v_TeampQueryVc=
					'DECLARE @v_Reccountf int
					DECLARE @v_TotalPagef int
					SELECT @v_Reccountf = Count(*) From (
					SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
					FROM tbl_Message m
						INNER JOIN tbl_MessageSent mi ON mi.msgId=m.msgId
									AND mi.msgStatus='''+@v_msgStatusVc+'''
									AND mi.msgStatus<>''trash''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE	msgToCCReply=''to''
							AND folderId= 0
							AND msgFromUserId='+cast(@v_UserIdN as varchar(10))+'
							AND '+@v_TempVariableVc+') AS DATA) AS TTT

					SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
					SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
							,m.[msgText],mi.[msgSentId] msgBoxId,mi.[msgToUserId],mi.[msgFromUserId]
							--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
							--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
							,fromLogin.emailid as FromEmail
							,toLogin.emailid as ToEmail
							,mi.[msgToCCReply],mi.[msgStatus]
							,mi.[creationDate]
							,mi.[folderId]
							,mi.[isPriority]
							,mi.[isMsgRead]
							,mi.[isDocUploaded]
					FROM tbl_Message m
							INNER JOIN tbl_MessageSent mi ON mi.msgId=m.msgId
										AND mi.msgStatus='''+@v_msgStatusVc+'''
										AND mi.msgStatus<>''trash''
							INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
							INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE	msgToCCReply=''to''
							AND msgFromUserId='+cast(@v_UserIdN as varchar(10))+'
							AND folderId= 0
							AND '+@v_TempVariableVc+') AS DATA
					where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
							AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''

					print '7'+@v_TeampQueryVc
					exec(@v_TeampQueryVc)

				END
				ELSE IF @v_MsgBoxTypeVc='Draft'  --if MessageBox Type is Draft and Email id= mi.[msgFromUserId]
				BEGIN
					set @v_TeampQueryVc=
					'DECLARE @v_Reccountf int
					DECLARE @v_TotalPagef int
					SELECT @v_Reccountf = Count(*) From (
					SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
					FROM tbl_Message m
							INNER JOIN tbl_MessageDraft mi ON mi.msgId=m.msgId
										AND mi.msgStatus='''+@v_msgStatusVc+'''
										AND mi.msgStatus<>''trash''
							INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
							INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE msgToUserId='+cast(@v_UserIdN as varchar(10))+'
							AND folderId= 0
							AND '+@v_TempVariableVc+') AS DATA) AS TTT

					SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')

					SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
							,m.[msgText],mi.[msgDraftId] msgBoxId,mi.[msgToUserId],mi.[msgFromUserId]
							--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
							--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
							,fromLogin.emailid as FromEmail
							,toLogin.emailid as ToEmail
							,mi.[msgToCCReply]
							,mi.[msgStatus]
							,mi.[creationDate]
							,mi.[folderId]
							,mi.[isPriority]
							,mi.[isMsgRead]
							,mi.[isDocUploaded]
					FROM tbl_Message m
							INNER JOIN tbl_MessageDraft mi ON mi.msgId=m.msgId
										AND mi.msgStatus='''+@v_msgStatusVc+'''
										AND mi.msgStatus<>''trash''
							INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
							INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE msgToUserId='+cast(@v_UserIdN as varchar(10))+'
						AND folderId= 0
						AND '+@v_TempVariableVc+') AS DATA
				where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
							AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''

					print '8'+@v_TeampQueryVc
					exec(@v_TeampQueryVc)

				END
				ELSE IF @v_MsgBoxTypeVc='Trash' ----if MessageBox Type is Trash and Email id= mi.[msgFromUserId]
				BEGIN
					--AND folderId= 0
					set @v_TeampQueryVc=
					'DECLARE @v_Reccountf int
					DECLARE @v_TotalPagef int
					SELECT @v_Reccountf = Count(*) From (
					SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
					FROM tbl_Message m
							INNER JOIN tbl_MessageTrash mi ON mi.msgId=m.msgId
								AND mi.msgStatus='''+@v_msgStatusVc+'''
							INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
							INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE	msgToUserId='+cast(@v_UserIdN as varchar(10))+'
							AND '+@v_TempVariableVc+') AS DATA) AS TTT

					SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
					SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
							,m.[msgText],mi.[msgTrashId] msgBoxId,mi.[msgToUserId],mi.[msgFromUserId]
							--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
							--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
							,fromLogin.emailid as FromEmail
							,toLogin.emailid as ToEmail
							,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
							,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded],mi.[processUrl]
					FROM tbl_Message m
							INNER JOIN tbl_MessageTrash  mi ON mi.msgId=m.msgId
								AND mi.msgStatus='''+@v_msgStatusVc+'''
							INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
							INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE	msgToUserId='+cast(@v_UserIdN as varchar(10))+'
							AND	'+@v_TempVariableVc+') AS DATA
				where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
							AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
					--AND folderId= 0
					print '9'+@v_TeampQueryVc
					exec(@v_TeampQueryVc)
				END
			END
			ELSE
			BEGIN
				set @v_TeampQueryVc=
				'DECLARE @v_Reccountf int
				DECLARE @v_TotalPagef int
				SELECT @v_Reccountf = Count(*) From (
				SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
				FROM tbl_Message m
						INNER JOIN tbl_MessageInBox mi ON mi.msgId=m.msgId
							AND mi.msgStatus='''+@v_msgStatusVc+'''
							AND mi.[folderId]='+cast(@v_FolderIdN as varchar(10))+'
							AND mi.msgStatus<>''trash''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
				WHERE	msgToUserId='+cast(@v_UserIdN as varchar(10))+'
						AND '+@v_TempVariableVc+') AS DATA) AS TTT

				SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')

				SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
						,m.[msgText],mi.[msgBoxId],mi.[msgToUserId],mi.[msgFromUserId]
						--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
						--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
						,fromLogin.emailid as FromEmail
						,toLogin.emailid as ToEmail
						,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
						,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded],mi.[processUrl]
				FROM tbl_Message m
						INNER JOIN tbl_MessageInBox mi ON mi.msgId=m.msgId
							AND mi.msgStatus='''+@v_msgStatusVc+'''
							AND mi.[folderId]='+cast(@v_FolderIdN as varchar(10))+'
							AND mi.msgStatus<>''trash''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
				WHERE	msgToUserId='+cast(@v_UserIdN as varchar(10))+'
						AND '+@v_TempVariableVc+') AS DATA
			where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
						AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''

				print '10'+@v_TeampQueryVc
				exec(@v_TeampQueryVc)

			END		--When FolderId is not equal to 0 and search is made in MESSAGE
		END
		ELSE IF @v_searchInVc=''  --When No searchin is select
		BEGIN
			IF @v_FolderIdN=0	--When FolderId = 0 and search is made in MESSAGE
			BEGIN
				IF @v_MsgBoxTypeVc='Inbox'  --if MessageBox Type is Inbox and Email id= mi.[msgFromUserId]
				BEGIN
					set @v_TeampQueryVc=
					'DECLARE @v_Reccountf int
					DECLARE @v_TotalPagef int
					SELECT @v_Reccountf = Count(*) From (
					SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
					FROM tbl_Message m
						INNER JOIN tbl_MessageInBox mi ON mi.msgId=m.msgId
									AND mi.msgStatus='''+@v_msgStatusVc+'''
									AND mi.msgStatus<>''trash''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE	msgToUserId='+cast(@v_UserIdN as varchar(10))+'
							AND folderId= 0 ) AS DATA) AS TTT

					SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
					SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
							,m.[msgText],mi.[msgBoxId],mi.[msgToUserId],mi.[msgFromUserId]
							--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
							--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
							,toLogin.emailid  as ToEmail
							,fromLogin.emailid  as FromEmail
							,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
							,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded],mi.[processUrl]
					FROM tbl_Message m
						INNER JOIN tbl_MessageInBox mi ON mi.msgId=m.msgId
									AND mi.msgStatus='''+@v_msgStatusVc+'''
									AND mi.msgStatus<>''trash''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE  msgToUserId='+cast(@v_UserIdN as varchar(10))+'
							AND folderId= 0 ) AS DATA
			where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
							AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
					print '11_0 '+@v_TeampQueryVc
					exec(@v_TeampQueryVc)
				END
				ELSE IF @v_MsgBoxTypeVc='InboxRead'  --if MessageBox Type is Inbox and Email id= mi.[msgFromUserId]
				BEGIN
					set @v_TeampQueryVc=
					'DECLARE @v_Reccountf int
					DECLARE @v_TotalPagef int
					SELECT @v_Reccountf = Count(*) From (
					SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
					FROM tbl_Message m
						INNER JOIN tbl_MessageInBox mi ON mi.msgId=m.msgId
									AND mi.isMsgRead = ''yes''
									AND mi.msgStatus='''+@v_msgStatusVc+'''
									AND mi.msgStatus<>''trash''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE	folderId= 0
							AND msgToUserId='+cast(@v_UserIdN as varchar(10))+' ) AS DATA) AS TTT

					SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
					SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
							,m.[msgText],mi.[msgBoxId],mi.[msgToUserId],mi.[msgFromUserId]
							--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
							--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
							,fromLogin.emailid as FromEmail
							,toLogin.emailid as ToEmail
							,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
							,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded],mi.[processUrl]
					FROM	tbl_Message m
							INNER JOIN tbl_MessageInBox mi ON mi.msgId=m.msgId
										AND mi.isMsgRead = ''yes''
										AND mi.msgStatus='''+@v_msgStatusVc+'''
										AND mi.msgStatus<>''trash''
							INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
							INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE	msgToUserId='+cast(@v_UserIdN as varchar(10))+'
							AND folderId= 0 ) AS DATA
			where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
							AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
					print '11_1'+@v_TeampQueryVc
					exec(@v_TeampQueryVc)


				END
				ELSE IF @v_MsgBoxTypeVc='InboxUnRead'  --if MessageBox Type is Inbox and Email id= mi.[msgFromUserId]
				BEGIN

					set @v_TeampQueryVc=
					'DECLARE @v_Reccountf int
					DECLARE @v_TotalPagef int
					SELECT @v_Reccountf = Count(*) From (
					SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
					FROM tbl_Message m
							INNER JOIN tbl_MessageInBox mi ON mi.msgId=m.msgId
										AND mi.isMsgRead = ''no''
										AND mi.msgStatus='''+@v_msgStatusVc+'''
										AND mi.msgStatus<>''trash''
							INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
							INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE	msgToUserId='+cast(@v_UserIdN as varchar(10))+'
							AND folderId= 0 ) AS DATA) AS TTT

					SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
					SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
							,m.[msgText],mi.[msgBoxId],mi.[msgToUserId],mi.[msgFromUserId]
							--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
							--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
							,fromLogin.emailid as FromEmail
							,toLogin.emailid as ToEmail
							,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
							,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded],mi.[processUrl]
					FROM tbl_Message m
							INNER JOIN tbl_MessageInBox mi ON mi.msgId=m.msgId
										AND mi.isMsgRead = ''no''
										AND mi.msgStatus='''+@v_msgStatusVc+'''
										AND mi.msgStatus<>''trash''
							INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
							INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE	msgToUserId='+cast(@v_UserIdN as varchar(10))+'
							AND folderId= 0 ) AS DATA
				where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
							AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
					print '11_2'+@v_TeampQueryVc
					exec(@v_TeampQueryVc)
				END
				ElSE IF @v_MsgBoxTypeVc='Sent'  --if MessageBox Type is Sent and Email id= mi.[msgToUserId]
				BEGIN
					set @v_TeampQueryVc=
					'DECLARE @v_Reccountf int
					DECLARE @v_TotalPagef int
					SELECT @v_Reccountf = Count(*) From (
					SELECT	* From (SELECT	ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,
											m.[msgId]
									FROM	tbl_Message m
							INNER JOIN tbl_MessageSent mi ON mi.msgId=m.msgId
										AND mi.msgStatus='''+@v_msgStatusVc+'''
										AND	mi.msgStatus<>''trash''
										AND mi.msgSentId in (	select	min(msgSentId)
																from	tbl_MessageSent
																where	msgToCCReply=''to''
																		AND msgFromUserId='+cast(@v_UserIdN as varchar(10))+'
																		AND  msgStatus=''Live''
																		AND folderId= 0
																		AND  msgStatus<>''trash''
																group by msgId
															)
							INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
							INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE	msgToCCReply=''to''
							AND msgFromUserId='+cast(@v_UserIdN as varchar(10))+'
							AND folderId= 0
						) AS DATA
					) AS TTT

					SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
					SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
							,m.[msgText],mi.[msgSentId] msgBoxId,mi.[msgToUserId],mi.[msgFromUserId]
							--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
							--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
							,fromLogin.emailid as FromEmail
							,toLogin.emailid as	ToEmail
							,mi.[msgToCCReply]
							,mi.[msgStatus]
							,mi.[creationDate]
							,mi.[folderId]
							,mi.[isPriority]
							,mi.[isMsgRead]
							,mi.[isDocUploaded]
					FROM tbl_Message m
						INNER JOIN tbl_MessageSent mi ON mi.msgId=m.msgId
									AND mi.msgStatus='''+@v_msgStatusVc+'''
									AND mi.msgStatus<>''trash''
									AND  mi.msgSentId in (	SELECT	min(msgSentId)
															FROM	tbl_MessageSent
															WHERE	msgToCCReply=''to''
																	AND msgFromUserId='+cast(@v_UserIdN as varchar(10))+'
																	AND  msgStatus=''Live''
																	AND folderId= 0
																	AND  msgStatus<>''trash''
															group by msgId
														)
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE	msgToCCReply=''to''
							AND msgFromUserId='+cast(@v_UserIdN as varchar(10))+'
							AND folderId= 0

					) AS DATA where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
					AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''

					print '12'+@v_TeampQueryVc
					exec(@v_TeampQueryVc)
				END
				ELSE IF @v_MsgBoxTypeVc='Draft'  --if MessageBox Type is Draft and Email id= mi.[msgFromUserId]
				BEGIN
					set @v_msgStatusVc='Live'
					set @v_TeampQueryVc=
					'DECLARE @v_Reccountf int
					DECLARE @v_TotalPagef int
					SELECT @v_Reccountf = Count(*) From (
					SELECT * From (SELECT	ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,
											m.[msgId]
									FROM	tbl_Message m
											INNER JOIN tbl_MessageDraft mi ON mi.msgId=m.msgId
														AND mi.msgStatus='''+@v_msgStatusVc+'''
														AND mi.msgStatus<>''trash''
											INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
											INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
									WHERE	msgToCCReply=''to''
											AND msgFromUserId='+cast(@v_UserIdN as varchar(10))+'
											AND folderId= 0
									 ) AS DATA) AS TTT

					SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
					SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
							,m.[msgText],mi.[msgDraftId] msgBoxId,mi.[msgToUserId],mi.[msgFromUserId]
							--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
							--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
							,fromLogin.emailid as FromEmail
							,toLogin.emailid as	 ToEmail
							,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
							,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded]
					FROM tbl_Message m
						INNER JOIN tbl_MessageDraft mi ON mi.msgId=m.msgId
									AND mi.msgStatus='''+@v_msgStatusVc+'''
									AND mi.msgStatus<>''trash''
									AND  mi.msgDraftId in (	SELECT	min(msgDraftId)
															FROM	tbl_MessageDraft
															WHERE	msgToCCReply=''to''
																	AND msgFromUserId='+cast(@v_UserIdN as varchar(10))+'
																	AND  msgStatus='''+@v_msgStatusVc+'''
																	AND folderId= 0
																	AND  msgStatus<>''trash''
															group by msgId
														)
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE	msgToCCReply=''to''
							AND msgFromUserId='+cast(@v_UserIdN as varchar(10))+'
							AND folderId= 0
					) AS DATA where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
					AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''

					--set @v_TeampQueryVc=
					--'DECLARE @v_Reccountf int
					--DECLARE @v_TotalPagef int
					--SELECT @v_Reccountf = Count(*) From (
					--SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
					--FROM tbl_Message m,tbl_MessageDraft mi
					--WHERE m.msgId=mi.msgId and msgToCCReply=''to'' AND msgToUserId='+cast(@v_UserIdN as varchar(10))+' AND mi.msgStatus='''+@v_msgStatusVc+''' AND folderId= 0 AND mi.msgStatus<>''trash'') AS DATA) AS TTT

					--SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
					--SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
					--		,m.[msgText],mi.[msgDraftId] msgBoxId,mi.[msgToUserId],mi.[msgFromUserId]
					--		,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as FromEmail
					--		,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as ToEmail
					--		,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
					--		,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded]
					--FROM tbl_Message m,tbl_MessageDraft mi
					--WHERE m.msgId=mi.msgId and msgToCCReply=''to'' AND msgFromUserId='+cast(@v_UserIdN as varchar(10))+' AND mi.msgStatus='''+@v_msgStatusVc+''' AND folderId= 0 AND mi.msgStatus<>''trash'') AS DATA where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
					--		AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''

					 print '13'+@v_TeampQueryVc
					exec(@v_TeampQueryVc)

				END
				ELSE IF @v_MsgBoxTypeVc='Trash' ----if MessageBox Type is Trash and Email id= mi.[msgFromUserId]
				BEGIN
					--AND folderId= 0
					set @v_TeampQueryVc=
					'DECLARE @v_Reccountf int
					DECLARE @v_TotalPagef int
					SELECT @v_Reccountf = Count(*)
					FROM (	SELECT	*
							From (	SELECT	ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
									FROM	tbl_Message m
											INNER JOIN tbl_MessageTrash mi ON mi.msgId=m.msgId
														AND mi.msgStatus='''+@v_msgStatusVc+'''
											INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
											INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
									WHERE msgToUserId='+cast(@v_UserIdN as varchar(10))+'
								) AS DATA
						) AS TTT

					SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
					SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
							,m.[msgText],mi.[msgTrashId] msgBoxId,mi.[msgToUserId],mi.[msgFromUserId]
							--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
							--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
							,fromLogin.emailid as FromEmail
							,toLogin.emailid as	 ToEmail
							,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
							,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded],mi.[processUrl]
					FROM tbl_Message m
						INNER JOIN tbl_MessageTrash mi ON mi.msgId=m.msgId
									AND mi.msgStatus='''+@v_msgStatusVc+'''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
					WHERE msgToUserId='+cast(@v_UserIdN as varchar(10))+' ) AS DATA
				where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
							AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
					--AND folderId= 0
					print '14'+@v_TeampQueryVc
					exec(@v_TeampQueryVc)
				END
			END
			ELSE
			BEGIN
				set @v_TeampQueryVc=
				'DECLARE @v_Reccountf int
				DECLARE @v_TotalPagef int
				SELECT @v_Reccountf = Count(*) From (
				SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
				FROM tbl_Message m
						INNER JOIN tbl_MessageInBox  mi ON mi.msgId=m.msgId
									AND mi.msgStatus='''+@v_msgStatusVc+'''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
				WHERE	msgToUserId='+cast(@v_UserIdN as varchar(10))+'
						AND folderId= '+cast(@v_FolderIdN as varchar(10))+' ) AS DATA) AS TTT

				SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
				SELECT *
						,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
						,m.[msgText],mi.[msgBoxId],mi.[msgToUserId],mi.[msgFromUserId]
						--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
						--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
						,fromLogin.emailid as FromEmail
						,toLogin.emailid as ToEmail
						,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
						,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded],mi.[processUrl]
				FROM	tbl_Message m
						INNER JOIN tbl_MessageInBox  mi ON mi.msgId=m.msgId
									AND mi.msgStatus='''+@v_msgStatusVc+'''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
				WHERE	msgToUserId='+cast(@v_UserIdN as varchar(10))+'
						AND folderId= '+cast(@v_FolderIdN as varchar(10))+') AS DATA
			where	Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
					AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
				print '15'+@v_TeampQueryVc

				exec(@v_TeampQueryVc)

			END		--When FolderId is not equal to 0 and search is made in MESSAGE
		END
	END
	ELSE				  --if View Message is password = 'yes'
	BEGIN
		IF @v_FolderIdN=0	--When FolderId = 0 and search is made in MESSAGE
		BEGIN

			IF @v_MsgBoxTypeVc='Inbox'		--if MessageBox Type is Inbox and Email id= mi.[msgFromUserId]
			BEGIN
				set @v_TeampQueryVc=
				'DECLARE @v_Reccountf int
				DECLARE @v_TotalPagef int
				SELECT @v_Reccountf = Count(*) From (
				SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
				FROM tbl_Message m
						INNER JOIN tbl_MessageInBox  mi ON mi.msgId=m.msgId
									AND mi.msgStatus='''+@v_msgStatusVc+'''
									AND mi.msgStatus<>''trash''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
				WHERE	folderId= 0
						AND m.[msgId]='''+cast(@v_msgidN as  varchar(10))+''') AS DATA) AS TTT

				SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
				SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
						,m.[msgText],mi.[msgBoxId],mi.[msgToUserId],mi.[msgFromUserId]
						--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
						--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
						,fromLogin.emailid as FromEmail
						,toLogin.emailid as ToEmail
						,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
						,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded],mi.[processUrl]
				FROM tbl_Message m
						INNER JOIN tbl_MessageInBox  mi ON mi.msgId=m.msgId
									AND mi.msgStatus='''+@v_msgStatusVc+'''
									AND mi.msgStatus<>''trash''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
				WHERE	folderId= 0
						AND m.[msgId]='''+cast(@v_msgidN as  varchar(10))+''') AS DATA
			where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
						AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
				--AND folderId= 0
				print '16 '+@v_TeampQueryVc
				exec(@v_TeampQueryVc)

			END
			ELSE IF @v_MsgBoxTypeVc='InboxRead'		--if MessageBox Type is Inbox and Email id= mi.[msgFromUserId]
			BEGIN
				set @v_TeampQueryVc=
				'DECLARE @v_Reccountf int
				DECLARE @v_TotalPagef int
				SELECT @v_Reccountf = Count(*) From (
				SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
				FROM	tbl_Message m
						INNER JOIN tbl_MessageInBox  mi ON mi.msgId=m.msgId
									AND mi.ismsgread=''YES''
									AND mi.msgStatus='''+@v_msgStatusVc+'''
									AND mi.msgStatus<>''trash''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
				WHERE	folderId= 0
						AND m.[msgId]='''+cast(@v_msgidN as  varchar(10))+''') AS DATA) AS TTT

				SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
				SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
						,m.[msgText],mi.[msgBoxId],mi.[msgToUserId],mi.[msgFromUserId]
						--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
						--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
						,fromLogin.emailid as FromEmail
						,toLogin.emailid as ToEmail
						,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
						,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded],mi.[processUrl]
				FROM tbl_Message m
						INNER JOIN tbl_MessageInBox  mi ON mi.msgId=m.msgId
									AND mi.ismsgread=''YES''
									AND mi.msgStatus='''+@v_msgStatusVc+'''
									AND mi.msgStatus<>''trash''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
				WHERE	folderId= 0
						AND m.[msgId]='''+cast(@v_msgidN as  varchar(10))+''') AS DATA where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
						AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
				--AND folderId= 0
				print '17 '+@v_TeampQueryVc
				exec(@v_TeampQueryVc)

			END
			ELSE IF @v_MsgBoxTypeVc='InboxUnRead'		--if MessageBox Type is Inbox and Email id= mi.[msgFromUserId]
			BEGIN
				set @v_TeampQueryVc=
				'DECLARE @v_Reccountf int
				DECLARE @v_TotalPagef int
				SELECT @v_Reccountf = Count(*) From (
				SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
				FROM	tbl_Message m
						INNER JOIN tbl_MessageInBox  mi ON mi.msgId=m.msgId
									AND mi.ismsgread=''No''
									AND mi.msgStatus='''+@v_msgStatusVc+'''
									AND mi.msgStatus<>''trash''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
				WHERE	folderId= 0
						AND m.[msgId]='''+cast(@v_msgidN as  varchar(10))+''') AS DATA) AS TTT

				SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
				SELECT	*
						,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
						,m.[msgText],mi.[msgBoxId],mi.[msgToUserId],mi.[msgFromUserId]
						--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
						--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
						,fromLogin.emailid as FromEmail
						,toLogin.emailid as ToEmail
						,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
						,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded],mi.[processUrl]
				FROM	tbl_Message m
						INNER JOIN tbl_MessageInBox  mi ON mi.msgId=m.msgId
									AND mi.ismsgread=''No''
									AND mi.msgStatus='''+@v_msgStatusVc+'''
									AND mi.msgStatus<>''trash''
						INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
						INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
				WHERE	folderId= 0
						AND m.[msgId]='''+cast(@v_msgidN as  varchar(10))+'''
					) AS DATA where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
						AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
				--AND folderId= 0
				print '18 '+@v_TeampQueryVc
				exec(@v_TeampQueryVc)
			END
			ElSE IF @v_MsgBoxTypeVc='Sent'	--if MessageBox Type is Sent and Email id= mi.[msgToUserId]
			BEGIN
				set @v_TeampQueryVc=
				'DECLARE @v_Reccountf int
				DECLARE @v_TotalPagef int
				SELECT @v_Reccountf = Count(*) From (
				SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
				FROM tbl_Message m
					INNER JOIN tbl_MessageSent   mi ON mi.msgId=m.msgId
							AND mi.msgStatus='''+@v_msgStatusVc+'''
							AND mi.msgStatus<>''trash''
					INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
					INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
				WHERE	folderId= 0
						AND m.[msgId]='''+cast(@v_msgidN as  varchar(10))+''') AS DATA) AS TTT

				SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
				SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
						,m.[msgText],mi.[msgSentId] msgBoxId,mi.[msgToUserId],mi.[msgFromUserId]
						--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
						--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
						,fromLogin.emailid as FromEmail
						,toLogin.emailid as ToEmail
						,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
						,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded]
				FROM tbl_Message m
					INNER JOIN tbl_MessageSent   mi ON mi.msgId=m.msgId
							AND mi.msgStatus='''+@v_msgStatusVc+'''
							AND mi.msgStatus<>''trash''
					INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
					INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
				WHERE folderId= 0
						AND m.[msgId]='''+cast(@v_msgidN as  varchar(10))+''') AS DATA where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
						AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''

				print '19 '+@v_TeampQueryVc
				exec(@v_TeampQueryVc)

			END
			ELSE IF @v_MsgBoxTypeVc='Draft'	--if MessageBox Type is Draft and Email id= mi.[msgFromUserId]
			BEGIN
				set @v_msgStatusVc='Live'
				set @v_TeampQueryVc=
				'DECLARE @v_Reccountf int
				DECLARE @v_TotalPagef int
				SELECT @v_Reccountf = Count(*) From (
				SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
				FROM tbl_Message m
					INNER JOIN tbl_MessageDraft   mi ON mi.msgId=m.msgId
							AND mi.msgStatus='''+@v_msgStatusVc+'''
							AND mi.msgStatus<>''trash''
					INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
					INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
				WHERE folderId= 0
						AND m.[msgId]='''+cast(@v_msgidN as  varchar(10))+''') AS DATA) AS TTT

				SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
				SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
						,m.[msgText],mi.[msgDraftId] msgBoxId,mi.[msgToUserId],mi.[msgFromUserId]
						--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
						--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
						,fromLogin.emailid as FromEmail
						,toLogin.emailid as ToEmail
						,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
						,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded]
				FROM tbl_Message m
					INNER JOIN tbl_MessageDraft   mi ON mi.msgId=m.msgId
							AND mi.msgStatus='''+@v_msgStatusVc+'''
							AND mi.msgStatus<>''trash''
					INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
					INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
				WHERE folderId= 0
						AND m.[msgId]='''+cast(@v_msgidN as  varchar(10))+''') AS DATA where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
						AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''

				print '20 '+@v_TeampQueryVc
				exec(@v_TeampQueryVc)

			END
			ELSE IF @v_MsgBoxTypeVc='Trash' ----if MessageBox Type is Trash and Email id= mi.[msgFromUserId]
			BEGIN
				--AND folderId= 0
				set @v_TeampQueryVc=
				'DECLARE @v_Reccountf int
				DECLARE @v_TotalPagef int
				SELECT @v_Reccountf = Count(*) From (
				SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
				FROM tbl_Message m
					INNER JOIN tbl_MessageTrash  mi ON mi.msgId=m.msgId
							AND mi.msgStatus='''+@v_msgStatusVc+'''
					INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
					INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
				WHERE  m.[msgId]='''+cast(@v_msgidN as  varchar(10))+''') AS DATA) AS TTT

				SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
				SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
						,m.[msgText],mi.[msgTrashId] msgBoxId,mi.[msgToUserId],mi.[msgFromUserId]
						--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
						--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
						,fromLogin.emailid as FromEmail
						,toLogin.emailid as	 ToEmail
						,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
						,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded],mi.[processUrl]
				FROM tbl_Message m
					INNER JOIN tbl_MessageTrash  mi ON mi.msgId=m.msgId
							AND mi.msgStatus='''+@v_msgStatusVc+'''
					INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
					INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
				WHERE m.[msgId]='''+cast(@v_msgidN as  varchar(10))+''') AS DATA
			where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
						AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
				--AND folderId= 0
				print '21 '+@v_TeampQueryVc
				exec(@v_TeampQueryVc)
			END
		END
		ELSE
		BEGIN
			set @v_TeampQueryVc=
			'DECLARE @v_Reccountf int
			DECLARE @v_TotalPagef int
			SELECT @v_Reccountf = Count(*) From (
			SELECT * From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId]
			FROM	tbl_Message m
					INNER JOIN tbl_MessageInBox  mi ON mi.msgId=m.msgId
                                                    AND mi.[folderId]='+cast(@v_FolderIdN as varchar(10))+'
                                                    AND mi.msgStatus='''+@v_msgStatusVc+'''
                                                    AND mi.msgStatus<>''trash''
					INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
					INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
			WHERE	msgToUserId='+cast(@v_UserIdN as varchar(10))+'
					AND m.[msgId]='''+cast(@v_msgidN as  varchar(10))+''') AS DATA) AS TTT

			SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
			SELECT *,@v_TotalPagef as TotalPages,cast(@v_Reccountf as int) as totalRecords From (SELECT ROW_NUMBER() OVER (order by '+@v_columnNameVc+' '+@v_orderTypeVc+') As Rownumber,m.[msgId],m.[subject]
					,m.[msgText],mi.[msgBoxId],mi.[msgToUserId],mi.[msgFromUserId]
					--,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail
					--,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
					,fromLogin.emailid as FromEmail
					,toLogin.emailid as ToEmail
					,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
					,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded],mi.[processUrl]
			FROM	tbl_Message m
					INNER JOIN tbl_MessageInBox  mi ON mi.msgId=m.msgId
                                                    AND mi.[folderId]='+cast(@v_FolderIdN as varchar(10))+'
                                                    AND mi.msgStatus='''+@v_msgStatusVc+'''
                                                    AND mi.msgStatus<>''trash''
					INNER JOIN tbl_LoginMaster fromLogin ON fromLogin.userid = mi.[msgFromUserId]
					INNER JOIN tbl_LoginMaster toLogin ON toLogin.userid = mi.[msgToUserId]
			WHERE msgToUserId='+cast(@v_UserIdN as varchar(10))+'
					AND m.[msgId]='''+cast(@v_msgidN as  varchar(10))+''') AS DATA
		where Rownumber between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1
					AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''

			print '22'+@v_TeampQueryVc
			exec(@v_TeampQueryVc)
		END	--When FolderId is not equal to 0 and search is made in MESSAGE
	END
END


GO
