SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: View Message Box.
--
--
-- Author: Rajesh Singh
-- Date: 08-10-2010
--
-- Last Modified:
-- Modified By:
-- Date 08-10-2010
-- Modification:
--------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[p_automatedsystemfiletransfer] 
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @v_activityId_inInt int, @v_objectId_inInt int, @v_childId_inInt int,@v_fromUserId_inInt int,@v_action_inVc varchar(50),@v_remarks_inVc varchar(250),@v_levelid_inInt int,@v_wftrigger_inVc varchar(15),@v_documentId_inVc varchar(100)
	DECLARE @v_fileID_inInt int
	
	DECLARE fileCursor CURSOR FOR  --Declaring Cursor for the all the records for today.
	SELECT wflowFileOnHandId FROM tbl_WorkFlowFileOnHand where CAST(FLOOR(CAST(endDate AS FLOAT))AS DATETIME)=CAST(FLOOR(CAST(GETDATE() AS FLOAT))AS DATETIME)
	
	OPEN fileCursor
	FETCH NEXT FROM fileCursor INTO @v_fileID_inInt
	print @v_fileID_inInt
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		IF ((SELECT [workflowDirection] FROM [tbl_WorkFlowFileOnHand] WHERE wflowFileOnHandId=@v_fileID_inInt)='Up')
			BEGIN
			
				--START--Updating Data with respect to UP direction   
				UPDATE [tbl_WorkFlowFileOnHand]
				SET [fromUserId] = (SELECT toUserId FROM tbl_WorkFlowFileOnHand WHERE wflowFileOnHandId=@v_fileID_inInt)
					  ,[toUserId] = (SELECT tbl_WorkFlowLevelConfig.userId
									FROM tbl_WorkFlowLevelConfig INNER JOIN
											tbl_WorkFlowFileOnHand ON tbl_WorkFlowLevelConfig.eventId = tbl_WorkFlowFileOnHand.eventId AND 
											tbl_WorkFlowLevelConfig.moduleId = tbl_WorkFlowFileOnHand.moduleId AND 
											tbl_WorkFlowLevelConfig.objectId = tbl_WorkFlowFileOnHand.objectId AND tbl_WorkFlowLevelConfig.childId = tbl_WorkFlowFileOnHand.childId AND 
											tbl_WorkFlowLevelConfig.activityId = tbl_WorkFlowFileOnHand.activityId 
									WHERE tbl_WorkFlowLevelConfig.wfLevel=(SELECT  (tbl_WorkFlowLevelConfig.wfLevel -1 )
									FROM  tbl_WorkFlowLevelConfig INNER JOIN tbl_WorkFlowFileOnHand ON tbl_WorkFlowLevelConfig.userId = tbl_WorkFlowFileOnHand.toUserId 
											WHERE wflowFileOnHandId=@v_fileID_inInt))
				WHERE wflowFileOnHandId=@v_fileID_inInt 
				--END--Updating Data with respect to UP direction
				
				--START--Calling SP "p_add_upd_wffile"   
				SELECT @v_activityId_inInt=tbl_WorkFlowFileOnHand.activityId, @v_objectId_inInt=tbl_WorkFlowFileOnHand.objectId, @v_childId_inInt=tbl_WorkFlowFileOnHand.childId, @v_fromUserId_inInt=tbl_WorkFlowFileOnHand.fromUserId, 
					   @v_action_inVc=tbl_WorkFlowFileOnHand.action, @v_remarks_inVc=tbl_WorkFlowFileOnHand.remarks, @v_levelid_inInt=tbl_WorkFlowLevelConfig.wfLevel, @v_wftrigger_inVc=tbl_WorkFlowFileOnHand.wfTrigger,@v_documentId_inVc=tbl_WorkFlowFileOnHand.documentId
				FROM  tbl_WorkFlowFileOnHand INNER JOIN
									  tbl_WorkFlowLevelConfig ON tbl_WorkFlowFileOnHand.eventId = tbl_WorkFlowLevelConfig.eventId AND 
									  tbl_WorkFlowFileOnHand.activityId = tbl_WorkFlowLevelConfig.activityId AND 
									  tbl_WorkFlowFileOnHand.moduleId = tbl_WorkFlowLevelConfig.moduleId AND 
									  tbl_WorkFlowFileOnHand.objectId = tbl_WorkFlowLevelConfig.objectId AND tbl_WorkFlowFileOnHand.childId = tbl_WorkFlowLevelConfig.childId WHERE tbl_WorkFlowFileOnHand.wflowFileOnHandId=@v_fileID_inInt

				EXEC [p_add_upd_wffile] @v_activityId_inInt,@v_objectId_inInt, @v_childId_inInt, @v_fromUserId_inInt, @v_action_inVc, @v_remarks_inVc,@v_levelid_inInt,@v_wftrigger_inVc,NULL,@v_documentId_inVc
				--END--Calling SP "p_add_upd_wffile"   
				
			END
		ELSE IF ((SELECT [workflowDirection] FROM [tbl_WorkFlowFileOnHand] WHERE wflowFileOnHandId=@v_fileID_inInt)='Down')
			BEGIN
					
				--END--Updating Data with respect to DOWN direction
				UPDATE [tbl_WorkFlowFileOnHand]
				SET [fromUserId] = (SELECT tbl_WorkFlowLevelConfig.userId
									FROM tbl_WorkFlowLevelConfig INNER JOIN
											tbl_WorkFlowFileOnHand ON tbl_WorkFlowLevelConfig.eventId = tbl_WorkFlowFileOnHand.eventId AND 
											tbl_WorkFlowLevelConfig.moduleId = tbl_WorkFlowFileOnHand.moduleId AND 
											tbl_WorkFlowLevelConfig.objectId = tbl_WorkFlowFileOnHand.objectId AND tbl_WorkFlowLevelConfig.childId = tbl_WorkFlowFileOnHand.childId AND 
											tbl_WorkFlowLevelConfig.activityId = tbl_WorkFlowFileOnHand.activityId 
									WHERE tbl_WorkFlowLevelConfig.wfLevel=(SELECT (tbl_WorkFlowLevelConfig.wfLevel + 1)
									FROM tbl_WorkFlowLevelConfig INNER JOIN tbl_WorkFlowFileOnHand ON tbl_WorkFlowLevelConfig.userId = tbl_WorkFlowFileOnHand.fromUserId
									WHERE (tbl_WorkFlowFileOnHand.wflowFileOnHandId = @v_fileID_inInt)) )
					,[toUserId] =(SELECT fromUserId FROM tbl_WorkFlowFileOnHand WHERE wflowFileOnHandId=@v_fileID_inInt) 
				WHERE wflowFileOnHandId=@v_fileID_inInt 
				--END--Updating Data with respect to DOWN direction
				
				--START--Calling SP "p_add_upd_wffile"   
				SELECT @v_activityId_inInt=tbl_WorkFlowFileOnHand.activityId, @v_objectId_inInt=tbl_WorkFlowFileOnHand.objectId, @v_childId_inInt=tbl_WorkFlowFileOnHand.childId, @v_fromUserId_inInt=tbl_WorkFlowFileOnHand.fromUserId, 
					   @v_action_inVc=tbl_WorkFlowFileOnHand.action, @v_remarks_inVc=tbl_WorkFlowFileOnHand.remarks, @v_levelid_inInt=tbl_WorkFlowLevelConfig.wfLevel, @v_wftrigger_inVc=tbl_WorkFlowFileOnHand.wfTrigger,@v_documentId_inVc=tbl_WorkFlowFileOnHand.documentId
				FROM  tbl_WorkFlowFileOnHand INNER JOIN
									  tbl_WorkFlowLevelConfig ON tbl_WorkFlowFileOnHand.eventId = tbl_WorkFlowLevelConfig.eventId AND 
									  tbl_WorkFlowFileOnHand.activityId = tbl_WorkFlowLevelConfig.activityId AND 
									  tbl_WorkFlowFileOnHand.moduleId = tbl_WorkFlowLevelConfig.moduleId AND 
									  tbl_WorkFlowFileOnHand.objectId = tbl_WorkFlowLevelConfig.objectId AND tbl_WorkFlowFileOnHand.childId = tbl_WorkFlowLevelConfig.childId WHERE tbl_WorkFlowFileOnHand.wflowFileOnHandId=@v_fileID_inInt

				EXEC [p_add_upd_wffile] @v_activityId_inInt,@v_objectId_inInt, @v_childId_inInt, @v_fromUserId_inInt, @v_action_inVc, @v_remarks_inVc,@v_levelid_inInt,@v_wftrigger_inVc,NULL,@v_documentId_inVc
				--END--Calling SP "p_add_upd_wffile"   
					
			END
		
		FETCH NEXT FROM fileCursor INTO @v_fileID_inInt
	END
	CLOSE fileCursor
	DEALLOCATE fileCursor
END


GO
