SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Inserts/Updates tables related Work Flow File.
--
--
-- Author: Karan
-- Date: 28-10-2010
--
-- Last Modified:
-- Modified By: Karan
-- Date 09-12-2010
-- Modification: Subject and Body Text changed for Forward file message.
--------------------------------------------------------------------------------

-- SP Name:    p_add_upd_wffile
-- Module: Work Flow
-- Function : 
-----------------------------------------------------------------------------
-- Insert: In work flow every files need to process in Work flow so this function does entry in related tables


CREATE PROCEDURE [dbo].[p_add_upd_wffile]

	-- Add the parameters for the stored procedure here
	@v_activityId_inInt int,
	@v_objectId_inInt int, 
	@v_childId_inInt int,
	@v_fromUserId_inInt int,
	@v_action_inVc varchar(50),
	@v_remarks_inVc varchar(max),
	@v_levelid_inInt int,
	@v_wftrigger_inVc varchar(15),
	@v_pubDt_inDt Date=NULL,
	@v_documentId_inVc varchar(100)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @v_eventId_Int int, @v_moduleId_Int int, @v_endDateDt smalldatetime, @v_toUserId_Int int, 
	@v_workFlowRoleId_Int int, @v_fileSentFrom_Vc varchar(100), @v_fileSentTo_Vc varchar(100), 
	@v_leveltype_Vc varchar(10), @v_fromUserLevelId_Int int,
	@v_ModuleName_Vc varchar(100), @v_EventName_Vc varchar(100), @v_ActivityName_Vc varchar(100),
	@v_wfflowFileOnHandId_Int int, @v_flag_bit bit, 
	@v_ToEmailId_Vc varchar(100), @v_FromEmailId_Vc varchar(100),@v_ProcurementRoleId_Vc varchar(100),
	@v_URL_Vc varchar(8000),@v_WorkflowEscDays int,@v_AppCode varchar(150)='APP',
	@evalCount int
	
	Declare @v_ToEmailId_inVc varchar(4000), -- To Email Id(s)
				@v_FromEmailId_inVc varchar(100), -- Sender Email Id
				@v_CCEmailId_inVc varchar(4000)=null, -- CC Eamil Id(s)
				@v_Subject_inVc varchar(150), -- Email Subject
				@v_MsgText_inVc varchar(4000), -- Email Content Text
				@v_ProcessUrl_inVc varchar(250)='',
				@v_IsPriority_inVc varchar(10), -- Priority
				@v_MsgBoxType_inVc varchar(50), -- Message Type. Eg; Inbox, Sent, or Draft				
				@v_MsgStatus_inVc varchar(10), -- Message Status. Eg; 
				@v_FolderId_inInt int, -- Folder Id
				@v_MsgReadStatus_inVc varchar(3), -- 
				@v_IsMsgReply_inVc varchar(5), -- 				
				@v_UpdMsgId_inInt int=null, -- Message Id(Primary Key) of Message (Update Case)
				@v_msgBoxTypeIdInt int=null, -- Message Inbox Id (Primary Key),
				---LOCAL VARIABLES
				@v_MsgIdInt int,
				@v_FromUserIdInt int,				
				@v_MsgDocId_inInt int,
				@v_isDocUploadedVc varchar(3),
				@v_UserIdInt int,
				@v_msgTypeIdInt int,
				@v_msgToCC_Vc varchar(10),
				@v_insDraft_Int int
	if @v_action_inVc ='Insert'
	Begin
	
    IF NOT EXISTS (Select worfklowId From tbl_WorkFlowLevelConfig  
					Where eventId=1 And activityId=7 
						And moduleId=1 
						And objectId=@v_objectId_inInt And childId=@v_childId_inInt)
				Begin
			 
					--	insert into  [tbl_WorkFlowLevelConfig]
					--	SELECT 
					--	   [eventId],[moduleId],[wfLevel],[userId],[wfRoleId],[objectId],GETDATE()
					--	  ,7,@v_childId_inInt,[fileOnHand],[procurementRoleId]
					--			 FROM [dbo].[tbl_WorkFlowLevelConfig]
					--				 Where eventId=1 And activityId=1 
					--						And moduleId=1 
					--						And objectId=@v_objectId_inInt And childId=@v_objectId_inInt
					--Set @v_flag_bit=1
					Select @v_flag_bit as flag, 'Record inserted.' as Message						
				End
    End
    Else
    begin
    Select @v_eventId_Int = eventid, @v_ActivityName_Vc=activityName from tbl_ActivityMaster where activityId = @v_activityId_inInt
    Select @v_moduleId_Int = moduleid, @v_EventName_Vc=eventName from tbl_EventMaster where eventId = @v_eventId_Int
    Select @v_toUserId_Int = userid, @v_workFlowRoleId_Int = wfRoleId from tbl_WorkFlowLevelConfig where eventId = @v_eventId_Int and moduleid = @v_moduleId_Int and wflevel = @v_levelid_inInt and objectId =@v_objectId_inInt
    Select @v_fromUserLevelId_Int = wfLevel from tbl_WorkFlowLevelConfig where eventId = @v_eventId_Int and moduleid = @v_moduleId_Int and userId = @v_fromUserId_inInt  and objectId =@v_objectId_inInt
    Select @v_fileSentFrom_Vc = p.employeeName + ' - '+(select designationname  from tbl_DesignationMaster where designationId = p.designationId) from tbl_EmployeeMaster p where userid = @v_fromUserId_inInt
    Select @v_fileSentTo_Vc = p.employeeName + ' - '+(select designationname  from tbl_DesignationMaster where designationId = p.designationId) from tbl_EmployeeMaster p where userid = @v_toUserId_Int
    Select @v_ModuleName_Vc = moduleName from tbl_ModuleMaster Where moduleId=@v_moduleId_Int    
    Select @v_leveltype_Vc = case when @v_levelid_inInt > @v_fromUserLevelId_Int then 'Up' else 'Down' end
    Select @v_ToEmailId_Vc=emailId  from dbo.tbl_LoginMaster where userId=@v_toUserId_Int
	Select @v_FromEmailId_Vc=emailId  from dbo.tbl_LoginMaster where userId=@v_fromUserId_inInt
    select @v_ProcurementRoleId_Vc=procurementRoleId   from tbl_WorkFlowLevelConfig where eventId = @v_eventId_Int and moduleid = @v_moduleId_Int and wflevel = @v_levelid_inInt and objectId =@v_objectId_inInt
    select @v_WorkflowEscDays= max(noOfDays) from tbl_WorkFlowEventConfig where eventId = @v_eventId_Int and objectId =@v_objectId_inInt
 if @v_ProcurementRoleId_Vc=20 or @v_fileSentTo_Vc is null or @v_fileSentFrom_Vc is null
 Begin
  Select @v_fileSentFrom_Vc = p.fullName + ' - '+  designation  from tbl_PartnerAdmin p where userid = @v_fromUserId_inInt
    Select @v_fileSentTo_Vc = p.fullName + ' - '+  designation    from tbl_PartnerAdmin p where userid = @v_toUserId_Int
  end
   
    --select @v_levelid_inInt as ToInd
    
    IF NOT EXISTS (Select wflowFileOnHandId From tbl_WorkFlowFileOnHand 
					Where eventId=@v_eventId_Int And activityId=@v_activityId_inInt And moduleId=@v_moduleId_Int 
						And objectId=@v_objectId_inInt And
						case when @v_activityId_inInt=4 OR @v_activityId_inInt=11 OR @v_activityId_inInt=1 OR @v_activityId_inInt=3 OR @v_activityId_inInt=10 then 
		case when childId = @v_childId_inInt then 1 else 0 end		
	else case when childId = @v_objectId_inInt then 1 else 0 end 
	end=1
						
						 )
	BEGIN
		BEGIN TRY
			BEGIN TRAN INS_TRAN	
			 print ('In NOT EXISTS')
				-- // INSERT RECORD CASE
				/* START CODE: TO INSERT INTO TABLE - tbl_WorkFlowFileOnHand */								
				INSERT INTO [tbl_WorkFlowFileOnHand]
						([eventId],[activityId],[moduleId],[objectId],[childId],[processDate],[endDate],[fromUserId],[toUserId],[action],[workflowDirection],[wfRoleId],[remarks],[wfTrigger],[fileSentFrom],[fileSentTo], [documentId])
				SELECT	@v_eventId_Int,@v_activityId_inInt, @v_moduleId_Int, @v_objectId_inInt, @v_childId_inInt,getdate(), dbo.f_get_businessdays(getdate(),@v_WorkflowEscDays), @v_fromUserId_inInt, @v_toUserId_Int, @v_action_inVc, 'Up', @v_workFlowRoleId_Int, @v_remarks_inVc, @v_wftrigger_inVc, @v_fileSentFrom_Vc, @v_fileSentTo_Vc, @v_documentId_inVc
				/* END CODE: TO INSERT INTO TABLE - tbl_WorkFlowFileOnHand */
				
				Set @v_wfflowFileOnHandId_Int = Ident_Current('dbo.[tbl_WorkFlowFileOnHand]') 
				--print ('In NOT EXISTS')
				/* START CODE : TO INSERT INTO HISTORY TABLE - tbl_WorkFlowFileHistory */	
				Insert into tbl_WorkFlowFileHistory
						(wfFileOnHandId, moduleName, eventName, activityName, activityId, 
						 childId, objectId, processDate, endDate, fromUserId, toUserId, 
						 fileSentFrom, fileSentTo, wfRoleid, fileOnHand, action, 
						 workflowDirection, remarks, wfTrigger, documentId)				 
				Select wflowFileOnHandId, @v_ModuleName_Vc, @v_EventName_Vc, @v_ActivityName_Vc, @v_activityId_inInt,
						@v_childId_inInt, @v_objectId_inInt, processDate, endDate, @v_fromUserId_inInt, @v_toUserId_Int,
						@v_fileSentFrom_Vc, @v_fileSentTo_Vc, @v_workFlowRoleId_Int, 'Yes', @v_action_inVc, 
						@v_leveltype_Vc, @v_remarks_inVc, wfTrigger, @v_documentId_inVc
				from tbl_WorkFlowFileOnHand 
				Where wflowFileOnHandId=@v_wfflowFileOnHandId_Int
				/* END CODE : TO INSERT INTO HISTORY TABLE - tbl_WorkFlowFileHistory */								
				--print ('In NOT EXISTS2')
				/* START CODE : TO UPDATE TABLE - tbl_WorkFlowLevelConfig */
				
				--print convert(varchar(10),@v_childId_inInt)+' '+convert(varchar(10),@v_objectId_inInt)+' '+convert(varchar(10),@v_activityId_inInt)+' '+convert(varchar(10),@v_eventId_Int)+' '+convert(varchar(10),@v_toUserId_Int)+' '+convert(varchar(10),@v_levelid_inInt)+('In NOT EXISTS3')
				
				if (@v_activityId_inInt =4 or  @v_activityId_inInt =11  or @v_activityId_inInt =1 )
				Begin				
					
					Update tbl_WorkFlowLevelConfig 
						Set actionDate=GETDATE(), fileOnHand='No'
						Where eventId=@v_eventId_Int And activityId=@v_activityId_inInt And moduleId=@v_moduleId_Int 
								And objectId=@v_objectId_inInt And childId=@v_objectId_inInt	
								
					Update tbl_WorkFlowLevelConfig 
						Set fileOnHand='Yes'
						Where Userid=@v_toUserId_Int And eventId=@v_eventId_Int And activityId=@v_activityId_inInt And moduleId=@v_moduleId_Int 
								And objectId=@v_objectId_inInt And childId=@v_objectId_inInt 
								And wfLevel=@v_levelid_inInt	
				End	
				Else
				Begin
				
					Update tbl_WorkFlowLevelConfig 
					Set actionDate=GETDATE(), fileOnHand='No'
					Where eventId=@v_eventId_Int And activityId=@v_activityId_inInt And moduleId=@v_moduleId_Int 
							And objectId=@v_objectId_inInt And childId=@v_childId_inInt	
							
					Update tbl_WorkFlowLevelConfig 
						Set fileOnHand='Yes'
						Where Userid=@v_toUserId_Int And eventId=@v_eventId_Int And activityId=@v_activityId_inInt And moduleId=@v_moduleId_Int 
								And objectId=@v_objectId_inInt And childId=@v_childId_inInt 
								And wfLevel=@v_levelid_inInt	
				End				
				/* END CODE : TO UPDATE TABLE - tbl_WorkFlowLevelConfig */	
				
				-- // 
				--print ('In NOT EXISTS4')
				/* START CODE: TO INSERT INTO MAIL MESSAGE TABLES */
				/* START : SET VALUES */	
				
				if (@v_activityId_inInt =4  or @v_activityId_inInt =1 or @v_activityId_inInt =11 )
				Begin
					Select @v_URL_Vc ='<br><a style="text-decoration:underline;" href="./../../officer/FileProcessing.jsp?objectid='+ CONVERT(varchar(50), @v_objectId_inInt)  + '&childid='+CONVERT(varchar(50), @v_objectId_inInt) +'&eventid='+CONVERT(varchar(50), @v_eventId_Int) +'&activityid='+CONVERT(varchar(50), @v_activityId_inInt) +'&fromaction=pending">Click here</a>'
				End
				Else
				Begin
					Select @v_URL_Vc ='<br><a style="text-decoration:underline;" href="./../../officer/FileProcessing.jsp?objectid='+ CONVERT(varchar(50), @v_objectId_inInt)  + '&childid='+CONVERT(varchar(50), @v_childId_inInt) +'&eventid='+CONVERT(varchar(50), @v_eventId_Int) +'&activityid='+CONVERT(varchar(50), @v_activityId_inInt) +'&fromaction=pending">Click here</a>'
				End
				
				 --SELECT @v_ToEmailId_inVc = @v_ToEmailId_Vc, 
					--@v_FromEmailId_inVc = @v_FromEmailId_Vc, 
					--@v_Subject_inVc ='File to be processed in Workflow',
					--@v_MsgText_inVc ='<p>   Dear User,</p>  <p>   &nbsp;</p>  <p>   A file has come to you for processing. Details of the file to be processed are as mentioned below:</p>  <p>   &nbsp;</p>  <p>   Module Name: '+@v_ModuleName_Vc+'</p>  <p>   Event: '+@v_EventName_Vc+'</p>  <p>   Id : '+convert(varchar(20),@v_objectId_inInt)+'</p>  <p>   &nbsp;</p>  <p>   File Sent From:- '+@v_FromEmailId_Vc+'</p>  <p>   Official Name - Designation: '+@v_fileSentFrom_Vc+'<p>   &nbsp;</p> <p> Perform below mentioned steps to process the same: </p>  <p>1. Login to eGP website</p> <p>2. Click on workflow menu</p>  <p>3. Select Pending task</p> <p>4. Then at last under action tab click on Process link</p> <p>   &nbsp;</p>  <p>   Thanks,</p>  <p>   <strong>e-GP System.</strong></p> ',					
					--@v_IsPriority_inVc ='High',
					--@v_MsgBoxType_inVc ='Inbox',
					--@v_Action_inVc ='Create',
					--@v_MsgStatus_inVc ='Live',
					--@v_FolderId_inInt = 0,
					--@v_MsgReadStatus_inVc ='No',
					--@v_IsMsgReply_inVc ='No',
					--@v_isDocUploadedVc = 'No',									 
					--@v_FromUserIdInt = @v_fromUserId_inInt, 
					--@v_UserIdInt=@v_toUserId_Int, 
					--@v_msgToCC_Vc='To' 
					if @v_activityId_inInt =1
					begin
					select @v_AppCode=appcode from tbl_AppMaster where appId=@v_objectId_inInt
					set @v_MsgText_inVc ='<p> Dear User,</p><br><p>A file has come to you for processing. Detail of the file to be processed is as mentioned below:<br><br></p><table style="width: 100%"><tr><td style="width: 25%"><strong>Module Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_ModuleName_Vc+'<td><tr><tr><td style="width: 25%"><strong>Process</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_EventName_Vc+'<td><tr><tr><td style="width: 25%"><strong>APP Code</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+convert(varchar(150),@v_AppCode)+'<td><tr><tr><td style="width: 25%"><strong>File Sent By</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_fileSentFrom_Vc+'<td><tr></table><p> <strong>'+@v_URL_Vc+'</strong> to process this file now. Or you can perform below mentioned steps to process this file later on:<br><br></p>  <p>1. Click on <strong>Workflow</strong> menu<br>2. Select <strong>Pending</strong> task<br>3. Click on <strong>Process</strong> link available in front of a particular file to be processed</p><br><p>Regards,<br><strong>e-GP Help Desk.</strong></p> '					
					end
					else
					begin
					set @v_MsgText_inVc ='<p> Dear User,</p><br><p>A file has come to you for processing. Detail of the file to be processed is as mentioned below:<br><br></p><table style="width: 100%"><tr><td style="width: 25%"><strong>Module Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_ModuleName_Vc+'<td><tr><tr><td style="width: 25%"><strong>Process</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_EventName_Vc+'<td><tr><tr><td style="width: 25%"><strong>ID</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+convert(varchar(20),@v_objectId_inInt)+'<td><tr><tr><td style="width: 25%"><strong>File Sent By</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_fileSentFrom_Vc+'<td><tr></table><p> <strong>'+@v_URL_Vc+'</strong> to process this file now. Or you can perform below mentioned steps to process this file later on:<br><br></p>  <p>1. Click on <strong>Workflow</strong> menu<br>2. Select <strong>Pending</strong> task<br>3. Click on <strong>Process</strong> link available in front of a particular file to be processed</p><br><p>Regards,<br><strong>e-GP Help Desk.</strong></p> '					
					end
						 SELECT @v_ToEmailId_inVc = @v_ToEmailId_Vc, 
					@v_FromEmailId_inVc = @v_FromEmailId_Vc,
					@v_Subject_inVc ='File to be processed in Workflow',					
					@v_MsgText_inVc =@v_MsgText_inVc,
					@v_IsPriority_inVc ='High',
					@v_MsgBoxType_inVc ='Inbox',
					@v_Action_inVc ='Create',
					@v_MsgStatus_inVc ='Live',
					@v_FolderId_inInt = 0,
					@v_MsgReadStatus_inVc ='No',
					@v_IsMsgReply_inVc ='No',
					@v_isDocUploadedVc = 'No',									 
					@v_FromUserIdInt = @v_fromUserId_inInt, 
					@v_UserIdInt=@v_toUserId_Int, 
					@v_msgToCC_Vc='To' 
					
					If @v_activityId_inInt=1
					Begin
						select @v_Subject_inVc ='APP Code: '+CONVERT(varchar(150), @v_AppCode)+ ' '+@v_Subject_inVc
					End
					Else
					Begin
						select @v_Subject_inVc ='Tender ID : '+CONVERT(varchar(50), @v_objectId_inInt)+' '+@v_Subject_inVc
					End
										
				/* END : SET VALUES */	 			
	 
				--/* START CODE : TO INSERT INTO TABLE - tbl_Message */
				--INSERT INTO tbl_Message (subject, msgText) VALUES (@v_Subject_inVc, @v_MsgText_inVc)				
				/* END CODE : TO INSERT INTO TABLE - tbl_Message */
				
				--Set @v_MsgIdInt=Ident_Current('dbo.tbl_Message') 
					
				/* START CODE : TO INSERT INTO TABLE - tbl_MessageInBox */	
				--Insert Into tbl_MessageInBox
				--		(msgToUserId, msgFromUserId, msgId, msgToCCReply, msgStatus, creationDate, 
				--		folderId, isPriority, isMsgRead, isDocUploaded, processUrl )
				--Select @v_UserIdInt , @v_FromUserIdInt, @v_MsgIdInt, @v_msgToCC_Vc, @v_MsgStatus_inVc, GETDATE(), 
				--		@v_FolderId_inInt, @v_IsPriority_inVc, @v_MsgReadStatus_inVc, @v_isDocUploadedVc, @v_ProcessUrl_inVc 
				/* END CODE : TO INSERT INTO TABLE - tbl_MessageInBox */
			
				/* START CODE : TO INSERT INTO TABLE - Tbl_MessageSent */	
				--Insert Into tbl_MessageSent
				--		(msgToUserId, msgFromUserId, msgId, msgToCCReply, msgStatus, creationDate, 
				--		folderId, isPriority, isMsgRead, isDocUploaded  )
				--Select @v_UserIdInt , @v_FromUserIdInt, @v_MsgIdInt, @v_msgToCC_Vc, @v_MsgStatus_inVc, GETDATE(), 
				--		@v_FolderId_inInt, @v_IsPriority_inVc, @v_MsgReadStatus_inVc, @v_isDocUploadedVc  
				/* END CODE : TO INSERT INTO TABLE - Tbl_MessageSent */
				
				/* END CODE: TO INSERT INTO MAIL MESSAGE TABLES */
				
				Set @v_flag_bit=1
				Select @v_flag_bit as flag, 'Record inserted. dd' as Message
			COMMIT TRAN INS_TRAN	
		END TRY 
		BEGIN CATCH
			BEGIN
				Set @v_flag_bit=0
				Select @v_flag_bit as flag, 'Record not inserted. ' + ERROR_MESSAGE() as Message					
				ROLLBACK TRAN INS_TRAN
			END
		END CATCH	
    END
    ELSE 
    BEGIN
    DECLARE @UPDATE_ACTION VARCHAR(30);
    SET @UPDATE_ACTION = @v_action_inVc
    IF (@v_action_inVc='Rejected / Re-Tendering')
    BEGIN
    SET @UPDATE_ACTION = 'Reject'
    END
		BEGIN TRY
			BEGIN TRAN UPD_TRAN	
			--print ('In EXISTS')
				-- UPDATE RECORD CASE
				/* START CODE : TO UPDATE TABLE - tbl_WorkFlowFileOnHand */
				
				
				UPDATE tbl_WorkFlowFileOnHand
					SET [eventId]=@v_eventId_Int,
						[activityId]=@v_activityId_inInt,
						[moduleId]=@v_moduleId_Int,
						[objectId]=@v_objectId_inInt,
						[childId]=@v_childId_inInt,
						[processDate]=getdate(),
						[endDate]=dbo.f_get_businessdays(getdate(),@v_WorkflowEscDays),
						[fromUserId]=@v_fromUserId_inInt,
						[toUserId]=@v_toUserId_Int,
						--[action]=@v_action_inVc,
						[action]=@UPDATE_ACTION,
						[workflowDirection]=@v_leveltype_Vc,
						[wfRoleId]=@v_workFlowRoleId_Int,
						[remarks]=@v_remarks_inVc,
						[wfTrigger]=@v_wftrigger_inVc,
						[fileSentFrom]=@v_fileSentFrom_Vc,
						[fileSentTo]=@v_fileSentTo_Vc,
						[documentId]=@v_documentId_inVc
				Where eventId=@v_eventId_Int And activityId=@v_activityId_inInt And moduleId=@v_moduleId_Int 
				And objectId=@v_objectId_inInt And
				case when @v_activityId_inInt=4  OR @v_activityId_inInt=1 OR @v_activityId_inInt=10 OR @v_activityId_inInt=11 then 
		case when childId = @v_childId_inInt then 1 else 0 end		
	else case when childId = @v_objectId_inInt then 1 else 0 end 
	end=1
				  	
				/* END CODE : TO UPDATE TABLE - tbl_WorkFlowFileOnHand */
								
				/* START CODE : TO INSERT INTO HISTORY TABLE - tbl_WorkFlowFileHistory */					
				Insert into tbl_WorkFlowFileHistory
						(wfFileOnHandId, moduleName, eventName, activityName, activityId, 
						 childId, objectId, processDate, endDate, fromUserId, toUserId, 
						 fileSentFrom, fileSentTo, wfRoleid, fileOnHand, action, 
						 workflowDirection, remarks, wfTrigger, documentId)				 
				Select wflowFileOnHandId, @v_ModuleName_Vc, @v_EventName_Vc, @v_ActivityName_Vc, @v_activityId_inInt,
						@v_childId_inInt, @v_objectId_inInt, processDate, endDate, @v_fromUserId_inInt, @v_toUserId_Int,
						@v_fileSentFrom_Vc, @v_fileSentTo_Vc, @v_workFlowRoleId_Int, 'Yes', --@v_action_inVc, 
						@UPDATE_ACTION,
						@v_leveltype_Vc, @v_remarks_inVc, wfTrigger, @v_documentId_inVc
				from tbl_WorkFlowFileOnHand 
				Where eventId=@v_eventId_Int And activityId=@v_activityId_inInt And moduleId=@v_moduleId_Int 
				And objectId=@v_objectId_inInt And childId=@v_childId_inInt	
				/* END CODE : TO INSERT INTO HISTORY TABLE - tbl_WorkFlowFileHistory */			
				
				/* START CODE : TO UPDATE TABLE - tbl_WorkFlowLevelConfig */
				
				if (@v_activityId_inInt =4 or @v_activityId_inInt =1  or @v_activityId_inInt =11)
				Begin
				
					Update tbl_WorkFlowLevelConfig 
						Set actionDate=GETDATE(), fileOnHand='No'
						Where eventId=@v_eventId_Int And activityId=@v_activityId_inInt And moduleId=@v_moduleId_Int 
								And objectId=@v_objectId_inInt And childId=@v_objectId_inInt	
								
					Update tbl_WorkFlowLevelConfig 
						Set fileOnHand='Yes'
						Where Userid=@v_toUserId_Int And eventId=@v_eventId_Int And activityId=@v_activityId_inInt And moduleId=@v_moduleId_Int 
								And objectId=@v_objectId_inInt And childId=@v_objectId_inInt 
								And wfLevel=@v_levelid_inInt	
				End	
				Else
				Begin
				
					Update tbl_WorkFlowLevelConfig 
					Set actionDate=GETDATE(), fileOnHand='No'
					Where eventId=@v_eventId_Int And activityId=@v_activityId_inInt And moduleId=@v_moduleId_Int 
							And objectId=@v_objectId_inInt And childId=@v_childId_inInt	
							
					Update tbl_WorkFlowLevelConfig 
						Set fileOnHand='Yes'
						Where Userid=@v_toUserId_Int And eventId=@v_eventId_Int And activityId=@v_activityId_inInt And moduleId=@v_moduleId_Int 
								And objectId=@v_objectId_inInt And childId=@v_childId_inInt 
								And wfLevel=@v_levelid_inInt	
				End		
				 
				/* END CODE : TO TO UPDATE TABLE - tbl_WorkFlowLevelConfig */
				 
				if (@v_action_inVc = 'Approve' or @v_action_inVc = 'Conditional Approval' or @v_action_inVc = 'reject' or @v_action_inVc = 'Rejected / Re-Tendering' or @v_action_inVc = 'Re-evaluation')		
				Begin
					delete from tbl_WorkFlowFileOnHand 	Where eventId=@v_eventId_Int And activityId=@v_activityId_inInt And moduleId=@v_moduleId_Int And objectId=@v_objectId_inInt And childId=@v_childId_inInt	 
					if @v_activityId_inInt =1 
					begin
						if @v_action_inVc='Approve'
						Begin
						set @v_action_inVc='Approved'
						End
						if @v_action_inVc='reject'
						Begin
						set @v_action_inVc='Rejected'
						End
						if (@v_objectId_inInt = @v_childId_inInt) 
							begin
								update tbl_AppPackages set workflowStatus=@v_action_inVc where appId=@v_objectId_inInt and workflowStatus in('Pending','Rejected')
							end
					    else
							begin
								update tbl_AppPackages set workflowStatus=@v_action_inVc where packageId=@v_childId_inInt and workflowStatus in('Pending','Rejected')
							end

					end
					if @v_activityId_inInt =2
					begin
						if @v_action_inVc='Approve'
						Begin
						set @v_action_inVc='Approved'
						End
						if @v_action_inVc='reject'
						Begin
						set @v_action_inVc='Rejected'
						End
						update tbl_TenderDetails set workflowStatus=@v_action_inVc where tenderId=@v_objectId_inInt
					end
					if @v_activityId_inInt =7 
					begin
						if @v_action_inVc='Approve'
						Begin
						set @v_action_inVc='Approved'
						End
						if @v_action_inVc='reject'
						Begin
						set @v_action_inVc='Rejected'
						End
						update tbl_AppPackages set workflowStatus=@v_action_inVc where packageId=@v_childId_inInt
					end
					if @v_activityId_inInt =5
					begin
						if @v_action_inVc='Approve'
						Begin
						set @v_action_inVc='Approved'
						End
						if @v_action_inVc='reject'
						Begin
						set @v_action_inVc='Rejected'
						End
						update tbl_Committee set workflowStatus=@v_action_inVc where tenderId=@v_childId_inInt and committeeType in('TOC','POC')
					end
					if @v_activityId_inInt =4
					begin
						if @v_action_inVc='Approve'
						Begin
						set @v_action_inVc='Approved'
						End
						if @v_action_inVc='reject'
						Begin
						set @v_action_inVc='Rejected'
						End
						update tbl_CorrigendumMaster set workflowStatus=@v_action_inVc where corrigendumId=@v_childId_inInt
					end
					if @v_activityId_inInt =3
					begin
						if @v_action_inVc='Approve'
						Begin
						set @v_action_inVc='Approved'
						End
						if @v_action_inVc='reject'
						Begin
						set @v_action_inVc='Rejected'
						End
						update tbl_PreTenderMetDocs set workflowStatus=@v_action_inVc where tenderId=@v_childId_inInt
					end
					if @v_activityId_inInt =6
					begin
						if @v_action_inVc='Approve'
						Begin
						set @v_action_inVc='Approved'
						End
						if @v_action_inVc='reject'
						Begin
						set @v_action_inVc='Rejected'
						End
						update tbl_Committee set workflowStatus=@v_action_inVc where tenderId=@v_childId_inInt and committeeType in('TEC','PEC')
					end
					if @v_activityId_inInt =8
					begin
						if @v_action_inVc='Approve'
						Begin
						set @v_action_inVc='Approved'
						End
						if @v_action_inVc='reject'
						Begin
						set @v_action_inVc='Rejected'
						End
						update tbl_Committee set workflowStatus=@v_action_inVc where tenderId=@v_childId_inInt and committeeType in('TSC')
					end
					if @v_activityId_inInt =9
					begin
						if @v_action_inVc='Approve'
						Begin
						set @v_action_inVc='Cancelled'
						update tbl_CancelTenderRequest set workflowStatus=@v_action_inVc,tenderStatus='Cancelled' where tenderId=@v_childId_inInt 
						update tbl_TenderDetails set workflowStatus=@v_action_inVc,tenderStatus='Cancelled' where tenderId=@v_childId_inInt 
						End
						if @v_action_inVc='reject'
						Begin
						set @v_action_inVc='Rejected'
						DELETE FROM tbl_CancelTenderRequest where tenderId=@v_childId_inInt 
						End
					end
					if @v_activityId_inInt =10
					begin
						if @v_action_inVc='Approve'
						Begin
						set @v_action_inVc='Approved'
						End
						if @v_action_inVc='reject'
						Begin
						set @v_action_inVc='Rejected'
						End
						update tbl_CMS_ContractTermination set workflowStatus=@v_action_inVc where contractTerminationId=@v_childId_inInt
					end
					if @v_activityId_inInt =11
					begin
						if @v_action_inVc='Approve'
						Begin
						set @v_action_inVc='Approved'
						End
						if @v_action_inVc='reject'
						Begin
						set @v_action_inVc='Rejected'
						End
						update tbl_CMS_VariationOrder set variOrdWFStatus=@v_action_inVc where variOrdId=@v_childId_inInt
					end
					if @v_activityId_inInt =12
					begin
						if @v_action_inVc='Approve'
						Begin
						set @v_action_inVc='Approved'
						End
						if @v_action_inVc='reject'
						Begin
						set @v_action_inVc='Rejected'
						End
						
						if @v_action_inVc='Rejected'
							Begin
								update tbl_CMS_ROMaster set rptOrderWFStatus=@v_action_inVc, isCurrent = 'no' where romId=@v_childId_inInt
							End
						Else
							Begin
								update tbl_CMS_ROMaster set rptOrderWFStatus=@v_action_inVc where romId=@v_childId_inInt
							End
					end
					/* Dohatec Start*/
					if @v_activityId_inInt =13
					begin
								DECLARE		@procurementMethod AS VARCHAR(50) ,@eventType AS VARCHAR(50),
											@is2Env as INT,@procurementNature AS varchar(10),
											@isCase2 AS INT,@isCase3 AS INT,@RptID as INT,@roundID as INT,
											@ReportID AS INT,@isCRSaved AS INT,@envelopeId AS INT, 
											@evalRptId AS INT,@AA_userId AS INT, @AA_sentBy AS INT ,
											@AA_TenderID AS INT, @AA_sentByGovUserId AS VARCHAR(MAX),
											@AA_sentRole AS VARCHAR(MAX), @AA_pkglotID AS INT, 
											@rptStatus AS VARCHAR(MAX)

								SELECT		@evalRptId = 0 , @AA_sentRole = 'AA' ,
											@rptStatus = @v_action_inVc


								SET  @is2Env = 0
								SET @isCase2 = 0
								SET @isCase3 = 0
								SET @envelopeId = 0
								/* SET @AA_pkglotID = @v_childId_inInt */

								SELECT @AA_pkglotID = min(appPkgLotId)
										FROM  dbo.tbl_TenderLotSecurity
										WHERE tenderId = @v_objectId_inInt
									
								SELECT @AA_sentBy = userId,@AA_TenderID = objectId, 
								@AA_sentByGovUserId =govUserId
								FROM tbl_WorkFlowLevelConfig WHERE objectId = @v_objectId_inInt AND 
								eventId = @v_eventId_Int and wfLevel = @v_fromUserLevelId_Int

								select @procurementMethod = ttd.procurementMethod, @eventType = ttd.eventType ,
								@procurementNature = ttd.procurementNature
								from Tbl_TenderDetails ttd 
								where ttd.tenderId=@v_objectId_inInt
								
								IF 	@procurementNature = 'Services'
								BEGIN
								SET @AA_pkglotID = 0
								END
								
								SELECT @roundID = ISNULL(MAX(roundId),0) FROM 
								tbl_EvalRoundMaster WHERE tenderId = @v_objectId_inInt and pkgLotId = @AA_pkglotID
                                AND (reportType = 'L1')

								select	@is2Env = noOfEnvelops
									from	tbl_ConfigEvalMethod cm ,
											tbl_TenderDetails td, 
											tbl_TenderTypes tt 
									where	cm.procurementMethodId=td.procurementMethodId 
											and cm.tenderTypeId=tt.tenderTypeId 
											and tt.tenderType=td.eventType 
											and cm.procurementNatureId=td.procurementNatureId 
											and tenderId=@v_objectId_inInt 
											
								IF 	@procurementNature = 'Services'
								BEGIN
									IF (	(@eventType = 'RFA' AND 
												(	
													@procurementMethod = 'DC' OR 
													@procurementMethod = 'SBCQ' OR 
													@procurementMethod = 'SSS' OR 
													@procurementMethod = 'IC' OR 
													@procurementMethod = 'CSO'
												)
											)         
											OR
											(
												@eventType = 'RFP' AND
												(
												@procurementMethod = 'QCBS' OR
												@procurementMethod = 'LCS' OR
												@procurementMethod = 'SFB' OR
												@procurementMethod = 'DC' OR
												@procurementMethod = 'SBCQ' OR
												@procurementMethod = 'CSO' OR
													(
														@procurementMethod = 'DC' OR
														@procurementMethod = 'SBCQ' OR
														@procurementMethod = 'SSS' OR
														@procurementMethod = 'SBCQ' OR
														@procurementMethod = 'IC' OR
														@procurementMethod = 'SBCQ' OR
														@procurementMethod = 'CSO'
														
													)
												)
											)
										)
									BEGIN
										SET @isCase3 = 1
										IF @is2Env = 2 
										BEGIN
										SET @isCase2 = 1
										SET @isCase3 = 0
										END
									END
									 SELECT @RptID = FieldValue1 FROM (select DISTINCT case when 
											COUNT(rm.reportId)!=0 then '1' else '0' end as FieldValue1
											from tbl_ReportMaster rm inner join tbl_ReportTableMaster rtm 
											on rm.reportId = rtm.reportId inner join tbl_ReportFormulaMaster rfm 
											on rfm.reportTableId = rtm.reportTableId
											where tenderId=@v_objectId_inInt and isTORTER='TER')T
											
										IF @RptID = 1 
										BEGIN
											select @ReportID = CONVERT(varchar(50),rm.reportId) ,
											@isCRSaved = (case when((select COUNT(bidderRankId) 
											from tbl_BidderRank br where 
											br.tenderId=@v_objectId_inInt and br.reportId=rm.reportId 
											and roundId=@roundID)=0)
											then '0' else '1' end )
											from tbl_ReportMaster rm 
											where rm.tenderId=@v_objectId_inInt and rm.isTORTER='TER'		

										END	

								END
									ELSE --goods works start from here
									BEGIN
										IF(@procurementMethod = 'OSTETM' AND @is2Env =2)
										BEGIN
										SET @isCase2 = 1
										END

										IF(@eventType = '2 stage-TSTM' OR (@procurementMethod = 'OTM' AND 	
										@eventType != 'PQ') OR @procurementMethod = 'LTM'
										OR @procurementMethod = 'RFQ' OR @procurementMethod = 'RFQU'
										OR @procurementMethod = 'RFQL' OR @procurementMethod = 'DPM'
										)
										BEGIN
										SET @isCase3 = 1
										END
										SELECT @RptID = FieldValue1 FROM (select case when 
													COUNT(rm.reportId)!=0 then '1' else '0' end as FieldValue1
													from tbl_ReportMaster rm inner join tbl_ReportTableMaster rtm 
													on rm.reportId = rtm.reportId inner join 
													tbl_ReportFormulaMaster rfm on rfm.reportTableId = rtm.reportTableId
													inner join tbl_ReportLots rl on rl.reportId = rm.reportId
													where tenderId=@v_objectId_inInt and isTORTER='TER' and rl.pkgLotId=@AA_pkglotID)T

										IF @RptID = 1 
										BEGIN
											select @ReportID = (CONVERT(varchar(50),rm.reportId) ), 
											@isCRSaved = (case when((select COUNT(bidderRankId) 
											from tbl_BidderRank br 
											where br.tenderId=@v_objectId_inInt and 
											br.pkgLotId=@AA_pkglotID and br.reportId=rm.reportId  and 
											roundId=@roundID)=0)
											then '0' else '1' end )
											from tbl_ReportMaster rm,tbl_ReportLots rl 
											where rm.tenderId= @v_objectId_inInt  
											and rm.reportId = rl.reportId 
											and rl.pkgLotId= @AA_pkglotID 
											and rm.isTORTER='TER'	 
										END
									END	

								IF (@isCase2 = 1 OR @isCase3 = 1)
								BEGIN
									IF (@isCRSaved = 1)
									BEGIN
									SET @envelopeId = 1
									END
									ELSE
									BEGIN
									SET @envelopeId = 0
									END
								END
								
								SET @rptStatus =@v_action_inVc
								if @v_action_inVc='Approve'
								Begin
								SET @rptStatus ='Approved'
								End
								if @v_action_inVc='Reject'
								Begin
								SET @rptStatus ='Rejected'
								End
								if @v_action_inVc='Re-evaluation'
								Begin
								SET @rptStatus ='Re-evaluation'
								End
				--changes for re-evaluation				
				if(@rptStatus <> 'Re-evaluation')
						begin
						set @evalCount = 0
						end
								
				else
						begin
						select @evalCount = (Case when (select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @AA_TenderID)=0
						then  1
						else (select max(evalCount)+1 from tbl_EvalRptSentToAA where tenderId = @AA_TenderID)
						end)
						--delete from tbl_TORRptSign where tenderId = @AA_TenderID
						delete from tbl_EvalCpMemClarification where evalMemStatusId in (select evalMemStatusId from tbl_EvalMemStatus where tenderId = @AA_TenderID)
						--delete from tbl_EvalMemStatus where tenderId = @AA_TenderID
						delete from tbl_EvalSentQueToCp where tenderId = @AA_TenderID	
						delete from tbl_EvalBidderResp where tenderId = @AA_TenderID
						insert into tbl_EvalBidderStatus  select tenderId, userId, bidderStatus,evalBy,evalDt,bidderMarks,evalStatus,passingMarks,remarks,result,pkgLotId,evalCount+1 from tbl_EvalBidderStatus where tenderId = @AA_TenderID and evalCount = (select max(evalcount) from tbl_EvalBidderStatus where tenderid=@AA_TenderID)
						
						end		
				if @procurementNature = 'Services'	
					BEGIN
						if not exists (
										SELECT evalRptToAAId FROM tbl_EvalRptSentToAA 
										WHERE tenderId = @AA_TenderID AND roundId = @roundID and evalCount = @evalCount
									   )
										BEGIN
											INSERT INTO tbl_EvalRptSentToAA
											([evalRptId],[userId],[sentBy],[tenderId]
												,[remarks],[sentDate],[aaGovUserId]
												,[sentByGovUserId],[sentRole],[pkgLotId]
												,[rptStatus],[aaRemarks],[envelopeId]
												,[roundId],[rptApproveDt],[evalCount])
											VALUES
											(
												@evalRptId,@AA_sentBy,@AA_sentBy,@AA_TenderID,
												@rptStatus + ' By WorkFlow',GETDATE(),
												@AA_sentByGovUserId,@AA_sentByGovUserId,
												@AA_sentRole,@AA_pkglotID,@rptStatus,
												@v_remarks_inVc,@envelopeId,@roundID,
												GETDATE(),@evalCount
											)	
										END
										ELSE
										BEGIN
											UPDATE tbl_EvalRptSentToAA SET rptStatus = @rptStatus,
											aaRemarks=@v_remarks_inVc,rptApproveDt= GETDATE()
											WHERE tenderId = @AA_TenderID AND roundId = @roundID
										END
					END
				ELSE
					BEGIN
					if(@procurementMethod = 'OSTETM')
					BEGIN
						if not exists (
										SELECT evalRptToAAId FROM tbl_EvalRptSentToAA 
										WHERE tenderId = @AA_TenderID AND roundId = @roundID and evalCount = @evalCount
									   )
										BEGIN
											INSERT INTO tbl_EvalRptSentToAA
											([evalRptId],[userId],[sentBy],[tenderId]
												,[remarks],[sentDate],[aaGovUserId]
												,[sentByGovUserId],[sentRole],[pkgLotId]
												,[rptStatus],[aaRemarks],[envelopeId]
												,[roundId],[rptApproveDt],[evalCount])
											VALUES
											(
												@evalRptId,@AA_sentBy,@AA_sentBy,@AA_TenderID,
												@rptStatus + ' By WorkFlow',GETDATE(),
												@AA_sentByGovUserId,@AA_sentByGovUserId,
												@AA_sentRole,@AA_pkglotID,@rptStatus,
												@v_remarks_inVc,@envelopeId,@roundID,
												GETDATE(),@evalCount
											)	
										END
										ELSE
										BEGIN
											UPDATE tbl_EvalRptSentToAA SET rptStatus = @rptStatus,
											aaRemarks=@v_remarks_inVc,rptApproveDt= GETDATE()
											WHERE tenderId = @AA_TenderID AND roundId = @roundID and evalCount = @evalCount
										END	
					END
					else
						BEGIN
						if not exists (
										SELECT evalRptToAAId FROM tbl_EvalRptSentToAA 
										WHERE tenderId = @AA_TenderID AND roundId = @roundID
									   )
										BEGIN
											DECLARE Tender_Lot CURSOR FOR SELECT appPkgLotId FROM  dbo.tbl_TenderLotSecurity WHERE tenderId = @v_objectId_inInt and appPkgLotId in (select pkgLotId from tbl_EvalRptSentTC where rptStatus = 'sentforloi')
											OPEN Tender_Lot
											FETCH NEXT FROM Tender_Lot INTO @AA_pkglotID
											WHILE @@FETCH_STATUS = 0
											BEGIN
												Select @roundID = roundId from tbl_EvalRoundMaster where tenderId = @AA_TenderID and pkgLotId = @AA_pkglotID
												INSERT INTO tbl_EvalRptSentToAA
												([evalRptId],[userId],[sentBy],[tenderId]
													,[remarks],[sentDate],[aaGovUserId]
													,[sentByGovUserId],[sentRole],[pkgLotId]
													,[rptStatus],[aaRemarks],[envelopeId]
													,[roundId],[rptApproveDt],[evalCount])
												VALUES
												(
													@evalRptId,@AA_sentBy,@AA_sentBy,@AA_TenderID,
													@rptStatus + ' By WorkFlow',GETDATE(),
													@AA_sentByGovUserId,@AA_sentByGovUserId,
													@AA_sentRole,@AA_pkglotID,@rptStatus,
													@v_remarks_inVc,@envelopeId,@roundID,
													GETDATE(),@evalCount
												)	

												FETCH NEXT FROM Tender_Lot INTO @AA_pkglotID
											END 
											CLOSE Tender_Lot
											DEALLOCATE Tender_Lot
											
										END
										ELSE
										BEGIN
											UPDATE tbl_EvalRptSentToAA SET rptStatus = @rptStatus,
											aaRemarks=@v_remarks_inVc,rptApproveDt= GETDATE()
											WHERE tenderId = @AA_TenderID AND roundId = @roundID
										END	
								END						
							END
						end
					/* Dohatec END*/
				End				
				
				if @v_workFlowRoleId_Int=2
				begin
					if @v_activityId_inInt=1
					begin
					set @v_ActivityName_Vc='APP'
					end
				set @v_Subject_inVc=@v_ActivityName_Vc+' Approval is required'
				end
				else
				begin
				set @v_Subject_inVc = 'File to be processed in Workflow'
				end
				
					/* START CODE: TO INSERT INTO MAIL MESSAGE TABLES */
				/* START : SET VALUES */	
				 --SELECT @v_ToEmailId_inVc = @v_ToEmailId_Vc, 
					--@v_FromEmailId_inVc = @v_FromEmailId_Vc, 
					--@v_Subject_inVc = @v_Subject_inVc,
					--@v_MsgText_inVc ='<p>   Dear User,</p>  <p>   &nbsp;</p>  <p>   A file has come to you for processing. Details of the file to be processed are as mentioned below:</p>  <p>   &nbsp;</p>  <p>   Module Name: '+@v_ModuleName_Vc+'</p>  <p>   Event: '+@v_EventName_Vc+'</p>  <p>   Id : '+convert(varchar(20),@v_objectId_inInt)+'</p>  <p>   &nbsp;</p>  <p>   File Sent From:- '+@v_FromEmailId_Vc+'</p>  <p>   Official Name - Designation: '+@v_fileSentFrom_Vc+'<p>   &nbsp;</p> <p> Perform below mentioned steps to process the same: </p>   <p>1. Login to eGP website</p> <p>2. Click on workflow menu</p> <p>3. Select Pending task</p> <p>4. Then at last under action tab click on Process link</p>   <p>   &nbsp;</p>  <p>   Thanks,</p>  <p>   <strong>e-GP System</strong></p> ',				
					--@v_IsPriority_inVc ='High',
					--@v_MsgBoxType_inVc ='Inbox',
					--@v_Action_inVc ='Create',
					--@v_MsgStatus_inVc ='Live',
					--@v_FolderId_inInt = 0,
					--@v_MsgReadStatus_inVc ='No',
					--@v_IsMsgReply_inVc ='No',
					--@v_isDocUploadedVc = 'No',									 
					--@v_FromUserIdInt = @v_fromUserId_inInt, 
					--@v_UserIdInt=@v_toUserId_Int, 
					--@v_msgToCC_Vc='To' 		
					
					if (@v_activityId_inInt =4  or @v_activityId_inInt =1 or @v_activityId_inInt =11 )
					Begin
						Select @v_URL_Vc ='<br><a style="text-decoration:underline;" href="./../../officer/FileProcessing.jsp?objectid='+ CONVERT(varchar(50), @v_objectId_inInt)  + '&childid='+CONVERT(varchar(50), @v_objectId_inInt) +'&eventid='+CONVERT(varchar(50), @v_eventId_Int) +'&activityid='+CONVERT(varchar(50), @v_activityId_inInt) +'&fromaction=pending">Click here</a>'
					End
					Else
					Begin
						Select @v_URL_Vc ='<br><a style="text-decoration:underline;" href="./../../officer/FileProcessing.jsp?objectid='+ CONVERT(varchar(50), @v_objectId_inInt)  + '&childid='+CONVERT(varchar(50), @v_childId_inInt) +'&eventid='+CONVERT(varchar(50), @v_eventId_Int) +'&activityid='+CONVERT(varchar(50), @v_activityId_inInt) +'&fromaction=pending">Click here</a>'
					End
					if @v_activityId_inInt =1
					begin
					select @v_AppCode=appcode from tbl_AppMaster where appId=@v_objectId_inInt
					end
					
					
					If @v_action_inVc='Approve' OR @v_action_inVc='Approved' Or @v_action_inVc='Cancelled'
					Begin
						Set @v_Subject_inVc = 'File Approved by Approver'
						if @v_activityId_inInt =1
						begin
						
						set @v_MsgText_inVc ='<p>Dear User,</p><br /><p>This is to inform you that Approver has approved below mentioned file :<br></p><table style="width: 100%"><tr><td style="width: 25%"><strong>Module Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_ModuleName_Vc+'<td><tr><tr><td style="width: 25%"><strong>Process Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_EventName_Vc+'<td><tr><tr><td style="width: 25%"><strong>App Code</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+convert(varchar(150),@v_AppCode)+'<td><tr></table><br /><p> You are requested to take necessary action on this file.</p><br /><p>Thanks,<br><strong>e-GP System.</strong></p>'
						end
						else
						begin
						set @v_MsgText_inVc ='<p>Dear User,</p><br /><p>This is to inform you that Approver has approved below mentioned file :<br></p><table style="width: 100%"><tr><td style="width: 25%"><strong>Module Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_ModuleName_Vc+'<td><tr><tr><td style="width: 25%"><strong>Process Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_EventName_Vc+'<td><tr><tr><td style="width: 25%"><strong>ID</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+convert(varchar(20),@v_objectId_inInt)+'<td><tr></table><br /><p> You are requested to take necessary action on this file.</p><br /><p>Thanks,<br><strong>e-GP System.</strong></p>'
						end
					End
					else
					begin
					if @v_activityId_inInt =1
						begin
						
					
						set @v_MsgText_inVc ='<p> Dear User,</p><br><p>A file has come to you for processing. Detail of the file to be processed is as mentioned below:<br><br></p><table style="width: 100%"><tr><td style="width: 25%"><strong>Module Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_ModuleName_Vc+'<td><tr><tr><td style="width: 25%"><strong>Process</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_EventName_Vc+'<td><tr><tr><td style="width: 25%"><strong>ID</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+convert(varchar(150),@v_AppCode)+'<td><tr><tr><td style="width: 25%"><strong>File Sent By</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_fileSentFrom_Vc+'<td><tr></table><p> <strong>'+@v_URL_Vc+'</strong> to process this file now. Or you can perform below mentioned steps to process this file later on:<br><br></p>  <p>1. Click on <strong>Workflow</strong> menu<br>2. Select <strong>Pending</strong> task<br>3. Click on <strong>Process</strong> link available in front of a particular file to be processed</p><br><p>Thanks,<br><strong>e-GP System</strong></p>'
						end
					else
					begin
					set @v_MsgText_inVc ='<p> Dear User,</p><br><p>A file has come to you for processing. Detail of the file to be processed is as mentioned below:<br><br></p><table style="width: 100%"><tr><td style="width: 25%"><strong>Module Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_ModuleName_Vc+'<td><tr><tr><td style="width: 25%"><strong>Process</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_EventName_Vc+'<td><tr><tr><td style="width: 25%"><strong>ID</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+convert(varchar(20),@v_objectId_inInt)+'<td><tr><tr><td style="width: 25%"><strong>File Sent By</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_fileSentFrom_Vc+'<td><tr></table><p> <strong>'+@v_URL_Vc+'</strong> to process this file now. Or you can perform below mentioned steps to process this file later on:<br><br></p>  <p>1. Click on <strong>Workflow</strong> menu<br>2. Select <strong>Pending</strong> task<br>3. Click on <strong>Process</strong> link available in front of a particular file to be processed</p><br><p>Thanks,<br><strong>e-GP System</strong></p>'
					end	
					end	
					
					
					--SELECT @v_ToEmailId_inVc = @v_ToEmailId_Vc, 
					--@v_FromEmailId_inVc = @v_FromEmailId_Vc, 
					--@v_Subject_inVc = @v_Subject_inVc,
					--@v_MsgText_inVc ='<p> Dear User,</p><br><p>A file has come to you for processing. Detail of the file to be processed is as mentioned below:<br><br></p><table style="width: 100%"><tr><td style="width: 25%"><strong>Module Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_ModuleName_Vc+'<td><tr><tr><td style="width: 25%"><strong>Process</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_EventName_Vc+'<td><tr><tr><td style="width: 25%"><strong>ID</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+convert(varchar(20),@v_objectId_inInt)+'<td><tr><tr><td style="width: 25%"><strong>File Sent By</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_fileSentFrom_Vc+'<td><tr></table><p> <strong>'+@v_URL_Vc+'</strong> to process this file now. Or you can perform below mentioned steps to process this file later on:<br><br></p>  <p>1. Click on <strong>Workflow</strong> menu<br>2. Select <strong>Pending</strong> task<br>3. Click on <strong>Process</strong> link available in front of a particular file to be processed</p><br><p>Thanks,<br><strong>e-GP System.</strong></p> ',
					--@v_IsPriority_inVc ='High',
					--@v_MsgBoxType_inVc ='Inbox',
					--@v_Action_inVc ='Create',
					--@v_MsgStatus_inVc ='Live',
					--@v_FolderId_inInt = 0,
					--@v_MsgReadStatus_inVc ='No',
					--@v_IsMsgReply_inVc ='No',
					--@v_isDocUploadedVc = 'No',									 
					--@v_FromUserIdInt = @v_fromUserId_inInt, 
					--@v_UserIdInt=@v_toUserId_Int, 
					--@v_msgToCC_Vc='To' 
					
					
				/* Dohatec Start*/
		IF (@v_activityId_inInt =13 AND (@v_action_inVc = 'Approve' OR @v_action_inVc = 'Rejected' OR @v_action_inVc = 'Rejected / Re-Tendering' or @v_action_inVc = 'Re-evaluation'))
		BEGIN
		Select @v_FromEmailId_Vc=emailId,@v_FromUserIdInt=userid  from dbo.tbl_LoginMaster where emailId='noreply@eprocure.gov.bd'
		--SET @v_FromEmailId_Vc='noreply@eprocure.gov.bd'
		
		IF (@v_action_inVc = 'Approve')
		BEGIN
		SET @v_Subject_inVc = 'Approval of Evaluation Report'
		set @v_MsgText_inVc ='<p>Dear User,</p><br /><p>This is to inform you that Approver has approved below mentioned file :<br></p><table style="width: 100%"><tr><td style="width: 25%"><strong>Module Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_ModuleName_Vc+'<td><tr><tr><td style="width: 25%"><strong>Process Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_EventName_Vc+'<td><tr><tr><td style="width: 25%"><strong>ID</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+convert(varchar(20),@v_objectId_inInt)+'<td><tr></table><br /><p>Thanks,<br><strong>e-GP System.</strong></p>'
		END
		
		IF (@v_action_inVc = 'Reject')
		BEGIN
		SET @v_Subject_inVc = 'e-GP System:  AA has rejected the Evaluation Report'
		SET @v_MsgText_inVc ='<p>Dear User,</p><br /><p>This is to inform you that Approver has Rejected below mentioned file :<br></p><table style="width: 100%"><tr><td style="width: 25%"><strong>Module Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_ModuleName_Vc+'<td><tr><tr><td style="width: 25%"><strong>Process Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_EventName_Vc+'<td><tr><tr><td style="width: 25%"><strong>ID</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+convert(varchar(20),@v_objectId_inInt)+'<td><tr></table><br /><p>Thanks,<br><strong>e-GP System.</strong></p>'
		END
		
		IF (@v_action_inVc = 'Rejected / Re-Tendering')
		BEGIN
		SET @v_Subject_inVc = 'e-GP System:  Recommended for Re-tendering'
		SET @v_MsgText_inVc ='<p>Dear User,</p><br /><p>This is to inform you that Approver has Rejected / Re-Tendering below mentioned file :<br></p><table style="width: 100%"><tr><td style="width: 25%"><strong>Module Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_ModuleName_Vc+'<td><tr><tr><td style="width: 25%"><strong>Process Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_EventName_Vc+'<td><tr><tr><td style="width: 25%"><strong>ID</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+convert(varchar(20),@v_objectId_inInt)+'<td><tr></table><br /><p>Thanks,<br><strong>e-GP System.</strong></p>'
		END
		
		IF (@v_action_inVc = 'Re-evaluation')
		BEGIN
		SET @v_Subject_inVc = 'e-GP System:  Recommended for Re-evaluation'
		SET @v_MsgText_inVc ='<p>Dear User,</p><br /><p>This is to inform you that Approver wants to re-evaluate below mentioned file :<br></p><table style="width: 100%"><tr><td style="width: 25%"><strong>Module Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_ModuleName_Vc+'<td><tr><tr><td style="width: 25%"><strong>Process Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_EventName_Vc+'<td><tr><tr><td style="width: 25%"><strong>ID</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+convert(varchar(20),@v_objectId_inInt)+'<td><tr></table><br /><p>Thanks,<br><strong>e-GP System.</strong></p>'
		END
		
		DECLARE @v_action_inVc_Param varchar(50)
		SET @v_action_inVc_Param = @v_Action_inVc
		SELECT 		@v_FromEmailId_inVc = @v_FromEmailId_Vc, 
					@v_IsPriority_inVc ='High',
					@v_MsgBoxType_inVc ='Inbox',
					@v_Action_inVc ='Create',
					@v_MsgStatus_inVc ='Live',
					@v_FolderId_inInt = 0,
					@v_MsgReadStatus_inVc ='No',
					@v_IsMsgReply_inVc ='No',
					@v_isDocUploadedVc = 'No',									 
					--@v_FromUserIdInt = 1344, 
					@v_UserIdInt=@v_toUserId_Int, 
					@v_msgToCC_Vc='To' 
					
				
				begin try
				
					
					DECLARE mail_cursor CURSOR FOR 					
					select l.emailId ,l.userId 
					from tbl_WorkFlowLevelConfig wc
					,tbl_LoginMaster l
					where activityId = @v_activityId_inInt 
					and objectId = @v_objectId_inInt
					 and l.userId=wc.userId
					 and l.userId not in
					(SELECT l.userId from  
					tbl_TenderMaster t,tbl_AppMaster a, 
					tbl_EmployeeMaster e,tbl_LoginMaster l
					where tenderId=@v_objectId_inInt and t.appId=a.appId
					and a.employeeId=e.employeeId
					and l.userId=e.userId)
	
		
					OPEN mail_cursor
					FETCH NEXT FROM mail_cursor 
					INTO @v_ToEmailId_inVc,@v_UserIdInt
						WHILE @@FETCH_STATUS = 0
						BEGIN					
						
							--/* START CODE : TO INSERT INTO TABLE - tbl_Message */
							--INSERT INTO tbl_Message (subject, msgText) VALUES (@v_Subject_inVc, @v_MsgText_inVc)				
							/* END CODE : TO INSERT INTO TABLE - tbl_Message */
				
							--Set @v_MsgIdInt=Ident_Current('dbo.tbl_Message') 
					
							/* START CODE : TO INSERT INTO TABLE - tbl_MessageInBox */	
							--Insert Into tbl_MessageInBox
							--	(msgToUserId, msgFromUserId, msgId, msgToCCReply, msgStatus, creationDate, 
							--	folderId, isPriority, isMsgRead, isDocUploaded, processUrl )
							--Select @v_UserIdInt , @v_FromUserIdInt, @v_MsgIdInt, @v_msgToCC_Vc, @v_MsgStatus_inVc, GETDATE(), 
							--@v_FolderId_inInt, @v_IsPriority_inVc, @v_MsgReadStatus_inVc, @v_isDocUploadedVc, @v_ProcessUrl_inVc 
							/* END CODE : TO INSERT INTO TABLE - tbl_MessageInBox */
			
							/* START CODE : TO INSERT INTO TABLE - Tbl_MessageSent */	
							--Insert Into tbl_MessageSent
							--(msgToUserId, msgFromUserId, msgId, msgToCCReply, msgStatus, creationDate, 
							--folderId, isPriority, isMsgRead, isDocUploaded  )
							--Select @v_UserIdInt , @v_FromUserIdInt, @v_MsgIdInt, @v_msgToCC_Vc, @v_MsgStatus_inVc, GETDATE(), 
							--@v_FolderId_inInt, @v_IsPriority_inVc, @v_MsgReadStatus_inVc, @v_isDocUploadedVc  
							/* END CODE : TO INSERT INTO TABLE - Tbl_MessageSent */
				
							/* END CODE: TO INSERT INTO MAIL MESSAGE TABLES */
						
							FETCH NEXT FROM mail_cursor 
							INTO @v_ToEmailId_inVc,@v_UserIdInt
						END
					CLOSE mail_cursor;
					DEALLOCATE mail_cursor;
			end try
			begin catch
					CLOSE mail_cursor;
					DEALLOCATE mail_cursor;
			end catch
				
					select	@v_ToEmailId_inVc = emailId ,
							@v_UserIdInt = l.userId from  
					tbl_TenderMaster t,tbl_AppMaster a, 
					tbl_EmployeeMaster e,tbl_LoginMaster l
					where tenderId=@v_objectId_inInt and t.appId=a.appId
					and a.employeeId=e.employeeId
					and l.userId=e.userId
					
					IF (@v_action_inVc_Param = 'Approve')
					BEGIN
						SET @v_MsgText_inVc ='<p>Dear User,</p><br /><p>This is to inform you that Approver has approved below mentioned file :<br></p><table style="width: 100%"><tr><td style="width: 25%"><strong>Module Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_ModuleName_Vc+'<td><tr><tr><td style="width: 25%"><strong>Process Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_EventName_Vc+'<td><tr><tr><td style="width: 25%"><strong>ID</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+convert(varchar(20),@v_objectId_inInt)+'<td><tr></table><br /><p> You are requested to take necessary action on this file.</p><br /><p>Thanks,<br><strong>e-GP System.</strong></p>'
					END
					IF (@v_action_inVc_Param = 'Reject')
					BEGIN
						SET @v_MsgText_inVc ='<p>Dear User,</p><br /><p>This is to inform you that Approver has Rejected below mentioned file :<br></p><table style="width: 100%"><tr><td style="width: 25%"><strong>Module Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_ModuleName_Vc+'<td><tr><tr><td style="width: 25%"><strong>Process Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_EventName_Vc+'<td><tr><tr><td style="width: 25%"><strong>ID</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+convert(varchar(20),@v_objectId_inInt)+'<td><tr></table><br /><p> You are requested to take necessary action on this file.</p><br /><p>Thanks,<br><strong>e-GP System.</strong></p>'
					END
		
					IF (@v_action_inVc_Param = 'Rejected / Re-Tendering')
					BEGIN
						SET @v_MsgText_inVc ='<p>Dear User,</p><br /><p>This is to inform you that Approver has Rejected / Re-Tendering below mentioned file :<br></p><table style="width: 100%"><tr><td style="width: 25%"><strong>Module Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_ModuleName_Vc+'<td><tr><tr><td style="width: 25%"><strong>Process Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_EventName_Vc+'<td><tr><tr><td style="width: 25%"><strong>ID</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+convert(varchar(20),@v_objectId_inInt)+'<td><tr></table><br /><p> You are requested to take necessary action on this file.</p><br /><p>Thanks,<br><strong>e-GP System.</strong></p>'
					END
					
					IF (@v_action_inVc_Param = 'Re-evaluation')
					BEGIN
						SET @v_MsgText_inVc ='<p>Dear User,</p><br /><p>This is to inform you that Approver wants to re-evaluate below mentioned file :<br></p><table style="width: 100%"><tr><td style="width: 25%"><strong>Module Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_ModuleName_Vc+'<td><tr><tr><td style="width: 25%"><strong>Process Name</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+@v_EventName_Vc+'<td><tr><tr><td style="width: 25%"><strong>ID</strong><td><td style="width: 1%">:<td><td style="width: 74%">'+convert(varchar(20),@v_objectId_inInt)+'<td><tr></table><br /><p> You are requested to take necessary action on this file.</p><br /><p>Thanks,<br><strong>e-GP System.</strong></p>'
					END
					
							--/* FOR PE START CODE : TO INSERT INTO TABLE - tbl_Message */
							--INSERT INTO tbl_Message (subject, msgText) VALUES (@v_Subject_inVc, @v_MsgText_inVc)				
							/* END CODE : TO INSERT INTO TABLE - tbl_Message */
				
							--Set @v_MsgIdInt=Ident_Current('dbo.tbl_Message') 
					
							/* START CODE : TO INSERT INTO TABLE - tbl_MessageInBox */	
							--Insert Into tbl_MessageInBox
							--	(msgToUserId, msgFromUserId, msgId, msgToCCReply, msgStatus, creationDate, 
							--	folderId, isPriority, isMsgRead, isDocUploaded, processUrl )
							--Select @v_UserIdInt , @v_FromUserIdInt, @v_MsgIdInt, @v_msgToCC_Vc, @v_MsgStatus_inVc, GETDATE(), 
							--@v_FolderId_inInt, @v_IsPriority_inVc, @v_MsgReadStatus_inVc, @v_isDocUploadedVc, @v_ProcessUrl_inVc 
							/* END CODE : TO INSERT INTO TABLE - tbl_MessageInBox */
			
							/* START CODE : TO INSERT INTO TABLE - Tbl_MessageSent */	
							--Insert Into tbl_MessageSent
							--(msgToUserId, msgFromUserId, msgId, msgToCCReply, msgStatus, creationDate, 
							--folderId, isPriority, isMsgRead, isDocUploaded  )
							--Select @v_UserIdInt , @v_FromUserIdInt, @v_MsgIdInt, @v_msgToCC_Vc, @v_MsgStatus_inVc, GETDATE(), 
							--@v_FolderId_inInt, @v_IsPriority_inVc, @v_MsgReadStatus_inVc, @v_isDocUploadedVc  
							/* END CODE : TO INSERT INTO TABLE - Tbl_MessageSent */
				
							/* FOR PE END CODE: TO INSERT INTO MAIL MESSAGE TABLES */
							
							
		END
		ELSE	
		
		BEGIN		
		/* Dohatec End*/
		
					
					SELECT @v_ToEmailId_inVc = @v_ToEmailId_Vc, 
					@v_FromEmailId_inVc = @v_FromEmailId_Vc, 
					@v_Subject_inVc = @v_Subject_inVc,
					@v_MsgText_inVc=@v_MsgText_inVc,
					@v_IsPriority_inVc ='High',
					@v_MsgBoxType_inVc ='Inbox',
					@v_Action_inVc ='Create',
					@v_MsgStatus_inVc ='Live',
					@v_FolderId_inInt = 0,
					@v_MsgReadStatus_inVc ='No',
					@v_IsMsgReply_inVc ='No',
					@v_isDocUploadedVc = 'No',									 
					@v_FromUserIdInt = @v_fromUserId_inInt, 
					@v_UserIdInt=@v_toUserId_Int, 
					@v_msgToCC_Vc='To' 
					
					If @v_activityId_inInt=1
					Begin
						select @v_Subject_inVc ='APP Code: '+CONVERT(varchar(150), @v_AppCode)+' '+@v_Subject_inVc
					End
					Else
					Begin
						select @v_Subject_inVc ='Tender ID : '+CONVERT(varchar(50), @v_objectId_inInt)+' '+@v_Subject_inVc
					End
								
								
								
								
				/* END : SET VALUES */	 			
	 
				--/* START CODE : TO INSERT INTO TABLE - tbl_Message */
				--INSERT INTO tbl_Message (subject, msgText) VALUES (@v_Subject_inVc, @v_MsgText_inVc)				
				/* END CODE : TO INSERT INTO TABLE - tbl_Message */
				
				--Set @v_MsgIdInt=Ident_Current('dbo.tbl_Message') 
					
				/* START CODE : TO INSERT INTO TABLE - tbl_MessageInBox */	
				--Insert Into tbl_MessageInBox
				--		(msgToUserId, msgFromUserId, msgId, msgToCCReply, msgStatus, creationDate, 
				--		folderId, isPriority, isMsgRead, isDocUploaded, processUrl )
				--Select @v_UserIdInt , @v_FromUserIdInt, @v_MsgIdInt, @v_msgToCC_Vc, @v_MsgStatus_inVc, GETDATE(), 
				--		@v_FolderId_inInt, @v_IsPriority_inVc, @v_MsgReadStatus_inVc, @v_isDocUploadedVc, @v_ProcessUrl_inVc 
				/* END CODE : TO INSERT INTO TABLE - tbl_MessageInBox */
			
				/* START CODE : TO INSERT INTO TABLE - Tbl_MessageSent */	
				--Insert Into tbl_MessageSent
				--		(msgToUserId, msgFromUserId, msgId, msgToCCReply, msgStatus, creationDate, 
				--		folderId, isPriority, isMsgRead, isDocUploaded  )
				--Select @v_UserIdInt , @v_FromUserIdInt, @v_MsgIdInt, @v_msgToCC_Vc, @v_MsgStatus_inVc, GETDATE(), 
				--		@v_FolderId_inInt, @v_IsPriority_inVc, @v_MsgReadStatus_inVc, @v_isDocUploadedVc  
				/* END CODE : TO INSERT INTO TABLE - Tbl_MessageSent */
				
				/* END CODE: TO INSERT INTO MAIL MESSAGE TABLES */
			
			/* Dohatec Start*/
			END	
			/* Dohatec End*/	
				
				Set @v_flag_bit=1
				Select @v_flag_bit as flag, 'Record updated.' as Message				
			COMMIT TRAN	UPD_TRAN
		END TRY 
		BEGIN CATCH
			BEGIN			
				Set @v_flag_bit=0
				Select @v_flag_bit as flag, 'Record not updated. ' + ERROR_MESSAGE() as Message									
				ROLLBACK TRAN UPD_TRAN	
			END
		END CATCH		
     END
    SET NOCOUNT OFF;
    END
END


GO
