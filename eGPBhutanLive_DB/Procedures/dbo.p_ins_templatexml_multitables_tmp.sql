SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Insert into multiple tables using XML text 
--
--
-- Author: Karan
-- Date: 15-11-2010
--
-- Last Modified:
-- Modified By: Karan
-- Date: 18-11-2010
-- Modification: Code added to delete from tables
--------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ins_templatexml_multitables_tmp]
-- INPUT VARIABLES
@v_Action_inVc varchar(50),
@v_Tbl1_inVc varchar(500),
@v_Tbl2_inVc varchar(500)=null,
@v_Tbl3_inVc varchar(500)=null,
@v_Tbl4_inVc varchar(500)=null,
@v_Tbl1_XmlTxt_inVc varchar(max)=null,
@v_Tbl2_XmlTxt_inVc varchar(max)=null,
@v_Tbl3_XmlTxt_inVc varchar(max)=null,
@v_Tbl4_XmlTxt_inVc varchar(max)=null,
@v_Tbl1_PKColumn_inVc varchar(500)=null,
@v_Tbl2_PKColumn_inVc varchar(500)=null,
@v_Tbl3_PKColumn_inVc varchar(500)=null,
@v_Condition_inVc varchar(4000)=null

AS

BEGIN
SET NOCOUNT ON;

-- LOCAL VARIABLES
Declare @v_Declaration_Vc varchar(max), @v_InsQry_Vc varchar(max), @v_NodePath_Vc varchar(500),
	    @v_DelQry_Vc varchar(max), @v_FinalQry_Vc varchar(max), @v_SuccessMsg_Vc varchar(2000),
	    @v_FailureMsg_Vc varchar(2000)

-- SET VARIABLES
Select @v_Declaration_Vc='
		;Declare 
		@v_flag_bit bit, 
		@i int, 
		@IdentVal int, 
		@ReQry varchar(max), 
		@CurrQry varchar(max), 
		@CurrPKId int,
		@CurrRowId int;'

IF @v_Action_inVc='delete' Or @v_Action_inVc='insdel' 
BEGIN
	/* START CODE: alter DYNAMIC QUERY TO DELETE FROM TABLE(S) */
	SELECT @v_DelQry_Vc='DELETE FROM ' + @v_Tbl1_inVc + ' WHERE ' + @v_Condition_inVc + '; '	
	
	IF @v_Tbl2_inVc is Not Null And @v_Tbl2_inVc<>''
	BEGIN
		SELECT @v_DelQry_Vc='DELETE FROM ' + @v_Tbl2_inVc + ' WHERE ' + @v_Tbl1_PKColumn_inVc + ' IN (Select ' + @v_Tbl1_PKColumn_inVc +' from ' + @v_Tbl1_inVc + ' where ' + @v_Condition_inVc + '); '
			+ @v_DelQry_Vc										 
		
		IF @v_Tbl3_inVc is Not Null And @v_Tbl3_inVc<>''
		BEGIN
			SELECT @v_DelQry_Vc='DELETE FROM ' + @v_Tbl3_inVc + ' WHERE ' + @v_Tbl2_PKColumn_inVc + ' IN (Select ' + @v_Tbl2_PKColumn_inVc + ' from ' + @v_Tbl2_inVc + ' where ' + @v_Tbl1_PKColumn_inVc + ' in (Select ' + @v_Tbl1_PKColumn_inVc +' from ' + @v_Tbl1_inVc + ' where ' + @v_Condition_inVc + ')); '
					+ @v_DelQry_Vc 				
			
				IF @v_Tbl4_inVc is Not Null And @v_Tbl4_inVc<>''
				BEGIN
					SELECT @v_DelQry_Vc='DELETE FROM ' + @v_Tbl4_inVc + ' WHERE ' + @v_Tbl3_PKColumn_inVc + ' IN (Select ' + @v_Tbl3_PKColumn_inVc + ' from ' + @v_Tbl3_inVc + ' where ' + @v_Tbl2_PKColumn_inVc + ' in (Select ' + @v_Tbl2_PKColumn_inVc + ' from ' + @v_Tbl2_inVc + ' where ' + @v_Tbl1_PKColumn_inVc + ' in (Select ' + @v_Tbl1_PKColumn_inVc +' from ' + @v_Tbl1_inVc + ' where ' + @v_Condition_inVc + '))); ' 
								+ @v_DelQry_Vc						
				END
		END
	END	
	/* END CODE: alter DYNAMIC QUERY TO DELETE FROM TABLE(S) */
	
	-- // PRINT QUERY
	--PRINT(@v_DelQry_Vc)
END

IF @v_Action_inVc='insert' OR @v_Action_inVc='insdel' 
BEGIN
	/* START CODE: alter DYNAMIC QUERY TO INSERT INTO TABLE(S) */
	Select @v_NodePath_Vc = '/root/'+@v_Tbl1_inVc

	SET @v_InsQry_Vc='
	EXEC sp_xml_preparedocument @i OUTPUT, '''+@v_Tbl1_XmlTxt_inVc+''';
	INSERT INTO ' +@v_Tbl1_inVc +'	
	SELECT * FROM OPENXML(@i, '''+ @v_NodePath_Vc + ''') With '+@v_Tbl1_inVc+';
	EXEC sp_xml_removedocument @i;
	Select @IdentVal=SCOPE_IDENTITY();'

	IF @v_Tbl2_inVc is Not Null And @v_Tbl2_inVc<>''
	BEGIN
		Select @v_NodePath_Vc = '/root/'+@v_Tbl2_inVc
		
		SELECT @v_InsQry_Vc = @v_InsQry_Vc + 
		' Select @ReQry=Replace('''+@v_Tbl2_XmlTxt_inVc+''',''IdentKey'',@IdentVal);
		/* Select @ReQry as Tbl2_XML;*/
		EXEC sp_xml_preparedocument @i OUTPUT, @ReQry;
		INSERT INTO ' +@v_Tbl2_inVc +'	
		SELECT * FROM OPENXML(@i, '''+ @v_NodePath_Vc + ''') With '+@v_Tbl2_inVc+';
		EXEC sp_xml_removedocument @i;
		'	
		
		IF @v_Tbl3_inVc is Not Null And @v_Tbl3_inVc<>''
		BEGIN
			--Insert into tbl_TempTesting (Text) Values (@v_Tbl3_XmlTxt_inVc)
			Select @v_NodePath_Vc = '/root/'+@v_Tbl3_inVc
			
			SELECT @v_InsQry_Vc =  @v_InsQry_Vc + 
			'Select @ReQry=Replace(Replace('''+@v_Tbl3_XmlTxt_inVc+''',''</root>'',''''),''<root>'',''''),
			@CurrQry='''';
			/* Select @ReQry; */
			/* START: CURSOR*/
			DECLARE cur1 cursor FAST_FORWARD For 
				SELECT DISTINCT '+@v_Tbl2_PKColumn_inVc+', ROW_NUMBER() Over (Order by '+@v_Tbl2_PKColumn_inVc+') FROM '+@v_Tbl2_inVc+' WHERE '+@v_Tbl1_PKColumn_inVc+'=@IdentVal
			OPEN cur1        
				FETCH NEXT FROM cur1 INTO @CurrPKId, @CurrRowId    

				WHILE @@Fetch_status = 0        
				BEGIN   
					If ''' + @v_Tbl3_inVc + '''=''tbl_TenderBidDetail''
					BEGIN
						Select @CurrQry=Replace(@ReQry, ''IdentKey'' + Convert(varchar(50),@CurrRowId), @CurrPKId);
						Select @ReQry = @CurrQry;
					END
					ELSE
					BEGIN
						If @CurrQry=''''
						Begin
							Select @CurrQry=Replace(@ReQry, ''IdentKey'', @CurrPKId)			
						End
						Else
						Begin			
							Select @CurrQry=@CurrQry+Replace(@ReQry, ''IdentKey'', @CurrPKId) 
						End
					END
					
					FETCH NEXT FROM cur1 INTO @CurrPKId, @CurrRowId        
				END      

			CLOSE cur1        
			DEALLOCATE cur1				
			 /* END: CURSOR */
			Select @CurrQry=''<root>''+@CurrQry+''</root>''
			/* Select @CurrQry as Tbl3_XML; */
			EXEC sp_xml_preparedocument @i OUTPUT, @CurrQry;
			
			INSERT INTO ' +@v_Tbl3_inVc +'	
			SELECT * FROM OPENXML(@i, '''+ @v_NodePath_Vc + ''') With '+@v_Tbl3_inVc+';
			EXEC sp_xml_removedocument @i;
			'
			
			IF @v_Tbl4_inVc is Not Null And @v_Tbl4_inVc<>''
			BEGIN
				Select @v_NodePath_Vc = '/root/'+@v_Tbl4_inVc
				
				SELECT @v_InsQry_Vc =  @v_InsQry_Vc + 
				'Select @ReQry=Replace(Replace('''+@v_Tbl4_XmlTxt_inVc+''',''</root>'',''''),''<root>'',''''),
				@CurrQry='''';
				/* Select @ReQry; */
				/* START: CURSOR */
				DECLARE cur12 cursor FAST_FORWARD For 
					SELECT DISTINCT '+@v_Tbl3_PKColumn_inVc+' 
					FROM '+@v_Tbl3_inVc+' T3 INNER JOIN '+@v_Tbl2_inVc+' T2 ON T3.'+@v_Tbl2_PKColumn_inVc+'=T2.'+@v_Tbl2_PKColumn_inVc+' 
					WHERE T2.'+@v_Tbl1_PKColumn_inVc+'=@IdentVal
				OPEN cur12        
					FETCH NEXT FROM cur12 INTO @CurrPKId    

					WHILE @@Fetch_status = 0        
					BEGIN   
						
						If @CurrQry=''''
						Begin
							Select @CurrQry=Replace(@ReQry, ''IdentKey'', @CurrPKId)			
						End
						Else
						Begin			
							Select @CurrQry=@CurrQry+Replace(@ReQry, ''IdentKey'', @CurrPKId) 
						End
						FETCH NEXT FROM cur12 INTO @CurrPKId       
					END      

				CLOSE cur12        
				DEALLOCATE cur12				
				/* END: CURSOR */
				Select @CurrQry=''<root>''+@CurrQry+''</root>''
				/* Select @CurrQry as Tbl4_XML; */
				EXEC sp_xml_preparedocument @i OUTPUT, @CurrQry;
				INSERT INTO ' +@v_Tbl4_inVc +'	
				SELECT * FROM OPENXML(@i, '''+ @v_NodePath_Vc + ''') With '+@v_Tbl4_inVc+';
				EXEC sp_xml_removedocument @i;
				'
				
				print 'kkkk'
			END
			
		END	
		
	END
	/* END CODE: alter DYNAMIC QUERY TO INSERT INTO TABLE(S) */	
END	

/* START CODE: SET THE SUCCESS & FAILURE MESSAGES AND THE FINAL QUERY */
SELECT 
	@v_SuccessMsg_Vc=
		Case @v_Action_inVc 
			When 'insert'				
			Then 'Record inserted.'
			When 'delete'
			Then 'Record deleted.'
			When 'insdel'				
			Then 'Record inserted.'
		End,
	@v_FailureMsg_Vc=
	Case @v_Action_inVc 
			When 'insert'			
			Then 'Record not inserted<br />ERROR: '			
			When 'delete'
			Then 'Record not deleted<br />ERROR: '
			When 'insdel'
			Then 'Record not inserted<br />ERROR: '
		End, 
	@v_FinalQry_Vc =
	Case @v_Action_inVc 
		When 'insert'
		Then 
			'/* START CODE: DECLAREED VARIABLES */'
			+@v_Declaration_Vc
			+'/* END CODE: DECLAREED VARIABLES */
			/* START CODE: FOR INSERT QUERY */'
			+@v_InsQry_Vc+
			+'/* END CODE: FOR INSERT QUERY */'
		
		When 'delete'			
		Then 
			'/* START CODE: DECLAREED VARIABLES */'
			+@v_Declaration_Vc
			+'/* END CODE: DECLAREED VARIABLES */'
			+'/* START CODE: FOR DELETE QUERY */'
			+@v_DelQry_Vc
			+'/* END CODE: FOR DELETE QUERY */'
		
		When 'insdel'
		Then	
			'/* START CODE: DECLAREED VARIABLES */'
			+@v_Declaration_Vc
			+'/* END CODE: DECLAREED VARIABLES */'
			+'/* START CODE: FOR DELETE QUERY */'
			+@v_DelQry_Vc
			+'/* END CODE: FOR DELETE QUERY */'
			+'/* START CODE: FOR INSERT QUERY */'
			+@v_InsQry_Vc+
			+'/* END CODE: FOR INSERT QUERY */'
		End	
/* END CODE: SET THE SUCCESS & FAILURE MESSAGES AND THE FINAL QUERY */

/* START CODE: SET THE FINAL QUERY WITH THE TRY/CATCH BLOCK  */
Select @v_FinalQry_Vc
SELECT @v_FinalQry_Vc=
		'BEGIN TRY
			BEGIN TRAN'
				
				+@v_FinalQry_Vc+
				+'Set @v_flag_bit=1
				Select @v_flag_bit as flag, ''' + @v_SuccessMsg_Vc + ''' as Message, @IdentVal as Id
			COMMIT TRAN	
		 END TRY 
		 BEGIN CATCH
			BEGIN
				Set @v_flag_bit=0
				Select @v_flag_bit as flag, ''' + @v_FailureMsg_Vc + ''' + Error_Message() as Message, 0 as Id				
				ROLLBACK TRAN
			END
		 END CATCH'
/* END CODE: SET THE FINAL QUERY WITH THE TRY/CATCH BLOCK  */
	
-- // PRINT QUERY
PRINT(@v_FinalQry_Vc)

-- // EXECUTE QUERY
EXEC (@v_FinalQry_Vc)
	
SET NOCOUNT OFF;

END


GO
