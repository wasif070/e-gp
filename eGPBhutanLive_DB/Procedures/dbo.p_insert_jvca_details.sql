SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Inserts into table tbl_TempCompanyJointVenture.
--
--
-- Author: Karan
-- Date: 28-10-2010
--
-- Last Modified:
-- Modified By:
-- Date 
-- Modification: 
--------------------------------------------------------------------------------
-- SP Name	   :   p_insert_jvca_details
-- Module	   :   JVCA
-- Description :   Creates Joint Venture
-- Function	   :   N.A.

CREATE PROCEDURE [dbo].[p_insert_jvca_details]

@v_userid_inInt int,
@v_jvcompanyid_inInt int,
@v_jvrole_inVc varchar(10)


AS

BEGIN

DECLARE 
 @v_compid_Int int, @v_flag_bit bit
SET NOCOUNT  ON

	Select @v_compid_Int = companyId from dbo.tbl_TempTendererMaster Where userid=@v_userid_inInt
	
	IF @v_jvrole_inVc='lead'
	BEGIN
		If Exists (Select ventureId From dbo.tbl_TempCompanyJointVenture Where companyId=@v_compid_Int And jvRole=@v_jvrole_inVc)
		Begin
			Set @v_flag_bit=0
			Select @v_flag_bit as flag, 'Lead partner already exists' as message
		End
		Else
		Begin
			If Not Exists (Select ventureId From dbo.tbl_TempCompanyJointVenture Where companyId=@v_compid_Int And jvCompanyId=@v_jvcompanyid_inInt)
			Begin
				Insert Into dbo.tbl_TempCompanyJointVenture (jvCompanyId, companyId, jvRole) Values (@v_jvcompanyid_inInt, @v_compid_Int, @v_jvrole_inVc )
				Set @v_flag_bit=1
				Select @v_flag_bit as flag, 'Company added successfully' as message
			End	
			Else
			Begin
				Set @v_flag_bit=0
				Select  @v_flag_bit  as flag, 'This company already exists' as message				
			End
		End			
	END
	ELSE IF @v_jvrole_inVc<>'lead'
	BEGIN
		If Not Exists (Select ventureId From dbo.tbl_TempCompanyJointVenture Where companyId=@v_compid_Int And jvCompanyId=@v_jvcompanyid_inInt)
		Begin
			Insert Into dbo.tbl_TempCompanyJointVenture (jvCompanyId, companyId, jvRole) Values (@v_jvcompanyid_inInt, @v_compid_Int, @v_jvrole_inVc )
			Set @v_flag_bit=1
			Select @v_flag_bit as flag, 'Company added successfully' as message
		End	
		Else
		Begin
			Set @v_flag_bit=0
			Select @v_flag_bit as flag, 'This company already exists' as message				
		End
	END
	 

SET NOCOUNT  OFF

END


GO
