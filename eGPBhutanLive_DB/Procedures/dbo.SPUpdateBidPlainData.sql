SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SPUpdateBidPlainData]
	@CellValueList as varchar(MAX),
	@ColumnIdList as varchar(MAX),
	@BidTableIdList as varchar(MAX),
	@RowIdList as varchar(MAX),
	@TenderFormId as int
	
AS

Declare @CellValueIndex int
Declare @SingleCellValue varchar(50)
Declare @ColumnIdIndex int
Declare @SingleColumnId varchar(50)
Declare @BidTableIdIndex int
Declare @SingleBidTableId varchar(50)
Declare @RowIdIndex int
Declare @SingleRowId varchar(50)

Declare @Count int
Declare @maxRowId int
Declare @sum MONEY
Declare @cellValue varchar(500)
Declare @parenthesisIndex int
Declare @findColumnId int
Declare @updateColumnId int


BEGIN
	Set @CellValueIndex = CHARINDEX(',',@CellValueList) 
	Set @ColumnIdIndex = CHARINDEX(',',@ColumnIdList) 
	Set @BidTableIdIndex = CHARINDEX(',',@BidTableIdList) 
	Set @RowIdIndex = CHARINDEX(',',@RowIdList) 
	
	PRINT @CellValueIndex
	
	BEGIN TRY
		BEGIN TRAN
			While(@CellValueIndex > -1)
				BEGIN
					IF(@CellValueIndex > 0)
						BEGIN
							Set @SingleCellValue =  LEFT(@CellValueList,LEN(@CellValueList)-(LEN(@CellValueList)-@CellValueIndex)-1)
							Set @CellValueList = RIGHT(@CellValueList,LEN(@CellValueList)-@CellValueIndex)
						
							Set @SingleColumnId =  LEFT(@ColumnIdList,LEN(@ColumnIdList)-(LEN(@ColumnIdList)-@ColumnIdIndex)-1)
							Set @ColumnIdList = RIGHT(@ColumnIdList,LEN(@ColumnIdList)-@ColumnIdIndex)
					
							Set @SingleBidTableId =  LEFT(@BidTableIdList,LEN(@BidTableIdList)-(LEN(@BidTableIdList)-@BidTableIdIndex)-1)
							Set @BidTableIdList = RIGHT(@BidTableIdList,LEN(@BidTableIdList)-@BidTableIdIndex)
							
							Set @SingleRowId =  LEFT(@RowIdList,LEN(@RowIdList)-(LEN(@RowIdList)-@RowIdIndex)-1)
							Set @RowIdList = RIGHT(@RowIdList,LEN(@RowIdList)-@RowIdIndex)
							
							Update tbl_TenderBidPlainData SET cellValue = @SingleCellValue where bidTableId = @SingleBidTableId and tenderColId = @SingleColumnId and rowId = @SingleRowId
				
							IF (CHARINDEX(@SingleBidTableId,@BidTableIdList)<1)
								BEGIN	
									select @maxRowId = MAX(rowId) from tbl_TenderBidPlainData where bidTableId  = @SingleBidTableId and tenderFormId = @TenderFormId
									select @parenthesisIndex = charIndex(')',formula)from tbl_TenderFormula where tenderFormId = @TenderFormId and isGrandTotal = 'yes'
									select @findColumnId = substring(formula,7,@parenthesisIndex-7)from tbl_TenderFormula where tenderFormId = @TenderFormId and isGrandTotal = 'yes'
									select @updateColumnId = columnId from tbl_TenderFormula where tenderFormId = @TenderFormId and isGrandTotal = 'yes'

									--select @maxRowId = MAX(rowId) from tbl_TenderBidPlainData where bidTableId  = @SingleBidTableId and tenderFormId = @TenderFormId
									set @Count = 1
									set @sum = 0
										While @Count <  @maxRowId 
											Begin
												select @cellValue = cellValue from	tbl_TenderBidPlainData where bidTableId  = @SingleBidTableId and tenderFormId = @TenderFormId and rowId = @Count and tenderColId = @findColumnId	
												select @sum = @sum + Convert(MONEY,@cellValue)
												Set @Count = @Count + 1
											End --End While
											PRINT @sum
									Update tbl_TenderBidPlainData SET cellValue = Convert(VARCHAR,@sum) where bidTableId = @SingleBidTableId and tenderColId = @updateColumnId and rowId = @maxRowId
								END
						END
					ELSE	
						BEGIN
							Set @SingleCellValue = @CellValueList
							Set @SingleColumnId = @ColumnIdList
							Set @SingleBidTableId = @BidTableIdList
							Set @SingleRowId = @RowIdList
						
						END
					
					
					if(@CellValueIndex = 0)
						BEGIN
						
						Set @CellValueIndex = -1
						
						Update tbl_TenderBidPlainData SET cellValue = @SingleCellValue where bidTableId = @SingleBidTableId and tenderColId = @SingleColumnId and rowId = @SingleRowId
				
						select @maxRowId = MAX(rowId) from tbl_TenderBidPlainData where bidTableId  = @SingleBidTableId and tenderFormId = @TenderFormId
						select @parenthesisIndex = charIndex(')',formula)from tbl_TenderFormula where tenderFormId = @TenderFormId and isGrandTotal = 'yes'
						select @findColumnId = substring(formula,7,@parenthesisIndex-7)from tbl_TenderFormula where tenderFormId = @TenderFormId and isGrandTotal = 'yes'
						select @updateColumnId = columnId from tbl_TenderFormula where tenderFormId = @TenderFormId and isGrandTotal = 'yes'

						--select @maxRowId = MAX(rowId) from tbl_TenderBidPlainData where bidTableId  = @SingleBidTableId and tenderFormId = @TenderFormId
						set @Count = 1
						set @sum = 0
							While @Count <  @maxRowId 
								Begin
									select @cellValue = cellValue from	tbl_TenderBidPlainData where bidTableId  = @SingleBidTableId and tenderFormId = @TenderFormId and rowId = @Count and tenderColId = @findColumnId	
									select @sum = @sum + Convert(MONEY,@cellValue)
									Set @Count = @Count + 1
								End --End While
								PRINT @sum
						Update tbl_TenderBidPlainData SET cellValue = Convert(VARCHAR,@sum) where bidTableId = @SingleBidTableId and tenderColId = @updateColumnId and rowId = @maxRowId

						END
					else
						BEGIN
							Set @CellValueIndex = CHARINDEX(',',@CellValueList)	
							Set @ColumnIdIndex = CHARINDEX(',',@ColumnIdList) 
							Set @BidTableIdIndex = CHARINDEX(',',@BidTableIdList) 
							Set @RowIdIndex = CHARINDEX(',',@RowIdList) 
						END	
					
					--PRINT @SingleCellValue
					--PRINT @CellValueList 
					--PRINT @SingleColumnId
					--PRINT @ColumnIdList 
					--PRINT @SingleBidTableId
					--PRINT @BidTableIdList
				END
				
				
				SELECT 'true' as UpdateStatus
		COMMIT TRAN
	END TRY
	BEGIN CATCH
			Select 'false' as UpdateStatus
			ROLLBACK TRAN
	END CATCH
END


GO
