SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_import_promisdata]
AS
BEGIN
 insert into Tbl_PromisIndicatorHistory([officeId]
           ,[PEOffice_Name]
           ,[tenderId]
           ,[PublishNewspaper]
           ,[PublishCPTUWebsite]
	   ,[GoBProcRules]
	   ,[DPRules]
           ,[MultiLocSubmission]
           ,[DaysBetTenPubTenderClosing]
           ,[SuffTenderSubTime]
           ,[TenderDocSold]
           ,[TenderersPartCount]
           ,[TenderSubVSDocSold]
           ,[TOCMemberFromTEC]
           ,[TECFormedByAA]
           ,[TECExternalMembers]
           ,[EvaluationTime]
           ,[EvalCompletedInTime]
           ,[ResponsiveTenderersCount]
           ,[IsReTendered]
           ,[IsTenderCancelled]
           ,[DaysBtwnTenEvalTenApp]
           ,[IsApprovedByProperAA]
           ,[SubofEvalReportAA]
           ,[AppofContractByAAInTimr]
           ,[TERReviewByOther]
           ,[ContractAppByHighAuth]
           ,[DaysBtwnContappNOA]
           ,[DaysBtwnTenOpenNOA]
           ,[DaysBtwnIFTNOA]
           ,[AwardPubCPTUWebsite]
           ,[AwardedInTenValidityPeriod]
           --- Added 20th Jan 2012 yagnesh ---
           ,[DeliveredInTime]
           ,[LiquidatedDamage]
           ,[FullyCompleted]
           ,[DaysReleasePayment]
           ,[LatePayment]
           ,[InterestPaid]
           ,[ComplaintReceived]
           ,[ResolAwardModification]
           ,[ComplaintResolved]
           ,[RevPanelDecUpheld]
           ,[IsContAmended]
           ,[ContUnresolvDispute]
           ,[DetFraudCorruption]
           --- Added 20th Jan 2012 yagnesh ---
           ,Is_Progress,Is_TenderOnline, modify_date
           ,Ministry_ID,Division_ID,Org_ID
           ,prmsPECode) select
           [officeId]
           ,[PEOffice_Name]
           ,[tenderId]
           ,[PublishNewspaper]
           ,[PublishCPTUWebsite]
	   ,[GoBProcRules]
	   ,[DPRules]
           ,[MultiLocSubmission]
           ,[DaysBetTenPubTenderClosing]
           ,[SuffTenderSubTime]
           ,[TenderDocSold]
           ,[TenderersPartCount]
           ,[TenderSubVSDocSold]
           ,[TOCMemberFromTEC]
           ,[TECFormedByAA]
           ,[TECExternalMembers]
           ,[EvaluationTime]
           ,[EvalCompletedInTime]
           ,[ResponsiveTenderersCount]
           ,[IsReTendered]
           ,[IsTenderCancelled]
           ,[DaysBtwnTenEvalTenApp]
           ,[IsApprovedByProperAA]
           ,[SubofEvalReportAA]
           ,[AppofContractByAAInTimr]
           ,[TERReviewByOther]
           ,[ContractAppByHighAuth]
           ,[DaysBtwnContappNOA]
           ,[DaysBtwnTenOpenNOA]
           ,[DaysBtwnIFTNOA]
           ,[AwardPubCPTUWebsite]
           ,[AwardedInTenValidityPeriod]
           --- Added 20th Jan 2012 yagnesh ---
           ,[DeliveredInTime]
           ,[LiquidatedDamage]
           ,[FullyCompleted]
           ,[DaysReleasePayment]
           ,[LatePayment]
           ,[InterestPaid]
           ,[ComplaintReceived]
           ,[ResolAwardModification]
           ,[ComplaintResolved]
           ,[RevPanelDecUpheld]
           ,[IsContAmended]
           ,[ContUnresolvDispute]
           ,[DetFraudCorruption]
           --- Added 20th Jan 2012 yagnesh ---
           ,Is_Progress,Is_TenderOnline, modify_date,
           Ministry_ID,Division_ID,Org_ID , prmsPECode
from Tbl_PromisIndicator

delete from Tbl_PromisIndicator


INSERT INTO [Tbl_PromisIndicator]
           ([officeId]
           ,[PEOffice_Name]
           ,[tenderId]
           ,[PublishNewspaper]
           ,[PublishCPTUWebsite]
	   ,[GoBProcRules]
	   ,[DPRules]
           ,[MultiLocSubmission]
           ,[DaysBetTenPubTenderClosing]
           ,[SuffTenderSubTime]
           ,[TenderDocSold]
           ,[TenderersPartCount]
           ,[TenderSubVSDocSold]
           ,[TOCMemberFromTEC]
           ,[TECFormedByAA]
           ,[TECExternalMembers]
           ,[EvaluationTime]
           ,[EvalCompletedInTime]
           ,[ResponsiveTenderersCount]
           ,[IsReTendered]
           ,[IsTenderCancelled]
           ,[DaysBtwnTenEvalTenApp]
           ,[IsApprovedByProperAA]
           ,[SubofEvalReportAA]
           ,[AppofContractByAAInTimr]
           ,[TERReviewByOther]
           ,[ContractAppByHighAuth]
           ,[DaysBtwnContappNOA]
           ,[DaysBtwnTenOpenNOA]
           ,[DaysBtwnIFTNOA]
           ,[AwardPubCPTUWebsite]
           ,[AwardedInTenValidityPeriod]
           ,Is_Progress,Is_TenderOnline, modify_date,
           Ministry_ID,Division_ID,Org_ID,
           [prmsPECode]
           --- Added 20th Jan 2012 yagnesh ---
           ,[DeliveredInTime]
           ,[LiquidatedDamage]
           ,[FullyCompleted]
           ,[DaysReleasePayment]
           ,[LatePayment]
           ,[InterestPaid]
           ,[ComplaintReceived]
           ,[ResolAwardModification]
           ,[ComplaintResolved]
           ,[RevPanelDecUpheld]
           ,[IsContAmended]
           ,[ContUnresolvDispute]
           ,[DetFraudCorruption]
           --- Added 20th Jan 2012 yagnesh ---


           )

select officeId,peOfficeName,tenderId,
case
	when
	--(select COUNT(ta.tenderId) from tbl_TenderAdvertisement ta where ta.tenderId = td.tenderId) != 0
	(select 1 as cnt) = 0
	then
	'100'	--Added 100 conditon on 23rdJan2012
	else
	case
		when
		(select COUNT(tenderAdvtId) from tbl_TenderAdvertisement ta
		where newsPaperPubDt is not null and ta.tenderId=td.tenderId ) = 0
		then
		'0'
		else
		'1'
	end
end PublishNewspaper,	--done
1 PublishCPTUWebsite,	--done
case
	when
	(select COUNT(ts.templateId) from tbl_TenderStd ts where ts.tenderId = td.tenderId) = 0
	then
	'100'	--Added 100 conditon on 23rdJan2012
	else
		case
			when
			(select count(tm.stdFor) from tbl_TenderStd ts, tbl_TemplateMaster tm where ts.templateId = tm.templateId
			and ts.tenderId = td.tenderId and tm.stdFor = 'Government of Bangladesh') > 0
			then
			'1'
			else
			'0'
		end
end GoBProcRules,		-- done on 23rdJan2012 yagnesh
case
	when
	(select COUNT(ts.templateId) from tbl_TenderStd ts where ts.tenderId = td.tenderId) = 0
	then
	'100'
	else
		case
			when
			(select count(tm.stdFor) from tbl_TenderStd ts, tbl_TemplateMaster tm where ts.templateId = tm.templateId
			and ts.tenderId = td.tenderId and tm.stdFor = 'Development Partner') > 0
			then
			'1'
			else
			'0'
		end
end DPRules,			-- done on 23rdJan2012 yagnesh
0 MultiLocSubmission,   --done
DATEDIFF(D,tenderPubDt,submissionDt) DaysBetTenPubTenderClosing,	--done
1 SuffTenderSubTime,	--done
(select COUNT(userId) from tbl_TenderPayment tp
where paymentFor='Document Fees' and  status='paid' and isLive='yes' and isVerified='yes'
and tp.tenderId=td.tenderId ) TenderDocSold, --done
(select COUNT(userId) from tbl_FinalSubmission ta
where ta.bidSubStatus='finalsubmission' and ta.tenderId=td.tenderId )
 TenderersPartCount,	--done
convert(varchar(20),(select COUNT(userId) from tbl_FinalSubmission ta
where ta.bidSubStatus='finalsubmission' and ta.tenderId=td.tenderId ) )
+':'+convert(varchar(20),(select COUNT(userId) from tbl_TenderPayment tp where paymentFor='Document Fees' and  status='paid' and isLive='yes' and isVerified='yes' and tp.tenderId=td.tenderId )) TenderSubVSDocSold, --done
1 TOCMemberFromTEC, --done
----------	Commented By Dohatec for accurate result	1 TECFormedByAA,	--done
case when (select COUNT(*) from tbl_ProcurementRole where procurementRoleId =
(select approvingAuthEmpId from tbl_AppPackages where packageId  = (
select packageId from tbl_TenderMaster  where tenderId = 2281))
and procurementRole in ('Minister','CCGP'))> 0
then
case WHEN	(select COUNT(*) from  		(select distinct objectId,userId from tbl_WorkFlowLevelConfig
		where moduleId = 2 and eventId=6 and activityId = 6 and wfRoleId = 2 and objectId in
		(select distinct tenderId from tbl_Committee tc where committeeType IN ( 'TEC','PEC' ) and isCurrent = 'YES'
		and tenderId = td.tenderId)) T where T.objectId=td.tenderId and T.userId = td.approvingAuthId)=1
		THEN '1'
	WHEN	(select COUNT(*) from  (select distinct objectId,userId from tbl_WorkFlowLevelConfig
		where moduleId = 2 and eventId=6 and activityId = 6 and wfRoleId = 2 and procurementRoleId in (2,3) and objectId in
		(select distinct tenderId from tbl_Committee tc where committeeType IN ( 'TEC','PEC' ) and isCurrent = 'YES'
		and tenderId = td.tenderId)) T where T.objectId=td.tenderId)=1
		THEN '1'
ELSE
	'0'
 end
else
case WHEN	(select COUNT(*) from
		(select distinct objectId,userId from tbl_WorkFlowLevelConfig
		where moduleId = 2 and eventId=6 and activityId = 6
		and wfRoleId = 2 and objectId in
		(select distinct tenderId from tbl_Committee tc
		where committeeType IN ( 'TEC','PEC' ) and isCurrent = 'YES'
		and tenderId = td.tenderId)) T
		where T.objectId=td.tenderId and T.userId = td.approvingAuthId)=1
		THEN '1'
ELSE
	'0'
 end
 end TECFormedByAA, -- Modified By Dohatec
--case when
--(select minMemOutSide from tbl_Committee c where c.committeeType in('TEC','PEC')
 --and c.tenderId=td.tenderId GROUP by c.tenderId,minMemOutSide) =0
--then
--'0'
--else
--'1'
--end
case
	when
		(selecT COUNT(cm.comMemberId) from tbl_Committee c, tbl_CommitteeMembers cm
			where c.committeeId = cm.committeeId
			and cm.memberFrom = 'Other PE' and tenderId = td.tenderId and (c.committeeType = 'TEC' or c.committeeType = 'PEC')) >= 2
	then
		'1'
	else
		'0'
end TECExternalMembers, --done
 /*ISNULL( convert(varchar(20),DATEDIFF(d,(
select max(openingDt)
 from tbl_TenderOpenDates tod where tod.tenderId=td.tenderId group by tenderId),
 (select MAX(rptApproveDt) from tbl_EvalRptSentToAA era
where era.tenderId=td.tenderId
group by tenderId ))),'NA') as EvaluationTime,  --done*/ -- commented by dohatec for


/*ISNULL( convert(varchar(20),DATEDIFF(d,(
select max(openingDt)from tbl_TenderOpenDates tod where tod.tenderId=td.tenderId group by tenderId),
	(case when (select COUNT(distinct tcm.userId) from tbl_Committee tc
			inner join tbl_CommitteeMembers tcm
			ON tc.committeeId = tcm.committeeId
			where tc.tenderId = td.tenderid
			and tc.committeeType in ('TEC','PEC'))=
		(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
			where trs.tenderId = td.tenderid and trs.reportType = 'TER4'
			and roundId =(select MAX(roundId) from tbl_TORRptSign
			where tenderId = td.tenderid and reportType = 'TER4')
		) then
		(	select MAX(signedDate) from tbl_TORRptSign trs
			where trs.tenderId = td.tenderid and trs.reportType = 'TER4'
			and roundId = (select MAX(roundId) from tbl_TORRptSign
			where tenderId = td.tenderid and reportType = 'TER4')
		)end
	))),'NA') as EvaluationTime  ------ Commented by dohatec to implement after ter2 signing*/

		ISNULL( convert(varchar(20),DATEDIFF(d,(
select  max(sentDate)from tbl_TosRptShare trs where trs.tenderId=td.tenderId group by tenderId),
	(case	when	(select COUNT(distinct tcm.userId) from tbl_Committee tc
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId
					where tc.tenderId = td.tenderid
					and tc.committeeType in ('TEC','PEC'))=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4'
						and roundId =(select MAX(roundId) from tbl_TORRptSign
						where tenderId = td.tenderid and reportType = 'TER4')
					) then
						(	select MAX(signedDate) from tbl_TORRptSign trs
							where trs.tenderId = td.tenderid and trs.reportType = 'TER4'
							and roundId = (select MAX(roundId) from tbl_TORRptSign
							where tenderId = td.tenderid and reportType = 'TER4')
						)

			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc
					inner join tbl_CommitteeMembers tcm
					ON tc.committeeId = tcm.committeeId
					where tc.tenderId = td.tenderid and td.procurementNatureId != 3
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' and td.procurementNatureId != 3
						and roundId =(select MAX(roundId) from tbl_TORRptSign
						where tenderId = td.tenderid and reportType = 'TER4')
					) then
					(
						case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc
							inner join tbl_CommitteeMembers tcm
							ON tc.committeeId = tcm.committeeId
							where tc.tenderId = td.tenderid and td.procurementNatureId != 3
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
							where trs.tenderId = td.tenderid and trs.reportType = 'TER2' and td.procurementNatureId != 3
							and roundId =(select MAX(roundId) from tbl_TORRptSign
							where tenderId = td.tenderid and reportType = 'TER2')
						) then
						(
							case  when
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid
									and roundId = (select MAX(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER2'))>0
									then
									(select MAX(signedDate) from tbl_TORRptSign trs
									where trs.tenderId = td.tenderid and trs.reportType = 'TER2'
									and roundId = (select MAX(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER2'))
									when
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = 0)>0 and (select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid
									and roundId = (select MAX(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER2'))=0
									then
									(select MAX(signedDate) from tbl_TORRptSign trs
									where trs.tenderId = td.tenderid and trs.reportType = 'TER2'
									and roundId = 0)

							end


						)	end
					)

				when	(select COUNT(distinct tcm.userId) from tbl_Committee tc
					inner join tbl_CommitteeMembers tcm
					ON tc.committeeId = tcm.committeeId
					where tc.tenderId = td.tenderid and td.procurementNatureId = 3
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' and td.procurementNatureId = 3
						and roundId =(select MAX(roundId) from tbl_TORRptSign
						where tenderId = td.tenderid and reportType = 'TER4')
					) then
					(
						case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc
							inner join tbl_CommitteeMembers tcm
							ON tc.committeeId = tcm.committeeId
							where tc.tenderId = td.tenderid and td.procurementNatureId = 3
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
							where trs.tenderId = td.tenderid and trs.reportType = 'TER1' and td.procurementNatureId = 3
							and roundId =(select MAX(roundId) from tbl_TORRptSign
							where tenderId = td.tenderid and reportType = 'TER1')
						) then
						(
							case  when
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid
									and roundId = (select MAX(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER1'))>0
									then
									(select MAX(signedDate) from tbl_TORRptSign trs
									where trs.tenderId = td.tenderid and trs.reportType = 'TER1'
									and roundId = (select MAX(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER1'))
							end


						)	end
					)

					end


	))),'NA') as EvaluationTime,  --done, -- Modified By Dohatec
--case
--	when
--		(select count(ersta.evalRptId) from tbl_EvalRptSentToAA ersta where ersta.tenderId = td.tenderId) = 0
--	then
--		'100'
--	else
--		'1'
--end EvalCompletedInTime,						--done
'0' AS EvalCompletedInTime , --end  -- Modified By Dohatec
case
	when
	(select COUNT(userId) from tbl_EvalBidderStatus eval
	where eval.tenderId=td.tenderId) = 0
	then
	'NA'
	else
	(select convert(varchar(20),COUNT(userId)) from tbl_EvalBidderStatus eval
	where eval.tenderId=td.tenderId and (bidderStatus ='Technically Responsive'
	or result='Pass'))
end ResponsiveTenderersCount,	--done --Modified by  Dohatec
case
	when
	(select count(tenderId) from tbl_PostQualification pq where pq.tenderId=td.tenderId and reTenderRecommendetion='yes') = 1
	then
	'1'
	else
	'0'
end IsReTendered,	--done
case when tenderStatus='Cancelled'
then 1 else 0 end IsTenderCancelled,	--done

 -- change by dohatec
/* ISNULL( convert(varchar(20),DATEDIFF(d,(
select MAX(sentDate) from tbl_EvalRptSentToAA era
where era.tenderId=td.tenderId
group by tenderId),
 (select MAX(rptApproveDt) from tbl_EvalRptSentToAA era
where era.tenderId=td.tenderId
group by tenderId ))),'NA') as DaysBtwnTenEvalTenApp  */

/*
ISNULL( convert(varchar(20),DATEDIFF(d,
	(case	when	(select COUNT(distinct tcm.userId) from tbl_Committee tc
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId
					where tc.tenderId = td.tenderid
					and tc.committeeType in ('TEC','PEC'))=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4'
						and roundId =(select MAX(roundId) from tbl_TORRptSign
						where tenderId = td.tenderid and reportType = 'TER4')
					)
					then
						(	SELECT ISNULL((select MAX(processDate)
							from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
							on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	and wfl.wfRoleId = 1
							where  [action] = 'Forward' and wfl.activityId = 13 and wfh.objectId = td.tenderid) ,
							(	select MAX(sentDate) from tbl_EvalRptSentToAA era
								where era.tenderId=td.tenderid
								and roundId = (select MAX(roundId) from tbl_TORRptSign
								where tenderId = td.tenderid and reportType = 'TER4')
								group by tenderId))
						)


			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc
					inner join tbl_CommitteeMembers tcm
					ON tc.committeeId = tcm.committeeId
					where tc.tenderId = td.tenderid and td.procurementNatureId != 3
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4'
						and roundId =(select MAX(roundId) from tbl_TORRptSign
						where tenderId = td.tenderid and reportType = 'TER4')
					) then
					(
						case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc
							inner join tbl_CommitteeMembers tcm
							ON tc.committeeId = tcm.committeeId
							where tc.tenderId = td.tenderid and td.procurementNatureId != 3
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
							where trs.tenderId = td.tenderid and trs.reportType = 'TER2' and td.procurementNatureId != 3
							and roundId =(select MAX(roundId) from tbl_TORRptSign
							where tenderId = td.tenderid and reportType = 'TER2')
						) then
						(
							case  when
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = (select MAX(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER2'))>0
									then
									(select MAX(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = (select MAX(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER2'))
									when
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = 0)>0 and (select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = (select MAX(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER2'))=0
									then
									(select MAX(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = 0)
							end


						)	end
					)

			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc
					inner join tbl_CommitteeMembers tcm
					ON tc.committeeId = tcm.committeeId
					where tc.tenderId = td.tenderid and td.procurementNatureId = 3
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4'
						and roundId =(select MAX(roundId) from tbl_TORRptSign
						where tenderId = td.tenderid and reportType = 'TER4')
					) then
					(
						case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc
							inner join tbl_CommitteeMembers tcm
							ON tc.committeeId = tcm.committeeId
							where tc.tenderId = td.tenderid and td.procurementNatureId = 3
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
							where trs.tenderId = td.tenderid and trs.reportType = 'TER1'
							and roundId =(select MAX(roundId) from tbl_TORRptSign
							where tenderId = td.tenderid and reportType = 'TER1')
						) then
						(
							case  when
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3
									and roundId = (select MAX(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER1'))>0
									then
									(	SELECT ISNULL((select MAX(processDate)
										from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
										on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId
										and wfl.wfRoleId = 1
										where  [action] = 'Forward' and wfl.activityId = 13
										and wfh.objectId = td.tenderid) ,
									(select MAX(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3
									and roundId = (select MAX(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER1'))))
							end


						)	end
					)

					end
	),(select MAX(rptApproveDt) from tbl_EvalRptSentToAA era
where era.tenderId=td.tenderId
group by tenderId ))),'NA') as DaysBtwnTenEvalTenApp, --Modified By Dohatec*/

case when (select count(distinct envelopeId) from tbl_TenderEnvelopes where tenderId = td.tenderid)=1
then ISNULL( convert(varchar(20),DATEDIFF(d,
	(case	when	(select COUNT(distinct tcm.userId) from tbl_Committee tc
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId
					where tc.tenderId = td.tenderid and tc.committeeType in ('TEC','PEC'))=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4'
						and roundId =(select MIN(roundId) from tbl_TORRptSign
						where tenderId = td.tenderid and reportType = 'TER4')
					)then
						(	SELECT ISNULL((select MIN(processDate)
							from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
							on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	and wfl.wfRoleId = 1
							where  [action] = 'Forward' and wfl.activityId = 13 and wfh.objectId = td.tenderid) ,
							(	select MIN(sentDate) from tbl_EvalRptSentToAA era where era.tenderId=td.tenderid
								and roundId = (select MIN(roundId) from tbl_TORRptSign
								where tenderId = td.tenderid and reportType = 'TER4')
								group by tenderId))
						)
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId
					where tc.tenderId = td.tenderid and td.procurementNatureId != 3
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4'
						and roundId =(select MIN(roundId) from tbl_TORRptSign
						where tenderId = td.tenderid and reportType = 'TER4')
					) then
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId
							where tc.tenderId = td.tenderid and td.procurementNatureId != 3
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
							where trs.tenderId = td.tenderid and trs.reportType = 'TER2' and td.procurementNatureId != 3
							and roundId =(select MIN(roundId) from tbl_TORRptSign
							where tenderId = td.tenderid and reportType = 'TER2')
						) then
						(	case  when
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = (select MIN(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER2'))>0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = (select MIN(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER2'))
									when
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = 0)>0 and (select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = (select MIN(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER2'))=0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = 0)
							end
						)	end
					)
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId
					where tc.tenderId = td.tenderid and td.procurementNatureId = 3
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4'
						and roundId =(select MIN(roundId) from tbl_TORRptSign
						where tenderId = td.tenderid and reportType = 'TER4')
					) then
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId
							where tc.tenderId = td.tenderid and td.procurementNatureId = 3
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
							where trs.tenderId = td.tenderid and trs.reportType = 'TER1'
							and roundId =(select MIN(roundId) from tbl_TORRptSign
							where tenderId = td.tenderid and reportType = 'TER1')
						) then
						(	case  when
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3
									and roundId = (select MIN(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER1'))>0
									then
									(	SELECT ISNULL((select MIN(processDate)
										from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
										on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId
										and wfl.wfRoleId = 1 where  [action] = 'Forward' and wfl.activityId = 13
										and wfh.objectId = td.tenderid) ,
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3
									and roundId = (select MIN(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER1'))))
							end
						)	end
					)
					end
	),(select MIN(rptApproveDt) from tbl_EvalRptSentToAA era
where era.tenderId=td.tenderId
group by tenderId ))),'NA')
when (select count(distinct envelopeId) from tbl_TenderEnvelopes where tenderId = td.tenderid)=2
then
(case when (select count(*) from tbl_EvalRptSentToAA where tenderId = td.tenderId and envelopeId =0 )>0
then
(case when (select count(*) from tbl_EvalRptSentToAA where tenderId = td.tenderId and envelopeId =1 )>0
then
 ISNULL( convert(varchar(20),(DATEDIFF(d,
	(case	when	(select COUNT(distinct tcm.userId) from tbl_Committee tc
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId
					where tc.tenderId = td.tenderid and tc.committeeType in ('TEC','PEC'))=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4'
						and roundId =(select MIN(roundId) from tbl_TORRptSign
						where tenderId = td.tenderid and reportType = 'TER4')
					)
					then
						(	SELECT ISNULL((select MIN(processDate) from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
							on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	and wfl.wfRoleId = 1
							where  [action] = 'Forward' and wfl.activityId = 13 and wfh.processDate > (select MIN(rptApproveDt) from tbl_EvalRptSentToAA era
							where era.tenderId=td.tenderId and envelopeId = 0 group by tenderId) and wfh.objectId = td.tenderid) ,
							(	select MIN(sentDate) from tbl_EvalRptSentToAA era where era.tenderId=td.tenderid  and envelopeId = 1
								and roundId = (select MIN(roundId) from tbl_TORRptSign where tenderId = td.tenderid and reportType = 'TER4')
								group by tenderId))
						)
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc inner join tbl_CommitteeMembers tcm
					ON tc.committeeId = tcm.committeeId where tc.tenderId = td.tenderid and td.procurementNatureId != 3
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs where trs.tenderId = td.tenderid and trs.reportType = 'TER4'
						and roundId =(select MIN(roundId) from tbl_TORRptSign where tenderId = td.tenderid and reportType = 'TER4')
					) then
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc inner join tbl_CommitteeMembers tcm
							ON tc.committeeId = tcm.committeeId where tc.tenderId = td.tenderid and td.procurementNatureId != 3
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
							where trs.tenderId = td.tenderid and trs.reportType = 'TER2' and td.procurementNatureId != 3
							and roundId =(select MIN(roundId) from tbl_TORRptSign
							where tenderId = td.tenderid and reportType = 'TER2')
						) then
						(	case  when
									(select count(distinct tenderId) from tbl_EvalRptSentToAA tersta
									where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = (select MIN(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER2'))>0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 and envelopeId =1
									and roundId = (select MIN(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER2'))
									when
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = 0)>0 and (select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = (select MIN(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER2'))=0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 and envelopeId = 1
									and roundId = 0)
							end
						)	end
					)
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc
					inner join tbl_CommitteeMembers tcm
					ON tc.committeeId = tcm.committeeId
					where tc.tenderId = td.tenderid and td.procurementNatureId = 3
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4'
						and roundId =(select MIN(roundId) from tbl_TORRptSign
						where tenderId = td.tenderid and reportType = 'TER4')
					) then
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId
							where tc.tenderId = td.tenderid and td.procurementNatureId = 3
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
							where trs.tenderId = td.tenderid and trs.reportType = 'TER1'
							and roundId =(select MIN(roundId) from tbl_TORRptSign
							where tenderId = td.tenderid and reportType = 'TER1')
						) then
						(	case  when
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3
									and roundId = (select MIN(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER1'))>0
									then
									(	SELECT ISNULL((select MIN(processDate)
										from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
										on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId
										and wfl.wfRoleId = 1 where  [action] = 'Forward' and wfl.activityId = 13 and wfh.processDate > (select MIN(rptApproveDt) from tbl_EvalRptSentToAA era
										where era.tenderId=td.tenderId and envelopeId = 0 group by tenderId)
										and wfh.objectId = td.tenderid) ,
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3 and tersta.envelopeId = 1
									and roundId = (select MIN(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER1'))))
							end
						)	end
					)
					end
	),(select MIN(rptApproveDt) from tbl_EvalRptSentToAA era
where era.tenderId=td.tenderId and era.envelopeId = 1
group by tenderId )))
+
(DATEDIFF(d,
	(case	when	(select COUNT(distinct tcm.userId) from tbl_Committee tc
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId
					where tc.tenderId = td.tenderid and tc.committeeType in ('TEC','PEC'))=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4'
						and roundId =(select MIN(roundId) from tbl_TORRptSign
						where tenderId = td.tenderid and reportType = 'TER4')
					)
					then
						(	SELECT ISNULL((select MIN(processDate)
							from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
							on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	and wfl.wfRoleId = 1
							where  [action] = 'Forward' and wfl.activityId = 13 and wfh.objectId = td.tenderid) ,
							(	select MIN(sentDate) from tbl_EvalRptSentToAA era
								where era.tenderId=td.tenderid  and era.envelopeId=0
								and roundId = (select MIN(roundId) from tbl_TORRptSign
								where tenderId = td.tenderid and reportType = 'TER4')
								group by tenderId))
						)
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId
					where tc.tenderId = td.tenderid and td.procurementNatureId != 3
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4'
						and roundId =(select MIN(roundId) from tbl_TORRptSign
						where tenderId = td.tenderid and reportType = 'TER4')
					) then
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId
							where tc.tenderId = td.tenderid and td.procurementNatureId != 3
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
							where trs.tenderId = td.tenderid and trs.reportType = 'TER2' and td.procurementNatureId != 3
							and roundId =(select MIN(roundId) from tbl_TORRptSign
							where tenderId = td.tenderid and reportType = 'TER2')
						) then
						(	case  when
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = (select MIN(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER2'))>0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 and tersta.envelopeId =0
									and roundId = (select MIN(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER2'))
									when
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = 0)>0 and (select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = (select MIN(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER2'))=0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 and tersta.envelopeId =0
									and roundId = 0)
							end
						)	end
					)
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId
					where tc.tenderId = td.tenderid and td.procurementNatureId = 3
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4'
						and roundId =(select MIN(roundId) from tbl_TORRptSign
						where tenderId = td.tenderid and reportType = 'TER4')
					) then
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId
							where tc.tenderId = td.tenderid and td.procurementNatureId = 3
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
							where trs.tenderId = td.tenderid and trs.reportType = 'TER1'
							and roundId =(select MIN(roundId) from tbl_TORRptSign
							where tenderId = td.tenderid and reportType = 'TER1')
						) then
						(	case  when
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3
									and roundId = (select MIN(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER1'))>0
									then
									(	SELECT ISNULL((select MIN(processDate)
										from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
										on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId
										and wfl.wfRoleId = 1 where  [action] = 'Forward' and wfl.activityId = 13
										and wfh.objectId = td.tenderid) ,
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3 and tersta.envelopeId =0
									and roundId = (select MIN(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER1'))))
							end
						)	end
					)
					end
	),(select MIN(rptApproveDt) from tbl_EvalRptSentToAA era
where era.tenderId=td.tenderId and era.envelopeId = 0
group by tenderId )))
),'NA')
else
ISNULL( convert(varchar(20),DATEDIFF(d,
	(case	when	(select COUNT(distinct tcm.userId) from tbl_Committee tc
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId
					where tc.tenderId = td.tenderid and tc.committeeType in ('TEC','PEC'))=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4'
						and roundId =(select MIN(roundId) from tbl_TORRptSign
						where tenderId = td.tenderid and reportType = 'TER4')
					)
					then
						(	SELECT ISNULL((select MIN(processDate)
							from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
							on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	and wfl.wfRoleId = 1
							where  [action] = 'Forward' and wfl.activityId = 13 and wfh.objectId = td.tenderid) ,
							(	select MIN(sentDate) from tbl_EvalRptSentToAA era where era.tenderId=td.tenderid  and era.envelopeId=0
								and roundId = (select MIN(roundId) from tbl_TORRptSign
								where tenderId = td.tenderid and reportType = 'TER4')
								group by tenderId))
						)
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId
					where tc.tenderId = td.tenderid and td.procurementNatureId != 3
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4'
						and roundId =(select MIN(roundId) from tbl_TORRptSign
						where tenderId = td.tenderid and reportType = 'TER4')
					) then
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId
							where tc.tenderId = td.tenderid and td.procurementNatureId != 3
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
							where trs.tenderId = td.tenderid and trs.reportType = 'TER2' and td.procurementNatureId != 3
							and roundId =(select MIN(roundId) from tbl_TORRptSign
							where tenderId = td.tenderid and reportType = 'TER2')
						) then
						(	case  when
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = (select MIN(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER2'))>0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 and tersta.envelopeId =0
									and roundId = (select MIN(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER2'))
									when
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = 0)>0 and (select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = (select MIN(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER2'))=0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 and tersta.envelopeId =0
									and roundId = 0)
							end
						)	end
					)
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId
					where tc.tenderId = td.tenderid and td.procurementNatureId = 3
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4'
						and roundId =(select MIN(roundId) from tbl_TORRptSign
						where tenderId = td.tenderid and reportType = 'TER4')
					) then
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId
							where tc.tenderId = td.tenderid and td.procurementNatureId = 3
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs
							where trs.tenderId = td.tenderid and trs.reportType = 'TER1'
							and roundId =(select MIN(roundId) from tbl_TORRptSign
							where tenderId = td.tenderid and reportType = 'TER1')
						) then
						(	case  when
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3
									and roundId = (select MIN(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER1'))>0
									then
									(	SELECT ISNULL((select MIN(processDate)
										from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
										on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId
										and wfl.wfRoleId = 1 where  [action] = 'Forward' and wfl.activityId = 13
										and wfh.objectId = td.tenderid) ,
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3 and tersta.envelopeId =0
									and roundId = (select MIN(roundId) from tbl_TORRptSign
									where tenderId = td.tenderid and reportType = 'TER1'))))
							end
						)	end
					)
					end
	),(select MIN(rptApproveDt) from tbl_EvalRptSentToAA era
where era.tenderId=td.tenderId and era.envelopeId = 0
group by tenderId ))),'NA')
end
)
else 'NA'
end )
else 'NA'
end as DaysBtwnTenEvalTenApp, --Modified By Dohatec
--- Financial Delegation
(case
       WHEN
		(SELECT COUNT(tenderId) FROM tbl_EvalRptSentToAA ea WHERE ea.tenderId = td.tenderId) = 0
	THEN
		'0'
	ELSE
		CASE WHEN
			(SELECT COUNT(*) FROM tbl_WorkFlowFileHistory wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13) = 0
		THEN
			'1'
		ELSE
					(CASE WHEN (select count(*) from tbl_DepartmentMaster where organizationType != 'yes' and
								departmentId in (select distinct departmentId from tbl_tenderdetails where tenderId = td.tenderid))>0
					THEN
						---Corporation Not Applicable
							(CASE WHEN td.projectName IS NOT NULL			
								THEN
									(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =7) or
													(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=7) and 
														((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
									THEN --- CCGP Approver
									(CASE WHEN
									(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
									where (select distinct amount from tbl_BidderRank
									where userId = (select userId from tbl_EvalRoundMaster
									where tenderId = td.tenderId and reportType = 'L1'
									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
									where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
									where tenderId = td.tenderId and reportType = 'L1'))
									>= fd.MinValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
									and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
									and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
									and ApprovingAuthority ='CCGP') T) > 0
										THEN
											'1'
										ELSE
											'0'
										END)
									
									
									
									ELSE 
															(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =3) or
																		(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=3) and 
																			((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																		THEN --- Minister Approver 
																				(CASE WHEN (select ISNULL(COUNT(employeeid),0) from tbl_EmployeeRoles 
																							where procurementRoleId = (select procurementRoleId from tbl_ProcurementRole where procurementRole = 'BOD')
																							and departmentId in (select departmentId from tbl_DepartmentMaster where departmentType='Organization' 
																							and departmentName = (select agency from tbl_TenderDetails where tenderId = td.tenderId)))>0
																							THEN
																								(CASE WHEN
																								(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																								where (select distinct amount from tbl_BidderRank
																								where userId = (select userId from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1'
																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1'))
																								between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																								and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																								and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																								and IsBoD = 'yes'
																								and ApprovingAuthority = 'Minister') T) > 0
																									THEN
																										'1'
																									ELSE
																										'0'																							
																									END)
																				ELSE
																						(CASE WHEN
																								(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																								where (select distinct amount from tbl_BidderRank
																								where userId = (select userId from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1'
																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1'))
																								between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																								and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																								and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																								and IsBoD != 'yes'
																								and ApprovingAuthority = 'Minister') T) > 0
																									THEN
																										'1'
																									ELSE
																										'0'
																									END)
																				END)
																				
																	ELSE
																
																										(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =6) or
																										(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=6) and 
																											((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																										THEN -- HOPE Approver
																												(CASE WHEN (select ISNULL(COUNT(employeeid),0) from tbl_EmployeeRoles 
																															where procurementRoleId = (select procurementRoleId from tbl_ProcurementRole where procurementRole = 'BOD')
																															and departmentId in (select departmentId from tbl_DepartmentMaster where departmentType='Organization' 
																															and departmentName = (select agency from tbl_TenderDetails where tenderId = td.tenderId)))>0
																															THEN
																															(CASE WHEN (SELECT procurementnatureid from tbl_tenderdetails where tenderId =td.tenderId) =2
																																THEN
																																(CASE WHEN
																																(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																where (select distinct amount from tbl_BidderRank
																																where userId = (select userId from tbl_EvalRoundMaster
																																where tenderId = td.tenderId and reportType = 'L1'
																																and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																where tenderId = td.tenderId and reportType = 'L1'))
																																between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																																and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																																and IsBoD = 'yes'
																																and ApprovingAuthority = 'HOPE') T) > 0
																																	THEN
																																		'1'
																																	ELSE
																																		'0'																							
																																	END)
																																	ELSE
																																			(CASE WHEN
																																			(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																			where (select distinct amount from tbl_BidderRank
																																			where userId = (select userId from tbl_EvalRoundMaster
																																			where tenderId = td.tenderId and reportType = 'L1'
																																			and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																			where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																			and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																			where tenderId = td.tenderId and reportType = 'L1'))
																																			between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																																			and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																			and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																																			and IsBoD != 'yes'
																																			and ApprovingAuthority = 'HOPE') T) > 0
																																				THEN
																																					'1'
																																				ELSE
																																					'0'																							
																																				END)																						
																																	END)
																												ELSE
																														(CASE WHEN
																																(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																where (select distinct amount from tbl_BidderRank
																																where userId = (select userId from tbl_EvalRoundMaster
																																where tenderId = td.tenderId and reportType = 'L1'
																																and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																where tenderId = td.tenderId and reportType = 'L1'))
																																between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																																and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																																and IsBoD != 'yes'
																																and ApprovingAuthority = 'HOPE') T) > 0
																																	THEN
																																		'1'
																																	ELSE
																																		'0'
																																	END)
																												END)
																												
																									ELSE
																																---- BOD Approver
																																				(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =4) or
																																				(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=4) and 
																																					((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																																				THEN 																																			
																																						(CASE WHEN
																																							(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																							where (select distinct amount from tbl_BidderRank
																																							where userId = (select userId from tbl_EvalRoundMaster
																																							where tenderId = td.tenderId and reportType = 'L1'
																																							and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																							where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																							and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																							where tenderId = td.tenderId and reportType = 'L1'))
																																							between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																																							and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																							and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																																							and ApprovingAuthority = 'BoD') T) > 0
																																									THEN
																																												'1'
																																									ELSE
																																												'0'																							
																																									END)
																																			ELSE
																																					-- PD Approver
																																							(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =19) or
																																									(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=19) and 
																																										((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																																									THEN 																																			
																																											(CASE WHEN
																																												(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																												where (select distinct amount from tbl_BidderRank
																																												where userId = (select userId from tbl_EvalRoundMaster
																																												where tenderId = td.tenderId and reportType = 'L1'
																																												and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																												where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																												and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																												where tenderId = td.tenderId and reportType = 'L1'))
																																												between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																																												and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																												and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no' 
																																												and dbo.f_get_ProjectCostWiseCheck(td.tenderId,fd.MinProjectValueBDT,fd.MaxProjectValueBDT)=1
																																												and ApprovingAuthority ='PD') T) > 0
																																														THEN
																																																	'1'
																																														ELSE
																																																	'0'																							
																																														END)
																																								ELSE
																																										'0'	
																																							END)
																																					-- PD approver
																																		END)
																																----
																								END)
																END)
										
									END)
						ELSE
							-- Revenue Budget
							(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =7) or
													(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=7) and 
														((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
									THEN --- CCGP Approver
									(CASE WHEN
									(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
									where (select distinct amount from tbl_BidderRank
									where userId = (select userId from tbl_EvalRoundMaster
									where tenderId = td.tenderId and reportType = 'L1'
									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
									where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
									where tenderId = td.tenderId and reportType = 'L1'))
									>= fd.MinValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
									and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
									and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
									and ApprovingAuthority ='CCGP') T) > 0
										THEN
											'1'
										ELSE
											'0'
										END)
									
									
									
									ELSE 
															(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =3) or
																		(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=3) and 
																			((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																		THEN --- Minister Approver 
																				(CASE WHEN (select ISNULL(COUNT(employeeid),0) from tbl_EmployeeRoles 
																							where procurementRoleId = (select procurementRoleId from tbl_ProcurementRole where procurementRole = 'BOD')
																							and departmentId in (select departmentId from tbl_DepartmentMaster where departmentType='Organization' 
																							and departmentName = (select agency from tbl_TenderDetails where tenderId = td.tenderId)))>0
																							THEN
																								(CASE WHEN
																								(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																								where (select distinct amount from tbl_BidderRank
																								where userId = (select userId from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1'
																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1'))
																								between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																								and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																								and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																								and IsBoD = 'yes'
																								and ApprovingAuthority = 'Minister') T) > 0
																									THEN
																										'1'
																									ELSE
																										'0'																							
																									END)
																				ELSE
																						(CASE WHEN
																								(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																								where (select distinct amount from tbl_BidderRank
																								where userId = (select userId from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1'
																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1'))
																								between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																								and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																								and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																								and IsBoD != 'yes'
																								and ApprovingAuthority = 'Minister') T) > 0
																									THEN
																										'1'
																									ELSE
																										'0'
																									END)
																				END)
																				
																	ELSE
																
																										(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =6) or
																										(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=6) and 
																											((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																										THEN -- HOPE Approver																									
																															(CASE WHEN
																																(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																where (select distinct amount from tbl_BidderRank
																																where userId = (select userId from tbl_EvalRoundMaster
																																where tenderId = td.tenderId and reportType = 'L1'
																																and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																where tenderId = td.tenderId and reportType = 'L1'))
																																between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																																and ApprovingAuthority = 'HOPE') T) > 0
																																	THEN
																																		'1'
																																	ELSE
																																		'0'																							
																																	END)
																									ELSE
																																---- BOD Approver
																																				(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =4) or
																																				(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=4) and 
																																					((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																																				THEN 																																			
																																						(CASE WHEN
																																							(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																							where (select distinct amount from tbl_BidderRank
																																							where userId = (select userId from tbl_EvalRoundMaster
																																							where tenderId = td.tenderId and reportType = 'L1'
																																							and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																							where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																							and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																							where tenderId = td.tenderId and reportType = 'L1'))
																																							between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																							and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																							and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																																							and ApprovingAuthority = 'BoD') T) > 0
																																									THEN
																																												'1'
																																									ELSE
																																												'0'																							
																																									END)
																																			ELSE
																																					-- Grade 5 Approver
																																							(CASE WHEN ((select gradeId from tbl_designationmaster where designationid in 
																																										(select designationId from tbl_employeemaster where userid in
																																										(select distinct userId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId and worfklowId = 
																																											(SELECT MAX(worfklowId) FROM tbl_WorkFlowLevelConfig wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13)))) <=5)  and 
																																										((select procurementNatureId from tbl_TenderDetails where  tenderId = td.tenderId) =3) and
																																										((select COUNT(*) from tbl_TenderDetails where  tenderId = td.tenderId and td.procurementMethod in ('IC','SSS','IC')) >0)
																																										THEN 																																			
																																											----Single Source And Individual Consultant
																																											(CASE WHEN
																																																	(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																																	where (select distinct amount from tbl_BidderRank
																																																	where userId = (select userId from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1'
																																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1'))
																																																	between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																																	and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																																	and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																																																	and ApprovingAuthority = 'HOPE') T) > 0
																																																		THEN
																																																			'1'
																																																		ELSE
																																																			'0'																							
																																																		END)
																																											----Single Source And Individual Consultant
																																									WHEN ((select gradeId from tbl_designationmaster where designationid in 
																																										(select designationId from tbl_employeemaster where userid in
																																										(select distinct userId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId and worfklowId = 
																																											(SELECT MAX(worfklowId) FROM tbl_WorkFlowLevelConfig wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13)))) <=5) and 
																																										((select procurementNatureId from tbl_TenderDetails where  tenderId = td.tenderId) =3) and
																																										((select COUNT(*) from tbl_TenderDetails where  tenderId = td.tenderId and td.procurementMethod not in ('IC','SSS','IC')) >0)
																																										THEN 
																																										----Without Single Source And Individual Consultant
																																										(CASE WHEN
																																																	(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																																	where (select distinct amount from tbl_BidderRank
																																																	where userId = (select userId from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1'
																																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1'))
																																																	< fd.MinValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																																	and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																																	and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																																																	and ApprovingAuthority = 'HOPE') T) > 0
																																																		THEN
																																																			'1'
																																																		ELSE
																																																			'0'																							
																																																		END)
																																										----Without Single Source And Individual Consultant
																																										WHEN ((select gradeId from tbl_designationmaster where designationid in 
																																										(select designationId from tbl_employeemaster where userid in
																																										(select distinct userId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId and worfklowId = 
																																											(SELECT MAX(worfklowId) FROM tbl_WorkFlowLevelConfig wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13)))) <=5)  and 
																																										((select procurementNatureId from tbl_TenderDetails where  tenderId = td.tenderId) !=3) and
																																										((select COUNT(*) from tbl_TenderDetails where  tenderId = td.tenderId and td.procurementMethod  in ('RFQ','RFQU','RFQL','DP','LTM')) >0)
																																										THEN 
																																										---For ('RFQ','RFQU','RFQL','DP','LTM')
																																															(CASE WHEN
																																																	(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																																	where (select distinct amount from tbl_BidderRank
																																																	where userId = (select userId from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1'
																																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1'))
																																																	between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																																	and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																																	and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																																																	and ApprovingAuthority = 'HOPE') T) > 0
																																																		THEN
																																																			'1'
																																																		ELSE
																																																			'0'																							
																																																		END)
																																															---For ('RFQ','RFQU','RFQL','DP','LTM')
																																										WHEN ((select gradeId from tbl_designationmaster where designationid in 
																																										(select designationId from tbl_employeemaster where userid in
																																										(select distinct userId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId and worfklowId = 
																																											(SELECT MAX(worfklowId) FROM tbl_WorkFlowLevelConfig wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13)))) <=5) and 
																																										((select procurementNatureId from tbl_TenderDetails where  tenderId = td.tenderId) !=3) and
																																										((select COUNT(*) from tbl_TenderDetails where  tenderId = td.tenderId and td.procurementMethod not in ('RFQ','RFQU','RFQL','DP','LTM')) >0)
																																										
																																										THEN 
																																										---Without ('RFQ','RFQU','RFQL','DP','LTM')
																																															(CASE WHEN
																																																	(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																																	where (select distinct amount from tbl_BidderRank
																																																	where userId = (select userId from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1'
																																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1'))
																																																	< fd.MinValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																																	and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																																	and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																																																	and ApprovingAuthority = 'HOPE') T) > 0
																																																		THEN
																																																			'1'
																																																		ELSE
																																																			'0'																							
																																																		END)
																																															---Without ('RFQ','RFQU','RFQL','DP','LTM')
																																								ELSE
																																										'0'	
																																							END)
																																					-- Grade 5 Approver
																																		END)
																																----
																								END)
																END)
										
									END)
							-- Revenue Budget
						END )
						---Corporation Not Applicable
					ELSE 	
						----Corporation Applicable
								(CASE WHEN td.projectName IS NOT NULL			
									THEN
										(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =7) or
														(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=7) and 
															((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
										THEN --- CCGP Approver
										(CASE WHEN
										(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
										where (select distinct amount from tbl_BidderRank
										where userId = (select userId from tbl_EvalRoundMaster
										where tenderId = td.tenderId and reportType = 'L1'
										and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
										where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
										and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
										where tenderId = td.tenderId and reportType = 'L1'))
										>= fd.MinValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
										and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
										and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
										and ApprovingAuthority ='CCGP') T) > 0
											THEN
												'1'
											ELSE
												'0'
											END)
										
										
										
										ELSE 
																(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =3) or
																			(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=3) and 
																				((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																			THEN --- Minister Approver 
																					(CASE WHEN (select ISNULL(COUNT(employeeid),0) from tbl_EmployeeRoles 
																								where procurementRoleId = (select procurementRoleId from tbl_ProcurementRole where procurementRole = 'BOD')
																								and departmentId in (select departmentId from tbl_DepartmentMaster where departmentType='Organization' 
																								and departmentName = (select agency from tbl_TenderDetails where tenderId = td.tenderId)))>0
																								THEN
																									(CASE WHEN
																									(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																									where (select distinct amount from tbl_BidderRank
																									where userId = (select userId from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1'
																									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1'))
																									between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																									and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																									and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																									and IsBoD = 'yes'
																									and ApprovingAuthority = 'Minister') T) > 0
																										THEN
																											'1'
																										ELSE
																											'0'																							
																										END)
																					ELSE
																							(CASE WHEN
																									(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																									where (select distinct amount from tbl_BidderRank
																									where userId = (select userId from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1'
																									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1'))
																									between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																									and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																									and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																									and IsBoD != 'yes'
																									and ApprovingAuthority = 'Minister') T) > 0
																										THEN
																											'1'
																										ELSE
																											'0'
																										END)
																					END)
																					
																		ELSE
																	
																											(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =6) or
																											(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=6) and 
																												((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																											THEN -- HOPE Approver
																													(CASE WHEN (select ISNULL(COUNT(employeeid),0) from tbl_EmployeeRoles 
																																where procurementRoleId = (select procurementRoleId from tbl_ProcurementRole where procurementRole = 'BOD')
																																and departmentId in (select departmentId from tbl_DepartmentMaster where departmentType='Organization' 
																																and departmentName = (select agency from tbl_TenderDetails where tenderId = td.tenderId)))>0
																																THEN
																																(CASE WHEN (SELECT procurementnatureid from tbl_tenderdetails where tenderId =td.tenderId) =2
																																	THEN
																																	(CASE WHEN
																																	(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																	where (select distinct amount from tbl_BidderRank
																																	where userId = (select userId from tbl_EvalRoundMaster
																																	where tenderId = td.tenderId and reportType = 'L1'
																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																	where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																	where tenderId = td.tenderId and reportType = 'L1'))
																																	between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																																	and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																	and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																																	and IsBoD = 'yes'
																																	and ApprovingAuthority = 'HOPE') T) > 0
																																		THEN
																																			'1'
																																		ELSE
																																			'0'																							
																																		END)
																																		ELSE
																																				(CASE WHEN
																																				(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																				where (select distinct amount from tbl_BidderRank
																																				where userId = (select userId from tbl_EvalRoundMaster
																																				where tenderId = td.tenderId and reportType = 'L1'
																																				and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																				where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																				and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																				where tenderId = td.tenderId and reportType = 'L1'))
																																				between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																																				and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																				and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																																				and IsBoD != 'yes'
																																				and ApprovingAuthority = 'HOPE') T) > 0
																																					THEN
																																						'1'
																																					ELSE
																																						'0'																							
																																					END)																						
																																		END)
																													ELSE
																															(CASE WHEN
																																	(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																	where (select distinct amount from tbl_BidderRank
																																	where userId = (select userId from tbl_EvalRoundMaster
																																	where tenderId = td.tenderId and reportType = 'L1'
																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																	where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																	where tenderId = td.tenderId and reportType = 'L1'))
																																	between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																																	and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																	and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																																	and IsBoD != 'yes'
																																	and ApprovingAuthority = 'HOPE') T) > 0
																																		THEN
																																			'1'
																																		ELSE
																																			'0'
																																		END)
																													END)
																													
																										ELSE
																																	---- BOD Approver
																																					(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =4) or
																																					(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=4) and 
																																						((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																																					THEN 																																			
																																							(CASE WHEN
																																								(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																								where (select distinct amount from tbl_BidderRank
																																								where userId = (select userId from tbl_EvalRoundMaster
																																								where tenderId = td.tenderId and reportType = 'L1'
																																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																								where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																								where tenderId = td.tenderId and reportType = 'L1'))
																																								between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																																								and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																								and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																																								and ApprovingAuthority = 'BoD') T) > 0
																																										THEN
																																													'1'
																																										ELSE
																																													'0'																							
																																										END)
																																				ELSE
																																						-- PD Approver
																																								(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =19) or
																																										(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=19) and 
																																											((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																																										THEN 																																			
																																												(CASE WHEN
																																													(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																													where (select distinct amount from tbl_BidderRank
																																													where userId = (select userId from tbl_EvalRoundMaster
																																													where tenderId = td.tenderId and reportType = 'L1'
																																													and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																													where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																													and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																													where tenderId = td.tenderId and reportType = 'L1'))
																																													between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																																													and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																													and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes' 
																																													and dbo.f_get_ProjectCostWiseCheck(td.tenderId,fd.MinProjectValueBDT,fd.MaxProjectValueBDT)=1
																																													and ApprovingAuthority ='PD') T) > 0
																																															THEN
																																																		'1'
																																															ELSE
																																																		'0'																							
																																															END)
																																									ELSE
																																											'0'	
																																								END)
																																						-- PD approver
																																			END)
																																	----
																									END)
																	END)
											
										END)
							ELSE
								-- Revenue Budget
								(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =7) or
														(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=7) and 
															((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
										THEN --- CCGP Approver
										(CASE WHEN
										(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
										where (select distinct amount from tbl_BidderRank
										where userId = (select userId from tbl_EvalRoundMaster
										where tenderId = td.tenderId and reportType = 'L1'
										and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
										where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
										and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
										where tenderId = td.tenderId and reportType = 'L1'))
										>= fd.MinValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
										and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
										and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
										and ApprovingAuthority ='CCGP') T) > 0
											THEN
												'1'
											ELSE
												'0'
											END)
										
										
										
										ELSE 
																(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =3) or
																			(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=3) and 
																				((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																			THEN --- Minister Approver 
																					(CASE WHEN (select ISNULL(COUNT(employeeid),0) from tbl_EmployeeRoles 
																								where procurementRoleId = (select procurementRoleId from tbl_ProcurementRole where procurementRole = 'BOD')
																								and departmentId in (select departmentId from tbl_DepartmentMaster where departmentType='Organization' 
																								and departmentName = (select agency from tbl_TenderDetails where tenderId = td.tenderId)))>0
																								THEN
																									(CASE WHEN
																									(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																									where (select distinct amount from tbl_BidderRank
																									where userId = (select userId from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1'
																									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1'))
																									between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																									and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																									and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																									and IsBoD = 'yes'
																									and ApprovingAuthority = 'Minister') T) > 0
																										THEN
																											'1'
																										ELSE
																											'0'																							
																										END)
																					ELSE
																							(CASE WHEN
																									(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																									where (select distinct amount from tbl_BidderRank
																									where userId = (select userId from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1'
																									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1'))
																									between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																									and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																									and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																									and IsBoD != 'yes'
																									and ApprovingAuthority = 'Minister') T) > 0
																										THEN
																											'1'
																										ELSE
																											'0'
																										END)
																					END)
																					
																		ELSE
																	
																											(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =6) or
																											(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=6) and 
																												((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																											THEN -- HOPE Approver																									
																																(CASE WHEN
																																	(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																	where (select distinct amount from tbl_BidderRank
																																	where userId = (select userId from tbl_EvalRoundMaster
																																	where tenderId = td.tenderId and reportType = 'L1'
																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																	where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																	where tenderId = td.tenderId and reportType = 'L1'))
																																	between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																	and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																	and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																																	and ApprovingAuthority = 'HOPE') T) > 0
																																		THEN
																																			'1'
																																		ELSE
																																			'0'																							
																																		END)
																										ELSE
																																	---- BOD Approver
																																					(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =4) or
																																					(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=4) and 
																																						((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																																					THEN 																																			
																																							(CASE WHEN
																																								(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																								where (select distinct amount from tbl_BidderRank
																																								where userId = (select userId from tbl_EvalRoundMaster
																																								where tenderId = td.tenderId and reportType = 'L1'
																																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																								where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																								where tenderId = td.tenderId and reportType = 'L1'))
																																								between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																								and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																								and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																																								and ApprovingAuthority = 'BoD') T) > 0
																																										THEN
																																													'1'
																																										ELSE
																																													'0'																							
																																										END)
																																				ELSE
																																						-- Grade 5 Approver
																																								(CASE WHEN ((select gradeId from tbl_designationmaster where designationid in 
																																											(select designationId from tbl_employeemaster where userid in
																																											(select distinct userId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId and worfklowId = 
																																												(SELECT MAX(worfklowId) FROM tbl_WorkFlowLevelConfig wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13)))) <=5)  and 
																																											((select procurementNatureId from tbl_TenderDetails where  tenderId = td.tenderId) =3) and
																																											((select COUNT(*) from tbl_TenderDetails where  tenderId = td.tenderId and td.procurementMethod in ('IC','SSS','IC')) >0)
																																											THEN 																																			
																																												----Single Source And Individual Consultant
																																												(CASE WHEN
																																																		(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																																		where (select distinct amount from tbl_BidderRank
																																																		where userId = (select userId from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1'
																																																		and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																																		and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1'))
																																																		between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																																		and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																																		and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																																																		and ApprovingAuthority = 'HOPE') T) > 0
																																																			THEN
																																																				'1'
																																																			ELSE
																																																				'0'																							
																																																			END)
																																												----Single Source And Individual Consultant
																																										WHEN ((select gradeId from tbl_designationmaster where designationid in 
																																											(select designationId from tbl_employeemaster where userid in
																																											(select distinct userId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId and worfklowId = 
																																												(SELECT MAX(worfklowId) FROM tbl_WorkFlowLevelConfig wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13)))) <=5) and 
																																											((select procurementNatureId from tbl_TenderDetails where  tenderId = td.tenderId) =3) and
																																											((select COUNT(*) from tbl_TenderDetails where  tenderId = td.tenderId and td.procurementMethod not in ('IC','SSS','IC')) >0)
																																											THEN 
																																											----Without Single Source And Individual Consultant
																																											(CASE WHEN
																																																		(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																																		where (select distinct amount from tbl_BidderRank
																																																		where userId = (select userId from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1'
																																																		and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																																		and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1'))
																																																		< fd.MinValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																																		and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																																		and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																																																		and ApprovingAuthority = 'HOPE') T) > 0
																																																			THEN
																																																				'1'
																																																			ELSE
																																																				'0'																							
																																																			END)
																																											----Without Single Source And Individual Consultant
																																											WHEN ((select gradeId from tbl_designationmaster where designationid in 
																																											(select designationId from tbl_employeemaster where userid in
																																											(select distinct userId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId and worfklowId = 
																																												(SELECT MAX(worfklowId) FROM tbl_WorkFlowLevelConfig wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13)))) <=5)  and 
																																											((select procurementNatureId from tbl_TenderDetails where  tenderId = td.tenderId) !=3) and
																																											((select COUNT(*) from tbl_TenderDetails where  tenderId = td.tenderId and td.procurementMethod  in ('RFQ','RFQU','RFQL','DP','LTM')) >0)
																																											THEN 
																																											---For ('RFQ','RFQU','RFQL','DP','LTM')
																																																(CASE WHEN
																																																		(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																																		where (select distinct amount from tbl_BidderRank
																																																		where userId = (select userId from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1'
																																																		and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																																		and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1'))
																																																		between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																																		and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																																		and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																																																		and ApprovingAuthority = 'HOPE') T) > 0
																																																			THEN
																																																				'1'
																																																			ELSE
																																																				'0'																							
																																																			END)
																																																---For ('RFQ','RFQU','RFQL','DP','LTM')
																																											WHEN ((select gradeId from tbl_designationmaster where designationid in 
																																											(select designationId from tbl_employeemaster where userid in
																																											(select distinct userId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId and worfklowId = 
																																												(SELECT MAX(worfklowId) FROM tbl_WorkFlowLevelConfig wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13)))) <=5) and 
																																											((select procurementNatureId from tbl_TenderDetails where  tenderId = td.tenderId) !=3) and
																																											((select COUNT(*) from tbl_TenderDetails where  tenderId = td.tenderId and td.procurementMethod not in ('RFQ','RFQU','RFQL','DP','LTM')) >0)
																																											
																																											THEN 
																																											---Without ('RFQ','RFQU','RFQL','DP','LTM')
																																																(CASE WHEN
																																																		(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																																		where (select distinct amount from tbl_BidderRank
																																																		where userId = (select userId from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1'
																																																		and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																																		and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1'))
																																																		< fd.MinValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																																		and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																																		and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																																																		and ApprovingAuthority = 'HOPE') T) > 0
																																																			THEN
																																																				'1'
																																																			ELSE
																																																				'0'																							
																																																			END)
																																																---Without ('RFQ','RFQU','RFQL','DP','LTM')
																																									ELSE
																																											'0'	
																																								END)
																																						-- Grade 5 Approver
																																			END)
																																	----
																									END)
																	END)
											
										END)
								-- Revenue Budget
							END )
						----Corporation Applicable
					END
					
					)
		END 	
end)
----- Financial deligation
IsApprovedByProperAA, --done /* Modified by Dohatec */
/*case
	when
	(select count(tenderId) from tbl_EvalRptSentToAA ea where ea.tenderId=td.tenderId) = 0
	then
	'100'
	else
	'1'
end SubofEvalReportAA,		--done
*/
case	when (select COUNT(distinct wfFileOnHandId)  from tbl_WorkFlowFileHistory where activityId = 13 
			and wfFileOnHandId =(select MAX(wfFileOnHandId) from tbl_WorkFlowFileHistory 
			where activityId = 13 and objectId = td.tenderId)) = 0
			then case when ((select count(tenderId) from tbl_EvalRptSentToAA ea where ea.tenderId=td.tenderId and rptApproveDt is not  null) > 0) 
			then	'1'	else	'0' end
		 when (select COUNT(distinct wfFileOnHandId)  from tbl_WorkFlowFileHistory where activityId = 13 
			and wfFileOnHandId =(select MAX(wfFileOnHandId) from tbl_WorkFlowFileHistory 
			where activityId = 13 and objectId = td.tenderId)) > 0 
			then 
				case	when (select count(distinct wfRoleid) from tbl_WorkFlowFileHistory wfl 
						inner join tbl_EvalRptSentToAA ersa on wfl.objectId = ersa.tenderId and activityId= 13 and rptStatus !='Pending' and rptApproveDt is not  null
						where   roundId = ( select MAX(roundId) from tbl_EvalRptSentToAA where rptStatus != 'Pending' and rptApproveDt is not  null and tenderId = td.tenderId)
						and tenderId = td.tenderId	GROUP BY objectId)<3 then '1'							
						when (select count(distinct wfRoleid) from tbl_WorkFlowFileHistory wfl 
						inner join tbl_EvalRptSentToAA ersa on wfl.objectId = ersa.tenderId and activityId= 13 and rptStatus !='Pending' and rptApproveDt is not  null
						where   roundId = ( select MAX(roundId) from tbl_EvalRptSentToAA where rptStatus != 'Pending' and rptApproveDt is not  null and tenderId = td.tenderId)
						and tenderId = td.tenderId	GROUP BY objectId)=3 then
							case	when (select COUNT(worfklowId) from tbl_WorkFlowLevelConfig where wfRoleId = 2 and activityId = 13 and procurementRoleId in (3,7) and objectId = td.tenderId) >0 then '1' 
									else
									case when (select COUNT(distinct procurementRoleId) from tbl_WorkFlowLevelConfig where wfRoleId = 3 and activityId = 13 and procurementRoleId not in (7)  and objectId = td.tenderId) > 0  then '0' else '1' end 
							end
				else		
					'0'		
				end 
else	'0' end SubofEvalReportAA, --Modified By Dohatec
/*case
	when
	(select count(tenderId) from tbl_EvalRptSentToAA ea where ea.tenderId=td.tenderId and ea.rptStatus != 'pending') = 0
	then
	'100'
	else
	'1'
end AppofContractByAAInTimr,*/	--done --Modified By Dohatec
'0' as AppofContractByAAInTimr, --done --Modified By Dohatec
--'0' as TERReviewByOther,		--done
case	when 
			(select COUNT(distinct wfFileOnHandId)  from tbl_WorkFlowFileHistory where activityId = 13 
			and wfFileOnHandId =(select MAX(wfFileOnHandId) from tbl_WorkFlowFileHistory 
			where activityId = 13 and objectId = td.tenderId)) > 0 
							then 
							case  when	(select count(distinct wfRoleid) from tbl_WorkFlowFileHistory wfl 
										inner join tbl_EvalRptSentToAA ersa on wfl.objectId = ersa.tenderId 
										and activityId= 13 and rptStatus !='Pending'
										where   roundId = ( select MAX(roundId) from tbl_EvalRptSentToAA where rptStatus != 'Pending' and tenderId = td.tenderId)
										and tenderId = td.tenderId	GROUP BY objectId)=3
										then
											case when (select COUNT(worfklowId) from tbl_WorkFlowLevelConfig where wfRoleId = 2 and activityId = 13 and procurementRoleId in (3,7) and objectId = td.tenderId) >0 then '0' 
											else case when  (select COUNT(distinct procurementRoleId) from tbl_WorkFlowLevelConfig where wfRoleId = 3 and activityId = 13 and procurementRoleId not in (7)  and objectId = td.tenderId) > 0 
															then '1' 
															else '0' 
														end 
											end 	
										else	'0'	end 
else	'0' end as TERReviewByOther,  --done /* Modified By Dohatec*/
--'0' as ContractAppByHighAuth,	--done
case
	WHEN
		(SELECT COUNT(tenderId) FROM tbl_EvalRptSentToAA ea WHERE ea.tenderId = td.tenderId) = 0
	THEN
		'0'
	ELSE
		CASE WHEN
			(SELECT COUNT(*) FROM tbl_WorkFlowFileHistory wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13) = 0
		THEN
			'0'
		ELSE
			(CASE
				WHEN
					--	Actual AA Rank =
					(select RankId from tbl_DelegationDesignationRank
					where ApprovingAuthority = (SELECT procurementRole FROM tbl_ProcurementRole
					WHERE procurementRoleId in (select procurementRoleId from tbl_WorkFlowLevelConfig
					where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId)))

					>

					-- Desire AA Rank =
					(select MIN(RankId) from tbl_DelegationDesignationRank
					where ApprovingAuthority in (select fd.ApprovingAuthority from tbl_FinancialDelegation fd
					where (select distinct(amount) from tbl_BidderRank
					where userId = (select userId from tbl_EvalRoundMaster
					where tenderId = td.tenderId and reportType = 'L1'
					and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
					where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId and roundId = (select MAX(roundId) from tbl_EvalRoundMaster where tenderId = td.tenderId and reportType = 'L1'))
					between fd.MinValueBDT and fd.MaxValueBDT and fd.BudgetType = td.budgetType and fd.ProcurementMethod = td.procurementMethod
					and fd.ProcurementNature = fd.ProcurementNature and fd.ProcurementType = td.procurementType	and fd.TenderType = td.eventType
					and fd.TenderEmergency = td.pkgUrgency
					and dbo.f_get_BoDUserCheck(td.tenderId,fd.IsBoD)=1
					and dbo.f_get_ProjectCostWiseCheck(td.tenderId,fd.MinProjectValueBDT,fd.MaxProjectValueBDT)=1))

				THEN '1'
				ELSE '0'
				END)
		END
end ContractAppByHighAuth, --  /* Modified By Dohatec */

ISNULL( convert(varchar(20),DATEDIFF(d,(
select MIN(rptApproveDt) from tbl_EvalRptSentToAA era
where era.tenderId=td.tenderId group by tenderId),
 (select MIN(createdDt) from tbl_NoaIssueDetails noi
where noi.tenderId=td.tenderId group by tenderId  ))),'NA') as DaysBtwnContappNOA,	--done /* Modified By Dohatec*/
ISNULL( convert(varchar(20),DATEDIFF(d,(
select max(openingDt) from tbl_TenderOpenDates tod where tod.tenderId=td.tenderId group by tenderId),
 (select MIN(createdDt) from tbl_NoaIssueDetails noi
where noi.tenderId=td.tenderId group by tenderId  ))),'NA') as DaysBtwnTenOpenNOA,	--done /* Modified By Dohatec*/
ISNULL( convert(varchar(20),DATEDIFF(d,(
td.tenderPubDt),
 (select MIN(createdDt) from tbl_NoaIssueDetails noi
where noi.tenderId=td.tenderId group by tenderId  ))),'NA') as DaysBtwnIFTNOA, --done /* Modified By Dohatec*/
case
	when
	eventType in('REOI','PQ','1 stage-TSTM')
	then
	'100'
	else
	case
		when
			(selecT COUNT(cs.contractSignId) from tbl_ContractSign cs, tbl_NoaIssueDetails nd, tbl_NoaAcceptance na
			where nd.tenderId = td.tenderId and na.acceptRejStatus = 'approved'
			and nd.noaIssueId = na.noaIssueId and nd.noaIssueId = cs.noaId) = 0
		then
		'100'
		else
		case
			when
			(selecT COUNT(cs.contractSignId) from tbl_ContractSign cs, tbl_NoaIssueDetails nd, tbl_NoaAcceptance na
			where cs.isPubAggOnWeb = 'yes' and nd.tenderId = td.tenderId and na.acceptRejStatus = 'approved'
			and nd.noaIssueId = na.noaIssueId and nd.noaIssueId = cs.noaId) > 0
			then
			'1'
			else
			'0'
		end
	end
end AwardPubCPTUWebsite,		--Added on 24th Jan 2012 yagnesh  /* Modified By Dohatec*/
case
	when
	eventType in('REOI','PQ','1 stage-TSTM')
	then
	'100'
	else
	case
		when
			(selecT COUNT(cs.contractSignId) from tbl_ContractSign cs, tbl_NoaIssueDetails nd, tbl_NoaAcceptance na
			where nd.tenderId = td.tenderId and na.acceptRejStatus = 'approved'
			and nd.noaIssueId = na.noaIssueId and nd.noaIssueId = cs.noaId) = 0
		then
		'100'
		else
		case
			when
			(select count(tenderId) from tbl_TenderValidityExtDate tve
			where tve.tenderId = td.tenderId and tve.extStatus = 'Approved') > 0
			then
			'0'
			else
			'1'
		end
	end
end AwardedInTenValidityPeriod,  -- done  /* Modified By Dohatec*/
case when eventType in('REOI','PQ','1 stage-TSTM')
	then
		case
			when
				((select count(evalRptId) from tbl_EvalRptSentToAA ea
				where rptStatus not in('Pending','Seek Clarification') and ea.tenderid=td.tenderId)>=1)
			then
				'Y'
			else
				'N'
		end
	else
		case
			when
				((select count(contractSignId)
				from tbl_CMS_WcCertificate wc, tbl_ContractSign c, tbl_NoaIssueDetails na
				where c.contractSignId = wc.contractId and na.noaIssueId=c.noaId
				and na.tenderId=td.tenderId and wc.isWorkComplete = 'yes')>=1)
			then
				'N'
			else
				'Y'
		end
		/*case
			when
				((select count(contractSignId) from tbl_ContractSign c,tbl_NoaIssueDetails na
				where na.noaIssueId=c.noaId and na.tenderId=td.tenderId )>=1)
			then
				'Y'
			else
				'N'
		end*/
end Is_Progress,	-- done on 9th Feb 2012
'Y' as Is_TenderOnline,
getdate() modify_date,
/*case
	when
		departmentType='Ministry'
	then
		dm.departmentId
	when
		departmentType='division'
		then
		(select isnull(departmentid,0) from tbl_departmentmaster where departmentid in(select parentDepartmentId from tbl_departmentmaster where departmentId= dm.departmentId)  )
		else
		(select isnull(departmentid,0)  from tbl_departmentmaster where departmentid in(select parentDepartmentId from tbl_departmentmaster where departmentid in(select parentDepartmentId from tbl_departmentmaster where departmentId= dm.departmentId)))
end as ministry_id,
case
	when
		departmentType='division'
	then
		dm.departmentId
	else
		isnull((select isnull(departmentid,0) from tbl_departmentmaster where departmentid in(select parentDepartmentId from tbl_departmentmaster where departmentId= dm.departmentId) and departmenttype='organization'),0)
end as division_id,*/
-- Changed by shreyansh shah to insert minstry id and division id for promis report
case
	when
		departmentType='Ministry'
	then
		dm.departmentId
	when
		departmentType='division'
		then
		(select isnull(departmentid,0) from tbl_departmentmaster where departmentid in(select parentDepartmentId from tbl_departmentmaster where departmentId= dm.departmentId)  )
		else
		case
		when (select td1.departmentType  from tbl_departmentmaster  td1 where departmentid in(select parentDepartmentId from tbl_departmentmaster where departmentid in(select parentDepartmentId from tbl_departmentmaster where departmentId= dm.departmentId))) like 'Cabinet%'
			then
				(select isnull(departmentid,0) from tbl_departmentmaster where departmentid in(select parentDepartmentId from tbl_departmentmaster where departmentId= dm.departmentId)  )
			else
				(select isnull(departmentid,0)  from tbl_departmentmaster where departmentid in(select parentDepartmentId from tbl_departmentmaster where departmentid in(select parentDepartmentId from tbl_departmentmaster where departmentId= dm.departmentId)))
			end
end as ministry_id,
case
	when
		departmentType='division'
	then
		dm.departmentId
	else
		case
		when (select td1.departmentType  from tbl_departmentmaster  td1 where departmentid in (select parentDepartmentId from tbl_departmentmaster where departmentId= dm.departmentId)) like 'Division'
		then
		(select isnull(departmentid,0) from tbl_departmentmaster where departmentid in(select parentDepartmentId from tbl_departmentmaster where departmentId= dm.departmentId) )
		else
		'0'
		end
end as division_id,
case
	when
		departmentType='organization'
	then
		dm.departmentId
	else
		0
end as org_id,
(select prmsPECode from tbl_OfficeMaster om where om.officeName = td.peOfficeName
  and om.departmentId = td.departmentId) as prmsPECode,

--- Added 20th Jan 2012 Yagnesh -------
-- Modified By Dohatec
/*case
	when td.procurementNatureId = 3	--Services
	then
		case
			when td.contractType = 'Lump - Sum'  --Services Lump - Sum
			then
				case
					when
						(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
					then
						'100'
					else
						case
							when	-- check payment schedule fully completed or not
								(select COUNT(srvPSId) from tbl_CMS_SrvPaymentSch sps, tbl_CMS_SrvFormMap sfm
								where sps.srvFormMapId = sfm.srvFormMapId and sps.status = 'pending'
								and sfm.srvBoqId = 12 and sfm.tenderId = td.tenderId) = 0
							then
								case
									when -- check milestone complted on time or not
									(select COUNT(srvPSId) from tbl_CMS_SrvPaymentSch sps, tbl_CMS_SrvFormMap sfm
									where sps.srvFormMapId = sfm.srvFormMapId and CAST(completedDt as DATE) > CAST(peenddate as DATE)
									and sfm.srvBoqId = 12 and sfm.tenderId = td.tenderId) > 0
									then
									'0'
									else
									'1'
								end
							else
								'2'
						end
				end
			else	--Services Time Based
				case
					when
						(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
					then
						'100'
					else	-- in service timebased checking for reimbusable expences
						case
							when -- check is fully completed or not (reimbusable expences)
							(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
							where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
							where a.wpLotId = b.appPkgLotId
							and b.tenderId = td.tenderId) and wd.isRepeatOrder = 'no'
							and wd.wpItemStatus = 'pending') = 0
							then
								case
									when  -- check items delivered on time or not
									(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
									where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
									where a.wpLotId = b.appPkgLotId
									and b.tenderId = td.tenderId) and wd.isRepeatOrder = 'no'
									and CAST (wd.completedDtNTime as date) > CAST (wpenddate as date)
									and wd.completedDtNTime  IS NOT NULL) > 0
									then
									'0'
									else
									'1'
								end
							else
							'2'
						end
				end
		end
	else	--Goods and Works
	case
		when
			(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
		then
			'100'
		else
			case
				when -- check is fully completed or not
				(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
				where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
				where a.wpLotId = b.appPkgLotId
				and b.tenderId = td.tenderId) and wd.isRepeatOrder = 'no'
				and wd.wpItemStatus = 'pending') = 0
				then
					case
						when  -- check items delivered on time or not
						(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
						where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
						where a.wpLotId = b.appPkgLotId
						and b.tenderId = td.tenderId) and wd.isRepeatOrder = 'no'
						and CAST (wd.completedDtNTime as date) > CAST (wpenddate as date)
						and wd.completedDtNTime  IS NOT NULL) > 0
						then
						'0'
						else
						'1'
					end
				else
				'2'
			end
	end
end DeliveredInTime,*/ -- Modified By Dohatec
-- Modified By Dohatec
case
	when td.procurementNatureId = 3	--Services
	then
		case
			when td.contractType = 'Lump - Sum'  --Services Lump - Sum
			then
				case
					when
						(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
					then
						'100'
					else
						case
							when	-- check payment schedule fully completed or not
								(select COUNT(srvPSId) from tbl_CMS_SrvPaymentSch sps, tbl_CMS_SrvFormMap sfm
								where sps.srvFormMapId = sfm.srvFormMapId and sps.status = 'pending'
								and sfm.srvBoqId = 12 and sfm.tenderId = td.tenderId) = 0
							then
								case
									when -- check milestone complted on time or not
									(select COUNT(srvPSId) from tbl_CMS_SrvPaymentSch sps, tbl_CMS_SrvFormMap sfm
									where sps.srvFormMapId = sfm.srvFormMapId and CAST(completedDt as DATE) > CAST(peenddate as DATE)
									and sfm.srvBoqId = 12 and sfm.tenderId = td.tenderId) > 0
									then
									'0'
									else
									'1'
								end
							else
								case when (select distinct paymentTerms from tbl_ContractSign cs inner join tbl_NoaIssueDetails nid
									on cs.noaId=nid.noaIssueId and nid.tenderId = td.tenderId) = 'allitem100p'
									then
									'100'
									when (select distinct paymentTerms from tbl_ContractSign cs inner join tbl_NoaIssueDetails nid
									on cs.noaId=nid.noaIssueId and nid.tenderId = td.tenderId) = 'itemwise100p'
									then

												case
													when	-- check payment schedule fully completed or not
														(select COUNT(srvPSId) from tbl_CMS_SrvPaymentSch sps, tbl_CMS_SrvFormMap sfm
														where sps.srvFormMapId = sfm.srvFormMapId and sps.status = 'completed'
														and sfm.srvBoqId = 12 and sfm.tenderId = td.tenderId) > 0
													then
														case
															when -- check milestone complted on time or not
															(select COUNT(srvPSId) from tbl_CMS_SrvPaymentSch sps, tbl_CMS_SrvFormMap sfm
															where sps.srvFormMapId = sfm.srvFormMapId and sps.status = 'completed' and CAST(completedDt as DATE) > CAST(peenddate as DATE)
															and sfm.srvBoqId = 12 and sfm.tenderId = td.tenderId) > 0
															then
															'0'
															else
															'1'
														end

												else
												'100'
												end
										else
										case
															when -- check milestone complted on time or not
															(select COUNT(srvPSId) from tbl_CMS_SrvPaymentSch sps, tbl_CMS_SrvFormMap sfm
															where sps.srvFormMapId = sfm.srvFormMapId and CAST(completedDt as DATE) > CAST(peenddate as DATE)
															and sfm.srvBoqId = 12 and sfm.tenderId = td.tenderId) > 0
															then
															'0'
															else
															'1'
														end

									end
						end
				end
			else	--Services Time Based
				case
					when
						(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
					then
						'100'
					else	-- in service timebased checking for reimbusable expences
						case
							when -- check is fully completed or not (reimbusable expences)
							(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
							where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
							where a.wpLotId = b.appPkgLotId
							and b.tenderId = td.tenderId) and wd.isRepeatOrder = 'no'
							and wd.wpItemStatus = 'pending') = 0
							then
								case
									when  -- check items delivered on time or not
									(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
									where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
									where a.wpLotId = b.appPkgLotId
									and b.tenderId = td.tenderId) and wd.isRepeatOrder = 'no'
									and CAST (wd.completedDtNTime as date) > CAST (wpenddate as date)
									and wd.completedDtNTime  IS NOT NULL) > 0
									then
									'0'
									else
									'1'
								end
							else
							case when (select distinct paymentTerms from tbl_ContractSign cs inner join tbl_NoaIssueDetails nid
							on cs.noaId=nid.noaIssueId and nid.tenderId = td.tenderId) = 'allitem100p'
							then
							'100'
							when (select distinct paymentTerms from tbl_ContractSign cs inner join tbl_NoaIssueDetails nid
							on cs.noaId=nid.noaIssueId and nid.tenderId = td.tenderId) = 'itemwise100p'
							then
							case
								when -- check is fully completed or not (reimbusable expences)
								(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
								where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
								where a.wpLotId = b.appPkgLotId
								and b.tenderId = td.tenderId) and wd.isRepeatOrder = 'no'
								and wd.wpItemStatus = 'completed') > 0
								then
								case
									when  -- check items delivered on time or not
									(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
									where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
									where a.wpLotId = b.appPkgLotId
									and b.tenderId = td.tenderId)  and wpItemStatus = 'completed'
									and wd.isRepeatOrder = 'no'
									and CAST (wd.completedDtNTime as date) > CAST (wpenddate as date)
									and wd.completedDtNTime  IS NOT NULL) > 0
									then
									'0'
									else
									'1'
								end
							end
							else

										case
											when  -- check items delivered on time or not
											(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
											where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
											where a.wpLotId = b.appPkgLotId
											and b.tenderId = td.tenderId)
											and wd.isRepeatOrder = 'no'
											and CAST (wd.completedDtNTime as date) > CAST (wpenddate as date)
											and wd.completedDtNTime  IS NOT NULL) > 0
											then
											'0'
											else
											'1'
										end

									end


						end
				end
		end
	else	--Goods and Works
	case
		when
			(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
		then
			'100'
		else
			case
				when -- check is fully completed or not
				(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
				where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
				where a.wpLotId = b.appPkgLotId
				and b.tenderId = td.tenderId) and wd.isRepeatOrder = 'no'
				and wd.wpItemStatus = 'pending') = 0
				then
					case
						when  -- check items delivered on time or not
						(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
						where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
						where a.wpLotId = b.appPkgLotId
						and b.tenderId = td.tenderId) and wd.isRepeatOrder = 'no'
						and CAST (wd.completedDtNTime as date) > CAST (wpenddate as date)
						and wd.completedDtNTime  IS NOT NULL) > 0
						then
						'0'
						else
						'1'
					end
				else
						case when (select distinct paymentTerms from tbl_ContractSign cs inner join tbl_NoaIssueDetails nid
							on cs.noaId=nid.noaIssueId and nid.tenderId = td.tenderId) = 'allitem100p'
							then
							'100'
							when (select distinct paymentTerms from tbl_ContractSign cs inner join tbl_NoaIssueDetails nid
							on cs.noaId=nid.noaIssueId and nid.tenderId = td.tenderId) = 'itemwise100p'
							then
							case
								when -- check is fully completed or not (reimbusable expences)
								(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
								where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
								where a.wpLotId = b.appPkgLotId
								and b.tenderId = td.tenderId) and wd.isRepeatOrder = 'no'
								and wd.wpItemStatus = 'completed') > 0
								then
								case
									when  -- check items delivered on time or not
									(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
									where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
									where a.wpLotId = b.appPkgLotId
									and b.tenderId = td.tenderId)  and wpItemStatus = 'completed'
									and wd.isRepeatOrder = 'no'
									and CAST (wd.completedDtNTime as date) > CAST (wpenddate as date)
									and wd.completedDtNTime  IS NOT NULL) > 0
									then
									'0'
									else
									'1'
								end
								else
								'100'

							end
							else

										case
											when  -- check items delivered on time or not
											(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
											where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
											where a.wpLotId = b.appPkgLotId
											and b.tenderId = td.tenderId)
											and wd.isRepeatOrder = 'no'
											and CAST (wd.completedDtNTime as date) > CAST (wpenddate as date)
											and wd.completedDtNTime  IS NOT NULL) > 0
											then
											'0'
											else
											'1'
										end

									end

			end
	end
end DeliveredInTime,
case
	when -- Dates configured
		(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
	then
		'100'
	else
		case
			when
			(select count(iad.ldAmt) from tbl_CMS_InvoiceAccDetails iad
			where iad.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
			where a.wpLotId = b.appPkgLotId
			and b.tenderId = td.tenderId) and iad.ldAmt > 0) > 0
			then
			'1'
			else
			'0'
		end
end LiquidatedDamage,
case
	when td.procurementNatureId = 3		--Services
	then
		case
			when td.contractType = 'Lump - Sum'  --Services Lump - Sum
			then
			case
				when
					(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
				then
					'100'
				else
					case
						when
						(select COUNT(srvPSId) from tbl_CMS_SrvPaymentSch sps, tbl_CMS_SrvFormMap sfm
						where sps.srvFormMapId = sfm.srvFormMapId and sps.status = 'pending'
						and sfm.srvBoqId = 12 and sfm.tenderId = td.tenderId) = 0
						then
						'1'
						else
						'0'
					end
			end
			else	--Services Time Based
				case
					when
						(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
					then
						'100'
					else
						case
							when -- check is fully completed or not (reimbusable expences)
							(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
							where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
							where a.wpLotId = b.appPkgLotId
							and b.tenderId = td.tenderId) and wd.isRepeatOrder = 'no'
							and wd.wpItemStatus = 'pending') = 0
							then
								'1'
							else
								'0'
						end
				end
		end
	else	--Goods and Works
		case
			when
				(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
			then
				'100'
			else
				case
					when
					(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
					where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
					where a.wpLotId = b.appPkgLotId
					and b.tenderId = td.tenderId) and wd.isRepeatOrder = 'no'
					and wd.wpItemStatus = 'pending') = 0
					then
					'1'
					else
					'0'
				end
		end
end FullyCompleted,
case
	when -- Dates configured
		(select count(invoiceId) from tbl_CMS_InvoiceMaster cdc where cdc.tenderId = td.tenderId) = 0
	then
		'NA'
	else
(select ISNULL(convert(varchar(20),AVG(abc.noOfDays)),'0') from (select ISNULL(DATEDIFF(d,(im.createdDate),(im.releasedDtNTime)),0) noOfDays
from tbl_CMS_InvoiceMaster im where im.tenderId = td.tenderId) abc where abc.noOfDays != 0)
end as DaysReleasePayment,		--done - Added on 24th Jan 2012
case
	when -- Dates configured
		(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
	then
		'100'
	else
		case
			when
			(select count(iad.ldAmt) from tbl_CMS_InvoiceAccDetails iad
			where iad.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
			where a.wpLotId = b.appPkgLotId and b.tenderId = td.tenderId) and iad.interestonDP > 0) > 0
			then
			--'1'
			CONVERT(varchar,(select count(iad.ldAmt) from tbl_CMS_InvoiceAccDetails iad
			where iad.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
			where a.wpLotId = b.appPkgLotId and b.tenderId = td.tenderId) and iad.interestonDP > 0))
			else
			'0'
		end
end	LatePayment,
case
	when -- Dates configured
		(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
	then
		'100'
	else
		case
			when
			(select count(iad.ldAmt) from tbl_CMS_InvoiceAccDetails iad
			where iad.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
			where a.wpLotId = b.appPkgLotId and b.tenderId = td.tenderId) and iad.interestonDP > 0) > 0
			then
			'1'
			else
			'0'
		end
end	InterestPaid,
case
	when
	(select COUNT(complaintId) from tbl_CMS_ComplaintMaster where tenderId = td.tenderId) > 0
	then
	'1'
	else
	'0'
end ComplaintReceived,
/*
case
	when (select 1 as abc) > 0
	--uncomment below query after adding "isAwardModified" column in tbl_ContractSign

	(selecT COUNT(cs.contractSignId) from tbl_ContractSign cs, tbl_NoaIssueDetails nd, tbl_NoaAcceptance na
	where cs.isAwardModified = 'yes' and nd.tenderId = td.tenderId and na.acceptRejStatus = 'approved'
	and nd.noaIssueId = na.noaIssueId and nd.noaIssueId = cs.noaId) > 0

	then
	'1'
	else
	'0'
end
*/
--'0' ResolAwardModification, --done
case
when
(select COUNT(complaintId) from tbl_CMS_ComplaintMaster where tenderId = td.tenderId and complaintTypeId = 6 and complaintLevelId = 4 and complaintStatus = 'accept' ) >0
then '1'
else
	'0'
end ResolAwardModification, --done
case
	when
	(select COUNT(complaintId) from tbl_CMS_ComplaintMaster
	where tenderId = td.tenderId and
	(complaintStatus = 'accept' or (complaintStatus = 'reject' and complaintLevelId = 4))) > 0
	then
	'1'
	else
		case
			when
				(select COUNT(complaintId) from tbl_CMS_ComplaintMaster
				where tenderId = td.tenderId and (complaintStatus = 'pending' or complaintStatus = 'Clarification')) > 0
			then
				'100'
			else
				'0'
		end
end ComplaintResolved,	--done on 9th Feb 2012 if at level 4 rejected then resolved
case
	when
		(selecT COUNT(complaintId) from tbl_CMS_ComplaintMaster
		where complaintLevelId = 4 and panelId != 0 and (complaintStatus = 'pending' OR complaintStatus = 'Clarification') and tenderId = td.tenderId) > 0
	then
		'100'
	else
		case
			when
				(selecT COUNT(complaintId) from tbl_CMS_ComplaintMaster
				where complaintLevelId = 4 and panelId != 0 and (complaintStatus != 'pending' or complaintStatus != 'Clarification') and tenderId = td.tenderId) > 0
			then
				'1'
			else
				'0'
		end
end RevPanelDecUpheld,
case
when eventType in('REOI','PQ','1 stage-TSTM')
	then '0'
else
	case when td.procurementNatureId = 1
	then
	'0'
	else
		case
			when
				td.procurementNatureId = 2
			then
				case
					when
						(select COUNT(variOrdId) from tbl_CMS_VariationOrder
						where wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
						where a.wpLotId = b.appPkgLotId and b.tenderId = td.tenderId) and variOrdStatus = 'accepted') > 0
					then
						'1'
					else
						case
							when
								(select COUNT(variOrdId) from tbl_CMS_VariationOrder
								where wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b
								where a.wpLotId = b.appPkgLotId and b.tenderId = td.tenderId) and variOrdStatus != 'accepted') > 0
							then
								'100'
							else
								'0'
						end
				end
			else
				case
					when
						(select COUNT(variOrdId) from tbl_CMS_VariationOrder
						where tenderId = td.tenderId and variOrdStatus = 'accepted') > 0
					then
						'1'
					else
						case
							when
								(select COUNT(variOrdId) from tbl_CMS_VariationOrder
								where tenderId = td.tenderId and variOrdStatus != 'accepted') > 0
							then
								'100'
							else
								'0'
						end
				end
		end
		end
end IsContAmended,
/*case
	when
		(selecT COUNT(complaintId) from tbl_CMS_ComplaintMaster
		where complaintTypeId = 6 and (complaintStatus = 'pending' or complaintStatus = 'Clarification') and tenderId = td.tenderId) > 0
	then
		'1'
	else
		case
			when
				(selecT COUNT(complaintId) from tbl_CMS_ComplaintMaster
				where complaintTypeId = 6 and tenderId = td.tenderId) > 0
			then
				'1'
			else
				'0'
		end
end
*/
case
	when
		(select COUNT(complaintId) from tbl_CMS_ComplaintMaster
		where tenderId = td.tenderId and complaintTypeId = 6) = 0
	then
		'0'
	else
		case
			when
				(select COUNT(complaintId) from tbl_CMS_ComplaintMaster
				where tenderId = td.tenderId and complaintTypeId = 6) >= 1
			then
				case
					when
						(select COUNT(complaintId) from tbl_CMS_ComplaintMaster
						where tenderId = td.tenderId and complaintTypeId = 6 and
						(complaintStatus = 'pending' or complaintStatus = 'Clarification'))	>= 1
					then
						'100'
					else
						case
							when
								(select COUNT(complaintId) from tbl_CMS_ComplaintMaster
								where tenderId = td.tenderId and complaintTypeId = 6 and
								(complaintStatus = 'accept'
								or (complaintStatus = 'reject' and complaintLevelId = 4))) >= 1
							then
								'0'
							else
								'1'
						end
				end
		end
		/*case
			when	--got at least one complaint with resolved status for contract
				(select COUNT(complaintId) from tbl_CMS_ComplaintMaster
				where tenderId = td.tenderId and complaintTypeId = 6
				and (complaintStatus = 'accept' or (complaintStatus = 'reject' and complaintLevelId = 4))) > 0
			then
				'0'
			else
				'1'
		end*/
end ContUnresolvDispute,	--Added on 23rdJan2012 yagnesh
'0' DetFraudCorruption		--Default No
--- Added 20th Jan 2012 Yagnesh -------

from tbl_TenderDetails td,tbl_DepartmentMaster dm

where tenderStatus not in('Pending')
and td.departmentId=dm.departmentId
and submissionDt < GETDATE();

--- INDICATOR 15
WITH CTE1 AS (select tp.tenderid , CASE when EvaluationTime  ='NA' then '0'
else
case when EvaluationTime <=
(case  when (select count(worfklowId) from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId)>0
then (case when (select COUNT(committeeId) from tbl_Committee where committeeType = 'TSC' and committeStatus ='approved' and tenderId = td.tenderid) >0
	then ( case when (SELECT procurementRole FROM tbl_ProcurementRole WHERE procurementRoleId in ( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId))='CCGP'
	then (case when (select DATEDIFF (D, (select max(publishDate) from tbl_Committee where committeeType = 'TSC' and committeStatus ='approved' and tenderId = 2235),(select max(sentDt) from tbl_EvalTSCNotification where tenderId = 2235)))>21
		then
		(select ISNULL( MAX(TotalNoOfDaysForEvaluation),-2) from tbl_ProcurementApprovalTimeline pat inner join
		( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
		( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId))T
			on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 21) ---- Complex TSC REQURED
		else
		(select ISNULL( MIN(TotalNoOfDaysForEvaluation),-2) from tbl_ProcurementApprovalTimeline pat inner join
		( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
		( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId))T
			on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 0) ---- Simple TSC REQURED
		end	)	-- CCGP Approver
	else
	( case when
	(select count(TotalNoOfDaysForEvaluation) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId))T
		on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 0)>0
	then
	(select ISNULL( TotalNoOfDaysForEvaluation,-2) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId))T
		on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 0)
	 else 0 -- 'TSC REQUIRED BUT APPROVING AUTHORITY IS NOT CONFIGURED Contract Approval Workflow is Created'
	 end ) 	 -- CCGP NOT Approver
	end) --TSC REQUIRED

	else
	( case when (SELECT procurementRole FROM tbl_ProcurementRole WHERE procurementRoleId in ( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId))='CCGP'
	then
	(select ISNULL(MIN(TotalNoOfDaysForEvaluation),-2) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId))T
		on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC = 0) -- CCGP APPROVER
	else
	( case when (select count( TotalNoOfDaysForEvaluation) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId))T
		on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC = 0) >0
	then
	(select ISNULL(TotalNoOfDaysForEvaluation,-2) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId))T
		on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC = 0)
	else 0-- 'TSC NOT REQUIRED BUT APPROVING AUTHORITY IS NOT CONFIGURED  Contract Approval Workflow is Created'
	end) -- CCGP NOT APPROVER
	end) --TSC NOT REQUIRED
	end)	-- Contract Approval Workflow is Created
else
 (case when (select COUNT(committeeId) from tbl_Committee where committeeType = 'TSC' and committeStatus ='approved' and tenderId = td.tenderid) >0
	then ( case when (SELECT procurementRole FROM tbl_ProcurementRole WHERE procurementRoleId in ( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in ( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = td.tenderid)))='CCGP'
	then
	( case when  (select DATEDIFF (D, (select max(publishDate) from tbl_Committee where committeeType = 'TSC' and committeStatus ='approved' and tenderId = 2235),(select max(sentDt) from tbl_EvalTSCNotification where tenderId = 2235)))>21
	then (select ISNULL( MAX(TotalNoOfDaysForEvaluation),-2) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in
	( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = td.tenderid)))T
	on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 0)
	else
	(select ISNULL( MIN(TotalNoOfDaysForEvaluation),-2) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in
	( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = td.tenderid)))T
		on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 0)
	 end )
	when (SELECT procurementRole FROM tbl_ProcurementRole WHERE procurementRoleId in ( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in ( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = td.tenderid)))!='CCGP'
	then
	(case when  (select COUNT(TotalNoOfDaysForEvaluation) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in ( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in
	( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = td.tenderid)))T	on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 0) >0
	then
	(select ISNULL( TotalNoOfDaysForEvaluation,-2) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in
	( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = td.tenderid)))T
		on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 0)
	else 0 -- 'TSC  REQUIRED BUT APPROVING AUTHORITY IS NOT CONFIGURED  Contract Approval Workflow is NOt Created'
	end)
	end) -- 'TSC  REQUIRED
	else
	( case when (SELECT procurementRole FROM tbl_ProcurementRole WHERE procurementRoleId in ( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in ( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = td.tenderid)))='CCGP'
	then	(select ISNULL( MIN(TotalNoOfDaysForEvaluation),-2) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in
	( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = td.tenderid)))T
		on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC = 0) -- CCGP APPROVER
	else
	( case when
	(select COUNT(TotalNoOfDaysForEvaluation) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in
	( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = td.tenderid)))T
		on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC = 0)>0
	then
	(select ISNULL( TotalNoOfDaysForEvaluation,-2) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in
	( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = td.tenderid)))T
		on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC = 0)
	else 0 --'TSC  NOT REQUIRED BUT APPROVING AUTHORITY IS NOT CONFIGURED  Contract Approval Workflow is NOt Created'
	end	)  -- CCGP NOT APPROVER
	end) -- TSC NOT REQUIRED
	end) ---Contract workflow not created
end) then '1'
else '0'
end
end AS EvalCompletedInTimeLine
from Tbl_PromisIndicator tp inner join tbl_TenderDetails td on tp.tenderId=td.tenderId)
UPDATE Tbl_PromisIndicator SET EvalCompletedInTime = CTE1.EvalCompletedInTimeLine FROM CTE1 WHERE CTE1.tenderId = Tbl_PromisIndicator.tenderId;;


-- Indicator 22
WITH CTE2 AS (select tp.tenderId,
case when DaysBtwnTenEvalTenApp = 'NA'   Then '0'
	else (case	when (select count(worfklowId) from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = tp.tenderId)>0
				then ( ----------------- Contract Approval Workflow is Created
					case	when (SELECT procurementRole FROM tbl_ProcurementRole WHERE procurementRoleId in ( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = tp.tenderId))='CCGP'
							then (case when (select ISNULL(MIN(rptApproveDt),(select MIN(rptApproveDt) from tbl_EvalRptSentToAA where tenderId = tp.tenderId and envelopeId = 0 )) from tbl_EvalRptSentToAA where tenderId = tp.tenderId and envelopeId = 1)<=convert(date,dbo.f_gettendervaliditydate(tp.tenderId))
										then '1'	else '0'	end	)  ----------------- APPROVING AUTHORITY IS CCGP
							else ( ----------------- APPROVING AUTHORITY IS NOT CCGP
									case	when (select COUNT(committeeId) from tbl_Committee where committeeType = 'TSC' and committeStatus ='approved' and tenderId = tp.tenderid) >0
											then ( 	case when (select ISNULL(NoOfDaysForApproval,0) from tbl_ProcurementApprovalTimeline pat inner join
															( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
															( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = tp.tenderId))T
															on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 0)>=DaysBtwnTenEvalTenApp
													then 	'1' else '0' -- 'TSC REQUIRED'
												end)
											else  (	case when (select ISNULL( NoOfDaysForApproval,0) from tbl_ProcurementApprovalTimeline pat inner join
															( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
															( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = tp.tenderId))T
															on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC = 0) >=DaysBtwnTenEvalTenApp
													then '1'	else '0' -- 'TSC NOT REQUIRED'
												end)
										end ) ----------------- APPROVING AUTHORITY IS NOT CCGP
				end )----------------- Contract Approval Workflow is Created
				else (----------------- Contract Approval Workflow is Not Created
					case	when (SELECT procurementRole FROM tbl_ProcurementRole WHERE procurementRoleId in ( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in ( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = tp.tenderid)))='CCGP'
							then (case when (select ISNULL(MIN(rptApproveDt),(select MIN(rptApproveDt) from tbl_EvalRptSentToAA where tenderId = tp.tenderId and envelopeId = 0 )) from tbl_EvalRptSentToAA where tenderId = tp.tenderId and envelopeId = 1)<=convert(date,dbo.f_gettendervaliditydate(tp.tenderId))
								then '1'	else '0'	end	) ----------------- APPROVING AUTHORITY IS CCGP
							else (	 ----------------- APPROVING AUTHORITY IS NOT CCGP
									case	when (select COUNT(committeeId) from tbl_Committee where committeeType = 'TSC' and committeStatus ='approved' and tenderId = tp.tenderid) >0
											then ( 	case when	( SELECT ISNULL(NoOfDaysForApproval,0) from tbl_ProcurementApprovalTimeline pat inner join
																( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in ( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in
																( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = tp.tenderid)))T	on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 0) >=DaysBtwnTenEvalTenApp
														then '1'	else '0' -- 'TSC  REQUIRED'
											end)

									else
									(  case when	(select ISNULL(NoOfDaysForApproval,0) from tbl_ProcurementApprovalTimeline pat inner join
													( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
													( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in
													( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = tp.tenderid)))T
														on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC = 0)>=DaysBtwnTenEvalTenApp
													then '1'	else '0' --'TSC NOT REQUIRED'
									end	)
							end) ----------------- APPROVING AUTHORITY IS NOT CCGP
					end )----------------- Contract Approval Workflow is Not Created
	end )
end AS ApprovalByAAInTime
from Tbl_PromisIndicator tp inner join tbl_TenderDetails td on tp.tenderId=td.tenderId)

UPDATE Tbl_PromisIndicator SET AppofContractByAAInTimr = ApprovalByAAInTime FROM CTE2 WHERE CTE2.tenderId = Tbl_PromisIndicator.tenderId;


--- IINDICATOR 42

UPDATE Tbl_PromisIndicator set DetFraudCorruption = debarmentNumber FROM (
SELECT DebarOfficeID , MintenderId ,COUNT(debarmentId) debarmentNumber from
(SELECT debarmentId,T.OfficeID DebarOfficeID ,ApproveDate , min(tenderId) MintenderId from
(SELECT debarmentId,T.OfficeID ,ApproveDate ,creationDate,tenderId  from
( SELECT debarmentId,OfficeID , MAX(entryDt) ApproveDate FROM
(SELECT d.debarmentId ,dbo.f_UserWiseOfficeID(debarmentBy) OfficeID,  dc.entryDt from tbl_DebarmentReq d
inner join tbl_DebarmentComments dc on d.debarmentId = dc.debarmentId
where debarmentStatus in ('appdebaregp','appdebarhope','appdebarcomhope')
and debarmentType in ('Corrupt Practice','Fraudulent Practice') )T
GROUP BY debarmentId,OfficeID) T
INNER JOIN (select tpi.tenderid, tpi.officeId , tm.creationDate from
tbl_promisindicator tpi inner join tbl_TenderMaster tm on tpi.tenderId=tm.tenderId) T1
on T.OfficeID=  t1.OfficeID where T1.creationDate >= T. ApproveDate
) T GROUP BY debarmentId,T.OfficeID ,ApproveDate)T
GROUP BY DebarOfficeID ,MintenderId) T WHERE tenderId= MinTenderId and officeId = DebarOfficeID


END


GO
