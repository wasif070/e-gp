SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--This sp is for getting data for rss
-- exec [p_eGP_RSS]
-- =============================================
-- Author:		Rajesh Singh
-- alter date: 23rd Jan 2011
-- =============================================
-- Exec p_eGP_RSS 'Dailjhy','http://localhost:8080/eGPWeb/'

-- SP Name	:   p_eGP_RSS
-- Module	:	RSS Feed (Daily and weekly)
-- Funftion	:	N.A.

CREATE PROCEDURE [dbo].[p_eGP_RSS]
	@Tag as varchar(100),
	@Path as varchar(200)
AS
BEGIN
	--Only change this part, Please dont change anything inside --Rajesh Singh
	Declare @URL as varchar(1000)
	--set @URL ='http://development.eprocure.gov.bd/resources/common/ViewTender.jsp?id='
	set @URL =@Path+'resources/common/ViewTender.jsp?id='
	-- End

	Declare @startTags as varchar(1000),@endTags as varchar(1000),@middleTags as varchar(max)
	DECLARE @ddlvalue int
	
	--set @startTags='<?xml version="1.0"?><rss version="2.0"><channel><title>Daily Tenders news from National e-Government Procurement (e-GP) </title><description>Visit eprocure.gov.bd for up-to-the-minute Tenders news, The e-GP system provides an on-line platform to carry out the procurement activities by the Public Agencies.</description><image><url>http://localhost:8080/eGPWeb/resources/images/Dashboard/e-GP.gif</url><link>http://localhost:8080/eGPWeb/resources/images/Dashboard/e-GP.gif</link></image>'
	IF @Tag='Daily'
		BEGIN
			set @startTags='<?xml version="1.0"?><rss version="2.0"><channel><title>Get Daily e-Tenders Updates  from e-GP System </title><description>You may subscribe to RSS Feed to get daily updates on new tenders floated on National e-Government Procurement (e-GP) portal ( i.e. http://eprocure.gov.bd ) of the Government of the People&apos;s Republic Bangladesh.</description><image><url>'+@Path+'resources/images/cptuEgpLogo.gif</url><link>'+@Path+'resources/images/cptuEgpLogo.gif</link></image>'		
		END
	ELSE
		BEGIN
			set @startTags='<?xml version="1.0"?><rss version="2.0"><channel><title>Get Weekly e-Tenders Updates  from e-GP System </title><description>You may subscribe to RSS Feed to get weekly updates on new tenders floated on National e-Government Procurement (e-GP) portal ( i.e. http://eprocure.gov.bd ) of the Government of the People&apos;s Republic Bangladesh.</description><image><url>'+@Path+'resources/images/cptuEgpLogo.gif</url><link>'+@Path+'resources/images/cptuEgpLogo.gif</link></image>'
		END
	
	set @endTags='</channel></rss>'
	
	set @middleTags=''
	Declare @InnerStart as varchar(100),@InnerEnd as varchar(100)
	set @InnerStart ='<item>'
	set @InnerEnd='</item>'
	
	Declare @FirstVarialbe as varchar(max),@SecondVarialbe as varchar(max),@ThirdVarialbe as varchar(max)
	
	IF @Tag='Daily'
	BEGIN
		--Cursor starts here
		DECLARE CustIDCursor CURSOR FOR
		SELECT A.tenderId FROM dbo.tbl_TenderDetails A Inner join (Select RowNumber, tenderDtlId From (SELECT ROW_NUMBER() Over (Order by tenderPubDt Desc) as RowNumber, tenderDtlId, tenderPubDt,tenderId FROM dbo.tbl_TenderDetails WHERE tenderStatus='Approved'  and procurementMethodId not in(1,3,11,14) and eventtype not in ('PQTender','RFP','LTM','2STM','2 stage-TSTM') ) B ) Tmp On A.tenderDtlId=tmp.tenderDtlId and CONVERT(DATE,tenderPubDt) =CONVERT(DATE,GETDATE()) ORDER BY A.tenderid DESC
		OPEN CustIDCursor
		FETCH NEXT FROM CustIDCursor INTO @ddlvalue

		WHILE @@FETCH_STATUS = 0
		BEGIN
		select @FirstVarialbe='<title><![CDATA['+replace(replace(replace(tenderBrief,'<p>',''),'</p>',''),'&nbsp;','')+']]></title><pubDate>'+cast(tenderPubDt as varchar(100))+'</pubDate>',
		@SecondVarialbe=replace(replace('<description>&lt;table cellpadding="" cellspacing="" width="100%"&gt;&lt;tr&gt;&lt;td width="50%" valign="top"&gt;&lt;font size="2" face="arial"&gt;&lt;ul&gt;&lt;li&gt;&lt;strong&gt;Tender ID :&lt;/strong&gt;'+cast(tenderId as varchar(20))+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Procurement Nature :&lt;/strong&gt;'+procurementNature+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Tender Brief/Title :&lt;/strong&gt;'+tenderBrief+'&lt;/li&gt;&lt;strong&gt;Ministry, Division, Organisation/Agency :&lt;/strong&gt;'+(select departmentName from tbl_DepartmentMaster where tbl_DepartmentMaster.departmentId=tbl_TenderDetails.departmentId)+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;District :&lt;/strong&gt;'+(select (select stateName from tbl_StateMaster where tbl_StateMaster.stateId= tbl_officemaster.stateId) as state from tbl_officemaster where tbl_OfficeMaster.officeId=tbl_TenderDetails.officeId) +'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Publishing Date And Time :&lt;/strong&gt;'+cast(tenderPubDt as varchar(100))+'&lt;/li&gt;&lt;/ul&gt;&lt;/font&gt;&lt;/td&gt;&lt;td width="50%" valign="top"&gt;&lt;font size="2" face="arial"&gt;&lt;ul&gt;&lt;li&gt;&lt;strong&gt;Reference No. :&lt;/strong&gt;'+cast(reoiRfpRefNo as varchar(100))+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Procurement Type :&lt;/strong&gt;'+procurementType+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Tender Brief/Title :&lt;/strong&gt;'+tenderBrief+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;PE :&lt;/strong&gt;'+peOfficeName+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Procurement Method :&lt;/strong&gt;'+ procurementMethod+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Closing Date And Time :&lt;/strong&gt;'+cast(submissionDt as varchar(100))+'&lt;/li&gt;&lt;/ul&gt;&lt;/font&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</description>','&nbsp;',''),' & ',' And '),
		@ThirdVarialbe='<link>'+@URL+convert(varchar(20),tenderid)+'</link>' 
		from tbl_TenderDetails where tenderId=@ddlvalue
		
		set @middleTags=@middleTags+(@InnerStart+@FirstVarialbe+@SecondVarialbe+@ThirdVarialbe+@InnerEnd)
		
		FETCH NEXT FROM CustIDCursor INTO @ddlvalue
		END
		CLOSE CustIDCursor
		DEALLOCATE CustIDCursor
		--Cursor end here
	END
	ELSE
	BEGIN
		--Cursor starts here
		DECLARE CustIDCursor CURSOR FOR
		SELECT A.tenderId FROM dbo.tbl_TenderDetails A Inner join (Select RowNumber, tenderDtlId From (SELECT ROW_NUMBER() Over (Order by tenderPubDt Desc) as RowNumber, tenderDtlId, tenderPubDt,tenderId FROM dbo.tbl_TenderDetails WHERE tenderStatus='Approved'  and procurementMethodId not in(1,3,11,14) and eventtype not in ('PQTender','RFP','LTM','2STM','2 stage-TSTM') ) B ) Tmp On A.tenderDtlId=tmp.tenderDtlId and CONVERT(DATE,tenderPubDt) <=CONVERT(DATE,GETDATE()-7) ORDER BY A.tenderid DESC
		OPEN CustIDCursor
		FETCH NEXT FROM CustIDCursor INTO @ddlvalue

		WHILE @@FETCH_STATUS = 0
		BEGIN
		select @FirstVarialbe='<title><![CDATA['+replace(replace(replace(tenderBrief,'<p>',''),'</p>',''),'&nbsp;','')+']]></title><pubDate>'+cast(tenderPubDt as varchar(100))+'</pubDate>',
		@SecondVarialbe=replace(replace('<description>&lt;table cellpadding="" cellspacing="" width="100%"&gt;&lt;tr&gt;&lt;td width="50%" valign="top"&gt;&lt;font size="2" face="arial"&gt;&lt;ul&gt;&lt;li&gt;&lt;strong&gt;Tender ID :&lt;/strong&gt;'+cast(tenderId as varchar(20))+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Procurement Nature :&lt;/strong&gt;'+procurementNature+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Ministry, Division, Organisation/Agency :&lt;/strong&gt;'+(select departmentName from tbl_DepartmentMaster where tbl_DepartmentMaster.departmentId=tbl_TenderDetails.departmentId)+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;District :&lt;/strong&gt;'+(select (select stateName from tbl_StateMaster where tbl_StateMaster.stateId= tbl_officemaster.stateId) as state from tbl_officemaster where tbl_OfficeMaster.officeId=tbl_TenderDetails.officeId) +'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Publishing Date And Time :&lt;/strong&gt;'+cast(tenderPubDt as varchar(100))+'&lt;/li&gt;&lt;/ul&gt;&lt;/font&gt;&lt;/td&gt;&lt;td width="50%" valign="top"&gt;&lt;font size="2" face="arial"&gt;&lt;ul&gt;&lt;li&gt;&lt;strong&gt;Reference No. :&lt;/strong&gt;'+cast(reoiRfpRefNo as varchar(100))+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Procurement Type :&lt;/strong&gt;'+procurementType+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;PE :&lt;/strong&gt;'+peOfficeName+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Procurement Method :&lt;/strong&gt;'+ procurementMethod+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Closing Date And Time :&lt;/strong&gt;'+cast(submissionDt as varchar(100))+'&lt;/li&gt;&lt;/ul&gt;&lt;/font&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</description>','&nbsp;',''),' & ',' And '),
		--@SecondVarialbe='<description>&lt;font size="2" face="arial"&gt;&lt;ul&gt;&lt;li&gt;&lt;strong&gt;Tender ID :&lt;/strong&gt;'+cast(tenderId as varchar(20))+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Reference No. :&lt;/strong&gt;'+cast(reoiRfpRefNo as varchar(100))+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Procurement Nature :&lt;/strong&gt;'+procurementNature+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Tender Brief/Title :&lt;/strong&gt;'+tenderBrief+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Ministry, Division, Organisation/Agency, PE :&lt;/strong&gt;'+ministry+','+ division+','+ agency+','+ peOfficeName+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Procurement Type, Procurement Method :&lt;/strong&gt;'+procurementType+','+ procurementMethod+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Publishing Date And Time :&lt;/strong&gt;'+cast(tenderPubDt as varchar(100))+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Closing Date And Time :&lt;/strong&gt;'+cast(submissionDt as varchar(100))+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;City/District/Country :&lt;/strong&gt;'+(select city from tbl_officemaster where tbl_OfficeMaster.officeId=tbl_TenderDetails.officeId)+' / '+(select (select stateName from tbl_StateMaster where tbl_StateMaster.stateId= tbl_officemaster.stateId) as state from tbl_officemaster where tbl_OfficeMaster.officeId=tbl_TenderDetails.officeId) +'/'+ (select countryName from tbl_countrymaster where tbl_countrymaster.countryid=(select countryId from tbl_officemaster where tbl_officemaster.officeId=tbl_TenderDetails.officeId))+'&lt;/li&gt;&lt;li&gt;&lt;strong&gt;URL for more details :&lt;/strong&gt;'+@URL+cast(tbl_TenderDetails.tenderId as varchar(20))+'&lt;/li&gt;&lt;/ul&gt;&lt;/font&gt;</description>',
		@ThirdVarialbe='<link>'+@URL+convert(varchar(20),tenderid)+'</link>' 
		from tbl_TenderDetails where tenderId=@ddlvalue
		
		set @middleTags=@middleTags+(@InnerStart+@FirstVarialbe+@SecondVarialbe+@ThirdVarialbe+@InnerEnd)
		
		FETCH NEXT FROM CustIDCursor INTO @ddlvalue
		END
		CLOSE CustIDCursor
		DEALLOCATE CustIDCursor
		--Cursor end here
	END
	
	select @startTags +@middleTags+ @endTags as RSSXML	
END


GO
