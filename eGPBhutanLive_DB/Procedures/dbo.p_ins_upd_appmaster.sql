SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Insert/Update/Delete table tbl_AppMaster
--
--
-- Author: Karan
-- Date: 30-10-2010
--
-- Modified:
-- Modified By: Karan
-- Date : 06-12-2010
-- Modification: The variable for DepartmentId during insert case is now selected from the OfficeMaster table

-- SP Name: [p_ins_upd_appmaster]
-- Module: APP
-- Function: Store Procedure is use for perform insert/update/delete operation for APP Master.

--------------------------------------------------------------------------------
-- Create:	Use for perform ADD operation for tbl_AppMaster.
-- Update:  Use for perform Update operation for tbl_AppMaster.
-- Delete:  Use for perform Delete operation for tbl_AppMaster.
--------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ins_upd_appmaster]

@v_FinancialYear_inVc varchar(30)=null,
@v_BudgetType_intInt tinyint=null,
@v_ProjectId_inInt int=null,
@v_ProjectName_inVc varchar(500)=null,
@v_OfficeId_inInt int=null,
@v_EmployeeId_inInt int=null,
@v_AAEmployeeId_inInt int=null,
@v_AppCode_inVc varchar(50)=null,
@v_CreatedBy_inInt int=null,
@v_DepartmentId_insInt smallint=null,
@v_AAProcurementRoleId_intInt tinyint=null,
@v_Action_inVc varchar(50),
--added by rased 09-05-2016
@v_App_inInt int=null,
@v_AppType_inVc varchar(50)=null,
@v_EntrustingAgency_inVc varchar(150)=null,
@v_RefAppId_inInt int=null,
@v_ReviseCount_inInt int=null,
@v_LastRevisionDate_inDateTime datetime=null,
@v_ActivityName_inVc varchar(MAX)=null

AS

BEGIN

DECLARE @v_flag_bit bit, @v_DepartmentId_Int int

	IF @v_Action_inVc='CountAPPRevise'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
				UPDATE [dbo].[tbl_AppMaster]
					   SET [reviseCount] = [reviseCount]+1
						  ,[lastRevisionDate] = @v_LastRevisionDate_inDateTime
					   WHERE appId=@v_App_inInt				 			
					Set @v_flag_bit=1
					Select @v_flag_bit as flag, 'Revision Counted Sucessfully' as Message									 
			COMMIT TRAN
		END TRY
		BEGIN CATCH
				BEGIN
					Set @v_flag_bit=0
					Select @v_flag_bit as flag, 'Revision Counted failed' as Message
					ROLLBACK TRAN
				END
		END CATCH
	END

	IF @v_Action_inVc='RevisionCount'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
				IF ((select reviseCount from tbl_AppMaster where appId = @v_App_inInt) < (select maxReviseCount from tbl_APPReviseConfiguration where financialYear = @v_FinancialYear_inVc))
					BEGIN				 			
					Set @v_flag_bit=1
					Select @v_flag_bit as flag, 'Revision Count Not Filled' as Message
					END
				 ELSE
				 BEGIN
				 Set @v_flag_bit=1
					Select @v_flag_bit as flag, 'Revision Count Filled' as Message
				 END	
			COMMIT TRAN
		END TRY
		BEGIN CATCH
				BEGIN
					Set @v_flag_bit=0
					Select @v_flag_bit as flag, 'Revision Updated failed' as Message
					ROLLBACK TRAN
				END
		END CATCH
	END


	IF @v_Action_inVc='Create'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
				/* START CODE : TO INSERT INTO TABLE - tbl_AppMaster */

				Select @v_DepartmentId_Int=departmentId  from tbl_OfficeMaster where officeId=@v_OfficeId_inInt

					INSERT INTO [dbo].[tbl_AppMaster]
				   ([financialYear]
				   ,[budgetType]
				   ,[projectId]
				   ,[projectName]
				   ,[officeId]
				   ,[employeeId]
				   ,[aAEmployeeId]
				   ,[appCode]
				   ,[createdBy]
				   ,[departmentId]
				   ,[aAProcurementRoleId]
				   ,[appType]
				   ,[entrustingAgency]
				   ,[refAppId]
				   ,[reviseCount]
				   ,[lastRevisionDate]
				   ,[activityName])
				VALUES
				   (@v_FinancialYear_inVc,
					@v_BudgetType_intInt,
					@v_ProjectId_inInt,
					@v_ProjectName_inVc,
					@v_OfficeId_inInt,
					@v_EmployeeId_inInt,
					@v_AAEmployeeId_inInt,
					@v_AppCode_inVc,
					@v_CreatedBy_inInt,
					@v_DepartmentId_Int,
					@v_AAProcurementRoleId_intInt,
					@v_AppType_inVc,
					@v_EntrustingAgency_inVc,
					@v_RefAppId_inInt,
					@v_ReviseCount_inInt,
					@v_LastRevisionDate_inDateTime,
					@v_ActivityName_inVc)

				--INSERT INTO [dbo].[tbl_AppMaster]
				--   ([financialYear]
				--   ,[budgetType]
				--   ,[projectId]
				--   ,[projectName]
				--   ,[officeId]
				--   ,[employeeId]
				--   ,[aAEmployeeId]
				--   ,[appCode]
				--   ,[createdBy]
				--   ,[departmentId]
				--   ,[aAProcurementRoleId])
				--VALUES
				--   (@v_FinancialYear_inVc,
				--	@v_BudgetType_intInt,
				--	@v_ProjectId_inInt,
				--	@v_ProjectName_inVc,
				--	@v_OfficeId_inInt,
				--	@v_EmployeeId_inInt,
				--	@v_AAEmployeeId_inInt,
				--	@v_AppCode_inVc,
				--	@v_CreatedBy_inInt,
				--	@v_DepartmentId_insInt,
				--	@v_AAProcurementRoleId_intInt)
				 /* END CODE : TO INSERT INTO TABLE - tbl_AppMaster */

				Set @v_flag_bit=1
				Select @v_flag_bit as flag, 'APP created.' as Message,Ident_Current('dbo.[tbl_AppMaster]') appId
			COMMIT TRAN
		END TRY
		BEGIN CATCH
				BEGIN
					Set @v_flag_bit=0
					Select @v_flag_bit as flag, 'Error while creating APP.' as Message,0 appId
					ROLLBACK TRAN
				END
		END CATCH
	END

	ELSE IF @v_Action_inVc='Update'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
				/* START CODE : TO UPDATE TABLE - tbl_AppMaster */
				UPDATE [dbo].[tbl_AppMaster]
				   SET [financialYear] = @v_FinancialYear_inVc,
					  [budgetType] = @v_BudgetType_intInt,
					  [projectId] = @v_ProjectId_inInt,
					  [projectName] = @v_ProjectName_inVc,
					  [officeId] = @v_OfficeId_inInt,
					  [employeeId] = @v_EmployeeId_inInt,
					  [aAEmployeeId] = @v_AAEmployeeId_inInt,
					  [appCode] = @v_AppCode_inVc,
					  [createdBy] = @v_CreatedBy_inInt,
					  [departmentId]= @v_DepartmentId_insInt,
					  [aAProcurementRoleId]=@v_AAProcurementRoleId_intInt,
					  [appType] = @v_AppType_inVc,
					   [entrustingAgency] = @v_EntrustingAgency_inVc,
					   [refAppId] = @v_RefAppId_inInt,
					   [reviseCount] = @v_ReviseCount_inInt,
					   [lastRevisionDate] = @v_LastRevisionDate_inDateTime
				 WHERE appId=@v_App_inInt
				 /* END CODE : TO UPDATE TABLE - tbl_AppMaster */

				 Set @v_flag_bit=1
				 Select @v_flag_bit as flag, 'APP updated.' as Message,@v_App_inInt appId
			COMMIT TRAN
		END TRY
		BEGIN CATCH
			BEGIN
				Set @v_flag_bit=0
				Select @v_flag_bit as flag, 'Error while updating APP.' as Message,@v_App_inInt appId
				ROLLBACK TRAN
			END
		END CATCH
	END

	ELSE IF @v_Action_inVc='Delete'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
				/* START CODE : TO DELETE FROM TABLE - tbl_AppPkgLots */
				DELETE FROM [dbo].[tbl_AppPkgLots]
				 WHERE appId=@v_App_inInt
				/* END CODE : TO DELETE FROM TABLE - tbl_AppPkgLots */

				/* START CODE : TO DELETE FROM TABLE - tbl_AppPackages */
				DELETE FROM [dbo].[tbl_AppPackages]
				 WHERE appId=@v_App_inInt
				/* END CODE : TO DELETE FROM TABLE - tbl_AppPackages */

				/* START CODE : TO DELETE FROM TABLE - tbl_AppMaster  */
				DELETE FROM [dbo].[tbl_AppMaster]
				 WHERE appId=@v_App_inInt
				/* END CODE : TO DELETE FROM TABLE - tbl_AppMaster  */

				 Set @v_flag_bit=1
				 Select @v_flag_bit as flag, 'APP deleted.' as Message,@v_App_inInt appId
			COMMIT TRAN
		END TRY
		BEGIN CATCH
			BEGIN
				Set @v_flag_bit=0
				Select @v_flag_bit as flag, 'Error while deleting APP.' as Message,@v_App_inInt appId
				ROLLBACK TRAN
			END
		END CATCH
	END
END


GO
