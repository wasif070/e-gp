SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--
-- Purpose: Display Project Details.
--
--
-- Author: Kinjal
-- Date: 08-11-2010
--

-- =============================================
--exec [dbo].[p_get_projectdetail] 1
CREATE PROCEDURE [dbo].[p_get_projectdetail]
	-- Add the parameters for the stored procedure here
	@v_ProjectId_inInt int -- Message Status. Eg; 
	
	
AS
BEGIN

DECLARE 
 @v_userIdInt int,
 @v_procurementRoleIdVc varchar(800),
 @v_procurementRoleVc varchar(max), 
 @v_employeeNameVc varchar(200),
 @v_officeNameVc varchar(150)

SET NOCOUNT ON;

/* Fetch Project Detail based on project id*/

declare cur_projectDetail cursor FAST_FORWARD For select distinct userid from tbl_ProjectRoles where projectid = @v_ProjectId_inInt        
			open cur_projectDetail        
			fetch next from cur_projectDetail into @v_userIdInt    

			While @@Fetch_status = 0        
			Begin        
		Select distinct @v_procurementRoleIdVc =   COALESCE(@v_procurementRoleIdVc+', ', ' ') + convert(varchar(30),tbl_ProjectRoles.procurementRoleId), @v_procurementRoleVc= COALESCE(@v_procurementRoleVc+', ', ' ') + convert(varchar(30),tbl_ProcurementRole.procurementRole)  from tbl_ProjectRoles inner join tbl_ProcurementRole on tbl_ProcurementRole.procurementRoleId = tbl_ProjectRoles.procurementRoleId where projectId = 1 and  userid = @v_userIdInt group by tbl_ProjectRoles.userId, tbl_ProjectRoles.procurementRoleId , tbl_ProcurementRole.procurementRole
	
	
	Select @v_employeeNameVc = employeeName from tbl_EmployeeMaster where userid = @v_userIdInt
	Select @v_officeNameVc = officeName from tbl_OfficeMaster where officeid in(
	select officeid from tbl_ProjectOffice where userid = @v_userIdInt and projectId = @v_ProjectId_inInt)
	
	
	Select 	@v_userIdInt as userid, @v_procurementRoleIdVc as procurementRoleId, @v_procurementRoleVc as procurementRole, @v_employeeNameVc as employeeName, @v_officeNameVc as officeName
	
		
			fetch next from cur_projectDetail into @v_userIdInt        
			End       

			close cur_projectDetail        
			deallocate cur_projectDetail
	 
SET NOCOUNT OFF
END


GO
