SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Generate mega mega hash for those tenders which are not approved 
--				 and closing date is greater than today's date
--
-- Author:	Krishnraj
-- Date:		18-12-2010
--
-- Last Modified: 
-- Modified By: 
-- Date 
-- Modification: 
--------------------------------------------------------------------------------
-- SP Name	   :   p_mega_hashgenerate
-- Module	   :   Tender Notice
-- Description :   Generates Mega Hash for bidder's bid submission for the tender
-- Function	   :   N.A.
-- Exec [dbo].[p_mega_hashgenerate]
CREATE PROCEDURE [dbo].[p_mega_hashgenerate]

AS

BEGIN

	SET NOCOUNT ON;

	DECLARE @v_tenderHash_Vc VARCHAR(MAX), @v_tenderId_Int INT, @v_megaHash_Vc VARCHAR(MAX)

	-- DECLARE CURSOR TO GO THRU EACH tenderId WHOSE STATUS ARE APPROVED
	DECLARE colTenderId CURSOR FAST_FORWARD READ_ONLY FOR SELECT tenderId FROM dbo.tbl_TenderDetails 
		WHERE tenderStatus = 'Approved' and CAST(FLOOR(CAST(submissionDt AS FLOAT)) AS DATETIME) >= CAST(FLOOR(CAST(GETDATE() AS FLOAT)) AS DATETIME)  

	OPEN colTenderId

	FETCH NEXT FROM colTenderId INTO @v_tenderId_Int
	WHILE @@FETCH_STATUS = 0 
	BEGIN

		-- CHECK WHETHER THIS TENDERID EXISTS IN tbl_FinalSubmission OR NOT
		IF EXISTS (SELECT 1 FROM dbo.tbl_FinalSubmission WHERE tenderId = @v_tenderId_Int)
			BEGIN
				
				-- FETCH TENDER-HASH
				SELECT @v_tenderHash_Vc = ISNULL(@v_tenderHash_Vc + '_' + tenderHash, tenderHash) FROM dbo.tbl_FinalSubmission 
				WHERE tenderId = @v_tenderId_Int
				
				-- NOW GENERATE SHA1 OF GENERATED HASH
				SELECT @v_megaHash_Vc = SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('SHA1', @v_tenderHash_Vc)), 3, 41)
				
				-- NOW INSERT THIS GENERATED HASH INTO tbl_TenderMegaHash
				INSERT INTO dbo.tbl_TenderMegaHash (tenderId, megaHash, megaHashDt)
					SELECT @v_tenderId_Int, @v_megaHash_Vc, GETDATE()
				
			END
			
			-- RESET THE @v_tenderHash_Vc VARIABLE TO NULL
			SET @v_tenderHash_Vc = NULL
		
		FETCH NEXT FROM colTenderId INTO @v_tenderId_Int
		
	END
	
	CLOSE colTenderId
	DEALLOCATE colTenderId
	
END


GO
