SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: alter a table matrix
--
--
-- Author: Krishnraj
-- Date: 14-11-2010
--

-- SP Name: [p_create_tablematrix]
-- Module: Bid Preparation
-- Function: Store Procedure is use for fetch data table Matrix.
--------------------------------------------------------------------------------

-- Exec [dbo].[p_create_tablematrix] 59, '3'
CREATE PROCEDURE [dbo].[p_create_tablematrix]

@v_tableid_inVc INT,
@v_ordinalposition_inInt VARCHAR(50)

AS

BEGIN

	SET NOCOUNT ON;

	DECLARE @v_colHeader_Vc VARCHAR(500), @v_tblScript_Vc VARCHAR(MAX), @v_colHeaderValue_Vc VARCHAR(500), 
	@v_colId_Int INT, @v_rowid_Int INT, @v_colCount_Int INT, @v_intFlag_Int INT

	-- GET THE RECORD COUNT
	SELECT @v_colCount_Int = COUNT(templateColId) FROM [dbo].[tbl_TemplateColumns] WHERE tableId = @v_tableid_inVc

	-- DECLARETION OF TABLE VARIABLE
	SET @v_tblScript_Vc = 'Declare @tmpCellValue Table('
		
	-- DECLARE CURSOR TO GO THRU EACH COLUMN NAME OF PARTICULAR TABLE
	DECLARE colNameCursor CURSOR FAST_FORWARD READ_ONLY FOR SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS 
		WHERE table_name = 'tbl_TemplateColumns' and ordinal_position = @v_ordinalposition_inInt

	OPEN colNameCursor

	FETCH NEXT FROM colNameCursor INTO @v_colHeader_Vc
	WHILE @@FETCH_STATUS = 0 
	BEGIN

		SET @v_intFlag_Int = 1
		
		-- DEFINE COLUMN NAME FOR @tmpCellValue TABLE
		WHILE (@v_intFlag_Int <= @v_colCount_Int)
		BEGIN
		
			SET @v_tblScript_Vc = @v_tblScript_Vc + '[' + @v_colHeader_Vc + CAST(@v_intFlag_Int as Varchar) + ']'+ ' Varchar(500), '
			
			SET @v_intFlag_Int = @v_intFlag_Int + 1
		END

		FETCH NEXT FROM colNameCursor INTO @v_colHeader_Vc
		
	END
	CLOSE colNameCursor
	DEALLOCATE colNameCursor

	SET @v_tblScript_Vc = SUBSTRING(@v_tblScript_Vc, 0, DATALENGTH(@v_tblScript_Vc) - 1) + '); '

	-- DECLARATION OF TABLE IS COMPLETED
	-- ==========================================================================
	-- NOW INSERT DATA FROM dbo.tbl_TemplateColumns TABLE
	SET @v_intFlag_Int = 1

	DECLARE colNameCursor CURSOR FAST_FORWARD READ_ONLY FOR SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS 
		WHERE table_name = 'tbl_TemplateColumns' and ordinal_position = @v_ordinalposition_inInt
		 
	OPEN colNameCursor

	FETCH NEXT FROM colNameCursor INTO @v_colHeader_Vc
	WHILE @@FETCH_STATUS = 0 
	BEGIN

		SET @v_intFlag_Int = 1
		
		SET @v_tblScript_Vc = @v_tblScript_Vc + ' Insert Into @tmpCellValue('
		
		-- GET THE COLUMN NAME FOR INSERING DATA INTO @tmpCellValue TABLE
		WHILE (@v_intFlag_Int <= @v_colCount_Int)
		BEGIN
		
			SET @v_tblScript_Vc = @v_tblScript_Vc + '[' + @v_colHeader_Vc + CAST(@v_intFlag_Int AS VARCHAR) + '], '
			
			SET @v_intFlag_Int = @v_intFlag_Int + 1
		END
		
		SET @v_tblScript_Vc = SUBSTRING(@v_tblScript_Vc, 0, DATALENGTH(@v_tblScript_Vc) - 1) + ')  Values('
		
		-- DECLARE CURSOR FOR INSERTING COLUMN-HEADER VALUE INTO @tmpCellValue TABLE
		EXEC('DECLARE colHeaderValueCursor CURSOR FAST_FORWARD READ_ONLY FOR SELECT ' + @v_colHeader_Vc + ' from dbo.tbl_TemplateColumns Where tableId = ' + @v_tableid_inVc)
			
		OPEN colHeaderValueCursor

		FETCH NEXT FROM colHeaderValueCursor INTO @v_colHeaderValue_Vc
		WHILE @@FETCH_STATUS = 0 
		BEGIN

			SET @v_tblScript_Vc = @v_tblScript_Vc + '''' + @v_colHeaderValue_Vc + ''', '
			
			FETCH NEXT FROM colHeaderValueCursor INTO @v_colHeaderValue_Vc
			
		END
		
		CLOSE colHeaderValueCursor
		DEALLOCATE colHeaderValueCursor
		
		SET @v_tblScript_Vc = SUBSTRING(@v_tblScript_Vc, 0, DATALENGTH(@v_tblScript_Vc) - 1) + '); '
		
		FETCH NEXT FROM colNameCursor INTO @v_colHeader_Vc
		
	END

	CLOSE colNameCursor
	DEALLOCATE colNameCursor

	-- ===========================================================================
	-- HERE WE USE NESTED CURSORS TO INSERT CELL-VALUE BY COLUMN AND ROWID WISE.
	
	SET @v_intFlag_Int = 1

	DECLARE colNameCursor CURSOR FAST_FORWARD READ_ONLY FOR SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS 
		WHERE TABLE_NAME = 'tbl_TemplateColumns' and ordinal_position = @v_ordinalposition_inInt
		 
	OPEN colNameCursor

	FETCH NEXT FROM colNameCursor INTO @v_colHeader_Vc
	WHILE @@FETCH_STATUS = 0 
	BEGIN

		SET @v_intFlag_Int = 1
		
		-- CONSTRUCT INSERT QUERY TO ENTER CELL VALUE
		SET @v_tblScript_Vc = @v_tblScript_Vc + ' Insert Into @tmpCellValue('
		
		WHILE (@v_intFlag_Int <= @v_colCount_Int)
		BEGIN
		
			SET @v_tblScript_Vc = @v_tblScript_Vc + '[' + @v_colHeader_Vc + CAST(@v_intFlag_Int AS VARCHAR) + '], '
			
			SET @v_intFlag_Int = @v_intFlag_Int + 1
			
		END
		
		SET @v_tblScript_Vc = SUBSTRING(@v_tblScript_Vc, 0, DATALENGTH(@v_tblScript_Vc) - 1) + ')  Values('
		
		-- HERE CONSTRUCT THE "VALUES" CLAUSE AND SET THE APPROPRIATE SELECT STATEMENT TO ENTER CELL VALUE
		-- DECLARE CURSOR TO ENUMERATE THRU EACH ROWID BY TABLEID
		DECLARE rowIdCursor CURSOR FAST_FORWARD READ_ONLY FOR SELECT rowid FROM [dbo].[tbl_TemplateCells] 
			WHERE tableId=@v_tableid_inVc GROUP BY rowid ORDER BY rowid
			
		OPEN rowIdCursor

		FETCH NEXT FROM rowIdCursor INTO @v_rowid_Int
		WHILE @@FETCH_STATUS = 0 
		BEGIN
			
			-- DECLARE CURSOR TO ENUMERATE THRU EACH COLUMNID BY TABLEID
			DECLARE columnIdCursor CURSOR FAST_FORWARD READ_ONLY FOR SELECT columnId from [dbo].[tbl_TemplateCells] 
			WHERE tableId=@v_tableid_inVc GROUP BY columnid ORDER BY columnId
			
			OPEN columnIdCursor

			FETCH NEXT FROM columnIdCursor INTO @v_colId_Int
			WHILE @@FETCH_STATUS = 0 
			BEGIN
			
				SET @v_tblScript_Vc = @v_tblScript_Vc + '(SELECT dbo.tbl_TemplateCells.cellvalue FROM dbo.tbl_TemplateCells 
												Where columnId = ' + CONVERT(VARCHAR(100), @v_colId_Int) + ' and 
												rowid = ' + CONVERT(VARCHAR(100), @v_rowid_Int) + ' and tableId = ' + CAST(@v_tableid_inVc AS VARCHAR) + '), '

				SET @v_intFlag_Int = @v_intFlag_Int + 1
				
				FETCH NEXT FROM columnIdCursor INTO @v_colId_Int
				
			END
		
			CLOSE columnIdCursor
			DEALLOCATE columnIdCursor
			
			SET @v_intFlag_Int = 1
			
			SET @v_tblScript_Vc = SUBSTRING(@v_tblScript_Vc, 0, DATALENGTH(@v_tblScript_Vc) - 1) + '), ('
			
			FETCH NEXT FROM rowIdCursor INTO @v_rowid_Int
		
		END
		
		CLOSE rowIdCursor
		DEALLOCATE rowIdCursor
		
	END
	CLOSE colNameCursor
	DEALLOCATE colNameCursor

	SET @v_tblScript_Vc = SUBSTRING(@v_tblScript_Vc, 0, DATALENGTH(@v_tblScript_Vc) - 2) + ';  Select * from @tmpCellValue'

	EXEC(@v_tblScript_Vc)

END


GO
