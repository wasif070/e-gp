SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Search In NOA
--
--
-- Author: Krishnraj,rishita
-- Date: 25-12-2010
--
-- Last Modified:
-- Modified By:
-- Modification:
--------------------------------------------------------------------------------
-- Exec dbo.p_search_noa 'laptop','','','0','0','','0','','','','0',1,10

-- SP Name	:   p_search_noa
-- Module	:	Home Page -> Award Contract, Advance Search Award Contract
-- Funftion	:	N.A.

CREATE PROCEDURE [dbo].[p_search_noa]
@v_Keyword_inVc				VARCHAR(500)	=NULL,
@v_contractDtFrom_inVc		VARCHAR(30)	=NULL,
@v_contractDtTo_inVc			VARCHAR(30)	=NULL,
@v_departmentId_inN			INT				=NULL,
@v_contractId_inN				INT				=NULL,
@v_contractNo_inVc				VARCHAR(200)	=NULL,
@v_tenderId_inN					INT				=NULL,
@v_tenderRefNo_inVc			VARCHAR(50)	=NULL,
@v_cpvCode_inVc				VARCHAR(500)	=NULL,
@v_contractAmt_inM			VARCHAR(50)	=NULL,
@v_stateName_inVc			VARCHAR(50)	=NULL,
@v_officeId_inN					INT				=NULL,
@v_advDt_inVc		VARCHAR(30)	=NULL,
--Added By Proshanto Kumar Saha
@v_advDtTo_inVc  VARCHAR(30)	=NULL,
@v_procurementMethod_inVc		VARCHAR(30)	=NULL,
@v_noaDt_inVc		VARCHAR(30)	=NULL,
--Added By Proshanto Kumar Saha
@v_noaDtTo_inVc VARCHAR(30)	=NULL,
@v_contractAwardTo_inVc		VARCHAR(50)	=NULL,
@v_Page_inN						INT				=1,
@v_RecordPerPage_inN			INT				=10
	
AS

BEGIN
	
	SET NOCOUNT ON;

	DECLARE @v_WhereCond_Vc VARCHAR(MAX) = '', @v_MainQry_Vc NVARCHAR(MAX), @v_Params_Vc NVARCHAR(100)
	
	-- SET THE PARAMETER
	SET @v_Params_Vc = N'@v_RecordPerPage_inN INT, @v_Page_inN INT'
	
	
	
	IF @v_WhereCond_Vc <> ''
		SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' AND '
	
	
	-- SEARCH KEYWORD CONDITION FOR dbo.tbl_NoaIssueDetails
	IF @v_WhereCond_Vc <> '' AND @v_Keyword_inVc <> ''
			SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' AND '
	
	IF @v_Keyword_inVc IS NOT NULL AND @v_Keyword_inVc <> ''
	BEGIN
			SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' contractName like ''%' + @v_Keyword_inVc + '%''' 
	END
	-- ==================================================	
	
	-- SEARCH district CONDITION
	IF @v_WhereCond_Vc <> '' AND @v_stateName_inVc <> ''
			SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' AND '
			
	IF @v_stateName_inVc <> ''
	BEGIN
			SET @v_WhereCond_Vc= @v_WhereCond_Vc + ' dbo.tbl_TenderDetails.peDistrict LIKE ''%' + @v_stateName_inVc + '%'''
	END
	-- ==================================================
	
	-- SEARCH district CONDITION
	IF @v_WhereCond_Vc <> '' AND @v_contractAwardTo_inVc <> ''
			SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' AND '
			
	IF @v_contractAwardTo_inVc <> ''
	BEGIN
			SET @v_WhereCond_Vc= @v_WhereCond_Vc + ' dbo.f_getbiddercompany(tbl_NoaIssueDetails.userId) LIKE ''%' + @v_contractAwardTo_inVc + '%'''
	END
	-- ==================================================
	
	-- SEARCH procurement method CONDITION
	IF @v_WhereCond_Vc <> '' AND @v_procurementMethod_inVc <> ''
			SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' AND '
			
	IF @v_procurementMethod_inVc <> ''
	BEGIN
			SET @v_WhereCond_Vc= @v_WhereCond_Vc + ' dbo.tbl_TenderDetails.procurementMethodId = ' + @v_procurementMethod_inVc + ''
	END
	-- ==================================================
	
	-- SEARCH contractDt from CONDITION FOR dbo.tbl_NoaIssueDetails
	IF @v_WhereCond_Vc <> '' AND @v_contractDtFrom_inVc <> ''
			SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' AND '
			
	IF  @v_contractDtFrom_inVc IS NOT NULL AND @v_contractDtFrom_inVc <> ''
	BEGIN
			SET @v_WhereCond_Vc= @v_WhereCond_Vc + ' CAST(FLOOR(CAST(contractDt AS FLOAT)) AS DATETIME) >= ' + CHAR(39) + @v_contractDtFrom_inVc + CHAR(39)
	END
	-- ==================================================
	--Code Comment by Proshanto Kumar Saha
	-- SEARCH advertisementDt from CONDITION 
	--IF @v_WhereCond_Vc <> '' AND @v_advDt_inVc <> ''
	--		SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' AND '
			
	--IF  @v_advDt_inVc IS NOT NULL AND @v_advDt_inVc <> ''
	--BEGIN
	--		SET @v_WhereCond_Vc= @v_WhereCond_Vc + ' CAST(tbl_TenderDetails.tenderPubDt AS DATETIME) >= ' + CHAR(39) + @v_advDt_inVc + CHAR(39)
	--END

	--Code By Proshanto Kumar Saha
	IF @v_advDt_inVc is not null And @v_advDtTo_inVc is not null AND @v_advDt_inVc!='' And @v_advDtTo_inVc !=''
		Begin
			SET @v_WhereCond_Vc =@v_WhereCond_Vc + ' Cast (Floor(Cast (tbl_TenderDetails.tenderPubDt as Float)) as Datetime) between ''' + @v_advDt_inVc + ''' And ''' + @v_advDtTo_inVc + ''''
		End
		ELSE IF @v_advDt_inVc is not null And @v_advDtTo_inVc is null AND @v_advDt_inVc!='' And @v_advDtTo_inVc !=''
		Begin
			SET @v_WhereCond_Vc =@v_WhereCond_Vc + ' Cast (Floor(Cast (tbl_TenderDetails.tenderPubDt as Float)) as Datetime) >= ''' + @v_advDt_inVc + ''''
		End
		ELSE IF @v_advDt_inVc is null And @v_advDtTo_inVc is not null AND @v_advDt_inVc!='' And @v_advDtTo_inVc !=''
		Begin
			SET @v_WhereCond_Vc =@v_WhereCond_Vc + ' Cast (Floor(Cast (tbl_TenderDetails.tenderPubDt as Float)) as Datetime) <= ''' + @v_advDtTo_inVc + ''''
		End
	-- ==================================================
	--Code Comment by Proshanto Kumar Saha
	-- SEARCH noaDt from CONDITION 
	--IF @v_WhereCond_Vc <> '' AND @v_noaDt_inVc <> ''
	--		SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' AND '
			
	--IF  @v_noaDt_inVc IS NOT NULL AND @v_noaDt_inVc <> ''
	--BEGIN
	--		SET @v_WhereCond_Vc= @v_WhereCond_Vc + ' CAST(tbl_NoaIssueDetails.contractDt AS DATETIME) >= ' + CHAR(39) + @v_noaDt_inVc + CHAR(39)
	--END

	--Code By Proshanto Kumar Saha
		IF @v_noaDt_inVc is not null And @v_noaDtTo_inVc is not null AND @v_noaDt_inVc!='' And @v_noaDtTo_inVc !=''
		Begin
			SET @v_WhereCond_Vc =@v_WhereCond_Vc + ' Cast (Floor(Cast (tbl_NoaIssueDetails.contractDt as Float)) as Datetime) between ''' + @v_noaDt_inVc + ''' And ''' + @v_noaDtTo_inVc + ''''
		End
		ELSE IF @v_noaDt_inVc is not null And @v_noaDtTo_inVc is null AND @v_noaDt_inVc!='' And @v_noaDtTo_inVc !=''
		Begin
			SET @v_WhereCond_Vc =@v_WhereCond_Vc + ' Cast (Floor(Cast (tbl_NoaIssueDetails.contractDt as Float)) as Datetime) >= ''' + @v_noaDt_inVc + ''''
		End
		ELSE IF @v_noaDt_inVc is null And @v_noaDtTo_inVc is not null AND @v_noaDt_inVc!='' And @v_noaDtTo_inVc !=''
		Begin
			SET @v_WhereCond_Vc =@v_WhereCond_Vc + ' Cast (Floor(Cast (tbl_NoaIssueDetails.contractDt as Float)) as Datetime) <= ''' + @v_noaDtTo_inVc + ''''
		End

	-- ==================================================
	
	-- SEARCH contractDt-To CONDITION FOR dbo.tbl_NoaIssueDetails
	IF @v_WhereCond_Vc <> '' AND @v_contractDtTo_inVc <> ''
			SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' AND '
			
	IF  @v_contractDtTo_inVc IS NOT NULL AND @v_contractDtTo_inVc <> ''
	BEGIN
			SET @v_WhereCond_Vc= @v_WhereCond_Vc + ' CAST(FLOOR(CAST(contractDt AS FLOAT)) AS DATETIME) <= ' + CHAR(39) + @v_contractDtTo_inVc + CHAR(39)
	END
	-- ==================================================
		
	-- SEARCH departmentId CONDITION
	IF @v_WhereCond_Vc <> '' AND CONVERT(varchar(10), @v_departmentId_inN) <> '0'
			SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' AND '
			
	IF CONVERT(VARCHAR(10), @v_departmentId_inN) <> '0'
	BEGIN
			SET @v_WhereCond_Vc= @v_WhereCond_Vc + ' dbo.tbl_TenderDetails.departmentId = ' + CHAR(39) + CONVERT(VARCHAR(10), @v_departmentId_inN) + CHAR(39)
	END
	-- ==================================================
	
	-- SEARCH contractId CONDITION
	IF @v_WhereCond_Vc <> '' AND CONVERT(varchar(10), @v_contractId_inN) <> '0'
			SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' AND '
			
	IF  CONVERT(varchar(10), @v_contractId_inN) <> '0'
	BEGIN
			SET @v_WhereCond_Vc= @v_WhereCond_Vc + ' dbo.tbl_NoaIssueDetails.noaIssueId = ' + CHAR(39) + CONVERT(varchar(10), @v_contractId_inN) + CHAR(39)
	END
	-- ==================================================
	
	-- SEARCH tenderRefNo CONDITION
	IF @v_WhereCond_Vc <> '' AND @v_contractNo_inVc <> ''
			SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' AND '
			
	IF @v_contractNo_inVc <> ''
	BEGIN
			SET @v_WhereCond_Vc= @v_WhereCond_Vc + ' dbo.tbl_NoaIssueDetails.contractNo LIKE ''%' + @v_contractNo_inVc + '%'''
	END
	-- ==================================================
	
	-- SEARCH tenderId CONDITION
	IF @v_WhereCond_Vc <> '' AND CONVERT(varchar(30), @v_tenderId_inN) <> '0'
			SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' AND '
			
	IF CONVERT(varchar(30), @v_tenderId_inN) <> '0'
	BEGIN
			SET @v_WhereCond_Vc= @v_WhereCond_Vc + ' dbo.tbl_NoaIssueDetails.tenderId = ' + CHAR(39) + CONVERT(varchar(10), @v_tenderId_inN) + CHAR(39)
	END
	-- ==================================================
	  
	-- SEARCH tenderRefNo CONDITION
	IF @v_WhereCond_Vc <> '' AND @v_tenderRefNo_inVc <> ''
			SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' AND '
			
	IF @v_tenderRefNo_inVc <> ''
	BEGIN
			SET @v_WhereCond_Vc= @v_WhereCond_Vc + ' dbo.tbl_TenderDetails.reoiRfpRefNo LIKE ''%' + @v_tenderRefNo_inVc + '%'''
	END
	-- ==================================================
	
	-- SEARCH contractAmt CONDITION
	IF @v_WhereCond_Vc <> '' AND CONVERT(varchar(10), @v_contractAmt_inM) <> ''
			SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' AND '
			
	IF CONVERT(varchar(10), @v_contractAmt_inM) <> ''
	BEGIN
			SET @v_WhereCond_Vc= @v_WhereCond_Vc + ' dbo.tbl_NoaIssueDetails.contractAmt = ' + CONVERT(varchar(10), @v_contractAmt_inM)
	END
	-- ==================================================
	
	-- SEARCH officeId CONDITION
	IF @v_WhereCond_Vc <> '' AND CONVERT(varchar(10), @v_officeId_inN) <> '0'
			SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' AND '
			
	IF CONVERT(varchar(10), @v_officeId_inN) <> '0'
	BEGIN
			SET @v_WhereCond_Vc= @v_WhereCond_Vc + ' dbo.tbl_TenderDetails.officeId = ' + CONVERT(varchar(10), @v_officeId_inN)
	END
	-- ==================================================
	
	-- SEARCH cpvCode CONDITION
	IF @v_WhereCond_Vc <> '' AND @v_cpvCode_inVc <> ''
			SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' AND '
			
	IF @v_cpvCode_inVc <> ''
	BEGIN
			SET @v_WhereCond_Vc= @v_WhereCond_Vc + ' dbo.tbl_TenderDetails.cpvCode LIKE ''%' + @v_cpvCode_inVc + +'%'''
	END
	-- ========================================================================
	
	-- SET TotalPage 
	--SET @v_TotalPagef = CEILING(@v_Reccountf / @v_RecordPerPage_inN)
	If @v_WhereCond_Vc<>''
	Begin
		select @v_WhereCond_Vc=' Where ' + @v_WhereCond_Vc
	End
	SET @v_MainQry_Vc = N'
		;WITH noaCTE (Row_ID,MDADetails , ContractId, ReferenceNo, Contractprice, ProcuringEntity, ContractName,District,ContractAwardedTo,NOADate,TenderId,officeId)
		AS
		(
			SELECT ROW_NUMBER() OVER(ORDER BY dbo.tbl_NoaIssueDetails.contractDt Desc) AS [Row_ID], 
			tbl_TenderDetails.ministry+ Case When dbo.tbl_TenderDetails.division <> '''' Then '' and <br/> '' + dbo.tbl_TenderDetails.division Else '''' End ,
			CONVERT(VARCHAR(30), dbo.tbl_NoaIssueDetails.noaIssueId) + '',<br/> '' + dbo.tbl_NoaIssueDetails.contractNo, 
			dbo.tbl_TenderDetails.reoiRfpRefNo + '', '' +ISNULL(REPLACE(CONVERT(VARCHAR(11), tbl_TenderDetails.tenderPubDt, 106), '' '', ''-'') + '' '' +SUBSTRING(CONVERT(VARCHAR(5), tbl_TenderDetails.tenderPubDt,108), 1, 5), ''-''),
			CONVERT(VARCHAR, dbo.tbl_NoaIssueDetails.contractAmt), 
			tbl_TenderDetails.peOfficeName + ''<br/> '' + tbl_TenderDetails.procurementMethod,
			contractName + '','' +CONVERT(VARCHAR(30), tbl_TenderDetails.tenderid)+ '','' +CONVERT(VARCHAR(30), tbl_NoaIssueDetails.pkgLotId)+ '','' +CONVERT(VARCHAR(30), tbl_NoaIssueDetails.userId),
			tbl_TenderDetails.peDistrict,
			dbo.f_getbiddercompany(tbl_NoaIssueDetails.userId)
			+ ''<br/> '' +
			
			(select case when cm.companyId=1 then tm.address1 else cm.regOffAddress end as FieldValue1
			from tbl_TendererMaster tm, tbl_CompanyMaster cm ,tbl_NoaIssueDetails tnid
			where cm.companyId = tm.companyId and tnid.userId = tm.userId and tnid.noaissueid = dbo.tbl_NoaIssueDetails.noaissueid ) 
			,
			ISNULL(REPLACE(CONVERT(VARCHAR(20), dbo.tbl_NoaIssueDetails.contractDt, 106), '' '', ''-''),''-''), tbl_NoaIssueDetails.tenderId as TenderId,
			tbl_TenderDetails.officeId as officeId
			FROM dbo.tbl_NoaIssueDetails INNER JOIN dbo.tbl_TenderDetails 
			ON dbo.tbl_NoaIssueDetails.tenderId = dbo.tbl_TenderDetails.tenderId and tbl_NoaIssueDetails.noaIssueId in 
			(select noaId from tbl_ContractNOARelation 
			-- where isPubAggOnWeb = ''yes''
			)' + @v_WhereCond_Vc + '
		),
		CTE_Total As (Select COUNT(*) AS TotalRowCount from noaCTE)
		SELECT c.Row_ID,c.MDADetails, c.ContractId, c.ReferenceNo, c.Contractprice, c.ProcuringEntity, c.ContractName,c.District,c.ContractAwardedTo,c.NOADate,
		CONVERT(VARCHAR(30), t.TotalRowCount) as TotalRowCount, CONVERT(VARCHAR(30), CEILING(t.TotalRowCount / @v_RecordPerPage_inN)) as TotalPages, c.TenderId,c.officeId
		FROM noaCTE c Cross Join CTE_Total t
		WHERE c.Row_ID >= ((@v_RecordPerPage_inN * @v_Page_inN) - (@v_RecordPerPage_inN - @v_Page_inN) - (@v_Page_inN - 1)) AND 
		c.Row_ID <= @v_RecordPerPage_inN * @v_Page_inN' 
		
	PRINT @v_MainQry_Vc
	EXEC sp_executesql @v_MainQry_Vc, @v_Params_Vc, @v_RecordPerPage_inN = @v_RecordPerPage_inN, @v_Page_inN = @v_Page_inN
	    
END


GO
