SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetContractAwardedOfflineData]
(
@searchFlag varchar(50),
@status varchar(50),
@procNature varchar(50),
@procMethod varchar(50),
@value varchar(50),
@refNo varchar(50),
@advDateFrom varchar(50),
@advDateTo varchar(50),
@ministry varchar(500),
@action varchar(50),
@comment varchar(500)
)
AS
BEGIN
	Declare
	@condition varchar(1000),
	@query varchar(max)
	DECLARE @flag_bit bit
	SET @condition = ''
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF @action = 'search'
		BEGIN
			IF @searchFlag = 'false' AND @status <>''
			BEGIN
				SELECT * FROM tbl_ContractAwardedOffline where [Status] = @status
			END
			ELSE IF @searchFlag = 'false' AND @refNo<>''
			BEGIN
				SELECT * FROM tbl_ContractAwardedOffline where RefNo = @refNo
			END
		ELSE IF @searchFlag = 'true'
		BEGIN
			IF @procNature is not null and @procNature<>''
				SELECT	@condition = @condition + ' AND AwardForPNature = '''+@procNature+''''
			IF @procMethod is not null and @procMethod<>''
				SELECT	@condition = @condition + ' AND procurementMethod = '''+@procMethod+''''
			IF @value is not null and @value<>''
				SELECT	@condition = @condition + ' AND ContractValue = '+Convert(varchar(50), @value)
			IF @refNo is not null and @refNo<>''
				SELECT	@condition = @condition + ' AND RefNo = '''+@refNo+''''
			IF @ministry is not null and @ministry<>''
				SELECT	@condition = @condition + ' AND (ministry = '''+@ministry+''' or division = '''+@ministry+'''or agency = '''+@ministry+''')'
			IF @advDateFrom is not null And @advDateTo is not null and @advDateFrom !='' and @advDateTo !=''
			Begin
				Select @condition =@condition + ' And Cast (Floor(Cast (DateofAdvertisement as Float)) as Datetime)  between ''' + @advDateFrom + ''' And ''' + @advDateTo + ''''
			End
			ELSE IF @advDateFrom is not null And @advDateTo is null and @advDateFrom !=''
			Begin
				Select @condition =@condition + ' And Cast (Floor(Cast (DateofAdvertisement as Float)) as Datetime)  >= ''' + @advDateFrom + ''''
			End
			ELSE IF @advDateFrom is null And @advDateTo is not null and @advDateTo =''
			Begin
				Select @condition =@condition + ' And Cast (Floor(Cast (DateofAdvertisement as Float)) as Datetime)  <= ''' + @advDateTo + ''''
			End

			--IF @txtAdvDateFrom is not null and @txtAdvDateFrom<>''
			--	SELECT	@condition = @condition + ' AND procurementMethod = '+@txtAdvDateFrom
			--IF @txtAdvDateTo is not null and @txtAdvDateTo<>''
			--	SELECT	@condition = @condition + ' AND procurementMethod = '''+@txtAdvDateTo+''''

			SELECT @query = 'SELECT * FROM tbl_ContractAwardedOffline where [Status] = '''+@status +'''' +@condition
			Print (@query)
			Exec (@query)
			--SELECT * FROM tbl_ContractAwardedOffline where [Status] = @status

		END
		END
	ELSE IF @action = 'approve'
		BEGIN
			UPDATE tbl_ContractAwardedOffline SET [status] = 'Approved',comment=@comment WHERE RefNo = @refNo
		END
	ELSE IF @action = 'delete'
		BEGIN
			DELETE FROM tbl_ContractAwardedOffline WHERE RefNo = @refNo
			Set @flag_bit=1
			Select @flag_bit as flag, 'Contract Award deleted.' as Message
		END

END


GO
