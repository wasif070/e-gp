SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spTenderDocInsert]
	-- Add the parameters for the stored procedure here
	@TenderID int,
	@v_CreatedBy_inInt int,
	@v_Procurementnature_inVc varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Declare @tenderStdId varchar(50)
Declare @tenderSectionId varchar(50)




Declare @sectionId varchar(50)
Declare @formId varchar(50)
Declare @tableId varchar(50)
Declare @noOfRows int
Declare @noOfCols int
Declare @pkgLotId varchar(50)
Declare @templatetableId int
Declare @isPriceBid varchar(50)
Declare @isMandatory varchar(50)

--select @tenderStdId=tts.tenderStdId from tbl_TenderStd tts where tts.tenderId=@tenderId 
--Select @tenderSectionId=tenderSectionId from tbl_TenderSection Where tenderStdId=@tenderStdId

--Select @tenderId TenderID,
--	   F.tenderSectionId SectionID,
--	   F.tenderFormId,
--	   T.tenderTableId, 
--	   T.noOfRows,
--	   T.noOfCols,
--	   pkgLotId,
--	   Tem.isMultipleFilling 
--from tbl_TenderForms F 
--left join tbl_TenderTables T on T.tenderFormId=F.tenderFormId
--left join tbl_TemplateTables Tem on tem.tableId=T.templatetableId
--Where F.tenderSectionId=@tenderSectionId
BEGIN TRY

DECLARE vendor_cursor CURSOR FOR 


Select @TenderID TenderID,
	   F.tenderSectionId SectionID,
	   F.tenderFormId,
	   T.tenderTableId, 
	   T.noOfRows,
	   T.noOfCols,
	   pkgLotId,
	   T.templatetableId,
	   F.isPriceBid,F.isMandatory
from tbl_TenderForms F 
left join tbl_TenderTables T on T.tenderFormId=F.tenderFormId
left join tbl_TemplateTables Tem on tem.tableId=T.templatetableId
left join tbl_TenderSection TS on TS.tenderSectionId=F.tenderSectionId
left join tbl_TenderStd TSTD on TSTD.tenderStdId=TS.tenderStdId
left join tbl_TenderFormula TF on TF.tenderTableId= T.tenderTableId
Where TSTD.tenderId=@TenderID And F.isPriceBid ='yes' 
Group by F.tenderSectionId ,F.tenderFormId,T.tenderTableId, T.noOfRows,T.noOfCols,pkgLotId,T.templatetableId,F.isPriceBid ,F.isMandatory


OPEN vendor_cursor
FETCH NEXT FROM vendor_cursor 
INTO @tenderId ,@sectionId,@formId,@tableId, @noOfRows, @noOfCols,@pkgLotId,@templatetableId,@isPriceBid,@isMandatory

WHILE @@FETCH_STATUS = 0
BEGIN
	--PRINT ' '
    --SELECT @tenderId tenderId,@sectionId sectionid,@formId formid,@tableId tableid, @noOfRows noofrows, @noOfCols noofcols,@pkgLotId,@templatetableId templatetableId
	
    DECLARE @LoopCol INT
    DECLARE @LoopRow INT
	SET @LoopCol = 0
	Set @noOfRows=2
	Declare @tenderSumId Int
	Update tbl_TenderTables Set noOfRows=@noOfRows Where tenderTableId=@tableId	
	
	if(@v_Procurementnature_inVc='Goods')
	Begin
		INSERT INTO [dbo].[tbl_TenderGrandSum]
			   ([tenderId]
			   ,[summaryName]
			   ,[creationDate]
			   ,[createdBy]
			   ,[pkgLotId])
			 VALUES
			   (@tenderId
			   ,'Total'
			   ,GETDATE()
			   ,@v_CreatedBy_inInt
			   ,@pkgLotId)
			set @tenderSumId=Ident_Current('dbo.[tbl_TenderGrandSum]') 
			
		if(@isMandatory='yes')
		begin
			Delete tbl_TenderColumns where tenderTableId = @tableId	
			
			exec spInserttbl_TenderColumns @tableId,1,'ItemNo','4','1','1','1','1','0'
			exec spInserttbl_TenderColumns @tableId ,2 ,'Description of Item' ,'2' ,'1','1','2','1','0'
			exec spInserttbl_TenderColumns @tableId ,3 ,'Measurement Unit' ,'1' ,'1','1','3','1','0'
			exec spInserttbl_TenderColumns @tableId ,4 ,'Quantity' ,'4' ,'1','1','4','1','0'
			exec spInserttbl_TenderColumns @tableId ,5 ,'Point of Delivery' ,'1' ,'1','1','5','1','0'
			exec spInserttbl_TenderColumns @tableId ,6 ,'Delivery Period(in days)' ,'4' ,'1','1','6','1','0'
			exec spInserttbl_TenderColumns @tableId ,7 ,'Country of Origin' ,'1' ,'2','1','7','1','0'
			exec spInserttbl_TenderColumns @tableId  ,8 ,'Unit Price in Figure  (BDT)' ,'13' ,'2','1','8','1','0'
			exec spInserttbl_TenderColumns @tableId ,9 ,'Unit Price in Words (BDT)' ,'2' ,'3','1','9','1','0'
			exec spInserttbl_TenderColumns @tableId ,10 ,'Total Price in Figure  (BDT)' ,'3' ,'3','7','10','1','0'
			exec spInserttbl_TenderColumns @tableId ,11 ,'Total Price in Words (BDT)' ,'2' ,'3','1','11','1','0' 
			
			Delete tbl_TenderCells where tenderTableId = @tableId
			
			exec spInserttbl_TenderCells @tableId, 1,4,'1',1,0,0,0
			exec spInserttbl_TenderCells @tableId, 1,2,'Laptop',2,0,0,1
			exec spInserttbl_TenderCells @tableId, 1,1,'Pcs',3,0,0,2
			exec spInserttbl_TenderCells @tableId, 1,4,'100',4,0,0,3
			exec spInserttbl_TenderCells @tableId, 1,1,'Dhaka',5,0,0,4
			exec spInserttbl_TenderCells @tableId, 1,4,'50',6,0,0,5
			exec spInserttbl_TenderCells @tableId, 1,1,'',7,0,0,6
			exec spInserttbl_TenderCells @tableId, 1,13,'',8,0,0,7
			exec spInserttbl_TenderCells @tableId, 1,2,'',9,0,0,8
			exec spInserttbl_TenderCells @tableId, 1,3,'',10,0,0,9
			exec spInserttbl_TenderCells @tableId, 1,2,'',11,0,0,10
			exec spInserttbl_TenderCells @tableId, 2,4,'',1,369,0,11
			exec spInserttbl_TenderCells @tableId, 2,2,'',2,369,0,12
			exec spInserttbl_TenderCells @tableId, 2,1,'',3,369,0,13
			exec spInserttbl_TenderCells @tableId, 2,4,'',4,369,0,14
			exec spInserttbl_TenderCells @tableId, 2,1,'',5,369,0,15
			exec spInserttbl_TenderCells @tableId, 2,4,'',6,369,0,16
			exec spInserttbl_TenderCells @tableId, 2,1,'',7,369,0,17
			exec spInserttbl_TenderCells @tableId, 2,13,'',8,369,0,18
			exec spInserttbl_TenderCells @tableId, 2,2,'',9,369,0,19
			exec spInserttbl_TenderCells @tableId, 2,3,'',10,369,0,20
			exec spInserttbl_TenderCells @tableId, 2,2,'',11,369,0,21
			
			Delete tbl_TenderListCellDetail Where  tenderTableId = @tableId
			
			INSERT INTO [dbo].[tbl_TenderGrandSumDetail]
			   ([tenderSumId]
			   ,[tenderFormId]
			   ,[cellId]
			   ,[tenderTableId]
			   ,[columnId])	
			 values(@tenderSumId,@formId,20,@tableId,null)  
			
		END
		else
			Begin
				Delete tbl_TenderColumns where tenderTableId = @tableId	
				exec spInserttbl_TenderColumns @tableId ,1,'Item No.','4','1','1','1','1',0
				exec spInserttbl_TenderColumns @tableId ,2,'Description of Item','2','1','1','2','1',0
				exec spInserttbl_TenderColumns @tableId ,3,'Measurement Unit','1','1','1','3','1',0
				exec spInserttbl_TenderColumns @tableId ,4,'Quantity','4','1','1','4','1',0
				exec spInserttbl_TenderColumns @tableId ,5,'Point of Completion','1','1','1','5','1',0
				exec spInserttbl_TenderColumns @tableId ,6,'Completion Period (in days)','4','1','1','6','1',0
				exec spInserttbl_TenderColumns @tableId ,7,'Country of Origin','1','2','1','7','1',0
				exec spInserttbl_TenderColumns @tableId ,8,'Unit Price in Figure (BDT)','13','2','1','8','1',0
				exec spInserttbl_TenderColumns @tableId ,9,'Unit Price in Words (BDT)','2','3','1','9','1',0
				exec spInserttbl_TenderColumns @tableId ,10,'Total Price in Figure (BDT)','3','3','7','10','1',0
				exec spInserttbl_TenderColumns @tableId ,11,'Total Price in Words (BDT)','2','3','1','11','1',0
				
				Delete tbl_TenderCells where tenderTableId = @tableId
				exec spInserttbl_TenderCells @tableId, 1,'4','2',1,0,0,0
				exec spInserttbl_TenderCells @tableId, 1,'2','Laptop Service',2,0,0,1
				exec spInserttbl_TenderCells @tableId, 1,'1','50',3,0,0,2
				exec spInserttbl_TenderCells @tableId, 1,'4','1000',4,0,0,3
				exec spInserttbl_TenderCells @tableId, 1,'1','Dhaka',5,0,0,4
				exec spInserttbl_TenderCells @tableId, 1,'4','35',6,0,0,5
				exec spInserttbl_TenderCells @tableId, 1,'1','',7,0,0,6
				exec spInserttbl_TenderCells @tableId, 1,'13','',8,0,0,7
				exec spInserttbl_TenderCells @tableId, 1,'2','',9,0,0,8
				exec spInserttbl_TenderCells @tableId, 1,'3','',10,0,0,9
				exec spInserttbl_TenderCells @tableId, 1,'2','',11,0,0,10
				exec spInserttbl_TenderCells @tableId, 2,'4','',1,370,0,11
				exec spInserttbl_TenderCells @tableId, 2,'2','',2,370,0,12
				exec spInserttbl_TenderCells @tableId, 2,'1','',3,370,0,13
				exec spInserttbl_TenderCells @tableId, 2,'4','',4,370,0,14
				exec spInserttbl_TenderCells @tableId, 2,'1','',5,370,0,15
				exec spInserttbl_TenderCells @tableId, 2,'4','',6,370,0,16
				exec spInserttbl_TenderCells @tableId, 2,'1','',7,370,0,17
				exec spInserttbl_TenderCells @tableId, 2,'13','',8,370,0,18
				exec spInserttbl_TenderCells @tableId, 2,'2','',9,370,0,19
				exec spInserttbl_TenderCells @tableId, 2,'3','',10,370,0,20
				exec spInserttbl_TenderCells @tableId, 2,'2','',11,370,0,21
				
				Delete tbl_TenderListCellDetail Where  tenderTableId = @tableId
				
				INSERT INTO [dbo].[tbl_TenderGrandSumDetail]
			   ([tenderSumId]
			   ,[tenderFormId]
			   ,[cellId]
			   ,[tenderTableId]
			   ,[columnId])	
			 values(@tenderSumId,@formId,20,@tableId,null)
			END
	END	
	
	Else	---------Works
		begin
			INSERT INTO [dbo].[tbl_TenderGrandSum]
			   ([tenderId]
			   ,[summaryName]
			   ,[creationDate]
			   ,[createdBy]
			   ,[pkgLotId])
			 VALUES
			   (@tenderId
			   ,'Total'
			   ,GETDATE()
			   ,@v_CreatedBy_inInt
			   ,@pkgLotId)
			set @tenderSumId=Ident_Current('dbo.[tbl_TenderGrandSum]') 
			
			if(@isMandatory='yes')
				Begin
				Delete tbl_TenderColumns where tenderTableId = @tableId	
				
				exec spInserttbl_TenderColumns @tableId,1,'Item no.','1','1','1','1',1,0
				exec spInserttbl_TenderColumns @tableId,2,'Groups','1','1','1','2',1,0
				exec spInserttbl_TenderColumns @tableId,3,'Item Code','2','1','1','3',1,0
				exec spInserttbl_TenderColumns @tableId,4,'Description of Item','2','1','1','4',1,0
				exec spInserttbl_TenderColumns @tableId,5,'Unit','1','1','1','5',1,0
				exec spInserttbl_TenderColumns @tableId,6,'Quantity','3','1','1','6',1,0
				exec spInserttbl_TenderColumns @tableId,7,'Unit Rate or price (In Figures) (BDT)','13','2','1','7',1,0
				exec spInserttbl_TenderColumns @tableId,8,'Unit Rate or price (In Words) (BDT)','2','3','1','8',1,0
				exec spInserttbl_TenderColumns @tableId,9,'Total Price / Amount (In Figures) (BDT)','3','3','7','9',1,0
				exec spInserttbl_TenderColumns @tableId,10,'Total Price / Amount (In Words) (BDT)','2','3','1','10',1,0

				
				Delete tbl_TenderCells where tenderTableId = @tableId
				
				exec spInserttbl_TenderCells @tableId, 1,'1','1',1,0,0,0
				exec spInserttbl_TenderCells @tableId, 1,'1','30 feet Road repair',2,0,0,1
				exec spInserttbl_TenderCells @tableId, 1,'2','Constraction',3,0,0,2
				exec spInserttbl_TenderCells @tableId, 1,'2','Road constraction',4,0,0,3
				exec spInserttbl_TenderCells @tableId, 1,'1','Feet',5,0,0,4
				exec spInserttbl_TenderCells @tableId, 1,'3','3000',6,0,0,5
				exec spInserttbl_TenderCells @tableId, 1,'13','',7,0,0,6
				exec spInserttbl_TenderCells @tableId, 1,'2','',8,0,0,7
				exec spInserttbl_TenderCells @tableId, 1,'3','',9,0,0,8
				exec spInserttbl_TenderCells @tableId, 1,'2','',10,0,0,9
				exec spInserttbl_TenderCells @tableId, 2,'1','',1,380,0,10
				exec spInserttbl_TenderCells @tableId, 2,'1','',2,380,0,11
				exec spInserttbl_TenderCells @tableId, 2,'2','',3,380,0,12
				exec spInserttbl_TenderCells @tableId, 2,'2','',4,380,0,13
				exec spInserttbl_TenderCells @tableId, 2,'1','',5,380,0,14
				exec spInserttbl_TenderCells @tableId, 2,'3','',6,380,0,15
				exec spInserttbl_TenderCells @tableId, 2,'13','',7,380,0,16
				exec spInserttbl_TenderCells @tableId, 2,'2','',8,380,0,17
				exec spInserttbl_TenderCells @tableId, 2,'3','',9,380,0,18
				exec spInserttbl_TenderCells @tableId, 2,'2','',10,380,0,19
				
				Delete tbl_TenderListCellDetail Where  tenderTableId = @tableId
				
				INSERT INTO [dbo].[tbl_TenderGrandSumDetail]
				   ([tenderSumId]
				   ,[tenderFormId]
				   ,[cellId]
				   ,[tenderTableId]
				   ,[columnId])	
				 values(@tenderSumId,@formId,20,@tableId,null)  
				
			END
			Else
				begin
					Delete tbl_TenderColumns where tenderTableId = @tableId	
					exec spInserttbl_TenderColumns @tableId ,1,'Item no.','1','1','1','1',1,0
					exec spInserttbl_TenderColumns @tableId ,2,'Groups','1','1','1','2',1,0
					exec spInserttbl_TenderColumns @tableId ,3,'Item Code','2','1','1','3',1,0
					exec spInserttbl_TenderColumns @tableId ,4,'Description of Item','2','1','1','4',1,0
					exec spInserttbl_TenderColumns @tableId ,5,'Unit','1','1','1','5',1,0
					exec spInserttbl_TenderColumns @tableId ,6,'Quantity','3','1','1','6',1,0
					exec spInserttbl_TenderColumns @tableId ,7,'Unit Rate or Price (In Figures) (BDT)','13','2','1','7',1,0
					exec spInserttbl_TenderColumns @tableId ,8,'Unit Rate or Price (In Words) (BDT)','2','3','1','8',1,0
					exec spInserttbl_TenderColumns @tableId ,9,'Total Price / Amount (In Figures) (BDT)','3','3','7','9',1,0
					exec spInserttbl_TenderColumns @tableId ,10,'Total Price / Amount (In Words) (BDT)','2','3','1','10',1,0
					
					
					Delete tbl_TenderCells where tenderTableId = @tableId
					exec spInserttbl_TenderCells @tableId, 1,'1','1',1,0,0,0
					exec spInserttbl_TenderCells @tableId, 1,'1','Pitch',2,0,0,1
					exec spInserttbl_TenderCells @tableId, 1,'2','Pitch 7',3,0,0,2
					exec spInserttbl_TenderCells @tableId, 1,'2','Roll Pitch in Road',4,0,0,3
					exec spInserttbl_TenderCells @tableId, 1,'1','Feet',5,0,0,4
					exec spInserttbl_TenderCells @tableId, 1,'3','5000',6,0,0,5
					exec spInserttbl_TenderCells @tableId, 1,'13','',7,0,0,6
					exec spInserttbl_TenderCells @tableId, 1,'2','',8,0,0,7
					exec spInserttbl_TenderCells @tableId, 1,'3','',9,0,0,8
					exec spInserttbl_TenderCells @tableId, 1,'2','',10,0,0,9
					exec spInserttbl_TenderCells @tableId, 2,'1','',1,381,0,10
					exec spInserttbl_TenderCells @tableId, 2,'1','',2,381,0,11
					exec spInserttbl_TenderCells @tableId, 2,'2','',3,381,0,12
					exec spInserttbl_TenderCells @tableId, 2,'2','',4,381,0,13
					exec spInserttbl_TenderCells @tableId, 2,'1','',5,381,0,14
					exec spInserttbl_TenderCells @tableId, 2,'3','',6,381,0,15
					exec spInserttbl_TenderCells @tableId, 2,'13','',7,381,0,16
					exec spInserttbl_TenderCells @tableId, 2,'2','',8,381,0,17
					exec spInserttbl_TenderCells @tableId, 2,'3','',9,381,0,18
					exec spInserttbl_TenderCells @tableId, 2,'2','',10,381,0,19

					
					Delete tbl_TenderListCellDetail Where  tenderTableId = @tableId
					
					INSERT INTO [dbo].[tbl_TenderGrandSumDetail]
				   ([tenderSumId]
				   ,[tenderFormId]
				   ,[cellId]
				   ,[tenderTableId]
				   ,[columnId])	
					values(@tenderSumId,@formId,20,@tableId,null)
				END
		end	
	--if(@isPriceBid ='yes')	 
	-- Begin
	--	SET @LoopRow = 0
	--	WHILE (@LoopRow < @noOfRows)
	--	BEGIN
	--	SET @LoopRow = @LoopRow + 1
	--	SET @LoopCol=0;
	--		WHILE (@LoopCol < @noOfCols)
	--			BEGIN
	--			SET @LoopCol = @LoopCol + 1
				
				
	--			DECLARE @dataType INT
	--			--DECLARE @dataType INT
	--			Select @dataType=dataType From tbl_TemplateColumns 
	--			Where tableId=@templatetableId And columnId=@LoopCol
				
	--			--INSERT INTO [egp_dev].[dbo].[tbl_TenderColumns]
	--			--Select @tableId tenderTableId,@LoopCol columnId,columnHeader,dataType,filledBy,columnType,sortOrder,showorhide,@templatetableId templateTableId 
	--			--From tbl_TemplateColumns 
	--			--Where tableId=@templatetableId And columnId=@LoopCol
	--			--select SCOPE_IDENTITY() tbl_TenderColumns
	--			INSERT INTO [egp_dev].[dbo].[tbl_TenderListCellDetail]
	--				   ([tenderListId]
	--				   ,[tenderTableId]             
	--				   ,[columnId]
	--				   ,[cellId]
	--				   ,[location])
	--			 VALUES
	--				   (2133
	--				   ,@tableId
	--				   ,@LoopCol
	--				   ,@LoopRow
	--				   ,@LoopRow)
	--				--select SCOPE_IDENTITY() tbl_TenderListCellDetail
	--				Declare @CellValue varchar(500)
	--				if @LoopRow=1 
	--				Begin
	--				Set @CellValue = case When @dataType=1 then  'Laptop' --Small Text
	--									  When @dataType=2 then  'Dell/Hp Laptop Core i3' --Long Text
	--									  When @dataType=3 then  '504' --Money Positive
	--									  When @dataType=4 then  '501' --Numeric
	--									  When @dataType=8 then  '400' --Money All
	--									  When @dataType=9 then  '1' --Combo Box with Calculation
	--									  When @dataType=10 then  '1' --Combo Box w/o Calculation
	--									  When @dataType=11 then  '205' --Money All (+5 to +5)
	--									  When @dataType=12 then  '05-06-2013' --Date
	--									  When @dataType=13 then  '506' --Money Positive(3 digits after decimal
	--									  else 'Nill'
	--									  END 	
	--				END
	--				Else
	--				Begin
	--				Set @CellValue=''
	--				END					  
	--				--Select @templatetableId templatetableId,@LoopRow LoopRow,@LoopCol LoopCol,@dataType datatype
	--				INSERT INTO [egp_dev].[dbo].[tbl_TenderCells]
	--					   ([tenderTableId]
	--					   ,[rowId]
	--					   ,[cellDatatype]
	--					   ,[cellvalue]
	--					   ,[columnId]
	--					   ,[templateTableId]
	--					   ,[templateColumnId]
	--					   ,[cellId]
	--					   ,[colId]
	--					   ,[tenderColId]
	--					   ,[showOrHide])
	--				VALUES
	--					   (@tableId
	--					   ,@LoopRow
	--					   ,@dataType					   
	--					   ,@CellValue
	--					   ,@LoopCol
	--					   ,Case When  @LoopRow=1 then @templatetableId else 0 END
	--					   ,Case When  @LoopRow=1 then @LoopCol else 0 END
	--					   ,@LoopCol
	--					   ,null--@LoopCol
	--					   ,(Select tenderColId from tbl_tenderColumns WHERE tenderTableId=@tableId and ColumnId=@LoopCol)
	--					   ,null)
	--			END
		
		
			
			
				
				   
	--			 --Select @tableId tenderTableId, @LoopRow,cellDatatype,cellvalue,columnId,@templatetableId templateTableId,columnId templateColumnId ,cellId,columnId,@LoopCol tenderColId,showOrHide
	--				--   from tbl_TemplateCells 
	--				--   WHERE tableId=@templatetableId  and columnId=@LoopCol
				
	--		--select SCOPE_IDENTITY() tbl_TenderCells,@tableId TableID	   
		    
		
		
	--	END
		
	-- END 
		
	
		FETCH NEXT FROM vendor_cursor 
		INTO @tenderId,@sectionId,@formId,@tableId, @noOfRows, @noOfCols,@pkgLotId,@templatetableId,@isPriceBid,@isMandatory
END

------/////////// Grand Total \\\\\\\\\\\\\\\--------------
--Declare @pkgLotId varchar(50)
--Declare @tenderFormId varchar(50)
--Declare @tenderSumId bigint
----Declare @tenderId varchar(50)

--DECLARE Grand_Total CURSOR FOR 
--Select F.pkgLotId,F.tenderFormId,TSTD.tenderId TenderID
-- from tbl_TenderForms F 
--left join tbl_TenderTables T on T.tenderFormId=F.tenderFormId
--left join tbl_TenderSection TS on TS.tenderSectionId=F.tenderSectionId
--left join tbl_TenderStd TSTD on TSTD.tenderStdId=TS.tenderStdId
--left join tbl_TenderFormula TF on TF.tenderTableId= T.tenderTableId
--Where TSTD.tenderId=@TenderID  and TF.isGrandTotal='yes'  AND F.isPriceBid='yes' 

--OPEN Grand_Total
--FETCH NEXT FROM Grand_Total 
--INTO @pkgLotId,@tenderFormId,@tenderId

--WHILE @@FETCH_STATUS = 0
--BEGIN

--	if((Select COUNT(tenderSumId) from tbl_TenderGrandSum Where pkgLotId=@pkgLotId and tenderId=@tenderId)=0)
--	begin
--		INSERT INTO [egp_dev].[dbo].[tbl_TenderGrandSum]
--           ([tenderId]
--           ,[summaryName]
--           ,[creationDate]
--           ,[createdBy]
--           ,[pkgLotId])
--	     VALUES
--		   (@tenderId
--		   ,'Total'
--		   ,GETDATE()
--		   ,@v_CreatedBy_inInt
--		   ,@pkgLotId)
--		set @tenderSumId=Ident_Current('dbo.[tbl_TenderGrandSum]') 
--	END
	
--	INSERT INTO [egp_dev].[dbo].[tbl_TenderGrandSumDetail]
--           ([tenderSumId]
--           ,[tenderFormId]
--           ,[cellId]
--           ,[tenderTableId]
--           ,[columnId])	
--		select @tenderSumId tenderSumId,@tenderFormId  tenderFormId, ttc.cellId, ttc.tenderTableId, null columnId
--		from tbl_TenderCells ttc, tbl_TenderColumns tc 
--		where	
--		tc.columnId = ttc.columnId and ttc.tenderTableId = tc.tenderTableId 
--		and ttc.columnId in (select ttf.columnId from tbl_TenderFormula ttf
--		where ttf.tenderFormId =@tenderFormId and ttf.formula like '%TOTAL%' ) 
--		and ttc.tenderTableId in (select ttt.tenderTableId from tbl_TenderTables ttt where ttt.tenderFormId =@tenderFormId)
--		and ttc.rowId in(select tt.noOfRows from tbl_TenderTables tt where tt.tenderTableId in
--		 (select t.tenderTableId from tbl_TenderTables t where t.tenderFormId =@tenderFormId))
--		group By ttc.cellId, ttc.tenderTableId, tc.columnHeader 
--	--Select Ident_Current('dbo.[tbl_TenderGrandSumDetail]') 
	
	
--	FETCH NEXT FROM Grand_Total 
--	INTO @pkgLotId,@tenderFormId,@tenderId
--END
--Select @tenderSumId tenderSumID ,'Insert Successfully' massage

----------------////////////// \\\\\\\\\\\\\\\\\-----------------
END TRY
	BEGIN CATCH
		select  ERROR_NUMBER() Number, ERROR_MESSAGE() as Message
	END CATCH;
	
CLOSE vendor_cursor;
DEALLOCATE vendor_cursor;	

--CLOSE Grand_Total;
--DEALLOCATE Grand_Total;
end


GO
