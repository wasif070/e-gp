SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SPGetTenderCellValueForIct]
@colList varchar(50),
@tenderFormId int,
@bidTableId int,
@tenderTableid int,
@tenderId int,
@rowId int

as
Declare @count int
Declare @countColumn int
Declare @column varchar(100)
Declare @columnValue varchar(100)
Declare @columnValueWithCur varchar(100)
Declare @colCount int
Declare @cellDataType int
Declare @cellId int
Declare @listBoxName varchar(100)
Declare @exchangeRate decimal(20,3)
Declare @CommaIndex int
set @column = ''
set @columnValue = ''
set  @colCount = len(@colList)
set @Count = 0
Set @CommaIndex = CHARINDEX(',',@colList)
        While (@CommaIndex >  -1)
            Begin
                if(@CommaIndex = 0)
                    Set @column = @colList
                else
                    Set @column = left(@colList,LEN(@colList)-(LEN(@colList)-@CommaIndex)-1)
                --SET @columnValueWithCur = @column
                Select @countColumn = count(*) from tbl_tenderCells where tenderTableId = @tenderTableid and cellValue <> '' and columnId = Convert(int,@column)
                if(@countColumn > 0)
                    BEGIN
                        select @columnValueWithCur = cellvalue from tbl_tenderCells where tenderTableId = @tenderTableid and cellValue <> '' and columnId = Convert(int,@column) and rowId = @rowId
                    END
                else
                    BEGIN
                        select @columnValueWithCur = cellvalue from tbl_TenderBidPlainData where bidTableId  = @bidTableId and tenderFormId = @tenderFormId and tenderColId = Convert(int,@column) and cellValue <> 'null' and rowId = @rowId
                        --currency check at tenderer end
                        select @cellDataType=  max(celldatatype) from tbl_TenderCells where tenderTableId=@tenderTableid and columnId = (Convert(int,@column)+1)
                        If(@cellDataType =9 OR @cellDataType =10)
                                    Begin                             
                                        select top 1 @cellId = cellId from tbl_TenderCells where tenderTableId=@tenderTableid and columnId=(Convert(int,@column)+1)                       
                                        select top 1 @listBoxName = tlb.listBoxName from tbl_TenderListCellDetail tlcd, tbl_TenderListBox tlb
                                        where tlcd.tenderTableId=@tenderTableid and tlcd.columnId = (Convert(int,@column)+1) and tlcd.cellId = @cellId and tlcd.tenderListId = tlb.tenderListId
                                       
                                        if @listBoxName = 'Currency'
                                            Begin
                                           
                                                select @exchangeRate = exchangeRate from tbl_tenderCurrency where tenderId = @tenderId and currencyId = Convert(decimal(20,3),(select cellvalue from tbl_TenderBidPlainData where bidTableId  = @bidTableId and tenderFormId = @tenderFormId and tenderColId = (Convert(int,@column)+1) and rowId = @rowId and cellValue <> 'null'))
                                                Set @columnValueWithCur = Convert(varchar,(Convert(decimal(20,3),@columnValueWithCur) * @exchangeRate))
                                            End
                                    End                       
                    END   
                Set @columnValue =     @columnValue +','+@columnValueWithCur
           
                --if(len(@colList)>1)
                    --Set @colList = RIGHT(@colList,len(@colList)-2)
                   
                if(@CommaIndex = 0)
                    Set @CommaIndex = -1
                else
                    begin   
                    Set @colList = RIGHT(@colList,LEN(@colList)-@CommaIndex)
                    Set @CommaIndex = CHARINDEX(',',@colList)    
                    end
                Set @Count = @Count + 2
            End --End While
           
Set @columnValue = RIGHT(@columnValue,len(@columnValue)-1) 
Select @columnValue as ColumnValue


GO
