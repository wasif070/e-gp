SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Inserts/Updates tables related to mail message.
--
--
-- Author: Karan
-- Date: 25-10-2010
--
-- Last Modified:
-- Modified By: Karan
-- Date 22-11-2010
-- Modification: Default Value of ProcessUrl changed from Null to ''
--------------------------------------------------------------------------------
-- SP Name	:   p_add_upd_mailmessage
-- Module	:	Message Box
-- Function	:	Inbox,trash - insert/update inbox-trash messages
--				sent,trash - insert/update sent-trash messages
--				draft,trash - insert/update draft-trash messages
--				Inbox - get list of inbox messages
--				Draft - get list of draft messages
--------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_add_upd_mailmessage]
@v_ToEmailId_inVc varchar(4000), -- To Email Id(s)
@v_FromEmailId_inVc varchar(100), -- Sender Email Id
@v_CCEmailId_inVc varchar(4000)=null, -- CC Eamil Id(s)
@v_Subject_inVc varchar(150), -- Email Subject
@v_MsgText_inVc varchar(max), -- Email Content Text
@v_ProcessUrl_inVc varchar(250)='',
@v_IsPriority_inVc varchar(10), -- Priority
@v_MsgBoxType_inVc varchar(50), -- Message Type. Eg; Inbox, Sent, or Draft
@v_Action_inVc varchar(50)=null, -- Action Type. Eg; Create, Edit or Trash
@v_MsgStatus_inVc varchar(10), -- Message Status. Eg; 
@v_FolderId_inInt int, -- Folder Id
@v_MsgReadStatus_inVc varchar(3), -- 
@v_IsMsgReply_inVc varchar(5), -- 
@v_MsgDocId_inVc varchar(4000), -- Comma(,) separated string of Document Id(s)
@v_UpdMsgId_inInt int=null, -- Message Id(Primary Key) of Message (Update Case)
@v_msgBoxTypeIdInt int=null -- Message Inbox Id (Primary Key)

AS

BEGIN

DECLARE 
 @v_MsgIdInt int,
 @v_FromUserIdInt int,
 @v_AllEmailId_inVc varchar(max),
 @v_MsgDocId_inInt int,
 @v_isDocUploadedVc varchar(3),
 @v_UserIdInt int,
 @v_msgTypeIdInt int,
 @v_msgToCC_Vc varchar(10),
 @v_insDraft_Int int,
 @v_flag_bit bit
SET NOCOUNT ON
	 
	 
/* START : SET VALUES */	 
 SELECT @v_FromUserIdInt= userid FROM tbl_LoginMaster WHERE emailId=@v_FromEmailId_inVc
	 
 SET @v_AllEmailId_inVc = @v_ToEmailId_inVc
 If @v_CCEmailId_inVc is Not Null And @v_CCEmailId_inVc <> ''
 Begin
	set @v_AllEmailId_inVc = @v_AllEmailId_inVc + ', ' + @v_CCEmailId_inVc
 End
/* END : SET VALUES */	 

 SET @v_isDocUploadedVc = (Select Case when @v_MsgDocId_inVc='0' Then 'No' Else 'Yes' End)	 
	
IF @v_MsgBoxType_inVc='Inbox' And @v_Action_inVc='Trash' And @v_msgBoxTypeIdInt is Not Null 
BEGIN
	BEGIN TRY
		BEGIN TRAN			
				-- // UPDATE INBOX MESSAGE TABLE - tbl_MessageInBox
				--UPDATE tbl_MessageInBox SET msgStatus='Trash' WHERE msgBoxID = @v_msgBoxTypeIdInt
				
				/* START CODE : TO INSERT INTO TRASH TABLE - tbl_MessageTrash */
				--INSERT INTO tbl_MessageTrash
				--	  (msgBoxTypeId, msgBoxType, msgId, msgToUserId, msgFromUserId, msgToCCReply, msgStatus, creationDate, folderId, isPriority, isMsgRead, isDocUpLoaded, processurl )
				--SELECT msgBoxId, @v_MsgBoxType_inVc, msgid, msgToUserId, msgFromUserId, msgToCCReply, msgStatus, GETDATE(), folderId, isPriority, ismsgRead, isDocUploaded, processUrl 
				--FROM tbl_MessageInBox 
				--	WHERE msgBoxId=@v_msgBoxTypeIdInt And msgStatus='Trash'
				/* END CODE : TO INSERT INTO TRASH TABLE - tbl_MessageTrash */
				Set @v_flag_bit=1
				Select @v_flag_bit as flag, 'Message has been sent to Trash.' as Message,@v_msgBoxTypeIdInt as Id
		COMMIT TRAN	
	END TRY 
	BEGIN CATCH
			BEGIN
				Set @v_flag_bit=0
				Select @v_flag_bit as flag, 'Error while moving message to Trash.' as Message,0 as Id
				ROLLBACK TRAN
			END
	END CATCH	 
END

ELSE IF @v_MsgBoxType_inVc='Sent' And @v_Action_inVc='Trash' And @v_UpdMsgId_inInt is Not Null And @v_FromUserIdInt is Not Null
BEGIN
	BEGIN TRY
		BEGIN TRAN	
			-- // UPDATE SENT MESSAGE TABLE - tbl_MessageInBox
			--UPDATE tbl_MessageSent SET msgStatus='Trash' WHERE msgID = @v_UpdMsgId_inInt And msgFromUserId=@v_FromUserIdInt  
			
			/* START CODE : TO INSERT INTO TRASH TABLE - tbl_MessageTrash */
			--INSERT INTO tbl_MessageTrash
			--	  (msgBoxtypeId, msgBoxType, msgId, msgToUserId, msgFromUserId, msgToCCReply, msgStatus, creationDate, folderId, isPriority, isMsgRead, isDocUpLoaded, processurl )
			--SELECT msgSentId, @v_MsgBoxType_inVc, msgId, msgToUserId, msgFromUserId, msgToCCReply, msgStatus, GETDATE(), folderId, isPriority, isMsgRead, isDocUpLoaded, '' 
			--FROM tbl_MessageSent
			--	WHERE msgID = @v_UpdMsgId_inInt And msgFromUserId=@v_FromUserIdInt And msgStatus='Trash'
			/* END CODE : TO INSERT INTO TRASH TABLE - tbl_MessageTrash */
			
			Set @v_flag_bit=1
			Select @v_flag_bit as flag, 'Message has been sent to Trash.' as Message,@v_UpdMsgId_inInt as Id
		COMMIT TRAN	
	END TRY 
	BEGIN CATCH
			BEGIN
				Set @v_flag_bit=0
				Select @v_flag_bit as flag, 'Error while moving message to Trash.' as Message,0 as Id
				ROLLBACK TRAN
			END
	END CATCH	 
END

ELSE IF @v_MsgBoxType_inVc='Draft' And @v_Action_inVc='Trash' And @v_UpdMsgId_inInt is Not Null And @v_FromUserIdInt is Not Null
BEGIN
	BEGIN TRY
		BEGIN TRAN	
			-- // UPDATE DRAFT MESSAGE TABLE - tbl_MessageDraft
			--UPDATE tbl_MessageDraft SET msgStatus='Trash' WHERE msgID = @v_UpdMsgId_inInt And msgFromUserId=@v_FromUserIdInt  
			
			/* START CODE : TO INSERT INTO TRASH TABLE - tbl_MessageTrash */
			--INSERT INTO tbl_MessageTrash
			--	  (msgBoxtypeId, msgBoxType, msgId, msgToUserId, msgFromUserId, msgToCCReply, msgStatus, creationDate, folderId, isPriority, isMsgRead, isDocUpLoaded, processurl )
			--SELECT msgDraftId, @v_MsgBoxType_inVc, msgId, msgToUserId, msgFromUserId, msgToCCReply, msgStatus, GETDATE(), folderId, isPriority, isMsgRead, isDocUpLoaded, '' 
			--FROM tbl_MessageDraft
			--	  WHERE msgID = @v_UpdMsgId_inInt And msgFromUserId=@v_FromUserIdInt And msgStatus='Trash'
			/* END CODE : TO INSERT INTO TRASH TABLE - tbl_MessageTrash */
			Set @v_flag_bit=1
			Select @v_flag_bit as flag, 'Message has been sent to Trash.' as Message,@v_UpdMsgId_inInt as Id
		COMMIT TRAN	
	END TRY 
	BEGIN CATCH
			BEGIN
				Set @v_flag_bit=0
				Select @v_flag_bit as flag, 'Error while moving message to Trash.' as Message,0 as Id
				ROLLBACK TRAN
			END
	END CATCH	 
END

Else IF @v_MsgBoxType_inVc='Inbox'
 BEGIN
	BEGIN TRY
		BEGIN TRAN Tran1	
			 
			if @v_FolderId_inInt <> 0 and @v_msgBoxTypeIdInt is not null and @v_Action_inVc = 'update'
			begin				
				--Update tbl_MessageInBox set folderid = @v_FolderId_inInt where msgBoxId = @v_msgBoxTypeIdInt
				Set @v_flag_bit=1
				Select @v_flag_bit as flag, 'Message Moved Successfully.' as Message,@v_msgBoxTypeIdInt as Id
				
			End
 
			/* START CODE : TO INSERT INTO TABLE - tbl_Message */
			--INSERT INTO tbl_Message (subject, msgText) VALUES (@v_Subject_inVc, @v_MsgText_inVc)
			--set @v_MsgIdInt=Ident_Current('dbo.tbl_Message') 
			/* END CODE : TO INSERT INTO TABLE - tbl_Message */
			
			--CURSOR STARTS
			--Blocked By Rokib
	/*		
			Declare CURSOR_INBOX cursor fast_forward 
			for 
				Select L.userId, A.ToCC from 
				(select dbo.f_trim(items) as Emailid , 'To' as ToCC from dbo.[f_split](@v_ToEmailId_inVc,',')
				Union
				select dbo.f_trim(items) as Emailidc, 'CC' as ToCC from dbo.[f_split](@v_CCEmailId_inVc,',')
				) as  A Inner join tbl_LoginMaster L On A.Emailid=L.emailId			
    
			Open CURSOR_INBOX    
			Fetch next from CURSOR_INBOX into @v_UserIdInt, @v_msgToCC_Vc    
			while @@Fetch_Status = 0    
			Begin  
				/* START CODE : TO INSERT INTO TABLE - tbl_MessageInBox */	
				Insert Into tbl_MessageInBox
					(msgToUserId, msgFromUserId, msgId, msgToCCReply, msgStatus, creationDate, 
					folderId, isPriority, isMsgRead, isDocUploaded, processUrl )
				Select @v_UserIdInt , @v_FromUserIdInt, @v_MsgIdInt, @v_msgToCC_Vc, @v_MsgStatus_inVc, GETDATE(), 
					@v_FolderId_inInt, @v_IsPriority_inVc, @v_MsgReadStatus_inVc, @v_isDocUploadedVc, @v_ProcessUrl_inVc 
			
				Set @v_msgTypeIdInt=Ident_Current('dbo.tbl_MessageInBox') 		
			
				/* START CODE : TO INSERT INTO TABLE - tbl_MessageDocDetails  */	
				If @v_isDocUploadedVc='Yes'	
				Begin			
					Insert into tbl_MessageDocDetails 
							(msgTypeID, msgType, msgDocId, msgUploadedDate)	
					Select @v_msgTypeIdInt, 'Inbox', msgDocId, GETDATE() 
						From tbl_MessageDocument 
						Where msgDocId in (Select items From dbo.f_split(@v_MsgDocId_inVc,','))
				End
				/* END CODE : TO INSERT INTO TABLE - tbl_MessageDocDetails  */
				
				/* END CODE : TO INSERT INTO TABLE - tbl_MessageInBox */
		
		
				/* START CODE : TO INSERT INTO TABLE - Tbl_MessageSent */	
				Insert Into tbl_MessageSent
					(msgToUserId, msgFromUserId, msgId, msgToCCReply, msgStatus, creationDate, 
					folderId, isPriority, isMsgRead, isDocUploaded  )
				Select @v_UserIdInt , @v_FromUserIdInt, @v_MsgIdInt, @v_msgToCC_Vc, @v_MsgStatus_inVc, GETDATE(), 
					@v_FolderId_inInt, @v_IsPriority_inVc, @v_MsgReadStatus_inVc, @v_isDocUploadedVc  
				
				Set @v_msgTypeIdInt=Ident_Current('dbo.tbl_MessageSent') 
			
				/* START CODE : TO INSERT INTO TABLE - tbl_MessageDocDetails  */	
				If @v_isDocUploadedVc='Yes'	
				Begin			
					Insert into tbl_MessageDocDetails 
							(msgTypeID, msgType, msgDocId, msgUploadedDate)	
					Select @v_msgTypeIdInt, 'Sent', msgDocId, GETDATE() 
						From tbl_MessageDocument 
						Where msgDocId in (Select items From dbo.f_split(@v_MsgDocId_inVc,','))
				End
				/* END CODE : TO INSERT INTO TABLE - tbl_MessageDocDetails  */
					
				/* END CODE : TO INSERT INTO TABLE - Tbl_MessageSent */
		
					Fetch next from CURSOR_INBOX into @v_UserIdInt, @v_msgToCC_Vc    
	    
				End    
			close CURSOR_INBOX    
			Deallocate CURSOR_INBOX 
			--CURSOR ENDS
	
			If @v_MsgIdInt is Not Null -- ON SUCCESS MESSAGE
			Begin
				Set @v_flag_bit=1
				Select @v_flag_bit as flag, 'Message inserted in Inbox.' as Message,@v_MsgIdInt as Id
			End
			Else -- ON FAIL MESSAGE
			Begin
				Set @v_flag_bit=0
				Select @v_flag_bit as flag, ERROR_MESSAGE() as msg,0 as Id
			End
	*/ -- Blocked By Rokib
		COMMIT TRAN Tran1	
	END TRY 
	BEGIN CATCH
			BEGIN
				If Cursor_Status ('Global', 'CURSOR_INBOX')=1
				Begin
					close CURSOR_INBOX    
					Deallocate CURSOR_INBOX 
				End
				If Cursor_Status ('Global', 'CURSOR_INBOX')=-1
				Begin					
					Deallocate CURSOR_INBOX 
				End
				
				Set @v_flag_bit=0
				Select @v_flag_bit as flag, ERROR_MESSAGE() as msg
			
				ROLLBACK TRAN Tran1
			END
	END CATCH
 END

ELSE IF @v_MsgBoxType_inVc='Draft'
 BEGIN
	BEGIN TRY
		BEGIN TRAN	
		
			If @v_Action_inVc='Create'
			Begin
				/* START CODE : TO INSERT INTO TABLE - tbl_Message */
				--INSERT INTO tbl_Message (subject, msgText) VALUES (@v_Subject_inVc, @v_MsgText_inVc)
				set @v_MsgIdInt=Ident_Current('dbo.tbl_Message') 
				/* END CODE : TO INSERT INTO TABLE - tbl_Message */
				
			End
			Else If @v_Action_inVc='Edit'
			Begin
				/* START CODE : TO UPDATE TABLE - tbl_Message */
				--UPDATE tbl_Message SET subject=@v_Subject_inVc , msgText=@v_MsgText_inVc Where msgID=@v_UpdMsgId_inInt
				set @v_MsgIdInt=@v_UpdMsgId_inInt
				/* END CODE : TO UPDATE TABLE - tbl_Message */	
				
				--DELETE from tbl_MessageDocDetails Where msgDocId In (Select msgDocId From tbl_MessageDraft Where msgId=@v_MsgIdInt)	
				--DELETE from tbl_MessageDraft Where msgId=@v_MsgIdInt			
			End
			
			--CURSOR STARTS 
			/*Declare curDraft cursor fast_forward 
			for
			Select L.userId, A.ToCC from 
			(select dbo.f_trim(items) as Emailid , 'To' as ToCC from dbo.[f_split](@v_ToEmailId_inVc,',')
			Union
			select dbo.f_trim(items) as Emailidc, 'CC' as ToCC from dbo.[f_split](@v_CCEmailId_inVc,',')
			) as  A Inner join tbl_LoginMaster L On A.Emailid=L.emailId				  	    
				
			Open curDraft    
			Fetch next from curDraft into @v_UserIdInt, @v_msgToCC_Vc    
			while @@Fetch_Status = 0    
			Begin  
				/* START CODE : TO INSERT INTO TABLE - tbl_MessageDraft */	
				Insert Into tbl_MessageDraft
					(msgId, msgToUserId, msgFromUserId, msgToCCReply, msgStatus, creationDate, 
					folderId, isPriority, isMsgRead, isDocUploaded )
				Select @v_MsgIdInt, @v_UserIdInt , @v_FromUserIdInt, @v_msgToCC_Vc, @v_MsgStatus_inVc, GETDATE(), 
					@v_FolderId_inInt, @v_IsPriority_inVc, @v_MsgReadStatus_inVc, @v_isDocUploadedVc
				/* END CODE : TO INSERT INTO TABLE - tbl_MessageDraft */	
							
				 Set @v_msgTypeIdInt=Ident_Current('dbo.tbl_MessageDraft') 
				
	 			/* START CODE : TO INSERT INTO TABLE - tbl_MessageDocDetails  */	
				If @v_isDocUploadedVc='Yes'	
				Begin			
					Insert into tbl_MessageDocDetails 
							(msgTypeID, msgType, msgDocId, msgUploadedDate)	
					Select @v_msgTypeIdInt, 'Draft', msgDocId, GETDATE() 
						From tbl_MessageDocument 
						Where msgDocId in (Select items From dbo.f_split(@v_MsgDocId_inVc,','))
				End			 
				/* END CODE : TO INSERT INTO TABLE - tbl_MessageDocDetails  */  
				
				
				 Fetch next from curDraft into @v_UserIdInt, @v_msgToCC_Vc    
			    
				End    
			Close curDraft    
			Deallocate curDraft 
			--CURSOR ENDS
			*/
				If @v_MsgIdInt is Not Null -- ON SUCCESS MESSAGE
				Begin
					If @v_Action_inVc='Create'
					Begin
					Set @v_flag_bit=1
						Select @v_flag_bit as flag, 'Draft created.' as Message,@v_MsgIdInt as Id
						End
					Else If @v_Action_inVc='Edit'	
					Begin
					Set @v_flag_bit=1
						Select @v_flag_bit flag, 'Draft updated.' as Message,@v_MsgIdInt as Id
						End
				End
				Else
				Begin -- MESSAGE ON FAIL
					If @v_Action_inVc='Create' 
					begin
					Set @v_flag_bit=0
						Select 0 as flag, error_message() as Message,0 as Id
					end	
					Else If @v_Action_inVc='Edit'	
					begin
						Set @v_flag_bit=0
						Select @v_flag_bit as flag, 'Draft not updated.' as Message,0 as Id
					End
				End
		
		COMMIT TRAN	
	END TRY 
	BEGIN CATCH
			BEGIN
				If Cursor_Status ('Global', 'curDraft')=1
				Begin
					close curDraft    
					Deallocate curDraft 
				End
				If Cursor_Status ('Global', 'curDraft')=-1
				Begin					
					Deallocate curDraft 
				End
			
				Set @v_flag_bit=0
				Select @v_flag_bit as flag, 'Error while creating draft.' as Message
				ROLLBACK TRAN
			END
	END CATCH
	
 END
 
 
SET NOCOUNT OFF
	
 END


GO
