SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: View Message Box.
--
--
-- Author: Rajesh Singh
-- Date: 29-10-2010
--
-- Last Modified:
-- Modified By:
-- Date 29-10-2010
-- Modification:
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- SP Name	:   p_add_upd_workflowruleengine
-- Module	:	Workflow
-- Function	:	Insert - inserting data into tbl_WorkflowRuleEngine and tbl_WorkFlowRuleEngineHist
--				Edit - inserting/updating data into tbl_WorkflowRuleEngine and tbl_WorkFlowRuleEngineHist
--				select - inserting data into tbl_WorkflowRuleEngine and tbl_WorkFlowRuleEngineHist
--------------------------------------------------------------------------------
-- For Action='insert'
	--p_add_upd_workflowruleengine 1,2,1,2,'yes',1,'insert',1,'2010-10-29 12:11:23.057' 
-- For Action='edit'
	--p_add_upd_workflowruleengine  @v_eventidN='1',@v_actionVc='edit',@v_initiatorwfroleidN='1',@v_approverwfroleidN='2',@v_initiatorprocurementroleidN=1,@v_approverprocurementroleidN=2,@v_ispublishdaterequiredVc='yes'
	
	
CREATE PROCEDURE [dbo].[p_add_upd_workflowruleengine] 
		@v_initiatorprocurementroleidN Int=NULL,
		@v_approverprocurementroleidN Int=NULL,
		@v_initiatorwfroleidN varchar(50)=NULL, --1,2,3
		@v_approverwfroleidN varchar(50)=NULL,  --1,2,3
		@v_ispublishdaterequiredVc varchar(3)=NULL,
		@v_eventidN Int=NULL,
		@v_actionVc varchar(10)=NULL, --(insert / edit / select)
		@v_actionbyN Int=NULL,
		@v_actiondated datetime=NULL,
		@v_wfinitruleidN int=NULL
	 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_flag_bit bit	
    IF @v_actionVc='insert'
		BEGIN
			BEGIN TRY
				BEGIN TRAN
					BEGIN
					
						--Inserting Data in tbl_WorkflowRuleEngine of initiator
						INSERT INTO tbl_WorkflowRuleEngine([eventId],[wfRoleId],[isPubDateReq], [procurementRoleId])
						SELECT @v_eventidN,@v_initiatorprocurementroleidN,@v_ispublishdaterequiredVc,items From dbo.f_split(@v_initiatorwfroleidN,',')
						
						--Inserting Data in tbl_WorkFlowRuleEngineHist of initiator
						INSERT INTO tbl_WorkFlowRuleEngineHist([wfRoleId],[eventId],[actionDate],[actionBy],[action],[isPubDateReq],[wfInitRuleId],[procurementRoleId])
						SELECT @v_initiatorprocurementroleidN,@v_eventidN,@v_actiondated,@v_actionbyN,@v_actionVc,@v_ispublishdaterequiredVc,@@IDENTITY,items From dbo.f_split(@v_initiatorwfroleidN,',')
						
						--Inserting Data in tbl_WorkFlowRuleEngine of approver
						INSERT INTO tbl_WorkflowRuleEngine([eventId],[wfRoleId],[isPubDateReq],[procurementRoleId])
						SELECT @v_eventidN,@v_approverprocurementroleidN,@v_ispublishdaterequiredVc,items From dbo.f_split(@v_approverwfroleidN,',')
						
						--Inserting Data in tbl_WorkFlowRuleEngineHist of approver
						INSERT INTO tbl_WorkFlowRuleEngineHist([procurementRoleId],[eventId],[actionDate],[actionBy],[action],[isPubDateReq],[wfInitRuleId],[wfRoleId])
						SELECT @v_approverprocurementroleidN,@v_eventidN,@v_actiondated,@v_actionbyN,@v_actionVc,@v_ispublishdaterequiredVc,@@IDENTITY,items From dbo.f_split(@v_approverwfroleidN,',')
						
						Set @v_flag_bit=1
						Select @v_flag_bit as flag, 'Data inserted succussfully' as Message
						COMMIT TRAN
						
					END
			END TRY
 
			BEGIN CATCH
					BEGIN
						Set @v_flag_bit=0
						Select @v_flag_bit as flag, 'Error while inserting data' as Message,error_message()
						ROLLBACK TRAN
					END
			END CATCH
		END
    ELSE IF @v_actionVc='edit'
		BEGIN
			--BEGIN TRY
			--	BEGIN TRAN
					BEGIN
						--Updating Data in tbl_WorkflowRuleEngine of initiator
						DELETE FROM tbl_WorkflowRuleEngine 
						WHERE  [eventId] = @v_eventidN
						
						INSERT INTO tbl_WorkflowRuleEngine([eventId],[wfRoleId],[isPubDateReq], [procurementRoleId])
						SELECT @v_eventidN,@v_initiatorprocurementroleidN,@v_ispublishdaterequiredVc,items From dbo.f_split(@v_initiatorwfroleidN,',')
						
						--Inserting Data in tbl_WorkFlowRuleEngineHist of initiator
						INSERT INTO tbl_WorkFlowRuleEngineHist([wfRoleId],[eventId],[actionDate],[actionBy],[action],[isPubDateReq],[wfInitRuleId],[procurementRoleId])
						SELECT @v_initiatorprocurementroleidN,@v_eventidN,@v_actiondated,@v_actionbyN,@v_actionVc,@v_ispublishdaterequiredVc,@@IDENTITY,items From dbo.f_split(@v_initiatorwfroleidN,',')
						
						--Inserting Data in tbl_WorkFlowRuleEngine of approver
						INSERT INTO tbl_WorkflowRuleEngine([eventId],[wfRoleId],[isPubDateReq],[procurementRoleId])
						SELECT @v_eventidN,@v_approverprocurementroleidN,@v_ispublishdaterequiredVc,items From dbo.f_split(@v_approverwfroleidN,',')
						
						--Inserting Data in tbl_WorkFlowRuleEngineHist of approver
						INSERT INTO tbl_WorkFlowRuleEngineHist([procurementRoleId],[eventId],[actionDate],[actionBy],[action],[isPubDateReq],[wfInitRuleId],[wfRoleId])
						SELECT @v_approverprocurementroleidN,@v_eventidN,@v_actiondated,@v_actionbyN,@v_actionVc,@v_ispublishdaterequiredVc,@@IDENTITY,items From dbo.f_split(@v_approverwfroleidN,',')
						
						Set @v_flag_bit=1
						Select @v_flag_bit as flag, 'Data updated succussfully' as Message
						--COMMIT TRAN
						
					END
			--END TRY
 
			--BEGIN CATCH
			--		BEGIN
			--			Set @v_flag_bit=0
			--			Select @v_flag_bit as flag, 'Error while updated data' as Message,error_message()
			--			ROLLBACK TRAN
			--		END
			--END CATCH
		END
    ELSE IF @v_actionVc='select'
		BEGIN
			--Inserting Data in tbl_WorkflowRuleEngine of initiator
			INSERT INTO tbl_WorkflowRuleEngine([eventId],[wfRoleId],[procurementRoleId],[isPubDateReq])
			VALUES(@v_eventidN,@v_initiatorprocurementroleidN,@v_initiatorwfroleidN,@v_ispublishdaterequiredVc)
		END
END


GO
