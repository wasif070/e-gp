SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- alter date: <alter Date,,>
-- Description:	<Description,,>
-- =============================================
-- SP Name	   :   p_gen_individualreport
-- Module	   :   Opening/Evaluation (TOC/TEC or POC/PEC User)
-- Description :   After the Tender has opened and all the Price Bid Forms are
--                 Decrypted by Opening/Evaluation Committee CP Individual Report is Generated.
-- Function	   :   N.A.

-- exec [dbo].[p_gen_individualreport]  11675,3031
CREATE PROCEDURE [dbo].[p_gen_individualreport]
	@v_formId_inInt INT =120,
	@v_userId_inInt INT=164
AS
begin

	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	/******* PARAMETERS DECLARATION ***********/
	DECLARE @v_IsPriceBid varchar(10)='',@v_FormHeader varchar(2000)='',@v_FormFooter varchar(2000)='',@v_TableNames_Vc VARCHAR(8000),@v_PrevTableId_Int int=0,  @v_FormName VARCHAR(5000), @v_TableId_Int INT,  @v_ColumnHeader_vc VARCHAR(max),  @v_FormTableHeader VARCHAR(max), @v_ColumnHeaderhtml_vc VARCHAR(max), @v_RowId_Int INT, @v_RowColId_int INT, @v_RowCellValue VARCHAR(max)='', @v_CellHtml_Vc VARCHAR(Max),  @v_RowHtml_Vc VARCHAR(max),  @v_colfill_Int INT, @v_BidTableId_Int INT, @v_FormfinalTableHeader VARCHAR(max),@v_TableNa_Vc varchar(5000), @v_Colspan_Vc varchar(100), @v_MainHeader_Vc varchar(max), @v_CmpName_Vc varchar(500),@v_tendererId varchar(50),@v_companyId varchar(50),@v_jvstatus varchar(5),@name_link varchar(5000),@v_subcontract int,@v_tId varchar(50),@spanMsg varchar(500),@v_PerName_Vc varchar(500), @v_ListBoxName_Vc varchar(100), @v_IsProcurementTypeIct bit = 0, @v_CellId_int int, @v_CellDataType_int int,@v_TotRowId_Int int=0,@v_TotColId_Int int=0,@v_DiscountFormId int=-1

	SET @v_ColumnHeaderhtml_vc = ''
	SET @v_CellHtml_Vc = ''
	SET @v_RowHtml_Vc =''

	/******* SELECT FORMNAME ***********/


	select @v_tId=tenderId from tbl_TenderForms tf,tbl_TenderSection ts,tbl_TenderStd tts
	where tf.tenderSectionId=ts.tenderSectionId and ts.tenderStdId = tts.tenderStdId and tf.tenderFormId=@v_formId_inInt
	--Dohatec: ICT Start
	IF(((SELECT COUNT(tenderId) from tbl_tenderdetails WHERE tenderId = convert(int,@v_tId) and procurementType='ICT') != 0) or ((select foreignCurrency from tbl_TemplateMaster where templateId = (select templateId from tbl_TenderStd where tenderId = convert(int,@v_tId))) = 'Yes'))
	--IF(((SELECT COUNT(tenderId) from tbl_tenderdetails WHERE tenderId = convert(int,@v_tId) and procurementType='ICT') != 0))
		Begin
			set @v_IsProcurementTypeIct = 1 --true
		End
	--Dohatec: ICT End
	select @v_subcontract=COUNT(*) from tbl_SubContracting tsc where tsc.invFromUserId=@v_userId_inInt and tsc.tenderId in
	(@v_tId)
	and tsc.invAcceptStatus='Approved'

	SELECT @v_FormName = formName,@v_FormHeader=formHeader,@v_FormFooter=formFooter,@v_IsPriceBid=isPriceBid FROM [tbl_TenderForms] WHERE tenderFormId = @v_formId_inInt
	--This Form << Form Name>> is Electronically Signed by <<Person Name>> on behalf of <<Company Name>>


	 If (select noOfEnvelops
		from tbl_ConfigEvalMethod cm ,tbl_TenderDetails td, tbl_TenderTypes tt
		where cm.procurementMethodId=td.procurementMethodId and
		cm.tenderTypeId=tt.tenderTypeId and tt.tenderType=td.eventType and cm.procurementNatureId=td.procurementNatureId
		and tenderId=@v_tId)=2 and @v_IsPriceBid='yes'
	Begin
			SELECT @v_CmpName_Vc = case tc.CompanyName when '-' then tm.firstName+' '+tm.lastName else tc.companyName end,
			@v_tendererId=convert(varchar(50),tm.tendererId),@v_companyId=convert(varchar(50),tm.companyId),
			@v_jvstatus=lm.isJvca,@v_PerName_Vc = tm.title+' '+tm.firstName+' '+tm.lastName
			FROM tbl_LoginMaster lm, tbl_TendererMaster tm , tbl_CompanyMaster tc
			WHERE lm.userId = tm.userId and tc.companyid = tm.companyId and lm.userId = @v_userId_inInt
			and lm.userId not in(select userId from tbl_EvalBidderStatus where tenderId=@v_tId and (bidderStatus='Technically Unresponsive' or ISNULL(result,'Pass')='Fail'))
	End
	Else
	Begin
			SELECT @v_CmpName_Vc = case tc.CompanyName when '-' then tm.firstName+' '+tm.lastName else tc.companyName end,
			@v_tendererId=convert(varchar(50),tm.tendererId),@v_companyId=convert(varchar(50),tm.companyId),
			@v_jvstatus=lm.isJvca,@v_PerName_Vc = tm.title+' '+tm.firstName+' '+tm.lastName
			FROM tbl_LoginMaster lm, tbl_TendererMaster tm , tbl_CompanyMaster tc
			WHERE lm.userId = tm.userId and tc.companyid = tm.companyId and lm.userId = @v_userId_inInt
	End

	select @name_link = '<a view="link" href="../tenderer/ViewRegistrationDetail.jsp?uId='+convert(varchar(50),@v_userId_inInt)+'&tId='+@v_tendererId+'&cId='+@v_companyId+'&top=no" target="_blank">'+@v_CmpName_Vc+'</a>'

	if(@v_jvstatus='yes')
	begin
		select @name_link=''
		select @name_link = '<a view="link" href="../tenderer/ViewJVCADetails.jsp?uId='+convert(varchar(50),@v_userId_inInt)+'&top=no" target="_blank">'+@v_CmpName_Vc+'</a>'
		select @name_link = @name_link + '<br/><a view="link" href="../tenderer/ViewJVCADetails.jsp?uId='+convert(varchar(50),@v_userId_inInt)+'&top=no" target="_blank" style="color:red">(JVCA - View Details)</a>'
	end

	if(@v_subcontract!=0)
	begin
		select @name_link =@name_link+'<br/><a view="link" href="../tenderer/ViewTenderSubContractor.jsp?uId='+convert(varchar(50),@v_userId_inInt)+'&tenderId='+@v_tId+'" target="_blank" style="color:red">(Sub Contractor/Consultant - View Details)</a>'
	end

	Set @v_MainHeader_Vc = '<table border="0" cellpadding="10" cellspacing="0" width="100%" id="mtable1" class="tableList_3 t_space"><tr><td class="bgColor-Green"><b><center>'+@name_link+'</center></b></td></tr><tr><th>  '+@v_FormName+'</th></tr><tr><th>  '+@v_FormHeader+'</th></tr><tr><td>'

	PRINT 'MHERAS : '+ @v_MainHeader_Vc

	SELECT @v_FormTableHeader=''

	/******* SELECT TABLES IN FORMS ***********/


	SELECT @v_TableNames_Vc=  COALESCE(@v_TableNames_Vc+', ', ' ') + convert(VARCHAR(20),tenderTableId) FROM [tbl_TenderTables] WHERE tenderFormId = @v_formId_inInt
	
	-- changed for Discount Form on 02/January/2017
	select @v_DiscountFormId = tenderFormId from tbl_TenderForms where formName in ('Discount Form','Discount Amount') and tenderFormId = @v_formId_inInt
	--  Discount Form END

DECLARE cur_tblNames CURSOR FAST_FORWARD FOR SELECT tenderTableId, bidTableId FROM
tbl_TenderBidForm tf,tbl_TenderBidTable tb WHERE tf.bidId = tb.bidid
and tf.userId = @v_userId_inInt and tf.tenderFormId = @v_formId_inInt

			OPEN cur_tblNames

			FETCH NEXT FROM cur_tblNames INTO @v_TableId_Int, @v_BidTableId_Int

			print 'STEP 1 : First table Cursor'
			WHILE @@Fetch_status = 0
			BEGIN
			
			SELECT @v_Colspan_vc = CONVERT(VARCHAR(15), noOfCols), @v_TableNa_vc = tablename FROM	tbl_TenderTables WHERE tenderFormId = @v_formId_inInt and tendertableId = @v_TableId_Int

			-- changed for Discount Form on 02/January/2017
			if(@v_DiscountFormId = @v_formId_inInt)
			Begin
				Set @v_Colspan_vc = 3
				SET @v_CellHtml_Vc = '<td></td>'
			End
			--  Discount Form END

			IF @v_FormfinalTableHeader <> ''
			BEGIN

			print 'in <>'
					SET @v_FormTableHeader= @v_FormfinalTableHeader + '</table><table border="0" cellpadding="10" cellspacing="0" width="100%"  class="tableList_3"><tr><td colspan="'+ @v_Colspan_vc+'">'+@v_TableNa_vc+'</td></tr><tr>'

			END

			ELSE
			BEGIN
				print 'in else <>'
			SET @v_FormTableHeader= @v_FormTableHeader + '<table border="0" cellpadding="10" cellspacing="0" width="100%"  class="tableList_3"><tr><td colspan="'+ @v_Colspan_vc+'">'+@v_TableNa_vc+'</td></tr><tr>'
			END
					print 'TableID: ' + convert(VARCHAR(200),@v_TableId_Int)
					print 'BidID: ' + convert(VARCHAR(200),@v_BidTableId_Int)

			-- changed for Discount Form on 02/January/2017
			if(@v_DiscountFormId = @v_formId_inInt)
			Begin
				SELECT @v_ColumnHeader_vc=  COALESCE(@v_ColumnHeader_vc+'</b></th><th><b>', ' ') + CONVERT(VARCHAR(5000),Result.columnHeader)
				From 
				(
					Select top 2 *
					FROM tbl_TenderColumns 
					WHERE [tenderTableId] = @v_TableId_Int order by columnId Desc
				) as Result
				Order by Result.columnId
			end
			Else
			Begin
				SELECT @v_ColumnHeader_vc=  COALESCE(@v_ColumnHeader_vc+'</b></th><th><b>', ' ') + CONVERT(VARCHAR(5000),columnHeader) FROM tbl_TenderColumns WHERE [tenderTableId] = @v_TableId_Int order by columnId -- changed by dtec 26/102016
			End
			--  Discount Form END


			SET @v_ColumnHeaderhtml_vc =''

			-- changed for Discount Form on 02/January/2017
			if(@v_DiscountFormId = @v_formId_inInt)
			Begin
				SET @v_ColumnHeaderhtml_vc ='<th></th>'
			End
			--  Discount Form END


			--print @v_ColumnHeader_vc
			if @v_TableId_Int!=@v_PrevTableId_Int
			begin
			SET @v_ColumnHeaderhtml_vc = @v_ColumnHeaderhtml_vc + '<th><b>'+@v_ColumnHeader_vc+'</b></th>'
			print 'header td:'+ @v_ColumnHeaderhtml_vc
			SET @v_FormfinalTableHeader= @v_FormTableHeader +@v_ColumnHeaderhtml_vc +'</tr>'

			set @v_PrevTableId_Int=@v_TableId_Int
			end
			SET @v_ColumnHeader_vc=null
			SET @v_FormTableHeader = ''
			print 'FormfinalTableHeader: ' + @v_FormfinalTableHeader

					/* FETCH DATA ROW WISE */

	DECLARE cur_rowValues CURSOR FAST_FORWARD FOR
	SELECT DISTINCT rowid FROM vw_get_tenderbiddata WHERE tenderTableId = @v_TableId_Int and bidTableId = @v_BidTableId_Int
			OPEN cur_rowValues
			FETCH NEXT FROM cur_rowValues INTO @v_rowId_Int
			print 'STEP 2 : 2nd Row Cursor'
			print 'in rowid: '+ convert(VARCHAR(20),@v_rowId_Int)

			WHILE @@FETCH_STATUS = 0
			BEGIN
				print 'Row id: ' +CONVERT (VARCHAR(50),@v_rowId_Int)
				SET @v_RowHtml_Vc = @v_RowHtml_Vc + '<tr>'

 				print 'taking COLUMN ID from [tbl_Tender_Columns]'

 					/* FETCH DATA ROW-COLUMN WISE */

 				DECLARE cur_columnId CURSOR FAST_FORWARD FOR
				SELECT DISTINCT columnid, filledby FROM tbl_TenderColumns
				WHERE [tenderTableId] =@v_TableId_Int order by columnid

				OPEN cur_columnId
				FETCH NEXT FROM cur_columnId into @v_RowColId_int, @v_colfill_Int

					print 'STEP 3 : 3nd cell Cursor'

					WHILE @@Fetch_status = 0
					BEGIN

						print 'Rowid: ' + CONVERT(VARCHAR(50),@v_rowId_Int)
						print 'Colid: ' + CONVERT(VARCHAR(50),@v_RowColId_int)

					IF @v_colfill_Int = 1
					BEGIN
						print 'taking cellrowValue val from [tbl_TenderCells]'
 						select @v_CellDataType_int=  max(celldatatype) from tbl_TenderCells where
						tenderTableId=@v_TableId_Int and columnId=@v_RowColId_int and rowId=@v_rowId_Int
						--SELECT   @v_RowCellValue = case when cellvalue='' then 'k' else 'j' end
						SELECT   @v_RowCellValue = case when cellvalue = 'null' then '' else isnull(cellvalue,'') end
						FROM vw_get_tendercolumndata WHERE tenderTableId =@v_TableId_Int
						and rowid = @v_rowId_Int and columnid = @v_RowColId_int
						ORDER BY rowid, columnid

 					END
 					ELSE
 					BEGIN

						SELECT  @v_TotRowId_Int=MAX(rowId)  ,
								@v_TotColId_Int=min(t.columnId-1)  
						FROM	tbl_TenderFormula t,tbl_TenderCells c 
						WHERE	formula like '%TOTAL%' 
								and t.tenderTableId=@v_TableId_Int
								and t.tenderTableId=c.tenderTableId
								and t.columnId-1=c.columnId 
						GROUP BY t.tenderTableId
 						
 						PRINT 'taking cellrowValue val from [tbl_TenderBidDetail]' +convert(varchar(20),@v_TotRowId_Int)
				
						select	@v_CellDataType_int=  max(celldatatype) 
						from	tbl_TenderCells 
						where	tenderTableId=@v_TableId_Int and columnId=@v_RowColId_int and rowId=@v_rowId_Int

						--Dohatec: ICT Start
						select	top 1 @v_CellId_int = cellId 
						from	tbl_TenderCells
						where	tenderTableId=@v_TableId_Int 
								and columnId=@v_RowColId_int 
								and rowId=@v_rowId_Int
					
						print 'CellId: ' + CONVERT(VARCHAR(50),@v_CellId_int)

						--get list box (combo box) name (e.g. Currency)
						set @v_ListBoxName_Vc = ''
						If(@v_CellDataType_int =9 OR @v_CellDataType_int =10)
						Begin
							select top 1 @v_ListBoxName_Vc = tlb.listBoxName from tbl_TenderListCellDetail tlcd, tbl_TenderListBox tlb
							where tlcd.tenderTableId=@v_TableId_Int and tlcd.columnId = @v_RowColId_int and tlcd.cellId = @v_CellId_int and tlcd.tenderListId = tlb.tenderListId
						End
						--Dohatec: ICT End

						SELECT	@v_RowCellValue =
								CASE	
								--Dohatec: ICT Start
								when (@v_CellDataType_int =9 OR @v_CellDataType_int =10) and @v_IsProcurementTypeIct = 1 and @v_ListBoxName_Vc = 'Currency' --Procurement Type: ICT
								then (	select	top 1 currencyShortName from tbl_CurrencyMaster cm,tbl_TenderBidPlainData tpd
										where	tpd.bidtableid = @v_BidTableId_Int and tpd.tenderTableId = @v_TableId_Int
												and tpd.rowid = @v_rowId_Int and tpd.tenderColId = @v_RowColId_int and tpd.cellValue != 'null' and tpd.cellValue = cm.currencyId
									)
								--Dohatec: ICT End
								when @v_CellDataType_int =9 OR @v_CellDataType_int =10
								then (	select	top 1 itemText
										from	tbl_TenderListCellDetail l,
												tbl_TenderListDetail ld,
												tbl_TenderBidPlainData p
										where	l.tenderTableId=p.tenderTableId 
												and l.columnId=p.tenderColId 
												and l.cellId=p.tenderCelId 
												and l.location=p.rowId 
												and l.tenderListId=ld.tenderListId 
												and l.tenderTableId=@v_TableId_Int
												and ld.itemValue=p.cellValue 
												and p.tenderTableId =@v_TableId_Int
												and rowid = @v_rowId_Int
												and columnid = @v_RowColId_int 
												and bidtableid = @v_BidTableId_Int
										)
								when cellvalue = 'null' 
								then '' 
								else isnull(cellvalue,'') 
								end  FROM vw_get_tenderbiddata
						WHERE tenderTableId =@v_TableId_Int 
								and rowid = @v_rowId_Int
								and columnid = @v_RowColId_int 
								and bidtableid = @v_BidTableId_Int
						ORDER BY rowid, columnid	
 					END
 					set @v_RowCellValue=ISNULL(@v_RowCellValue,'') 
 				       select @v_RowCellValue=case when @v_TotRowId_Int=@v_rowId_Int and @v_TotColId_Int=@v_RowColId_int then '<b>Grand Total:</b>' else @v_RowCellValue end
 						print 'Cell Value: '+@v_RowCellValue 
			 			if @v_CellDataType_int =3 or  @v_CellDataType_int =8 or @v_CellDataType_int =11 or @v_CellDataType_int =4 or @v_CellDataType_int =13
			 			begin
			 				if @v_RowCellValue !='' and @v_RowCellValue not like '%<b>Grand Total:</b>%'
			 				begin
			 					print convert(varchar(200),@v_CellDataType_int)+'@v_RowCellValue'+@v_RowCellValue 
			 					if @v_CellDataType_int!=4 and @v_RowCellValue not like '%<b>Grand Total:</b>%'
			 					begin
			 						set @v_RowCellValue=convert(varchar(8000),cast(@v_RowCellValue as decimal(18,3))) 
			 					end
			 				end
			 				else
			 				begin
			 					print '@v_RowCellValuenotnull'+@v_RowCellValue
			 				end

							-- changed for Discount Form on 02/January/2017
							if(@v_DiscountFormId != @v_formId_inInt or @v_RowCellValue != '')
							Begin
								SET @v_CellHtml_vc = @v_CellHtml_vc + '<td style=''text-align:right;''>'+@v_RowCellValue+'</td>'
							End
							--  Discount Form END


 						end
 						else
 						begin
 							SET @v_CellHtml_vc = @v_CellHtml_vc + '<td>'+@v_RowCellValue+'</td>'
 						end
 						set @v_RowCellValue = ''
 						print @v_CellHtml_vc


 				FETCH NEXT FROM cur_columnId INTO @v_RowColId_int, @v_colfill_Int
				END
					print 'cell fetch'

						SET @v_RowHtml_Vc = @v_RowHtml_Vc + @v_CellHtml_vc + '</tr>'
						PRINT 'row HTML' + @v_RowHtml_Vc
				CLOSE cur_columnId
				DEALLOCATE cur_columnId

 			print 'row HTML' + @v_RowHtml_Vc
 			print 'row fetch'

 			SET @v_CellHtml_vc = ''


 			FETCH NEXT FROM cur_rowValues into @v_rowId_Int
 			     SET @v_FormfinalTableHeader = @v_FormfinalTableHeader + @v_RowHtml_Vc
				 SET @v_RowHtml_Vc = ''
			END

			CLOSE cur_rowValues
			DEALLOCATE cur_rowValues



		  print 'final html'+ @v_FormfinalTableHeader



			FETCH NEXT FROM cur_tblNames INTO @v_TableId_Int, @v_BidTableId_Int

			END

			CLOSE cur_tblNames
			DEALLOCATE cur_tblNames

	print 'Mainheradder: '  + @v_MainHeader_Vc
	print 'dffffffffffffff'
	print 'FormfinalTableHeader1111111111111111 : ' + @v_FormfinalTableHeader

	print 'a'+@v_PerName_Vc
	print 'b'+@v_CmpName_Vc
	print 'c'+@v_FormName
	print 'd'+@v_FormFooter
	select @spanMsg ='<div style="color: red;" id="spanMsg" class="t_space">This '+@v_FormName+' is Electronically Signed by '+@v_PerName_Vc
	if @v_companyId = '1'
	begin
		select @spanMsg = @spanMsg +'</div>'
	end
	else
	begin
		select @spanMsg = @spanMsg + ' on behalf of '+@v_CmpName_Vc+'</div>'
	end

	SELECT isnull(@v_MainHeader_Vc + @v_FormfinalTableHeader + '</table></td></tr><tr><td style="border:1px solid #ddd; border-bottom:2px solid #ddd; text-align:left; padding:5px; vertical-align: middle;  font-weight:bold; background:#E4FAD0;" >  '+@v_FormFooter+'</td></tr></table>'+@spanMsg,'' )as reportHtml
	SET NOCOUNT OFF
end


GO
