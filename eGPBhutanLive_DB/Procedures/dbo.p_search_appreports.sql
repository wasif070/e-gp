SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--
-- Purpose: Search In APP Reports
--
--
-- Author: Karan Kuniyal
-- Date: 16-02-2011

-- SP Name: [p_search_appreports]
-- Module: APP
-- Function: Procedure use for search APP details for APP Reports.
--------------------------------------------------------------------------------
-- getLotInfo: Use for get Package Lot details on the basis of package id.
-- searchData: Use for get APP Package details base on given search criteria.
-- getTopInfo: Use for get APP Report Top details like Ministry, Division and Agency,etc.
--------------------------------------------------------------------------------
/*

EXEC [dbo].[p_search_appreports]
@v_Action_inVc ='searchData',
@v_DepartmentId_insInt = 14,
@v_OfficeId_inInt =7,
@v_FinancialYear_inVc=null,
@v_BudgetType_intInt=null,
@v_ProjectId_inInt =57,
@v_ProcurementNature_inVc=null,
@v_PackageId_inInt=null,
@v_AppId_inInt =0

*/

--ALTER PROCEDURE [dbo].[p_search_appreports]
--@v_Action_inVc varchar(50),
--@v_DepartmentId_insInt smallint,
--@v_OfficeId_inInt int,
--@v_FinancialYear_inVc varchar(50)=null,
--@v_BudgetType_intInt tinyint=null,
--@v_ProjectName_inVc varchar(150)=null,
--@v_ProcurementNature_inVc varchar(10)=null,
--@v_PackageId_inInt int=null
CREATE PROCEDURE [dbo].[p_search_appreports]
@v_Action_inVc varchar(50),
@v_DepartmentId_insInt smallint,
@v_OfficeId_inInt int,
@v_FinancialYear_inVc varchar(50)='',
@v_BudgetType_intInt tinyint=0,
@v_ProjectId_inInt int=0,
@v_ProcurementNature_inVc varchar(10)='',
@v_PackageId_inInt int=0,
@v_AppId_inInt int=0
AS

IF @v_Action_inVc='getLotInfo'
BEGIN
	Select lotNo, lotDesc, quantity, unit From tbl_AppPkgLots Where packageId=@v_PackageId_inInt
END
ELSE IF @v_Action_inVc='searchData'
BEGIN
	DECLARE @v_FinalQueryVc varchar(Max)=null, @v_QueryVc varchar(max)=null,
	@v_ConditionString_Vc varchar(4000),
	@v_CntQry_Vc varchar(max)
	
	SET @v_ConditionString_Vc=''
	
	--Select P.packageId, P.packageNo, PM.procurementMethod, P.procurementType,PR.procurementRole,
	--replace(IsNull(REPLACE(CONVERT(VARCHAR(11),advtDt, 106), ' ', '-'),''),'01-Jan-1900','-') as advtDt,
	--	replace(IsNull(REPLACE(CONVERT(VARCHAR(11),subDt, 106), ' ', '-'),''),'01-Jan-1900','-') as subDt,
	--	replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderAdvertDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderAdvertDt,
	--	replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderSubDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderSubDt,
	--	replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderOpenDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderOpenDt,
	--	replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderNoaIssueDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderNoaIssueDt,
	--	replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderContractSignDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderContractSignDt,
	--	replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderContractCompDt, 106), ' ', '-'),''),'01-Jan-1900','-')  as tenderContractCompDt,		
	--	replace(IsNull(REPLACE(CONVERT(VARCHAR(11),techSubCmtRptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as  techSubCmtRptDt,
	--	replace( IsNull(REPLACE(CONVERT(VARCHAR(11),tenderEvalRptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderEvalRptDt,
	--	replace( IsNull(REPLACE(CONVERT(VARCHAR(11),tenderEvalRptAppDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderEvalRptAppDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderContractAppDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderContractAppDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),reoiReceiptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as reoiReceiptDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfpTechEvalDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfpTechEvalDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfpFinancialOpenDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfpFinancialOpenDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfpNegCompDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfpNegCompDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfpContractAppDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfpContractAppDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaAdvertDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaAdvertDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaReceiptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaReceiptDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaEvalDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaEvalDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaInterviewDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaInterviewDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaFinalSelDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaFinalSelDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaEvalRptSubDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaEvalRptSubDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaAppConsultantDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaAppConsultantDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actSubDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actSubDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actAdvtDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actAdvtDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actEvalRptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actEvalRptDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actAppLstDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actAppLstDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderAdvertDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderAdvertDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderSubDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderSubDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderOpenDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderOpenDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTechSubCmtRptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTechSubCmtRptDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderEvalRptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderEvalRptDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),acttenderEvalRptAppDt, 106), ' ', '-'),''),'01-Jan-1900','-') as acttenderEvalRptAppDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderContractAppDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderContractAppDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderNoaIssueDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderNoaIssueDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderContractSignDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderContractSignDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderContractCompDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderContractCompDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actReoiReceiptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actReoiReceiptDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfpTechEvalDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfpTechEvalDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfpFinancialOpenDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfpFinancialOpenDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfpNegComDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfpNegComDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfpContractAppDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfpContractAppDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaAdvertDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaAdvertDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaReceiptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaReceiptDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaEvalDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaEvalDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaInterviewDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaInterviewDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaFinalSelDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaFinalSelDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaEvalRptSubDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaEvalRptSubDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaAppConsultantDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaAppConsultantDt,
	--	 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),evalRptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as evalRptDt,		 
	--	 IsNull(REPLACE(CONVERT(VARCHAR(11),appLstDt, 106), ' ', '-'),'') as appLstDt,
	--	 subDays, advtDays,openDays,
	--	 IsNull(REPLACE(CONVERT(VARCHAR(11),openDt, 106), ' ', '-'),'') as openDt, 
 --        evalRptDays,tenderAdvertDays,tenderSubDays, 
 --        tenderOpenDays,techSubCmtRptDays,tenderEvalRptdays, 
 --        tenderEvalRptAppDays,tenderContractAppDays, 
 --        tenderNoaIssueDays,tenderContractSignDays,rfaAdvertDays, 
 --        rfaReceiptDays,rfaEvalDays,rfaInterviewDays, 
 --        rfaFinalSelDays,rfaEvalRptSubDays,rfpContractAppDays, 
 --        rfpNegCompDays,rfpFinancialOpenDays,rfpTechEvalDays, 
 --        reoiReceiptDays
                  	  
	--from tbl_AppPackages P 
	--Inner Join tbl_AppMaster AM ON P.appId=AM.appId
	--Inner join tbl_ProcurementMethod PM On P.procurementMethodId=PM.procurementMethodId
	--Inner Join tbl_ProcurementRole PR On P.approvingAuthEmpId=PR.procurementRoleId
	--Inner Join tbl_AppPqTenderDates TD On TD.packageId=P.packageId
	--Where P.packageId is not null
	--here add tenderLetterIntentDt and tnderLetterIntentDays by Proshanto Kumar Saha
	SET @v_QueryVc='Select  (select 

isnull((select progName from tbl_ProgrammeMaster pm where p.progId=pm.progId ),''Not Available'')+
''/''+ projectName +'' and ''+ projectCode from tbl_ProjectMaster p where am.projectid=p.projectid ) as projectnamecode,dbo.f_get_Ministry_new(AM.appId) as MinistryDetails, AM.appId,P.packageId, P.packageNo, P.packageDesc, P.procurementnature, PM.procurementMethod, 
	P.procurementType,PR.procurementRole,P.sourceOfFund, 
	Case budgetType  
              When 1 Then ''Development''
              When 2 Then ''Revenue''
              When 3 Then ''Own Fund''
              End as BudgetType ,
	P.estimatedCost,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),advtDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as advtDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),subDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as subDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderAdvertDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as tenderAdvertDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderSubDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as tenderSubDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderOpenDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as tenderOpenDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderLetterIntentDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as tenderLetterIntentDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderNoaIssueDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as tenderNoaIssueDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderContractSignDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as tenderContractSignDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderContractCompDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'')  as tenderContractCompDt,		
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),techSubCmtRptDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as  techSubCmtRptDt,
replace( IsNull(REPLACE(CONVERT(VARCHAR(11),tenderEvalRptDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as tenderEvalRptDt,
replace( IsNull(REPLACE(CONVERT(VARCHAR(11),tenderEvalRptAppDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as tenderEvalRptAppDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderContractAppDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as tenderContractAppDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),reoiReceiptDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as reoiReceiptDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfpTechEvalDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as rfpTechEvalDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfpFinancialOpenDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as rfpFinancialOpenDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfpNegCompDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as rfpNegCompDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfpContractAppDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as rfpContractAppDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaAdvertDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as rfaAdvertDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaReceiptDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as rfaReceiptDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaEvalDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as rfaEvalDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaInterviewDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as rfaInterviewDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaFinalSelDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as rfaFinalSelDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaEvalRptSubDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as rfaEvalRptSubDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaAppConsultantDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as rfaAppConsultantDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actSubDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actSubDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actAdvtDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actAdvtDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actEvalRptDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actEvalRptDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actAppLstDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actAppLstDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderAdvertDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actTenderAdvertDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderSubDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actTenderSubDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderOpenDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actTenderOpenDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTechSubCmtRptDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actTechSubCmtRptDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderEvalRptDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actTenderEvalRptDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),acttenderEvalRptAppDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as acttenderEvalRptAppDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderContractAppDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actTenderContractAppDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderNoaIssueDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actTenderNoaIssueDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderContractSignDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actTenderContractSignDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderContractCompDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actTenderContractCompDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actReoiReceiptDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actReoiReceiptDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfpTechEvalDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actRfpTechEvalDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfpFinancialOpenDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actRfpFinancialOpenDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfpNegComDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actRfpNegComDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfpContractAppDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actRfpContractAppDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaAdvertDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actRfaAdvertDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaReceiptDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actRfaReceiptDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaEvalDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actRfaEvalDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaInterviewDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actRfaInterviewDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaFinalSelDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actRfaFinalSelDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaEvalRptSubDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actRfaEvalRptSubDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaAppConsultantDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as actRfaAppConsultantDt,
replace(IsNull(REPLACE(CONVERT(VARCHAR(11),evalRptDt, 106), '' '', ''-''),''''),''01-Jan-1900'',''-'') as evalRptDt,		 
IsNull(REPLACE(CONVERT(VARCHAR(11),appLstDt, 106), '' '', ''-''),'''') as appLstDt,
subDays, advtDays,openDays,
IsNull(REPLACE(CONVERT(VARCHAR(11),openDt, 106), '' '', ''-''),'''') as openDt, 
evalRptDays,tenderAdvertDays,tenderSubDays, 
tenderOpenDays,techSubCmtRptDays,tenderEvalRptdays, 
tenderEvalRptAppDays,tenderContractAppDays,tnderLetterIntentDays, 
tenderNoaIssueDays,tenderContractSignDays,rfaAdvertDays, 
rfaReceiptDays,rfaEvalDays,rfaInterviewDays, 
rfaFinalSelDays,rfaEvalRptSubDays,rfpContractAppDays, 
rfpNegCompDays,rfpFinancialOpenDays,rfpTechEvalDays, 
reoiReceiptDays,isPQRequired
from tbl_AppPackages P 
Inner Join tbl_AppMaster AM ON P.appId=AM.appId
Inner join tbl_ProcurementMethod PM On P.procurementMethodId=PM.procurementMethodId
Inner Join tbl_ProcurementRole PR On P.approvingAuthEmpId=PR.procurementRoleId
Inner Join tbl_AppPqTenderDates TD On TD.packageId=P.packageId
--Inner Join 
--(Select Id, Items from dbo.f_splitX(''Goods,Works,Services'','','')) as GWS
--On P.procurementnature=GWS.Items
--Inner Join 
--(Select Id, Items from dbo.f_splitX(''Aid Grant / Credit,Government,Own Fund'','','')) as AGO
--On P.sourceOfFund=AGO.Items
Where P.packageId is not null'
	
/* Start : Building Condition String */	
	If @v_AppId_inInt is not null And @v_AppId_inInt<>0
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And AM.appId=' + Convert(varchar(50), @v_AppId_inInt)
	End

	If @v_DepartmentId_insInt is not null And @v_DepartmentId_insInt<>'' And @v_DepartmentId_insInt<>0
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And AM.DepartmentId=' + Convert(varchar(50), @v_DepartmentId_insInt)
	End
	
	If @v_OfficeId_inInt is not null And @v_OfficeId_inInt<>'' And @v_OfficeId_inInt<>0
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And AM.OfficeId=' + Convert(varchar(50), @v_OfficeId_inInt) 
	End
	
	If @v_FinancialYear_inVc is not null And @v_FinancialYear_inVc<>''
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And AM.FinancialYear=''' + @v_FinancialYear_inVc  + ''''
	End
	
	If @v_BudgetType_intInt is not null And @v_BudgetType_intInt<>'' And @v_BudgetType_intInt<>0
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And AM.BudgetType=' + Convert(varchar(50), @v_BudgetType_intInt) 
	End
	
	If @v_ProjectId_inInt is not null And @v_ProjectId_inInt<>'' And @v_ProjectId_inInt<>0
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And AM.projectId=' ++ Convert(varchar(50), @v_ProjectId_inInt)   
	End
	
	If @v_ProcurementNature_inVc is not null And @v_ProcurementNature_inVc<>''
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And P.ProcurementNature=''' + @v_ProcurementNature_inVc  + ''''
	End
/* End : Building Condition String */	

Print @v_QueryVc
Print @v_ConditionString_Vc
	select @v_FinalQueryVc = @v_QueryVc
	IF @v_ConditionString_Vc is not null And @v_ConditionString_Vc<>''
	Begin
		select @v_FinalQueryVc = @v_FinalQueryVc + @v_ConditionString_Vc
	End

	Select @v_FinalQueryVc=@v_FinalQueryVc + ' --Order by GWS.Id, AGO.Id'

--	PRINT(@v_FinalQueryVc)
	Exec (@v_FinalQueryVc)
	
	
END


IF @v_Action_inVc='getTopInfo'
BEGIN

DECLARE 
@v_CurrDepartmentId_Int int,
@v_CurrDepartmentType_Vc varchar(15),
@v_CurrDepartmentName_Vc varchar(150),
@v_Ministry_Vc varchar(150),
@v_Division_Vc varchar(150),
@v_Agency_Vc varchar(150),
@v_PeCOde_Vc varchar(50), 
@v_OfficeName_Vc varchar(150),
@v_ProjectName_Vc varchar(150), 
@v_ProjectCode_Vc varchar(50)

		/* START CODE: TO SET VALUES OF Ministry, Division and Agency */
				Select @v_CurrDepartmentId_Int=departmentId,@v_CurrDepartmentType_Vc=departmentType, @v_CurrDepartmentName_Vc=departmentName 
					from tbl_DepartmentMaster where departmentId=@v_DepartmentId_insInt
				
				If @v_CurrDepartmentType_Vc='Ministry'
				Begin
					Select @v_Ministry_Vc=@v_CurrDepartmentName_Vc, @v_Division_Vc='',@v_Agency_Vc=''
				End
				Else If @v_CurrDepartmentType_Vc='Division'
				Begin
					Select @v_Ministry_Vc=(select departmentName from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_CurrDepartmentId_Int)), 
					@v_Division_Vc=@v_CurrDepartmentName_Vc,					
					@v_Agency_Vc=''
				End
				Else -- Agency/Organisation Case
				Begin
					If (select departmentType from tbl_DepartmentMaster where departmentId=(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_CurrDepartmentId_Int))='Ministry'
					Begin
						Select @v_Ministry_Vc=(select departmentName from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_CurrDepartmentId_Int)),					
						@v_Division_Vc= '',
						@v_Agency_Vc=@v_CurrDepartmentName_Vc
					End
					Else
					Begin
						Select @v_Ministry_Vc=(select departmentName from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_CurrDepartmentId_Int))), 
						@v_Division_Vc= (select departmentName from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_CurrDepartmentId_Int)),					
						@v_Agency_Vc=@v_CurrDepartmentName_Vc
					End				
				End
				/* END CODE: TO SET VALUES OF Ministry, Division and Agency */				
				
				
				Select @v_PeCOde_Vc=PeCOde, @v_OfficeName_Vc=OfficeName 
				from tbl_OfficeMaster Where officeId=@v_OfficeId_inInt								
				
				
				If @v_ProjectId_inInt is Not Null And @v_ProjectId_inInt<>0
				Begin
					Select @v_ProjectName_Vc=projectName, @v_ProjectCode_Vc=projectCode
					From tbl_ProjectMaster
					where projectId=@v_ProjectId_inInt
					
					Select @v_Ministry_Vc as Ministry, @v_Division_Vc as Division, @v_Agency_Vc as Agency,
					@v_PeCOde_Vc as PECode, @v_OfficeName_Vc as OfficeName,
					@v_ProjectName_Vc as ProjectName, @v_ProjectCode_Vc as ProjectCode
				End
				Else
				Begin
					Select @v_Ministry_Vc as Ministry, @v_Division_Vc as Division, @v_Agency_Vc as Agency,
					@v_PeCOde_Vc as PECode, @v_OfficeName_Vc as OfficeName,
					'' as ProjectName, '' as ProjectCode
				End
			
END


GO
