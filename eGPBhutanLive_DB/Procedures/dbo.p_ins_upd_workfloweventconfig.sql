SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Insert/Update/Delete table tbl_WorkFlowEventConfig
--
--
-- Author: Kinjal
-- Date: 30-10-2010
--
-- Last Modified:
-- Modified By: 
-- Date 
-- Modification: 
--------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ins_upd_workfloweventconfig]
	
@v_noOfReviewer_inInt smallint,
@v_noOfDays_inInt smallInt,
@v_isDonorConReq_inVc varchar(3),
@v_eventid_inInt smallInt,
@v_objectId_inInt int,
@v_WfRoleId_inInt tinyint,
@v_Action_inVc varchar(50)=null,
@v_wfEvtConfId_inInt int=null,
@v_actiondate_indt datetime,
@v_actionby_inInt int

	
AS

BEGIN

declare @v_wfEvtCnfigID_Int int, @v_flag_bit bit	


	IF @v_Action_inVc='Create'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
				/* START CODE : TO INSERT INTO TABLE - tbl_WorkFloweventConfig */	
				INSERT INTO [dbo].[tbl_WorkFlowEventConfig]
			   ([noOfReviewer]
			   ,[noOfDays]
			   ,[isDonorConReq]
			   ,[eventId]
			   ,[objectId])
				VALUES
					(@v_noOfReviewer_inInt,
					@v_noOfDays_inInt,
					@v_isDonorConReq_inVc,
					@v_eventid_inInt,
					@v_objectId_inInt
					)
				/* END CODE : TO INSERT INTO TABLE - tbl_WorkFlowEventConfig */	
					
					--Set @v_wfEvtCnfigID_Int = Ident_Current('dbo.[tbl_WorkFlowEventConfig]') 
					
					
					/* START CODE : TO INSERT INTO TABLE - [tbl_WorkFlowEventConfigHist] */
					INSERT INTO [dbo].[tbl_WorkFlowEventConfigHist]
					   (
					   [noOfReviewer]
					   ,[noOfDays]
					   ,[isDonorConReq]
					   ,[eventId]
					   ,[objectId]
					   ,[actionDate]
					   ,[actionBy]
					   ,[action])
     
					VALUES
					(
					@v_noOfReviewer_inInt,
					@v_noOfDays_inInt,
					@v_isDonorConReq_inVc,
					@v_eventid_inInt,
					@v_objectId_inInt,
					@v_actiondate_indt,
					@v_actionby_inInt,
					@v_action_invc
					)
										
				/* END CODE : TO INSERT INTO TABLE - [tbl_WorkFlowEventConfigHist] */			
				Set @v_flag_bit=1			
				Select @v_flag_bit as flag,'Success' as Message
			COMMIT TRAN	
		END TRY 
		BEGIN CATCH
				BEGIN
					Set @v_flag_bit=0			
				Select @v_flag_bit as flag, ERROR_MESSAGE() as Message
					ROLLBACK TRAN
				END
		END CATCH	 
	END
	
	ELSE IF @v_Action_inVc='Edit'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
					/* START CODE : TO UPDATE TABLE - tbl_WorkFlowEventConfig */
					UPDATE [dbo].[tbl_WorkFlowEventConfig]
					   SET [noOfReviewer] = @v_noOfReviewer_inInt,
						  [noOfDays] = @v_noOfDays_inInt,
						  [isDonorConReq] = @v_isDonorConReq_inVc,
						  [eventId] = @v_eventid_inInt,
						  [objectId] = @v_objectId_inInt
					 WHERE eventId=@v_eventid_inInt and objectId=@v_objectId_inInt
					 /* END CODE : TO UPDATE TABLE - tbl_WorkFlowEventConfig */
					 
					 					 									
					/* START CODE : TO INSERT INTO TABLE - [tbl_WorkFlowEventConfigHist] */
					INSERT INTO [dbo].[tbl_WorkFlowEventConfigHist]
					   (
					   [noOfReviewer]
					   ,[noOfDays]
					   ,[isDonorConReq]
					   ,[eventId]
					   ,[objectId]
					   ,[actionDate]
					   ,[actionBy]
					   ,[action])
     
					VALUES
					(
					@v_noOfReviewer_inInt,
					@v_noOfDays_inInt,
					@v_isDonorConReq_inVc,
					@v_eventid_inInt,
					@v_objectId_inInt,
					@v_actiondate_indt,
					@v_actionby_inInt,
					@v_action_invc
					)
					 
					/* END CODE : TO INSERT INTO TABLE - [tbl_WorkFlowEventConfigHist] */			 
					 Set @v_flag_bit=1			
				Select @v_flag_bit as flag, 'Success' as Message
					 COMMIT TRAN	
			END TRY 
			BEGIN CATCH
					BEGIN
						Set @v_flag_bit=0			
				Select @v_flag_bit as flag,  'Error while updating Work Flow event Config ' as Message
						ROLLBACK TRAN
					END
			END CATCH	 
	END
	
	ELSE IF @v_Action_inVc='Delete'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
					
					/* START CODE : TO INSERT INTO TABLE - [tbl_WorkFlowEventConfigHist] */
					INSERT INTO [dbo].[tbl_WorkFlowEventConfigHist]
					   (
					   [noOfReviewer]
					   ,[noOfDays]
					   ,[isDonorConReq]
					   ,[eventId]
					   ,[objectId]
					   ,[actionDate]
					   ,[actionBy]
					   ,[action])    
					
					(SELECT [noOfReviewer]
					  ,[noOfDays]
					  ,[isDonorConReq]
					  ,[eventId]
					  ,[objectId]
					  ,@v_actiondate_indt
					 ,@v_actionby_inInt
					 ,@v_action_invc
  FROM [dbo].[tbl_WorkFlowEventConfig] where eventId=@v_eventid_inInt and objectId=@v_objectId_inInt					
					)
					 
					/* END CODE : TO INSERT INTO TABLE - [tbl_WorkFlowEventConfigHist] */
					
					
					
					/* START CODE : TO DELETE FROM TABLE - tbl_WorkFlowEventConfig */
					DELETE FROM [dbo].tbl_WorkFlowEventConfig      
					 WHERE  eventId=@v_eventid_inInt and objectId=@v_objectId_inInt				 
					 /* END CODE : TO DELETE FROM TABLE - tbl_WorkFlowEventConfig */
					 
					 Set @v_flag_bit=1			
				Select @v_flag_bit as flag,  'Work Flow Event Config deleted.' as Message
					 COMMIT TRAN	
			END TRY 
			BEGIN CATCH
					BEGIN
						Set @v_flag_bit=0			
				Select @v_flag_bit as flag,  'Error while deleting Work Flow Event Config.' as Message
						ROLLBACK TRAN
					END
			END CATCH	 
	END	

END


GO
