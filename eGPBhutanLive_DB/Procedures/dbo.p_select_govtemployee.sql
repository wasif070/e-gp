SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Select government employee details-info
--
--
-- Author: Kinjal
-- Date: 11-11-2010

-- SP Name:  p_select_govtemployee
-- Module:   Manage Users 
-- Function: Store Procedure is use for get government employee details.

--------------------------------------------------------------------------------

-- exec [p_select_govtemployee] 13

CREATE PROCEDURE [dbo].[p_select_govtemployee]

@v_employeeId_inInt Int

AS

BEGIN

SET NOCOUNT ON;

DECLARE @v_flag_bit bit, 
 @v_deptIdInt int,
 @v_officenameVc varchar(8000),
 @v_procurementRoleVc varchar(2000), 
 @v_employeeNameVc varchar(2000),
 @v_designationNameVc varchar(2000),
 @v_officeIdVc Varchar(800),
 @v_procurementRoleIdVc varchar(800),
 @v_employeeRoleIdVc varchar(800)
 


	
	
		/* Fetch Employee Info based on department id*/
DECLARE @empdetail TABLE (departmentId int, departmentname varchar(250), employeeName varchar(2000),finPowerBy varchar(150),userId int, officeName varchar(200), designationName varchar(2000),procurementRole varchar(200), procurementRoleId varchar(500), employeeRoleId varchar(500), officeid varchar(500)--, employeeRoleId int, procurementRoleId varchar(500),  employeeOfficeId int, officeId int, employeeId int 
)


declare cur_empDetail cursor FAST_FORWARD For select distinct departmentid from tbl_EmployeeRoles where employeeid =  @v_employeeId_inInt       
			open cur_empDetail        
			fetch next from cur_empDetail into @v_deptIdInt
				


			While @@Fetch_status = 0        
			Begin 					
						
			
			select @v_officeIdVc=  COALESCE(@v_officeIdVc+', ', ' ') + convert(varchar(20),o.officeId), @v_officenameVc = COALESCE(@v_officenameVc+', ', ' ') + officeName from tbl_OfficeMaster o,tbl_EmployeeOffices e where o.officeId=e.officeId and e.employeeId = @v_employeeId_inInt and departmentId = @v_deptIdInt
	
	
			select @v_designationNameVc = COALESCE(@v_designationNameVc+', ', ' ') + designationName from tbl_EmployeeOffices e,tbl_DesignationMaster d
where e.designationid=d.designationId and e.employeeId = @v_employeeId_inInt and departmentId = @v_deptIdInt
group by e.designationId, d.designationName


Select @v_employeeRoleIdVc= COALESCE(@v_employeeRoleIdVc+', ', ' ') + convert(varchar(10),e.employeeRoleId), @v_procurementRoleIdVc =   COALESCE(@v_procurementRoleIdVc+', ', ' ') + convert(varchar(10),p.procurementRoleId),@v_procurementRoleVc= COALESCE(@v_procurementRoleVc+', ', ' ') + convert(varchar(50),procurementRole) from tbl_EmployeeRoles e,tbl_ProcurementRole p where e.procurementRoleId=p.procurementRoleId
and e.employeeId = @v_employeeId_inInt and departmentId = @v_deptIdInt
	       
		
	
	insert into @empdetail
		Select distinct d.departmentId,departmentname,employeeName,finPowerBy,userId,@v_officenameVc,@v_designationNameVc, @v_procurementRoleVc, @v_procurementRoleIdVc, @v_employeeRoleIdVc, @v_officeIdVc  from tbl_EmployeeRoles e,tbl_DepartmentMaster d,tbl_EmployeeMaster em
 where e.departmentId=d.departmentId and  em.employeeId=e.employeeId and e.employeeId = @v_employeeId_inInt and d.departmentId = @v_deptIdInt
	
			
			set @v_procurementRoleVc=null
			Set @v_designationNameVc =null
			set @v_officenameVc = null
			set @v_procurementRoleIdVc = null
			set @v_employeeRoleIdVc = null
			set @v_officeIdVc = null
			
			fetch next from cur_empDetail into @v_deptIdInt        
			End       

			close cur_empDetail        
			deallocate cur_empDetail
		
	Select 	departmentId,departmentname,employeeName,finPowerBy,userId, officeName, designationName, procurementRole, procurementRoleId, employeeRoleId, officeid from @empdetail

SET NOCOUNT OFF;

END


GO
