SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spInserttbl_TenderColumns]
	@tenderTableId int,
	@columnId smallint,
	@columnHeader varchar(500),
	@dataType Varchar(100),
	@filledBy Varchar(100),
	@columnType varchar(15),
	@sortOrder smallint,
	@showorhide varchar(4),
	@templateTableId int
As
Begin

	Insert Into tbl_TenderColumns	(tenderTableId, columnId, columnHeader, dataType, 
		filledBy, columnType, sortOrder, showorhide, templateTableId)
	Values(@tenderTableId, @columnId, @columnHeader, @dataType, 
		@filledBy, @columnType, @sortOrder, @showorhide, @templateTableId)

End


GO
