SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Insert/Update/Delete tables tbl_AppPackages & tbl_AppPkgLots
--
--
-- Author: Karan
-- Date: 30-10-2010
--
-- Last Modified:
-- Modified By: Karan
-- Date: 03-11-2010
-- Modification: Parameteres default value set as Null because of Delete Case,
--				 Change for new column lotEstCost in table tbl_AppPkgLots,
--				 PackageId returned


-- SP Name: [p_ins_upd_apppackage]
-- Module: APP Package
-- Function: Store Procedure is use for perform insert/update/delete operation for APP Package.

--------------------------------------------------------------------------------
-- Create:	Use for perform Insert operation for tbl_AppPackages & tbl_AppPkgLots.
-- Update:  Use for perform Update operation for tbl_AppPackages & tbl_AppPkgLots.
-- Delete:  Use for perform Delete operation for tbl_AppPackages & tbl_AppPkgLots.
--------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ins_upd_apppackage]

@v_AppId_inInt int=null,
@v_Procurementnature_inVc varchar(10)=null,
@v_ServicesType_inVc varchar(100)=null,
@v_PackageNo_inVc varchar(50)=null,
@v_PackageDesc_inVc varchar(2000)=null,
@v_AllocateBudget_inM money=null,
@v_EstimatedCost_inM money=null,
@v_CpvCode_inVc varchar(8000)=null,
@v_PkgEstCost_inM money=null,
@v_ApprovingAuthEmpId_inInt int=null,
@v_IsPQRequired_inVc varchar(3)=null,
@v_ReoiRfaRequired_inVc varchar(10)=null,
@v_ProcurementMethodId_intInt tinyint=null,
@v_ProcurementType_inVc varchar(3)=null,
@v_SourceOfFund_inVc varchar(200)=null,
@v_AppStatus_inVc varchar(20)=null,
@v_WorkflowStatus_inVc varchar(10)=null,
@v_NoOfStages_intInt tinyint=null,
@v_Action_inVc varchar(50),
@v_PackageId_inInt int=null,
/* START: PARAMETERS FOR PACKAGE LOT TABLE*/
@v_LotNo_inVc varchar(max)=null,
@v_LotDesc_inVc varchar(max)=null,
@v_Quantity_inVc varchar(max)=0,
@v_Unit_inVc varchar(max)='',
@v_LotEstCost_inVc varchar(max)=null,
@v_pkgUrgency_inVc varchar(20)=null,
/* END: PARAMETERS FOR PACKAGE LOT TABLE*/
-- aprojit Start
@v_bidderCategory_inVc varchar(25)=null, -- Work Category
@v_workCategory_inVc varchar(20)=null, --Work type
@v_depoplanWork_inVc varchar(20)=null,
@v_entrustingAgency_inVc varchar(20)=null,
@v_timeFrame_inVc varchar(20)=null,
@v_w1 varchar(10) = null,
@v_w2 varchar(10) = null,
@v_w3 varchar(10) = null,
@v_w4 varchar(10) = null
--aprojit End
AS

BEGIN
if @v_Quantity_inVc is null or @v_Quantity_inVc=''
begin
set @v_Quantity_inVc=0
end
DECLARE @v_flag_bit bit

	IF @v_Action_inVc='Create'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
				/* START CODE : TO INSERT INTO TABLE - tbl_AppPackages */
				INSERT INTO [dbo].[tbl_AppPackages]
					   ([appId]
					   ,[procurementnature]
					   ,[servicesType]
					   ,[packageNo]
					   ,[packageDesc]
					   ,[allocateBudget]
					   ,[estimatedCost]
					   ,[cpvCode]
					   ,[pkgEstCost]
					   ,[approvingAuthEmpId]
					   ,[isPQRequired]
					   ,[reoiRfaRequired]
					   ,[procurementMethodId]
					   ,[procurementType]
					   ,[sourceOfFund]
					   ,[appStatus]
					   ,[workflowStatus]
					   ,[noOfStages],[pkgUrgency]
					   ,[bidderCategory]
					   ,[workCategory],
					    [depoplanWork],
						[entrustingAgency],
						[timeFrame])
				 VALUES
					   (@v_AppId_inInt ,
						@v_Procurementnature_inVc,
						@v_ServicesType_inVc,
						@v_PackageNo_inVc,
						@v_PackageDesc_inVc,
						@v_AllocateBudget_inM,
						@v_EstimatedCost_inM,
						@v_CpvCode_inVc,
						@v_PkgEstCost_inM,
						@v_ApprovingAuthEmpId_inInt,
						@v_IsPQRequired_inVc,
						@v_ReoiRfaRequired_inVc ,
						@v_ProcurementMethodId_intInt,
						@v_ProcurementType_inVc,
						@v_SourceOfFund_inVc,
						@v_AppStatus_inVc,
						@v_WorkflowStatus_inVc,
						@v_NoOfStages_intInt,@v_pkgUrgency_inVc,
						--aprojit Start
						@v_bidderCategory_inVc,
						@v_workCategory_inVc,
						@v_depoplanWork_inVc,
						@v_entrustingAgency_inVc,
						@v_timeFrame_inVc)
						
						--aprojit End
				/* END CODE : TO INSERT INTO TABLE - tbl_AppPackages */

				Set @v_PackageId_inInt= Ident_Current('dbo.tbl_AppPackages')

				/* START: REPLACE '@$' WITH ',' */
				Select @v_LotNo_inVc=REPLACE(@v_LotNo_inVc,'@$',','),
					   @v_LotDesc_inVc=REPLACE(@v_LotDesc_inVc,'@$','#'),
					   @v_Quantity_inVc=REPLACE(@v_Quantity_inVc,'@$',','),
					   @v_Unit_inVc=REPLACE(@v_Unit_inVc,'@$',','),
					   @v_LotEstCost_inVc=REPLACE(@v_LotEstCost_inVc,'@$',',')
				/* END: REPLACE '@$' WITH ',' */

				/* START CODE : TO INSERT INTO TABLE - tbl_AppPkgLots */
				--INSERT INTO [dbo].[tbl_AppPkgLots]
				--	   ([packageid]
				--	   ,[appId]
				--	   ,[lotNo]
				--	   ,[lotDesc]
				--	   ,[quantity]
				--	   ,[unit]
				--	   ,[lotEstCost])
				--VALUES(@v_PackageId_inInt,
				--	   @v_AppId_inInt,
				--	   --dbo.f_HandleSpChar(@v_LotDesc_inVc),
				--	   @v_LotNo_inVc,
				--	   @v_LotDesc_inVc,
				--	   @v_Quantity_inVc,
				--	   @v_Unit_inVc,
				--	   @v_LotEstCost_inVc)
					   
			   IF LEN (@v_w1) > 0 
			   BEGIN
			   INSERT INTO [dbo].[tbl_AppPermission]
				   ([packageId]
				   ,[WorkType]
				   ,[WorkCategroy])
			   VALUES
				   (@v_PackageId_inInt
				   ,@v_workCategory_inVc
				   ,'W1')
			   END
			   
			   IF LEN (@v_w2) > 0 
			   BEGIN
			   INSERT INTO [dbo].[tbl_AppPermission]
				   ([packageId]
				   ,[WorkType]
				   ,[WorkCategroy])
			   VALUES
				   (@v_PackageId_inInt
				   ,@v_workCategory_inVc
				   ,'W2')
			   END
			   
			   IF LEN (@v_w3) > 0 
			   BEGIN
			   INSERT INTO [dbo].[tbl_AppPermission]
				   ([packageId]
				   ,[WorkType]
				   ,[WorkCategroy])
			   VALUES
				   (@v_PackageId_inInt
				   ,@v_workCategory_inVc
				   ,'W3')
			   END
			   
			   IF LEN (@v_w4) > 0 
			   BEGIN
			   INSERT INTO [dbo].[tbl_AppPermission]
				   ([packageId]
				   ,[WorkType]
				   ,[WorkCategroy])
			   VALUES
				   (@v_PackageId_inInt
				   ,@v_workCategory_inVc
				   ,'W4')
			   END
			   
			   INSERT INTO [dbo].[tbl_AppPkgLots]
					   ([packageid]
					   ,[appId]
					   ,[lotNo]
					   ,[lotDesc]
					   ,[quantity]
					   ,[unit]
					   ,[lotEstCost])
				SELECT @v_PackageId_inInt as packageId,
					   @v_AppId_inInt as appId,
					   dbo.f_trim(A.items) as LotNo,
					   dbo.f_trim(B.items) as Description,
					   dbo.f_trim(C.items) as Quantity,
					   dbo.f_trim(D.items) as Unit,
					   dbo.f_trim(E.Items) as LotEstCost
				FROM dbo.f_splitX(@v_LotNo_inVc,',') A
					Inner join	dbo.f_splitX(@v_LotDesc_inVc,'#') B On A.ID=B.ID
					Inner Join dbo.f_splitX(@v_Quantity_inVc,',') C On A.Id=C.Id
					Inner Join dbo.f_splitX(@v_Unit_inVc,',') D On A.Id=D.Id
					Inner Join dbo.f_splitX(@v_LotEstCost_inVc,',') E On A.Id=E.Id
				/* END CODE : TO INSERT INTO TABLE - tbl_AppPkgLots */

				Set @v_flag_bit=1
				Select @v_flag_bit as flag, 'APP Package created.' as Message, @v_PackageId_inInt as PackageId
			COMMIT TRAN
		END TRY
		BEGIN CATCH
			BEGIN
				Set @v_flag_bit=0
				Select @v_flag_bit as flag, ERROR_MESSAGE() as Message, @v_PackageId_inInt as PackageId
				ROLLBACK TRAN
			END
		END CATCH
	END

	ELSE IF @v_Action_inVc='Update'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
				/* START CODE : TO UPDATE TABLE - tbl_AppPackages */
				UPDATE [dbo].[tbl_AppPackages]
					SET [appId] = @v_AppId_inInt,
					  [procurementnature] = @v_Procurementnature_inVc,
					  [servicesType] = @v_ServicesType_inVc,
					  [packageNo] = @v_PackageNo_inVc,
					  [packageDesc] = @v_PackageDesc_inVc,
					  [allocateBudget] = @v_AllocateBudget_inM,
					  [estimatedCost] = @v_EstimatedCost_inM,
					  [cpvCode] = @v_CpvCode_inVc,
					  [pkgEstCost] = @v_PkgEstCost_inM,
					  [approvingAuthEmpId] = @v_ApprovingAuthEmpId_inInt,
					  [isPQRequired] = @v_IsPQRequired_inVc,
					  [reoiRfaRequired] = @v_ReoiRfaRequired_inVc ,
					  [procurementMethodId] = @v_ProcurementMethodId_intInt,
					  [procurementType] = @v_ProcurementType_inVc,
					  [sourceOfFund] = @v_SourceOfFund_inVc,
					  [appStatus] = @v_AppStatus_inVc,

					  [noOfStages] = @v_NoOfStages_intInt,
					  [pkgUrgency]=@v_pkgUrgency_inVc,
					   --aprojit Start
					  [bidderCategory]=@v_bidderCategory_inVc,
					  [workCategory]=@v_workCategory_inVc,
					  [depoplanWork]=@v_depoplanWork_inVc,
					  [entrustingAgency]=@v_entrustingAgency_inVc,
					  [timeFrame]=@v_timeFrame_inVc
					  --aprojit End
					 WHERE packageId=@v_PackageId_inInt
				/* END CODE : TO UPDATE TABLE - tbl_AppPackages */


				/* START CODE : TO DELETE FROM TABLE - tbl_AppPkgLots */
					DELETE FROM [dbo].[tbl_AppPkgLots]
					 WHERE packageid = @v_PackageId_inInt
				/* END CODE : TO DELETE FROM TABLE - tbl_AppPkgLots */
				DELETE FROM [dbo].[tbl_AppPermission]
					 WHERE packageid = @v_PackageId_inInt

				/* START: REPLACE '@$' WITH ',' */
				Select @v_LotNo_inVc=REPLACE(@v_LotNo_inVc,'@$',','),
					   @v_LotDesc_inVc=REPLACE(@v_LotDesc_inVc,'@$','#'),
				       @v_Quantity_inVc=REPLACE(@v_Quantity_inVc,'@$',','),
				       @v_Unit_inVc=REPLACE(@v_Unit_inVc,'@$',','),
				       @v_LotEstCost_inVc=REPLACE(@v_LotEstCost_inVc,'@$',',')  
				/* END: REPLACE '@$' WITH ',' */

				/* START CODE : TO INSERT INTO TABLE - tbl_AppPkgLots */
				--INSERT INTO [dbo].[tbl_AppPkgLots]
				--	   ([packageid]
				--	   ,[appId]
				--	   ,[lotNo]
				--	   ,[lotDesc]
				--	   ,[quantity]
				--	   ,[unit]
				--	   ,[lotEstCost])
				--VALUES(@v_PackageId_inInt,
				--	   @v_AppId_inInt,
				--	   @v_LotNo_inVc,
				--	   --dbo.f_HandleSpChar(@v_LotDesc_inVc),
				--	   @v_LotDesc_inVc,
				--	   @v_Quantity_inVc,
				--	   @v_Unit_inVc,
				--	   @v_LotEstCost_inVc)
					   
			   IF  LEN (@v_w1) > 0 
			   BEGIN
			   INSERT INTO [dbo].[tbl_AppPermission]
				   ([packageId]
				   ,[WorkType]
				   ,[WorkCategroy])
			   VALUES
				   (@v_PackageId_inInt
				   ,@v_workCategory_inVc
				   ,'W1')
			   END
			   
			   IF  LEN (@v_w2) > 0 
			   BEGIN
			   INSERT INTO [dbo].[tbl_AppPermission]
				   ([packageId]
				   ,[WorkType]
				   ,[WorkCategroy])
			   VALUES
				   (@v_PackageId_inInt
				   ,@v_workCategory_inVc
				   ,'W2')
			   END
			   
			   IF LEN (@v_w3) > 0 
			   BEGIN
			   INSERT INTO [dbo].[tbl_AppPermission]
				   ([packageId]
				   ,[WorkType]
				   ,[WorkCategroy])
			   VALUES
				   (@v_PackageId_inInt
				   ,@v_workCategory_inVc
				   ,'W3')
			   END
			   
			   IF LEN (@v_w4) > 0 
			   BEGIN
			   INSERT INTO [dbo].[tbl_AppPermission]
				   ([packageId]
				   ,[WorkType]
				   ,[WorkCategroy])
			   VALUES
				   (@v_PackageId_inInt
				   ,@v_workCategory_inVc
				   ,'W4')
			   END
			   
			   INSERT INTO [dbo].[tbl_AppPkgLots]
					   ([packageid]
					   ,[appId]
					   ,[lotNo]
					   ,[lotDesc]
					   ,[quantity]
					   ,[unit]
					   ,[lotEstCost])
				SELECT @v_PackageId_inInt as packageId,
					   @v_AppId_inInt as appId,
					   dbo.f_trim(A.items) as LotNo,
					   dbo.f_trim(B.items) as Description,
					   dbo.f_trim(C.items) as Quantity,
					   dbo.f_trim(D.items) as Unit,
					   dbo.f_trim(E.Items) as LotEstCost
				FROM dbo.f_splitX(@v_LotNo_inVc,',') A
					Inner join	dbo.f_splitX(@v_LotDesc_inVc,'#') B On A.ID=B.ID
					Inner Join dbo.f_splitX(@v_Quantity_inVc,',') C On A.Id=C.Id
					Inner Join dbo.f_splitX(@v_Unit_inVc,',') D On A.Id=D.Id
					Inner Join dbo.f_splitX(@v_LotEstCost_inVc,',') E On A.Id=E.Id
				/* END CODE : TO INSERT INTO TABLE - tbl_AppPkgLots */

				Set @v_flag_bit=1
				Select @v_flag_bit as flag, 'APP Package updated.' as Message,@v_PackageId_inInt as PackageId
			COMMIT TRAN
		END TRY
		BEGIN CATCH
			BEGIN
				Set @v_flag_bit=0
				Select @v_flag_bit as flag, ERROR_MESSAGE() as Message,@v_PackageId_inInt as PackageId
				ROLLBACK TRAN
			END
		END CATCH
	END
	ELSE IF @v_Action_inVc='Revise'
	BEGIN
		BEGIN TRY
			BEGIN TRAN

                                /* copying original data into temp tables starts */
                               IF NOT EXISTS( SELECT packageId from [dbo].[tbl_TempAppPackages] where  appId=@v_AppId_inInt and packageId=@v_PackageId_inInt )
                               BEGIN
                                INSERT INTO [dbo].[tbl_TempAppPackages] SELECT * FROM [dbo].[tbl_AppPackages] where appId=@v_AppId_inInt and packageId=@v_PackageId_inInt
                                INSERT INTO [dbo].[tbl_TempAppPkgLots] SELECT * FROM [dbo].[tbl_AppPkgLots] where appId=@v_AppId_inInt  and packageId=@v_PackageId_inInt
                                INSERT INTO [dbo].[tbl_TempAppPqTenderDates] SELECT * FROM [dbo].[tbl_AppPqTenderDates] where appId=@v_AppId_inInt and packageId=@v_PackageId_inInt
                               END

                              /* copying original data into temp tables starts */
                              /* START CODE : TO UPDATE TABLE - tbl_AppPackages */
				UPDATE [dbo].[tbl_AppPackages]
					SET [appId] = @v_AppId_inInt,
					  [procurementnature] = @v_Procurementnature_inVc,
					  [servicesType] = @v_ServicesType_inVc,
					  [packageNo] = @v_PackageNo_inVc,
					  [packageDesc] = @v_PackageDesc_inVc,
					  [allocateBudget] = @v_AllocateBudget_inM,
					  [estimatedCost] = @v_EstimatedCost_inM,
					  [cpvCode] = @v_CpvCode_inVc,
					  [pkgEstCost] = @v_PkgEstCost_inM,
					  [approvingAuthEmpId] = @v_ApprovingAuthEmpId_inInt,
					  [isPQRequired] = @v_IsPQRequired_inVc,
					  [reoiRfaRequired] = @v_ReoiRfaRequired_inVc ,
					  [procurementMethodId] = @v_ProcurementMethodId_intInt,
					  [procurementType] = @v_ProcurementType_inVc,
					  [sourceOfFund] = @v_SourceOfFund_inVc,
					  [appStatus] = @v_AppStatus_inVc,
                                          [workflowStatus] = @v_WorkflowStatus_inVc,
					  [noOfStages] = @v_NoOfStages_intInt,
					  [pkgUrgency]=@v_pkgUrgency_inVc,
					  --aprojit Start
					  [bidderCategory]=@v_bidderCategory_inVc,
					  [workCategory]=@v_workCategory_inVc,
					  [depoplanWork]=@v_depoplanWork_inVc,
					  [entrustingAgency]=@v_entrustingAgency_inVc,
					  [timeFrame]=@v_timeFrame_inVc
					  --aprojit End
					 WHERE packageId=@v_PackageId_inInt
				/* END CODE : TO UPDATE TABLE - tbl_AppPackages */


				/* START CODE : TO DELETE FROM TABLE - tbl_AppPkgLots */
					DELETE FROM [dbo].[tbl_AppPkgLots]
					 WHERE packageid = @v_PackageId_inInt
				/* END CODE : TO DELETE FROM TABLE - tbl_AppPkgLots */
					DELETE FROM [dbo].[tbl_AppPermission]
					 WHERE packageid = @v_PackageId_inInt

				/* START: REPLACE '@$' WITH ',' */
				Select @v_LotNo_inVc=REPLACE(@v_LotNo_inVc,'@$',','),
					   @v_LotDesc_inVc=REPLACE(@v_LotDesc_inVc,'@$','#'),
				       @v_Quantity_inVc=REPLACE(@v_Quantity_inVc,'@$',','),
				       @v_Unit_inVc=REPLACE(@v_Unit_inVc,'@$',','),
				       @v_LotEstCost_inVc=REPLACE(@v_LotEstCost_inVc,'@$',',')  
				/* END: REPLACE '@$' WITH ',' */

				/* START CODE : TO INSERT INTO TABLE - tbl_AppPkgLots */
				--INSERT INTO [dbo].[tbl_AppPkgLots]
				--	   ([packageid]
				--	   ,[appId]
				--	   ,[lotNo]
				--	   ,[lotDesc]
				--	   ,[quantity]
				--	   ,[unit]
				--	   ,[lotEstCost])
				--VALUES(@v_PackageId_inInt,
				--	   @v_AppId_inInt,
				--	   @v_LotNo_inVc,
				--	   --dbo.f_HandleSpChar(@v_LotDesc_inVc),
				--	   @v_LotDesc_inVc,
				--	   @v_Quantity_inVc,
				--	   @v_Unit_inVc,
				--	   @v_LotEstCost_inVc)
				
			   IF  LEN (@v_w1) > 0 
			   BEGIN
			   INSERT INTO [dbo].[tbl_AppPermission]
				   ([packageId]
				   ,[WorkType]
				   ,[WorkCategroy])
			   VALUES
				   (@v_PackageId_inInt
				   ,@v_workCategory_inVc
				   ,'W1')
			   END
			   
			   IF  LEN (@v_w2) > 0 
			   BEGIN
			   INSERT INTO [dbo].[tbl_AppPermission]
				   ([packageId]
				   ,[WorkType]
				   ,[WorkCategroy])
			   VALUES
				   (@v_PackageId_inInt
				   ,@v_workCategory_inVc
				   ,'W2')
			   END
			   
			   IF LEN (@v_w3) > 0 
			   BEGIN
			   INSERT INTO [dbo].[tbl_AppPermission]
				   ([packageId]
				   ,[WorkType]
				   ,[WorkCategroy])
			   VALUES
				   (@v_PackageId_inInt
				   ,@v_workCategory_inVc
				   ,'W3')
			   END
			   
			   IF LEN (@v_w4) > 0 
			   BEGIN
			   INSERT INTO [dbo].[tbl_AppPermission]
				   ([packageId]
				   ,[WorkType]
				   ,[WorkCategroy])
			   VALUES
				   (@v_PackageId_inInt
				   ,@v_workCategory_inVc
				   ,'W4')
			   END	   
			   
			   INSERT INTO [dbo].[tbl_AppPkgLots]
					   ([packageid]
					   ,[appId]
					   ,[lotNo]
					   ,[lotDesc]
					   ,[quantity]
					   ,[unit]
					   ,[lotEstCost])
				SELECT @v_PackageId_inInt as packageId,
					   @v_AppId_inInt as appId,
					   dbo.f_trim(A.items) as LotNo,
					   dbo.f_trim(B.items) as Description,
					   dbo.f_trim(C.items) as Quantity,
					   dbo.f_trim(D.items) as Unit,
					   dbo.f_trim(E.Items) as LotEstCost
				FROM dbo.f_splitX(@v_LotNo_inVc,',') A
					Inner join	dbo.f_splitX(@v_LotDesc_inVc,'#') B On A.ID=B.ID
					Inner Join dbo.f_splitX(@v_Quantity_inVc,',') C On A.Id=C.Id
					Inner Join dbo.f_splitX(@v_Unit_inVc,',') D On A.Id=D.Id
					Inner Join dbo.f_splitX(@v_LotEstCost_inVc,',') E On A.Id=E.Id
				/* END CODE : TO INSERT INTO TABLE - tbl_AppPkgLots */

				Set @v_flag_bit=1
				Select @v_flag_bit as flag, 'APP Package updated.' as Message,@v_PackageId_inInt as PackageId
			COMMIT TRAN
		END TRY
		BEGIN CATCH
			BEGIN
				Set @v_flag_bit=0
				Select @v_flag_bit as flag, 'Error while updating APP Package.' as Message,@v_PackageId_inInt as PackageId
				ROLLBACK TRAN
			END
		END CATCH
	END
	ELSE IF @v_Action_inVc='Delete'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
				/* START CODE : TO DELETE FROM TABLE - tbl_AppPkgLots */
				DELETE FROM [dbo].[tbl_AppPkgLots]
				 WHERE packageid = @v_PackageId_inInt
				/* END CODE : TO DELETE FROM TABLE - tbl_AppPkgLots */
				delete from [dbo].tbl_AppEngEstDoc  WHERE packageid = @v_PackageId_inInt
				--Code to delete App Permission
				DELETE FROM [dbo].[tbl_AppPermission]
					 WHERE packageid = @v_PackageId_inInt				
				/* START CODE : TO DELETE FROM TABLE - tbl_AppPackages */
				DELETE FROM [dbo].[tbl_AppPackages]
				 WHERE packageid = @v_PackageId_inInt
				/* END CODE : TO DELETE FROM TABLE - tbl_AppPackages */

				 Set @v_flag_bit=1
				 Select @v_flag_bit as flag, 'APP Package deleted.' as Message
				 COMMIT TRAN
			END TRY
			BEGIN CATCH
				BEGIN
					Set @v_flag_bit=0
					Select @v_flag_bit as flag, 'Error while deleting APP Package.' as Message
					ROLLBACK TRAN
				END
			END CATCH
	END

	ELSE IF @v_action_inVc='publishApp'
	BEGIN
		BEGIN TRY
			BEGIN TRAN	
				update tbl_AppMaster set appStatus = 'Approved' where appId = @v_AppId_inInt
				update tbl_AppPackages set appStatus = 'Approved', workflowStatus = 'Approved' where appId = @v_AppId_inInt and packageNo = @v_PackageNo_inVc and Appstatus in ('BeingRevised','Pending') 
				Set @v_flag_bit=1
				Select @v_flag_bit as flag, 'App Pubished Sucessfully' as Message, 0 as PackageId		
			COMMIT TRAN	
		END TRY 
		BEGIN CATCH
			BEGIN
				Set @v_flag_bit=0
				 
				Select @v_flag_bit as flag, 'Error while publishing app' + ERROR_MESSAGE() as Message
				ROLLBACK TRAN
			END
		END CATCH		
	END
END

GO
