SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Procurement Forum.
-- Author: Rajesh Singh
-- Date: 26-02-2011
--
-- Last Modified: Rajesh
-- Modified By:
--15/11/2010

-- SP Name:  p_search_procurementforum
-- Module:   Public Procuremernt Forum
-- Function: Store Procedure is use for Search query and reply on the basis of given search criteria.
--------------------------------------------------------------------------------

--Exec [p_search_Procurementforum] '','','','','',1,10000
--Exec [p_search_Procurementforum] 'MYTOPIC','','Ketan Prajapati','','1011'
--Exec [p_search_Procurementforum] 'MYREPLIED','','Ketan Prajapati','','1011'
CREATE PROCEDURE [dbo].[p_search_procurementforum]
	@v_Tab varchar(50), 
	@v_Keyword varchar(1000)=NULL,
	@v_PostedBy varchar(100)=NULL,
	@v_DateofPosting varchar(100)=NULL,
	@v_UserId int=NULL, 
	@v_Page int=1,
	@v_PagePerRecord int=10
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	
	Declare @FinalQuery as varchar(max),@v_IntialQuery_Vc Varchar(1000)
	
	--Search By Keywords
	IF @v_Keyword !=NULL OR @v_Keyword <>''
		BEGIN
			Set @FinalQuery='(subject LIKE ''%'+@v_Keyword+'%'' OR description LIKE ''%'+@v_Keyword+'%'') '		
		END
	ELSE
		BEGIN
			Set @FinalQuery=''
		END
	
	--Search By Posted By
	IF @v_PostedBy !=NULL OR @v_PostedBy <>''
		BEGIN
			IF @FinalQuery=''
				BEGIN
					Set @FinalQuery=@FinalQuery+'and fullName ='''+@v_PostedBy+''' '		
				END
			ELSE
				BEGIN
					Set @FinalQuery=@FinalQuery+'and fullName ='''+@v_PostedBy+''' '		
				END
		END
		
	--Search By DateofPosting
	IF @v_DateofPosting !=NULL OR @v_DateofPosting <>''
		BEGIN
			IF @FinalQuery=''
				BEGIN
					Set @FinalQuery=@FinalQuery+'and (( CAST(floor(cast([postDate] as float))as datetime)) = '''+convert(varchar(30),CAST(floor(cast(convert(datetime,@v_DateofPosting,105) as float))as datetime),120)+''') '
				END
			ELSE
				BEGIN
					Set @FinalQuery=@FinalQuery+'and (( CAST(floor(cast([postDate] as float))as datetime)) = '''+convert(varchar(30),CAST(floor(cast(convert(datetime,@v_DateofPosting,105) as float))as datetime),120)+''') '
				END
		END
		
	--Search By UserID
	IF @v_UserId !=NULL OR @v_UserId <>''
		BEGIN
			IF @v_Tab='MYTOPIC'
				BEGIN
					IF @FinalQuery=''
						BEGIN
							Set @FinalQuery=@FinalQuery+'and postedBy='''+cast(@v_UserId as varchar(20))+''' '
						END
					ELSE
						BEGIN
							Set @FinalQuery=@FinalQuery+'and postedBy='''+cast(@v_UserId as varchar(20))+''' '
						END
				END		
			ELSE IF @v_Tab='MYREPLIED'
				BEGIN
					IF @FinalQuery=''
						BEGIN
							Set @FinalQuery=@FinalQuery+'and postedBy='''+cast(@v_UserId as varchar(20))+''' and ppfParentId <>''0'' '
						END
					ELSE
						BEGIN
							Set @FinalQuery=@FinalQuery+'and postedBy='''+cast(@v_UserId as varchar(20))+''' and ppfParentId <>''0'' '
						END
				END
			ELSE 
				BEGIN
					Set @FinalQuery=@FinalQuery+''
				END	
			
		END
		
		IF @FinalQuery=''
			BEGIN
				SET @v_IntialQuery_Vc ='from tbl_PublicProcureForum where ppfId=tbl_PublicProcureForum.ppfId '		
			END
		ELSE
			BEGIN
				SET @v_IntialQuery_Vc= 'from tbl_PublicProcureForum where ppfId=tbl_PublicProcureForum.ppfId '
			END
		
		declare @v_ExecutingQuery_Vc as varchar(max)
		
		
		set @v_ExecutingQuery_Vc='DECLARE @v_Reccountf Int
			DECLARE @v_TotalPagef Int
			SELECT @v_Reccountf = Count(*) From (
			SELECT * From (SELECT ROW_NUMBER() OVER (order by ppfId desc) As Rownumber
			'+@v_IntialQuery_Vc+''+@FinalQuery+') AS DATA) AS TTT
			SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecord as varchar(10))+')
			SELECT *,@v_TotalPagef as TotalPages,@v_Reccountf as TotalRecords From (SELECT ROW_NUMBER() OVER (order by ppfId desc) As Rownumber,[ppfId],[subject] as Topics,fullName as Author,(select  COUNT(*)  from tbl_PublicProcureForum pf where pf.ppfParentId=tbl_PublicProcureForum.ppfParentId  group by ppfParentId) as Replies,''By ''+ fullName +''  On  ''+cast(postDate AS varchar(20))+'''' as LastReply 
			'+@v_IntialQuery_Vc+''+@FinalQuery+') AS DATA where Rownumber between '+cast(((@v_Page - 1) * @v_PagePerRecord + 1) as varchar(10))+' 
			AND '+cast((@v_Page * @v_PagePerRecord) as varchar(10))+''
			
		print @v_ExecutingQuery_Vc
		Exec(@v_ExecutingQuery_Vc)
END


GO
