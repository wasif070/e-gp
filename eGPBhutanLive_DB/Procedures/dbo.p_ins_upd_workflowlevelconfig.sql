SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Insert/Update/Delete table tbl_WorkFlowLevelConfig
--
--
-- Author: Karan
-- Date: 30-10-2010
--
-- Last Modified:
-- Modified By: Karan
-- Date: 04-11-2010
-- Modification: Logic Modified for multiple rows Inser/Update/Delete.
--
--------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_ins_upd_workflowlevelconfig]
	
@v_EventId_insInt smallint=null,
@v_ObjectId_inInt int=null,
@v_ActionDate_insDt datetime=null,
@v_ActivityId_insInt smallint=null,
@v_ChildId_inInt int=null,
@v_FileOnHand_inVc varchar(max)=null,
@v_Action_inVc varchar(50),
@v_worfklowId_inInt int=null,
@v_actionBy_inVc varchar(100)=null,
@v_WfLevel_inVc varchar(max),
@v_UserId_inVc varchar(max),
@v_WfRoleId_inVc varchar(max),
@v_Procurementroleid_inVc varchar(max)

 
	
AS

BEGIN

DECLARE @v_flag_bit bit	, @v_ModuleId_Int tinyint

SET NOCOUNT ON;

	IF @v_Action_inVc='Create'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
			
				Select @v_ModuleId_Int = moduleid from tbl_EventMaster where eventid = @v_EventId_insInt				
				
				/* START: REPLACE '@$' WITH ',' */
				Select @v_WfLevel_inVc=REPLACE(@v_WfLevel_inVc,'@$',','),
					   @v_UserId_inVc=REPLACE(@v_UserId_inVc,'@$',','),
					   @v_WfRoleId_inVc=REPLACE(@v_WfRoleId_inVc,'@$',','),
					   @v_Procurementroleid_inVc=REPLACE(@v_Procurementroleid_inVc,'@$',','),
					   @v_FileOnHand_inVc=REPLACE(@v_FileOnHand_inVc,'@$',',')					  
				/* END: REPLACE '@$' WITH ',' */					
				
				/* START CODE : TO INSERT INTO TABLE - tbl_WorkFlowLevelConfig */				
				INSERT INTO [dbo].[tbl_WorkFlowLevelConfig]
						([eventId]
						,[moduleId]
						,[wfLevel]
						,[userId]
						,[wfRoleId]
						,[objectId]
						,[actionDate]
						,[activityId]
						,[childId]
						,[fileOnHand]
						,[procurementRoleId])
				SELECT  @v_EventId_insInt,
						@v_ModuleId_Int,
						dbo.f_trim(A.items) as wfLevel,
						dbo.f_trim(B.items) as userId,
						dbo.f_trim(C.items) as wfRoleId,
						@v_ObjectId_inInt,
						@v_ActionDate_insDt,
						@v_ActivityId_insInt,
						@v_ChildId_inInt,
						dbo.f_trim(E.items) as fileOnHand,
						dbo.f_trim(D.items) as procurementRoleId
						FROM dbo.f_splitX(@v_WfLevel_inVc,',') A 
						Inner join	dbo.f_splitX(@v_UserId_inVc,',') B On A.ID=B.ID
						Inner Join dbo.f_splitX(@v_WfRoleId_inVc,',') C On A.Id=C.Id
						Inner Join dbo.f_splitX(@v_Procurementroleid_inVc,',') D On A.Id=D.Id
						Inner Join dbo.f_splitX(@v_FileOnHand_inVc,',') E On A.Id=E.Id
				/* END CODE : TO INSERT INTO TABLE - tbl_WorkFlowLevelConfig */									
				 update tbl_WorkFlowLevelConfig set govuserid=et.govUserId
 from  tbl_WorkFlowLevelConfig l,tbl_EmployeeTrasfer et
where l.userId=et.userId and isCurrent='yes' and objectId=@v_ObjectId_inInt
 and childId=@v_ChildId_inInt and activityId=@v_ActivityId_insInt
				/* START CODE: TO INSERT INTO HISTORY TABLE - tbl_WorkFlowLevelConfigHist */
				Insert Into tbl_WorkFlowLevelConfigHist
						(eventid, moduleId, wfLevel, userId, action, wfRoleId, objectId, childId, actionDate, actionBy, fileOnHand, workflowId, procurementRoleId )
				Select eventid, moduleId, wfLevel, userId, @v_Action_inVc, wfRoleId, objectId, childId, actionDate, @v_actionBy_inVc, fileOnHand, worfklowId, procurementRoleId 
				From tbl_WorkFlowLevelConfig 
				Where activityId=@v_ActivityId_insInt And objectId=@v_ObjectId_inInt And childId=@v_ChildId_inInt 					
				/* END CODE: TO INSERT INTO HISTORY TABLE - tbl_WorkFlowLevelConfigHist */
							
				Set @v_flag_bit=1			
				Select @v_flag_bit as flag, 'Work Flow Level Config inserted.' as Message
			COMMIT TRAN	
		END TRY 
		BEGIN CATCH
			BEGIN
				Set @v_flag_bit=0
				Select @v_flag_bit as flag, 'Error while inserting Work Flow Level Config.' as Message
				ROLLBACK TRAN
			END
		END CATCH	 
	END
	
	ELSE IF @v_Action_inVc='Edit'
	BEGIN
	Declare @flag bit
		BEGIN TRY
			BEGIN TRAN
			
			IF ((select noOfReviewer+2 as reviewer from tbl_WorkFlowEventConfig where objectId = @v_ObjectId_inInt and eventId = @v_EventId_insInt) > (select COUNT(wfLevel) from tbl_workflowlevelconfig where objectId = @v_ObjectId_inInt and childId = @v_ChildId_inInt and activityId = @v_ActivityId_insInt and eventId = @v_EventId_insInt))
				BEGIN
				set @flag = 1
				END
			
				/* START CODE : TO DELETE FROM TABLE - tbl_WorkFlowLevelConfig */
				DELETE FROM [dbo].[tbl_WorkFlowLevelConfig]      
						WHERE activityId=@v_ActivityId_insInt And objectId=@v_ObjectId_inInt And childId=@v_ChildId_inInt
				 /* END CODE : TO DELETE FROM TABLE - tbl_WorkFlowLevelConfig */
					 
				Select @v_ModuleId_Int = moduleid from tbl_EventMaster where eventid = @v_EventId_insInt				
				
				/* START: REPLACE '@$' WITH ',' */
				Select @v_WfLevel_inVc=REPLACE(@v_WfLevel_inVc,'@$',','),
					   @v_UserId_inVc=REPLACE(@v_UserId_inVc,'@$',','),
					   @v_WfRoleId_inVc=REPLACE(@v_WfRoleId_inVc,'@$',','),
					   @v_Procurementroleid_inVc=REPLACE(@v_Procurementroleid_inVc,'@$',','),
					   @v_FileOnHand_inVc=REPLACE(@v_FileOnHand_inVc,'@$',',')					  
				/* END: REPLACE '@$' WITH ',' */					
				
				/* START CODE : TO INSERT INTO TABLE - tbl_WorkFlowLevelConfig */				
				INSERT INTO [dbo].[tbl_WorkFlowLevelConfig]
						([eventId]
						,[moduleId]
						,[wfLevel]
						,[userId]
						,[wfRoleId]
						,[objectId]
						,[actionDate]
						,[activityId]
						,[childId]
						,[fileOnHand]
						,[procurementRoleId])
				SELECT  @v_EventId_insInt,
						@v_ModuleId_Int,
						dbo.f_trim(A.items) as wfLevel,
						dbo.f_trim(B.items) as userId,
						dbo.f_trim(C.items) as wfRoleId,
						@v_ObjectId_inInt,
						@v_ActionDate_insDt,
						@v_ActivityId_insInt,
						@v_ChildId_inInt,
						dbo.f_trim(E.items) as fileOnHand,
						dbo.f_trim(D.items) as procurementRoleId
						FROM dbo.f_splitX(@v_WfLevel_inVc,',') A 
						Inner join	dbo.f_splitX(@v_UserId_inVc,',') B On A.ID=B.ID
						Inner Join dbo.f_splitX(@v_WfRoleId_inVc,',') C On A.Id=C.Id
						Inner Join dbo.f_splitX(@v_Procurementroleid_inVc,',') D On A.Id=D.Id
						Inner Join dbo.f_splitX(@v_FileOnHand_inVc,',') E On A.Id=E.Id
				/* END CODE : TO INSERT INTO TABLE - tbl_WorkFlowLevelConfig */					
				 update tbl_WorkFlowLevelConfig set govuserid=et.govUserId
 from  tbl_WorkFlowLevelConfig l,tbl_EmployeeTrasfer et
where l.userId=et.userId and isCurrent='yes' and objectId=@v_ObjectId_inInt
 and childId=@v_ChildId_inInt and activityId=@v_ActivityId_insInt
				/* START CODE: TO INSERT INTO HISTORY TABLE - tbl_WorkFlowLevelConfigHist */
				Insert Into tbl_WorkFlowLevelConfigHist
						(eventid, moduleId, wfLevel, userId, action, wfRoleId, objectId, childId, actionDate, actionBy, fileOnHand, workflowId, procurementRoleId )
				Select eventid, moduleId, wfLevel, userId, @v_Action_inVc, wfRoleId, objectId, childId, getdate(), @v_actionBy_inVc, fileOnHand, worfklowId, procurementRoleId 
				From tbl_WorkFlowLevelConfig 
				Where activityId=@v_ActivityId_insInt And objectId=@v_ObjectId_inInt And childId=@v_ChildId_inInt 					
				/* END CODE: TO INSERT INTO HISTORY TABLE - tbl_WorkFlowLevelConfigHist */
				  /* start: cody by dohatec*/
				 IF (@flag = 1)
					BEGIN	
					delete from tbl_WorkFlowFileOnHand where objectId = @v_ObjectId_inInt and childId = @v_ChildId_inInt and activityId = @v_ActivityId_insInt and eventId = @v_EventId_insInt
					update tbl_workflowlevelconfig set fileOnHand = 'NO' where fileOnHand = 'YES' and objectId = @v_ObjectId_inInt and activityId = @v_ActivityId_insInt and childId = @v_ChildId_inInt and eventId = @v_EventId_insInt
					update tbl_workflowlevelconfig set fileOnHand = 'YES' where wfRoleId = 1 and objectId = @v_ObjectId_inInt and activityId = @v_ActivityId_insInt and childId = @v_ChildId_inInt and eventId = @v_EventId_insInt
					END
				 /* end: cody by dohatec*/
				 Set @v_flag_bit=1
				 Select @v_flag_bit as flag, 'Work Flow Level Config updated.' as Message
				 COMMIT TRAN	
			END TRY 
			BEGIN CATCH
				BEGIN
					Set @v_flag_bit=0
					Select @v_flag_bit as flag, 'Error while updating Work Flow Level Config Lot.' as Message
					ROLLBACK TRAN
				END
			END CATCH	 
	END
	
	ELSE IF @v_Action_inVc='Delete'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
				 /* START CODE: TO INSERT INTO HISTORY TABLE - tbl_WorkFlowLevelConfigHist */
				Insert Into tbl_WorkFlowLevelConfigHist
						(eventid, moduleId, wfLevel, userId, action, wfRoleId, objectId, childId, actionDate, actionBy, fileOnHand, workflowId, procurementRoleId )
				Select eventid, moduleId, wfLevel, userId, @v_Action_inVc, wfRoleId, objectId, childId,  getdate(), @v_actionBy_inVc, fileOnHand, worfklowId, procurementRoleId 
				From tbl_WorkFlowLevelConfig 
				Where activityId=@v_ActivityId_insInt And objectId=@v_ObjectId_inInt And childId=@v_ChildId_inInt
				/* END CODE: TO INSERT INTO HISTORY TABLE - tbl_WorkFlowLevelConfigHist */
		
				/* START CODE : TO DELETE FROM TABLE - tbl_WorkFlowLevelConfig */
				DELETE FROM [dbo].[tbl_WorkFlowLevelConfig]      
				 WHERE activityId=@v_ActivityId_insInt And objectId=@v_ObjectId_inInt And childId=@v_ChildId_inInt
				 /* END CODE : TO DELETE FROM TABLE - tbl_WorkFlowLevelConfig */
				 
				 Set @v_flag_bit=1
				 Select @v_flag_bit as flag, 'Work Flow Level Config deleted.' as Message
				 COMMIT TRAN	
			END TRY 
			BEGIN CATCH
				BEGIN
					Set @v_flag_bit=0
					Select @v_flag_bit as flag, 'Error while deleting Work Flow Level Config.' as Message
					ROLLBACK TRAN
				END
			END CATCH	 
	END	
	
	ELSE IF @v_Action_inVc='Tender Cancel'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
			update tbl_CancelTenderRequest set workflowStatus='Cancelled',tenderStatus='Cancelled' where tenderId=@v_childId_inInt 
			update tbl_TenderDetails set workflowStatus='Cancelled',tenderStatus='Cancelled' where tenderId=@v_childId_inInt 					
			COMMIT TRAN
		END TRY
		BEGIN CATCH
			BEGIN
				Set @v_flag_bit=0
				Select @v_flag_bit as flag,  'Error while canceling the tender.' as Message
				ROLLBACK TRAN
			END
		END CATCH
	END
	ELSE IF @v_Action_inVc='UpdateAfterForwarding'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
				
				DELETE FROM [dbo].[tbl_WorkFlowFileOnHand]
				WHERE activityId =  @v_ActivityId_insInt And objectId=@v_ObjectId_inInt And childId=@v_ChildId_inInt
				
				DELETE FROM [dbo].[tbl_WorkFlowFileHistory]
				WHERE activityId =  @v_ActivityId_insInt And objectId=@v_ObjectId_inInt And childId=@v_ChildId_inInt

				
				UPDATE [dbo].[tbl_workflowlevelconfig] set fileOnHand = 'NO' 
				WHERE fileOnHand = 'YES' and objectId = @v_ObjectId_inInt and activityId = @v_ActivityId_insInt and childId = @v_ChildId_inInt
				
				UPDATE [dbo].[tbl_workflowlevelconfig] set fileOnHand = 'YES' 
				WHERE wfLevel = 1 and objectId = @v_ObjectId_inInt and activityId = @v_ActivityId_insInt and childId = @v_ChildId_inInt


				 Set @v_flag_bit=1
				 Select @v_flag_bit as flag, 'Work Flow Level Config updated.' as Message
				 COMMIT TRAN	
			END TRY 
			BEGIN CATCH
				BEGIN
					Set @v_flag_bit=0
					Select @v_flag_bit as flag, 'Error while updating Work Flow Level Config.' as Message
					ROLLBACK TRAN
				END
			END CATCH	 
	END	
SET NOCOUNT OFF;
END


GO
