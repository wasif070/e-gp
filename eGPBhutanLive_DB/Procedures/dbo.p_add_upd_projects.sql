SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Insert/Update/Delete tables tbl_ProjectMaster & tbl_ProjectPartners
--
--
-- Author: Karan
-- Date: 03-11-2010
--
-- Last Modified:
-- Modified By: Kinjal
-- Date
-- Modification: EditPartner functionality
-- exec [dbo].[p_add_upd_projects] @v_Action_inVc='EditPartner', @v_ProjectId_inInt = 1, @v_progId_inInt =null

-- SP Name : p_add_upd_projects
-- Module : Project Master
-- Function : The purpose of this sp is to insert, update and delete data of prject master

-----------------------------------------------------------------------------------
-- Create     : To insert data of  new created project
-- Update     : To Update data of existing project
-- Delete     : To Delete data of project
-- GetProject :
-- GetFinPower:
-- GetPartner :
-- EditPartner :

--------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_add_upd_projects]

@v_projectName_inVc varchar(500)=null,
@v_projectCode_inVc varchar(150)=null,
@v_progId_inInt int=null,
@v_projectCost_inM money=null,
@v_projectStartDate_inD date=null,
@v_projectEndDate_inD date=null,
@v_sourceOfFund_inVc varchar(200)=null,
@v_SBankDevelopId_inVc varchar(max)=null,
@v_Action_inVc varchar(50),
@v_ProjectId_inInt int=null,
@v_CreatedBy_inInt int=0

AS

BEGIN

SET NOCOUNT ON;

DECLARE @v_flag_bit bit,
 @v_userIdInt int,
 @v_procurementRoleIdVc varchar(800),
 @v_procurementRoleVc varchar(max),
 @v_employeeNameVc varchar(200),
 @v_officeNameVc varchar(150),
 @v_officeIdInt int

	IF @v_Action_inVc='Create'
	BEGIN
		BEGIN TRY
			--BEGIN TRAN
				/* START CODE : TO INSERT INTO TABLE - tbl_ProjectMaster */
				INSERT INTO [dbo].[tbl_ProjectMaster]
				   ([projectName]
				   ,[projectCode]
				   ,[progId]
				   ,[projectCost]
				   ,[projectStartDate]
				   ,[projectEndDate]
				   ,[sourceOfFund],createdBy)
				VALUES
				   (@v_projectName_inVc,
					@v_projectCode_inVc,
					@v_progId_inInt,
					@v_projectCost_inM,
					@v_projectStartDate_inD,
					@v_projectEndDate_inD,
					@v_sourceOfFund_inVc,@v_CreatedBy_inInt)
				/* END CODE : TO INSERT INTO TABLE - tbl_ProjectMaster */
				Set @v_ProjectId_inInt= Ident_Current('dbo.tbl_ProjectMaster')

				-- // REPLACE '@$' WITH ','
				Select @v_SBankDevelopId_inVc=REPLACE(@v_SBankDevelopId_inVc,'@$',',')
				if @v_SBankDevelopId_inVc !='' or @v_SBankDevelopId_inVc != Null
				Begin
				/* START CODE : TO INSERT INTO TABLE - tbl_ProjectPartners */
				INSERT INTO [dbo].[tbl_ProjectPartners] (sBankDevelopId, projectId)
				SELECT dbo.f_trim(items) as sBankDevelopId, @v_ProjectId_inInt as projectId
					FROM dbo.f_split(@v_SBankDevelopId_inVc,',')
				/* END CODE : TO INSERT INTO TABLE - tbl_ProjectPartners */
				End
				else
				Begin
				if @v_ProjectId_inInt !=0
				Begin
				DELETE FROM [dbo].[tbl_ProjectPartners]
					 WHERE projectId = @v_ProjectId_inInt
				End
				End
				Set @v_flag_bit=1
				Select @v_flag_bit as flag, 'Project created.' as Message, Ident_Current('dbo.tbl_ProjectMaster') as projectId
			--COMMIT TRAN
		END TRY
		BEGIN CATCH
			BEGIN
				Set @v_flag_bit=0
				Select @v_flag_bit as flag, 'Error while creating Project.' as Message
				--ROLLBACK TRAN
			END
		END CATCH
	END

	ELSE IF @v_Action_inVc='Update'
	BEGIN
		BEGIN TRY
			--BEGIN TRAN
				/* START CODE : TO UPDATE TABLE - tbl_ProjectMaster */
				UPDATE [dbo].[tbl_ProjectMaster]
				   SET [projectName] = @v_projectName_inVc,
					  [projectCode] = @v_projectCode_inVc,
					  [progId] = @v_progId_inInt,
					  [projectCost] = @v_projectCost_inM,
					  [projectStartDate] = @v_projectStartDate_inD,
					  [projectEndDate] = @v_projectEndDate_inD,
					  [sourceOfFund] = @v_sourceOfFund_inVc,
					  createdby=@v_CreatedBy_inInt
				 WHERE projectID=@v_ProjectId_inInt
				/* END CODE : TO UPDATE TABLE - tbl_ProjectMaster */

				/* START CODE : TO DELETE FROM TABLE - tbl_ProjectPartners */
				DELETE FROM [dbo].[tbl_ProjectPartners]
					 WHERE projectId = @v_ProjectId_inInt
				/* END CODE : TO DELETE FROM TABLE - tbl_ProjectPartners */

				-- // REPLACE '@$' WITH ','
				Select @v_SBankDevelopId_inVc=REPLACE(@v_SBankDevelopId_inVc,'@$',',')

				/* START CODE : TO INSERT INTO TABLE - tbl_ProjectPartners */
				if @v_SBankDevelopId_inVc !=''   or @v_SBankDevelopId_inVc !=null
				Begin
				INSERT INTO [dbo].[tbl_ProjectPartners] (sBankDevelopId, projectId)
				SELECT dbo.f_trim(items) as sBankDevelopId, @v_ProjectId_inInt as projectId
					FROM dbo.f_split(@v_SBankDevelopId_inVc,',')
				/* END CODE : TO INSERT INTO TABLE - tbl_ProjectPartners */
				end
				else
				begin
				DELETE FROM [dbo].[tbl_ProjectPartners]
					 WHERE projectId = @v_ProjectId_inInt
				end
				Set @v_flag_bit=1
				Select @v_flag_bit as flag, 'Project updated.' as Message
			--COMMIT TRAN
		END TRY
		BEGIN CATCH
			BEGIN
				Set @v_flag_bit=0
				Select @v_flag_bit as flag, 'Error while updating Project.' as Message
				--ROLLBACK TRAN
			END
		END CATCH
	END

	ELSE IF @v_Action_inVc='Delete'
	BEGIN
		BEGIN TRY
			--BEGIN TRAN
				/* START CODE : TO DELETE FROM TABLE - tbl_ProjectPartners */
				DELETE FROM [dbo].[tbl_ProjectPartners]
					 WHERE projectId = @v_ProjectId_inInt
				/* END CODE : TO DELETE FROM TABLE - tbl_ProjectPartners */

				/* START CODE : TO DELETE FROM TABLE - tbl_ProjectMaster */
				DELETE FROM [dbo].[tbl_ProjectMaster]
				  WHERE projectId = @v_ProjectId_inInt
				/* END CODE : TO DELETE FROM TABLE - tbl_ProjectMaster */

				 Set @v_flag_bit=1
				 Select @v_flag_bit as flag, 'Project deleted.' as Message
				-- COMMIT TRAN
			END TRY
			BEGIN CATCH
				BEGIN
					Set @v_flag_bit=0
					Select @v_flag_bit as flag, 'Error while deleting Project.' as Message
					--ROLLBACK TRAN
				END
			END CATCH
	END
ELSE IF @v_Action_inVc='GetProject'
	BEGIN
	select isnull((select progName from tbl_ProgrammeMaster pm where pm.progId=pj.progId ),'') progName,progId,[projectName]
      ,[projectCode]
      ,pj.projectId
      , projectCost
      ,REPLACE(CONVERT(VARCHAR(11),[projectStartDate], 106), ' ', '-') projectStartDate
      ,REPLACE(CONVERT(VARCHAR(11),[projectEndDate], 106), ' ', '-') projectEndDate
   --  , 
		 --CASE [sourceOfFund]
   --                      WHEN 'Government' THEN 'RGoB'
   --                      WHEN 'Aid or Grant' THEN 'Donor'
   --                      WHEN 'Own fund' THEN 'Own fund'
   --                      WHEN 'Government, Aid or Grant' THEN 'RGoB, Donor'
   --                      WHEN 'Aid or Grant, Own fund' THEN 'Donor, Own fund'
   --                      WHEN 'Government, Own fund' THEN 'RGoB, Own fund'
   --                      WHEN 'Government, Aid or Grant, Own Fund' THEN 'RGoB, Donor, Own Fund'
   --                  END [sourceOfFund]
      , replace([sourceOfFund],',',', ') [sourceOfFund]
      ,[projectStatus], 0 sBankDevelopId,'' sbDevelopName from tbl_projectmaster pj
where   projectId=@v_ProjectId_inInt
	END

ELSE IF @v_Action_inVc='GetFinPower'
	BEGIN
	select projectid,userid,convert(varchar(120),tp.procurementmethodid) procurementmethodid,convert(varchar(120),'') as procurementMethod ,operation,
	  amount from tbl_ProjectFinPower tp
where    projectid=@v_ProjectId_inInt and userId=@v_progId_inInt
	END

ELSE IF @v_Action_inVc='GetPartner'
	BEGIN
		select '' progName,0 progId,pj.*, pm.sBankDevelopId,sbDevelopName from tbl_projectmaster pj,
		tbl_projectpartners pm,tbl_ScBankDevPartnerMaster sc
where pj.projectId=pm.projectid and  pj.projectId=@v_ProjectId_inInt and sc.sBankDevelopId=pm.sBankDevelopId
	END
ELSE IF @v_Action_inVc='EditPartner'
	BEGIN

		/* Fetch Project Detail based on project id*/
DECLARE @projectdetail TABLE (userId int, rolesId varchar(800),rolesName varchar(max),empName varchar(200),  officeName varchar(150), officeId int )


declare cur_projectDetail cursor FAST_FORWARD For select distinct userid from tbl_ProjectRoles where projectid = @v_ProjectId_inInt and userid = ISNULL(@v_progId_inInt, userid)
			open cur_projectDetail
			fetch next from cur_projectDetail into @v_userIdInt



			While @@Fetch_status = 0
			Begin


		Select  @v_procurementRoleIdVc =   COALESCE(@v_procurementRoleIdVc+', ', ' ') + convert(varchar(30),tbl_ProjectRoles.procurementRoleId), @v_procurementRoleVc= COALESCE(@v_procurementRoleVc+', ', ' ') + convert(varchar(30),tbl_ProcurementRole.procurementRole)  from tbl_ProjectRoles inner join tbl_ProcurementRole on tbl_ProcurementRole.procurementRoleId = tbl_ProjectRoles.procurementRoleId where tbl_ProjectRoles.projectId = @v_ProjectId_inInt and  tbl_ProjectRoles.userId = @v_userIdInt group by tbl_ProjectRoles.userId, tbl_ProjectRoles.procurementRoleId , tbl_ProcurementRole.procurementRole, tbl_ProjectRoles.projectId


	Select @v_employeeNameVc = (employeeName + ' - ' + designationName)
		from tbl_EmployeeMaster
		inner join tbl_DesignationMaster DM On tbl_EmployeeMaster.designationId=DM.designationId
		where userid = @v_userIdInt

	Select @v_officeNameVc = officeName, @v_officeIdInt = officeId from tbl_OfficeMaster where officeid in(
	select officeid from tbl_ProjectOffice where userid = @v_userIdInt and projectId = @v_ProjectId_inInt)

	insert into @projectdetail
	Select 	@v_userIdInt , @v_procurementRoleIdVc , @v_procurementRoleVc , @v_employeeNameVc , @v_officeNameVc, @v_officeIdInt

			set @v_procurementRoleIdVc=null
			set @v_procurementRoleVc=null


			fetch next from cur_projectDetail into @v_userIdInt
			End

			close cur_projectDetail
			deallocate cur_projectDetail
	END
	Select 	userId, rolesId, rolesName, empName,  officeName, officeId from @projectdetail

SET NOCOUNT OFF;

END


GO
