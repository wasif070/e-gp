SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--
-- Purpose: DebarredTenderers WebService Reports
--
--
-- Author: Karan
-- Date: 04-05-2011
--
-- Last Modified:
-- Modified By: 
-- Date:
-- Modification:
--------------------------------------------------------------------------------
-- SP Name	: p_get_PEOfficeDetail
-- Module	: PromisIntegrationService
-- Function	: Select all promis details for webservice.

/*

EXEC [dbo].[[p_get_PEOfficeDetail]

*/


CREATE PROCEDURE [dbo].[p_get_PEOfficeDetail]
AS
BEGIN
	SELECT	dm.departmentType as DeptType, 
			dm.departmentName as OrganizationName, 
			(CASE dm.departmentType 
				WHEN 'Ministry' 
					THEN '-' 
				WHEN 'Division' 
					THEN pdm.departmentName 
				WHEN 'Organization' 
					THEN pdm.departmentName END)
			 AS DivisionName,
			(
			CASE pdm.departmentType 
			WHEN 'Division' 
				THEN (	SELECT	departmentName as OrganizationName FROM	tbl_DepartmentMaster 
						WHERE	departmentId=pdm.parentDepartmentId) 
				ELSE '-' 
			END
			) AS MinistryName,
			officeName as PEOfficeName,
			officeId as PEID,
			sm.stateName as DistrictName
	FROM	tbl_DepartmentMaster dm,tbl_OfficeMaster om, tbl_DepartmentMaster pdm, tbl_StateMaster sm 
	WHERE	dm.departmentId=om.departmentId 
			AND dm.parentDepartmentId=pdm.departmentId 
			AND om.stateId=sm.stateId
	ORDER BY MinistryName DESC
END


GO
