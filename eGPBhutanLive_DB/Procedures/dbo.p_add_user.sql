SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Insert new user.
--		  : Insert Company Information
--		  : Insert Personal Information
--		  : Bidder Modify Notification
--		  : Govt. User Creation
--
--
-- Author: Karan
-- Date: 08-10-2010
--
-- Last Modified:
-- Modified By: Sristy
-- Date: 05-Nov-2016
-- Modification: Govt. User Creation
--------------------------------------------------------------------------------

-- SP Name	:   p_add_user
-- Module	:	New User Registration(Whole cycle upto final submission)
-- Function	:	N.A.
--------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[p_add_user]

@v_UserId_inInt int,
@v_Action VARCHAR(500),
@v_Query VARCHAR(MAX)

AS

BEGIN

SET NOCOUNT ON;

--action added by sristy
DECLARE @v_isJvca_Vc varchar(10), @v_RegistrationType_Vc varchar(50), @v_TendererId_Int int, @v_FolderId_Int int,
		@v_CompanyId_Int int, @v_flag_bit bit,@v_emailId_inVc varchar(50), @v_ExCompanyId_Int int, @v_isadmin varchar(3)
		
SET @v_ExCompanyId_Int = 0
IF @v_Action='FinalSubmission'

BEGIN




SELECT @v_isJvca_Vc=isJvca, @v_RegistrationType_Vc=registrationType 
		   FROM tbl_LoginMaster WHERE userid=@v_Userid_inInt;  
	update tbl_LoginMaster set 	registeredDate=GETDATE()   WHERE userid=@v_Userid_inInt ;
	IF @v_RegistrationType_Vc='individualconsultant'  Or @v_RegistrationType_Vc='other'
	BEGIN
		BEGIN TRY
			BEGIN TRAN		
				-- // SET THE VALUE OF CompanyId TO 1
				Set @v_CompanyId_Int=1
				
				/* START CODE: TO INSERT INTO TABLE - tbl_BiddingPermission */
				INSERT INTO [dbo].[tbl_BiddingPermission]
						([userId],[CompanyID],[ProcurementCategory],[WorkCategroy],[WorkType])
						
				SELECT  [userId],@v_CompanyId_Int,[ProcurementCategory],[WorkCategroy],[WorkType]
						FROM [dbo].[tbl_TempBiddingPermission]
						WHERE userid  = @v_UserId_inInt 

				/* END CODE: TO INSERT INTO TABLE - tbl_BiddingPermission */

				/* START CODE: TO INSERT INTO TABLE - tbl_TendererMaster */
				INSERT INTO [dbo].[tbl_TendererMaster]
						([userId],[companyId],[isAdmin],[title],[firstName],[middleName],[lastName],[fullNameInBangla],[nationality],
						[nationalIdNo],[address1],[address2],[country],[state],[city],[upJilla],[postcode],[phoneNo],[mobileNo],[faxNo]
						,[tinNo],[tinDocName],[comments],[specialization],[website],[designation],[department],[subDistrict], [emailAddress], [UserType])
				SELECT 
					top 1 [userId],@v_CompanyId_Int,'Yes',[title],[firstName],[middleName],[lasatName],[fullNameInBangla],[nationality],
					[nationalIdNo],[address1],[address2],[country],[state],[city],[upJilla],[postcode],[phoneNo],[mobileNo],[faxNo],
					[tinNo],[tinDocName],[comments],[specialization],[website],[designation],[department],[subDistrict], [emailAddress], [UserType]
				FROM tbl_tempTendererMaster	
				WHERE userid = @v_UserId_inInt
				/* END CODE: TO INSERT INTO TABLE - tbl_TendererMaster */
				
				Set @v_TendererId_Int=IDENT_CURRENT('dbo.tbl_TendererMaster')
				
				/* START CODE: TO INSERT INTO TABLE - tbl_TendererFolderMaster */
				INSERT INTO [dbo].[tbl_TendererFolderMaster]
						([tendererId],[folderName],[creationDate])
				SELECT 	@v_TendererId_Int, 'Registration Docs', getdate()	
				/* END CODE: TO INSERT INTO TABLE - tbl_TendererFolderMaster */
					 
				Set @v_FolderId_Int=IDENT_CURRENT('dbo.tbl_TendererFolderMaster')				
				
				/* START CODE: TO INSERT INTO TABLE - tbl_CompanyDocuments */
				INSERT INTO [dbo].[tbl_CompanyDocuments]
						([tendererId],[documentName],[documentSize],[documentBrief],[uploadedDate],[folderId],[documentTypeId],docHash,IsReapplied)
				SELECT	@v_TendererId_Int, [documentName],[documentSize],[documentBrief],[uploadedDate],@v_FolderId_Int, [documentType],docHash,0
						FROM [dbo].[tbl_TempCompanyDocuments]
						Where tendererId in (SELECT  tendererid FROM tbl_TempTendererMaster WHERE userid = @v_UserId_inInt)
				/* END CODE: TO INSERT INTO TABLE - tbl_CompanyDocuments */
				
				/* START CODE: TO INSERT INTO TABLE - tbl_TendererEsignature */
				INSERT INTO [dbo].[tbl_TendererEsignature]
						([documentName],[documentSize],[documentBrief],[uploadedDate],[tendererId])
				SELECT 	documentName,documentSize,documentBrief,uploadedDate,@v_TendererId_Int 
					FROM tbl_TempTendererEsignature 
						Where tendererId in (SELECT  tendererid FROM tbl_TempTendererMaster WHERE userid = @v_UserId_inInt)
				/* END CODE: TO INSERT INTO TABLE - tbl_TendererEsignature */
				
			
				/* START CODE: TO DELETE DATA FROM TEMPORARY TABLES */				
				DELETE FROM tbl_TempTendererEsignature 
						WHERE tendererId in (SELECT  tendererid FROM tbl_TempTendererMaster WHERE userid = @v_UserId_inInt)				
				DELETE FROM [dbo].[tbl_TempCompanyDocuments]
						WHERE tendererId in (SELECT tendererid FROM tbl_TempTendererMaster WHERE userid = @v_UserId_inInt)
				DELETE FROM dbo.tbl_TempCompanyJointVenture 
						WHERE companyId in (SELECT companyId FROM tbl_TempCompanyMaster WHERE userid = @v_UserId_inInt)
				DELETE FROM tbl_tempTendererMaster WHERE userid = @v_UserId_inInt
				DELETE FROM [dbo].[tbl_TempCompanyMaster] WHERE userid  = @v_UserId_inInt 
				DELETE FROM [dbo].[tbl_TempBiddingPermission] WHERE userid  = @v_UserId_inInt 
				/* END CODE: TO DELETE DATA FROM TEMPORARY TABLES */
				
				/* START CODE: TO UPDATE TABLE -  tbl_LoginMaster */
					UPDATE tbl_LoginMaster SET status='pending' WHERE userid=@v_Userid_inInt;  
				/* END CODE: TO UPDATE TABLE -  tbl_LoginMaster */
				Set @v_flag_bit=1
				 select @v_emailId_inVc=emailid from tbl_loginmaster where userid=@v_Userid_inInt
				Select @v_flag_bit as flag,@v_emailId_inVc  as Message
			COMMIT TRAN	
		END TRY 
		BEGIN CATCH
				BEGIN
					Set @v_flag_bit=0
					Select @v_flag_bit as flag, 'User not created.' as Message	,ERROR_MESSAGE()				
					ROLLBACK TRAN
				END
		END CATCH	 
	END
	
	ELSE IF @v_RegistrationType_Vc='contractor' Or @v_RegistrationType_Vc='supplier' 
			Or @v_RegistrationType_Vc='contractsupplier' Or @v_RegistrationType_Vc='consultant' Or @v_RegistrationType_Vc='govtundertaking' 
		Or @v_RegistrationType_Vc='media'
	BEGIN		
		BEGIN TRY
			BEGIN TRAN	
				
				/* START CODE: TO INSERT INTO TABLE - tbl_CompanyMaster */
				--print('inserting for table : [tbl_CompanyMaster]')
				
					/* START : CHECK IF SAME COMP_REG_NO IS ALREADY EXISTS THEN TAKE COMP ID */
						--SELECT   @v_ExCompanyId_Int =0 
						--companyId FROM tbl_CompanyMaster WHERE companyRegNumber in ( SELECT distinct companyRegNumber FROM [tbl_TempCompanyMaster] WHERE userid = @v_UserId_inInt
					--) ORDER BY companyId					
					/* END*/
					
					IF @v_ExCompanyId_Int = 0
					BEGIN
				
				INSERT INTO [dbo].[tbl_CompanyMaster]
						([userId],[companyName],[companyNameInBangla],[companyRegNumber],[establishmentYear],[licIssueDate],[licExpiryDate]
						,[regOffAddress],[regOffCountry],[regOffState],[regOffCity],[regOffUpjilla],[regOffPostcode],[regOffPhoneNo]
						,[regOffFaxNo],[corpOffAddress],[corpOffCountry],[corpOffState],[corpOffCity],[corpOffUpjilla],[corpOffPostcode]
						,[corpOffPhoneno],[corpOffFaxNo],[specialization],[legalStatus],[tinNo],[website],tinDocName, workCategory, originCountry, statutoryCertificateNo, tradeLicenseNumber
						,[regOffMobileNo], [corpOffMobMobileNo], [regOffSubDistrict], [corpOffSubDistrict])
				SELECT  [userId],[companyName],[companyNameInBangla],[companyRegNumber],[establishmentYear],[licIssueDate],[licExpiryDate]
						,[regOffAddress],[regOffCountry],[regOffState],[regOffCity],[regOffUpjilla],[regOffPostcode],[regOffPhoneNo]
						,[regOffFaxNo],[corpOffAddress],[corpOffCountry],[corpOffState],[corpOffCity],[corpOffUpjilla],[corpOffPostcode]
						,[corpOffPhoneno],[corpOffFaxNo],[specialization],[legalStatus],[tinNo],[website],tinDocName, workCategory, originCountry, statutoryCertificateNo, tradeLicenseNumber
						,[regOffMobileNo], [corpOffMobMobileNo], [regOffSubDistrict], [corpOffSubDistrict]
						FROM [dbo].[tbl_TempCompanyMaster]
						WHERE userid  = @v_UserId_inInt 		
				/* END CODE: TO INSERT INTO TABLE - tbl_CompanyMaster */
				
			
					Set @v_CompanyId_Int=IDENT_CURRENT('dbo.tbl_CompanyMaster')
					Set @v_isadmin = 'Yes'
					
				END
				ELSE
				BEGIN
					Set @v_CompanyId_Int= @v_ExCompanyId_Int
					Set @v_isadmin = 'No'
				END
				
				IF @v_isJvca_Vc='yes'
				BEGIN
					/* START CODE: TO INSERT INTO TABLE - tbl_CompanyJointVenture */
					--print('inserting for table : [tbl_CompanyJointVenture]')
					INSERT INTO [dbo].[tbl_CompanyJointVenture]
							([jvCompanyId],[companyId],[jvRole])
					SELECT  jvCompanyId,@v_CompanyId_Int,jvRole
							FROM dbo.tbl_TempCompanyJointVenture 
							WHERE companyId=(SELECT companyId FROM tbl_TempCompanyMaster WHERE userid = @v_UserId_inInt)
					/* END CODE: TO INSERT INTO TABLE - tbl_CompanyJointVenture */
				END
				
				/* START CODE: TO INSERT INTO TABLE - tbl_BiddingPermission */
				INSERT INTO [dbo].[tbl_BiddingPermission]
						([userId],[CompanyID],[ProcurementCategory],[WorkCategroy],[WorkType], [IsReapplied])
						
				SELECT  [userId],@v_CompanyId_Int,[ProcurementCategory],[WorkCategroy],[WorkType], 0
						FROM [dbo].[tbl_TempBiddingPermission]
						WHERE userid  = @v_UserId_inInt 

				/* END CODE: TO INSERT INTO TABLE - tbl_BiddingPermission */

				/* START CODE: TO INSERT INTO TABLE - tbl_TendererMaster */
				--print('inserting for table : [ tbl_TendererMaster]')
				--print (@v_UserId_inInt)
				--print (@v_CompanyId_Int)
				INSERT INTO [dbo].[tbl_TendererMaster]
						([userId],[companyId],[isAdmin],[title],[firstName],[middleName],[lastName],[fullNameInBangla],[nationality],
						[nationalIdNo],[address1],[address2],[country],[state],[city],[upJilla],[postcode],[phoneNo],[mobileNo],[faxNo]
						,[tinNo],[tinDocName],[comments],[specialization],[website],[designation],[department],[subDistrict], [emailAddress], [UserType])
				SELECT top 1
					[userId],@v_CompanyId_Int,@v_isadmin,[title],[firstName],[middleName],[lasatName],[fullNameInBangla],[nationality],
					[nationalIdNo],[address1],[address2],[country],[state],[city],[upJilla],[postcode],[phoneNo],[mobileNo],[faxNo],
					[tinNo],[tinDocName],[comments],[specialization],[website],[designation],[department],[subDistrict], [emailAddress], [UserType]
				FROM tbl_tempTendererMaster	
				WHERE userid = @v_UserId_inInt
				/* END CODE: TO INSERT INTO TABLE - tbl_TendererMaster */
				
				--print ('hello')
				--Set @v_TendererId_Int=IDENT_CURRENT('dbo.tbl_TendererMaster')
				Set @v_TendererId_Int=(SELECT tendererId FROM tbl_TendererMaster where userId = @v_UserId_inInt)
		
				--print (@v_TendererId_Int)
				/* START CODE: TO INSERT INTO TABLE - tbl_TendererFolderMaster */
				--print('inserting for table : [tbl_TendererFolderMaster]')
				INSERT INTO [dbo].[tbl_TendererFolderMaster]
						([tendererId],[folderName],[creationDate])
				SELECT 	@v_TendererId_Int, 'Registration Docs', getdate()	
				/* END CODE: TO INSERT INTO TABLE - tbl_TendererFolderMaster */
				
				--Set @v_FolderId_Int=IDENT_CURRENT('dbo.tbl_TendererFolderMaster')	
				Set @v_FolderId_Int=(SELECT folderId FROM tbl_TendererFolderMaster where tendererId = @v_TendererId_Int)			
				
				/* START CODE: TO INSERT INTO TABLE - tbl_CompanyDocuments */
				--print('inserting for table : [tbl_CompanyDocuments]')
				INSERT INTO [dbo].[tbl_CompanyDocuments]
						([tendererId],[documentName],[documentSize],[documentBrief],[uploadedDate],[folderId],[documentTypeId],docHash,DocRefNo,DocDescription,IsReapplied)
				SELECT	@v_TendererId_Int, [documentName],[documentSize],[documentBrief],[uploadedDate],@v_FolderId_Int, [documentType],docHash,DocRefNo,DocDescription,0
						FROM [dbo].[tbl_TempCompanyDocuments]
						Where tendererId=(SELECT tendererid FROM tbl_TempTendererMaster WHERE userid = @v_UserId_inInt)
				/* END CODE: TO INSERT INTO TABLE - tbl_CompanyDocuments */
				
				/* START CODE: TO INSERT INTO TABLE - tbl_TendererEsignature */
				--print('inserting for table : [tbl_TendererEsignature]')
				INSERT INTO [dbo].[tbl_TendererEsignature]
						([documentName],[documentSize],[documentBrief],[uploadedDate],[tendererId])
				SELECT 	documentName,documentSize,documentBrief,uploadedDate,@v_TendererId_Int 
					FROM tbl_TempTendererEsignature 
						Where tendererId=(SELECT tendererid FROM tbl_TempTendererMaster WHERE userid = @v_UserId_inInt)
				/* END CODE: TO INSERT INTO TABLE - tbl_TendererEsignature */
				
				/* START CODE: TO DELETE DATA FROM TEMPORARY TABLES */				
				DELETE FROM tbl_TempTendererEsignature 
						WHERE tendererId=(SELECT tendererid FROM tbl_TempTendererMaster WHERE userid = @v_UserId_inInt)				
				DELETE FROM [dbo].[tbl_TempCompanyDocuments]
						WHERE tendererId=(SELECT tendererid FROM tbl_TempTendererMaster WHERE userid = @v_UserId_inInt)
				DELETE FROM tbl_tempTendererMaster WHERE userid = @v_UserId_inInt
				DELETE FROM dbo.tbl_TempCompanyJointVenture 
							WHERE companyId=(SELECT companyId FROM tbl_TempCompanyMaster WHERE userid = @v_UserId_inInt)
				DELETE FROM [dbo].[tbl_TempCompanyMaster] WHERE userid  = @v_UserId_inInt 
				DELETE FROM [dbo].[tbl_TempBiddingPermission] WHERE userid  = @v_UserId_inInt 
				/* END CODE: TO DELETE DATA FROM TEMPORARY TABLES */
				
				/* START CODE: TO UPDATE TABLE -  tbl_LoginMaster */
					UPDATE tbl_LoginMaster SET status='pending' WHERE userid=@v_Userid_inInt;  
				/* END CODE: TO UPDATE TABLE -  tbl_LoginMaster */
				Set @v_flag_bit=1
				 select @v_emailId_inVc=emailid from tbl_loginmaster where userid=@v_Userid_inInt
				Select @v_flag_bit as flag,@v_emailId_inVc  as Message
				
				
				
			COMMIT TRAN	
		END TRY 
		BEGIN CATCH
				BEGIN					
					Set @v_flag_bit=0
					Select @v_flag_bit as flag, 'User not created.' as Message	,ERROR_MESSAGE()
					--print (@@Error)
					ROLLBACK TRAN					
				END
		END CATCH	 
	END
END
-- added by sristy
IF @v_Action='RegisterCompanyDetails'

BEGIN
	BEGIN TRY
		BEGIN TRAN
			print(@v_Query)
			exec(@v_Query)
			Set @v_flag_bit=1
			SELECT @v_emailId_inVc = emailid FROM tbl_loginmaster WHERE userid=@v_Userid_inInt
			SELECT @v_flag_bit as flag, @v_emailId_inVc  as Message
		COMMIT TRAN	
	END TRY
	BEGIN CATCH
		BEGIN					
			Set @v_flag_bit=0
			Select @v_flag_bit as flag, 'Problem in inserting Company Details.' as Message, ERROR_MESSAGE()
			--print (@@Error)
			ROLLBACK TRAN					
		END
	END CATCH
END

IF @v_Action='RegisterCompanyDetailsUpdate'

BEGIN
	BEGIN TRY
		BEGIN TRAN
			print(@v_Query)
			exec(@v_Query)
			Set @v_flag_bit = 1
			SELECT @v_emailId_inVc = emailid FROM tbl_loginmaster WHERE userid=@v_Userid_inInt
			SELECT @v_flag_bit as flag, @v_emailId_inVc  as Message
		COMMIT TRAN	
	END TRY
	BEGIN CATCH
		BEGIN					
			Set @v_flag_bit = 0
			Select @v_flag_bit as flag, 'Problem in updating Company Details.' as Message, ERROR_MESSAGE()
			--print (@@Error)
			ROLLBACK TRAN					
		END
	END CATCH
END

IF @v_Action='RegisterIndividual'

BEGIN
	BEGIN TRY
		BEGIN TRAN
			print(@v_Query)
			exec(@v_Query)
			Set @v_flag_bit = 1
			SELECT @v_emailId_inVc = emailid FROM tbl_loginmaster WHERE userid = @v_Userid_inInt
			SELECT @v_flag_bit as flag, @v_emailId_inVc  as Message
		COMMIT TRAN	
	END TRY
	BEGIN CATCH
		BEGIN					
			Set @v_flag_bit = 0
			Select @v_flag_bit as flag, 'Problem in inserting Individual Person Details.' as Message, ERROR_MESSAGE()
			--print (@@Error)
			ROLLBACK TRAN					
		END
	END CATCH
END

IF @v_Action='RegisterJVCACompany'

BEGIN
	BEGIN TRY
		BEGIN TRAN
			print(@v_Query)
			exec(@v_Query)
			Set @v_flag_bit = 1
			SELECT @v_emailId_inVc = emailid FROM tbl_loginmaster WHERE userid = @v_Userid_inInt
			SELECT @v_flag_bit as flag, @v_emailId_inVc  as Message
		COMMIT TRAN	
	END TRY
	BEGIN CATCH
		BEGIN					
			Set @v_flag_bit = 0
			Select @v_flag_bit as flag, 'Problem in inserting JVCA Company Details.' as Message, ERROR_MESSAGE()
			--print (@@Error)
			ROLLBACK TRAN					
		END
	END CATCH
END

IF @v_Action='CreateUserByCompanyAdmin'

BEGIN
	BEGIN TRY
		BEGIN TRAN
			print(@v_Query)
			exec(@v_Query)
			Set @v_flag_bit = 1
			SELECT @v_emailId_inVc = emailid FROM tbl_loginmaster WHERE userid = @v_Userid_inInt
			SELECT @v_flag_bit as flag, @v_emailId_inVc  as Message
		COMMIT TRAN	
	END TRY
	BEGIN CATCH
		BEGIN					
			Set @v_flag_bit = 0
			Select @v_flag_bit as flag, 'Problem in inserting Company User.' as Message, ERROR_MESSAGE()
			--print (@@Error)
			ROLLBACK TRAN					
		END
	END CATCH
END


IF @v_Action='BidderRejection'

BEGIN

DECLARE @v_TempTendererId_Int int, @v_TempCompanyId_Int int
--@v_isJvca_Vc varchar(10), @v_RegistrationType_Vc varchar(50), @v_FolderId_Int int,
--		 @v_flag_bit bit,@v_emailId_inVc varchar(50), @v_ExCompanyId_Int int, @v_isadmin varchar(3)
		
SET @v_ExCompanyId_Int = 0


SELECT @v_isJvca_Vc=isJvca, @v_RegistrationType_Vc=registrationType 
		   FROM tbl_LoginMaster WHERE userid=@v_Userid_inInt;  
	--update tbl_LoginMaster set 	registeredDate=GETDATE()   WHERE userid=@v_Userid_inInt ;
	IF @v_RegistrationType_Vc='individualconsultant'  Or @v_RegistrationType_Vc='other'
	BEGIN
		BEGIN TRY
			BEGIN TRAN		
				-- // SET THE VALUE OF CompanyId TO 1
				Set @v_TempCompanyId_Int=1
				
				/* START CODE: TO INSERT INTO TABLE - tbl_TempBiddingPermission */
				INSERT INTO [dbo].[tbl_TempBiddingPermission]
						([userId],[CompanyID],[ProcurementCategory],[WorkCategroy],[WorkType])
						
				SELECT  [userId],@v_TempCompanyId_Int,[ProcurementCategory],[WorkCategroy],[WorkType]
						FROM [dbo].[tbl_BiddingPermission]
						WHERE userid  = @v_UserId_inInt 

				/* END CODE: TO INSERT INTO TABLE - tbl_TempBiddingPermission */

				/* START CODE: TO INSERT INTO TABLE - tbl_TempTendererMaster */
				INSERT INTO [dbo].[tbl_TempTendererMaster]
						([userId],[companyId],[isAdmin],[title],[firstName],[middleName],[lasatName],[fullNameInBangla],[nationality],
						[nationalIdNo],[address1],[address2],[country],[state],[city],[upJilla],[postcode],[phoneNo],[mobileNo],[faxNo]
						,[tinNo],[tinDocName],[comments],[specialization],[website],[designation],[department],[subDistrict], [emailAddress], [UserType])
				SELECT 
					top 1 [userId],@v_TempCompanyId_Int,[isAdmin],[title],[firstName],[middleName],[lastName],[fullNameInBangla],[nationality],
					[nationalIdNo],[address1],[address2],[country],[state],[city],[upJilla],[postcode],[phoneNo],[mobileNo],[faxNo],
					[tinNo],[tinDocName],[comments],[specialization],[website],[designation],[department],[subDistrict], [emailAddress], [UserType]
				FROM tbl_TendererMaster	
				WHERE userid = @v_UserId_inInt
				/* END CODE: TO INSERT INTO TABLE - tbl_TempTendererMaster */
				
				Set @v_TempTendererId_Int=IDENT_CURRENT('dbo.tbl_TempTendererMaster')
				
				/* START CODE: TO INSERT INTO TABLE - tbl_TendererFolderMaster */
				--INSERT INTO [dbo].[tbl_TendererFolderMaster]
				--		([tendererId],[folderName],[creationDate])
				--SELECT 	@v_TendererId_Int, 'Registration Docs', getdate()	
				/* END CODE: TO INSERT INTO TABLE - tbl_TendererFolderMaster */
					 
				--Set @v_FolderId_Int=IDENT_CURRENT('dbo.tbl_TendererFolderMaster')				
				
				/* START CODE: TO INSERT INTO TABLE - tbl_TempCompanyDocuments */
				INSERT INTO [dbo].[tbl_TempCompanyDocuments]
						([tendererId],[documentName],[documentSize],[documentBrief],[uploadedDate],[documentType],docHash)
				SELECT	@v_TempTendererId_Int, [documentName],[documentSize],[documentBrief],[uploadedDate], [documentTypeId],docHash
						FROM [dbo].[tbl_CompanyDocuments]
						Where tendererId in (SELECT  tendererid FROM tbl_TendererMaster WHERE userid = @v_UserId_inInt)
				/* END CODE: TO INSERT INTO TABLE - tbl_TempCompanyDocuments */
				
				/* START CODE: TO INSERT INTO TABLE - tbl_TempTendererEsignature */
				INSERT INTO [dbo].[tbl_TempTendererEsignature]
						([documentName],[documentSize],[documentBrief],[uploadedDate],[tendererId])
				SELECT 	documentName,documentSize,documentBrief,uploadedDate,@v_TempTendererId_Int 
					FROM tbl_TendererEsignature 
						Where tendererId in (SELECT  tendererid FROM tbl_TendererMaster WHERE userid = @v_UserId_inInt)
				/* END CODE: TO INSERT INTO TABLE - tbl_TempTendererEsignature */
				
			
				/* START CODE: TO DELETE DATA FROM MASTER TABLES */				
				DELETE FROM tbl_TendererEsignature 
						WHERE tendererId in (SELECT  tendererid FROM tbl_TendererMaster WHERE userid = @v_UserId_inInt)				
				DELETE FROM [dbo].[tbl_CompanyDocuments]
						WHERE tendererId in (SELECT tendererid FROM tbl_TendererMaster WHERE userid = @v_UserId_inInt)
				DELETE FROM dbo.tbl_CompanyJointVenture 
						WHERE companyId in (SELECT companyId FROM tbl_CompanyMaster WHERE userid = @v_UserId_inInt)
				DELETE FROM [dbo].[tbl_TendererFolderMaster]
						WHERE tendererId in (SELECT tendererid FROM tbl_TendererMaster WHERE userid = @v_UserId_inInt)
				DELETE FROM tbl_TendererMaster WHERE userid = @v_UserId_inInt
				DELETE FROM [dbo].[tbl_CompanyMaster] WHERE userid  = @v_UserId_inInt 
				DELETE FROM [dbo].[tbl_BiddingPermission] WHERE userid  = @v_UserId_inInt
				
				/* END CODE: TO DELETE DATA FROM MASTER TABLES */
				
				/* START CODE: TO UPDATE TABLE -  tbl_LoginMaster */
					UPDATE tbl_LoginMaster SET status='incomplete' WHERE userid=@v_Userid_inInt;  
				/* END CODE: TO UPDATE TABLE -  tbl_LoginMaster */
				Set @v_flag_bit=1
				 select @v_emailId_inVc=emailid from tbl_loginmaster where userid=@v_Userid_inInt
				Select @v_flag_bit as flag,@v_emailId_inVc  as Message
			COMMIT TRAN	
		END TRY 
		BEGIN CATCH
				BEGIN
					Set @v_flag_bit=0
					Select @v_flag_bit as flag, 'User not rejected.' as Message	,ERROR_MESSAGE()				
					ROLLBACK TRAN
				END
		END CATCH	 
	END
	
	ELSE IF @v_RegistrationType_Vc='contractor' Or @v_RegistrationType_Vc='supplier' 
			Or @v_RegistrationType_Vc='contractsupplier' Or @v_RegistrationType_Vc='consultant' Or @v_RegistrationType_Vc='govtundertaking' 
		Or @v_RegistrationType_Vc='media'
	BEGIN		
		BEGIN TRY
			BEGIN TRAN	
				/* START CODE: TO INSERT INTO TABLE - tbl_TempCompanyMaster */
				--print('inserting for table : [tbl_CompanyMaster]')
				
					/* START : CHECK IF SAME COMP_REG_NO IS ALREADY EXISTS THEN TAKE COMP ID */
						--SELECT   @v_ExCompanyId_Int =0 
						--companyId FROM tbl_CompanyMaster WHERE companyRegNumber in ( SELECT distinct companyRegNumber FROM [tbl_TempCompanyMaster] WHERE userid = @v_UserId_inInt
					--) ORDER BY companyId					
					/* END*/
					
				IF @v_ExCompanyId_Int = 0
				BEGIN
				
				INSERT INTO [dbo].[tbl_TempCompanyMaster]
						([userId],[companyName],[companyNameInBangla],[companyRegNumber],[establishmentYear],[licIssueDate],[licExpiryDate]
						,[regOffAddress],[regOffCountry],[regOffState],[regOffCity],[regOffUpjilla],[regOffPostcode],[regOffPhoneNo]
						,[regOffFaxNo],[corpOffAddress],[corpOffCountry],[corpOffState],[corpOffCity],[corpOffUpjilla],[corpOffPostcode]
						,[corpOffPhoneno],[corpOffFaxNo],[specialization],[legalStatus],[tinNo],[website],tinDocName, workCategory, originCountry, statutoryCertificateNo, tradeLicenseNumber
						,[regOffMobileNo], [corpOffMobMobileNo], [regOffSubDistrict], [corpOffSubDistrict])
				SELECT  [userId],[companyName],[companyNameInBangla],[companyRegNumber],[establishmentYear],[licIssueDate],[licExpiryDate]
						,[regOffAddress],[regOffCountry],[regOffState],[regOffCity],[regOffUpjilla],[regOffPostcode],[regOffPhoneNo]
						,[regOffFaxNo],[corpOffAddress],[corpOffCountry],[corpOffState],[corpOffCity],[corpOffUpjilla],[corpOffPostcode]
						,[corpOffPhoneno],[corpOffFaxNo],[specialization],[legalStatus],[tinNo],[website],tinDocName, workCategory, originCountry, statutoryCertificateNo, tradeLicenseNumber
						,[regOffMobileNo], [corpOffMobMobileNo], [regOffSubDistrict], [corpOffSubDistrict]
						FROM [dbo].[tbl_CompanyMaster]
						WHERE userid  = @v_UserId_inInt 		
				/* END CODE: TO INSERT INTO TABLE - tbl_TempCompanyMaster */
				
			
					Set @v_TempCompanyId_Int=IDENT_CURRENT('dbo.tbl_TempCompanyMaster')
					Set @v_isadmin = 'no'
					
				END
				ELSE
				BEGIN
					Set @v_TempCompanyId_Int= @v_ExCompanyId_Int
					Set @v_isadmin = 'No'
				END
				
				IF @v_isJvca_Vc='yes'
				BEGIN
					/* START CODE: TO INSERT INTO TABLE - tbl_TempCompanyJointVenture */
					--print('inserting for table : [tbl_CompanyJointVenture]')
					INSERT INTO [dbo].[tbl_TempCompanyJointVenture]
							([jvCompanyId],[companyId],[jvRole])
					SELECT  jvCompanyId,@v_TempCompanyId_Int,jvRole
							FROM dbo.tbl_CompanyJointVenture 
							WHERE companyId=(SELECT companyId FROM tbl_CompanyMaster WHERE userid = @v_UserId_inInt)
					/* END CODE: TO INSERT INTO TABLE - tbl_TempCompanyJointVenture */
				END
				
				/* START CODE: TO INSERT INTO TABLE - tbl_TempBiddingPermission */
				INSERT INTO [dbo].[tbl_TempBiddingPermission]
						([userId],[CompanyID],[ProcurementCategory],[WorkCategroy],[WorkType])
						
				SELECT  [userId],@v_TempCompanyId_Int,[ProcurementCategory],[WorkCategroy],[WorkType]
						FROM [dbo].[tbl_BiddingPermission]
						WHERE userid  = @v_UserId_inInt 

				/* END CODE: TO INSERT INTO TABLE - tbl_TempBiddingPermission */

				/* START CODE: TO INSERT INTO TABLE - tbl_TempTendererMaster */
				--print('inserting for table : [ tbl_TendererMaster]')
				--print (@v_UserId_inInt)
				--print (@v_CompanyId_Int)
				INSERT INTO [dbo].[tbl_TempTendererMaster]
						([userId],[companyId],[isAdmin],[title],[firstName],[middleName],[lasatName],[fullNameInBangla],[nationality],
						[nationalIdNo],[address1],[address2],[country],[state],[city],[upJilla],[postcode],[phoneNo],[mobileNo],[faxNo]
						,[tinNo],[tinDocName],[comments],[specialization],[website],[designation],[department],[subDistrict], [emailAddress], [UserType])
				SELECT top 1
					[userId],@v_TempCompanyId_Int,@v_isadmin,[title],[firstName],[middleName],[lastName],[fullNameInBangla],[nationality],
					[nationalIdNo],[address1],[address2],[country],[state],[city],[upJilla],[postcode],[phoneNo],[mobileNo],[faxNo],
					[tinNo],[tinDocName],[comments],[specialization],[website],[designation],[department],[subDistrict], [emailAddress], [UserType]
				FROM tbl_TendererMaster	
				WHERE userid = @v_UserId_inInt
				/* END CODE: TO INSERT INTO TABLE - tbl_TempTendererMaster */
				
				--print ('hello')
				--Set @v_TendererId_Int=IDENT_CURRENT('dbo.tbl_TendererMaster')
				Set @v_TempTendererId_Int=(SELECT tendererId FROM tbl_TempTendererMaster where userId = @v_UserId_inInt)
		
				--print (@v_TendererId_Int)
				/* START CODE: TO INSERT INTO TABLE - tbl_TendererFolderMaster */
				--print('inserting for table : [tbl_TendererFolderMaster]')
				--INSERT INTO [dbo].[tbl_TendererFolderMaster]
				--		([tendererId],[folderName],[creationDate])
				--SELECT 	@v_TendererId_Int, 'Registration Docs', getdate()	
				/* END CODE: TO INSERT INTO TABLE - tbl_TendererFolderMaster */
				
				--Set @v_FolderId_Int=IDENT_CURRENT('dbo.tbl_TendererFolderMaster')	
				--Set @v_FolderId_Int=(SELECT folderId FROM tbl_TendererFolderMaster where tendererId = @v_TendererId_Int)			
				
				/* START CODE: TO INSERT INTO TABLE - tbl_TempCompanyDocuments */
				print('inserting for table : [tbl_TempCompanyDocuments]')
				INSERT INTO [dbo].[tbl_TempCompanyDocuments]
						([tendererId],[documentName],[documentSize],[documentBrief],[uploadedDate],[documentType],docHash,DocRefNo,DocDescription)
				SELECT	@v_TempTendererId_Int, [documentName],[documentSize],[documentBrief],[uploadedDate], [documentTypeId],docHash,DocRefNo,DocDescription
						FROM [dbo].[tbl_CompanyDocuments]
						Where tendererId=(SELECT tendererid FROM tbl_TendererMaster WHERE userid = @v_UserId_inInt)
				/* END CODE: TO INSERT INTO TABLE - tbl_TempCompanyDocuments */
				
				/* START CODE: TO INSERT INTO TABLE - tbl_TempTendererEsignature */
				print('inserting for table : [tbl_TempTendererEsignature]')
				INSERT INTO [dbo].[tbl_TempTendererEsignature]
						([documentName],[documentSize],[documentBrief],[uploadedDate],[tendererId])
				SELECT 	documentName,documentSize,documentBrief,uploadedDate,@v_TempTendererId_Int 
					FROM tbl_TendererEsignature 
						Where tendererId=(SELECT tendererid FROM tbl_TendererMaster WHERE userid = @v_UserId_inInt)
				/* END CODE: TO INSERT INTO TABLE - tbl_TendererEsignature */
				
				/* START CODE: TO DELETE DATA FROM MASTER TABLES */				
				DELETE FROM tbl_TendererEsignature 
						WHERE tendererId=(SELECT tendererid FROM tbl_TempTendererMaster WHERE userid = @v_UserId_inInt)				
				DELETE FROM [dbo].[tbl_CompanyDocuments]
						WHERE tendererId=(SELECT tendererid FROM tbl_TempTendererMaster WHERE userid = @v_UserId_inInt)
				DELETE FROM [dbo].[tbl_TendererFolderMaster] 
						WHERE tendererId=(SELECT tendererid FROM tbl_TempTendererMaster WHERE userid = @v_UserId_inInt)
				DELETE FROM tbl_TendererMaster WHERE userid = @v_UserId_inInt
				DELETE FROM dbo.tbl_CompanyJointVenture 
							WHERE companyId=(SELECT companyId FROM tbl_CompanyMaster WHERE userid = @v_UserId_inInt)
				DELETE FROM [dbo].[tbl_CompanyMaster] WHERE userid  = @v_UserId_inInt 
				DELETE FROM [dbo].[tbl_BiddingPermission] WHERE userid  = @v_UserId_inInt 
				print('delete data')
				/* END CODE: TO DELETE DATA FROM MASTER TABLES */
				
				/* START CODE: TO UPDATE TABLE -  tbl_LoginMaster */
					UPDATE tbl_LoginMaster SET status='incomplete' WHERE userid=@v_Userid_inInt;  
				/* END CODE: TO UPDATE TABLE -  tbl_LoginMaster */
				
				Set @v_flag_bit=1
				 select @v_emailId_inVc=emailid from tbl_loginmaster where userid=@v_Userid_inInt
				Select @v_flag_bit as flag,@v_emailId_inVc  as Message
			COMMIT TRAN	
		END TRY 
		BEGIN CATCH
				BEGIN					
					Set @v_flag_bit=0
					Select @v_flag_bit as flag, 'User not rejected.' as Message	,ERROR_MESSAGE()
					--print (@@Error)
					ROLLBACK TRAN					
				END
		END CATCH	 
	END
END


-- added 
IF @v_Action='GovtUserCreation'

BEGIN
	BEGIN TRY
		BEGIN TRAN
			print(@v_Query)
			exec(@v_Query)
			Set @v_flag_bit = 1
			--Set @v_Userid_inInt=IDENT_CURRENT('dbo.tbl_loginmaster')
			--SELECT @v_emailId_inVc = emailid FROM tbl_loginmaster WHERE userid = @v_Userid_inInt
			DECLARE @v_EmpId_inInt VARCHAR(500)
			SET @v_EmpId_inInt=IDENT_CURRENT('dbo.tbl_EmployeeMaster')
			
			SELECT @v_flag_bit as flag, @v_EmpId_inInt  as Message
		COMMIT TRAN	
	END TRY
	BEGIN CATCH
		BEGIN					
			Set @v_flag_bit = 0
			Select @v_flag_bit as flag, 'Problem in inserting Govt. User.' as Message, ERROR_MESSAGE()
			--print (@@Error)
			ROLLBACK TRAN					
		END
	END CATCH
END

IF @v_Action='createGovtUserRole'

BEGIN
	BEGIN TRY
		BEGIN TRAN
			print(@v_Query)
			exec(@v_Query)
			Set @v_flag_bit = 1
			--Set @v_Userid_inInt=IDENT_CURRENT('dbo.tbl_loginmaster')
			--SELECT @v_emailId_inVc = emailid FROM tbl_loginmaster WHERE userid = @v_Userid_inInt
			--DECLARE @v_EmpId_inInt INT
			SET @v_EmpId_inInt=IDENT_CURRENT('dbo.tbl_EmployeeMaster')
			
			SELECT @v_flag_bit as flag, @v_EmpId_inInt  as Message
		COMMIT TRAN	
	END TRY
	BEGIN CATCH
		BEGIN					
			Set @v_flag_bit = 0
			Select @v_flag_bit as flag, 'Problem in inserting Govt. User Role.' as Message, ERROR_MESSAGE()
			--print (@@Error)
			ROLLBACK TRAN					
		END
	END CATCH
END

IF @v_Action='createPAAdmin'

BEGIN
	BEGIN TRY
		BEGIN TRAN
			print(@v_Query)
			exec(@v_Query)
			Set @v_flag_bit = 1
			DECLARE @v_Userid_inVar VARCHAR (500)
			Set @v_Userid_inVar=IDENT_CURRENT('dbo.tbl_loginmaster')
			SELECT @v_flag_bit as flag, @v_Userid_inVar  as Message
		COMMIT TRAN	
	END TRY
	BEGIN CATCH
		BEGIN					
			Set @v_flag_bit = 0
			Select @v_flag_bit as flag, 'Problem in inserting PA Admin.' as Message, ERROR_MESSAGE()
			--print (@@Error)
			ROLLBACK TRAN					
		END
	END CATCH
END

IF @v_Action='updatePAAdmin' or @v_Action='transferGovtUser'

BEGIN
	BEGIN TRY
		BEGIN TRAN
			print(@v_Query)
			exec(@v_Query)
			Set @v_flag_bit=1
			--SELECT @v_emailId_inVc = emailid FROM tbl_loginmaster WHERE userid=@v_Userid_inInt
			IF @v_Action='updatePAAdmin'
				SELECT @v_flag_bit as flag, 'Update successfully PA Admin Info.'  as Message
			ELSE IF @v_Action='transferGovtUser'
				SELECT @v_flag_bit as flag, 'Govt. User transfers successfully'  as Message
		COMMIT TRAN	
	END TRY
	BEGIN CATCH
		BEGIN					
			Set @v_flag_bit=0
			IF @v_Action='updatePAAdmin'
				Select @v_flag_bit as flag, 'Problem in updating PA Admin Info.' as Message, ERROR_MESSAGE()
			ELSE IF @v_Action='transferGovtUser'
				Select @v_flag_bit as flag, 'Problem in transferinf Govt. User.' as Message, ERROR_MESSAGE()
			--print (@@Error)
			ROLLBACK TRAN					
		END
	END CATCH
END

--end 
	
SET NOCOUNT OFF;

END


GO
