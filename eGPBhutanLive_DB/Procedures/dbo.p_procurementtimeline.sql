SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	<Dohatec,,Istiak>
-- Create date: <26.May.15>
-- Description:	<Storeprocedure for new functionality of Procurement Approval Timeline>
-- =============================================
CREATE PROCEDURE [dbo].[p_procurementtimeline]
	 @v_fieldName1Vc VARCHAR(MAX)=NULL,		  -- Action
	 @v_fieldName2Vc VARCHAR(MAX)=NULL,
	 @v_fieldName3Vc VARCHAR(MAX)=NULL,
	 @v_fieldName4Vc VARCHAR(MAX)=NULL,
	 @v_fieldName5Vc VARCHAR(MAX)=NULL,
	 @v_fieldName6Vc VARCHAR(MAX)=NULL,
	 @v_fieldName7Vc VARCHAR(MAX)=NULL,
	 @v_fieldName8Vc VARCHAR(MAX)=NULL,
	 @v_fieldName9Vc VARCHAR(MAX)=NULL,
	 @v_fieldName10Vc VARCHAR(MAX)=NULL,
	 @v_fieldName11Vc VARCHAR(MAX)=NULL,
	 @v_fieldName12Vc VARCHAR(MAX)=NULL,
	 @v_fieldName13Vc VARCHAR(MAX)=NULL,
	 @v_fieldName14Vc VARCHAR(MAX)=NULL,
	 @v_fieldName15Vc VARCHAR(MAX)=NULL
AS
BEGIN
	IF @v_fieldName1Vc = 'ViewTimeline'
	BEGIN

		DECLARE @v_query VARCHAR(MAX)
		SET @v_query = 'SELECT CONVERT(VARCHAR(128), RuleId) AS FieldValue1
						  ,ApprovalAuthority AS FieldValue2
						  ,ProcurementNature AS FieldValue3
						  ,CONVERT(VARCHAR(128), NoOfDaysForTSC) AS FieldValue4
						  ,CONVERT(VARCHAR(128), NoOfDaysForTECorPEC) AS FieldValue5
						  ,CONVERT(VARCHAR(128), TotalNoOfDaysForEvaluation) AS FieldValue6
						  ,CONVERT(VARCHAR(128), NoOfDaysForApproval) AS FieldValue7
						FROM tbl_ProcurementApprovalTimeline'

		IF @v_fieldName2Vc = 'single'
			SET @v_query = @v_query + ' WHERE RuleId = ' + @v_fieldName3Vc

		print @v_query
		exec (@v_query)
	END

	ELSE IF @v_fieldName1Vc = 'DeleteRows'
	BEGIN
		SET @v_query = 'DELETE
			FROM tbl_ProcurementApprovalTimeline
			WHERE RuleId in (' +  @v_fieldName2Vc + ')'

		print @v_query
		exec (@v_query)

		IF @@ROWCOUNT > 0
		BEGIN
			SELECT 'true' AS FieldValue1
		END
		ELSE
		BEGIN
			SELECT 'false' AS FieldValue1
		END
	END

	ELSE IF @v_fieldName1Vc = 'CountRules'
	BEGIN
		SELECT CONVERT(VARCHAR(10), COUNT(*)) AS FieldValue1  FROM tbl_ProcurementApprovalTimeline
	END

	ELSE IF @v_fieldName1Vc = 'InsertOrUpdate'
	BEGIN
		BEGIN TRY
			DECLARE @TransactionName VARCHAR(50)
			SET @TransactionName = 'InsertOrUpdate'
			BEGIN TRANSACTION @TransactionName
				/*IF @v_fieldName2Vc != ''
				BEGIN
					print @v_fieldName2Vc
					exec (@v_fieldName2Vc)
				END

				IF @v_fieldName3Vc != ''
				BEGIN
					print @v_fieldName3Vc
					exec (@v_fieldName3Vc)
				END*/

				PRINT  ' ' + @v_fieldName2Vc + ' ' + @v_fieldName3Vc
				EXEC (' ' + @v_fieldName2Vc + ' ' + @v_fieldName3Vc)

			COMMIT TRANSACTION @TransactionName
			SELECT 'true' as FieldValue1
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION @TransactionName
			SELECT 'false' AS FieldValue1
		END CATCH
	END
END


GO
