SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Dohatec,,Istiak>
-- Create date: <21.Apr.15>
-- Description:	<Common storeprocedure for new functionality of Evaluation>
-- =============================================
CREATE PROCEDURE [dbo].[p_get_evalcommondata]
	 @v_fieldName1Vc VARCHAR(500)=NULL,		  -- Action
	 @v_fieldName2Vc VARCHAR(500)=NULL,
	 @v_fieldName3Vc VARCHAR(500)=NULL,
	 @v_fieldName4Vc VARCHAR(500)=NULL,
	 @v_fieldName5Vc VARCHAR(500)=NULL,
	 @v_fieldName6Vc VARCHAR(500)=NULL,
	 @v_fieldName7Vc VARCHAR(500)=NULL,
	 @v_fieldName8Vc VARCHAR(500)=NULL,
	 @v_fieldName9Vc VARCHAR(500)=NULL,
	 @v_fieldName10Vc VARCHAR(500)=NULL,
	 @v_fieldName11Vc VARCHAR(500)=NULL,
	 @v_fieldName12Vc VARCHAR(500)=NULL,
	 @v_fieldName13Vc VARCHAR(500)=NULL,
	 @v_fieldName14Vc VARCHAR(500)=NULL,
	 @v_fieldName15Vc VARCHAR(500)=NULL
	 
AS
BEGIN
Declare @evalCount INT
Declare @noaCount int
	IF @v_fieldName1Vc = 'PreviousAllTenders'
	BEGIN
		/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
		/*
			@v_fieldName14Vc : Page Number
			@v_fieldName15Vc : Record Per Page
		*/
		DECLARE @v_StartAt_Int int, @v_StopAt_Int int
		Set @v_StartAt_Int = ((CONVERT(int, @v_fieldName14Vc)-1) *  CONVERT(int, @v_fieldName15Vc)) + 1
		Set @v_StopAt_Int= (CONVERT(int, @v_fieldName14Vc) * CONVERT(int, @v_fieldName15Vc))
		/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */

		DECLARE @v_PHQuery VARCHAR(MAX)
		DECLARE @v_ConditionString_Vc VARCHAR(MAX)

		SET @v_ConditionString_Vc =
			' FROM tbl_NoaIssueDetails n
				INNER JOIN tbl_ContractSign cs ON n.noaIssueId=cs.noaId
				INNER JOIN tbl_TenderDetails td ON td.tenderId = n.tenderId
			WHERE n.userId IN (SELECT tm.userId FROM tbl_tenderermaster tm 
			WHERE tm.companyId = (SELECT companyId FROM tbl_tenderermaster WHERE userId = ' + @v_fieldName2Vc + '))' 		
			
		IF @v_fieldName3Vc !=''	-- Ministry/Division/Organization
			SET @v_ConditionString_Vc = @v_ConditionString_Vc + ' AND td.departmentId = '''+ @v_fieldName3Vc +''''
		IF @v_fieldName4Vc !=''	-- Procuring Entity
			SET @v_ConditionString_Vc = @v_ConditionString_Vc + ' AND td.officeId = '''+ @v_fieldName4Vc +''''
		IF @v_fieldName5Vc !=''	-- Procurement Nature
			SET @v_ConditionString_Vc = @v_ConditionString_Vc + ' AND td.procurementNatureId = '''+ @v_fieldName5Vc +''''
		IF @v_fieldName6Vc !=''	-- Procurement Method
			SET @v_ConditionString_Vc = @v_ConditionString_Vc + ' AND td.procurementMethodId = '''+ @v_fieldName6Vc +''''
		IF @v_fieldName7Vc !=''	-- Procurement Type
			SET @v_ConditionString_Vc = @v_ConditionString_Vc + ' AND td.procurementType = '''+ @v_fieldName7Vc +''''
		IF @v_fieldName8Vc !=''	-- Tender ID
			SET @v_ConditionString_Vc = @v_ConditionString_Vc + ' AND td.tenderId = '''+ @v_fieldName8Vc +''''

		SET @v_PHQuery =
			'SELECT * FROM(SELECT
				CONVERT(VARCHAR(500), td.tenderId) as FieldValue1,
				td.reoiRfpRefNo as FieldValue2,
				td.tenderStatus as FieldValue3,
				td.procurementNature as FieldValue4,
				td.tenderBrief as FieldValue5,
				td.ministry as FieldValue6,
				td.division as FieldValue7,
				td.agency as FieldValue8,
				td.peOfficeName as FieldValue9,
				td.procurementType as FieldValue10,
				td.procurementMethod as FieldValue11,
				CONVERT(VARCHAR(50), ROW_NUMBER() OVER(ORDER BY td.tenderId desc)) as FieldValue12'

		SET @v_PHQuery = @v_PHQuery + ', CONVERT(VARCHAR(500), (SELECT COUNT(td.tenderId)' + @v_ConditionString_Vc + ')) FieldValue13,
						CONVERT(VARCHAR(500), n.userId) AS FieldValue14'

		SET @v_PHQuery = @v_PHQuery + @v_ConditionString_Vc

		SET @v_PHQuery = @v_PHQuery + ') T where T.FieldValue12 BETWEEN '+ CONVERT(VARCHAR(50),@v_StartAt_Int) +' AND '+ CONVERT(VARCHAR(50),@v_StopAt_Int)

		print @v_PHQuery
		exec (@v_PHQuery)
	END
	
    IF @v_fieldName1Vc = 'PreviousAllMappedTenders'
	BEGIN
		/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
		/*
			@v_fieldName14Vc : Page Number
			@v_fieldName15Vc : Record Per Page
		*/
		DECLARE @v_StartAt_Int1 int, @v_StopAt_Int1 int
		Set @v_StartAt_Int1 = ((CONVERT(int, @v_fieldName14Vc)-1) *  CONVERT(int, @v_fieldName15Vc)) + 1
		Set @v_StopAt_Int1= (CONVERT(int, @v_fieldName14Vc) * CONVERT(int, @v_fieldName15Vc))
		/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */

		DECLARE @v_PHQuery1 VARCHAR(MAX)
		DECLARE @v_ConditionString_Vc1 VARCHAR(MAX)

		SET @v_ConditionString_Vc1 =
			' FROM tbl_NoaIssueDetails n
				INNER JOIN tbl_ContractSign cs ON n.noaIssueId=cs.noaId
				INNER JOIN tbl_TenderDetails td ON td.tenderId = n.tenderId
				INNER JOIN (SELECT DISTINCT companyDocId,tenderId FROM tbl_BidDocuments) tbd ON tbd.tenderId = td.tenderId
			WHERE tbd.companyDocId = '+ @v_fieldName9Vc +' and n.userId IN (SELECT tm.userId FROM tbl_tenderermaster tm 
			WHERE tm.companyId = (SELECT companyId FROM tbl_tenderermaster WHERE userId = ' + @v_fieldName2Vc + '))' 		
			
		IF @v_fieldName3Vc !=''	-- Ministry/Division/Organization
			SET @v_ConditionString_Vc1 = @v_ConditionString_Vc1 + ' AND td.departmentId = '''+ @v_fieldName3Vc +''''
		IF @v_fieldName4Vc !=''	-- Procuring Entity
			SET @v_ConditionString_Vc1 = @v_ConditionString_Vc1 + ' AND td.officeId = '''+ @v_fieldName4Vc +''''
		IF @v_fieldName5Vc !=''	-- Procurement Nature
			SET @v_ConditionString_Vc1 = @v_ConditionString_Vc1 + ' AND td.procurementNatureId = '''+ @v_fieldName5Vc +''''
		IF @v_fieldName6Vc !=''	-- Procurement Method
			SET @v_ConditionString_Vc1 = @v_ConditionString_Vc1 + ' AND td.procurementMethodId = '''+ @v_fieldName6Vc +''''
		IF @v_fieldName7Vc !=''	-- Procurement Type
			SET @v_ConditionString_Vc1 = @v_ConditionString_Vc1 + ' AND td.procurementType = '''+ @v_fieldName7Vc +''''
		IF @v_fieldName8Vc !=''	-- Tender ID
			SET @v_ConditionString_Vc1 = @v_ConditionString_Vc1 + ' AND td.tenderId = '''+ @v_fieldName8Vc +''''

		
		SET @v_PHQuery1 =
			'SELECT * FROM(SELECT 
				CONVERT(VARCHAR(500), td.tenderId) as FieldValue1,
				td.reoiRfpRefNo as FieldValue2,
				td.tenderStatus as FieldValue3,
				td.procurementNature as FieldValue4,
				td.tenderBrief as FieldValue5,
				td.ministry as FieldValue6,
				td.division as FieldValue7,
				td.agency as FieldValue8,
				td.peOfficeName as FieldValue9,
				td.procurementType as FieldValue10,
				td.procurementMethod as FieldValue11,
				CONVERT(VARCHAR(50), ROW_NUMBER() OVER(ORDER BY td.tenderId desc)) as FieldValue12'

		SET @v_PHQuery1 = @v_PHQuery1 + ', CONVERT(VARCHAR(500), (SELECT COUNT(td.tenderId)' + @v_ConditionString_Vc1 + ')) FieldValue13,
						CONVERT(VARCHAR(500), n.userId) AS FieldValue14'

		SET @v_PHQuery1 = @v_PHQuery1 + @v_ConditionString_Vc1

		SET @v_PHQuery1 = @v_PHQuery1 + ') T where T.FieldValue12 BETWEEN '+ CONVERT(VARCHAR(50),@v_StartAt_Int1) +' AND '+ CONVERT(VARCHAR(50),@v_StopAt_Int1)

		print(@v_PHQuery1)
		exec (@v_PHQuery1)
	END

	IF @v_fieldName1Vc = 'TendererDashboard' -- Action Name
	BEGIN
	DECLARE @v_Page_inInt INT , @v_RecordPerPage_inInt INT;
	DECLARE @v_PHQuery2 VARCHAR(MAX)
	DECLARE @v_ConditionString_Vc2 VARCHAR(MAX), @v_SearchCondition_Vc2 VARCHAR(MAX)
	DECLARE @v_CntRecord_Vc VARCHAR(MAX)

	SELECT @v_Page_inInt = CONVERT(INT,@v_fieldName4Vc),@v_RecordPerPage_inInt = CONVERT(INT,@v_fieldName5Vc);
	--SET @v_StartAt_Int  = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1;
	--SET  @v_StopAt_Int  = (@v_Page_inInt * @v_RecordPerPage_inInt);
	
	select @v_PHQuery2='DECLARE @v_StartAt_Int INT , @v_StopAt_Int INT, @v_TotalRecordCnt_Int int; SET @v_StartAt_Int  = (('+convert(VARCHAR,@v_Page_inInt)+'-1) * '+convert(VARCHAR,@v_RecordPerPage_inInt)+') + 1;
	SET  @v_StopAt_Int  = ('+convert(VARCHAR,@v_Page_inInt)+' * '+convert(VARCHAR,@v_RecordPerPage_inInt)+');'

	SET @v_ConditionString_Vc2=''
	set @v_SearchCondition_Vc2=''
	if (@v_fieldName6Vc!='' and @v_fieldName6Vc!='0') -- Procurement Nature
		 SET @v_SearchCondition_Vc2 = @v_SearchCondition_Vc2 + ' AND td.procurementNatureId = '''+ @v_fieldName6Vc +''''

	IF (@v_fieldName7Vc !='' and @v_fieldName7Vc!='0')	-- Procurement Method
			SET @v_SearchCondition_Vc2 = @v_SearchCondition_Vc2 + ' AND td.procurementMethodId = '''+ @v_fieldName7Vc +''''

	IF @v_fieldName8Vc !=''	-- Procurement Type
			SET @v_SearchCondition_Vc2 = @v_SearchCondition_Vc2 + ' AND td.procurementType = '''+ @v_fieldName8Vc +''''

	IF (@v_fieldName9Vc !='' and @v_fieldName9Vc!='0')	-- Tender ID
			SET @v_SearchCondition_Vc2 = @v_SearchCondition_Vc2 + ' AND td.tenderId = '''+ @v_fieldName9Vc +''''

	-- publication data  from date is '@v_fieldName10Vc' and to date is @v_fieldName11Vc
		IF @v_fieldName10Vc is not null And @v_fieldName11Vc is not null and @v_fieldName10Vc !='' and @v_fieldName11Vc !=''
		Begin
			Select @v_SearchCondition_Vc2 =@v_SearchCondition_Vc2 + ' And Cast (Floor(Cast (td.tenderPubDt as Float)) as Datetime)  between ''' + @v_fieldName10Vc + ''' And ''' + @v_fieldName11Vc + ''''
		End
		ELSE IF @v_fieldName10Vc is not null And @v_fieldName11Vc is null and @v_fieldName10Vc !=''
		Begin
			Select @v_SearchCondition_Vc2 =@v_SearchCondition_Vc2 + ' And Cast (Floor(Cast (td.tenderPubDt as Float)) as Datetime)  >= ''' + @v_fieldName10Vc + ''''
		End
		ELSE IF @v_fieldName10Vc is null And @v_fieldName11Vc is not null and @v_fieldName10Vc =''
		Begin
			Select @v_SearchCondition_Vc2 =@v_SearchCondition_Vc2 + ' And Cast (Floor(Cast (td.tenderPubDt as Float)) as Datetime)  <= ''' + @v_fieldName11Vc + ''''
		End

		IF(@v_fieldName2Vc = 'Pending')
		BEGIN
		
	/*	set @v_ConditionString_Vc2=' from tbl_tenderpayment tp,tbl_BidConfirmation bc
			inner join  tbl_TenderDetails td on td.tenderId=bc.tenderId
			where paymentFor = ''Document Fees'' and  tp.userid = '+@v_fieldName3Vc+'  and isVerified = ''yes'' and confirmationStatus = null ' + @v_SearchCondition_Vc2
*/		set @v_ConditionString_Vc2=' from tbl_BidConfirmation bc
			inner join  tbl_TenderDetails td on td.tenderId=bc.tenderId
			WHERE  bc.userId = '+@v_fieldName3Vc+' and bc.tenderid not in		
				(select tenderid from tbl_tenderdetails td where submissionDt < GETDATE() )
			and bc.tenderid NOT IN 
				(select tenderid from tbl_finalsubmission where userid ='+@v_fieldName3Vc+') '
				+ @v_SearchCondition_Vc2 


		Select @v_CntRecord_Vc='SELECT Count(bc.tenderId) ' + @v_ConditionString_Vc2
		select @v_PHQuery2= @v_PHQuery2+' Set @v_TotalRecordCnt_Int = ('+@v_CntRecord_Vc+'); 
			 WITH VirtualTable AS
			 (SELECT * ,CONVERT(VARCHAR(50),(ROW_NUMBER() OVER (ORDER BY FieldValue1 DESC))) AS FieldValue13, CONVERT(VARCHAR,@v_TotalRecordCnt_Int) AS FieldValue14 FROM
			(select	CONVERT(VARCHAR(50),bc.tenderId) as FieldValue1, td.reoiRfpRefNo as FieldValue2, td.procurementNature as FieldValue3, td.tenderBrief as FieldValue4, td.ministry as FieldValue5,
			td.division as FieldValue6, td.agency as FieldValue7, td.peOfficeName as FieldValue8, td.procurementType as FieldValue9, td.procurementMethod as FieldValue10,
			CONVERT(VARCHAR(50), td.tenderPubDt, 104)  as FieldValue11, CONVERT(VARCHAR(50), td.submissionDt, 104)  as FieldValue12 '+ @v_ConditionString_Vc2+' )T)
			SELECT * FROM VirtualTable WHERE FieldValue13 BETWEEN @v_StartAt_Int AND @v_StopAt_Int;'
		print(1)
		print(@v_PHQuery2)
		exec (@v_PHQuery2)
		END
		-- progress
		IF(@v_fieldName2Vc = 'Progress')
		BEGIN

		print(@v_fieldName3Vc)
		set @v_ConditionString_Vc2='from tbl_BidConfirmation bc 
			inner join tbl_FinalSubmission fs  
			on fs.tenderId = bc.tenderId and fs.userid = bc.userId inner join  tbl_TenderDetails td on bc.tenderId=td.tenderId and fs.tenderId=td.tenderId
			where  fs.userId = '+@v_fieldName3Vc+'
			 and confirmationStatus = ''accepted'' and bidSubStatus in (''pending'',''modify'') ' + @v_SearchCondition_Vc2
		print(@v_PHQuery2)

		set @v_CntRecord_Vc='SELECT Count(bc.tenderId) ' + @v_ConditionString_Vc2
		select @v_PHQuery2= @v_PHQuery2+'Set @v_TotalRecordCnt_Int = ('+@v_CntRecord_Vc+'); '

		select @v_PHQuery2= @v_PHQuery2+	'WITH VirtualTable AS
			(select  CONVERT(VARCHAR(50),(ROW_NUMBER() OVER (ORDER BY bc.tenderId DESC))) AS FieldValue13, CONVERT(VARCHAR,@v_TotalRecordCnt_Int)  AS FieldValue14,
			 CONVERT(VARCHAR(50),bc.tenderId) as FieldValue1, td.reoiRfpRefNo as FieldValue2, td.procurementNature as FieldValue3, td.tenderBrief as FieldValue4, td.ministry as FieldValue5,
			td.division as FieldValue6, td.agency as FieldValue7, td.peOfficeName as FieldValue8, td.procurementType as FieldValue9, td.procurementMethod as FieldValue10,
			CONVERT(VARCHAR(50), td.tenderPubDt, 104)  as FieldValue11, CONVERT(VARCHAR(50), td.submissionDt, 104)  as FieldValue12 '+@v_ConditionString_Vc2+'  )
			
			 SELECT * FROM VirtualTable WHERE FieldValue13 BETWEEN @v_StartAt_Int AND @v_StopAt_Int;'

		print(@v_PHQuery2)

		exec (@v_PHQuery2)
		END

		IF(@v_fieldName2Vc = 'Complete')
		BEGIN
		set @v_CntRecord_Vc='SELECT Count(td.tenderId) ' + @v_ConditionString_Vc2
		set @v_ConditionString_Vc2=' from tbl_FinalSubmission 
			inner join tbl_TenderDetails td on tbl_FinalSubmission.tenderId=td.tenderId
			where userId = '+@v_fieldName3Vc+' and bidSubStatus = ''finalsubmission''' + @v_SearchCondition_Vc2
		set @v_CntRecord_Vc='SELECT Count(td.tenderId) ' + @v_ConditionString_Vc2
		select @v_PHQuery2=@v_PHQuery2+'Set @v_TotalRecordCnt_Int = ('+@v_CntRecord_Vc+'); 
			WITH VirtualTable AS
			(select  CONVERT(VARCHAR(50),(ROW_NUMBER() OVER (ORDER BY td.tenderId DESC))) AS FieldValue13, CONVERT(VARCHAR,@v_TotalRecordCnt_Int)  AS FieldValue14,
			CONVERT(VARCHAR(50),td.tenderId) as FieldValue1, td.reoiRfpRefNo as FieldValue2, td.procurementNature as FieldValue3, td.tenderBrief as FieldValue4, td.ministry as FieldValue5,
			td.division as FieldValue6, td.agency as FieldValue7, td.peOfficeName as FieldValue8, td.procurementType as FieldValue9, td.procurementMethod as FieldValue10,
			CONVERT(VARCHAR(50), td.tenderPubDt, 104)  as FieldValue11, CONVERT(VARCHAR(50), td.submissionDt, 104)  as FieldValue12 '+@v_ConditionString_Vc2+' )
			SELECT * FROM VirtualTable WHERE FieldValue13 BETWEEN @v_StartAt_Int AND @v_StopAt_Int;'
		print(@v_PHQuery2)

		exec (@v_PHQuery2)
		END
	END
	
		IF @v_fieldName1Vc = 'getFinalUserIdForReport' -- for indreport.jsp
	BEGIN
	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
				SET @evalCount = 0
			else
				select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

	if((select procurementnature from tbl_TenderDetails where tenderid=@v_fieldName2Vc)!='Services' )
	begin
	SELECT distinct convert(varchar(20), tbl_FinalSubmission.userId) as FieldValue1, (select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue2 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c
		where l.userId=t.userId and t.companyId=c.companyId and l.userid = tbl_FinalSubmission.userId) as FieldValue2
	FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
	 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
	 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
	 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
	  and tenderid = @v_fieldName2Vc
	  --and userid not in(select userid from tbl_PostQualification where postQualStatus='Disqualify' and  tenderid=@v_fieldName2Vc and pkgLotId in(select pkglotid  from tbl_ReportLots where reportId= @v_fieldName3Vc ) )
	  and  tbl_FinalSubmission.userId in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and evalCount = @evalCount and (bidderStatus='Technically Responsive'or result='pass'))
	  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc) order by FieldValue2
	 end
	 else
	 begin
	 if((Select tcm.evalMethod
		from tbl_TenderDetails ttd,tbl_TenderTypes ttt,tbl_ConfigEvalMethod tcm
		 where ttd.eventType =ttt.tenderType
		 and ttd.procurementMethodId =tcm.procurementMethodId
		 and ttt.tenderTypeId=tcm.tenderTypeId
		 and ttd.procurementNatureId =tcm.procurementNatureId
		 and ttd.tenderId=@v_fieldName2Vc)!='3')
		 begin
	 SELECT distinct convert(varchar(20), tbl_FinalSubmission.userId) as FieldValue1, (select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue2 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c
		where l.userId=t.userId and t.companyId=c.companyId and l.userid = tbl_FinalSubmission.userId) as FieldValue2
	FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
	 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
	 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
	 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
	  and tenderid = @v_fieldName2Vc
	 and  tbl_FinalSubmission.userId in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and (bidderStatus='Technically Responsive'or result='pass') and evalCount = @evalCount)
	  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc) order by FieldValue2
		end
		else
		begin
		SELECT distinct convert(varchar(20), tbl_FinalSubmission.userId) as FieldValue1, (select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue2 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c
		where l.userId=t.userId and t.companyId=c.companyId and l.userid = tbl_FinalSubmission.userId) as FieldValue2
	FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
	 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
	 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
	 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
	  and tenderid = @v_fieldName2Vc
	 and  tbl_FinalSubmission.userId in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and
	  result='pass' and evalCount = @evalCount)
	  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc) order by FieldValue2
		end
	 end
	End

	IF @v_fieldName1Vc = 'getAllBidders' -- for indreport.jsp
	BEGIN

	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
				SET @evalCount = 0
		else
				select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

	IF(select count(tbl_noaissuedetails.userId)  from tbl_noaacceptance inner join tbl_noaissuedetails 
			on tbl_noaissuedetails.noaissueId = tbl_noaacceptance.noaissueId where tenderId = @v_fieldName2Vc and acceptRejStatus in ('decline','Performance Security not paid')
	) > 0
	BEGIN
	SELECT distinct convert(varchar(20), tbl_FinalSubmission.userId) as FieldValue1, (select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue2 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c
		where l.userId=t.userId and t.companyId=c.companyId and l.userid = tbl_FinalSubmission.userId) as FieldValue2
	FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
	 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
	 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
	 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
	  and tenderid = @v_fieldName2Vc
	   and userId not in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and evalCount = @evalCount and
	  ((isnull(result,'pass')='fail') or (isnull(bidderStatus,'Technically Responsive')='Technically Unresponsive')))
	  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc)
	  and  userId not in (select (tbl_noaissuedetails.userId)  from tbl_noaacceptance inner join tbl_noaissuedetails 
			on tbl_noaissuedetails.noaissueId = tbl_noaacceptance.noaissueId where tenderId = @v_fieldName2Vc and acceptRejStatus in ('decline','Performance Security not paid'))
	   order by FieldValue2
	END
	ELSE
	BEGIN
		SELECT distinct convert(varchar(20), tbl_FinalSubmission.userId) as FieldValue1, (select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue2 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c
		where l.userId=t.userId and t.companyId=c.companyId and l.userid = tbl_FinalSubmission.userId) as FieldValue2
	 FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
	 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
	 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
	 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
	  and tenderid = @v_fieldName2Vc
	   and userId not in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and evalCount = @evalCount and
	  ((isnull(result,'pass')='fail') or (isnull(bidderStatus,'Technically Responsive')='Technically Unresponsive')))
	  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc)
	   order by FieldValue2
	END
	End
	
	IF @v_fieldName1Vc = 'getAllBiddersForTOR' -- for TOR2.jsp
	BEGIN
		SELECT	distinct convert(varchar(20), tbl_FinalSubmission.userId) as FieldValue1, (select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue2 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c
							where l.userId=t.userId and t.companyId=c.companyId and l.userid = tbl_FinalSubmission.userId) as FieldValue2
		FROM	tbl_FinalSubDetail
				INNER JOIN tbl_FinalSubmission ON tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
							AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
		WHERE	tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
				and tenderid = @v_fieldName2Vc
				and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc) order by FieldValue2
	End
	
	IF @v_fieldName1Vc = 'getFinalUserIdForReportLOT' -- for indreport.jsp
	BEGIN
	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
				SET @evalCount = 0
			else
				select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

	if((select procurementnature from tbl_TenderDetails where tenderid=@v_fieldName2Vc)!='Services' )
	begin
	SELECT distinct convert(varchar(20), tbl_FinalSubmission.userId) as FieldValue1, (select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue2 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c
		where l.userId=t.userId and t.companyId=c.companyId and l.userid = tbl_FinalSubmission.userId) as FieldValue2
	FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
	 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
	 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
	 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
	  and tenderid = @v_fieldName2Vc
	  and  tbl_FinalSubmission.userId in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and evalCount = @evalCount and (bidderStatus='Technically Responsive'or result='pass'))
	  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc) 
	  and tenderFormid in (select tenderFormid from tbl_tenderforms where pkgLotId= @v_fieldName4Vc)
	  order by FieldValue2
	 end
	 else
	 begin
	 if((Select tcm.evalMethod
		from tbl_TenderDetails ttd,tbl_TenderTypes ttt,tbl_ConfigEvalMethod tcm
		 where ttd.eventType =ttt.tenderType
		 and ttd.procurementMethodId =tcm.procurementMethodId
		 and ttt.tenderTypeId=tcm.tenderTypeId
		 and ttd.procurementNatureId =tcm.procurementNatureId
		 and ttd.tenderId=@v_fieldName2Vc)!='3')
		 begin
	 SELECT distinct convert(varchar(20), tbl_FinalSubmission.userId) as FieldValue1, (select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue2 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c
		where l.userId=t.userId and t.companyId=c.companyId and l.userid = tbl_FinalSubmission.userId) as FieldValue2
	FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
	 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
	 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
	 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
	  and tenderid = @v_fieldName2Vc
	 and  tbl_FinalSubmission.userId in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and (bidderStatus='Technically Responsive'or result='pass') and evalCount = @evalCount)
	  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc) 
	  and tenderFormid in (select tenderFormid from tbl_tenderforms where pkgLotId= @v_fieldName4Vc)
	  order by FieldValue2
		
		end
		else
		begin
		SELECT distinct convert(varchar(20), tbl_FinalSubmission.userId) as FieldValue1, (select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue2 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c
		where l.userId=t.userId and t.companyId=c.companyId and l.userid = tbl_FinalSubmission.userId) as FieldValue2
	FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
	 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
	 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
	 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
	  and tenderid = @v_fieldName2Vc
	 and  tbl_FinalSubmission.userId in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and
	  result='pass' and evalCount = @evalCount)
	  and tenderFormid in (select tenderFormid from tbl_tenderforms where pkgLotId= @v_fieldName4Vc)
	  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc) order by FieldValue2
		end
	 end
	End

	IF @v_fieldName1Vc = 'getAllBiddersLOT' -- for indreport.jsp
	BEGIN

	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
				SET @evalCount = 0
		else
				select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

	IF(select count(tbl_noaissuedetails.userId)  from tbl_noaacceptance inner join tbl_noaissuedetails 
			on tbl_noaissuedetails.noaissueId = tbl_noaacceptance.noaissueId where tenderId = @v_fieldName2Vc and acceptRejStatus in ('decline','Performance Security not paid')
	) > 0
	BEGIN
	SELECT distinct convert(varchar(20), tbl_FinalSubmission.userId) as FieldValue1, (select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue2 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c
		where l.userId=t.userId and t.companyId=c.companyId and l.userid = tbl_FinalSubmission.userId) as FieldValue2
	FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
	 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
	 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
	 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
	  and tenderid = @v_fieldName2Vc
	   and userId not in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and evalCount = @evalCount and
	  ((isnull(result,'pass')='fail') or (isnull(bidderStatus,'Technically Responsive')='Technically Unresponsive')))
	  and tenderFormid in (select tenderFormid from tbl_tenderforms where pkgLotId= @v_fieldName4Vc)
	  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc)
	  and  userId not in (select (tbl_noaissuedetails.userId)  from tbl_noaacceptance inner join tbl_noaissuedetails 
			on tbl_noaissuedetails.noaissueId = tbl_noaacceptance.noaissueId where tenderId = @v_fieldName2Vc and acceptRejStatus in ('decline','Performance Security not paid'))
	   order by FieldValue2
	END
	ELSE
	BEGIN
		SELECT distinct convert(varchar(20), tbl_FinalSubmission.userId) as FieldValue1, (select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue2 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c
		where l.userId=t.userId and t.companyId=c.companyId and l.userid = tbl_FinalSubmission.userId) as FieldValue2
	 FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
	 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
	 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
	 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
	  and tenderid = @v_fieldName2Vc
	   and tenderFormid in (select tenderFormid from tbl_tenderforms where pkgLotId= @v_fieldName4Vc)
	   and userId not in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and evalCount = @evalCount and
	  ((isnull(result,'pass')='fail') or (isnull(bidderStatus,'Technically Responsive')='Technically Unresponsive')))
	  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc)
	   order by FieldValue2
	END
	End
	
	IF @v_fieldName1Vc = 'getAllBiddersForTORLOT' -- for TOR2.jsp
	BEGIN
		SELECT	distinct convert(varchar(20), tbl_FinalSubmission.userId) as FieldValue1, (select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue2 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c
							where l.userId=t.userId and t.companyId=c.companyId and l.userid = tbl_FinalSubmission.userId) as FieldValue2
		FROM	tbl_FinalSubDetail
				INNER JOIN tbl_FinalSubmission ON tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
							AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
		WHERE	tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
				and tenderid = @v_fieldName2Vc
				and tenderFormid in (select tenderFormid from tbl_tenderforms where pkgLotId= @v_fieldName4Vc)
				and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc) order by FieldValue2
	End
	
	IF @v_fieldName1Vc = 'getGrandSum'     -- For tenderer/BidPreperation.jsp
	BEGIN
	
				select CONVERT(varchar(2000),formName) as FieldValue1,cellValue as FieldValue2,ISNULL(FormType,'na') as FieldValue3,a.tenderTableId,
				(SELECT [password] FROM tbl_loginmaster WHERE userId=@v_fieldName3Vc)  as FieldValue4  from 
				(select tg.cellId,tg.tendertableid,formName,FormType from tbl_TenderGrandSum g,tbl_TenderGrandSumDetail tg,tbl_TenderForms tf
				where g.tenderSumId=tg.tenderSumId  and tenderId=@v_fieldName2Vc
				and tf.tenderFormId=tg.tenderFormId 
				and g.pkgLotId=(SELECT pkgLotId FROM tbl_tenderforms where tenderformid = @v_fieldName4Vc))a
				inner join (select tb.tenderTableId,b.cellId,b.cellValue from 
				tbl_TenderBidDetail b,tbl_TenderBidTable tb,tbl_tenderbidform bf 
				where bf.bidId=tb.bidId and tb.bidTableId=b.bidTableId 
				and tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc )b
				on a.tenderTableId=b.tenderTableId and a.cellId=b.cellId				
	END
	
	IF @v_fieldName1Vc = 'getPercantageValue'     -- For tenderer/BidPreperation.jsp
	BEGIN
		IF(SELECT tenderformid FROM tbl_tenderforms where tenderformid = @v_fieldName4Vc and FormType='BOQSalvage')>1
			BEGIN
			DECLARE @PASS  VARCHAR(MAX) = '' 
				SET @PASS = (SELECT  [password] FROM tbl_loginmaster WHERE userId=@v_fieldName3Vc)
				SELECT cellValue as FieldValue1,@PASS as FieldValue2
				FROM tbl_TenderBidDetail b 
				WHERE bidTableId in 
				(select bidTableId from tbl_TenderBidTable where bidid = @v_fieldName5Vc) 
				AND columnId = 3
				AND rowId = @v_fieldName6Vc
			END
		
	END
END


GO
