SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TaherT
-- alter date: 15-Dec-2010
-- Description:	Proc created for Search Bid data
-- Last Modified
-- Modified BY: Karan
-- =============================================

-- SP Name:	p_get_egpcmsdatamore
-- Module: Progress Report (Works), NOA-Consolidation, Payment-Financial Progress Report, Variation Order
-- Function: Purpose
-----------------------------------------------------------------------------
-- getListForEditForWorks: Get list of items in case of works procurement type in edit mode
-- getQualifiedUser: Gets qualified user in Goods, works n Services case
-- isConsolidateShowOrNot: Get records to identify whether consolidation is finished or not
-- financialProgressReport: Get records to dispaly in Finalcial progress report
-- getListBoxTextForService: Get Values of Lable for Combo box data type
-- getListBoxTextForRE: Get Values of Lable for Combo box data type (For reimbusable expense)
-- CheckForVariOrderSerCase: Checks is variation order place for perticular type of form in case of service
-- getNOADetailWithTenderId: Get NOA details from tenderId
-- avoidepW2STD: Get records to handle requriement of ePW2(b) case STD.
-----------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[p_get_egpcmsdatamore] 
	-- Add the parameters for the stored procedure here
	 @v_fieldName1Vc varchar(500)=NULL,		  -- Action 
	 @v_fieldName2Vc varchar(500)=NULL,
	 @v_fieldName3Vc varchar(500)=NULL, 
	 @v_fieldName4Vc varchar(500)=NULL,
	 @v_fieldName5Vc varchar(500)=NULL,
	 @v_fieldName6Vc varchar(500)=NULL,
	 @v_fieldName7Vc varchar(500)=NULL,
	 @v_fieldName8Vc varchar(500)=NULL,
	 @v_fieldName9Vc varchar(500)=NULL,
	 @v_fieldName10Vc varchar(500)=NULL,
	 @v_fieldName11Vc varchar(500)=NULL,
	 @v_fieldName12Vc varchar(500)=NULL,
	 @v_fieldName13Vc varchar(500)=NULL,
	 @v_fieldName14Vc varchar(500)=NULL,
	 @v_fieldName15Vc varchar(500)=NULL,
	 @v_fieldName16Vc varchar(500)=NULL,
	 @v_fieldName17Vc varchar(500)=NULL,
	 @v_fieldName18Vc varchar(500)=NULL,
	 @v_fieldName19Vc varchar(500)=NULL,
	 @v_fieldName20Vc varchar(500)=NULL
AS
BEGIN
   -- Insert statements for procedure here
 if @v_fieldName1Vc='getListForEditForWorks'
 begin
	select td.wpSrNo as FieldValue1,td.groupId as FieldValue2,td.wpDescription as FieldValue3,
	td.wpUom as FieldValue4,convert(varchar(100),td.wpQty) as FieldValue5,convert(varchar(100),
	ISNULL(tr.qtyAcceptTillThisPr,'0.00')) as FieldValue6,convert(varchar(100),ISNULL(tr.qtyDlvrdCurrPr,'0.00')) as FieldValue7,
	convert(varchar(100),ISNULL(td.wpQty,'0.00')-ISNULL(tr.qtyAcceptTillThisPr,'0.00')) as FieldValue9,ISNULL(REPLACE(CONVERT(VARCHAR(11), tr.itemEntryDt, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), tr.itemEntryDt,108), 1, 5), '') as FieldValue10,
	tr.remarks as FieldValue11,convert(varchar(100),tr.progressReptDtlId) as  FieldValue12,
	convert(varchar(100),tr.progressRepId) as FieldValue13,convert(varchar(100),
	td.wpRowId) as FieldValue14,convert(varchar(100),td.tenderTableId) as FieldValue8,convert(varchar(100),td.amendmentFlag) as FieldValue15, 
	ISNULL(REPLACE(CONVERT(VARCHAR(11), td.wpStartDate, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), td.wpStartDate,108), 1, 5), '') as FieldValue16,
	ISNULL(REPLACE(CONVERT(VARCHAR(11), td.wpEndDate, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), td.wpEndDate,108), 1, 5), '') as FieldValue17,
	convert(varchar(100),td.wpDetailId) as FieldValue18,convert(varchar(100),convert(varchar(100),td.wpRate)) as FieldValue19
	from (select wpSrNo, groupId, wpDescription, wpUom, wpQty, wpRowId,wpDetailId,wpRate,tenderTableId,amendmentFlag,wpStartDate,wpEndDate from  tbl_CMS_WpDetail 
	where wpId=@v_fieldName2Vc and wpId in (select wpId from tbl_CMS_PrMaster where wpId = @v_fieldName2Vc))td 
	left outer join (select rowId, qtyAcceptTillThisPr, qtyDlvrdCurrPr, tenderTableId, totalPendingQty, 
	itemEntryDt, remarks, progressReptDtlId, progressRepId from tbl_CMS_PrDetail 
	where progressRepId=@v_fieldName3Vc)tr on td.tenderTableId=tr.tenderTableId and td.wpRowId = tr.rowId 
	order BY td.groupId
 end  
 
 if @v_fieldName1Vc='getQualifiedUser'
 begin
	IF(select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='Services'
	Begin
		select top 1  (select convert(varchar(20),em.userId) from tbl_EvalRoundMaster em where tenderId=@v_fieldName2Vc and reportType='N1') as FieldValue1,
		rptStatus as FieldValue2 from tbl_EvalRptSentToAA ea,
		tbl_EvalRoundMaster er 
		where ea.roundId=er.roundId and ea.tenderId=@v_fieldName2Vc
		and rptStatus='Approved' 
		order BY er.roundId desc
	End
	Else
	Begin
		select top 1  convert(varchar(100),er.userId) as FieldValue1,rptStatus as FieldValue2 from tbl_EvalRptSentToAA ea,
		tbl_EvalRoundMaster er 
		where ea.roundId=er.roundId and ea.tenderId=@v_fieldName2Vc
		and rptStatus='Approved' 
		order BY er.roundId desc
	End	
 end  
 
 if @v_fieldName1Vc='isConsolidateShowOrNot'
 begin
	select b.acceptRejStatus as FieldValue1 from tbl_NoaIssueDetails a,tbl_NoaAcceptance b 
	where a.tenderId = @v_fieldName2Vc 
	and a.noaIssueId=b.noaIssueId 
	and a.userId=@v_fieldName3Vc
 end
   
 if @v_fieldName1Vc='financialProgressReport'
 
	 if @v_fieldName4Vc <> '0'
		 begin
			select convert(varchar(100),bb.invoiceId) as FieldValue1, 
			convert(varchar(100),bb.invStatus) as FieldValue2, 
			convert(varchar(100),bb.totalInvAmt) as FieldValue3,
			ISNULL(REPLACE(CONVERT(VARCHAR(11), bb.createdDate, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), bb.createdDate,108), 1, 5), '') as FieldValue4, 
			ISNULL(REPLACE(CONVERT(VARCHAR(11), aa.generatedDate, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), aa.generatedDate,108), 1, 5), '') as FieldValue5, 
			ISNULL(convert(varchar(100),aa.grossAmt),'') as FieldValue6, 
			ISNULL(convert(varchar(100),aa.vatAmt),'') as FieldValue7, 
			ISNULL(convert(varchar(100), aa.aitAmt),'') as FieldValue8,
			ISNULL(convert(varchar(100), bb.wpId),'') as FieldValue9,
			ISNULL(convert(varchar(100), bb.invoiceNo),'') as FieldValue10
			from (select a.invoiceId, a.totalInvAmt, a.invStatus, a.createdDate ,a.wpId, a.invoiceNo
			from tbl_CMS_InvoiceMaster a,tbl_ContractSign tc,tbl_NoaIssueDetails tn,tbl_EvalRoundMaster te,tbl_CMS_ROMap tr
			where te.roundId = tn.roundId and tr.romId=te.romId and tn.noaIssueId=tc.noaId and a.wpId=tr.wpId
			and tc.contractSignId= @v_fieldName4Vc) bb left join 
			(select invoiceId, invoiceAmt, generatedDate, grossAmt, vatAmt, aitAmt 
			from tbl_CMS_InvoiceAccDetails where wpId in 
			(select tr.wpId from 
			tbl_ContractSign tc,tbl_NoaIssueDetails tn,tbl_EvalRoundMaster te,tbl_CMS_ROMap tr,Tbl_Cms_InvoiceMaster ti
			where te.roundId = tn.roundId and tr.romId=te.romId and tn.noaIssueId=tc.noaId and ti.wpId=tr.wpId
			and tc.contractSignId= @v_fieldName4Vc) )
			aa on aa.invoiceId = bb.invoiceId order by bb.createdDate asc
		 end
	ELSE
		begin
			select bb.wpid, convert(varchar(100),bb.invoiceId) as FieldValue1, 
			convert(varchar(100),bb.invStatus) as FieldValue2, 
			convert(varchar(100),bb.totalInvAmt) as FieldValue3,
			ISNULL(REPLACE(CONVERT(VARCHAR(11), bb.createdDate, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), bb.createdDate,108), 1, 5), '') as FieldValue4, 
			ISNULL(REPLACE(CONVERT(VARCHAR(11), aa.generatedDate, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), aa.generatedDate,108), 1, 5), '') as FieldValue5, 
			ISNULL(convert(varchar(100),aa.grossAmt),'') as FieldValue6, 
			ISNULL(convert(varchar(100),aa.vatAmt),'') as FieldValue7, 
			ISNULL(convert(varchar(100), aa.aitAmt),'') as FieldValue8,
			ISNULL(convert(varchar(100), bb.wpId),'') as FieldValue9,
			ISNULL(convert(varchar(100), bb.invoiceNo),'') as FieldValue10
			from 
			(select a.invoiceId, a.totalInvAmt, a.invStatus, a.createdDate,a.wpId, a.invoiceNo from tbl_CMS_InvoiceMaster a, tbl_CMS_WpMaster b
			where a.wpId = b.wpId and a.tenderId = @v_fieldName2Vc and b.isRepeatOrder = 'no') 
			bb left join (select invoiceId, invoiceAmt, generatedDate, grossAmt, vatAmt, aitAmt 
			from tbl_CMS_InvoiceAccDetails where wpId in (selecT wpId from tbl_CMS_WpMaster where wpLotId = @v_fieldName3Vc 
			and isRepeatOrder = 'no') )
			aa on aa.invoiceId = bb.invoiceId order by bb.createdDate asc
		end

 if @v_fieldName1Vc='getListBoxTextForService'
	Begin
		select 
		convert(varchar(100), a.itemId) as FieldValue1, 
		convert(varchar(100), a.itemText) as FieldValue2, 
		convert(varchar(100), a.itemValue) as FieldValue3
		from tbl_TenderListDetail a, tbl_TenderListCellDetail b, tbl_TenderTables c
		where a.tenderListId = b.tenderListId and c.tenderTableId = b.tenderTableId and 
		c.tenderFormId = @v_fieldName2Vc and b.columnId = @v_fieldName3Vc and b.location = 1
	End

if @v_fieldName1Vc='getListBoxTextForRE'
	Begin
		select 
		convert(varchar(100), a.itemId) as FieldValue1, 
		convert(varchar(100), a.itemText) as FieldValue2, 
		convert(varchar(100), a.itemValue) as FieldValue3
		from tbl_TenderListDetail a, tbl_TenderListCellDetail b, tbl_TenderTables c, tbl_CMS_SrvFormMap d
		where a.tenderListId = b.tenderListId and c.tenderTableId = b.tenderTableId and d.formId = c.tenderFormId and
		d.tenderId = @v_fieldName2Vc and b.columnId = 2 and b.location = 1 and d.srvboqid = 16
	End	
if @v_fieldName1Vc='CheckForVariOrderSerCase'
Begin		
		If exists (select variOrdId from tbl_CMS_SrvTCVari tc where tc.variOrdId=@v_fieldName2Vc)
		Begin
			select '1' as FieldValue1
		End
		Else IF exists(select variOrdId from tbl_CMS_SrvSSVari ss where ss.variOrdId=@v_fieldName2Vc)
		Begin
			select '1' as FieldValue1
		End
		Else IF exists(select variOrdId from tbl_CMS_SrvsrVari sr  where sr.variOrdId=@v_fieldName2Vc)
		Begin		
			select '1' as FieldValue1
		End
		Else IF exists(select variOrdId from tbl_CMS_SrvreVari re  where re.variOrdId=@v_fieldName2Vc)
				Begin
					select '1' as FieldValue1
		End
				Else IF exists(select variOrdId from tbl_CMS_SrvccVari cc  where cc.variOrdId=@v_fieldName2Vc)
				Begin
					select '1' as FieldValue1
		End
				Else IF exists(select variOrdId from tbl_CMS_SrvWPVari wp  where wp.variOrdId=@v_fieldName2Vc)
				Begin
					select '1' as FieldValue1
		End
				Else IF exists(select variOrdId from tbl_CMS_SrvPSVari ps  where ps.variOrdId=@v_fieldName2Vc)
				Begin
					select '1' as FieldValue1
		End
		Else
		Begin
			select '0' as FieldValue1
		End		
End

if @v_fieldName1Vc='getNOADetailWithTenderId'
	BEGIN
		select convert(varchar(10), a.userId) as FieldValue1, convert(varchar(10), a.noaIssueId) as FieldValue2  from tbl_NoaIssueDetails a, tbl_ContractSign b
		where a.noaIssueId = b.noaId and a.tenderId = @v_fieldName2Vc and a.isRepeatOrder = 'no'
	END

if @v_fieldName1Vc='avoidepW2STD'
	BEGIN
		select convert(varchar(10), tmap.formId) as FieldValue1 
		from tbl_TenderStd tsd,tbl_epwtwobmapping tmap  
		where tsd.tenderId = @v_fieldName2Vc
		and tmap.stdId = tsd.templateId
	
	END
	
if @v_fieldName1Vc='getAwardedUserId'
	BEGIN
	  select convert(varchar(10), na.userId) as FieldValue1 from tbl_NoaIssueDetails nd,tbl_NoaAcceptance na,tbl_ContractSign cs
	  where nd.tenderId = @v_fieldName2Vc and nd.noaIssueId = na.noaIssueId and na.acceptRejStatus = 'approved'
	  and cs.noaId=nd.noaIssueId	
	END
	
if @v_fieldName1Vc='FlushIfErrorInService'
	BEGIN	
		declare @tenderId int, @lotId int
		select @tenderId = @v_fieldName2Vc
		select @lotId = (select appPkgLotId from tbl_TenderLotSecurity where tenderId = @tenderId)
		delete from tbl_CMS_SrvWorkPlan where srvFormMapId in (selecT srvFormMapId from tbl_CMS_SrvFormMap where tenderId = @tenderId) 
		delete from tbl_CMS_SrvPaymentSch where srvFormMapId in (selecT srvFormMapId from tbl_CMS_SrvFormMap where tenderId = @tenderId) 
		delete from tbl_CMS_SrvCnsltComp where srvFormMapId in (selecT srvFormMapId from tbl_CMS_SrvFormMap where tenderId = @tenderId) 
		delete from tbl_CMS_SrvTeamComp where srvFormMapId in (selecT srvFormMapId from tbl_CMS_SrvFormMap where tenderId = @tenderId) 
		delete from tbl_CMS_SrvStaffSch where srvFormMapId in (selecT srvFormMapId from tbl_CMS_SrvFormMap where tenderId = @tenderId)
		delete from tbl_CMS_SrvSalaryRe where srvFormMapId in (selecT srvFormMapId from tbl_CMS_SrvFormMap where tenderId = @tenderId) 
		delete From tbl_CMS_SrvReExpense where srvFormMapId in (selecT srvFormMapId from tbl_CMS_SrvFormMap where tenderId = @tenderId) 
		delete From tbl_CMS_SrvStaffDaysCnt where tenderId = @tenderId
		delete from tbl_CMS_SrvWPHistory where srvFormMapId in (selecT srvFormMapId from tbl_CMS_SrvFormMap where tenderId = @tenderId) 
		delete from tbl_CMS_SrvPSHistory where srvFormMapId in (selecT srvFormMapId from tbl_CMS_SrvFormMap where tenderId = @tenderId)
		delete from tbl_CMS_SrvCCHistory where srvFormMapId in (selecT srvFormMapId from tbl_CMS_SrvFormMap where tenderId = @tenderId)
		delete from tbl_CMS_SrvTCHistory where srvFormMapId in (selecT srvFormMapId from tbl_CMS_SrvFormMap where tenderId = @tenderId) 
		delete from tbl_CMS_SrvSSHistory where srvFormMapId in (selecT srvFormMapId from tbl_CMS_SrvFormMap where tenderId = @tenderId) 
		delete from tbl_CMS_SrvReExHistory where srvFormMapId in (selecT srvFormMapId from tbl_CMS_SrvFormMap where tenderId = @tenderId)
		delete from tbl_CMS_WpMaster where wpLotId = @lotId 
		delete from tbl_CMS_SrvFormMap where tenderId = @tenderId

	END
	
END


GO
