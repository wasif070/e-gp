SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Search App Data By Specified Criteria
--
-- Author: Krishnraj
-- Date: 09-02-2011
--
-- Last Modified: 09-03-2011
-- Modified By: Krishnraj
-- Modification: Remove budgettype and appId column from query and also change in where cond.
--------------------------------------------------------------------------------
-- Exec dbo.p_app_search '2010-2009','122','','0',1,10
-- Exec dbo.p_app_search '','0','0','0',1,10
--------------------------------------------------------------------------------
-- SP Name	:   p_app_search
-- Module	:	APP
-- Funftion	:	N.A.
---------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_app_search]
@v_financialYear_inVc			VARCHAR(10)	=NULL,
@v_departmentId_inN			INT				=NULL,
@v_stateId_inN					INT				=NULL,
@v_officeId_inN					INT				=NULL,
@v_Page_inN						INT				=1,
@v_RecordPerPage_inN			INT				=10
	
AS

BEGIN
	
	SET NOCOUNT ON;

	DECLARE @v_WhereCond_Vc VARCHAR(MAX) = '', @v_SubWhereCond_Vc VARCHAR(100) = '', @v_MainQry_Vc NVARCHAR(MAX), @v_Params_Vc NVARCHAR(100)
	
	-- SET THE PARAMETER
	SET @v_Params_Vc = N'@v_RecordPerPage_inN INT, @v_Page_inN INT'
	
	-- ==================================================
	-- SEARCH financialYear CONDITION
	IF @v_financialYear_inVc <> ''
	BEGIN
			SET @v_SubWhereCond_Vc = 'AND financialYear LIKE ''%' + @v_financialYear_inVc + '%'''
	END
	-- ==================================================	
	-- WHERE COND. IS START HERE
	SET @v_WhereCond_Vc = 'om.officeId in (Select officeId from dbo.tbl_AppMaster Where appStatus = ''Approved'' ' + @v_SubWhereCond_Vc + ')'
	
	-- SEARCH departmentId CONDITION
	IF @v_WhereCond_Vc <> '' AND CONVERT(varchar(10), @v_departmentId_inN) <> '0'
			SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' AND '
			
	IF CONVERT(VARCHAR(10), @v_departmentId_inN) <> '0'
	BEGIN
			SET @v_WhereCond_Vc= @v_WhereCond_Vc + ' dp.departmentId = ' + CHAR(39) + CONVERT(VARCHAR(10), @v_departmentId_inN) + CHAR(39)
	END
	-- ==================================================
	
	-- SEARCH stateId CONDITION
	IF @v_WhereCond_Vc <> '' AND CONVERT(varchar(10), @v_stateId_inN) <> '0'
			SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' AND '
			
	IF  CONVERT(varchar(10), @v_stateId_inN) <> '0'
	BEGIN
			SET @v_WhereCond_Vc= @v_WhereCond_Vc + ' om.stateId = ' + CHAR(39) + CONVERT(varchar(10), @v_stateId_inN) + CHAR(39)
	END
	-- ==================================================
	
	-- SEARCH officeId CONDITION
	IF @v_WhereCond_Vc <> '' AND CONVERT(varchar(10), @v_officeId_inN) <> '0'
			SET @v_WhereCond_Vc = @v_WhereCond_Vc + ' AND '
			
	IF CONVERT(varchar(10), @v_officeId_inN) <> '0'
	BEGIN
			SET @v_WhereCond_Vc= @v_WhereCond_Vc + ' om.officeId = ' + CONVERT(varchar(10), @v_officeId_inN)
	END
	-- ==================================================
	
	SET @v_MainQry_Vc = N'
		;WITH appCTE (Row_ID, Ministry, Division, Oranisation, OfficeName, OfficeId)
		AS
		(
			SELECT ROW_NUMBER() OVER(ORDER BY officeName) AS [Row_ID], 
				Case departmentType 
			When ''Ministry'' Then
				[departmentName]
			When ''Autonomus'' Then
				[departmentName]
			When ''District'' Then
				[departmentName]
			When ''SubDistrict'' Then
				[departmentName]
			When ''Gewog'' Then
				[departmentName]
			When ''Division'' Then
				(select [departmentName] from dbo.tbl_DepartmentMaster where departmentId = dp.parentDepartmentId)
			When ''Organization'' Then
				Case When ((Select departmentType from tbl_DepartmentMaster where departmentId = dp.parentDepartmentId) = ''Ministry'') Then 
					(select departmentName from tbl_DepartmentMaster where departmentId = dp.parentDepartmentId)
				else
					(select departmentName from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=dp.parentDepartmentId))
				end
			End as ''Ministry'',
	Case departmentType 
			When ''Ministry'' Then
				''-''
			When ''Division'' Then
				[departmentName]			
			When ''Organization'' Then
				Case When ((Select departmentType from tbl_DepartmentMaster where departmentId = dp.parentDepartmentId) = ''Ministry'') Then 
					'' - ''
				else
					(select [departmentName] from dbo.tbl_DepartmentMaster where departmentId = dp.parentDepartmentId)
				end
			End as ''Division'',
	Case departmentType 
			When ''Ministry'' Then
				''-''
			When ''Division'' Then
				''-''
			When ''Organization'' Then
				[departmentName]			
			End as ''Organization'', 
			officeName, Convert(Varchar(10), om.officeId) as officeId FROM  
		dbo.tbl_DepartmentMaster dp INNER JOIN dbo.tbl_OfficeMaster om ON om.departmentId = dp.departmentId  
		WHERE ' + @v_WhereCond_Vc + '
		),
		CTE_Total As (Select COUNT(*) AS TotalRowCount from appCTE)
		SELECT c.Row_ID, c.Ministry, c.Division, c.Oranisation, c.OfficeName, c.OfficeId, 
		CONVERT(VARCHAR(30), t.TotalRowCount) as TotalRowCount, CONVERT(VARCHAR(30), CEILING(t.TotalRowCount / @v_RecordPerPage_inN)) as TotalPages 
		FROM appCTE c Cross Join CTE_Total t
		WHERE c.Row_ID >= ((@v_RecordPerPage_inN * @v_Page_inN) - (@v_RecordPerPage_inN - @v_Page_inN) - (@v_Page_inN - 1)) AND 
		c.Row_ID <= @v_RecordPerPage_inN * @v_Page_inN  
		Order By c.Row_ID ASC' 
		
	/*
	SET @v_MainQry_Vc = N'
		;WITH appCTE (Row_ID, Ministry, Division, Oranisation, OfficeName, BudgetType, OfficeId, AppId)
		AS
		(
			SELECT ROW_NUMBER() OVER(ORDER BY appId DESC) AS [Row_ID], 
				Case departmentType 
			When ''Ministry'' Then
				[departmentName]
			When ''Division'' Then
				(select [departmentName] from dbo.tbl_DepartmentMaster where departmentId = dp.parentDepartmentId)
			When ''Organization'' Then
				Case When ((Select departmentType from tbl_DepartmentMaster where departmentId = dp.parentDepartmentId) = ''Ministry'') Then 
					(select departmentName from tbl_DepartmentMaster where departmentId = dp.parentDepartmentId)
				else
					(select departmentName from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=dp.parentDepartmentId))
				end
			End as ''Ministry'',
	Case departmentType 
			When ''Ministry'' Then
				''-''
			When ''Division'' Then
				[departmentName]			
			When ''Organization'' Then
				Case When ((Select departmentType from tbl_DepartmentMaster where departmentId = dp.parentDepartmentId) = ''Ministry'') Then 
					'' - ''
				else
					(select [departmentName] from dbo.tbl_DepartmentMaster where departmentId = dp.parentDepartmentId)
				end
			End as ''Division'',
	Case departmentType 
			When ''Ministry'' Then
				''-''
			When ''Division'' Then
				''-''
			When ''Organization'' Then
				[departmentName]			
			End as ''Organization'', 
			officeName, Convert(Varchar(10), budgetType) as budgetType, Convert(Varchar(10), ap.officeId) as officeId, Convert(Varchar(10), ap.appId) as appId FROM  
		dbo.tbl_AppMaster ap INNER JOIN
		dbo.tbl_DepartmentMaster dp ON ap.departmentId = dp.departmentId INNER JOIN
        dbo.tbl_OfficeMaster om ON ap.officeId = om.officeId 
		WHERE ' + @v_WhereCond_Vc + '
		),
		CTE_Total As (Select COUNT(*) AS TotalRowCount from appCTE)
		SELECT c.Row_ID, c.Ministry, c.Division, c.Oranisation, c.OfficeName, c.BudgetType, c.OfficeId, c.AppId, 
		CONVERT(VARCHAR(30), t.TotalRowCount) as TotalRowCount, CONVERT(VARCHAR(30), CEILING(t.TotalRowCount / @v_RecordPerPage_inN)) as TotalPages 
		FROM appCTE c Cross Join CTE_Total t
		WHERE c.Row_ID >= ((@v_RecordPerPage_inN * @v_Page_inN) - (@v_RecordPerPage_inN - @v_Page_inN) - (@v_Page_inN - 1)) AND 
		c.Row_ID <= @v_RecordPerPage_inN * @v_Page_inN  
		Order By c.Row_ID ASC' 
	*/
	
	PRINT @v_MainQry_Vc   -- AND departmentType in (''Ministry'', ''Division'', ''Organization'') 
	EXEC sp_executesql @v_MainQry_Vc, @v_Params_Vc, @v_RecordPerPage_inN = @v_RecordPerPage_inN, @v_Page_inN = @v_Page_inN
    
END


GO
