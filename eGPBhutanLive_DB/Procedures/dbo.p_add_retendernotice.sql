SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Procedure for Re-Tender Process
--
--
-- Author: Karan
-- Date: 08-12-2010
--
-- Last Modified:
-- Modified By:
-- Date:
-- Modification:
--------------------------------------------------------------------------------
-- SP Name	   :   p_add_retendernotice
-- Module	   :   Tender Notice(PE User)
-- Description :   If the current tender got failed due to applicable reasons
--				   than PE User go for Re-Tendering and a fresh Tender is created
--                 which is copy of the current Tender
-- Function	   :   N.A.
CREATE PROCEDURE [dbo].[p_add_retendernotice]
    @v_Action_inVc varchar(50),
    @v_Tenderid_inInt int = 0,
    @v_EventType_inVc varchar(100),
    @v_userIds_inVc varchar(100),-- For REOI Mapping of 7 tenderer case
    @v_loginUserName varchar(200) -- For RE-TENDER Current logged in user PE Name
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @v_flag_bit bit,
            @v_NuTenderId_Int int,
            @v_reoiTenderId_Int int,
            @v_ProcurementMethod_Vc varchar(20),
            @v_ProcurementMethodId_tInt tinyint,
            @v_ReTenderId_Int int=0,
            @v_PQTenderId_Int int =0,
            @v_selectedREOIUserIds_Int int = 0,
            @v_query nvarchar(max),
            @v_tpename varchar(200),
            @countrecord int;


---- Block added for fetching actual PE user -- BY tarun for fixing officer  transfer issue while re-tendering
--    SELECT	@countrecord = COUNT(*)
--    FROM	tbl_EmployeeTrasfer
--    WHERE	replacedBy = (	SELECT	pename
--							FROM	tbl_tenderdetails
--							WHERE	tenderid = @v_Tenderid_inInt)
--			AND isCurrent = 'yes'

--    IF @countrecord = 0
--    BEGIN
--        SET @v_tpename = @v_loginUserName
--    END
--    ELSE
--    BEGIN
--        SELECT	@v_tpename =  employeename
--		FROM	tbl_EmployeeTrasfer
--		WHERE	replacedBy = (	SELECT	pename
--								FROM	tbl_tenderdetails
--								WHERE	tenderid = @v_Tenderid_inInt)
--				AND isCurrent = 'yes'
--    END
----Block completed
--Dohatec Start
SELECT	@countrecord = COUNT(*)
    FROM	tbl_EmployeeTrasfer
    WHERE	employeeId = (	SELECT	employeeId
							FROM	tbl_EmployeeMaster
							WHERE	userId =@v_userIds_inVc )
			AND isCurrent = 'yes' and action='Transfer'	
			AND govUserId = (select MAX(govUserId) from tbl_EmployeeTrasfer WHERE	userId =@v_userIds_inVc) 		

    IF @countrecord = 0
    BEGIN
        SET @v_tpename = @v_loginUserName
    END
    ELSE
    BEGIN
        SELECT	@v_tpename =  employeename
		FROM	tbl_EmployeeTrasfer
		WHERE	employeeId = (	SELECT	employeeId
							FROM	tbl_EmployeeMaster
							WHERE	userId =@v_userIds_inVc )
			AND isCurrent = 'yes' and action='Transfer'		
			AND govUserId = (select MAX(govUserId) from tbl_EmployeeTrasfer WHERE	userId =@v_userIds_inVc)
    END
--Dohatec End
	IF @v_Action_inVc='INSERT'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
				/* START CODE: TO INSERT INTO TABLE - tbl_TenderMaster */
				INSERT INTO [dbo].[tbl_TenderMaster]
						([appId],[packageId],[createdBy],[creationDate],[financialYear],[tenderType])
				SELECT [appId],[packageId],[createdBy],[creationDate],[financialYear],[tenderType]
					FROM tbl_TenderMaster WHERE tenderId=@v_Tenderid_inInt
				Set @v_NuTenderId_Int=IDENT_CURRENT('dbo.tbl_TenderMaster')
                                if @v_EventType_inVc='Rejected/Re-Tendering'
				begin
					set @v_ReTenderId_Int=@v_Tenderid_inInt
				end
				else
				begin
                                    set @v_PQTenderId_Int= @v_Tenderid_inInt
                                    --set @v_ReTenderId_Int=@v_Tenderid_inInt
				end

				set @v_reoiTenderId_Int=@v_Tenderid_inInt

				/* END CODE: TO INSERT INTO TABLE - tbl_TenderMaster */



				/* Start Code: To Insert into table - tbl_BidConfirmation */

				IF @v_NuTenderId_Int is Not Null and @v_EventType_inVc!='Rejected/Re-Tendering'
				Begin
                                    if((select procurementNatureId from tbl_tenderdetails where tenderId=@v_Tenderid_inInt)=3)
                                    begin
                                        /*Insert into tbl_LimitedTenders
                                        (
                                                tenderId,
                                                userId,
                                                mappedDate,
                                                pkgLotId
                                        )
                                        select	distinct @v_NuTenderId_Int,
                                                        userId,
                                                        GETDATE(),
                                                        pkgLotId
                                        from	tbl_EvalBidderStatus
                                        where	tenderId=@v_Tenderid_inInt
                                                        and (bidderStatus='Technically Responsive'or result='pass')
                                                        and   pkgLotId not in(	select	pkgLotId
                                                                                                        from	tbl_EvalRptSentToAA
                                                                                                        where	tenderId=@v_Tenderid_inInt
                                                                                                        and		rptStatus='Rejected/Re-Tendering'
                                                                                                )
								and userId IN (CASE WHEN @v_userIds_inVc != '' THEN @v_userIds_inVc ELSE userId END)*/
						set @v_query ='Insert into tbl_LimitedTenders ( tenderId, userId, mappedDate, pkgLotId)
									   select distinct '+convert(nvarchar, @v_NuTenderId_Int)+', userId, GETDATE(), pkgLotId
									   from	tbl_EvalBidderStatus
									   where tenderId='+convert(nvarchar, @v_Tenderid_inInt)+' and (bidderStatus=''Technically Responsive''or result=''pass'') and
											 pkgLotId not in (select pkgLotId  from	tbl_EvalRptSentToAA
															  where tenderId='+convert(nvarchar, @v_Tenderid_inInt)+' and rptStatus=''Rejected/Re-Tendering'')';
						if (@v_userIds_inVc != '')
						begin
							set @v_query =@v_query+' and userId IN ('+ @v_userIds_inVc+')';
						End
						execute sp_executesql @v_query;
					end
					else
					begin
						Insert into tbl_LimitedTenders (tenderId, userId, mappedDate,pkgLotId)
					    select distinct @v_NuTenderId_Int,userId, GETDATE() ,case when pkgLotId=0 then (select appPkgLotId from tbl_TenderLotSecurity where tenderId=@v_Tenderid_inInt ) else pkgLotId end
						from tbl_EvalBidderStatus where tenderId=@v_Tenderid_inInt and (bidderStatus='Technically Responsive'or result='pass')
						and   pkgLotId not in(select pkgLotId  from tbl_EvalRptSentToAA where tenderId=@v_Tenderid_inInt
					    and rptStatus='Rejected/Re-Tendering')
					end
				End
				else
				begin
							Insert into tbl_LimitedTenders (tenderId, userId, mappedDate,pkgLotId) select @v_NuTenderId_Int, userId, GETDATE(),pkgLotId from tbl_LimitedTenders where   tenderId=@v_Tenderid_inInt
				end
				/* End Code: To Insert into table - tbl_BidConfirmation */

				/* START CODE: TO INSERT INTO TABLE - tbl_TenderLots */
				if @v_EventType_inVc!='Rejected/Re-Tendering'
				begin
				if((select procurementNature from tbl_TenderDetails where tenderId=@v_Tenderid_inInt)!='Services')
						begin
				INSERT INTO [dbo].[tbl_TenderLots]
						([tenderId],[appPkgLotId])
				SELECT 	@v_NuTenderId_Int, appPkgLotId
					FROM tbl_TenderLots WHERE tenderId=@v_Tenderid_inInt
					and appPkgLotId not in(select pkgLotId  from tbl_EvalRptSentToAA where tenderId=@v_Tenderid_inInt
					 and rptStatus='Rejected/Re-Tendering')
					 end
					 else
					 begin
					 INSERT INTO [dbo].[tbl_TenderLots]
						([tenderId],[appPkgLotId])
				SELECT 	@v_NuTenderId_Int, appPkgLotId
					FROM tbl_TenderLots WHERE tenderId=@v_Tenderid_inInt
					and tenderId not in(select tenderId  from tbl_EvalRptSentToAA where tenderId=@v_Tenderid_inInt
					 and rptStatus='Rejected/Re-Tendering')
					 end
				end
				else
				begin
						if((select procurementNature from tbl_TenderDetails where tenderId=@v_Tenderid_inInt)!='Services')
						begin
						if ((select count(pkgLotId)  from tbl_EvalRptSentToAA where tenderId=@v_Tenderid_inInt
							 and rptStatus='Rejected/Re-Tendering')>=1)
							 begin
						INSERT INTO [dbo].[tbl_TenderLots]
								([tenderId],[appPkgLotId])
						SELECT 	@v_NuTenderId_Int, appPkgLotId
							FROM tbl_TenderLots WHERE tenderId=@v_Tenderid_inInt
							and   appPkgLotId   in(select pkgLotId  from tbl_EvalRptSentToAA where tenderId=@v_Tenderid_inInt
							 and rptStatus='Rejected/Re-Tendering')
							 end
							 else
							 begin
							 INSERT INTO [dbo].[tbl_TenderLots]
								([tenderId],[appPkgLotId])
						SELECT 	@v_NuTenderId_Int, appPkgLotId
							FROM tbl_TenderLots WHERE tenderId=@v_Tenderid_inInt
							 end
						end
						else
						begin
						if ((select count(pkgLotId)  from tbl_EvalRptSentToAA where tenderId=@v_Tenderid_inInt
							 and rptStatus='Rejected/Re-Tendering')>=1)
						begin
							INSERT INTO [dbo].[tbl_TenderLots]
								([tenderId],[appPkgLotId])
						SELECT 	@v_NuTenderId_Int, appPkgLotId
							FROM tbl_TenderLots WHERE tenderId=@v_Tenderid_inInt
							and   tenderId   in(select tenderId  from tbl_EvalRptSentToAA where tenderId=@v_Tenderid_inInt
							 and rptStatus='Rejected/Re-Tendering')
						end
						else
						begin
							INSERT INTO [dbo].[tbl_TenderLots]
								([tenderId],[appPkgLotId])
						SELECT 	@v_NuTenderId_Int, appPkgLotId
							FROM tbl_TenderLots WHERE tenderId=@v_Tenderid_inInt

						end
						end

				end
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderLots */


				/* START CODE: TO SET Procurement Method and Id */
				--If @v_EventType_inVc='PQTENDER'
				If @v_EventType_inVc='PQ'
				Begin
					Select @v_ProcurementMethod_Vc=procurementMethod,
						   @v_ProcurementMethodId_tInt=procurementMethodId
					from tbl_ProcurementMethod Where procurementMethod='OTM'
					Select @v_EventType_inVc='2 Stage-PQ'
				End
				Else
				Begin
					If @v_EventType_inVc='REOI'
					Begin
						Select @v_EventType_inVc='RFP'
					End
					Else If @v_EventType_inVc='1 stage-TSTM'
					Begin
						Select @v_EventType_inVc='2 stage-TSTM'
					End
					Select @v_ProcurementMethod_Vc=procurementMethod,
						   @v_ProcurementMethodId_tInt=procurementMethodId
					from tbl_TenderDetails Where tenderId=@v_Tenderid_inInt
				End

				select @v_EventType_inVc= case @v_EventType_inVc when 'Rejected/Re-Tendering' THEN
				eventtype else @v_EventType_inVc end from tbl_TenderDetails Where tenderId=@v_Tenderid_inInt
				/* END CODE: TO SET Procurement Method and Id */

				/* START CODE: TO INSERT INTO TABLE - tbl_TenderDetails */
				INSERT INTO [dbo].[tbl_TenderDetails]
				(
				[departmentId],
				[tenderId],
				[ministry],
				[division],
				[agency],
				[peOfficeName],
				[peCode],
				[peDistrict],
				[eventType],
				[invitationFor],
				[reoiRfpFor],
				[contractType],
				[reoiRfpRefNo],
				[procurementMethod],
				[budgetType],
				[sourceOfFund],
				[devPartners],
				[projectCode],
				[projectName],
				[packageNo],
				[packageDescription],
				[cpvCode],
				[docEndDate],
				[preBidStartDt],
				[preBidEndDt],
				[submissionDt],
				[openingDt],
				[eligibilityCriteria],
				[tenderBrief],
				[deliverables],
				[otherDetails],
				[foreignFirm],
				[docAvlMethod],
				[evalType],
				[docFeesMethod],
				[docFeesMode],
				[docOfficeAdd],
				[securityLastDt],
				[securitySubOff],
				[procurementNature],
				[procurementType],
				[modeOfTender],
				[reTenderId],
				[pqTenderId],
				[reoiTenderId],
				[tenderStatus],
				[tenderPubDt],
				[peName],
				[peDesignation],
				[peAddress],
				[peContactDetails],
				[estCost],
				[approvingAuthId],
				[stdTemplateId],
				[docAccess],
				[tenderValDays],
				[tenderValidityDt],
				[tenderSecurityDays],
				[tenderSecurityDt],
				[procurementNatureId],
				[procurementMethodId],
				[officeId],
				[budgetTypeId],
				[workflowStatus],
				[pkgDocFees],
				pkgUrgency,
				servicesType
				)
				SELECT	[departmentId],
					@v_NuTenderId_Int,
					[ministry],
					[division],
					[agency],
					[peOfficeName],
					[peCode],
					[peDistrict],
					@v_EventType_inVc,
					[invitationFor],
					[reoiRfpFor],
					[contractType],
					[reoiRfpRefNo],
					@v_ProcurementMethod_Vc,
					[budgetType],
					[sourceOfFund],
					[devPartners],
					[projectCode],
					[projectName],
					[packageNo],
					[packageDescription],
					[cpvCode],
					null,
					null,
					null,
					null,
					null,
					eligibilityCriteria,
					tenderBrief,
					[deliverables],
				[otherDetails],
					[foreignFirm],
					[docAvlMethod],
					[evalType],
					[docFeesMethod],
					[docFeesMode],
					[docOfficeAdd],
					'',
					[securitySubOff],
					[procurementNature],
					[procurementType],
					[modeOfTender],
					@v_ReTenderId_Int,
					 @v_PQTenderId_Int ,
					@v_reoiTenderId_Int,
					'Pending',
					null,
                                        @v_tpename,
					[peDesignation],
					[peAddress],
					[peContactDetails],
					[estCost],
					'',
					'',
					'Restricted',
					'',
					'',
					'',
					'',
					[procurementNatureId],
					@v_ProcurementMethodId_tInt,
					[officeId],
					[budgetTypeId],
					'Pending',
					[pkgDocFees],
					pkgUrgency,
					servicesType
				FROM	tbl_TenderDetails
				WHERE	tenderId=@v_Tenderid_inInt
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderDetails */


				/* START CODE: TO INSERT INTO TABLE - tbl_TenderLotSecurity */
				if @v_EventType_inVc='Rejected/Re-Tendering'
				begin
				if((select procurementNature from tbl_TenderDetails where tenderId=@v_Tenderid_inInt)!='Services')
						begin
				INSERT INTO [dbo].[tbl_TenderLotSecurity]
					   ([tenderId],[appPkgLotId],[lotNo],[lotDesc],[location],[docFess],[tenderSecurityAmt],[completionTime])
				SELECT @v_NuTenderId_Int,[appPkgLotId],[lotNo],[lotDesc],[location],[docFess],[tenderSecurityAmt],[completionTime]
					FROM tbl_TenderLotSecurity WHERE tenderId=@v_Tenderid_inInt
					and   appPkgLotId in(select pkgLotId  from tbl_EvalRptSentToAA where tenderId=@v_Tenderid_inInt
					 and rptStatus='Rejected/Re-Tendering')
					end
					else
					begin
					INSERT INTO [dbo].[tbl_TenderLotSecurity]
					   ([tenderId],[appPkgLotId],[lotNo],[lotDesc],[location],[docFess],[tenderSecurityAmt],[completionTime])
				SELECT @v_NuTenderId_Int,[appPkgLotId],[lotNo],[lotDesc],[location],[docFess],[tenderSecurityAmt],[completionTime]
					FROM tbl_TenderLotSecurity WHERE tenderId=@v_Tenderid_inInt
					and   tenderId in(select tenderId  from tbl_EvalRptSentToAA where tenderId=@v_Tenderid_inInt
					 and rptStatus='Rejected/Re-Tendering')

					end
				end
				else
				begin

				if((select procurementNature from tbl_TenderDetails where tenderId=@v_Tenderid_inInt)!='Services')
						begin
				INSERT INTO [dbo].[tbl_TenderLotSecurity]
					   ([tenderId],[appPkgLotId],[lotNo],[lotDesc],[location],[docFess],[tenderSecurityAmt],[completionTime])
				SELECT @v_NuTenderId_Int,[appPkgLotId],[lotNo],[lotDesc],[location],[docFess],0.00,[completionTime]
					FROM tbl_TenderLotSecurity WHERE tenderId=@v_Tenderid_inInt
					and   appPkgLotId not in(select pkgLotId  from tbl_EvalRptSentToAA where tenderId=@v_Tenderid_inInt
					 and rptStatus='Rejected/Re-Tendering')
				end
				else
				begin
					INSERT INTO [dbo].[tbl_TenderLotSecurity]
					   ([tenderId],[appPkgLotId],[lotNo],[lotDesc],[location],[docFess],[tenderSecurityAmt],[completionTime])
				SELECT @v_NuTenderId_Int,[appPkgLotId],[lotNo],[lotDesc],[location],[docFess],0.00,[completionTime]
					FROM tbl_TenderLotSecurity WHERE tenderId=@v_Tenderid_inInt
					and   tenderId not in(select tenderId  from tbl_EvalRptSentToAA where tenderId=@v_Tenderid_inInt
					 and rptStatus='Rejected/Re-Tendering')
				end
				end

				/* END CODE: TO INSERT INTO TABLE - tbl_TenderLotSecurity */

				/* START CODE: TO INSERT INTO TABLE - tbl_TenderPhasing */
				INSERT INTO [dbo].[tbl_TenderPhasing]
					   ([tenderId],[phasingRefNo],[phasingOfService],[location],[indStartDt],[indEndDt])
				SELECT @v_NuTenderId_Int,[phasingRefNo],[phasingOfService],[location],[indStartDt],[indEndDt]
					FROM tbl_TenderPhasing WHERE tenderId=@v_Tenderid_inInt
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderPhasing */

				Set @v_flag_bit=1
				Select @v_NuTenderId_Int as Id,@v_flag_bit as flag, 'Record inserted.' as Msg
			COMMIT TRAN
		END TRY
	BEGIN CATCH
		BEGIN
			Set @v_flag_bit=0
			Select 0 as Id, @v_flag_bit as flag, ERROR_MESSAGE() as Msg
			ROLLBACK TRAN
		END
	END CATCH
	END

    SET NOCOUNT OFF;
END


GO
