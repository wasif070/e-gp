SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteCorrigendumOffline]
--@corrigendumID int
@tenderOLID int
AS
DECLARE @flag_bit bit
	Begin
		DELETE FROM tbl_CorrigendumDetailOffline
		WHERE tenderOfflineId = @tenderOLID and CorrigendumStatus = 'Pending'
		Set @flag_bit=1
		Select @flag_bit as flag, 'Corrigendum has been deleted.' as Message
	End


GO
