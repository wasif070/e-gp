SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Dipal Shah>
-- Create date: <21/12/2011>

-- SP Name: [p_dump_neg_forms_data]
-- Module: Negotiation
-- Function:	Dump tenderer bid form data which are not in negotiation, into negotiation tables.
-- =============================================
CREATE PROCEDURE [dbo].[p_dump_neg_forms_data]
	-- INPUT VARIABLES
	@v_fieldName1Vc varchar(500)=NULL,		  -- Tender Id 
	@v_fieldName2Vc varchar(500)=NULL,		  -- User Id
	@v_fieldName3Vc varchar(500)=NULL,		  -- Negotiation Id	
	@v_fieldName4Vc varchar(500)=NULL,
	@v_fieldName5Vc varchar(500)=NULL,
	@v_fieldName6Vc varchar(500)=NULL,
	@v_fieldName7Vc varchar(500)=NULL,
	@v_fieldName8Vc varchar(500)=NULL,
	@v_fieldName9Vc varchar(500)=NULL,
	@v_fieldName10Vc varchar(500)=NULL
AS
BEGIN
	
	SET NOCOUNT ON;

	-- LOCAL VARIABLES
	Declare @v_bidId int,@v_tenderFormId int,@v_negBidFormId int,@v_tenderTableId int,@v_negBidTableId int,@v_bidTableId int
	Declare @v_bidAgree varchar(100) = NULL
	BEGIN TRAN
		BEGIN TRY
		
			select @v_bidAgree = bidAggree from tbl_NegNotifyTenderer where negId=@v_fieldName3Vc
			
			IF (@v_bidAgree = 'Yes')  
				BEGIN
					insert into tbl_NegBidForm
						(negId,bidId,tenderFormId,userId,submissionDt)
					select @v_fieldName3Vc,bidId,tenderFormId,@v_fieldName2Vc,GETDATE() from tbl_TenderBidForm where tenderId = @v_fieldName1Vc and userId = @v_fieldName2Vc
						and tenderFormId not in 
						(select formId from tbl_NegotiationForms where negId = @v_fieldName3Vc)
						and tenderFormId not in
						( select tenderFormId from tbl_NegBidForm where negId = @v_fieldName3Vc )
						
					DECLARE cur1 cursor For  -- Outer Cursor 
					select bidId,tenderFormId,negBidFormId from tbl_NegBidForm
					where tenderFormId in 
					(
						select tenderFormId from tbl_TenderBidForm where tenderId = @v_fieldName1Vc and userId = @v_fieldName2Vc
						and tenderFormId not in 
						(
							select formId from tbl_NegotiationForms where negId = @v_fieldName3Vc
						)
					)
					and bidId in
					(
						select bidId from tbl_TenderBidForm where tenderId = @v_fieldName1Vc and userId = @v_fieldName2Vc
							and tenderFormId not in 
							(
								select formId from tbl_NegotiationForms where negId = @v_fieldName3Vc
							)
					)	
										
				END
			ELSE
				BEGIN
					delete from tbl_NegBidForm where negId=@v_fieldName3Vc and userId=@v_fieldName2Vc
					
					insert into tbl_NegBidForm
						(negId,bidId,tenderFormId,userId,submissionDt)
					select @v_fieldName3Vc,bidId,tenderFormId,@v_fieldName2Vc,GETDATE() 
					from tbl_TenderBidForm 
					where tenderId = @v_fieldName1Vc and userId = @v_fieldName2Vc
					
					DECLARE cur1 cursor For  -- Outer Cursor 
					select bidId,tenderFormId,negBidFormId from tbl_NegBidForm
					where negId=@v_fieldName3Vc and userId=@v_fieldName2Vc
				END						      

		
			OPEN cur1  
			FETCH NEXT FROM cur1 INTO @v_bidId, @v_tenderFormId,@v_negBidFormId  
			WHILE (@@Fetch_status = 0)-- Iterating Outer cursor loop        
			BEGIN   
				    DECLARE cur2 cursor For  -- Inner Cursor 
						select tenderTableId,bidTableId from tbl_TenderBidTable where tenderFormId= @v_tenderFormId and bidId= @v_bidId
					OPEN cur2  
					FETCH NEXT FROM cur2 INTO  @v_tenderTableId,@v_bidTableId
					WHILE (@@Fetch_status = 0) -- Iterating Inner cursor loop   
					BEGIN   
						insert into tbl_NegBidTable
						(bidId,tenderFormId,tenderTableId,submissionDt,negBidFormId)
						values
						(@v_bidId,@v_tenderFormId,@v_tenderTableId,GETDATE(),@v_negBidFormId);
						
						select @v_negBidTableId = SCOPE_IDENTITY();
						
						insert into tbl_NegBidDtlSrv
						( negBidTableId,tenderColumnId,tenderTableId,cellValue,rowId,cellId,formId,negId)
						select @v_negBidTableId,b.columnId,a.tenderTableId,a.cellValue,a.rowId,a.tenderCelId,a.tenderFormId,@v_fieldName3Vc
						from tbl_TenderBidPlainData a
						inner join tbl_TenderBidDetail b on (a.bidTableId = b.bidTableId and a.tenderTableId=b.tenderTableId and a.rowId=b.rowId and a.tenderCelId = b.cellId) 
						where a.bidTableId= @v_bidTableId
						
					FETCH NEXT FROM cur2 INTO @v_tenderTableId,@v_bidTableId
					End -- Inner cursor loop   
					CLOSE cur2        
					DEALLOCATE cur2
					
			FETCH NEXT FROM cur1 INTO @v_bidId, @v_tenderFormId ,@v_negBidFormId  
			End -- Outer cursor loop  
			CLOSE cur1        
			DEALLOCATE cur1		
			COMMIT  TRAN
			select 'true' as FieldValue1
		END TRY 
		BEGIN CATCH
				BEGIN
					ROLLBACK TRAN
					select 'false' FieldValue1
				END
		END CATCH	
		
END


GO
