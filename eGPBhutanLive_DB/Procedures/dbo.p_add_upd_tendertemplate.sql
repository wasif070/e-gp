SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Inserts/Updates tables related to tender template data.
--
--
-- Author: Karan
-- Date: 14-11-2010
--
-- Last Modified:
-- Modified By: Karan
-- Date: 08-12-2010
-- Modification: Code added to insert into table tbl_TenderEnvelopes


-- SP Name: [p_add_upd_tendertemplate]
-- Module: Tender
-- Function: Store Procedure is use for perform insert operation for Tender Template Details.
--			 It will dump STD Template data into Tender Template data into following tables:
--			[tbl_TenderStd], tbl_TenderSection, [tbl_TenderSectionDocs], [tbl_TenderForms],
--			[tbl_TenderMandatoryDoc], [tbl_TenderEnvelopes], [tbl_TenderListBox], [tbl_TenderListDetail],
--			[tbl_TenderTables], [tbl_TenderListCellDetail], [tbl_TenderColumns], [tbl_TenderCells],
--			[tbl_TenderFormula], [tbl_TenderIttHeader], [tbl_TenderIttClause], [tbl_TenderIttSubClause],
--			[tbl_TenderTdsSubClause]


--------------------------------------------------------------------------------
-- INSERT -> Package:	This is use to perfrom insert operation for tender where tender is Package wise.
--						It will perform insert operation into
-- INSERT -> Lot:		This is use to perfrom insert operation for tender where tender is Lot wise.
--------------------------------------------------------------------------------

/*

EXEC p_add_upd_tendertemplate
@v_Action_inVc='INSERT',
@v_TenderId_inInt=20,
@v_TemplateId_inInt=33,
@v_CreatedBy_inInt=1

*/
CREATE PROCEDURE [dbo].[p_add_upd_tendertemplate]
@v_Action_inVc varchar(50),
@v_TenderId_inInt int,
@v_TemplateId_inInt int,
@v_CreatedBy_inInt int

AS

BEGIN
SET NOCOUNT ON;

DECLARE @v_flag_bit bit,
@v_PackageId_Int int,
@v_DocAvlMethod_Vc varchar(20),
@v_TenderStdId_Int int,
@v_TenderLotId_Int int,
@v_EventType_Vc varchar(100),
@v_DerivedTenderID varchar(50)

Select @v_PackageId_Int=TM.packageId, @v_DocAvlMethod_Vc=docAvlMethod, @v_EventType_Vc=eventType
from tbl_TenderMaster TM
Inner Join tbl_TenderDetails TD On TM.tenderId=TD.tenderId
where TM.tenderId=@v_TenderId_inInt

IF @v_Action_inVc='INSERT'
BEGIN
	BEGIN TRY
		BEGIN TRAN
			IF @v_DocAvlMethod_Vc='Package'
			BEGIN
				/* START CODE: TO INSERT INTO TABLE - tbl_TenderStd */
				INSERT INTO [dbo].[tbl_TenderStd]
						([templateId],[templateName],[noOfSections],[status],[tenderId],[packageLotId],[createdBy],procType)
				SELECT templateId, templateName, noOfSections, status, @v_TenderId_inInt, @v_PackageId_Int, @v_CreatedBy_inInt,procType
					FROM tbl_TemplateMaster WHERE templateId=@v_TemplateId_inInt
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderStd */

				Set @v_TenderStdId_Int=IDENT_CURRENT('dbo.tbl_TenderStd')

				--print(@v_TenderStdId_Int)

				/* START CODE: TO INSERT INTO TABLE - tbl_TenderSection */
				INSERT INTO [dbo].[tbl_TenderSection]
						([templateSectionId],[tenderStdId],[sectionName],[contentType],[isSubSection])
				SELECT sectionId, tenderStdId, sectionName, contentType, isSubSection
				FROM tbl_TemplateSections TS INNER JOIN tbl_TenderStd S ON TS.templateId=S.templateId
				WHERE TS.templateId=@v_TemplateId_inInt And S.tenderStdId=@v_TenderStdId_Int
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderSection */

				--/* START CODE: TO INSERT INTO TABLE - tbl_TenderSectionDocs */
				INSERT INTO [dbo].[tbl_TenderSectionDocs]
					([description],[docName],[docSize],[tenderSectionId],[templateSectionDocId],[status],[tenderId])
				SELECT description, docName, docSize, tenderSectionId, TS.templateSectionId, status, @v_TenderId_inInt
				FROM tbl_TemplateSectionDocs TSD INNER JOIN tbl_TenderSection TS ON TSD.sectionId=TS.templateSectionId
				WHERE TSD.templateId=@v_TemplateId_inInt And TS.tenderStdId=@v_TenderStdId_Int
				Order by TS.templateSectionId
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderSectionDocs */

				/* START CODE: TO INSERT INTO TABLE - tbl_TenderForms */
				-- // Code To Insert Technical Forms
				INSERT INTO [dbo].[tbl_TenderForms]
					  ([tenderSectionId],[templateFormId],[filledBy],[formName],[formHeader],[formFooter],[noOfTables],
							[isMultipleFilling],[isEncryption],[isPriceBid],isMandatory,pkgLotId)
				SELECT tenderSectionId, formId, filledBy,formName, formHeader, formFooter, noOfTables,
							isMultipleFilling, isEncryption, isPriceBid,isMandatory,0
				FROM tbl_TemplateSectionForm TSF INNER JOIN tbl_TenderSection TS ON TSF.sectionId=TS.templateSectionId
				WHERE TSF.templateId=@v_TemplateId_inInt And TS.tenderStdId=@v_TenderStdId_Int
				And isPriceBid='No'
				Order by TS.templateSectionId


				-- // Code To Insert Price Bid Forms
				DECLARE cur_Form cursor local FAST_FORWARD For
					SELECT DISTINCT appPkgLotId FROM dbo.tbl_TenderLots WHERE tenderId=@v_TenderId_inInt
				OPEN cur_Form
					FETCH NEXT FROM cur_Form INTO @v_TenderLotId_Int

					WHILE @@Fetch_status = 0
					BEGIN
						INSERT INTO [dbo].[tbl_TenderForms]
							  ([tenderSectionId],[templateFormId],[filledBy],[formName],[formHeader],[formFooter],[noOfTables],
									[isMultipleFilling],[isEncryption],[isPriceBid],isMandatory, pkgLotId,FormType)
						SELECT tenderSectionId, formId, filledBy,formName, formHeader, formFooter, noOfTables,
									isMultipleFilling, isEncryption, isPriceBid,isMandatory,@v_TenderLotId_Int,FormType
						FROM tbl_TemplateSectionForm TSF INNER JOIN tbl_TenderSection TS ON TSF.sectionId=TS.templateSectionId
						WHERE TSF.templateId=@v_TemplateId_inInt And TS.tenderStdId=@v_TenderStdId_Int
						And isPriceBid='Yes'
						Order by TS.templateSectionId

					FETCH NEXT FROM cur_Form INTO @v_TenderLotId_Int
					END

				CLOSE cur_Form
				DEALLOCATE cur_Form

				/* END CODE: TO INSERT INTO TABLE - tbl_TenderForms */

				/* START CODE: TO INSERT INTO TABLE - tbl_TenderMandatoryDoc] */
				INSERT INTO [dbo].[tbl_TenderMandatoryDoc]
						([tenderId],[tenderFormId],[templateFormId],[documentName],[createdBy],[createdDate])
				SELECT  @v_TenderId_inInt, TF.tenderFormId, TMD.templateFormId, documentName, @v_CreatedBy_inInt, GETDATE()
				From tbl_TemplateMandatoryDoc TMD
				INNER JOIN tbl_TenderForms TF ON TMD.templateFormId=TF.templateFormId
				INNER JOIN tbl_TenderSection TS ON TF.tenderSectionId=TS.tenderSectionId
				WHERE TS.tenderStdId=@v_TenderStdId_Int
				Order by TF.templateFormId
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderMandatoryDoc */


				/* START CODE: TO INSERT INTO TABLE - tbl_TenderEnvelopes] */
				INSERT INTO [dbo].[tbl_TenderEnvelopes]
					  ([tenderId],[envelopeId],[tenderFormId])
				SELECT  @v_TenderId_inInt,
						Case
							When @v_EventType_Vc<>'RFP'
							Then 1
							When @v_EventType_Vc='RFP' And isPriceBid='no'
							Then 1
							When @v_EventType_Vc='RFP' And isPriceBid='yes'
							Then 2
						End as envelopeId,
						tenderFormId
				From tbl_TenderForms TF INNER JOIN tbl_TenderSection TS ON TF.tenderSectionId=TS.tenderSectionId
				WHERE TS.tenderStdId=@v_TenderStdId_Int
				Order by TF.templateFormId
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderEnvelopes */

				/* START CODE: TO INSERT INTO TABLE - tbl_TenderListBox */
				INSERT INTO [dbo].[tbl_TenderListBox]
							([listBoxName] ,[tenderFormId], [listBoxId], [isCalcReq])
				SELECT [listBoxName], TF.tenderFormId, [listBoxId], [isCalcReq]
				FROM [dbo].[tbl_ListBoxMaster] LBM
				INNER JOIN tbl_TenderForms TF ON LBM.templateFormId=TF.templateFormId
				INNER JOIN tbl_TenderSection TS ON TF.tenderSectionId=TS.tenderSectionId
				WHERE TS.tenderStdId=@v_TenderStdId_Int
				Order by TF.templateFormId, TS.templateSectionId
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderListBox */

				/* START CODE: TO INSERT INTO TABLE - Tbl_tenderListDetail */
				INSERT INTO [dbo].[tbl_TenderListDetail]
							([tenderListId],[itemId],[itemValue],[itemText],[isDefault])
				SELECT TLB.tenderListId,[itemId],[itemValue],[itemText],[isDefault]
				FROM [dbo].[tbl_ListBoxDetail] LBD
				INNER JOIN tbl_TenderListBox TLB ON LBD.listBoxId=TLB.listBoxId
				INNER JOIN tbl_TenderForms TF ON TLB.tenderFormId=TF.tenderFormId
				--INNER JOIN tbl_TenderForms TF ON TLB.tenderFormId=TF.templateFormId
				INNER JOIN tbl_TenderSection TS ON TF.tenderSectionId=TS.tenderSectionId
				WHERE TS.tenderStdId=@v_TenderStdId_Int
				Order by TF.templateFormId, TS.templateSectionId
				/* END CODE: TO INSERT INTO TABLE - Tbl_tenderListDetail */


				/* START CODE: TO INSERT INTO TABLE - tbl_TenderTables */
				INSERT INTO [dbo].[tbl_TenderTables]
						([tenderFormId],[tableName],[tableHeader],[tableFooter],[noOfRows],[noOfCols],[isMultipleFilling],[templatetableId])
				SELECT tenderFormId, tableName, tableHeader, tableFooter, noOfRows, noOfCols, TT.isMultipleFilling , tableId
				FROM  tbl_templatetables TT INNER JOIN tbl_TenderForms TF ON TT.formId=TF.templateFormId
											INNER JOIN tbl_TenderSection TS ON TF.tenderSectionId=TS.tenderSectionId
				WHERE TT.templateId=@v_TemplateId_inInt And TS.tenderStdId=@v_TenderStdId_Int
				Order by TF.templateFormId, TS.templateSectionId
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderTables */


				/* START CODE: TO INSERT INTO TABLE - Tbl_tenderlistcelldetail  */
				INSERT INTO [dbo].[tbl_TenderListCellDetail]
							([tenderListId],[tenderTableId],[columnId],[cellId],[location])
				SELECT TLB.tenderListId, TT.tenderTableId,[columnId],[cellId],[location]
				FROM [dbo].[tbl_ListCellDetail] LCD
				INNER JOIN tbl_TenderListBox TLB ON LCD.listBoxId=TLB.listBoxId
				INNER JOIN tbl_TenderTables TT ON LCD.tenderTableId=TT.templatetableId
				INNER JOIN tbl_TenderForms TF ON TT.tenderFormId=TF.tenderFormId
				INNER JOIN tbl_TenderSection TS ON TF.tenderSectionId=TS.tenderSectionId
				WHERE TS.tenderStdId=@v_TenderStdId_Int
				and tenderListId in (SELECT TLB.tenderListId
				FROM [dbo].[tbl_ListBoxDetail] LBD
				INNER JOIN tbl_TenderListBox TLB ON LBD.listBoxId=TLB.listBoxId
				INNER JOIN tbl_TenderForms TF ON TLB.tenderFormId=TF.tenderFormId
				--INNER JOIN tbl_TenderForms TF ON TLB.tenderFormId=TF.templateFormId
				INNER JOIN tbl_TenderSection TS ON TF.tenderSectionId=TS.tenderSectionId
				WHERE TS.tenderStdId=@v_TenderStdId_Int)

				Order by TT.templatetableId, TF.templateFormId, TS.templateSectionId
				/* START CODE: TO INSERT INTO TABLE - Tbl_tenderlistcelldetail  */

				/* START CODE: TO INSERT INTO TABLE - tbl_TenderColumns */
				INSERT INTO [dbo].[tbl_TenderColumns]
					   ([tenderTableId],[columnId],[columnHeader],[dataType],[filledBy],[columnType],[sortOrder],[showorhide],[templateTableId])
				SELECT tenderTableId, columnId, columnHeader, dataType, tc.filledBy, columnType, sortOrder, showorhide,templatetableId
				FROM tbl_templatecolumns TC INNER JOIN tbl_TenderTables TT ON TC.tableId=TT.templatetableId
											INNER JOIN tbl_TenderForms TF ON TT.tenderFormId=TF.tenderFormId
											INNER JOIN tbl_TenderSection TS ON TF.tenderSectionId=TS.tenderSectionId
				WHERE TS.tenderStdId=@v_TenderStdId_Int
				Order by TT.templatetableId, TF.templateFormId, TS.templateSectionId
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderColumns */

				/* START CODE: TO INSERT INTO TABLE - tbl_TenderCells */
				INSERT INTO [dbo].[tbl_TenderCells]
						([tenderColId],[tenderTableId],[rowId],[cellDatatype],[cellvalue],[columnId],[templateTableId],[templateColumnId],[cellId])
				SELECT  tenderColId, TC.tenderTableId, rowId, cellDatatype, cellvalue, TmpCell.columnId, TmpCell.tableId, TmpCell.columnId, TmpCell.cellId
				FROM tbl_templatecells TmpCell INNER JOIN tbl_TenderColumns TC ON TmpCell.tableId=TC.templatetableId And TmpCell.columnId=TC.columnId
											   INNER JOIN tbl_TenderTables TT ON TC.tenderTableId=TT.tenderTableId
											   INNER JOIN tbl_TenderForms TF ON TT.tenderFormId=TF.tenderFormId
											   INNER JOIN tbl_TenderSection TS ON TF.tenderSectionId=TS.tenderSectionId
				WHERE TS.tenderStdId=@v_TenderStdId_Int
				Order by TC.columnId, TT.templatetableId, TF.templateFormId, TS.templateSectionId
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderCells */

				/* START CODE: TO INSERT INTO TABLE - tbl_TenderFormula */
				INSERT INTO [dbo].[tbl_TenderFormula]
						([tenderFormId],[tenderTableId],[columnId],[formula],[isGrandTotal])
				SELECT TF.tenderFormId, TT.tenderTableId, TmpF.columnId, formula, isGrandTotal
				FROM tbl_templateformulas TmpF INNER JOIN tbl_TenderColumns TC ON TmpF.tableId=TC.templatetableId And TmpF.columnId=TC.columnId
											   INNER JOIN tbl_TenderTables TT ON TC.tenderTableId=TT.tenderTableId
											   INNER JOIN tbl_TenderForms TF ON TT.tenderFormId=TF.tenderFormId
											   INNER JOIN tbl_TenderSection TS ON TF.tenderSectionId=TS.tenderSectionId
				WHERE TS.tenderStdId=@v_TenderStdId_Int
				Order by TC.columnId, TT.templatetableId, TF.templateFormId, TS.templateSectionId
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderFormula */

				/* START CODE: TO INSERT INTO TABLE - tbl_TenderIttHeader */
				INSERT INTO [dbo].[tbl_TenderIttHeader]
					  ([templateIttHeaderId],[ittHeaderName],[tenderSectionId],[temlateSectionId],[tenderId])
				SELECT ittHeaderId, ittHeaderName, TS.tenderSectionId, TS.templateSectionId,  @v_TenderId_inInt
				FROM tbl_Ittheader TmpH INNER JOIN tbl_TenderSection TS ON TmpH.sectionId=TS.templateSectionId
				WHERE TS.tenderStdId=@v_TenderStdId_Int
				Order by TS.templateSectionId
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderIttHeader */

				/* START CODE: TO INSERT INTO TABLE - tbl_TenderIttClause */
				INSERT INTO [dbo].[tbl_TenderIttClause]
						([tendederIttHeaderId],[ittClauseName], [tempplateIttClauseId])
				SELECT tenderIttHeaderId, ittClauseName, ittClauseId
				FROM tbl_IttClause	TmpC INNER JOIN tbl_TenderIttHeader TH ON TmpC.ittHeaderId=TH.templateIttHeaderId
									     INNER JOIN tbl_TenderSection TS ON TH.tenderSectionId=TS.tenderSectionId
				WHERE TS.tenderStdId=@v_TenderStdId_Int
				Order by TH.templateIttHeaderId, TS.templateSectionId
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderIttClause */

				/* START CODE: TO INSERT INTO TABLE - tbl_TenderIttSubClause */
				INSERT INTO [dbo].[tbl_TenderIttSubClause]
						([tenderIttClauseId],[ittSubClauseName],[isTdsApplicable],[templateIttSubClauseId])
				SELECT tenderIttClauseId, ittSubClauseName, isTdsApplicable, ittSubClauseId
				FROM tbl_IttSubClause TmpSC INNER JOIN tbl_TenderIttClause TC ON TmpSC.ittClauseId=TC.tempplateIttClauseId
											INNER JOIN tbl_TenderIttHeader TH ON TC.tendederIttHeaderId=TH.tenderIttHeaderId
											INNER JOIN tbl_TenderSection TS ON TH.tenderSectionId=TS.tenderSectionId
				WHERE TS.tenderStdId=@v_TenderStdId_Int and isTdsApplicable = 'yes' --for large goods, unnecessay clause was being inserted in BDS (changed by Mickey 19th Dec 2017)
				Order by TC.tempplateIttClauseId, TH.templateIttHeaderId, TS.templateSectionId
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderIttSubClause */

				/* START CODE: TO INSERT INTO TABLE - tbl_TenderTdsSubClause */

				--INSERT INTO [dbo].[tbl_TenderTdsSubClause]
				--		([tenderIttRefrence],[orderNumber],[tenderIttHeaderId],[templateTdsSubClauseId], tenderTdsClauseName)
				--SELECT ittReference, orderNumber, tenderIttHeaderId, tdsSubClauseId, tdsSubClauseName
				--FROM tbl_TdsSubClause TmpTds INNER JOIN tbl_TenderIttHeader TH ON TmpTds.ittHeaderId=TH.templateIttHeaderId
				--							 INNER JOIN tbl_TenderSection TS ON TH.tenderSectionId=TS.tenderSectionId
				--WHERE TS.tenderStdId=@v_TenderStdId_Int
				--Order by TH.templateIttHeaderId, TS.templateSectionId

				-- // FOR ittReference<>0 CASE
				INSERT INTO [dbo].[tbl_TenderTdsSubClause]
						([tenderIttRefrence],[orderNumber],[tenderIttHeaderId],[templateTdsSubClauseId], tenderTdsClauseName)
				select i.tenderIttSubClauseId, orderNumber, tenderIttHeaderId, tdsSubClauseId, tdsSubClauseName   from
				tbl_TenderIttSubClause i,tbl_tenderittclause ic,tbl_TenderIttHeader it,tbl_TenderSection ts,
				tbl_TdsSubClause tds
				where i.tenderIttClauseId=ic.tenderIttClauseId and it.tenderIttHeaderId=ic.tendederIttHeaderId
				and ts.tenderSectionId =it.tenderSectionId  and tenderStdId=@v_TenderStdId_Int
				and tds.ittReference=i.templateIttSubClauseId and tds.ittReference<>0

				-- // FOR ittReference=0 CASE
				INSERT INTO [dbo].[tbl_TenderTdsSubClause]
						([tenderIttRefrence],[orderNumber],[tenderIttHeaderId],[templateTdsSubClauseId], tenderTdsClauseName)
				SELECT ittReference, orderNumber, tenderIttHeaderId, tdsSubClauseId, tdsSubClauseName
				FROM tbl_TdsSubClause TmpTds INNER JOIN tbl_TenderIttHeader TH ON TmpTds.ittHeaderId=TH.templateIttHeaderId
											 INNER JOIN tbl_TenderSection TS ON TH.tenderSectionId=TS.tenderSectionId
				WHERE TS.tenderStdId=@v_TenderStdId_Int And ittReference=0
				Order by TH.templateIttHeaderId, TS.templateSectionId

				/* END CODE: TO INSERT INTO TABLE - tbl_TenderTdsSubClause */

			END


			ELSE IF @v_DocAvlMethod_Vc='Lot'
			BEGIN
				--print('enter')
				DECLARE cur1 cursor FAST_FORWARD For
					SELECT DISTINCT appPkgLotId FROM dbo.tbl_TenderLots WHERE tenderId=@v_TenderId_inInt
				OPEN cur1
					FETCH NEXT FROM cur1 INTO @v_TenderLotId_Int

					WHILE @@Fetch_status = 0
					BEGIN
						/* START CODE: TO INSERT INTO TABLE - tbl_TenderStd */
						--print('inside cursor')
						INSERT INTO [dbo].[tbl_TenderStd]
								([templateId],[templateName],[noOfSections],[status],[tenderId],[packageLotId],[createdBy],procType)
						SELECT templateId, templateName, noOfSections, status, @v_TenderId_inInt, @v_TenderLotId_Int, @v_CreatedBy_inInt,procType
							FROM tbl_TemplateMaster WHERE templateId=@v_TemplateId_inInt
						/* END CODE: TO INSERT INTO TABLE - tbl_TenderStd */

						Set @v_TenderStdId_Int=IDENT_CURRENT('dbo.tbl_TenderStd')
						--print(@v_TenderStdId_Int)
						/* START CODE: TO INSERT INTO TABLE - tbl_TenderSection */
						INSERT INTO [dbo].[tbl_TenderSection]
								([templateSectionId],[tenderStdId],[sectionName],[contentType],[isSubSection])
						SELECT sectionId, tenderStdId, sectionName, contentType, isSubSection
						FROM tbl_TemplateSections TS INNER JOIN tbl_TenderStd S ON TS.templateId=S.templateId
						WHERE TS.templateId=@v_TemplateId_inInt And S.tenderStdId=@v_TenderStdId_Int
						/* END CODE: TO INSERT INTO TABLE - tbl_TenderSection */

					/* START CODE: TO INSERT INTO TABLE - tbl_TenderSectionDocs */
						INSERT INTO [dbo].[tbl_TenderSectionDocs]
							([description],[docName],[docSize],[tenderSectionId],[templateSectionDocId],[status],[tenderId])
						SELECT description, docName, docSize, tenderSectionId, TS.templateSectionId, status, @v_TenderId_inInt
						FROM tbl_TemplateSectionDocs TSD INNER JOIN tbl_TenderSection TS ON TSD.sectionId=TS.templateSectionId
						WHERE TSD.templateId=@v_TemplateId_inInt And TS.tenderStdId=@v_TenderStdId_Int
						Order by TS.templateSectionId
						/* END CODE: TO INSERT INTO TABLE - tbl_TenderSectionDocs */

						/* START CODE: TO INSERT INTO TABLE - tbl_TenderForms */
						INSERT INTO [dbo].[tbl_TenderForms]
							  ([tenderSectionId],[templateFormId],[filledBy],[formName],[formHeader],[formFooter],[noOfTables],
									[isMultipleFilling],[isEncryption],[isPriceBid],isMandatory, pkgLotId)
						SELECT tenderSectionId, formId, filledBy,formName, formHeader, formFooter, noOfTables,
									isMultipleFilling, isEncryption, isPriceBid,isMandatory, @v_TenderLotId_Int
						FROM tbl_TemplateSectionForm TSF INNER JOIN tbl_TenderSection TS ON TSF.sectionId=TS.templateSectionId
						WHERE TSF.templateId=@v_TemplateId_inInt And TS.tenderStdId=@v_TenderStdId_Int
						Order by TS.templateSectionId
						/* END CODE: TO INSERT INTO TABLE - tbl_TenderForms */


						/* START CODE: TO INSERT INTO TABLE - tbl_TenderMandatoryDoc] */
						INSERT INTO [dbo].[tbl_TenderMandatoryDoc]
								([tenderId],[tenderFormId],[templateFormId],[documentName],[createdBy],[createdDate])
						SELECT  @v_TenderId_inInt, TF.tenderFormId, TMD.templateFormId, documentName, @v_CreatedBy_inInt, GETDATE()
						From tbl_TemplateMandatoryDoc TMD
						INNER JOIN tbl_TenderForms TF ON TMD.templateFormId=TF.templateFormId
						INNER JOIN tbl_TenderSection TS ON TF.tenderSectionId=TS.tenderSectionId
						WHERE TS.tenderStdId=@v_TenderStdId_Int
						Order by TF.templateFormId
						/* END CODE: TO INSERT INTO TABLE - tbl_TenderMandatoryDoc */

						/* START CODE: TO INSERT INTO TABLE - tbl_TenderEnvelopes */
						INSERT INTO [dbo].[tbl_TenderEnvelopes]
							  ([tenderId],[envelopeId],[tenderFormId])
						SELECT  @v_TenderId_inInt,
								Case
									When @v_EventType_Vc<>'RFP'
									Then 1
									When @v_EventType_Vc='RFP' And isPriceBid='no'
									Then 1
									When @v_EventType_Vc='RFP' And isPriceBid='yes'
									Then 2
								End as envelopeId,
								tenderFormId
						From tbl_TenderForms TF INNER JOIN tbl_TenderSection TS ON TF.tenderSectionId=TS.tenderSectionId
						WHERE TS.tenderStdId=@v_TenderStdId_Int
						Order by TF.templateFormId
						/* END CODE: TO INSERT INTO TABLE - tbl_TenderEnvelopes */

				/* START CODE: TO INSERT INTO TABLE - tbl_TenderListBox */
				INSERT INTO [dbo].[tbl_TenderListBox]
							([listBoxName] ,[tenderFormId], [listBoxId], [isCalcReq])
				SELECT [listBoxName], TF.tenderFormId, [listBoxId], [isCalcReq]
				FROM [dbo].[tbl_ListBoxMaster] LBM
				INNER JOIN tbl_TenderForms TF ON LBM.templateFormId=TF.templateFormId
				INNER JOIN tbl_TenderSection TS ON TF.tenderSectionId=TS.tenderSectionId
				WHERE TS.tenderStdId=@v_TenderStdId_Int
				Order by TF.templateFormId, TS.templateSectionId
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderListBox */

				/* START CODE: TO INSERT INTO TABLE - Tbl_tenderListDetail */
				INSERT INTO [dbo].[tbl_TenderListDetail]
							([tenderListId],[itemId],[itemValue],[itemText],[isDefault])
				SELECT TLB.tenderListId,[itemId],[itemValue],[itemText],[isDefault]
				FROM [dbo].[tbl_ListBoxDetail] LBD
				INNER JOIN tbl_TenderListBox TLB ON LBD.listBoxId=TLB.listBoxId
				--INNER JOIN tbl_TenderForms TF ON TLB.tenderFormId=TF.templateFormId
				INNER JOIN tbl_TenderForms TF ON TLB.tenderFormId=TF.tenderFormId
				INNER JOIN tbl_TenderSection TS ON TF.tenderSectionId=TS.tenderSectionId
				WHERE TS.tenderStdId=@v_TenderStdId_Int
				Order by TF.templateFormId, TS.templateSectionId
				/* END CODE: TO INSERT INTO TABLE - Tbl_tenderListDetail */


						/* START CODE: TO INSERT INTO TABLE - tbl_TenderTables */
						INSERT INTO [dbo].[tbl_TenderTables]
								([tenderFormId],[tableName],[tableHeader],[tableFooter],[noOfRows],[noOfCols],[isMultipleFilling],[templatetableId])
						SELECT tenderFormId, tableName, tableHeader, tableFooter, noOfRows, noOfCols, TT.isMultipleFilling , tableId
						FROM  tbl_templatetables TT INNER JOIN tbl_TenderForms TF ON TT.formId=TF.templateFormId
													INNER JOIN tbl_TenderSection TS ON TF.tenderSectionId=TS.tenderSectionId
						WHERE TT.templateId=@v_TemplateId_inInt And TS.tenderStdId=@v_TenderStdId_Int
						Order by TF.templateFormId, TS.templateSectionId
						/* END CODE: TO INSERT INTO TABLE - tbl_TenderTables */

				/* START CODE: TO INSERT INTO TABLE - Tbl_tenderlistcelldetail  */
				INSERT INTO [dbo].[tbl_TenderListCellDetail]
							([tenderListId],[tenderTableId],[columnId],[cellId],[location])
				SELECT TLB.tenderListId, TT.tenderTableId,[columnId],[cellId],[location]
				FROM [dbo].[tbl_ListCellDetail] LCD
				INNER JOIN tbl_TenderListBox TLB ON LCD.listBoxId=TLB.listBoxId
				INNER JOIN tbl_TenderTables TT ON LCD.tenderTableId=TT.templatetableId
				INNER JOIN tbl_TenderForms TF ON TT.tenderFormId=TF.tenderFormId
				INNER JOIN tbl_TenderSection TS ON TF.tenderSectionId=TS.tenderSectionId
				WHERE TS.tenderStdId=@v_TenderStdId_Int
				and tenderListId in (SELECT TLB.tenderListId
				FROM [dbo].[tbl_ListBoxDetail] LBD
				INNER JOIN tbl_TenderListBox TLB ON LBD.listBoxId=TLB.listBoxId
				INNER JOIN tbl_TenderForms TF ON TLB.tenderFormId=TF.tenderFormId
				--INNER JOIN tbl_TenderForms TF ON TLB.tenderFormId=TF.templateFormId
				INNER JOIN tbl_TenderSection TS ON TF.tenderSectionId=TS.tenderSectionId
				WHERE TS.tenderStdId=@v_TenderStdId_Int)
				Order by TT.templatetableId, TF.templateFormId, TS.templateSectionId
				/* START CODE: TO INSERT INTO TABLE - Tbl_tenderlistcelldetail  */

						/* START CODE: TO INSERT INTO TABLE - tbl_TenderColumns */
						INSERT INTO [dbo].[tbl_TenderColumns]
							   ([tenderTableId],[columnId],[columnHeader],[dataType],[filledBy],[columnType],[sortOrder],[showorhide],[templateTableId])
						SELECT tenderTableId, columnId, columnHeader, dataType, tc.filledBy, columnType, sortOrder, showorhide,templatetableId
						FROM tbl_templatecolumns TC INNER JOIN tbl_TenderTables TT ON TC.tableId=TT.templatetableId
													INNER JOIN tbl_TenderForms TF ON TT.tenderFormId=TF.tenderFormId
													INNER JOIN tbl_TenderSection TS ON TF.tenderSectionId=TS.tenderSectionId
						WHERE TS.tenderStdId=@v_TenderStdId_Int
						Order by TT.templatetableId, TF.templateFormId, TS.templateSectionId
						/* END CODE: TO INSERT INTO TABLE - tbl_TenderColumns */

						/* START CODE: TO INSERT INTO TABLE - tbl_TenderCells */
						INSERT INTO [dbo].[tbl_TenderCells]
								([tenderColId],[tenderTableId],[rowId],[cellDatatype],[cellvalue],[columnId],[templateTableId],[templateColumnId],[cellId])
						SELECT  tenderColId, TC.tenderTableId, rowId, cellDatatype, cellvalue, TmpCell.columnId, TmpCell.tableId, TmpCell.columnId, TmpCell.cellId
						FROM tbl_templatecells TmpCell INNER JOIN tbl_TenderColumns TC ON TmpCell.tableId=TC.templatetableId And TmpCell.columnId=TC.columnId
													   INNER JOIN tbl_TenderTables TT ON TC.tenderTableId=TT.tenderTableId
													   INNER JOIN tbl_TenderForms TF ON TT.tenderFormId=TF.tenderFormId
													   INNER JOIN tbl_TenderSection TS ON TF.tenderSectionId=TS.tenderSectionId
						WHERE TS.tenderStdId=@v_TenderStdId_Int
						Order by TC.columnId, TT.templatetableId, TF.templateFormId, TS.templateSectionId
						/* END CODE: TO INSERT INTO TABLE - tbl_TenderCells */

						/* START CODE: TO INSERT INTO TABLE - tbl_TenderFormula */
						INSERT INTO [dbo].[tbl_TenderFormula]
								([tenderFormId],[tenderTableId],[columnId],[formula],[isGrandTotal])
						SELECT TF.tenderFormId, TT.tenderTableId, TmpF.columnId, formula, isGrandTotal
						FROM tbl_templateformulas TmpF INNER JOIN tbl_TenderColumns TC ON TmpF.tableId=TC.templatetableId And TmpF.columnId=TC.columnId
													   INNER JOIN tbl_TenderTables TT ON TC.tenderTableId=TT.tenderTableId
													   INNER JOIN tbl_TenderForms TF ON TT.tenderFormId=TF.tenderFormId
													   INNER JOIN tbl_TenderSection TS ON TF.tenderSectionId=TS.tenderSectionId
						WHERE TS.tenderStdId=@v_TenderStdId_Int
						Order by TC.columnId, TT.templatetableId, TF.templateFormId, TS.templateSectionId
						/* END CODE: TO INSERT INTO TABLE - tbl_TenderFormula */

							/* START CODE: TO INSERT INTO TABLE - tbl_TenderIttHeader */
						INSERT INTO [dbo].[tbl_TenderIttHeader]
							  ([templateIttHeaderId],[ittHeaderName],[tenderSectionId],[temlateSectionId],[tenderId])
						SELECT ittHeaderId, ittHeaderName, TS.tenderSectionId, TS.templateSectionId,  @v_TenderId_inInt
						FROM tbl_Ittheader TmpH INNER JOIN tbl_TenderSection TS ON TmpH.sectionId=TS.templateSectionId
						WHERE TS.tenderStdId=@v_TenderStdId_Int
						Order by TS.templateSectionId
						/* END CODE: TO INSERT INTO TABLE - tbl_TenderIttHeader */

						/* START CODE: TO INSERT INTO TABLE - tbl_TenderIttClause */
						INSERT INTO [dbo].[tbl_TenderIttClause]
								([tendederIttHeaderId],[ittClauseName], [tempplateIttClauseId])
						SELECT tenderIttHeaderId, ittClauseName, ittClauseId
						FROM tbl_IttClause	TmpC INNER JOIN tbl_TenderIttHeader TH ON TmpC.ittHeaderId=TH.templateIttHeaderId
												 INNER JOIN tbl_TenderSection TS ON TH.tenderSectionId=TS.tenderSectionId
						WHERE TS.tenderStdId=@v_TenderStdId_Int
						Order by TH.templateIttHeaderId, TS.templateSectionId
						/* END CODE: TO INSERT INTO TABLE - tbl_TenderIttClause */

						/* START CODE: TO INSERT INTO TABLE - tbl_TenderIttSubClause */
						INSERT INTO [dbo].[tbl_TenderIttSubClause]
								([tenderIttClauseId],[ittSubClauseName],[isTdsApplicable],[templateIttSubClauseId])
						SELECT tenderIttClauseId, ittSubClauseName, isTdsApplicable, ittSubClauseId
						FROM tbl_IttSubClause TmpSC INNER JOIN tbl_TenderIttClause TC ON TmpSC.ittClauseId=TC.tempplateIttClauseId
													INNER JOIN tbl_TenderIttHeader TH ON TC.tendederIttHeaderId=TH.tenderIttHeaderId
													INNER JOIN tbl_TenderSection TS ON TH.tenderSectionId=TS.tenderSectionId
						WHERE TS.tenderStdId=@v_TenderStdId_Int and isTdsApplicable = 'yes'  --for large goods, unnecessay clause was being inserted in BDS (changed by Mickey 19th Dec 2017)
						Order by TC.tempplateIttClauseId, TH.templateIttHeaderId, TS.templateSectionId
						/* END CODE: TO INSERT INTO TABLE - tbl_TenderIttSubClause */

						/* START CODE: TO INSERT INTO TABLE - tbl_TenderTdsSubClause */

						--INSERT INTO [dbo].[tbl_TenderTdsSubClause]
						--		([tenderIttRefrence],[orderNumber],[tenderIttHeaderId],[templateTdsSubClauseId], tenderTdsClauseName)
						--SELECT ittReference, orderNumber, tenderIttHeaderId, tdsSubClauseId, tdsSubClauseName
						--FROM tbl_TdsSubClause TmpTds INNER JOIN tbl_TenderIttHeader TH ON TmpTds.ittHeaderId=TH.templateIttHeaderId
						--							 INNER JOIN tbl_TenderSection TS ON TH.tenderSectionId=TS.tenderSectionId
						--WHERE TS.tenderStdId=@v_TenderStdId_Int
						--Order by TH.templateIttHeaderId, TS.templateSectionId


						-- // FOR ittReference<>0 CASE
						INSERT INTO [dbo].[tbl_TenderTdsSubClause]
								([tenderIttRefrence],[orderNumber],[tenderIttHeaderId],[templateTdsSubClauseId], tenderTdsClauseName)
						select i.tenderIttSubClauseId, orderNumber, tenderIttHeaderId, tdsSubClauseId, tdsSubClauseName   from
						tbl_TenderIttSubClause i,tbl_tenderittclause ic,tbl_TenderIttHeader it,tbl_TenderSection ts,
						tbl_TdsSubClause tds
						where i.tenderIttClauseId=ic.tenderIttClauseId and it.tenderIttHeaderId=ic.tendederIttHeaderId
						and ts.tenderSectionId =it.tenderSectionId  and tenderStdId=@v_TenderStdId_Int
						and tds.ittReference=i.templateIttSubClauseId and tds.ittReference<>0

						-- // FOR ittReference=0 CASE
						INSERT INTO [dbo].[tbl_TenderTdsSubClause]
								([tenderIttRefrence],[orderNumber],[tenderIttHeaderId],[templateTdsSubClauseId], tenderTdsClauseName)
						SELECT ittReference, orderNumber, tenderIttHeaderId, tdsSubClauseId, tdsSubClauseName
						FROM tbl_TdsSubClause TmpTds INNER JOIN tbl_TenderIttHeader TH ON TmpTds.ittHeaderId=TH.templateIttHeaderId
													 INNER JOIN tbl_TenderSection TS ON TH.tenderSectionId=TS.tenderSectionId
						WHERE TS.tenderStdId=@v_TenderStdId_Int And ittReference=0
						Order by TH.templateIttHeaderId, TS.templateSectionId


						/* END CODE: TO INSERT INTO TABLE - tbl_TenderTdsSubClause */

						FETCH NEXT FROM cur1 INTO @v_TenderLotId_Int
					END

				CLOSE cur1
				DEALLOCATE cur1

			END

			If @v_TenderStdId_Int is Not Null
			Begin
				Set @v_flag_bit=1
			    Select @v_flag_bit as flag, 'Record inserted.' as Msg, @v_TenderStdId_Int as Id
			End
			Else
			Begin
				Set @v_flag_bit=0
				Select @v_flag_bit as flag, 'Record not inserted. Document available method is not specified.' as Msg, 0 as Id
			End

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		BEGIN
			If Cursor_Status ('Global', 'cur1')=1
				Begin
					close cur1
					Deallocate cur1
				End
				If Cursor_Status ('Global', 'cur1')=-1
				Begin
					Deallocate cur1
				End

			Set @v_flag_bit=0
			Select @v_flag_bit as flag, ERROR_MESSAGE() as Msg
			ROLLBACK TRAN
		END
	END CATCH
END
IF @v_Action_inVc='DocumentDump'
BEGIN
BEGIN TRAN
	BEGIN TRY
	SET @v_DerivedTenderID = @v_TemplateId_inInt
	
	DECLARE @v_TenderStdId_Int_old int ,@v_tenderSectionId int ,
	@v_tenderSectionId_old int,@v_tenderFormId_old int,@v_tenderFormId int,
	@v_tenderTableId_old int,@v_tenderTableId int,@v_tenderListId_old int,
	@v_tenderListId int,@v_tenderIttHeaderId_old int,@v_tenderIttHeaderId int,
	@v_tenderIttClauseId_old int,@v_tenderIttClauseId int,
	@v_tenderIttSubClauseId_old int,@v_tenderIttSubClauseId int

		DELETE FROM tbl_TenderGrandSum where tenderId = @v_DerivedTenderID
		DELETE FROM tbl_TenderStd where tenderId = @v_DerivedTenderID
		DELETE FROM tbl_TenderSectionDocs where tenderId = @v_DerivedTenderID
		DELETE FROM tbl_TenderMandatoryDoc where tenderId = @v_DerivedTenderID
		DELETE FROM tbl_TenderEnvelopes where tenderId = @v_DerivedTenderID
		DELETE FROM tbl_TenderIttHeader where tenderId = @v_DerivedTenderID
		DELETE FROM tbl_TenderIttHeader where tenderId = @v_DerivedTenderID
		
			IF @v_DocAvlMethod_Vc='Package'
			BEGIN

				SELECT @v_TenderLotId_Int = MAX(appPkgLotId) FROM dbo.tbl_TenderLots 
				WHERE tenderId = @v_DerivedTenderID

				INSERT INTO [dbo].[tbl_TenderStd]
				([templateId],[templateName],[noOfSections],[status],[tenderId],[packageLotId],[createdBy],procType)
				SELECT	templateId,templateName,noOfSections,[status],@v_DerivedTenderID,packageLotId,createdBy,procType
				FROM tbl_TenderStd	where tenderId = @v_TenderId_inInt
				
				SELECT @v_TenderStdId_Int = MAX(tenderStdId) FROM tbl_TenderStd where tenderId = @v_DerivedTenderID

				SELECT @v_TenderStdId_Int_old = MAX(tenderStdId) FROM tbl_TenderStd where tenderId = @v_TenderId_inInt

				DECLARE cur_Section1 cursor FAST_FORWARD For
					SELECT DISTINCT tenderSectionId FROM tbl_TenderSection WHERE tenderStdId = @v_TenderStdId_Int_old
					OPEN cur_Section1
						FETCH NEXT FROM cur_Section1 INTO @v_tenderSectionId_old
							WHILE @@Fetch_status = 0
							BEGIN
								
								INSERT INTO [dbo].[tbl_TenderSection]
								([templateSectionId],[tenderStdId],[sectionName],[contentType],[isSubSection])
								select templateSectionId,@v_TenderStdId_Int,sectionName,
								contentType,isSubSection FROM tbl_TenderSection 
								where tenderSectionId = @v_tenderSectionId_old 
								
								select @v_tenderSectionId =MAX(tenderSectionId) 
								FROM tbl_TenderSection 
								where tenderStdId = @v_TenderStdId_Int 
								
								INSERT INTO tbl_TenderSectionDocs
								([description],docName,docSize,tenderSectionId,
								templateSectionDocId,[status],tenderId)
								SELECT [description],docName,docSize,@v_tenderSectionId,
								templateSectionDocId,status,@v_DerivedTenderID
								FROM tbl_TenderSectionDocs 
								where tenderSectionId = @v_tenderSectionId_old
											
									DECLARE cur_DocumentForms cursor FAST_FORWARD For
										select tenderFormId from tbl_TenderForms where (formstatus is null or formstatus not in ('c','cp')) and tenderSectionId = @v_tenderSectionId_old
										OPEN cur_DocumentForms
											FETCH NEXT FROM cur_DocumentForms INTO @v_tenderFormId_old
												WHILE @@Fetch_status = 0
												BEGIN
													INSERT INTO tbl_TenderForms (tenderSectionId ,templateFormId,filledBy, 
													formName, formHeader, formFooter, noOfTables,isMultipleFilling, 
													isEncryption, isPriceBid, isMandatory, formStatus,pkgLotId, FormType)
													select @v_tenderSectionId , templateFormId , filledBy , formName , 
													formHeader,formFooter , noOfTables , isMultipleFilling , isEncryption , 
													isPriceBid , isMandatory , formStatus , 
													(case WHEN pkgLotId<>0 then @v_TenderLotId_Int ELSE pkgLotId END) , FormType  
													from tbl_TenderForms tf where tenderFormId = @v_tenderFormId_old
												
													select @v_tenderFormId =MAX(tenderFormId) FROM tbl_TenderForms 
													where tenderSectionId = @v_tenderSectionId 
													
													INSERT INTO tbl_TenderMandatoryDoc (tenderId,tenderFormId,templateFormId,
													documentName,createdBy,createdDate)
													SELECT  @v_DerivedTenderID, @v_tenderFormId, templateFormId, 
													documentName, @v_CreatedBy_inInt, GETDATE() FROM tbl_TenderMandatoryDoc
													where tenderId = @v_TenderId_inInt and tenderFormId = @v_tenderFormId_old
												
													INSERT INTO tbl_TenderEnvelopes(tenderId,envelopeId,tenderFormId)
													SELECT @v_DerivedTenderID,envelopeId,@v_tenderFormId 
													FROM tbl_TenderEnvelopes where tenderFormId = @v_tenderFormId_old
													
														DECLARE cur_TenderTable cursor FAST_FORWARD For
															SELECT tenderTableId FROM tbl_TenderTables where tenderFormId = @v_tenderFormId_old
															OPEN cur_TenderTable
																FETCH NEXT FROM cur_TenderTable INTO @v_tenderTableId_old
																WHILE @@Fetch_status = 0
																BEGIN
																
																	INSERT INTO tbl_TenderTables(tenderFormId,tableName,tableHeader,tableFooter,
																	noOfRows,noOfCols,isMultipleFilling,templatetableId)
																	SELECT @v_tenderFormId,tableName,tableHeader,tableFooter,
																	noOfRows,noOfCols,isMultipleFilling,templatetableId 
																	FROM tbl_TenderTables where tenderTableId=@v_tenderTableId_old
																	
																	select @v_tenderTableId =MAX(tenderTableId) FROM tbl_TenderTables 
																	where tenderFormId = @v_tenderFormId 
																	
																	INSERT INTO tbl_TenderColumns(tenderTableId,columnId,columnHeader,
																	dataType,filledBy,columnType,sortOrder,showorhide,templateTableId)
																	SELECT @v_tenderTableId,columnId,columnHeader,dataType,filledBy,
																	columnType,sortOrder,showorhide,templateTableId FROM tbl_TenderColumns
																	where tenderTableId=@v_tenderTableId_old
																	
																	INSERT INTO tbl_TenderCells(tenderColId,tenderTableId,rowId,
																	cellDatatype,cellvalue,columnId,templateTableId,templateColumnId,cellId)
																	SELECT tenderColId,@v_tenderTableId,rowId,cellDatatype,cellvalue,columnId,
																	templateTableId,templateColumnId,cellId FROM tbl_TenderCells 
																	where tenderTableId = @v_tenderTableId_old
																	
																	INSERT INTO tbl_TenderFormula(tenderFormId,tenderTableId,columnId,formula,isGrandTotal)
																	SELECT @v_tenderFormId,@v_tenderTableId,columnId,formula,isGrandTotal FROM tbl_TenderFormula
																	where tenderTableId = @v_tenderTableId_old
																										
																		DECLARE cur_List cursor FAST_FORWARD For
																		SELECT tenderListId FROM tbl_TenderListBox where tenderFormId = @v_tenderFormId_old
																			OPEN cur_List
																			FETCH NEXT FROM cur_List INTO @v_tenderListId_old
																				WHILE @@Fetch_status = 0
																				BEGIN
																
																					INSERT INTO tbl_TenderListBox(listBoxName,tenderFormId, listBoxId, isCalcReq)
																					SELECT listBoxName,@v_tenderFormId,listBoxId,isCalcReq FROM tbl_TenderListBox 
																					where tenderListId=@v_tenderListId_old
																	
																					select @v_tenderListId =MAX(tenderListId) FROM tbl_TenderListBox 
																					where tenderFormId = @v_tenderFormId 
																	
																					INSERT INTO tbl_TenderListDetail(tenderListId,itemId,itemValue,itemText,isDefault)
																					SELECT 	@v_tenderListId,itemId,itemValue,itemText,isDefault FROM tbl_TenderListDetail
																					where tenderListId=@v_tenderListId_old	
																					
																					INSERT INTO tbl_TenderListCellDetail(tenderListId,tenderTableId,columnId,cellId,location)
																					SELECT @v_tenderListId,@v_tenderTableId,columnId,cellId,location FROM tbl_TenderListCellDetail
																					where tenderListId=@v_tenderListId_old						
																	
																					FETCH NEXT FROM cur_List INTO @v_tenderListId_old
																				END
																			CLOSE cur_List
																		DEALLOCATE cur_List
																		
																		
												
																	FETCH NEXT FROM cur_TenderTable INTO @v_tenderTableId_old
																END
															CLOSE cur_TenderTable
														DEALLOCATE cur_TenderTable
														
														
												FETCH NEXT FROM cur_DocumentForms INTO @v_tenderFormId_old
												END
										CLOSE cur_DocumentForms
									DEALLOCATE cur_DocumentForms
									
									
									DECLARE cur_IttHeader cursor FAST_FORWARD For
										select tenderIttHeaderId from tbl_TenderIttHeader where tenderSectionId =  @v_tenderSectionId_old
										OPEN cur_IttHeader
											FETCH NEXT FROM cur_IttHeader INTO @v_tenderIttHeaderId_old
												WHILE @@Fetch_status = 0
												BEGIN
													
													INSERT INTO tbl_TenderIttHeader (templateIttHeaderId,ittHeaderName,
													tenderSectionId,temlateSectionId,tenderId)
													SELECT templateIttHeaderId,ittHeaderName,@v_tenderSectionId,
													temlateSectionId,@v_DerivedTenderID FROM tbl_TenderIttHeader
													WHERE tenderIttHeaderId = @v_tenderIttHeaderId_old
													
													select @v_tenderIttHeaderId = MAX(tenderIttHeaderId) from tbl_TenderIttHeader 
													where tenderSectionId =  @v_tenderSectionId
													
														DECLARE cur_IttClause cursor FAST_FORWARD For
															select tenderIttClauseId from tbl_TenderIttClause where tendederIttHeaderId = @v_tenderIttHeaderId_old
															OPEN cur_IttClause
																FETCH NEXT FROM cur_IttClause INTO @v_tenderIttClauseId_old
																	WHILE @@Fetch_status = 0
																	BEGIN
																		
																		INSERT INTO tbl_TenderIttClause(tendederIttHeaderId,ittClauseName,tempplateIttClauseId)
																		SELECT @v_tenderIttHeaderId,ittClauseName,tempplateIttClauseId 
																		from tbl_TenderIttClause
																		WHERE tenderIttClauseId = @v_tenderIttClauseId_old
																		
																		select @v_tenderIttClauseId = MAX(tenderIttClauseId) from tbl_TenderIttClause 
																		where tendederIttHeaderId = @v_tenderIttHeaderId
																		
																		DECLARE cur_IttSubClause cursor FAST_FORWARD For
																			select tenderIttSubClauseId from tbl_TenderIttSubClause where tenderIttClauseId = @v_tenderIttClauseId_old
																			OPEN cur_IttSubClause
																			FETCH NEXT FROM cur_IttSubClause INTO @v_tenderIttSubClauseId_old
																				WHILE @@Fetch_status = 0
																				BEGIN
																		
																		
																					INSERT INTO tbl_TenderIttSubClause
																					(tenderIttClauseId,ittSubClauseName,isTdsApplicable,templateIttSubClauseId)
																					SELECT @v_tenderIttClauseId,ittSubClauseName,isTdsApplicable,
																					templateIttSubClauseId FROM tbl_TenderIttSubClause 
																					WHERE tenderIttSubClauseId = @v_tenderIttSubClauseId_old
																					
																					select @v_tenderIttSubClauseId = MAX(tenderIttSubClauseId) from tbl_TenderIttSubClause 
																					where tenderIttClauseId =@v_tenderIttClauseId
																																						
																					INSERT INTO tbl_TenderTdsSubClause
																					(tenderIttRefrence,orderNumber,tenderIttHeaderId,templateTdsSubClauseId,tenderTdsClauseName)
																					SELECT @v_tenderIttSubClauseId , orderNumber,@v_tenderIttHeaderId,templateTdsSubClauseId,tenderTdsClauseName
																					FROM tbl_TenderTdsSubClause 
																					WHERE tenderIttRefrence = @v_tenderIttSubClauseId_old and tenderIttHeaderId=@v_tenderIttHeaderId_old
																			
																				FETCH NEXT FROM cur_IttSubClause INTO @v_tenderIttSubClauseId_old
																				END
																			CLOSE cur_IttSubClause
																		DEALLOCATE cur_IttSubClause
																							
																		FETCH NEXT FROM cur_IttClause INTO @v_tenderIttClauseId_old
																	END
															CLOSE cur_IttClause
														DEALLOCATE cur_IttClause
													
													INSERT INTO tbl_TenderTdsSubClause
													(tenderIttRefrence,orderNumber,tenderIttHeaderId,templateTdsSubClauseId,tenderTdsClauseName)
													SELECT tenderIttRefrence , orderNumber,@v_tenderIttHeaderId,templateTdsSubClauseId,tenderTdsClauseName
													FROM tbl_TenderTdsSubClause 
													WHERE tenderIttRefrence = 0 and tenderIttHeaderId=@v_tenderIttHeaderId_old
																					
													FETCH NEXT FROM cur_IttHeader INTO @v_tenderIttHeaderId_old
												END
										CLOSE cur_IttHeader
									DEALLOCATE cur_IttHeader
									
								FETCH NEXT FROM cur_Section1 INTO @v_tenderSectionId_old
							END
					CLOSE cur_Section1
				DEALLOCATE cur_Section1


			END


			ELSE IF @v_DocAvlMethod_Vc='Lot'
			BEGIN
				--print('enter')
				DECLARE cur1 cursor FAST_FORWARD For
					SELECT DISTINCT appPkgLotId FROM dbo.tbl_TenderLots WHERE tenderId=@v_TenderId_inInt
				OPEN cur1
					FETCH NEXT FROM cur1 INTO @v_TenderLotId_Int

					WHILE @@Fetch_status = 0
					BEGIN

						INSERT INTO [dbo].[tbl_TenderStd]
						([templateId],[templateName],[noOfSections],[status],[tenderId],[packageLotId],[createdBy],procType)
						SELECT	templateId,templateName,noOfSections,[status],@v_DerivedTenderID,packageLotId,createdBy,procType
						FROM tbl_TenderStd	where tenderId = @v_TenderId_inInt

						SELECT @v_TenderStdId_Int = MAX(tenderStdId) FROM tbl_TenderStd where tenderId = @v_DerivedTenderID

						SELECT @v_TenderStdId_Int_old = MAX(tenderStdId) FROM tbl_TenderStd where tenderId = @v_TenderId_inInt

						DECLARE cur_Section1 cursor FAST_FORWARD For
							SELECT DISTINCT tenderSectionId FROM tbl_TenderSection WHERE tenderStdId = @v_TenderStdId_Int_old
							OPEN cur_Section1
								FETCH NEXT FROM cur_Section1 INTO @v_tenderSectionId_old
									WHILE @@Fetch_status = 0
									BEGIN
										
										INSERT INTO [dbo].[tbl_TenderSection]
										([templateSectionId],[tenderStdId],[sectionName],[contentType],[isSubSection])
										select templateSectionId,@v_TenderStdId_Int,sectionName,
										contentType,isSubSection FROM tbl_TenderSection 
										where tenderSectionId = @v_tenderSectionId_old 
										
										select @v_tenderSectionId =MAX(tenderSectionId) 
										FROM tbl_TenderSection 
										where tenderStdId = @v_TenderStdId_Int 
										
										INSERT INTO tbl_TenderSectionDocs
										([description],docName,docSize,tenderSectionId,
										templateSectionDocId,[status],tenderId)
										SELECT [description],docName,docSize,@v_tenderSectionId,
										templateSectionDocId,status,@v_DerivedTenderID
										FROM tbl_TenderSectionDocs 
										where tenderSectionId = @v_tenderSectionId_old
													
											DECLARE cur_DocumentForms cursor FAST_FORWARD For
												select tenderFormId from tbl_TenderForms where  (formstatus is null or formstatus not in ('c','cp')) and tenderSectionId = @v_tenderSectionId_old
												OPEN cur_DocumentForms
													FETCH NEXT FROM cur_DocumentForms INTO @v_tenderFormId_old
														WHILE @@Fetch_status = 0
														BEGIN
															INSERT INTO tbl_TenderForms (tenderSectionId ,templateFormId,filledBy, 
															formName, formHeader, formFooter, noOfTables,isMultipleFilling, 
															isEncryption, isPriceBid, isMandatory, formStatus,pkgLotId, FormType)
															select @v_tenderSectionId , templateFormId , filledBy , formName , 
															formHeader,formFooter , noOfTables , isMultipleFilling , isEncryption , 
															isPriceBid , isMandatory , formStatus , 
															(case WHEN pkgLotId<>0 then @v_TenderLotId_Int ELSE pkgLotId END) , FormType  
															from tbl_TenderForms tf where tenderFormId = @v_tenderFormId_old
														
															select @v_tenderFormId =MAX(tenderFormId) FROM tbl_TenderForms 
															where tenderSectionId = @v_tenderSectionId 
															
															INSERT INTO tbl_TenderMandatoryDoc (tenderId,tenderFormId,templateFormId,
															documentName,createdBy,createdDate)
															SELECT  @v_DerivedTenderID, @v_tenderFormId, templateFormId, 
															documentName, @v_CreatedBy_inInt, GETDATE() FROM tbl_TenderMandatoryDoc
															where tenderId = @v_TenderId_inInt and tenderFormId = @v_tenderFormId_old
														
															INSERT INTO tbl_TenderEnvelopes(tenderId,envelopeId,tenderFormId)
															SELECT @v_DerivedTenderID,envelopeId,@v_tenderFormId 
															FROM tbl_TenderEnvelopes where tenderFormId = @v_tenderFormId_old
															
																DECLARE cur_TenderTable cursor FAST_FORWARD For
																	SELECT tenderTableId FROM tbl_TenderTables where tenderFormId = @v_tenderFormId_old
																	OPEN cur_TenderTable
																		FETCH NEXT FROM cur_TenderTable INTO @v_tenderTableId_old
																		WHILE @@Fetch_status = 0
																		BEGIN
																		
																			INSERT INTO tbl_TenderTables(tenderFormId,tableName,tableHeader,tableFooter,
																			noOfRows,noOfCols,isMultipleFilling,templatetableId)
																			SELECT @v_tenderFormId,tableName,tableHeader,tableFooter,
																			noOfRows,noOfCols,isMultipleFilling,templatetableId 
																			FROM tbl_TenderTables where tenderTableId=@v_tenderTableId_old
																			
																			select @v_tenderTableId =MAX(tenderTableId) FROM tbl_TenderTables 
																			where tenderFormId = @v_tenderFormId  
																			
																			INSERT INTO tbl_TenderColumns(tenderTableId,columnId,columnHeader,
																			dataType,filledBy,columnType,sortOrder,showorhide,templateTableId)
																			SELECT @v_tenderTableId,columnId,columnHeader,dataType,filledBy,
																			columnType,sortOrder,showorhide,templateTableId FROM tbl_TenderColumns
																			where tenderTableId=@v_tenderTableId_old
																			
																			INSERT INTO tbl_TenderCells(tenderColId,tenderTableId,rowId,
																			cellDatatype,cellvalue,columnId,templateTableId,templateColumnId,cellId)
																			SELECT tenderColId,@v_tenderTableId,rowId,cellDatatype,cellvalue,columnId,
																			templateTableId,templateColumnId,cellId FROM tbl_TenderCells 
																			where tenderTableId = @v_tenderTableId_old
																			
																			INSERT INTO tbl_TenderFormula(tenderFormId,tenderTableId,columnId,formula,isGrandTotal)
																			SELECT @v_tenderFormId,@v_tenderTableId,columnId,formula,isGrandTotal FROM tbl_TenderFormula
																			where tenderTableId = @v_tenderTableId_old
																												
																				DECLARE cur_List cursor FAST_FORWARD For
																				SELECT tenderListId FROM tbl_TenderListBox where tenderFormId = @v_tenderFormId_old
																					OPEN cur_List
																					FETCH NEXT FROM cur_List INTO @v_tenderListId_old
																						WHILE @@Fetch_status = 0
																						BEGIN
																		
																							INSERT INTO tbl_TenderListBox(listBoxName,tenderFormId, listBoxId, isCalcReq)
																							SELECT listBoxName,@v_tenderFormId,listBoxId,isCalcReq FROM tbl_TenderListBox 
																							where tenderListId=@v_tenderListId_old
																			
																							select @v_tenderListId =MAX(tenderListId) FROM tbl_TenderListBox 
																							where tenderFormId = @v_tenderFormId 
																			
																							INSERT INTO tbl_TenderListDetail(tenderListId,itemId,itemValue,itemText,isDefault)
																							SELECT 	@v_tenderListId,itemId,itemValue,itemText,isDefault FROM tbl_TenderListDetail
																							where tenderListId=@v_tenderListId_old	
																							
																							INSERT INTO tbl_TenderListCellDetail(tenderListId,tenderTableId,columnId,cellId,location)
																							SELECT @v_tenderListId,@v_tenderTableId,columnId,cellId,location FROM tbl_TenderListCellDetail
																							where tenderListId=@v_tenderListId_old						
																			
																							FETCH NEXT FROM cur_List INTO @v_tenderListId_old
																						END
																					CLOSE cur_List
																				DEALLOCATE cur_List
																				
																				
														
																			FETCH NEXT FROM cur_TenderTable INTO @v_tenderTableId_old
																		END
																	CLOSE cur_TenderTable
																DEALLOCATE cur_TenderTable
																
																
														FETCH NEXT FROM cur_DocumentForms INTO @v_tenderFormId_old
														END
												CLOSE cur_DocumentForms
											DEALLOCATE cur_DocumentForms
											
											
											DECLARE cur_IttHeader cursor FAST_FORWARD For
												select tenderIttHeaderId from tbl_TenderIttHeader where tenderSectionId =  @v_tenderSectionId_old
												OPEN cur_IttHeader
													FETCH NEXT FROM cur_IttHeader INTO @v_tenderIttHeaderId_old
														WHILE @@Fetch_status = 0
														BEGIN
															
															INSERT INTO tbl_TenderIttHeader (templateIttHeaderId,ittHeaderName,
															tenderSectionId,temlateSectionId,tenderId)
															SELECT templateIttHeaderId,ittHeaderName,@v_tenderSectionId,
															temlateSectionId,@v_DerivedTenderID FROM tbl_TenderIttHeader
															WHERE tenderIttHeaderId = @v_tenderIttHeaderId_old
															
															select @v_tenderIttHeaderId = MAX(tenderIttHeaderId) from tbl_TenderIttHeader 
															where tenderSectionId =  @v_tenderSectionId
															
																DECLARE cur_IttClause cursor FAST_FORWARD For
																	select tenderIttClauseId from tbl_TenderIttClause where tendederIttHeaderId = @v_tenderIttHeaderId_old
																	OPEN cur_IttClause
																		FETCH NEXT FROM cur_IttClause INTO @v_tenderIttClauseId_old
																			WHILE @@Fetch_status = 0
																			BEGIN
																				
																				INSERT INTO tbl_TenderIttClause(tendederIttHeaderId,ittClauseName,tempplateIttClauseId)
																				SELECT @v_tenderIttHeaderId,ittClauseName,tempplateIttClauseId 
																				from tbl_TenderIttClause
																				WHERE tenderIttClauseId = @v_tenderIttClauseId_old
																				
																				select @v_tenderIttClauseId = MAX(tenderIttClauseId) from tbl_TenderIttClause 
																				where tendederIttHeaderId = @v_tenderIttHeaderId
																				
																				DECLARE cur_IttSubClause cursor FAST_FORWARD For
																					select tenderIttSubClauseId from tbl_TenderIttSubClause where tenderIttClauseId = @v_tenderIttClauseId_old
																					OPEN cur_IttSubClause
																					FETCH NEXT FROM cur_IttSubClause INTO @v_tenderIttSubClauseId_old
																						WHILE @@Fetch_status = 0
																						BEGIN
																				
																				
																							INSERT INTO tbl_TenderIttSubClause
																							(tenderIttClauseId,ittSubClauseName,isTdsApplicable,templateIttSubClauseId)
																							SELECT @v_tenderIttClauseId,ittSubClauseName,isTdsApplicable,
																							templateIttSubClauseId FROM tbl_TenderIttSubClause 
																							WHERE tenderIttSubClauseId = @v_tenderIttSubClauseId_old
																							
																							select @v_tenderIttSubClauseId = MAX(tenderIttSubClauseId) from tbl_TenderIttSubClause 
																							where tenderIttClauseId =@v_tenderIttClauseId
																																								
																							INSERT INTO tbl_TenderTdsSubClause
																							(tenderIttRefrence,orderNumber,tenderIttHeaderId,templateTdsSubClauseId,tenderTdsClauseName)
																							SELECT @v_tenderIttSubClauseId , orderNumber,@v_tenderIttHeaderId,templateTdsSubClauseId,tenderTdsClauseName
																							FROM tbl_TenderTdsSubClause 
																							WHERE tenderIttRefrence = @v_tenderIttSubClauseId_old and tenderIttHeaderId=@v_tenderIttHeaderId_old
																					
																						FETCH NEXT FROM cur_IttSubClause INTO @v_tenderIttSubClauseId_old
																						END
																					CLOSE cur_IttSubClause
																				DEALLOCATE cur_IttSubClause
																									
																				FETCH NEXT FROM cur_IttClause INTO @v_tenderIttClauseId_old
																			END
																	CLOSE cur_IttClause
																DEALLOCATE cur_IttClause
															
															INSERT INTO tbl_TenderTdsSubClause
															(tenderIttRefrence,orderNumber,tenderIttHeaderId,templateTdsSubClauseId,tenderTdsClauseName)
															SELECT tenderIttRefrence , orderNumber,@v_tenderIttHeaderId,templateTdsSubClauseId,tenderTdsClauseName
															FROM tbl_TenderTdsSubClause 
															WHERE tenderIttRefrence = 0 and tenderIttHeaderId=@v_tenderIttHeaderId_old
																							
															FETCH NEXT FROM cur_IttHeader INTO @v_tenderIttHeaderId_old
														END
												CLOSE cur_IttHeader
											DEALLOCATE cur_IttHeader
											
										FETCH NEXT FROM cur_Section1 INTO @v_tenderSectionId_old
									END
							CLOSE cur_Section1
						DEALLOCATE cur_Section1


						FETCH NEXT FROM cur1 INTO @v_TenderLotId_Int
					END

				CLOSE cur1
				DEALLOCATE cur1

			END

			If @v_TenderStdId_Int is Not Null
			Begin
				Set @v_flag_bit=1
			   --Select @v_flag_bit as FieldValue1, 'Record inserted.' as FieldValue2, @v_TenderStdId_Int as Id
			End
			Else
			Begin
				Set @v_flag_bit=0
				--Select @v_flag_bit as FieldValue1, 'Record not inserted. Document available method is not specified.' as FieldValue2, 0 as Id
			End

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		BEGIN
				If Cursor_Status ('Global', 'cur1')=1
				Begin
					close cur1
					Deallocate cur1
				End
				If Cursor_Status ('Global', 'cur1')=-1
				Begin
					Deallocate cur1
				End
				
				If Cursor_Status ('Global', 'cur_List')=1
				Begin
					close cur_List
					Deallocate cur_List
				End
				
				If Cursor_Status ('Global', 'cur_List')=-1
				Begin
					Deallocate cur_List
				End
				
				If Cursor_Status ('Global', 'cur_TenderTable')=1
				Begin
					close cur_TenderTable
					Deallocate cur_TenderTable
				End
				
				If Cursor_Status ('Global', 'cur_TenderTable')=-1
				Begin
					Deallocate cur_TenderTable
				End
				
				If Cursor_Status ('Global', 'cur_DocumentForms')=1
				Begin
					close cur_DocumentForms
					Deallocate cur_DocumentForms
				End
				
				If Cursor_Status ('Global', 'cur_DocumentForms')=-1
				Begin
					Deallocate cur_DocumentForms
				End
				
				
				If Cursor_Status ('Global', 'cur_IttSubClause')=1
				Begin
					close cur_IttSubClause
					Deallocate cur_IttSubClause
				End
				
				If Cursor_Status ('Global', 'cur_IttSubClause')=-1
				Begin
					Deallocate cur_IttSubClause
				End
				
				If Cursor_Status ('Global', 'cur_IttClause')=1
				Begin
					close cur_IttClause
					Deallocate cur_IttClause
				End
				
				If Cursor_Status ('Global', 'cur_IttClause')=-1
				Begin
					Deallocate cur_IttClause
				End
				
				If Cursor_Status ('Global', 'cur_IttHeader')=1
				Begin
					close cur_IttHeader
					Deallocate cur_IttHeader
				End
				
				If Cursor_Status ('Global', 'cur_IttHeader')=-1
				Begin
					Deallocate cur_IttHeader
				End
								
				If Cursor_Status ('Global', 'cur_Section1')=1
				Begin
					close cur_Section1
					Deallocate cur_Section1
				End
				
				If Cursor_Status ('Global', 'cur_Section1')=-1
				Begin
					Deallocate cur_Section1
				End
				
				
			--Set @v_flag_bit=0
			--Select '0' as FieldValue1, ERROR_MESSAGE() as FieldValue2
			ROLLBACK TRAN
		END
	END CATCH
END
SET NOCOUNT OFF;
END


GO
