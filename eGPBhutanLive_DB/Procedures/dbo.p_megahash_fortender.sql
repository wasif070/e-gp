SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: generate mega hash by tenderid and userid
--
--
-- Author: Krishnraj
-- Date: 05-02-2011
--
-- Last Modified:
-- Modified By:
-- Date
--------------------------------------------------------------------------------
-- SP Name	   :   p_megahash_fortender
-- Module	   :   Tender Notice
-- Description :   Generates Mega Hash for bidder's bid submission for the tender
-- Function	   :   N.A.
-- EXEC [dbo].[p_megahash_fortender] 1499
CREATE PROC [dbo].[p_megahash_fortender]
@v_status_inInt		INT,
@v_tenderId_inInt	INT,
@v_userId_inInt		INT

AS

BEGIN

	SET NOCOUNT ON;

	-- DECLARE REQUIRED VARIABLE
	DECLARE @v_tenderMegaHash_Vc VARCHAR(MAX), @v_tenderHash_Vc VARCHAR(MAX), @v_finalSubmissionId_Int INT, @v_formHash_Vc VARCHAR(MAX),
	@v_tenderFormId_Int INT, @v_companyDocId_Int INT, @v_docHash_Vc VARCHAR(MAX), @v_finalSubDetailId_Int INT, @v_newFormCombinedHash_Vc VARCHAR(MAX)
	,@v_flag_bit bit, @v_RetId_Int int, @FieldLen INT = 0
	IF @v_status_inInt = 1
	BEGIN

		BEGIN TRY

			-- DECLARE CURSOR TO GO THRU EACH SubmissionId WHOSE bidSubStatus ARE NOT "withdrawal"
			DECLARE curFinalSubId CURSOR FAST_FORWARD READ_ONLY FOR SELECT finalSubmissionId FROM dbo.tbl_FinalSubmission
				WHERE tenderId = @v_tenderId_inInt and userId = @v_userId_inInt and bidSubStatus <> 'withdrawal'

			OPEN curFinalSubId

			FETCH NEXT FROM curFinalSubId INTO @v_finalSubmissionId_Int
			WHILE @@FETCH_STATUS = 0
			BEGIN

				-- FETCH tenderFomrID and formHash
				DECLARE curTenderFormid CURSOR FAST_FORWARD READ_ONLY FOR SELECT finalSubDetailId, tenderFormid, signText FROM dbo.tbl_FinalSubDetail
					INNER JOIN dbo.tbl_TenderBidSign ON dbo.tbl_FinalSubDetail.bidId = dbo.tbl_TenderBidSign.bidId
					WHERE finalSubmissionId = @v_finalSubmissionId_Int

				OPEN curTenderFormid

				FETCH NEXT FROM curTenderFormid INTO @v_finalSubDetailId_Int, @v_tenderFormId_Int , @v_formHash_Vc
				WHILE @@FETCH_STATUS = 0
				BEGIN

					IF EXISTS (SELECT companyDocId FROM dbo.tbl_BidDocuments
						WHERE tenderId = @v_tenderId_inInt and userId = @v_userId_inInt and formId = @v_tenderFormId_Int)
					BEGIN

							SELECT @v_docHash_Vc = ISNULL(@v_docHash_Vc + '_' + docHash, docHash) FROM dbo.tbl_CompanyDocuments
								WHERE companyDocId IN (SELECT companyDocId FROM dbo.tbl_BidDocuments
								WHERE tenderId = @v_tenderId_inInt and userId = @v_userId_inInt and formId = @v_tenderFormId_Int)

						--SELECT docHash FROM dbo.tbl_CompanyDocuments WHERE companyDocId = @v_companyDocId_Int
					END

						-- BEGIN THE TRANSACTION
					BEGIN TRAN

						SELECT @v_docHash_Vc = @v_formHash_Vc + Isnull('_' + @v_docHash_Vc, '')

						-- NOW GENERATE SHA1 OF GENERATED HASH
						/* Added By Dohatec Starts For TEnder hash for data legth more than 8000*/
						SET @FieldLen  = DATALENGTH(@v_docHash_Vc)
						IF @FieldLen >8000
						BEGIN
							SELECT @v_tenderHash_Vc = SUBSTRING(MASTER.dbo.fn_varbintohexstr(dbo.f_get_LongHash(@v_docHash_Vc)), 3, 41)
						END
						ELSE
						BEGIN
							SELECT @v_tenderHash_Vc = SUBSTRING(MASTER.dbo.fn_varbintohexstr(HASHBYTES('SHA1', @v_docHash_Vc)), 3, 41)
						END
						/* Added By Dohatec Ends For TEnder hash for data legth more than 8000*/
						
						-- UPDATE NEW GENERATED HASH IN TABLE
						UPDATE dbo.tbl_FinalSubDetail SET newFormCombinedHash = @v_tenderHash_Vc
							WHERE finalSubDetailId = @v_finalSubDetailId_Int

						-- RESET THE @v_tenderHash_Vc VARIABLE TO NULL
						SET @v_docHash_Vc = NULL
					COMMIT TRAN

					FETCH NEXT FROM curTenderFormid INTO @v_finalSubDetailId_Int, @v_tenderFormId_Int, @v_formHash_Vc

				END

				CLOSE curTenderFormid
				DEALLOCATE curTenderFormid

				BEGIN TRAN

					SELECT @v_newFormCombinedHash_Vc = ISNULL(@v_newFormCombinedHash_Vc + '_' + newFormCombinedHash, newFormCombinedHash) FROM dbo.tbl_FinalSubDetail
								WHERE finalSubmissionId = @v_finalSubmissionId_Int AND ISNULL(newFormCombinedHash, '') <> ''

					-- NOW GENERATE SHA1 OF GENERATED HASH
					SELECT @v_tenderMegaHash_Vc = SUBSTRING(MASTER.dbo.fn_varbintohexstr(HASHBYTES('SHA1', @v_newFormCombinedHash_Vc)), 3, 41)

					-- NOW UPDATE NEWLY GENERATED HASH
					UPDATE dbo.tbl_FinalSubmission SET tenderHash = @v_tenderMegaHash_Vc WHERE finalSubmissionId = @v_finalSubmissionId_Int

				COMMIT TRAN

				SET @v_newFormCombinedHash_Vc = NULL

				FETCH NEXT FROM curFinalSubId INTO @v_finalSubmissionId_Int

			END

			CLOSE curFinalSubId
			DEALLOCATE curFinalSubId
			Select @v_flag_bit=1, @v_RetId_Int=0
			SELECT @v_flag_bit AS flag, 'Mega hash generated successfully.' AS Message, @v_RetId_Int as Id

		END TRY
		BEGIN CATCH

			ROLLBACK TRAN
                            BEGIN TRY
                                UPDATE dbo.tbl_FinalSubmission SET bidSubStatus = 'pending'
                                WHERE tenderId = @v_tenderId_inInt and userId = @v_userId_inInt and bidSubStatus = 'finalsubmission'
                            END TRY
                            BEGIN CATCH
                                ROLLBACK TRAN
                            END CATCH
			Select @v_flag_bit=0, @v_RetId_Int=0
			SELECT @v_flag_bit AS flag, 'Error while generating megahash. ' + ERROR_MESSAGE() AS Message, @v_RetId_Int as Id

		END CATCH

	END
	ELSE
	BEGIN

		BEGIN TRY

			-- CHECK WHETHER THIS TENDERID EXISTS IN tbl_FinalSubmission OR NOT
			IF EXISTS (SELECT 1 FROM dbo.tbl_FinalSubmission WHERE tenderId = @v_tenderId_inInt AND bidSubStatus = 'finalsubmission')
			BEGIN

				BEGIN TRAN

					-- FETCH TENDER-HASH
					SELECT @v_tenderHash_Vc = ISNULL(@v_tenderHash_Vc + '_' + tenderHash, tenderHash) FROM dbo.tbl_FinalSubmission
					WHERE tenderId = @v_tenderId_inInt AND bidSubStatus = 'finalsubmission' GROUP BY userId ,tenderHash order by userId

					-- NOW GENERATE SHA1 OF GENERATED HASH
					/* Added By Dohatec Start For TEnder hash for data legth more than 8000*/
						SET @FieldLen  = DATALENGTH(@v_tenderHash_Vc)
						IF @FieldLen >8000
						BEGIN
							SELECT @v_tenderHash_Vc = SUBSTRING(MASTER.dbo.fn_varbintohexstr(dbo.f_get_LongHash(@v_tenderHash_Vc)), 3, 41)
						END
						ELSE
						BEGIN
							SELECT @v_tenderHash_Vc = SUBSTRING(MASTER.dbo.fn_varbintohexstr(HASHBYTES('SHA1', @v_tenderHash_Vc)), 3, 41)
						END	
					/* Added By Dohatec Ends For TEnder hash for data legth more than 8000*/
					delete from tbl_TenderMegaHash WHERE tenderId = @v_tenderId_inInt
					-- NOW INSERT THIS GENERATED HASH INTO tbl_TenderMegaHash
					INSERT INTO dbo.tbl_TenderMegaHash (tenderId, megaHash, megaHashDt)
						SELECT @v_tenderId_inInt, @v_tenderHash_Vc, GETDATE()

					-- RESET THE @v_tenderHash_Vc VARIABLE TO NULL
					SET @v_tenderHash_Vc = NULL
					Select @v_flag_bit=1, @v_RetId_Int=1
					SELECT @v_flag_bit AS flag, 'Mega hash generated successfully.' AS Message, @v_RetId_Int as Id

				COMMIT TRAN

			END
			ELSE
			Begin
				Select @v_flag_bit=0, @v_RetId_Int=1
				SELECT @v_flag_bit AS flag, 'Data not found.' AS Message, @v_RetId_Int as Id
			End

		END TRY
		BEGIN CATCH

			ROLLBACK TRAN
                            BEGIN TRY
                                UPDATE dbo.tbl_FinalSubmission SET bidSubStatus = 'pending'
                                WHERE tenderId = @v_tenderId_inInt and userId = @v_userId_inInt and bidSubStatus = 'finalsubmission'
                            END TRY
                            BEGIN CATCH
                                ROLLBACK TRAN
                            END CATCH
			Select @v_flag_bit=0, @v_RetId_Int=0
			SELECT @v_flag_bit AS flag, 'Error while generating megahash. ' + ERROR_MESSAGE() AS Message, @v_RetId_Int as Id

		END CATCH

	END

END
GO
