SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Get Form Data
--
--
-- Author: Sachin Patel / Sanjay Prajapati
-- Date: 11-16-2010 / 11-17-2010
--exec [p_getformdata] 0,0,439,0,0,3335,'checkGrandSumNeedAtCorri'

-- SP Name:  p_getformdata
-- Module:   Bid Preparation, Bid Submission
-- Function: Store Procedure is use for get tender form common data.

--------------------------------------------------------------------------------
-- GetTableDataFormWise: Get tender table details on the basis of given form id.
-- GetColumnData: 	 Get Tender column details on the basis of given tender table id.
-- GetFormula: 	 	 Get Formula details on the basis of given tender table id.
-- GetColumnCount: 	 Get column count for given tender table id.
-- GetTableColumn:	 Get tender table column details for given table id.
-- GetColumnNo:		 Get column count for given tender table id.
-- GetRowNo:		 Get row count for given tender table id.
-- GetAutoColumn:	 Get Auto column details for given tender table id.
-- GetTableDataTableWise:  Get Tender table details for given tender table id.
-- GetAllTableCount: 	 Get no of tables for given tender form id.
-- GetBidCell:		 Get bid cell details for given tender table id.
-- GetNegBidCell:	 Get Negotiation bid cell details for given tender table id.
-- GetBidCount:		 Get bid count for given tender form id and given user id.
-- GetBidSignData:	 Get tender bid sign details for given tender form id, bidid and user id.
-- GetBidTableId:	 Get bid table id on the basis of given bid id, tender tableid and user id.
-- GetBidData:		 Get tender bid details for given bid id and user id.
-- GetFormData:		 Get Tender form details on the basis of given form id.
-- GetBidCellData:	 Get Tender bid cell details on the basis of given bid id, user id, table id.
-- GetBidCellCnt:	 Get no of bid cell count for given table id, which is not filled by 'PE'.
-- GetBidTable:		 Get no of bid cell count for given table id, which is not filled by 'PE'.
-- GetNegBidData:	 Get Negotiation bid data for given bid id and user id.
-- GetNegColumns:	 Get column data for negotiation on the basis of given bid id.
-- GetNegTableColumn:	 Get Table column details for Negotiation on the basis of given table id.
-- GetBidPlainData:	 Get Bid Plain data on the basis of given bid id and user id.
-- GetNegBidderBidData:	 Get bid data for negotiation on the basis of given bid id and user id.
-- GetNegBidPlainData:	 Get negotiation bid data  on the basis of given bid id and user id.
-- GetNegBidTableId:	 Get negotiation bid table id for given tender table id.
-- GetNegBidSrcData:	 Get negotiation bid data for tender type="service" on the basis of given neg bid form id.
-- CheckExistInGrandSum: Use to check table is exist in grand summary for given form id and table id.
-- CheckExistInCorriGrandSum:	Use to check table is exist in grand summary for given form id and table id and 		
--						        corrigendum id.
-- checkGrandSumNeedAtCorri:	Use to check, at the time of corri publish or workflow publish user need to edit Grand 		
--								Summary if he has already prepare GS and he has deleted existing form of 
--								Grand Summary or He has added new price bid form?
-------------------------------------------------------------------------------- 
                    
CREATE PROCEDURE [dbo].[p_getformdata]
	@v_tableId int=0,
	@v_formId int=0,
	@v_columnId int=0,
	@v_userId int=0,
	@v_bidId int=0,
	@v_tenderId int=0,
	@v_funcNameinVc varchar(30)=NULL
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
	Declare @v_tenderSumId int =0 ,@v_message varchar(500) = '', @v_cnt int =0 , @v_cnt1 int =0
	
 
	
IF @v_funcNameinVc = 'GetTableDataFormWise'  
BEGIN
select    tenderTableId, tableName  ,tableHeader,
tablefooter from 
tbl_TenderTables where tenderFormId=@v_formId
END 
IF @v_funcNameinVc = 'GetColumnData'  
BEGIN
select   columnId,columnHeader,dataType,filledBy,columnType,showorhide  from 
tbl_TenderColumns where tenderTableId=@v_tableId 
END 
IF @v_funcNameinVc = 'GetCellData'  
BEGIN
select tc2.columnId as columnId,tc2.rowId as rowId,tc2.cellDatatype as cellDatatype,tc2.cellValue as cellValue, tc2.cellId as cellId,tc1.columnType as columnType 
from tbl_TenderColumns tc1,tbl_TenderCells tc2 where tc1.columnId = tc2.columnId 
and tc1.tenderTableId = tc2.tenderTableId 
and tc1.tenderTableId = @v_tableId order by tc2.rowId,tc2.columnId
END 
IF @v_funcNameinVc = 'GetFormula'  
BEGIN
select tenderFormulaId,tenderformId,tenderTableId,columnId,formula,isGrandTotal 
from tbl_TenderFormula where tenderTableId=@v_tableId order by  columnId,tenderFormulaId
END 
IF @v_funcNameinVc = 'GetColumnCount'  
BEGIN
select count(columnId) as cnt 
from tbl_TenderColumns where tenderTableId = @v_tableId
END 
IF @v_funcNameinVc = 'GetTableColumn'  
BEGIN
select columnId,columnHeader,dataType,filledBy,columnType,showorhide
from tbl_TenderColumns where tenderTableId = @v_tableId order by sortOrder
END 
IF @v_funcNameinVc = 'GetColumnNo'  
BEGIN
select count(columnId) as cnt from 
tbl_TenderColumns where tenderTableId = @v_tableId
END 
IF @v_funcNameinVc = 'GetRowNo'  
BEGIN
if((select COUNT(t.tenderTableId) from tbl_TenderTables t,tbl_TenderFormula tf
where t.tenderTableId= @v_tableId and t.tendertableid=tf.tendertableid
  and t.isMultipleFilling='yes')!=0)
  begin
  
select count(rowId) as cnt from 
tbl_TenderCells where tenderTableId = @v_tableId and columnId = @v_columnId

end
else
begin
select count(rowId) as cnt from 
tbl_TenderCells where tenderTableId = @v_tableId and columnId = @v_columnId
end
END 
IF @v_funcNameinVc = 'GetAutoColumn'  
BEGIN
select ttc.columnId, ttc.columnHeader from tbl_TenderColumns ttc where 
ttc.templateTableId = @v_tableId and ttc.filledBy = 3 and ttc.columnId not in 
(select ttf.columnId from tbl_TenderFormula ttf where ttf.tenderTableId = @v_tableId)
END 
IF @v_funcNameinVc = 'GetTableDataTableWise'  
BEGIN
select   tenderTableId, tableName  ,tableHeader,tablefooter,noOfCols,noOfRows,isMultipleFilling from 
tbl_TenderTables where tendertableId = @v_tableId 
END 
IF @v_funcNameinVc = 'GetAllTableCount'  
BEGIN
select count(tenderTableId) as cnt from 
tbl_TenderTables where tenderFormId=@v_formId 
END 
IF @v_funcNameinVc = 'GetBidCell'  
BEGIN
select tc.columnId,columnHeader,dataType,filledBy,columnType,sortOrder,tc.showorhide,tenderCelId,rowId,cellDatatype,cellValue,cellId from 
tbl_TenderColumns tc,tbl_TenderCells tcv where 
tc.tenderTableId = tcv.tenderTableId  and tc.columnId = tcv.columnId 
and (filledBy = 2 or filledBy = 3)
and tc.tenderTableId = @v_tableId order by rowId,columnId
END 
IF @v_funcNameinVc = 'GetNegBidCell'  
BEGIN

select * from (select tc.columnId,columnHeader,dataType,filledBy,columnType,sortOrder,tc.showorhide,tenderCelId,rowId,cellDatatype,cellValue,cellId from 
tbl_TenderColumns tc,tbl_TenderCells tcv where 
tc.tenderTableId = tcv.tenderTableId  and tc.columnId = tcv.columnId 
and (filledBy = 1) and columnType=2 and tc.tenderTableId = @v_tableId
union
select tc.columnId,columnHeader,dataType,filledBy,columnType,sortOrder,tc.showorhide,tenderCelId,rowId,cellDatatype,cellValue,cellId from 
tbl_TenderColumns tc,tbl_TenderCells tcv where 
tc.tenderTableId = tcv.tenderTableId  and tc.columnId = tcv.columnId 
and (filledBy = 2 or filledBy = 3)
and tc.tenderTableId = @v_tableId)a order by rowId,columnId
END 
IF @v_funcNameinVc = 'GetBidCount'  
BEGIN
select count(distinct tbt.tenderTableId) as cnt from tbl_TenderBidTable tbt , tbl_TenderBidForm tbf where 
tbt.bidId = tbf.bidId and tbt.tenderFormId = tbf.tenderFormId  
and tbf.tenderFormId = @v_formId  and tbf.userId = @v_userId
END 
IF @v_funcNameinVc = 'GetBidSignData'  
BEGIN
select signText as cellValue from tbl_tenderBidsign bs,tbl_TenderBidForm bf where bs.bidId = bf.bidId and 
bs.bidId= @v_bidId and tenderFormId = @v_formId and userid = @v_userId
END 
IF @v_funcNameinVc = 'GetBidTableId'  
BEGIN
select distinct bidTableId from tbl_TenderBidTable tbt,tbl_TenderBidForm tbf where 
tbt.tenderFormId = tbf.tenderFormId   and tbf.bidId=tbt.bidId
and tbf.userId = @v_userId and tbt.bidId = @v_bidId and tbt.tenderTableId = @v_tableId
order by bidTableId
END 
IF @v_funcNameinVc = 'GetBidData'  
BEGIN
select  btd.BIDTABLEID as bidTableId,bt.tenderTableId as tenderTableId, btd.cellId as cellId ,
 btd.cellValue as cellValue,btd.rowId as rowId
from tbl_TenderBidDetail btd,tbl_TenderBidTable bt,tbl_TenderBidForm bf,
tbl_TenderCells tc,tbl_TenderColumns tdc
where bt.bidTableId=btd.bidtableid and bt.bidId=bf.bidid and 
tc.tenderTableId=btd.tenderTableId
and btd.columnId=tc.columnId and btd.cellid=tc.cellid and 
tc.tenderTableId=tdc.tenderTableId 
and tc.columnId=tdc.columnId and bf.bidid=@v_bidId and bf.userId = @v_userId
order by btd.bidTableId ,tc.rowId , sortOrder,tc.columnId
END 
IF @v_funcNameinVc = 'GetFormData'  
BEGIN
select formName,formHeader,formFooter,isMultipleFilling as isMultipleFormFeeling, isEncryption, 
isPriceBid, isMandatory from tbl_TenderForms where tenderFormId = @v_formId
END 
IF @v_funcNameinVc = 'GetBidCellData'  
BEGIN
select  btd.bidTableId,tc.columnId,btd.rowId,cellDatatype,tc.cellValue, btd.cellId
from tbl_TenderBidDetail btd,tbl_TenderBidTable bt,tbl_TenderBidForm bf,
tbl_TenderCells tc,tbl_TenderColumns tdc
where bt.bidTableId=btd.bidtableid and bt.bidId=bf.bidid and 
tc.tenderTableId=btd.tenderTableId
and btd.columnId=tc.columnId and btd.cellid=tc.cellid and 
tc.tenderTableId=tdc.tenderTableId 
and tc.columnId=tdc.columnId and bf.userId = @v_userId and bt.tenderTableId = @v_tableId and bf.bidid=@v_bidId 
order by btd.bidTableId ,tc.rowId , sortOrder,tc.columnId;
END 
IF @v_funcNameinVc = 'GetBidCellCnt'  
BEGIN
select COUNT(cellId) as cnt from tbl_TenderCells tc, tbl_TenderColumns tcl where tc.tenderTableId = tcl.tenderTableId 
and tc.columnId = tcl.columnId  
and tc.tenderTableId = @v_tableId and rowId = 1 and filledBy!=1;
END 
IF @v_funcNameinVc = 'GetBidTable'  
BEGIN
select COUNT(cellId) as cnt from tbl_TenderCells tc, tbl_TenderColumns tcl where tc.tenderTableId = tcl.tenderTableId 
and tc.columnId = tcl.columnId  
and tc.tenderTableId = @v_tableId and rowId = 1 and filledBy!=1;
END 
IF @v_funcNameinVc = 'GetNegBidData'  
BEGIN
select  btd.BIDTABLEID as bidTableId,bt.tenderTableId as tenderTableId, btd.cellId as cellId ,
 btd.cellValue as cellValue,btd.rowId as rowId
from tbl_NegotiatedBid btd,tbl_TenderBidTable bt,tbl_TenderBidForm bf,
tbl_TenderCells tc,tbl_TenderColumns tdc
where bt.bidTableId=btd.bidtableid and bt.bidId=bf.bidid and 
tc.tenderTableId=btd.tenderTableId
and btd.tendercolumnId=tc.columnId and btd.cellid=tc.cellid and 
tc.tenderTableId=tdc.tenderTableId 
and tc.columnId=tdc.columnId  and bf.bidid=@v_bidId and bf.userId = @v_userId
order by btd.bidTableId ,tc.rowId , sortOrder,tc.columnId
END 
IF @v_funcNameinVc = 'GetNegColumns'  
BEGIN
select columnId,columnHeader,dataType,filledBy,columnType,showorhide,bidTableId,t.tenderTableId
from tbl_TenderColumns t,(
select distinct  bidTableId,tbt.tenderTableId from tbl_TenderBidTable tbt,tbl_TenderBidForm tbf where 
tbt.tenderFormId = tbf.tenderFormId   and tbf.bidId=tbt.bidId
  and tbt.tenderTableId = @v_tableId and tbf.bidId=@v_bidId) tb
   where t.tenderTableId = @v_tableId and t.tenderTableId=tb.tendertableid
END 
 IF @v_funcNameinVc = 'GetNegTableColumn'  
BEGIN
select columnId,columnHeader,dataType,filledBy,columnType,showorhide,t.tenderTableId
from tbl_TenderColumns t
   where t.tenderTableId = @v_tableId order by sortOrder
END 
IF @v_funcNameinVc = 'GetBidPlainData'  
BEGIN

select  btd.BIDTABLEID as bidTableId,bt.tenderTableId as tenderTableId, btd.tenderCelId as cellId ,
 case 
						when cellDatatype =9 OR cellDatatype =10
						then (select top 1 itemText from tbl_TenderListCellDetail l,tbl_TenderListDetail ld,
 tbl_TenderBidPlainData p

where 
l.tenderTableId=p.tenderTableId and
 l.columnId=p.tenderColId and
 l.cellId=p.tenderCelId and l.location=p.rowId and 
l.tenderListId=ld.tenderListId   and tc.cellId=p.tenderCelId
and ld.itemValue=p.cellValue and p.bidTableId=btd.bidTableId and btd.tenderTableId=p.tenderTableId   
   ) 

						when btd.cellValue = 'null' then '' else isnull(btd.cellValue,'') end  as cellValue,btd.rowId as rowId
from tbl_TenderBidPlainData btd,tbl_TenderBidTable bt,tbl_TenderBidForm bf,
tbl_TenderCells tc,tbl_TenderColumns tdc
where bt.bidTableId=btd.bidtableid and bt.bidId=bf.bidid and 
tc.tenderTableId=btd.tenderTableId
and btd.tenderColId=tc.columnId and btd.tenderCelId=tc.cellid and 
tc.tenderTableId=tdc.tenderTableId 
and tc.columnId=tdc.columnId and bf.bidid=@v_bidId  and bf.userId = @v_userId
order by btd.bidTableId ,tc.rowId , sortOrder,tc.columnId
END 

IF @v_funcNameinVc = 'GetNegBidderBidData'  
BEGIN
select  btd.BIDTABLEID as bidTableId,bt.tenderTableId as tenderTableId, btd.cellId as cellId ,
 btd.cellValue as cellValue,btd.rowId as rowId
from tbl_NegotiatedbidderBid btd,tbl_TenderBidTable bt,tbl_TenderBidForm bf,
tbl_TenderCells tc,tbl_TenderColumns tdc
where bt.bidTableId=btd.bidtableid and bt.bidId=bf.bidid and 
tc.tenderTableId=btd.tenderTableId
and btd.tendercolumnId=tc.columnId and btd.cellid=tc.cellid and 
tc.tenderTableId=tdc.tenderTableId 
and tc.columnId=tdc.columnId  and bf.bidid=@v_bidId and bf.userId = @v_userId
order by btd.bidTableId ,tc.rowId , sortOrder,tc.columnId
END 
END

IF @v_funcNameinVc = 'GetNegBidPlainData'  
BEGIN
	select  btd.BIDTABLEID as bidTableId,bt.tenderTableId as tenderTableId, btd.tenderCelId as cellId ,
	 case 
	when btd.cellValue = 'null' then '' else isnull(btd.cellValue,'') end  as cellValue,btd.rowId as rowId
	from tbl_TenderBidPlainData btd,tbl_TenderBidTable bt,tbl_TenderBidForm bf,
	tbl_TenderCells tc,tbl_TenderColumns tdc
	where bt.bidTableId=btd.bidtableid and bt.bidId=bf.bidid and 
	tc.tenderTableId=btd.tenderTableId
	and btd.tenderColId=tc.columnId and btd.tenderCelId=tc.cellid and 
	tc.tenderTableId=tdc.tenderTableId 
	and tc.columnId=tdc.columnId and bf.bidid=@v_bidId  and bf.userId = @v_userId
	order by btd.bidTableId ,tc.rowId , sortOrder,tc.columnId
END 
IF @v_funcNameinVc = 'GetNegBidTableId'  -- Added by Dipal For Neg Bid Form 
BEGIN
	select  negBidTableId as bidTableId from tbl_NegBidTable tbt 
	where negBidFormId=@v_formId and tenderTableId=@v_tableId
	order by negBidTableId
END 
IF @v_funcNameinVc = 'GetNegBidSrcData'  -- Added by Dipal For Neg Bid Form 
BEGIN
select a.negBidTableId as bidTableId,a.tenderTableId,b.cellId,b.cellValue,b.rowId
from tbl_NegBidTable a
inner join tbl_NegBidDtlSrv b on a.negBidTableId = b.negBidTableId
where a.negBidFormId=@v_formId
order by a.negBidTableId,b.rowId,b.tenderColumnId
END

IF @v_funcNameinVc = 'CheckExistInGrandSum'  -- Added by Dipal For bug id: 5482
BEGIN
if @v_tableId != 0 
	select  a.tenderSumId as tenderTableId  from tbl_TenderGrandSum a
	inner join tbl_TenderGrandSumDetail b on a.tenderSumId = b.tenderSumId
	where a.tenderId= @v_tenderId and b.tenderFormId=@v_formId and b.tenderTableId=@v_tableId
else
	select  a.tenderSumId as tenderTableId  from tbl_TenderGrandSum a
	inner join tbl_TenderGrandSumDetail b on a.tenderSumId = b.tenderSumId
	where a.tenderId= @v_tenderId and b.tenderFormId=@v_formId
END

IF @v_funcNameinVc = 'CheckExistInCorriGrandSum'  -- Added by Dipal For bug id: 5482
BEGIN
if @v_tableId != 0 
	select  a.tenderSumId as tenderTableId  from tbl_TenderGrandSum a
	inner join tbl_TenderCoriGrandSumDetail b on a.tenderSumId = b.tenderSumId
	where a.tenderId= @v_tenderId and b.tenderFormId=@v_formId and b.tenderTableId=@v_tableId and b.corrigendumId=@v_columnId
else
	select  a.tenderSumId as tenderTableId  from tbl_TenderGrandSum a
	inner join tbl_TenderCoriGrandSumDetail b on a.tenderSumId = b.tenderSumId
	where a.tenderId= @v_tenderId and b.tenderFormId=@v_formId and b.corrigendumId=@v_columnId
END

/* Following if condition will check, at the time of corri publish or workflow publish user
need to edit Grand Summary if he has already prepare gs and he has deleted existing form of Grand Summary.
or He has added new price bid form??????????????
*/
IF @v_funcNameinVc = 'checkGrandSumNeedAtCorri'  -- Added by Dipal For bug id: 5482
BEGIN
	set @v_tenderSumId = 0
	
	set @v_message =''
	
	select @v_tenderSumId = tenderSumId from tbl_TenderGrandSum
	where tenderId=@v_tenderId
	
	print 'tender sum id='+convert(varchar,@v_tenderSumId)
	
	if(@v_tenderSumId != 0) -- If Grand summary Created?
	-- {
		begin
		
			print '1'
			select  @v_cnt1 = a.tenderSumId  from tbl_TenderGrandSum a
			inner join tbl_TenderCoriGrandSumDetail b on a.tenderSumId = b.tenderSumId
			where a.tenderId= @v_tenderId and b.corrigendumId=@v_columnId
			
			print 'corri grand sum created or not grand sum id='+convert(varchar,@v_cnt1)
			
			if(@v_cnt1 = 0) -- If Corrigendum Grand summary not created?
			--{
				begin
					print '2'
					select @v_cnt = COUNT(*)  from tbl_TenderGrandSum a
					inner join tbl_TenderGrandSumDetail b on a.tenderSumId= b.tenderSumId
					inner join tbl_TenderForms c on b.tenderFormId= c.tenderFormId
					where c.formStatus = 'cp' and 
					a.tenderId= @v_tenderId
					
					print 'cancel peending count='+convert(varchar,@v_cnt)
					
					if @v_cnt > 0  -- If Grand Summary exist and form of original grand summary was canceled?
						set @v_message= 'Deleted' 
				end
			-- }			
			
			set @v_cnt=0
				
			if(@v_message = '') -- corri gs not created or original gs has no form canceled...
			--{
				begin
					--select @v_cnt = count (distinct c.tenderFormId) from tbl_TenderStd a
					--inner join tbl_TenderSection b on a.tenderStdId = b.tenderStdId
					--inner join tbl_TenderForms c on b.tenderSectionId= c.tenderSectionId
					--inner join Tbl_TenderFormula d on c.tenderFormId= d.tenderFormId
					--where a.tenderId=@v_tenderId
					--and c.isPriceBid='yes' and c.formStatus='createp'
					--and d.formula like '%TOTAL%' 
					
					--print 'Added new form count='+convert(varchar,@v_cnt)
					
					--if(@v_cnt > 0 )  -- If Grand Summary exist and New Price bid form added?
					--	set @v_message= 'Added'
					--else 
					--	set @v_message= 'No Change' -- No change during Corrigendum
					
					if exists (select c.tenderFormId from tbl_TenderStd a
						inner join tbl_TenderSection b on a.tenderStdId = b.tenderStdId
						inner join tbl_TenderForms c on b.tenderSectionId= c.tenderSectionId
						inner join Tbl_TenderFormula d on c.tenderFormId= d.tenderFormId
						where a.tenderId=@v_tenderId
						and c.isPriceBid='yes' and c.formStatus='createp'
						and d.formula like '%TOTAL%')
						
					--{
						begin
						print 'exists'
						select  @v_cnt= count(*) from tbl_TenderCoriGrandSumDetail 
						where tenderFormId in (
						select c.tenderFormId from tbl_TenderStd a
						inner join tbl_TenderSection b on a.tenderStdId = b.tenderStdId
						inner join tbl_TenderForms c on b.tenderSectionId= c.tenderSectionId
						inner join Tbl_TenderFormula d on c.tenderFormId= d.tenderFormId
						where a.tenderId=@v_tenderId
						and c.isPriceBid='yes' and c.formStatus='createp'
						and d.formula like '%TOTAL%' )

						print 'Added new form count='+convert(varchar,@v_cnt)
					
						if(@v_cnt = 0 )
							set @v_message='NotAddinGrandSummerry'
						else if(@v_cnt <> 0 )  -- If Grand Summary exist and New Price bid form added?
							set @v_message= 'Added'
						else 
							set @v_message= 'No Change' -- No change during Corrigendum
						end
					--}
					
					else
					begin
						print 'not exists'
						set @v_message= 'No Change'
					end
				end
			-- }
		end	
	-- }				
	else -- If Grand summary Not created
		set @v_message = 'GS Not Exist'
		
		
	select @v_message as formula	
		
END


GO
