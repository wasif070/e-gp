SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- exec [p_get_CPV]
-- =============================================
-- Author:		Rajesh Singh
-- alter date: 24th Feb 2011
-- =============================================

-- SP Name	:   p_get_CPV
-- Module	:	Tender (Search), APP (Create, Search)
-- Funftion	:	N.A.

CREATE PROCEDURE [dbo].[p_get_CPV]
	@Tag as varchar(100)=NULL
AS
BEGIN
		Declare @ddlvalue as varchar(20),@parentchild as varchar(3),@parent as varchar(2),@child as varchar(1),@output as varchar(max)
		Declare @finalvalue as varchar(20),@fvalue as varchar(max)
		--select * from dbo.tbl_CpvClassification
	
		set @output =''
		set @fvalue =''
	
		IF @Tag=NULL or @Tag=''
			BEGIN
				select * from dbo.tbl_CpvClassification
			END
		ELSE
			BEGIN
				--Cursor starts here
				DECLARE CustIDCursor CURSOR FOR
				SELECT top 15 CpvCode FROM dbo.tbl_CpvClassification where CpvDescription like '%'+@Tag+'%'
				OPEN CustIDCursor
				FETCH NEXT FROM CustIDCursor INTO @ddlvalue
				WHILE @@FETCH_STATUS = 0
				BEGIN
				
				select @parentchild = @ddlvalue
				set @parent=left(@parentchild,2) --01
				set @child=right(left(@parentchild,3),1)  --1
				
				--print @parentchild+','+@parent+','+@child
				-- Exec p_get_CPV 'and'
				if @child=0  --Find Parents
					BEGIN
					select max(CpvCode) FROM tbl_CpvClassification where cast(CpvCode as varchar(2)) ='01'
						set @finalvalue=@parentchild --Store Parent code -01
					END
				ELSE
					BEGIN
						IF (cast(@finalvalue as varchar(2))=@parent) And @child <> 0 --if it is child
							BEGIN
								set @finalvalue=@finalvalue+','+@parentchild
							END
					END
				--set @fvalue=@fvalue+','+ @finalvalue
				--This is used to Avoid first blank value
				if @output=''
					BEGIN
						set @output=@parentchild
					END
				ELSE
					BEGIN
						set @output=@output + ','+@parentchild
					END
				--This is used to Avoid first blank value
				
				FETCH NEXT FROM CustIDCursor INTO @ddlvalue
				END
				CLOSE CustIDCursor
				DEALLOCATE CustIDCursor
				--Cursor end here
				print @finalvalue
				exec('select * from tbl_CpvClassification where cast(CpvCode as varchar(3)) in ('+@output+')')
			END
			
			 
END


GO
