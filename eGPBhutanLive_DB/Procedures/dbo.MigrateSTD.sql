SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[MigrateSTD]
	@v_templateName VARCHAR(150), 	
	@v_newTemplateName VARCHAR(150)
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

	DECLARE @v_tempalteID INT,
			@v_newTempalteID INT,
			@v_sectionID INT,
			@v_newSectionID INT,
			@v_FormID INT,
			@v_newFormID INT,
			@v_tableID INT,
			@v_newTableID INT,
			@v_ittHeaderID INT,
			@v_newIttHeaderID INT,
			@v_ittClauseID INT,
			@v_newIttClauseID INT,
			@v_ittSubClauseID INT,
			@v_newIttSubClauseID INT,
			@v_tdsSubClauseID INT,
			@v_tableColumnID INT,
			@v_newTableColumnID INT,
			@v_listBoxID INT,
			@v_newListBoxID INT,
			@v_RowCount INT = 0,
			@v_Row INT = 0,
			@v_newRowCount INT = 0,
			@v_newRow INT = 0,
			@v_itemID INT,
			@v_listDetailID INT,
			@v_listcellID INT,
			@v_templateColId INT,
			@errorCode VARCHAR(MAX)
			
	DECLARE	@v_allIDTable TABLE
		(
			rowID INT IDENTITY,
			templateID INT,
			newtemplateID INT,
			sectionID INT,
			newSectionID INT,
			formID INT,
			newFormID INT
		)
				
/*	DECLARE	@v_SectionIDTable TABLE
		(
			rowID INT IDENTITY,
			sectionID INT,
			newSectionID INT
		)
	DECLARE	@v_newSectionIDTable TABLE
		(
			rowID INT IDENTITY,
			sectionID INT
		)
*/		
	DECLARE	@v_FormTable TABLE
		(
			rowID INT IDENTITY,
			FormID INT,
			sectionID INT,
			newFormID INT,
			newSectionID INT		
		)	
	DECLARE	@v_TableIDTable TABLE
		(
			rowID INT IDENTITY,
			tableID INT,
			FormID INT,
			sectionID INT,
			newTableID INT,
			newFormID INT,
			newSectionID INT
		)
	DECLARE	@v_newTableIDTable TABLE
		(
			rowID INT IDENTITY,
			FormID INT,
			sectionID INT
		)
		
	DECLARE	@v_ITTHeadTable TABLE
		(
			rowID INT IDENTITY,
			ittHeaderID INT,
			sectionID INT,
			newIttHeaderid INT,
			newSectionID INT
		)
			
	DECLARE	@v_ITTClauseTbl TABLE
		(
			rowID INT IDENTITY,
			ittClauseID INT,
			ittHeaderID INT,
			newIttHeaderID INT
		)

	DECLARE	@v_ITTClauseTable TABLE
		(
			rowID INT IDENTITY,
			ittHeaderID INT,
			ittClauseID INT,
			ittSubClauseID INT,
			newIttHeaderID INT,
			newIttClauseID INT
		)

	DECLARE	@v_TDSClauseTable TABLE
		(
			rowID INT IDENTITY,
			ittHeaderID INT,
			ittReferanceID INT,
			ittTDSID INT,
			newIttHeaderID INT,
			newIttreferanceID INT
		)		
		
	DECLARE	@v_TableColumnTable TABLE
		(
			rowID INT IDENTITY,
			tableColumnID INT,
			tableID INT,
			formID INT,
			templateColID INT,
			sectionID INT,
			newTableID INT,
			newFormID INT,
			newSectionID INT		
		)
	DECLARE	@v_newTableColumnTable TABLE
		(
			rowID INT IDENTITY,
			tableColumnID INT,
			tableID INT
		)
	DECLARE	@v_ListBoxTable TABLE
		(
			rowID INT IDENTITY,
			listID INT,
			formID INT,
			newListID INT,
			newFormID INT
		)

	DECLARE	@v_ListBoxDetailTable TABLE
		(
			rowID INT IDENTITY,
			listBoxID INT,
			listDetailID INT,
			itemID INT
		)
		
	DECLARE	@v_ListCellDetailTable TABLE
		(
			rowID INT IDENTITY,
			listCellId INT,
			listID INT,
			templateTableId INT,
			columnID INT,
			newTemplateTableId INT,
			newListID INT		
		)	
	DECLARE	@v_templateCellTable TABLE
		(
			rowID INT IDENTITY,
			templateCellId INT,
			tableID INT,
			columnID INT,
			newTableID INT	
		)		
	DECLARE	@v_FormulaTable TABLE
		(
			rowID INT IDENTITY,
			oldTableID INT,
			formulaID INT,
			formId INT,
			tableID INT,
			columnID INT	
		)


	/*********** Insert into TEMPLATE MASTER **********************/
	--select * from tbl_TemplateMaster where templateName = @v_templateName
	if(SELECT COUNT(*) from tbl_TemplateMaster where templateName = @v_newTemplateName) <= 0
	BEGIN
		insert into tbl_TemplateMaster 
		select @v_newTemplateName,noOfSections,'P',procType,stdFor,devPartnerId,'No' from eGPBhutanTestServer.dbo.tbl_TemplateMaster where templateName = @v_templateName

		PRINT 'TEMPLATE MASTER COMPLITED'
		SET @v_newTempalteID=IDENT_CURRENT('tbl_TemplateMaster')

		SELECT @v_tempalteID=templateId FROM eGPBhutanTestServer.dbo.tbl_TemplateMaster where templateName = @v_templateName

		/*********** Insert into TEMPLATE GUIDELINES **********************/
		/*
		insert into tbl_templateGuildeLines
		select [description],docName,@v_newTempalteID,docSize,GETDATE(),uploadedBy from eGPBhutanTestServer.dbo.tbl_templateGuildeLines where templateId = @v_tempalteID
		PRINT 'TEMPLATE GUIDELINES COMPLITED'
		*/	
		--SELECT * FROM tbl_templateGuildeLines where templateId = @v_tempalteID

		/*********** Insert into TEMPLATE SECTION **********************/	
		INSERT INTO @v_allIDTable
		SELECT @v_tempalteID,@v_newTempalteID,sectionId,0,0,0 FROM eGPBhutanTestServer.dbo.tbl_TemplateSections where templateId = @v_tempalteID --order by sectionId

		SET @v_RowCount = @@ROWCOUNT
		SET @v_Row = 1	

		--SELECT * FROM @v_allIDTable

		WHILE	@v_Row <= @v_RowCount
		BEGIN
				SELECT	@v_sectionID = sectionID
				FROM	@v_allIDTable 
				WHERE	rowID = @v_Row
				
				insert into tbl_TemplateSections
				select sectionName,@v_newTempalteID,contentType,isSubSection,status from eGPBhutanTestServer.dbo.tbl_TemplateSections where templateId = @v_tempalteID and sectionId = @v_sectionID
				
				set @v_newSectionID = IDENT_CURRENT('tbl_TemplateSections')
				UPDATE @v_allIDTable set newSectionID = @v_newSectionID where rowID = @v_Row and sectionID = @v_sectionID
				
				PRINT 'TEMPLATE SECTION COMPLITED OLD '+CONVERT(VARCHAR(10),@v_sectionID)+' , New '+CONVERT(VARCHAR(10),@v_newSectionID)
				
				INSERT INTO @v_ITTHeadTable
				SELECT ittHeaderId,sectionId,0,@v_newSectionID FROM eGPBhutanTestServer.dbo.tbl_IttHeader where sectionId =@v_sectionID
				
				SET @v_Row = @v_Row  + 1		
				
		END		
		/*********** Insert into TEMPLATE SECTION DOC AND ITT HEADER**********************/
		SELECT @v_RowCount = COUNT(*) FROM @v_ITTHeadTable 

		--SET @v_RowCount = @@ROWCOUNT
		SET @v_Row = 1	

		WHILE	@v_Row <= @v_RowCount
		BEGIN
				SELECT	@v_sectionID = sectionID,
						@v_newSectionID = newSectionID,
						@v_ittHeaderID = ittHeaderID						
				FROM	@v_ITTHeadTable 
				WHERE	rowID = @v_Row
			/*
				insert into tbl_TemplateSectionDocs
				select description,docName,docSize, @v_newSectionID as sectionID,status,@v_newTempalteID as templateId from eGPBhutanTestServer.dbo.tbl_TemplateSectionDocs where sectionId = @v_sectionID and templateId = @v_tempalteID
			*/
		
				-- ITT Header insertation
				INSERT INTO tbl_IttHeader
				SELECT ittHeaderName,@v_newSectionID FROM eGPBhutanTestServer.dbo.tbl_IttHeader where sectionId = @v_sectionID and ittHeaderId = @v_ittHeaderID
				--SELECT * FROM tbl_IttHeader order by sectionid,ittheaderid
				
				set @v_newIttHeaderID = IDENT_CURRENT('tbl_IttHeader')
				UPDATE @v_ITTHeadTable set newIttHeaderid = @v_newIttHeaderID where rowID = @v_Row and ittHeaderID = @v_ittHeaderID

				INSERT INTO @v_ITTClauseTable
				select	ic.ittHeaderId,
						ic.ittClauseId,
						isc.ittSubClauseId,
						@v_newIttHeaderID,
						0 
				FROM	eGPBhutanTestServer.dbo.tbl_IttClause ic,
						eGPBhutanTestServer.dbo.tbl_IttSubClause isc 
				WHERE	isc.ittClauseId=ic.ittClauseId 
						and ic.ittHeaderId = @v_IttHeaderID

				INSERT INTO @v_TDSClauseTable
--				SELECT ic.ittHeaderId,tds.ittReference,tds.tdsSubClauseId,@v_newIttHeaderID,0 FROM eGPBhutanTestServer.dbo.tbl_IttSubClause ic,eGPBhutanTestServer.dbo.tbl_TdsSubClause tds where tds.ittReference=ic.ittClauseId and ic.ittHeaderId = @v_IttHeaderID
				SELECT	tds.ittHeaderId,
						tds.ittReference,
						tds.tdsSubClauseId,
						@v_newIttHeaderID,
						0 
				FROM	eGPBhutanTestServer.dbo.tbl_IttSubClause isc,	
						eGPBhutanTestServer.dbo.tbl_TdsSubClause tds 
				WHERE	tds.ittReference=isc.ittSubClauseId 
						AND isc.isTdsApplicable = 'yes'
						AND tds.ittHeaderId = @v_IttHeaderID

				SET @v_Row = @v_Row  + 1
		END
		PRINT 'TEMPLATE SECTION DOC AND ITT HEADER COMPLITED'	
		/*********** Insert into TEMPLATE SECTION FORM **********************/	
		--SELECT formid,sectionId,0,@v_newSectionID where 
		INSERT INTO @v_FormTable 
		SELECT tsf.formId,tsf.sectionId,0,tblid.newSectionID from eGPBhutanTestServer.dbo.tbl_TemplateSectionForm tsf
				INNER JOIN @v_allIDTable tblID ON tblID.sectionID = tsf.sectionId
		where tsf.templateId = @v_tempalteID

		SET @v_RowCount = @@ROWCOUNT
		SET @v_Row = 1	

		WHILE	@v_Row <= @v_RowCount
		BEGIN
				
				SELECT	@v_sectionID = sectionID,
						@v_newSectionID = newSectionID,
						@v_FormID = FormID
				FROM	@v_FormTable 
				WHERE	rowID = @v_Row

				insert into tbl_TemplateSectionForm
				select @v_newTempalteID,@v_newSectionID,filledBy, formName,formHeader,formFooter,noOfTables,isMultipleFilling,isEncryption,isPriceBid,status,isMandatory,FormType from eGPBhutanTestServer.dbo.tbl_TemplateSectionForm where formId = @v_FormID and sectionId = @v_sectionID and templateId = @v_tempalteID --order by templateId,sectionId,formId
				
				SET @v_newFormID = IDENT_CURRENT('tbl_TemplateSectionForm')
				UPDATE @v_FormTable SET newFormID = @v_newFormID where rowId = @v_Row and formID = @v_FormID

				INSERT INTO tbl_CMS_TemplateSrvBoqDetail
		--		select @v_newFormID,srvboqid from eGPBhutanTestServer.dbo.tbl_CMS_TemplateSrvBoqDetail where templateformid = @v_FormID
				select srvboqid ,@v_newFormID from eGPBhutanTestServer.dbo.tbl_CMS_TemplateSrvBoqDetail where templateformid = @v_FormID

				SET @v_Row = @v_Row  + 1
		END
		PRINT 'TEMPLATE SECTION FORM AND CMS BOQ DETAIL COMPLITED'	
		/*********** Insert into ITT CLAUSE **********************/	

		--SELECT @v_RowCount = COUNT(*) FROM @v_ITTHeadTable
		INSERT INTO @v_ITTClauseTbl
		SELECT	DISTINCT ittClauseID,
				ittHeaderID,
				newIttHeaderid
		FROM	@v_ITTClauseTable 
		
		SELECT @v_RowCount = COUNT(*) FROM @v_ITTClauseTbl
		SET @v_Row = 1	

--SELECT '@v_ITTClauseTable',* FROM @v_ITTClauseTbl

--		DECLARE @v_tempittclause INT = 0
		WHILE	@v_Row <= @v_RowCount
		BEGIN
				SELECT	@v_IttHeaderID = ittHeaderID,
						@v_newIttHeaderID = newIttHeaderid,
						@v_ittClauseID = ittClauseID
				FROM	@v_ITTClauseTbl 
				WHERE	rowID = @v_Row
		
--				IF @v_tempittclause <> @v_ittClauseID 
--				BEGIN
--					SET @v_tempittclause = @v_ittClauseID
					
					INSERT INTO tbl_IttClause
					SELECT @v_newIttHeaderID,ittClauseName from eGPBhutanTestServer.dbo.tbl_IttClause where ittClauseId = @v_ittClauseID
		
					SET @v_newIttClauseID = IDENT_CURRENT('tbl_IttClause')
--				END
				--SELECT 'OLD',@v_ittClauseID,'NEW ITTCLAUSE', @v_newIttClauseID 
				-- FOR ITT-SUBCLAUSE
				UPDATE @v_ITTClauseTable SET newIttClauseID = @v_newIttClauseID where ittClauseId = @v_ittClauseID
				--UPDATE @v_TDSClauseTable SET newIttClauseID = @v_newIttClauseID where ittClauseId = @v_ittClauseID
				
				SET @v_Row = @v_Row  + 1
		END
		PRINT 'ITT CLAUSE COMPLITED'	
		/*********** Insert into ITT SUB CLAUSE **********************/	


		SELECT @v_RowCount = COUNT(*) FROM @v_ITTClauseTable
		SET @v_Row = 1	

--		INSERT INTO @v_newITTClauseTable
--		select ittClauseId,ittHeaderId from eGPBhutanTestServer.dbo.tbl_IttClause where ittHeaderId in (select ittHeaderId from @v_newITTHeadTable) order by ittHeaderId,ittClauseId
--SELECT '@v_ITTClauseTable',* FROM @v_ITTClauseTable

		WHILE	@v_Row <= @v_RowCount
		BEGIN
				SELECT	@v_ittClauseID = ittClauseID,
						@v_ittSubClauseID = ittSubClauseID,
						@v_newIttClauseID = newittClauseID
				FROM	@v_ITTClauseTable 
				WHERE	rowID = @v_Row

				insert into tbl_IttSubClause
				select @v_newIttClauseID,ittsubClauseName,isTdsApplicable from eGPBhutanTestServer.dbo.tbl_IttSubClause where ittSubClauseId=@v_ittSubClauseId 

				SET @v_newIttSubClauseID = IDENT_CURRENT('tbl_IttSubClause')
				UPDATE @v_TDSClauseTable SET newIttreferanceID = @v_newIttSubClauseID where ittReferanceID = @v_ittSubClauseID				
				
				SET @v_Row = @v_Row  + 1
		END
		PRINT 'ITT SUB CLAUSE COMPLITED'	
		/*********** Insert into TDS SUB CLAUSE **********************/	

		SELECT @v_RowCount = COUNT(*) FROM @v_TDSClauseTable
		SET @v_Row = 1
--SELECT '@v_TDSClauseTable',* FROM @v_TDSClauseTable

		WHILE	@v_Row <= @v_RowCount
		BEGIN
				SELECT	@v_ittHeaderID = ittheaderID,
						@v_ittSubClauseID = ittReferanceID,
						@v_tdsSubClauseID = ittTDSID,
						@v_newIttSubClauseID = newIttreferanceID,
						@v_newIttHeaderID = newIttHeaderID						
				FROM	@v_TDSClauseTable 
				WHERE	rowID = @v_Row

--	select 'TDS',@v_newIttSubClauseID,orderNumber,tdsSubClauseName,@v_newIttHeaderID from eGPBhutanTestServer.dbo.tbl_TdsSubClause where tdsSubClauseId=@v_tdsSubClauseID
				
				insert into tbl_TdsSubClause
				select @v_newIttSubClauseID,orderNumber,tdsSubClauseName,@v_newIttHeaderID from eGPBhutanTestServer.dbo.tbl_TdsSubClause where tdsSubClauseId=@v_tdsSubClauseID
				
				SET @v_Row = @v_Row  + 1
		END
		PRINT 'TDS SUB CLAUSE COMPLITED'	
		/*********** Insert into TEMPLATE MANDATORY DOC **********************/	
		--select * from tbl_TemplateMandatoryDoc where templateId = 25
		-- Old form and Secion ID
		--INSERT INTO @v_FormTable
		--SELECT formId,sectionId,0,0 FROM tbl_TemplateSectionForm where templateId = @v_tempalteID
		SELECT @v_RowCount = COUNT(*) FROM @v_FormTable 
		--SET @v_RowCount = @@ROWCOUNT
		SET @v_Row = 1

		-- New form and Secion ID
		--INSERT INTO @v_newFormTable
		--SELECT formId,sectionId FROM tbl_TemplateSectionForm where templateId = @v_newtempalteID order by templateID,sectionid,formid

		WHILE	@v_Row <= @v_RowCount
		BEGIN
				SELECT	@v_sectionID = sectionID,
						@v_FormID = FormID,
						@v_newSectionID = newSectionID,
						@v_newFormID = newFormID				
				FROM	@v_FormTable 
				WHERE	rowID = @v_Row
				
				insert into tbl_TemplateMandatoryDoc
				select @v_newTempalteID,@v_newSectionID,@v_newFormID,documentName,createdBy,GETDATE() from eGPBhutanTestServer.dbo.tbl_TemplateMandatoryDoc where templateId = @v_tempalteID and templateSectionId = @v_sectionID and templateFormId = @v_FormID
				
				PRINT 'TEMPLATE MANDATORY COMPLITED'	
				
		/*********** Insert into LIST MASTER **********************/	
				INSERT INTO @v_ListBoxTable
				SELECT listBoxId,templateFormId,0,0 FROM eGPBhutanTestServer.dbo.tbl_ListBoxMaster WHERE templateFormId = @v_FormID

				--SET @v_newRowCount = @@ROWCOUNT
				SELECT @v_newRowCount = COUNT(*) FROM @v_ListBoxTable
				SET @v_newRow = 1
				
				WHILE	@v_newRow <= @v_newRowCount
				BEGIN
						SELECT	@v_listBoxID = listID,
								@v_FormID = formID
						FROM	@v_ListBoxTable
						WHERE	rowID = @v_newRow
								AND @v_FormID = formID
						
						INSERT INTO tbl_ListBoxMaster
						SELECT listBoxName,@v_newFormID,isCalcReq from eGPBhutanTestServer.dbo.tbl_ListBoxMaster where templateFormId =  @v_FormID and listBoxId = @v_listBoxID
						
						UPDATE @v_ListBoxTable set newListID = IDENT_CURRENT('tbl_ListBoxMaster'), newFormID = @v_newFormID where formID =  @v_FormID and listId = @v_listBoxID and rowID = @v_newRow
						PRINT 'LISTBOX MASTER COMPLITED'-- FORM '+CONVERT(VARCHAR(10),@v_FormID)+' LIST '+CONVERT(VARCHAR(10),@v_listBoxID)+' ROW '+CONVERT(VARCHAR(10),@v_newRow)
						SET @v_newRow = @v_newRow  + 1
				END		
				SET @v_Row = @v_Row  + 1
		END

		/*********** Insert into TEMPLATE TABLE **********************/	
		--SELECT * FROM tbl_TemplateTables
		INSERT INTO @v_TableIDTable
		SELECT TT.tableId,TT.formId,TT.sectionId,0,ft.newFormID,ft.newSectionID
		FROM eGPBhutanTestServer.dbo.tbl_TemplateTables TT
				INNER JOIN @v_FormTable ft ON ft.FormID = TT.formId and ft.sectionID = TT.sectionId
		where templateId = @v_tempalteID --order by templateId,sectionId,formId,tableId

		SET @v_RowCount = @@ROWCOUNT
		SET @v_Row = 1

		--SELECT '@v_TableIDTable',* FROM @v_TableIDTable

		--INSERT INTO @v_newTableIDTable
		--SELECT formId,sectionId FROM tbl_TemplateTables where templateId = @v_newTempalteID order by templateId,sectionId,formId,tableId
		--select * from @v_newTableIDTable

		WHILE	@v_Row <= @v_RowCount
		BEGIN
				SELECT	@v_sectionID = sectionID,
						@v_FormID = FormID,
						@v_tableID = tableID,
						@v_newSectionID = newSectionID,
						@v_newFormID = newFormID				
				FROM	@v_TableIDTable
				WHERE	rowID = @v_Row
			
--SELECT 'TABLE ID ',@v_tableID,@v_FormID ,@v_sectionID
 							
				insert into tbl_TemplateTables
				select @v_newFormID,@v_newTempalteID,@v_newSectionID,tableName,tableHeader,tableFooter,noOfRows,noOfCols,isMultipleFilling from eGPBhutanTestServer.dbo.tbl_TemplateTables where templateId = @v_tempalteID and sectionId = @v_sectionID and formId = @v_FormID and tableId= @v_tableID
				/*** Update new tableID,FormID etc in temp table which table already having old tableId,FormId et. ***/
				set @v_newTableID = IDENT_CURRENT('tbl_TemplateTables')
				PRINT 'tbl_TemplateTables COMPLITED'	
				
			
				/*********** Insert into TEMPLATE COLUMNS **********************/					
				insert into tbl_TemplateColumns
				select columnId,columnHeader,dataType,filledBy,columnType,sortOrder,@v_newTableID,@v_newFormID,@v_newSectionID,showorhide FROM eGPBhutanTestServer.dbo.tbl_TemplateColumns where sectionId = @v_sectionID and formId = @v_FormID and tableId= @v_tableID
		--		select * FROM tbl_TemplateColumns where sectionId = @v_sectionID and formId = @v_FormID and tableId= @v_tableID
				PRINT 'tbl_TemplateColumns COMPLITED'
				
				INSERT INTO @v_TableColumnTable
				SELECT columnId,tableId,formId,templateColId,sectionId,@v_newTableID,@v_newFormID,@v_newSectionID FROM eGPBhutanTestServer.dbo.tbl_TemplateColumns WHERE sectionId = @v_sectionID and formId = @v_FormID and tableId= @v_tableID
				/*********** Insert into TEMPLATE CELLS **********************/					
				INSERT INTO @v_templateCellTable
				SELECT templateCelId,tableId,columnId,@v_newTableID from eGPBhutanTestServer.dbo.tbl_TemplateCells where tableId = @v_tableID

--SELECT '@v_templateCellTable',* FROM @v_templateCellTable
				
				--SET @v_newRowCount = @@ROWCOUNT
				--SET @v_newRow = 1
				SELECT @v_newRowCount = MAX(rowID) from @v_templateCellTable
				SELECT @v_newRow=MIN(rowID) from @v_templateCellTable

/*				IF @v_newRowCount > 1
				BEGIN
					SET @v_newRowCount = @v_newRow + @v_newRowCount
				END 
				ELSE
				BEGIN
					SET @v_newRowCount = @v_newRow 
				END
*/
				
/*				IF @v_newRowCount > 1
				BEGIN
					IF @v_newRow = 1
					BEGIN
						SET @v_newRowCount = @v_newRowCount
					END
					ELSE
					BEGIN
						SET @v_newRowCount = @v_newRow + @v_newRowCount
					END
				END 
				ELSE
				BEGIN
					SET @v_newRowCount = @v_newRow 
				END
*/
				WHILE	@v_newRow <= @v_newRowCount
				BEGIN
						SELECT	@v_tableColumnID = columnID,
								@v_tableID = tableID,
								@v_templateColId = templateCellId,
								@v_newTableID = newTableID
						FROM	@v_templateCellTable
						WHERE	rowID = @v_newRow

						--SELECT 'template cells',@v_newTableID,@v_tableColumnID,cellId,rowId,cellDatatype,cellvalue,colId,showOrHide FROM tbl_TemplateCells where columnId = @v_tableColumnID and tableId = @v_tableID and templateCelId =@v_templateColId

						INSERT INTO tbl_TemplateCells
						SELECT	@v_newTableID,
								@v_tableColumnID,
								cellId,
								rowId,
								cellDatatype,
								cellvalue,
								colId,
								showOrHide 
						FROM eGPBhutanTestServer.dbo.tbl_TemplateCells 
						where templateCelId =@v_templateColId
						
						PRINT 'tbl_TemplateCells COMPLITED'
						SET @v_newRow = @v_newRow  + 1
				END	
				DELETE FROM @v_templateCellTable
		/*********** Insert into TEMPLATE FORMULAS **********************/	
				INSERT INTO @v_FormulaTable
				SELECT	tf.tableId,formulaId,tct.newFormID,tct.newTableID,tf.columnId
				from	eGPBhutanTestServer.dbo.tbl_TemplateFormulas tf
						INNER JOIN @v_TableColumnTable tct on tct.formID = tf.formId 
									and tct.tableID = tf.tableID 
									and tct.tableColumnID = tf.columnId
				where tf.tableId = @v_tableID
					  and tf.formId = @v_FormID
					  
				DECLARE @v_formulaID INT
				
				--SET @v_newRowCount = @@ROWCOUNT
				--SET @v_newRow = 1
				SELECT @v_newRowCount = MAX(rowID) from @v_FormulaTable
				SELECT @v_newRow=MIN(rowID) from @v_FormulaTable
/*				IF @v_newRowCount > 1
				BEGIN
					IF @v_newRow = 1
					BEGIN
						SET @v_newRowCount = @v_newRowCount
					END
					ELSE
					BEGIN
						SET @v_newRowCount = @v_newRow + @v_newRowCount
					END
				END 
				ELSE
				BEGIN
					SET @v_newRowCount = @v_newRow 
				END				
*/
--				SELECT '@v_FormulaTable',@v_newRow row,@v_newRowCount as rowcnt,* FROM @v_FormulaTable

				WHILE	@v_newRow <= @v_newRowCount
				BEGIN										
						SELECT	@v_tableColumnID = columnID,
								@v_tableID = oldTableID,
								@v_newFormID = formId,
								@v_newTableID = tableID,
								@v_formulaID = formulaID
						FROM	@v_FormulaTable
						WHERE	rowID = @v_newRow
																
--						SELECT 'Formula',@v_newFormID,@v_newTableID,columnID,formula,isGrandTotal FROM eGPBhutanTestServer.dbo.tbl_TemplateFormulas where formulaId = @v_formulaID
						INSERT INTO tbl_TemplateFormulas
						SELECT @v_newFormID,@v_newTableID,columnID,formula,isGrandTotal FROM eGPBhutanTestServer.dbo.tbl_TemplateFormulas where formulaId = @v_formulaID --and tableId = @v_tableID
						PRINT 'tbl_TemplateFormulas COMPLITED'	
						SET @v_newRow = @v_newRow  + 1
				END
				DELETE FROM @v_FormulaTable
				
				SET @v_Row = @v_Row  + 1
		END
		
		/*********** Insert into LISTBOX DETAIL **********************/	
--		SELECT 'listbox',* FROM @v_ListBoxTable 
		
		INSERT INTO @v_ListBoxDetailTable
		SELECT	listBoxID,listDetailId,itemId FROM	eGPBhutanTestServer.dbo.tbl_ListBoxDetail  where listBoxId IN (select listId from @v_ListBoxTable)

		SET @v_RowCount = @@ROWCOUNT
		SET @v_Row = 1

--		SELECT * from @v_ListBoxDetailTable

		DECLARE @v_newListID INT	
		--PRINT 'Start insertion in tbl_ListBoxDetail '
			
		WHILE	@v_Row <= @v_RowCount
		BEGIN
				SELECT	@v_listBoxID = listBoxID,
						@v_itemID = itemID,
						@v_listDetailID = listDetailID
				FROM	@v_ListBoxDetailTable
				WHERE	rowID = @v_Row
				
				--SELECT 'listBoxID',@v_listBoxID
				--PRINT 'Start insertion STEP 1'
				
				select	@v_newListID = newListID 
				from	@v_ListBoxTable 
				where	listID = @v_listBoxID
						--AND rowID = @v_Row
						
				--PRINT 'Start insertion STEP 2'
				--SELECT 'newListID',@v_newListID
				--PRINT 'Start insertion STEP 3'

				INSERT INTO tbl_ListBoxDetail
				SELECT @v_newListID,itemid,itemvalue,itemtext,isdefault from eGPBhutanTestServer.dbo.tbl_ListBoxDetail where listDetailId = @v_listDetailID and listBoxId = @v_listBoxID
				
				SET @v_Row = @v_Row + 1		
		END
		PRINT 'tbl_ListBoxDetail COMPLITED'	
		/*********** Insert into LISTCELLDETAILS **********************/
		
		Insert into @v_ListCellDetailTable 
		SELECT listCellId,listboxid,tenderTableId,columnId,tct.newTableID,lbt.newListID from eGPBhutanTestServer.dbo.tbl_ListCellDetail  lcd 
				INNER JOIN @v_TableColumnTable tct ON tct.tableID = lcd.tenderTableId
							and tct.tableColumnID = columnId
				INNER JOIN @v_ListBoxTable lbt on lbt.listID = lcd.listBoxId
							and lbt.formID = tct.formID

		SET @v_RowCount = @@ROWCOUNT
		SET @v_Row = 1
				
		WHILE	@v_Row <= @v_RowCount
		BEGIN
				SELECT	@v_listBoxID = listID,
						@v_tableColumnID = columnID,
						@v_tableID = templateTableId,
						@v_listCellId = listCellId,
						@v_newListID = newListID,
						@v_newTableID = newTemplateTableId
				FROM	@v_ListCellDetailTable
				WHERE	rowID = @v_Row
								
				INSERT INTO tbl_ListCellDetail
				SELECT	@v_newTableID,
						columnID,
						cellId,
						location,
						@v_newListID
				FROM	eGPBhutanTestServer.dbo.tbl_ListCellDetail lcd 						
				where listCellId = @v_listCellId
					--	and listBoxId = @v_listBoxID
					--	and lcd.tenderTableId = @v_tableID
					--	and columnId = @v_tableColumnID
				
				SET @v_Row = @v_Row + 1			
		END	
		PRINT 'tbl_ListCellDetail COMPLITED'	
	--select * from tbl_ListBoxMaster
	--select * from tbl_ListBoxDetail
	--select * from tbl_ListCellDetail

	--select * from @v_ListCellDetailTable
		
		PRINT 'New STD Created as -->'+@v_newTemplateName
	END
	ELSE
	BEGIN
			SET @errorCode = 'STD Already Exists.Please select new one.'
			--PRINT @errorCode 
			RAISERROR(@errorCode , 12, 1)
	END	

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SET @errorCode = error_message()
		--PRINT @errorCode 
		RAISERROR(@errorCode , 12, 1)
	END CATCH
END




GO
