SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TaherT
-- alter date: 15-Dec-2010
-- Description:	Proc created for Search Bid data
-- Last Modified
-- Modified BY: Karan
-- =============================================
-- SP Name:    p_get_egpdatamore
-- Module: evalution,negotiation,promise indicator
-- Function: Purpose
--EvaluationRptSignChk          :- check of evaluation rpt sign
--isTERSignedByAllLTM           :- check TER sign by all LTM
--getNegotiatedBidders          :- get the all negotiated bidders
--isNegCompleted                :- check that negotiation complate or not
--getNegPriceRpt                :- get the negotiated price rpt
--BiddersNegList                :- get negotiated bidder list
--GeteGPAuditTrail              :- get e-GP audit trail
--isRegFeePaidByTendere         :- check that registration fees paid by tenderer
--eMailAfter7DaysIfPaid         :- mail after 7 days if fees paid
--eMailAfter14DaysToBlock       :- mail after 14 days to block
--DisableTendererAfter14Days    :- disable tenderer after 14 days
--getFinalResponseEditstatus    :- get final response edit status
--getMemUserIdMadeEval          :- gt menu user id made for evaluation
--GetT1L1RoundIdByL1RoundId     :- get T1L1 round id by L1 round Id
--eMailAfter7DaysIfUnPaid       :- email after 7 days if fees unpaid
--eMailAfter7DaysIfUnPaidDelete :- email after 7 days if unpaid fees deleted
--evalservicebidderResult       :- get eval service bidder result
--TenderPubServiceChk           :- tender publish service check
--deleteUnpubCorriConfigMarks   :- delete un published corrigendum config marks
--getEvalBidderForNegotiation   :- get evaluated bidder for negotiation
--ChkChngPasswordBidder4Tender  :- check change password bidder for tenderer
--getResponsiveBidderCount      :- get responsive bidder count
--forimportpromisedata          :- import promise data
--getRankToSaveN1               :- get rank to save N1
--getNUROnlineTranDetail        :- get nur online transaction details
--getRegVASPaymentData          :- get reg vas payment data


CREATE PROCEDURE [dbo].[p_get_egpdatamore]
	-- Add the parameters for the stored procedure here
	 @v_fieldName1Vc varchar(500)=NULL,		  -- Action
	 @v_fieldName2Vc varchar(500)=NULL,
	 @v_fieldName3Vc varchar(500)=NULL,
	 @v_fieldName4Vc varchar(500)=NULL,
	 @v_fieldName5Vc varchar(500)=NULL,
	 @v_fieldName6Vc varchar(500)=NULL,
	 @v_fieldName7Vc varchar(500)=NULL,
	 @v_fieldName8Vc varchar(500)=NULL,
	 @v_fieldName9Vc varchar(500)=NULL,
	 @v_fieldName10Vc varchar(500)=NULL,
	 @v_fieldName11Vc varchar(500)=NULL,
	 @v_fieldName12Vc varchar(500)=NULL,
	 @v_fieldName13Vc varchar(500)=NULL,
	 @v_fieldName14Vc varchar(500)=NULL,
	 @v_fieldName15Vc varchar(500)=NULL,
	 @v_fieldName16Vc varchar(500)=NULL,
	 @v_fieldName17Vc varchar(500)=NULL,
	 @v_fieldName18Vc varchar(500)=NULL,
	 @v_fieldName19Vc varchar(500)=NULL,
	 @v_fieldName20Vc varchar(500)=NULL
AS


DECLARE @v_FinalQueryVc varchar(Max)=null,
	@v_ConditionString_Vc varchar(4000),
	@v_StartAt_Int int, @v_StopAt_Int int,
	@v_Page_inInt int ,
	@v_RecordPerPage_inInt int,
	@evalCount int

BEGIN



-- Debarment List by nafiul
IF @v_fieldName1Vc = 'getDebarmentList'
	Begin
	declare @pageNoDeb int; set @pageNoDeb=@v_fieldName6Vc
	declare @pageSizeDeb int; set @pageSizeDeb=@v_fieldName7Vc
	declare @totalRows int
	Declare @temp_query varchar(max)
	Declare @temp_condition varchar(max)
	
	if @v_fieldName2Vc = 'company'
		Begin
		if @v_fieldName3Vc is null or  @v_fieldName3Vc = '' 
		begin
			select @totalRows=count(userId) from vw_CompnayDebarredList

			;with debarred as (
			select  ROW_NUMBER() over(order by userId) as RowNo, procurementCategory as FieldValue1, cast(UserId as varchar) as FieldValue2, companyName as FieldValue3, cast(debarredUpto as varchar) as FieldValue4, cast(debarredFrom as varchar) as FieldValue5, Comments as FieldValue6, debarredReason as FieldValue7, regOffState as FieldValue8, cast(CompanyId as varchar) as	FieldValue10 from vw_CompnayDebarredList
			) 
			select *, cast(@totalRows as varchar) as FieldValue9 from debarred
			where RowNo between (@pageNoDeb-1)*@pageSizeDeb+1 and @pageNoDeb*@pageSizeDeb
		end
		else
		begin
			select @totalRows=count(userId) from vw_CompnayDebarredList  where companyName like '%'+ @v_fieldName3Vc +'%'

			;with debarred as (
			select  ROW_NUMBER() over(order by userId) as RowNo, procurementCategory as FieldValue1, cast(UserId as varchar) as FieldValue2, companyName as FieldValue3, cast(debarredUpto as varchar) as FieldValue4, cast(debarredFrom as varchar) as FieldValue5, Comments as FieldValue6, debarredReason as FieldValue7, regOffState as FieldValue8, cast(CompanyId as varchar) as	FieldValue10 from vw_CompnayDebarredList where companyName like '%'+ @v_fieldName3Vc +'%'
			) 
			select *, cast(@totalRows as varchar) as FieldValue9 from debarred
			where RowNo between (@pageNoDeb-1)*@pageSizeDeb+1 and @pageNoDeb*@pageSizeDeb 
		end
	End


		if @v_fieldName2Vc = 'individual'
		Begin
		if @v_fieldName3Vc is null or  @v_fieldName3Vc = '' 
		begin
		select @totalRows = count(userid) from vw_individualDebarredList

;with debarred as (
select  ROW_NUMBER() over(order by userId) as RowNo, procurementCategory as FieldValue1, cast(UserId as varchar) as FieldValue2, cast(name as varchar) as FieldValue3, cast(debarredUpto as varchar) as FieldValue4, cast(debarredFrom as varchar) as FieldValue5, Comments as FieldValue6, debarredReason as FieldValue7, StateName as FieldValue8 from vw_individualDebarredList
) 

	select *, cast(@totalRows as varchar) as FieldValue9 from debarred
	where RowNo between (@pageNoDeb-1)*@pageSizeDeb+1 and @pageNoDeb*@pageSizeDeb
	end
	else 
	begin
			select @totalRows = count(userid) from vw_individualDebarredList where name like '%'+ @v_fieldName3Vc +'%'

;with debarred as (
select  ROW_NUMBER() over(order by userId) as RowNo, procurementCategory as FieldValue1, cast(UserId as varchar) as FieldValue2, cast(name as varchar) as FieldValue3, cast(debarredUpto as varchar) as FieldValue4, cast(debarredFrom as varchar) as FieldValue5, Comments as FieldValue6, debarredReason as FieldValue7, StateName as FieldValue8 from vw_individualDebarredList where name like '%'+ @v_fieldName3Vc +'%')

	select *, cast(@totalRows as varchar) as FieldValue9 from debarred
	where RowNo between (@pageNoDeb-1)*@pageSizeDeb+1 and @pageNoDeb*@pageSizeDeb
	end
		End
	End
--end nafiul

   -- Insert statements for procedure here

	IF @v_fieldName1Vc = 'EvaluationRptSignChk'
	Begin
		select CONVERT(varchar(20),COUNT(torSignId)) as FieldValue1 from tbl_TORRptSign where reportType=''+@v_fieldName6Vc +'' and
		tenderId=@v_fieldName2Vc  and pkgLotId=@v_fieldName3Vc  and userId=@v_fieldName4Vc
		and roundId=@v_fieldName5Vc and reportType=@v_fieldName6Vc and evalCount = @v_fieldName7Vc
	End
	If @v_fieldName1Vc='isTERSignedByAllLTM'
	Begin
		if @v_fieldName4Vc ='4'
		begin
			select case
			when
			(
				(select COUNT(cm.userId)*convert(int,@v_fieldName4Vc) from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
				where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved'  and c.committeeType in ('TEC','PEC'))
				=
				(select COUNT(trs.torSignId) from tbl_TORRptSign trs
				Where tenderId=@v_fieldName2Vc and trs.reportType in ('ter1','ter2','ter3','ter4')
				and pkgLotId=@v_fieldName3Vc and roundId = @v_fieldName6Vc)
			)
		then '1' else '0' end as FieldValue1
		End
		else if @v_fieldName4Vc ='2'
		Begin
			select case
			when
			(
				(select COUNT(cm.userId)*convert(int,@v_fieldName4Vc) from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
				where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved'  and c.committeeType in ('TEC','PEC'))
				=
				(select COUNT(trs.torSignId) from tbl_TORRptSign trs
				Where tenderId=@v_fieldName2Vc and trs.reportType in ('ter1','ter2')
				and pkgLotId=@v_fieldName3Vc and roundId = @v_fieldName6Vc)
			)
		then '1' else '0' end as FieldValue1
		End
	End
If @v_fieldName1Vc='getNegotiatedBidders'
Begin
	--1. T1
	--2. L1
	--3. T1L1
	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
		SET @evalCount = 0
	else
		select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

	Declare @v_evalMethod int
	select @v_evalMethod = evalMethod from tbl_ConfigEvalMethod cm ,tbl_TenderDetails td, tbl_TenderTypes tt where cm.procurementMethodId=td.procurementMethodId and cm.tenderTypeId=tt.tenderTypeId and tt.tenderType=td.eventType and cm.procurementNatureId=td.procurementNatureId and tenderId=@v_fieldName2Vc
	IF @v_evalMethod=1
	BEGIN
	select   CONVERT(varchar(20),bd.userId) as FieldValue1,
		(select CONVERT(varchar(20),MAX(roundId)) from tbl_EvalRoundMaster er where reportType='L1' and tenderId=@v_fieldName2Vc)  as FieldValue2,
		dbo.f_getbiddercompany(bd.userId) as FieldValue3
		from tbl_EvalRoundMaster er,tbl_BidderRank bd
		where reportType='T1' and er.roundId=bd.roundId and er.tenderId=@v_fieldName2Vc and er.evalCount = @evalCount
		and amount in(select max(amount) from tbl_EvalRoundMaster er,tbl_BidderRank bd
		where reportType='T1' and er.roundId=bd.roundId and er.tenderId=@v_fieldName2Vc and er.evalCount = @evalCount and bd.userId not in (select nb.userId from tbl_Negotiation n
		inner join tbl_NegNotifyTenderer nb on n.negId=nb.negId
		where n.tenderId=@v_fieldName2Vc and n.negStatus='Failed' and n.evalCount = @evalCount)	)
		and bd.userId not in (select nb.userId from tbl_Negotiation n inner join tbl_NegNotifyTenderer nb on n.negId=nb.negId
		where n.tenderId=@v_fieldName2Vc and n.negStatus='Failed' and n.evalCount = @evalCount)
		order by rank
	--select top 1  CONVERT(varchar(20),bd.userId) as FieldValue1,
	--(select CONVERT(varchar(20),MAX(roundId)) from tbl_EvalRoundMaster er where reportType='L1' and tenderId=@v_fieldName2Vc)  as FieldValue2,
	--dbo.f_getbiddercompany(bd.userId) as FieldValue3
	--from tbl_EvalRoundMaster er,tbl_BidderRank bd
	--where reportType='T1' and er.roundId=bd.roundId and er.tenderId=@v_fieldName2Vc
	--and bd.userId in(select br.userId from tbl_BidderRank br inner join tbl_EvalRoundMaster em on br.userId = em.userId
	-- where em.reportType='T1' and br.roundId in (
	--select top 1 e2.roundId from tbl_EvalRoundMaster e2 where e2.tenderId=@v_fieldName2Vc and e2.reportType='L1' order by e2.roundId desc))
	--order by rank

	END
	ELSE IF @v_evalMethod=2
	BEGIN
	select   CONVERT(varchar(20),bd.userId) as FieldValue1,
		(select CONVERT(varchar(20),MAX(roundId)) from tbl_EvalRoundMaster er where reportType='L1' and tenderId=@v_fieldName2Vc)  as FieldValue2,
		dbo.f_getbiddercompany(bd.userId) as FieldValue3
		from tbl_EvalRoundMaster er,tbl_BidderRank bd
		where reportType='L1' and er.roundId=bd.roundId and er.tenderId=@v_fieldName2Vc and er.evalCount = @evalCount
		and amount in(select min(amount) from tbl_EvalRoundMaster er,tbl_BidderRank bd
		where reportType='L1'
		and er.roundId=bd.roundId and er.tenderId=@v_fieldName2Vc and er.evalCount = @evalCount and bd.userId not in (select nb.userId from tbl_Negotiation n
		inner join tbl_NegNotifyTenderer nb on n.negId=nb.negId
		where n.tenderId=@v_fieldName2Vc and n.negStatus='Failed' and n.evalCount = @evalCount) and er.roundId in(select MAX(roundId) from tbl_EvalRoundMaster where tenderId=@v_fieldName2Vc and reportType='L1' and evalCount = @evalCount)	)
		and bd.userId not in (select nb.userId from tbl_Negotiation n inner join tbl_NegNotifyTenderer nb on n.negId=nb.negId
		where n.tenderId=@v_fieldName2Vc and n.negStatus='Failed' and n.evalCount = @evalCount and er.roundId in(select MAX(roundId) from tbl_EvalRoundMaster where tenderId=@v_fieldName2Vc and reportType='L1' and evalCount = @evalCount))
		and er.roundId in(select MAX(roundId) from tbl_EvalRoundMaster where tenderId=@v_fieldName2Vc and reportType='L1' and evalCount = @evalCount)
		order by rank
		--select CONVERT(varchar(20),userId) as FieldValue1,CONVERT(varchar(20),roundId)  as FieldValue2 ,dbo.f_getbiddercompany(userId) as FieldValue3
		--	from tbl_EvalRoundMaster where reportType='L1' and tenderId=@v_fieldName2Vc order by roundId desc
	END
	ELSE IF @v_evalMethod=3
	BEGIN

		select   CONVERT(varchar(20),bd.userId) as FieldValue1,
		(select CONVERT(varchar(20),MAX(roundId)) from tbl_EvalRoundMaster er where reportType='L1' and tenderId=@v_fieldName2Vc)  as FieldValue2,
		dbo.f_getbiddercompany(bd.userId) as FieldValue3
		from tbl_EvalRoundMaster er,tbl_BidderRank bd
		where reportType='T1L1' and er.roundId=bd.roundId and er.tenderId=@v_fieldName2Vc and er.evalCount = @evalCount
		and amount in(select max(amount) from tbl_EvalRoundMaster er,tbl_BidderRank bd
		where reportType='T1L1'
		and er.roundId=bd.roundId and er.tenderId=@v_fieldName2Vc and er.evalCount = @evalCount and bd.userId not in (select nb.userId from tbl_Negotiation n
		inner join tbl_NegNotifyTenderer nb on n.negId=nb.negId
		where n.tenderId=@v_fieldName2Vc and n.negStatus='Failed' and n.evalCount = @evalCount)	)
		and bd.userId not in (select nb.userId from tbl_Negotiation n inner join tbl_NegNotifyTenderer nb on n.negId=nb.negId
		where n.tenderId=@v_fieldName2Vc and n.negStatus='Failed' and n.evalCount = @evalCount)
		order by rank
		--select CONVERT(varchar(20),bd.userId) as FieldValue1,
		--	(select CONVERT(varchar(20),MAX(roundId)) from tbl_EvalRoundMaster er where reportType='L1' and tenderId=@v_fieldName2Vc)  as FieldValue2,
		--	dbo.f_getbiddercompany(bd.userId) as FieldValue3
		--	from tbl_EvalRoundMaster er,tbl_BidderRank bd
		--	where reportType='T1L1' and er.roundId=bd.roundId and er.tenderId=@v_fieldName2Vc
		--	and bd.userId in(select br.userId from tbl_BidderRank br inner join tbl_EvalRoundMaster em on br.userId = em.userId
		--	 where em.reportType='T1L1' and br.roundId in (
		--	select top 1 e2.roundId from tbl_EvalRoundMaster e2 where e2.tenderId=@v_fieldName2Vc and e2.reportType='L1' order by e2.roundId desc))
		--	order by rank
	END

	 --If (select COUNT(tenderId) from tbl_TenderDetails where procurementMethodId=6 and eventType='RFP' and tenderId=@v_fieldName2Vc)!=0
		-- Begin
		--	select CONVERT(varchar(20),userId) as FieldValue1,
		--	(select CONVERT(varchar(20),MAX(roundId)) from tbl_EvalRoundMaster er where reportType='L1' and tenderId=@v_fieldName2Vc)  as FieldValue2,
		--	dbo.f_getbiddercompany(em.userId) as FieldValue3
		--	from tbl_EvalRoundMaster em where reportType='T1L1' and tenderId=@v_fieldName2Vc order by roundId desc
		-- End
	 --Else
		--Begin
		--	select CONVERT(varchar(20),userId) as FieldValue1,CONVERT(varchar(20),roundId)  as FieldValue2 ,dbo.f_getbiddercompany(userId) as FieldValue3
		--	from tbl_EvalRoundMaster where reportType='L1' and tenderId=@v_fieldName2Vc order by roundId desc
		--End
End
If @v_fieldName1Vc='isNegCompleted'
	Begin
		select Case When tn.negStatus!='Pending' then '1' Else '0' End  as FieldValue1 from tbl_Negotiation tn
		where tn.roundId = @v_fieldName2Vc
	End

--Modified By Dohatec For Reevaluation
If @v_fieldName1Vc='getNegPriceRpt'
	Begin
		select convert(varchar(20),br.rank) as FieldValue1,
		dbo.f_getbiddercompany(br.userId)  as FieldValue2,
		(select top 1 convert(varchar(20),br1.amount) from tbl_EvalRoundMaster em1
		inner join tbl_BidderRank br1 on em1.roundId = br1.roundId and em1.reportType='L1' and em1.evalCount = @v_fieldName4Vc
		and em1.tenderId=@v_fieldName2Vc and br1.userId=em.userId order by em.roundId desc)
		as FieldValue3,
		convert(varchar(50),amount) as FieldValue4,
		convert(varchar(10),br.userId) as FieldValue5
		from tbl_bidderrank br inner join tbl_EvalRoundMaster em
		on br.roundId = em.roundId
		where em.reportType='N1' and em.tenderId=@v_fieldName2Vc and em.evalCount = @v_fieldName4Vc
	End
--Modified By Dohatec For Reevaluation
If @v_fieldName1Vc='BiddersNegList'
	Begin
		select convert(varchar(50),n.negId) as FieldValue1,dbo.f_getbiddercompany(nt.userId) as FieldValue2,
		negStatus  as FieldValue3,convert(varchar(50),nt.userId) as FieldValue4 from tbl_Negotiation n inner join tbl_NegNotifyTenderer nt on n.negId = nt.negId
		where negStatus!='Pending' and tenderId=@v_fieldName2Vc and n.evalCount = @v_fieldName3Vc order by n.negId
	End
END


 IF @v_fieldName1Vc = 'GeteGPAuditTrail'
BEGIN

	/*
	 Start: Input Params
	 @v_fieldName1Vc =Action,
	 @v_fieldName2Vc =PageNo,
	 @v_fieldName3Vc =RecordSize,
	 @v_fieldName4Vc =UserTypeId,
	 @v_fieldName5Vc =IPAddress,
	 @v_fieldName6Vc =FromDate,
	 @v_fieldName7Vc =ToDate,
	 @v_fieldName8Vc =EmailId,
	 @v_fieldName9Vc =Activity,
	 @v_fieldName10Vc =DepartmentId,
	 @v_fieldName11Vc =PEOfficeId,
	 @v_fieldName12Vc =tenderId,
	 @v_fieldName13Vc =tenderAppLabel,
	 @v_fieldName14Vc =moduleName
	 End: Input Params
	 */


	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName2Vc, @v_RecordPerPage_inInt=@v_fieldName3Vc

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */


	/*Start: Dynamic Condition string*/

	SET @v_ConditionString_Vc=''
	If @v_fieldName4Vc is not null And @v_fieldName4Vc<>''
	Begin
		IF @v_fieldName4Vc='Media'
			Begin
				Select @v_ConditionString_Vc += ' And UserTyperId=2 and userId in (select tpa.userId from tbl_PartnerAdmin tpa where tpa.isAdmin=''no'')'
			End
		Else IF @v_fieldName4Vc='schadmin'
			Begin
				Select @v_ConditionString_Vc += ' And UserTyperId=7  and userId in (select tpa.userId from tbl_PartnerAdmin tpa where tpa.isAdmin=''yes'')'
			End
		Else IF @v_fieldName4Vc='7'
			Begin
				Select @v_ConditionString_Vc += ' And UserTyperId=7  and userId in (select tpa.userId from tbl_PartnerAdmin tpa where tpa.isAdmin=''no'')'
			End
		Else IF @v_fieldName4Vc='devadmin'
			Begin
				Select @v_ConditionString_Vc += ' And UserTyperId=6  and userId in (select tpa.userId from tbl_PartnerAdmin tpa where tpa.isAdmin=''yes'')'
			End
		Else IF @v_fieldName4Vc='6'
			Begin
				Select @v_ConditionString_Vc += ' And UserTyperId=6 and registrationType=''media'''
			End
		Else
			Begin
				Select @v_ConditionString_Vc += ' And UserTyperId in (' +  @v_fieldName4Vc+')'
			End
	End

	If @v_fieldName5Vc is not null And @v_fieldName5Vc<>''
	Begin
		Select @v_ConditionString_Vc += ' And ipAddress=''' +  @v_fieldName5Vc + ''''
	End


	If (@v_fieldName6Vc is not null And @v_fieldName6Vc<>'') AND( @v_fieldName7Vc is not null And @v_fieldName7Vc<>'')
		Begin

			Select @v_ConditionString_Vc += ' And activityDtTime  >= ''' + @v_fieldName6Vc +''' And activityDtTime  <= '''+ @v_fieldName7Vc +''''
		End
	Else If (@v_fieldName6Vc is not null And @v_fieldName6Vc <> '') AND( @v_fieldName7Vc is null Or @v_fieldName7Vc = '')
		Begin

			Select @v_ConditionString_Vc += ' And activityDtTime  >= ''' + @v_fieldName6Vc +''''
		End
	Else If (@v_fieldName6Vc is null Or @v_fieldName6Vc='') AND( @v_fieldName7Vc is not null And @v_fieldName7Vc<>'')
		Begin

			Select @v_ConditionString_Vc += ' And activityDtTime  <= ''' + @v_fieldName7Vc +''''
		End



	If @v_fieldName8Vc is not null And @v_fieldName8Vc<>''
	Begin
		Select @v_ConditionString_Vc += ' And emailId = ''' + @v_fieldName8Vc +''''
	End

	If @v_fieldName9Vc is not null And @v_fieldName9Vc<>''
	Begin
		Select @v_ConditionString_Vc += ' And actionPerformed=''' +  @v_fieldName9Vc + ''''
	End


	If @v_fieldName11Vc is not null And @v_fieldName11Vc<>''
	Begin
		Select @v_ConditionString_Vc += ' And userId in (select userId from tbl_EmployeeMaster em inner join tbl_EmployeeOffices eo on em.employeeId=eo.employeeId
															where eo.officeId='''+@v_fieldName11Vc+'''
															union all
															select userId from tbl_OfficeAdmin oa where officeId='''+@v_fieldName11Vc+''')'
	End


	If @v_fieldName10Vc is not null And @v_fieldName10Vc<>''
	Begin

		select @v_ConditionString_Vc += Case dm.departmentType  When 'Organization'
	Then ' And userId in (
			Select userId from tbl_EmployeeMaster em
			inner join tbl_EmployeeOffices eo On em.employeeId=eo.employeeId
			inner join tbl_OfficeMaster om On eo.officeId=om.officeId
			inner join tbl_DepartmentMaster dm On om.departmentId=dm.departmentId
			where dm.departmentId = '''+@v_fieldName10Vc+'''
			union
			Select userId from tbl_OfficeAdmin oa
			inner join tbl_OfficeMaster om On oa.officeId=om.officeId
			inner join tbl_DepartmentMaster dm On om.departmentId=dm.departmentId
			where dm.departmentId = '''+@v_fieldName10Vc+'''
		)
	'

	When 'Division'
	Then
		' And userId in (
			Select userId from tbl_EmployeeMaster em
			inner join tbl_EmployeeOffices eo On em.employeeId=eo.employeeId
			inner join tbl_OfficeMaster om On eo.officeId=om.officeId
			inner join tbl_DepartmentMaster dm On (om.departmentId=dm.departmentId Or om.departmentId in (select departmentId from tbl_DepartmentMaster where parentDepartmentId=dm.departmentId))
			where dm.departmentId = '''+@v_fieldName10Vc+'''
			union
			Select userId from tbl_OfficeAdmin oa
			inner join tbl_OfficeMaster om On oa.officeId=om.officeId
			inner join tbl_DepartmentMaster dm On (om.departmentId=dm.departmentId Or om.departmentId in (select departmentId from tbl_DepartmentMaster where parentDepartmentId=dm.departmentId))
			where dm.departmentId = '''+@v_fieldName10Vc+'''
		)
	'

	When 'Ministry'

	Then

		' And userId in (
			Select userId from tbl_EmployeeMaster em
			inner join tbl_EmployeeOffices eo On em.employeeId=eo.employeeId
			inner join tbl_OfficeMaster om On eo.officeId=om.officeId
			inner join tbl_DepartmentMaster dm
			On (
					om.departmentId=dm.departmentId Or
					om.departmentId in (select departmentId from tbl_DepartmentMaster where parentDepartmentId=dm.departmentId) Or
					om.departmentId in (select departmentId from tbl_DepartmentMaster where parentDepartmentId=(select departmentId from tbl_DepartmentMaster where parentDepartmentId=dm.departmentId and departmentType=''Division''))
			)
			where dm.departmentId = '''+@v_fieldName10Vc+'''
			union
			Select userId from tbl_OfficeAdmin oa
			inner join tbl_OfficeMaster om On oa.officeId=om.officeId
			inner join tbl_DepartmentMaster dm
			On (
					om.departmentId=dm.departmentId Or
					om.departmentId in (select departmentId from tbl_DepartmentMaster where parentDepartmentId=dm.departmentId) Or
					om.departmentId in (select departmentId from tbl_DepartmentMaster where parentDepartmentId=(select departmentId from tbl_DepartmentMaster where parentDepartmentId=dm.departmentId and departmentType=''Division''))
			)
			where dm.departmentId = '''+@v_fieldName10Vc+'''
		)
	'

	ELSE
	' And userId in (
		Select userId from tbl_EmployeeMaster em
			inner join tbl_EmployeeOffices eo On em.employeeId=eo.employeeId
			inner join tbl_OfficeMaster om On eo.officeId=om.officeId
			inner join tbl_DepartmentMaster dm
			On (
					om.departmentId=dm.departmentId Or
					om.departmentId in (select departmentId from tbl_DepartmentMaster where parentDepartmentId=dm.departmentId)
					)
			where dm.departmentId = '''+@v_fieldName10Vc+''')'
	End
		from tbl_DepartmentMaster dm
		Where dm.departmentId= @v_fieldName10Vc
	End

	If @v_fieldName12Vc is not null And @v_fieldName12Vc<>''
	Begin
		Select @v_ConditionString_Vc += ' And objectId=' +  @v_fieldName12Vc + ' And objectType='''+@v_fieldName13Vc+''''
	End
	If @v_fieldName14Vc is not null And @v_fieldName14Vc<>''
	Begin
		Select @v_ConditionString_Vc += ' And moduleName = '''+@v_fieldName14Vc+''''
	End

	--print('Condition String : '+@v_ConditionString_Vc)
	/*End: Dynamic Condition string*/


	/*Select @v_FinalQueryVc='
	With CTE as
(
Select ROW_NUMBER() Over (Order By list.activityDtTime desc) as RowNumber, * From
(
select distinct * from(
select lm.userId,lm.userTyperId,lm.emailId,dbo.f_GetAuditUsername(lm.userId,lm.userTyperId) as UserName,dbo.f_GetAuditOfficeDept(lm.userId,lm.userTyperId) as Department,sm.sessionStartDt,sm.sessionEndDt,
ga.ipAddress,ga.moduleName,ga.activityURL,ga.objectId,ga.objectType,ga.actionPerformed,ga.activityDtTime,ga.activityRemarks, ga.auditId,lm.registrationType
from tbl_GovUserAuditDetails ga
inner join tbl_SessionMaster sm on sm.sessionId = ga.sessionId
inner join tbl_LoginMaster lm on lm.userId = sm.userId
union all
select lm.userId,lm.userTyperId,lm.emailId,ISNULL(dbo.f_getbiddercompany(lm.userId),''NA'') as UserName,''NA'' as Department, sm.sessionStartDt,sm.sessionEndDt,
ta.ipAddress,ta.moduleName,ta.activityURL,ta.objectId,ta.objectType,ta.actionPerformed,ta.activityDtTime,ta.activityRemarks, ta.auditId,lm.registrationType
from tbl_TendererAuditDetails ta inner join tbl_SessionMaster sm on
sm.sessionId = ta.sessionId inner join tbl_LoginMaster lm on lm.userId = sm.userId
) as A Where userTyperId is not null '+ @v_ConditionString_Vc + '
) as list
)
*/
Select @v_FinalQueryVc='

/* Temp table which will store all data from both audit tables*/
DECLARE	@v_AuditData TABLE
		(
			userID	INT,
			userTyperId INT,
			emailId VARCHAR(100),
			userName VARCHAR(100),
			department VARCHAR(2000),
			sessionStartDt DATETIME,
			sessionEndDt DATETIME,
			ipAddress VARCHAR(200),
			moduleName VARCHAR(200),
			activityURL VARCHAR(2000),
			objectId INT,
			objectType VARCHAR(25),
			actionPerformed VARCHAR(200),
			activityDtTime DATETIME,
			activityRemarks VARCHAR(5000),
			auditId INT,
			registrationType VARCHAR(100)
		)

/* Data insert from tbl_GovUserAuditDetails table*/
INSERT INTO @v_AuditData
select	lm.userId,
		lm.userTyperId,
		lm.emailId,
		CASE WHEN lm.userTyperId = 3 OR lm.userTyperId = 17 OR lm.userTyperId = 6 or lm.userTyperId = 7 or lm.userTyperId = 15 THEN
			(SELECT dbo.f_Gov_Part_UserName(lm.userId,sm.sessionStartDt,lm.userTyperId))
			--(select employeeName from tbl_EmployeeMaster where userId =lm.userId)
		WHEN lm.userTyperId = 14 THEN
			(select	fullname from tbl_ExternalMemInfo where userId =lm.userId)
--		WHEN lm.userTyperId = 6 or lm.userTyperId = 7 or lm.userTyperId = 15 THEN
-- 			(select fullname from tbl_PartnerAdmin where userId =lm.userId)
		WHEN lm.userTyperId = 4 or lm.userTyperId = 5 or lm.userTyperId = 8 or lm.userTyperId = 12 OR lm.userTyperId = 18 or lm.userTyperId = 19 THEN
 			(select fullname from tbl_AdminMaster where userId = lm.userId)
		WHEN lm.userTyperId = 1 THEN
 			''eGP-Admin''
		WHEN lm.userTyperId = 16 THEN
 			''DG-CPTU''
		END as UserName,
		CASE  WHEN   lm.userTyperId = 3 THEN
			ISNULL((select	dm.departmentName + '' / '' + om.officeName
			from	tbl_EmployeeOffices eo
					inner join tbl_EmployeeMaster em on eo.employeeId = em.employeeId
					inner join tbl_OfficeMaster om on om.officeId=eo.officeId
					inner join tbl_DepartmentMaster dm on dm.departmentId = om.departmentId
			where	em.userId=lm.userId),''NA'')
		WHEN  lm.userTyperId = 5 THEN
			ISNULL((select dm.departmentName + '' / NA'' from
			tbl_AdminMaster am inner join tbl_DepartmentMaster dm on dm.approvingAuthorityId= am.userId
			where am.userId=lm.userId),''NA'')
		WHEN  lm.userTyperId = 4 THEN
			ISNULL((select dm.departmentName + '' / '' + om.officeName from
			tbl_OfficeAdmin oa inner join tbl_AdminMaster am on oa.userId = am.userId
			inner join tbl_OfficeMaster om on om.officeId = oa.officeId
			inner join tbl_DepartmentMaster dm on dm.departmentId = om.departmentId
			where am.userId=lm.userId),''NA'')
		WHEN lm.userTyperId = 6 or lm.userTyperId = 7 or lm.userTyperId = 15 or lm.userTyperId = 1 or lm.userTyperId = 8 or lm.userTyperId = 12 or lm.userTyperId = 14 or lm.userTyperId = 16 or lm.userTyperId = 17 or lm.userTyperId = 18 or lm.userTyperId = 19 THEN
			''NA''
		END AS Department,
		--dbo.f_GetAuditOfficeDept1(lm.userId,lm.userTyperId) as Department,
		sm.sessionStartDt,
		sm.sessionEndDt,
		ga.ipAddress,
		ga.moduleName,
		ga.activityURL,
		ga.objectId,
		ga.objectType,
		ga.actionPerformed,
		ga.activityDtTime,
		ga.activityRemarks,
		ga.auditId,
		lm.registrationType
from	tbl_GovUserAuditDetails ga
		inner join tbl_SessionMaster sm on sm.sessionId = ga.sessionId
		inner join tbl_LoginMaster lm on lm.userId = sm.userId

/* Data insert from tbl_TendererAuditDetails table*/
INSERT INTO @v_AuditData
select	lm.userId,
		lm.userTyperId,
		lm.emailId,
		(SELECT CASE WHEN cm.companyId = 1 then tm.firstName +'' ''+tm.lastName else CM.companyName END
		FROM tbl_TendererMaster tm,tbl_CompanyMaster CM WHERE tm.userId = lm.userId and CM.companyId=tm.companyId) as UserName ,
		''NA'' as Department,
		sm.sessionStartDt,
		sm.sessionEndDt,
		ta.ipAddress,
		ta.moduleName,
		ta.activityURL,
		ta.objectId,
		ta.objectType,
		ta.actionPerformed,
		ta.activityDtTime,
		ta.activityRemarks,
		ta.auditId,
		lm.registrationType
from	tbl_TendererAuditDetails ta
		inner join tbl_SessionMaster sm on sm.sessionId = ta.sessionId
		inner join tbl_LoginMaster lm on lm.userId = sm.userId
;
With CTE as
(
	Select ROW_NUMBER() Over (Order By list.activityDtTime desc) as RowNumber, * From
	(
		SELECT distinct	* from @v_AuditData Where userTyperId is not null '+ @v_ConditionString_Vc + '
	) as list
)

select RowNumber,userTyperId,registrationType,emailId as FieldValue1,UserName as FieldValue2,Department as FieldValue3,
ISNULL(REPLACE(CONVERT(VARCHAR(11), sessionStartDt, 106), '' '', ''-'') + '' '' +SUBSTRING(CONVERT(VARCHAR(8), sessionStartDt,108), 1, 8), ''N.A.'') as FieldValue4,
case when sessionStartDt = sessionEndDt then ''NA'' Else
ISNULL(REPLACE(CONVERT(VARCHAR(11), sessionEndDt, 106), '' '', ''-'') + '' '' +SUBSTRING(CONVERT(VARCHAR(8), sessionEndDt,108), 1, 8), ''N.A.'') End as FieldValue5,
ipAddress as FieldValue6,moduleName as FieldValue7,
activityURL as FieldValue8,convert(varchar(20),objectId) as FieldValue9,objectType as FieldValue10,actionPerformed as FieldValue11,
ISNULL(REPLACE(CONVERT(VARCHAR(11), activityDtTime, 106), '' '', ''-'') + '' '' +SUBSTRING(CONVERT(VARCHAR(8), activityDtTime,108), 1, 8), ''N.A.'') as FieldValue12,
activityRemarks as FieldValue13,auditId,
convert(varchar(20),(select count(RowNumber) from CTE)) as FieldValue14,
Ceiling ((Cast ((select count (RowNumber) from CTE) as Float) / Cast ('+CONVERT(varchar(10), @v_RecordPerPage_inInt)+' as Float))) as TotalPages
  from CTE
Where RowNumber between  '+cast(((@v_Page_inInt - 1) * @v_RecordPerPage_inInt + 1) as varchar(10))+' AND '+cast((@v_Page_inInt * @v_RecordPerPage_inInt) as varchar(10)) +
' Order by activityDtTime desc'
--' Order by RowNumber'



	--- OLD
--	Select @v_FinalQueryVc='With CTE as (
--select distinct ROW_NUMBER() Over (Order by sessionStartDt desc) as RowNumber, auditId, sessionId, sessionStartDt, userId, ipAddress,
--moduleName,activityURL,objectId,objectType,actionPerformed,activityDtTime,activityRemarks
--From
--(select ga.auditId,sm.sessionId,sm.sessionStartDt, sm.userId, ga.ipAddress,
--ga.moduleName,ga.activityURL,ga.objectId,ga.objectType,ga.actionPerformed,ga.activityDtTime,ga.activityRemarks
--from tbl_GovUserAuditDetails ga
--inner join tbl_SessionMaster sm on sm.sessionId = ga.sessionId
--inner join tbl_LoginMaster lm on lm.userId = sm.userId

--union all

--select ta.auditId,sm.sessionId,sm.sessionStartDt, sm.userId, ta.ipAddress,
--ta.moduleName,ta.activityURL,ta.objectId,ta.objectType,ta.actionPerformed,ta.activityDtTime,ta.activityRemarks
--from tbl_TendererAuditDetails ta
--inner join tbl_SessionMaster sm on sm.sessionId = ta.sessionId
--inner join tbl_LoginMaster lm on lm.userId = sm.userId
--) as A
--inner join tbl_TendererAuditDetails ta on (A.auditId=ta.auditId And A.sessionId=ta.sessionId)
--inner join tbl_GovUserAuditDetails ga on (A.auditId=ga.auditId And A.sessionId=ga.sessionId)
--inner join tbl_SessionMaster sm on sm.sessionId = A.sessionId
--inner join tbl_LoginMaster lm on lm.userId = A.userId
--Where A.auditId is not null ' + @v_ConditionString_Vc +
--')


--Select RowNumber, list.auditId, list.sessionId, list.sessionStartDt, list.userId, list.ipAddress,
--list.moduleName,list.activityURL,list.objectId,list.objectType,list.actionPerformed,list.activityDtTime,list.activityRemarks
--lm.EmailId,
--dbo.f_GetAuditUsername(lm.userId,lm.userTyperId),
--dbo.f_GetAuditOfficeDept(lm.userId,lm.userTyperId),
--sm.sessionStartDt,sm.sessionEndDt,

--(select count (RowNumber) from CTE)	as TotalCount,
-- Ceiling ((Cast ((select count (RowNumber) from CTE) as Float) / Cast ('+CONVERT(varchar(10), @v_RecordPerPage_inInt)+' as Float))) as TotalPages
--From
--(
--select RowNumber, auditId, sessionId, sessionStartDt, userId, ipAddress,
--moduleName,activityURL,objectId,objectType,actionPerformed,activityDtTime,activityRemarks
--from CTE
--where RowNumber between  '+cast(((@v_Page_inInt - 1) * @v_RecordPerPage_inInt + 1) as varchar(10))+' AND '+cast((@v_Page_inInt * @v_RecordPerPage_inInt) as varchar(10)) +
--') as list
--inner join tbl_TendererAuditDetails ta on (list.auditId=ta.auditId And list.sessionId=ta.sessionId)
--inner join tbl_GovUserAuditDetails ga on (list.auditId=ga.auditId And list.sessionId=ga.sessionId)
--inner join tbl_SessionMaster sm on sm.sessionId = list.sessionId
--inner join tbl_LoginMaster lm on lm.userId = list.userId
--'
print(@v_FinalQueryVc)
exec (@v_FinalQueryVc)
END

If @v_fieldName1Vc='isRegFeePaidByTenderer'
	Begin
		select isVerified as FieldValue1
		 from tbl_RegFeePayment where userId=@v_fieldName2Vc and isLive='yes' and status!='freezed'
		 order by regPaymentId desc
	End

If @v_fieldName1Vc='eMailAfter7DaysIfPaid'
	Begin
		select emailId as FieldValue1
		from tbl_LoginMaster
		where DATEDIFF(D, CAST(FLOOR(CAST(transdate as float)) as DATETIME) ,CAST(FLOOR(CAST(GETDATE() as float)) as DATETIME))= 7
		and status='incomplete' and userTyperId=2  and registrationType!='media'
		and userId in (select userId from tbl_RegFeePayment
		where isLive='yes' and isVerified='yes')
	END
If @v_fieldName1Vc='eMailAfter14DaysToBlock'
Begin
	select emailId as FieldValue1,Convert(varchar(20),userId) as FieldValue2
	from tbl_LoginMaster
	where DATEDIFF(D, CAST(FLOOR(CAST(transdate as float)) as DATETIME) ,CAST(FLOOR(CAST(GETDATE() as float)) as DATETIME))= 14
	and status='incomplete' and userTyperId=2  and registrationType!='media'
	and userId in (select userId from tbl_RegFeePayment
	where isLive='yes' and isVerified='yes')
END
If @v_fieldName1Vc='DisableTendererAfter14Days'
Begin
	Begin try
		begin tran
			update tbl_LoginMaster set status='disable' where userId in (@v_fieldName2Vc)
			update tbl_RegFeePayment set status='freezed' where userId in (@v_fieldName2Vc)
			select 'Success' as FieldValue1
		commit tran
	end try
	begin catch
		select 'Error' as FieldValue1, ERROR_MESSAGE() as FieldValue2
		rollback tran
	end catch
END
IF @v_fieldName1Vc = 'getFinalResponseEditstatus'
BEGIN
	select Convert(varchar(50),td.tenderId) as FieldValue1,
	td.docAvlMethod as FieldValue2,
Case td.docAvlMethod
	When 'Lot'
	Then
		Case
			When (Select Count(SignID) as SignCnt from
					(select distinct tenderId, appPkgLotId,
						(select top 1 torSignId from tbl_TORRptSign where tenderId=a.tenderId and pkgLotId=a.appPkgLotId and reportType=''+@v_fieldName4Vc+'' and evalCount = @v_fieldName5Vc ) as  SignID
							from tbl_TenderLotSecurity a
							where tenderId=td.tenderId and appPkgLotId=@v_fieldName3Vc and tenderId=@v_fieldName2Vc) as F
				)

				=

				(
					Select Count(appPkgLotId) from tbl_TenderLotSecurity where tenderId=td.tenderId  and appPkgLotId=@v_fieldName3Vc and tenderId=@v_fieldName2Vc
				)
			Then 'Block'
			Else 'Allow'
		End
	When 'Package'
	Then
		Case
			When
			Exists (Select top 1 torSignId from tbl_TORRptSign Where tenderId=td.tenderId And reportType=''+@v_fieldName4Vc+'' and evalCount = @v_fieldName5Vc)
			Then 'Block'
			Else 'Allow'
		End
End
as FieldValue3,
Case td.docAvlMethod
	When 'Lot'
	Then
		Case
			When (@v_fieldName3Vc IS not null And @v_fieldName3Vc<>'0')
			Then
				Case
					When Exists (Select top 1 torSignId from tbl_TORRptSign Where tenderId=td.tenderId and pkgLotId=@v_fieldName3Vc and tenderId = @v_fieldName2Vc And reportType=''+@v_fieldName4Vc+'' and evalCount = @v_fieldName5Vc)
					Then 'Block'
					Else 'Allow'
				End
			Else 'N.A.'
		End
	Else 'N.A.'
End	as FieldValue4

from tbl_TenderDetails td
inner join tbl_TORRptSign rpt on td.tenderId=rpt.tenderId
where td.tenderId=@v_fieldName2Vc
and rpt.reportType=''+@v_fieldName4Vc+'' and rpt.evalCount = @v_fieldName5Vc

END
IF @v_fieldName1Vc = 'getMemUserIdMadeEval'
Begin
	SELECT
		CASE WHEN ((select configType from tbl_EvalConfig where tenderId=@v_fieldName2Vc)='team')
		 THEN
				(select CONVERT(VARCHAR(20),tecMemberId) from tbl_EvalConfig where tenderId=@v_fieldName2Vc)
		 ELSE
				(select CONVERT(VARCHAR(20),cm.userId) from tbl_Committee c
					inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
					where tenderId = @v_fieldName2Vc and committeeType in ('TEC','PEC') and cm.memberRole='cp')
		 END  as FieldValue1,
		 ((select CONVERT(VARCHAR(20),cm.userId) from tbl_Committee c
					inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
					where tenderId = @v_fieldName2Vc and committeeType in ('TEC','PEC') and cm.memberRole='cp'))
			as FieldValue2,
			(select configType from tbl_EvalConfig where tenderId=@v_fieldName2Vc) as FieldValue3
End
IF @v_fieldName1Vc = 'GetT1L1RoundIdByL1RoundId'
Begin
	select top 1 convert(varchar(20),roundId) as FieldValue1
	from tbl_EvalRoundMaster where tenderId =  @v_fieldName2Vc and reportType='T1L1' order by roundId asc
End
If @v_fieldName1Vc='eMailAfter7DaysIfUnPaid'
Begin
    SELECT  emailId as FieldValue1,
            CONVERT(VARCHAR(20),userId)as FieldValue2
	from tbl_LoginMaster
	where DATEDIFF(D, CAST(FLOOR(CAST(registeredDate as float)) as DATETIME) ,CAST(FLOOR(CAST(GETDATE() as float)) as DATETIME))>= 7
            AND STATUS='incomplete'
            AND userTyperId=2
            AND registrationType!='media'
            AND userId NOT IN (SELECT userId FROM tbl_RegFeePayment)
END

If @v_fieldName1Vc='eMailAfter7DaysIfUnPaidDelete'
Begin	 					
	BEGIN TRY
		BEGIN TRAN
            DECLARE @v_totalDeleteRecord INT,
                    @v_delMailID varchar(MAX)
            SET @v_delMailID = ''
			SET @v_totalDeleteRecord = 0
			/* START CODE: TO DELETE DATA FROM TEMPORARY TABLES */
			DELETE FROM tbl_TempTendererEsignature WHERE tendererId in
			(SELECT  tendererid FROM tbl_TempTendererMaster WHERE userid in
            (SELECT items FROM dbo.f_split(@v_fieldName2Vc,',')))

--			SET @v_totalDeleteRecord = @v_totalDeleteRecord +@@ROWCOUNT

			DELETE FROM [dbo].[tbl_TempCompanyDocuments] WHERE tendererId in
			(SELECT tendererid FROM tbl_TempTendererMaster WHERE userid in
                (SELECT items FROM dbo.f_split(@v_fieldName2Vc,','))
			)

--			SET @v_totalDeleteRecord = @v_totalDeleteRecord +@@ROWCOUNT

			DELETE FROM dbo.tbl_TempCompanyJointVenture WHERE companyId in
			(	SELECT companyId FROM tbl_TempCompanyMaster WHERE userid in
                    (SELECT items FROM dbo.f_split(@v_fieldName2Vc,','))
			)

--			SET @v_totalDeleteRecord = @v_totalDeleteRecord +@@ROWCOUNT

			DELETE FROM tbl_tempTendererMaster WHERE userId In
            (SELECT items FROM dbo.f_split(@v_fieldName2Vc,','))

--			SET @v_totalDeleteRecord = @v_totalDeleteRecord +@@ROWCOUNT

			DELETE FROM [dbo].[tbl_TempCompanyMaster] WHERE userId In
            (SELECT items FROM dbo.f_split(@v_fieldName2Vc,','))

--			SET @v_totalDeleteRecord = @v_totalDeleteRecord +@@ROWCOUNT

			/* END CODE: TO DELETE DATA FROM TEMPORARY TABLES */


			/* START CODE: TO DELETE FROM LOGIN MASTER TABLE - tbl_LoginMaster*/
			DELETE FROM tbl_MessageInBox WHERE msgToUserId In
            (SELECT items FROM dbo.f_split(@v_fieldName2Vc,','))

--			SET @v_totalDeleteRecord = @v_totalDeleteRecord +@@ROWCOUNT

			DELETE FROM tbl_MessageSent WHERE msgFromUserId In
            (SELECT items FROM dbo.f_split(@v_fieldName2Vc,','))

--            SET @v_totalDeleteRecord = @v_totalDeleteRecord +@@ROWCOUNT

            DELETE FROM tbl_TenderWatchList WHERE userId In
            (SELECT items FROM dbo.f_split(@v_fieldName2Vc,','))

            SELECT	@v_delMailID = @v_delMailID + emailId +','
            FROM	tbl_LoginMaster
            WHERE	userId in (SELECT items FROM dbo.f_split(@v_fieldName2Vc,','))
			DELETE	FROM	tbl_LoginMaster
            WHERE	userId in (SELECT items FROM dbo.f_split(@v_fieldName2Vc,','))
			/* END CODE: TO DELETE FROM LOGIN MASTER TABLE - tbl_LoginMaster*/

			SET @v_totalDeleteRecord = @v_totalDeleteRecord +@@ROWCOUNT

			Select 'Records deleted' as FieldValue1
            INSERT INTO tbl_deleteUserJob VALUES (CONVERT(VARCHAR(5),@v_totalDeleteRecord) +' records deleted successfully(Cron MailAfter7DaysIfUnPaid) ['+@v_delMailID+'].',GETDATE())
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		BEGIN
			Select 'Records not deleted' as FieldValue1,ERROR_MESSAGE() as FieldValue2
			INSERT INTO tbl_deleteUserJob VALUES ('Unable to delete record due to '+ERROR_MESSAGE()+'.(Cron MailAfter7DaysIfUnPaid)',GETDATE())
			ROLLBACK TRAN
		END
	END CATCH
END
--Modified By Dohatec For Reevaluation
If @v_fieldName1Vc='evalservicebidderResult'
Begin
	select eb.result as FieldValue1,eb.remarks as FieldValue2,dbo.f_getbiddercompany(eb.userId)  as FieldValue3
	from Tbl_EvalBidderStatus eb where eb.tenderId=@v_fieldName2Vc and eb.evalStatus=''+@v_fieldName3Vc+'' and eb.evalCount = @v_fieldName4Vc order by FieldValue3
End
If @v_fieldName1Vc='ContractDetailsofUser'
Begin
	select dbo.f_getbiddercompany(ni.userId) as FieldValue1,dbo.f_GetAuditUserName(cs.createdBy,3) as FieldValue2,dbo.f_GetAuditOfficeDept(cs.createdBy,3) as FieldValue3,
	convert(varchar(20),ni.contractAmt) as FieldValue4,
	ISNULL(REPLACE(CONVERT(VARCHAR(11), cs.contractSignDt, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), cs.contractSignDt,108), 1, 5), 'N.A.') as FieldValue5,
	cs.witnessInfo as FieldValue6,cs.contractSignLocation as FieldValue7,
	cs.isPubAggOnWeb as FieldValue8
	from tbl_ContractSign cs inner join tbl_NoaIssueDetails ni on cs.noaId = ni.noaIssueId
	where cs.contractSignId=@v_fieldName2Vc
End
If @v_fieldName1Vc='GetEnvCntNCommonMember'
Begin
	select
	(select top 1 convert(varchar(5),noOfEnvelops)
		from tbl_ConfigEvalMethod cm ,tbl_TenderDetails td, tbl_TenderTypes tt
		where cm.procurementMethodId=td.procurementMethodId and
		cm.tenderTypeId=tt.tenderTypeId and tt.tenderType=td.eventType and cm.procurementNatureId=td.procurementNatureId
		and tenderId=@v_fieldName2Vc) as FieldValue1,
	(
	select top 1 convert(varchar(15),userId)from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId=cm.committeeId
		where tenderId=@v_fieldName2Vc and c.committeeType in ('TOC','POC') and cm.memberFrom in ('TEC','PEC')
	) as FieldValue2
END

If @v_fieldName1Vc='TenderPubServiceChk'
Begin

	select case when (select count(distinct m.tenderFormId) from tbl_EvalServiceForms m where
	m.tenderId=@v_fieldName2Vc and m.isCurrent='no')=
	(select count(distinct t.tenderFormId) from tbl_TenderForms t,tbl_TenderSection s,tbl_TenderStd std
	where t.tenderSectionId=s.tenderSectionId and s.tenderStdId=std.tenderStdId and t.isPriceBid='no'
	and (t.formStatus is Null or t.formStatus not in ('createp','c'))
	and std.tenderId=@v_fieldName2Vc)
	Then
		'1'
	Else
		'0'
	End as FieldValue1


	--select convert(varchar(20),count(t.tenderFormId)) from tbl_TenderForms t,tbl_TenderSection s,tbl_TenderStd std
	--where t.tenderSectionId=s.tenderSectionId and s.tenderStdId=std.tenderStdId and t.isPriceBid='no'
	--and (t.formStatus is Null or t.formStatus not in ('createp','c'))
	--and std.tenderId=@v_fieldName2Vc and
	--t.tenderFormId not in
	--(select distinct(m.tenderFormId) from tbl_EvalServiceForms m where
	--m.tenderId=@v_fieldName2Vc and m.isCurrent='no')
End

If @v_fieldName1Vc='CorriPubServiceChk'
Begin
	IF(select count(distinct(m.tenderFormId)) from tbl_EvalServiceForms m where m.tenderId=@v_fieldName2Vc and m.isCurrent='yes')=0
	Begin
		select case when(select count(distinct t.tenderFormId) from tbl_TenderForms t,tbl_TenderSection s,tbl_TenderStd std
			where t.tenderSectionId=s.tenderSectionId and s.tenderStdId=std.tenderStdId and t.isPriceBid='no'
			and (t.formStatus is Null or t.formStatus not in ('cp','c'))
			and std.tenderId=@v_fieldName2Vc and
			t.tenderFormId not in
			(select distinct(m.tenderFormId) from tbl_EvalServiceForms m where
			m.tenderId=@v_fieldName2Vc and m.isCurrent='no'))=0
			then
					case when (
						select count(distinct(m.tenderFormId)) from tbl_EvalServiceForms m where
							m.tenderId=@v_fieldName2Vc and m.isCurrent='no'
							and
							m.tenderFormId not in
							(select distinct(t.tenderFormId) from tbl_TenderForms t,tbl_TenderSection s,tbl_TenderStd std
							where t.tenderSectionId=s.tenderSectionId and s.tenderStdId=std.tenderStdId and t.isPriceBid='no'
							and (t.formStatus is Null or t.formStatus not in ('cp','c')) and std.tenderId=@v_fieldName2Vc ))=0
					then '1'
					Else '0' End
			 Else '0' End  as FieldValue1
	End
	Else
	Begin
			select case when(select count(distinct t.tenderFormId) from tbl_TenderForms t,tbl_TenderSection s,tbl_TenderStd std
			where t.tenderSectionId=s.tenderSectionId and s.tenderStdId=std.tenderStdId and t.isPriceBid='no'
			and (t.formStatus is Null or t.formStatus not in ('cp','c'))
			and std.tenderId=@v_fieldName2Vc and
			t.tenderFormId not in
			(select distinct(m.tenderFormId) from tbl_EvalServiceForms m where
			m.tenderId=@v_fieldName2Vc and m.isCurrent='yes'))=0
			then
					case when (
						select count(distinct(m.tenderFormId)) from tbl_EvalServiceForms m where
							m.tenderId=@v_fieldName2Vc and m.isCurrent='yes'
							and
							m.tenderFormId not in
							(select distinct(t.tenderFormId) from tbl_TenderForms t,tbl_TenderSection s,tbl_TenderStd std
							where t.tenderSectionId=s.tenderSectionId and s.tenderStdId=std.tenderStdId and t.isPriceBid='no'
							and (t.formStatus is Null or t.formStatus not in ('cp','c')) and std.tenderId=@v_fieldName2Vc ))=0
					then '1'
					Else '0' End
			 Else '0' End  as FieldValue1
	End

	--select case when (select count(distinct m.tenderFormId) from tbl_EvalServiceForms m where
	--m.tenderId=@v_fieldName2Vc and m.isCurrent='yes')=
	--(select count(distinct t.tenderFormId) from tbl_TenderForms t,tbl_TenderSection s,tbl_TenderStd std
	--where t.tenderSectionId=s.tenderSectionId and s.tenderStdId=std.tenderStdId and t.isPriceBid='no'
	--and (t.formStatus is Null or t.formStatus not in ('cp','c'))
	--and std.tenderId=@v_fieldName2Vc)
	--Then
	--	'1'
	--Else
	--	'0'
	--End as FieldValue1
	--case when(select count(distinct t.tenderFormId) from tbl_TenderForms t,tbl_TenderSection s,tbl_TenderStd std
	--where t.tenderSectionId=s.tenderSectionId and s.tenderStdId=std.tenderStdId and t.isPriceBid='no'
	--and (t.formStatus is Null or t.formStatus not in ('cp','c'))
	--and std.tenderId=@v_fieldName2Vc and
	--t.tenderFormId not in
	--(select distinct(m.tenderFormId) from tbl_EvalServiceForms m where
	--m.tenderId=@v_fieldName2Vc and m.isCurrent='yes'))
End
If @v_fieldName1Vc='deleteUnpubCorriConfigMarks'
Begin
	Begin try
		begin tran
			delete tbl_EvalServiceForms  where isCurrent='yes'
				and tenderId in
				(select tenderId from tbl_TenderDetails
					where CAST(FLOOR(CAST(submissionDt as float)) as DATETIME)  = CAST(FLOOR(CAST(GETDATE() as float)) as DATETIME)
				and procurementNature='Services')
			select 'Success' as FieldValue1
		commit tran
	end try
	begin catch
		select 'Error' as FieldValue1, ERROR_MESSAGE() as FieldValue2
		rollback tran
	end catch
END
If @v_fieldName1Vc='getEvalBidderForNegotiation'
Begin
	select convert(varchar(20),br.userId) as FieldValue1 from tbl_EvalRoundMaster em inner join tbl_BidderRank br
	on em.roundId = br.roundId and reportType=@v_fieldName4Vc and em.tenderId=@v_fieldName2Vc and
	br.userId not in (select nt.userId from tbl_Negotiation n inner join tbl_NegNotifyTenderer nt on n.negId = nt.negId where n.roundId=@v_fieldName3Vc
	and n.negStatus='Failed') order by rank
End

If @v_fieldName1Vc='ChkChngPasswordBidder4Tender'
Begin
	select case when
	((select COUNT(distinct bidTableId) from tbl_TenderBidDetail
	where bidtableid  in(select bidTableId from tbl_TenderBidTable where bidId in(select bidId from tbl_TenderBidForm b,tbl_TenderDetails td where b.tenderid=td.tenderid and b.submissionDt > GETDATE() and userId=@v_fieldName2Vc)))
	 =
	(select COUNT(distinct bidTableId) from tbl_TenderBidEncrypt
	where bidtableid  in(select bidTableId from tbl_TenderBidTable where bidId in(select bidId from tbl_TenderBidForm b,tbl_TenderDetails td where b.tenderid=td.tenderid and b.submissionDt > GETDATE()  and userId=@v_fieldName2Vc))))
	Then '1'
	Else '0' End
	as FieldValue1
End

--Modified By Dohatec For Reevaluation
If @v_fieldName1Vc='getRoundsforEvalByMethod'
Begin
	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
	SET @evalCount = 0
 else
	select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

	select dbo.f_getbiddercompany(br.userId) as FieldValue1 from tbl_EvalRoundMaster em inner join tbl_BidderRank br on em.roundId = br.roundId
	and em.tenderId=@v_fieldName2Vc and reportType=@v_fieldName3Vc and em.evalCount  = @evalCount order by rank asc
End

If @v_fieldName1Vc='getResponsiveBidderCount'
Begin
	select CONVERT(varchar(20),COUNT(userId)) as FieldValue1
	from tbl_EvalBidderStatus
	where tenderId=@v_fieldName2Vc and bidderStatus='Technically Responsive'
End
If @v_fieldName1Vc='forimportpromisedata'
Begin
	exec dbo.p_import_promisdata
End

IF @v_fieldName1Vc = 'getDebarment'
Begin
	set @v_ConditionString_Vc=''

 /* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName11Vc--(5)
	, @v_RecordPerPage_inInt=@v_fieldName12Vc --(10)

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
/*			2. individual / company
			3. companyname or firstname
			4. lastname
			5. 3 equals
			6. 4 equals
			7. debarstartdate
			8. debarenddate
			9.try coun
			10. State             */

			--select @v_fieldName5Vc = case when @v_fieldName5Vc='' then ' = ' end
			--select @v_fieldName6Vc = case when @v_fieldName6Vc='' then ' = ' end
			IF @v_fieldName2Vc is not null And @v_fieldName2Vc != ''
			 begin
				 If @v_fieldName2Vc='company'
				 Begin
					set @v_ConditionString_Vc =' and tm.companyId !=1'
					If @v_fieldName3Vc is not null and @v_fieldName3Vc != ''
					Begin
					    If @v_fieldName5Vc='like'
					    Begin
							set @v_ConditionString_Vc = @v_ConditionString_Vc  + ' and tcm.companyName like ''%'+@v_fieldName3Vc+'%'''
					    End
					    Else
					    Begin
							set @v_ConditionString_Vc = @v_ConditionString_Vc  + ' and tcm.companyName = '''+@v_fieldName3Vc+''''
					    End
					End
					If @v_fieldName9Vc is not null and @v_fieldName9Vc != ''
					Begin
						set @v_ConditionString_Vc = @v_ConditionString_Vc  + ' and tcm.regOffCountry='''+@v_fieldName9Vc+''''
					End
					If @v_fieldName10Vc is not null and @v_fieldName10Vc != ''
					Begin
						set @v_ConditionString_Vc = @v_ConditionString_Vc  + ' and tcm.regOffState='''+@v_fieldName10Vc+''''
					End
				 End
				 Else
				 Begin
					set @v_ConditionString_Vc =' and tm.companyId =1'
					If @v_fieldName3Vc is not null and @v_fieldName3Vc != ''
					Begin
						 If @v_fieldName5Vc='like'
					    Begin
							set @v_ConditionString_Vc = @v_ConditionString_Vc  + ' and tm.firstName like ''%'+@v_fieldName3Vc+'%'''
					    End
					    Else
					    Begin
							set @v_ConditionString_Vc = @v_ConditionString_Vc  + ' and tm.firstName = '''+@v_fieldName3Vc+''''
					    End
					End
					If @v_fieldName4Vc is not null and @v_fieldName4Vc != ''
					Begin
						 If @v_fieldName6Vc='like'
					    Begin
							set @v_ConditionString_Vc = @v_ConditionString_Vc  + ' and tm.lastName like ''%'+@v_fieldName4Vc+'%'''
					    End
					    Else
					    Begin
							set @v_ConditionString_Vc = @v_ConditionString_Vc  + ' and tm.lastName = '''+@v_fieldName4Vc+''''
					    End
					End
					If @v_fieldName9Vc is not null and @v_fieldName9Vc != ''
					Begin
						set @v_ConditionString_Vc = @v_ConditionString_Vc  + ' and tm.country='''+@v_fieldName9Vc+''''
					End
					If @v_fieldName10Vc is not null and @v_fieldName10Vc != ''
					Begin
						set @v_ConditionString_Vc = @v_ConditionString_Vc  + ' and tm.state='''+@v_fieldName10Vc+''''
					End
				 End
			 end
			 If @v_fieldName7Vc is not null and @v_fieldName7Vc != '' and @v_fieldName8Vc is not null and @v_fieldName8Vc != ''
			 Begin
					set @v_ConditionString_Vc = @v_ConditionString_Vc  + ' and tdd.debarStartDt >= '''+@v_fieldName7Vc+''' and tdd.debarEdnDt <= '''+@v_fieldName8Vc+''''
			 End
			 else If @v_fieldName7Vc is not null and @v_fieldName7Vc != ''
			 Begin
					set @v_ConditionString_Vc = @v_ConditionString_Vc  + ' and tdd.debarStartDt >= '''+@v_fieldName7Vc+''''
			 End
			 else If @v_fieldName8Vc is not null and @v_fieldName8Vc != ''
			 Begin
					set @v_ConditionString_Vc = @v_ConditionString_Vc  + ' and tdd.debarEdnDt <= '''+@v_fieldName8Vc+''''
			 End

			 Select @v_FinalQueryVc='With CTE1 as (
			 select  Row_Number() Over(order by tdr.debarmentId desc) as RowNumber ,(dbo.f_getbiddercompany(tdr.userId)) as FieldValue1,
			(CASE WHEN tcm.companyId = 1 THEN tm.country ELSE tcm.regOffCountry END) as FieldValue2,(CASE WHEN tcm.companyId = 1 THEN tm.state ELSE tcm.regOffState END) as FieldValue3,
			 (select top 1 dbo.f_getmindivorg(om.departmentId) from tbl_OfficeMaster om inner join tbl_EmployeeOffices eo on om.officeId = eo.officeId where  eo.employeeId = em.employeeId) as FieldValue4,
			 tdr.clarification as FieldValue5,convert(varchar(20),tdr.debarmentId) as FieldValue6,
			 REPLACE(CONVERT(VARCHAR(11), tdd.debarStartDt, 106), '' '', ''-'') as FieldValue9,REPLACE(CONVERT(VARCHAR(11), tdd.debarEdnDt, 106), '' '', ''-'')  as FieldValue10
			,dbo.f_get_ScopeOfDebarment(tdd.debartypeid,tdd.debarIds) as FieldValue11
			from Tbl_DebarmentReq tdr,Tbl_TendererMaster tm,Tbl_CompanyMaster tcm,Tbl_EmployeeMaster em,tbl_DebarmentDetails tdd where
			em.userId = tdr.debarmentBy and tdr.debarmentId = tdd.debarmentId and tdd.debarStatus=''byhope'' and
			tdr.userId=tm.userId and tm.companyId=tcm.companyId and tdr.debarmentStatus in (''appdebaregp'',''appdebarhope'',''appdebarcomhope'')
			  '+ @v_ConditionString_Vc  +'
			)
			select *,
			(select CONVERT(varchar(50), COUNT(*)) from CTE1) as FieldValue7,convert(varchar(20),RowNumber) as FieldValue8
			from CTE1
			WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
			+' Order by RowNumber'
			print (@v_FinalQueryVc)

			exec (@v_FinalQueryVc)
end

If @v_fieldName1Vc='getRankToSaveN1'
Begin
	Declare @v_evalM int
	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
		SET @evalCount = 0
	else
		select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

	select @v_evalM = evalMethod
		from tbl_ConfigEvalMethod cm ,tbl_TenderDetails td, tbl_TenderTypes tt
		where cm.procurementMethodId=td.procurementMethodId and
		cm.tenderTypeId=tt.tenderTypeId and tt.tenderType=td.eventType and cm.procurementNatureId=td.procurementNatureId
		and tenderId=@v_fieldName2Vc
	IF @v_evalM=1 --T1
	Begin
		select convert(varchar(20),br.rank) as FieldValue1 from tbl_BidderRank br inner join tbl_EvalRoundMaster em on br.roundId = em.roundId where
		 em.tenderId=@v_fieldName2Vc and em.reportType='T1' and em.evalCount = @evalCount
		 and br.userId in (select nt.userId from tbl_Negotiation ng inner join tbl_NegNotifyTenderer nt on ng.negId = nt.negId and ng.tenderId=@v_fieldName2Vc and ng.negStatus='Successful' and ng.evalCount = @evalCount)
	End
	Else IF @v_evalM=2 --L1
	Begin
		select convert(varchar(20),br.rank) as FieldValue1 from tbl_BidderRank br inner join tbl_EvalRoundMaster em on br.roundId = em.roundId where
		 em.tenderId=@v_fieldName2Vc and em.reportType='L1' and br.userId = em.userId and em.evalCount = @evalCount
		 and br.userId in (select nt.userId from tbl_Negotiation ng inner join tbl_NegNotifyTenderer nt on ng.negId = nt.negId and ng.tenderId=@v_fieldName2Vc and ng.negStatus='Successful' and ng.evalCount = @evalCount)
		 order by em.roundId desc
	End
	Else IF @v_evalM=3 --T1L1
	Begin
		select convert(varchar(20),br.rank) as FieldValue1 from tbl_BidderRank br inner join tbl_EvalRoundMaster em on br.roundId = em.roundId where
		 em.tenderId=@v_fieldName2Vc and em.reportType='T1L1' and em.evalCount = @evalCount
		 and br.userId in (select nt.userId from tbl_Negotiation ng inner join tbl_NegNotifyTenderer nt on ng.negId = nt.negId and ng.tenderId=@v_fieldName2Vc and ng.negStatus='Successful' and ng.evalCount = @evalCount)
	End
End
If @v_fieldName1Vc='DeclineTendererDebared'
Begin
	Declare @debarId varchar(20),@debarTypeId varchar(20),@userId int

	select @debarId=debarIds,@debarTypeId=tdr.debarTypeId, @userId=userId
	from tbl_DebarmentReq tdr,tbl_DebarmentDetails tdd
	where userId=@v_fieldName3Vc and tdr.debarmentId=tdd.debarmentId and tdd.debarStatus='byhope' and tdr.debarmentId=@v_fieldName2Vc
	 and tdr.debarmentStatus in ('appdebarcomhope','appdebaregp','appdebarhope')

	if @debarTypeId = 1
	begin
		update noa set noa.acceptRejStatus='decline',acceptRejType='By System'
		from tbl_NoaIssueDetails na,tbl_NoaAcceptance noa
		where na.noaIssueId=noa.noaIssueId
		and noa.acceptRejStatus='Pending'
		and tenderId in(select tenderId from tbl_TenderMaster where convert(varchar(20),tenderId) in (select Items from dbo.f_split(@debarId,',')))
		and
		 (na.userId in(select userId from tbl_TendererMaster where companyId in(select companyId from tbl_TendererMaster where userId=@userId) and companyId!=1) or na.userId=@userId)
	end

	if @debarTypeId = 2
	begin
		update noa set noa.acceptRejStatus='decline',acceptRejType='By System'
		from tbl_NoaIssueDetails na,tbl_NoaAcceptance noa
		where na.noaIssueId=noa.noaIssueId
		and noa.acceptRejStatus='Pending'
		and tenderId in(select tenderId from tbl_TenderMaster where convert(varchar(20),packageid) in (select Items from dbo.f_split(@debarId,',')))
		and
		 (na.userId in(select userId from tbl_TendererMaster where companyId in(select companyId from tbl_TendererMaster where userId=@userId) and companyId!=1) or na.userId=@userId)
	end

	if @debarTypeId = 3
	begin
		update noa set noa.acceptRejStatus='decline',acceptRejType='By System'
		from tbl_NoaIssueDetails na,tbl_NoaAcceptance noa
		where na.noaIssueId=noa.noaIssueId
		and noa.acceptRejStatus='Pending'
		and tenderId in(select tenderId from tbl_TenderMaster where convert(varchar(20),tenderid) in ( select tenderId from tbl_TenderMaster t,tbl_AppMaster a where t.appId=a.appId and projectid in(select Items from dbo.f_split(@debarId,','))))
		and
		 (na.userId in(select userId from tbl_TendererMaster where companyId in(select companyId from tbl_TendererMaster where userId=@userId) and companyId!=1) or na.userId=@userId)
	end

	if @debarTypeId = 4
	begin
		update noa set noa.acceptRejStatus='decline',acceptRejType='By System'
		from tbl_NoaIssueDetails na,tbl_NoaAcceptance noa
		where na.noaIssueId=noa.noaIssueId
		and noa.acceptRejStatus='Pending'
		and tenderId in(select t.tenderId from tbl_TenderMaster t,tbl_tenderdetails td where t.tenderid=td.tenderid and convert(varchar(20),officeid) in (select Items from dbo.f_split(@debarId,',')))
		and
		 (na.userId in(select userId from tbl_TendererMaster where companyId in(select companyId from tbl_TendererMaster where userId=@userId) and companyId!=1) or na.userId=@userId)
	end


	if @debarTypeId = 5
	begin
		update noa set noa.acceptRejStatus='decline',acceptRejType='By System'
		from tbl_NoaIssueDetails na,tbl_NoaAcceptance noa
		where na.noaIssueId=noa.noaIssueId
		and noa.acceptRejStatus='Pending'
		and tenderId in(select t.tenderId from tbl_TenderMaster t,tbl_tenderdetails td where t.tenderid=td.tenderid and convert(varchar(20),departmentid) in (select Items from dbo.f_split(@debarId,',')))
		and
		 (na.userId in(select userId from tbl_TendererMaster where companyId in(select companyId from tbl_TendererMaster where userId=@userId) and companyId!=1) or na.userId=@userId)
	end
End

If @v_fieldName1Vc='getNUROnlineTranDetail'
Begin
		SELECT	ResponseMsg as FieldValue1,
				TransId as FieldValue2,
				CONVERT(varchar(25),TransEnDateTime,20) as FieldValue3,
				CONVERT(varchar(25),TotalAmount) as FieldValue4,
				CONVERT(varchar(25),Amount) as FieldValue5,
				PaymentGateway as FieldValue6,
				CONVERT(varchar(25),serviceChargeAmt) as FieldValue7
		FROM	tbl_OnlinePaymentTransDetails
		WHERE	userId=@v_fieldName2Vc
				and PaymentFor='NUR'
				and TransactionStatus = 'Successful'
End

If @v_fieldName1Vc='getHopeUserOffice'
Begin
		select convert(varchar(20),eo.officeId) as FieldValue1,officeName as FieldValue2
		 from tbl_EmployeeMaster e inner join tbl_EmployeeOffices eo on e.employeeId=eo.employeeId
		 inner join tbl_EmployeeRoles er on e.employeeId=er.employeeId inner join tbl_OfficeMaster om on om.officeId=eo.officeId
		where
		er.procurementRoleId=6
		and e.userId=@v_fieldName2Vc
End

If @v_fieldName1Vc='getRegVASPaymentData'
Begin
		select rvp.emailId as FieldValue1,
		rvp.cardNo as FieldValue2,
		ISNULL(REPLACE(CONVERT(VARCHAR(11), rvp.dtofPayment , 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), rvp.dtofPayment ,108), 1, 5), 'N.A.') as FieldValue3,
		rvp.payeeName as FieldValue4,
		rvp.payeeAddress as FieldValue5,
		rvp.remarks as FieldValue6,
		convert(varchar(20),opt.Amount) as FieldValue7,
		convert(varchar(20),opt.serviceChargeAmt) as FieldValue8,
		convert(varchar(20),opt.TotalAmount) as FieldValue9,
		opt.ResponseMsg as FieldValue10,
		opt.PaymentFor as FieldValue11,
		convert(varchar(20),opt.onlinePaymentID) as FieldValue12,
		convert(varchar(150),PaymentGateway) as FieldValue13
		from tbl_OnlinePaymentTransDetails opt inner join tbl_RegVasfeePayment rvp on opt.TransId = rvp.transId
		where opt.TransId = @v_fieldName2Vc
End

IF @v_fieldName1Vc = 'FinancialYear'
BEGIN
	select convert(varchar(400),financialYearId) as FieldValue1, financialYear as FieldValue2,isCurrent AS FieldValue3,
	'' AS FieldValue4,'' AS  FieldValue5,'' AS FieldValue6 from tbl_FinancialYear order by financialYear asc
END
IF @v_fieldName1Vc = 'getProjectName'
BEGIN
	if @v_fieldName2Vc=''
	Begin
		select convert(varchar(20),projectId) as FieldValue1,projectName as FieldValue2 from tbl_ProjectMaster where projectStatus='approved' order by projectName asc
	End
	Else
	Begin
		select convert(varchar(20),projectId) as FieldValue1,projectName as FieldValue2 from tbl_ProjectMaster where projectStatus='approved' and progId=@v_fieldName2Vc order by projectName asc
	End
END
IF @v_fieldName1Vc = 'getProgramName'
BEGIN
	select convert(varchar(20),progId) as FieldValue1,progName as FieldValue2 from tbl_ProgrammeMaster order by progName asc
END
IF @v_fieldName1Vc = 'getProcTransactReport'
BEGIN
  --      financialyear : @v_fieldName4Vc
  --      quater : @v_fieldName5Vc '0'
  --      depId : @v_fieldName6Vc ''
  --      offId : @v_fieldName7Vc '0'
  --      program : @v_fieldName8Vc '0'
  --      project : @v_fieldName9Vc '0'
set @v_ConditionString_Vc= ' and am.financialYear='''+@v_fieldName4Vc+''''
IF @v_fieldName5Vc!='0' And @v_fieldName10Vc!='0'
Begin
	select @v_ConditionString_Vc = @v_ConditionString_Vc+ ' and aat.actionDate > = '''+@v_fieldName5Vc+''' and aat.actionDate <= '''+@v_fieldName10Vc+''''
End
IF @v_fieldName6Vc!=''
Begin
	select @v_ConditionString_Vc = @v_ConditionString_Vc+ ' and am.departmentId in ('+@v_fieldName6Vc+')'
End
IF @v_fieldName7Vc!='0'
Begin
	select @v_ConditionString_Vc = @v_ConditionString_Vc+ ' and am.officeId ='+@v_fieldName7Vc
End
IF @v_fieldName8Vc!='0'
Begin
	select @v_ConditionString_Vc = @v_ConditionString_Vc+ ' and am.projectId in (select projectId from tbl_ProjectMaster where progId='+@v_fieldName8Vc+')'
End
IF @v_fieldName9Vc!='0'
Begin
	select @v_ConditionString_Vc = @v_ConditionString_Vc+ ' and am.projectId ='+@v_fieldName9Vc
End
IF @v_fieldName11Vc!='0'
Begin
	select @v_ConditionString_Vc = @v_ConditionString_Vc+ ' and am.appType ='+@v_fieldName11Vc
End
select @v_FinalQueryVc = '
		select	ap.procurementnature as FieldValue1,
				pm.procurementMethod as FieldValue2,
				convert(varchar(50),COUNT(am.appId)) as FieldValue3 ,
				convert(varchar(50),sum(estimatedCost)) as FieldValue4,
				convert(varchar(50),COUNT(nd.noaIssueId)) as FieldValue5,
				convert(varchar(50),sum(contractAmt)) as FieldValue6,
				ap.procurementType	as FieldValue7
		from	tbl_AppMaster am
				inner join tbl_AppPackages ap ON am.appId = ap.appId
							AND ap.appStatus=''approved''
				inner join tbl_AppAuditTrial aat ON aat.appId = am.appId
							AND aat.childId = ap.packageId
				inner join tbl_ProcurementMethod pm ON ap.procurementMethodId = pm.procurementMethodId
				left outer join tbl_TenderMaster tm ON  tm.packageId=ap.packageid
				left outer join tbl_NoaIssueDetails nd ON nd.tenderId=tm.tenderId
							AND nd.noaIssueId IN (SELECT noaIssueId FROM tbl_NoaAcceptance WHERE acceptRejStatus=''approved'')
				left outer join tbl_ContractSign cs on nd.noaIssueId = cs.noaId
		where	am.budgetType='''+@v_fieldName2Vc+'''
				and ap.procurementType ='''+@v_fieldName3Vc+''''+@v_ConditionString_Vc+'
		group by	ap.procurementnature,
					pm.procurementMethod,
					ap.procurementType
		order by	ap.procurementnature'
		print(@v_FinalQueryVc)
		exec(@v_FinalQueryVc)
End
IF @v_fieldName1Vc = 'getProcTransactOverAllReport'
BEGIN
  --      financialyear : @v_fieldName4Vc
  --      quater : @v_fieldName5Vc '0'
  --      depId : @v_fieldName6Vc ''
  --      offId : @v_fieldName7Vc '0'
  --      program : @v_fieldName8Vc '0'
  --      project : @v_fieldName9Vc '0'
set @v_ConditionString_Vc= ' and am.financialYear='''+@v_fieldName4Vc+''''
IF @v_fieldName5Vc!='0' And @v_fieldName10Vc!='0'
Begin
	select @v_ConditionString_Vc = @v_ConditionString_Vc+ ' and aat.actionDate > = '''+@v_fieldName5Vc+''' and aat.actionDate <= '''+@v_fieldName10Vc+''''
End
IF @v_fieldName6Vc!=''
Begin
	select @v_ConditionString_Vc = @v_ConditionString_Vc+ ' and am.departmentId in ('+@v_fieldName6Vc+')'
End
IF @v_fieldName7Vc!='0'
Begin
	select @v_ConditionString_Vc = @v_ConditionString_Vc+ ' and am.officeId ='+@v_fieldName7Vc
End
IF @v_fieldName8Vc!='0'
Begin
	select @v_ConditionString_Vc = @v_ConditionString_Vc+ ' and am.projectId in (select projectId from tbl_ProjectMaster where progId='+@v_fieldName8Vc+')'
End
IF @v_fieldName9Vc!='0'
Begin
	select @v_ConditionString_Vc = @v_ConditionString_Vc+ ' and am.projectId ='+@v_fieldName9Vc
End
IF @v_fieldName11Vc!='0'
Begin
	select @v_ConditionString_Vc = @v_ConditionString_Vc+ ' and am.appType ='+@v_fieldName11Vc
End
select @v_FinalQueryVc = '
select	ap.procurementnature as FieldValue1,
		pm.procurementMethod as FieldValue2,
		convert(varchar(50),COUNT(am.appId)) as FieldValue3,
		convert(varchar(50),CONVERT(NUMERIC(10,3),(sum(estimatedCost)/10000000))) as FieldValue4,
		convert(varchar(50),COUNT(td.tenderId)) as FieldValue5,
		convert(varchar(50),CONVERT(NUMERIC(10,3),(sum(td.estCost)/10000000))) as FieldValue6,
		convert(varchar(50),COUNT(ndp.tenderId)) as FieldValue7,
		convert(varchar(50),CASE WHEN COUNT(ndp.tenderId) = 0 THEN NULL ELSE (CONVERT(NUMERIC(10,3),(sum(tdb.estCost)/10000000))) END)as FieldValue8,
		convert(varchar(50),COUNT(ndn.tenderId)) as FieldValue9,
		convert(varchar(50),CASE WHEN COUNT(ndn.tenderId) = 0 THEN NULL ELSE (CONVERT(NUMERIC(10,3),(sum(tdb.estCost)/10000000))) END) as FieldValue10,
		convert(varchar(50),COUNT(nd.noaIssueId)) as FieldValue11,
		convert(varchar(50),CONVERT(NUMERIC(10,3),(sum(nd.contractAmt)/10000000))) as FieldValue12,
		ap.procurementType as FieldValue13
from	tbl_AppMaster am
		inner join tbl_AppPackages ap on am.appId = ap.appId
					AND ap.appStatus=''approved''
		inner join tbl_AppAuditTrial aat on aat.appId = am.appId
					and aat.childId = ap.packageId
		inner join tbl_ProcurementMethod pm on ap.procurementMethodId = pm.procurementMethodId
		left outer join tbl_TenderMaster tm on  tm.packageId=ap.packageid
		left outer join tbl_TenderDetails td on  td.tenderId=tm.tenderId
					AND td.tenderStatus = ''Approved''
					AND GETDATE() between td.tenderPubDt and td.submissionDt
					AND td.budgetTypeId = am.budgetType
					AND td.procurementType = ap.procurementType
		left outer join tbl_TenderDetails tdb on  tdb.tenderId=tm.tenderId
					AND tdb.tenderStatus = ''Approved''
					AND tdb.submissionDt < GETDATE()
					AND tdb.budgetTypeId = am.budgetType
					AND tdb.procurementType = ap.procurementType
		left outer join tbl_NoaIssueDetails ndp on ndp.tenderId=tdb.tenderId
					AND ndp.noaIssueId IN (SELECT noaIssueId FROM tbl_NoaAcceptance WHERE acceptRejStatus!=''approved'')
		left outer join tbl_NoaIssueDetails ndn on ndn.tenderId=tdb.tenderId
					AND ndn.noaIssueId IN (SELECT noaIssueId FROM tbl_NoaAcceptance WHERE acceptRejStatus=''approved'')
					AND ndn.noaIssueId NOT IN (SELECT  noaId FROM tbl_ContractSign)
		left outer join tbl_NoaIssueDetails nd on nd.tenderId=tm.tenderId
					AND nd.noaIssueId IN (SELECT noaIssueId FROM tbl_NoaAcceptance WHERE acceptRejStatus=''approved'')
		left outer join tbl_ContractSign cs on nd.noaIssueId = cs.noaId
where	am.budgetType='''+@v_fieldName2Vc+''' and ap.procurementType ='''+@v_fieldName3Vc+''''+@v_ConditionString_Vc+'
group by	ap.procurementnature,
			pm.procurementMethod,
			ap.procurementType
order by	ap.procurementnature'
		print(@v_FinalQueryVc)
		exec(@v_FinalQueryVc)
END

IF @v_fieldName1Vc = 'geteGPSaveCalc'
BEGIN
	DECLARE	@v_saveCalcDetail Table
				(
					PEName VARCHAR(200),
					totalAward VARCHAR(50),
					totalOffCost VARCHAR(50),
					totalContrCost VARCHAR(50),
					totalSaved VARCHAR(50),
					totalPerSave VARCHAR(50),
					totalGoodsContact VARCHAR(50),
					totalGoodsCost VARCHAR(50),
					totalGoodsSaved VARCHAR(50),
					totalGoodsPerSave VARCHAR(50),
					totalWorksContact VARCHAR(50),
					totalWorksCost VARCHAR(50),
					totalWorksSaved VARCHAR(50),
					totalWorksPerSave VARCHAR(50),
					totalSrvsContact VARCHAR(50),
					totalSrvsCost VARCHAR(50),
					totalSrvsSaved VARCHAR(50),
					totalSrvsPerSave VARCHAR(50),
					totalContrCompleted VARCHAR(50)
--					procurementType  VARCHAR(50)
				)

	DECLARE	@v_procNatureDetail Table
				(
					--rowID INT IDENTITY,
					procNatPEName VARCHAR(200),
					procNatAward VARCHAR(50),
					procNatCost VARCHAR(50),
					procNatSaved VARCHAR(50),
					procNatPerSave VARCHAR(50)
				)

	-- MAIN DATA SELECT AND STORE IN TEMP_TABLE
	INSERT INTO @v_saveCalcDetail
	SELECT	--em.employeeName,
			--td.peName,
			td.peOfficeName, 
			convert(varchar(50),COUNT(cs.noaId)) AS AwardContract,
			convert(varchar(50),CONVERT(NUMERIC(10,3),(SUM(td.estCost)/10000000))) AS estimateCost,
			ISNULL(convert(varchar(50),CONVERT(NUMERIC(10,3),(SUM(nd.contractAmt))/10000000)),'N.A.') AS ContractAmt,
			ISNULL(convert(varchar(50),CONVERT(NUMERIC(10,3),(SUM(td.estCost) - SUM(nd.contractAmt))/10000000)),'N.A.') AS savedAmt,
			ISNULL(convert(varchar(50),CONVERT(NUMERIC(10,3),((((SUM(td.estCost) - SUM(nd.contractAmt))/10000000)*100)/(SUM(td.estCost)/10000000)))),'N.A.') AS savedPer,
			'N.A.',
			'N.A.',
			'N.A.',
			'N.A.',
			'N.A.',
			'N.A.',
			'N.A.',
			'N.A.',
			'N.A.',
			'N.A.',
			'N.A.',
			'N.A.',
			convert(varchar(50),COUNT(wc.contractId)) AS execContract
--			td.procurementType
	FROM	tbl_TenderDetails td
			--INNER JOIN tbl_EmployeeOffices eo on eo.officeId = td.officeId
			--INNER JOIN tbl_EmployeeMaster em on em.employeeId = eo.employeeId
			--INNER JOIN tbl_EmployeeRoles er ON er.employeeId= em.employeeId
			--			AND er.procurementRoleId IN (select procurementRoleId from tbl_ProcurementRole WHERE procurementRole = 'PE')
			LEFT JOIN tbl_NoaIssueDetails nd on nd.tenderId=td.tenderId
			LEFT JOIN tbl_NoaAcceptance  na ON na.noaIssueId  = nd.noaIssueId
						and acceptRejStatus='approved'
			LEFT JOIN tbl_ContractSign cs on cs.noaId = na.noaIssueId
			LEFT JOIN tbl_CMS_WcCertificate wc on wc.contractId = cs.contractSignId
						AND wc.isWorkComplete = 'yes'
	WHERE	td.tenderStatus = 'approved'
			AND td.departmentId IN (CASE WHEN @v_fieldName2Vc = '' OR @v_fieldName2Vc IS NULL THEN td.departmentId ELSE @v_fieldName2Vc END)
			AND  td.officeId =  CASE WHEN @v_fieldName3Vc = '' OR @v_fieldName3Vc IS NULL THEN td.officeId ELSE CAST(@v_fieldName3Vc AS INT) END
			AND td.procurementNature = CASE WHEN @v_fieldName4Vc = '' OR @v_fieldName4Vc IS NULL THEN td.procurementNature ELSE @v_fieldName4Vc END
			AND (td.budgetTypeId = CASE WHEN @v_fieldName5Vc = '' OR  @v_fieldName5Vc IS NULL THEN td.budgetTypeId ELSE LEFT(@v_fieldName5Vc, 1) END OR
				td.budgetTypeId = CASE WHEN @v_fieldName5Vc = '' OR  @v_fieldName5Vc IS NULL THEN td.budgetTypeId ELSE RIGHT(@v_fieldName5Vc, 1) END)
			AND td.peDistrict like CASE WHEN @v_fieldName6Vc = '' OR  @v_fieldName6Vc IS NULL THEN '%'+td.peDistrict+'%' ELSE '%'+@v_fieldName6Vc+'%' END
			AND td.cpvCode like CASE WHEN @v_fieldName7Vc = '' OR  @v_fieldName7Vc IS NULL THEN '%'+td.cpvCode+'%' ELSE '%'+@v_fieldName7Vc+'%' END
			AND td.procurementType = CASE WHEN @v_fieldName8Vc = '' OR @v_fieldName8Vc IS NULL THEN ''+td.procurementType+'' ELSE ''+@v_fieldName8Vc+'' END
			AND td.procurementMethodId = CASE WHEN @v_fieldName9Vc = '' OR @v_fieldName9Vc IS NULL THEN td.procurementMethodId ELSE @v_fieldName9Vc END
	GROUP by td.peOfficeName--em.employeeName,td.procurementType
	ORDER BY td.peOfficeName--em.employeeName,td.procurementType

	-- GOODS DATA SELECT AND STORE IN TEMP_TABLE
	IF @v_fieldName4Vc = 'Goods' OR @v_fieldName4Vc = ''
	BEGIN

		INSERT INTO @v_procNatureDetail
		SELECT	--em.employeeName,
				--td.peName,
			    td.peOfficeName, 
				convert(varchar(50),COUNT(cs.noaId)) AS AwardContract,
				ISNULL(convert(varchar(50),CONVERT(NUMERIC(10,3),(SUM(nd.contractAmt)/10000000))),'N.A.') AS ContractAmt,
				ISNULL(convert(varchar(50),CONVERT(NUMERIC(10,3),(SUM(td.estCost) - SUM(nd.contractAmt))/10000000)),'N.A.') AS savedAmt,
				ISNULL(convert(varchar(50),CONVERT(NUMERIC(10,3),((((SUM(td.estCost) - SUM(nd.contractAmt))/10000000)*100)/(SUM(td.estCost)/10000000)))),'N.A.') AS savedPer
		FROM	tbl_TenderDetails td
				--INNER JOIN tbl_EmployeeOffices eo on eo.officeId = td.officeId
				--INNER JOIN tbl_EmployeeMaster em on em.employeeId = eo.employeeId
				--INNER JOIN tbl_EmployeeRoles er ON er.employeeId= em.employeeId
				--			AND er.procurementRoleId IN (select procurementRoleId from tbl_ProcurementRole WHERE procurementRole = 'PE')
				LEFT JOIN tbl_NoaIssueDetails nd on nd.tenderId=td.tenderId
				left JOIN tbl_NoaAcceptance  na ON na.noaIssueId  = nd.noaIssueId
							and acceptRejStatus='approved'
				left JOIN tbl_ContractSign cs on cs.noaId = na.noaIssueId
		WHERE	td.tenderStatus = 'approved'
				AND td.procurementNature = 'Goods'
				AND td.departmentId IN (CASE WHEN @v_fieldName2Vc = '' OR @v_fieldName2Vc IS NULL THEN td.departmentId ELSE @v_fieldName2Vc END)
				AND  td.officeId =  CASE WHEN @v_fieldName3Vc = '' OR @v_fieldName3Vc IS NULL THEN td.officeId ELSE CAST(@v_fieldName3Vc AS INT) END
				AND (td.budgetTypeId = CASE WHEN @v_fieldName5Vc = '' OR  @v_fieldName5Vc IS NULL THEN td.budgetTypeId ELSE LEFT(@v_fieldName5Vc, 1) END OR
					td.budgetTypeId = CASE WHEN @v_fieldName5Vc = '' OR  @v_fieldName5Vc IS NULL THEN td.budgetTypeId ELSE RIGHT(@v_fieldName5Vc, 1) END)
				AND td.peDistrict like CASE WHEN @v_fieldName6Vc = '' OR  @v_fieldName6Vc IS NULL THEN '%'+td.peDistrict+'%' ELSE '%'+@v_fieldName6Vc+'%' END
				AND td.cpvCode like CASE WHEN @v_fieldName7Vc = '' OR  @v_fieldName7Vc IS NULL THEN '%'+td.cpvCode+'%' ELSE '%'+@v_fieldName7Vc+'%' END
				AND td.procurementType = CASE WHEN @v_fieldName8Vc = '' OR @v_fieldName8Vc IS NULL THEN ''+td.procurementType+'' ELSE ''+@v_fieldName8Vc+'' END
				AND td.procurementMethodId = CASE WHEN @v_fieldName9Vc = '' OR @v_fieldName9Vc IS NULL THEN td.procurementMethodId ELSE @v_fieldName9Vc END
		GROUP by td.peOfficeName --em.employeeName
		ORDER BY td.peOfficeName --em.employeeName

		UPDATE	@v_saveCalcDetail
		SET		totalGoodsContact = pnd.procNatAward,
				totalGoodsCost = pnd.procNatCost,
				totalGoodsSaved = pnd.procNatSaved,
				totalGoodsPerSave = pnd.procNatPerSave
		FROM	@v_saveCalcDetail savCalc
				INNER JOIN @v_procNatureDetail pnd ON pnd.procNatPEName = savCalc.PEName

		DELETE FROM @v_procNatureDetail
	END

	-- WORKS DATA SELECT AND STORE IN TEMP_TABLE
	IF @v_fieldName4Vc = 'Works' OR @v_fieldName4Vc = ''
	BEGIN

		INSERT INTO @v_procNatureDetail
		SELECT	--em.employeeName,
				--td.peName,
				td.peOfficeName, 
				convert(varchar(50),COUNT(cs.noaId)) AS AwardContract,
				ISNULL(convert(varchar(50),CONVERT(NUMERIC(10,3),(SUM(nd.contractAmt)/10000000))),'N.A.') AS ContractAmt,
				ISNULL(convert(varchar(50),CONVERT(NUMERIC(10,3),(SUM(td.estCost) - SUM(nd.contractAmt))/10000000)),'N.A.') AS savedAmt,
				ISNULL(convert(varchar(50),CONVERT(NUMERIC(10,3),((((SUM(td.estCost) - SUM(nd.contractAmt))/10000000)*100)/(SUM(td.estCost)/10000000)))),'N.A.')  AS savedPer
		FROM	tbl_TenderDetails td
				--INNER JOIN tbl_EmployeeOffices eo on eo.officeId = td.officeId
				--INNER JOIN tbl_EmployeeMaster em on em.employeeId = eo.employeeId
				--INNER JOIN tbl_EmployeeRoles er ON er.employeeId= em.employeeId
				--			AND er.procurementRoleId IN (select procurementRoleId from tbl_ProcurementRole WHERE procurementRole = 'PE')
				LEFT JOIN tbl_NoaIssueDetails nd on nd.tenderId=td.tenderId
				left JOIN tbl_NoaAcceptance  na ON na.noaIssueId  = nd.noaIssueId
							and acceptRejStatus='approved'
				left JOIN tbl_ContractSign cs on cs.noaId = na.noaIssueId
		WHERE	td.tenderStatus = 'approved'
				AND td.procurementNature = 'works'
				AND td.departmentId IN (CASE WHEN @v_fieldName2Vc = '' OR @v_fieldName2Vc IS NULL THEN td.departmentId ELSE @v_fieldName2Vc END)
				AND  td.officeId =  CASE WHEN @v_fieldName3Vc = '' OR @v_fieldName3Vc IS NULL THEN td.officeId ELSE CAST(@v_fieldName3Vc AS INT) END
				AND (td.budgetTypeId = CASE WHEN @v_fieldName5Vc = '' OR  @v_fieldName5Vc IS NULL THEN td.budgetTypeId ELSE LEFT(@v_fieldName5Vc, 1) END OR
					td.budgetTypeId = CASE WHEN @v_fieldName5Vc = '' OR  @v_fieldName5Vc IS NULL THEN td.budgetTypeId ELSE RIGHT(@v_fieldName5Vc, 1) END)
				AND td.peDistrict like CASE WHEN @v_fieldName6Vc = '' OR  @v_fieldName6Vc IS NULL THEN '%'+td.peDistrict+'%' ELSE '%'+@v_fieldName6Vc+'%' END
				AND td.cpvCode like CASE WHEN @v_fieldName7Vc = '' OR  @v_fieldName7Vc IS NULL THEN '%'+td.cpvCode+'%' ELSE '%'+@v_fieldName7Vc+'%' END
				AND td.procurementType = CASE WHEN @v_fieldName8Vc = '' OR @v_fieldName8Vc IS NULL THEN ''+td.procurementType+'' ELSE ''+@v_fieldName8Vc+'' END
				AND td.procurementMethodId = CASE WHEN @v_fieldName9Vc = '' OR @v_fieldName9Vc IS NULL THEN td.procurementMethodId ELSE @v_fieldName9Vc END
		GROUP by td.peOfficeName -- em.employeeName
		ORDER BY td.peOfficeName -- em.employeeName

		UPDATE	@v_saveCalcDetail
		SET		totalWorksContact = pnd.procNatAward,
				totalWorksCost = pnd.procNatCost,
				totalWorksSaved = pnd.procNatSaved,
				totalWorksPerSave = pnd.procNatPerSave
		FROM	@v_saveCalcDetail savCalc
				INNER JOIN @v_procNatureDetail pnd ON pnd.procNatPEName = savCalc.PEName

		DELETE FROM @v_procNatureDetail
	END

	-- SERVICE DATA SELECT AND STORE IN TEMP_TABLE
	IF @v_fieldName4Vc = 'Service' OR @v_fieldName4Vc = ''
	BEGIN

		INSERT INTO @v_procNatureDetail
		SELECT	--em.employeeName,
				--td.peName,
				td.peOfficeName, 
				convert(varchar(50),COUNT(cs.noaId)) AS AwardContract,
				ISNULL(convert(varchar(50),CONVERT(NUMERIC(10,3),(SUM(nd.contractAmt)/10000000))),'N.A.') AS ContractAmt,
				ISNULL(convert(varchar(50),CONVERT(NUMERIC(10,3),(SUM(td.estCost) - SUM(nd.contractAmt))/10000000)),'N.A.') AS savedAmt,
				ISNULL(convert(varchar(50),CONVERT(NUMERIC(10,3),((((SUM(td.estCost) - SUM(nd.contractAmt))/10000000)*100)/(SUM(td.estCost)/10000000)))),'N.A.') AS savedPer
		FROM	tbl_TenderDetails td
				--INNER JOIN tbl_EmployeeOffices eo on eo.officeId = td.officeId
				--INNER JOIN tbl_EmployeeMaster em on em.employeeId = eo.employeeId
				--INNER JOIN tbl_EmployeeRoles er ON er.employeeId= em.employeeId
				--			AND er.procurementRoleId IN (select procurementRoleId from tbl_ProcurementRole WHERE procurementRole = 'PE')
				LEFT JOIN tbl_NoaIssueDetails nd on nd.tenderId=td.tenderId
				left JOIN tbl_NoaAcceptance  na ON na.noaIssueId  = nd.noaIssueId
							and acceptRejStatus='approved'
				left JOIN tbl_ContractSign cs on cs.noaId = na.noaIssueId
		WHERE	td.tenderStatus = 'approved'
				AND td.procurementNature = 'services'
				AND td.departmentId IN (CASE WHEN @v_fieldName2Vc = '' OR @v_fieldName2Vc IS NULL THEN td.departmentId ELSE @v_fieldName2Vc END)
				AND  td.officeId =  CASE WHEN @v_fieldName3Vc = '' OR @v_fieldName3Vc IS NULL THEN td.officeId ELSE CAST(@v_fieldName3Vc AS INT) END
				AND (td.budgetTypeId = CASE WHEN @v_fieldName5Vc = '' OR  @v_fieldName5Vc IS NULL THEN td.budgetTypeId ELSE LEFT(@v_fieldName5Vc, 1) END OR
					td.budgetTypeId = CASE WHEN @v_fieldName5Vc = '' OR  @v_fieldName5Vc IS NULL THEN td.budgetTypeId ELSE RIGHT(@v_fieldName5Vc, 1) END)
				AND td.peDistrict like CASE WHEN @v_fieldName6Vc = '' OR  @v_fieldName6Vc IS NULL THEN '%'+td.peDistrict+'%' ELSE '%'+@v_fieldName6Vc+'%' END
				AND td.cpvCode like CASE WHEN @v_fieldName7Vc = '' OR  @v_fieldName7Vc IS NULL THEN '%'+td.cpvCode+'%' ELSE '%'+@v_fieldName7Vc+'%' END
				AND td.procurementType = CASE WHEN @v_fieldName8Vc = '' OR @v_fieldName8Vc IS NULL THEN ''+td.procurementType+'' ELSE ''+@v_fieldName8Vc+'' END
				AND td.procurementMethodId = CASE WHEN @v_fieldName9Vc = '' OR @v_fieldName9Vc IS NULL THEN td.procurementMethodId ELSE @v_fieldName9Vc END
		GROUP by td.peOfficeName -- em.employeeName
		ORDER BY td.peOfficeName --em.employeeName

		UPDATE	srvCalc
		SET		totalSrvsContact = pnd.procNatAward,
				totalSrvsCost = pnd.procNatCost,
				totalSrvsSaved = pnd.procNatSaved,
				totalSrvsPerSave = pnd.procNatPerSave
		FROM	@v_saveCalcDetail srvCalc
				INNER JOIN @v_procNatureDetail pnd ON pnd.procNatPEName = srvCalc.PEName

		DELETE FROM @v_procNatureDetail
	END

	SELECT	PEName AS FieldValue1,
			totalAward AS FieldValue2,
			totalOffCost AS FieldValue3,
			totalContrCost AS FieldValue4,
			totalSaved AS FieldValue5,
			totalPerSave AS FieldValue6,
			totalGoodsContact AS FieldValue7,
			totalGoodsCost AS FieldValue8,
			totalGoodsSaved AS FieldValue9,
			totalGoodsPerSave AS FieldValue10,
			totalWorksContact AS FieldValue11,
			totalWorksCost AS FieldValue12,
			totalWorksSaved AS FieldValue13,
			totalWorksPerSave AS FieldValue14,
			totalSrvsContact AS FieldValue15,
			totalSrvsCost AS FieldValue16,
			totalSrvsSaved AS FieldValue17,
			totalSrvsPerSave AS FieldValue18,
			totalContrCompleted AS FieldValue19
	FROM	@v_saveCalcDetail
END

IF @v_fieldName1Vc = 'tenderWisePartRpt'
BEGIN

	SELECT	td.peName AS FieldValue1,
			td.packageDescription AS FieldValue2,
			ISNULL(convert(varchar(50),COUNT(bd.userId)),'N.A.') AS FieldValue3,
			ISNULL(convert(varchar(50),COUNT(fs.userId)),'N.A.') AS FieldValue4,
			CASE	WHEN td.tenderStatus = 'Cancelled' THEN 'Cancelled'
					WHEN es.rptStatus = 'Rejected / Re-Tendering' OR es.rptStatus = 'Rejected/Re-Tendering' OR es.rptStatus = 'Rejected' THEN 'Retendered'
					WHEN COUNT(cs.contractSignId) > 0 THEN 'Awarded'
					ELSE 'N.A.'
			END AS FieldValue5,
			CASE WHEN  COUNT(cs.contractSignId) > 0 THEN nd.companyName ELSE 'N.A.' END AS FieldValue6,
			CASE	WHEN COUNT(wc.contractId) > 0 THEN 'Completed'
					WHEN COUNT(ct.contractId) > 0 THEN 'Terminated'
					WHEN COUNT(cs.contractSignId) > 0 THEN 'Undergoing'
					ELSE 'N.A.'
			END AS FieldValue7,
			td.tenderid,
			td.tenderStatus,
			es.rptStatus
	FROM	tbl_TenderDetails td
			LEFT JOIN	tbl_BidConfirmation bd on bd.tenderId = td.tenderId
						and bd.confirmationStatus = 'accepted'
			LEFT JOIN	tbl_TenderPayment tp on tp.tenderId = bd.tenderId
						AND tp.paymentFor = 'Document Fees'
						AND tp.userId = bd.userId
						AND tp.tenderPaymentId IN (select paymentId from tbl_TenderPaymentVerification)
			LEFT JOIN	tbl_FinalSubmission fs on fs.tenderId = td.tenderId
						AND fs.bidSubStatus = 'finalsubmission'
						AND fs.userId = bd.userId
			LEFT JOIN	tbl_EvalRptSentToAA es on es.tenderId = td.tenderId
						--AND es.userId = fs.userId
			LEFT JOIN	tbl_NoaIssueDetails nd on nd.tenderId = td.tenderId
						AND nd.noaIssueId IN (	SELECT	noaIssueId
												FROM	tbl_NoaAcceptance
												WHERE	acceptRejStatus = 'approved'
											)
			LEFT JOIN	tbl_NoaAcceptance na on na.noaIssueId = nd.noaIssueId
						AND na.acceptRejStatus = 'approved'
			LEFT JOIN	tbl_ContractSign cs on cs.noaId = na.noaIssueId
			LEFT JOIN	tbl_CMS_WcCertificate wc on wc.contractId = cs.contractSignId
						AND wc.isWorkComplete = 'yes'
			LEFT JOIN	tbl_CMS_ContractTermination ct on ct.contractId = cs.contractSignId
						AND ct.status = 'approved'
	WHERE td.tenderStatus = 'Approved'
			AND td.departmentId IN (CASE WHEN @v_fieldName2Vc = '' OR @v_fieldName2Vc IS NULL THEN td.departmentId ELSE @v_fieldName2Vc END)
			AND  td.officeId =  CASE WHEN @v_fieldName3Vc = '' OR @v_fieldName3Vc IS NULL THEN td.officeId ELSE CAST(@v_fieldName3Vc AS INT) END
			AND td.procurementNature = CASE WHEN @v_fieldName4Vc = '' OR @v_fieldName4Vc IS NULL THEN td.procurementNature ELSE @v_fieldName4Vc END
			AND (td.budgetTypeId = CASE WHEN @v_fieldName5Vc = '' OR  @v_fieldName5Vc IS NULL THEN td.budgetTypeId ELSE LEFT(@v_fieldName5Vc, 1) END OR
				td.budgetTypeId = CASE WHEN @v_fieldName5Vc = '' OR  @v_fieldName5Vc IS NULL THEN td.budgetTypeId ELSE RIGHT(@v_fieldName5Vc, 1) END)
			AND td.peDistrict like CASE WHEN @v_fieldName6Vc = '' OR  @v_fieldName6Vc IS NULL THEN '%'+td.peDistrict+'%' ELSE '%'+@v_fieldName6Vc+'%' END
			AND td.cpvCode like CASE WHEN @v_fieldName7Vc = '' OR  @v_fieldName7Vc IS NULL THEN '%'+td.cpvCode+'%' ELSE '%'+@v_fieldName7Vc+'%' END
			AND td.procurementMethodId = CASE WHEN @v_fieldName8Vc = '' OR @v_fieldName8Vc IS NULL THEN td.procurementMethodId ELSE @v_fieldName8Vc END
	GROUP BY	td.peName,
				td.packageDescription,
				td.tenderStatus,
				es.rptStatus,
				nd.companyName,
				td.tenderid
	ORDER BY	td.tenderid DESC
END

IF @v_fieldName1Vc = 'geteGPStatistics'
BEGIN
	DECLARE @v_NCT NUMERIC,
			@v_ICT NUMERIC,
			@v_Fieldvalue1 VARCHAR(500),
			@v_Fieldvalue2 VARCHAR(500),
			@v_Fieldvalue3 VARCHAR(500),
			@v_Fieldvalue4 VARCHAR(500),
			@v_Fieldvalue5 VARCHAR(500),
			@v_Fieldvalue6 VARCHAR(500),
			@v_Fieldvalue7 VARCHAR(500),
			@v_Fieldvalue8 VARCHAR(500),
			@v_Fieldvalue9 VARCHAR(500),
			@v_Fieldvalue10 VARCHAR(500),
			@v_Fieldvalue11 VARCHAR(500),
			@v_Fieldvalue12 VARCHAR(500),
			@v_Fieldvalue13 VARCHAR(500),
			@v_Fieldvalue14 VARCHAR(500),
			@v_Fieldvalue15 VARCHAR(500),
			@v_Fieldvalue16 VARCHAR(500),
			@v_Fieldvalue17 VARCHAR(500),
			@v_Fieldvalue18 VARCHAR(500),
			@v_Fieldvalue19 VARCHAR(500),
			@v_Fieldvalue20 VARCHAR(500),
			@v_Fieldvalue21 VARCHAR(500),
			@v_Fieldvalue22 VARCHAR(500),
			@v_Fieldvalue23 VARCHAR(500)

	SELECT @v_NCT = 0, @v_ICT = 0

	---Total Registered Tenderers/Consultants
	-- contractor NCT
	SELECT	@v_NCT = count(LM.userId)
	FROM	tbl_CompanyMaster CM
			INNER JOIN tbl_LoginMaster LM ON LM.userId = CM.userId
						AND registrationType = 'contractor'
						AND status='Approved'
						AND userTyperId=2
						AND isJvca = 'no'
			INNER JOIN tbl_TendererMaster TM ON TM.userId = LM.userId
						AND tm.companyId = CM.companyId
	WHERE	CM.regOffCountry = 'Bhutan'

	-- individualconsultant NCT
	SELECT	@v_NCT = (@v_NCT + COUNT(userId))
	FROM	tbl_LoginMaster
	WHERE	status='Approved'
			AND userTyperId=2
			AND registrationType='individualconsultant'
			AND nationality='Bhutanese'
			AND isJvca = 'no'

	-- contractor ICT
	SELECT	@v_ICT  = count(LM.userId)
	FROM	tbl_CompanyMaster CM
			INNER JOIN tbl_LoginMaster LM ON LM.userId = CM.userId
						AND registrationType = 'contractor'
						AND status='Approved'
						AND userTyperId=2
						AND isJvca = 'no'
			INNER JOIN tbl_TendererMaster TM ON TM.userId = LM.userId
						AND tm.companyId = CM.companyId
	WHERE	CM.regOffCountry <> 'Bhutan'

	-- individualconsultant ICT
	SELECT	@v_ICT = (@v_ICT + COUNT(userId))
	FROM	tbl_LoginMaster
	WHERE	status='Approved'
			AND userTyperId=2
			AND registrationType='individualconsultant'
			AND nationality <> 'Bhutanese'
			AND isJvca = 'no'

	SET @v_Fieldvalue1 = convert(varchar(50),@v_NCT)+'#'+convert(varchar(50),@v_ICT)+'#'+convert(varchar(50),@v_NCT+@v_ICT)
	SELECT @v_NCT = 0, @v_ICT = 0

	-- Valid Tenderers/Consultants
	-- NCT
	SELECT	@v_NCT = count(LM.userId)
	FROM	tbl_CompanyMaster CM
			INNER JOIN tbl_LoginMaster LM ON LM.userId = CM.userId
						AND registrationType ='contractor'
						AND status='Approved'
						AND userTyperId=2
						AND isJvca = 'no'
						AND validUpTo < GETDATE()
			INNER JOIN tbl_TendererMaster TM ON TM.userId = LM.userId
						AND tm.companyId = CM.companyId
	WHERE	CM.regOffCountry = 'Bhutan'

	-- individualconsultant NCT
	SELECT	@v_NCT = (@v_NCT + COUNT(userId))
	FROM	tbl_LoginMaster
	WHERE	status='Approved'
			AND userTyperId=2
			AND registrationType='individualconsultant'
			AND nationality='Bhutanese'
			AND isJvca = 'no'
			AND validUpTo < GETDATE()
	-- ICT
	SELECT	@v_ICT = count(LM.userId)
	FROM	tbl_CompanyMaster CM
			INNER JOIN tbl_LoginMaster LM ON LM.userId = CM.userId
						AND registrationType = 'contractor'
						AND status='Approved'
						AND userTyperId=2
						AND isJvca = 'no'
						AND validUpTo < GETDATE()
			INNER JOIN tbl_TendererMaster TM ON TM.userId = LM.userId
						AND tm.companyId = CM.companyId
	WHERE	CM.regOffCountry <> 'Bhutan'

	-- individualconsultant ICT
	SELECT	@v_ICT = (@v_ICT + COUNT(userId))
	FROM	tbl_LoginMaster
	WHERE	status='Approved'
			AND userTyperId=2
			AND registrationType='individualconsultant'
			AND nationality <> 'Bhutanese'
			AND isJvca = 'no'
			AND validUpTo < GETDATE()

	SET  @v_Fieldvalue2 =  convert(varchar(50),@v_NCT)+'#'+convert(varchar(50),@v_ICT)+'#'+convert(varchar(50),@v_NCT+@v_ICT)
	SELECT @v_NCT = 0, @v_ICT = 0

	-- Participating Tenderers/Consultants
	-- NCT
	SELECT	 @v_NCT = count( DISTINCT LM.userId)
	FROM	tbl_CompanyMaster CM
			INNER JOIN tbl_LoginMaster LM ON LM.userId = CM.userId
						AND registrationType = 'contractor'
						AND status='Approved'
						AND userTyperId=2
						AND isJvca = 'no'
			INNER JOIN tbl_TendererMaster TM ON TM.userId = LM.userId
						AND tm.companyId = CM.companyId
			INNER JOIN 	tbl_BidConfirmation BC ON BC.userId = LM.userId
						AND BC.confirmationStatus = 'accepted'
	WHERE	CM.regOffCountry = 'Bhutan'

	-- individualconsultant NCT
	SELECT	@v_NCT = (@v_NCT + COUNT(DISTINCT LM.userId))
	FROM	tbl_LoginMaster LM
			INNER JOIN 	tbl_BidConfirmation BC ON BC.userId = LM.userId
						AND BC.confirmationStatus = 'accepted'
	WHERE	status='Approved'
			AND userTyperId=2
			AND registrationType='individualconsultant'
			AND nationality='Bhutanese'
			AND isJvca = 'no'

   -- ICT
	SELECT	 @v_ICT = count(DISTINCT LM.userId)
	FROM	tbl_CompanyMaster CM
			INNER JOIN tbl_LoginMaster LM ON LM.userId = CM.userId
						AND registrationType = 'contractor'
						AND status='Approved'
						AND userTyperId=2
						AND isJvca = 'no'
			INNER JOIN tbl_TendererMaster TM ON TM.userId = LM.userId
						AND tm.companyId = CM.companyId
			INNER JOIN 	tbl_BidConfirmation BC ON BC.userId = LM.userId
						AND BC.confirmationStatus = 'accepted'
	WHERE	CM.regOffCountry <> 'Bhutan'

	-- individualconsultant ICT
	SELECT	@v_ICT = (@v_ICT + COUNT(DISTINCT LM.userId))
	FROM	tbl_LoginMaster LM
			INNER JOIN 	tbl_BidConfirmation BC ON BC.userId = LM.userId
						AND BC.confirmationStatus = 'accepted'
	WHERE	status='Approved'
			AND userTyperId=2
			AND registrationType='individualconsultant'
			AND nationality <> 'Bhutanese'
			AND isJvca = 'no'

	SET  @v_Fieldvalue3 = convert(varchar(50),@v_NCT)+'#'+convert(varchar(50),@v_ICT)+'#'+convert(varchar(50),@v_NCT+@v_ICT)
	SELECT @v_NCT = 0, @v_ICT = 0

	--Government Organizations Registered
	SELECT	@v_NCT = COUNT(departmentId)
	FROM	tbl_DepartmentMaster
	WHERE	departmentType='Organization'

	SET @v_Fieldvalue4 = convert(varchar(50),@v_NCT)+'#N.A.#'+convert(varchar(50),@v_NCT)
	SELECT @v_NCT = 0, @v_ICT = 0

	-- Procuring Entities Registered
	SELECT	@v_NCT = COUNT(officeId)
	FROM	tbl_OfficeMaster
	WHERE	departmentId in (	SELECT	DISTINCT departmentId
								FROM	tbl_DepartmentMaster
								WHERE	departmentType IN ('Ministry','Division','Organization')
							)

	SET @v_Fieldvalue5 = convert(varchar(50),@v_NCT)+'#N.A.#'+convert(varchar(50),@v_NCT)
	SELECT @v_NCT = 0, @v_ICT = 0

	-- Registered Government Procurement Users
	SELECT	@v_NCT = COUNT(DISTINCT LM.userId)
	FROM	tbl_LoginMaster LM
			INNER JOIN tbl_EmployeeMaster em on em.userId = lm.userId
			INNER JOIN tbl_EmployeeRoles er ON er.employeeId= em.employeeId
						AND er.procurementRoleId IN (	SELECT	procurementRoleId
														FROM	tbl_ProcurementRole
														WHERE	procurementRole IN(	'PE','Minister','HOPE','Secretary','AO',
																					'AU','TOC/POC','TEC/PEC','Account Officer',
																					'CCGP','BOD'
																					)
													)
	WHERE	LM.status = 'approved'
			AND  userTyperId = 3

	SET @v_Fieldvalue6 = convert(varchar(50),@v_NCT)+'#N.A.#'+convert(varchar(50),@v_NCT)
	SELECT @v_NCT = 0, @v_ICT = 0

	-- Procuring Entities using e-GP for procurement
	SELECT	@v_NCT = COUNT(officeId)
	FROM	tbl_OfficeMaster
	WHERE	departmentId IN (	SELECT	DISTINCT departmentId
								FROM	tbl_DepartmentMaster
								WHERE	departmentType IN ('Ministry','Division','Organization')
							)
			AND officeId IN (	SELECT	officeId
								FROM	tbl_AppMaster
								WHERE	appId IN (	SELECT	appId
													FROM	tbl_AppPackages
													WHERE	appStatus='approved'
												)
							)

	SET @v_Fieldvalue7 = convert(varchar(50),@v_NCT)+'#N.A.#'+convert(varchar(50),@v_NCT)
	SELECT @v_NCT = 0, @v_ICT = 0

	-- Registered Development Partners
	SELECT	@v_NCT = COUNT(*)
	FROM (	SELECT	(	SELECT CONVERT(VARCHAR(50),COUNT(DISTINCT aa.sBankDevelopId))
	FROM		tbl_ScBankDevPartnerMaster aa,
				tbl_PartnerAdmin bb,
				tbl_LoginMaster cc
	WHERE		aa.partnerType='Development'
								AND aa.sBankDevelHeadId=A.sBankDevelopId
				AND aa.sBankDevelopId=bb.sBankDevelopId
				AND bb.userId=cc.userId
				AND cc.status!='deactive'
					) AS field
			FROM	tbl_ScBankDevPartnerMaster A
			WHERE	partnerType='Development'
				AND isBranchOffice='no'
					AND countryId IN (	SELECT	countryId
										FROM	tbl_CountryMaster
										WHERE	countryName = 'Bangladesh'
									)
			) AS A

	SELECT	@v_ICT = COUNT(*)
	FROM (	SELECT	(	SELECT CONVERT(VARCHAR(50),COUNT(diStinct aa.sBankDevelopId))
	FROM		tbl_ScBankDevPartnerMaster aa,
				tbl_PartnerAdmin bb,
				tbl_LoginMaster cc
	WHERE		aa.partnerType='Development'
								AND aa.sBankDevelHeadId=A.sBankDevelopId
				AND aa.sBankDevelopId=bb.sBankDevelopId
				AND bb.userId=cc.userId
				AND cc.status!='deactive'
					) AS field
			FROM	tbl_ScBankDevPartnerMaster A
			WHERE	partnerType='Development'
					AND isBranchOffice='no'
					AND countryId IN (	SELECT	countryId
										FROM	tbl_CountryMaster
										WHERE	countryName <> 'Bangladesh'
									)
			) AS A

	SET @v_Fieldvalue8 =  convert(varchar(50),@v_NCT)+'#'+convert(varchar(50),@v_ICT)+'#'+convert(varchar(50),@v_NCT+@v_ICT)
	SELECT @v_NCT = 0, @v_ICT = 0

	-- Registered Media
	SELECT	 @v_NCT = COUNT(DISTINCT LM.userId)
	FROM	tbl_CompanyMaster CM
			INNER JOIN tbl_LoginMaster LM ON LM.userId = CM.userId
						AND registrationType='media'
						AND status='Approved'
						AND userTyperId=2
						AND isJvca = 'no'
			INNER JOIN tbl_TendererMaster TM ON TM.userId = LM.userId
						AND tm.companyId = CM.companyId
	WHERE	CM.regOffCountry = 'Bhutan'

	SELECT	 @v_ICT = COUNT(DISTINCT LM.userId)
	FROM	tbl_CompanyMaster CM
			INNER JOIN tbl_LoginMaster LM ON LM.userId = CM.userId
						AND registrationType='media'
						AND status='Approved'
						AND userTyperId=2
						AND isJvca = 'no'
			INNER JOIN tbl_TendererMaster TM ON TM.userId = LM.userId
						AND tm.companyId = CM.companyId
	WHERE	CM.regOffCountry <> 'Bhutan'

	SET @v_Fieldvalue9 =  convert(varchar(50),@v_NCT)+'#'+convert(varchar(50),@v_ICT)+'#'+convert(varchar(50),@v_NCT+@v_ICT)
	SELECT @v_NCT = 0, @v_ICT = 0

	-- Registered Banks
	SELECT	@v_NCT = count(sBankDevelopId)
	FROM	tbl_ScBankDevPartnerMaster
	WHERE	partnerType='ScheduleBank' And isBranchOffice='no'
			AND countryId IN (	SELECT	countryId
								FROM	tbl_CountryMaster
								WHERE	countryName  = 'Bangladesh'
							)

	SELECT	@v_ICT = count(sBankDevelopId)
	FROM	tbl_ScBankDevPartnerMaster
	WHERE	partnerType='ScheduleBank' And isBranchOffice='no'
			AND countryId IN (	SELECT	countryId
								FROM	tbl_CountryMaster
								WHERE	countryName  <> 'Bangladesh'
							)

	SET @v_Fieldvalue10 =  convert(varchar(50),@v_NCT)+'#'+convert(varchar(50),@v_ICT)+'#'+convert(varchar(50),@v_NCT+@v_ICT)
	SELECT @v_NCT = 0, @v_ICT = 0

	-- Procurement Packages/Lots
	SELECT	@v_NCT = COUNT(am.appId)
	FROM	tbl_AppMaster am
			INNER JOIN tbl_AppPackages ap ON ap.appId = am.appId
						AND ap.appStatus='approved'
						AND procurementType = 'NCT'
	WHERE	am.appStatus = 'approved'

	SELECT	@v_ICT = COUNT(am.appId)
	FROM	tbl_AppMaster am
			INNER JOIN tbl_AppPackages ap ON ap.appId = am.appId
						AND ap.appStatus='approved'
						AND procurementType = 'ICT'
	WHERE	am.appStatus = 'approved'

	SET @v_Fieldvalue11 =  convert(varchar(50),@v_NCT)+'#'+convert(varchar(50),@v_ICT)+'#'+convert(varchar(50),@v_NCT+@v_ICT)
	SELECT @v_NCT = 0, @v_ICT = 0

	-- Procurement Notice published
	SELECT	@v_NCT = COUNT(tm.tenderid)
	FROM	tbl_TenderMaster tm
		INNER JOIN tbl_TenderDetails td on td.tenderId  = tm.tenderId
					AND tenderStatus='approved'
					AND procurementType = 'NCT'

	SELECT	@v_ICT = COUNT(tm.tenderid)
	FROM	tbl_TenderMaster tm
			INNER JOIN tbl_TenderDetails td on td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND procurementType = 'ICT'

	SET @v_Fieldvalue12 =  convert(varchar(50),@v_NCT)+'#'+convert(varchar(50),@v_ICT)+'#'+convert(varchar(50),@v_NCT+@v_ICT)
	SELECT @v_NCT = 0, @v_ICT = 0

	--Goods Tender published
	SELECT	@v_NCT = COUNT(tm.tenderid)
	FROM	tbl_TenderMaster tm
			INNER JOIN tbl_TenderDetails td on td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND	procurementType = 'NCT'
						AND procurementNature = 'Goods'

	SELECT	@v_ICT = COUNT(tm.tenderid)
	FROM	tbl_TenderMaster tm
			INNER JOIN tbl_TenderDetails td on td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND procurementType = 'ICT'
						AND procurementNature = 'Goods'

	SET @v_Fieldvalue13 =  convert(varchar(50),@v_NCT)+'#'+convert(varchar(50),@v_ICT)+'#'+convert(varchar(50),@v_NCT+@v_ICT)
	SELECT @v_NCT = 0, @v_ICT = 0

	-- Works Tender published
	SELECT	@v_NCT = COUNT(tm.tenderid)
	FROM	tbl_TenderMaster tm
			INNER JOIN tbl_TenderDetails td on td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND procurementType = 'NCT'
						AND procurementNature = 'Works'

	SELECT	@v_ICT = COUNT(tm.tenderid)
	FROM	tbl_TenderMaster tm
			INNER JOIN tbl_TenderDetails td on td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND procurementType = 'ICT'
						AND procurementNature = 'Works'

	SET @v_Fieldvalue14 =  convert(varchar(50),@v_NCT)+'#'+convert(varchar(50),@v_ICT)+'#'+convert(varchar(50),@v_NCT+@v_ICT)
	SELECT @v_NCT = 0, @v_ICT = 0

	-- Services REOI/RFP published
	SELECT	@v_NCT = COUNT(tm.tenderid)
	FROM	tbl_TenderMaster tm
			INNER JOIN tbl_TenderDetails td on td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND procurementType = 'NCT'
						AND procurementNature = 'Services'

	SELECT	@v_ICT = COUNT(tm.tenderid)
	FROM	tbl_TenderMaster tm
			INNER JOIN tbl_TenderDetails td on td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND procurementType = 'ICT'
						AND procurementNature = 'Services'

	SET @v_Fieldvalue15 =  convert(varchar(50),@v_NCT)+'#'+convert(varchar(50),@v_ICT)+'#'+convert(varchar(50),@v_NCT+@v_ICT)
	SELECT @v_NCT = 0, @v_ICT = 0

	-- NoA Issued
	SELECT	@v_NCT = COUNT(tm.tenderid)
	FROM	tbl_TenderMaster tm
			INNER JOIN tbl_TenderDetails td on td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND procurementType = 'NCT'
			INNER JOIN tbl_NoaIssueDetails nd ON nd.tenderId = tm.tenderId

	SELECT	@v_ICT = COUNT(tm.tenderid)
	FROM	tbl_TenderMaster tm
			INNER JOIN tbl_TenderDetails td on td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND procurementType = 'ICT'
			INNER JOIN tbl_NoaIssueDetails nd ON nd.tenderId = tm.tenderId

	SET @v_Fieldvalue16 =  convert(varchar(50),@v_NCT)+'#'+convert(varchar(50),@v_ICT)+'#'+convert(varchar(50),@v_NCT+@v_ICT)
	SELECT @v_NCT = 0, @v_ICT = 0

	-- Contracts Awarded
	SELECT	@v_NCT = COUNT(tm.tenderid)
	FROM	tbl_TenderMaster tm
			INNER JOIN tbl_TenderDetails td on td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND procurementType = 'NCT'
			INNER JOIN tbl_NoaIssueDetails nd ON nd.tenderId = tm.tenderId
			INNER JOIN tbl_NoaAcceptance na on na.noaIssueId = nd.noaIssueId
						and na.acceptRejStatus = 'approved'
			INNER JOIN tbl_ContractSign cs on cs.noaId = na.noaIssueId

	SELECT	@v_ICT = COUNT(tm.tenderid)
	FROM	tbl_TenderMaster tm
			INNER JOIN tbl_TenderDetails td on td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND procurementType = 'ICT'
			INNER JOIN tbl_NoaIssueDetails nd ON nd.tenderId = tm.tenderId
			INNER JOIN tbl_NoaAcceptance na on na.noaIssueId = nd.noaIssueId
						and na.acceptRejStatus = 'approved'
			INNER JOIN tbl_ContractSign cs on cs.noaId = na.noaIssueId

	SET @v_Fieldvalue17 =  convert(varchar(50),@v_NCT)+'#'+convert(varchar(50),@v_ICT)+'#'+convert(varchar(50),@v_NCT+@v_ICT)
	SELECT @v_NCT = 0, @v_ICT = 0

	-- Contracts Cancelled
	SELECT	@v_NCT = COUNT(tm.tenderid)
	FROM	tbl_TenderMaster tm
			INNER JOIN tbl_TenderDetails td on td.tenderId  = tm.tenderId
						AND procurementType = 'NCT'
						AND tenderStatus = 'cancelled'

	SELECT	@v_ICT = COUNT(tm.tenderid)
	FROM	tbl_TenderMaster tm
			INNER JOIN tbl_TenderDetails td on td.tenderId  = tm.tenderId
						AND procurementType = 'ICT'
						AND tenderStatus = 'cancelled'

	SET @v_Fieldvalue18 =  convert(varchar(50),@v_NCT)+'#'+convert(varchar(50),@v_ICT)+'#'+convert(varchar(50),@v_NCT+@v_ICT)
	SELECT @v_NCT = 0, @v_ICT = 0

	-- Contracts Completed
	SELECT	@v_NCT = COUNT(tm.tenderid)
	FROM	tbl_TenderMaster tm
			INNER JOIN tbl_TenderDetails td on td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND procurementType = 'NCT'
			INNER JOIN tbl_NoaIssueDetails nd ON nd.tenderId = tm.tenderId
						AND nd.noaIssueId IN (SELECT noaIssueId FROM tbl_NoaAcceptance WHERE acceptRejStatus = 'approved')
			INNER JOIN tbl_ContractSign cs on cs.noaId = nd.noaIssueId
			INNER JOIN tbl_CMS_WcCertificate wc on wc.contractId = cs.contractSignId
						AND wc.isWorkComplete = 'yes'

	SELECT	@v_ICT = COUNT(tm.tenderid)
	FROM	tbl_TenderMaster tm
			INNER JOIN tbl_TenderDetails td on td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND procurementType = 'ICT'
			INNER JOIN tbl_NoaIssueDetails nd ON nd.tenderId = tm.tenderId
						AND nd.noaIssueId IN (SELECT noaIssueId FROM tbl_NoaAcceptance WHERE acceptRejStatus = 'approved')
			INNER JOIN tbl_ContractSign cs on cs.noaId = nd.noaIssueId
			INNER JOIN tbl_CMS_WcCertificate wc on wc.contractId = cs.contractSignId
						AND wc.isWorkComplete = 'yes'

	SET @v_Fieldvalue19 =  convert(varchar(50),@v_NCT)+'#'+convert(varchar(50),@v_ICT)+'#'+convert(varchar(50),@v_NCT+@v_ICT)
	SELECT @v_NCT = 0, @v_ICT = 0

	--Value Procurement
	SELECT	@v_NCT = sum(nd.contractAmt)
	FROM	tbl_TenderMaster tm
			INNER JOIN tbl_TenderDetails td ON td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND procurementType = 'NCT'
			INNER JOIN tbl_NoaIssueDetails nd ON nd.tenderId = tm.tenderId
						AND nd.noaIssueId IN (SELECT noaIssueId FROM tbl_NoaAcceptance WHERE acceptRejStatus = 'approved')
			INNER JOIN tbl_ContractSign cs ON cs.noaId = nd.noaIssueId

	SELECT	@v_ICT = sum(nd.contractAmt)
	FROM	tbl_TenderMaster tm
			INNER JOIN tbl_TenderDetails td ON td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND procurementType = 'ICT'
			INNER JOIN tbl_NoaIssueDetails nd ON nd.tenderId = tm.tenderId
						AND nd.noaIssueId IN (SELECT noaIssueId FROM tbl_NoaAcceptance WHERE acceptRejStatus = 'approved')
			INNER JOIN tbl_ContractSign cs ON cs.noaId = nd.noaIssueId

	SET @v_Fieldvalue20 =  convert(varchar(50),CONVERT(NUMERIC(10,3),(@v_NCT/10000000)))+'#'+convert(varchar(50),CONVERT(NUMERIC(10,3),(@v_ICT/10000000)))+'#'+convert(varchar(50),CONVERT(NUMERIC(10,3),((@v_NCT+@v_ICT)/10000000)))
	SELECT @v_NCT = 0, @v_ICT = 0

	-- Total Amount of Procurement in fiscal year <this year>
	SELECT	@v_NCT = sum(nd.contractAmt)
	FROM	tbl_TenderMaster tm
			INNER JOIN tbl_TenderDetails td on td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND procurementType = 'NCT'
			INNER JOIN tbl_NoaIssueDetails nd ON nd.tenderId = tm.tenderId
						AND nd.noaIssueId IN (SELECT noaIssueId FROM tbl_NoaAcceptance WHERE acceptRejStatus = 'approved')
			INNER JOIN tbl_ContractSign cs on cs.noaId = nd.noaIssueId
			INNER JOIN tbl_FinancialYear fy on fy.financialYear = tm.financialYear
						AND fy.isCurrent = 'yes'

	SELECT	@v_ICT = sum(nd.contractAmt)
	FROM	tbl_TenderMaster tm
			INNER JOIN tbl_TenderDetails td on td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND procurementType = 'ICT'
			INNER JOIN tbl_NoaIssueDetails nd ON nd.tenderId = tm.tenderId
						AND nd.noaIssueId IN (SELECT noaIssueId FROM tbl_NoaAcceptance WHERE acceptRejStatus = 'approved')
			INNER JOIN tbl_ContractSign cs on cs.noaId = nd.noaIssueId
			INNER JOIN tbl_FinancialYear fy on fy.financialYear = tm.financialYear
						AND fy.isCurrent = 'yes'

	SET @v_Fieldvalue21 =  convert(varchar(50),CONVERT(NUMERIC(10,3),(@v_NCT/10000000)))+'#'+convert(varchar(50),CONVERT(NUMERIC(10,3),(@v_ICT/10000000)))+'#'+convert(varchar(50),CONVERT(NUMERIC(10,3),((@v_NCT+@v_ICT)/10000000)))
	SELECT @v_NCT = 0, @v_ICT = 0

	-- No. of Complaints lodged
	SELECT	@v_NCT = COUNT(complaintId)
	FROM	tbl_CMS_ComplaintMaster cm
			INNER JOIN tbl_TenderMaster tm ON tm.tenderId = cm.tenderId
			INNER JOIN tbl_TenderDetails td ON td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND procurementType = 'NCT'

	SELECT	@v_ICT = COUNT(complaintId)
	FROM	tbl_CMS_ComplaintMaster cm
			INNER JOIN tbl_TenderMaster tm on tm.tenderId = cm.tenderId
			INNER JOIN tbl_TenderDetails td on td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND procurementType = 'ICT'

	SET @v_Fieldvalue22 =  convert(varchar(50),@v_NCT)+'#'+convert(varchar(50),@v_ICT)+'#'+convert(varchar(50),@v_NCT+@v_ICT)
	SELECT @v_NCT = 0, @v_ICT = 0

	-- No. of Complaints Resolved
	SELECT	@v_NCT = COUNT(complaintId)
	FROM	tbl_CMS_ComplaintMaster cm
			INNER JOIN tbl_TenderMaster tm ON tm.tenderId = cm.tenderId
			INNER JOIN tbl_TenderDetails td ON td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND procurementType = 'NCT'
	WHERE	complaintStatus = 'accept' or (complaintStatus = 'reject' and complaintLevelId = 4)

	SELECT	@v_ICT = COUNT(complaintId)
	FROM	tbl_CMS_ComplaintMaster cm
			INNER JOIN tbl_TenderMaster tm ON tm.tenderId = cm.tenderId
			INNER JOIN tbl_TenderDetails td ON td.tenderId  = tm.tenderId
						AND tenderStatus='approved'
						AND procurementType = 'ICT'
	WHERE	complaintStatus = 'accept' or (complaintStatus = 'reject' and complaintLevelId = 4)

	SET @v_Fieldvalue23 =  convert(varchar(50),@v_NCT)+'#'+convert(varchar(50),@v_ICT)+'#'+convert(varchar(50),@v_NCT+@v_ICT)
	SELECT @v_NCT = 0, @v_ICT = 0

	-- Final Result  for screen
	SELECT	ISNULL(@v_Fieldvalue1,'N.A.#N.A.#N.A.') AS FieldValue1,
			ISNULL(@v_Fieldvalue2,'N.A.#N.A.#N.A.') AS FieldValue2,
			ISNULL(@v_Fieldvalue3,'N.A.#N.A.#N.A.') AS FieldValue3,
			ISNULL(@v_Fieldvalue4,'N.A.#N.A.#N.A.') AS FieldValue4,
			ISNULL(@v_Fieldvalue5,'N.A.#N.A.#N.A.') AS FieldValue5,
			ISNULL(@v_Fieldvalue6,'N.A.#N.A.#N.A.') AS FieldValue6,
			ISNULL(@v_Fieldvalue7,'N.A.#N.A.#N.A.') AS FieldValue7,
			ISNULL(@v_Fieldvalue8,'N.A.#N.A.#N.A.') AS FieldValue8,
			ISNULL(@v_Fieldvalue9,'N.A.#N.A.#N.A.') AS FieldValue9,
			ISNULL(@v_Fieldvalue10,'N.A.#N.A.#N.A.') AS FieldValue10,
			ISNULL(@v_Fieldvalue11,'N.A.#N.A.#N.A.') AS FieldValue11,
			ISNULL(@v_Fieldvalue12,'N.A.#N.A.#N.A.') AS FieldValue12,
			ISNULL(@v_Fieldvalue13,'N.A.#N.A.#N.A.') AS FieldValue13,
			ISNULL(@v_Fieldvalue14,'N.A.#N.A.#N.A.') AS FieldValue14,
			ISNULL(@v_Fieldvalue15,'N.A.#N.A.#N.A.') AS FieldValue15,
			ISNULL(@v_Fieldvalue16,'N.A.#N.A.#N.A.') AS FieldValue16,
			ISNULL(@v_Fieldvalue17,'N.A.#N.A.#N.A.') AS FieldValue17,
			ISNULL(@v_Fieldvalue18,'N.A.#N.A.#N.A.')AS FieldValue18,
			ISNULL(@v_Fieldvalue19,'N.A.#N.A.#N.A.') AS FieldValue19,
			ISNULL(@v_Fieldvalue20,'N.A.#N.A.#N.A.') AS FieldValue20,
			ISNULL(@v_Fieldvalue21,'N.A.#N.A.#N.A.') AS FieldValue21,
			ISNULL(@v_Fieldvalue22,'N.A.#N.A.#N.A.') AS FieldValue22,
			ISNULL(@v_Fieldvalue23,'N.A.#N.A.#N.A.') AS FieldValue23
END
IF @v_fieldName1Vc = 'getCurrentYear'
BEGIN
	SELECT	financialYear as FieldValue1
	FROM	tbl_FinancialYear
	WHERE	isCurrent = 'Yes'
END
IF @v_fieldName1Vc = 'getAPPProgress'
BEGIN

	DECLARE	@v_appProgDetail Table
			(
					offices VARCHAR(200),
					estCost NUMERIC(25,3),
					totalGoodsApp INT,
					totalWorksApp INT,
					totalSrvsApp INT ,
					totalApps  NUMERIC(25,3) ,
					totalGoodsAward INT,
					totalWorksAward INT,
					totalSrvsAward INT,
					totalAward  NUMERIC(25,3),
					costOfAwardCont NUMERIC(25,3),
					actualExp NUMERIC(25,3)
				)

	DECLARE	@v_procNatureData Table
				(
					offices VARCHAR(200),
					pubApp INT,
					awardCont INT
				)

	INSERT INTO @v_appProgDetail
	SELECT	dm.departmentHirarchy,
			SUM(CAST(ap.pkgEstCost AS NUMERIC(25,3))) estCost,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			SUM(CASE WHEN ap.procurementnature = 'GOODS' THEN ndg.contractAmt ELSE vc.contractValue END) as awardCost,
			SUM(CAST(imd.invoiceAmt AS NUMERIC(25,3))) actExp
	FROM	tbl_AppMaster am
			INNER JOIN tbl_DepartmentMaster dm on dm.departmentId = am.departmentId
			INNER JOIN tbl_OfficeMaster om on om.departmentId = dm.departmentId
						AND om.officeId = am.officeId
			INNER JOIN tbl_AppPackages ap on ap.appId = am.appId
					AND ap.appStatus='approved'
			INNER JOIN tbl_AppPkgLots al on al.appId = am.appId
					AND al.packageId = ap.packageId
			LEFT JOIN tbl_CMS_VariContractVal vc on vc.lotId = al.appPkgLotId
						AND vc.isCurrent = 'yes'
			LEFT JOIN  tbl_TenderMaster tm on tm.appId = am.appId
						AND tm.packageId = ap.packageId
						AND tm.financialYear = am.financialYear
			LEFT JOIN tbl_NoaIssueDetails ndg  on ndg.tenderId = tm.tenderId
						AND ndg.noaIssueId IN (SELECT noaIssueId FROM tbl_NoaAcceptance WHERE acceptRejStatus = 'approved')
			LEFT JOIN tbl_ContractSign csg on csg.noaId = ndg.noaIssueId
			LEFT JOIN tbl_CMS_InvoiceMaster im on im.tenderId = tm.tenderId
						AND im.invStatus = 'sendtotenderer'
			LEFT JOIN tbl_CMS_InvoiceAccDetails	imd on imd.invoiceId = im.invoiceId
			LEFT JOIN tbl_CMS_WpMaster cwp on cwp.wpLotId = al.appPkgLotId
	where	am.appStatus = 'approved'
			AND am.financialYear = CASE WHEN @v_fieldName2Vc = '' OR @v_fieldName2Vc IS NULL THEN ''+am.financialYear+'' ELSE ''+@v_fieldName2Vc+'' END
			AND am.departmentId = CASE WHEN @v_fieldName3Vc = '' OR @v_fieldName3Vc IS NULL THEN am.departmentId ELSE @v_fieldName3Vc END
			AND am.officeId = CASE WHEN @v_fieldName4Vc = '' OR @v_fieldName4Vc IS NULL THEN am.officeId ELSE @v_fieldName4Vc END
			AND ap.procurementNature = CASE WHEN @v_fieldName5Vc = '' OR @v_fieldName5Vc IS NULL THEN ''+ap.procurementNature+'' ELSE ''+@v_fieldName5Vc+'' END
			AND om.stateId = CASE WHEN @v_fieldName6Vc = '' OR @v_fieldName6Vc IS NULL THEN om.stateId ELSE @v_fieldName6Vc END
			AND ap.cpvCode like CASE WHEN @v_fieldName7Vc = '' OR @v_fieldName7Vc IS NULL THEN '%'+ap.cpvCode+'%' ELSE '%'+@v_fieldName7Vc+'%' END
			AND ap.procurementMethodId = CASE WHEN @v_fieldName8Vc = '' OR @v_fieldName8Vc IS NULL THEN ap.procurementMethodId ELSE @v_fieldName8Vc END
			AND (am.budgetType = CASE WHEN @v_fieldName9Vc = '' OR  @v_fieldName9Vc IS NULL THEN am.budgetType ELSE LEFT(@v_fieldName9Vc, 1) END OR
				am.budgetType = CASE WHEN @v_fieldName9Vc = '' OR  @v_fieldName9Vc IS NULL THEN am.budgetType ELSE RIGHT(@v_fieldName9Vc, 1) END)
	GROUP BY dm.departmentHirarchy
	order BY dm.departmentHirarchy

	-- GOODS DATA SELECT AND STORE IN TEMP_TABLE
	IF @v_fieldName5Vc = 'Goods' OR @v_fieldName5Vc = '' OR @v_fieldName5Vc = '1'
	BEGIN
		INSERT INTO @v_procNatureData
		SELECT	dm.departmentHirarchy,
				COUNT(apg.appId) goodsPkg,
				COUNT(csg.contractSignId) goodsAward
		FROM	tbl_AppMaster am
				INNER JOIN tbl_DepartmentMaster dm on dm.departmentId = am.departmentId
				INNER JOIN tbl_OfficeMaster om on om.departmentId = dm.departmentId
						AND om.officeId = am.officeId
				INNER JOIN tbl_AppPackages apg on apg.appId = am.appId
						AND apg.appStatus='approved'
						AND apg.procurementnature = 'goods'
				LEFT JOIN  tbl_TenderMaster tmg on tmg.appId = am.appId
							AND tmg.packageId = apg.packageId
							AND tmg.financialYear = am.financialYear
				LEFT JOIN tbl_NoaIssueDetails ndg  on ndg.tenderId = tmg.tenderId
							AND ndg.noaIssueId IN (SELECT noaIssueId FROM tbl_NoaAcceptance WHERE acceptRejStatus = 'approved')
				LEFT JOIN tbl_ContractSign csg on csg.noaId = ndg.noaIssueId
		WHERE	am.appStatus = 'approved'
				AND am.financialYear = CASE WHEN @v_fieldName2Vc = '' OR @v_fieldName2Vc IS NULL THEN ''+am.financialYear+'' ELSE ''+@v_fieldName2Vc+'' END
				AND am.departmentId = CASE WHEN @v_fieldName3Vc = '' OR @v_fieldName3Vc IS NULL THEN am.departmentId ELSE @v_fieldName3Vc END
				AND am.officeId = CASE WHEN @v_fieldName4Vc = '' OR @v_fieldName4Vc IS NULL THEN am.officeId ELSE @v_fieldName4Vc END
				AND apg.procurementNature = CASE WHEN @v_fieldName5Vc = '' OR @v_fieldName5Vc IS NULL THEN ''+apg.procurementNature+'' ELSE ''+@v_fieldName5Vc+'' END
				AND om.stateId = CASE WHEN @v_fieldName6Vc = '' OR @v_fieldName6Vc IS NULL THEN om.stateId ELSE @v_fieldName6Vc END
				AND apg.cpvCode like CASE WHEN @v_fieldName7Vc = '' OR @v_fieldName7Vc IS NULL THEN '%'+apg.cpvCode+'%' ELSE '%'+@v_fieldName7Vc+'%' END
				AND apg.procurementMethodId = CASE WHEN @v_fieldName8Vc = '' OR @v_fieldName8Vc IS NULL THEN apg.procurementMethodId ELSE @v_fieldName8Vc END
				AND (am.budgetType = CASE WHEN @v_fieldName9Vc = '' OR  @v_fieldName9Vc IS NULL THEN am.budgetType ELSE LEFT(@v_fieldName9Vc, 1) END OR
					am.budgetType = CASE WHEN @v_fieldName9Vc = '' OR  @v_fieldName9Vc IS NULL THEN am.budgetType ELSE RIGHT(@v_fieldName9Vc, 1) END)
		GROUP BY dm.departmentHirarchy
		order BY dm.departmentHirarchy

		UPDATE	@v_appProgDetail
		SET		totalGoodsApp = pnd.pubApp,
				totalGoodsAward = pnd.awardCont,
				totalApps = totalApps + pnd.pubApp,
				totalAward = totalAward + pnd.awardCont
		FROM	@v_appProgDetail apd
			INNER JOIN @v_procNatureData pnd ON pnd.offices = apd.offices

		DELETE FROM @v_procNatureData
	END

	-- WORKS DATA SELECT AND STORE IN TEMP_TABLE
	IF @v_fieldName5Vc = 'works' OR @v_fieldName5Vc = '' OR @v_fieldName5Vc = '2'
	BEGIN
		INSERT INTO @v_procNatureData
		SELECT	dm.departmentHirarchy,
				COUNT(apw.appId) worksPkg,
				COUNT(csw.contractSignId) worksAward
		FROM	tbl_AppMaster am
				INNER JOIN tbl_DepartmentMaster dm on dm.departmentId = am.departmentId
				INNER JOIN tbl_OfficeMaster om on om.departmentId = dm.departmentId
						AND om.officeId = am.officeId
				INNER JOIN tbl_AppPackages apw on apw.appId = am.appId
						AND apw.appStatus='approved'
						AND apw.procurementnature = 'works'
				LEFT JOIN  tbl_TenderMaster tmw on tmw.appId = apw.appId
							AND tmw.packageId = apw.packageId
							AND tmw.financialYear = am.financialYear
				LEFT JOIN tbl_NoaIssueDetails ndw  on ndw.tenderId = tmw.tenderId
							AND ndw.noaIssueId IN (SELECT noaIssueId FROM tbl_NoaAcceptance WHERE acceptRejStatus = 'approved')
				LEFT JOIN tbl_ContractSign csw on csw.noaId = ndw.noaIssueId
		where	am.appStatus = 'approved'
				AND am.financialYear = CASE WHEN @v_fieldName2Vc = '' OR @v_fieldName2Vc IS NULL THEN ''+am.financialYear+'' ELSE ''+@v_fieldName2Vc+'' END
				AND am.departmentId = CASE WHEN @v_fieldName3Vc = '' OR @v_fieldName3Vc IS NULL THEN am.departmentId ELSE @v_fieldName3Vc END
				AND am.officeId = CASE WHEN @v_fieldName4Vc = '' OR @v_fieldName4Vc IS NULL THEN am.officeId ELSE @v_fieldName4Vc END
				AND apw.procurementNature = CASE WHEN @v_fieldName5Vc = '' OR @v_fieldName5Vc IS NULL THEN ''+apw.procurementNature+'' ELSE ''+@v_fieldName5Vc+'' END
				AND om.stateId = CASE WHEN @v_fieldName6Vc = '' OR @v_fieldName6Vc IS NULL THEN om.stateId ELSE @v_fieldName6Vc END
				AND apw.cpvCode like CASE WHEN @v_fieldName7Vc = '' OR @v_fieldName7Vc IS NULL THEN '%'+apw.cpvCode+'%' ELSE '%'+@v_fieldName7Vc+'%' END
				AND apw.procurementMethodId = CASE WHEN @v_fieldName8Vc = '' OR @v_fieldName8Vc IS NULL THEN apw.procurementMethodId ELSE @v_fieldName8Vc END
				AND (am.budgetType = CASE WHEN @v_fieldName9Vc = '' OR  @v_fieldName9Vc IS NULL THEN am.budgetType ELSE LEFT(@v_fieldName9Vc, 1) END OR
					am.budgetType = CASE WHEN @v_fieldName9Vc = '' OR  @v_fieldName9Vc IS NULL THEN am.budgetType ELSE RIGHT(@v_fieldName9Vc, 1) END)
		GROUP BY dm.departmentHirarchy
		order BY dm.departmentHirarchy

		UPDATE	@v_appProgDetail
		SET		totalWorksApp = pnd.pubApp,
				totalWorksAward = pnd.awardCont,
				totalApps = totalApps + pnd.pubApp,
				totalAward = totalAward + pnd.awardCont
		FROM	@v_appProgDetail apd
			INNER JOIN @v_procNatureData pnd ON pnd.offices = apd.offices

		DELETE FROM @v_procNatureData
	END

	-- SERVICE DATA SELECT AND STORE IN TEMP_TABLE
	IF @v_fieldName5Vc = 'services' OR @v_fieldName5Vc = '' OR @v_fieldName5Vc = '3'
	BEGIN
		INSERT INTO @v_procNatureData
		select	dm.departmentHirarchy,
				COUNT(aps.appId) srvssPkg,
				COUNT(css.contractSignId) srvssAward
		from	tbl_AppMaster am
				INNER JOIN tbl_DepartmentMaster dm on dm.departmentId = am.departmentId
				INNER JOIN tbl_OfficeMaster om on om.departmentId = dm.departmentId
						AND om.officeId = am.officeId
				INNER JOIN tbl_AppPackages aps on aps.appId = am.appId
						AND aps.appStatus='approved'
						AND aps.procurementnature = 'services'
				LEFT JOIN  tbl_TenderMaster tms on tms.appId = aps.appId
							AND tms.packageId = aps.packageId
							AND tms.financialYear = am.financialYear
				LEFT JOIN tbl_NoaIssueDetails nds  on nds.tenderId = tms.tenderId
							AND nds.noaIssueId IN (SELECT noaIssueId FROM tbl_NoaAcceptance WHERE acceptRejStatus = 'approved')
				LEFT JOIN tbl_ContractSign css on css.noaId = nds.noaIssueId
		where	am.appStatus = 'approved'
				AND am.financialYear = CASE WHEN @v_fieldName2Vc = '' OR @v_fieldName2Vc IS NULL THEN ''+am.financialYear+'' ELSE ''+@v_fieldName2Vc+'' END
				AND am.departmentId = CASE WHEN @v_fieldName3Vc = '' OR @v_fieldName3Vc IS NULL THEN am.departmentId ELSE @v_fieldName3Vc END
				AND am.officeId = CASE WHEN @v_fieldName4Vc = '' OR @v_fieldName4Vc IS NULL THEN am.officeId ELSE @v_fieldName4Vc END
				AND aps.procurementNature = CASE WHEN @v_fieldName5Vc = '' OR @v_fieldName5Vc IS NULL THEN ''+aps.procurementNature+'' ELSE ''+@v_fieldName5Vc+'' END
				AND om.stateId = CASE WHEN @v_fieldName6Vc = '' OR @v_fieldName6Vc IS NULL THEN om.stateId ELSE @v_fieldName6Vc END
				AND aps.cpvCode like CASE WHEN @v_fieldName7Vc = '' OR @v_fieldName7Vc IS NULL THEN '%'+aps.cpvCode+'%' ELSE '%'+@v_fieldName7Vc+'%' END
				AND aps.procurementMethodId = CASE WHEN @v_fieldName8Vc = '' OR @v_fieldName8Vc IS NULL THEN aps.procurementMethodId ELSE @v_fieldName8Vc END
				AND (am.budgetType = CASE WHEN @v_fieldName9Vc = '' OR  @v_fieldName9Vc IS NULL THEN am.budgetType ELSE LEFT(@v_fieldName9Vc, 1) END OR
					am.budgetType = CASE WHEN @v_fieldName9Vc = '' OR  @v_fieldName9Vc IS NULL THEN am.budgetType ELSE RIGHT(@v_fieldName9Vc, 1) END)
		GROUP BY dm.departmentHirarchy
		order BY dm.departmentHirarchy

		UPDATE	@v_appProgDetail
		SET		totalSrvsApp = pnd.pubApp,
				totalSrvsAward = awardCont,
				totalApps = totalApps + pnd.pubApp,
				totalAward = totalAward + pnd.awardCont
		FROM	@v_appProgDetail apd
			INNER JOIN @v_procNatureData pnd ON pnd.offices = apd.offices

		DELETE FROM @v_procNatureData
	END

	SELECT	offices AS FieldValue1,
			CONVERT(VARCHAR(50),CONVERT(NUMERIC(10,3),(estCost/10000000))) AS FieldValue2,
			CONVERT(VARCHAR(50),totalGoodsApp) AS FieldValue3,
			CONVERT(VARCHAR(50),totalWorksApp) AS FieldValue4,
			CONVERT(VARCHAR(50),totalSrvsApp) AS FieldValue5,
			CONVERT(VARCHAR(50),totalApps) AS FieldValue6,
			CONVERT(VARCHAR(50),totalGoodsAward) AS FieldValue7,
			CONVERT(VARCHAR(50),totalWorksAward) AS FieldValue8,
			CONVERT(VARCHAR(50),totalSrvsAward) AS FieldValue9,
			CONVERT(VARCHAR(50),totalAward) AS FieldValue10,
			CONVERT(VARCHAR(50),CONVERT(NUMERIC(25,3),((totalAward/totalApps)*100))) AS FieldValue11,
			CONVERT(VARCHAR(50),ISNULL(CONVERT(NUMERIC(10,3),(costOfAwardCont/10000000)),0)) AS FieldValue12,
			CONVERT(VARCHAR(50),ISNULL(CONVERT(NUMERIC(10,3),(actualExp/10000000)),0)) AS FieldValue13,
			CONVERT(VARCHAR(50),CONVERT(NUMERIC(25,3),(ISNULL(((actualExp/estCost)*100),0)))) AS FieldValue14
	FROM	@v_appProgDetail
END


If @v_fieldName1Vc='getResponsiveBidderCountAfterNOA'
Begin

Declare @field1 int
Declare @field2 int


if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
	SET @evalCount = 0
 else
	select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

select @field2 = COUNT(tbl_noaissuedetails.userId)  from tbl_noaacceptance inner join tbl_noaissuedetails 
	on tbl_noaissuedetails.noaissueId = tbl_noaacceptance.noaissueId where tenderId = @v_fieldName2Vc and acceptRejStatus in ('decline','Performance Security not paid')

--if(@field2 = 0)
--	select @field1 = COUNT(userId) from tbl_EvalBidderStatus where tenderId=@v_fieldName2Vc and bidderStatus='Technically Responsive' and evalCount = @evalCount
if ((select TOP 1 DATEDIFF(Minute, ersa.sentDate, tnid.createdDt) from tbl_NoaIssueDetails tnid ,tbl_EvalRptSentToAA  ersa
			where tnid.tenderId = @v_fieldName2Vc and tnid.tenderId = ersa.tenderId and ersa.evalCount != 0 and ersa.evalCount = (select MAX(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc))<0)		
	if(select COUNT(userId) from tbl_EvalBidderStatus where tenderId=@v_fieldName2Vc and bidderStatus in ('Technically Responsive') and evalCount = @evalCount) = 0
		set @field1 = 0
	else
		select @field1 = COUNT(userId) from tbl_EvalBidderStatus where tenderId=@v_fieldName2Vc and bidderStatus in ('Technically Responsive','Technically Unresponsive') and evalCount = @evalCount-1
else 
	select @field1 = COUNT(userId) from tbl_EvalBidderStatus where tenderId=@v_fieldName2Vc and bidderStatus='Technically Responsive' and evalCount = @evalCount	
if (convert(varchar(50),@field1-@field2) < 0)
	set @field2 = 0
else
	set @field2 = convert(varchar(50),@field1-@field2)
select convert(varchar(50),@field1) as FieldValue1,convert(varchar(50),@field2) as FieldValue2
	
End
--Modified By Dohatec for re-evaluation
If @v_fieldName1Vc='getResponsiveBidderCountService'
Begin
	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
	SET @evalCount = 0
 else
	select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

	select CONVERT(varchar(20),COUNT(userId)) as FieldValue1
	from tbl_EvalBidderStatus
	where tenderId=@v_fieldName2Vc and bidderStatus='evaluated' 
		and result = 'Pass' and evalCount = @evalCount
End
--Modified By Dohatec for re-evaluation
IF @v_fieldName1Vc = 'AllBidderDisqualifiedService'
BEGIN
Declare @totalCount varchar(50)
if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
	SET @evalCount = 0
 else
	select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

set @totalCount = (SELECT COUNT(DISTINCT CONVERT(varchar(20),negId)) as FieldValue1 
from tbl_Negotiation where negStatus='Failed' and evalCount = @evalCount and
tenderid=@v_fieldName2Vc) 

IF @totalCount  = 
(SELECT COUNT (DISTINCT convert(varchar(20), tbl_FinalSubmission.userId)) as FieldValue1
FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
  and tenderid = @v_fieldName2Vc
   and userId not in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and evalCount = @evalCount and
  (isnull(result,'pass')='fail'))
  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc))
	Begin
		select '1' AS FieldValue1
	END
 ELSE
	Begin
		select '0' AS FieldValue1
	END
 
END
--Added By Dohatec for re-evaluation
IF @v_fieldName1Vc = 'AllBidderDisqualifiedServiceAfterNOA'
BEGIN
Declare @totalCount1 varchar(50)
if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
	SET @evalCount = 0
 else
	select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

set @totalCount1 = (SELECT COUNT(DISTINCT CONVERT(varchar(20),negId)) as FieldValue1 
from tbl_Negotiation where negStatus='Failed' and evalCount = @evalCount and 
tenderid=@v_fieldName2Vc) + (SELECT    COUNT(tna.userId) AS NOADeclineNumber FROM 
tbl_NoaAcceptance tna INNER JOIN tbl_NoaIssueDetails tnd 
ON tna.noaIssueId = tnd.noaIssueId where tna.acceptRejType = 'By System'
and tna.acceptRejStatus IN ('decline','Performance Security not paid') and tnd.tenderId =@v_fieldName2Vc)

IF @totalCount1  = 
(SELECT COUNT (DISTINCT convert(varchar(20), tbl_FinalSubmission.userId)) as FieldValue1
FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
  and tenderid = @v_fieldName2Vc
   and userId not in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and evalCount = @evalCount and 
  (isnull(result,'pass')='fail'))
  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc))
	Begin
		select '1' AS FieldValue1
	END
 ELSE
	Begin
		select '0' AS FieldValue1
	END
 
END


GO
