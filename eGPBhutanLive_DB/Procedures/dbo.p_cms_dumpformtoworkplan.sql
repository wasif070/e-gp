SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_cms_dumpformtoworkplan] 
	-- Add the parameters for the stored procedure here
	@tenderid int = NULL, 
	@pkglotid int = NULL,
	@contractid int = NULL,
	@userid int = NULL
AS
declare @procurementType int
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select @procurementType = (Select procurementMethodId from tbl_TenderDetails where tenderId = @tenderid)
	print @procurementType
	if(@procurementType = 1) -- Goods
	begin
		select * from tbl_ProcurementNature
	end
	else
	begin
		select * from tbl_TenderMaster where tenderId = @tenderid
	end
	
END


GO
