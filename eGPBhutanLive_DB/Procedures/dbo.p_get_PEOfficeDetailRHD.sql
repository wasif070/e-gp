SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--
-- Purpose: PE Sharing WebService Reports For RHD Dept
--
--
-- Author: Sudhir Chahvan
-- Date: 15-07-2011
--
-- Last Modified:
-- Modified By: 
-- Date:
-- Modification:
--------------------------------------------------------------------------------
-- SP Name	: p_get_PEOfficeDetailRHD
-- Module	: RHDPEIntegration
-- Function	: Returns promis and RHD Data for RHDPEIntegration WebService
/*

EXEC [dbo].[p_get_PEOfficeDetailRHD]

*/


CREATE PROCEDURE [dbo].[p_get_PEOfficeDetailRHD]
AS
BEGIN
	SELECT	
	officeId AS PEID,
	officeName AS PEOfficeName,
	dm.departmentType AS DeptType,
	
	(CASE dm.departmentType 
				WHEN 'Ministry' THEN dm.departmentName 
				WHEN 'Division' THEN pdm.departmentName 
				WHEN 'Organization' THEN
				 (CASE WHEN pdm.departmentType = 'Division' THEN 
				       (SELECT	departmentName 
						FROM	tbl_DepartmentMaster 
						WHERE	departmentId=pdm.parentDepartmentId )
								WHEN pdm.departmentType = 'Ministry' THEN pdm.departmentName 
			ELSE '' END) END )
			AS MinistryName,

	(CASE dm.departmentType 
			 WHEN 'Division' THEN dm.departmentName
			 WHEN 'Organization' THEN 
			    (CASE 
					   WHEN pdm.departmentType = 'Division' THEN pdm.departmentName 
					   ELSE 'N/A' END)
			     ELSE 'N/A' END) AS DivisionName,
		     			
	(CASE dm.departmentType 
			 WHEN 'Organization'	THEN dm.departmentName
			 ELSE 'N/A' END) AS RHDOrgName,

	stateName AS DistrictName,
	om.address AS OfficeAddr
	FROM	tbl_DepartmentMaster dm,
			tbl_OfficeMaster om, 
			tbl_DepartmentMaster pdm, 
			tbl_StateMaster sm
	WHERE	dm.departmentId=om.departmentId
			AND dm.parentDepartmentId=pdm.departmentId
			AND om.departmentId = (SELECT dm1.departmentId FROM tbl_DepartmentMaster dm1 
			     WHERE dm1.departmentName = 'Roads and Highways Department (RHD)')
			AND om.stateId=sm.stateId
	ORDER BY officeId ASC
END


GO
