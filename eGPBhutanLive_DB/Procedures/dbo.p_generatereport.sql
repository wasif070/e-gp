SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Kinjal Shah>
-- alter date: <alter Date,,>
-- Description:    Generate Price Comparation Report

-- =============================================
-- exec [p_generatereport] 576,5, 1596

-- SP Name	:   p_generatereport
-- Module	:	Evaluation, Opening (Price Comparation Report)
-- Funftion	:	N.A.

CREATE PROCEDURE [dbo].[p_generatereport]
    -- Add the parameters for the stored procedure here
    @v_rptTableId_inInt INT,
    @v_columnId_inInt INT,
    @v_userId_inInt INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;


/* START : PARA DECLARATION */

    DECLARE @v_rptFormula VARCHAR(MAX), @v_dataId varchar(5000), @v_cnt int, @v_formulaStr varchar(8000), @v_condStr varchar(MAX), @v_cellVal varchar(5000), @v_tableId float, @v_colId float, @v_celId  float, @v_formulaExeStr nvarchar(MAX), @v_sql_statement nvarchar(4000), @v_result money,@v_wordcnt int,@v_lotId int, @v_cellVal1 money, @v_estCost money=0.00, @v_displayformula varchar(2000),@v_CellFilledBy_int int

/* END : PARA DECLARATION */


SET @v_condStr = ''
SET @v_formulaExeStr = ''
SET @v_cnt = 1


/* START */

/*SELECt Estimated cost*/
print 'estcost1'
    select  top 1 @v_estCost = estCost from tbl_ReportTableMaster rt, tbl_ReportLots rl,
     tbl_TenderEstCost EC ,tbl_ReportMaster rm WHERE
     RT.reportId = rl.reportId AND rl.pkgLotId = EC.pkgLotId
     AND reportTableId = @v_rptTableId_inInt
	 AND rt.reportId = rm.reportId AND rm.tenderId = EC.tenderId -- Added By Dohatec to solve eventum issue #5588
     print 'estcost2'+convert(varchar(20),@v_estCost)
     if @v_estCost=0.00
     begin
       select  top 1 @v_estCost = estCost from tbl_ReportTableMaster rt,
		tbl_ReportMaster rl,
		tbl_TenderEstCost EC WHERE
		RT.reportId = rl.reportId AND rl.tenderId = EC.tenderId
		AND reportTableId = @v_rptTableId_inInt
     end

print 'estcost3'+convert(varchar(20),@v_estCost)

/*END Estimated Cost*/


SELECT @v_rptFormula = formula, @v_displayformula =displayFormula FROM tbl_ReportFormulaMaster WHERE reportTableId = @v_rptTableId_inInt and columnId = @v_columnId_inInt

    print @v_rptFormula
     --if @v_rptFormula not like 'estcost%'
     --begin
    DECLARE cur_rptDetail CURSOR FAST_FORWARD For SELECT items FROM [dbo].[f_split]( REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE (Replace (ltrim(rtrim(@v_rptFormula)), '+','_'),')','_'),'(','_'), '-','_'),'*','_'),'/','_'),'%','_'),'_') WHERE Items <> ''

            OPEN cur_rptDetail
            FETCH NEXT FROM cur_rptDetail INTO @v_dataId

            WHILE @@Fetch_status = 0
            BEGIN
            print 'dataid: '+@v_dataId
            print CHARINDEX('.', @v_dataId)

            IF CHARINDEX('N', @v_dataId) = 0
            BEGIN
            IF CHARINDEX('W', @v_dataId) = 0
            BEGIN

            IF CHARINDEX('.', @v_dataId) = 0
            BEGIN


            Print 'dataval222 : ' +@v_dataId
            Print 'cnt : ' + convert(varchar(20), @v_cnt)

            IF @v_cnt = 4
            BEGIN
            print 'final data123: '+ @v_formulaStr
				select @v_CellFilledBy_int=  filledBy from tbl_TenderColumns where
				tenderTableId=@v_tableId and columnId=@v_colId

				print 'filby'+convert(varchar(20),@v_CellFilledBy_int)

				if @v_CellFilledBy_int=1
				begin

                SELECT @v_cellVal1 = isnull(Sum(cast(cellValue as money)),0)
                FROM tbl_TenderCells where
                --SELECT @v_cellVal = case when ISNUMERIC(tbpd.cellValue)=1 then Sum(cast(tbpd.cellValue as numeric)) else 0 end FROM tbl_TenderBidPlainData tbpd INNER JOIN

                tenderTableId = @v_tableId and columnId = @v_colId and  cellid = @v_celId
                print 'tenderTableId'+convert(varchar(20),@v_cellVal1)
                print 'tenderColId'+convert(varchar(20),@v_cellVal1)
                print 'asdf'+convert(varchar(20),@v_cellVal1)

                print 'asdf'+convert(varchar(20),@v_cellVal1)
                end
                else
                begin
                SELECT @v_cellVal1 = isnull(Sum(cast(cellValue as money)),0) FROM --tbl_TenderBidPlainData Modified By dohatec to fetch distinct data
                (SELECT DISTINCT tbp.bidTableId, tbp.tenderCelId,tbp.tenderColId,
				tbp.tenderTableId,tbp.cellValue,tbp.rowId,tbp.tenderFormId,
				tbp.cellId,tbp.submissionDt FROM 
                tbl_TenderBidPlainData tbp )
                 tbpd INNER JOIN
                --SELECT @v_cellVal = case when ISNUMERIC(tbpd.cellValue)=1 then Sum(cast(tbpd.cellValue as numeric)) else 0 end FROM tbl_TenderBidPlainData tbpd INNER JOIN
                tbl_TenderBidTable  tbd ON tbpd.bidTableId = tbd.bidTableId AND
                tbpd.bidTableId = tbd.bidTableId INNER JOIN
                tbl_TenderBidForm tbf ON tbd.bidId = tbf.bidId AND tbd.bidId = tbf.bidId AND
                tbpd.tenderTableId = @v_tableId and tbpd.tenderColId = @v_colId and tbpd.tenderCelId = @v_celId and tbpd.cellId IS NULL and UserId = @v_userId_inInt
                end
                set @v_cellVal=CONVERT(varchar(5000),@v_cellVal1,2)
                print 'final data456: '+ @v_cellVal


                print 'final formula exec cnt=4 :'+@v_formulaExeStr


                if (@v_rptFormula like '%+%' or @v_rptFormula like '%*%' or @v_rptFormula like '%-%' or @v_rptFormula like '%/%' )and( @v_formulaExeStr <>'' and @v_formulaExeStr is not null)
                begin
                print 'NEw cnt 4:' + 'REPLACE('+ @v_formulaExeStr +','+ @v_formulaStr+' ,'+@v_cellVal+')'
                SET @v_formulaExeStr =  REPLACE(@v_formulaExeStr, @v_formulaStr ,@v_cellVal)
                print 'final formula like cnt 4 + :'+ @v_formulaExeStr
                END
                Else
                BEGIN
                SET @v_formulaExeStr = @v_formulaExeStr + REPLACE(@v_rptFormula, @v_formulaStr ,@v_cellVal)
                print 'final formula'+ @v_formulaExeStr
                END



                /*
                ORIGINAL ABOVE FORMULA


                SET @v_formulaExeStr = @v_formulaExeStr + REPLACE(@v_rptFormula, @v_formulaStr ,@v_cellVal)
                print 'final formula exe str : '+ @v_formulaExeStr
                */




                SET @v_cnt = 1
                SET @v_tableId =0
                SET @v_formulaStr =0
                IF CHARINDEX('_', @v_formulaExeStr) = 0
                Break;

            END

            IF @v_cnt = 1
            BEGIN
            Print 'dataval 333: ' +@v_dataId
                SET @v_tableId =  @v_dataId
                SET @v_formulaStr = @v_dataId
            END
            IF @v_cnt = 2
            BEGIN
                SET @v_colId = @v_dataId
                SET @v_formulaStr = @v_formulaStr +'_'+@v_dataId
            END
            IF @v_cnt = 3
            BEGIN
                SET @v_celId =  @v_dataId
                SET @v_formulaStr = @v_formulaStr +'_'+@v_dataId
            END

            Print 'tableid : '+ convert(varchar(20), @v_tableId)
            Print 'column id : '+ convert(varchar(20), @v_colId)
            Print 'cel id : '+ convert(varchar(20), @v_celId)
            Print 'Formula : '+ @v_formulaStr
            Print '--------------------'

            SET @v_cnt = @v_cnt +1
            END
            ELSE /*Estimate cost condition*/
            BEGIN

                if @v_displayformula like '%est_cost%'
                BEGIN
                set @v_rptFormula = REPLACE(@v_rptFormula, @v_dataId,@v_estCost)
                END
            END
            END
            END
            FETCH NEXT FROM cur_rptDetail INTO @v_dataId
            END


            CLOSE cur_rptDetail
            DEALLOCATE cur_rptDetail

            IF @v_cnt > 3
            BEGIN
            PRINT 'iN OUTER LOOP11 : '

            select @v_CellFilledBy_int=  filledBy from tbl_TenderColumns where
				tenderTableId=@v_tableId and columnId=@v_colId
				print 'fil11111by'+convert(varchar(20),@v_CellFilledBy_int)
				if @v_CellFilledBy_int=1
				begin

                SELECT @v_cellVal1 = isnull(Sum(cast(cellValue as money)),0)
                FROM tbl_TenderCells where
                --SELECT @v_cellVal = case when ISNUMERIC(tbpd.cellValue)=1 then Sum(cast(tbpd.cellValue as numeric)) else 0 end FROM tbl_TenderBidPlainData tbpd INNER JOIN

                tenderTableId = @v_tableId and columnId = @v_colId and  cellId = @v_celId

                end
                else
                begin

                SELECT @v_cellVal1 = isnull(Sum(cast(cellValue as money)),0) FROM --tbl_TenderBidPlainData Modified By dohatec to fetch distinct data
                (SELECT DISTINCT tbp.bidTableId, tbp.tenderCelId,tbp.tenderColId,
				tbp.tenderTableId,tbp.cellValue,tbp.rowId,tbp.tenderFormId,
				tbp.cellId,tbp.submissionDt FROM 
                tbl_TenderBidPlainData tbp ) 
                tbpd INNER JOIN
                --SELECT @v_cellVal = case when ISNUMERIC(tbpd.cellValue)=1 then Sum(cast(tbpd.cellValue as numeric)) else 0 end FROM tbl_TenderBidPlainData tbpd INNER JOIN
                tbl_TenderBidTable  tbd ON tbpd.bidTableId = tbd.bidTableId AND
                tbpd.bidTableId = tbd.bidTableId INNER JOIN
                tbl_TenderBidForm tbf ON tbd.bidId = tbf.bidId AND tbd.bidId = tbf.bidId AND
                tbpd.tenderTableId = @v_tableId and tbpd.tenderColId = @v_colId
                and tbpd.tenderCelId = @v_celId and UserId = @v_userId_inInt
                end
                print @v_cellVal1
                set @v_cellVal=CONVERT(varchar(5000),@v_cellVal1,2)
                print @v_cellVal
                print  @v_tableId
                print '@v_celId'+convert(varchar(20),@v_celId)
                print '@@v_userId_inInt'+convert(varchar(20),@v_userId_inInt)
                print '@v_colId'+convert(varchar(20),@v_colId)
                print '@v_rptFormula'+@v_rptFormula
                print 'final data'+ @v_formulaStr
                print 'cel val : ' + @v_cellVal
                print 'Formula exe val : ' + @v_formulaExeStr
                PRINT '@v_displayformula'+@v_displayformula
                PRINT '@@v_rptFormula'+@v_rptFormula
                print '@v_dataId'+convert(varchar(500),@v_dataId)
                print '@v_estCost'+convert(varchar(500),@v_estCost)
                if @v_displayformula like '%est_cost%' AND (convert(varchar(500),@v_dataId) like '%.00')
                BEGIN
                set @v_rptFormula = REPLACE(@v_rptFormula, @v_dataId,@v_estCost)
                set @v_formulaExeStr = REPLACE(@v_formulaExeStr, @v_dataId,@v_estCost)
                END
                print 'NEW : ' + @v_formulaExeStr + 'REPLACE('+@v_rptFormula+','+ @v_formulaStr +','+@v_cellVal+')'
                if @v_rptFormula like '%+%' or @v_rptFormula like '%*%' or @v_rptFormula like '%-%' or @v_rptFormula like '%/%'
                begin
                /*changes for (-) Value*/
                print '@v_rptFormula'+@v_rptFormula
                IF @v_formulaExeStr = ''
                Begin
					SET @v_formulaExeStr =  REPLACE(@v_rptFormula, @v_formulaStr ,@v_cellVal)
                END
                Else
                Begin
                SET @v_formulaExeStr =  REPLACE(@v_formulaExeStr, @v_formulaStr ,@v_cellVal)
                END
                /*ENd CHange */

                print 'final formula like + :'+ @v_formulaExeStr
                END
                Else
                BEGIN
                SET @v_formulaExeStr = @v_formulaExeStr + REPLACE(@v_rptFormula, @v_formulaStr ,@v_cellVal)
                print 'final formula'+ @v_formulaExeStr
                END


            END

            Print 'Formula Exec: '+@v_formulaExeStr

            IF @v_formulaExeStr like '%Word%'
            BEGIN
                SET @v_wordcnt = 1
                SET @v_formulaExeStr = replace(@v_formulaExeStr,'word','')
            END
            IF @v_formulaExeStr like '%N%'
            BEGIN
                --SET @v_wordcnt = 1
                SET @v_formulaExeStr = replace(@v_formulaExeStr,'N','')
            END

                SET @v_sql_statement = N'SET @v_result = ' + @v_formulaExeStr

                EXEC sp_executesql
                @v_sql_statement,
                N'@v_result money out',
                @v_result = @v_result out
                print @v_result

                SELECT CASE WHEN @v_wordcnt = 1 THEN  'W_'+ convert(varchar(500),@v_result,2) Else convert(varchar(500),@v_result,2) end as result

    --end
    --else
    --begin
    --SELECT @v_lotId=cast(items as int) FROM [dbo].[f_split](@v_rptFormula,'_') where Items != 'estcost'

    --select convert(varchar(500),estcost) as result from tbl_tenderestcost where pkglotid=@v_lotId
    --end

/* END */
END


GO
