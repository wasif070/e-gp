SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalReportQry] (
		[evalRptClrId]         [int] IDENTITY(1, 1) NOT NULL,
		[evalRptReqId]         [int] NOT NULL,
		[evalRptId]            [int] NOT NULL,
		[reportQuery]          [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[expRspDt]             [date] NOT NULL,
		[sentBy]               [int] NOT NULL,
		[sentTo]               [int] NOT NULL,
		[reportResponse]       [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[reportResponseDt]     [date] NULL,
		[reportQryDt]          [smalldatetime] NULL,
		CONSTRAINT [PK_tbl_EvalReportQry]
		PRIMARY KEY
		CLUSTERED
		([evalRptClrId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalReportQry]
	WITH CHECK
	ADD CONSTRAINT [quryReportReq_FK1]
	FOREIGN KEY ([evalRptReqId]) REFERENCES [dbo].[tbl_EvalReportReq] ([evalRptReqId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EvalReportQry]
	CHECK CONSTRAINT [quryReportReq_FK1]

GO
ALTER TABLE [dbo].[tbl_EvalReportQry] SET (LOCK_ESCALATION = TABLE)
GO
