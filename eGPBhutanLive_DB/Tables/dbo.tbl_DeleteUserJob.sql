SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_DeleteUserJob] (
		[deleteUserJobID]     [int] IDENTITY(1, 1) NOT NULL,
		[JobStatus]           [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[runDate]             [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_DeleteUserJob] SET (LOCK_ESCALATION = TABLE)
GO
