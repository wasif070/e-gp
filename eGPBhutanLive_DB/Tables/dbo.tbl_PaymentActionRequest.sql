SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_PaymentActionRequest] (
		[requestId]           [int] IDENTITY(1, 1) NOT NULL,
		[paymentId]           [int] NOT NULL,
		[requestBy]           [int] NOT NULL,
		[requestTo]           [int] NOT NULL,
		[requestAction]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[requestComments]     [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[actionStatus]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[actionCompDt]        [datetime] NOT NULL,
		[actionComments]      [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[requestDate]         [datetime] NOT NULL,
		CONSTRAINT [PK_tbl_PaymentActionRequest]
		PRIMARY KEY
		CLUSTERED
		([requestId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_PaymentActionRequest] SET (LOCK_ESCALATION = TABLE)
GO
