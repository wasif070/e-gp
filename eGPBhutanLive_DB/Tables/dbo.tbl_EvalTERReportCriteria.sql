SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalTERReportCriteria] (
		[criteriaId]          [int] IDENTITY(1, 1) NOT NULL,
		[criteria]            [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isAppInGoods]        [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isAppInWorks]        [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isAppInServices]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[reportType]          [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_EvalTERReportCriteria]
		PRIMARY KEY
		CLUSTERED
		([criteriaId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalTERReportCriteria] SET (LOCK_ESCALATION = TABLE)
GO
