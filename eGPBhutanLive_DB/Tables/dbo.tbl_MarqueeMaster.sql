SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_MarqueeMaster] (
		[marqueeId]          [int] IDENTITY(1, 1) NOT NULL,
		[marqueeText]        [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[marqueeStartDt]     [datetime] NOT NULL,
		[marqueeEndDt]       [datetime] NOT NULL,
		[locationId]         [smallint] NOT NULL,
		[userTypeId]         [int] NOT NULL,
		[createdBy]          [int] NOT NULL,
		[createdTime]        [smalldatetime] NOT NULL,
		CONSTRAINT [PK_tbl_MarqueeMaster]
		PRIMARY KEY
		CLUSTERED
		([marqueeId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_MarqueeMaster]
	ADD
	CONSTRAINT [DF_tbl_MarqueeMaster_createdBy]
	DEFAULT ((0)) FOR [createdBy]
GO
ALTER TABLE [dbo].[tbl_MarqueeMaster]
	ADD
	CONSTRAINT [DF_tbl_MarqueeMaster_createdTime]
	DEFAULT (getdate()) FOR [createdTime]
GO
ALTER TABLE [dbo].[tbl_MarqueeMaster]
	WITH CHECK
	ADD CONSTRAINT [marqueeLocationId_FK1]
	FOREIGN KEY ([locationId]) REFERENCES [dbo].[tbl_LocationMaster] ([locationId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_MarqueeMaster]
	CHECK CONSTRAINT [marqueeLocationId_FK1]

GO
ALTER TABLE [dbo].[tbl_MarqueeMaster] SET (LOCK_ESCALATION = TABLE)
GO
