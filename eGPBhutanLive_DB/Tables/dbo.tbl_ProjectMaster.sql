SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ProjectMaster] (
		[projectId]            [int] IDENTITY(1, 1) NOT NULL,
		[projectName]          [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[projectCode]          [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[progId]               [int] NOT NULL,
		[projectCost]          [money] NOT NULL,
		[projectStartDate]     [date] NOT NULL,
		[projectEndDate]       [date] NOT NULL,
		[sourceOfFund]         [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[projectStatus]        [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]            [int] NOT NULL,
		[createdDate]          [datetime] NOT NULL,
		CONSTRAINT [PK_tbl_ProjectMaster]
		PRIMARY KEY
		CLUSTERED
		([projectId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ProjectMaster]
	ADD
	CONSTRAINT [DF_tbl_ProjectMaster_createdBy]
	DEFAULT ((0)) FOR [createdBy]
GO
ALTER TABLE [dbo].[tbl_ProjectMaster]
	ADD
	CONSTRAINT [DF_tbl_ProjectMaster_createdDate]
	DEFAULT (getdate()) FOR [createdDate]
GO
ALTER TABLE [dbo].[tbl_ProjectMaster]
	ADD
	CONSTRAINT [DF_tbl_ProjectMaster_projectStatus]
	DEFAULT ('Pending') FOR [projectStatus]
GO
ALTER TABLE [dbo].[tbl_ProjectMaster] SET (LOCK_ESCALATION = TABLE)
GO
