SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_AppEngEstDoc] (
		[appEngEstId]      [int] IDENTITY(1, 1) NOT NULL,
		[appId]            [int] NOT NULL,
		[packageId]        [int] NOT NULL,
		[documentName]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[documentDesc]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]     [smalldatetime] NOT NULL,
		[uploadedBy]       [int] NOT NULL,
		[docSize]          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [appEngEstId_PK]
		PRIMARY KEY
		CLUSTERED
		([appEngEstId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_AppEngEstDoc]
	WITH NOCHECK
	ADD CONSTRAINT [engAppId_FK1]
	FOREIGN KEY ([appId]) REFERENCES [dbo].[tbl_AppMaster] ([appId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_AppEngEstDoc]
	CHECK CONSTRAINT [engAppId_FK1]

GO
ALTER TABLE [dbo].[tbl_AppEngEstDoc]
	WITH NOCHECK
	ADD CONSTRAINT [engPackageId_FK2]
	FOREIGN KEY ([packageId]) REFERENCES [dbo].[tbl_AppPackages] ([packageId])
ALTER TABLE [dbo].[tbl_AppEngEstDoc]
	CHECK CONSTRAINT [engPackageId_FK2]

GO
ALTER TABLE [dbo].[tbl_AppEngEstDoc]
	WITH NOCHECK
	ADD CONSTRAINT [FKBEB0223014F1608F]
	FOREIGN KEY ([packageId]) REFERENCES [dbo].[tbl_AppPackages] ([packageId])
ALTER TABLE [dbo].[tbl_AppEngEstDoc]
	CHECK CONSTRAINT [FKBEB0223014F1608F]

GO
ALTER TABLE [dbo].[tbl_AppEngEstDoc]
	WITH NOCHECK
	ADD CONSTRAINT [FKBEB02230448F6D5F]
	FOREIGN KEY ([appId]) REFERENCES [dbo].[tbl_AppMaster] ([appId])
ALTER TABLE [dbo].[tbl_AppEngEstDoc]
	CHECK CONSTRAINT [FKBEB02230448F6D5F]

GO
ALTER TABLE [dbo].[tbl_AppEngEstDoc] SET (LOCK_ESCALATION = TABLE)
GO
