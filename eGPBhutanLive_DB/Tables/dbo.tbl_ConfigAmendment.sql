SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_ConfigAmendment] (
		[configAmendmentId]       [int] IDENTITY(1, 1) NOT NULL,
		[amendMinDays]            [smallint] NOT NULL,
		[amendPercent]            [smallint] NOT NULL,
		[procurementMethodId]     [tinyint] NOT NULL,
		[procurementTypeId]       [tinyint] NOT NULL,
		CONSTRAINT [UQ__tbl_Conf__5D5CBA5A034FF802]
		UNIQUE
		NONCLUSTERED
		([configAmendmentId])
		ON [PRIMARY],
		CONSTRAINT [PK__tbl_Conf__5D5CBA5B5B59A644]
		PRIMARY KEY
		CLUSTERED
		([configAmendmentId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ConfigAmendment]
	WITH CHECK
	ADD CONSTRAINT [FKA5C9B4AA5C4169D]
	FOREIGN KEY ([procurementMethodId]) REFERENCES [dbo].[tbl_ProcurementMethod] ([procurementMethodId])
ALTER TABLE [dbo].[tbl_ConfigAmendment]
	CHECK CONSTRAINT [FKA5C9B4AA5C4169D]

GO
ALTER TABLE [dbo].[tbl_ConfigAmendment]
	WITH CHECK
	ADD CONSTRAINT [FKA5C9B4AA90681AE]
	FOREIGN KEY ([procurementTypeId]) REFERENCES [dbo].[tbl_ProcurementTypes] ([procurementTypeId])
ALTER TABLE [dbo].[tbl_ConfigAmendment]
	CHECK CONSTRAINT [FKA5C9B4AA90681AE]

GO
ALTER TABLE [dbo].[tbl_ConfigAmendment] SET (LOCK_ESCALATION = TABLE)
GO
