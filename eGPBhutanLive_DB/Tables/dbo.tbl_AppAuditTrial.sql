SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_AppAuditTrial] (
		[appAuditTrialId]     [int] IDENTITY(1, 1) NOT NULL,
		[action]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[actionDate]          [datetime] NOT NULL,
		[childId]             [int] NOT NULL,
		[signature]           [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[appId]               [int] NOT NULL,
		[actionBy]            [int] NOT NULL,
		[remarks]             [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [UQ__tbl_AppA__E55C280232FE3320]
		UNIQUE
		NONCLUSTERED
		([appAuditTrialId])
		ON [PRIMARY],
		CONSTRAINT [PK__tbl_AppA__E55C28031A5DC337]
		PRIMARY KEY
		CLUSTERED
		([appAuditTrialId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_AppAuditTrial] SET (LOCK_ESCALATION = TABLE)
GO
