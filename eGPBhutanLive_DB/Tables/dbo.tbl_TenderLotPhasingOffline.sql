SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_TenderLotPhasingOffline] (
		[tenderLotPhaId]            [int] IDENTITY(1, 1) NOT NULL,
		[tenderOfflineId]           [int] NOT NULL,
		[lotOrRefNo]                [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[lotIdentOrPhasingServ]     [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[location]                  [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderSecurityAmt]         [money] NOT NULL,
		[startDateTime]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[completionDateTime]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [tenderLotSecIdOfflineNew_PK]
		PRIMARY KEY
		CLUSTERED
		([tenderLotPhaId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderLotPhasingOffline] SET (LOCK_ESCALATION = TABLE)
GO
