SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_TenderValRequest] (
		[tenderValRequestId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]               [int] NOT NULL,
		[valExtDtId]             [int] NOT NULL,
		[userId]                 [int] NOT NULL,
		[tenderValRequestDt]     [smalldatetime] NOT NULL,
		CONSTRAINT [PK_tbl_TenderValRequest]
		PRIMARY KEY
		CLUSTERED
		([tenderValRequestId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderValRequest]
	WITH NOCHECK
	ADD CONSTRAINT [requestTenderId_FK1]
	FOREIGN KEY ([valExtDtId]) REFERENCES [dbo].[tbl_TenderValidityExtDate] ([valExtDtId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderValRequest]
	CHECK CONSTRAINT [requestTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderValRequest] SET (LOCK_ESCALATION = TABLE)
GO
