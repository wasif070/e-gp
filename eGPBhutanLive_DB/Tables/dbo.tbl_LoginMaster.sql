SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_LoginMaster] (
		[userId]                  [int] IDENTITY(1, 1) NOT NULL,
		[emailId]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[password]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[hintQuestion]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[hintAnswer]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[registrationType]        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isJvca]                  [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[businessCountryName]     [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[nextScreen]              [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[userTyperId]             [tinyint] NOT NULL,
		[isEmailVerified]         [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[failedAttempt]           [tinyint] NOT NULL,
		[validUpTo]               [smalldatetime] NULL,
		[firstLogin]              [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isPasswordReset]         [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[resetPasswordCode]       [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[status]                  [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[registeredDate]          [smalldatetime] NOT NULL,
		[nationality]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[transDate]               [smalldatetime] NULL,
		[countResetSMS]           [tinyint] NOT NULL,
		CONSTRAINT [userId_PK]
		PRIMARY KEY
		CLUSTERED
		([userId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_LoginMaster]
	ADD
	CONSTRAINT [DF_tbl_LoginMaster_countResetSMS_1]
	DEFAULT ((0)) FOR [countResetSMS]
GO
ALTER TABLE [dbo].[tbl_LoginMaster]
	ADD
	CONSTRAINT [DF_tbl_LoginMaster_failedAttempt]
	DEFAULT ((0)) FOR [failedAttempt]
GO
ALTER TABLE [dbo].[tbl_LoginMaster]
	ADD
	CONSTRAINT [DF_tbl_LoginMaster_firstLogin]
	DEFAULT ('yes') FOR [firstLogin]
GO
ALTER TABLE [dbo].[tbl_LoginMaster]
	ADD
	CONSTRAINT [DF_tbl_LoginMaster_isEmailVerified]
	DEFAULT ('no') FOR [isEmailVerified]
GO
ALTER TABLE [dbo].[tbl_LoginMaster]
	ADD
	CONSTRAINT [DF_tbl_LoginMaster_isJvca]
	DEFAULT ('no') FOR [isJvca]
GO
ALTER TABLE [dbo].[tbl_LoginMaster]
	ADD
	CONSTRAINT [DF_tbl_LoginMaster_isPasswordReset]
	DEFAULT ('No') FOR [isPasswordReset]
GO
ALTER TABLE [dbo].[tbl_LoginMaster]
	ADD
	CONSTRAINT [DF_tbl_LoginMaster_status]
	DEFAULT ('incomplete') FOR [status]
GO
ALTER TABLE [dbo].[tbl_LoginMaster]
	ADD
	CONSTRAINT [DF_tbl_LoginMaster_userTyperId]
	DEFAULT ((2)) FOR [userTyperId]
GO
ALTER TABLE [dbo].[tbl_LoginMaster]
	WITH CHECK
	ADD CONSTRAINT [FK4F072C0AD3CBE34F]
	FOREIGN KEY ([userTyperId]) REFERENCES [dbo].[tbl_UserTypeMaster] ([userTypeId])
ALTER TABLE [dbo].[tbl_LoginMaster]
	CHECK CONSTRAINT [FK4F072C0AD3CBE34F]

GO
ALTER TABLE [dbo].[tbl_LoginMaster]
	WITH CHECK
	ADD CONSTRAINT [FK4F072C0AF30AF588]
	FOREIGN KEY ([userTyperId]) REFERENCES [dbo].[tbl_UserTypeMaster] ([userTypeId])
ALTER TABLE [dbo].[tbl_LoginMaster]
	CHECK CONSTRAINT [FK4F072C0AF30AF588]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Country name information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_LoginMaster', 'COLUMN', N'businessCountryName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Registering user email address information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_LoginMaster', 'COLUMN', N'emailId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Failed login attempts information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_LoginMaster', 'COLUMN', N'failedAttempt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'To identify user is login first time on eGp portal -Yes/No information 
stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_LoginMaster', 'COLUMN', N'firstLogin'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Registering user secret answer information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_LoginMaster', 'COLUMN', N'hintAnswer'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Registering user hint question information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_LoginMaster', 'COLUMN', N'hintQuestion'
GO
EXEC sp_addextendedproperty N'MS_Description', N'To identify registering user email is valid  or not ,Yes/No', 'SCHEMA', N'dbo', 'TABLE', N'tbl_LoginMaster', 'COLUMN', N'isEmailVerified'
GO
EXEC sp_addextendedproperty N'MS_Description', N'To identify company is registering  as joint venture or not -Yes/No', 'SCHEMA', N'dbo', 'TABLE', N'tbl_LoginMaster', 'COLUMN', N'isJvca'
GO
EXEC sp_addextendedproperty N'MS_Description', N'To identify user password  is reset or not -Yes/No  information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_LoginMaster', 'COLUMN', N'isPasswordReset'
GO
EXEC sp_addextendedproperty N'MS_Description', N'To identify which screen should be displayed to user after login  ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_LoginMaster', 'COLUMN', N'nextScreen'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Registering user password information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_LoginMaster', 'COLUMN', N'password'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stores date of created user', 'SCHEMA', N'dbo', 'TABLE', N'tbl_LoginMaster', 'COLUMN', N'registeredDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Registration type information stores here ,
registration type - contractor,supplier,contractor & supplier,consultant,individual, consultant, media
 ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_LoginMaster', 'COLUMN', N'registrationType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Reset password code information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_LoginMaster', 'COLUMN', N'resetPasswordCode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'User type information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_LoginMaster', 'COLUMN', N'status'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Unique id of temporary registration stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_LoginMaster', 'COLUMN', N'userId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'User type information stores here this field contain information like 
officer ,admin ,tenderer,DEO,partner ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_LoginMaster', 'COLUMN', N'userTyperId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Till which date and time login should be valid that information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_LoginMaster', 'COLUMN', N'validUpTo'
GO
ALTER TABLE [dbo].[tbl_LoginMaster] SET (LOCK_ESCALATION = TABLE)
GO
