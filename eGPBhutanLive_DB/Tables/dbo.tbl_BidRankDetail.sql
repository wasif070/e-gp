SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_BidRankDetail] (
		[bidRankDetailId]     [int] IDENTITY(1, 1) NOT NULL,
		[reportColumnId]      [int] NOT NULL,
		[cellId]              [int] NOT NULL,
		[cellValue]           [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[bidderRankId]        [int] NOT NULL,
		CONSTRAINT [PK_tbl_BidRankDetail]
		PRIMARY KEY
		CLUSTERED
		([bidRankDetailId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_BidRankDetail]
	WITH CHECK
	ADD CONSTRAINT [bidderRankId_FK1]
	FOREIGN KEY ([bidderRankId]) REFERENCES [dbo].[tbl_BidderRank] ([bidderRankId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_BidRankDetail]
	CHECK CONSTRAINT [bidderRankId_FK1]

GO
ALTER TABLE [dbo].[tbl_BidRankDetail]
	WITH CHECK
	ADD CONSTRAINT [FK15E7DD991E650AFB]
	FOREIGN KEY ([bidderRankId]) REFERENCES [dbo].[tbl_BidderRank] ([bidderRankId])
ALTER TABLE [dbo].[tbl_BidRankDetail]
	CHECK CONSTRAINT [FK15E7DD991E650AFB]

GO
ALTER TABLE [dbo].[tbl_BidRankDetail] SET (LOCK_ESCALATION = TABLE)
GO
