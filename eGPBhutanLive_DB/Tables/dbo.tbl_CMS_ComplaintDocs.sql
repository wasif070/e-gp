SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_ComplaintDocs] (
		[complaintDocId]       [int] IDENTITY(1, 1) NOT NULL,
		[complaintId]          [int] NOT NULL,
		[uploadedBy]           [int] NOT NULL,
		[userRole]             [tinyint] NULL,
		[docSize]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docName]              [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDt]           [datetime] NOT NULL,
		[complaintLevelId]     [tinyint] NOT NULL,
		[docDescription]       [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_CMS_ComplaintDocs]
		PRIMARY KEY
		CLUSTERED
		([complaintDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintDocs]
	WITH CHECK
	ADD CONSTRAINT [cmsComplaintDocsLevelId_FK3]
	FOREIGN KEY ([complaintLevelId]) REFERENCES [dbo].[tbl_CMS_ComplaintLevels] ([complaintLevelId])
ALTER TABLE [dbo].[tbl_CMS_ComplaintDocs]
	CHECK CONSTRAINT [cmsComplaintDocsLevelId_FK3]

GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintDocs]
	WITH CHECK
	ADD CONSTRAINT [complaintDocComplaintId_FK1]
	FOREIGN KEY ([complaintId]) REFERENCES [dbo].[tbl_CMS_ComplaintMaster] ([complaintId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_ComplaintDocs]
	CHECK CONSTRAINT [complaintDocComplaintId_FK1]

GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintDocs]
	WITH CHECK
	ADD CONSTRAINT [complaintDocUserTypeId_FK1]
	FOREIGN KEY ([userRole]) REFERENCES [dbo].[tbl_UserTypeMaster] ([userTypeId])
ALTER TABLE [dbo].[tbl_CMS_ComplaintDocs]
	CHECK CONSTRAINT [complaintDocUserTypeId_FK1]

GO
EXEC sp_addextendedproperty N'MS_Description', N'System generated id information stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintDocs', 'COLUMN', N'complaintDocId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK tbl_CMS_ComplaintMaster', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintDocs', 'COLUMN', N'complaintId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Complaint Level Id information stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintDocs', 'COLUMN', N'complaintLevelId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Document Name information stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintDocs', 'COLUMN', N'docName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Document Size information stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintDocs', 'COLUMN', N'docSize'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Uploaded By User Id information stoeres here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintDocs', 'COLUMN', N'uploadedBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Uploaded Date & Time info stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintDocs', 'COLUMN', N'uploadedDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'User Role Info stores here(Tenderer/Officer)', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintDocs', 'COLUMN', N'userRole'
GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintDocs] SET (LOCK_ESCALATION = TABLE)
GO
