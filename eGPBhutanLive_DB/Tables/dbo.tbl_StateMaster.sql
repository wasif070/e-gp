SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_StateMaster] (
		[stateId]       [smallint] IDENTITY(1, 1) NOT NULL,
		[stateName]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[countryId]     [smallint] NOT NULL,
		[areaCode]      [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [stateId_PK]
		PRIMARY KEY
		CLUSTERED
		([stateId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_StateMaster]
	ADD
	CONSTRAINT [DF_tbl_StateMaster_countryId]
	DEFAULT ((0)) FOR [countryId]
GO
ALTER TABLE [dbo].[tbl_StateMaster]
	WITH CHECK
	ADD CONSTRAINT [FK47B18332F059B349]
	FOREIGN KEY ([countryId]) REFERENCES [dbo].[tbl_CountryMaster] ([countryId])
ALTER TABLE [dbo].[tbl_StateMaster]
	CHECK CONSTRAINT [FK47B18332F059B349]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Refers table tbl_CountryMaster stores id here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_StateMaster', 'COLUMN', N'countryId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Unique Id of state stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_StateMaster', 'COLUMN', N'stateId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name of state stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_StateMaster', 'COLUMN', N'stateName'
GO
ALTER TABLE [dbo].[tbl_StateMaster] SET (LOCK_ESCALATION = TABLE)
GO
