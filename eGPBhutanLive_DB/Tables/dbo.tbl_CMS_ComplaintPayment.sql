SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_ComplaintPayment] (
		[complaintPaymentId]     [int] IDENTITY(1, 1) NOT NULL,
		[complaintId]            [int] NOT NULL,
		[paymentFor]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[paymentInstType]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[instRefNumber]          [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[amount]                 [money] NOT NULL,
		[instDate]               [smalldatetime] NOT NULL,
		[instValidUpto]          [smalldatetime] NOT NULL,
		[bankName]               [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[branchName]             [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[comments]               [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]              [int] NOT NULL,
		[createdDate]            [smalldatetime] NOT NULL,
		[status]                 [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[paymentMode]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[currency]               [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[issuanceBank]           [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[issuanceBranch]         [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[isVerified]             [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isLive]                 [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[dtOfAction]             [smalldatetime] NULL,
		[partTransId]            [int] NULL,
		[instValidityDt]         [datetime] NULL,
		CONSTRAINT [PK_tbl_CMS_ComplaintPayment]
		PRIMARY KEY
		CLUSTERED
		([complaintPaymentId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintPayment]
	WITH CHECK
	ADD CONSTRAINT [FK_CMS_ComplaintMaster]
	FOREIGN KEY ([complaintId]) REFERENCES [dbo].[tbl_CMS_ComplaintMaster] ([complaintId])
ALTER TABLE [dbo].[tbl_CMS_ComplaintPayment]
	CHECK CONSTRAINT [FK_CMS_ComplaintMaster]

GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintPayment]
	WITH CHECK
	ADD CONSTRAINT [FK865EFD322F838873]
	FOREIGN KEY ([complaintId]) REFERENCES [dbo].[tbl_CMS_ComplaintMaster] ([complaintId])
ALTER TABLE [dbo].[tbl_CMS_ComplaintPayment]
	CHECK CONSTRAINT [FK865EFD322F838873]

GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintPayment] SET (LOCK_ESCALATION = TABLE)
GO
