SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_WS_Log] (
		[wsLogId]            [int] IDENTITY(1, 1) NOT NULL,
		[wsId]               [int] NOT NULL,
		[orgId]              [int] NOT NULL,
		[wsDesc]             [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isAuthenticate]     [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[lgIpMac]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[visitDate]          [datetime] NOT NULL,
		[visitKey]           [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_WS_Log]
		PRIMARY KEY
		CLUSTERED
		([wsLogId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_WS_Log]
	ADD
	CONSTRAINT [DF_tbl_WS_Log_isAuthenticate]
	DEFAULT ('N') FOR [isAuthenticate]
GO
ALTER TABLE [dbo].[tbl_WS_Log]
	WITH CHECK
	ADD CONSTRAINT [FK811C42E21EC93C69]
	FOREIGN KEY ([orgId]) REFERENCES [dbo].[tbl_WS_OrgMaster] ([orgId])
ALTER TABLE [dbo].[tbl_WS_Log]
	CHECK CONSTRAINT [FK811C42E21EC93C69]

GO
ALTER TABLE [dbo].[tbl_WS_Log]
	WITH CHECK
	ADD CONSTRAINT [FK811C42E26C7B5ED5]
	FOREIGN KEY ([wsId]) REFERENCES [dbo].[tbl_WS_Master] ([wsId])
ALTER TABLE [dbo].[tbl_WS_Log]
	CHECK CONSTRAINT [FK811C42E26C7B5ED5]

GO
ALTER TABLE [dbo].[tbl_WS_Log]
	WITH CHECK
	ADD CONSTRAINT [wsLogOrgId_FK1]
	FOREIGN KEY ([orgId]) REFERENCES [dbo].[tbl_WS_OrgMaster] ([orgId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_WS_Log]
	CHECK CONSTRAINT [wsLogOrgId_FK1]

GO
ALTER TABLE [dbo].[tbl_WS_Log]
	WITH CHECK
	ADD CONSTRAINT [wsLogWsId_FK2]
	FOREIGN KEY ([wsId]) REFERENCES [dbo].[tbl_WS_Master] ([wsId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_WS_Log]
	CHECK CONSTRAINT [wsLogWsId_FK2]

GO
ALTER TABLE [dbo].[tbl_WS_Log] SET (LOCK_ESCALATION = TABLE)
GO
