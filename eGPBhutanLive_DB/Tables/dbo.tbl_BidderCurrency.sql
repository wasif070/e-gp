SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_BidderCurrency] (
		[bidCurrencyId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]          [int] NOT NULL,
		[currencyId]        [int] NOT NULL,
		[userId]            [int] NOT NULL,
		[exchangeRate]      [numeric](19, 2) NULL,
		CONSTRAINT [PK_tbl_BidderCurrency]
		PRIMARY KEY
		CLUSTERED
		([bidCurrencyId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_BidderCurrency]
	WITH NOCHECK
	ADD CONSTRAINT [bidderCurrencyId_FK2]
	FOREIGN KEY ([currencyId]) REFERENCES [dbo].[tbl_CurrencyMaster] ([currencyId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_BidderCurrency]
	CHECK CONSTRAINT [bidderCurrencyId_FK2]

GO
ALTER TABLE [dbo].[tbl_BidderCurrency]
	WITH NOCHECK
	ADD CONSTRAINT [BidderTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_BidderCurrency]
	CHECK CONSTRAINT [BidderTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_BidderCurrency]
	WITH NOCHECK
	ADD CONSTRAINT [FK3C56F0E64FDC0C3F]
	FOREIGN KEY ([currencyId]) REFERENCES [dbo].[tbl_CurrencyMaster] ([currencyId])
ALTER TABLE [dbo].[tbl_BidderCurrency]
	CHECK CONSTRAINT [FK3C56F0E64FDC0C3F]

GO
ALTER TABLE [dbo].[tbl_BidderCurrency] SET (LOCK_ESCALATION = TABLE)
GO
