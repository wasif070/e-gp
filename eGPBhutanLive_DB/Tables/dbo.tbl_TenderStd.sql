SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderStd] (
		[tenderStdId]      [int] IDENTITY(1, 1) NOT NULL,
		[templateId]       [smallint] NOT NULL,
		[templateName]     [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[noOfSections]     [tinyint] NOT NULL,
		[status]           [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderId]         [int] NOT NULL,
		[packageLotId]     [int] NOT NULL,
		[createdBy]        [int] NOT NULL,
		[procTYpe]         [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_TenderStd]
		PRIMARY KEY
		CLUSTERED
		([tenderStdId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderStd]
	WITH CHECK
	ADD CONSTRAINT [FK8ABE374E904D9C11]
	FOREIGN KEY ([templateId]) REFERENCES [dbo].[tbl_TemplateMaster] ([templateId])
ALTER TABLE [dbo].[tbl_TenderStd]
	CHECK CONSTRAINT [FK8ABE374E904D9C11]

GO
ALTER TABLE [dbo].[tbl_TenderStd]
	WITH CHECK
	ADD CONSTRAINT [tenderTemplate_FK1]
	FOREIGN KEY ([templateId]) REFERENCES [dbo].[tbl_TemplateMaster] ([templateId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderStd]
	CHECK CONSTRAINT [tenderTemplate_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderStd] SET (LOCK_ESCALATION = TABLE)
GO
