SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderAuditTrail] (
		[tenderAuditTrailId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]               [int] NOT NULL,
		[action]                 [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[actionBy]               [int] NOT NULL,
		[actionDate]             [smalldatetime] NOT NULL,
		[eSignature]             [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[digitalSignature]       [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[activity]               [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[remarks]                [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [tenderAuditTrailId_PK]
		PRIMARY KEY
		CLUSTERED
		([tenderAuditTrailId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderAuditTrail]
	WITH NOCHECK
	ADD CONSTRAINT [auditTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderAuditTrail]
	CHECK CONSTRAINT [auditTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderAuditTrail]
	WITH NOCHECK
	ADD CONSTRAINT [FK6F11876098B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_TenderAuditTrail]
	CHECK CONSTRAINT [FK6F11876098B66EC5]

GO
ALTER TABLE [dbo].[tbl_TenderAuditTrail] SET (LOCK_ESCALATION = TABLE)
GO
