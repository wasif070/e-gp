SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderBidEncrypt] (
		[bidEncryptId]       [int] IDENTITY(1, 1) NOT NULL,
		[bidTableId]         [int] NOT NULL,
		[tenderColumnId]     [int] NOT NULL,
		[tenderTableId]      [int] NOT NULL,
		[cellValue]          [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[rowId]              [int] NOT NULL,
		[cellId]             [int] NOT NULL,
		[formId]             [int] NOT NULL,
		CONSTRAINT [PK_tbl_TenderBidEncrypt]
		PRIMARY KEY
		CLUSTERED
		([bidEncryptId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderBidEncrypt]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_TenderBidEncrypt_tbl_TenderBidTable]
	FOREIGN KEY ([bidTableId]) REFERENCES [dbo].[tbl_TenderBidTable] ([bidTableId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderBidEncrypt]
	CHECK CONSTRAINT [FK_tbl_TenderBidEncrypt_tbl_TenderBidTable]

GO
ALTER TABLE [dbo].[tbl_TenderBidEncrypt] SET (LOCK_ESCALATION = TABLE)
GO
