SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_CMS_InvoiceDetails] (
		[invoiceDtlId]     [int] IDENTITY(1, 1) NOT NULL,
		[invoiceId]        [int] NOT NULL,
		[wpDetailId]       [int] NOT NULL,
		[itemInvQty]       [decimal](15, 3) NOT NULL,
		[itemInvAmt]       [decimal](15, 3) NOT NULL,
		CONSTRAINT [PK_tbl_CMS_Invoice]
		PRIMARY KEY
		CLUSTERED
		([invoiceDtlId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_InvoiceDetails]
	WITH CHECK
	ADD CONSTRAINT [FKInvoiceId_InvoiceMaster]
	FOREIGN KEY ([invoiceId]) REFERENCES [dbo].[tbl_CMS_InvoiceMaster] ([invoiceId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_InvoiceDetails]
	CHECK CONSTRAINT [FKInvoiceId_InvoiceMaster]

GO
ALTER TABLE [dbo].[tbl_CMS_InvoiceDetails] SET (LOCK_ESCALATION = TABLE)
GO
