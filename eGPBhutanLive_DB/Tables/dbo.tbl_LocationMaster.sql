SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_LocationMaster] (
		[locationId]       [smallint] NOT NULL,
		[locationName]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_LocationMaster]
		PRIMARY KEY
		CLUSTERED
		([locationId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_LocationMaster] SET (LOCK_ESCALATION = TABLE)
GO
