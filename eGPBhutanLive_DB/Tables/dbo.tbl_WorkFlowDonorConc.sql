SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_WorkFlowDonorConc] (
		[wfDonorId]       [int] IDENTITY(1, 1) NOT NULL,
		[objectId]        [int] NOT NULL,
		[moduleId]        [tinyint] NOT NULL,
		[isDonConReq]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [wfDonorId_PK]
		PRIMARY KEY
		CLUSTERED
		([wfDonorId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_WorkFlowDonorConc]
	WITH NOCHECK
	ADD CONSTRAINT [donorConcModule_FK1]
	FOREIGN KEY ([moduleId]) REFERENCES [dbo].[tbl_ModuleMaster] ([moduleId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_WorkFlowDonorConc]
	CHECK CONSTRAINT [donorConcModule_FK1]

GO
ALTER TABLE [dbo].[tbl_WorkFlowDonorConc]
	WITH NOCHECK
	ADD CONSTRAINT [FK3A4A4327D2A63DF5]
	FOREIGN KEY ([moduleId]) REFERENCES [dbo].[tbl_ModuleMaster] ([moduleId])
ALTER TABLE [dbo].[tbl_WorkFlowDonorConc]
	CHECK CONSTRAINT [FK3A4A4327D2A63DF5]

GO
ALTER TABLE [dbo].[tbl_WorkFlowDonorConc] SET (LOCK_ESCALATION = TABLE)
GO
