SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EmailVerificationCode] (
		[emailVerificationId]     [int] IDENTITY(1, 1) NOT NULL,
		[userId]                  [int] NOT NULL,
		[randCode]                [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[generatedDt]             [smalldatetime] NOT NULL,
		CONSTRAINT [PK_tbl_EmailVerificationCode]
		PRIMARY KEY
		CLUSTERED
		([emailVerificationId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EmailVerificationCode] SET (LOCK_ESCALATION = TABLE)
GO
