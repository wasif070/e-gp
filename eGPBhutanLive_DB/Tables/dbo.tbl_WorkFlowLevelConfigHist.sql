SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_WorkFlowLevelConfigHist] (
		[workflowLevelHistId]     [int] IDENTITY(1, 1) NOT NULL,
		[eventId]                 [smallint] NOT NULL,
		[moduleId]                [tinyint] NOT NULL,
		[wfLevel]                 [smallint] NOT NULL,
		[userId]                  [int] NOT NULL,
		[action]                  [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[wfRoleId]                [tinyint] NOT NULL,
		[objectId]                [int] NOT NULL,
		[childId]                 [int] NOT NULL,
		[actionDate]              [smalldatetime] NOT NULL,
		[actionBy]                [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[fileOnHand]              [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[workflowId]              [int] NOT NULL,
		[procurementRoleId]       [tinyint] NOT NULL,
		CONSTRAINT [workflowId_PK]
		PRIMARY KEY
		CLUSTERED
		([workflowLevelHistId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_WorkFlowLevelConfigHist] SET (LOCK_ESCALATION = TABLE)
GO
