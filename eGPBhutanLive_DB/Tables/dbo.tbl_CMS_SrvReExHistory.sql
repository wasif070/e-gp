SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_SrvReExHistory] (
		[srvREHistId]      [int] IDENTITY(1, 1) NOT NULL,
		[srvREId]          [int] NOT NULL,
		[srvFormMapId]     [int] NOT NULL,
		[srNo]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[catagoryDesc]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[description]      [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[unit]             [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[qty]              [decimal](15, 3) NOT NULL,
		[unitCost]         [decimal](15, 3) NOT NULL,
		[totalAmt]         [decimal](15, 3) NOT NULL,
		[histCnt]          [int] NOT NULL,
		[createdDate]      [datetime] NULL,
		CONSTRAINT [PK_tbl_CMS_SrvReExHistory]
		PRIMARY KEY
		CLUSTERED
		([srvREHistId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvReExHistory] SET (LOCK_ESCALATION = TABLE)
GO
