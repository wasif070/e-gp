SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderLotSecurity] (
		[tenderLotSecId]           [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]                 [int] NOT NULL,
		[appPkgLotId]              [int] NOT NULL,
		[lotNo]                    [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[lotDesc]                  [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[location]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docFess]                  [money] NOT NULL,
		[tenderSecurityAmt]        [money] NOT NULL,
		[completionTime]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[startTime]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[tenderSecurityAmtUSD]     [money] NULL,
		[BidSecurityType]          [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [tenderLotSecId_PK]
		PRIMARY KEY
		CLUSTERED
		([tenderLotSecId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderLotSecurity]
	WITH CHECK
	ADD CONSTRAINT [FK7674BBDC98B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_TenderLotSecurity]
	CHECK CONSTRAINT [FK7674BBDC98B66EC5]

GO
ALTER TABLE [dbo].[tbl_TenderLotSecurity]
	WITH CHECK
	ADD CONSTRAINT [lsTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderLotSecurity]
	CHECK CONSTRAINT [lsTenderId_FK1]

GO
EXEC sp_addextendedproperty N'MS_Description', N'1=Finacial Institution Payment, 2=Bid Security Decalaration, 3=Anyone', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TenderLotSecurity', 'COLUMN', N'BidSecurityType'
GO
ALTER TABLE [dbo].[tbl_TenderLotSecurity] SET (LOCK_ESCALATION = TABLE)
GO
