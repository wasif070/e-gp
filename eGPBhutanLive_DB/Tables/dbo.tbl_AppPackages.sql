SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_AppPackages] (
		[packageId]               [int] IDENTITY(1, 1) NOT NULL,
		[appId]                   [int] NOT NULL,
		[procurementnature]       [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[servicesType]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[packageNo]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[packageDesc]             [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[allocateBudget]          [money] NOT NULL,
		[estimatedCost]           [money] NOT NULL,
		[cpvCode]                 [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[pkgEstCost]              [money] NOT NULL,
		[approvingAuthEmpId]      [int] NOT NULL,
		[isPQRequired]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[reoiRfaRequired]         [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[procurementMethodId]     [tinyint] NOT NULL,
		[procurementType]         [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sourceOfFund]            [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[appStatus]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[workflowStatus]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[noOfStages]              [tinyint] NOT NULL,
		[pkgUrgency]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[pkgEstCode]              [numeric](19, 2) NULL,
		[bidderCategory]          [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[workCategory]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[depoplanWork]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[entrustingAgency]        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[timeFrame]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [packageId_PK]
		PRIMARY KEY
		CLUSTERED
		([packageId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_AppPackages]
	WITH CHECK
	ADD CONSTRAINT [FK65459B0D448F6D5F]
	FOREIGN KEY ([appId]) REFERENCES [dbo].[tbl_AppMaster] ([appId])
ALTER TABLE [dbo].[tbl_AppPackages]
	CHECK CONSTRAINT [FK65459B0D448F6D5F]

GO
ALTER TABLE [dbo].[tbl_AppPackages]
	WITH CHECK
	ADD CONSTRAINT [FK65459B0DA5C4169D]
	FOREIGN KEY ([procurementMethodId]) REFERENCES [dbo].[tbl_ProcurementMethod] ([procurementMethodId])
ALTER TABLE [dbo].[tbl_AppPackages]
	CHECK CONSTRAINT [FK65459B0DA5C4169D]

GO
ALTER TABLE [dbo].[tbl_AppPackages]
	WITH CHECK
	ADD CONSTRAINT [pkgAppId_FK1]
	FOREIGN KEY ([appId]) REFERENCES [dbo].[tbl_AppMaster] ([appId])
	ON DELETE CASCADE
ALTER TABLE [dbo].[tbl_AppPackages]
	CHECK CONSTRAINT [pkgAppId_FK1]

GO
ALTER TABLE [dbo].[tbl_AppPackages]
	WITH CHECK
	ADD CONSTRAINT [pkgProcurementMethodId_Fk3]
	FOREIGN KEY ([procurementMethodId]) REFERENCES [dbo].[tbl_ProcurementMethod] ([procurementMethodId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_AppPackages]
	CHECK CONSTRAINT [pkgProcurementMethodId_Fk3]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Budget information stores here 
', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPackages', 'COLUMN', N'allocateBudget'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Refer appId from tbl_AppMaster, to identify the appID', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPackages', 'COLUMN', N'appId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Referencr fron tbl_EmployeeMaster, to identify the employeeId', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPackages', 'COLUMN', N'approvingAuthEmpId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Status information stores here ,either pending/approved
', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPackages', 'COLUMN', N'appStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Cpv code information stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPackages', 'COLUMN', N'cpvCode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Estimatedcost information stores here 
', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPackages', 'COLUMN', N'estimatedCost'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Is required or not means yes or no information stores here 
', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPackages', 'COLUMN', N'isPQRequired'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number Of stages  of App  ,iinformation  stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPackages', 'COLUMN', N'noOfStages'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Package description information stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPackages', 'COLUMN', N'packageDesc'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Its system generated Id information stores here
', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPackages', 'COLUMN', N'packageId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Package number information stores here  
', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPackages', 'COLUMN', N'packageNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PackageEstimation code information  stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPackages', 'COLUMN', N'pkgEstCost'
GO
EXEC sp_addextendedproperty N'MS_Description', N'App package urgency details information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPackages', 'COLUMN', N'pkgUrgency'
GO
EXEC sp_addextendedproperty N'MS_Description', N'reference from tbl_ProcurementMethod to identify the procurementMethodId', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPackages', 'COLUMN', N'procurementMethodId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Procurementnature information stores here nature like goods,services,woks', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPackages', 'COLUMN', N'procurementnature'
GO
EXEC sp_addextendedproperty N'MS_Description', N'ProcurementType information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPackages', 'COLUMN', N'procurementType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'ReoiRfaRequired is yes or no information stores here 
', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPackages', 'COLUMN', N'reoiRfaRequired'
GO
EXEC sp_addextendedproperty N'MS_Description', N'stand alone  services, professional, and intellectual services, this field should be displayed only if user has selected  services  in procurement nature', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPackages', 'COLUMN', N'servicesType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Fund information stores here like Govt fund or AIDGrant
', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPackages', 'COLUMN', N'sourceOfFund'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Workflowstatus information stores here  either pending /approved', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPackages', 'COLUMN', N'workflowStatus'
GO
ALTER TABLE [dbo].[tbl_AppPackages] SET (LOCK_ESCALATION = TABLE)
GO
