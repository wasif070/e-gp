SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalRptDocs] (
		[evalRptDocId]       [int] IDENTITY(1, 1) NOT NULL,
		[evalRptId]          [int] NOT NULL,
		[documentName]       [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]       [smalldatetime] NOT NULL,
		[uploadedBy]         [int] NOT NULL,
		[roundId]            [int] NOT NULL,
		CONSTRAINT [PK_tbl_EvalRptDocs]
		PRIMARY KEY
		CLUSTERED
		([evalRptDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalRptDocs]
	ADD
	CONSTRAINT [DF_tbl_EvalRptDocs_roundId]
	DEFAULT ((0)) FOR [roundId]
GO
ALTER TABLE [dbo].[tbl_EvalRptDocs]
	WITH CHECK
	ADD CONSTRAINT [evalRptId_FK1]
	FOREIGN KEY ([evalRptId]) REFERENCES [dbo].[tbl_EvalReportMaster] ([evalRptId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EvalRptDocs]
	CHECK CONSTRAINT [evalRptId_FK1]

GO
ALTER TABLE [dbo].[tbl_EvalRptDocs] SET (LOCK_ESCALATION = TABLE)
GO
