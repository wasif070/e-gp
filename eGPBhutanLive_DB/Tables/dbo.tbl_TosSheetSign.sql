SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TosSheetSign] (
		[tosId]           [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]        [int] NOT NULL,
		[userId]          [int] NOT NULL,
		[comments]        [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[signedDate]      [smalldatetime] NOT NULL,
		[committeeId]     [int] NOT NULL,
		[tosStatus]       [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_TosSheetSign]
		PRIMARY KEY
		CLUSTERED
		([tosId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TosSheetSign]
	ADD
	CONSTRAINT [DF_tbl_TosSheetSign_tosStatus]
	DEFAULT ('Approved') FOR [tosStatus]
GO
ALTER TABLE [dbo].[tbl_TosSheetSign]
	WITH NOCHECK
	ADD CONSTRAINT [sheetSignTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TosSheetSign]
	CHECK CONSTRAINT [sheetSignTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_TosSheetSign] SET (LOCK_ESCALATION = TABLE)
GO
