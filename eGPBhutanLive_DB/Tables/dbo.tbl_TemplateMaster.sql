SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TemplateMaster] (
		[templateId]          [smallint] IDENTITY(1, 1) NOT NULL,
		[templateName]        [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[noOfSections]        [tinyint] NOT NULL,
		[status]              [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[procType]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[stdFor]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[devPartnerId]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[foreignCurrency]     [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [templateId_PK]
		PRIMARY KEY
		CLUSTERED
		([templateId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TemplateMaster]
	ADD
	CONSTRAINT [DF_tbl_TemplateMaster_foreignCurrency]
	DEFAULT ('No') FOR [foreignCurrency]
GO
ALTER TABLE [dbo].[tbl_TemplateMaster] SET (LOCK_ESCALATION = TABLE)
GO
