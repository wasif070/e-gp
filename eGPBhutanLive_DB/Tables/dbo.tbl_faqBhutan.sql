SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_faqBhutan] (
		[faqID]        [int] IDENTITY(1, 1) NOT NULL,
		[question]     [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[answer]       [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_faqBhutan]
		PRIMARY KEY
		CLUSTERED
		([faqID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_faqBhutan] SET (LOCK_ESCALATION = TABLE)
GO
