SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_MessageDraft] (
		[msgDraftId]        [int] IDENTITY(1, 1) NOT NULL,
		[msgId]             [int] NOT NULL,
		[msgToUserId]       [int] NOT NULL,
		[msgFromUserId]     [int] NOT NULL,
		[msgToCCReply]      [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[msgStatus]         [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[creationDate]      [datetime] NOT NULL,
		[folderId]          [int] NOT NULL,
		[isPriority]        [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isMsgRead]         [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isDocUploaded]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [msgDraftId_PK]
		PRIMARY KEY
		CLUSTERED
		([msgDraftId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_MessageDraft]
	ADD
	CONSTRAINT [DF_tbl_MessageDraft_folderId]
	DEFAULT ((0)) FOR [folderId]
GO
ALTER TABLE [dbo].[tbl_MessageDraft]
	ADD
	CONSTRAINT [DF_tbl_MessageDraft_msgFromUserId]
	DEFAULT ((0)) FOR [msgFromUserId]
GO
ALTER TABLE [dbo].[tbl_MessageDraft]
	ADD
	CONSTRAINT [DF_tbl_MessageDraft_msgStatus]
	DEFAULT ('Live') FOR [msgStatus]
GO
ALTER TABLE [dbo].[tbl_MessageDraft]
	WITH NOCHECK
	ADD CONSTRAINT [draftMsgId_FK2]
	FOREIGN KEY ([msgId]) REFERENCES [dbo].[tbl_Message] ([msgId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_MessageDraft]
	CHECK CONSTRAINT [draftMsgId_FK2]

GO
ALTER TABLE [dbo].[tbl_MessageDraft]
	WITH CHECK
	ADD CONSTRAINT [FK23ED78DB3876063C]
	FOREIGN KEY ([msgFromUserId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_MessageDraft]
	CHECK CONSTRAINT [FK23ED78DB3876063C]

GO
ALTER TABLE [dbo].[tbl_MessageDraft]
	WITH NOCHECK
	ADD CONSTRAINT [FK23ED78DBA922AEE3]
	FOREIGN KEY ([msgId]) REFERENCES [dbo].[tbl_Message] ([msgId])
ALTER TABLE [dbo].[tbl_MessageDraft]
	CHECK CONSTRAINT [FK23ED78DBA922AEE3]

GO
ALTER TABLE [dbo].[tbl_MessageDraft]
	WITH CHECK
	ADD CONSTRAINT [msgDraftFromUserId_FK1]
	FOREIGN KEY ([msgFromUserId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_MessageDraft]
	CHECK CONSTRAINT [msgDraftFromUserId_FK1]

GO
ALTER TABLE [dbo].[tbl_MessageDraft] SET (LOCK_ESCALATION = TABLE)
GO
