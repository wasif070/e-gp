SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalRptApprove] (
		[evalRptAppId]          [int] IDENTITY(1, 1) NOT NULL,
		[evalRptToAAId]         [int] NOT NULL,
		[actionByUserId]        [int] NOT NULL,
		[tenderId]              [int] NOT NULL,
		[action]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[remarks]               [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[actionDate]            [smalldatetime] NOT NULL,
		[actionByGovUserId]     [int] NOT NULL,
		[isCurrent]             [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_EvalRptApprove]
		PRIMARY KEY
		CLUSTERED
		([evalRptAppId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalRptApprove]
	ADD
	CONSTRAINT [DF_tbl_EvalRptApprove_actionByGovUserId]
	DEFAULT ((0)) FOR [actionByGovUserId]
GO
ALTER TABLE [dbo].[tbl_EvalRptApprove]
	ADD
	CONSTRAINT [DF_tbl_EvalRptApprove_isCurrent]
	DEFAULT ('No') FOR [isCurrent]
GO
ALTER TABLE [dbo].[tbl_EvalRptApprove]
	WITH NOCHECK
	ADD CONSTRAINT [EvalRptSentToAA_FK1]
	FOREIGN KEY ([evalRptToAAId]) REFERENCES [dbo].[tbl_EvalRptSentToAA] ([evalRptToAAId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EvalRptApprove]
	CHECK CONSTRAINT [EvalRptSentToAA_FK1]

GO
ALTER TABLE [dbo].[tbl_EvalRptApprove]
	WITH NOCHECK
	ADD CONSTRAINT [FK6481A0F4CAA50A9D]
	FOREIGN KEY ([evalRptToAAId]) REFERENCES [dbo].[tbl_EvalRptSentToAA] ([evalRptToAAId])
ALTER TABLE [dbo].[tbl_EvalRptApprove]
	CHECK CONSTRAINT [FK6481A0F4CAA50A9D]

GO
ALTER TABLE [dbo].[tbl_EvalRptApprove] SET (LOCK_ESCALATION = TABLE)
GO
