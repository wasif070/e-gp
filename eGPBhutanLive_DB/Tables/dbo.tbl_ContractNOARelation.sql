SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_ContractNOARelation] (
		[ContractNOAId]     [bigint] IDENTITY(1, 1) NOT NULL,
		[ContractID]        [int] NOT NULL,
		[NOAID]             [int] NOT NULL,
		[UserID]            [int] NOT NULL,
		[TenderID]          [int] NOT NULL,
		CONSTRAINT [PK_ContractNOARelation]
		PRIMARY KEY
		CLUSTERED
		([ContractNOAId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ContractNOARelation] SET (LOCK_ESCALATION = TABLE)
GO
