SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderIttClause] (
		[tenderIttClauseId]        [int] IDENTITY(1, 1) NOT NULL,
		[tendederIttHeaderId]      [int] NOT NULL,
		[ittClauseName]            [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tempplateIttClauseId]     [int] NOT NULL,
		CONSTRAINT [PK_tbl_TenderIttClause]
		PRIMARY KEY
		CLUSTERED
		([tenderIttClauseId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderIttClause]
	WITH CHECK
	ADD CONSTRAINT [FK954ADC634461CAE0]
	FOREIGN KEY ([tendederIttHeaderId]) REFERENCES [dbo].[tbl_TenderIttHeader] ([tenderIttHeaderId])
ALTER TABLE [dbo].[tbl_TenderIttClause]
	CHECK CONSTRAINT [FK954ADC634461CAE0]

GO
ALTER TABLE [dbo].[tbl_TenderIttClause]
	WITH CHECK
	ADD CONSTRAINT [tenderIttHeaderId_FK1]
	FOREIGN KEY ([tendederIttHeaderId]) REFERENCES [dbo].[tbl_TenderIttHeader] ([tenderIttHeaderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderIttClause]
	CHECK CONSTRAINT [tenderIttHeaderId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderIttClause] SET (LOCK_ESCALATION = TABLE)
GO
