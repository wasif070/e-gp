SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Negotiation] (
		[negId]                       [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]                    [int] NOT NULL,
		[negStartDt]                  [datetime] NOT NULL,
		[negEndDt]                    [datetime] NOT NULL,
		[negStatus]                   [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[negCreatedDt]                [datetime] NOT NULL,
		[negCreatedBy]                [int] NOT NULL,
		[negLocDetails]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[negRemarks]                  [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[negMode]                     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[negFinalSub]                 [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[negFinalSubRemarks]          [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[negOfficeAggree]             [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[negOfficerAggreeRemarks]     [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[negOfficerAggreeDt]          [smalldatetime] NULL,
		[roundId]                     [int] NOT NULL,
		[evalCount]                   [int] NOT NULL,
		CONSTRAINT [negId_PK]
		PRIMARY KEY
		CLUSTERED
		([negId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_Negotiation]
	ADD
	CONSTRAINT [DF__tbl_Negot__evalC__48DABF76]
	DEFAULT ('0') FOR [evalCount]
GO
ALTER TABLE [dbo].[tbl_Negotiation]
	ADD
	CONSTRAINT [DF_tbl_Negotiation_negFinalSub]
	DEFAULT ('No') FOR [negFinalSub]
GO
ALTER TABLE [dbo].[tbl_Negotiation]
	ADD
	CONSTRAINT [DF_tbl_Negotiation_roundId]
	DEFAULT ((0)) FOR [roundId]
GO
ALTER TABLE [dbo].[tbl_Negotiation]
	WITH NOCHECK
	ADD CONSTRAINT [FKCA6694C098B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_Negotiation]
	CHECK CONSTRAINT [FKCA6694C098B66EC5]

GO
ALTER TABLE [dbo].[tbl_Negotiation]
	WITH NOCHECK
	ADD CONSTRAINT [negTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_Negotiation]
	CHECK CONSTRAINT [negTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_Negotiation] SET (LOCK_ESCALATION = TABLE)
GO
