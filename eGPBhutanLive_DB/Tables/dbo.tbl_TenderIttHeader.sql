SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderIttHeader] (
		[tenderIttHeaderId]       [int] IDENTITY(1, 1) NOT NULL,
		[templateIttHeaderId]     [int] NOT NULL,
		[ittHeaderName]           [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderSectionId]         [int] NOT NULL,
		[temlateSectionId]        [int] NOT NULL,
		[tenderId]                [int] NOT NULL,
		CONSTRAINT [PK_tbl_TenderIttHeader]
		PRIMARY KEY
		CLUSTERED
		([tenderIttHeaderId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderIttHeader]
	WITH CHECK
	ADD CONSTRAINT [FK9D703181DB57665D]
	FOREIGN KEY ([tenderSectionId]) REFERENCES [dbo].[tbl_TenderSection] ([tenderSectionId])
ALTER TABLE [dbo].[tbl_TenderIttHeader]
	CHECK CONSTRAINT [FK9D703181DB57665D]

GO
ALTER TABLE [dbo].[tbl_TenderIttHeader]
	WITH CHECK
	ADD CONSTRAINT [tenderIttSectionId_FK1]
	FOREIGN KEY ([tenderSectionId]) REFERENCES [dbo].[tbl_TenderSection] ([tenderSectionId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderIttHeader]
	CHECK CONSTRAINT [tenderIttSectionId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderIttHeader] SET (LOCK_ESCALATION = TABLE)
GO
