SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_DebarmentResp] (
		[debarRespId]     [int] IDENTITY(1, 1) NOT NULL,
		[debarmentId]     [int] NOT NULL,
		[responseTxt]     [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[responseDt]      [smalldatetime] NOT NULL,
		[userId]          [int] NOT NULL,
		CONSTRAINT [PK_tbl_DebarmentResp]
		PRIMARY KEY
		CLUSTERED
		([debarRespId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_DebarmentResp]
	WITH CHECK
	ADD CONSTRAINT [debarment_FK1]
	FOREIGN KEY ([debarmentId]) REFERENCES [dbo].[tbl_DebarmentReq] ([debarmentId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_DebarmentResp]
	CHECK CONSTRAINT [debarment_FK1]

GO
ALTER TABLE [dbo].[tbl_DebarmentResp] SET (LOCK_ESCALATION = TABLE)
GO
