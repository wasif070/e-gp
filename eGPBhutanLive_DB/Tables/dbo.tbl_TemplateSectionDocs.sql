SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TemplateSectionDocs] (
		[sectionDocId]     [int] IDENTITY(1, 1) NOT NULL,
		[description]      [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docName]          [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sectionId]        [smallint] NOT NULL,
		[status]           [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[templateId]       [smallint] NOT NULL,
		CONSTRAINT [UQ__tbl_Temp__9DD13B27C7B0503C]
		UNIQUE
		NONCLUSTERED
		([sectionDocId])
		ON [PRIMARY],
		CONSTRAINT [PK__tbl_Temp__9DD13B268BBB132D]
		PRIMARY KEY
		CLUSTERED
		([sectionDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TemplateSectionDocs] SET (LOCK_ESCALATION = TABLE)
GO
