SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ProjectFinPower] (
		[projFinPowerId]          [int] IDENTITY(1, 1) NOT NULL,
		[projectId]               [int] NOT NULL,
		[userId]                  [int] NOT NULL,
		[procurementMethodId]     [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[amount]                  [money] NOT NULL,
		[operation]               [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_ProjectFinPower]
		PRIMARY KEY
		CLUSTERED
		([projFinPowerId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ProjectFinPower]
	WITH NOCHECK
	ADD CONSTRAINT [finProj_FK1]
	FOREIGN KEY ([projectId]) REFERENCES [dbo].[tbl_ProjectMaster] ([projectId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ProjectFinPower]
	CHECK CONSTRAINT [finProj_FK1]

GO
ALTER TABLE [dbo].[tbl_ProjectFinPower]
	WITH NOCHECK
	ADD CONSTRAINT [finUserId_FK2]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ProjectFinPower]
	CHECK CONSTRAINT [finUserId_FK2]

GO
ALTER TABLE [dbo].[tbl_ProjectFinPower]
	WITH NOCHECK
	ADD CONSTRAINT [FKB17FF3526174DBD1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_ProjectFinPower]
	CHECK CONSTRAINT [FKB17FF3526174DBD1]

GO
ALTER TABLE [dbo].[tbl_ProjectFinPower]
	WITH NOCHECK
	ADD CONSTRAINT [FKB17FF35280EAD74F]
	FOREIGN KEY ([projectId]) REFERENCES [dbo].[tbl_ProjectMaster] ([projectId])
ALTER TABLE [dbo].[tbl_ProjectFinPower]
	CHECK CONSTRAINT [FKB17FF35280EAD74F]

GO
ALTER TABLE [dbo].[tbl_ProjectFinPower] SET (LOCK_ESCALATION = TABLE)
GO
