SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_WorkFlowLevelConfig] (
		[worfklowId]            [int] IDENTITY(1, 1) NOT NULL,
		[eventId]               [smallint] NOT NULL,
		[moduleId]              [tinyint] NOT NULL,
		[wfLevel]               [smallint] NOT NULL,
		[userId]                [int] NOT NULL,
		[wfRoleId]              [tinyint] NOT NULL,
		[objectId]              [int] NOT NULL,
		[actionDate]            [smalldatetime] NULL,
		[activityId]            [smallint] NOT NULL,
		[childId]               [int] NOT NULL,
		[fileOnHand]            [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[procurementRoleId]     [tinyint] NOT NULL,
		[govUserId]             [int] NOT NULL,
		CONSTRAINT [worfklowId_PK]
		PRIMARY KEY
		CLUSTERED
		([worfklowId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_WorkFlowLevelConfig]
	ADD
	CONSTRAINT [DF_tbl_WorkFlowLevelConfig_fileOnHand]
	DEFAULT ('No') FOR [fileOnHand]
GO
ALTER TABLE [dbo].[tbl_WorkFlowLevelConfig]
	ADD
	CONSTRAINT [DF_tbl_WorkFlowLevelConfig_govUserId]
	DEFAULT ((0)) FOR [govUserId]
GO
ALTER TABLE [dbo].[tbl_WorkFlowLevelConfig]
	WITH CHECK
	ADD CONSTRAINT [activityLevelConfig_FK1]
	FOREIGN KEY ([activityId]) REFERENCES [dbo].[tbl_ActivityMaster] ([activityId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_WorkFlowLevelConfig]
	CHECK CONSTRAINT [activityLevelConfig_FK1]

GO
ALTER TABLE [dbo].[tbl_WorkFlowLevelConfig]
	WITH CHECK
	ADD CONSTRAINT [activityLevelUser_FK2]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_WorkFlowLevelConfig]
	CHECK CONSTRAINT [activityLevelUser_FK2]

GO
ALTER TABLE [dbo].[tbl_WorkFlowLevelConfig]
	WITH CHECK
	ADD CONSTRAINT [FK42FFAC666174DBD1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_WorkFlowLevelConfig]
	CHECK CONSTRAINT [FK42FFAC666174DBD1]

GO
ALTER TABLE [dbo].[tbl_WorkFlowLevelConfig]
	WITH CHECK
	ADD CONSTRAINT [FK42FFAC668ECC943B]
	FOREIGN KEY ([activityId]) REFERENCES [dbo].[tbl_ActivityMaster] ([activityId])
ALTER TABLE [dbo].[tbl_WorkFlowLevelConfig]
	CHECK CONSTRAINT [FK42FFAC668ECC943B]

GO
ALTER TABLE [dbo].[tbl_WorkFlowLevelConfig] SET (LOCK_ESCALATION = TABLE)
GO
