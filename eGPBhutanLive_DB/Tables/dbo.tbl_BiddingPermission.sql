SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_BiddingPermission] (
		[BiddingPermissionId]     [bigint] IDENTITY(1, 1) NOT NULL,
		[ProcurementCategory]     [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[WorkType]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[WorkCategroy]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CompanyID]               [int] NOT NULL,
		[UserId]                  [int] NOT NULL,
		[IsReapplied]             [int] NULL,
		CONSTRAINT [PK_tbl_BiddingPermission]
		PRIMARY KEY
		CLUSTERED
		([BiddingPermissionId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'if Individual Consultant selected Then company ID 1 , Otherwise bidder Company ID', 'SCHEMA', N'dbo', 'TABLE', N'tbl_BiddingPermission', 'COLUMN', N'CompanyID'
GO
EXEC sp_addextendedproperty N'MS_TempDescription', N'if Individual Consultant selected Then company ID 1 , Otherwise bidder Company ID', 'SCHEMA', N'dbo', 'TABLE', N'tbl_BiddingPermission', 'COLUMN', N'CompanyID'
GO
ALTER TABLE [dbo].[tbl_BiddingPermission] SET (LOCK_ESCALATION = TABLE)
GO
