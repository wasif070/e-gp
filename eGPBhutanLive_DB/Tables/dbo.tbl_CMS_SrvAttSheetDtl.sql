SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_SrvAttSheetDtl] (
		[attSheetDtlId]     [int] IDENTITY(1, 1) NOT NULL,
		[attSheetId]        [int] NOT NULL,
		[srvTCId]           [int] NOT NULL,
		[workFrom]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[noOfDays]          [int] NOT NULL,
		[rate]              [decimal](15, 3) NOT NULL,
		[remarksByPe]       [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_CMS_SrvAttSheetDtl]
		PRIMARY KEY
		CLUSTERED
		([attSheetDtlId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvAttSheetDtl]
	WITH CHECK
	ADD CONSTRAINT [FK_attSheetId_AttSheetDtl_PK_attSheetId_AttSheetMaster]
	FOREIGN KEY ([attSheetId]) REFERENCES [dbo].[tbl_CMS_SrvAttSheetMaster] ([attSheetId])
ALTER TABLE [dbo].[tbl_CMS_SrvAttSheetDtl]
	CHECK CONSTRAINT [FK_attSheetId_AttSheetDtl_PK_attSheetId_AttSheetMaster]

GO
ALTER TABLE [dbo].[tbl_CMS_SrvAttSheetDtl] SET (LOCK_ESCALATION = TABLE)
GO
