SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_CTReasonType] (
		[ctReasonTypeId]     [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ctReason]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_CMS_CTReasonType]
		PRIMARY KEY
		CLUSTERED
		([ctReasonTypeId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_CTReasonType] SET (LOCK_ESCALATION = TABLE)
GO
