SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_PreTenderQuery] (
		[queryId]         [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]        [int] NOT NULL,
		[queryText]       [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[userId]          [int] NOT NULL,
		[queryDt]         [smalldatetime] NOT NULL,
		[querySign]       [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[queryStatus]     [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[queryType]       [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_PreTenderQuery]
		PRIMARY KEY
		CLUSTERED
		([queryId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_PreTenderQuery]
	ADD
	CONSTRAINT [DF_tbl_PreTenderQuery_queryStatus]
	DEFAULT ('Pending') FOR [queryStatus]
GO
ALTER TABLE [dbo].[tbl_PreTenderQuery]
	ADD
	CONSTRAINT [DF_tbl_PreTenderQuery_queryType]
	DEFAULT ('Prebid') FOR [queryType]
GO
ALTER TABLE [dbo].[tbl_PreTenderQuery]
	WITH NOCHECK
	ADD CONSTRAINT [FK7541227298B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_PreTenderQuery]
	CHECK CONSTRAINT [FK7541227298B66EC5]

GO
ALTER TABLE [dbo].[tbl_PreTenderQuery]
	WITH NOCHECK
	ADD CONSTRAINT [queryTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_PreTenderQuery]
	CHECK CONSTRAINT [queryTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_PreTenderQuery] SET (LOCK_ESCALATION = TABLE)
GO
