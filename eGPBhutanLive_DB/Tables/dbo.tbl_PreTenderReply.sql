SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_PreTenderReply] (
		[preTenderReplyId]     [int] IDENTITY(1, 1) NOT NULL,
		[queryId]              [int] NOT NULL,
		[rephraseQuery]        [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[replyText]            [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[userId]               [int] NOT NULL,
		[repliedDate]          [smalldatetime] NOT NULL,
		[eSignature]           [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[rephraseQryText]      [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[replyAction]          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_PreTenderReply]
		PRIMARY KEY
		CLUSTERED
		([preTenderReplyId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_PreTenderReply]
	WITH CHECK
	ADD CONSTRAINT [FK75481C9411E6C914]
	FOREIGN KEY ([queryId]) REFERENCES [dbo].[tbl_PreTenderQuery] ([queryId])
ALTER TABLE [dbo].[tbl_PreTenderReply]
	CHECK CONSTRAINT [FK75481C9411E6C914]

GO
ALTER TABLE [dbo].[tbl_PreTenderReply]
	WITH CHECK
	ADD CONSTRAINT [replyQueryId_FK1]
	FOREIGN KEY ([queryId]) REFERENCES [dbo].[tbl_PreTenderQuery] ([queryId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_PreTenderReply]
	CHECK CONSTRAINT [replyQueryId_FK1]

GO
ALTER TABLE [dbo].[tbl_PreTenderReply] SET (LOCK_ESCALATION = TABLE)
GO
