SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_BidTemper] (
		[bidTemperId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]        [int] NOT NULL,
		[entryDate]       [datetime] NOT NULL,
		CONSTRAINT [PK_tbl_BidTemper]
		PRIMARY KEY
		CLUSTERED
		([bidTemperId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_BidTemper]
	ADD
	CONSTRAINT [DF_tbl_BidTemper_entryDate]
	DEFAULT (getdate()) FOR [entryDate]
GO
ALTER TABLE [dbo].[tbl_BidTemper] SET (LOCK_ESCALATION = TABLE)
GO
