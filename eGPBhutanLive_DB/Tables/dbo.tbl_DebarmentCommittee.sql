SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_DebarmentCommittee] (
		[debarCommitteeId]     [int] IDENTITY(1, 1) NOT NULL,
		[committeeName]        [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]            [int] NOT NULL,
		[createdDate]          [smalldatetime] NOT NULL,
		[debarmentId]          [int] NOT NULL,
		[comStatus]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_DebarmentCommittee]
		PRIMARY KEY
		CLUSTERED
		([debarCommitteeId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_DebarmentCommittee]
	ADD
	CONSTRAINT [DF_tbl_DebarmentCommittee_comStatus]
	DEFAULT ('Pending') FOR [comStatus]
GO
ALTER TABLE [dbo].[tbl_DebarmentCommittee]
	WITH CHECK
	ADD CONSTRAINT [debarmentComId_FK1]
	FOREIGN KEY ([debarmentId]) REFERENCES [dbo].[tbl_DebarmentReq] ([debarmentId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_DebarmentCommittee]
	CHECK CONSTRAINT [debarmentComId_FK1]

GO
ALTER TABLE [dbo].[tbl_DebarmentCommittee]
	WITH CHECK
	ADD CONSTRAINT [FK37270F4E99D31119]
	FOREIGN KEY ([debarmentId]) REFERENCES [dbo].[tbl_DebarmentReq] ([debarmentId])
ALTER TABLE [dbo].[tbl_DebarmentCommittee]
	CHECK CONSTRAINT [FK37270F4E99D31119]

GO
ALTER TABLE [dbo].[tbl_DebarmentCommittee] SET (LOCK_ESCALATION = TABLE)
GO
