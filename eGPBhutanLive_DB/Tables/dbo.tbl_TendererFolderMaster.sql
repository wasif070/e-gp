SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TendererFolderMaster] (
		[tendererId]       [int] NOT NULL,
		[folderId]         [int] IDENTITY(1, 1) NOT NULL,
		[folderName]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[creationDate]     [smalldatetime] NOT NULL,
		CONSTRAINT [folderId_PK]
		PRIMARY KEY
		CLUSTERED
		([folderId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TendererFolderMaster]
	WITH CHECK
	ADD CONSTRAINT [FK2F1A4F92435CF11F]
	FOREIGN KEY ([tendererId]) REFERENCES [dbo].[tbl_TendererMaster] ([tendererId])
ALTER TABLE [dbo].[tbl_TendererFolderMaster]
	CHECK CONSTRAINT [FK2F1A4F92435CF11F]

GO
ALTER TABLE [dbo].[tbl_TendererFolderMaster]
	WITH CHECK
	ADD CONSTRAINT [tendererFolder_FK1]
	FOREIGN KEY ([tendererId]) REFERENCES [dbo].[tbl_TendererMaster] ([tendererId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TendererFolderMaster]
	CHECK CONSTRAINT [tendererFolder_FK1]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Creation Dtae stores here.', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererFolderMaster', 'COLUMN', N'creationDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'System generated unique id information stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererFolderMaster', 'COLUMN', N'folderId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Folder Name stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererFolderMaster', 'COLUMN', N'folderName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'refer tbl_TendererMaster', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererFolderMaster', 'COLUMN', N'tendererId'
GO
ALTER TABLE [dbo].[tbl_TendererFolderMaster] SET (LOCK_ESCALATION = TABLE)
GO
