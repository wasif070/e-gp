SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_AppAuditTrail] (
		[appAuditTrialId]     [int] IDENTITY(1, 1) NOT NULL,
		[appId]               [int] NOT NULL,
		[childId]             [int] NOT NULL,
		[actionDate]          [smalldatetime] NOT NULL,
		[actionBy]            [int] NOT NULL,
		[action]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[signature]           [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[remarks]             [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [appAuditTrialId_PK]
		PRIMARY KEY
		CLUSTERED
		([appAuditTrialId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_AppAuditTrail]
	WITH NOCHECK
	ADD CONSTRAINT [auditAppId_Fk1]
	FOREIGN KEY ([appId]) REFERENCES [dbo].[tbl_AppMaster] ([appId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_AppAuditTrail]
	CHECK CONSTRAINT [auditAppId_Fk1]

GO
ALTER TABLE [dbo].[tbl_AppAuditTrail]
	WITH NOCHECK
	ADD CONSTRAINT [auditUserId_Fk2]
	FOREIGN KEY ([actionBy]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_AppAuditTrail]
	CHECK CONSTRAINT [auditUserId_Fk2]

GO
ALTER TABLE [dbo].[tbl_AppAuditTrail]
	WITH NOCHECK
	ADD CONSTRAINT [FK8CD0F21B30927EB8]
	FOREIGN KEY ([actionBy]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_AppAuditTrail]
	CHECK CONSTRAINT [FK8CD0F21B30927EB8]

GO
ALTER TABLE [dbo].[tbl_AppAuditTrail]
	WITH NOCHECK
	ADD CONSTRAINT [FK8CD0F21B448F6D5F]
	FOREIGN KEY ([appId]) REFERENCES [dbo].[tbl_AppMaster] ([appId])
ALTER TABLE [dbo].[tbl_AppAuditTrail]
	CHECK CONSTRAINT [FK8CD0F21B448F6D5F]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Whose perform the action, that  details information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppAuditTrail', 'COLUMN', N'action'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Refer from tbl_LogInMaster, to identify the who perform the actionBy  details are  stores here
 ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppAuditTrail', 'COLUMN', N'actionBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'AppAuditTrial action date information stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppAuditTrail', 'COLUMN', N'actionDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'System generated appAuditTrialId information stores here 
', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppAuditTrail', 'COLUMN', N'appAuditTrialId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Refer  from tbl_AppMaster, to identify the appId
', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppAuditTrail', 'COLUMN', N'appId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'ChildId information stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppAuditTrail', 'COLUMN', N'childId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Signature information stores here 
', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppAuditTrail', 'COLUMN', N'signature'
GO
ALTER TABLE [dbo].[tbl_AppAuditTrail] SET (LOCK_ESCALATION = TABLE)
GO
