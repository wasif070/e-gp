SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ConfigDocFees] (
		[docFeesId]               [int] IDENTITY(1, 1) NOT NULL,
		[docAccess]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isDocFeesReq]            [varchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[procurementMethodId]     [tinyint] NOT NULL,
		[procurementTypeId]       [tinyint] NOT NULL,
		[tenderTypeId]            [tinyint] NOT NULL,
		[isPerSecFeesReq]         [varchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isTenderSecReq]          [varchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isTenderValReq]          [varchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [UQ__tbl_Conf__EDF6C2B818B6AB08]
		UNIQUE
		NONCLUSTERED
		([docFeesId])
		ON [PRIMARY],
		CONSTRAINT [PK__tbl_Conf__EDF6C2B915DA3E5D]
		PRIMARY KEY
		CLUSTERED
		([docFeesId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ConfigDocFees]
	WITH CHECK
	ADD CONSTRAINT [FK1E582682A5C4169D]
	FOREIGN KEY ([procurementMethodId]) REFERENCES [dbo].[tbl_ProcurementMethod] ([procurementMethodId])
ALTER TABLE [dbo].[tbl_ConfigDocFees]
	CHECK CONSTRAINT [FK1E582682A5C4169D]

GO
ALTER TABLE [dbo].[tbl_ConfigDocFees]
	WITH CHECK
	ADD CONSTRAINT [FK1E582682A90681AE]
	FOREIGN KEY ([procurementTypeId]) REFERENCES [dbo].[tbl_ProcurementTypes] ([procurementTypeId])
ALTER TABLE [dbo].[tbl_ConfigDocFees]
	CHECK CONSTRAINT [FK1E582682A90681AE]

GO
ALTER TABLE [dbo].[tbl_ConfigDocFees] SET (LOCK_ESCALATION = TABLE)
GO
