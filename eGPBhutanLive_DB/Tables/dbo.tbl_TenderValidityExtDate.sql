SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderValidityExtDate] (
		[valExtDtId]          [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]            [int] NOT NULL,
		[lastPropValDt]       [date] NOT NULL,
		[newPropValDt]        [date] NOT NULL,
		[tenderSecLastDt]     [date] NULL,
		[tenderSecNewDt]      [date] NULL,
		[extReason]           [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[extStatus]           [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[extActionBy]         [int] NOT NULL,
		[extActionDt]         [smalldatetime] NOT NULL,
		[valExtReqId]         [int] NOT NULL,
		[extReasonSign]       [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[extReplySign]        [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[lastValAcceptDt]     [date] NOT NULL,
		[notifyComments]      [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[extSentToRole]       [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[extReplyDt]          [smalldatetime] NULL,
		[extSentBy]           [int] NULL,
		[govUserId]           [int] NOT NULL,
		CONSTRAINT [PK_tbl_TenderValidityExtDate]
		PRIMARY KEY
		CLUSTERED
		([valExtDtId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderValidityExtDate]
	ADD
	CONSTRAINT [DF_tbl_TenderValidityExtDate_govUserId]
	DEFAULT ((0)) FOR [govUserId]
GO
ALTER TABLE [dbo].[tbl_TenderValidityExtDate]
	WITH NOCHECK
	ADD CONSTRAINT [extDtTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderValidityExtDate]
	CHECK CONSTRAINT [extDtTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderValidityExtDate]
	WITH NOCHECK
	ADD CONSTRAINT [FKCF5D4A8898B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_TenderValidityExtDate]
	CHECK CONSTRAINT [FKCF5D4A8898B66EC5]

GO
ALTER TABLE [dbo].[tbl_TenderValidityExtDate] SET (LOCK_ESCALATION = TABLE)
GO
