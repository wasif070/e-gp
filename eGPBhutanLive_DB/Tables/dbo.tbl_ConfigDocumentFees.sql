SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_ConfigDocumentFees] (
		[DocFeeID]             [int] NOT NULL,
		[ProcurementType]      [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[MaximumDocAmtBDT]     [int] NOT NULL,
		[MaximumDocAmtUSD]     [int] NULL,
		CONSTRAINT [PK_tbl_ConfigDocumentFees]
		PRIMARY KEY
		CLUSTERED
		([DocFeeID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ConfigDocumentFees] SET (LOCK_ESCALATION = TABLE)
GO
