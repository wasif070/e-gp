SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderFormula] (
		[tenderFormulaId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderFormId]        [int] NOT NULL,
		[tenderTableId]       [int] NOT NULL,
		[columnId]            [smallint] NOT NULL,
		[formula]             [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isGrandTotal]        [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_TenderFormula]
		PRIMARY KEY
		CLUSTERED
		([tenderFormulaId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderFormula]
	WITH CHECK
	ADD CONSTRAINT [FK4FD53D11735760EE]
	FOREIGN KEY ([tenderFormId]) REFERENCES [dbo].[tbl_TenderForms] ([tenderFormId])
ALTER TABLE [dbo].[tbl_TenderFormula]
	CHECK CONSTRAINT [FK4FD53D11735760EE]

GO
ALTER TABLE [dbo].[tbl_TenderFormula]
	WITH CHECK
	ADD CONSTRAINT [tenderFormulaFormId_FK1]
	FOREIGN KEY ([tenderFormId]) REFERENCES [dbo].[tbl_TenderForms] ([tenderFormId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderFormula]
	CHECK CONSTRAINT [tenderFormulaFormId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderFormula] SET (LOCK_ESCALATION = TABLE)
GO
