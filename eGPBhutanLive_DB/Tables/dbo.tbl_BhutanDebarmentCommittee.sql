SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_BhutanDebarmentCommittee] (
		[id]                 [int] IDENTITY(1, 1) NOT NULL,
		[memberName]         [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[designation]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[role]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[emailId]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[mobileNo]           [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[nationalId]         [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]          [int] NOT NULL,
		[contactAddress]     [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[fromDate]           [smalldatetime] NOT NULL,
		[toDate]             [smalldatetime] NOT NULL,
		[createDate]         [smalldatetime] NOT NULL,
		[isActive]           [bit] NOT NULL,
		[remarks]            [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_DebarmentCommittee1]
		PRIMARY KEY
		CLUSTERED
		([id])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_BhutanDebarmentCommittee] SET (LOCK_ESCALATION = TABLE)
GO
