SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CompanyMaster] (
		[companyId]                  [int] IDENTITY(1, 1) NOT NULL,
		[userId]                     [int] NOT NULL,
		[companyName]                [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[companyNameInBangla]        [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[companyRegNumber]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[establishmentYear]          [varchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[licIssueDate]               [smalldatetime] NULL,
		[licExpiryDate]              [smalldatetime] NULL,
		[regOffAddress]              [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[regOffCountry]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[regOffState]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[regOffCity]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[regOffUpjilla]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[regOffPostcode]             [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[regOffPhoneNo]              [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[regOffFaxNo]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[corpOffAddress]             [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[corpOffCountry]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[corpOffState]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[corpOffCity]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[corpOffUpjilla]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[corpOffPostcode]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[corpOffPhoneno]             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[corpOffFaxNo]               [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[specialization]             [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[legalStatus]                [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[tinNo]                      [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[website]                    [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[tinDocName]                 [varchar](130) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[workCategory]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[originCountry]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[statutoryCertificateNo]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[tradeLicenseNumber]         [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[corpOffMobMobileNo]         [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[regOffSubDistrict]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[regOffMobileNo]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[corpOffSubDistrict]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [companyId_PK]
		PRIMARY KEY
		CLUSTERED
		([companyId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CompanyMaster]
	ADD
	CONSTRAINT [DF_tbl_CompanyMaster_corpOffCountry]
	DEFAULT ((0)) FOR [corpOffCountry]
GO
ALTER TABLE [dbo].[tbl_CompanyMaster]
	ADD
	CONSTRAINT [DF_tbl_CompanyMaster_corpOffState]
	DEFAULT ((0)) FOR [corpOffState]
GO
ALTER TABLE [dbo].[tbl_CompanyMaster]
	ADD
	CONSTRAINT [DF_tbl_CompanyMaster_licExpiryDate]
	DEFAULT (((0)-(0))-(0)) FOR [licExpiryDate]
GO
ALTER TABLE [dbo].[tbl_CompanyMaster]
	ADD
	CONSTRAINT [DF_tbl_CompanyMaster_licIssueDate]
	DEFAULT (((0)-(0))-(0)) FOR [licIssueDate]
GO
ALTER TABLE [dbo].[tbl_CompanyMaster]
	ADD
	CONSTRAINT [DF_tbl_CompanyMaster_regOffCountry]
	DEFAULT ((0)) FOR [regOffCountry]
GO
ALTER TABLE [dbo].[tbl_CompanyMaster]
	ADD
	CONSTRAINT [DF_tbl_CompanyMaster_regOffState]
	DEFAULT ((0)) FOR [regOffState]
GO
ALTER TABLE [dbo].[tbl_CompanyMaster]
	ADD
	CONSTRAINT [DF_tbl_CompanyMaster_userId]
	DEFAULT ((0)) FOR [userId]
GO
ALTER TABLE [dbo].[tbl_CompanyMaster]
	WITH CHECK
	ADD CONSTRAINT [FK2F49E5E6174DBD1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_CompanyMaster]
	CHECK CONSTRAINT [FK2F49E5E6174DBD1]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Syatem generated id  information stores  here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'companyId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Company name information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'companyName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Company in bangla information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'companyNameInBangla'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Company registration number information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'companyRegNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Company''s registered  address information stores  here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'corpOffAddress'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Corporate address city information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'corpOffCity'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Corporate address country information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'corpOffCountry'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Corporate office address faxno information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'corpOffFaxNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Corporate office address phone number information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'corpOffPhoneno'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Corporate office address postal code information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'corpOffPostcode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Corporate  address state  information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'corpOffState'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Corporate office address upjilla information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'corpOffUpjilla'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Company establishment year information stores  here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'establishmentYear'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Company license expiry date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'licExpiryDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Company''s license issue date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'licIssueDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Company''s registered address information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'regOffAddress'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Registered address city information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'regOffCity'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Registered addrress country information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'regOffCountry'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Registered office address fax number information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'regOffFaxNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Registered Office address phone number information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'regOffPhoneNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Registered office address postal code information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'regOffPostcode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Registered address state information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'regOffState'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Registered address uplilla information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'regOffUpjilla'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Company business category information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'specialization'
GO
EXEC sp_addextendedproperty N'MS_Description', N'refer tbl_LoginMaster', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyMaster', 'COLUMN', N'userId'
GO
ALTER TABLE [dbo].[tbl_CompanyMaster] SET (LOCK_ESCALATION = TABLE)
GO
