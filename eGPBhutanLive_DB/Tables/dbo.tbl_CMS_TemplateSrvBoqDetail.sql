SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_CMS_TemplateSrvBoqDetail] (
		[srvBoqDtlId]        [int] IDENTITY(1, 1) NOT NULL,
		[srvBoqId]           [int] NOT NULL,
		[templateFormId]     [int] NOT NULL,
		CONSTRAINT [srvBoqDtlId_PK]
		PRIMARY KEY
		CLUSTERED
		([srvBoqDtlId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_TemplateSrvBoqDetail]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_CMS_TemplateSrvBoqDetai_PKl_tbl_CMS_SrvBoqMaster]
	FOREIGN KEY ([srvBoqId]) REFERENCES [dbo].[tbl_CMS_SrvBoqMaster] ([srvBoqId])
ALTER TABLE [dbo].[tbl_CMS_TemplateSrvBoqDetail]
	CHECK CONSTRAINT [FK_tbl_CMS_TemplateSrvBoqDetai_PKl_tbl_CMS_SrvBoqMaster]

GO
ALTER TABLE [dbo].[tbl_CMS_TemplateSrvBoqDetail]
	WITH CHECK
	ADD CONSTRAINT [templateSrvBoqDetFormId_FK1]
	FOREIGN KEY ([templateFormId]) REFERENCES [dbo].[tbl_TemplateSectionForm] ([formId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_TemplateSrvBoqDetail]
	CHECK CONSTRAINT [templateSrvBoqDetFormId_FK1]

GO
ALTER TABLE [dbo].[tbl_CMS_TemplateSrvBoqDetail] SET (LOCK_ESCALATION = TABLE)
GO
