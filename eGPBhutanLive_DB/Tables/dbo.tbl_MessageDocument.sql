SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_MessageDocument] (
		[msgDocId]         [int] IDENTITY(1, 1) NOT NULL,
		[msgType]          [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[msgTypeId]        [int] NOT NULL,
		[documentName]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]     [smalldatetime] NOT NULL,
		[uploadedBy]       [int] NOT NULL,
		CONSTRAINT [msgDocId_PK]
		PRIMARY KEY
		CLUSTERED
		([msgDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_MessageDocument] SET (LOCK_ESCALATION = TABLE)
GO
