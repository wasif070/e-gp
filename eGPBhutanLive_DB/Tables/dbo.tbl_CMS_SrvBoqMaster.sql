SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_SrvBoqMaster] (
		[srvBoqId]       [int] IDENTITY(12, 1) NOT NULL,
		[srvBoqType]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sortOrder]      [int] NOT NULL,
		CONSTRAINT [srvBoqId_PK]
		PRIMARY KEY
		CLUSTERED
		([srvBoqId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvBoqMaster] SET (LOCK_ESCALATION = TABLE)
GO
