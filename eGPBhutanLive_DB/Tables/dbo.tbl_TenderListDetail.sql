SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderListDetail] (
		[tenderListDtId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderListId]       [int] NOT NULL,
		[itemId]             [smallint] NOT NULL,
		[itemValue]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[itemText]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isDefault]          [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_TenderListDetail]
		PRIMARY KEY
		CLUSTERED
		([tenderListDtId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderListDetail]
	WITH CHECK
	ADD CONSTRAINT [FK622370426513F66]
	FOREIGN KEY ([tenderListId]) REFERENCES [dbo].[tbl_TenderListBox] ([tenderListId])
ALTER TABLE [dbo].[tbl_TenderListDetail]
	CHECK CONSTRAINT [FK622370426513F66]

GO
ALTER TABLE [dbo].[tbl_TenderListDetail]
	WITH CHECK
	ADD CONSTRAINT [tenderListDetailId_FK1]
	FOREIGN KEY ([tenderListId]) REFERENCES [dbo].[tbl_TenderListBox] ([tenderListId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderListDetail]
	CHECK CONSTRAINT [tenderListDetailId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderListDetail] SET (LOCK_ESCALATION = TABLE)
GO
