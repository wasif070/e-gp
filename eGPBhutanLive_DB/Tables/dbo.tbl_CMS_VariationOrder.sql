SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_VariationOrder] (
		[variOrdId]             [int] IDENTITY(1, 1) NOT NULL,
		[wpId]                  [int] NULL,
		[variOrdCreationDt]     [datetime] NOT NULL,
		[variOrdStatus]         [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]             [int] NOT NULL,
		[variOrdWFStatus]       [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[variationNo]           [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[tenderId]              [int] NULL,
		CONSTRAINT [PK_tbl_CMS_VariationOrder]
		PRIMARY KEY
		CLUSTERED
		([variOrdId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_VariationOrder] SET (LOCK_ESCALATION = TABLE)
GO
