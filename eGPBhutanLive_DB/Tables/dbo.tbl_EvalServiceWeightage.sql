SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_EvalServiceWeightage] (
		[servWeightId]        [int] IDENTITY(1, 1) NOT NULL,
		[techWeight]          [int] NOT NULL,
		[financialWeight]     [int] NOT NULL,
		[tenderId]            [int] NOT NULL,
		CONSTRAINT [PK_tbl_EvalServiceWeightage]
		PRIMARY KEY
		CLUSTERED
		([servWeightId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalServiceWeightage]
	WITH NOCHECK
	ADD CONSTRAINT [FK_tbl_EvalServiceWeightage_tbl_TenderMaster]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EvalServiceWeightage]
	CHECK CONSTRAINT [FK_tbl_EvalServiceWeightage_tbl_TenderMaster]

GO
ALTER TABLE [dbo].[tbl_EvalServiceWeightage] SET (LOCK_ESCALATION = TABLE)
GO
