SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_TenderWatchList] (
		[watchListId]      [int] IDENTITY(1, 1) NOT NULL,
		[creationDate]     [datetime] NOT NULL,
		[tenderId]         [int] NOT NULL,
		[userId]           [int] NOT NULL,
		[packageId]        [int] NULL,
		CONSTRAINT [UQ__tbl_Tend__52C7F89637CCF7B6]
		UNIQUE
		NONCLUSTERED
		([watchListId])
		ON [PRIMARY],
		CONSTRAINT [PK__tbl_Tend__52C7F897A99E78B8]
		PRIMARY KEY
		CLUSTERED
		([watchListId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderWatchList]
	WITH NOCHECK
	ADD CONSTRAINT [watchListTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderWatchList]
	CHECK CONSTRAINT [watchListTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderWatchList]
	WITH CHECK
	ADD CONSTRAINT [watchlistTenderId_Fk2]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_TenderWatchList]
	CHECK CONSTRAINT [watchlistTenderId_Fk2]

GO
ALTER TABLE [dbo].[tbl_TenderWatchList] SET (LOCK_ESCALATION = TABLE)
GO
