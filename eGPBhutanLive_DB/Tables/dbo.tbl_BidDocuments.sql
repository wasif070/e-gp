SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_BidDocuments] (
		[bidDocId]         [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]         [int] NOT NULL,
		[formId]           [int] NOT NULL,
		[userId]           [int] NOT NULL,
		[mappedDate]       [smalldatetime] NOT NULL,
		[companyDocId]     [int] NOT NULL,
		[manDocId]         [int] NOT NULL,
		CONSTRAINT [PK_tbl_BidDocuments]
		PRIMARY KEY
		CLUSTERED
		([bidDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_BidDocuments]
	ADD
	CONSTRAINT [DF_tbl_BidDocuments_manDocId]
	DEFAULT ((0)) FOR [manDocId]
GO
ALTER TABLE [dbo].[tbl_BidDocuments] SET (LOCK_ESCALATION = TABLE)
GO
