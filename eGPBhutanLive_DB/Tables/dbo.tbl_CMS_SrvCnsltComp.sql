SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_SrvCnsltComp] (
		[srvCCId]            [int] IDENTITY(1, 1) NOT NULL,
		[srvFormMapId]       [int] NOT NULL,
		[srNo]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[consultantCtg]      [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[keyNosPE]           [int] NOT NULL,
		[supportNosPE]       [int] NOT NULL,
		[months]             [decimal](15, 3) NULL,
		[consultantName]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[keyNos]             [int] NOT NULL,
		[supportNos]         [int] NOT NULL,
		[totalNosPE]         [int] NULL,
		[variOrdId]          [int] NULL,
		CONSTRAINT [PK_tbl_CMS_SrvCnsltComp]
		PRIMARY KEY
		CLUSTERED
		([srvCCId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvCnsltComp] SET (LOCK_ESCALATION = TABLE)
GO
