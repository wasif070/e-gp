SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_CMS_SrvPSHistory] (
		[srvPSHistId]      [int] IDENTITY(1, 1) NOT NULL,
		[srvPSId]          [int] NOT NULL,
		[srvFormMapId]     [int] NOT NULL,
		[peEndDate]        [datetime] NOT NULL,
		[endDate]          [datetime] NOT NULL,
		[histCnt]          [int] NOT NULL,
		[createdDate]      [datetime] NULL,
		CONSTRAINT [PK_tbl_CMS_SrvPSHistory]
		PRIMARY KEY
		CLUSTERED
		([srvPSHistId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvPSHistory]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_CMS_SrvPSHistory_tbl_CMS_SrvPaymentSch]
	FOREIGN KEY ([srvPSId]) REFERENCES [dbo].[tbl_CMS_SrvPaymentSch] ([srvPSId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_SrvPSHistory]
	CHECK CONSTRAINT [FK_tbl_CMS_SrvPSHistory_tbl_CMS_SrvPaymentSch]

GO
ALTER TABLE [dbo].[tbl_CMS_SrvPSHistory] SET (LOCK_ESCALATION = TABLE)
GO
