SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderHash] (
		[tenderHashId]         [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]             [int] NOT NULL,
		[tenderHashUserId]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderHash]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[creationDate]         [smalldatetime] NOT NULL,
		[createdBy]            [int] NOT NULL,
		CONSTRAINT [PK_tbl_TenderHash]
		PRIMARY KEY
		CLUSTERED
		([tenderHashId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderHash] SET (LOCK_ESCALATION = TABLE)
GO
