SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_ReportForms] (
		[reportFormId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderFormId]     [int] NOT NULL,
		[reportId]         [int] NOT NULL,
		CONSTRAINT [PK_tbl_ReportForms]
		PRIMARY KEY
		CLUSTERED
		([reportFormId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ReportForms]
	WITH CHECK
	ADD CONSTRAINT [FKFBDB803AB55AD385]
	FOREIGN KEY ([reportId]) REFERENCES [dbo].[tbl_ReportMaster] ([reportId])
ALTER TABLE [dbo].[tbl_ReportForms]
	CHECK CONSTRAINT [FKFBDB803AB55AD385]

GO
ALTER TABLE [dbo].[tbl_ReportForms]
	WITH CHECK
	ADD CONSTRAINT [reportFormId_FK1]
	FOREIGN KEY ([reportId]) REFERENCES [dbo].[tbl_ReportMaster] ([reportId])
ALTER TABLE [dbo].[tbl_ReportForms]
	CHECK CONSTRAINT [reportFormId_FK1]

GO
ALTER TABLE [dbo].[tbl_ReportForms] SET (LOCK_ESCALATION = TABLE)
GO
