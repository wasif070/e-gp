SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EmployeeSubFinPower] (
		[employeeFinPowerId]      [int] IDENTITY(1, 1) NOT NULL,
		[employeeId]              [int] NOT NULL,
		[procurementNatureId]     [tinyint] NOT NULL,
		[budgetTypeId]            [tinyint] NOT NULL,
		[operator]                [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[amount]                  [money] NOT NULL,
		[employeeRoleId]          [int] NOT NULL,
		[procurementMethodId]     [tinyint] NOT NULL,
		[financialYearId]         [int] NOT NULL,
		CONSTRAINT [PK_tbl_EmployeeSubPower]
		PRIMARY KEY
		CLUSTERED
		([employeeFinPowerId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EmployeeSubFinPower]
	ADD
	CONSTRAINT [DF_tbl_EmployeeFinancialPower1_procurementNatureId]
	DEFAULT ((0)) FOR [procurementNatureId]
GO
ALTER TABLE [dbo].[tbl_EmployeeSubFinPower]
	WITH NOCHECK
	ADD CONSTRAINT [empBudgetType1_FK3]
	FOREIGN KEY ([budgetTypeId]) REFERENCES [dbo].[tbl_BudgetType] ([budgetTypeId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EmployeeSubFinPower]
	CHECK CONSTRAINT [empBudgetType1_FK3]

GO
ALTER TABLE [dbo].[tbl_EmployeeSubFinPower]
	WITH NOCHECK
	ADD CONSTRAINT [FK5B1EF06BD6793EF9]
	FOREIGN KEY ([budgetTypeId]) REFERENCES [dbo].[tbl_BudgetType] ([budgetTypeId])
ALTER TABLE [dbo].[tbl_EmployeeSubFinPower]
	CHECK CONSTRAINT [FK5B1EF06BD6793EF9]

GO
ALTER TABLE [dbo].[tbl_EmployeeSubFinPower] SET (LOCK_ESCALATION = TABLE)
GO
