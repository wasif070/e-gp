SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_PreTenderQryDocs] (
		[prebidQueryDocId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]             [int] NOT NULL,
		[userId]               [int] NOT NULL,
		[queryId]              [int] NOT NULL,
		[documentName]         [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]       [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]              [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]         [smalldatetime] NOT NULL,
		[uploadedBy]           [int] NOT NULL,
		[docType]              [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_PreTenderQryDocs]
		PRIMARY KEY
		CLUSTERED
		([prebidQueryDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_PreTenderQryDocs]
	ADD
	CONSTRAINT [DF_tbl_PreTenderQryDocs_docType]
	DEFAULT ('Prebid') FOR [docType]
GO
ALTER TABLE [dbo].[tbl_PreTenderQryDocs] SET (LOCK_ESCALATION = TABLE)
GO
