SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_WcCertiHistory] (
		[wcCertiHistId]            [int] IDENTITY(1, 1) NOT NULL,
		[wcCertId]                 [int] NOT NULL,
		[actualDateOfComplete]     [datetime] NOT NULL,
		[isWorkComplete]           [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[remarks]                  [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]                [int] NOT NULL,
		[vendorRating]             [int] NOT NULL,
		[createdDt]                [datetime] NULL,
		CONSTRAINT [PK_tbl_CMS_WcCertiHistory]
		PRIMARY KEY
		CLUSTERED
		([wcCertiHistId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_WcCertiHistory]
	WITH CHECK
	ADD CONSTRAINT [FK_WcCertiHist_PK_WcCerti]
	FOREIGN KEY ([wcCertId]) REFERENCES [dbo].[tbl_CMS_WcCertificate] ([wcCertId])
	ON DELETE CASCADE
ALTER TABLE [dbo].[tbl_CMS_WcCertiHistory]
	CHECK CONSTRAINT [FK_WcCertiHist_PK_WcCerti]

GO
ALTER TABLE [dbo].[tbl_CMS_WcCertiHistory] SET (LOCK_ESCALATION = TABLE)
GO
