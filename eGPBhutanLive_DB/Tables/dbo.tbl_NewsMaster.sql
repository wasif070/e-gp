SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_NewsMaster] (
		[newsId]           [int] IDENTITY(1, 1) NOT NULL,
		[locationId]       [smallint] NOT NULL,
		[newsType]         [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isImp]            [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[newsBrief]        [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[newsDetails]      [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdTime]      [datetime] NOT NULL,
		[isArchive]        [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isDeleted]        [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[fileName]         [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[letterNumber]     [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_NewsMaster]
		PRIMARY KEY
		CLUSTERED
		([newsId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_NewsMaster] SET (LOCK_ESCALATION = TABLE)
GO
