SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderFrameWork] (
		[tenderFrameWorkOfficeId]     [bigint] IDENTITY(1, 1) NOT NULL,
		[APPId]                       [int] NOT NULL,
		[PackageId]                   [int] NOT NULL,
		[TenderId]                    [int] NOT NULL,
		[PEOfficeId]                  [int] NOT NULL,
		[PEOfficeName]                [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[OfficeDistrict]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[OfficeAddress]               [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ItemName]                    [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Quantity]                    [float] NOT NULL,
		[Unit]                        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CreateDate]                  [datetime] NOT NULL,
		[CreateBy]                    [int] NOT NULL,
		CONSTRAINT [PK_tbl_TenderFrameWork]
		PRIMARY KEY
		CLUSTERED
		([tenderFrameWorkOfficeId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderFrameWork] SET (LOCK_ESCALATION = TABLE)
GO
