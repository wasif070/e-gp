SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalDroppedMembers] (
		[memberId]             [int] IDENTITY(1, 1) NOT NULL,
		[committeeId]          [int] NOT NULL,
		[userId]               [int] NOT NULL,
		[createdDate]          [smalldatetime] NOT NULL,
		[appStatus]            [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[appDate]              [smalldatetime] NOT NULL,
		[remarks]              [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[eSignature]           [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[digitalSignature]     [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[memberRole]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[memberFrom]           [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderId]             [int] NOT NULL,
		[action]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[actionBy]             [int] NOT NULL,
		[actionDt]             [smalldatetime] NOT NULL,
		CONSTRAINT [PK_tbl_EvalDroppedMembers]
		PRIMARY KEY
		CLUSTERED
		([memberId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalDroppedMembers] SET (LOCK_ESCALATION = TABLE)
GO
