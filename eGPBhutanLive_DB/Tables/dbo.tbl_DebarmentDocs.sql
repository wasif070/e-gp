SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_DebarmentDocs] (
		[debarDocId]       [int] IDENTITY(1, 1) NOT NULL,
		[debarmentId]      [int] NOT NULL,
		[uploadedBy]       [int] NOT NULL,
		[uploadedDate]     [datetime] NOT NULL,
		[documentName]     [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[documentDesc]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedRole]     [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tendererId]       [int] NULL,
		CONSTRAINT [PK_tbl_DebarmentDocs]
		PRIMARY KEY
		CLUSTERED
		([debarDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_DebarmentDocs] SET (LOCK_ESCALATION = TABLE)
GO
