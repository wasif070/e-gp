SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_JointVenture] (
		[JVId]             [int] IDENTITY(1, 1) NOT NULL,
		[JVName]           [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[creationDate]     [datetime] NOT NULL,
		[createdBy]        [int] NOT NULL,
		[JVStatus]         [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[newJVUserId]      [int] NOT NULL,
		CONSTRAINT [PK_tbl_JointVenture]
		PRIMARY KEY
		CLUSTERED
		([JVId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_JointVenture]
	ADD
	CONSTRAINT [DF_tbl_JointVenture_userId]
	DEFAULT ((0)) FOR [newJVUserId]
GO
ALTER TABLE [dbo].[tbl_JointVenture] SET (LOCK_ESCALATION = TABLE)
GO
