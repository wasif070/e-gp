SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CompanyJointVenture] (
		[ventureId]       [int] IDENTITY(1, 1) NOT NULL,
		[jvCompanyId]     [int] NOT NULL,
		[companyId]       [int] NOT NULL,
		[jvRole]          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [ventureId_PK]
		PRIMARY KEY
		CLUSTERED
		([ventureId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CompanyJointVenture]
	WITH NOCHECK
	ADD CONSTRAINT [FK89A4E7A52CE47E4B]
	FOREIGN KEY ([jvCompanyId]) REFERENCES [dbo].[tbl_CompanyMaster] ([companyId])
ALTER TABLE [dbo].[tbl_CompanyJointVenture]
	CHECK CONSTRAINT [FK89A4E7A52CE47E4B]

GO
ALTER TABLE [dbo].[tbl_CompanyJointVenture]
	WITH NOCHECK
	ADD CONSTRAINT [jvCompanyId_FK1]
	FOREIGN KEY ([jvCompanyId]) REFERENCES [dbo].[tbl_CompanyMaster] ([companyId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CompanyJointVenture]
	CHECK CONSTRAINT [jvCompanyId_FK1]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Refer tbl_CompanyMaster , Id of new joint venture company stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyJointVenture', 'COLUMN', N'companyId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Refer tbl_CompanyMaster , Id of company who is performing joint venture', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyJointVenture', 'COLUMN', N'jvCompanyId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Joint venture role information stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyJointVenture', 'COLUMN', N'jvRole'
GO
EXEC sp_addextendedproperty N'MS_Description', N'System generated id information stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyJointVenture', 'COLUMN', N'ventureId'
GO
ALTER TABLE [dbo].[tbl_CompanyJointVenture] SET (LOCK_ESCALATION = TABLE)
GO
