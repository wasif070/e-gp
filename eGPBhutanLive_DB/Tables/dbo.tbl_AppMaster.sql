SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_AppMaster] (
		[appId]                   [int] IDENTITY(1, 1) NOT NULL,
		[financialYear]           [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[budgetType]              [tinyint] NOT NULL,
		[projectId]               [int] NOT NULL,
		[projectName]             [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[officeId]                [int] NOT NULL,
		[employeeId]              [int] NOT NULL,
		[aAEmployeeId]            [int] NOT NULL,
		[appCode]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]               [int] NOT NULL,
		[departmentId]            [smallint] NOT NULL,
		[aAProcurementRoleId]     [tinyint] NOT NULL,
		[appStatus]               [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[appType]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[entrustingAgency]        [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[refAppId]                [int] NULL,
		[reviseCount]             [int] NULL,
		[lastRevisionDate]        [datetime] NULL,
		[activityName]            [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [appId_PK]
		PRIMARY KEY
		CLUSTERED
		([appId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_AppMaster]
	ADD
	CONSTRAINT [DF_tbl_AppMaster_aAProcurementRole]
	DEFAULT ((0)) FOR [aAProcurementRoleId]
GO
ALTER TABLE [dbo].[tbl_AppMaster]
	ADD
	CONSTRAINT [DF_tbl_AppMaster_appStatus]
	DEFAULT ('Pending') FOR [appStatus]
GO
ALTER TABLE [dbo].[tbl_AppMaster]
	ADD
	CONSTRAINT [DF_tbl_AppMaster_projectId]
	DEFAULT ((0)) FOR [projectId]
GO
ALTER TABLE [dbo].[tbl_AppMaster]
	WITH CHECK
	ADD CONSTRAINT [appAAEmployee_Fk2]
	FOREIGN KEY ([aAEmployeeId]) REFERENCES [dbo].[tbl_EmployeeMaster] ([employeeId])
	ON DELETE CASCADE
ALTER TABLE [dbo].[tbl_AppMaster]
	CHECK CONSTRAINT [appAAEmployee_Fk2]

GO
ALTER TABLE [dbo].[tbl_AppMaster]
	WITH CHECK
	ADD CONSTRAINT [appDepartmentId_Fk5]
	FOREIGN KEY ([departmentId]) REFERENCES [dbo].[tbl_DepartmentMaster] ([departmentId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_AppMaster]
	CHECK CONSTRAINT [appDepartmentId_Fk5]

GO
ALTER TABLE [dbo].[tbl_AppMaster]
	WITH CHECK
	ADD CONSTRAINT [appEmployeeId_FK1]
	FOREIGN KEY ([employeeId]) REFERENCES [dbo].[tbl_EmployeeMaster] ([employeeId])
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_AppMaster]
	CHECK CONSTRAINT [appEmployeeId_FK1]

GO
ALTER TABLE [dbo].[tbl_AppMaster]
	WITH CHECK
	ADD CONSTRAINT [appOfficeId_Fk3]
	FOREIGN KEY ([officeId]) REFERENCES [dbo].[tbl_OfficeMaster] ([officeId])
	ON DELETE CASCADE
ALTER TABLE [dbo].[tbl_AppMaster]
	CHECK CONSTRAINT [appOfficeId_Fk3]

GO
ALTER TABLE [dbo].[tbl_AppMaster]
	WITH CHECK
	ADD CONSTRAINT [appUserId_Fk4]
	FOREIGN KEY ([createdBy]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
ALTER TABLE [dbo].[tbl_AppMaster]
	CHECK CONSTRAINT [appUserId_Fk4]

GO
ALTER TABLE [dbo].[tbl_AppMaster]
	WITH CHECK
	ADD CONSTRAINT [FK42AC7882B6F41AEA]
	FOREIGN KEY ([createdBy]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_AppMaster]
	CHECK CONSTRAINT [FK42AC7882B6F41AEA]

GO
ALTER TABLE [dbo].[tbl_AppMaster]
	WITH CHECK
	ADD CONSTRAINT [FK42AC7882E4B351C1]
	FOREIGN KEY ([departmentId]) REFERENCES [dbo].[tbl_DepartmentMaster] ([departmentId])
ALTER TABLE [dbo].[tbl_AppMaster]
	CHECK CONSTRAINT [FK42AC7882E4B351C1]

GO
ALTER TABLE [dbo].[tbl_AppMaster]
	WITH CHECK
	ADD CONSTRAINT [FK42AC7882EAD91E95]
	FOREIGN KEY ([officeId]) REFERENCES [dbo].[tbl_OfficeMaster] ([officeId])
ALTER TABLE [dbo].[tbl_AppMaster]
	CHECK CONSTRAINT [FK42AC7882EAD91E95]

GO
ALTER TABLE [dbo].[tbl_AppMaster]
	WITH CHECK
	ADD CONSTRAINT [FK42AC7882F5489C79]
	FOREIGN KEY ([employeeId]) REFERENCES [dbo].[tbl_EmployeeMaster] ([employeeId])
ALTER TABLE [dbo].[tbl_AppMaster]
	CHECK CONSTRAINT [FK42AC7882F5489C79]

GO
EXEC sp_addextendedproperty N'MS_Description', N'AAProcurementRole information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppMaster', 'COLUMN', N'aAProcurementRoleId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'projectName information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppMaster', 'COLUMN', N'projectName'
GO
ALTER TABLE [dbo].[tbl_AppMaster] SET (LOCK_ESCALATION = TABLE)
GO
