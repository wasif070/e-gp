SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_SrvREVari] (
		[srvREVariId]       [int] IDENTITY(1, 1) NOT NULL,
		[srNo]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[catagoryDesc]      [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[description]       [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[unit]              [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[qty]               [decimal](15, 3) NOT NULL,
		[unitCost]          [decimal](15, 3) NOT NULL,
		[totalAmt]          [decimal](15, 3) NOT NULL,
		[rowId]             [int] NOT NULL,
		[tenderTableId]     [int] NOT NULL,
		[srvFormMapId]      [int] NULL,
		[variOrdId]         [int] NOT NULL,
		CONSTRAINT [PK_tbl_CMS_SrvREVari]
		PRIMARY KEY
		CLUSTERED
		([srvREVariId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvREVari] SET (LOCK_ESCALATION = TABLE)
GO
