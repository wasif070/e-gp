SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_PeTaxConfig] (
		[peTaxConfigId]             [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]                  [int] NOT NULL,
		[lotId]                     [int] NOT NULL,
		[isAdvanceApplicable]       [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[advancePercent]            [decimal](6, 3) NOT NULL,
		[advanceAdjustment]         [decimal](6, 3) NOT NULL,
		[isRetentionApplicable]     [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[retentionPercent]          [decimal](6, 3) NOT NULL,
		[retentionAdjustment]       [decimal](6, 3) NOT NULL,
		[isPgApplicable]            [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[pgPercent]                 [decimal](6, 3) NOT NULL,
		[pgAdjustment]              [decimal](6, 3) NOT NULL,
		[isVatApplicable]           [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[vatPercent]                [decimal](6, 3) NOT NULL,
		[isBonusApplicable]         [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[bonusPercent]              [decimal](6, 3) NOT NULL,
		[bonusAfter]                [decimal](6, 3) NOT NULL,
		[isLdaApplicable]           [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[idPercent]                 [decimal](6, 3) NOT NULL,
		[idAfter]                   [decimal](6, 3) NOT NULL,
		[isPenaltyApplicable]       [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[penaltyPercent]            [decimal](6, 3) NOT NULL,
		[penaltyAfter]              [decimal](6, 3) NOT NULL,
		[createdBy]                 [int] NOT NULL,
		[createdDate]               [datetime] NOT NULL,
		CONSTRAINT [peTaxConfigId_PK]
		PRIMARY KEY
		CLUSTERED
		([peTaxConfigId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_PeTaxConfig]
	ADD
	CONSTRAINT [DF_tbl_CMS_PetaxConfig_isAdvanceApplicable]
	DEFAULT ('Yes') FOR [isAdvanceApplicable]
GO
ALTER TABLE [dbo].[tbl_CMS_PeTaxConfig]
	ADD
	CONSTRAINT [DF_tbl_CMS_PetaxConfig_isBonusApplicable]
	DEFAULT ('Yes') FOR [isBonusApplicable]
GO
ALTER TABLE [dbo].[tbl_CMS_PeTaxConfig]
	ADD
	CONSTRAINT [DF_tbl_CMS_PetaxConfig_isLdaApplicable]
	DEFAULT ('Yes') FOR [isLdaApplicable]
GO
ALTER TABLE [dbo].[tbl_CMS_PeTaxConfig]
	ADD
	CONSTRAINT [DF_tbl_CMS_PetaxConfig_isPenaltyApplicable]
	DEFAULT ('Yes') FOR [isPenaltyApplicable]
GO
ALTER TABLE [dbo].[tbl_CMS_PeTaxConfig]
	ADD
	CONSTRAINT [DF_tbl_CMS_PetaxConfig_isPgApplicable]
	DEFAULT ('Yes') FOR [isPgApplicable]
GO
ALTER TABLE [dbo].[tbl_CMS_PeTaxConfig]
	ADD
	CONSTRAINT [DF_tbl_CMS_PetaxConfig_isRetentionApplicable]
	DEFAULT ('Yes') FOR [isRetentionApplicable]
GO
ALTER TABLE [dbo].[tbl_CMS_PeTaxConfig]
	ADD
	CONSTRAINT [DF_tbl_CMS_PetaxConfig_isVatApplicable]
	DEFAULT ('Yes') FOR [isVatApplicable]
GO
ALTER TABLE [dbo].[tbl_CMS_PeTaxConfig]
	WITH NOCHECK
	ADD CONSTRAINT [petaxConfigTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_PeTaxConfig]
	CHECK CONSTRAINT [petaxConfigTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_CMS_PeTaxConfig] SET (LOCK_ESCALATION = TABLE)
GO
