SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_WorkFlowRuleEngineHist] (
		[wfRuleAuditId]         [int] IDENTITY(1, 1) NOT NULL,
		[procurementRoleId]     [tinyint] NOT NULL,
		[wfRoleId]              [tinyint] NOT NULL,
		[eventId]               [smallint] NOT NULL,
		[actionDate]            [smalldatetime] NOT NULL,
		[actionBy]              [int] NOT NULL,
		[action]                [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isPubDateReq]          [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[wfInitRuleId]          [int] NOT NULL,
		CONSTRAINT [PK_tbl_WorkFlowRuleAuditTrail]
		PRIMARY KEY
		CLUSTERED
		([wfRuleAuditId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_WorkFlowRuleEngineHist] SET (LOCK_ESCALATION = TABLE)
GO
