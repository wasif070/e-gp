SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_TenderLots] (
		[tenderLotId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]        [int] NOT NULL,
		[appPkgLotId]     [int] NOT NULL,
		CONSTRAINT [tenderLotId_PK]
		PRIMARY KEY
		CLUSTERED
		([tenderLotId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderLots]
	WITH CHECK
	ADD CONSTRAINT [FKCD05737798B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_TenderLots]
	CHECK CONSTRAINT [FKCD05737798B66EC5]

GO
ALTER TABLE [dbo].[tbl_TenderLots]
	WITH CHECK
	ADD CONSTRAINT [lotTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderLots]
	CHECK CONSTRAINT [lotTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderLots] SET (LOCK_ESCALATION = TABLE)
GO
