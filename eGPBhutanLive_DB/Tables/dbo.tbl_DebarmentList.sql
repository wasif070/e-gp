SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_DebarmentList] (
		[debarListId]      [int] IDENTITY(1, 1) NOT NULL,
		[userId]           [int] NOT NULL,
		[debarStartDt]     [smalldatetime] NOT NULL,
		[debarEndDt]       [smalldatetime] NULL,
		[debarredBy]       [int] NOT NULL,
		[remarks]          [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[debarDt]          [smalldatetime] NOT NULL,
		[debarIds]         [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[debarTypeId]      [int] NOT NULL,
		CONSTRAINT [PK_tbl_DebarmentList]
		PRIMARY KEY
		CLUSTERED
		([debarListId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_DebarmentList] SET (LOCK_ESCALATION = TABLE)
GO
