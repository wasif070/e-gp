SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_DateConfig] (
		[actContractDtId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]            [int] NOT NULL,
		[appPkgLotId]         [int] NOT NULL,
		[actualStartDt]       [datetime] NOT NULL,
		[actualEndDate]       [datetime] NOT NULL,
		[noOfDays]            [int] NOT NULL,
		[createdBy]           [int] NOT NULL,
		[createdDate]         [datetime] NOT NULL,
		[userTypeId]          [int] NOT NULL,
		[contractId]          [int] NOT NULL,
		[wfStatus]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [actContractDtId_PK]
		PRIMARY KEY
		CLUSTERED
		([actContractDtId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_DateConfig]
	WITH CHECK
	ADD CONSTRAINT [cmsDateConfigContractId_FK2]
	FOREIGN KEY ([contractId]) REFERENCES [dbo].[tbl_ContractSign] ([contractSignId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_DateConfig]
	CHECK CONSTRAINT [cmsDateConfigContractId_FK2]

GO
ALTER TABLE [dbo].[tbl_CMS_DateConfig]
	WITH NOCHECK
	ADD CONSTRAINT [cmsDateConfigTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_DateConfig]
	CHECK CONSTRAINT [cmsDateConfigTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_CMS_DateConfig] SET (LOCK_ESCALATION = TABLE)
GO
