SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_ReportLots] (
		[rptLotId]     [int] IDENTITY(1, 1) NOT NULL,
		[reportId]     [int] NOT NULL,
		[pkgLotId]     [int] NOT NULL,
		CONSTRAINT [PK_tbl_ReportLots]
		PRIMARY KEY
		CLUSTERED
		([rptLotId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ReportLots] SET (LOCK_ESCALATION = TABLE)
GO
