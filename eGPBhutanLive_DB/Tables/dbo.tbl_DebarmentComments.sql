SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_DebarmentComments] (
		[debarCommentId]     [int] IDENTITY(1, 1) NOT NULL,
		[debarmentId]        [int] NOT NULL,
		[userId]             [int] NOT NULL,
		[entryDt]            [smalldatetime] NOT NULL,
		[comments]           [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[commentsBy]         [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_DebarmentComments]
		PRIMARY KEY
		CLUSTERED
		([debarCommentId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_DebarmentComments] SET (LOCK_ESCALATION = TABLE)
GO
