SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalRptForwardToAA] (
		[evalFFRptId]           [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]              [int] NOT NULL,
		[pkgLotId]              [int] NOT NULL,
		[roundId]               [int] NOT NULL,
		[rptStatus]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sentByUserId]          [int] NOT NULL,
		[sentByGovUserId]       [int] NOT NULL,
		[sentDate]              [smalldatetime] NOT NULL,
		[sentUserRemarks]       [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[actionUserRole]        [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[actionUserId]          [int] NOT NULL,
		[actionGovUserId]       [int] NOT NULL,
		[action]                [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[actionDt]              [smalldatetime] NOT NULL,
		[actionUserRemarks]     [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_EvalRptForwardToAA]
		PRIMARY KEY
		CLUSTERED
		([evalFFRptId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalRptForwardToAA]
	ADD
	CONSTRAINT [DF_tbl_EvalRptForwardToAA_actionUserRole]
	DEFAULT ('Reviewer') FOR [actionUserRole]
GO
ALTER TABLE [dbo].[tbl_EvalRptForwardToAA]
	ADD
	CONSTRAINT [DF_tbl_EvalRptForwardToAA_pkgLotId]
	DEFAULT ((0)) FOR [pkgLotId]
GO
ALTER TABLE [dbo].[tbl_EvalRptForwardToAA]
	ADD
	CONSTRAINT [DF_tbl_EvalRptForwardToAA_roundId]
	DEFAULT ((1)) FOR [roundId]
GO
ALTER TABLE [dbo].[tbl_EvalRptForwardToAA]
	ADD
	CONSTRAINT [DF_tbl_EvalRptForwardToAA_rptStatus]
	DEFAULT ('Pending') FOR [rptStatus]
GO
ALTER TABLE [dbo].[tbl_EvalRptForwardToAA]
	WITH NOCHECK
	ADD CONSTRAINT [evalRptForwardToAATenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EvalRptForwardToAA]
	CHECK CONSTRAINT [evalRptForwardToAATenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_EvalRptForwardToAA] SET (LOCK_ESCALATION = TABLE)
GO
