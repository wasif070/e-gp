SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_DateConfigHistory] (
		[actContractDtHistId]     [int] IDENTITY(1, 1) NOT NULL,
		[actContractDtId]         [int] NOT NULL,
		[actualStartDate]         [datetime] NOT NULL,
		[actualEndDate]           [datetime] NOT NULL,
		[noofDays]                [int] NOT NULL,
		[createdDate]             [datetime] NULL,
		[wfStatus]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [actContractDtHistId_PK]
		PRIMARY KEY
		CLUSTERED
		([actContractDtHistId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_DateConfigHistory]
	WITH CHECK
	ADD CONSTRAINT [cmsDateConfigHistActContractDtId_FK1]
	FOREIGN KEY ([actContractDtId]) REFERENCES [dbo].[tbl_CMS_DateConfig] ([actContractDtId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_DateConfigHistory]
	CHECK CONSTRAINT [cmsDateConfigHistActContractDtId_FK1]

GO
ALTER TABLE [dbo].[tbl_CMS_DateConfigHistory] SET (LOCK_ESCALATION = TABLE)
GO
