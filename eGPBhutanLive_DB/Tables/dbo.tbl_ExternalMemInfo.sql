SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ExternalMemInfo] (
		[memberId]       [int] IDENTITY(1, 1) NOT NULL,
		[userId]         [int] NOT NULL,
		[fullName]       [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[nameBangla]     [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[nationalId]     [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[phoneNo]        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[mobileNo]       [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]      [int] NOT NULL,
		[creationDt]     [smalldatetime] NOT NULL,
		CONSTRAINT [PK_tbl_ExternalMemInfo]
		PRIMARY KEY
		CLUSTERED
		([memberId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ExternalMemInfo]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_ExternalMemInfo_tbl_LoginMaster]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_ExternalMemInfo]
	CHECK CONSTRAINT [FK_tbl_ExternalMemInfo_tbl_LoginMaster]

GO
ALTER TABLE [dbo].[tbl_ExternalMemInfo]
	WITH CHECK
	ADD CONSTRAINT [FK447687376174DBD1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_ExternalMemInfo]
	CHECK CONSTRAINT [FK447687376174DBD1]

GO
ALTER TABLE [dbo].[tbl_ExternalMemInfo] SET (LOCK_ESCALATION = TABLE)
GO
