SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_ConfigNoa] (
		[configNoaId]             [int] IDENTITY(1, 1) NOT NULL,
		[procurementNatureId]     [tinyint] NOT NULL,
		[procurementMethodId]     [tinyint] NOT NULL,
		[procurementTypeId]       [tinyint] NOT NULL,
		[minValue]                [numeric](15, 2) NOT NULL,
		[maxValue]                [numeric](15, 2) NOT NULL,
		[noaAcceptDays]           [smallint] NOT NULL,
		[perSecSubDays]           [smallint] NOT NULL,
		[conSignDays]             [smallint] NOT NULL,
		[loiNoa]                  [smallint] NULL,
		CONSTRAINT [configNoaId_Pk]
		PRIMARY KEY
		CLUSTERED
		([configNoaId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ConfigNoa]
	WITH CHECK
	ADD CONSTRAINT [FKC49B94BD3C4194A9]
	FOREIGN KEY ([procurementNatureId]) REFERENCES [dbo].[tbl_ProcurementNature] ([procurementNatureId])
ALTER TABLE [dbo].[tbl_ConfigNoa]
	CHECK CONSTRAINT [FKC49B94BD3C4194A9]

GO
ALTER TABLE [dbo].[tbl_ConfigNoa]
	WITH CHECK
	ADD CONSTRAINT [FKC49B94BDA5C4169D]
	FOREIGN KEY ([procurementMethodId]) REFERENCES [dbo].[tbl_ProcurementMethod] ([procurementMethodId])
ALTER TABLE [dbo].[tbl_ConfigNoa]
	CHECK CONSTRAINT [FKC49B94BDA5C4169D]

GO
ALTER TABLE [dbo].[tbl_ConfigNoa]
	WITH CHECK
	ADD CONSTRAINT [FKC49B94BDA90681AE]
	FOREIGN KEY ([procurementTypeId]) REFERENCES [dbo].[tbl_ProcurementTypes] ([procurementTypeId])
ALTER TABLE [dbo].[tbl_ConfigNoa]
	CHECK CONSTRAINT [FKC49B94BDA90681AE]

GO
ALTER TABLE [dbo].[tbl_ConfigNoa]
	WITH CHECK
	ADD CONSTRAINT [noaProcurementMethodId_FK2]
	FOREIGN KEY ([procurementMethodId]) REFERENCES [dbo].[tbl_ProcurementMethod] ([procurementMethodId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ConfigNoa]
	CHECK CONSTRAINT [noaProcurementMethodId_FK2]

GO
ALTER TABLE [dbo].[tbl_ConfigNoa]
	WITH CHECK
	ADD CONSTRAINT [noaProcurementNatureId_FK1]
	FOREIGN KEY ([procurementNatureId]) REFERENCES [dbo].[tbl_ProcurementNature] ([procurementNatureId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ConfigNoa]
	CHECK CONSTRAINT [noaProcurementNatureId_FK1]

GO
ALTER TABLE [dbo].[tbl_ConfigNoa]
	WITH CHECK
	ADD CONSTRAINT [noaProcurementTypeId_FK3]
	FOREIGN KEY ([procurementTypeId]) REFERENCES [dbo].[tbl_ProcurementTypes] ([procurementTypeId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ConfigNoa]
	CHECK CONSTRAINT [noaProcurementTypeId_FK3]

GO
ALTER TABLE [dbo].[tbl_ConfigNoa] SET (LOCK_ESCALATION = TABLE)
GO
