SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_WorkFlowFileHistory] (
		[wfHistoryId]           [int] IDENTITY(1, 1) NOT NULL,
		[wfFileOnHandId]        [int] NOT NULL,
		[moduleName]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[eventName]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[activityName]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[activityId]            [smallint] NOT NULL,
		[childId]               [int] NOT NULL,
		[objectId]              [int] NOT NULL,
		[processDate]           [datetime] NOT NULL,
		[endDate]               [smalldatetime] NOT NULL,
		[fromUserId]            [int] NOT NULL,
		[toUserId]              [int] NOT NULL,
		[fileSentFrom]          [varchar](400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[fileSentTo]            [varchar](400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[wfRoleid]              [tinyint] NOT NULL,
		[fileOnHand]            [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[action]                [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[workflowDirection]     [varchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[remarks]               [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[wfTrigger]             [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[documentId]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [wfHistoryId_PK]
		PRIMARY KEY
		CLUSTERED
		([wfHistoryId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_WorkFlowFileHistory] SET (LOCK_ESCALATION = TABLE)
GO
