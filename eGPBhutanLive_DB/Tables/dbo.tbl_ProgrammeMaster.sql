SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ProgrammeMaster] (
		[progId]        [int] IDENTITY(1, 1) NOT NULL,
		[progCode]      [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[progName]      [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[progOwner]     [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_ProgrammeMaster]
		PRIMARY KEY
		CLUSTERED
		([progId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ProgrammeMaster] SET (LOCK_ESCALATION = TABLE)
GO
