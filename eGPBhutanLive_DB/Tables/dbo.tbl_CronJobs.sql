SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CronJobs] (
		[cronJobId]            [int] IDENTITY(1, 1) NOT NULL,
		[cronJobName]          [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cronJobStartTime]     [datetime] NOT NULL,
		[cronJobEndTime]       [datetime] NULL,
		[cronJobStatus]        [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cronJobMsg]           [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_CronJobs]
		PRIMARY KEY
		CLUSTERED
		([cronJobId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CronJobs] SET (LOCK_ESCALATION = TABLE)
GO
