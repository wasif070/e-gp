SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ProcurementMethod] (
		[procurementMethodId]     [tinyint] NOT NULL,
		[procurementMethod]       [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[procurementFullName]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [procurementMethodId_PK]
		PRIMARY KEY
		CLUSTERED
		([procurementMethodId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ProcurementMethod] SET (LOCK_ESCALATION = TABLE)
GO
