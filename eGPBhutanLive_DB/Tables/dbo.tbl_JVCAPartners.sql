SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_JVCAPartners] (
		[JVPartnerId]            [int] IDENTITY(1, 1) NOT NULL,
		[JVId]                   [int] NOT NULL,
		[userId]                 [int] NOT NULL,
		[companyId]              [int] NOT NULL,
		[jvRole]                 [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sentRequestDt]          [datetime] NOT NULL,
		[isNominatedPartner]     [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[JvAcceptRejStatus]      [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[JvAccpetDatetime]       [datetime] NULL,
		CONSTRAINT [PK_tbl_JVCAPartners]
		PRIMARY KEY
		CLUSTERED
		([JVPartnerId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_JVCAPartners]
	ADD
	CONSTRAINT [DF_tbl_JVCAPartners_isNominatedPartner]
	DEFAULT ('No') FOR [isNominatedPartner]
GO
ALTER TABLE [dbo].[tbl_JVCAPartners]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_JVCAPartners_tbl_JointVenture]
	FOREIGN KEY ([JVId]) REFERENCES [dbo].[tbl_JointVenture] ([JVId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_JVCAPartners]
	CHECK CONSTRAINT [FK_tbl_JVCAPartners_tbl_JointVenture]

GO
ALTER TABLE [dbo].[tbl_JVCAPartners]
	WITH CHECK
	ADD CONSTRAINT [FK85C77456E68F9190]
	FOREIGN KEY ([JVId]) REFERENCES [dbo].[tbl_JointVenture] ([JVId])
ALTER TABLE [dbo].[tbl_JVCAPartners]
	CHECK CONSTRAINT [FK85C77456E68F9190]

GO
ALTER TABLE [dbo].[tbl_JVCAPartners] SET (LOCK_ESCALATION = TABLE)
GO
