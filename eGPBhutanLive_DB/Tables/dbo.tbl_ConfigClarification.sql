SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_ConfigClarification] (
		[configClarificationId]     [int] IDENTITY(1, 1) NOT NULL,
		[templateId]                [smallint] NOT NULL,
		[clarificationDays]         [smallint] NOT NULL,
		CONSTRAINT [PK_tbl_ConfigClarification]
		PRIMARY KEY
		CLUSTERED
		([configClarificationId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ConfigClarification]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_ConfigClarification_tbl_TemplateMaster]
	FOREIGN KEY ([templateId]) REFERENCES [dbo].[tbl_TemplateMaster] ([templateId])
ALTER TABLE [dbo].[tbl_ConfigClarification]
	CHECK CONSTRAINT [FK_tbl_ConfigClarification_tbl_TemplateMaster]

GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tbl_ConfigClarification]
	ON [dbo].[tbl_ConfigClarification] ([templateId], [clarificationDays])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ConfigClarification] SET (LOCK_ESCALATION = TABLE)
GO
