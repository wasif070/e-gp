SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderPayment] (
		[tenderPaymentId]     [int] IDENTITY(1, 1) NOT NULL,
		[paymentFor]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[paymentInstType]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[instRefNumber]       [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[amount]              [money] NOT NULL,
		[instDate]            [smalldatetime] NOT NULL,
		[instValidUpto]       [smalldatetime] NOT NULL,
		[bankName]            [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[branchName]          [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[comments]            [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderId]            [int] NOT NULL,
		[userId]              [int] NOT NULL,
		[createdBy]           [int] NOT NULL,
		[createdDate]         [smalldatetime] NOT NULL,
		[pkgLotId]            [int] NOT NULL,
		[eSignature]          [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[status]              [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[paymentMode]         [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[extValidityRef]      [int] NULL,
		[currency]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[issuanceBank]        [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[issuanceBranch]      [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[isVerified]          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isLive]              [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[dtOfAction]          [smalldatetime] NULL,
		[partTransId]         [int] NULL,
		[OnlineTransId]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_TenderPayment]
		PRIMARY KEY
		CLUSTERED
		([tenderPaymentId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderPayment]
	ADD
	CONSTRAINT [DF_tbl_TenderPayment_isLive]
	DEFAULT ('Yes') FOR [isLive]
GO
ALTER TABLE [dbo].[tbl_TenderPayment]
	ADD
	CONSTRAINT [DF_tbl_TenderPayment_isVerified]
	DEFAULT ('No') FOR [isVerified]
GO
ALTER TABLE [dbo].[tbl_TenderPayment]
	ADD
	CONSTRAINT [DF_tbl_TenderPayment_status]
	DEFAULT ('paid') FOR [status]
GO
ALTER TABLE [dbo].[tbl_TenderPayment]
	WITH NOCHECK
	ADD CONSTRAINT [FK49521DB198B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_TenderPayment]
	CHECK CONSTRAINT [FK49521DB198B66EC5]

GO
ALTER TABLE [dbo].[tbl_TenderPayment]
	WITH NOCHECK
	ADD CONSTRAINT [paymentTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderPayment]
	CHECK CONSTRAINT [paymentTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderPayment] SET (LOCK_ESCALATION = TABLE)
GO
