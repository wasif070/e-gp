SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ConfigPaThreshold] (
		[configPAThresholdId]     [int] IDENTITY(1, 1) NOT NULL,
		[area]                    [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[procurementNatureId]     [tinyint] NOT NULL,
		[maxValue]                [numeric](19, 2) NOT NULL,
		[minValue]                [numeric](19, 2) NOT NULL,
		CONSTRAINT [PK_tbl_ConfigPAThreshold]
		PRIMARY KEY
		CLUSTERED
		([configPAThresholdId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ConfigPaThreshold]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_ConfigPAThreshold_tbl_ProcurementNature]
	FOREIGN KEY ([procurementNatureId]) REFERENCES [dbo].[tbl_ProcurementNature] ([procurementNatureId])
ALTER TABLE [dbo].[tbl_ConfigPaThreshold]
	CHECK CONSTRAINT [FK_tbl_ConfigPAThreshold_tbl_ProcurementNature]

GO
ALTER TABLE [dbo].[tbl_ConfigPaThreshold] SET (LOCK_ESCALATION = TABLE)
GO
