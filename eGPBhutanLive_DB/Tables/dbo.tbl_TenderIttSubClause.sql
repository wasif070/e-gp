SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderIttSubClause] (
		[tenderIttSubClauseId]       [int] IDENTITY(1, 1) NOT NULL,
		[tenderIttClauseId]          [int] NOT NULL,
		[ittSubClauseName]           [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isTdsApplicable]            [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[templateIttSubClauseId]     [int] NOT NULL,
		CONSTRAINT [PK_tbl_TenderIttSubClause]
		PRIMARY KEY
		CLUSTERED
		([tenderIttSubClauseId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderIttSubClause]
	WITH CHECK
	ADD CONSTRAINT [FK1D07645BD7993CA3]
	FOREIGN KEY ([tenderIttClauseId]) REFERENCES [dbo].[tbl_TenderIttClause] ([tenderIttClauseId])
ALTER TABLE [dbo].[tbl_TenderIttSubClause]
	CHECK CONSTRAINT [FK1D07645BD7993CA3]

GO
ALTER TABLE [dbo].[tbl_TenderIttSubClause]
	WITH CHECK
	ADD CONSTRAINT [tenderIttClauseId_FK1]
	FOREIGN KEY ([tenderIttClauseId]) REFERENCES [dbo].[tbl_TenderIttClause] ([tenderIttClauseId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderIttSubClause]
	CHECK CONSTRAINT [tenderIttClauseId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderIttSubClause] SET (LOCK_ESCALATION = TABLE)
GO
