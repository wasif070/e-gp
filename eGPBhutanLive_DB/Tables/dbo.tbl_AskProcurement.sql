SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_AskProcurement] (
		[procQueId]          [int] IDENTITY(1, 1) NOT NULL,
		[question]           [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[postedDate]         [smalldatetime] NOT NULL,
		[postedBy]           [int] NOT NULL,
		[answer]             [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[answerDate]         [smalldatetime] NULL,
		[answeredBy]         [int] NULL,
		[queCatId]           [int] NOT NULL,
		[postedByName]       [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[answeredByName]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[userTypeId]         [int] NULL,
		CONSTRAINT [PK_tbl_AskProcurement]
		PRIMARY KEY
		CLUSTERED
		([procQueId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_AskProcurement]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_AskProcurement_tbl_QuestionCategory]
	FOREIGN KEY ([queCatId]) REFERENCES [dbo].[tbl_QuestionCategory] ([queCatId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_AskProcurement]
	CHECK CONSTRAINT [FK_tbl_AskProcurement_tbl_QuestionCategory]

GO
ALTER TABLE [dbo].[tbl_AskProcurement]
	WITH CHECK
	ADD CONSTRAINT [FKE5F45538A46FD9D4]
	FOREIGN KEY ([queCatId]) REFERENCES [dbo].[tbl_QuestionCategory] ([queCatId])
ALTER TABLE [dbo].[tbl_AskProcurement]
	CHECK CONSTRAINT [FKE5F45538A46FD9D4]

GO
ALTER TABLE [dbo].[tbl_AskProcurement] SET (LOCK_ESCALATION = TABLE)
GO
