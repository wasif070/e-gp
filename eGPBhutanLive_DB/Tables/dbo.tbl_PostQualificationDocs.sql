SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_PostQualificationDocs] (
		[postQualDocId]      [int] IDENTITY(1, 1) NOT NULL,
		[postQualId]         [int] NOT NULL,
		[documentName]       [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]       [smalldatetime] NOT NULL,
		[uploadedBy]         [int] NOT NULL,
		CONSTRAINT [PK_tbl_PostQualification]
		PRIMARY KEY
		CLUSTERED
		([postQualDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_PostQualificationDocs]
	WITH CHECK
	ADD CONSTRAINT [postQual_FK1]
	FOREIGN KEY ([postQualId]) REFERENCES [dbo].[tbl_PostQualification] ([postQaulId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_PostQualificationDocs]
	CHECK CONSTRAINT [postQual_FK1]

GO
ALTER TABLE [dbo].[tbl_PostQualificationDocs] SET (LOCK_ESCALATION = TABLE)
GO
