SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_TenderCoriGrandSumDetail] (
		[sumDtlId]          [int] IDENTITY(1, 1) NOT NULL,
		[tenderSumId]       [int] NOT NULL,
		[tenderFormId]      [int] NOT NULL,
		[cellId]            [int] NOT NULL,
		[tenderTableId]     [int] NOT NULL,
		[corrigendumId]     [int] NOT NULL,
		CONSTRAINT [PK_tbl_TenderCoriGrandSumDetail]
		PRIMARY KEY
		CLUSTERED
		([sumDtlId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderCoriGrandSumDetail]
	WITH CHECK
	ADD CONSTRAINT [FK_CoriGrandSumDTL_TenderGrandSum]
	FOREIGN KEY ([tenderSumId]) REFERENCES [dbo].[tbl_TenderGrandSum] ([tenderSumId])
ALTER TABLE [dbo].[tbl_TenderCoriGrandSumDetail]
	CHECK CONSTRAINT [FK_CoriGrandSumDTL_TenderGrandSum]

GO
ALTER TABLE [dbo].[tbl_TenderCoriGrandSumDetail] SET (LOCK_ESCALATION = TABLE)
GO
