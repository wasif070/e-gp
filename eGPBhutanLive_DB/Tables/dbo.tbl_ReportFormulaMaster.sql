SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ReportFormulaMaster] (
		[reportFormulaId]     [int] IDENTITY(1, 1) NOT NULL,
		[reportTableId]       [int] NOT NULL,
		[formula]             [varchar](520) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[columnId]            [int] NOT NULL,
		[displayFormula]      [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [reportFormulaId_pk]
		PRIMARY KEY
		CLUSTERED
		([reportFormulaId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ReportFormulaMaster]
	WITH CHECK
	ADD CONSTRAINT [FK4F92FC738BD8F631]
	FOREIGN KEY ([reportTableId]) REFERENCES [dbo].[tbl_ReportTableMaster] ([reportTableId])
ALTER TABLE [dbo].[tbl_ReportFormulaMaster]
	CHECK CONSTRAINT [FK4F92FC738BD8F631]

GO
ALTER TABLE [dbo].[tbl_ReportFormulaMaster]
	WITH CHECK
	ADD CONSTRAINT [formulaRptTableId_FK1]
	FOREIGN KEY ([reportTableId]) REFERENCES [dbo].[tbl_ReportTableMaster] ([reportTableId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ReportFormulaMaster]
	CHECK CONSTRAINT [formulaRptTableId_FK1]

GO
ALTER TABLE [dbo].[tbl_ReportFormulaMaster] SET (LOCK_ESCALATION = TABLE)
GO
