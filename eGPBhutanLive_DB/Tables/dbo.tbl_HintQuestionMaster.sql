SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_HintQuestionMaster] (
		[hintQuestionId]     [tinyint] IDENTITY(1, 1) NOT NULL,
		[hintQuestion]       [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [hintQuestionId_PK]
		PRIMARY KEY
		CLUSTERED
		([hintQuestionId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Secret question stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_HintQuestionMaster', 'COLUMN', N'hintQuestion'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Unique id of secret question stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_HintQuestionMaster', 'COLUMN', N'hintQuestionId'
GO
ALTER TABLE [dbo].[tbl_HintQuestionMaster] SET (LOCK_ESCALATION = TABLE)
GO
