SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ActivityMaster] (
		[activityId]       [smallint] IDENTITY(1, 1) NOT NULL,
		[activityName]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[eventId]          [tinyint] NOT NULL,
		CONSTRAINT [activityId_PK]
		PRIMARY KEY
		CLUSTERED
		([activityId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ActivityMaster] SET (LOCK_ESCALATION = TABLE)
GO
