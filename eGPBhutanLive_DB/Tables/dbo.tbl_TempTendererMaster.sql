SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TempTendererMaster] (
		[tendererId]           [int] IDENTITY(1, 1) NOT NULL,
		[userId]               [int] NOT NULL,
		[companyId]            [int] NOT NULL,
		[isAdmin]              [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[title]                [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[firstName]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[middleName]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[lasatName]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[fullNameInBangla]     [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[nationalIdNo]         [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[address1]             [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[address2]             [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[country]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[state]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[city]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[upJilla]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[postcode]             [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[phoneNo]              [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[mobileNo]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[faxNo]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tinNo]                [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tinDocName]           [varchar](130) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[comments]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[specialization]       [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[website]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[designation]          [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[department]           [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[nationality]          [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[emailAddress]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[subDistrict]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OtherTitle]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UserType]             [int] NULL,
		CONSTRAINT [temp_tendererId_PK]
		PRIMARY KEY
		CLUSTERED
		([tendererId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_address1]
	DEFAULT ('N/A') FOR [address1]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_address2]
	DEFAULT ('N/A') FOR [address2]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_city]
	DEFAULT ('N/A') FOR [city]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_comments]
	DEFAULT ('N/A') FOR [comments]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_companyId]
	DEFAULT ((0)) FOR [companyId]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_country]
	DEFAULT ((0)) FOR [country]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_department]
	DEFAULT ('N/A') FOR [department]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_designation]
	DEFAULT ('N/A') FOR [designation]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_faxNo]
	DEFAULT ('N/A') FOR [faxNo]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_firstName]
	DEFAULT ('N/A') FOR [firstName]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_fullNameInBangla]
	DEFAULT (N'N/A') FOR [fullNameInBangla]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_isAdmin]
	DEFAULT ('N/A') FOR [isAdmin]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_lasatName]
	DEFAULT ('N/A') FOR [lasatName]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_mobileNo]
	DEFAULT ('N/A') FOR [mobileNo]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_moddleName]
	DEFAULT ('N/A') FOR [middleName]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_nationalIdNo]
	DEFAULT ('N/A') FOR [nationalIdNo]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_phoneNo]
	DEFAULT ('N/A') FOR [phoneNo]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_postcode]
	DEFAULT ('N/A') FOR [postcode]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_specialization]
	DEFAULT ('N/A') FOR [specialization]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_state]
	DEFAULT ((0)) FOR [state]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_tinDocName]
	DEFAULT ('N/A') FOR [tinDocName]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_tinNo]
	DEFAULT ('N/A') FOR [tinNo]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_title]
	DEFAULT ('N/A') FOR [title]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_upJilla]
	DEFAULT ('N/A') FOR [upJilla]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_userId]
	DEFAULT ((0)) FOR [userId]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TempTendererMaster_website]
	DEFAULT ('N/A') FOR [website]
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	WITH CHECK
	ADD CONSTRAINT [FKA888D7786174DBD1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	CHECK CONSTRAINT [FKA888D7786174DBD1]

GO
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	WITH CHECK
	ADD CONSTRAINT [FKA888D778E991DAA3]
	FOREIGN KEY ([companyId]) REFERENCES [dbo].[tbl_TempCompanyMaster] ([companyId])
ALTER TABLE [dbo].[tbl_TempTendererMaster]
	CHECK CONSTRAINT [FKA888D778E991DAA3]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Address information of company''s registered  user stores  here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'address1'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Alternate address information of company''s registered user stores here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'address2'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Location of registering user stores  here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'city'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Registering user remarks information stores  here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'comments'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Refer tbl_CompanyMaster for temp table , to identify in which company user belongs', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'companyId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Refer tbl_CountryMaster, Id of country stores  here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'country'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Company user department  information stores here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'department'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Company user designation information stores here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'designation'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Fax no of registering user stores  here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'faxNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'First name of an registering user stores here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'firstName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Full name in bangla information stores here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'fullNameInBangla'
GO
EXEC sp_addextendedproperty N'MS_Description', N'To identify in user is company admin or not this information stores here for temp table ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'isAdmin'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Lastname of registering user stores here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'lasatName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Middlename of registering user stores here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'middleName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mobile no or registering user information stores  here for temp table ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'mobileNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'National Id information stores here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'nationalIdNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Phone number of registering user stores  here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'phoneNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'postal code of registering user stores  here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'postcode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Registering user business category information stores here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'specialization'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Refer tbl_StateMaster,Id of stste stores here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'state'
GO
EXEC sp_addextendedproperty N'MS_Description', N' System generated id information stores here for temp table ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'tendererId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Tin document name information stores  here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'tinDocName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Tin no or registering user information stores  here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'tinNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Title of user stores here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'title'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Upjilla information stores here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'upJilla'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Refer tbl_LoginMaster for temp table  ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'userId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'registering user website  information stores here for temp table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererMaster', 'COLUMN', N'website'
GO
ALTER TABLE [dbo].[tbl_TempTendererMaster] SET (LOCK_ESCALATION = TABLE)
GO
