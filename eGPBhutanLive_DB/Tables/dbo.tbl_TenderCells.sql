SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderCells] (
		[tenderCelId]          [int] IDENTITY(1, 1) NOT NULL,
		[tenderTableId]        [int] NOT NULL,
		[rowId]                [int] NOT NULL,
		[cellDatatype]         [tinyint] NOT NULL,
		[cellvalue]            [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[columnId]             [smallint] NOT NULL,
		[templateTableId]      [int] NOT NULL,
		[templateColumnId]     [smallint] NOT NULL,
		[cellId]               [int] NOT NULL,
		[colId]                [smallint] NULL,
		[tenderColId]          [int] NULL,
		[showOrHide]           [tinyint] NULL,
		CONSTRAINT [PK_tbl_TenderCells]
		PRIMARY KEY
		CLUSTERED
		([tenderCelId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderCells]
	WITH CHECK
	ADD CONSTRAINT [FKD3257DBCDA7D6BEE]
	FOREIGN KEY ([tenderTableId]) REFERENCES [dbo].[tbl_TenderTables] ([tenderTableId])
ALTER TABLE [dbo].[tbl_TenderCells]
	CHECK CONSTRAINT [FKD3257DBCDA7D6BEE]

GO
ALTER TABLE [dbo].[tbl_TenderCells]
	WITH CHECK
	ADD CONSTRAINT [tenderTableCell_FK1]
	FOREIGN KEY ([tenderTableId]) REFERENCES [dbo].[tbl_TenderTables] ([tenderTableId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderCells]
	CHECK CONSTRAINT [tenderTableCell_FK1]

GO
CREATE NONCLUSTERED INDEX [IX_tbl_TenderCellsIncludeTenderTableId]
	ON [dbo].[tbl_TenderCells] ([tenderTableId])
	INCLUDE ([columnId], [templateTableId], [cellId])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tbl_TenderCellstemplateTableId]
	ON [dbo].[tbl_TenderCells] ([templateTableId])
	INCLUDE ([tenderTableId], [columnId], [cellId])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderCells] SET (LOCK_ESCALATION = TABLE)
GO
