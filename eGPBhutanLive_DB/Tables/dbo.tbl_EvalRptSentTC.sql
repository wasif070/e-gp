SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalRptSentTC] (
		[evalRptTCId]      [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]         [int] NOT NULL,
		[pkgLotId]         [int] NOT NULL,
		[roundId]          [int] NOT NULL,
		[sentByUserId]     [int] NOT NULL,
		[sentToUserId]     [int] NOT NULL,
		[sentDate]         [smalldatetime] NOT NULL,
		[remarks]          [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[rptStatus]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[evalCount]        [tinyint] NOT NULL,
		CONSTRAINT [PK_tbl_evalRptSentTC]
		PRIMARY KEY
		CLUSTERED
		([evalRptTCId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalRptSentTC]
	ADD
	CONSTRAINT [DF_tbl_EvalRptSentTC_evalCount]
	DEFAULT ((0)) FOR [evalCount]
GO
ALTER TABLE [dbo].[tbl_EvalRptSentTC] SET (LOCK_ESCALATION = TABLE)
GO
