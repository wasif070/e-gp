SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_PostQualification] (
		[postQaulId]                 [int] IDENTITY(1, 1) NOT NULL,
		[userId]                     [int] NOT NULL,
		[tenderId]                   [int] NOT NULL,
		[siteVisit]                  [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[siteVisitReqDt]             [smalldatetime] NOT NULL,
		[comments]                   [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[entryDate]                  [smalldatetime] NOT NULL,
		[createdBy]                  [int] NOT NULL,
		[postQualStatus]             [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[siteVisitDate]              [smalldatetime] NULL,
		[siteVisitStatus]            [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[siteVisitComments]          [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[pkgLotId]                   [int] NOT NULL,
		[rank]                       [int] NOT NULL,
		[noaStatus]                  [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[reTenderRecommendetion]     [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[evalCount]                  [int] NOT NULL,
		CONSTRAINT [PK_tbl_PostQualification_1]
		PRIMARY KEY
		CLUSTERED
		([postQaulId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_PostQualification]
	ADD
	CONSTRAINT [DF__tbl_PostQ__evalC__58F12BAE]
	DEFAULT ('0') FOR [evalCount]
GO
ALTER TABLE [dbo].[tbl_PostQualification]
	ADD
	CONSTRAINT [DF_tbl_PostQualification_noaStatus]
	DEFAULT ('Pending') FOR [noaStatus]
GO
ALTER TABLE [dbo].[tbl_PostQualification]
	ADD
	CONSTRAINT [DF_tbl_PostQualification_pkgLotId]
	DEFAULT ((0)) FOR [pkgLotId]
GO
ALTER TABLE [dbo].[tbl_PostQualification]
	ADD
	CONSTRAINT [DF_tbl_PostQualification_postQualStatus]
	DEFAULT ('Pending') FOR [postQualStatus]
GO
ALTER TABLE [dbo].[tbl_PostQualification]
	ADD
	CONSTRAINT [DF_tbl_PostQualification_rank]
	DEFAULT ((1)) FOR [rank]
GO
ALTER TABLE [dbo].[tbl_PostQualification]
	ADD
	CONSTRAINT [DF_tbl_PostQualification_siteVisitReqDt]
	DEFAULT (getdate()) FOR [siteVisitReqDt]
GO
ALTER TABLE [dbo].[tbl_PostQualification]
	ADD
	CONSTRAINT [DF_tbl_PostQualification_siteVisitStatus]
	DEFAULT ('-') FOR [siteVisitStatus]
GO
ALTER TABLE [dbo].[tbl_PostQualification]
	WITH NOCHECK
	ADD CONSTRAINT [postQualTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_PostQualification]
	CHECK CONSTRAINT [postQualTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_PostQualification] SET (LOCK_ESCALATION = TABLE)
GO
