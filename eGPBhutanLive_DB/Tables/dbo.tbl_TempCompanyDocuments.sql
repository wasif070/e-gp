SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TempCompanyDocuments] (
		[tendererId]         [int] NOT NULL,
		[companyDocId]       [int] IDENTITY(1, 1) NOT NULL,
		[documentName]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[documentSize]       [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[documentBrief]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]       [smalldatetime] NOT NULL,
		[documentType]       [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docHash]            [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DocRefNo]           [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DocDescription]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [temp_companyDocId_PK]
		PRIMARY KEY
		CLUSTERED
		([companyDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TempCompanyDocuments]
	WITH CHECK
	ADD CONSTRAINT [FKB5ACC4B0ED53FE13]
	FOREIGN KEY ([tendererId]) REFERENCES [dbo].[tbl_TempTendererMaster] ([tendererId])
ALTER TABLE [dbo].[tbl_TempCompanyDocuments]
	CHECK CONSTRAINT [FKB5ACC4B0ED53FE13]

GO
ALTER TABLE [dbo].[tbl_TempCompanyDocuments]
	WITH CHECK
	ADD CONSTRAINT [tendererDoc_FK2]
	FOREIGN KEY ([tendererId]) REFERENCES [dbo].[tbl_TempTendererMaster] ([tendererId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TempCompanyDocuments]
	CHECK CONSTRAINT [tendererDoc_FK2]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Unique id of document stores here , its system generated id stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempCompanyDocuments', 'COLUMN', N'companyDocId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Document brief information stores here it will explain brief information about company’s document', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempCompanyDocuments', 'COLUMN', N'documentBrief'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name of document stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempCompanyDocuments', 'COLUMN', N'documentName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Size of document stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempCompanyDocuments', 'COLUMN', N'documentSize'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Unique Id of eGP System stores here ,  refers tbl_TendererMaster', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempCompanyDocuments', 'COLUMN', N'tendererId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date of uploaded document stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempCompanyDocuments', 'COLUMN', N'uploadedDate'
GO
ALTER TABLE [dbo].[tbl_TempCompanyDocuments] SET (LOCK_ESCALATION = TABLE)
GO
