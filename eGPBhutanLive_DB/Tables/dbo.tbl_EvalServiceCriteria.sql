SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalServiceCriteria] (
		[evalSerCriteId]     [int] IDENTITY(1, 1) NOT NULL,
		[criteriaId]         [int] NOT NULL,
		[mainCriteria]       [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[subCriteriaId]      [int] NOT NULL,
		[subCriteria]        [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [evalSerCrietId_PK]
		PRIMARY KEY
		CLUSTERED
		([evalSerCriteId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalServiceCriteria] SET (LOCK_ESCALATION = TABLE)
GO
