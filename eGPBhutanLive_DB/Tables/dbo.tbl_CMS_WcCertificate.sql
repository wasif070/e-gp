SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_WcCertificate] (
		[wcCertId]                   [int] IDENTITY(1, 1) NOT NULL,
		[actualDateOfCompletion]     [datetime] NOT NULL,
		[isWorkComplete]             [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[remarks]                    [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdDate]                [datetime] NOT NULL,
		[createdBy]                  [int] NOT NULL,
		[contractId]                 [int] NOT NULL,
		[userId]                     [int] NOT NULL,
		[vendorRating]               [int] NOT NULL,
		[sendToTenderer]             [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[pgStatus]                   [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[newPGRequire]               [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[newPGAmt]                   [decimal](15, 3) NULL,
		CONSTRAINT [wcCertId_PK]
		PRIMARY KEY
		CLUSTERED
		([wcCertId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_WcCertificate]
	WITH CHECK
	ADD CONSTRAINT [cmsWCCertificateContractId_FK1]
	FOREIGN KEY ([contractId]) REFERENCES [dbo].[tbl_ContractSign] ([contractSignId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_WcCertificate]
	CHECK CONSTRAINT [cmsWCCertificateContractId_FK1]

GO
ALTER TABLE [dbo].[tbl_CMS_WcCertificate] SET (LOCK_ESCALATION = TABLE)
GO
