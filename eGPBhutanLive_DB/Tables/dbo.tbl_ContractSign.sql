SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ContractSign] (
		[contractSignId]           [int] IDENTITY(1, 1) NOT NULL,
		[lastDtContractDt]         [smalldatetime] NOT NULL,
		[contractSignDt]           [smalldatetime] NOT NULL,
		[contractSignLocation]     [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isPubAggOnWeb]            [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdDt]                [smalldatetime] NOT NULL,
		[createdBy]                [int] NOT NULL,
		[witnessInfo]              [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[noaId]                    [int] NOT NULL,
		[paymentTerms]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[isRepeatOrder]            [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_ContractSign]
		PRIMARY KEY
		CLUSTERED
		([contractSignId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ContractSign]
	ADD
	CONSTRAINT [DF_tbl_ContractSign_isPubAggOnWeb]
	DEFAULT ('No') FOR [isPubAggOnWeb]
GO
ALTER TABLE [dbo].[tbl_ContractSign] SET (LOCK_ESCALATION = TABLE)
GO
