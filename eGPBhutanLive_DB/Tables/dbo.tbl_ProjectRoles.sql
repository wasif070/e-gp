SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_ProjectRoles] (
		[projRoleId]            [int] IDENTITY(1, 1) NOT NULL,
		[projectId]             [int] NOT NULL,
		[procurementRoleId]     [tinyint] NOT NULL,
		[userId]                [int] NOT NULL,
		CONSTRAINT [PK_tbl_ProjectRoles]
		PRIMARY KEY
		CLUSTERED
		([projRoleId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ProjectRoles]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_ProjectRoles_tbl_LoginMaster]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ProjectRoles]
	CHECK CONSTRAINT [FK_tbl_ProjectRoles_tbl_LoginMaster]

GO
ALTER TABLE [dbo].[tbl_ProjectRoles]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_ProjectRoles_tbl_ProcurementRole]
	FOREIGN KEY ([procurementRoleId]) REFERENCES [dbo].[tbl_ProcurementRole] ([procurementRoleId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ProjectRoles]
	CHECK CONSTRAINT [FK_tbl_ProjectRoles_tbl_ProcurementRole]

GO
ALTER TABLE [dbo].[tbl_ProjectRoles]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_ProjectRoles_tbl_ProjectMaster]
	FOREIGN KEY ([projectId]) REFERENCES [dbo].[tbl_ProjectMaster] ([projectId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ProjectRoles]
	CHECK CONSTRAINT [FK_tbl_ProjectRoles_tbl_ProjectMaster]

GO
ALTER TABLE [dbo].[tbl_ProjectRoles]
	WITH CHECK
	ADD CONSTRAINT [FKA95A5E256174DBD1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_ProjectRoles]
	CHECK CONSTRAINT [FKA95A5E256174DBD1]

GO
ALTER TABLE [dbo].[tbl_ProjectRoles]
	WITH CHECK
	ADD CONSTRAINT [FKA95A5E2580EAD74F]
	FOREIGN KEY ([projectId]) REFERENCES [dbo].[tbl_ProjectMaster] ([projectId])
ALTER TABLE [dbo].[tbl_ProjectRoles]
	CHECK CONSTRAINT [FKA95A5E2580EAD74F]

GO
ALTER TABLE [dbo].[tbl_ProjectRoles]
	WITH CHECK
	ADD CONSTRAINT [FKA95A5E25C90CE487]
	FOREIGN KEY ([procurementRoleId]) REFERENCES [dbo].[tbl_ProcurementRole] ([procurementRoleId])
ALTER TABLE [dbo].[tbl_ProjectRoles]
	CHECK CONSTRAINT [FKA95A5E25C90CE487]

GO
ALTER TABLE [dbo].[tbl_ProjectRoles] SET (LOCK_ESCALATION = TABLE)
GO
