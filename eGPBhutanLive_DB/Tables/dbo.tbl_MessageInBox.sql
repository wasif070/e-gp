SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_MessageInBox] (
		[msgBoxId]          [int] IDENTITY(1, 1) NOT NULL,
		[msgToUserId]       [int] NOT NULL,
		[msgFromUserId]     [int] NOT NULL,
		[msgId]             [int] NOT NULL,
		[msgToCCReply]      [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[msgStatus]         [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[creationDate]      [datetime] NOT NULL,
		[folderId]          [int] NOT NULL,
		[isPriority]        [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isMsgRead]         [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isDocUploaded]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[processUrl]        [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [msgBoxId_PK]
		PRIMARY KEY
		CLUSTERED
		([msgBoxId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_MessageInBox]
	WITH CHECK
	ADD CONSTRAINT [FK2431A9A02D37788D]
	FOREIGN KEY ([msgToUserId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_MessageInBox]
	CHECK CONSTRAINT [FK2431A9A02D37788D]

GO
ALTER TABLE [dbo].[tbl_MessageInBox]
	WITH CHECK
	ADD CONSTRAINT [FK2431A9A03876063C]
	FOREIGN KEY ([msgFromUserId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_MessageInBox]
	CHECK CONSTRAINT [FK2431A9A03876063C]

GO
ALTER TABLE [dbo].[tbl_MessageInBox]
	WITH CHECK
	ADD CONSTRAINT [FK2431A9A0A922AEE3]
	FOREIGN KEY ([msgId]) REFERENCES [dbo].[tbl_Message] ([msgId])
ALTER TABLE [dbo].[tbl_MessageInBox]
	CHECK CONSTRAINT [FK2431A9A0A922AEE3]

GO
ALTER TABLE [dbo].[tbl_MessageInBox]
	WITH CHECK
	ADD CONSTRAINT [msgFromUserId_FK3]
	FOREIGN KEY ([msgFromUserId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_MessageInBox]
	CHECK CONSTRAINT [msgFromUserId_FK3]

GO
ALTER TABLE [dbo].[tbl_MessageInBox]
	WITH CHECK
	ADD CONSTRAINT [msgId_FK1]
	FOREIGN KEY ([msgId]) REFERENCES [dbo].[tbl_Message] ([msgId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_MessageInBox]
	CHECK CONSTRAINT [msgId_FK1]

GO
ALTER TABLE [dbo].[tbl_MessageInBox] SET (LOCK_ESCALATION = TABLE)
GO
