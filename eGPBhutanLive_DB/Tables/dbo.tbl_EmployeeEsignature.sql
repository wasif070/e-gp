SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EmployeeEsignature] (
		[empEsignatureId]     [int] NOT NULL,
		[documentName]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[documentSize]        [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[documentBrief]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]        [smalldatetime] NOT NULL,
		[employeeId]          [int] NOT NULL,
		CONSTRAINT [empEsignatureId_PK]
		PRIMARY KEY
		CLUSTERED
		([empEsignatureId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EmployeeEsignature]
	WITH NOCHECK
	ADD CONSTRAINT [employeeIdEsignature_FK1]
	FOREIGN KEY ([employeeId]) REFERENCES [dbo].[tbl_EmployeeMaster] ([employeeId])
ALTER TABLE [dbo].[tbl_EmployeeEsignature]
	CHECK CONSTRAINT [employeeIdEsignature_FK1]

GO
ALTER TABLE [dbo].[tbl_EmployeeEsignature]
	WITH NOCHECK
	ADD CONSTRAINT [FK47E0F982F5489C79]
	FOREIGN KEY ([employeeId]) REFERENCES [dbo].[tbl_EmployeeMaster] ([employeeId])
ALTER TABLE [dbo].[tbl_EmployeeEsignature]
	CHECK CONSTRAINT [FK47E0F982F5489C79]

GO
ALTER TABLE [dbo].[tbl_EmployeeEsignature] SET (LOCK_ESCALATION = TABLE)
GO
