SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_WcCertDoc] (
		[wcCertDocId]        [int] IDENTITY(1, 1) NOT NULL,
		[wcCertId]           [int] NOT NULL,
		[documentName]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]     [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedBy]         [int] NOT NULL,
		[uploadedDate]       [datetime] NOT NULL,
		[wcCertiHistId]      [int] NULL,
		CONSTRAINT [wcCertDocId_PK]
		PRIMARY KEY
		CLUSTERED
		([wcCertDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_WcCertDoc]
	WITH CHECK
	ADD CONSTRAINT [cmsWcCertificateDocWcCertId_FK1]
	FOREIGN KEY ([wcCertId]) REFERENCES [dbo].[tbl_CMS_WcCertificate] ([wcCertId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_WcCertDoc]
	CHECK CONSTRAINT [cmsWcCertificateDocWcCertId_FK1]

GO
ALTER TABLE [dbo].[tbl_CMS_WcCertDoc] SET (LOCK_ESCALATION = TABLE)
GO
