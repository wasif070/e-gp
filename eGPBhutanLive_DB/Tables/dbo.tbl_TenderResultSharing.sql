SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderResultSharing] (
		[resultShareId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]          [int] NOT NULL,
		[formId]            [int] NOT NULL,
		[shareDate]         [smalldatetime] NOT NULL,
		[sharedBy]          [int] NOT NULL,
		[reportType]        [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_TenderResultSharing]
		PRIMARY KEY
		CLUSTERED
		([resultShareId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderResultSharing]
	WITH NOCHECK
	ADD CONSTRAINT [resultSharingTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderResultSharing]
	CHECK CONSTRAINT [resultSharingTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderResultSharing] SET (LOCK_ESCALATION = TABLE)
GO
