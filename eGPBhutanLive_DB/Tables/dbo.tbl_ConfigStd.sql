SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ConfigStd] (
		[configStdId]             [int] IDENTITY(1, 1) NOT NULL,
		[operator]                [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderValue]             [numeric](19, 2) NOT NULL,
		[procurementMethodId]     [tinyint] NOT NULL,
		[procurementNatureId]     [tinyint] NOT NULL,
		[procurementTypeId]       [tinyint] NOT NULL,
		[templateId]              [smallint] NOT NULL,
		[tenderTypeId]            [tinyint] NOT NULL,
		CONSTRAINT [UQ__tbl_Conf__A3391C9D69C6B1F5]
		UNIQUE
		NONCLUSTERED
		([configStdId])
		ON [PRIMARY],
		CONSTRAINT [PK__tbl_Conf__A3391C9C66EA454A]
		PRIMARY KEY
		CLUSTERED
		([configStdId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ConfigStd]
	ADD
	CONSTRAINT [DF_tbl_ConfigStd_tenderTypeId]
	DEFAULT ((1)) FOR [tenderTypeId]
GO
ALTER TABLE [dbo].[tbl_ConfigStd]
	WITH CHECK
	ADD CONSTRAINT [FKC49BA8203C4194A9]
	FOREIGN KEY ([procurementNatureId]) REFERENCES [dbo].[tbl_ProcurementNature] ([procurementNatureId])
ALTER TABLE [dbo].[tbl_ConfigStd]
	CHECK CONSTRAINT [FKC49BA8203C4194A9]

GO
ALTER TABLE [dbo].[tbl_ConfigStd]
	WITH CHECK
	ADD CONSTRAINT [FKC49BA820904D9C11]
	FOREIGN KEY ([templateId]) REFERENCES [dbo].[tbl_TemplateMaster] ([templateId])
ALTER TABLE [dbo].[tbl_ConfigStd]
	CHECK CONSTRAINT [FKC49BA820904D9C11]

GO
ALTER TABLE [dbo].[tbl_ConfigStd]
	WITH CHECK
	ADD CONSTRAINT [FKC49BA820A5C4169D]
	FOREIGN KEY ([procurementMethodId]) REFERENCES [dbo].[tbl_ProcurementMethod] ([procurementMethodId])
ALTER TABLE [dbo].[tbl_ConfigStd]
	CHECK CONSTRAINT [FKC49BA820A5C4169D]

GO
ALTER TABLE [dbo].[tbl_ConfigStd]
	WITH CHECK
	ADD CONSTRAINT [FKC49BA820A90681AE]
	FOREIGN KEY ([procurementTypeId]) REFERENCES [dbo].[tbl_ProcurementTypes] ([procurementTypeId])
ALTER TABLE [dbo].[tbl_ConfigStd]
	CHECK CONSTRAINT [FKC49BA820A90681AE]

GO
ALTER TABLE [dbo].[tbl_ConfigStd] SET (LOCK_ESCALATION = TABLE)
GO
