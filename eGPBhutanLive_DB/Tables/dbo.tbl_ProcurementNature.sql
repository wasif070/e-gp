SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ProcurementNature] (
		[procurementNatureId]     [tinyint] IDENTITY(1, 1) NOT NULL,
		[procurementNature]       [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [procurementNatureId_PK]
		PRIMARY KEY
		CLUSTERED
		([procurementNatureId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ProcurementNature] SET (LOCK_ESCALATION = TABLE)
GO
