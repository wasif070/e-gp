SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_NegBidTable] (
		[negBidTableId]     [int] IDENTITY(1, 1) NOT NULL,
		[bidId]             [int] NOT NULL,
		[tenderFormId]      [int] NOT NULL,
		[tenderTableId]     [int] NOT NULL,
		[submissionDt]      [datetime] NOT NULL,
		[negBidFormId]      [int] NOT NULL,
		CONSTRAINT [PK_tbl_NegBidTable]
		PRIMARY KEY
		CLUSTERED
		([negBidTableId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_NegBidTable]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_NegBidTable_PK_tbl_NegBidForm]
	FOREIGN KEY ([negBidFormId]) REFERENCES [dbo].[tbl_NegBidForm] ([negBidFormId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_NegBidTable]
	CHECK CONSTRAINT [FK_tbl_NegBidTable_PK_tbl_NegBidForm]

GO
ALTER TABLE [dbo].[tbl_NegBidTable] SET (LOCK_ESCALATION = TABLE)
GO
