SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_SrvSRVari] (
		[srvSRVariId]        [int] IDENTITY(1, 1) NOT NULL,
		[srvTCVariId]        [int] NOT NULL,
		[workFrom]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[workMonths]         [decimal](5, 2) NOT NULL,
		[empMonthSalary]     [decimal](15, 3) NOT NULL,
		[totalSalary]        [decimal](15, 3) NOT NULL,
		[noOfDays]           [int] NOT NULL,
		[empName]            [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[srvFormMapId]       [int] NOT NULL,
		[srNo]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[variOrdId]          [int] NOT NULL,
		CONSTRAINT [PK_tbl_CMS_SrvSRVari]
		PRIMARY KEY
		CLUSTERED
		([srvSRVariId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvSRVari] SET (LOCK_ESCALATION = TABLE)
GO
