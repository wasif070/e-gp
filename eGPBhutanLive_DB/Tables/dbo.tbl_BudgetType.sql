SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_BudgetType] (
		[budgetTypeId]     [tinyint] IDENTITY(1, 1) NOT NULL,
		[budgetType]       [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [budgetTypeId_PK]
		PRIMARY KEY
		CLUSTERED
		([budgetTypeId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_BudgetType] SET (LOCK_ESCALATION = TABLE)
GO
