SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_bidderLots] (
		[bidderLotId]      [int] IDENTITY(1, 1) NOT NULL,
		[insdate]          [smalldatetime] NOT NULL,
		[pkgLotId]         [int] NOT NULL,
		[tenderId]         [int] NOT NULL,
		[userId]           [int] NOT NULL,
		[deleteDate]       [datetime] NULL,
		[deleteStatus]     [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [UQ__tbl_bidd__0E1CF9F55F7E2DAC]
		UNIQUE
		NONCLUSTERED
		([bidderLotId])
		ON [PRIMARY],
		CONSTRAINT [PK__tbl_bidd__0E1CF9F45CA1C101]
		PRIMARY KEY
		CLUSTERED
		([bidderLotId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_bidderLots] SET (LOCK_ESCALATION = TABLE)
GO
