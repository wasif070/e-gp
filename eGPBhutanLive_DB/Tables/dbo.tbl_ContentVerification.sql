SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ContentVerification] (
		[contentVerificationId]     [int] IDENTITY(1, 1) NOT NULL,
		[isPublicForum]             [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isProcureExpert]           [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [contentVerificationId_PK]
		PRIMARY KEY
		CLUSTERED
		([contentVerificationId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'system generated unique id information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ContentVerification', 'COLUMN', N'contentVerificationId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'is Procurement expert or not ( yes/no) information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ContentVerification', 'COLUMN', N'isProcureExpert'
GO
EXEC sp_addextendedproperty N'MS_Description', N'is public or not ( Yes/No) information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ContentVerification', 'COLUMN', N'isPublicForum'
GO
ALTER TABLE [dbo].[tbl_ContentVerification] SET (LOCK_ESCALATION = TABLE)
GO
