SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_UserActivationHistory] (
		[historyId]        [int] IDENTITY(1, 1) NOT NULL,
		[Id]               [int] NOT NULL,
		[userTypeId]       [int] NOT NULL,
		[action]           [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[actionDt]         [datetime] NOT NULL,
		[actionBy]         [int] NOT NULL,
		[actionBYName]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[userId]           [int] NOT NULL,
		[fullName]         [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[comments]         [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_UserActivationHistory]
		PRIMARY KEY
		CLUSTERED
		([historyId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_UserActivationHistory]
	ADD
	CONSTRAINT [DF_tbl_UserActivationHistory_userId]
	DEFAULT ((0)) FOR [userId]
GO
ALTER TABLE [dbo].[tbl_UserActivationHistory] SET (LOCK_ESCALATION = TABLE)
GO
