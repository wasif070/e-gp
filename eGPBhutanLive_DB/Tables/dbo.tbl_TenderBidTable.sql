SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_TenderBidTable] (
		[bidTableId]         [int] IDENTITY(1, 1) NOT NULL,
		[bidId]              [int] NOT NULL,
		[tenderFormId]       [int] NOT NULL,
		[tenderTableId]      [int] NOT NULL,
		[submittedTimes]     [smallint] NOT NULL,
		[submissionDt]       [smalldatetime] NOT NULL,
		CONSTRAINT [bidTableId_PK]
		PRIMARY KEY
		CLUSTERED
		([bidTableId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderBidTable]
	WITH CHECK
	ADD CONSTRAINT [tenderBidId_FK1]
	FOREIGN KEY ([bidId]) REFERENCES [dbo].[tbl_TenderBidForm] ([bidId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderBidTable]
	CHECK CONSTRAINT [tenderBidId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderBidTable] SET (LOCK_ESCALATION = TABLE)
GO
