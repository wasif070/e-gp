SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderSectionDocs] (
		[tenderSectionDocId]       [int] IDENTITY(1, 1) NOT NULL,
		[description]              [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docName]                  [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]                  [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderSectionId]          [int] NOT NULL,
		[templateSectionDocId]     [smallint] NOT NULL,
		[status]                   [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderId]                 [smallint] NOT NULL,
		CONSTRAINT [UQ__tbl_Tend__9DD13B2723EA2F0A]
		UNIQUE
		NONCLUSTERED
		([tenderSectionDocId])
		ON [PRIMARY],
		CONSTRAINT [PK__tbl_Tend__9DD13B26210DC25F]
		PRIMARY KEY
		CLUSTERED
		([tenderSectionDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderSectionDocs]
	WITH CHECK
	ADD CONSTRAINT [docTenderSectionId_Fk1]
	FOREIGN KEY ([tenderSectionId]) REFERENCES [dbo].[tbl_TenderSection] ([tenderSectionId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderSectionDocs]
	CHECK CONSTRAINT [docTenderSectionId_Fk1]

GO
ALTER TABLE [dbo].[tbl_TenderSectionDocs] SET (LOCK_ESCALATION = TABLE)
GO
