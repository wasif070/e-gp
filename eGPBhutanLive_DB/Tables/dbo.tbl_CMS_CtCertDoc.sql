SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_CtCertDoc] (
		[ctCertDocId]               [int] IDENTITY(1, 1) NOT NULL,
		[contractTerminationId]     [int] NOT NULL,
		[documentName]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]                   [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]            [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedBy]                [int] NOT NULL,
		[uploadedDate]              [datetime] NOT NULL,
		[userTypeId]                [int] NULL,
		CONSTRAINT [ctCertDocId_PK]
		PRIMARY KEY
		CLUSTERED
		([ctCertDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_CtCertDoc]
	WITH CHECK
	ADD CONSTRAINT [ctTerminationId_FK1]
	FOREIGN KEY ([contractTerminationId]) REFERENCES [dbo].[tbl_CMS_ContractTermination] ([contractTerminationId])
ALTER TABLE [dbo].[tbl_CMS_CtCertDoc]
	CHECK CONSTRAINT [ctTerminationId_FK1]

GO
ALTER TABLE [dbo].[tbl_CMS_CtCertDoc] SET (LOCK_ESCALATION = TABLE)
GO
