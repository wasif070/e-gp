SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderAdvtDetails] (
		[tenderAdvtDetId]     [int] IDENTITY(1, 1) NOT NULL,
		[nameOfPaper]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[nameOfWebsite]       [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[PaperPubDt]          [datetime] NOT NULL,
		[websitePubDt]        [datetime] NOT NULL,
		[tenderId]            [int] NOT NULL,
		CONSTRAINT [UQ__tbl_Tend__989B844B42E2C084]
		UNIQUE
		NONCLUSTERED
		([tenderAdvtDetId])
		ON [PRIMARY],
		CONSTRAINT [PK__tbl_Tend__989B844A930054BA]
		PRIMARY KEY
		CLUSTERED
		([tenderAdvtDetId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderAdvtDetails]
	WITH NOCHECK
	ADD CONSTRAINT [FK4FAF130C98B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_TenderAdvtDetails]
	CHECK CONSTRAINT [FK4FAF130C98B66EC5]

GO
ALTER TABLE [dbo].[tbl_TenderAdvtDetails] SET (LOCK_ESCALATION = TABLE)
GO
