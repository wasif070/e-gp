SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_QuizAnswer] (
		[answerId]       [int] IDENTITY(1, 1) NOT NULL,
		[answer]         [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[questionId]     [int] NOT NULL,
		[isCorrect]      [bit] NOT NULL,
		CONSTRAINT [PK_tbl_QuizAnswer]
		PRIMARY KEY
		CLUSTERED
		([answerId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_QuizAnswer]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_QuizAnswer_tbl_QuizQuestion]
	FOREIGN KEY ([questionId]) REFERENCES [dbo].[tbl_QuizQuestion] ([questionId])
ALTER TABLE [dbo].[tbl_QuizAnswer]
	CHECK CONSTRAINT [FK_tbl_QuizAnswer_tbl_QuizQuestion]

GO
ALTER TABLE [dbo].[tbl_QuizAnswer] SET (LOCK_ESCALATION = TABLE)
GO
