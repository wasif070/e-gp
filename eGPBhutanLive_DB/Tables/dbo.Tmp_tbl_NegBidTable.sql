SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Tmp_tbl_NegBidTable] (
		[negBidTableId]     [int] IDENTITY(1, 1) NOT NULL,
		[bidId]             [int] NOT NULL,
		[tenderFormId]      [int] NOT NULL,
		[tenderTableId]     [int] NOT NULL,
		[submissionDt]      [datetime] NOT NULL,
		[negBidFormId]      [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tmp_tbl_NegBidTable] SET (LOCK_ESCALATION = TABLE)
GO
