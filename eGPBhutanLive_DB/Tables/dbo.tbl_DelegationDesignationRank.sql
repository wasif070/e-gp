SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_DelegationDesignationRank] (
		[RankId]                 [int] IDENTITY(1, 1) NOT NULL,
		[ApprovingAuthority]     [varchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_DelegationDesignationRank]
		PRIMARY KEY
		CLUSTERED
		([RankId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_DelegationDesignationRank] SET (LOCK_ESCALATION = TABLE)
GO
