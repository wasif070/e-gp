SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TrackLogin] (
		[trackId]              [int] IDENTITY(1, 1) NOT NULL,
		[userId]               [int] NOT NULL,
		[loginAttemptDate]     [smalldatetime] NOT NULL,
		[loginStatus]          [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ipAddress]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [trackId_PK]
		PRIMARY KEY
		CLUSTERED
		([trackId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TrackLogin]
	ADD
	CONSTRAINT [DF_tbl_TrackLogin_loginAttemptDate]
	DEFAULT (((0)-(0))-(0)) FOR [loginAttemptDate]
GO
ALTER TABLE [dbo].[tbl_TrackLogin]
	WITH CHECK
	ADD CONSTRAINT [trackUserId_FK1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TrackLogin]
	CHECK CONSTRAINT [trackUserId_FK1]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Client ip address information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TrackLogin', 'COLUMN', N'ipAddress'
GO
EXEC sp_addextendedproperty N'MS_Description', N'login Attemt date and time information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TrackLogin', 'COLUMN', N'loginAttemptDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Status of login store here,it may be successful or unsuccessful ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TrackLogin', 'COLUMN', N'loginStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Unique id of track login stores here , its sytem generated id', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TrackLogin', 'COLUMN', N'trackId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Unique Id of eGP system  stores here ,its refer tbl_LoginMaster', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TrackLogin', 'COLUMN', N'userId'
GO
ALTER TABLE [dbo].[tbl_TrackLogin] SET (LOCK_ESCALATION = TABLE)
GO
