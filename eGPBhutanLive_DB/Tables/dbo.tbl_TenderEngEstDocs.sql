SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderEngEstDocs] (
		[tenderEstDocId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]           [int] NOT NULL,
		[documentName]       [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]       [smalldatetime] NOT NULL,
		[uploadedBy]         [int] NOT NULL,
		CONSTRAINT [PK_tbl_TenderEngEstDocs]
		PRIMARY KEY
		CLUSTERED
		([tenderEstDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderEngEstDocs]
	WITH NOCHECK
	ADD CONSTRAINT [FKF994D69898B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_TenderEngEstDocs]
	CHECK CONSTRAINT [FKF994D69898B66EC5]

GO
ALTER TABLE [dbo].[tbl_TenderEngEstDocs]
	WITH NOCHECK
	ADD CONSTRAINT [tenderEngEstTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderEngEstDocs]
	CHECK CONSTRAINT [tenderEngEstTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderEngEstDocs] SET (LOCK_ESCALATION = TABLE)
GO
