SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_SrvWrkSchDoc] (
		[wsDocId]            [int] IDENTITY(1, 1) NOT NULL,
		[srvFormMapId]       [int] NOT NULL,
		[documentName]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]     [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedBy]         [int] NOT NULL,
		[uploadedDate]       [datetime] NOT NULL,
		[process]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[userTypeId]         [int] NULL,
		[variOrdId]          [int] NULL,
		CONSTRAINT [PK_tbl_CMS_SrvWrkSchDoc]
		PRIMARY KEY
		CLUSTERED
		([wsDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvWrkSchDoc] SET (LOCK_ESCALATION = TABLE)
GO
