SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderPaymentStatus] (
		[paymentStatusId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderPaymentId]     [int] NOT NULL,
		[paymentStatus]       [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[paymentStatusDt]     [smalldatetime] NOT NULL,
		[createdBy]           [int] NOT NULL,
		[comments]            [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderPayRefId]      [int] NOT NULL,
		[refPartTransId]      [int] NULL,
		CONSTRAINT [PK_tbl_TenderPaymentStatus]
		PRIMARY KEY
		CLUSTERED
		([paymentStatusId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderPaymentStatus]
	ADD
	CONSTRAINT [DF_tbl_TenderPaymentStatus_tenderPayRefId]
	DEFAULT ((0)) FOR [tenderPayRefId]
GO
ALTER TABLE [dbo].[tbl_TenderPaymentStatus]
	WITH CHECK
	ADD CONSTRAINT [statusTenderPaymentId_FK1]
	FOREIGN KEY ([tenderPaymentId]) REFERENCES [dbo].[tbl_TenderPayment] ([tenderPaymentId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderPaymentStatus]
	CHECK CONSTRAINT [statusTenderPaymentId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderPaymentStatus] SET (LOCK_ESCALATION = TABLE)
GO
