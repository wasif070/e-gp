SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ConfigurationMaster] (
		[configId]               [smallint] IDENTITY(1, 1) NOT NULL,
		[failedLoginAttempt]     [tinyint] NOT NULL,
		[isPkiRequired]          [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[fileSize]               [tinyint] NOT NULL,
		[totalSize]              [tinyint] NOT NULL,
		[allowedExtension]       [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[regFeeRequired]         [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[amount]                 [money] NOT NULL,
		[renewalAmount]          [money] NOT NULL,
		[validityInYear]         [tinyint] NOT NULL,
		[isCurrent]              [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[userType]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_ConfigurationMaster]
		PRIMARY KEY
		CLUSTERED
		([configId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ConfigurationMaster]
	ADD
	CONSTRAINT [DF_tbl_ConfigurationMaster_amount]
	DEFAULT ((0)) FOR [amount]
GO
ALTER TABLE [dbo].[tbl_ConfigurationMaster]
	ADD
	CONSTRAINT [DF_tbl_ConfigurationMaster_isCurrent]
	DEFAULT ('no') FOR [isCurrent]
GO
ALTER TABLE [dbo].[tbl_ConfigurationMaster]
	ADD
	CONSTRAINT [DF_tbl_ConfigurationMaster_isPkiRequired]
	DEFAULT ('no') FOR [isPkiRequired]
GO
ALTER TABLE [dbo].[tbl_ConfigurationMaster]
	ADD
	CONSTRAINT [DF_tbl_ConfigurationMaster_renewalAmount]
	DEFAULT ((0)) FOR [renewalAmount]
GO
ALTER TABLE [dbo].[tbl_ConfigurationMaster]
	ADD
	CONSTRAINT [DF_tbl_ConfigurationMaster_userType]
	DEFAULT ('officer') FOR [userType]
GO
ALTER TABLE [dbo].[tbl_ConfigurationMaster]
	ADD
	CONSTRAINT [DF_tbl_ConfigurationMaster_validityInYear]
	DEFAULT ((0)) FOR [validityInYear]
GO
ALTER TABLE [dbo].[tbl_ConfigurationMaster] SET (LOCK_ESCALATION = TABLE)
GO
