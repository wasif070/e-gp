SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_BidderRank] (
		[bidderRankId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]         [int] NOT NULL,
		[pkgLotId]         [int] NOT NULL,
		[userId]           [int] NOT NULL,
		[createdTime]      [smalldatetime] NOT NULL,
		[createdBy]        [int] NOT NULL,
		[rank]             [int] NOT NULL,
		[reportId]         [int] NOT NULL,
		[amount]           [numeric](20, 3) NOT NULL,
		[rowId]            [int] NOT NULL,
		[tableId]          [int] NOT NULL,
		[companyName]      [varchar](350) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[roundId]          [int] NOT NULL,
		[t1Score]          [numeric](20, 2) NULL,
		[l1Score]          [numeric](20, 2) NULL,
		CONSTRAINT [bidderRankId_PK]
		PRIMARY KEY
		CLUSTERED
		([bidderRankId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_BidderRank]
	ADD
	CONSTRAINT [DF_tbl_BidderRank_roundId]
	DEFAULT ((1)) FOR [roundId]
GO
ALTER TABLE [dbo].[tbl_BidderRank]
	WITH NOCHECK
	ADD CONSTRAINT [bidderRankTenderId_Fk1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_BidderRank]
	CHECK CONSTRAINT [bidderRankTenderId_Fk1]

GO
ALTER TABLE [dbo].[tbl_BidderRank]
	WITH NOCHECK
	ADD CONSTRAINT [FKE223FF8198B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_BidderRank]
	CHECK CONSTRAINT [FKE223FF8198B66EC5]

GO
ALTER TABLE [dbo].[tbl_BidderRank] SET (LOCK_ESCALATION = TABLE)
GO
