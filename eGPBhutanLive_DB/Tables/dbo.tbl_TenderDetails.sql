SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderDetails] (
		[tenderDtlId]             [int] IDENTITY(1, 1) NOT NULL,
		[departmentId]            [smallint] NOT NULL,
		[tenderId]                [int] NOT NULL,
		[ministry]                [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[division]                [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[agency]                  [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[peOfficeName]            [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[peCode]                  [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[peDistrict]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[eventType]               [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[invitationFor]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[reoiRfpFor]              [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[contractType]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[reoiRfpRefNo]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[procurementMethod]       [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[budgetType]              [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sourceOfFund]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[devPartners]             [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[projectCode]             [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[projectName]             [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[packageNo]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[packageDescription]      [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cpvCode]                 [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docEndDate]              [smalldatetime] NULL,
		[preBidStartDt]           [smalldatetime] NULL,
		[preBidEndDt]             [smalldatetime] NULL,
		[submissionDt]            [smalldatetime] NULL,
		[openingDt]               [smalldatetime] NULL,
		[eligibilityCriteria]     [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderBrief]             [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[deliverables]            [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[otherDetails]            [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[foreignFirm]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docAvlMethod]            [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[evalType]                [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docFeesMethod]           [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docFeesMode]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docOfficeAdd]            [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[securityLastDt]          [smalldatetime] NULL,
		[securitySubOff]          [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[procurementNature]       [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[procurementType]         [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[modeOfTender]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[reTenderId]              [int] NOT NULL,
		[pqTenderId]              [int] NOT NULL,
		[reoiTenderId]            [int] NOT NULL,
		[tenderStatus]            [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderPubDt]             [smalldatetime] NULL,
		[peName]                  [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[peDesignation]           [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[peAddress]               [varchar](5000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[peContactDetails]        [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[estCost]                 [money] NOT NULL,
		[approvingAuthId]         [int] NOT NULL,
		[stdTemplateId]           [int] NOT NULL,
		[docAccess]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderValDays]           [smallint] NOT NULL,
		[tenderValidityDt]        [date] NULL,
		[tenderSecurityDays]      [smallint] NOT NULL,
		[tenderSecurityDt]        [smalldatetime] NULL,
		[procurementNatureId]     [tinyint] NOT NULL,
		[procurementMethodId]     [tinyint] NOT NULL,
		[officeId]                [int] NOT NULL,
		[budgetTypeId]            [tinyint] NOT NULL,
		[workflowStatus]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[pkgDocFees]              [money] NULL,
		[pkgUrgency]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[servicesType]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[passingMarks]            [int] NOT NULL,
		[domesticPref]            [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[domesticPercent]         [money] NOT NULL,
		[pkgDocFeesUSD]           [money] NULL,
		CONSTRAINT [tenderDtlId_PK]
		PRIMARY KEY
		CLUSTERED
		([tenderDtlId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderDetails]
	ADD
	CONSTRAINT [DF_tbl_TenderDetails_domesticPercent]
	DEFAULT ((0)) FOR [domesticPercent]
GO
ALTER TABLE [dbo].[tbl_TenderDetails]
	ADD
	CONSTRAINT [DF_tbl_TenderDetails_domesticPref]
	DEFAULT ('No') FOR [domesticPref]
GO
ALTER TABLE [dbo].[tbl_TenderDetails]
	ADD
	CONSTRAINT [DF_tbl_TenderDetails_evalType]
	DEFAULT ('(Package)') FOR [evalType]
GO
ALTER TABLE [dbo].[tbl_TenderDetails]
	ADD
	CONSTRAINT [DF_tbl_TenderDetails_passingMarks]
	DEFAULT ((0)) FOR [passingMarks]
GO
ALTER TABLE [dbo].[tbl_TenderDetails]
	ADD
	CONSTRAINT [DF_tbl_TenderDetails_pkgDocFees]
	DEFAULT ((0)) FOR [pkgDocFees]
GO
ALTER TABLE [dbo].[tbl_TenderDetails]
	ADD
	CONSTRAINT [DF_tbl_TenderDetails_pqTenderId]
	DEFAULT ((0)) FOR [pqTenderId]
GO
ALTER TABLE [dbo].[tbl_TenderDetails]
	ADD
	CONSTRAINT [DF_tbl_TenderDetails_reoiTenderId]
	DEFAULT ((0)) FOR [reoiTenderId]
GO
ALTER TABLE [dbo].[tbl_TenderDetails]
	ADD
	CONSTRAINT [DF_tbl_TenderDetails_reTenderId]
	DEFAULT ((0)) FOR [reTenderId]
GO
ALTER TABLE [dbo].[tbl_TenderDetails]
	ADD
	CONSTRAINT [DF_tbl_TenderDetails_workflowStatus]
	DEFAULT ('Pending') FOR [workflowStatus]
GO
ALTER TABLE [dbo].[tbl_TenderDetails]
	WITH CHECK
	ADD CONSTRAINT [FKD50EDDED98B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_TenderDetails]
	CHECK CONSTRAINT [FKD50EDDED98B66EC5]

GO
ALTER TABLE [dbo].[tbl_TenderDetails]
	WITH CHECK
	ADD CONSTRAINT [tenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderDetails]
	CHECK CONSTRAINT [tenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderDetails] SET (LOCK_ESCALATION = TABLE)
GO
