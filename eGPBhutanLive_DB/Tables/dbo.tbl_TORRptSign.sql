SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TORRptSign] (
		[torSignId]           [int] IDENTITY(1, 1) NOT NULL,
		[reportType]          [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderId]            [int] NOT NULL,
		[pkgLotId]            [int] NOT NULL,
		[userId]              [int] NOT NULL,
		[comments]            [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[signedDate]          [datetime] NOT NULL,
		[govUserId]           [int] NULL,
		[aggDisAggStatus]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[roundId]             [int] NOT NULL,
		[evalCount]           [int] NOT NULL,
		CONSTRAINT [PK_tbl_TORRptSign]
		PRIMARY KEY
		CLUSTERED
		([torSignId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TORRptSign]
	ADD
	CONSTRAINT [DF__tbl_TORRp__evalC__23DE44F1]
	DEFAULT ('0') FOR [evalCount]
GO
ALTER TABLE [dbo].[tbl_TORRptSign]
	ADD
	CONSTRAINT [DF_tbl_TORRptSign_aggDisAggStatus]
	DEFAULT ('i Aggree') FOR [aggDisAggStatus]
GO
ALTER TABLE [dbo].[tbl_TORRptSign]
	ADD
	CONSTRAINT [DF_tbl_TORRptSign_roundId]
	DEFAULT ((1)) FOR [roundId]
GO
ALTER TABLE [dbo].[tbl_TORRptSign] SET (LOCK_ESCALATION = TABLE)
GO
