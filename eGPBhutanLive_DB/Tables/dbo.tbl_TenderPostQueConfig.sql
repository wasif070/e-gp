SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderPostQueConfig] (
		[postQueConfigId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]            [int] NOT NULL,
		[isQueAnsConfig]      [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]           [int] NOT NULL,
		[createdDt]           [datetime] NOT NULL,
		[postQueLastDt]       [datetime] NULL,
		CONSTRAINT [PK_tbl_TenderPostQueConfig]
		PRIMARY KEY
		CLUSTERED
		([postQueConfigId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderPostQueConfig]
	ADD
	CONSTRAINT [DF_tbl_TenderPostQueConfig_isQueAnsConfig]
	DEFAULT ('No') FOR [isQueAnsConfig]
GO
ALTER TABLE [dbo].[tbl_TenderPostQueConfig]
	WITH NOCHECK
	ADD CONSTRAINT [FK690ACC0E98B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_TenderPostQueConfig]
	CHECK CONSTRAINT [FK690ACC0E98B66EC5]

GO
ALTER TABLE [dbo].[tbl_TenderPostQueConfig]
	WITH NOCHECK
	ADD CONSTRAINT [tenderIdPostQue_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderPostQueConfig]
	CHECK CONSTRAINT [tenderIdPostQue_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderPostQueConfig] SET (LOCK_ESCALATION = TABLE)
GO
