SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_NegotiatedBid] (
		[negBidId]           [int] IDENTITY(1, 1) NOT NULL,
		[bidtableId]         [int] NOT NULL,
		[tenderColumnId]     [int] NOT NULL,
		[tenderTableId]      [int] NOT NULL,
		[cellValue]          [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[rowId]              [int] NOT NULL,
		[cellId]             [int] NOT NULL,
		[formId]             [int] NOT NULL,
		[isQty]              [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[negId]              [int] NOT NULL,
		CONSTRAINT [PK_tbl_NegotiatedBid_1]
		PRIMARY KEY
		CLUSTERED
		([negBidId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_NegotiatedBid]
	ADD
	CONSTRAINT [DF_tbl_NegotiatedBid_isQty]
	DEFAULT ('no') FOR [isQty]
GO
ALTER TABLE [dbo].[tbl_NegotiatedBid]
	ADD
	CONSTRAINT [DF_tbl_NegotiatedBid_negId]
	DEFAULT ((0)) FOR [negId]
GO
ALTER TABLE [dbo].[tbl_NegotiatedBid] SET (LOCK_ESCALATION = TABLE)
GO
