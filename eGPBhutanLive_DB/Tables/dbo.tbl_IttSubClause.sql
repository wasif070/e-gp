SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_IttSubClause] (
		[ittSubClauseId]       [int] IDENTITY(1, 1) NOT NULL,
		[ittClauseId]          [int] NOT NULL,
		[ittSubClauseName]     [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isTdsApplicable]      [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [ittSubClauseId_PK]
		PRIMARY KEY
		CLUSTERED
		([ittSubClauseId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_IttSubClause]
	WITH CHECK
	ADD CONSTRAINT [FK788C0275F40406B]
	FOREIGN KEY ([ittClauseId]) REFERENCES [dbo].[tbl_IttClause] ([ittClauseId])
ALTER TABLE [dbo].[tbl_IttSubClause]
	CHECK CONSTRAINT [FK788C0275F40406B]

GO
ALTER TABLE [dbo].[tbl_IttSubClause]
	WITH CHECK
	ADD CONSTRAINT [subIttClauseId_Fk2]
	FOREIGN KEY ([ittClauseId]) REFERENCES [dbo].[tbl_IttClause] ([ittClauseId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_IttSubClause]
	CHECK CONSTRAINT [subIttClauseId_Fk2]

GO
ALTER TABLE [dbo].[tbl_IttSubClause] SET (LOCK_ESCALATION = TABLE)
GO
