SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_UserMenuRights] (
		[Id]         [int] IDENTITY(1, 1) NOT NULL,
		[userId]     [int] NOT NULL,
		[menuId]     [int] NOT NULL,
		CONSTRAINT [PK_tbl_UserMenuRights]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_UserMenuRights]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_UserMenuRights_tbl_MenuMaster]
	FOREIGN KEY ([menuId]) REFERENCES [dbo].[tbl_MenuMaster] ([menuId])
ALTER TABLE [dbo].[tbl_UserMenuRights]
	CHECK CONSTRAINT [FK_tbl_UserMenuRights_tbl_MenuMaster]

GO
ALTER TABLE [dbo].[tbl_UserMenuRights] SET (LOCK_ESCALATION = TABLE)
GO
