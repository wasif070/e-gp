SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderAdvertisement] (
		[tenderAdvtId]       [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]           [int] NOT NULL,
		[newsPaper]          [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[newsPaperPubDt]     [date] NOT NULL,
		[webSite]            [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[webSiteAdvtDt]      [date] NOT NULL,
		[creationDate]       [smalldatetime] NOT NULL,
		CONSTRAINT [PK_tbl_TenderAdvertisement]
		PRIMARY KEY
		CLUSTERED
		([tenderAdvtId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderAdvertisement]
	WITH NOCHECK
	ADD CONSTRAINT [FK6301117098B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_TenderAdvertisement]
	CHECK CONSTRAINT [FK6301117098B66EC5]

GO
ALTER TABLE [dbo].[tbl_TenderAdvertisement]
	WITH NOCHECK
	ADD CONSTRAINT [tenderAdvertId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderAdvertisement]
	CHECK CONSTRAINT [tenderAdvertId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderAdvertisement] SET (LOCK_ESCALATION = TABLE)
GO
