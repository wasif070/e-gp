SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvaluationReportDocs] (
		[evalDocId]          [int] IDENTITY(1, 1) NOT NULL,
		[documentName]       [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]       [smalldatetime] NOT NULL,
		[uploadedBy]         [int] NOT NULL,
		[tenderId]           [int] NOT NULL,
		[roundId]            [int] NOT NULL,
		CONSTRAINT [PK_tbl_EvaluationReportDocs]
		PRIMARY KEY
		CLUSTERED
		([evalDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvaluationReportDocs] SET (LOCK_ESCALATION = TABLE)
GO
