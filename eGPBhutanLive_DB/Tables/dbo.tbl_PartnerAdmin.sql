SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_PartnerAdmin] (
		[partnerId]          [int] IDENTITY(1, 1) NOT NULL,
		[userId]             [int] NOT NULL,
		[fullName]           [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[nationalId]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[mobileNo]           [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sBankDevelopId]     [int] NOT NULL,
		[isAdmin]            [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[designation]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[isMakerChecker]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_PartnerAdmin]
		PRIMARY KEY
		CLUSTERED
		([partnerId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_PartnerAdmin]
	ADD
	CONSTRAINT [DF_tbl_PartnerAdmin_designation]
	DEFAULT ('a') FOR [designation]
GO
ALTER TABLE [dbo].[tbl_PartnerAdmin]
	ADD
	CONSTRAINT [DF_tbl_PartnerAdmin_isMakerChecker]
	DEFAULT ('maker') FOR [isMakerChecker]
GO
ALTER TABLE [dbo].[tbl_PartnerAdmin]
	WITH CHECK
	ADD CONSTRAINT [FK7A0BF6686174DBD1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_PartnerAdmin]
	CHECK CONSTRAINT [FK7A0BF6686174DBD1]

GO
ALTER TABLE [dbo].[tbl_PartnerAdmin]
	WITH CHECK
	ADD CONSTRAINT [partnerAdminUserId_Fk2]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_PartnerAdmin]
	CHECK CONSTRAINT [partnerAdminUserId_Fk2]

GO
ALTER TABLE [dbo].[tbl_PartnerAdmin]
	WITH CHECK
	ADD CONSTRAINT [partnerUserId_FK1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_PartnerAdmin]
	CHECK CONSTRAINT [partnerUserId_FK1]

GO
ALTER TABLE [dbo].[tbl_PartnerAdmin] SET (LOCK_ESCALATION = TABLE)
GO
