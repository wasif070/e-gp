SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CompanyDocuments] (
		[tendererId]         [int] NOT NULL,
		[companyDocId]       [int] IDENTITY(1, 1) NOT NULL,
		[documentName]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[documentSize]       [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[documentBrief]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]       [smalldatetime] NOT NULL,
		[folderId]           [int] NOT NULL,
		[documentTypeId]     [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[docStatus]          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docHash]            [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DocRefNo]           [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DocDescription]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ReferenceID]        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsReapplied]        [int] NULL,
		CONSTRAINT [companyDocId_PK]
		PRIMARY KEY
		CLUSTERED
		([companyDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CompanyDocuments]
	ADD
	CONSTRAINT [DF_tbl_CompanyDocuments_docStatus]
	DEFAULT ('Approved') FOR [docStatus]
GO
ALTER TABLE [dbo].[tbl_CompanyDocuments]
	WITH CHECK
	ADD CONSTRAINT [FK1C2F8D3C435CF11F]
	FOREIGN KEY ([tendererId]) REFERENCES [dbo].[tbl_TendererMaster] ([tendererId])
ALTER TABLE [dbo].[tbl_CompanyDocuments]
	CHECK CONSTRAINT [FK1C2F8D3C435CF11F]

GO
ALTER TABLE [dbo].[tbl_CompanyDocuments]
	WITH CHECK
	ADD CONSTRAINT [tendererDoc_FK1]
	FOREIGN KEY ([tendererId]) REFERENCES [dbo].[tbl_TendererMaster] ([tendererId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CompanyDocuments]
	CHECK CONSTRAINT [tendererDoc_FK1]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Unique id of document stores here , its system generated id stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyDocuments', 'COLUMN', N'companyDocId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Document brief information stores here it will explain brief information about company’s document', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyDocuments', 'COLUMN', N'documentBrief'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name of document stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyDocuments', 'COLUMN', N'documentName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Size of document stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyDocuments', 'COLUMN', N'documentSize'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Refer tbl_FolderMaster', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyDocuments', 'COLUMN', N'folderId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Unique Id of eGP System stores here ,  refers tbl_TendererMaster', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyDocuments', 'COLUMN', N'tendererId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date of uploaded document stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CompanyDocuments', 'COLUMN', N'uploadedDate'
GO
ALTER TABLE [dbo].[tbl_CompanyDocuments] SET (LOCK_ESCALATION = TABLE)
GO
