SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_NegQuery] (
		[negQueryId]      [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]        [int] NOT NULL,
		[queryText]       [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[userId]          [int] NOT NULL,
		[queryDt]         [smalldatetime] NOT NULL,
		[querySign]       [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[queryStatus]     [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[queryType]       [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[negId]           [int] NOT NULL,
		CONSTRAINT [negQueryId_PK]
		PRIMARY KEY
		CLUSTERED
		([negQueryId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_NegQuery]
	WITH NOCHECK
	ADD CONSTRAINT [FKCA4CA49998B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_NegQuery]
	CHECK CONSTRAINT [FKCA4CA49998B66EC5]

GO
ALTER TABLE [dbo].[tbl_NegQuery]
	WITH NOCHECK
	ADD CONSTRAINT [negQueryTenderId_Fk1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_NegQuery]
	CHECK CONSTRAINT [negQueryTenderId_Fk1]

GO
ALTER TABLE [dbo].[tbl_NegQuery] SET (LOCK_ESCALATION = TABLE)
GO
