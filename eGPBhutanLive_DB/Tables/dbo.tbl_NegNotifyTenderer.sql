SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_NegNotifyTenderer] (
		[negNotId]            [int] IDENTITY(1, 1) NOT NULL,
		[negId]               [int] NOT NULL,
		[userId]              [int] NOT NULL,
		[createdDt]           [datetime] NOT NULL,
		[remarks]             [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[acceptStatus]        [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[acceptDt]            [datetime] NOT NULL,
		[reportAppStatus]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[reportAppDt]         [smalldatetime] NULL,
		[bidAggree]           [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[bidAggreeDt]         [smalldatetime] NULL,
		CONSTRAINT [negNotId_PK]
		PRIMARY KEY
		CLUSTERED
		([negNotId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_NegNotifyTenderer]
	ADD
	CONSTRAINT [DF_tbl_NegNotifyTenderer_bidAggree]
	DEFAULT ('No') FOR [bidAggree]
GO
ALTER TABLE [dbo].[tbl_NegNotifyTenderer]
	ADD
	CONSTRAINT [DF_tbl_NegNotifyTenderer_reportAppStatus]
	DEFAULT ('Pending') FOR [reportAppStatus]
GO
ALTER TABLE [dbo].[tbl_NegNotifyTenderer]
	WITH CHECK
	ADD CONSTRAINT [FKBB466C3914E6A50C]
	FOREIGN KEY ([negId]) REFERENCES [dbo].[tbl_Negotiation] ([negId])
ALTER TABLE [dbo].[tbl_NegNotifyTenderer]
	CHECK CONSTRAINT [FKBB466C3914E6A50C]

GO
ALTER TABLE [dbo].[tbl_NegNotifyTenderer]
	WITH CHECK
	ADD CONSTRAINT [tendererNegId_FK1]
	FOREIGN KEY ([negId]) REFERENCES [dbo].[tbl_Negotiation] ([negId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_NegNotifyTenderer]
	CHECK CONSTRAINT [tendererNegId_FK1]

GO
ALTER TABLE [dbo].[tbl_NegNotifyTenderer] SET (LOCK_ESCALATION = TABLE)
GO
