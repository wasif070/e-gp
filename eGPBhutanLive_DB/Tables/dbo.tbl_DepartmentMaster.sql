SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_DepartmentMaster] (
		[departmentId]             [smallint] IDENTITY(1, 1) NOT NULL,
		[departmentName]           [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[address]                  [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[countryId]                [smallint] NOT NULL,
		[stateId]                  [smallint] NOT NULL,
		[upJilla]                  [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[phoneNo]                  [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[faxNo]                    [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[city]                     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[createdBy]                [int] NOT NULL,
		[createdDate]              [smalldatetime] NOT NULL,
		[status]                   [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[parentDepartmentId]       [smallint] NOT NULL,
		[departmentType]           [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[approvingAuthorityId]     [int] NULL,
		[deptNameInBangla]         [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[website]                  [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[postcode]                 [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[mobileno]                 [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[departmentHirarchy]       [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[organizationType]         [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [id_PK]
		PRIMARY KEY
		CLUSTERED
		([departmentId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_DepartmentMaster]
	ADD
	CONSTRAINT [DF__tbl_Depar__addre__31EC6D26]
	DEFAULT ('N/A') FOR [address]
GO
ALTER TABLE [dbo].[tbl_DepartmentMaster]
	ADD
	CONSTRAINT [DF__tbl_Depar__creat__398D8EEE]
	DEFAULT ((0)) FOR [createdBy]
GO
ALTER TABLE [dbo].[tbl_DepartmentMaster]
	ADD
	CONSTRAINT [DF__tbl_Depar__creat__3A81B327]
	DEFAULT (((0)-(0))-(0)) FOR [createdDate]
GO
ALTER TABLE [dbo].[tbl_DepartmentMaster]
	ADD
	CONSTRAINT [DF__tbl_Depar__faxNo__37A5467C]
	DEFAULT ((0)) FOR [faxNo]
GO
ALTER TABLE [dbo].[tbl_DepartmentMaster]
	ADD
	CONSTRAINT [DF__tbl_Depar__jilla__34C8D9D1]
	DEFAULT ((0)) FOR [upJilla]
GO
ALTER TABLE [dbo].[tbl_DepartmentMaster]
	ADD
	CONSTRAINT [DF__tbl_Depar__paren__403A8C7D]
	DEFAULT ((0)) FOR [parentDepartmentId]
GO
ALTER TABLE [dbo].[tbl_DepartmentMaster]
	ADD
	CONSTRAINT [DF__tbl_Depar__phone__36B12243]
	DEFAULT ((0)) FOR [phoneNo]
GO
ALTER TABLE [dbo].[tbl_DepartmentMaster]
	ADD
	CONSTRAINT [DF__tbl_Depar__statu__3B75D760]
	DEFAULT ('Pending') FOR [status]
GO
ALTER TABLE [dbo].[tbl_DepartmentMaster]
	ADD
	CONSTRAINT [DF__tbl_Depart__city__38996AB5]
	DEFAULT ('N/A') FOR [city]
GO
ALTER TABLE [dbo].[tbl_DepartmentMaster]
	ADD
	CONSTRAINT [DF__tbl_Depart__name__30F848ED]
	DEFAULT ('N/A') FOR [departmentName]
GO
ALTER TABLE [dbo].[tbl_DepartmentMaster]
	ADD
	CONSTRAINT [DF_tbl_DepartmentMaster_approvingAuthorityId]
	DEFAULT ((0)) FOR [approvingAuthorityId]
GO
ALTER TABLE [dbo].[tbl_DepartmentMaster]
	ADD
	CONSTRAINT [DF_tbl_DepartmentMaster_organizationType]
	DEFAULT ('Large') FOR [organizationType]
GO
ALTER TABLE [dbo].[tbl_DepartmentMaster]
	WITH CHECK
	ADD CONSTRAINT [countryId_DM_FK1]
	FOREIGN KEY ([countryId]) REFERENCES [dbo].[tbl_CountryMaster] ([countryId])
ALTER TABLE [dbo].[tbl_DepartmentMaster]
	CHECK CONSTRAINT [countryId_DM_FK1]

GO
ALTER TABLE [dbo].[tbl_DepartmentMaster]
	WITH CHECK
	ADD CONSTRAINT [FKA728AED51ADFF23F]
	FOREIGN KEY ([stateId]) REFERENCES [dbo].[tbl_StateMaster] ([stateId])
ALTER TABLE [dbo].[tbl_DepartmentMaster]
	CHECK CONSTRAINT [FKA728AED51ADFF23F]

GO
ALTER TABLE [dbo].[tbl_DepartmentMaster]
	WITH CHECK
	ADD CONSTRAINT [FKA728AED5F059B349]
	FOREIGN KEY ([countryId]) REFERENCES [dbo].[tbl_CountryMaster] ([countryId])
ALTER TABLE [dbo].[tbl_DepartmentMaster]
	CHECK CONSTRAINT [FKA728AED5F059B349]

GO
ALTER TABLE [dbo].[tbl_DepartmentMaster]
	WITH NOCHECK
	ADD CONSTRAINT [stateId_tbl_DepartmentMaster_FK2]
	FOREIGN KEY ([stateId]) REFERENCES [dbo].[tbl_StateMaster] ([stateId])
ALTER TABLE [dbo].[tbl_DepartmentMaster]
	CHECK CONSTRAINT [stateId_tbl_DepartmentMaster_FK2]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Address of the Department', 'SCHEMA', N'dbo', 'TABLE', N'tbl_DepartmentMaster', 'COLUMN', N'address'
GO
EXEC sp_addextendedproperty N'MS_Description', N'City name', 'SCHEMA', N'dbo', 'TABLE', N'tbl_DepartmentMaster', 'COLUMN', N'city'
GO
EXEC sp_addextendedproperty N'MS_Description', N'ID(pk) of the Country related from CountryMaster table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_DepartmentMaster', 'COLUMN', N'countryId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'UserId of the user one who is creating', 'SCHEMA', N'dbo', 'TABLE', N'tbl_DepartmentMaster', 'COLUMN', N'createdBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Current Date and Time when the data was updated', 'SCHEMA', N'dbo', 'TABLE', N'tbl_DepartmentMaster', 'COLUMN', N'createdDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stores unique Id of department, system generated  id stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_DepartmentMaster', 'COLUMN', N'departmentId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name of the Department', 'SCHEMA', N'dbo', 'TABLE', N'tbl_DepartmentMaster', 'COLUMN', N'departmentName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'To identify type of department it may be division/organization/ministry', 'SCHEMA', N'dbo', 'TABLE', N'tbl_DepartmentMaster', 'COLUMN', N'departmentType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Fax Number of the office', 'SCHEMA', N'dbo', 'TABLE', N'tbl_DepartmentMaster', 'COLUMN', N'faxNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Id of the parent hirarchy or immediate superior office can be  ministry,division,etc.', 'SCHEMA', N'dbo', 'TABLE', N'tbl_DepartmentMaster', 'COLUMN', N'parentDepartmentId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Phone number of contact office', 'SCHEMA', N'dbo', 'TABLE', N'tbl_DepartmentMaster', 'COLUMN', N'phoneNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Related to state  Id from State Master Table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_DepartmentMaster', 'COLUMN', N'stateId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Flag for Approval status(pending/Approved/Rejected/Disabled/Cancelled) etc', 'SCHEMA', N'dbo', 'TABLE', N'tbl_DepartmentMaster', 'COLUMN', N'status'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Related to jilla Master Table Id', 'SCHEMA', N'dbo', 'TABLE', N'tbl_DepartmentMaster', 'COLUMN', N'upJilla'
GO
ALTER TABLE [dbo].[tbl_DepartmentMaster] SET (LOCK_ESCALATION = TABLE)
GO
