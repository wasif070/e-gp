SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_SrvWs] (
		[srvTimeBasedId]      [int] IDENTITY(1, 1) NOT NULL,
		[wpid]                [int] NOT NULL,
		[srNo]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[employeeName]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[postionAssigned]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[homeOrField]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[salary]              [decimal](6, 3) NOT NULL,
		[noOfDays]            [int] NOT NULL,
		[month]               [int] NOT NULL,
		CONSTRAINT [srvTimeBasedId_PK]
		PRIMARY KEY
		CLUSTERED
		([srvTimeBasedId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvWs]
	WITH NOCHECK
	ADD CONSTRAINT [cmsSrvWsWpId_FK1]
	FOREIGN KEY ([wpid]) REFERENCES [dbo].[tbl_CMS_WpMaster] ([wpId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_SrvWs]
	CHECK CONSTRAINT [cmsSrvWsWpId_FK1]

GO
ALTER TABLE [dbo].[tbl_CMS_SrvWs] SET (LOCK_ESCALATION = TABLE)
GO
