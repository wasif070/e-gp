SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_PrDetail] (
		[progressReptDtlId]       [int] IDENTITY(1, 1) NOT NULL,
		[progressRepId]           [int] NOT NULL,
		[qtyDlvrdCurrPr]          [decimal](15, 3) NOT NULL,
		[qtyAcceptTillThisPr]     [decimal](15, 3) NOT NULL,
		[totalPendingQty]         [decimal](15, 3) NOT NULL,
		[rowId]                   [int] NOT NULL,
		[remarks]                 [varchar](3000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[deliveryStatus]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[itemEntryDt]             [datetime] NULL,
		[tenderTableId]           [int] NULL,
		[qtyDuringThisPr]         [decimal](15, 3) NOT NULL,
		CONSTRAINT [progressReptDtlId_PK]
		PRIMARY KEY
		CLUSTERED
		([progressReptDtlId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_PrDetail]
	ADD
	CONSTRAINT [DF_tbl_CMS_PrDetail_deliveryStatus]
	DEFAULT ('pending') FOR [deliveryStatus]
GO
ALTER TABLE [dbo].[tbl_CMS_PrDetail]
	ADD
	CONSTRAINT [DF_tbl_CMS_PrDetail_qtyDuringThisPr]
	DEFAULT ((0.00)) FOR [qtyDuringThisPr]
GO
ALTER TABLE [dbo].[tbl_CMS_PrDetail]
	WITH CHECK
	ADD CONSTRAINT [cmsPrDetailProgressRepId_FK1]
	FOREIGN KEY ([progressRepId]) REFERENCES [dbo].[tbl_CMS_PrMaster] ([progressRepId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_PrDetail]
	CHECK CONSTRAINT [cmsPrDetailProgressRepId_FK1]

GO
ALTER TABLE [dbo].[tbl_CMS_PrDetail] SET (LOCK_ESCALATION = TABLE)
GO
