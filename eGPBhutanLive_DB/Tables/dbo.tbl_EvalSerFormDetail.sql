SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalSerFormDetail] (
		[esFormDetailId]      [int] IDENTITY(1, 1) NOT NULL,
		[tenderFormId]        [int] NOT NULL,
		[userId]              [int] NOT NULL,
		[maxMarks]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[actualMarks]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[evalBy]              [int] NOT NULL,
		[evalDate]            [smalldatetime] NOT NULL,
		[evalStatus]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[evalStatusDt]        [smalldatetime] NULL,
		[ratingWeightage]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ratedScore]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[bidId]               [int] NOT NULL,
		[isMemberDropped]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[remarks]             [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[subCriteriaId]       [int] NOT NULL,
		[tenderId]            [int] NOT NULL,
		[evalCount]           [int] NOT NULL,
		CONSTRAINT [PK_tbl_EvalSerFormDetail]
		PRIMARY KEY
		CLUSTERED
		([esFormDetailId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalSerFormDetail]
	ADD
	CONSTRAINT [DF__tbl_EvalS__evalC__24B26D99]
	DEFAULT ('0') FOR [evalCount]
GO
ALTER TABLE [dbo].[tbl_EvalSerFormDetail]
	ADD
	CONSTRAINT [DF_tbl_EvalSerFormDetail_evalDate]
	DEFAULT (getdate()) FOR [evalDate]
GO
ALTER TABLE [dbo].[tbl_EvalSerFormDetail]
	ADD
	CONSTRAINT [DF_tbl_EvalSerFormDetail_isMemberDropped]
	DEFAULT ('No') FOR [isMemberDropped]
GO
ALTER TABLE [dbo].[tbl_EvalSerFormDetail]
	WITH CHECK
	ADD CONSTRAINT [FK27472118735760EE]
	FOREIGN KEY ([tenderFormId]) REFERENCES [dbo].[tbl_TenderForms] ([tenderFormId])
ALTER TABLE [dbo].[tbl_EvalSerFormDetail]
	CHECK CONSTRAINT [FK27472118735760EE]

GO
ALTER TABLE [dbo].[tbl_EvalSerFormDetail] SET (LOCK_ESCALATION = TABLE)
GO
