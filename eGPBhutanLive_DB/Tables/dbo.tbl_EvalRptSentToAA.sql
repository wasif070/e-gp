SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalRptSentToAA] (
		[evalRptToAAId]       [int] IDENTITY(1, 1) NOT NULL,
		[evalRptId]           [int] NOT NULL,
		[userId]              [int] NOT NULL,
		[sentBy]              [int] NOT NULL,
		[tenderId]            [int] NOT NULL,
		[remarks]             [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sentDate]            [smalldatetime] NOT NULL,
		[aaGovUserId]         [int] NULL,
		[sentByGovUserId]     [int] NULL,
		[sentRole]            [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[pkgLotId]            [int] NOT NULL,
		[rptStatus]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[aaRemarks]           [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[envelopeId]          [int] NOT NULL,
		[roundId]             [int] NOT NULL,
		[rptApproveDt]        [smalldatetime] NULL,
		[evalCount]           [int] NOT NULL,
		CONSTRAINT [PK_tbl_EvalRptSentToAA]
		PRIMARY KEY
		CLUSTERED
		([evalRptToAAId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalRptSentToAA]
	ADD
	CONSTRAINT [DF__tbl_EvalR__evalC__0FB750B3]
	DEFAULT ('0') FOR [evalCount]
GO
ALTER TABLE [dbo].[tbl_EvalRptSentToAA]
	ADD
	CONSTRAINT [DF_tbl_EvalRptSentToAA_employeeId]
	DEFAULT ((1)) FOR [envelopeId]
GO
ALTER TABLE [dbo].[tbl_EvalRptSentToAA]
	ADD
	CONSTRAINT [DF_tbl_EvalRptSentToAA_pkgLotId]
	DEFAULT ((0)) FOR [pkgLotId]
GO
ALTER TABLE [dbo].[tbl_EvalRptSentToAA]
	ADD
	CONSTRAINT [DF_tbl_EvalRptSentToAA_roundId]
	DEFAULT ((1)) FOR [roundId]
GO
ALTER TABLE [dbo].[tbl_EvalRptSentToAA]
	ADD
	CONSTRAINT [DF_tbl_EvalRptSentToAA_rptStatus]
	DEFAULT ('pending') FOR [rptStatus]
GO
ALTER TABLE [dbo].[tbl_EvalRptSentToAA]
	ADD
	CONSTRAINT [DF_tbl_EvalRptSentToAA_sentRole]
	DEFAULT ('AA') FOR [sentRole]
GO
ALTER TABLE [dbo].[tbl_EvalRptSentToAA]
	WITH NOCHECK
	ADD CONSTRAINT [evalRptSentToTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_EvalRptSentToAA]
	CHECK CONSTRAINT [evalRptSentToTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_EvalRptSentToAA] SET (LOCK_ESCALATION = TABLE)
GO
