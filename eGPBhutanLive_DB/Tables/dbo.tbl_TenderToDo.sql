SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderToDo] (
		[id]             [tinyint] IDENTITY(1, 1) NOT NULL,
		[uptodt]         [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[loginId]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[password]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[oneTimePwd]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[app]            [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderer]       [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tender]         [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[auction]        [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[emailId]        [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_TenderToDo]
		PRIMARY KEY
		CLUSTERED
		([id])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderToDo] SET (LOCK_ESCALATION = TABLE)
GO
