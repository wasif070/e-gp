SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderColumns] (
		[tenderColId]         [int] IDENTITY(1, 1) NOT NULL,
		[tenderTableId]       [int] NOT NULL,
		[columnId]            [smallint] NOT NULL,
		[columnHeader]        [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[dataType]            [tinyint] NOT NULL,
		[filledBy]            [tinyint] NOT NULL,
		[columnType]          [varchar](115) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sortOrder]           [smallint] NOT NULL,
		[showorhide]          [varchar](14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[templateTableId]     [int] NOT NULL,
		CONSTRAINT [PK_tbl_TenderColumns]
		PRIMARY KEY
		CLUSTERED
		([tenderColId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderColumns]
	WITH CHECK
	ADD CONSTRAINT [FKB0D18388DA7D6BEE]
	FOREIGN KEY ([tenderTableId]) REFERENCES [dbo].[tbl_TenderTables] ([tenderTableId])
ALTER TABLE [dbo].[tbl_TenderColumns]
	CHECK CONSTRAINT [FKB0D18388DA7D6BEE]

GO
ALTER TABLE [dbo].[tbl_TenderColumns]
	WITH CHECK
	ADD CONSTRAINT [tenderTableId_FK1]
	FOREIGN KEY ([tenderTableId]) REFERENCES [dbo].[tbl_TenderTables] ([tenderTableId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderColumns]
	CHECK CONSTRAINT [tenderTableId_FK1]

GO
CREATE NONCLUSTERED INDEX [IX_tbl_TenderColumnsTenderTableIdFilledByDataType]
	ON [dbo].[tbl_TenderColumns] ([tenderTableId], [filledBy], [dataType])
	INCLUDE ([columnId], [columnHeader], [columnType])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TBlTenderColumns_filledBydataType]
	ON [dbo].[tbl_TenderColumns] ([filledBy], [dataType])
	INCLUDE ([tenderTableId], [columnId], [columnType])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderColumns] SET (LOCK_ESCALATION = TABLE)
GO
