SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Advertisement] (
		[adId]             [int] IDENTITY(1, 1) NOT NULL,
		[userName]         [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[bannerName]       [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[description]      [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[comments]         [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[duration]         [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[address]          [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createDate]       [smalldatetime] NOT NULL,
		[paymentDate]      [smalldatetime] NULL,
		[approvalDate]     [smalldatetime] NULL,
		[publishDate]      [smalldatetime] NULL,
		[expiryDate]       [smalldatetime] NULL,
		[location]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[dimension]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[bannerSize]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[price]            [money] NULL,
		[organization]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[eMailId]          [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[phoneNumber]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[url]              [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[status]           [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[transId]          [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[userId]           [int] NULL,
		CONSTRAINT [PK_tbl_Advertisement]
		PRIMARY KEY
		CLUSTERED
		([adId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_Advertisement] SET (LOCK_ESCALATION = TABLE)
GO
