SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_PrMaster] (
		[progressRepId]            [int] IDENTITY(1, 1) NOT NULL,
		[progressRepNo]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[progressRepcreatedDt]     [datetime] NOT NULL,
		[wpId]                     [int] NOT NULL,
		[status]                   [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[previousPrId]             [int] NULL,
		[createdBy]                [int] NOT NULL,
		[contractId]               [int] NOT NULL,
		[createdDate]              [datetime] NOT NULL,
		CONSTRAINT [progressRepId_PK]
		PRIMARY KEY
		CLUSTERED
		([progressRepId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_PrMaster]
	ADD
	CONSTRAINT [DF_tbl_CMS_PrMaster_status]
	DEFAULT ('pending') FOR [status]
GO
ALTER TABLE [dbo].[tbl_CMS_PrMaster]
	WITH CHECK
	ADD CONSTRAINT [cmsPrMasterContractId_FK2]
	FOREIGN KEY ([contractId]) REFERENCES [dbo].[tbl_ContractSign] ([contractSignId])
ALTER TABLE [dbo].[tbl_CMS_PrMaster]
	CHECK CONSTRAINT [cmsPrMasterContractId_FK2]

GO
ALTER TABLE [dbo].[tbl_CMS_PrMaster]
	WITH CHECK
	ADD CONSTRAINT [cmsPrMasterWpId_FK1]
	FOREIGN KEY ([wpId]) REFERENCES [dbo].[tbl_CMS_WpMaster] ([wpId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_PrMaster]
	CHECK CONSTRAINT [cmsPrMasterWpId_FK1]

GO
ALTER TABLE [dbo].[tbl_CMS_PrMaster] SET (LOCK_ESCALATION = TABLE)
GO
