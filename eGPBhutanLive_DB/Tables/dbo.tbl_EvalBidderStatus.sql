SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalBidderStatus] (
		[evaBidderStatusId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]              [int] NOT NULL,
		[userId]                [int] NOT NULL,
		[bidderStatus]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[evalBy]                [int] NOT NULL,
		[evalDt]                [smalldatetime] NOT NULL,
		[bidderMarks]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[evalStatus]            [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[passingMarks]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[remarks]               [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[result]                [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[pkgLotId]              [int] NOT NULL,
		[evalCount]             [int] NOT NULL,
		CONSTRAINT [PK_tbl_EvalBidderStatus]
		PRIMARY KEY
		CLUSTERED
		([evaBidderStatusId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalBidderStatus]
	ADD
	CONSTRAINT [DF__tbl_EvalB__evalC__5B438874]
	DEFAULT ('0') FOR [evalCount]
GO
ALTER TABLE [dbo].[tbl_EvalBidderStatus]
	ADD
	CONSTRAINT [DF_tbl_EvalBidderStatus_evalStatus]
	DEFAULT ('evaluation') FOR [evalStatus]
GO
ALTER TABLE [dbo].[tbl_EvalBidderStatus]
	ADD
	CONSTRAINT [DF_tbl_EvalBidderStatus_pkgLotId]
	DEFAULT ((0)) FOR [pkgLotId]
GO
ALTER TABLE [dbo].[tbl_EvalBidderStatus] SET (LOCK_ESCALATION = TABLE)
GO
