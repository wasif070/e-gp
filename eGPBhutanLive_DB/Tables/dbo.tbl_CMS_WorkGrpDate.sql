SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_WorkGrpDate] (
		[wrkGroupDateId]     [int] IDENTITY(1, 1) NOT NULL,
		[grpMasterId]        [int] NOT NULL,
		[startDate]          [datetime] NOT NULL,
		[endDate]            [datetime] NOT NULL,
		[createdBy]          [int] NOT NULL,
		[createdDate]        [datetime] NOT NULL,
		[tenderId]           [int] NOT NULL,
		[lotId]              [int] NOT NULL,
		[isCurrent]          [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cotractId]          [int] NOT NULL,
		CONSTRAINT [wrkGroupDateId_PK]
		PRIMARY KEY
		CLUSTERED
		([wrkGroupDateId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_WorkGrpDate]
	ADD
	CONSTRAINT [DF_tbl_CMS_WorkGrpDate_isCurrent]
	DEFAULT ('Yes') FOR [isCurrent]
GO
ALTER TABLE [dbo].[tbl_CMS_WorkGrpDate]
	WITH NOCHECK
	ADD CONSTRAINT [cmsWorkGrpDateContractId_FK3]
	FOREIGN KEY ([cotractId]) REFERENCES [dbo].[tbl_ContractSign] ([contractSignId])
ALTER TABLE [dbo].[tbl_CMS_WorkGrpDate]
	CHECK CONSTRAINT [cmsWorkGrpDateContractId_FK3]

GO
ALTER TABLE [dbo].[tbl_CMS_WorkGrpDate]
	WITH CHECK
	ADD CONSTRAINT [cmsWorkGrpDateGrpMasterId_FK1]
	FOREIGN KEY ([grpMasterId]) REFERENCES [dbo].[tbl_CMS_GrpMaster] ([grpMasterId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_WorkGrpDate]
	CHECK CONSTRAINT [cmsWorkGrpDateGrpMasterId_FK1]

GO
ALTER TABLE [dbo].[tbl_CMS_WorkGrpDate]
	WITH NOCHECK
	ADD CONSTRAINT [cmsWorkGrpMasterTenderId_FK2]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_WorkGrpDate]
	CHECK CONSTRAINT [cmsWorkGrpMasterTenderId_FK2]

GO
ALTER TABLE [dbo].[tbl_CMS_WorkGrpDate] SET (LOCK_ESCALATION = TABLE)
GO
