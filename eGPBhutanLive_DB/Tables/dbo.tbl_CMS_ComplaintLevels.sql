SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_ComplaintLevels] (
		[complaintLevelId]     [tinyint] IDENTITY(1, 1) NOT NULL,
		[complaintLevel]       [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_CMS_ComplaintLevels]
		PRIMARY KEY
		CLUSTERED
		([complaintLevelId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Complaint Level Name info. Stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintLevels', 'COLUMN', N'complaintLevel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'System generated Id stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintLevels', 'COLUMN', N'complaintLevelId'
GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintLevels] SET (LOCK_ESCALATION = TABLE)
GO
