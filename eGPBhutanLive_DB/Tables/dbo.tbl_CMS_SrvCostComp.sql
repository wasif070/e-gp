SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_SrvCostComp] (
		[srvCCId]           [int] IDENTITY(1, 1) NOT NULL,
		[rowId]             [int] NOT NULL,
		[tenderTableId]     [int] NOT NULL,
		[srNo]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[description]       [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[totalCost]         [decimal](15, 3) NOT NULL,
		[lotId]             [int] NOT NULL,
		[wpId]              [int] NOT NULL,
		CONSTRAINT [PK_tbl_CMS_SrvCostComp]
		PRIMARY KEY
		CLUSTERED
		([srvCCId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvCostComp] SET (LOCK_ESCALATION = TABLE)
GO
