SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_SrvWPHistory] (
		[srvWPHistId]       [int] IDENTITY(1, 1) NOT NULL,
		[srvWorkPlanId]     [int] NOT NULL,
		[srNo]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[activity]          [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[startDt]           [datetime] NOT NULL,
		[noOfDays]          [int] NOT NULL,
		[endDt]             [datetime] NOT NULL,
		[remarks]           [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[histCnt]           [int] NOT NULL,
		[srvFormMapId]      [int] NOT NULL,
		[createdDate]       [datetime] NULL,
		CONSTRAINT [PK_tbl_CMS_SrvWPHistory]
		PRIMARY KEY
		CLUSTERED
		([srvWPHistId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvWPHistory]
	WITH CHECK
	ADD CONSTRAINT [FK_srvWPhist_PK_srvWP]
	FOREIGN KEY ([srvWorkPlanId]) REFERENCES [dbo].[tbl_CMS_SrvWorkPlan] ([srvWorkPlanId])
	ON DELETE CASCADE
ALTER TABLE [dbo].[tbl_CMS_SrvWPHistory]
	CHECK CONSTRAINT [FK_srvWPhist_PK_srvWP]

GO
ALTER TABLE [dbo].[tbl_CMS_SrvWPHistory] SET (LOCK_ESCALATION = TABLE)
GO
