SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TCPermission] (
		[TCPermissionId]     [bigint] NOT NULL,
		[FromUserId]         [int] NOT NULL,
		[ToUserId]           [int] NOT NULL,
		[TenderId]           [int] NOT NULL,
		[CreatedBy]          [int] NOT NULL,
		[CreateDate]         [datetime] NOT NULL,
		[Status]             [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Comments]           [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_TCPermission]
		PRIMARY KEY
		CLUSTERED
		([TCPermissionId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TCPermission] SET (LOCK_ESCALATION = TABLE)
GO
