SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_FinancialPowerByRole] (
		[financialPowerByRoleId]     [int] IDENTITY(1, 1) NOT NULL,
		[procurementRoleId]          [tinyint] NOT NULL,
		[procurementNatureId]        [tinyint] NOT NULL,
		[procurementMethodId]        [tinyint] NOT NULL,
		[budgetTypeId]               [tinyint] NOT NULL,
		[operator]                   [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[fpAmount]                   [money] NOT NULL,
		[financialYearId]            [int] NOT NULL,
		CONSTRAINT [PK_tbl_FinancialPowerByRole]
		PRIMARY KEY
		CLUSTERED
		([financialPowerByRoleId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_FinancialPowerByRole]
	WITH NOCHECK
	ADD CONSTRAINT [FKD11188A3C4194A9]
	FOREIGN KEY ([procurementNatureId]) REFERENCES [dbo].[tbl_ProcurementNature] ([procurementNatureId])
ALTER TABLE [dbo].[tbl_FinancialPowerByRole]
	CHECK CONSTRAINT [FKD11188A3C4194A9]

GO
ALTER TABLE [dbo].[tbl_FinancialPowerByRole]
	WITH NOCHECK
	ADD CONSTRAINT [FKD11188AA4056EC7]
	FOREIGN KEY ([financialYearId]) REFERENCES [dbo].[tbl_FinancialYear] ([financialYearId])
ALTER TABLE [dbo].[tbl_FinancialPowerByRole]
	CHECK CONSTRAINT [FKD11188AA4056EC7]

GO
ALTER TABLE [dbo].[tbl_FinancialPowerByRole]
	WITH NOCHECK
	ADD CONSTRAINT [FKD11188AA5C4169D]
	FOREIGN KEY ([procurementMethodId]) REFERENCES [dbo].[tbl_ProcurementMethod] ([procurementMethodId])
ALTER TABLE [dbo].[tbl_FinancialPowerByRole]
	CHECK CONSTRAINT [FKD11188AA5C4169D]

GO
ALTER TABLE [dbo].[tbl_FinancialPowerByRole]
	WITH NOCHECK
	ADD CONSTRAINT [FKD11188AC90CE487]
	FOREIGN KEY ([procurementRoleId]) REFERENCES [dbo].[tbl_ProcurementRole] ([procurementRoleId])
ALTER TABLE [dbo].[tbl_FinancialPowerByRole]
	CHECK CONSTRAINT [FKD11188AC90CE487]

GO
ALTER TABLE [dbo].[tbl_FinancialPowerByRole]
	WITH NOCHECK
	ADD CONSTRAINT [FKD11188AD6793EF9]
	FOREIGN KEY ([budgetTypeId]) REFERENCES [dbo].[tbl_BudgetType] ([budgetTypeId])
ALTER TABLE [dbo].[tbl_FinancialPowerByRole]
	CHECK CONSTRAINT [FKD11188AD6793EF9]

GO
ALTER TABLE [dbo].[tbl_FinancialPowerByRole] SET (LOCK_ESCALATION = TABLE)
GO
