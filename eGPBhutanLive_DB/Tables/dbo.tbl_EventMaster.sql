SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EventMaster] (
		[eventId]       [smallint] IDENTITY(1, 1) NOT NULL,
		[eventName]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[moduleId]      [tinyint] NOT NULL,
		CONSTRAINT [eventId_PK]
		PRIMARY KEY
		CLUSTERED
		([eventId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EventMaster]
	WITH CHECK
	ADD CONSTRAINT [FK70AC67BD2A63DF5]
	FOREIGN KEY ([moduleId]) REFERENCES [dbo].[tbl_ModuleMaster] ([moduleId])
ALTER TABLE [dbo].[tbl_EventMaster]
	CHECK CONSTRAINT [FK70AC67BD2A63DF5]

GO
ALTER TABLE [dbo].[tbl_EventMaster]
	WITH CHECK
	ADD CONSTRAINT [moduleEvent_FK1]
	FOREIGN KEY ([moduleId]) REFERENCES [dbo].[tbl_ModuleMaster] ([moduleId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EventMaster]
	CHECK CONSTRAINT [moduleEvent_FK1]

GO
ALTER TABLE [dbo].[tbl_EventMaster] SET (LOCK_ESCALATION = TABLE)
GO
