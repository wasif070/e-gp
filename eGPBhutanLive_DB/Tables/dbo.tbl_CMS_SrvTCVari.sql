SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_SrvTCVari] (
		[srvTCVariId]          [int] IDENTITY(1, 1) NOT NULL,
		[empName]              [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[positionAssigned]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[organization]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[staffCat]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[positionDefined]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[areaOfExpertise]      [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[taskAssigned]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[srvFormMapId]         [int] NOT NULL,
		[srno]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[consultPropCat]       [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[variOrdId]            [int] NOT NULL,
		CONSTRAINT [PK_tbl_CMS_SrvTCVari]
		PRIMARY KEY
		CLUSTERED
		([srvTCVariId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvTCVari] SET (LOCK_ESCALATION = TABLE)
GO
