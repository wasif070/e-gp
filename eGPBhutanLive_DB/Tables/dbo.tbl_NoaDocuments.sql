SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_NoaDocuments] (
		[noaDocId]           [int] IDENTITY(1, 1) NOT NULL,
		[documentName]       [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]       [datetime] NOT NULL,
		[uploadedBy]         [int] NOT NULL,
		[tenderId]           [int] NOT NULL,
		[pkgLotId]           [int] NOT NULL,
		[userId]             [int] NOT NULL,
		[roundId]            [int] NOT NULL,
		CONSTRAINT [noaDocId_Pk]
		PRIMARY KEY
		CLUSTERED
		([noaDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_NoaDocuments]
	ADD
	CONSTRAINT [DF_tbl_NoaDocuments_pkgLotId]
	DEFAULT ((0)) FOR [pkgLotId]
GO
ALTER TABLE [dbo].[tbl_NoaDocuments]
	ADD
	CONSTRAINT [DF_tbl_NoaDocuments_roundId]
	DEFAULT ((1)) FOR [roundId]
GO
ALTER TABLE [dbo].[tbl_NoaDocuments]
	ADD
	CONSTRAINT [DF_tbl_NoaDocuments_tenderId]
	DEFAULT ((0)) FOR [tenderId]
GO
ALTER TABLE [dbo].[tbl_NoaDocuments]
	ADD
	CONSTRAINT [DF_tbl_NoaDocuments_userId]
	DEFAULT ((0)) FOR [userId]
GO
ALTER TABLE [dbo].[tbl_NoaDocuments] SET (LOCK_ESCALATION = TABLE)
GO
