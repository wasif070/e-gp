SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ReportColumnMaster] (
		[reportColId]       [int] IDENTITY(1, 1) NOT NULL,
		[reportTableId]     [int] NOT NULL,
		[columnId]          [int] NOT NULL,
		[colHeader]         [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[filledBy]          [tinyint] NOT NULL,
		[sortOrder]         [int] NOT NULL,
		[govCol]            [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [reportColId_pK]
		PRIMARY KEY
		CLUSTERED
		([reportColId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ReportColumnMaster]
	WITH CHECK
	ADD CONSTRAINT [colReportTableId_FK1]
	FOREIGN KEY ([reportTableId]) REFERENCES [dbo].[tbl_ReportTableMaster] ([reportTableId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ReportColumnMaster]
	CHECK CONSTRAINT [colReportTableId_FK1]

GO
ALTER TABLE [dbo].[tbl_ReportColumnMaster]
	WITH CHECK
	ADD CONSTRAINT [FKCD270AED8BD8F631]
	FOREIGN KEY ([reportTableId]) REFERENCES [dbo].[tbl_ReportTableMaster] ([reportTableId])
ALTER TABLE [dbo].[tbl_ReportColumnMaster]
	CHECK CONSTRAINT [FKCD270AED8BD8F631]

GO
ALTER TABLE [dbo].[tbl_ReportColumnMaster] SET (LOCK_ESCALATION = TABLE)
GO
