SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_SrvTCHistory] (
		[srvTCHistId]         [int] IDENTITY(1, 1) NOT NULL,
		[srvTCId]             [int] NOT NULL,
		[empName]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[organization]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[positionDefined]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[consultPropCat]      [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[areaOfExpertise]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[taskAssigned]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[histCnt]             [int] NOT NULL,
		[srvFormMapId]        [int] NULL,
		[createdDate]         [datetime] NULL,
		CONSTRAINT [PK_tbl_CMS_SrvTCHistory]
		PRIMARY KEY
		CLUSTERED
		([srvTCHistId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvTCHistory] SET (LOCK_ESCALATION = TABLE)
GO
