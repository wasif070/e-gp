SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_MessageTrash] (
		[msgTrashId]        [int] IDENTITY(1, 1) NOT NULL,
		[msgBoxTypeId]      [int] NOT NULL,
		[msgBoxType]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[msgId]             [int] NOT NULL,
		[msgToUserId]       [int] NULL,
		[msgFromUserId]     [int] NOT NULL,
		[msgToCCReply]      [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[msgStatus]         [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[creationDate]      [datetime] NOT NULL,
		[folderId]          [int] NOT NULL,
		[isPriority]        [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isMsgRead]         [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isDocUpLoaded]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[processUrl]        [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [msgTrashId_PK]
		PRIMARY KEY
		CLUSTERED
		([msgTrashId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_MessageTrash]
	WITH NOCHECK
	ADD CONSTRAINT [FK24CEF272A922AEE3]
	FOREIGN KEY ([msgId]) REFERENCES [dbo].[tbl_Message] ([msgId])
ALTER TABLE [dbo].[tbl_MessageTrash]
	CHECK CONSTRAINT [FK24CEF272A922AEE3]

GO
ALTER TABLE [dbo].[tbl_MessageTrash]
	WITH NOCHECK
	ADD CONSTRAINT [trashMsgId_FK1]
	FOREIGN KEY ([msgId]) REFERENCES [dbo].[tbl_Message] ([msgId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_MessageTrash]
	CHECK CONSTRAINT [trashMsgId_FK1]

GO
ALTER TABLE [dbo].[tbl_MessageTrash] SET (LOCK_ESCALATION = TABLE)
GO
