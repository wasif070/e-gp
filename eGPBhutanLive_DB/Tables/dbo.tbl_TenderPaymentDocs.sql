SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderPaymentDocs] (
		[paymentRefDocId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderPaymentId]     [int] NOT NULL,
		[documentName]        [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]      [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]             [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]        [smalldatetime] NOT NULL,
		[uploadedBy]          [int] NOT NULL,
		CONSTRAINT [PK_tbl_TenderPaymentDocs]
		PRIMARY KEY
		CLUSTERED
		([paymentRefDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderPaymentDocs]
	WITH CHECK
	ADD CONSTRAINT [docTenderPaymentId_FK1]
	FOREIGN KEY ([tenderPaymentId]) REFERENCES [dbo].[tbl_TenderPayment] ([tenderPaymentId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderPaymentDocs]
	CHECK CONSTRAINT [docTenderPaymentId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderPaymentDocs]
	WITH CHECK
	ADD CONSTRAINT [FKF2DA738C5A50885F]
	FOREIGN KEY ([tenderPaymentId]) REFERENCES [dbo].[tbl_TenderPayment] ([tenderPaymentId])
ALTER TABLE [dbo].[tbl_TenderPaymentDocs]
	CHECK CONSTRAINT [FKF2DA738C5A50885F]

GO
ALTER TABLE [dbo].[tbl_TenderPaymentDocs] SET (LOCK_ESCALATION = TABLE)
GO
