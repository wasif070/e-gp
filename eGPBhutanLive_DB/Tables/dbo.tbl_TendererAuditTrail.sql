SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TendererAuditTrail] (
		[regLogId]         [int] IDENTITY(1, 1) NOT NULL,
		[userId]           [int] NOT NULL,
		[activityDate]     [smalldatetime] NOT NULL,
		[activity]         [varchar](1050) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[activityBy]       [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [regLogId_PK]
		PRIMARY KEY
		CLUSTERED
		([regLogId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TendererAuditTrail]
	ADD
	CONSTRAINT [DF_tbl_TendererAuditTrail_activityDate]
	DEFAULT (((0)-(0))-(0)) FOR [activityDate]
GO
ALTER TABLE [dbo].[tbl_TendererAuditTrail]
	ADD
	CONSTRAINT [DF_tbl_TendererAuditTrail_userId]
	DEFAULT ((0)) FOR [userId]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Tenderer profile activity audit trail information stores here 
Registration , final submission ,Email sent,Approve,Reject,Blacklisted', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererAuditTrail', 'COLUMN', N'activity'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name of person who performs activity', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererAuditTrail', 'COLUMN', N'activityBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Activity  date & time information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererAuditTrail', 'COLUMN', N'activityDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'System generated Id  information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererAuditTrail', 'COLUMN', N'regLogId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'refer tbl_LoginMaster', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererAuditTrail', 'COLUMN', N'userId'
GO
ALTER TABLE [dbo].[tbl_TendererAuditTrail] SET (LOCK_ESCALATION = TABLE)
GO
