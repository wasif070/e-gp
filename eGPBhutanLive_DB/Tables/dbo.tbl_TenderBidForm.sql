SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_TenderBidForm] (
		[bidId]               [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]            [int] NOT NULL,
		[userId]              [int] NOT NULL,
		[tenderFormId]        [int] NOT NULL,
		[submittedTime]       [smallint] NOT NULL,
		[submissionDt]        [smalldatetime] NOT NULL,
		[pkgLotId]            [int] NULL,
		[tenderSectionId]     [int] NULL,
		CONSTRAINT [PK_tbl_TenderBidForm]
		PRIMARY KEY
		CLUSTERED
		([bidId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderBidForm]
	WITH CHECK
	ADD CONSTRAINT [bidTenderFormId_FK3]
	FOREIGN KEY ([tenderFormId]) REFERENCES [dbo].[tbl_TenderForms] ([tenderFormId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderBidForm]
	CHECK CONSTRAINT [bidTenderFormId_FK3]

GO
ALTER TABLE [dbo].[tbl_TenderBidForm]
	WITH CHECK
	ADD CONSTRAINT [FK7128230C735760EE]
	FOREIGN KEY ([tenderFormId]) REFERENCES [dbo].[tbl_TenderForms] ([tenderFormId])
ALTER TABLE [dbo].[tbl_TenderBidForm]
	CHECK CONSTRAINT [FK7128230C735760EE]

GO
ALTER TABLE [dbo].[tbl_TenderBidForm]
	WITH CHECK
	ADD CONSTRAINT [FK7128230CDB57665D]
	FOREIGN KEY ([tenderSectionId]) REFERENCES [dbo].[tbl_TenderSection] ([tenderSectionId])
ALTER TABLE [dbo].[tbl_TenderBidForm]
	CHECK CONSTRAINT [FK7128230CDB57665D]

GO
ALTER TABLE [dbo].[tbl_TenderBidForm]
	WITH NOCHECK
	ADD CONSTRAINT [tenderBidTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderBidForm]
	CHECK CONSTRAINT [tenderBidTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderBidForm] SET (LOCK_ESCALATION = TABLE)
GO
