SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TempCompanyJointVenture] (
		[ventureId]       [int] IDENTITY(1, 1) NOT NULL,
		[jvCompanyId]     [int] NOT NULL,
		[companyId]       [int] NOT NULL,
		[jvRole]          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [temp_ventureId_PK]
		PRIMARY KEY
		CLUSTERED
		([ventureId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TempCompanyJointVenture]
	WITH NOCHECK
	ADD CONSTRAINT [FK3C3D0BB12CE47E4B]
	FOREIGN KEY ([jvCompanyId]) REFERENCES [dbo].[tbl_CompanyMaster] ([companyId])
ALTER TABLE [dbo].[tbl_TempCompanyJointVenture]
	CHECK CONSTRAINT [FK3C3D0BB12CE47E4B]

GO
ALTER TABLE [dbo].[tbl_TempCompanyJointVenture]
	WITH NOCHECK
	ADD CONSTRAINT [FK3C3D0BB1E991DAA3]
	FOREIGN KEY ([companyId]) REFERENCES [dbo].[tbl_TempCompanyMaster] ([companyId])
ALTER TABLE [dbo].[tbl_TempCompanyJointVenture]
	CHECK CONSTRAINT [FK3C3D0BB1E991DAA3]

GO
ALTER TABLE [dbo].[tbl_TempCompanyJointVenture]
	WITH NOCHECK
	ADD CONSTRAINT [tempJointVentureCompId_FK1]
	FOREIGN KEY ([companyId]) REFERENCES [dbo].[tbl_TempCompanyMaster] ([companyId])
	ON DELETE CASCADE
ALTER TABLE [dbo].[tbl_TempCompanyJointVenture]
	CHECK CONSTRAINT [tempJointVentureCompId_FK1]

GO
ALTER TABLE [dbo].[tbl_TempCompanyJointVenture]
	WITH NOCHECK
	ADD CONSTRAINT [tempJvCompanyId_FK2]
	FOREIGN KEY ([jvCompanyId]) REFERENCES [dbo].[tbl_CompanyMaster] ([companyId])
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TempCompanyJointVenture]
	CHECK CONSTRAINT [tempJvCompanyId_FK2]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Refer tbl_CompanyMaster , Id of new joint venture company stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempCompanyJointVenture', 'COLUMN', N'companyId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Refer tbl_CompanyMaster , Id of company who is performing joint venture', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempCompanyJointVenture', 'COLUMN', N'jvCompanyId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Joint venture role information stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempCompanyJointVenture', 'COLUMN', N'jvRole'
GO
EXEC sp_addextendedproperty N'MS_Description', N'System generated id information stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempCompanyJointVenture', 'COLUMN', N'ventureId'
GO
ALTER TABLE [dbo].[tbl_TempCompanyJointVenture] SET (LOCK_ESCALATION = TABLE)
GO
