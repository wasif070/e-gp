SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_RoleMenuRights] (
		[Id]         [int] IDENTITY(1, 1) NOT NULL,
		[rollId]     [int] NOT NULL,
		[menuId]     [int] NOT NULL,
		CONSTRAINT [PK_tbl_RoleMenuRights]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_RoleMenuRights]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_RoleMenuRights_tbl_MenuMaster]
	FOREIGN KEY ([menuId]) REFERENCES [dbo].[tbl_MenuMaster] ([menuId])
ALTER TABLE [dbo].[tbl_RoleMenuRights]
	CHECK CONSTRAINT [FK_tbl_RoleMenuRights_tbl_MenuMaster]

GO
ALTER TABLE [dbo].[tbl_RoleMenuRights]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_RoleMenuRights_tbl_RoleMaster]
	FOREIGN KEY ([rollId]) REFERENCES [dbo].[tbl_RoleMaster] ([rollId])
ALTER TABLE [dbo].[tbl_RoleMenuRights]
	CHECK CONSTRAINT [FK_tbl_RoleMenuRights_tbl_RoleMaster]

GO
ALTER TABLE [dbo].[tbl_RoleMenuRights] SET (LOCK_ESCALATION = TABLE)
GO
