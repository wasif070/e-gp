SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_PartnerAdminTransfer] (
		[partTransId]           [int] IDENTITY(1, 1) NOT NULL,
		[userId]                [int] NOT NULL,
		[fullName]              [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[nationalId]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sBankDevelopId]        [int] NOT NULL,
		[isAdmin]               [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[designation]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isMakerChecker]        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[partnerId]             [int] NOT NULL,
		[transferedBy]          [int] NOT NULL,
		[transeredDt]           [datetime] NOT NULL,
		[emailId]               [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[replacedBy]            [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[replacedByEmailId]     [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[comments]              [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isCurrent]             [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_PartnerAdminTransfer]
		PRIMARY KEY
		CLUSTERED
		([partTransId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_PartnerAdminTransfer]
	WITH CHECK
	ADD CONSTRAINT [FK8DB62A736174DBD1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_PartnerAdminTransfer]
	CHECK CONSTRAINT [FK8DB62A736174DBD1]

GO
ALTER TABLE [dbo].[tbl_PartnerAdminTransfer]
	WITH CHECK
	ADD CONSTRAINT [partnerId_FK1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_PartnerAdminTransfer]
	CHECK CONSTRAINT [partnerId_FK1]

GO
ALTER TABLE [dbo].[tbl_PartnerAdminTransfer] SET (LOCK_ESCALATION = TABLE)
GO
