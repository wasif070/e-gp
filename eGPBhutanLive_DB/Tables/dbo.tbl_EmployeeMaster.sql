SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EmployeeMaster] (
		[employeeId]             [int] IDENTITY(1, 1) NOT NULL,
		[employeeName]           [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[employeeNameBangla]     [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[designationId]          [int] NOT NULL,
		[mobileNo]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[nationalId]             [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[userId]                 [int] NULL,
		[finPowerBy]             [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[createdBy]              [int] NULL,
		[userEmployeeId]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contactAddress]         [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_EmployeeMaster]
		PRIMARY KEY
		CLUSTERED
		([employeeId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EmployeeMaster]
	ADD
	CONSTRAINT [DF_tbl_EmployeeMaster_createdBy]
	DEFAULT ((0)) FOR [createdBy]
GO
ALTER TABLE [dbo].[tbl_EmployeeMaster]
	WITH CHECK
	ADD CONSTRAINT [employeeuserId_FK2]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_EmployeeMaster]
	CHECK CONSTRAINT [employeeuserId_FK2]

GO
ALTER TABLE [dbo].[tbl_EmployeeMaster]
	WITH CHECK
	ADD CONSTRAINT [FK75AB09716174DBD1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_EmployeeMaster]
	CHECK CONSTRAINT [FK75AB09716174DBD1]

GO
ALTER TABLE [dbo].[tbl_EmployeeMaster]
	WITH CHECK
	ADD CONSTRAINT [FK75AB0971A91B630B]
	FOREIGN KEY ([designationId]) REFERENCES [dbo].[tbl_DesignationMaster] ([designationId])
ALTER TABLE [dbo].[tbl_EmployeeMaster]
	CHECK CONSTRAINT [FK75AB0971A91B630B]

GO
ALTER TABLE [dbo].[tbl_EmployeeMaster] SET (LOCK_ESCALATION = TABLE)
GO
