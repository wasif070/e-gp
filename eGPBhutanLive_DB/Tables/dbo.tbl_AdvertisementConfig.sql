SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_AdvertisementConfig] (
		[adConfigId]       [int] IDENTITY(1, 1) NOT NULL,
		[userId]           [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[location]         [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[bannerSize]       [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[duration]         [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[instructions]     [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[fileFormat]       [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[dimensions]       [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createDate]       [smalldatetime] NOT NULL,
		[fee]              [money] NULL,
		CONSTRAINT [PK_tbl_AdvertisementConfig]
		PRIMARY KEY
		CLUSTERED
		([adConfigId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_AdvertisementConfig] SET (LOCK_ESCALATION = TABLE)
GO
