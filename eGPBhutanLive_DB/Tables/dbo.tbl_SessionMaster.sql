SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_SessionMaster] (
		[sessionId]          [int] IDENTITY(1, 1) NOT NULL,
		[userId]             [int] NOT NULL,
		[sessionStartDt]     [datetime] NOT NULL,
		[sessionEndDt]       [datetime] NOT NULL,
		[ipAddress]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [sessionId_PK]
		PRIMARY KEY
		CLUSTERED
		([sessionId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_SessionMaster]
	WITH CHECK
	ADD CONSTRAINT [FKBC8D2DB76174DBD1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_SessionMaster]
	CHECK CONSTRAINT [FKBC8D2DB76174DBD1]

GO
ALTER TABLE [dbo].[tbl_SessionMaster]
	WITH CHECK
	ADD CONSTRAINT [sessionUserId_FK1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_SessionMaster]
	CHECK CONSTRAINT [sessionUserId_FK1]

GO
ALTER TABLE [dbo].[tbl_SessionMaster] SET (LOCK_ESCALATION = TABLE)
GO
