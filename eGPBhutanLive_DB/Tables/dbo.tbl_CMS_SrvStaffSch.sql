SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_SrvStaffSch] (
		[srvSSId]          [int] IDENTITY(1, 1) NOT NULL,
		[srvTCId]          [int] NOT NULL,
		[workFrom]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[startDt]          [datetime] NOT NULL,
		[endDt]            [datetime] NOT NULL,
		[noOfDays]         [int] NOT NULL,
		[srNo]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[srvFormMapId]     [int] NOT NULL,
		[empName]          [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[variOrdId]        [int] NULL,
		CONSTRAINT [PK_tbl_CMS_SrvStaffSch]
		PRIMARY KEY
		CLUSTERED
		([srvSSId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvStaffSch] SET (LOCK_ESCALATION = TABLE)
GO
