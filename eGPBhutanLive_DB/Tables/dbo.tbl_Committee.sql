SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Committee] (
		[committeeId]        [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]           [int] NOT NULL,
		[committeeName]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]          [int] NOT NULL,
		[workflowStatus]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[committeStatus]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[minMembers]         [tinyint] NOT NULL,
		[maxMembers]         [tinyint] NOT NULL,
		[minMemOutSide]      [tinyint] NOT NULL,
		[committeeType]      [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isCurrent]          [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[remarks]            [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdDate]        [smalldatetime] NOT NULL,
		[publishDate]        [smalldatetime] NULL,
		[minMemFromPe]       [tinyint] NOT NULL,
		[minMemFromTec]      [tinyint] NOT NULL,
		CONSTRAINT [PK_tbl_Committee]
		PRIMARY KEY
		CLUSTERED
		([committeeId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_Committee]
	ADD
	CONSTRAINT [DF_tbl_Committee_minMemFromPe]
	DEFAULT ((0)) FOR [minMemFromPe]
GO
ALTER TABLE [dbo].[tbl_Committee]
	ADD
	CONSTRAINT [DF_tbl_Committee_minMemFromTec]
	DEFAULT ((0)) FOR [minMemFromTec]
GO
ALTER TABLE [dbo].[tbl_Committee]
	WITH NOCHECK
	ADD CONSTRAINT [committeeTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_Committee]
	CHECK CONSTRAINT [committeeTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_Committee]
	WITH NOCHECK
	ADD CONSTRAINT [FK9BADBC1C98B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_Committee]
	CHECK CONSTRAINT [FK9BADBC1C98B66EC5]

GO
EXEC sp_addextendedproperty N'MS_Description', N' system generated  unique Id information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_Committee', 'COLUMN', N'committeeId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'committeeName information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_Committee', 'COLUMN', N'committeeName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'committee type like Chairperson,Member Secretary,Member information stores here 
', 'SCHEMA', N'dbo', 'TABLE', N'tbl_Committee', 'COLUMN', N'committeeType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'committe status approved/reject   information stores here.', 'SCHEMA', N'dbo', 'TABLE', N'tbl_Committee', 'COLUMN', N'committeStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'who is creating the committe, information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_Committee', 'COLUMN', N'createdBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'committee creation date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_Committee', 'COLUMN', N'createdDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'is Current ( YES/No) information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_Committee', 'COLUMN', N'isCurrent'
GO
EXEC sp_addextendedproperty N'MS_Description', N'As per the business rule configured, system should display the minimum members required details to the user', 'SCHEMA', N'dbo', 'TABLE', N'tbl_Committee', 'COLUMN', N'maxMembers'
GO
EXEC sp_addextendedproperty N'MS_Description', N'As per the business rule configured, system should display the minimum members required details to the user', 'SCHEMA', N'dbo', 'TABLE', N'tbl_Committee', 'COLUMN', N'minMembers'
GO
EXEC sp_addextendedproperty N'MS_Description', N'As per the business rule configured, system should display the minimum members required details to the user', 'SCHEMA', N'dbo', 'TABLE', N'tbl_Committee', 'COLUMN', N'minMemOutSide'
GO
EXEC sp_addextendedproperty N'MS_Description', N'committe publish date creation date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_Committee', 'COLUMN', N'publishDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'committe remarks information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_Committee', 'COLUMN', N'remarks'
GO
EXEC sp_addextendedproperty N'MS_Description', N'refer tenderId from tbl_TenderMaster, to identify the  tenderId', 'SCHEMA', N'dbo', 'TABLE', N'tbl_Committee', 'COLUMN', N'tenderId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'work flow status information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_Committee', 'COLUMN', N'workflowStatus'
GO
ALTER TABLE [dbo].[tbl_Committee] SET (LOCK_ESCALATION = TABLE)
GO
