SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_UserRegInfo] (
		[userRegRptId]     [int] IDENTITY(1, 1) NOT NULL,
		[userId]           [int] NOT NULL,
		[expiryDate]       [datetime] NOT NULL,
		[actionDate]       [datetime] NOT NULL,
		[actionBy]         [int] NOT NULL,
		[action]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_RegistrationInfo]
		PRIMARY KEY
		CLUSTERED
		([userRegRptId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_UserRegInfo] SET (LOCK_ESCALATION = TABLE)
GO
