SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_SrvSSHistory] (
		[srvSSHistId]      [int] IDENTITY(1, 1) NOT NULL,
		[srvSSId]          [int] NOT NULL,
		[srvFormMapId]     [int] NOT NULL,
		[srno]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[srvTCId]          [int] NOT NULL,
		[workFrom]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[startDt]          [datetime] NOT NULL,
		[endDt]            [datetime] NOT NULL,
		[noOfDays]         [int] NOT NULL,
		[histCnt]          [int] NOT NULL,
		[createdDate]      [datetime] NULL,
		CONSTRAINT [PK_tbl_CMS_SrvSSHistory]
		PRIMARY KEY
		CLUSTERED
		([srvSSHistId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvSSHistory]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_CMS_SrvSSHistory_PK_tbl_CMS_SrvStaffSch]
	FOREIGN KEY ([srvSSId]) REFERENCES [dbo].[tbl_CMS_SrvStaffSch] ([srvSSId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_SrvSSHistory]
	CHECK CONSTRAINT [FK_tbl_CMS_SrvSSHistory_PK_tbl_CMS_SrvStaffSch]

GO
ALTER TABLE [dbo].[tbl_CMS_SrvSSHistory] SET (LOCK_ESCALATION = TABLE)
GO
