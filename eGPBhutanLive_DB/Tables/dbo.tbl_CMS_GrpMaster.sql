SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_GrpMaster] (
		[grpMasterId]        [int] IDENTITY(1, 1) NOT NULL,
		[tenderListDtId]     [int] NOT NULL,
		[groupId]            [int] NOT NULL,
		[groupValue]         [int] NOT NULL,
		[groupText]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[groupDefault]       [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[contratcId]         [int] NOT NULL,
		CONSTRAINT [grpMasterId_PK]
		PRIMARY KEY
		CLUSTERED
		([grpMasterId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_GrpMaster]
	ADD
	CONSTRAINT [DF_tbl_CMS_GrpMaster_groupDefault]
	DEFAULT ('No') FOR [groupDefault]
GO
ALTER TABLE [dbo].[tbl_CMS_GrpMaster]
	WITH NOCHECK
	ADD CONSTRAINT [cmsGrpMasterContractId_FK1]
	FOREIGN KEY ([contratcId]) REFERENCES [dbo].[tbl_ContractSign] ([contractSignId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_GrpMaster]
	CHECK CONSTRAINT [cmsGrpMasterContractId_FK1]

GO
ALTER TABLE [dbo].[tbl_CMS_GrpMaster] SET (LOCK_ESCALATION = TABLE)
GO
