SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TempAppPackages] (
		[packageId]               [int] NOT NULL,
		[appId]                   [int] NOT NULL,
		[procurementnature]       [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[servicesType]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[packageNo]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[packageDesc]             [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[allocateBudget]          [money] NOT NULL,
		[estimatedCost]           [money] NOT NULL,
		[cpvCode]                 [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[pkgEstCost]              [money] NOT NULL,
		[approvingAuthEmpId]      [int] NOT NULL,
		[isPQRequired]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[reoiRfaRequired]         [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[procurementMethodId]     [tinyint] NOT NULL,
		[procurementType]         [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sourceOfFund]            [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[appStatus]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[workflowStatus]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[noOfStages]              [tinyint] NOT NULL,
		[pkgUrgency]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[pkgEstCode]              [numeric](19, 2) NULL,
		[bidderCategory]          [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[workCategory]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[depoplanWork]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[entrustingAgency]        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[timeFrame]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [packageId_PKTemp]
		PRIMARY KEY
		CLUSTERED
		([packageId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TempAppPackages] SET (LOCK_ESCALATION = TABLE)
GO
