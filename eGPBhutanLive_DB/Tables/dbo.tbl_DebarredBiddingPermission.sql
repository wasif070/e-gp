SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_DebarredBiddingPermission] (
		[BiddingPermissionId]     [bigint] IDENTITY(1, 1) NOT NULL,
		[ProcurementCategory]     [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[WorkType]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[WorkCategroy]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CompanyID]               [int] NOT NULL,
		[UserId]                  [int] NOT NULL,
		[IsReapplied]             [int] NULL,
		[DebarredUpto]            [smalldatetime] NULL,
		[DebarredFrom]            [smalldatetime] NULL,
		[Comments]                [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DebarredDate]            [smalldatetime] NULL,
		[DebarredBy]              [int] NULL,
		[DebarredReason]          [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ReinstateComments]       [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]                [int] NULL,
		CONSTRAINT [PK_tbl_DebarredBiddingPermission]
		PRIMARY KEY
		CLUSTERED
		([BiddingPermissionId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_DebarredBiddingPermission] SET (LOCK_ESCALATION = TABLE)
GO
