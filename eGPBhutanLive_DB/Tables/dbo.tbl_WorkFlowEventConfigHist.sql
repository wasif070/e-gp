SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_WorkFlowEventConfigHist] (
		[wfEvtConfId]       [int] IDENTITY(1, 1) NOT NULL,
		[noOfReviewer]      [smallint] NOT NULL,
		[noOfDays]          [smallint] NOT NULL,
		[isDonorConReq]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[eventId]           [smallint] NOT NULL,
		[objectId]          [int] NOT NULL,
		[actionDate]        [smalldatetime] NOT NULL,
		[actionBy]          [int] NOT NULL,
		[action]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_WorkFlowEventConfigHist]
		PRIMARY KEY
		CLUSTERED
		([wfEvtConfId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_WorkFlowEventConfigHist] SET (LOCK_ESCALATION = TABLE)
GO
