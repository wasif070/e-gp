SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalCpMemClarification] (
		[evalCpQueId]         [int] IDENTITY(1, 1) NOT NULL,
		[evalMemStatusId]     [int] NOT NULL,
		[cpQuestion]          [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[memAnswer]           [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cpQuestionDt]        [smalldatetime] NOT NULL,
		[memAnwserDt]         [smalldatetime] NULL,
		[cpQuestionBy]        [int] NOT NULL,
		[memAnswerBy]         [int] NOT NULL,
		[tenderFormId]        [int] NOT NULL,
		[userId]              [int] NOT NULL,
		[noteOfDescent]       [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cpcomments]          [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cpGovUserId]         [int] NULL,
		[memGovUserId]        [int] NULL,
		CONSTRAINT [PK_tbl_EvalCpMemClarification]
		PRIMARY KEY
		CLUSTERED
		([evalCpQueId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalCpMemClarification]
	ADD
	CONSTRAINT [DF_tbl_EvalCpMemClarification_noteOfDescent]
	DEFAULT ('No') FOR [noteOfDescent]
GO
ALTER TABLE [dbo].[tbl_EvalCpMemClarification]
	ADD
	CONSTRAINT [DF_tbl_EvalCpMemClarification_tenderFormId]
	DEFAULT ((0)) FOR [tenderFormId]
GO
ALTER TABLE [dbo].[tbl_EvalCpMemClarification]
	ADD
	CONSTRAINT [DF_tbl_EvalCpMemClarification_userId]
	DEFAULT ((0)) FOR [userId]
GO
ALTER TABLE [dbo].[tbl_EvalCpMemClarification] SET (LOCK_ESCALATION = TABLE)
GO
