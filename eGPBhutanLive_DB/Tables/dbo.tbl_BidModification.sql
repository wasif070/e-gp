SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_BidModification] (
		[bidModId]      [int] IDENTITY(1, 1) NOT NULL,
		[bidStatus]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[comments]      [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[bidModDt]      [smalldatetime] NOT NULL,
		[tenderId]      [int] NOT NULL,
		[userId]        [int] NOT NULL,
		[ipAddress]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_BidModification]
		PRIMARY KEY
		CLUSTERED
		([bidModId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_BidModification]
	WITH NOCHECK
	ADD CONSTRAINT [bidModTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_BidModification]
	CHECK CONSTRAINT [bidModTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_BidModification] SET (LOCK_ESCALATION = TABLE)
GO
