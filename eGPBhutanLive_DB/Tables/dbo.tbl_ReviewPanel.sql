SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ReviewPanel] (
		[reviewPanelId]       [int] IDENTITY(1, 1) NOT NULL,
		[reviewPanelName]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[userId]              [int] NOT NULL,
		[user_userId]         [int] NULL,
		[reviewMembers]       [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_ReviewPanel]
		PRIMARY KEY
		CLUSTERED
		([reviewPanelId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ReviewPanel]
	WITH CHECK
	ADD CONSTRAINT [FK5C23126B8DAE22C5]
	FOREIGN KEY ([user_userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_ReviewPanel]
	CHECK CONSTRAINT [FK5C23126B8DAE22C5]

GO
ALTER TABLE [dbo].[tbl_ReviewPanel] SET (LOCK_ESCALATION = TABLE)
GO
