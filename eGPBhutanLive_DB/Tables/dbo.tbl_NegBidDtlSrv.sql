SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_NegBidDtlSrv] (
		[negBidDtlId]        [int] IDENTITY(1, 1) NOT NULL,
		[negBidTableId]      [int] NOT NULL,
		[tenderColumnId]     [int] NOT NULL,
		[tenderTableId]      [int] NOT NULL,
		[cellValue]          [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[rowId]              [int] NOT NULL,
		[cellId]             [int] NOT NULL,
		[formId]             [int] NOT NULL,
		[negId]              [int] NOT NULL,
		CONSTRAINT [PK_tbl_NegBidDtlSrv]
		PRIMARY KEY
		CLUSTERED
		([negBidDtlId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_NegBidDtlSrv]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_NegBidDtlSrv_tbl_NegBidTable]
	FOREIGN KEY ([negBidTableId]) REFERENCES [dbo].[tbl_NegBidTable] ([negBidTableId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_NegBidDtlSrv]
	CHECK CONSTRAINT [FK_tbl_NegBidDtlSrv_tbl_NegBidTable]

GO
ALTER TABLE [dbo].[tbl_NegBidDtlSrv] SET (LOCK_ESCALATION = TABLE)
GO
