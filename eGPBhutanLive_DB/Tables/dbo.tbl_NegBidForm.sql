SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_NegBidForm] (
		[negBidFormId]     [int] IDENTITY(1, 1) NOT NULL,
		[negId]            [int] NOT NULL,
		[bidId]            [int] NOT NULL,
		[tenderFormId]     [int] NOT NULL,
		[userId]           [int] NOT NULL,
		[submissionDt]     [datetime] NOT NULL,
		CONSTRAINT [PK_tbl_NegBidForm]
		PRIMARY KEY
		CLUSTERED
		([negBidFormId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_NegBidForm] SET (LOCK_ESCALATION = TABLE)
GO
