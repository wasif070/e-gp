SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_InvoiceMaster] (
		[invoiceId]           [int] IDENTITY(1, 1) NOT NULL,
		[totalInvAmt]         [decimal](15, 3) NOT NULL,
		[wpId]                [int] NOT NULL,
		[createdDate]         [datetime] NOT NULL,
		[createdBy]           [int] NOT NULL,
		[invStatus]           [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tillPRId]            [int] NOT NULL,
		[tenderId]            [int] NOT NULL,
		[remarks]             [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[isAdvInv]            [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[invoiceNo]           [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[conRemarks]          [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[releasedDtNTime]     [datetime] NULL,
		CONSTRAINT [PK_tbl_CMS_InvoiceMaster]
		PRIMARY KEY
		CLUSTERED
		([invoiceId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_InvoiceMaster]
	WITH CHECK
	ADD CONSTRAINT [FKWpId_InvoiceId]
	FOREIGN KEY ([wpId]) REFERENCES [dbo].[tbl_CMS_WpMaster] ([wpId])
ALTER TABLE [dbo].[tbl_CMS_InvoiceMaster]
	CHECK CONSTRAINT [FKWpId_InvoiceId]

GO
ALTER TABLE [dbo].[tbl_CMS_InvoiceMaster] SET (LOCK_ESCALATION = TABLE)
GO
