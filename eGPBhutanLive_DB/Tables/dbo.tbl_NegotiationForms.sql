SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_NegotiationForms] (
		[negFormId]      [int] IDENTITY(1, 1) NOT NULL,
		[negId]          [int] NOT NULL,
		[formId]         [int] NOT NULL,
		[mappedBy]       [int] NOT NULL,
		[mappedDate]     [smalldatetime] NOT NULL,
		CONSTRAINT [PK_tbl_NegotiationForms]
		PRIMARY KEY
		CLUSTERED
		([negFormId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_NegotiationForms] SET (LOCK_ESCALATION = TABLE)
GO
