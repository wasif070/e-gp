SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_CMS_SrvREHistory] (
		[srvREHistId]       [int] IDENTITY(1, 1) NOT NULL,
		[srvFormMapId]      [int] NOT NULL,
		[histCount]         [int] NOT NULL,
		[qty]               [decimal](15, 3) NOT NULL,
		[unitCost]          [decimal](15, 3) NOT NULL,
		[rowId]             [int] NOT NULL,
		[tenderTableId]     [int] NOT NULL,
		[srvREId]           [int] NOT NULL,
		CONSTRAINT [PK_tbl_CMS_SrvREHistory]
		PRIMARY KEY
		CLUSTERED
		([srvREHistId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvREHistory]
	WITH NOCHECK
	ADD CONSTRAINT [cmsSrvReyHistoryId_FK1]
	FOREIGN KEY ([srvREId]) REFERENCES [dbo].[tbl_CMS_SrvReExpense] ([srvREId])
ALTER TABLE [dbo].[tbl_CMS_SrvREHistory]
	CHECK CONSTRAINT [cmsSrvReyHistoryId_FK1]

GO
ALTER TABLE [dbo].[tbl_CMS_SrvREHistory] SET (LOCK_ESCALATION = TABLE)
GO
