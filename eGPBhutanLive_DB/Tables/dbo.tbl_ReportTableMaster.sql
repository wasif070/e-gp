SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ReportTableMaster] (
		[reportTableId]      [int] IDENTITY(1, 1) NOT NULL,
		[reportId]           [int] NOT NULL,
		[noOfCols]           [int] NOT NULL,
		[rptTableHeader]     [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[rptTableFooter]     [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[pkglotId]           [int] NOT NULL,
		CONSTRAINT [reportTableId_pk]
		PRIMARY KEY
		CLUSTERED
		([reportTableId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ReportTableMaster]
	WITH CHECK
	ADD CONSTRAINT [FK46153F9BB55AD385]
	FOREIGN KEY ([reportId]) REFERENCES [dbo].[tbl_ReportMaster] ([reportId])
ALTER TABLE [dbo].[tbl_ReportTableMaster]
	CHECK CONSTRAINT [FK46153F9BB55AD385]

GO
ALTER TABLE [dbo].[tbl_ReportTableMaster]
	WITH CHECK
	ADD CONSTRAINT [masterReportId_FK1]
	FOREIGN KEY ([reportId]) REFERENCES [dbo].[tbl_ReportMaster] ([reportId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ReportTableMaster]
	CHECK CONSTRAINT [masterReportId_FK1]

GO
ALTER TABLE [dbo].[tbl_ReportTableMaster] SET (LOCK_ESCALATION = TABLE)
GO
