SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ActionMaster] (
		[actionId]     [tinyint] IDENTITY(0, 1) NOT NULL,
		[action]       [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [actionId_PK]
		PRIMARY KEY
		CLUSTERED
		([actionId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ActionMaster] SET (LOCK_ESCALATION = TABLE)
GO
