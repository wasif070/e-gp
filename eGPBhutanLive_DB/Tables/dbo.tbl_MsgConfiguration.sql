SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_MsgConfiguration] (
		[msgId]        [int] IDENTITY(1, 1) NOT NULL,
		[msgKey]       [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[msgValue]     [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_MsgConfiguration]
		PRIMARY KEY
		CLUSTERED
		([msgId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_MsgConfiguration] SET (LOCK_ESCALATION = TABLE)
GO
