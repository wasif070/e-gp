SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_IttClause] (
		[ittClauseId]       [int] IDENTITY(1, 1) NOT NULL,
		[ittHeaderId]       [int] NOT NULL,
		[ittClauseName]     [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [ittClauseId_PK]
		PRIMARY KEY
		CLUSTERED
		([ittClauseId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_IttClause]
	WITH CHECK
	ADD CONSTRAINT [FK476FF417FB8A1B27]
	FOREIGN KEY ([ittHeaderId]) REFERENCES [dbo].[tbl_IttHeader] ([ittHeaderId])
ALTER TABLE [dbo].[tbl_IttClause]
	CHECK CONSTRAINT [FK476FF417FB8A1B27]

GO
ALTER TABLE [dbo].[tbl_IttClause]
	WITH CHECK
	ADD CONSTRAINT [ittHeaderId_FK1]
	FOREIGN KEY ([ittHeaderId]) REFERENCES [dbo].[tbl_IttHeader] ([ittHeaderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_IttClause]
	CHECK CONSTRAINT [ittHeaderId_FK1]

GO
ALTER TABLE [dbo].[tbl_IttClause] SET (LOCK_ESCALATION = TABLE)
GO
