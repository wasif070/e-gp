SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_InvoiceDocument] (
		[invoiceDocId]       [int] IDENTITY(1, 1) NOT NULL,
		[invoiceId]          [int] NOT NULL,
		[documentName]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]     [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedBy]         [int] NOT NULL,
		[uploadedDate]       [datetime] NOT NULL,
		[userTypeId]         [int] NOT NULL,
		CONSTRAINT [invoiceDocId_PK]
		PRIMARY KEY
		CLUSTERED
		([invoiceDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_InvoiceDocument]
	WITH CHECK
	ADD CONSTRAINT [FKInvoiceIdPK_InvoiceMaster]
	FOREIGN KEY ([invoiceId]) REFERENCES [dbo].[tbl_CMS_InvoiceMaster] ([invoiceId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_InvoiceDocument]
	CHECK CONSTRAINT [FKInvoiceIdPK_InvoiceMaster]

GO
ALTER TABLE [dbo].[tbl_CMS_InvoiceDocument] SET (LOCK_ESCALATION = TABLE)
GO
