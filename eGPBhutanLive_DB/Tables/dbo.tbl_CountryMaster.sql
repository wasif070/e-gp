SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CountryMaster] (
		[countryId]       [smallint] IDENTITY(1, 1) NOT NULL,
		[countryName]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[countryCode]     [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [countryId_PK]
		PRIMARY KEY
		CLUSTERED
		([countryId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CountryMaster]
	ADD
	CONSTRAINT [DF_tbl_CountryMaster_countryCode]
	DEFAULT ((91)) FOR [countryCode]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Unique id of country stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CountryMaster', 'COLUMN', N'countryId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name of country stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CountryMaster', 'COLUMN', N'countryName'
GO
ALTER TABLE [dbo].[tbl_CountryMaster] SET (LOCK_ESCALATION = TABLE)
GO
