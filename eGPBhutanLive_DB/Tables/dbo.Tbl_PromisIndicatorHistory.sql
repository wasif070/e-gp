SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_PromisIndicatorHistory] (
		[IndicatorHist_ID]               [int] IDENTITY(1, 1) NOT NULL,
		[officeId]                       [int] NOT NULL,
		[PEOffice_Name]                  [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[tenderId]                       [int] NULL,
		[PublishNewspaper]               [int] NULL,
		[PublishCPTUWebsite]             [int] NULL,
		[GoBProcRules]                   [int] NULL,
		[DPRules]                        [int] NULL,
		[MultiLocSubmission]             [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DaysBetTenPubTenderClosing]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SuffTenderSubTime]              [int] NULL,
		[TenderDocSold]                  [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TenderersPartCount]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TenderSubVSDocSold]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TOCMemberFromTEC]               [int] NULL,
		[TECFormedByAA]                  [int] NULL,
		[TECExternalMembers]             [int] NULL,
		[EvaluationTime]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EvalCompletedInTime]            [int] NULL,
		[ResponsiveTenderersCount]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsReTendered]                   [int] NULL,
		[IsTenderCancelled]              [int] NULL,
		[DaysBtwnTenEvalTenApp]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsApprovedByProperAA]           [int] NULL,
		[SubofEvalReportAA]              [int] NULL,
		[AppofContractByAAInTimr]        [int] NULL,
		[TERReviewByOther]               [int] NULL,
		[ContractAppByHighAuth]          [int] NULL,
		[DaysBtwnContappNOA]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DaysBtwnTenOpenNOA]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DaysBtwnIFTNOA]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AwardPubCPTUWebsite]            [int] NULL,
		[AwardedInTenValidityPeriod]     [int] NULL,
		[DeliveredInTime]                [int] NULL,
		[LiquidatedDamage]               [int] NULL,
		[FullyCompleted]                 [int] NULL,
		[DaysReleasePayment]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LatePayment]                    [int] NULL,
		[InterestPaid]                   [int] NULL,
		[ComplaintReceived]              [int] NULL,
		[ResolAwardModification]         [int] NULL,
		[ComplaintResolved]              [int] NULL,
		[RevPanelDecUpheld]              [int] NULL,
		[IsContAmended]                  [int] NULL,
		[ContUnresolvDispute]            [int] NULL,
		[DetFraudCorruption]             [int] NULL,
		[Ministry_ID]                    [int] NULL,
		[Division_ID]                    [int] NULL,
		[Org_ID]                         [int] NULL,
		[Modify_Date]                    [smalldatetime] NULL,
		[Is_TenderOnline]                [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Is_Progress]                    [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[prmsPECode]                     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_Tbl_PromisIndicatorHistory]
		PRIMARY KEY
		CLUSTERED
		([IndicatorHist_ID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tbl_PromisIndicatorHistory] SET (LOCK_ESCALATION = TABLE)
GO
