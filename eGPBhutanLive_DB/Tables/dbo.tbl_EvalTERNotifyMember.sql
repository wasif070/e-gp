SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalTERNotifyMember] (
		[evalNotId]      [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]       [int] NOT NULL,
		[pkgLotId]       [int] NOT NULL,
		[sentBy]         [int] NOT NULL,
		[sentDt]         [datetime] NOT NULL,
		[reportType]     [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[notifyType]     [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[roundId]        [int] NOT NULL,
		CONSTRAINT [PK_tbl_EvalTERNotifyMembar]
		PRIMARY KEY
		CLUSTERED
		([evalNotId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalTERNotifyMember]
	ADD
	CONSTRAINT [DF_tbl_EvalTERNotifyMembar_notifyType]
	DEFAULT ('notify') FOR [notifyType]
GO
ALTER TABLE [dbo].[tbl_EvalTERNotifyMember]
	ADD
	CONSTRAINT [DF_tbl_EvalTERNotifyMembar_sentDt]
	DEFAULT (getdate()) FOR [sentDt]
GO
ALTER TABLE [dbo].[tbl_EvalTERNotifyMember]
	ADD
	CONSTRAINT [DF_tbl_EvalTERNotifyMember_roundId]
	DEFAULT ((1)) FOR [roundId]
GO
ALTER TABLE [dbo].[tbl_EvalTERNotifyMember] SET (LOCK_ESCALATION = TABLE)
GO
