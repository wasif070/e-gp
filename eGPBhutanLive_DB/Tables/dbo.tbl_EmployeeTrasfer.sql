SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EmployeeTrasfer] (
		[govUserId]              [int] IDENTITY(1, 1) NOT NULL,
		[userId]                 [int] NOT NULL,
		[transferDt]             [datetime] NOT NULL,
		[createdBy]              [int] NOT NULL,
		[remarks]                [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[employeeName]           [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[employeeNameBangla]     [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[mobileNo]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[nationalId]             [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[finPowerBy]             [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[employeeId]             [int] NOT NULL,
		[action]                 [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[emailId]                [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[replacedBy]             [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[replacedByEmailId]      [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isCurrent]              [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[comments]               [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[userEmployeeId]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contactAddress]         [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_EmployeeTrasfer]
		PRIMARY KEY
		CLUSTERED
		([govUserId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EmployeeTrasfer]
	ADD
	CONSTRAINT [DF_tbl_EmployeeTrasfer_isCurrent]
	DEFAULT ('No') FOR [isCurrent]
GO
ALTER TABLE [dbo].[tbl_EmployeeTrasfer]
	WITH CHECK
	ADD CONSTRAINT [FKCE060D346174DBD1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_EmployeeTrasfer]
	CHECK CONSTRAINT [FKCE060D346174DBD1]

GO
ALTER TABLE [dbo].[tbl_EmployeeTrasfer]
	WITH CHECK
	ADD CONSTRAINT [userLogin_FK1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EmployeeTrasfer]
	CHECK CONSTRAINT [userLogin_FK1]

GO
ALTER TABLE [dbo].[tbl_EmployeeTrasfer] SET (LOCK_ESCALATION = TABLE)
GO
