SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderForms] (
		[tenderFormId]          [int] IDENTITY(1, 1) NOT NULL,
		[tenderSectionId]       [int] NOT NULL,
		[templateFormId]        [int] NOT NULL,
		[filledBy]              [tinyint] NOT NULL,
		[formName]              [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[formHeader]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[formFooter]            [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[noOfTables]            [tinyint] NOT NULL,
		[isMultipleFilling]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isEncryption]          [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isPriceBid]            [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isMandatory]           [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[formStatus]            [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[pkgLotId]              [int] NOT NULL,
		[FormType]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[isSalvage]             [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_TenderForms]
		PRIMARY KEY
		CLUSTERED
		([tenderFormId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderForms]
	ADD
	CONSTRAINT [DF_tbl_TenderForms_isMandatory]
	DEFAULT ('no') FOR [isMandatory]
GO
ALTER TABLE [dbo].[tbl_TenderForms]
	ADD
	CONSTRAINT [DF_tbl_TenderForms_pkgLotId]
	DEFAULT ((0)) FOR [pkgLotId]
GO
ALTER TABLE [dbo].[tbl_TenderForms]
	WITH CHECK
	ADD CONSTRAINT [tenderFormTenderSectionId_FK1]
	FOREIGN KEY ([tenderSectionId]) REFERENCES [dbo].[tbl_TenderSection] ([tenderSectionId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderForms]
	CHECK CONSTRAINT [tenderFormTenderSectionId_FK1]

GO
CREATE NONCLUSTERED INDEX [IX_tbl_TenderFormsTenderSectionId]
	ON [dbo].[tbl_TenderForms] ([tenderSectionId])
	INCLUDE ([tenderFormId], [templateFormId], [formStatus], [FormType])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderForms] SET (LOCK_ESCALATION = TABLE)
GO
