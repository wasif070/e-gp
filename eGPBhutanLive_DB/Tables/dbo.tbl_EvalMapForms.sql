SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalMapForms] (
		[evalFormId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]       [int] NOT NULL,
		[pkgLotId]       [int] NOT NULL,
		[formId]         [int] NOT NULL,
		[tecCom]         [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tscCom]         [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[configBy]       [int] NOT NULL,
		[configDate]     [smalldatetime] NOT NULL,
		CONSTRAINT [evalTscFormId_PK1]
		PRIMARY KEY
		CLUSTERED
		([evalFormId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalMapForms]
	WITH NOCHECK
	ADD CONSTRAINT [evalTscFrmTenderId_Fk1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EvalMapForms]
	CHECK CONSTRAINT [evalTscFrmTenderId_Fk1]

GO
ALTER TABLE [dbo].[tbl_EvalMapForms] SET (LOCK_ESCALATION = TABLE)
GO
