SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CpvClassification] (
		[CpvId]                [int] NOT NULL,
		[CpvDescription]       [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CpvCode]              [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CpvDivision]          [tinyint] NOT NULL,
		[CpvGroup]             [tinyint] NOT NULL,
		[CpvClass]             [tinyint] NOT NULL,
		[CpvCategory]          [int] NOT NULL,
		[CpvSubCategory]       [tinyint] NOT NULL,
		[CpvSubCategoryL2]     [tinyint] NOT NULL,
		[Expanded]             [int] NOT NULL,
		CONSTRAINT [PK_tbl_CpvClassification]
		PRIMARY KEY
		CLUSTERED
		([CpvId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CpvClassification] SET (LOCK_ESCALATION = TABLE)
GO
