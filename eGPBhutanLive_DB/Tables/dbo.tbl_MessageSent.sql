SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_MessageSent] (
		[msgSentId]         [int] IDENTITY(1, 1) NOT NULL,
		[msgId]             [int] NOT NULL,
		[msgToUserId]       [int] NOT NULL,
		[msgFromUserId]     [int] NOT NULL,
		[msgToCCReply]      [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[msgStatus]         [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[creationDate]      [datetime] NOT NULL,
		[folderId]          [int] NOT NULL,
		[isPriority]        [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isMsgRead]         [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isDocUploaded]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [msgSentId_PK]
		PRIMARY KEY
		CLUSTERED
		([msgSentId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_MessageSent]
	WITH CHECK
	ADD CONSTRAINT [FKD7E5011E2D37788D]
	FOREIGN KEY ([msgToUserId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_MessageSent]
	CHECK CONSTRAINT [FKD7E5011E2D37788D]

GO
ALTER TABLE [dbo].[tbl_MessageSent]
	WITH CHECK
	ADD CONSTRAINT [FKD7E5011E3876063C]
	FOREIGN KEY ([msgFromUserId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_MessageSent]
	CHECK CONSTRAINT [FKD7E5011E3876063C]

GO
ALTER TABLE [dbo].[tbl_MessageSent]
	WITH CHECK
	ADD CONSTRAINT [FKD7E5011EA922AEE3]
	FOREIGN KEY ([msgId]) REFERENCES [dbo].[tbl_Message] ([msgId])
ALTER TABLE [dbo].[tbl_MessageSent]
	CHECK CONSTRAINT [FKD7E5011EA922AEE3]

GO
ALTER TABLE [dbo].[tbl_MessageSent]
	WITH CHECK
	ADD CONSTRAINT [msgSent_FK1]
	FOREIGN KEY ([msgId]) REFERENCES [dbo].[tbl_Message] ([msgId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_MessageSent]
	CHECK CONSTRAINT [msgSent_FK1]

GO
ALTER TABLE [dbo].[tbl_MessageSent]
	WITH CHECK
	ADD CONSTRAINT [msgSentToUserId_FK2]
	FOREIGN KEY ([msgToUserId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_MessageSent]
	CHECK CONSTRAINT [msgSentToUserId_FK2]

GO
ALTER TABLE [dbo].[tbl_MessageSent] SET (LOCK_ESCALATION = TABLE)
GO
