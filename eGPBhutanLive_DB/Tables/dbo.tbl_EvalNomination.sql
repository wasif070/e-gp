SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalNomination] (
		[nomineeId]           [int] IDENTITY(1, 1) NOT NULL,
		[evalConfigId]        [int] NOT NULL,
		[tenderId]            [int] NOT NULL,
		[envelopeId]          [int] NULL,
		[nomineeUserId]       [int] NOT NULL,
		[nominationDate]      [smalldatetime] NOT NULL,
		[nomineeAction]       [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[evaltype]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[nomineeStatus]       [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[remarks]             [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[isCurrent]           [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[nomineeActionDt]     [smalldatetime] NULL,
		CONSTRAINT [PK_tbl_EvalNomination]
		PRIMARY KEY
		CLUSTERED
		([nomineeId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalNomination]
	ADD
	CONSTRAINT [DF_tbl_EvalNomination_isCurrent]
	DEFAULT ('Yes') FOR [isCurrent]
GO
ALTER TABLE [dbo].[tbl_EvalNomination] SET (LOCK_ESCALATION = TABLE)
GO
