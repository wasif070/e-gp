SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderBidSign] (
		[bidSignId]     [int] IDENTITY(1, 1) NOT NULL,
		[signText]      [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[signDt]        [smalldatetime] NOT NULL,
		[bidId]         [int] NULL,
		CONSTRAINT [bidSignId_PK]
		PRIMARY KEY
		CLUSTERED
		([bidSignId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderBidSign]
	WITH CHECK
	ADD CONSTRAINT [tenderBidSign_FK1]
	FOREIGN KEY ([bidId]) REFERENCES [dbo].[tbl_TenderBidForm] ([bidId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderBidSign]
	CHECK CONSTRAINT [tenderBidSign_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderBidSign] SET (LOCK_ESCALATION = TABLE)
GO
