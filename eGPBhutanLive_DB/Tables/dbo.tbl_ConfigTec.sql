SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ConfigTec] (
		[configTec]               [int] IDENTITY(1, 1) NOT NULL,
		[committeeType]           [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[procurementNatureId]     [tinyint] NOT NULL,
		[minTenderVal]            [money] NOT NULL,
		[maxTenderVal]            [money] NOT NULL,
		[minMemReq]               [tinyint] NOT NULL,
		[maxMemReq]               [tinyint] NOT NULL,
		[minMemOutSidePe]         [tinyint] NOT NULL,
		[minMemFromPe]            [tinyint] NOT NULL,
		[minMemFromTec]           [tinyint] NOT NULL,
		[minApproval]             [tinyint] NULL,
		CONSTRAINT [PK_tbl_ConfigTec]
		PRIMARY KEY
		CLUSTERED
		([configTec])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ConfigTec]
	WITH CHECK
	ADD CONSTRAINT [FKC49BAA0F3C4194A9]
	FOREIGN KEY ([procurementNatureId]) REFERENCES [dbo].[tbl_ProcurementNature] ([procurementNatureId])
ALTER TABLE [dbo].[tbl_ConfigTec]
	CHECK CONSTRAINT [FKC49BAA0F3C4194A9]

GO
ALTER TABLE [dbo].[tbl_ConfigTec]
	WITH CHECK
	ADD CONSTRAINT [tecProcurementNatureId_FK1]
	FOREIGN KEY ([procurementNatureId]) REFERENCES [dbo].[tbl_ProcurementNature] ([procurementNatureId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ConfigTec]
	CHECK CONSTRAINT [tecProcurementNatureId_FK1]

GO
ALTER TABLE [dbo].[tbl_ConfigTec] SET (LOCK_ESCALATION = TABLE)
GO
