SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_TenderListCellDetail] (
		[tenderListCellId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderListId]         [int] NOT NULL,
		[tenderTableId]        [int] NOT NULL,
		[columnId]             [smallint] NOT NULL,
		[cellId]               [int] NOT NULL,
		[location]             [smallint] NOT NULL,
		CONSTRAINT [PK_tbl_TenderListCellDetail]
		PRIMARY KEY
		CLUSTERED
		([tenderListCellId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderListCellDetail]
	WITH CHECK
	ADD CONSTRAINT [tenderListBoxId_FK1]
	FOREIGN KEY ([tenderListId]) REFERENCES [dbo].[tbl_TenderListBox] ([tenderListId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderListCellDetail]
	CHECK CONSTRAINT [tenderListBoxId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderListCellDetail]
	WITH CHECK
	ADD CONSTRAINT [tenderListTableId_FK1]
	FOREIGN KEY ([tenderTableId]) REFERENCES [dbo].[tbl_TenderTables] ([tenderTableId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderListCellDetail]
	CHECK CONSTRAINT [tenderListTableId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderListCellDetail] SET (LOCK_ESCALATION = TABLE)
GO
