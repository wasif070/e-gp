SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderValAcceptance] (
		[tenderValAcceptId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]              [int] NOT NULL,
		[valExtDtId]            [int] NOT NULL,
		[userId]                [int] NOT NULL,
		[tenderValAcceptDt]     [smalldatetime] NOT NULL,
		[comments]              [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[status]                [nchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_TenderValAcceptance]
		PRIMARY KEY
		CLUSTERED
		([tenderValAcceptId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderValAcceptance]
	WITH CHECK
	ADD CONSTRAINT [acceptanceTenderId_FK1]
	FOREIGN KEY ([valExtDtId]) REFERENCES [dbo].[tbl_TenderValidityExtDate] ([valExtDtId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderValAcceptance]
	CHECK CONSTRAINT [acceptanceTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderValAcceptance]
	WITH CHECK
	ADD CONSTRAINT [FK4388DFC3C52FDBF4]
	FOREIGN KEY ([valExtDtId]) REFERENCES [dbo].[tbl_TenderValidityExtDate] ([valExtDtId])
ALTER TABLE [dbo].[tbl_TenderValAcceptance]
	CHECK CONSTRAINT [FK4388DFC3C52FDBF4]

GO
ALTER TABLE [dbo].[tbl_TenderValAcceptance] SET (LOCK_ESCALATION = TABLE)
GO
