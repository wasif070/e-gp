SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_WS_OrgMasterClient] (
		[cOrgId]          [int] IDENTITY(1, 1) NOT NULL,
		[cEmailId]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cLoginPass]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cOrgNameEn]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cOrgNameBg]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cWSDesc]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cWSRights]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]       [int] NOT NULL,
		[createdDate]     [datetime] NOT NULL,
		[isDeleted]       [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Email Id for Client Organisation', 'SCHEMA', N'dbo', 'TABLE', N'tbl_WS_OrgMasterClient', 'COLUMN', N'cEmailId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Login Passward for Client Organisation in encrypted format', 'SCHEMA', N'dbo', 'TABLE', N'tbl_WS_OrgMasterClient', 'COLUMN', N'cLoginPass'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary Id for Client Organisation Master', 'SCHEMA', N'dbo', 'TABLE', N'tbl_WS_OrgMasterClient', 'COLUMN', N'cOrgId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Client Organisation Name in Bangala language', 'SCHEMA', N'dbo', 'TABLE', N'tbl_WS_OrgMasterClient', 'COLUMN', N'cOrgNameBg'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Client Organisation Name in English language', 'SCHEMA', N'dbo', 'TABLE', N'tbl_WS_OrgMasterClient', 'COLUMN', N'cOrgNameEn'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Created user', 'SCHEMA', N'dbo', 'TABLE', N'tbl_WS_OrgMasterClient', 'COLUMN', N'createdBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Created date', 'SCHEMA', N'dbo', 'TABLE', N'tbl_WS_OrgMasterClient', 'COLUMN', N'createdDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Web-service description ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_WS_OrgMasterClient', 'COLUMN', N'cWSDesc'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Web-service access rights', 'SCHEMA', N'dbo', 'TABLE', N'tbl_WS_OrgMasterClient', 'COLUMN', N'cWSRights'
GO
EXEC sp_addextendedproperty N'MS_Description', N'active / deactive', 'SCHEMA', N'dbo', 'TABLE', N'tbl_WS_OrgMasterClient', 'COLUMN', N'isDeleted'
GO
ALTER TABLE [dbo].[tbl_WS_OrgMasterClient] SET (LOCK_ESCALATION = TABLE)
GO
