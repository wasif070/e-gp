SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_OfficeMaster_old] (
		[officeId]               [int] IDENTITY(1, 1) NOT NULL,
		[officeName]             [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[address]                [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[countryId]              [smallint] NOT NULL,
		[stateId]                [smallint] NOT NULL,
		[city]                   [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[upjilla]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[phoneNo]                [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[faxNo]                  [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[postCode]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[departmentId]           [int] NOT NULL,
		[creationDate]           [smalldatetime] NOT NULL,
		[createdBy]              [int] NOT NULL,
		[status]                 [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[officeNameInBangla]     [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[pecode]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[prmsPECode]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[officeType]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_OfficeMaster_old] SET (LOCK_ESCALATION = TABLE)
GO
