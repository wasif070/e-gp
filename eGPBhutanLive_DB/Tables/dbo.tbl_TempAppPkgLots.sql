SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TempAppPkgLots] (
		[appPkgLotId]     [int] NOT NULL,
		[packageId]       [int] NOT NULL,
		[appId]           [int] NOT NULL,
		[lotNo]           [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[lotDesc]         [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[quantity]        [numeric](18, 2) NULL,
		[unit]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[lotEstCost]      [money] NOT NULL,
		CONSTRAINT [PK_tbl_AppPkgLotsTemp]
		PRIMARY KEY
		CLUSTERED
		([appPkgLotId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TempAppPkgLots] SET (LOCK_ESCALATION = TABLE)
GO
