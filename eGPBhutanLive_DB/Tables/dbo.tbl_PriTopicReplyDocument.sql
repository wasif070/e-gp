SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_PriTopicReplyDocument] (
		[docId]              [int] IDENTITY(1, 1) NOT NULL,
		[docName]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]     [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[topicId]            [int] NULL,
		[topicReplyId]       [int] NULL,
		[uploadedBy]         [int] NOT NULL,
		CONSTRAINT [PK_tbl_TopicReplyDocument]
		PRIMARY KEY
		CLUSTERED
		([docId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_PriTopicReplyDocument] SET (LOCK_ESCALATION = TABLE)
GO
