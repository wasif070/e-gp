SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_EvalTSCNotification] (
		[evalTscNotId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]         [int] NOT NULL,
		[sentBy]           [int] NOT NULL,
		[sentDt]           [datetime] NOT NULL,
		CONSTRAINT [PK_tbl_EvalTSCNotification]
		PRIMARY KEY
		CLUSTERED
		([evalTscNotId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalTSCNotification]
	ADD
	CONSTRAINT [DF_tbl_EvalTSCNotification_sentDt]
	DEFAULT (getdate()) FOR [sentDt]
GO
ALTER TABLE [dbo].[tbl_EvalTSCNotification]
	WITH NOCHECK
	ADD CONSTRAINT [tenderTscNot_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EvalTSCNotification]
	CHECK CONSTRAINT [tenderTscNot_FK1]

GO
ALTER TABLE [dbo].[tbl_EvalTSCNotification] SET (LOCK_ESCALATION = TABLE)
GO
