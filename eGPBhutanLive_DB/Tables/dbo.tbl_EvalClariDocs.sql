SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalClariDocs] (
		[evalClrDocId]       [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]           [int] NOT NULL,
		[userId]             [int] NOT NULL,
		[formId]             [int] NOT NULL,
		[docType]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[documentName]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedBy]         [int] NOT NULL,
		[uploadedDt]         [smalldatetime] NOT NULL,
		[docSize]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[evalCount]          [int] NOT NULL,
		CONSTRAINT [PK_tbl_EvalClariDocs]
		PRIMARY KEY
		CLUSTERED
		([evalClrDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalClariDocs]
	ADD
	CONSTRAINT [DF__tbl_EvalC__evalC__174363E2]
	DEFAULT ('0') FOR [evalCount]
GO
ALTER TABLE [dbo].[tbl_EvalClariDocs]
	WITH NOCHECK
	ADD CONSTRAINT [evalClarinTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EvalClariDocs]
	CHECK CONSTRAINT [evalClarinTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_EvalClariDocs] SET (LOCK_ESCALATION = TABLE)
GO
