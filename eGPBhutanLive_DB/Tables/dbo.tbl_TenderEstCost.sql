SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_TenderEstCost] (
		[estCostLotId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]         [int] NOT NULL,
		[pkgLotId]         [int] NOT NULL,
		[estCost]          [money] NOT NULL,
		[createdBy]        [int] NOT NULL,
		[createdDt]        [datetime] NOT NULL,
		CONSTRAINT [PK_tbl_TenderEstCost]
		PRIMARY KEY
		CLUSTERED
		([estCostLotId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderEstCost]
	ADD
	CONSTRAINT [DF_tbl_TenderEstCost_pkgLotId]
	DEFAULT ((0)) FOR [pkgLotId]
GO
ALTER TABLE [dbo].[tbl_TenderEstCost]
	WITH NOCHECK
	ADD CONSTRAINT [tenderEstCost_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderEstCost]
	CHECK CONSTRAINT [tenderEstCost_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderEstCost] SET (LOCK_ESCALATION = TABLE)
GO
