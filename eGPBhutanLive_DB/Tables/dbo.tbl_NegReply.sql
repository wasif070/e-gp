SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_NegReply] (
		[negReplyId]      [int] IDENTITY(1, 1) NOT NULL,
		[negQueryId]      [int] NOT NULL,
		[replyText]       [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[userId]          [int] NOT NULL,
		[repliedDate]     [smalldatetime] NOT NULL,
		[eSignature]      [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[replyAction]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [negReplyId_Pk]
		PRIMARY KEY
		CLUSTERED
		([negReplyId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_NegReply]
	WITH CHECK
	ADD CONSTRAINT [FKCA539EBB75727AEB]
	FOREIGN KEY ([negQueryId]) REFERENCES [dbo].[tbl_NegQuery] ([negQueryId])
ALTER TABLE [dbo].[tbl_NegReply]
	CHECK CONSTRAINT [FKCA539EBB75727AEB]

GO
ALTER TABLE [dbo].[tbl_NegReply]
	WITH CHECK
	ADD CONSTRAINT [replyNegQueryId_FK1]
	FOREIGN KEY ([negQueryId]) REFERENCES [dbo].[tbl_NegQuery] ([negQueryId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_NegReply]
	CHECK CONSTRAINT [replyNegQueryId_FK1]

GO
ALTER TABLE [dbo].[tbl_NegReply] SET (LOCK_ESCALATION = TABLE)
GO
