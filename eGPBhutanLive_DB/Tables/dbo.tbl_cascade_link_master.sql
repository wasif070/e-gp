SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_cascade_link_master] (
		[linkId]                [int] IDENTITY(1, 1) NOT NULL,
		[linkName]              [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[parentLinkId]          [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[userTypeId]            [varchar](60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[hyperLinkPageName]     [varchar](60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_cascade_link_master]
		PRIMARY KEY
		CLUSTERED
		([linkId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_cascade_link_master] SET (LOCK_ESCALATION = TABLE)
GO
