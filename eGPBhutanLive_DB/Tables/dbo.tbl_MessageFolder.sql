SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_MessageFolder] (
		[msgFolderId]      [int] IDENTITY(1, 1) NOT NULL,
		[folderName]       [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[creationDate]     [smalldatetime] NOT NULL,
		[userId]           [int] NOT NULL,
		CONSTRAINT [msgFolderId_PK]
		PRIMARY KEY
		CLUSTERED
		([msgFolderId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_MessageFolder]
	WITH CHECK
	ADD CONSTRAINT [FK5D0604746174DBD1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_MessageFolder]
	CHECK CONSTRAINT [FK5D0604746174DBD1]

GO
ALTER TABLE [dbo].[tbl_MessageFolder]
	WITH CHECK
	ADD CONSTRAINT [userFolderId_FK1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_MessageFolder]
	CHECK CONSTRAINT [userFolderId_FK1]

GO
ALTER TABLE [dbo].[tbl_MessageFolder] SET (LOCK_ESCALATION = TABLE)
GO
