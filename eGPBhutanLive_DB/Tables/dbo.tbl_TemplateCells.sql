SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TemplateCells] (
		[templateCelId]     [int] IDENTITY(1, 1) NOT NULL,
		[tableId]           [int] NOT NULL,
		[columnId]          [smallint] NOT NULL,
		[cellId]            [int] NOT NULL,
		[rowId]             [int] NOT NULL,
		[cellDatatype]      [tinyint] NOT NULL,
		[cellvalue]         [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[colId]             [smallint] NULL,
		[showOrHide]        [tinyint] NULL,
		CONSTRAINT [PK_tbl_TemplateCells_1]
		PRIMARY KEY
		CLUSTERED
		([templateCelId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TemplateCells]
	WITH CHECK
	ADD CONSTRAINT [FKC4010016F1EE9A68]
	FOREIGN KEY ([tableId]) REFERENCES [dbo].[tbl_TemplateTables] ([tableId])
ALTER TABLE [dbo].[tbl_TemplateCells]
	CHECK CONSTRAINT [FKC4010016F1EE9A68]

GO
ALTER TABLE [dbo].[tbl_TemplateCells]
	WITH CHECK
	ADD CONSTRAINT [tblTemplateCelTableId_FK1]
	FOREIGN KEY ([tableId]) REFERENCES [dbo].[tbl_TemplateTables] ([tableId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TemplateCells]
	CHECK CONSTRAINT [tblTemplateCelTableId_FK1]

GO
ALTER TABLE [dbo].[tbl_TemplateCells] SET (LOCK_ESCALATION = TABLE)
GO
