SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_PreTenderReplyDocs] (
		[replyDocId]         [int] IDENTITY(1, 1) NOT NULL,
		[queryId]            [int] NOT NULL,
		[documentName]       [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]       [smalldatetime] NOT NULL,
		[uploadedBy]         [int] NOT NULL,
		[tenderId]           [int] NOT NULL,
		[docType]            [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_PreTenderReplyDocs]
		PRIMARY KEY
		CLUSTERED
		([replyDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_PreTenderReplyDocs]
	ADD
	CONSTRAINT [DF_tbl_PreTenderReplyDocs_docType]
	DEFAULT ('Prebid') FOR [docType]
GO
ALTER TABLE [dbo].[tbl_PreTenderReplyDocs]
	WITH CHECK
	ADD CONSTRAINT [FK242048EF11E6C914]
	FOREIGN KEY ([queryId]) REFERENCES [dbo].[tbl_PreTenderQuery] ([queryId])
ALTER TABLE [dbo].[tbl_PreTenderReplyDocs]
	CHECK CONSTRAINT [FK242048EF11E6C914]

GO
ALTER TABLE [dbo].[tbl_PreTenderReplyDocs]
	WITH CHECK
	ADD CONSTRAINT [replyDocQueryId_FK1]
	FOREIGN KEY ([queryId]) REFERENCES [dbo].[tbl_PreTenderQuery] ([queryId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_PreTenderReplyDocs]
	CHECK CONSTRAINT [replyDocQueryId_FK1]

GO
ALTER TABLE [dbo].[tbl_PreTenderReplyDocs] SET (LOCK_ESCALATION = TABLE)
GO
