SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_ProjectOffice] (
		[projOfficeId]     [int] IDENTITY(1, 1) NOT NULL,
		[projectId]        [int] NOT NULL,
		[officeId]         [int] NOT NULL,
		[userId]           [int] NOT NULL,
		CONSTRAINT [projOfficeId_PK]
		PRIMARY KEY
		CLUSTERED
		([projOfficeId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ProjectOffice]
	ADD
	CONSTRAINT [DF_tbl_ProjectOffice_userId]
	DEFAULT ((94)) FOR [userId]
GO
ALTER TABLE [dbo].[tbl_ProjectOffice]
	WITH CHECK
	ADD CONSTRAINT [FK7C515C546174DBD1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_ProjectOffice]
	CHECK CONSTRAINT [FK7C515C546174DBD1]

GO
ALTER TABLE [dbo].[tbl_ProjectOffice]
	WITH CHECK
	ADD CONSTRAINT [FK7C515C54EAD91E95]
	FOREIGN KEY ([officeId]) REFERENCES [dbo].[tbl_OfficeMaster] ([officeId])
ALTER TABLE [dbo].[tbl_ProjectOffice]
	CHECK CONSTRAINT [FK7C515C54EAD91E95]

GO
ALTER TABLE [dbo].[tbl_ProjectOffice]
	WITH CHECK
	ADD CONSTRAINT [projectOfficeId_FK1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ProjectOffice]
	CHECK CONSTRAINT [projectOfficeId_FK1]

GO
EXEC sp_addextendedproperty N'MS_Description', N'reference from tbl_OfficeMaster to identify the officeId', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ProjectOffice', 'COLUMN', N'officeId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'project Id information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ProjectOffice', 'COLUMN', N'projectId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'system generated unique Id information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ProjectOffice', 'COLUMN', N'projOfficeId'
GO
ALTER TABLE [dbo].[tbl_ProjectOffice] SET (LOCK_ESCALATION = TABLE)
GO
