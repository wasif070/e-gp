SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_VendorReqWcc] (
		[venReqWccId]       [int] IDENTITY(1, 1) NOT NULL,
		[tendererId]        [int] NOT NULL,
		[reqDate]           [datetime] NOT NULL,
		[lotId]             [int] NOT NULL,
		[status]            [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[wcCertiHistId]     [int] NULL,
		[cntId]             [int] NULL,
		CONSTRAINT [PK_tbl_CMS_VendorReqWcc]
		PRIMARY KEY
		CLUSTERED
		([venReqWccId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_VendorReqWcc]
	ADD
	CONSTRAINT [DF_tbl_CMS_VendorReqWcc_cntId]
	DEFAULT ((0)) FOR [cntId]
GO
ALTER TABLE [dbo].[tbl_CMS_VendorReqWcc] SET (LOCK_ESCALATION = TABLE)
GO
