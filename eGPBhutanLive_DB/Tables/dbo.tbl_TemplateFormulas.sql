SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TemplateFormulas] (
		[formulaId]        [int] IDENTITY(1, 1) NOT NULL,
		[formId]           [int] NOT NULL,
		[tableId]          [int] NOT NULL,
		[columnId]         [int] NOT NULL,
		[formula]          [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isGrandTotal]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [formulaId_PK]
		PRIMARY KEY
		CLUSTERED
		([formulaId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TemplateFormulas]
	WITH CHECK
	ADD CONSTRAINT [formulasTableId_FK1]
	FOREIGN KEY ([tableId]) REFERENCES [dbo].[tbl_TemplateTables] ([tableId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TemplateFormulas]
	CHECK CONSTRAINT [formulasTableId_FK1]

GO
ALTER TABLE [dbo].[tbl_TemplateFormulas] SET (LOCK_ESCALATION = TABLE)
GO
