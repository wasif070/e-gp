SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_ListCellDetail] (
		[listCellId]        [int] IDENTITY(1, 1) NOT NULL,
		[tenderTableId]     [int] NOT NULL,
		[columnId]          [smallint] NOT NULL,
		[cellId]            [int] NOT NULL,
		[location]          [smallint] NOT NULL,
		[listBoxId]         [int] NOT NULL,
		CONSTRAINT [listCellId_PK]
		PRIMARY KEY
		CLUSTERED
		([listCellId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ListCellDetail]
	WITH CHECK
	ADD CONSTRAINT [FK4F42A252B9654DD7]
	FOREIGN KEY ([listBoxId]) REFERENCES [dbo].[tbl_ListBoxMaster] ([listBoxId])
ALTER TABLE [dbo].[tbl_ListCellDetail]
	CHECK CONSTRAINT [FK4F42A252B9654DD7]

GO
ALTER TABLE [dbo].[tbl_ListCellDetail]
	WITH CHECK
	ADD CONSTRAINT [listBocListId_FK3]
	FOREIGN KEY ([listBoxId]) REFERENCES [dbo].[tbl_ListBoxMaster] ([listBoxId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ListCellDetail]
	CHECK CONSTRAINT [listBocListId_FK3]

GO
ALTER TABLE [dbo].[tbl_ListCellDetail]
	WITH CHECK
	ADD CONSTRAINT [listTableId_FK1]
	FOREIGN KEY ([tenderTableId]) REFERENCES [dbo].[tbl_TemplateTables] ([tableId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ListCellDetail]
	CHECK CONSTRAINT [listTableId_FK1]

GO
ALTER TABLE [dbo].[tbl_ListCellDetail] SET (LOCK_ESCALATION = TABLE)
GO
