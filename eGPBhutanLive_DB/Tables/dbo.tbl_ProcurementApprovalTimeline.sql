SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ProcurementApprovalTimeline] (
		[RuleId]                         [int] IDENTITY(1, 1) NOT NULL,
		[ApprovalAuthority]              [varchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ProcurementNature]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[NoOfDaysForTSC]                 [int] NOT NULL,
		[NoOfDaysForTECorPEC]            [int] NOT NULL,
		[TotalNoOfDaysForEvaluation]     AS ([NoOfDaysForTSC]+[NoOfDaysForTECorPEC]),
		[NoOfDaysForApproval]            [int] NOT NULL,
		CONSTRAINT [PK_tbl_ProcurementApprovalTimeline]
		PRIMARY KEY
		CLUSTERED
		([RuleId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ProcurementApprovalTimeline] SET (LOCK_ESCALATION = TABLE)
GO
