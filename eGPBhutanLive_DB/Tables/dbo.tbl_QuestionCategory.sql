SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_QuestionCategory] (
		[queCatId]     [int] IDENTITY(1, 1) NOT NULL,
		[catagory]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_QuestionCategory]
		PRIMARY KEY
		CLUSTERED
		([queCatId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_QuestionCategory] SET (LOCK_ESCALATION = TABLE)
GO
