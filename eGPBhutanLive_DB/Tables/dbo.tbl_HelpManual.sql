SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_HelpManual] (
		[helpManualId]     [int] IDENTITY(1, 1) NOT NULL,
		[helpUrl]          [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[helpContent]      [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[entryDate]        [smalldatetime] NOT NULL,
		[isDeleted]        [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_HelpManual]
		PRIMARY KEY
		CLUSTERED
		([helpManualId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_HelpManual]
	ADD
	CONSTRAINT [DF_tbl_HelpManual_isDeleted]
	DEFAULT ('no') FOR [isDeleted]
GO
ALTER TABLE [dbo].[tbl_HelpManual] SET (LOCK_ESCALATION = TABLE)
GO
