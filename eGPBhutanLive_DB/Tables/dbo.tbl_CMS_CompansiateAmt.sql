SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_CMS_CompansiateAmt] (
		[compId]         [int] IDENTITY(1, 1) NOT NULL,
		[paymentId]      [int] NOT NULL,
		[compAmt]        [decimal](13, 3) NOT NULL,
		[balanceAmt]     [decimal](13, 3) NOT NULL,
		CONSTRAINT [PK_Tbl_CMS_CompansiateAmt]
		PRIMARY KEY
		CLUSTERED
		([compId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_CompansiateAmt] SET (LOCK_ESCALATION = TABLE)
GO
