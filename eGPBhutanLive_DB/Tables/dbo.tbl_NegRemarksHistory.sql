SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_NegRemarksHistory] (
		[negRemarksId]     [int] IDENTITY(1, 1) NOT NULL,
		[negId]            [int] NOT NULL,
		[remarks]          [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_CMS_NegRemarksHistory]
		PRIMARY KEY
		CLUSTERED
		([negRemarksId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_NegRemarksHistory] SET (LOCK_ESCALATION = TABLE)
GO
