SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_PriTopicReply] (
		[topicReplyId]     [int] IDENTITY(1, 1) NOT NULL,
		[topicId]          [int] NOT NULL,
		[description]      [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[postedBy]         [int] NOT NULL,
		[postedDate]       [datetime] NOT NULL,
		[hasSeen]          [int] NOT NULL,
		CONSTRAINT [PK_tbl_PriTopicReply]
		PRIMARY KEY
		CLUSTERED
		([topicReplyId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_PriTopicReply]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_PriTopicReply_tbl_PriTopicMaster]
	FOREIGN KEY ([topicId]) REFERENCES [dbo].[tbl_PriTopicMaster] ([topicId])
ALTER TABLE [dbo].[tbl_PriTopicReply]
	CHECK CONSTRAINT [FK_tbl_PriTopicReply_tbl_PriTopicMaster]

GO
ALTER TABLE [dbo].[tbl_PriTopicReply] SET (LOCK_ESCALATION = TABLE)
GO
