SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_CMS_AitConfig] (
		[aitConfigId]         [int] IDENTITY(1, 1) NOT NULL,
		[financialyearId]     [int] NOT NULL,
		[slabStartAmt]        [decimal](15, 3) NOT NULL,
		[slabEndAmt]          [decimal](15, 3) NOT NULL,
		[slabPercent]         [decimal](6, 3) NULL,
		[createdBy]           [int] NOT NULL,
		[flatPercent]         [decimal](15, 3) NOT NULL,
		CONSTRAINT [aitConfigId_PK]
		PRIMARY KEY
		CLUSTERED
		([aitConfigId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_AitConfig]
	WITH NOCHECK
	ADD CONSTRAINT [aitConfigFinancialYearId_FK1]
	FOREIGN KEY ([financialyearId]) REFERENCES [dbo].[tbl_FinancialYear] ([financialYearId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_AitConfig]
	CHECK CONSTRAINT [aitConfigFinancialYearId_FK1]

GO
ALTER TABLE [dbo].[tbl_CMS_AitConfig] SET (LOCK_ESCALATION = TABLE)
GO
