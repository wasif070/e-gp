SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderMandatoryDoc] (
		[tenderFormDocId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]            [int] NOT NULL,
		[tenderFormId]        [int] NOT NULL,
		[templateFormId]      [int] NOT NULL,
		[documentName]        [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[createdBy]           [int] NOT NULL,
		[createdDate]         [datetime] NOT NULL,
		CONSTRAINT [PK_tbl_TenderMandatoryDoc]
		PRIMARY KEY
		CLUSTERED
		([tenderFormDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderMandatoryDoc]
	WITH CHECK
	ADD CONSTRAINT [tenderMandatoryFormDocId_FK1]
	FOREIGN KEY ([tenderFormId]) REFERENCES [dbo].[tbl_TenderForms] ([tenderFormId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderMandatoryDoc]
	CHECK CONSTRAINT [tenderMandatoryFormDocId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderMandatoryDoc] SET (LOCK_ESCALATION = TABLE)
GO
