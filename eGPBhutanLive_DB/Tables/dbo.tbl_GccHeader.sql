SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_GccHeader] (
		[gccHeaderId]       [int] IDENTITY(1, 1) NOT NULL,
		[gccHeaderName]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sectionId]         [int] NOT NULL,
		CONSTRAINT [UQ__tbl_GccH__5D3BDC0C5694AD47]
		UNIQUE
		NONCLUSTERED
		([gccHeaderId])
		ON [PRIMARY],
		CONSTRAINT [PK__tbl_GccH__5D3BDC0D53B8409C]
		PRIMARY KEY
		CLUSTERED
		([gccHeaderId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_GccHeader]
	WITH NOCHECK
	ADD CONSTRAINT [FKBDCE8113EC995508]
	FOREIGN KEY ([sectionId]) REFERENCES [dbo].[tbl_TemplateSections] ([sectionId])
ALTER TABLE [dbo].[tbl_GccHeader]
	CHECK CONSTRAINT [FKBDCE8113EC995508]

GO
ALTER TABLE [dbo].[tbl_GccHeader]
	WITH NOCHECK
	ADD CONSTRAINT [gccHeaderSection_FK2]
	FOREIGN KEY ([sectionId]) REFERENCES [dbo].[tbl_TemplateSections] ([sectionId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_GccHeader]
	CHECK CONSTRAINT [gccHeaderSection_FK2]

GO
ALTER TABLE [dbo].[tbl_GccHeader] SET (LOCK_ESCALATION = TABLE)
GO
