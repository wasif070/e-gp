SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TemplateMandatoryDoc] (
		[templateDocId]         [int] IDENTITY(1, 1) NOT NULL,
		[templateId]            [int] NOT NULL,
		[templateSectionId]     [int] NOT NULL,
		[templateFormId]        [int] NOT NULL,
		[documentName]          [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[createdBy]             [int] NOT NULL,
		[createdDate]           [datetime] NOT NULL,
		CONSTRAINT [PK_tbl_TemplateMandatoryDoc]
		PRIMARY KEY
		CLUSTERED
		([templateDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TemplateMandatoryDoc]
	WITH CHECK
	ADD CONSTRAINT [templateFormdocId_FK1]
	FOREIGN KEY ([templateFormId]) REFERENCES [dbo].[tbl_TemplateSectionForm] ([formId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TemplateMandatoryDoc]
	CHECK CONSTRAINT [templateFormdocId_FK1]

GO
ALTER TABLE [dbo].[tbl_TemplateMandatoryDoc] SET (LOCK_ESCALATION = TABLE)
GO
