SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_TrackVariation] (
		[variTrackId]       [int] IDENTITY(1, 1) NOT NULL,
		[wpdetailid]        [int] NULL,
		[variOrdId]         [int] NOT NULL,
		[srno]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[groupname]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[description]       [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[uom]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[rate]              [decimal](15, 3) NULL,
		[qty]               [decimal](15, 3) NULL,
		[rowId]             [int] NOT NULL,
		[tenderTableId]     [int] NOT NULL,
		[startDate]         [datetime] NOT NULL,
		[endDate]           [datetime] NOT NULL,
		[flag]              [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_CMS_TrackVariation]
		PRIMARY KEY
		CLUSTERED
		([variTrackId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_TrackVariation] SET (LOCK_ESCALATION = TABLE)
GO
