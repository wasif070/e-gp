SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_ROItems] (
		[roItemsId]         [int] IDENTITY(1, 1) NOT NULL,
		[wpId]              [int] NOT NULL,
		[srno]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[description]       [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uom]               [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[qty]               [decimal](15, 3) NOT NULL,
		[rate]              [decimal](15, 3) NOT NULL,
		[noofdays]          [int] NOT NULL,
		[rowid]             [int] NOT NULL,
		[tendertableid]     [int] NOT NULL,
		[romId]             [int] NOT NULL,
		[detailId]          [int] NOT NULL,
		CONSTRAINT [PK_tbl_CMS_ROItems]
		PRIMARY KEY
		CLUSTERED
		([roItemsId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_ROItems] SET (LOCK_ESCALATION = TABLE)
GO
