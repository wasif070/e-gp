SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_UserTypeMaster] (
		[userTypeId]     [tinyint] IDENTITY(1, 1) NOT NULL,
		[userType]       [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [userTypeId_PK]
		PRIMARY KEY
		CLUSTERED
		([userTypeId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Type of user stores here , it may be supplier,officer ,admin 
dataentry operator , partner', 'SCHEMA', N'dbo', 'TABLE', N'tbl_UserTypeMaster', 'COLUMN', N'userType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Unique id of user type stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_UserTypeMaster', 'COLUMN', N'userTypeId'
GO
ALTER TABLE [dbo].[tbl_UserTypeMaster] SET (LOCK_ESCALATION = TABLE)
GO
