SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_TaxConfig] (
		[taxConfigId]               [int] IDENTITY(1, 1) NOT NULL,
		[procurementNatureId]       [tinyint] NOT NULL,
		[isAdvanceApplicable]       [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[advancePercent]            [decimal](6, 3) NULL,
		[advanceAdjustment]         [decimal](6, 3) NULL,
		[isRetentionApplicable]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[retentionPercent]          [decimal](6, 3) NULL,
		[retentionAdjustment]       [decimal](6, 3) NULL,
		[isPGApplicable]            [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[pgPercent]                 [decimal](6, 3) NULL,
		[pgAdjustment]              [decimal](6, 3) NULL,
		[isVATApplicable]           [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[vatPercent]                [decimal](6, 3) NULL,
		[isBonusApplicable]         [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[bonusPercent]              [decimal](6, 3) NULL,
		[bonusAfter]                [decimal](6, 3) NULL,
		[isLDApplicable]            [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ldPercent]                 [decimal](6, 3) NULL,
		[ldAfter]                   [decimal](6, 3) NULL,
		[isPenaltyApplicable]       [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[penaltyPercent]            [decimal](6, 3) NULL,
		[penaltyAfter]              [decimal](6, 3) NULL,
		[isCurrent]                 [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]                 [int] NOT NULL,
		[createdDate]               [datetime] NOT NULL,
		CONSTRAINT [PK_tbl_CMS_TaxConfig]
		PRIMARY KEY
		CLUSTERED
		([taxConfigId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_TaxConfig]
	ADD
	CONSTRAINT [DF_tbl_CMS_TaxConfig_isAdvanceApplicable]
	DEFAULT ('yes') FOR [isAdvanceApplicable]
GO
ALTER TABLE [dbo].[tbl_CMS_TaxConfig]
	ADD
	CONSTRAINT [DF_tbl_CMS_TaxConfig_isBonusApplicable]
	DEFAULT ('yes') FOR [isBonusApplicable]
GO
ALTER TABLE [dbo].[tbl_CMS_TaxConfig]
	ADD
	CONSTRAINT [DF_tbl_CMS_TaxConfig_isCurrent]
	DEFAULT ('yes') FOR [isCurrent]
GO
ALTER TABLE [dbo].[tbl_CMS_TaxConfig]
	ADD
	CONSTRAINT [DF_tbl_CMS_TaxConfig_isLDApplicable]
	DEFAULT ('yes') FOR [isLDApplicable]
GO
ALTER TABLE [dbo].[tbl_CMS_TaxConfig]
	ADD
	CONSTRAINT [DF_tbl_CMS_TaxConfig_isPenaltyApplicable]
	DEFAULT ('yes') FOR [isPenaltyApplicable]
GO
ALTER TABLE [dbo].[tbl_CMS_TaxConfig]
	ADD
	CONSTRAINT [DF_tbl_CMS_TaxConfig_isPGApplicable]
	DEFAULT ('yes') FOR [isPGApplicable]
GO
ALTER TABLE [dbo].[tbl_CMS_TaxConfig]
	ADD
	CONSTRAINT [DF_tbl_CMS_TaxConfig_isRetentionApplicable]
	DEFAULT ('yes') FOR [isRetentionApplicable]
GO
ALTER TABLE [dbo].[tbl_CMS_TaxConfig]
	ADD
	CONSTRAINT [DF_tbl_CMS_TaxConfig_isVATApplicable]
	DEFAULT ('yes') FOR [isVATApplicable]
GO
ALTER TABLE [dbo].[tbl_CMS_TaxConfig]
	WITH NOCHECK
	ADD CONSTRAINT [FK_taxConfigNature]
	FOREIGN KEY ([procurementNatureId]) REFERENCES [dbo].[tbl_ProcurementNature] ([procurementNatureId])
ALTER TABLE [dbo].[tbl_CMS_TaxConfig]
	CHECK CONSTRAINT [FK_taxConfigNature]

GO
ALTER TABLE [dbo].[tbl_CMS_TaxConfig] SET (LOCK_ESCALATION = TABLE)
GO
