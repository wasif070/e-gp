SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalConfig] (
		[evalConfigId]      [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]          [int] NOT NULL,
		[configType]        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[evalCommittee]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[configBy]          [int] NOT NULL,
		[configDate]        [smalldatetime] NOT NULL,
		[isPostQualReq]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tecMemberId]       [int] NOT NULL,
		[tscMemberId]       [int] NOT NULL,
		[isTscReq]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[evalStatus]        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[tecGovUserId]      [int] NULL,
		CONSTRAINT [evalConfigId_PK]
		PRIMARY KEY
		CLUSTERED
		([evalConfigId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalConfig]
	ADD
	CONSTRAINT [DF_tbl_EvalConfig_isTscReq]
	DEFAULT ('No') FOR [isTscReq]
GO
ALTER TABLE [dbo].[tbl_EvalConfig]
	ADD
	CONSTRAINT [DF_tbl_EvalConfig_tecMemberId]
	DEFAULT ((0)) FOR [tecMemberId]
GO
ALTER TABLE [dbo].[tbl_EvalConfig]
	ADD
	CONSTRAINT [DF_tbl_EvalConfig_tscMemberId]
	DEFAULT ((0)) FOR [tscMemberId]
GO
ALTER TABLE [dbo].[tbl_EvalConfig]
	WITH NOCHECK
	ADD CONSTRAINT [evalConfigTenderId_Fk1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EvalConfig]
	CHECK CONSTRAINT [evalConfigTenderId_Fk1]

GO
ALTER TABLE [dbo].[tbl_EvalConfig] SET (LOCK_ESCALATION = TABLE)
GO
