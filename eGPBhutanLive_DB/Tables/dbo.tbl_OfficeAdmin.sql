SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_OfficeAdmin] (
		[officeAdminId]     [int] IDENTITY(1, 1) NOT NULL,
		[userId]            [int] NOT NULL,
		[officeId]          [int] NOT NULL,
		[adminType]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [officeAdminId_PK]
		PRIMARY KEY
		CLUSTERED
		([officeAdminId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_OfficeAdmin]
	WITH CHECK
	ADD CONSTRAINT [FKC421C2D26174DBD1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_OfficeAdmin]
	CHECK CONSTRAINT [FKC421C2D26174DBD1]

GO
ALTER TABLE [dbo].[tbl_OfficeAdmin]
	WITH CHECK
	ADD CONSTRAINT [FKC421C2D2EAD91E95]
	FOREIGN KEY ([officeId]) REFERENCES [dbo].[tbl_OfficeMaster] ([officeId])
ALTER TABLE [dbo].[tbl_OfficeAdmin]
	CHECK CONSTRAINT [FKC421C2D2EAD91E95]

GO
ALTER TABLE [dbo].[tbl_OfficeAdmin]
	WITH CHECK
	ADD CONSTRAINT [officeId_FK2]
	FOREIGN KEY ([officeId]) REFERENCES [dbo].[tbl_OfficeMaster] ([officeId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_OfficeAdmin]
	CHECK CONSTRAINT [officeId_FK2]

GO
ALTER TABLE [dbo].[tbl_OfficeAdmin]
	WITH CHECK
	ADD CONSTRAINT [userId_FK1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_OfficeAdmin]
	CHECK CONSTRAINT [userId_FK1]

GO
ALTER TABLE [dbo].[tbl_OfficeAdmin] SET (LOCK_ESCALATION = TABLE)
GO
