SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_AppPqTenderDates] (
		[pqDtId]                      [int] IDENTITY(1, 1) NOT NULL,
		[appid]                       [int] NOT NULL,
		[packageId]                   [int] NOT NULL,
		[advtDt]                      [date] NULL,
		[advtDays]                    [smallint] NOT NULL,
		[subDt]                       [date] NULL,
		[subDays]                     [smallint] NOT NULL,
		[openDt]                      [date] NULL,
		[openDays]                    [smallint] NOT NULL,
		[evalRptDt]                   [date] NULL,
		[evalRptDays]                 [smallint] NOT NULL,
		[appLstDt]                    [date] NULL,
		[tenderAdvertDt]              [date] NULL,
		[tenderAdvertDays]            [smallint] NOT NULL,
		[tenderSubDt]                 [date] NULL,
		[tenderSubDays]               [smallint] NOT NULL,
		[tenderOpenDt]                [date] NULL,
		[tenderOpenDays]              [smallint] NOT NULL,
		[techSubCmtRptDt]             [date] NULL,
		[techSubCmtRptDays]           [smallint] NOT NULL,
		[tenderEvalRptDt]             [date] NULL,
		[tenderEvalRptdays]           [smallint] NOT NULL,
		[tenderEvalRptAppDt]          [date] NULL,
		[tenderEvalRptAppDays]        [smallint] NOT NULL,
		[tenderContractAppDt]         [date] NULL,
		[tenderContractAppDays]       [smallint] NOT NULL,
		[tenderLetterIntentDt]        [date] NULL,
		[tnderLetterIntentDays]       [smallint] NULL,
		[tenderNoaIssueDt]            [date] NULL,
		[tenderNoaIssueDays]          [smallint] NOT NULL,
		[tenderContractSignDt]        [date] NULL,
		[tenderContractSignDays]      [smallint] NOT NULL,
		[tenderContractCompDt]        [date] NULL,
		[reoiReceiptDt]               [date] NULL,
		[reoiReceiptDays]             [smallint] NOT NULL,
		[rfpTechEvalDt]               [date] NULL,
		[rfpTechEvalDays]             [smallint] NOT NULL,
		[rfpFinancialOpenDt]          [date] NULL,
		[rfpFinancialOpenDays]        [smallint] NOT NULL,
		[rfpNegCompDt]                [date] NULL,
		[rfpNegCompDays]              [smallint] NOT NULL,
		[rfpContractAppDt]            [date] NULL,
		[rfpContractAppDays]          [smallint] NOT NULL,
		[rfaAdvertDt]                 [date] NULL,
		[rfaAdvertDays]               [smallint] NOT NULL,
		[rfaReceiptDt]                [date] NULL,
		[rfaReceiptDays]              [smallint] NOT NULL,
		[rfaEvalDt]                   [date] NULL,
		[rfaEvalDays]                 [smallint] NOT NULL,
		[rfaInterviewDt]              [date] NULL,
		[rfaInterviewDays]            [smallint] NOT NULL,
		[rfaFinalSelDt]               [date] NULL,
		[rfaFinalSelDays]             [smallint] NOT NULL,
		[rfaEvalRptSubDt]             [date] NULL,
		[rfaEvalRptSubDays]           [smallint] NOT NULL,
		[rfaAppConsultantDt]          [date] NULL,
		[actAdvtDt]                   [date] NULL,
		[actSubDt]                    [date] NULL,
		[actEvalRptDt]                [date] NULL,
		[actAppLstDt]                 [date] NULL,
		[actTenderAdvertDt]           [date] NULL,
		[actTenderSubDt]              [date] NULL,
		[actTenderOpenDt]             [date] NULL,
		[actTechSubCmtRptDt]          [date] NULL,
		[actTenderEvalRptDt]          [date] NULL,
		[acttenderEvalRptAppDt]       [date] NULL,
		[actTenderContractAppDt]      [date] NULL,
		[actTenderNoaIssueDt]         [date] NULL,
		[actTenderContractSignDt]     [date] NULL,
		[actTenderContractCompDt]     [date] NULL,
		[actReoiReceiptDt]            [date] NULL,
		[actRfpTechEvalDt]            [date] NULL,
		[actRfpFinancialOpenDt]       [date] NULL,
		[actRfpNegComDt]              [date] NULL,
		[actRfpContractAppDt]         [date] NULL,
		[actRfaAdvertDt]              [date] NULL,
		[actRfaReceiptDt]             [date] NULL,
		[actRfaEvalDt]                [date] NULL,
		[actRfaInterviewDt]           [date] NULL,
		[actRfaFinalSelDt]            [date] NULL,
		[actRfaEvalRptSubDt]          [date] NULL,
		[actRfaAppConsultantDt]       [date] NULL,
		[pkgPubDate]                  [smalldatetime] NULL,
		CONSTRAINT [pqDtId_PK]
		PRIMARY KEY
		CLUSTERED
		([pqDtId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTenderDates_openDays]
	DEFAULT ((0)) FOR [openDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTenderDates_rfaReceiptDays]
	DEFAULT ((0)) FOR [rfaReceiptDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTenderDates_rfpFinancialOpenDays1]
	DEFAULT ((0)) FOR [rfpFinancialOpenDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTenderDates_rfpTechEvalDays1]
	DEFAULT ((0)) FOR [rfpTechEvalDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTenderDates_tenderContractSignDays]
	DEFAULT ((0)) FOR [tenderContractSignDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTendersDates_advtdays]
	DEFAULT ((0)) FOR [advtDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTendersDates_evalRptDays]
	DEFAULT ((0)) FOR [evalRptDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTendersDates_reoiReceiptDays]
	DEFAULT ((0)) FOR [reoiReceiptDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTendersDates_rfaAdvertDays]
	DEFAULT ((0)) FOR [rfaAdvertDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTendersDates_rfaEvalDays]
	DEFAULT ((0)) FOR [rfaEvalDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTendersDates_rfaEvalRptSubDays]
	DEFAULT ((0)) FOR [rfaEvalRptSubDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTendersDates_rfaFinalSelDays]
	DEFAULT ((0)) FOR [rfaFinalSelDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTendersDates_rfaInterviewDays]
	DEFAULT ((0)) FOR [rfaInterviewDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTendersDates_rfpContractAppDays]
	DEFAULT ((0)) FOR [rfpContractAppDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTendersDates_rfpNegCompDays]
	DEFAULT ((0)) FOR [rfpNegCompDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTendersDates_subDays]
	DEFAULT ((0)) FOR [subDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTendersDates_techSubCmtRptDays]
	DEFAULT ((0)) FOR [techSubCmtRptDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTendersDates_tenderAdvtDays]
	DEFAULT ((0)) FOR [tenderAdvertDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTendersDates_tenderContractAppDays]
	DEFAULT ((0)) FOR [tenderContractAppDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTendersDates_tenderEvalRptAppDays]
	DEFAULT ((0)) FOR [tenderEvalRptAppDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTendersDates_tenderEvalRptdays]
	DEFAULT ((0)) FOR [tenderEvalRptdays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTendersDates_tenderNoaIssueDays]
	DEFAULT ((0)) FOR [tenderNoaIssueDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTendersDates_tenderOpenDays]
	DEFAULT ((0)) FOR [tenderOpenDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	ADD
	CONSTRAINT [DF_tbl_AppPqTendersDates_tenderSubDays]
	DEFAULT ((0)) FOR [tenderSubDays]
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	WITH CHECK
	ADD CONSTRAINT [FKD3AD53014F1608F]
	FOREIGN KEY ([packageId]) REFERENCES [dbo].[tbl_AppPackages] ([packageId])
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	CHECK CONSTRAINT [FKD3AD53014F1608F]

GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	WITH CHECK
	ADD CONSTRAINT [FKD3AD530448F6D5F]
	FOREIGN KEY ([appid]) REFERENCES [dbo].[tbl_AppMaster] ([appId])
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	CHECK CONSTRAINT [FKD3AD530448F6D5F]

GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	WITH CHECK
	ADD CONSTRAINT [pqDtPkgId_FK1]
	FOREIGN KEY ([packageId]) REFERENCES [dbo].[tbl_AppPackages] ([packageId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_AppPqTenderDates]
	CHECK CONSTRAINT [pqDtPkgId_FK1]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually advertisement invitation date ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actAdvtDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually approval list of date ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actAppLstDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'actually evaluation report date ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actEvalRptDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually REOI Receipt date', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actReoiReceiptDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually RFA advertisement date ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actRfaAdvertDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually date of Approval of Consultants', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actRfaAppConsultantDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually RFA evaluation date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actRfaEvalDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually RFA evaluation report date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actRfaEvalRptSubDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually RFA Dinal selection date ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actRfaFinalSelDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually date of interview of selected Individuals', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actRfaInterviewDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually RFA receipt date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actRfaReceiptDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually date of Approval for Award of Contract', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actRfpContractAppDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually RFP financial opening  date ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actRfpFinancialOpenDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually date of Completion of Negotiation ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actRfpNegComDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually RFP tech evaluation date ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actRfpTechEvalDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'actually application submission date ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actSubDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually Date of Submission of Technical Sub-Committee Report', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actTechSubCmtRptDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'actually tender advertisement date ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actTenderAdvertDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually tender Contract approval Date', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actTenderContractAppDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually Tender Contract completion date ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actTenderContractCompDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually tender contract sign date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actTenderContractSignDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Tender evaluation report approval date ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'acttenderEvalRptAppDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually tender evaluation report date ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actTenderEvalRptDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually tender notice of award  date information store here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actTenderNoaIssueDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually tender open date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actTenderOpenDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actually tender submission date ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'actTenderSubDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'expected advertisement  of invitation days information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'advtDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'expected advertisement of invitation date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'advtDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'referemce from tbl_AppMaster, to identify the appId
', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'appid'
GO
EXEC sp_addextendedproperty N'MS_Description', N'date of approval list information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'appLstDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'days of submission of evaluation report information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'evalRptDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'date of submission of evaluation report information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'evalRptDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'reference from tbl_AppPaclages , to identify the  packageId', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'packageId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'pqDtId is system generated ,that information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'pqDtId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Last Days of Receipt of EOI’s', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'reoiReceiptDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Last Date of Receipt of EOI’s', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'reoiReceiptDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Days of advertisement information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfaAdvertDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date of advertisement information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfaAdvertDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date of Approval of Consultants', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfaAppConsultantDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'RFA evaluation days information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfaEvalDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'RFA evaluation  date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfaEvalDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'RFA evaluation submission report  days  information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfaEvalRptSubDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'RFA evaluation submission report  date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfaEvalRptSubDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'RFA final selection days  information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfaFinalSelDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'RFA final selection date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfaFinalSelDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Days of interview of selected Individuals', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfaInterviewDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date of interview of selected Individuals', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfaInterviewDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'RFA receipt days information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfaReceiptDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'RFA receipt date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfaReceiptDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Daysof Approval for Award of Contract', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfpContractAppDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date of Approval for Award of Contract', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfpContractAppDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Days of RFP financial opening days information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfpFinancialOpenDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'date of RFP financial opening information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfpFinancialOpenDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Days of Completion of Negotiation ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfpNegCompDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date of Completion of Negotiation ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfpNegCompDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'RFP technical evaluation days information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfpTechEvalDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'RFP technical evaluation date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'rfpTechEvalDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Submission of application days information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'subDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Submission of application date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'subDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Days of Submission of Technical Sub-Committee Report', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'techSubCmtRptDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date of Submission of Technical Sub-Committee Report', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'techSubCmtRptDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Days of issue of Final Tender Document to qualified tenderers', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'tenderAdvertDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date of issue of Final Tender Document to qualified tenderers', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'tenderAdvertDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'days of tender contract approval information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'tenderContractAppDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'date of tender contract approval  information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'tenderContractAppDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date of Completion of Contract', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'tenderContractCompDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Days of Signing of Contract', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'tenderContractSignDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date of Signing of Contract', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'tenderContractSignDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Days of Approval of the Evaluation Report', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'tenderEvalRptAppDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'date of tender evaluation of report approval information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'tenderEvalRptAppDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Days of tender evaluation report information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'tenderEvalRptdays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date of tender evaluation report information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'tenderEvalRptDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Days of Issue of the Notification of Award', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'tenderNoaIssueDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date of Issue of the Notification of Award', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'tenderNoaIssueDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Days of Opening of Tender', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'tenderOpenDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date of Opening of Tender', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'tenderOpenDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Days of Submission of Tender', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'tenderSubDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date of Submission of Tender', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPqTenderDates', 'COLUMN', N'tenderSubDt'
GO
ALTER TABLE [dbo].[tbl_AppPqTenderDates] SET (LOCK_ESCALATION = TABLE)
GO
