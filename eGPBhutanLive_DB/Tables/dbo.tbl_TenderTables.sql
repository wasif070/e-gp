SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderTables] (
		[tenderTableId]         [int] IDENTITY(1, 1) NOT NULL,
		[tenderFormId]          [int] NOT NULL,
		[tableName]             [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tableHeader]           [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tableFooter]           [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[noOfRows]              [smallint] NOT NULL,
		[noOfCols]              [smallint] NOT NULL,
		[isMultipleFilling]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[templatetableId]       [int] NOT NULL,
		CONSTRAINT [PK_tbl_TenderTables]
		PRIMARY KEY
		CLUSTERED
		([tenderTableId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderTables]
	WITH CHECK
	ADD CONSTRAINT [FKAE4FB15A735760EE]
	FOREIGN KEY ([tenderFormId]) REFERENCES [dbo].[tbl_TenderForms] ([tenderFormId])
ALTER TABLE [dbo].[tbl_TenderTables]
	CHECK CONSTRAINT [FKAE4FB15A735760EE]

GO
ALTER TABLE [dbo].[tbl_TenderTables]
	WITH CHECK
	ADD CONSTRAINT [tenderFormId_FK1]
	FOREIGN KEY ([tenderFormId]) REFERENCES [dbo].[tbl_TenderForms] ([tenderFormId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderTables]
	CHECK CONSTRAINT [tenderFormId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderTables] SET (LOCK_ESCALATION = TABLE)
GO
