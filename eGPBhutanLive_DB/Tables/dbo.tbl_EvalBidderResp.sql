SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalBidderResp] (
		[evalBidderRspId]         [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]                [int] NOT NULL,
		[userId]                  [int] NOT NULL,
		[sentBy]                  [int] NOT NULL,
		[sentDate]                [smalldatetime] NOT NULL,
		[expectedComplDt]         [smalldatetime] NOT NULL,
		[remarks]                 [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[evalstatus]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isClarificationComp]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [evalbidderRspId_PK]
		PRIMARY KEY
		CLUSTERED
		([evalBidderRspId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalBidderResp]
	ADD
	CONSTRAINT [DF_tbl_EvalBidderResp_isClarificationComp]
	DEFAULT ('No') FOR [isClarificationComp]
GO
ALTER TABLE [dbo].[tbl_EvalBidderResp]
	WITH NOCHECK
	ADD CONSTRAINT [evlbidderRspTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EvalBidderResp]
	CHECK CONSTRAINT [evlbidderRspTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_EvalBidderResp] SET (LOCK_ESCALATION = TABLE)
GO
