SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_PccSubClause] (
		[pccSubClauseId]       [int] IDENTITY(1, 1) NOT NULL,
		[gccReference]         [int] NOT NULL,
		[orderNumber]          [smallint] NOT NULL,
		[pccSubClauseName]     [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[gccHeaderId]          [int] NOT NULL,
		[pccHeaderId]          [int] NULL,
		CONSTRAINT [UQ__tbl_PccS__3AE7B2064B98F47F]
		UNIQUE
		NONCLUSTERED
		([pccSubClauseId])
		ON [PRIMARY],
		CONSTRAINT [PK__tbl_PccS__3AE7B207D02109E9]
		PRIMARY KEY
		CLUSTERED
		([pccSubClauseId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_PccSubClause]
	WITH CHECK
	ADD CONSTRAINT [FK2153E3E0A1003B35]
	FOREIGN KEY ([pccHeaderId]) REFERENCES [dbo].[tbl_PccHeader] ([pccHeaderId])
ALTER TABLE [dbo].[tbl_PccSubClause]
	CHECK CONSTRAINT [FK2153E3E0A1003B35]

GO
ALTER TABLE [dbo].[tbl_PccSubClause]
	WITH CHECK
	ADD CONSTRAINT [gccHeader_FK1]
	FOREIGN KEY ([gccHeaderId]) REFERENCES [dbo].[tbl_GccHeader] ([gccHeaderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_PccSubClause]
	CHECK CONSTRAINT [gccHeader_FK1]

GO
ALTER TABLE [dbo].[tbl_PccSubClause] SET (LOCK_ESCALATION = TABLE)
GO
