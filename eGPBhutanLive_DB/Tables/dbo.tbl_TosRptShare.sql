SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TosRptShare] (
		[tosRptShareId]       [int] IDENTITY(1, 1) NOT NULL,
		[listingId]           [int] NOT NULL,
		[tenderId]            [int] NOT NULL,
		[comments]            [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sentBy]              [int] NOT NULL,
		[sentTo]              [int] NOT NULL,
		[sentDate]            [smalldatetime] NOT NULL,
		[sentByGovUserId]     [int] NULL,
		[sentToGovUserId]     [int] NULL,
		CONSTRAINT [tosRptShareId_Pk]
		PRIMARY KEY
		CLUSTERED
		([tosRptShareId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TosRptShare]
	WITH NOCHECK
	ADD CONSTRAINT [tosRptShareTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TosRptShare]
	CHECK CONSTRAINT [tosRptShareTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_TosRptShare] SET (LOCK_ESCALATION = TABLE)
GO
