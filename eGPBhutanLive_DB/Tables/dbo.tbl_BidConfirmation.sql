SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_BidConfirmation] (
		[confirmationId]         [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]               [int] NOT NULL,
		[maskName]               [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[confirmationDt]         [smalldatetime] NOT NULL,
		[eSignature]             [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[digitalSignature]       [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[userId]                 [int] NOT NULL,
		[confirmationStatus]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_BidConfirmation]
		PRIMARY KEY
		CLUSTERED
		([confirmationId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_BidConfirmation]
	ADD
	CONSTRAINT [DF_tbl_BidConfirmation_userId]
	DEFAULT ((0)) FOR [userId]
GO
ALTER TABLE [dbo].[tbl_BidConfirmation]
	WITH NOCHECK
	ADD CONSTRAINT [FKAE795B3198B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_BidConfirmation]
	CHECK CONSTRAINT [FKAE795B3198B66EC5]

GO
ALTER TABLE [dbo].[tbl_BidConfirmation] SET (LOCK_ESCALATION = TABLE)
GO
