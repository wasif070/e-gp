SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EmployeeFinancialPower] (
		[employeeFinPowerId]      [int] IDENTITY(1, 1) NOT NULL,
		[employeeId]              [int] NOT NULL,
		[procurementNatureId]     [tinyint] NOT NULL,
		[budgetTypeId]            [tinyint] NOT NULL,
		[operator]                [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[amount]                  [money] NOT NULL,
		[employeeRoleId]          [int] NOT NULL,
		[procurementMethodId]     [tinyint] NOT NULL,
		CONSTRAINT [PK_tbl_EmployeeFinancialPower]
		PRIMARY KEY
		CLUSTERED
		([employeeFinPowerId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EmployeeFinancialPower]
	ADD
	CONSTRAINT [DF_tbl_EmployeeFinancialPower_procurementNatureId]
	DEFAULT ((0)) FOR [procurementNatureId]
GO
ALTER TABLE [dbo].[tbl_EmployeeFinancialPower]
	WITH NOCHECK
	ADD CONSTRAINT [empBudgetType_FK3]
	FOREIGN KEY ([budgetTypeId]) REFERENCES [dbo].[tbl_BudgetType] ([budgetTypeId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EmployeeFinancialPower]
	CHECK CONSTRAINT [empBudgetType_FK3]

GO
ALTER TABLE [dbo].[tbl_EmployeeFinancialPower]
	WITH NOCHECK
	ADD CONSTRAINT [empFinancialPower_FK1]
	FOREIGN KEY ([employeeId]) REFERENCES [dbo].[tbl_EmployeeMaster] ([employeeId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EmployeeFinancialPower]
	CHECK CONSTRAINT [empFinancialPower_FK1]

GO
ALTER TABLE [dbo].[tbl_EmployeeFinancialPower]
	WITH NOCHECK
	ADD CONSTRAINT [FKD26E8A0BD6793EF9]
	FOREIGN KEY ([budgetTypeId]) REFERENCES [dbo].[tbl_BudgetType] ([budgetTypeId])
ALTER TABLE [dbo].[tbl_EmployeeFinancialPower]
	CHECK CONSTRAINT [FKD26E8A0BD6793EF9]

GO
ALTER TABLE [dbo].[tbl_EmployeeFinancialPower]
	WITH NOCHECK
	ADD CONSTRAINT [FKD26E8A0BF5489C79]
	FOREIGN KEY ([employeeId]) REFERENCES [dbo].[tbl_EmployeeMaster] ([employeeId])
ALTER TABLE [dbo].[tbl_EmployeeFinancialPower]
	CHECK CONSTRAINT [FKD26E8A0BF5489C79]

GO
ALTER TABLE [dbo].[tbl_EmployeeFinancialPower] SET (LOCK_ESCALATION = TABLE)
GO
