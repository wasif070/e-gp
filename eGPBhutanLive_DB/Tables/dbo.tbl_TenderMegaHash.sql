SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderMegaHash] (
		[tenderMegaHashId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]             [int] NOT NULL,
		[megaHash]             [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[megaHashDt]           [smalldatetime] NOT NULL,
		[isVerified]           [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_TenderMegaHash]
		PRIMARY KEY
		CLUSTERED
		([tenderMegaHashId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderMegaHash]
	ADD
	CONSTRAINT [DF_tbl_TenderMegaHash_isVerified]
	DEFAULT ('no') FOR [isVerified]
GO
ALTER TABLE [dbo].[tbl_TenderMegaHash]
	WITH NOCHECK
	ADD CONSTRAINT [tenderMegaHash_TenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderMegaHash]
	CHECK CONSTRAINT [tenderMegaHash_TenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderMegaHash] SET (LOCK_ESCALATION = TABLE)
GO
