SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ModuleMaster] (
		[moduleId]       [tinyint] IDENTITY(1, 1) NOT NULL,
		[moduleName]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [moduleId_PK]
		PRIMARY KEY
		CLUSTERED
		([moduleId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ModuleMaster] SET (LOCK_ESCALATION = TABLE)
GO
