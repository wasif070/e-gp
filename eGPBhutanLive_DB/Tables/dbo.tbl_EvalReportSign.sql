SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalReportSign] (
		[evalRptSignId]     [int] IDENTITY(1, 1) NOT NULL,
		[evalRptId]         [int] NOT NULL,
		[userId]            [int] NOT NULL,
		[signDt]            [smalldatetime] NOT NULL,
		[comments]          [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[signStatus]        [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_EvalReportSign]
		PRIMARY KEY
		CLUSTERED
		([evalRptSignId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalReportSign]
	WITH CHECK
	ADD CONSTRAINT [signEvalReportId_FK1]
	FOREIGN KEY ([evalRptId]) REFERENCES [dbo].[tbl_EvalReportMaster] ([evalRptId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EvalReportSign]
	CHECK CONSTRAINT [signEvalReportId_FK1]

GO
ALTER TABLE [dbo].[tbl_EvalReportSign] SET (LOCK_ESCALATION = TABLE)
GO
