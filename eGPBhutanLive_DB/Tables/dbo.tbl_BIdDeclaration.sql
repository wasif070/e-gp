SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_BIdDeclaration] (
		[DeclarationId]     [bigint] IDENTITY(1, 1) NOT NULL,
		[TenderId]          [int] NOT NULL,
		[LotId]             [int] NOT NULL,
		[UserId]            [int] NOT NULL,
		[CreatedDate]       [smalldatetime] NULL,
		[Remarks]           [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_BIdDeclaration]
		PRIMARY KEY
		CLUSTERED
		([DeclarationId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_BIdDeclaration] SET (LOCK_ESCALATION = TABLE)
GO
