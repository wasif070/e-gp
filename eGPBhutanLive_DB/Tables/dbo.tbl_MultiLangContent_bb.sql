SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_MultiLangContent_bb] (
		[contentId]        [int] IDENTITY(1, 1) NOT NULL,
		[title]            [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[subTitle]         [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[value]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[language]         [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[displayTitle]     [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_ContentManagment]
		PRIMARY KEY
		CLUSTERED
		([contentId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_MultiLangContent_bb] SET (LOCK_ESCALATION = TABLE)
GO
