SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TOSListing] (
		[ListingId]           [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]            [int] NOT NULL,
		[comments]            [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sentBy]              [int] NOT NULL,
		[sentTo]              [int] NOT NULL,
		[sentDate]            [smalldatetime] NOT NULL,
		[sentByGovUserId]     [int] NULL,
		[sentToGovUserId]     [int] NULL,
		CONSTRAINT [PK_tbl_TOSListing]
		PRIMARY KEY
		CLUSTERED
		([ListingId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TOSListing] SET (LOCK_ESCALATION = TABLE)
GO
