SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_TenderDocStatistics] (
		[docStatsId]     [int] IDENTITY(1, 1) NOT NULL,
		[downloadDt]     [datetime] NOT NULL,
		[pkgLotId]       [int] NOT NULL,
		[tenderId]       [int] NOT NULL,
		[userId]         [int] NOT NULL,
		CONSTRAINT [UQ__tbl_Tend__AF4116902F2B5F59]
		UNIQUE
		NONCLUSTERED
		([docStatsId])
		ON [PRIMARY],
		CONSTRAINT [PK__tbl_Tend__AF411691B019FBB1]
		PRIMARY KEY
		CLUSTERED
		([docStatsId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderDocStatistics] SET (LOCK_ESCALATION = TABLE)
GO
