SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_PriTopicMaster] (
		[topicId]         [int] IDENTITY(1, 1) NOT NULL,
		[topic]           [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[description]     [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[postedBy]        [int] NOT NULL,
		[postedDate]      [datetime] NOT NULL,
		[tenderId]        [int] NOT NULL,
		CONSTRAINT [PK_tbl_PriTopicMaster]
		PRIMARY KEY
		CLUSTERED
		([topicId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_PriTopicMaster] SET (LOCK_ESCALATION = TABLE)
GO
