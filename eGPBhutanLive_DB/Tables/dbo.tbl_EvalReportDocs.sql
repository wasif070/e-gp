SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalReportDocs] (
		[evalRptDocId]       [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]           [int] NOT NULL,
		[pkgLotId]           [int] NOT NULL,
		[documentName]       [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedBy]         [int] NOT NULL,
		[uploadedDt]         [datetime] NOT NULL,
		[roundId]            [int] NOT NULL,
		CONSTRAINT [PK_tbl_EvalReportDocs]
		PRIMARY KEY
		CLUSTERED
		([evalRptDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalReportDocs]
	ADD
	CONSTRAINT [DF_tbl_EvalReportDocs_roundId]
	DEFAULT ((1)) FOR [roundId]
GO
ALTER TABLE [dbo].[tbl_EvalReportDocs]
	WITH NOCHECK
	ADD CONSTRAINT [evalReportTenderDocId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_EvalReportDocs]
	CHECK CONSTRAINT [evalReportTenderDocId_FK1]

GO
ALTER TABLE [dbo].[tbl_EvalReportDocs] SET (LOCK_ESCALATION = TABLE)
GO
