SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderClose] (
		[tenderClosingId]       [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]              [int] NOT NULL,
		[comments]              [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]             [int] NOT NULL,
		[closingDate]           [smalldatetime] NOT NULL,
		[closedByGovUserId]     [int] NULL,
		CONSTRAINT [PK_tbl_TenderClose]
		PRIMARY KEY
		CLUSTERED
		([tenderClosingId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderClose] SET (LOCK_ESCALATION = TABLE)
GO
