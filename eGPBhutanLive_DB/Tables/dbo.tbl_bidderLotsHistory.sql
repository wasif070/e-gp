SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_bidderLotsHistory] (
		[bidderLotIdHis]     [int] IDENTITY(1, 1) NOT NULL,
		[deleDate]           [smalldatetime] NOT NULL,
		[insDate]            [smalldatetime] NOT NULL,
		[pkgLotId]           [int] NOT NULL,
		[tenderId]           [int] NOT NULL,
		[userId]             [int] NOT NULL,
		CONSTRAINT [UQ__tbl_bidd__1D5F844758D1301D]
		UNIQUE
		NONCLUSTERED
		([bidderLotIdHis])
		ON [PRIMARY],
		CONSTRAINT [PK__tbl_bidd__1D5F844655F4C372]
		PRIMARY KEY
		CLUSTERED
		([bidderLotIdHis])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_bidderLotsHistory] SET (LOCK_ESCALATION = TABLE)
GO
