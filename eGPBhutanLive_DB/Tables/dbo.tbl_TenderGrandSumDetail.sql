SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_TenderGrandSumDetail] (
		[sumDtlId]          [int] IDENTITY(1, 1) NOT NULL,
		[tenderSumId]       [int] NOT NULL,
		[tenderFormId]      [int] NOT NULL,
		[cellId]            [int] NOT NULL,
		[tenderTableId]     [int] NOT NULL,
		[columnId]          [int] NULL,
		CONSTRAINT [PK_tbl_TenderSumDetail]
		PRIMARY KEY
		CLUSTERED
		([sumDtlId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderGrandSumDetail]
	WITH CHECK
	ADD CONSTRAINT [FKDD8E18C5D3E48C65]
	FOREIGN KEY ([tenderSumId]) REFERENCES [dbo].[tbl_TenderGrandSum] ([tenderSumId])
ALTER TABLE [dbo].[tbl_TenderGrandSumDetail]
	CHECK CONSTRAINT [FKDD8E18C5D3E48C65]

GO
ALTER TABLE [dbo].[tbl_TenderGrandSumDetail]
	WITH CHECK
	ADD CONSTRAINT [tenderGrandSumId_FK1]
	FOREIGN KEY ([tenderSumId]) REFERENCES [dbo].[tbl_TenderGrandSum] ([tenderSumId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderGrandSumDetail]
	CHECK CONSTRAINT [tenderGrandSumId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderGrandSumDetail] SET (LOCK_ESCALATION = TABLE)
GO
