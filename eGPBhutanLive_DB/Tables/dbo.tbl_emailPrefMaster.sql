SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_emailPrefMaster] (
		[epid]              [int] IDENTITY(1, 1) NOT NULL,
		[prefranceType]     [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[usrtTypeID]        [varchar](60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[state]             [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[govtRoles]         [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_emailPrefMaster]
		PRIMARY KEY
		CLUSTERED
		([epid])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_emailPrefMaster] SET (LOCK_ESCALATION = TABLE)
GO
