SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_VariContractVal] (
		[variOrdCntValId]     [int] NOT NULL,
		[lotId]               [int] NOT NULL,
		[variOrdId]           [int] NOT NULL,
		[contractValue]       [decimal](15, 3) NOT NULL,
		[isCurrent]           [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_CMS_VariContractVal]
		PRIMARY KEY
		CLUSTERED
		([variOrdCntValId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_VariContractVal] SET (LOCK_ESCALATION = TABLE)
GO
