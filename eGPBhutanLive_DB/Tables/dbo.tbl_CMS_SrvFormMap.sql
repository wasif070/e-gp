SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_CMS_SrvFormMap] (
		[srvFormMapId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]         [int] NOT NULL,
		[lotId]            [int] NOT NULL,
		[formId]           [int] NOT NULL,
		[srvBoqId]         [int] NOT NULL,
		[createdDate]      [datetime] NOT NULL,
		[createdBy]        [int] NOT NULL,
		CONSTRAINT [PK_tbl_CMS_SrvFormMap]
		PRIMARY KEY
		CLUSTERED
		([srvFormMapId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvFormMap] SET (LOCK_ESCALATION = TABLE)
GO
