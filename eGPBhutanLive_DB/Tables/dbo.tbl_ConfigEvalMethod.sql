SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_ConfigEvalMethod] (
		[evalMethodConfId]        [int] IDENTITY(1, 1) NOT NULL,
		[procurementMethodId]     [int] NOT NULL,
		[tenderTypeId]            [int] NOT NULL,
		[procurementNatureId]     [int] NOT NULL,
		[noOfEnvelops]            [int] NOT NULL,
		[evalMethod]              [int] NOT NULL,
		CONSTRAINT [PK_tbl_ConfigEvalMethod]
		PRIMARY KEY
		CLUSTERED
		([evalMethodConfId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ConfigEvalMethod] SET (LOCK_ESCALATION = TABLE)
GO
