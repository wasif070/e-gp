SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_ComplaintHistory] (
		[complaintHistId]      [int] IDENTITY(1, 1) NOT NULL,
		[complaintId]          [int] NOT NULL,
		[comments]             [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[commentsDt]           [datetime] NOT NULL,
		[complaintLevelId]     [tinyint] NOT NULL,
		[fileStatus]           [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[complaintStatus]      [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[panelId]              [int] NOT NULL,
		[userTypeId]           [tinyint] NOT NULL,
		[govuserId]            [int] NULL,
		[tendererId]           [int] NULL,
		CONSTRAINT [PK_tbl_CMS_ComplaintHistory]
		PRIMARY KEY
		CLUSTERED
		([complaintHistId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintHistory]
	ADD
	CONSTRAINT [DF_tbl_CMS_ComplaintHistory_complaintStatus]
	DEFAULT ('pending') FOR [complaintStatus]
GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintHistory]
	ADD
	CONSTRAINT [DF_tbl_CMS_ComplaintHistory_fileStatus]
	DEFAULT ('register') FOR [fileStatus]
GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintHistory]
	ADD
	CONSTRAINT [DF_tbl_CMS_ComplaintHistory_userTypeId]
	DEFAULT ('tenderer') FOR [userTypeId]
GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintHistory]
	WITH CHECK
	ADD CONSTRAINT [cmsComplaintHistComplainrLevelId_FK3]
	FOREIGN KEY ([complaintLevelId]) REFERENCES [dbo].[tbl_CMS_ComplaintLevels] ([complaintLevelId])
ALTER TABLE [dbo].[tbl_CMS_ComplaintHistory]
	CHECK CONSTRAINT [cmsComplaintHistComplainrLevelId_FK3]

GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintHistory]
	WITH CHECK
	ADD CONSTRAINT [complaintHistUserTypeId_FK2]
	FOREIGN KEY ([userTypeId]) REFERENCES [dbo].[tbl_UserTypeMaster] ([userTypeId])
ALTER TABLE [dbo].[tbl_CMS_ComplaintHistory]
	CHECK CONSTRAINT [complaintHistUserTypeId_FK2]

GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintHistory]
	WITH CHECK
	ADD CONSTRAINT [historyComplaintId_FK1]
	FOREIGN KEY ([complaintId]) REFERENCES [dbo].[tbl_CMS_ComplaintMaster] ([complaintId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_ComplaintHistory]
	CHECK CONSTRAINT [historyComplaintId_FK1]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Processing Complaint User enters remarks that info stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintHistory', 'COLUMN', N'comments'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Processing Complaint Date & time info stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintHistory', 'COLUMN', N'commentsDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'System generated id info stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintHistory', 'COLUMN', N'complaintHistId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK tbl_CMS_ComplaintMaster', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintHistory', 'COLUMN', N'complaintId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Complaint Level Id info stoers here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintHistory', 'COLUMN', N'complaintLevelId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Complaint Status info stores here (pending / accept / reject / under clarification)', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintHistory', 'COLUMN', N'complaintStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'register/escalate/processed/reviewpanel/clarification', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintHistory', 'COLUMN', N'fileStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Panel Id info stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintHistory', 'COLUMN', N'panelId'
GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintHistory] SET (LOCK_ESCALATION = TABLE)
GO
