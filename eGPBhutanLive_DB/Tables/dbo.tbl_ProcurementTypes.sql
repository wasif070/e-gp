SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ProcurementTypes] (
		[procurementTypeId]     [tinyint] IDENTITY(1, 1) NOT NULL,
		[procurementType]       [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [UQ__tbl_Proc__73D39424CB986373]
		UNIQUE
		NONCLUSTERED
		([procurementTypeId])
		ON [PRIMARY],
		CONSTRAINT [PK__tbl_Proc__73D39425837FFCFB]
		PRIMARY KEY
		CLUSTERED
		([procurementTypeId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ProcurementTypes] SET (LOCK_ESCALATION = TABLE)
GO
