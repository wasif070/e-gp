SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalSentQueToCp] (
		[evalClrToCpId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]          [int] NOT NULL,
		[sentBy]            [int] NOT NULL,
		[sentDt]            [smalldatetime] NOT NULL,
		[evalStatus]        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[remarks]           [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sentFor]           [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_EvalSentQueToCp]
		PRIMARY KEY
		CLUSTERED
		([evalClrToCpId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalSentQueToCp]
	ADD
	CONSTRAINT [DF_tbl_EvalSentQueToCp_sentFor]
	DEFAULT ('question') FOR [sentFor]
GO
ALTER TABLE [dbo].[tbl_EvalSentQueToCp] SET (LOCK_ESCALATION = TABLE)
GO
