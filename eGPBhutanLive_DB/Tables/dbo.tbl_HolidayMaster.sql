SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_HolidayMaster] (
		[holidayId]       [int] IDENTITY(1, 1) NOT NULL,
		[holidayDate]     [date] NOT NULL,
		[isWeekend]       [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[year]            [varchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Description]     [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [holidayId_PK]
		PRIMARY KEY
		CLUSTERED
		([holidayId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Holiday date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_HolidayMaster', 'COLUMN', N'holidayDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'HolidayId is unique system generated Id , information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_HolidayMaster', 'COLUMN', N'holidayId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'isWeekend or not information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_HolidayMaster', 'COLUMN', N'isWeekend'
GO
ALTER TABLE [dbo].[tbl_HolidayMaster] SET (LOCK_ESCALATION = TABLE)
GO
