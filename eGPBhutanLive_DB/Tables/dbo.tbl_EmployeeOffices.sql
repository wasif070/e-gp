SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_EmployeeOffices] (
		[employeeOfficeId]     [int] IDENTITY(1, 1) NOT NULL,
		[employeeId]           [int] NOT NULL,
		[officeId]             [int] NOT NULL,
		[designationId]        [int] NULL,
		CONSTRAINT [employeeOfficeId_PK]
		PRIMARY KEY
		CLUSTERED
		([employeeOfficeId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EmployeeOffices]
	WITH CHECK
	ADD CONSTRAINT [employeeId_FK1]
	FOREIGN KEY ([employeeId]) REFERENCES [dbo].[tbl_EmployeeMaster] ([employeeId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EmployeeOffices]
	CHECK CONSTRAINT [employeeId_FK1]

GO
ALTER TABLE [dbo].[tbl_EmployeeOffices]
	WITH CHECK
	ADD CONSTRAINT [EmployeeOfficeId_Fk2]
	FOREIGN KEY ([officeId]) REFERENCES [dbo].[tbl_OfficeMaster] ([officeId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EmployeeOffices]
	CHECK CONSTRAINT [EmployeeOfficeId_Fk2]

GO
ALTER TABLE [dbo].[tbl_EmployeeOffices]
	WITH CHECK
	ADD CONSTRAINT [FKB14E9C08EAD91E95]
	FOREIGN KEY ([officeId]) REFERENCES [dbo].[tbl_OfficeMaster] ([officeId])
ALTER TABLE [dbo].[tbl_EmployeeOffices]
	CHECK CONSTRAINT [FKB14E9C08EAD91E95]

GO
ALTER TABLE [dbo].[tbl_EmployeeOffices]
	WITH CHECK
	ADD CONSTRAINT [FKB14E9C08F5489C79]
	FOREIGN KEY ([employeeId]) REFERENCES [dbo].[tbl_EmployeeMaster] ([employeeId])
ALTER TABLE [dbo].[tbl_EmployeeOffices]
	CHECK CONSTRAINT [FKB14E9C08F5489C79]

GO
ALTER TABLE [dbo].[tbl_EmployeeOffices] SET (LOCK_ESCALATION = TABLE)
GO
