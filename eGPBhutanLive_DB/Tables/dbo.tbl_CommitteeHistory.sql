SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CommitteeHistory] (
		[committeeHistId]     [int] IDENTITY(1, 1) NOT NULL,
		[committeeId]         [int] NOT NULL,
		[tenderId]            [int] NOT NULL,
		[committeeName]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]           [int] NOT NULL,
		[workflowStatus]      [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[committeStatus]      [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[minMembers]          [tinyint] NOT NULL,
		[maxMembers]          [tinyint] NOT NULL,
		[minMemOutSide]       [tinyint] NOT NULL,
		[committeeType]       [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isCurrent]           [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[remarks]             [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdDate]         [smalldatetime] NOT NULL,
		[publishDate]         [smalldatetime] NULL,
		[minMemFromPe]        [tinyint] NOT NULL,
		[minMemFromTec]       [tinyint] NOT NULL,
		CONSTRAINT [PK_tbl_CommitteeHistId]
		PRIMARY KEY
		CLUSTERED
		([committeeHistId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CommitteeHistory] SET (LOCK_ESCALATION = TABLE)
GO
