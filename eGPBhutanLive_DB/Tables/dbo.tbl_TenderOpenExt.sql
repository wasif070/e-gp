SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderOpenExt] (
		[ExtDtId]          [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]         [int] NOT NULL,
		[userId]           [int] NOT NULL,
		[extReqDt]         [smalldatetime] NOT NULL,
		[extReqBy]         [int] NOT NULL,
		[remarks]          [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[newOpenDt]        [smalldatetime] NOT NULL,
		[envelopeId]       [tinyint] NOT NULL,
		[extStatus]        [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[finalOpenDt]      [smalldatetime] NOT NULL,
		[hopeComments]     [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[approvalDt]       [smalldatetime] NOT NULL,
		CONSTRAINT [PK_tbl_TenderOpenExt]
		PRIMARY KEY
		CLUSTERED
		([ExtDtId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderOpenExt]
	WITH NOCHECK
	ADD CONSTRAINT [FK2CEABB2298B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_TenderOpenExt]
	CHECK CONSTRAINT [FK2CEABB2298B66EC5]

GO
ALTER TABLE [dbo].[tbl_TenderOpenExt]
	WITH NOCHECK
	ADD CONSTRAINT [tenderOpenId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderOpenExt]
	CHECK CONSTRAINT [tenderOpenId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderOpenExt] SET (LOCK_ESCALATION = TABLE)
GO
