SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TendererEsignature] (
		[eSignatureId]      [int] IDENTITY(1, 1) NOT NULL,
		[documentName]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[documentSize]      [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[documentBrief]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]      [smalldatetime] NOT NULL,
		[tendererId]        [int] NOT NULL,
		CONSTRAINT [eSignatureId_PK]
		PRIMARY KEY
		CLUSTERED
		([eSignatureId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TendererEsignature]
	WITH NOCHECK
	ADD CONSTRAINT [FKEC7D4195435CF11F]
	FOREIGN KEY ([tendererId]) REFERENCES [dbo].[tbl_TendererMaster] ([tendererId])
ALTER TABLE [dbo].[tbl_TendererEsignature]
	CHECK CONSTRAINT [FKEC7D4195435CF11F]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Document brief information stores here it will explain brief information about Tenderer eSignature document', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererEsignature', 'COLUMN', N'documentBrief'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name of document stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererEsignature', 'COLUMN', N'documentName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Size of document stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererEsignature', 'COLUMN', N'documentSize'
GO
EXEC sp_addextendedproperty N'MS_Description', N'System generated id information stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererEsignature', 'COLUMN', N'eSignatureId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'refer tbl_TendererMaster', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererEsignature', 'COLUMN', N'tendererId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date of uploaded document stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererEsignature', 'COLUMN', N'uploadedDate'
GO
ALTER TABLE [dbo].[tbl_TendererEsignature] SET (LOCK_ESCALATION = TABLE)
GO
