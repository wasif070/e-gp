SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_WorkFlowRoleMaster] (
		[wfRoleId]       [tinyint] IDENTITY(1, 1) NOT NULL,
		[wfRoleName]     [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [wfRoleId_PK]
		PRIMARY KEY
		CLUSTERED
		([wfRoleId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_WorkFlowRoleMaster] SET (LOCK_ESCALATION = TABLE)
GO
