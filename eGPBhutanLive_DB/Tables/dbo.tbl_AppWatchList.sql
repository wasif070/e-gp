SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_AppWatchList] (
		[appWatchId]       [int] IDENTITY(1, 1) NOT NULL,
		[userId]           [int] NOT NULL,
		[appId]            [int] NOT NULL,
		[packageId]        [int] NOT NULL,
		[creationDate]     [smalldatetime] NOT NULL,
		CONSTRAINT [appWatchId_PK]
		PRIMARY KEY
		CLUSTERED
		([appWatchId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_AppWatchList]
	WITH NOCHECK
	ADD CONSTRAINT [FKC74667ED14F1608F]
	FOREIGN KEY ([packageId]) REFERENCES [dbo].[tbl_AppPackages] ([packageId])
ALTER TABLE [dbo].[tbl_AppWatchList]
	CHECK CONSTRAINT [FKC74667ED14F1608F]

GO
ALTER TABLE [dbo].[tbl_AppWatchList]
	WITH NOCHECK
	ADD CONSTRAINT [FKC74667ED448F6D5F]
	FOREIGN KEY ([appId]) REFERENCES [dbo].[tbl_AppMaster] ([appId])
ALTER TABLE [dbo].[tbl_AppWatchList]
	CHECK CONSTRAINT [FKC74667ED448F6D5F]

GO
ALTER TABLE [dbo].[tbl_AppWatchList]
	WITH CHECK
	ADD CONSTRAINT [FKC74667ED6174DBD1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_AppWatchList]
	CHECK CONSTRAINT [FKC74667ED6174DBD1]

GO
ALTER TABLE [dbo].[tbl_AppWatchList]
	WITH NOCHECK
	ADD CONSTRAINT [watchAppId_Fk2]
	FOREIGN KEY ([appId]) REFERENCES [dbo].[tbl_AppMaster] ([appId])
ALTER TABLE [dbo].[tbl_AppWatchList]
	CHECK CONSTRAINT [watchAppId_Fk2]

GO
ALTER TABLE [dbo].[tbl_AppWatchList]
	WITH NOCHECK
	ADD CONSTRAINT [watchPackageId_Fk3]
	FOREIGN KEY ([packageId]) REFERENCES [dbo].[tbl_AppPackages] ([packageId])
ALTER TABLE [dbo].[tbl_AppWatchList]
	CHECK CONSTRAINT [watchPackageId_Fk3]

GO
ALTER TABLE [dbo].[tbl_AppWatchList]
	WITH CHECK
	ADD CONSTRAINT [watchUserId_FK1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_AppWatchList]
	CHECK CONSTRAINT [watchUserId_FK1]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Refer from tbl_AppMaster, to identify the AppId
', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppWatchList', 'COLUMN', N'appId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'System generated appWatchI information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppWatchList', 'COLUMN', N'appWatchId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'creationDate information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppWatchList', 'COLUMN', N'creationDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Refer from tbl_AppPackages, to identify the packageId', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppWatchList', 'COLUMN', N'packageId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Erefer from tbl_LoginMaster, to identify the userId', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppWatchList', 'COLUMN', N'userId'
GO
ALTER TABLE [dbo].[tbl_AppWatchList] SET (LOCK_ESCALATION = TABLE)
GO
