SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_WpMaster] (
		[wpId]              [int] IDENTITY(1, 1) NOT NULL,
		[wpLotId]           [int] NOT NULL,
		[wpName]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdDate]       [datetime] NOT NULL,
		[createdBy]         [int] NOT NULL,
		[isDateEdited]      [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[wrkWpStatus]       [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[isRepeatOrder]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_CMS_WpMaster]
		PRIMARY KEY
		CLUSTERED
		([wpId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_WpMaster]
	ADD
	CONSTRAINT [DF_tbl_CMS_WpMaster_isDateEdited]
	DEFAULT ('no') FOR [isDateEdited]
GO
ALTER TABLE [dbo].[tbl_CMS_WpMaster]
	ADD
	CONSTRAINT [DF_tbl_CMS_WpMaster_isRepeatOrder]
	DEFAULT ('no') FOR [isRepeatOrder]
GO
ALTER TABLE [dbo].[tbl_CMS_WpMaster] SET (LOCK_ESCALATION = TABLE)
GO
