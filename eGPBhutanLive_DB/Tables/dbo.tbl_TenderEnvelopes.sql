SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_TenderEnvelopes] (
		[tenderEnvelopeId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]             [int] NOT NULL,
		[envelopeId]           [tinyint] NOT NULL,
		[tenderFormId]         [int] NOT NULL,
		CONSTRAINT [PK_tbl_TenderEnvelopes]
		PRIMARY KEY
		CLUSTERED
		([tenderEnvelopeId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderEnvelopes]
	WITH CHECK
	ADD CONSTRAINT [FK78C9978E735760EE]
	FOREIGN KEY ([tenderFormId]) REFERENCES [dbo].[tbl_TenderForms] ([tenderFormId])
ALTER TABLE [dbo].[tbl_TenderEnvelopes]
	CHECK CONSTRAINT [FK78C9978E735760EE]

GO
ALTER TABLE [dbo].[tbl_TenderEnvelopes]
	WITH NOCHECK
	ADD CONSTRAINT [FK78C9978E98B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_TenderEnvelopes]
	CHECK CONSTRAINT [FK78C9978E98B66EC5]

GO
ALTER TABLE [dbo].[tbl_TenderEnvelopes]
	WITH NOCHECK
	ADD CONSTRAINT [tenderEnvelopeTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderEnvelopes]
	CHECK CONSTRAINT [tenderEnvelopeTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderEnvelopes]
	WITH CHECK
	ADD CONSTRAINT [tenderEnvForm_FK2]
	FOREIGN KEY ([tenderFormId]) REFERENCES [dbo].[tbl_TenderForms] ([tenderFormId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderEnvelopes]
	CHECK CONSTRAINT [tenderEnvForm_FK2]

GO
ALTER TABLE [dbo].[tbl_TenderEnvelopes] SET (LOCK_ESCALATION = TABLE)
GO
