SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_SrvSSVari] (
		[srvSSVariId]      [int] IDENTITY(1, 1) NOT NULL,
		[srvTCVariId]      [int] NOT NULL,
		[workFrom]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[startDt]          [datetime] NOT NULL,
		[endDt]            [datetime] NOT NULL,
		[noOfDays]         [int] NOT NULL,
		[empName]          [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[srvFormMapId]     [int] NOT NULL,
		[srno]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[variOrdId]        [int] NOT NULL,
		CONSTRAINT [PK_tbl_CMS_SrvSSVari]
		PRIMARY KEY
		CLUSTERED
		([srvSSVariId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvSSVari] SET (LOCK_ESCALATION = TABLE)
GO
