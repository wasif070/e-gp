SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_WorkFlowDocuments] (
		[wfDocumentId]     [int] IDENTITY(1, 1) NOT NULL,
		[documentName]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[description]      [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]     [smalldatetime] NOT NULL,
		[objectId]         [int] NOT NULL,
		[childId]          [int] NOT NULL,
		[activityId]       [smallint] NOT NULL,
		[userId]           [int] NOT NULL,
		[uploadedBy]       [int] NULL,
		CONSTRAINT [PK_tbl_WorkFlowDocuments]
		PRIMARY KEY
		CLUSTERED
		([wfDocumentId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_WorkFlowDocuments] SET (LOCK_ESCALATION = TABLE)
GO
