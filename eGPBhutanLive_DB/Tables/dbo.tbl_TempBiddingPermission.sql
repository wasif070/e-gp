SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TempBiddingPermission] (
		[BiddingPermissionId]     [bigint] IDENTITY(1, 1) NOT NULL,
		[ProcurementCategory]     [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[WorkType]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[WorkCategroy]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CompanyID]               [int] NOT NULL,
		[UserId]                  [int] NOT NULL,
		CONSTRAINT [PK_tbl_TempBiddingPermission]
		PRIMARY KEY
		CLUSTERED
		([BiddingPermissionId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TempBiddingPermission] SET (LOCK_ESCALATION = TABLE)
GO
