SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CommitteeMembersHistory] (
		[comMemberHistId]      [int] IDENTITY(1, 1) NOT NULL,
		[committeeId]          [int] NOT NULL,
		[userId]               [int] NOT NULL,
		[createdDate]          [smalldatetime] NOT NULL,
		[appStatus]            [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[appDate]              [smalldatetime] NOT NULL,
		[remarks]              [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[eSignature]           [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[digitalSignature]     [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[memberRole]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[memberFrom]           [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[govUserId]            [int] NOT NULL,
		CONSTRAINT [PK_tbl_CommitteeMembersHistId]
		PRIMARY KEY
		CLUSTERED
		([comMemberHistId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CommitteeMembersHistory]
	ADD
	CONSTRAINT [DF_tbl_CommitteeMembersHistory_govUserId]
	DEFAULT ((0)) FOR [govUserId]
GO
ALTER TABLE [dbo].[tbl_CommitteeMembersHistory] SET (LOCK_ESCALATION = TABLE)
GO
