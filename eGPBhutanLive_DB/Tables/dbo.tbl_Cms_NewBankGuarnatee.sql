SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Cms_NewBankGuarnatee] (
		[bankGId]              [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]             [int] NOT NULL,
		[pkgLotId]             [int] NOT NULL,
		[bgAmt]                [decimal](15, 3) NOT NULL,
		[createdBy]            [int] NOT NULL,
		[createdDt]            [datetime] NOT NULL,
		[lastDtOfPayment]      [datetime] NOT NULL,
		[userId]               [int] NOT NULL,
		[paymentId]            [int] NOT NULL,
		[validityDays]         [int] NOT NULL,
		[lastDtOfValidity]     [datetime] NULL,
		[remarks]              [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[currencyName]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_Cms_NewPerfGuarnatee]
		PRIMARY KEY
		CLUSTERED
		([bankGId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_Cms_NewBankGuarnatee]
	ADD
	CONSTRAINT [DF_tbl_Cms_NewBankGuarnatee_paymentId]
	DEFAULT ((0)) FOR [paymentId]
GO
ALTER TABLE [dbo].[tbl_Cms_NewBankGuarnatee]
	ADD
	CONSTRAINT [DF_tbl_Cms_NewBankGuarnatee_validityDays]
	DEFAULT ((0)) FOR [validityDays]
GO
ALTER TABLE [dbo].[tbl_Cms_NewBankGuarnatee] SET (LOCK_ESCALATION = TABLE)
GO
