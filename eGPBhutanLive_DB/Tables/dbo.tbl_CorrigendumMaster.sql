SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CorrigendumMaster] (
		[corrigendumId]         [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]              [int] NOT NULL,
		[corrigendumText]       [varchar](5000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[corrigendumStatus]     [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[workFlowStatus]        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_CorrigendumMaster]
		PRIMARY KEY
		CLUSTERED
		([corrigendumId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CorrigendumMaster]
	WITH NOCHECK
	ADD CONSTRAINT [corriTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CorrigendumMaster]
	CHECK CONSTRAINT [corriTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_CorrigendumMaster] SET (LOCK_ESCALATION = TABLE)
GO
