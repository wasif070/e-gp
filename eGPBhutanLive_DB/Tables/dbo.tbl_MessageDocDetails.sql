SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_MessageDocDetails] (
		[msgDocDetailId]      [int] IDENTITY(1, 1) NOT NULL,
		[msgTypeId]           [int] NOT NULL,
		[msgType]             [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[msgDocId]            [int] NOT NULL,
		[msgUploadedDate]     [smalldatetime] NOT NULL,
		CONSTRAINT [msgDocDetailId_PK]
		PRIMARY KEY
		CLUSTERED
		([msgDocDetailId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_MessageDocDetails]
	WITH CHECK
	ADD CONSTRAINT [FK7CCBAAF046C99A34]
	FOREIGN KEY ([msgDocId]) REFERENCES [dbo].[tbl_MessageDocument] ([msgDocId])
ALTER TABLE [dbo].[tbl_MessageDocDetails]
	CHECK CONSTRAINT [FK7CCBAAF046C99A34]

GO
ALTER TABLE [dbo].[tbl_MessageDocDetails]
	WITH CHECK
	ADD CONSTRAINT [msgDocId_FK1]
	FOREIGN KEY ([msgDocId]) REFERENCES [dbo].[tbl_MessageDocument] ([msgDocId])
ALTER TABLE [dbo].[tbl_MessageDocDetails]
	CHECK CONSTRAINT [msgDocId_FK1]

GO
ALTER TABLE [dbo].[tbl_MessageDocDetails] SET (LOCK_ESCALATION = TABLE)
GO
