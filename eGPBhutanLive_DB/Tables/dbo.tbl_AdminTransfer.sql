SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_AdminTransfer] (
		[admintransId]          [int] IDENTITY(1, 1) NOT NULL,
		[userId]                [int] NOT NULL,
		[fullName]              [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[nationalId]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[adminId]               [smallint] NOT NULL,
		[emailId]               [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[transferedBy]          [int] NULL,
		[transferDt]            [datetime] NULL,
		[replacedBy]            [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[replacedByEmailId]     [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[comments]              [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[isCurrent]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[mobileNo]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_AdminTransfer]
		PRIMARY KEY
		CLUSTERED
		([admintransId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_AdminTransfer]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_AdminTransfer_tbl_LoginMaster]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_AdminTransfer]
	CHECK CONSTRAINT [FK_tbl_AdminTransfer_tbl_LoginMaster]

GO
ALTER TABLE [dbo].[tbl_AdminTransfer] SET (LOCK_ESCALATION = TABLE)
GO
