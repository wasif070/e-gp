SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_WS_OrgMaster] (
		[orgId]             [int] IDENTITY(1, 1) NOT NULL,
		[emailId]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[loginPass]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[orgNameEn]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[orgNameBg]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[orgDetail]         [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[contactPerson]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[mobileNo]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[wsRights]          [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]         [int] NOT NULL,
		[createdDate]       [datetime] NOT NULL,
		[isDeleted]         [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_WS_OrgMaster]
		PRIMARY KEY
		CLUSTERED
		([orgId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_WS_OrgMaster]
	ADD
	CONSTRAINT [DF_tbl_WS_OrgMaster_isDeleted]
	DEFAULT ('N') FOR [isDeleted]
GO
ALTER TABLE [dbo].[tbl_WS_OrgMaster] SET (LOCK_ESCALATION = TABLE)
GO
