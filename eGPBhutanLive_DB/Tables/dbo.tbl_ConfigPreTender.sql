SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ConfigPreTender] (
		[preTenderMeetId]            [int] IDENTITY(1, 1) NOT NULL,
		[isPreTenderMeetingfReq]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[postQueDays]                [smallint] NOT NULL,
		[pubBidDays]                 [smallint] NOT NULL,
		[uploadedTenderDays]         [smallint] NOT NULL,
		[procurementMethodId]        [tinyint] NOT NULL,
		[procurementNatureId]        [tinyint] NOT NULL,
		[procurementTypeId]          [tinyint] NOT NULL,
		[tenderTypeId]               [tinyint] NOT NULL,
		CONSTRAINT [UQ__tbl_Conf__6FD5147DADB0D696]
		UNIQUE
		NONCLUSTERED
		([preTenderMeetId])
		ON [PRIMARY],
		CONSTRAINT [PK__tbl_Conf__6FD5147CBA387FA8]
		PRIMARY KEY
		CLUSTERED
		([preTenderMeetId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ConfigPreTender]
	WITH CHECK
	ADD CONSTRAINT [configProcMethodId_FK3]
	FOREIGN KEY ([procurementMethodId]) REFERENCES [dbo].[tbl_ProcurementMethod] ([procurementMethodId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ConfigPreTender]
	CHECK CONSTRAINT [configProcMethodId_FK3]

GO
ALTER TABLE [dbo].[tbl_ConfigPreTender]
	WITH CHECK
	ADD CONSTRAINT [configProcNatureId_FK2]
	FOREIGN KEY ([procurementNatureId]) REFERENCES [dbo].[tbl_ProcurementNature] ([procurementNatureId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ConfigPreTender]
	CHECK CONSTRAINT [configProcNatureId_FK2]

GO
ALTER TABLE [dbo].[tbl_ConfigPreTender]
	WITH CHECK
	ADD CONSTRAINT [configProcTypeId_FK4]
	FOREIGN KEY ([procurementTypeId]) REFERENCES [dbo].[tbl_ProcurementTypes] ([procurementTypeId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ConfigPreTender]
	CHECK CONSTRAINT [configProcTypeId_FK4]

GO
ALTER TABLE [dbo].[tbl_ConfigPreTender]
	WITH CHECK
	ADD CONSTRAINT [configTenderTypeId_FK1]
	FOREIGN KEY ([tenderTypeId]) REFERENCES [dbo].[tbl_TenderTypes] ([tenderTypeId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ConfigPreTender]
	CHECK CONSTRAINT [configTenderTypeId_FK1]

GO
ALTER TABLE [dbo].[tbl_ConfigPreTender]
	WITH CHECK
	ADD CONSTRAINT [FK957583943C4194A9]
	FOREIGN KEY ([procurementNatureId]) REFERENCES [dbo].[tbl_ProcurementNature] ([procurementNatureId])
ALTER TABLE [dbo].[tbl_ConfigPreTender]
	CHECK CONSTRAINT [FK957583943C4194A9]

GO
ALTER TABLE [dbo].[tbl_ConfigPreTender]
	WITH CHECK
	ADD CONSTRAINT [FK957583948C90EA2E]
	FOREIGN KEY ([tenderTypeId]) REFERENCES [dbo].[tbl_TenderTypes] ([tenderTypeId])
ALTER TABLE [dbo].[tbl_ConfigPreTender]
	CHECK CONSTRAINT [FK957583948C90EA2E]

GO
ALTER TABLE [dbo].[tbl_ConfigPreTender]
	WITH CHECK
	ADD CONSTRAINT [FK95758394A5C4169D]
	FOREIGN KEY ([procurementMethodId]) REFERENCES [dbo].[tbl_ProcurementMethod] ([procurementMethodId])
ALTER TABLE [dbo].[tbl_ConfigPreTender]
	CHECK CONSTRAINT [FK95758394A5C4169D]

GO
ALTER TABLE [dbo].[tbl_ConfigPreTender]
	WITH CHECK
	ADD CONSTRAINT [FK95758394A90681AE]
	FOREIGN KEY ([procurementTypeId]) REFERENCES [dbo].[tbl_ProcurementTypes] ([procurementTypeId])
ALTER TABLE [dbo].[tbl_ConfigPreTender]
	CHECK CONSTRAINT [FK95758394A90681AE]

GO
ALTER TABLE [dbo].[tbl_ConfigPreTender] SET (LOCK_ESCALATION = TABLE)
GO
