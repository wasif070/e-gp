SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_DebarmentTypes] (
		[debarTypeId]     [int] IDENTITY(1, 1) NOT NULL,
		[debarType]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_DebarmentTypes]
		PRIMARY KEY
		CLUSTERED
		([debarTypeId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_DebarmentTypes] SET (LOCK_ESCALATION = TABLE)
GO
