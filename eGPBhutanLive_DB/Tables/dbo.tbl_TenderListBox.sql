SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderListBox] (
		[tenderListId]     [int] IDENTITY(1, 1) NOT NULL,
		[listBoxName]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderFormId]     [int] NOT NULL,
		[listBoxId]        [int] NOT NULL,
		[isCalcReq]        [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_TenderListBox]
		PRIMARY KEY
		CLUSTERED
		([tenderListId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderListBox]
	ADD
	CONSTRAINT [DF_tbl_TenderListBox_isCalcReq]
	DEFAULT ('No') FOR [isCalcReq]
GO
ALTER TABLE [dbo].[tbl_TenderListBox] SET (LOCK_ESCALATION = TABLE)
GO
