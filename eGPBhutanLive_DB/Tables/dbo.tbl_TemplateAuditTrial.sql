SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TemplateAuditTrial] (
		[templateAuditId]     [int] IDENTITY(1, 1) NOT NULL,
		[templateId]          [smallint] NOT NULL,
		[childId]             [int] NOT NULL,
		[childType]           [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[oldValue]            [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[newValue]            [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[childName]           [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[actionDate]          [smalldatetime] NOT NULL,
		[actionBY]            [int] NOT NULL,
		CONSTRAINT [templateAuditId_PK]
		PRIMARY KEY
		CLUSTERED
		([templateAuditId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TemplateAuditTrial]
	WITH NOCHECK
	ADD CONSTRAINT [auditTrialTemplateId_FK1]
	FOREIGN KEY ([templateId]) REFERENCES [dbo].[tbl_TemplateMaster] ([templateId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TemplateAuditTrial]
	CHECK CONSTRAINT [auditTrialTemplateId_FK1]

GO
ALTER TABLE [dbo].[tbl_TemplateAuditTrial] SET (LOCK_ESCALATION = TABLE)
GO
