SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_WpDetail] (
		[wpDetailId]            [int] IDENTITY(1, 1) NOT NULL,
		[wpId]                  [int] NOT NULL,
		[groupId]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[wpSrNo]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[wpDescription]         [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[wpUom]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[wpQty]                 [decimal](15, 3) NULL,
		[wpRate]                [decimal](15, 3) NULL,
		[wpRowId]               [int] NOT NULL,
		[wpQualityCheckReq]     [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[wpItemStatus]          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[wpStartDate]           [datetime] NULL,
		[wpEndDate]             [datetime] NULL,
		[wpNoOfDays]            [int] NOT NULL,
		[createdBy]             [int] NOT NULL,
		[userTypeId]            [int] NOT NULL,
		[amendmentFlag]         [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderTableId]         [int] NULL,
		[itemInvStatus]         [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[itemInvGenQty]         [decimal](15, 3) NULL,
		[isRepeatOrder]         [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[completedDtNTime]      [datetime] NULL,
		[currencyName]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[supplierVat]           [decimal](15, 3) NULL,
		[InlandOthers]          [decimal](15, 3) NULL,
		CONSTRAINT [wpDetailId_PK]
		PRIMARY KEY
		CLUSTERED
		([wpDetailId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_WpDetail]
	ADD
	CONSTRAINT [DF_tbl_CMS_WpDetail_amendmentFlag]
	DEFAULT ('original') FOR [amendmentFlag]
GO
ALTER TABLE [dbo].[tbl_CMS_WpDetail]
	ADD
	CONSTRAINT [DF_tbl_Cms_WpDetail_InlandOthers]
	DEFAULT ((0)) FOR [InlandOthers]
GO
ALTER TABLE [dbo].[tbl_CMS_WpDetail]
	ADD
	CONSTRAINT [DF_tbl_CMS_WpDetail_isRepeatOrder]
	DEFAULT ('no') FOR [isRepeatOrder]
GO
ALTER TABLE [dbo].[tbl_CMS_WpDetail]
	ADD
	CONSTRAINT [DF_tbl_Cms_WpDetail_supplierVat]
	DEFAULT ((0)) FOR [supplierVat]
GO
ALTER TABLE [dbo].[tbl_CMS_WpDetail]
	ADD
	CONSTRAINT [DF_tbl_CMS_WpDetail_wpItemStatus]
	DEFAULT ('pending') FOR [wpItemStatus]
GO
ALTER TABLE [dbo].[tbl_CMS_WpDetail]
	ADD
	CONSTRAINT [DF_tbl_CMS_WpDetail_wpQualityCheckReq]
	DEFAULT ('No') FOR [wpQualityCheckReq]
GO
ALTER TABLE [dbo].[tbl_CMS_WpDetail]
	WITH CHECK
	ADD CONSTRAINT [cmsWpDetailWpId_FK1]
	FOREIGN KEY ([wpId]) REFERENCES [dbo].[tbl_CMS_WpMaster] ([wpId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_WpDetail]
	CHECK CONSTRAINT [cmsWpDetailWpId_FK1]

GO
ALTER TABLE [dbo].[tbl_CMS_WpDetail] SET (LOCK_ESCALATION = TABLE)
GO
