SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderValidityExt] (
		[valExtReqId]             [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]                [int] NOT NULL,
		[extReason]               [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[extRequestSignature]     [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[requestDate]             [smalldatetime] NOT NULL,
		[extStatus]               [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[extReplyActionBy]        [int] NOT NULL,
		[extReplyActionDate]      [smalldatetime] NOT NULL,
		[extReplySignature]       [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[extReqBy]                [int] NOT NULL,
		[extSentToRole]           [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_TenderValidityExt]
		PRIMARY KEY
		CLUSTERED
		([valExtReqId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderValidityExt]
	ADD
	CONSTRAINT [DF_tbl_TenderValidityExt_extReqBy]
	DEFAULT ((0)) FOR [extReqBy]
GO
ALTER TABLE [dbo].[tbl_TenderValidityExt]
	WITH NOCHECK
	ADD CONSTRAINT [valExtTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderValidityExt]
	CHECK CONSTRAINT [valExtTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderValidityExt] SET (LOCK_ESCALATION = TABLE)
GO
