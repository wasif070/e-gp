SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalServiceForms] (
		[esFormId]         [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]         [int] NOT NULL,
		[pkgLotId]         [int] NOT NULL,
		[tenderFormId]     [int] NOT NULL,
		[maxMarks]         [int] NULL,
		[configBy]         [int] NOT NULL,
		[configDt]         [smalldatetime] NOT NULL,
		[subCrietId]       [int] NOT NULL,
		[isCurrent]        [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_EvalServiceForms]
		PRIMARY KEY
		CLUSTERED
		([esFormId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalServiceForms]
	ADD
	CONSTRAINT [DF_tbl_EvalServiceForms_configDt]
	DEFAULT (getdate()) FOR [configDt]
GO
ALTER TABLE [dbo].[tbl_EvalServiceForms]
	ADD
	CONSTRAINT [DF_tbl_EvalServiceForms_isCurrent]
	DEFAULT ('no') FOR [isCurrent]
GO
ALTER TABLE [dbo].[tbl_EvalServiceForms]
	ADD
	CONSTRAINT [DF_tbl_EvalServiceForms_subCrietId]
	DEFAULT ((0)) FOR [subCrietId]
GO
ALTER TABLE [dbo].[tbl_EvalServiceForms] SET (LOCK_ESCALATION = TABLE)
GO
