SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ForgotPassword] (
		[forgotId]     [int] IDENTITY(1, 1) NOT NULL,
		[emailId]      [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[userHash]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[reqDt]        [datetime] NOT NULL,
		CONSTRAINT [PK_tbl_ForgotPassword]
		PRIMARY KEY
		CLUSTERED
		([forgotId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ForgotPassword] SET (LOCK_ESCALATION = TABLE)
GO
