SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_AuditTrailMaster] (
		[auditTrailId]     [int] IDENTITY(1, 1) NOT NULL,
		[sessionId]        [int] NOT NULL,
		[ipAddress]        [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[auditDt]          [datetime] NOT NULL,
		[pageName]         [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[objectId]         [int] NOT NULL,
		[module]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_AuditTrailMaster]
		PRIMARY KEY
		CLUSTERED
		([auditTrailId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_AuditTrailMaster] SET (LOCK_ESCALATION = TABLE)
GO
