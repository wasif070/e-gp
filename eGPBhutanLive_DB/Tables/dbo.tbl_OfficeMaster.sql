SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_OfficeMaster] (
		[officeId]               [int] IDENTITY(1, 1) NOT NULL,
		[officeName]             [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[address]                [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[countryId]              [smallint] NOT NULL,
		[stateId]                [smallint] NOT NULL,
		[city]                   [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[upjilla]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[phoneNo]                [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[faxNo]                  [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[postCode]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[departmentId]           [int] NOT NULL,
		[creationDate]           [smalldatetime] NOT NULL,
		[createdBy]              [int] NOT NULL,
		[status]                 [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[officeNameInBangla]     [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[pecode]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[prmsPECode]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[officeType]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [officeId_PK]
		PRIMARY KEY
		CLUSTERED
		([officeId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_OfficeMaster]
	ADD
	CONSTRAINT [DF__tbl_Offic__offic__3C54ED00]
	DEFAULT ('Others') FOR [officeType]
GO
ALTER TABLE [dbo].[tbl_OfficeMaster]
	ADD
	CONSTRAINT [DF_tbl_OfficeMaster_status]
	DEFAULT ('Pending') FOR [status]
GO
ALTER TABLE [dbo].[tbl_OfficeMaster]
	WITH CHECK
	ADD CONSTRAINT [countryId_FK1]
	FOREIGN KEY ([countryId]) REFERENCES [dbo].[tbl_CountryMaster] ([countryId])
ALTER TABLE [dbo].[tbl_OfficeMaster]
	CHECK CONSTRAINT [countryId_FK1]

GO
ALTER TABLE [dbo].[tbl_OfficeMaster]
	WITH CHECK
	ADD CONSTRAINT [FKD4695B3F1ADFF23F]
	FOREIGN KEY ([stateId]) REFERENCES [dbo].[tbl_StateMaster] ([stateId])
ALTER TABLE [dbo].[tbl_OfficeMaster]
	CHECK CONSTRAINT [FKD4695B3F1ADFF23F]

GO
ALTER TABLE [dbo].[tbl_OfficeMaster]
	WITH CHECK
	ADD CONSTRAINT [FKD4695B3FF059B349]
	FOREIGN KEY ([countryId]) REFERENCES [dbo].[tbl_CountryMaster] ([countryId])
ALTER TABLE [dbo].[tbl_OfficeMaster]
	CHECK CONSTRAINT [FKD4695B3FF059B349]

GO
ALTER TABLE [dbo].[tbl_OfficeMaster]
	WITH CHECK
	ADD CONSTRAINT [stateId_FK2]
	FOREIGN KEY ([stateId]) REFERENCES [dbo].[tbl_StateMaster] ([stateId])
ALTER TABLE [dbo].[tbl_OfficeMaster]
	CHECK CONSTRAINT [stateId_FK2]

GO
ALTER TABLE [dbo].[tbl_OfficeMaster] SET (LOCK_ESCALATION = TABLE)
GO
