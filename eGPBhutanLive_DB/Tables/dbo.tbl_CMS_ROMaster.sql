SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_ROMaster] (
		[romId]                 [int] IDENTITY(1, 1) NOT NULL,
		[lotId]                 [int] NOT NULL,
		[orderCreationDate]     [datetime] NOT NULL,
		[rptOrderStatus]        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]             [int] NOT NULL,
		[rptOrderWFStatus]      [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isCurrent]             [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[acceptRejStatus]       [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_CMS_ROMaster]
		PRIMARY KEY
		CLUSTERED
		([romId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_ROMaster]
	ADD
	CONSTRAINT [DF_tbl_CMS_ROMaster_isCurrent]
	DEFAULT ('yes') FOR [isCurrent]
GO
ALTER TABLE [dbo].[tbl_CMS_ROMaster] SET (LOCK_ESCALATION = TABLE)
GO
