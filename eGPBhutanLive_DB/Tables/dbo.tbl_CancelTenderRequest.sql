SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CancelTenderRequest] (
		[canTenderId]        [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]           [int] NOT NULL,
		[remarks]            [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]          [int] NOT NULL,
		[reqDateTime]        [datetime] NOT NULL,
		[tenderStatus]       [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[workflowStatus]     [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_CancelTenderRequest]
		PRIMARY KEY
		CLUSTERED
		([canTenderId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CancelTenderRequest]
	ADD
	CONSTRAINT [DF_tbl_CancelTenderRequest_workflowStatus]
	DEFAULT ('pending') FOR [workflowStatus]
GO
ALTER TABLE [dbo].[tbl_CancelTenderRequest]
	WITH NOCHECK
	ADD CONSTRAINT [cancelTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CancelTenderRequest]
	CHECK CONSTRAINT [cancelTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_CancelTenderRequest]
	WITH NOCHECK
	ADD CONSTRAINT [FK6C7E22A098B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_CancelTenderRequest]
	CHECK CONSTRAINT [FK6C7E22A098B66EC5]

GO
ALTER TABLE [dbo].[tbl_CancelTenderRequest] SET (LOCK_ESCALATION = TABLE)
GO
