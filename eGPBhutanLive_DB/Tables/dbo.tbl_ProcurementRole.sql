SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ProcurementRole] (
		[procurementRoleId]     [tinyint] IDENTITY(1, 1) NOT NULL,
		[procurementRole]       [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [procurementRoleId_PK]
		PRIMARY KEY
		CLUSTERED
		([procurementRoleId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ProcurementRole] SET (LOCK_ESCALATION = TABLE)
GO
