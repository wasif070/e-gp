SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ListBoxMaster] (
		[listBoxId]          [int] IDENTITY(1, 1) NOT NULL,
		[listBoxName]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[templateFormId]     [int] NOT NULL,
		[isCalcReq]          [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [listBoxId_PK]
		PRIMARY KEY
		CLUSTERED
		([listBoxId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ListBoxMaster]
	ADD
	CONSTRAINT [DF_tbl_ListBoxMaster_isCalcReq]
	DEFAULT ('No') FOR [isCalcReq]
GO
ALTER TABLE [dbo].[tbl_ListBoxMaster]
	ADD
	CONSTRAINT [DF_tbl_ListBoxMaster_templateFormId]
	DEFAULT ((0)) FOR [templateFormId]
GO
ALTER TABLE [dbo].[tbl_ListBoxMaster] SET (LOCK_ESCALATION = TABLE)
GO
