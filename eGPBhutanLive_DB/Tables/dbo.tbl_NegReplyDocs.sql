SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_NegReplyDocs] (
		[negReplyDocId]      [int] IDENTITY(1, 1) NOT NULL,
		[negQueryId]         [int] NOT NULL,
		[documentName]       [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]       [smalldatetime] NOT NULL,
		[uploadedBy]         [int] NOT NULL,
		[tenderId]           [int] NOT NULL,
		[docType]            [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [negReplyDocId_PK]
		PRIMARY KEY
		CLUSTERED
		([negReplyDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_NegReplyDocs]
	WITH CHECK
	ADD CONSTRAINT [FK25C05F9675727AEB]
	FOREIGN KEY ([negQueryId]) REFERENCES [dbo].[tbl_NegQuery] ([negQueryId])
ALTER TABLE [dbo].[tbl_NegReplyDocs]
	CHECK CONSTRAINT [FK25C05F9675727AEB]

GO
ALTER TABLE [dbo].[tbl_NegReplyDocs]
	WITH CHECK
	ADD CONSTRAINT [replyDocNegQueryId_FK1]
	FOREIGN KEY ([negQueryId]) REFERENCES [dbo].[tbl_NegQuery] ([negQueryId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_NegReplyDocs]
	CHECK CONSTRAINT [replyDocNegQueryId_FK1]

GO
ALTER TABLE [dbo].[tbl_NegReplyDocs] SET (LOCK_ESCALATION = TABLE)
GO
