SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_PromisIndicator] (
		[Indicator_ID]                   [int] IDENTITY(1, 1) NOT NULL,
		[officeId]                       [int] NOT NULL,
		[PEOffice_Name]                  [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[tenderId]                       [int] NULL,
		[PublishNewspaper]               [int] NULL,
		[PublishCPTUWebsite]             [int] NULL,
		[MultiLocSubmission]             [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DaysBetTenPubTenderClosing]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SuffTenderSubTime]              [int] NULL,
		[TenderDocSold]                  [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TenderersPartCount]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TenderSubVSDocSold]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TOCMemberFromTEC]               [int] NULL,
		[TECFormedByAA]                  [int] NULL,
		[TECExternalMembers]             [int] NULL,
		[EvaluationTime]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EvalCompletedInTime]            [int] NULL,
		[ResponsiveTenderersCount]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsReTendered]                   [int] NULL,
		[IsTenderCancelled]              [int] NULL,
		[DaysBtwnTenEvalTenApp]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsApprovedByProperAA]           [int] NULL,
		[SubofEvalReportAA]              [int] NULL,
		[AppofContractByAAInTimr]        [int] NULL,
		[TERReviewByOther]               [int] NULL,
		[ContractAppByHighAuth]          [int] NULL,
		[DaysBtwnContappNOA]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DaysBtwnTenOpenNOA]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DaysBtwnIFTNOA]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AwardPubCPTUWebsite]            [int] NULL,
		[AwardedInTenValidityPeriod]     [int] NULL,
		[DeliveredInTime]                [int] NULL,
		[LiquidatedDamage]               [int] NULL,
		[FullyCompleted]                 [int] NULL,
		[DaysReleasePayment]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LatePayment]                    [int] NULL,
		[InterestPaid]                   [int] NULL,
		[ComplaintReceived]              [int] NULL,
		[ResolAwardModification]         [int] NULL,
		[ComplaintResolved]              [int] NULL,
		[RevPanelDecUpheld]              [int] NULL,
		[IsContAmended]                  [int] NULL,
		[ContUnresolvDispute]            [int] NULL,
		[DetFraudCorruption]             [int] NULL,
		[Ministry_ID]                    [int] NULL,
		[Division_ID]                    [int] NULL,
		[Org_ID]                         [int] NULL,
		[Modify_Date]                    [smalldatetime] NULL,
		[Is_TenderOnline]                [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Is_Progress]                    [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DPRules]                        [int] NULL,
		[GoBProcRules]                   [int] NULL,
		[prmsPECode]                     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_Tbl_PromisIndicator]
		PRIMARY KEY
		CLUSTERED
		([Indicator_ID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tbl_PromisIndicator]
	ADD
	CONSTRAINT [DF_Tbl_PromisIndicator_ComplaintReceived]
	DEFAULT ((0)) FOR [ComplaintReceived]
GO
ALTER TABLE [dbo].[Tbl_PromisIndicator]
	ADD
	CONSTRAINT [DF_Tbl_PromisIndicator_ComplaintResolved]
	DEFAULT ((0)) FOR [ComplaintResolved]
GO
ALTER TABLE [dbo].[Tbl_PromisIndicator]
	ADD
	CONSTRAINT [DF_Tbl_PromisIndicator_ContUnresolvDispute]
	DEFAULT ((0)) FOR [ContUnresolvDispute]
GO
ALTER TABLE [dbo].[Tbl_PromisIndicator]
	ADD
	CONSTRAINT [DF_Tbl_PromisIndicator_DaysReleasePayment]
	DEFAULT (N'NA') FOR [DaysReleasePayment]
GO
ALTER TABLE [dbo].[Tbl_PromisIndicator]
	ADD
	CONSTRAINT [DF_Tbl_PromisIndicator_DeliveredInTime]
	DEFAULT ((0)) FOR [DeliveredInTime]
GO
ALTER TABLE [dbo].[Tbl_PromisIndicator]
	ADD
	CONSTRAINT [DF_Tbl_PromisIndicator_DetFraudCorruption]
	DEFAULT ((0)) FOR [DetFraudCorruption]
GO
ALTER TABLE [dbo].[Tbl_PromisIndicator]
	ADD
	CONSTRAINT [DF_Tbl_PromisIndicator_FullyCompleted]
	DEFAULT ((0)) FOR [FullyCompleted]
GO
ALTER TABLE [dbo].[Tbl_PromisIndicator]
	ADD
	CONSTRAINT [DF_Tbl_PromisIndicator_InterestPaid]
	DEFAULT ((0)) FOR [InterestPaid]
GO
ALTER TABLE [dbo].[Tbl_PromisIndicator]
	ADD
	CONSTRAINT [DF_Tbl_PromisIndicator_IsContAmended]
	DEFAULT ((0)) FOR [IsContAmended]
GO
ALTER TABLE [dbo].[Tbl_PromisIndicator]
	ADD
	CONSTRAINT [DF_Tbl_PromisIndicator_LatePayment]
	DEFAULT ((0)) FOR [LatePayment]
GO
ALTER TABLE [dbo].[Tbl_PromisIndicator]
	ADD
	CONSTRAINT [DF_Tbl_PromisIndicator_LiquidatedDamage]
	DEFAULT ((0)) FOR [LiquidatedDamage]
GO
ALTER TABLE [dbo].[Tbl_PromisIndicator]
	ADD
	CONSTRAINT [DF_Tbl_PromisIndicator_ResolAwardModification]
	DEFAULT ((0)) FOR [ResolAwardModification]
GO
ALTER TABLE [dbo].[Tbl_PromisIndicator]
	ADD
	CONSTRAINT [DF_Tbl_PromisIndicator_RevPanelDecUpheld]
	DEFAULT ((0)) FOR [RevPanelDecUpheld]
GO
ALTER TABLE [dbo].[Tbl_PromisIndicator]
	WITH CHECK
	ADD CONSTRAINT [FK_Tbl_PromisIndicator_tbl_OfficeMaster]
	FOREIGN KEY ([officeId]) REFERENCES [dbo].[tbl_OfficeMaster] ([officeId])
ALTER TABLE [dbo].[Tbl_PromisIndicator]
	CHECK CONSTRAINT [FK_Tbl_PromisIndicator_tbl_OfficeMaster]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key of Tbl_PromisIndicator', 'SCHEMA', N'dbo', 'TABLE', N'Tbl_PromisIndicator', 'COLUMN', N'Indicator_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Foregin key of Tbl_OfficeMaster', 'SCHEMA', N'dbo', 'TABLE', N'Tbl_PromisIndicator', 'COLUMN', N'officeId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PE Office Name', 'SCHEMA', N'dbo', 'TABLE', N'Tbl_PromisIndicator', 'COLUMN', N'PEOffice_Name'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Tender Id', 'SCHEMA', N'dbo', 'TABLE', N'Tbl_PromisIndicator', 'COLUMN', N'tenderId'
GO
ALTER TABLE [dbo].[Tbl_PromisIndicator] SET (LOCK_ESCALATION = TABLE)
GO
