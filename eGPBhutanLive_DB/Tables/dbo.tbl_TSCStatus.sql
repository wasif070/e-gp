SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TSCStatus] (
		[tscStatusId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]        [int] NOT NULL,
		[peUserId]        [int] NOT NULL,
		[sendToAA]        [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AAUserId]        [int] NOT NULL,
		[AASentDt]        [datetime] NULL,
		[PESentDt]        [datetime] NULL,
		[isTECCP]         [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[status]          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_TSCStatus]
		PRIMARY KEY
		CLUSTERED
		([tscStatusId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TSCStatus] SET (LOCK_ESCALATION = TABLE)
GO
