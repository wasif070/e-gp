SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_VendorRatingMaster] (
		[vendorRatingId]     [int] IDENTITY(1, 1) NOT NULL,
		[vendorStars]        [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[vendorRatings]      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_CMS_VendorRatingMaster]
		PRIMARY KEY
		CLUSTERED
		([vendorRatingId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_VendorRatingMaster] SET (LOCK_ESCALATION = TABLE)
GO
