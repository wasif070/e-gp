SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_PublicProcureForum] (
		[ppfId]           [int] IDENTITY(1, 1) NOT NULL,
		[postedBy]        [int] NOT NULL,
		[subject]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[description]     [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[postDate]        [smalldatetime] NOT NULL,
		[ppfParentId]     [int] NULL,
		[fullName]        [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[status]          [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[comments]        [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [ppfId_PK]
		PRIMARY KEY
		CLUSTERED
		([ppfId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_PublicProcureForum]
	ADD
	CONSTRAINT [DF_tbl_PublicProcureForum_ppfParentId]
	DEFAULT ((0)) FOR [ppfParentId]
GO
ALTER TABLE [dbo].[tbl_PublicProcureForum] SET (LOCK_ESCALATION = TABLE)
GO
