SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TemplateColumns] (
		[templateColId]     [int] IDENTITY(1, 1) NOT NULL,
		[columnId]          [smallint] NOT NULL,
		[columnHeader]      [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[dataType]          [tinyint] NOT NULL,
		[filledBy]          [tinyint] NOT NULL,
		[columnType]        [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sortOrder]         [smallint] NOT NULL,
		[tableId]           [int] NOT NULL,
		[formId]            [int] NOT NULL,
		[sectionId]         [int] NOT NULL,
		[showorhide]        [varchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_TemplateColumns]
		PRIMARY KEY
		CLUSTERED
		([templateColId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TemplateColumns]
	WITH CHECK
	ADD CONSTRAINT [FKD8D5D762F1EE9A68]
	FOREIGN KEY ([tableId]) REFERENCES [dbo].[tbl_TemplateTables] ([tableId])
ALTER TABLE [dbo].[tbl_TemplateColumns]
	CHECK CONSTRAINT [FKD8D5D762F1EE9A68]

GO
ALTER TABLE [dbo].[tbl_TemplateColumns]
	WITH CHECK
	ADD CONSTRAINT [tablecolumn_FK1]
	FOREIGN KEY ([tableId]) REFERENCES [dbo].[tbl_TemplateTables] ([tableId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TemplateColumns]
	CHECK CONSTRAINT [tablecolumn_FK1]

GO
ALTER TABLE [dbo].[tbl_TemplateColumns] SET (LOCK_ESCALATION = TABLE)
GO
