SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ConfigProcurement] (
		[configProcurementId]     [int] IDENTITY(1, 1) NOT NULL,
		[isNationalDisaster]      [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isUrgency]               [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[maxSubDays]              [smallint] NOT NULL,
		[maxValue]                [numeric](19, 2) NOT NULL,
		[minSubDays]              [smallint] NOT NULL,
		[minValue]                [numeric](19, 2) NOT NULL,
		[budgetTypeId]            [tinyint] NOT NULL,
		[procurementMethodId]     [tinyint] NOT NULL,
		[procurementNatureId]     [tinyint] NOT NULL,
		[procurementTypeId]       [tinyint] NOT NULL,
		[tenderTypeId]            [tinyint] NOT NULL,
		CONSTRAINT [UQ__tbl_Conf__AC3B81EB45099505]
		UNIQUE
		NONCLUSTERED
		([configProcurementId])
		ON [PRIMARY],
		CONSTRAINT [PK__tbl_Conf__AC3B81EA422D285A]
		PRIMARY KEY
		CLUSTERED
		([configProcurementId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ConfigProcurement]
	WITH CHECK
	ADD CONSTRAINT [FK444993AD3C4194A9]
	FOREIGN KEY ([procurementNatureId]) REFERENCES [dbo].[tbl_ProcurementNature] ([procurementNatureId])
ALTER TABLE [dbo].[tbl_ConfigProcurement]
	CHECK CONSTRAINT [FK444993AD3C4194A9]

GO
ALTER TABLE [dbo].[tbl_ConfigProcurement]
	WITH CHECK
	ADD CONSTRAINT [FK444993AD8C90EA2E]
	FOREIGN KEY ([tenderTypeId]) REFERENCES [dbo].[tbl_TenderTypes] ([tenderTypeId])
ALTER TABLE [dbo].[tbl_ConfigProcurement]
	CHECK CONSTRAINT [FK444993AD8C90EA2E]

GO
ALTER TABLE [dbo].[tbl_ConfigProcurement]
	WITH CHECK
	ADD CONSTRAINT [FK444993ADA5C4169D]
	FOREIGN KEY ([procurementMethodId]) REFERENCES [dbo].[tbl_ProcurementMethod] ([procurementMethodId])
ALTER TABLE [dbo].[tbl_ConfigProcurement]
	CHECK CONSTRAINT [FK444993ADA5C4169D]

GO
ALTER TABLE [dbo].[tbl_ConfigProcurement]
	WITH CHECK
	ADD CONSTRAINT [FK444993ADA90681AE]
	FOREIGN KEY ([procurementTypeId]) REFERENCES [dbo].[tbl_ProcurementTypes] ([procurementTypeId])
ALTER TABLE [dbo].[tbl_ConfigProcurement]
	CHECK CONSTRAINT [FK444993ADA90681AE]

GO
ALTER TABLE [dbo].[tbl_ConfigProcurement]
	WITH CHECK
	ADD CONSTRAINT [FK444993ADD6793EF9]
	FOREIGN KEY ([budgetTypeId]) REFERENCES [dbo].[tbl_BudgetType] ([budgetTypeId])
ALTER TABLE [dbo].[tbl_ConfigProcurement]
	CHECK CONSTRAINT [FK444993ADD6793EF9]

GO
ALTER TABLE [dbo].[tbl_ConfigProcurement] SET (LOCK_ESCALATION = TABLE)
GO
