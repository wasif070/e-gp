SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderDomesticPref] (
		[domPriceId]       [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]         [int] NOT NULL,
		[userId]           [int] NOT NULL,
		[createdBy]        [int] NOT NULL,
		[createdDt]        [datetime] NOT NULL,
		[domesticPref]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[evalStatus]       [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_TenderDomesticPref]
		PRIMARY KEY
		CLUSTERED
		([domPriceId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderDomesticPref]
	ADD
	CONSTRAINT [DF_tbl_TenderDomesticPref_domesticPref]
	DEFAULT ('No') FOR [domesticPref]
GO
ALTER TABLE [dbo].[tbl_TenderDomesticPref]
	ADD
	CONSTRAINT [DF_tbl_TenderDomesticPref_evalStatus]
	DEFAULT ('evaluation') FOR [evalStatus]
GO
ALTER TABLE [dbo].[tbl_TenderDomesticPref]
	WITH NOCHECK
	ADD CONSTRAINT [FKF5A4BF9698B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_TenderDomesticPref]
	CHECK CONSTRAINT [FKF5A4BF9698B66EC5]

GO
ALTER TABLE [dbo].[tbl_TenderDomesticPref]
	WITH NOCHECK
	ADD CONSTRAINT [tenderDomPefId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderDomesticPref]
	CHECK CONSTRAINT [tenderDomPefId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderDomesticPref] SET (LOCK_ESCALATION = TABLE)
GO
