SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_SrvAttSheetHistory] (
		[srvAttSheetHistId]     [int] NOT NULL,
		[attSheetId]            [int] NOT NULL,
		[srvTCId]               [int] NOT NULL,
		[workFrom]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[noOfDays]              [int] NOT NULL,
		[histCnt]               [int] NOT NULL,
		[attSheetDtlId]         [int] NOT NULL,
		CONSTRAINT [PK_tbl_CMS_SrvAttSheetHistory]
		PRIMARY KEY
		CLUSTERED
		([srvAttSheetHistId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvAttSheetHistory] SET (LOCK_ESCALATION = TABLE)
GO
