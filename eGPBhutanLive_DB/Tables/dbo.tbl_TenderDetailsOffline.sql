SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderDetailsOffline] (
		[tenderOfflineId]               [int] IDENTITY(1, 1) NOT NULL,
		[ministryOrDivision]            [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[agency]                        [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[peOfficeName]                  [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[peCode]                        [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[peDistrict]                    [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[eventType]                     [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[invitationFor]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[reoiRfpFor]                    [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[reoiRfpRefNo]                  [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IssueDate]                     [smalldatetime] NULL,
		[procurementMethod]             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[procurementNature]             [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[procurementType]               [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[budgetType]                    [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sourceOfFund]                  [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[devPartners]                   [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[projectCode]                   [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[projectName]                   [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[packageNo]                     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[PackageName]                   [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[tenderPubDate]                 [smalldatetime] NULL,
		[preTenderREOIDate]             [smalldatetime] NULL,
		[PreTenderREOIPlace]            [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LastSellingDate]               [smalldatetime] NULL,
		[ClosingDate]                   [smalldatetime] NULL,
		[OpeningDate]                   [smalldatetime] NULL,
		[SellingAddPrinciple]           [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SellingAddOthers]              [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ReceivingAdd]                  [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OpeningAdd]                    [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EligibilityCriteria]           [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BriefDescription]              [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RelServicesOrDeliverables]     [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OtherDetails]                  [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ForeignFirm]                   [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DocumentPrice]                 [money] NULL,
		[TenderStatus]                  [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[peName]                        [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[peDesignation]                 [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[peAddress]                     [varchar](5000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[peContactDetails]              [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UserID]                        [int] NULL,
		[InsertUpdateDate]              [smalldatetime] NULL,
		[ApproveComments]               [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [tenderOfflineId_PK]
		PRIMARY KEY
		CLUSTERED
		([tenderOfflineId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderDetailsOffline]
	ADD
	CONSTRAINT [DF_tbl_TenderDetailsOffline_TenderStatus]
	DEFAULT ('Pending') FOR [TenderStatus]
GO
ALTER TABLE [dbo].[tbl_TenderDetailsOffline] SET (LOCK_ESCALATION = TABLE)
GO
