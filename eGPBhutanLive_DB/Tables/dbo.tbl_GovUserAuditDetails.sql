SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_GovUserAuditDetails] (
		[auditId]             [int] IDENTITY(1, 1) NOT NULL,
		[sessionId]           [int] NOT NULL,
		[objectId]            [int] NOT NULL,
		[objectType]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[actionPerformed]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[activityDtTime]      [datetime] NOT NULL,
		[moduleName]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[activityURL]         [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[activityRemarks]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ipAddress]           [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [auditId_PK]
		PRIMARY KEY
		CLUSTERED
		([auditId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_GovUserAuditDetails]
	WITH CHECK
	ADD CONSTRAINT [govUserAuditDetSessionId_FK1]
	FOREIGN KEY ([sessionId]) REFERENCES [dbo].[tbl_SessionMaster] ([sessionId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_GovUserAuditDetails]
	CHECK CONSTRAINT [govUserAuditDetSessionId_FK1]

GO
ALTER TABLE [dbo].[tbl_GovUserAuditDetails] SET (LOCK_ESCALATION = TABLE)
GO
