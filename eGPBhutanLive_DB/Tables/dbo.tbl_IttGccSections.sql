SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_IttGccSections] (
		[ittGccSectionId]     [int] IDENTITY(1, 1) NOT NULL,
		[sectionType]         [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[templateId]          [smallint] NOT NULL,
		[sectionId]           [int] NOT NULL,
		[noOfSection]         [smallint] NOT NULL,
		CONSTRAINT [ittGccSectionId_PK]
		PRIMARY KEY
		CLUSTERED
		([ittGccSectionId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_IttGccSections]
	WITH NOCHECK
	ADD CONSTRAINT [FKC3D9DCAD904D9C11]
	FOREIGN KEY ([templateId]) REFERENCES [dbo].[tbl_TemplateMaster] ([templateId])
ALTER TABLE [dbo].[tbl_IttGccSections]
	CHECK CONSTRAINT [FKC3D9DCAD904D9C11]

GO
ALTER TABLE [dbo].[tbl_IttGccSections]
	WITH NOCHECK
	ADD CONSTRAINT [FKC3D9DCADEC995508]
	FOREIGN KEY ([sectionId]) REFERENCES [dbo].[tbl_TemplateSections] ([sectionId])
ALTER TABLE [dbo].[tbl_IttGccSections]
	CHECK CONSTRAINT [FKC3D9DCADEC995508]

GO
ALTER TABLE [dbo].[tbl_IttGccSections]
	WITH NOCHECK
	ADD CONSTRAINT [ittGccSectionId_FK2]
	FOREIGN KEY ([sectionId]) REFERENCES [dbo].[tbl_TemplateSections] ([sectionId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_IttGccSections]
	CHECK CONSTRAINT [ittGccSectionId_FK2]

GO
ALTER TABLE [dbo].[tbl_IttGccSections]
	WITH NOCHECK
	ADD CONSTRAINT [ittGccTemplateId_FK1]
	FOREIGN KEY ([templateId]) REFERENCES [dbo].[tbl_TemplateMaster] ([templateId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_IttGccSections]
	CHECK CONSTRAINT [ittGccTemplateId_FK1]

GO
ALTER TABLE [dbo].[tbl_IttGccSections] SET (LOCK_ESCALATION = TABLE)
GO
