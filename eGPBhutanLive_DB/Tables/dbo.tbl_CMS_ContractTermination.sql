SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_ContractTermination] (
		[contractTerminationId]     [int] IDENTITY(1, 1) NOT NULL,
		[dateOfTermination]         [datetime] NOT NULL,
		[reason]                    [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[reasonType]                [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[status]                    [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[workflowStatus]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[contractId]                [int] NOT NULL,
		[releasePG]                 [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]                 [int] NOT NULL,
		[userTypeId]                [int] NULL,
		[paySetAmt]                 [decimal](15, 3) NULL,
		[paySetAmtRemarks]          [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_CMS_ContractTermination]
		PRIMARY KEY
		CLUSTERED
		([contractTerminationId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_ContractTermination]
	WITH CHECK
	ADD CONSTRAINT [FK_ContractId]
	FOREIGN KEY ([contractId]) REFERENCES [dbo].[tbl_ContractSign] ([contractSignId])
ALTER TABLE [dbo].[tbl_CMS_ContractTermination]
	CHECK CONSTRAINT [FK_ContractId]

GO
ALTER TABLE [dbo].[tbl_CMS_ContractTermination] SET (LOCK_ESCALATION = TABLE)
GO
