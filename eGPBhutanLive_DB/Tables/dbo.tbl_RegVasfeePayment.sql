SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_RegVasfeePayment] (
		[paymnetId]        [int] IDENTITY(1, 1) NOT NULL,
		[paymentDesc]      [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[bankName]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[currancy]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Amount]           [decimal](15, 3) NOT NULL,
		[paymentType]      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cardNo]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[dtofPayment]      [datetime] NULL,
		[remarks]          [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[transId]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[payeeName]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[payeeAddress]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[emailId]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_RegVasfeePayment]
		PRIMARY KEY
		CLUSTERED
		([paymnetId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_RegVasfeePayment] SET (LOCK_ESCALATION = TABLE)
GO
