SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_WS_Master] (
		[wsId]               [int] IDENTITY(1, 1) NOT NULL,
		[wsName]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[wsDesc]             [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isAuthenticate]     [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]          [int] NOT NULL,
		[createdDate]        [datetime] NOT NULL,
		[isActive]           [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_WS_Master]
		PRIMARY KEY
		CLUSTERED
		([wsId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_WS_Master]
	ADD
	CONSTRAINT [DF_tbl_WS_Master_isActive]
	DEFAULT ('Y') FOR [isActive]
GO
ALTER TABLE [dbo].[tbl_WS_Master]
	ADD
	CONSTRAINT [DF_tbl_WS_Master_isAuthenticate]
	DEFAULT ('N') FOR [isAuthenticate]
GO
ALTER TABLE [dbo].[tbl_WS_Master] SET (LOCK_ESCALATION = TABLE)
GO
