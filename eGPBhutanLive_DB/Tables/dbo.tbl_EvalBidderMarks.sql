SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalBidderMarks] (
		[evalBidderId]     [int] IDENTITY(1, 1) NOT NULL,
		[userId]           [int] NOT NULL,
		[marks]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderId]         [int] NOT NULL,
		[createdBy]        [int] NOT NULL,
		[tenderFormId]     [int] NOT NULL,
		[subCrietId]       [int] NOT NULL,
		[evalCount]        [int] NOT NULL,
		CONSTRAINT [PK_tbl_EvalBidderMarks]
		PRIMARY KEY
		CLUSTERED
		([evalBidderId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalBidderMarks]
	ADD
	CONSTRAINT [DF__tbl_EvalB__evalC__155B1B70]
	DEFAULT ('0') FOR [evalCount]
GO
ALTER TABLE [dbo].[tbl_EvalBidderMarks] SET (LOCK_ESCALATION = TABLE)
GO
