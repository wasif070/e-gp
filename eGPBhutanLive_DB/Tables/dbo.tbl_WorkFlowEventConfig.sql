SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_WorkFlowEventConfig] (
		[wfEvtConfId]       [int] IDENTITY(1, 1) NOT NULL,
		[noOfReviewer]      [smallint] NOT NULL,
		[noOfDays]          [smallint] NOT NULL,
		[isDonorConReq]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[eventId]           [smallint] NOT NULL,
		[objectId]          [int] NOT NULL,
		CONSTRAINT [wfEvtConfId_PK]
		PRIMARY KEY
		CLUSTERED
		([wfEvtConfId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_WorkFlowEventConfig]
	WITH CHECK
	ADD CONSTRAINT [eventWorkflow_FK1]
	FOREIGN KEY ([eventId]) REFERENCES [dbo].[tbl_EventMaster] ([eventId])
ALTER TABLE [dbo].[tbl_WorkFlowEventConfig]
	CHECK CONSTRAINT [eventWorkflow_FK1]

GO
ALTER TABLE [dbo].[tbl_WorkFlowEventConfig]
	WITH CHECK
	ADD CONSTRAINT [FKDF305AFCF9418551]
	FOREIGN KEY ([eventId]) REFERENCES [dbo].[tbl_EventMaster] ([eventId])
ALTER TABLE [dbo].[tbl_WorkFlowEventConfig]
	CHECK CONSTRAINT [FKDF305AFCF9418551]

GO
ALTER TABLE [dbo].[tbl_WorkFlowEventConfig] SET (LOCK_ESCALATION = TABLE)
GO
