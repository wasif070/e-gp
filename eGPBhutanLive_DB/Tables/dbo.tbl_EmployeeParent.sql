SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_EmployeeParent] (
		[parentEmpId]      [int] IDENTITY(1, 1) NOT NULL,
		[userId]           [int] NOT NULL,
		[parentUserId]     [int] NOT NULL,
		CONSTRAINT [PK_tbl_EmployeeBoss]
		PRIMARY KEY
		CLUSTERED
		([parentEmpId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EmployeeParent] SET (LOCK_ESCALATION = TABLE)
GO
