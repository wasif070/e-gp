SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_DebarmentDetails] (
		[debarmentDetId]     [int] IDENTITY(1, 1) NOT NULL,
		[debarmentId]        [int] NOT NULL,
		[debarIds]           [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[debarTypeId]        [int] NOT NULL,
		[debarStatus]        [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[debarmentBy]        [int] NOT NULL,
		[debarmentDt]        [smalldatetime] NOT NULL,
		[debarStartDt]       [smalldatetime] NULL,
		[debarEdnDt]         [datetime] NULL,
		CONSTRAINT [PK_tbl_DebarmentDetails]
		PRIMARY KEY
		CLUSTERED
		([debarmentDetId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_DebarmentDetails]
	WITH CHECK
	ADD CONSTRAINT [debarmentDet_FK1]
	FOREIGN KEY ([debarmentId]) REFERENCES [dbo].[tbl_DebarmentReq] ([debarmentId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_DebarmentDetails]
	CHECK CONSTRAINT [debarmentDet_FK1]

GO
ALTER TABLE [dbo].[tbl_DebarmentDetails]
	WITH CHECK
	ADD CONSTRAINT [FK7CF6CA5399D31119]
	FOREIGN KEY ([debarmentId]) REFERENCES [dbo].[tbl_DebarmentReq] ([debarmentId])
ALTER TABLE [dbo].[tbl_DebarmentDetails]
	CHECK CONSTRAINT [FK7CF6CA5399D31119]

GO
ALTER TABLE [dbo].[tbl_DebarmentDetails] SET (LOCK_ESCALATION = TABLE)
GO
