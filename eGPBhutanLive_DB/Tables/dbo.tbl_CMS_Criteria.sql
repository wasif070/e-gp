SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_Criteria] (
		[cmsCriteriaId]       [int] IDENTITY(1, 1) NOT NULL,
		[cmsPaymentTerms]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderId]            [int] NOT NULL,
		[createdBy]           [int] NOT NULL,
		[createdDate]         [datetime] NOT NULL,
		[lotId]               [int] NOT NULL,
		[contractId]          [int] NULL,
		CONSTRAINT [cmsCriteriaId_PK]
		PRIMARY KEY
		CLUSTERED
		([cmsCriteriaId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_Criteria]
	WITH NOCHECK
	ADD CONSTRAINT [cmsCriteriaContractId_FK2]
	FOREIGN KEY ([contractId]) REFERENCES [dbo].[tbl_ContractSign] ([contractSignId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_Criteria]
	CHECK CONSTRAINT [cmsCriteriaContractId_FK2]

GO
ALTER TABLE [dbo].[tbl_CMS_Criteria]
	WITH NOCHECK
	ADD CONSTRAINT [cmsCriteriaTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_Criteria]
	CHECK CONSTRAINT [cmsCriteriaTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_CMS_Criteria] SET (LOCK_ESCALATION = TABLE)
GO
