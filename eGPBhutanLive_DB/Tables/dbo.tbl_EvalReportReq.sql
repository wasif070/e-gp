SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalReportReq] (
		[evalRptReqId]     [int] IDENTITY(1, 1) NOT NULL,
		[evalRptId]        [int] NOT NULL,
		[sentBy]           [int] NOT NULL,
		[sentTo]           [int] NOT NULL,
		[comments]         [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sendDt]           [datetime] NOT NULL,
		CONSTRAINT [PK_tbl_EvalReportReq]
		PRIMARY KEY
		CLUSTERED
		([evalRptReqId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalReportReq]
	ADD
	CONSTRAINT [DF_tbl_EvalReportReq_sendDt]
	DEFAULT (getdate()) FOR [sendDt]
GO
ALTER TABLE [dbo].[tbl_EvalReportReq]
	WITH CHECK
	ADD CONSTRAINT [reqEvalRptId_FK1]
	FOREIGN KEY ([evalRptId]) REFERENCES [dbo].[tbl_EvalReportMaster] ([evalRptId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EvalReportReq]
	CHECK CONSTRAINT [reqEvalRptId_FK1]

GO
ALTER TABLE [dbo].[tbl_EvalReportReq] SET (LOCK_ESCALATION = TABLE)
GO
