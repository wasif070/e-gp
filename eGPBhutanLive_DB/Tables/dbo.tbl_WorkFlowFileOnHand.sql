SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_WorkFlowFileOnHand] (
		[wflowFileOnHandId]     [int] IDENTITY(1, 1) NOT NULL,
		[eventId]               [smallint] NOT NULL,
		[activityId]            [smallint] NOT NULL,
		[moduleId]              [tinyint] NOT NULL,
		[objectId]              [int] NOT NULL,
		[childId]               [int] NOT NULL,
		[processDate]           [datetime] NOT NULL,
		[endDate]               [smalldatetime] NOT NULL,
		[fromUserId]            [int] NOT NULL,
		[toUserId]              [int] NOT NULL,
		[action]                [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[workflowDirection]     [varchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[wfRoleId]              [tinyint] NOT NULL,
		[remarks]               [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[wfTrigger]             [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[fileSentFrom]          [varchar](400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[fileSentTo]            [varchar](400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[documentId]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [wflowFileOnHandId_PK]
		PRIMARY KEY
		CLUSTERED
		([wflowFileOnHandId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_WorkFlowFileOnHand]
	WITH CHECK
	ADD CONSTRAINT [activityFileOnHand_FK2]
	FOREIGN KEY ([activityId]) REFERENCES [dbo].[tbl_ActivityMaster] ([activityId])
ALTER TABLE [dbo].[tbl_WorkFlowFileOnHand]
	CHECK CONSTRAINT [activityFileOnHand_FK2]

GO
ALTER TABLE [dbo].[tbl_WorkFlowFileOnHand]
	WITH CHECK
	ADD CONSTRAINT [eventFileOnHand_FK1]
	FOREIGN KEY ([eventId]) REFERENCES [dbo].[tbl_EventMaster] ([eventId])
ALTER TABLE [dbo].[tbl_WorkFlowFileOnHand]
	CHECK CONSTRAINT [eventFileOnHand_FK1]

GO
ALTER TABLE [dbo].[tbl_WorkFlowFileOnHand]
	WITH CHECK
	ADD CONSTRAINT [fileForwadedFrom_FK4]
	FOREIGN KEY ([fromUserId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_WorkFlowFileOnHand]
	CHECK CONSTRAINT [fileForwadedFrom_FK4]

GO
ALTER TABLE [dbo].[tbl_WorkFlowFileOnHand]
	WITH CHECK
	ADD CONSTRAINT [fileForwardedTo_FK3]
	FOREIGN KEY ([toUserId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_WorkFlowFileOnHand]
	CHECK CONSTRAINT [fileForwardedTo_FK3]

GO
ALTER TABLE [dbo].[tbl_WorkFlowFileOnHand]
	WITH CHECK
	ADD CONSTRAINT [FK63C82B0A4DDB0BFB]
	FOREIGN KEY ([fromUserId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_WorkFlowFileOnHand]
	CHECK CONSTRAINT [FK63C82B0A4DDB0BFB]

GO
ALTER TABLE [dbo].[tbl_WorkFlowFileOnHand]
	WITH CHECK
	ADD CONSTRAINT [FK63C82B0A6350E20C]
	FOREIGN KEY ([toUserId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_WorkFlowFileOnHand]
	CHECK CONSTRAINT [FK63C82B0A6350E20C]

GO
ALTER TABLE [dbo].[tbl_WorkFlowFileOnHand]
	WITH CHECK
	ADD CONSTRAINT [FK63C82B0A8ECC943B]
	FOREIGN KEY ([activityId]) REFERENCES [dbo].[tbl_ActivityMaster] ([activityId])
ALTER TABLE [dbo].[tbl_WorkFlowFileOnHand]
	CHECK CONSTRAINT [FK63C82B0A8ECC943B]

GO
ALTER TABLE [dbo].[tbl_WorkFlowFileOnHand]
	WITH CHECK
	ADD CONSTRAINT [FK63C82B0AF9418551]
	FOREIGN KEY ([eventId]) REFERENCES [dbo].[tbl_EventMaster] ([eventId])
ALTER TABLE [dbo].[tbl_WorkFlowFileOnHand]
	CHECK CONSTRAINT [FK63C82B0AF9418551]

GO
ALTER TABLE [dbo].[tbl_WorkFlowFileOnHand] SET (LOCK_ESCALATION = TABLE)
GO
