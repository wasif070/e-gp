SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_QuizQuestion] (
		[questionId]     [int] IDENTITY(1, 1) NOT NULL,
		[moduleId]       [int] NOT NULL,
		[question]       [varchar](512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_QuizQuestion]
		PRIMARY KEY
		CLUSTERED
		([questionId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_QuizQuestion]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_QuizQuestion_tbl_QuestionModule]
	FOREIGN KEY ([moduleId]) REFERENCES [dbo].[tbl_QuestionModule] ([moduleId])
ALTER TABLE [dbo].[tbl_QuizQuestion]
	CHECK CONSTRAINT [FK_tbl_QuizQuestion_tbl_QuestionModule]

GO
ALTER TABLE [dbo].[tbl_QuizQuestion] SET (LOCK_ESCALATION = TABLE)
GO
