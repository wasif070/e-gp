SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalReportClarification] (
		[evalRptClariId]             [int] IDENTITY(1, 1) NOT NULL,
		[evalRptToAAId]              [int] NOT NULL,
		[query]                      [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[queryDt]                    [datetime] NOT NULL,
		[answer]                     [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[answerDt]                   [datetime] NULL,
		[queryPostedByUserId]        [int] NOT NULL,
		[queryPostedByGovUserId]     [int] NOT NULL,
		[answeredByUserId]           [int] NOT NULL,
		[answeredByGovUserId]        [int] NOT NULL,
		[lastResponseDate]           [datetime] NULL,
		[roundId]                    [int] NOT NULL,
		CONSTRAINT [PK_tbl_EvalReportClarification]
		PRIMARY KEY
		CLUSTERED
		([evalRptClariId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalReportClarification]
	ADD
	CONSTRAINT [DF_tbl_EvalReportClarification_answeredByGovUserId]
	DEFAULT ((0)) FOR [answeredByGovUserId]
GO
ALTER TABLE [dbo].[tbl_EvalReportClarification]
	ADD
	CONSTRAINT [DF_tbl_EvalReportClarification_answeredByUserId]
	DEFAULT ((0)) FOR [answeredByUserId]
GO
ALTER TABLE [dbo].[tbl_EvalReportClarification]
	ADD
	CONSTRAINT [DF_tbl_EvalReportClarification_queryPostedByGovUserId]
	DEFAULT ((0)) FOR [queryPostedByGovUserId]
GO
ALTER TABLE [dbo].[tbl_EvalReportClarification]
	ADD
	CONSTRAINT [DF_tbl_EvalReportClarification_queryPostedByUserId]
	DEFAULT ((0)) FOR [queryPostedByUserId]
GO
ALTER TABLE [dbo].[tbl_EvalReportClarification]
	ADD
	CONSTRAINT [DF_tbl_EvalReportClarification_roundId]
	DEFAULT ((1)) FOR [roundId]
GO
ALTER TABLE [dbo].[tbl_EvalReportClarification]
	WITH CHECK
	ADD CONSTRAINT [evalReportClarificationId_FK1]
	FOREIGN KEY ([evalRptToAAId]) REFERENCES [dbo].[tbl_EvalRptSentToAA] ([evalRptToAAId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EvalReportClarification]
	CHECK CONSTRAINT [evalReportClarificationId_FK1]

GO
ALTER TABLE [dbo].[tbl_EvalReportClarification]
	WITH CHECK
	ADD CONSTRAINT [FKCC538493CAA50A9D]
	FOREIGN KEY ([evalRptToAAId]) REFERENCES [dbo].[tbl_EvalRptSentToAA] ([evalRptToAAId])
ALTER TABLE [dbo].[tbl_EvalReportClarification]
	CHECK CONSTRAINT [FKCC538493CAA50A9D]

GO
ALTER TABLE [dbo].[tbl_EvalReportClarification] SET (LOCK_ESCALATION = TABLE)
GO
