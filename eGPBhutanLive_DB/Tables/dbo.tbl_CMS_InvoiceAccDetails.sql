SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_InvoiceAccDetails] (
		[invoiceAccDtlId]      [int] IDENTITY(1, 1) NOT NULL,
		[wpId]                 [int] NOT NULL,
		[invoiceAmt]           [decimal](15, 3) NULL,
		[advAdjAmt]            [decimal](15, 3) NULL,
		[retentionMDeduct]     [decimal](15, 3) NULL,
		[retentionMAdd]        [decimal](15, 3) NULL,
		[pgDeduct]             [decimal](15, 3) NULL,
		[pgAdd]                [decimal](15, 3) NULL,
		[vatAmt]               [decimal](15, 3) NULL,
		[aitAmt]               [decimal](15, 3) NULL,
		[advVatAmt]            [decimal](15, 3) NULL,
		[advAitAmt]            [decimal](15, 3) NULL,
		[bonusAmt]             [decimal](15, 3) NULL,
		[penaltyAmt]           [decimal](15, 3) NULL,
		[ldAmt]                [decimal](15, 3) NULL,
		[grossAmt]             [decimal](15, 3) NOT NULL,
		[generatedDate]        [datetime] NOT NULL,
		[generatedBy]          [int] NOT NULL,
		[modeOfPayment]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[dateOfPayment]        [datetime] NOT NULL,
		[bankName]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[branchName]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[instrumentNo]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[invoiceId]            [int] NOT NULL,
		[advAmt]               [decimal](15, 3) NULL,
		[aitBankName]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[aitBranchName]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[aitInstrumentNo]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[aitDateOfPayment]     [datetime] NULL,
		[aitMOdeOfPayment]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[interestOnDP]         [decimal](15, 3) NULL,
		[vatBankName]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[vatBranchName]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[vatInstrumentNo]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[vatDateOfPayment]     [datetime] NULL,
		[vatModeOfPayment]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[salvageAdjAmt]        [decimal](15, 3) NULL,
		CONSTRAINT [invoiceId_PK]
		PRIMARY KEY
		CLUSTERED
		([invoiceAccDtlId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_InvoiceAccDetails]
	WITH CHECK
	ADD CONSTRAINT [cmsInvoiceWpId_FK1]
	FOREIGN KEY ([wpId]) REFERENCES [dbo].[tbl_CMS_WpMaster] ([wpId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_InvoiceAccDetails]
	CHECK CONSTRAINT [cmsInvoiceWpId_FK1]

GO
ALTER TABLE [dbo].[tbl_CMS_InvoiceAccDetails]
	WITH CHECK
	ADD CONSTRAINT [FK_InvocieId_PK_InvoiceMaster]
	FOREIGN KEY ([invoiceId]) REFERENCES [dbo].[tbl_CMS_InvoiceMaster] ([invoiceId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_InvoiceAccDetails]
	CHECK CONSTRAINT [FK_InvocieId_PK_InvoiceMaster]

GO
ALTER TABLE [dbo].[tbl_CMS_InvoiceAccDetails] SET (LOCK_ESCALATION = TABLE)
GO
