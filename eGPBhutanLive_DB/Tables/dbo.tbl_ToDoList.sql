SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ToDoList] (
		[taskId]          [int] IDENTITY(1, 1) NOT NULL,
		[userId]          [int] NOT NULL,
		[taskBrief]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[taskDetails]     [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[priority]        [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[startDate]       [datetime] NOT NULL,
		[endDate]         [datetime] NOT NULL,
		[isComplete]      [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [taskId_PK]
		PRIMARY KEY
		CLUSTERED
		([taskId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ToDoList]
	WITH CHECK
	ADD CONSTRAINT [FK106C91A56174DBD1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_ToDoList]
	CHECK CONSTRAINT [FK106C91A56174DBD1]

GO
ALTER TABLE [dbo].[tbl_ToDoList]
	WITH CHECK
	ADD CONSTRAINT [toDoListUser_FK1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ToDoList]
	CHECK CONSTRAINT [toDoListUser_FK1]

GO
ALTER TABLE [dbo].[tbl_ToDoList] SET (LOCK_ESCALATION = TABLE)
GO
