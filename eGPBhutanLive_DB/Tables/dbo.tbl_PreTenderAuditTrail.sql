SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_PreTenderAuditTrail] (
		[preTenderAudIt]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]           [int] NOT NULL,
		[actionDate]         [smalldatetime] NOT NULL,
		[actionBy]           [int] NOT NULL,
		[remarks]            [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_PreTenderAuditTrail]
		PRIMARY KEY
		CLUSTERED
		([preTenderAudIt])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_PreTenderAuditTrail]
	WITH NOCHECK
	ADD CONSTRAINT [auditTrialTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_PreTenderAuditTrail]
	CHECK CONSTRAINT [auditTrialTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_PreTenderAuditTrail] SET (LOCK_ESCALATION = TABLE)
GO
