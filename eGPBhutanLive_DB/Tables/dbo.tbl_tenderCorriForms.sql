SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_tenderCorriForms] (
		[tenderCorriFormId]     [int] IDENTITY(1, 1) NOT NULL,
		[corrigendumId]         [int] NOT NULL,
		[formId]                [int] NOT NULL,
		[formStatus]            [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_tenderCorriForms]
		PRIMARY KEY
		CLUSTERED
		([tenderCorriFormId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_tenderCorriForms] SET (LOCK_ESCALATION = TABLE)
GO
