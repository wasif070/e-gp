SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_NegotiationDocs] (
		[negDocId]           [int] IDENTITY(1, 1) NOT NULL,
		[negId]              [int] NOT NULL,
		[documentName]       [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]       [datetime] NOT NULL,
		[uploadedBy]         [int] NOT NULL,
		[userId]             [int] NULL,
		[uploadedWhen]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [negDocId_PK]
		PRIMARY KEY
		CLUSTERED
		([negDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_NegotiationDocs]
	ADD
	CONSTRAINT [DF_tbl_NegotiationDocs_uploadedWhen]
	DEFAULT ('negotiation') FOR [uploadedWhen]
GO
ALTER TABLE [dbo].[tbl_NegotiationDocs]
	WITH CHECK
	ADD CONSTRAINT [docNegId_FK1]
	FOREIGN KEY ([negId]) REFERENCES [dbo].[tbl_Negotiation] ([negId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_NegotiationDocs]
	CHECK CONSTRAINT [docNegId_FK1]

GO
ALTER TABLE [dbo].[tbl_NegotiationDocs]
	WITH CHECK
	ADD CONSTRAINT [FK57AECB1B14E6A50C]
	FOREIGN KEY ([negId]) REFERENCES [dbo].[tbl_Negotiation] ([negId])
ALTER TABLE [dbo].[tbl_NegotiationDocs]
	CHECK CONSTRAINT [FK57AECB1B14E6A50C]

GO
ALTER TABLE [dbo].[tbl_NegotiationDocs] SET (LOCK_ESCALATION = TABLE)
GO
