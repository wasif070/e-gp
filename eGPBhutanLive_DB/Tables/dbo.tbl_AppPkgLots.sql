SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_AppPkgLots] (
		[appPkgLotId]     [int] IDENTITY(1, 1) NOT NULL,
		[packageId]       [int] NOT NULL,
		[appId]           [int] NOT NULL,
		[lotNo]           [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[lotDesc]         [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[quantity]        [numeric](18, 2) NULL,
		[unit]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[lotEstCost]      [money] NOT NULL,
		CONSTRAINT [PK_tbl_AppPkgLots]
		PRIMARY KEY
		CLUSTERED
		([appPkgLotId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_AppPkgLots]
	WITH NOCHECK
	ADD CONSTRAINT [FKC1EA9D0E14F1608F]
	FOREIGN KEY ([packageId]) REFERENCES [dbo].[tbl_AppPackages] ([packageId])
ALTER TABLE [dbo].[tbl_AppPkgLots]
	CHECK CONSTRAINT [FKC1EA9D0E14F1608F]

GO
ALTER TABLE [dbo].[tbl_AppPkgLots]
	WITH NOCHECK
	ADD CONSTRAINT [FKC1EA9D0E448F6D5F]
	FOREIGN KEY ([appId]) REFERENCES [dbo].[tbl_AppMaster] ([appId])
ALTER TABLE [dbo].[tbl_AppPkgLots]
	CHECK CONSTRAINT [FKC1EA9D0E448F6D5F]

GO
ALTER TABLE [dbo].[tbl_AppPkgLots]
	WITH NOCHECK
	ADD CONSTRAINT [lotAppId_Fk1]
	FOREIGN KEY ([appId]) REFERENCES [dbo].[tbl_AppMaster] ([appId])
	ON DELETE CASCADE
ALTER TABLE [dbo].[tbl_AppPkgLots]
	CHECK CONSTRAINT [lotAppId_Fk1]

GO
ALTER TABLE [dbo].[tbl_AppPkgLots]
	WITH NOCHECK
	ADD CONSTRAINT [lotPackageId_Fk2]
	FOREIGN KEY ([packageId]) REFERENCES [dbo].[tbl_AppPackages] ([packageId])
ALTER TABLE [dbo].[tbl_AppPkgLots]
	CHECK CONSTRAINT [lotPackageId_Fk2]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Refer from tbl_appMaster, to identify the appid
', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPkgLots', 'COLUMN', N'appId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'System generated Id information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPkgLots', 'COLUMN', N'appPkgLotId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Lot description information stores here 
', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPkgLots', 'COLUMN', N'lotDesc'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Lot number information stores here 
', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPkgLots', 'COLUMN', N'lotNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Reference from tbl_AppPackages
', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPkgLots', 'COLUMN', N'packageId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Lot quantity information  stores here 
', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPkgLots', 'COLUMN', N'quantity'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Unit information stores here 
', 'SCHEMA', N'dbo', 'TABLE', N'tbl_AppPkgLots', 'COLUMN', N'unit'
GO
ALTER TABLE [dbo].[tbl_AppPkgLots] SET (LOCK_ESCALATION = TABLE)
GO
