SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_RegistrationValidity] (
		[validityId]          [int] IDENTITY(1, 1) NOT NULL,
		[validityYear]        [int] NULL,
		[validityYrWords]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[validityMonths]      [int] NOT NULL,
		[displayText]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[amtBDT]              [money] NOT NULL,
		[amtUSD]              [money] NOT NULL,
		[renewAmtDBT]         [money] NOT NULL,
		[renewAmtUSD]         [money] NOT NULL,
		[renewLateAmtBDT]     [money] NOT NULL,
		[renewLateAmtUSD]     [money] NOT NULL,
		[status]              [bit] NOT NULL,
		[transdate]           [datetime] NOT NULL,
		CONSTRAINT [PK_tbl_RegistrationValidityMaster]
		PRIMARY KEY
		CLUSTERED
		([validityId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_RegistrationValidity]
	ADD
	CONSTRAINT [DF_tbl_RegistrationValidityMaster_status]
	DEFAULT ((1)) FOR [status]
GO
ALTER TABLE [dbo].[tbl_RegistrationValidity]
	ADD
	CONSTRAINT [DF_tbl_RegistrationValidityMaster_transdate]
	DEFAULT (getdate()) FOR [transdate]
GO
ALTER TABLE [dbo].[tbl_RegistrationValidity] SET (LOCK_ESCALATION = TABLE)
GO
