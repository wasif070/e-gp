SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_DebarmentReq] (
		[debarmentId]            [int] IDENTITY(1, 1) NOT NULL,
		[userId]                 [int] NOT NULL,
		[clarification]          [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[lastResponseDt]         [smalldatetime] NOT NULL,
		[debarmentBy]            [int] NOT NULL,
		[clarificationReqDt]     [smalldatetime] NOT NULL,
		[debarmentStatus]        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[debarmentType]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[comments]               [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[hopeId]                 [int] NOT NULL,
		[debarTypeId]            [int] NOT NULL,
		[companyId]              [int] NULL,
		CONSTRAINT [PK_tbl_DebarmentReq]
		PRIMARY KEY
		CLUSTERED
		([debarmentId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_DebarmentReq]
	ADD
	CONSTRAINT [DF_tbl_DebarmentReq_debarmentStatus]
	DEFAULT ('Pending') FOR [debarmentStatus]
GO
ALTER TABLE [dbo].[tbl_DebarmentReq]
	WITH CHECK
	ADD CONSTRAINT [debarTypeReq_FK1]
	FOREIGN KEY ([debarTypeId]) REFERENCES [dbo].[tbl_DebarmentTypes] ([debarTypeId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_DebarmentReq]
	CHECK CONSTRAINT [debarTypeReq_FK1]

GO
ALTER TABLE [dbo].[tbl_DebarmentReq] SET (LOCK_ESCALATION = TABLE)
GO
