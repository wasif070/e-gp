SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_WorkflowRuleEngine] (
		[wfInitRuleId]          [int] IDENTITY(1, 1) NOT NULL,
		[eventId]               [smallint] NOT NULL,
		[procurementRoleId]     [tinyint] NOT NULL,
		[wfRoleId]              [tinyint] NOT NULL,
		[isPubDateReq]          [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [wfInitRuleId_PK]
		PRIMARY KEY
		CLUSTERED
		([wfInitRuleId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_WorkflowRuleEngine]
	WITH CHECK
	ADD CONSTRAINT [FKF5259FBE8809DCD7]
	FOREIGN KEY ([wfRoleId]) REFERENCES [dbo].[tbl_WorkFlowRoleMaster] ([wfRoleId])
ALTER TABLE [dbo].[tbl_WorkflowRuleEngine]
	CHECK CONSTRAINT [FKF5259FBE8809DCD7]

GO
ALTER TABLE [dbo].[tbl_WorkflowRuleEngine]
	WITH CHECK
	ADD CONSTRAINT [FKF5259FBEC90CE487]
	FOREIGN KEY ([procurementRoleId]) REFERENCES [dbo].[tbl_ProcurementRole] ([procurementRoleId])
ALTER TABLE [dbo].[tbl_WorkflowRuleEngine]
	CHECK CONSTRAINT [FKF5259FBEC90CE487]

GO
ALTER TABLE [dbo].[tbl_WorkflowRuleEngine]
	WITH CHECK
	ADD CONSTRAINT [workflowProcurementRole_FK1]
	FOREIGN KEY ([procurementRoleId]) REFERENCES [dbo].[tbl_ProcurementRole] ([procurementRoleId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_WorkflowRuleEngine]
	CHECK CONSTRAINT [workflowProcurementRole_FK1]

GO
ALTER TABLE [dbo].[tbl_WorkflowRuleEngine]
	WITH CHECK
	ADD CONSTRAINT [workflowRole_FK2]
	FOREIGN KEY ([wfRoleId]) REFERENCES [dbo].[tbl_WorkFlowRoleMaster] ([wfRoleId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_WorkflowRuleEngine]
	CHECK CONSTRAINT [workflowRole_FK2]

GO
ALTER TABLE [dbo].[tbl_WorkflowRuleEngine] SET (LOCK_ESCALATION = TABLE)
GO
