SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_AppFrameworkOffice] (
		[FrameWorkOfficeId]     [bigint] IDENTITY(1, 1) NOT NULL,
		[APPId]                 [int] NOT NULL,
		[PackageID]             [int] NOT NULL,
		[PEOfficeID]            [int] NOT NULL,
		[OfficeName]            [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[State]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Address]               [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_AppFrameworkOffice]
		PRIMARY KEY
		CLUSTERED
		([FrameWorkOfficeId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_AppFrameworkOffice]
	WITH CHECK
	ADD CONSTRAINT [FK_Pkg_Framework]
	FOREIGN KEY ([PackageID]) REFERENCES [dbo].[tbl_AppPackages] ([packageId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_AppFrameworkOffice]
	CHECK CONSTRAINT [FK_Pkg_Framework]

GO
ALTER TABLE [dbo].[tbl_AppFrameworkOffice] SET (LOCK_ESCALATION = TABLE)
GO
