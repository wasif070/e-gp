SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalTscDocs] (
		[tscDocId]         [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]         [int] NOT NULL,
		[pkgLotId]         [int] NOT NULL,
		[documentName]     [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[documentDesc]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDt]       [datetime] NOT NULL,
		[uploadedBy]       [int] NOT NULL,
		CONSTRAINT [PK_tbl_EvalTscDocs]
		PRIMARY KEY
		CLUSTERED
		([tscDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalTscDocs]
	WITH NOCHECK
	ADD CONSTRAINT [evalTSCTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EvalTscDocs]
	CHECK CONSTRAINT [evalTSCTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_EvalTscDocs] SET (LOCK_ESCALATION = TABLE)
GO
