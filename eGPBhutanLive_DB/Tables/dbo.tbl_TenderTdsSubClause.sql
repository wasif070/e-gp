SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderTdsSubClause] (
		[tenderTdsSubClauseId]       [int] IDENTITY(1, 1) NOT NULL,
		[tenderIttRefrence]          [int] NOT NULL,
		[orderNumber]                [smallint] NOT NULL,
		[tenderIttHeaderId]          [int] NOT NULL,
		[templateTdsSubClauseId]     [int] NOT NULL,
		[tenderTdsClauseName]        [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_TenderTdsSubClause]
		PRIMARY KEY
		CLUSTERED
		([tenderTdsSubClauseId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderTdsSubClause]
	WITH CHECK
	ADD CONSTRAINT [FK2F14E24173E3175F]
	FOREIGN KEY ([tenderIttHeaderId]) REFERENCES [dbo].[tbl_TenderIttHeader] ([tenderIttHeaderId])
ALTER TABLE [dbo].[tbl_TenderTdsSubClause]
	CHECK CONSTRAINT [FK2F14E24173E3175F]

GO
ALTER TABLE [dbo].[tbl_TenderTdsSubClause]
	WITH CHECK
	ADD CONSTRAINT [tenderIttSubClauseHeaderId_FK1]
	FOREIGN KEY ([tenderIttHeaderId]) REFERENCES [dbo].[tbl_TenderIttHeader] ([tenderIttHeaderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderTdsSubClause]
	CHECK CONSTRAINT [tenderIttSubClauseHeaderId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderTdsSubClause] SET (LOCK_ESCALATION = TABLE)
GO
