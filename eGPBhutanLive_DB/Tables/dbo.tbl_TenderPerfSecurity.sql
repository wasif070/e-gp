SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_TenderPerfSecurity] (
		[perCostLotId]       [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]           [int] NOT NULL,
		[pkgLotId]           [int] NOT NULL,
		[perSecurityAmt]     [money] NOT NULL,
		[createdBy]          [int] NOT NULL,
		[createdDt]          [datetime] NOT NULL,
		[roundId]            [int] NOT NULL,
		[percentage]         [money] NOT NULL,
		[lowestAmt]          [money] NOT NULL,
		[currencyID]         [int] NULL,
		CONSTRAINT [PK_tbl_TenderPerfSecurity]
		PRIMARY KEY
		CLUSTERED
		([perCostLotId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderPerfSecurity]
	ADD
	CONSTRAINT [DF_tbl_TenderPerfSecurity_lowestAmt]
	DEFAULT ((0.0)) FOR [lowestAmt]
GO
ALTER TABLE [dbo].[tbl_TenderPerfSecurity]
	ADD
	CONSTRAINT [DF_tbl_TenderPerfSecurity_percentage]
	DEFAULT ((0.0)) FOR [percentage]
GO
ALTER TABLE [dbo].[tbl_TenderPerfSecurity]
	ADD
	CONSTRAINT [DF_tbl_TenderPerfSecurity_pkgLotId]
	DEFAULT ((0)) FOR [pkgLotId]
GO
ALTER TABLE [dbo].[tbl_TenderPerfSecurity]
	ADD
	CONSTRAINT [DF_tbl_TenderPerfSecurity_roundId]
	DEFAULT ((1)) FOR [roundId]
GO
ALTER TABLE [dbo].[tbl_TenderPerfSecurity]
	WITH NOCHECK
	ADD CONSTRAINT [TenderPerfSecurity_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderPerfSecurity]
	CHECK CONSTRAINT [TenderPerfSecurity_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderPerfSecurity] SET (LOCK_ESCALATION = TABLE)
GO
