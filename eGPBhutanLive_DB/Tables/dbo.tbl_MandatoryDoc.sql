SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_MandatoryDoc] (
		[mandatoryDocId]        [int] IDENTITY(1, 1) NOT NULL,
		[regType]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isBangladesh]          [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docType]               [varchar](400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docTypeValue]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isMandatory]           [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[procurementNature]     [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[userType]              [int] NULL,
		CONSTRAINT [IX_tbl_MandatoryDoc]
		UNIQUE
		NONCLUSTERED
		([docType], [isBangladesh], [docTypeValue], [procurementNature], [regType])
		ON [PRIMARY],
		CONSTRAINT [PK_tbl_MandatoryDoc]
		PRIMARY KEY
		CLUSTERED
		([mandatoryDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_MandatoryDoc]
	ADD
	CONSTRAINT [DF_tbl_MandatoryDoc_isMandatory]
	DEFAULT (N'yes') FOR [isMandatory]
GO
ALTER TABLE [dbo].[tbl_MandatoryDoc] SET (LOCK_ESCALATION = TABLE)
GO
