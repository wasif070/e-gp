SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_FinalSubmission] (
		[finalSubmissionId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]              [int] NOT NULL,
		[finalSubmissionDt]     [smalldatetime] NULL,
		[userId]                [int] NOT NULL,
		[ipAddress]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[bidSubStatus]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderHash]            [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[receiptNo]             [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[otp]                   [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[otpDateTime]           [smalldatetime] NULL,
		CONSTRAINT [finalSubmissionId_PK]
		PRIMARY KEY
		CLUSTERED
		([finalSubmissionId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_FinalSubmission]
	WITH NOCHECK
	ADD CONSTRAINT [FK95EE8EC198B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_FinalSubmission]
	CHECK CONSTRAINT [FK95EE8EC198B66EC5]

GO
ALTER TABLE [dbo].[tbl_FinalSubmission] SET (LOCK_ESCALATION = TABLE)
GO
