SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_ContractAwardedOffline] (
		[conAwardOfflineId]         [int] IDENTITY(1, 1) NOT NULL,
		[ministry]                  [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[division]                  [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[agency]                    [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[peOfficeName]              [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[peCode]                    [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[peDistrict]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AwardForPNature]           [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[RefNo]                     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[procurementMethod]         [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[budgetType]                [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sourceOfFund]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[devPartners]               [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[projectCode]               [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[projectName]               [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[packageNo]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[packageName]               [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DateofAdvertisement]       [smalldatetime] NULL,
		[DateofNOA]                 [smalldatetime] NULL,
		[DateofContractSign]        [smalldatetime] NULL,
		[DateofPCCompletion]        [smalldatetime] NULL,
		[Sold]                      [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Received]                  [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Response]                  [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DescriptionofContract]     [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ContractValue]             [money] NOT NULL,
		[NameofTenderer]            [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AddressofTenderer]         [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DeliveryPlace]             [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[IsSamePersonNOA]           [smallint] NULL,
		[NOAReason]                 [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsPSecurityDueTime]        [smallint] NULL,
		[PSecurityReason]           [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsSignedDueTime]           [smallint] NULL,
		[SignedReason]              [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OfficerName]               [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[OfficerDesignation]        [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UserID]                    [int] NULL,
		[Date]                      [smalldatetime] NULL,
		[Status]                    [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Comment]                   [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [conAwardOfflineId_PK]
		PRIMARY KEY
		CLUSTERED
		([conAwardOfflineId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ContractAwardedOffline] SET (LOCK_ESCALATION = TABLE)
GO
