SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderGrandSum] (
		[tenderSumId]      [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]         [int] NOT NULL,
		[summaryName]      [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[creationDate]     [smalldatetime] NOT NULL,
		[createdBy]        [int] NOT NULL,
		[pkgLotId]         [int] NOT NULL,
		CONSTRAINT [PK_tbl_TenderGrandSum]
		PRIMARY KEY
		CLUSTERED
		([tenderSumId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderGrandSum]
	ADD
	CONSTRAINT [DF_tbl_TenderGrandSum_pkgLotId]
	DEFAULT ((0)) FOR [pkgLotId]
GO
ALTER TABLE [dbo].[tbl_TenderGrandSum]
	WITH NOCHECK
	ADD CONSTRAINT [FK9469E35498B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_TenderGrandSum]
	CHECK CONSTRAINT [FK9469E35498B66EC5]

GO
ALTER TABLE [dbo].[tbl_TenderGrandSum]
	WITH NOCHECK
	ADD CONSTRAINT [tenderGrandSumTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderGrandSum]
	CHECK CONSTRAINT [tenderGrandSumTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderGrandSum] SET (LOCK_ESCALATION = TABLE)
GO
