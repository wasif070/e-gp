SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_GccSubClause] (
		[gccSubClauseId]       [int] IDENTITY(1, 1) NOT NULL,
		[gccSubClauseName]     [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isTdsApplicable]      [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[gccClauseId]          [int] NOT NULL,
		CONSTRAINT [UQ__tbl_GccS__F53C08DDFF598E40]
		UNIQUE
		NONCLUSTERED
		([gccSubClauseId])
		ON [PRIMARY],
		CONSTRAINT [PK__tbl_GccS__F53C08DCB958F946]
		PRIMARY KEY
		CLUSTERED
		([gccSubClauseId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_GccSubClause]
	WITH CHECK
	ADD CONSTRAINT [FKDC151389924430A7]
	FOREIGN KEY ([gccClauseId]) REFERENCES [dbo].[tbl_GccClause] ([gccClauseId])
ALTER TABLE [dbo].[tbl_GccSubClause]
	CHECK CONSTRAINT [FKDC151389924430A7]

GO
ALTER TABLE [dbo].[tbl_GccSubClause] SET (LOCK_ESCALATION = TABLE)
GO
