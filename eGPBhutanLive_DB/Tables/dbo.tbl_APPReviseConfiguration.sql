SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_APPReviseConfiguration] (
		[appConfgId]          [int] IDENTITY(1, 1) NOT NULL,
		[financialYear]       [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[startDate]           [smalldatetime] NOT NULL,
		[endDate]             [smalldatetime] NOT NULL,
		[maxReviseCount]      [int] NOT NULL,
		[userId]              [int] NOT NULL,
		[lastUpdatedDate]     [smalldatetime] NOT NULL,
		CONSTRAINT [PK_tbl_APPReviseConfiguration]
		PRIMARY KEY
		CLUSTERED
		([appConfgId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_APPReviseConfiguration] SET (LOCK_ESCALATION = TABLE)
GO
