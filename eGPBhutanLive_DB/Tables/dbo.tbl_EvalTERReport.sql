SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalTERReport] (
		[terRptId]       [int] IDENTITY(1, 1) NOT NULL,
		[criteriaId]     [int] NOT NULL,
		[tenderId]       [int] NOT NULL,
		[userId]         [int] NOT NULL,
		[value]          [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[pkgLotId]       [int] NOT NULL,
		[evalType]       [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[reportType]     [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]      [int] NOT NULL,
		[createdDt]      [datetime] NOT NULL,
		[tecRole]        [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[roundId]        [int] NOT NULL,
		[evalCount]      [int] NOT NULL,
		CONSTRAINT [PK_tbl_EvalTERReport]
		PRIMARY KEY
		CLUSTERED
		([terRptId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalTERReport]
	ADD
	CONSTRAINT [DF__tbl_EvalT__evalC__37B03374]
	DEFAULT ('0') FOR [evalCount]
GO
ALTER TABLE [dbo].[tbl_EvalTERReport]
	ADD
	CONSTRAINT [DF_tbl_EvalTERReport_createdBy]
	DEFAULT ((0)) FOR [createdBy]
GO
ALTER TABLE [dbo].[tbl_EvalTERReport]
	ADD
	CONSTRAINT [DF_tbl_EvalTERReport_createdDt]
	DEFAULT (getdate()) FOR [createdDt]
GO
ALTER TABLE [dbo].[tbl_EvalTERReport]
	ADD
	CONSTRAINT [DF_tbl_EvalTERReport_roundId]
	DEFAULT ((0)) FOR [roundId]
GO
ALTER TABLE [dbo].[tbl_EvalTERReport]
	ADD
	CONSTRAINT [DF_tbl_EvalTERReport_tecRole]
	DEFAULT ('cp') FOR [tecRole]
GO
ALTER TABLE [dbo].[tbl_EvalTERReport]
	WITH CHECK
	ADD CONSTRAINT [evalRptCriteriaId_FK1]
	FOREIGN KEY ([criteriaId]) REFERENCES [dbo].[tbl_EvalTERReportCriteria] ([criteriaId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EvalTERReport]
	CHECK CONSTRAINT [evalRptCriteriaId_FK1]

GO
ALTER TABLE [dbo].[tbl_EvalTERReport]
	WITH NOCHECK
	ADD CONSTRAINT [evalTERTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EvalTERReport]
	CHECK CONSTRAINT [evalTERTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_EvalTERReport]
	WITH CHECK
	ADD CONSTRAINT [FK191A5C988F74F912]
	FOREIGN KEY ([criteriaId]) REFERENCES [dbo].[tbl_EvalTERReportCriteria] ([criteriaId])
ALTER TABLE [dbo].[tbl_EvalTERReport]
	CHECK CONSTRAINT [FK191A5C988F74F912]

GO
ALTER TABLE [dbo].[tbl_EvalTERReport] SET (LOCK_ESCALATION = TABLE)
GO
