SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EmailLog] (
		[emailLogId]       [int] IDENTITY(1, 1) NOT NULL,
		[emailId]          [varchar](5000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[emailSubject]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[emailSentDt]      [datetime] NOT NULL,
		[exceptionLog]     [varchar](5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_EmailLog]
		PRIMARY KEY
		CLUSTERED
		([emailLogId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EmailLog] SET (LOCK_ESCALATION = TABLE)
GO
