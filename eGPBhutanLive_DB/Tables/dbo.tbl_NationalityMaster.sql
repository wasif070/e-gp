SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_NationalityMaster] (
		[nationalityId]       [smallint] IDENTITY(1, 1) NOT NULL,
		[nationalityName]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [nationalityId_PK]
		PRIMARY KEY
		CLUSTERED
		([nationalityId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Unique id of nationality stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_NationalityMaster', 'COLUMN', N'nationalityId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name of nationality name stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_NationalityMaster', 'COLUMN', N'nationalityName'
GO
ALTER TABLE [dbo].[tbl_NationalityMaster] SET (LOCK_ESCALATION = TABLE)
GO
