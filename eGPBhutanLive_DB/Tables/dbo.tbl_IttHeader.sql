SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_IttHeader] (
		[ittHeaderId]       [int] IDENTITY(1, 1) NOT NULL,
		[ittHeaderName]     [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sectionId]         [int] NOT NULL,
		CONSTRAINT [ittHeaderId_PK]
		PRIMARY KEY
		CLUSTERED
		([ittHeaderId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_IttHeader]
	WITH CHECK
	ADD CONSTRAINT [FK4F954935EC995508]
	FOREIGN KEY ([sectionId]) REFERENCES [dbo].[tbl_TemplateSections] ([sectionId])
ALTER TABLE [dbo].[tbl_IttHeader]
	CHECK CONSTRAINT [FK4F954935EC995508]

GO
ALTER TABLE [dbo].[tbl_IttHeader]
	WITH CHECK
	ADD CONSTRAINT [headerSectionId_FK2]
	FOREIGN KEY ([sectionId]) REFERENCES [dbo].[tbl_TemplateSections] ([sectionId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_IttHeader]
	CHECK CONSTRAINT [headerSectionId_FK2]

GO
ALTER TABLE [dbo].[tbl_IttHeader] SET (LOCK_ESCALATION = TABLE)
GO
