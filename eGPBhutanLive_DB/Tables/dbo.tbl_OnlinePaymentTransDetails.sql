SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_OnlinePaymentTransDetails] (
		[onlinePaymentID]       [int] IDENTITY(1, 1) NOT NULL,
		[userId]                [int] NOT NULL,
		[emailId]               [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[TransId]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Amount]                [money] NOT NULL,
		[serviceChargeAmt]      [money] NOT NULL,
		[TotalAmount]           [money] NOT NULL,
		[Description]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[TransStDateTime]       [datetime] NOT NULL,
		[TransEnDateTime]       [datetime] NULL,
		[TransactionStatus]     [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[PaymentFor]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[TenderId]              [int] NULL,
		[ResponseMsg]           [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[PaymentGateway]        [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_OnlinePaymentTransDetails]
		PRIMARY KEY
		CLUSTERED
		([onlinePaymentID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_OnlinePaymentTransDetails] SET (LOCK_ESCALATION = TABLE)
GO
