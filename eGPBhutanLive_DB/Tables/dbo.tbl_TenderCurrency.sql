SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_TenderCurrency] (
		[tenderCurId]      [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]         [int] NOT NULL,
		[currencyId]       [int] NOT NULL,
		[exchangeRate]     [money] NOT NULL,
		[createdBy]        [int] NOT NULL,
		[createdDate]      [datetime] NOT NULL,
		CONSTRAINT [PK_tbl_TenderCurrency]
		PRIMARY KEY
		CLUSTERED
		([tenderCurId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderCurrency] SET (LOCK_ESCALATION = TABLE)
GO
