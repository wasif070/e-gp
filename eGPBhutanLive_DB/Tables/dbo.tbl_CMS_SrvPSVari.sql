SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_SrvPSVari] (
		[srvPSVariId]         [int] IDENTITY(1, 1) NOT NULL,
		[srvFormMapId]        [int] NOT NULL,
		[srNo]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[milestone]           [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[description]         [varchar](5000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[percentOfCtrVal]     [decimal](6, 3) NULL,
		[peenddate]           [datetime] NOT NULL,
		[endDate]             [datetime] NOT NULL,
		[variOrdId]           [int] NOT NULL,
		CONSTRAINT [PK_tbl_CMS_SrvPSVari]
		PRIMARY KEY
		CLUSTERED
		([srvPSVariId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvPSVari] SET (LOCK_ESCALATION = TABLE)
GO
