SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_ComplaintMaster] (
		[complaintId]                 [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]                    [int] NOT NULL,
		[tendererId]                  [int] NOT NULL,
		[complaintSubject]            [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[complaintDetails]            [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tendererComments]            [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[govUserId]                   [int] NOT NULL,
		[officerComments]             [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[panelId]                     [int] NOT NULL,
		[complaintLevelId]            [tinyint] NOT NULL,
		[complaintStatus]             [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[complaintCreationDt]         [datetime] NOT NULL,
		[officerProcessDate]          [datetime] NOT NULL,
		[notificationRemarksToPe]     [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[notificationDtToPe]          [datetime] NOT NULL,
		[lastModificationDt]          [datetime] NOT NULL,
		[assignedToPanelDt]           [datetime] NOT NULL,
		[complaintTypeId]             [tinyint] NOT NULL,
		[paymentStatus]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderRefNo]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[peResolved]                  [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[hopeResolved]                [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[secResolved]                 [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[rpResolved]                  [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_CMS_ComplaintMaster]
		PRIMARY KEY
		CLUSTERED
		([complaintId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintMaster]
	ADD
	CONSTRAINT [DF_tbl_CMS_ComplaintMaster_complaintStatus]
	DEFAULT ('pending') FOR [complaintStatus]
GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintMaster]
	WITH CHECK
	ADD CONSTRAINT [complaintMasterComplaintLevelId_FK4]
	FOREIGN KEY ([complaintLevelId]) REFERENCES [dbo].[tbl_CMS_ComplaintLevels] ([complaintLevelId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_ComplaintMaster]
	CHECK CONSTRAINT [complaintMasterComplaintLevelId_FK4]

GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintMaster]
	WITH CHECK
	ADD CONSTRAINT [complaintMasterComplaintTypeId_Fk5]
	FOREIGN KEY ([complaintTypeId]) REFERENCES [dbo].[tbl_CMS_ComplaintTypes] ([complaintTypeId])
ALTER TABLE [dbo].[tbl_CMS_ComplaintMaster]
	CHECK CONSTRAINT [complaintMasterComplaintTypeId_Fk5]

GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintMaster]
	WITH CHECK
	ADD CONSTRAINT [PK_EmployeeTransfer_FK_ComplaintMater]
	FOREIGN KEY ([govUserId]) REFERENCES [dbo].[tbl_EmployeeTrasfer] ([govUserId])
ALTER TABLE [dbo].[tbl_CMS_ComplaintMaster]
	CHECK CONSTRAINT [PK_EmployeeTransfer_FK_ComplaintMater]

GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintMaster]
	WITH CHECK
	ADD CONSTRAINT [PK_TendererMaster_FK_ComplaintMaster]
	FOREIGN KEY ([tendererId]) REFERENCES [dbo].[tbl_TendererMaster] ([tendererId])
ALTER TABLE [dbo].[tbl_CMS_ComplaintMaster]
	CHECK CONSTRAINT [PK_TendererMaster_FK_ComplaintMaster]

GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintMaster]
	WITH NOCHECK
	ADD CONSTRAINT [PK_TenderMaster_FK_ComplaintMaster]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_CMS_ComplaintMaster]
	CHECK CONSTRAINT [PK_TenderMaster_FK_ComplaintMaster]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Complain assign to panel that info stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintMaster', 'COLUMN', N'assignedToPanelDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Complaint Generation Date Info stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintMaster', 'COLUMN', N'complaintCreationDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Complaint Details Info stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintMaster', 'COLUMN', N'complaintDetails'
GO
EXEC sp_addextendedproperty N'MS_Description', N'System Generated Id', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintMaster', 'COLUMN', N'complaintId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK tbl_ComplaintLevel ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintMaster', 'COLUMN', N'complaintLevelId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Complaint Status Info stores here (pending  / accept / reject / clarification)', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintMaster', 'COLUMN', N'complaintStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Subject Information stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintMaster', 'COLUMN', N'complaintSubject'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK tbl_ComplaintTypes ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintMaster', 'COLUMN', N'complaintTypeId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK tbl_EmployeeTransfer', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintMaster', 'COLUMN', N'govUserId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Last Modification Date & Time by tenderer info stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintMaster', 'COLUMN', N'lastModificationDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PE Notification Date & time info stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintMaster', 'COLUMN', N'notificationDtToPe'
GO
EXEC sp_addextendedproperty N'MS_Description', N'CPTU notifies PE that remarks info stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintMaster', 'COLUMN', N'notificationRemarksToPe'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Officer Comments Info stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintMaster', 'COLUMN', N'officerComments'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Officer Process Date & time info stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintMaster', 'COLUMN', N'officerProcessDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Fk tbl_ReviewPanel', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintMaster', 'COLUMN', N'panelId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Payment Status info stores here  (complaint wise)', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintMaster', 'COLUMN', N'paymentStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Tenderer Comments Info stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintMaster', 'COLUMN', N'tendererComments'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK,tbl_TendererMaster', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintMaster', 'COLUMN', N'tendererId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK,tbl_TenderMaster ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintMaster', 'COLUMN', N'tenderId'
GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintMaster] SET (LOCK_ESCALATION = TABLE)
GO
