SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_AppPermission] (
		[AppPermissionid]     [bigint] IDENTITY(1, 1) NOT NULL,
		[packageId]           [int] NOT NULL,
		[WorkType]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[WorkCategroy]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_AppPermission]
		PRIMARY KEY
		CLUSTERED
		([AppPermissionid])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_AppPermission] SET (LOCK_ESCALATION = TABLE)
GO
