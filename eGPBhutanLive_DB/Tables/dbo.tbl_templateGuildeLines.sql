SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_templateGuildeLines] (
		[guidelinesId]     [int] IDENTITY(1, 1) NOT NULL,
		[description]      [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docName]          [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[templateId]       [int] NOT NULL,
		[docSize]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDt]       [datetime] NOT NULL,
		[uploadedBy]       [int] NOT NULL,
		CONSTRAINT [PK_tbl_templateGuildeLines]
		PRIMARY KEY
		CLUSTERED
		([guidelinesId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_templateGuildeLines] SET (LOCK_ESCALATION = TABLE)
GO
