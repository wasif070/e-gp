SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CorrigendumDetail] (
		[corriDetailId]     [int] IDENTITY(1, 1) NOT NULL,
		[corrigendumId]     [int] NOT NULL,
		[fieldName]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[oldValue]          [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[newValue]          [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_CorrigendumDetail]
		PRIMARY KEY
		CLUSTERED
		([corriDetailId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CorrigendumDetail]
	WITH CHECK
	ADD CONSTRAINT [corrigendumId_FK1]
	FOREIGN KEY ([corrigendumId]) REFERENCES [dbo].[tbl_CorrigendumMaster] ([corrigendumId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CorrigendumDetail]
	CHECK CONSTRAINT [corrigendumId_FK1]

GO
ALTER TABLE [dbo].[tbl_CorrigendumDetail]
	WITH CHECK
	ADD CONSTRAINT [FKA0F05079CFBAA36F]
	FOREIGN KEY ([corrigendumId]) REFERENCES [dbo].[tbl_CorrigendumMaster] ([corrigendumId])
ALTER TABLE [dbo].[tbl_CorrigendumDetail]
	CHECK CONSTRAINT [FKA0F05079CFBAA36F]

GO
ALTER TABLE [dbo].[tbl_CorrigendumDetail] SET (LOCK_ESCALATION = TABLE)
GO
