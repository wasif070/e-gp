SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_DivisionOrder] (
		[ordId]            [int] IDENTITY(1, 1) NOT NULL,
		[DivisionName]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_DivisionOrder]
		PRIMARY KEY
		CLUSTERED
		([ordId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_DivisionOrder] SET (LOCK_ESCALATION = TABLE)
GO
