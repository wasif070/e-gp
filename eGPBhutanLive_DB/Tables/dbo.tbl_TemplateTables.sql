SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TemplateTables] (
		[tableId]               [int] IDENTITY(1, 1) NOT NULL,
		[formId]                [int] NOT NULL,
		[templateId]            [smallint] NOT NULL,
		[sectionId]             [int] NOT NULL,
		[tableName]             [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tableHeader]           [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tableFooter]           [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[noOfRows]              [smallint] NOT NULL,
		[noOfCols]              [smallint] NOT NULL,
		[isMultipleFilling]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [tableId_PK]
		PRIMARY KEY
		CLUSTERED
		([tableId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TemplateTables]
	WITH CHECK
	ADD CONSTRAINT [FKD8E47A40904D9C11]
	FOREIGN KEY ([templateId]) REFERENCES [dbo].[tbl_TemplateMaster] ([templateId])
ALTER TABLE [dbo].[tbl_TemplateTables]
	CHECK CONSTRAINT [FKD8E47A40904D9C11]

GO
ALTER TABLE [dbo].[tbl_TemplateTables]
	WITH CHECK
	ADD CONSTRAINT [FKD8E47A40D4713FCE]
	FOREIGN KEY ([formId]) REFERENCES [dbo].[tbl_TemplateSectionForm] ([formId])
ALTER TABLE [dbo].[tbl_TemplateTables]
	CHECK CONSTRAINT [FKD8E47A40D4713FCE]

GO
ALTER TABLE [dbo].[tbl_TemplateTables]
	WITH CHECK
	ADD CONSTRAINT [FormTable_FK1]
	FOREIGN KEY ([formId]) REFERENCES [dbo].[tbl_TemplateSectionForm] ([formId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TemplateTables]
	CHECK CONSTRAINT [FormTable_FK1]

GO
ALTER TABLE [dbo].[tbl_TemplateTables]
	WITH CHECK
	ADD CONSTRAINT [templatetable_FK2]
	FOREIGN KEY ([templateId]) REFERENCES [dbo].[tbl_TemplateMaster] ([templateId])
ALTER TABLE [dbo].[tbl_TemplateTables]
	CHECK CONSTRAINT [templatetable_FK2]

GO
ALTER TABLE [dbo].[tbl_TemplateTables] SET (LOCK_ESCALATION = TABLE)
GO
