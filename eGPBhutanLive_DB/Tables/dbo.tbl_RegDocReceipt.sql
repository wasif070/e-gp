SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_RegDocReceipt] (
		[regDocReceiptId]       [int] IDENTITY(1, 1) NOT NULL,
		[userId]                [int] NOT NULL,
		[docReceiptDt]          [smalldatetime] NOT NULL,
		[courierTrackingNo]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[comments]              [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[receivedBy]            [int] NOT NULL,
		[entryDt]               [smalldatetime] NOT NULL,
		CONSTRAINT [PK_tbl_RegDocReceipt]
		PRIMARY KEY
		CLUSTERED
		([regDocReceiptId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_RegDocReceipt]
	WITH CHECK
	ADD CONSTRAINT [FKF4BDFEB36174DBD1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_RegDocReceipt]
	CHECK CONSTRAINT [FKF4BDFEB36174DBD1]

GO
ALTER TABLE [dbo].[tbl_RegDocReceipt]
	WITH CHECK
	ADD CONSTRAINT [regReceiptUserId_FK1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_RegDocReceipt]
	CHECK CONSTRAINT [regReceiptUserId_FK1]

GO
ALTER TABLE [dbo].[tbl_RegDocReceipt] SET (LOCK_ESCALATION = TABLE)
GO
