SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_AppPlannedDays] (
		[expCompDaysId]         [int] IDENTITY(1, 1) NOT NULL,
		[procurementRoleId]     [int] NOT NULL,
		[expRptSubDays]         [smallint] NOT NULL,
		[expRptAppDays]         [smallint] NOT NULL,
		[expNoaDays]            [smallint] NOT NULL,
		CONSTRAINT [PK_tbl_AppPlannedDays]
		PRIMARY KEY
		CLUSTERED
		([expCompDaysId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_AppPlannedDays] SET (LOCK_ESCALATION = TABLE)
GO
