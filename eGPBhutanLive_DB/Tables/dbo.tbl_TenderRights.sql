SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderRights] (
		[rightsId]      [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]      [int] NOT NULL,
		[userId]        [int] NOT NULL,
		[isCurrent]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[mapDate]       [datetime] NOT NULL,
		[mappedBy]      [int] NOT NULL,
		CONSTRAINT [PK_tbl_TenderRights]
		PRIMARY KEY
		CLUSTERED
		([rightsId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderRights]
	ADD
	CONSTRAINT [DF_tbl_TenderRights_isCurrent]
	DEFAULT ('Yes') FOR [isCurrent]
GO
ALTER TABLE [dbo].[tbl_TenderRights]
	WITH NOCHECK
	ADD CONSTRAINT [FKAB58F4CC98B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_TenderRights]
	CHECK CONSTRAINT [FKAB58F4CC98B66EC5]

GO
ALTER TABLE [dbo].[tbl_TenderRights]
	WITH NOCHECK
	ADD CONSTRAINT [tenderRights_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderRights]
	CHECK CONSTRAINT [tenderRights_FK1]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Pk of rights ID', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TenderRights', 'COLUMN', N'rightsId'
GO
ALTER TABLE [dbo].[tbl_TenderRights] SET (LOCK_ESCALATION = TABLE)
GO
