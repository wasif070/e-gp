SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TemplateSections] (
		[sectionId]        [int] IDENTITY(1, 1) NOT NULL,
		[sectionName]      [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[templateId]       [smallint] NOT NULL,
		[contentType]      [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isSubSection]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[status]           [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [sectionId_PK]
		PRIMARY KEY
		CLUSTERED
		([sectionId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TemplateSections] SET (LOCK_ESCALATION = TABLE)
GO
