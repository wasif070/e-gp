SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TendererMaster] (
		[tendererId]           [int] IDENTITY(1, 1) NOT NULL,
		[userId]               [int] NOT NULL,
		[companyId]            [int] NOT NULL,
		[isAdmin]              [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[title]                [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[firstName]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[middleName]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[lastName]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[fullNameInBangla]     [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[nationality]          [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[nationalIdNo]         [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[address1]             [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[address2]             [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[country]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[state]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[city]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[upJilla]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[postcode]             [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[phoneNo]              [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[mobileNo]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[faxNo]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tinNo]                [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tinDocName]           [varchar](130) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[comments]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[specialization]       [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[website]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[designation]          [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[department]           [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[lasatName]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[emailAddress]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[subDistrict]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OtherTitle]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UserType]             [int] NULL,
		CONSTRAINT [tendererId_PK]
		PRIMARY KEY
		CLUSTERED
		([tendererId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TendererMaster_companyId]
	DEFAULT ((0)) FOR [companyId]
GO
ALTER TABLE [dbo].[tbl_TendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TendererMaster_country]
	DEFAULT ((0)) FOR [country]
GO
ALTER TABLE [dbo].[tbl_TendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TendererMaster_isAdmin]
	DEFAULT ('no') FOR [isAdmin]
GO
ALTER TABLE [dbo].[tbl_TendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TendererMaster_state]
	DEFAULT ((0)) FOR [state]
GO
ALTER TABLE [dbo].[tbl_TendererMaster]
	ADD
	CONSTRAINT [DF_tbl_TendererMaster_userId]
	DEFAULT ((0)) FOR [userId]
GO
ALTER TABLE [dbo].[tbl_TendererMaster]
	WITH CHECK
	ADD CONSTRAINT [companyIdTenderer_FK2]
	FOREIGN KEY ([companyId]) REFERENCES [dbo].[tbl_CompanyMaster] ([companyId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TendererMaster]
	CHECK CONSTRAINT [companyIdTenderer_FK2]

GO
ALTER TABLE [dbo].[tbl_TendererMaster]
	WITH CHECK
	ADD CONSTRAINT [FK9DFC53046174DBD1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_TendererMaster]
	CHECK CONSTRAINT [FK9DFC53046174DBD1]

GO
ALTER TABLE [dbo].[tbl_TendererMaster]
	WITH CHECK
	ADD CONSTRAINT [FK9DFC5304EC585617]
	FOREIGN KEY ([companyId]) REFERENCES [dbo].[tbl_CompanyMaster] ([companyId])
ALTER TABLE [dbo].[tbl_TendererMaster]
	CHECK CONSTRAINT [FK9DFC5304EC585617]

GO
ALTER TABLE [dbo].[tbl_TendererMaster]
	WITH CHECK
	ADD CONSTRAINT [userIdTenderer_FK1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TendererMaster]
	CHECK CONSTRAINT [userIdTenderer_FK1]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Address information of company''s registered  user stores  here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'address1'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Alternate address information of company''s registered user stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'address2'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Location of registering user stores  here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'city'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Registering user remarks information stores  here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'comments'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Refer tbl_CompanyMaster , to identify in which company user belongs', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'companyId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Refer tbl_CountryMaster, Id of country stores  here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'country'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Company user department  information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'department'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Company user designation information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'designation'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Fax no of registering user stores  here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'faxNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'First name of an registering user stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'firstName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Full name in bangla information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'fullNameInBangla'
GO
EXEC sp_addextendedproperty N'MS_Description', N'To identify in user is company admin or not this information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'isAdmin'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Lastname of registering user stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'lastName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Middlename of registering user stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'middleName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mobile no or registering user information stores  here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'mobileNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'National Id information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'nationalIdNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Registering user nationality information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'nationality'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Phone number of registering user stores  here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'phoneNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'postal code of registering user stores  here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'postcode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Registering user business category information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'specialization'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Refer tbl_StateMaster,Id of stste stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'state'
GO
EXEC sp_addextendedproperty N'MS_Description', N'System generated id information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'tendererId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Tin document name information stores  here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'tinDocName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Tin no or registering user information stores  here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'tinNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Title of user stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'title'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Upjilla information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'upJilla'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Refer tbl_LoginMaster', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'userId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'registering user website  information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TendererMaster', 'COLUMN', N'website'
GO
ALTER TABLE [dbo].[tbl_TendererMaster] SET (LOCK_ESCALATION = TABLE)
GO
