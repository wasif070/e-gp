SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_SrvAttSheetMaster] (
		[attSheetId]     [int] IDENTITY(1, 1) NOT NULL,
		[createdBy]      [int] NOT NULL,
		[createdDt]      [datetime] NOT NULL,
		[status]         [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderId]       [int] NOT NULL,
		[contractId]     [int] NULL,
		[monthId]        [int] NOT NULL,
		[year]           [int] NOT NULL,
		CONSTRAINT [PK_tbl_CMS_AttSheetMaster]
		PRIMARY KEY
		CLUSTERED
		([attSheetId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvAttSheetMaster] SET (LOCK_ESCALATION = TABLE)
GO
