SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_FinalSubDetail] (
		[finalSubDetailId]        [int] IDENTITY(1, 1) NOT NULL,
		[tenderFormid]            [int] NOT NULL,
		[ipAddress]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[finalSubmissionId]       [int] NOT NULL,
		[formHash]                [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[bidId]                   [int] NOT NULL,
		[newFormCombinedHash]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [finalSubDetailId_PK]
		PRIMARY KEY
		CLUSTERED
		([finalSubDetailId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_FinalSubDetail]
	WITH CHECK
	ADD CONSTRAINT [FK197DFC9C9C3639BF]
	FOREIGN KEY ([finalSubmissionId]) REFERENCES [dbo].[tbl_FinalSubmission] ([finalSubmissionId])
ALTER TABLE [dbo].[tbl_FinalSubDetail]
	CHECK CONSTRAINT [FK197DFC9C9C3639BF]

GO
ALTER TABLE [dbo].[tbl_FinalSubDetail] SET (LOCK_ESCALATION = TABLE)
GO
