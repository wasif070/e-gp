SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_LimitedTenders] (
		[limitedId]      [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]       [int] NOT NULL,
		[userId]         [int] NOT NULL,
		[mappedDate]     [smalldatetime] NOT NULL,
		[pkgLotId]       [int] NOT NULL,
		CONSTRAINT [PK_tbl_LimitedTenders]
		PRIMARY KEY
		CLUSTERED
		([limitedId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_LimitedTenders]
	ADD
	CONSTRAINT [DF_tbl_LimitedTenders_pkgLotId]
	DEFAULT ((0)) FOR [pkgLotId]
GO
ALTER TABLE [dbo].[tbl_LimitedTenders]
	WITH NOCHECK
	ADD CONSTRAINT [limitedTenderId_Fk1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_LimitedTenders]
	CHECK CONSTRAINT [limitedTenderId_Fk1]

GO
ALTER TABLE [dbo].[tbl_LimitedTenders] SET (LOCK_ESCALATION = TABLE)
GO
