SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ScBankDevPartnerMaster] (
		[sBankDevelopId]       [int] IDENTITY(1, 1) NOT NULL,
		[sbDevelopName]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[officeAddress]        [varchar](1500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[countryId]            [smallint] NOT NULL,
		[district]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[stateId]              [smallint] NOT NULL,
		[cityTown]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[thanaUpzilla]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[postCode]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[phoneNo]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[faxNo]                [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sBankDevelHeadId]     [int] NOT NULL,
		[swiftCode]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[website]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]            [int] NOT NULL,
		[createdDate]          [smalldatetime] NOT NULL,
		[isBranchOffice]       [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[partnerType]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [sBankDevelopId_PK]
		PRIMARY KEY
		CLUSTERED
		([sBankDevelopId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ScBankDevPartnerMaster]
	WITH CHECK
	ADD CONSTRAINT [FK4F2335C21ADFF23F]
	FOREIGN KEY ([stateId]) REFERENCES [dbo].[tbl_StateMaster] ([stateId])
ALTER TABLE [dbo].[tbl_ScBankDevPartnerMaster]
	CHECK CONSTRAINT [FK4F2335C21ADFF23F]

GO
ALTER TABLE [dbo].[tbl_ScBankDevPartnerMaster]
	WITH CHECK
	ADD CONSTRAINT [FK4F2335C2F059B349]
	FOREIGN KEY ([countryId]) REFERENCES [dbo].[tbl_CountryMaster] ([countryId])
ALTER TABLE [dbo].[tbl_ScBankDevPartnerMaster]
	CHECK CONSTRAINT [FK4F2335C2F059B349]

GO
ALTER TABLE [dbo].[tbl_ScBankDevPartnerMaster]
	WITH CHECK
	ADD CONSTRAINT [partnerCountry_FK2]
	FOREIGN KEY ([countryId]) REFERENCES [dbo].[tbl_CountryMaster] ([countryId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ScBankDevPartnerMaster]
	CHECK CONSTRAINT [partnerCountry_FK2]

GO
ALTER TABLE [dbo].[tbl_ScBankDevPartnerMaster]
	WITH CHECK
	ADD CONSTRAINT [partnerState_FK1]
	FOREIGN KEY ([stateId]) REFERENCES [dbo].[tbl_StateMaster] ([stateId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ScBankDevPartnerMaster]
	CHECK CONSTRAINT [partnerState_FK1]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Name of the City gets stored here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ScBankDevPartnerMaster', 'COLUMN', N'cityTown'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stores country of scheduled bank/development partner', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ScBankDevPartnerMaster', 'COLUMN', N'countryId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Userid one who is creating gets stored here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ScBankDevPartnerMaster', 'COLUMN', N'createdBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Creation date is captured here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ScBankDevPartnerMaster', 'COLUMN', N'createdDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'If partner type is Scheduled bank than district name is stored here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ScBankDevPartnerMaster', 'COLUMN', N'district'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Fax Number is stored here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ScBankDevPartnerMaster', 'COLUMN', N'faxNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Yes or No value gets stored here Yes means Its a branch of the Scheduled Bank OR Development office Branch of the development partner.', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ScBankDevPartnerMaster', 'COLUMN', N'isBranchOffice'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stores Address of the head office', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ScBankDevPartnerMaster', 'COLUMN', N'officeAddress'
GO
EXEC sp_addextendedproperty N'MS_Description', N'This field specifies type of partner (Scheduled bank or Development Partner)', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ScBankDevPartnerMaster', 'COLUMN', N'partnerType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Contact Phone number gets stored here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ScBankDevPartnerMaster', 'COLUMN', N'phoneNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Postal Code is stored here. In case Partner type is Development than Zip code can be stored here.', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ScBankDevPartnerMaster', 'COLUMN', N'postCode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'If partner are getting registered as Branch office or develop Office than the Main Id of the Bank or Development id is stored here. Refer column sBankDevelopId in same table', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ScBankDevPartnerMaster', 'COLUMN', N'sBankDevelHeadId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'System Generated ID stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ScBankDevPartnerMaster', 'COLUMN', N'sBankDevelopId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name of the Bank/Development Partner stores here,', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ScBankDevPartnerMaster', 'COLUMN', N'sbDevelopName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name of the State get stored here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ScBankDevPartnerMaster', 'COLUMN', N'stateId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Swift code of the Scheduled Bank Branch is stored here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ScBankDevPartnerMaster', 'COLUMN', N'swiftCode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'If partner type is Scheduled bank than thanna/upzilla name gets stored here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ScBankDevPartnerMaster', 'COLUMN', N'thanaUpzilla'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Website URL gets stored here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_ScBankDevPartnerMaster', 'COLUMN', N'website'
GO
ALTER TABLE [dbo].[tbl_ScBankDevPartnerMaster] SET (LOCK_ESCALATION = TABLE)
GO
