SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TempTendererEsignature] (
		[eSignatureId]      [int] IDENTITY(1, 1) NOT NULL,
		[documentName]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[documentSize]      [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[documentBrief]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]      [smalldatetime] NOT NULL,
		[tendererId]        [int] NOT NULL,
		CONSTRAINT [temp_eSignatureId_PK]
		PRIMARY KEY
		CLUSTERED
		([eSignatureId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TempTendererEsignature]
	WITH NOCHECK
	ADD CONSTRAINT [FK1B8A6C09ED53FE13]
	FOREIGN KEY ([tendererId]) REFERENCES [dbo].[tbl_TempTendererMaster] ([tendererId])
ALTER TABLE [dbo].[tbl_TempTendererEsignature]
	CHECK CONSTRAINT [FK1B8A6C09ED53FE13]

GO
ALTER TABLE [dbo].[tbl_TempTendererEsignature]
	WITH NOCHECK
	ADD CONSTRAINT [tempTendererSign_FK1]
	FOREIGN KEY ([tendererId]) REFERENCES [dbo].[tbl_TempTendererMaster] ([tendererId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TempTendererEsignature]
	CHECK CONSTRAINT [tempTendererSign_FK1]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Document brief information stores here it will explain brief information about Tenderer eSignature document', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererEsignature', 'COLUMN', N'documentBrief'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name of document stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererEsignature', 'COLUMN', N'documentName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Size of document stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererEsignature', 'COLUMN', N'documentSize'
GO
EXEC sp_addextendedproperty N'MS_Description', N'System generated id information stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererEsignature', 'COLUMN', N'eSignatureId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'refer tbl_TendererMaster', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererEsignature', 'COLUMN', N'tendererId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date of uploaded document stores here', 'SCHEMA', N'dbo', 'TABLE', N'tbl_TempTendererEsignature', 'COLUMN', N'uploadedDate'
GO
ALTER TABLE [dbo].[tbl_TempTendererEsignature] SET (LOCK_ESCALATION = TABLE)
GO
