SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_PromisProcess] (
		[Process_ID]                         [int] IDENTITY(1, 1) NOT NULL,
		[officeId]                           [int] NOT NULL,
		[PEOffice_Name]                      [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AvgTrainedProcStaffPE]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PerPETrainedProcStaff]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TotalProcPersonOrgProcTraining]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Ministry_ID]                        [int] NULL,
		[Division_ID]                        [int] NULL,
		[Org_ID]                             [int] NULL,
		[Modify_Date]                        [smalldatetime] NOT NULL,
		[Is_TenderOnline]                    [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Is_Progress]                        [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[prmsPECode]                         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_Tbl_PromisProcess]
		PRIMARY KEY
		CLUSTERED
		([Process_ID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Average no of trained procurement staff in each procuring entity.', 'SCHEMA', N'dbo', 'TABLE', N'Tbl_PromisProcess', 'COLUMN', N'AvgTrainedProcStaffPE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Division Id', 'SCHEMA', N'dbo', 'TABLE', N'Tbl_PromisProcess', 'COLUMN', N'Division_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Is Promise Indecator progess or complete?', 'SCHEMA', N'dbo', 'TABLE', N'Tbl_PromisProcess', 'COLUMN', N'Is_Progress'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Is tender online or offline?', 'SCHEMA', N'dbo', 'TABLE', N'Tbl_PromisProcess', 'COLUMN', N'Is_TenderOnline'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Ministry Id', 'SCHEMA', N'dbo', 'TABLE', N'Tbl_PromisProcess', 'COLUMN', N'Ministry_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Last Modification Date', 'SCHEMA', N'dbo', 'TABLE', N'Tbl_PromisProcess', 'COLUMN', N'Modify_Date'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PE office Id', 'SCHEMA', N'dbo', 'TABLE', N'Tbl_PromisProcess', 'COLUMN', N'officeId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Organisation Id', 'SCHEMA', N'dbo', 'TABLE', N'Tbl_PromisProcess', 'COLUMN', N'Org_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PE Office Name', 'SCHEMA', N'dbo', 'TABLE', N'Tbl_PromisProcess', 'COLUMN', N'PEOffice_Name'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Percentage of procuring entity which has at least ONE trained/certifies  procurement staff.', 'SCHEMA', N'dbo', 'TABLE', N'Tbl_PromisProcess', 'COLUMN', N'PerPETrainedProcStaff'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PROMIS PE code.', 'SCHEMA', N'dbo', 'TABLE', N'Tbl_PromisProcess', 'COLUMN', N'prmsPECode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key of Tbl_PromisProcess', 'SCHEMA', N'dbo', 'TABLE', N'Tbl_PromisProcess', 'COLUMN', N'Process_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Total number of procurement persons in the organization with Procurement training.', 'SCHEMA', N'dbo', 'TABLE', N'Tbl_PromisProcess', 'COLUMN', N'TotalProcPersonOrgProcTraining'
GO
ALTER TABLE [dbo].[Tbl_PromisProcess] SET (LOCK_ESCALATION = TABLE)
GO
