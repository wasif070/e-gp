SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CpvClassification] (
		[CpvId]                [int] IDENTITY(1, 1) NOT NULL,
		[CpvCategory]          [tinyint] NOT NULL,
		[CpvClass]             [tinyint] NOT NULL,
		[CpvCode]              [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CpvDescription]       [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CpvDivision]          [tinyint] NOT NULL,
		[CpvGroup]             [tinyint] NOT NULL,
		[CpvSubCategory]       [tinyint] NOT NULL,
		[CpvSubCategoryL2]     [tinyint] NOT NULL,
		[Expanded]             [int] NOT NULL,
		CONSTRAINT [UQ__CpvClass__C915B4E91C50F768]
		UNIQUE
		NONCLUSTERED
		([CpvId])
		ON [PRIMARY],
		CONSTRAINT [PK__CpvClass__C915B4E865AB0EE6]
		PRIMARY KEY
		CLUSTERED
		([CpvId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CpvClassification] SET (LOCK_ESCALATION = TABLE)
GO
