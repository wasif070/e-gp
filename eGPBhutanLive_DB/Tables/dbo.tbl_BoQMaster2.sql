SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_BoQMaster2] (
		[BoQId]           [bigint] IDENTITY(1, 1) NOT NULL,
		[Category]        [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SubCategory]     [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Code]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Description]     [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Unit]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_BoQMaster2]
		PRIMARY KEY
		CLUSTERED
		([BoQId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_BoQMaster2] SET (LOCK_ESCALATION = TABLE)
GO
