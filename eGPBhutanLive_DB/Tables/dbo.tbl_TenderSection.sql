SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderSection] (
		[tenderSectionId]       [int] IDENTITY(1, 1) NOT NULL,
		[templateSectionId]     [int] NOT NULL,
		[tenderStdId]           [int] NOT NULL,
		[sectionName]           [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[contentType]           [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isSubSection]          [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_TenderSection]
		PRIMARY KEY
		CLUSTERED
		([tenderSectionId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderSection]
	WITH CHECK
	ADD CONSTRAINT [FKEDA56A9050F62959]
	FOREIGN KEY ([tenderStdId]) REFERENCES [dbo].[tbl_TenderStd] ([tenderStdId])
ALTER TABLE [dbo].[tbl_TenderSection]
	CHECK CONSTRAINT [FKEDA56A9050F62959]

GO
ALTER TABLE [dbo].[tbl_TenderSection]
	WITH CHECK
	ADD CONSTRAINT [tenderStdId_FK2]
	FOREIGN KEY ([tenderStdId]) REFERENCES [dbo].[tbl_TenderStd] ([tenderStdId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderSection]
	CHECK CONSTRAINT [tenderStdId_FK2]

GO
CREATE NONCLUSTERED INDEX [IX_tbl_TenderSection_TenderSTDID]
	ON [dbo].[tbl_TenderSection] ([tenderStdId])
	INCLUDE ([tenderSectionId])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderSection] SET (LOCK_ESCALATION = TABLE)
GO
