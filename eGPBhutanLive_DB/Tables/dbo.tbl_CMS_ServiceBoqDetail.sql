SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_CMS_ServiceBoqDetail] (
		[srvTenderBoqDtlId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderFormId]          [int] NOT NULL,
		[srvBoqid]              [int] NOT NULL,
		[templateFormID]        [int] NULL,
		CONSTRAINT [srvTenderBoqDtlId_PK]
		PRIMARY KEY
		CLUSTERED
		([srvTenderBoqDtlId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_ServiceBoqDetail]
	WITH NOCHECK
	ADD CONSTRAINT [FK_tbl_CMS_ServiceBoqDetail_PKr_tbl_CMS_SrvBoqMaster]
	FOREIGN KEY ([srvBoqid]) REFERENCES [dbo].[tbl_CMS_SrvBoqMaster] ([srvBoqId])
ALTER TABLE [dbo].[tbl_CMS_ServiceBoqDetail]
	CHECK CONSTRAINT [FK_tbl_CMS_ServiceBoqDetail_PKr_tbl_CMS_SrvBoqMaster]

GO
ALTER TABLE [dbo].[tbl_CMS_ServiceBoqDetail]
	WITH NOCHECK
	ADD CONSTRAINT [serviceBoqDetlTenderFormId_FK1]
	FOREIGN KEY ([templateFormID]) REFERENCES [dbo].[tbl_TenderForms] ([tenderFormId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_ServiceBoqDetail]
	CHECK CONSTRAINT [serviceBoqDetlTenderFormId_FK1]

GO
ALTER TABLE [dbo].[tbl_CMS_ServiceBoqDetail] SET (LOCK_ESCALATION = TABLE)
GO
