SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalMemStatus] (
		[evalMemStatusId]        [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]               [int] NOT NULL,
		[userId]                 [int] NOT NULL,
		[formId]                 [int] NOT NULL,
		[isComplied]             [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[evalStatus]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[evalStatusDt]           [smalldatetime] NOT NULL,
		[evalBy]                 [int] NOT NULL,
		[evalNonCompRemarks]     [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[actualMarks]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_EvalMemStatus]
		PRIMARY KEY
		CLUSTERED
		([evalMemStatusId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalMemStatus]
	ADD
	CONSTRAINT [DF_tbl_EvalMemStatus_actualMarks]
	DEFAULT ((0)) FOR [actualMarks]
GO
ALTER TABLE [dbo].[tbl_EvalMemStatus] SET (LOCK_ESCALATION = TABLE)
GO
