SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_SrvWPVari] (
		[srvWPVariId]      [int] IDENTITY(1, 1) NOT NULL,
		[srNo]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[activity]         [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[startDt]          [datetime] NOT NULL,
		[noOfDays]         [int] NOT NULL,
		[endDt]            [datetime] NOT NULL,
		[remarks]          [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[srvFormMapId]     [int] NOT NULL,
		[variOrdId]        [int] NOT NULL,
		CONSTRAINT [PK_tbl_CMS_SrvWPVari]
		PRIMARY KEY
		CLUSTERED
		([srvWPVariId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvWPVari] SET (LOCK_ESCALATION = TABLE)
GO
