SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_MenuMaster] (
		[menuId]           [int] IDENTITY(1, 1) NOT NULL,
		[menuName]         [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[parentMenuId]     [int] NOT NULL,
		[menuLink]         [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[menuImage]        [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[activation]       [int] NOT NULL,
		CONSTRAINT [PK_tbl_MenuMaster]
		PRIMARY KEY
		CLUSTERED
		([menuId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_MenuMaster] SET (LOCK_ESCALATION = TABLE)
GO
