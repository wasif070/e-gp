SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_SubContracting] (
		[subConId]            [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]            [int] NOT NULL,
		[invToUserId]         [int] NOT NULL,
		[invFromUserId]       [int] NOT NULL,
		[remarks]             [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[lastAcceptDt]        [smalldatetime] NOT NULL,
		[invSentDt]           [smalldatetime] NOT NULL,
		[invAcceptStatus]     [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[invAcceptDate]       [smalldatetime] NOT NULL,
		[acceptComments]      [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[pkgLotId]            [int] NOT NULL,
		CONSTRAINT [subConId_PK]
		PRIMARY KEY
		CLUSTERED
		([subConId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_SubContracting]
	ADD
	CONSTRAINT [DF_tbl_SubContracting_pkgLotId]
	DEFAULT ((0)) FOR [pkgLotId]
GO
ALTER TABLE [dbo].[tbl_SubContracting]
	WITH NOCHECK
	ADD CONSTRAINT [contTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_SubContracting]
	CHECK CONSTRAINT [contTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_SubContracting]
	WITH NOCHECK
	ADD CONSTRAINT [FK88BBDDF198B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_SubContracting]
	CHECK CONSTRAINT [FK88BBDDF198B66EC5]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Invitation accept date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_SubContracting', 'COLUMN', N'invAcceptDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Invitation accept status like pending\reject information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_SubContracting', 'COLUMN', N'invAcceptStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'invitation from userId information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_SubContracting', 'COLUMN', N'invFromUserId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Invitation sent date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_SubContracting', 'COLUMN', N'invSentDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'invitation to userId information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_SubContracting', 'COLUMN', N'invToUserId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Last accepting invitation date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_SubContracting', 'COLUMN', N'lastAcceptDt'
GO
EXEC sp_addextendedproperty N'MS_Description', N'sub contract comments information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_SubContracting', 'COLUMN', N'remarks'
GO
EXEC sp_addextendedproperty N'MS_Description', N'system generated unique Id information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_SubContracting', 'COLUMN', N'subConId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'refer tenderId from tbl_TenderMaster, to identify the tenderId', 'SCHEMA', N'dbo', 'TABLE', N'tbl_SubContracting', 'COLUMN', N'tenderId'
GO
ALTER TABLE [dbo].[tbl_SubContracting] SET (LOCK_ESCALATION = TABLE)
GO
