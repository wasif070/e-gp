SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_PaymentVerification] (
		[verificationId]        [int] IDENTITY(1, 1) NOT NULL,
		[paymentId]             [int] NOT NULL,
		[verifiedBy]            [int] NOT NULL,
		[remarks]               [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[verificationDt]        [smalldatetime] NOT NULL,
		[verifyPartTransId]     [int] NULL,
		CONSTRAINT [PK_tbl_PaymentVerification]
		PRIMARY KEY
		CLUSTERED
		([verificationId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_PaymentVerification] SET (LOCK_ESCALATION = TABLE)
GO
