SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalTSCComments] (
		[commentsId]       [int] IDENTITY(1, 1) NOT NULL,
		[tscUserId]        [int] NOT NULL,
		[tenderId]         [int] NOT NULL,
		[pkgLotId]         [int] NOT NULL,
		[formId]           [int] NOT NULL,
		[userId]           [int] NOT NULL,
		[comments]         [varchar](5000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[commentsDt]       [datetime] NOT NULL,
		[evalStatus]       [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tscGovUserId]     [int] NULL,
		CONSTRAINT [PK_tbl_EvalTSCComments]
		PRIMARY KEY
		CLUSTERED
		([commentsId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalTSCComments]
	ADD
	CONSTRAINT [DF_tbl_EvalTSCComments_tscGovUserId]
	DEFAULT ((0)) FOR [tscGovUserId]
GO
ALTER TABLE [dbo].[tbl_EvalTSCComments] SET (LOCK_ESCALATION = TABLE)
GO
