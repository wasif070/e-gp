SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_EmployeeRoles] (
		[employeeRoleId]        [int] IDENTITY(1, 1) NOT NULL,
		[procurementRoleId]     [tinyint] NOT NULL,
		[employeeId]            [int] NOT NULL,
		[departmentId]          [smallint] NOT NULL,
		CONSTRAINT [employeeRoleId_PK]
		PRIMARY KEY
		CLUSTERED
		([employeeRoleId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EmployeeRoles]
	WITH CHECK
	ADD CONSTRAINT [employee_FK3]
	FOREIGN KEY ([employeeId]) REFERENCES [dbo].[tbl_EmployeeMaster] ([employeeId])
ALTER TABLE [dbo].[tbl_EmployeeRoles]
	CHECK CONSTRAINT [employee_FK3]

GO
ALTER TABLE [dbo].[tbl_EmployeeRoles]
	WITH CHECK
	ADD CONSTRAINT [employeeRoleDept_FK4]
	FOREIGN KEY ([departmentId]) REFERENCES [dbo].[tbl_DepartmentMaster] ([departmentId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EmployeeRoles]
	CHECK CONSTRAINT [employeeRoleDept_FK4]

GO
ALTER TABLE [dbo].[tbl_EmployeeRoles]
	WITH CHECK
	ADD CONSTRAINT [empProcurementRole_FK5]
	FOREIGN KEY ([procurementRoleId]) REFERENCES [dbo].[tbl_ProcurementRole] ([procurementRoleId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EmployeeRoles]
	CHECK CONSTRAINT [empProcurementRole_FK5]

GO
ALTER TABLE [dbo].[tbl_EmployeeRoles]
	WITH CHECK
	ADD CONSTRAINT [FK56AD11CEC90CE487]
	FOREIGN KEY ([procurementRoleId]) REFERENCES [dbo].[tbl_ProcurementRole] ([procurementRoleId])
ALTER TABLE [dbo].[tbl_EmployeeRoles]
	CHECK CONSTRAINT [FK56AD11CEC90CE487]

GO
ALTER TABLE [dbo].[tbl_EmployeeRoles]
	WITH CHECK
	ADD CONSTRAINT [FK56AD11CEE4B351C1]
	FOREIGN KEY ([departmentId]) REFERENCES [dbo].[tbl_DepartmentMaster] ([departmentId])
ALTER TABLE [dbo].[tbl_EmployeeRoles]
	CHECK CONSTRAINT [FK56AD11CEE4B351C1]

GO
ALTER TABLE [dbo].[tbl_EmployeeRoles]
	WITH CHECK
	ADD CONSTRAINT [FK56AD11CEF5489C79]
	FOREIGN KEY ([employeeId]) REFERENCES [dbo].[tbl_EmployeeMaster] ([employeeId])
ALTER TABLE [dbo].[tbl_EmployeeRoles]
	CHECK CONSTRAINT [FK56AD11CEF5489C79]

GO
ALTER TABLE [dbo].[tbl_EmployeeRoles] SET (LOCK_ESCALATION = TABLE)
GO
