SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CommitteeMembers] (
		[comMemberId]          [int] IDENTITY(1, 1) NOT NULL,
		[committeeId]          [int] NOT NULL,
		[userId]               [int] NOT NULL,
		[createdDate]          [smalldatetime] NOT NULL,
		[appStatus]            [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[appDate]              [smalldatetime] NOT NULL,
		[remarks]              [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[eSignature]           [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[digitalSignature]     [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[memberRole]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[memberFrom]           [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[govUserId]            [int] NOT NULL,
		CONSTRAINT [PK_tbl_CommitteeMembers]
		PRIMARY KEY
		CLUSTERED
		([comMemberId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CommitteeMembers]
	ADD
	CONSTRAINT [DF_tbl_CommitteeMembers_govUserId]
	DEFAULT ((0)) FOR [govUserId]
GO
ALTER TABLE [dbo].[tbl_CommitteeMembers]
	WITH CHECK
	ADD CONSTRAINT [FKAD0F0D5DF3444755]
	FOREIGN KEY ([committeeId]) REFERENCES [dbo].[tbl_Committee] ([committeeId])
ALTER TABLE [dbo].[tbl_CommitteeMembers]
	CHECK CONSTRAINT [FKAD0F0D5DF3444755]

GO
ALTER TABLE [dbo].[tbl_CommitteeMembers]
	WITH CHECK
	ADD CONSTRAINT [memCommitteeId_FK1]
	FOREIGN KEY ([committeeId]) REFERENCES [dbo].[tbl_Committee] ([committeeId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CommitteeMembers]
	CHECK CONSTRAINT [memCommitteeId_FK1]

GO
EXEC sp_addextendedproperty N'MS_Description', N'App date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CommitteeMembers', 'COLUMN', N'appDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'App status information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CommitteeMembers', 'COLUMN', N'appStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'system generated unique Id information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CommitteeMembers', 'COLUMN', N'comMemberId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'refer from tbl_Committee, to identify the committeId', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CommitteeMembers', 'COLUMN', N'committeeId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Committe member creation date information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CommitteeMembers', 'COLUMN', N'createdDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Digital signature information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CommitteeMembers', 'COLUMN', N'digitalSignature'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Electronic Signature information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CommitteeMembers', 'COLUMN', N'eSignature'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Committee member remarks information stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CommitteeMembers', 'COLUMN', N'remarks'
GO
EXEC sp_addextendedproperty N'MS_Description', N'refer from tbl_LoginMaster, to identify the userId', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CommitteeMembers', 'COLUMN', N'userId'
GO
ALTER TABLE [dbo].[tbl_CommitteeMembers] SET (LOCK_ESCALATION = TABLE)
GO
