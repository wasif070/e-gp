SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TosDocs] (
		[tosDocId]           [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]           [int] NOT NULL,
		[documentName]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]       [smalldatetime] NOT NULL,
		[uploadedBy]         [int] NOT NULL,
		CONSTRAINT [PK_tbl_TosDocs]
		PRIMARY KEY
		CLUSTERED
		([tosDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TosDocs]
	WITH NOCHECK
	ADD CONSTRAINT [FK34974C1298B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_TosDocs]
	CHECK CONSTRAINT [FK34974C1298B66EC5]

GO
ALTER TABLE [dbo].[tbl_TosDocs]
	WITH NOCHECK
	ADD CONSTRAINT [tosTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TosDocs]
	CHECK CONSTRAINT [tosTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_TosDocs] SET (LOCK_ESCALATION = TABLE)
GO
