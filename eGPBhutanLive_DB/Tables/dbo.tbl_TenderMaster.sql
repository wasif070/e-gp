SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderMaster] (
		[tenderId]          [int] IDENTITY(1, 1) NOT NULL,
		[appId]             [int] NOT NULL,
		[packageId]         [int] NOT NULL,
		[createdBy]         [int] NOT NULL,
		[creationDate]      [smalldatetime] NOT NULL,
		[financialYear]     [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderType]        [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [tenderId_PK]
		PRIMARY KEY
		CLUSTERED
		([tenderId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderMaster]
	WITH NOCHECK
	ADD CONSTRAINT [FKA2659D5714F1608F]
	FOREIGN KEY ([packageId]) REFERENCES [dbo].[tbl_AppPackages] ([packageId])
ALTER TABLE [dbo].[tbl_TenderMaster]
	CHECK CONSTRAINT [FKA2659D5714F1608F]

GO
ALTER TABLE [dbo].[tbl_TenderMaster]
	WITH NOCHECK
	ADD CONSTRAINT [tenderPackageId_FK2]
	FOREIGN KEY ([packageId]) REFERENCES [dbo].[tbl_AppPackages] ([packageId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderMaster]
	CHECK CONSTRAINT [tenderPackageId_FK2]

GO
ALTER TABLE [dbo].[tbl_TenderMaster] SET (LOCK_ESCALATION = TABLE)
GO
