SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TdsSubClause] (
		[tdsSubClauseId]       [int] IDENTITY(1, 1) NOT NULL,
		[ittReference]         [int] NOT NULL,
		[orderNumber]          [smallint] NOT NULL,
		[tdsSubClauseName]     [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ittHeaderId]          [int] NOT NULL,
		CONSTRAINT [UQ__tbl_TdsS__039885BE3631FF56]
		UNIQUE
		NONCLUSTERED
		([tdsSubClauseId])
		ON [PRIMARY],
		CONSTRAINT [PK__tbl_TdsS__039885BF335592AB]
		PRIMARY KEY
		CLUSTERED
		([tdsSubClauseId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TdsSubClause]
	WITH CHECK
	ADD CONSTRAINT [FK19963E0DFB8A1B27]
	FOREIGN KEY ([ittHeaderId]) REFERENCES [dbo].[tbl_IttHeader] ([ittHeaderId])
ALTER TABLE [dbo].[tbl_TdsSubClause]
	CHECK CONSTRAINT [FK19963E0DFB8A1B27]

GO
ALTER TABLE [dbo].[tbl_TdsSubClause] SET (LOCK_ESCALATION = TABLE)
GO
