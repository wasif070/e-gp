SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CorrigendumDocs] (
		[corrDocId]          [int] IDENTITY(1, 1) NOT NULL,
		[corrigendumId]      [int] NOT NULL,
		[documentName]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]     [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]       [smalldatetime] NOT NULL,
		[uploadedBy]         [int] NOT NULL,
		[pkgLotId]           [int] NULL,
		[tenderId]           [int] NULL,
		CONSTRAINT [PK_tbl_CorrigendumDocs]
		PRIMARY KEY
		CLUSTERED
		([corrDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CorrigendumDocs]
	WITH CHECK
	ADD CONSTRAINT [corriDocCorrigendumId_FK1]
	FOREIGN KEY ([corrigendumId]) REFERENCES [dbo].[tbl_CorrigendumMaster] ([corrigendumId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CorrigendumDocs]
	CHECK CONSTRAINT [corriDocCorrigendumId_FK1]

GO
ALTER TABLE [dbo].[tbl_CorrigendumDocs] SET (LOCK_ESCALATION = TABLE)
GO
