SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_SrvPaymentSch] (
		[srvPSId]             [int] IDENTITY(1, 1) NOT NULL,
		[srvFormMapId]        [int] NOT NULL,
		[srNo]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[milestone]           [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[description]         [varchar](5000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[percentOfCtrVal]     [decimal](6, 3) NOT NULL,
		[peenddate]           [datetime] NOT NULL,
		[endDate]             [datetime] NOT NULL,
		[status]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[completedDt]         [datetime] NULL,
		[variOrdId]           [int] NULL,
		CONSTRAINT [srvLumpsumDtlId_PK]
		PRIMARY KEY
		CLUSTERED
		([srvPSId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvPaymentSch]
	ADD
	CONSTRAINT [DF_tbl_CMS_SrvLumpsumDtl_status]
	DEFAULT ('pending') FOR [status]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvPaymentSch] SET (LOCK_ESCALATION = TABLE)
GO
