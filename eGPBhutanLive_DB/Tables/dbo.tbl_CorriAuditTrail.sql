SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CorriAuditTrail] (
		[corrAuditId]          [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]             [int] NOT NULL,
		[corrigendumId]        [int] NOT NULL,
		[remarks]              [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[action]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[actionBy]             [int] NOT NULL,
		[actionDate]           [smalldatetime] NOT NULL,
		[digitalSignature]     [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[eSignature]           [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_CorriAuditTrail]
		PRIMARY KEY
		CLUSTERED
		([corrAuditId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CorriAuditTrail]
	WITH CHECK
	ADD CONSTRAINT [auditCorrigendumId_FK1]
	FOREIGN KEY ([corrigendumId]) REFERENCES [dbo].[tbl_CorrigendumMaster] ([corrigendumId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CorriAuditTrail]
	CHECK CONSTRAINT [auditCorrigendumId_FK1]

GO
ALTER TABLE [dbo].[tbl_CorriAuditTrail] SET (LOCK_ESCALATION = TABLE)
GO
