SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_UserPrefrence] (
		[userPrefId]     [int] IDENTITY(1, 1) NOT NULL,
		[userId]         [int] NOT NULL,
		[emailAlert]     [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[smsAlert]       [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_UserPrefrence]
		PRIMARY KEY
		CLUSTERED
		([userPrefId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_UserPrefrence] SET (LOCK_ESCALATION = TABLE)
GO
