SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_WpDetailHistory] (
		[wpDtlHistoryId]          [int] IDENTITY(1, 1) NOT NULL,
		[wpDetailId]              [int] NOT NULL,
		[wpId]                    [int] NOT NULL,
		[historyCount]            [int] NOT NULL,
		[wpQty]                   [decimal](15, 3) NULL,
		[wpRate]                  [decimal](15, 3) NULL,
		[wpStartDate]             [datetime] NOT NULL,
		[wpEndDate]               [datetime] NOT NULL,
		[wpNoOfDays]              [int] NOT NULL,
		[wpRowId]                 [int] NOT NULL,
		[actContractDtHistId]     [int] NOT NULL,
		[amendmentFlag]           [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderTableId]           [int] NULL,
		[variOrdNo]               [int] NULL,
		[createdDate]             [datetime] NULL,
		[currencyName]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[supplierVat]             [decimal](15, 3) NULL,
		[InlandOthers]            [decimal](15, 3) NULL,
		CONSTRAINT [wpDtlHistoryId_PK]
		PRIMARY KEY
		CLUSTERED
		([wpDtlHistoryId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_WpDetailHistory]
	ADD
	CONSTRAINT [DF_tbl_CMS_WpDetailHistory_amendmentFlag]
	DEFAULT ('original') FOR [amendmentFlag]
GO
ALTER TABLE [dbo].[tbl_CMS_WpDetailHistory]
	ADD
	CONSTRAINT [DF_tbl_Cms_WpDetailHistory_InlandOthers]
	DEFAULT ((0)) FOR [InlandOthers]
GO
ALTER TABLE [dbo].[tbl_CMS_WpDetailHistory]
	ADD
	CONSTRAINT [DF_tbl_Cms_WpDetailHistory_supplierVat]
	DEFAULT ((0)) FOR [supplierVat]
GO
ALTER TABLE [dbo].[tbl_CMS_WpDetailHistory]
	ADD
	CONSTRAINT [DF_tbl_CMS_WpDetailHistory_variOrdNo]
	DEFAULT ((0)) FOR [variOrdNo]
GO
ALTER TABLE [dbo].[tbl_CMS_WpDetailHistory]
	WITH CHECK
	ADD CONSTRAINT [cmswpDetlHistWpDetailId_FK1]
	FOREIGN KEY ([wpDetailId]) REFERENCES [dbo].[tbl_CMS_WpDetail] ([wpDetailId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_WpDetailHistory]
	CHECK CONSTRAINT [cmswpDetlHistWpDetailId_FK1]

GO
ALTER TABLE [dbo].[tbl_CMS_WpDetailHistory] SET (LOCK_ESCALATION = TABLE)
GO
