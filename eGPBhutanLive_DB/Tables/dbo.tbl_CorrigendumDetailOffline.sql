SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CorrigendumDetailOffline] (
		[corrigendumOLId]        [int] IDENTITY(1, 1) NOT NULL,
		[tenderOfflineId]        [int] NOT NULL,
		[LotorPhaseIdentity]     [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[fieldName]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[oldValue]               [varchar](5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[newValue]               [varchar](5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CorrigendumStatus]      [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CorNo]                  [int] NULL,
		[Comments]               [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[userID]                 [int] NULL,
		[insertUpdateDate]       [smalldatetime] NULL,
		CONSTRAINT [corrigendumOLId_PK]
		PRIMARY KEY
		CLUSTERED
		([corrigendumOLId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CorrigendumDetailOffline] SET (LOCK_ESCALATION = TABLE)
GO
