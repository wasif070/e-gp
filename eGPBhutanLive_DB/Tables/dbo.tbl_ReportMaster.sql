SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ReportMaster] (
		[reportId]         [int] IDENTITY(1, 1) NOT NULL,
		[reportName]       [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[reportHeader]     [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[reportFooter]     [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[reportType]       [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderId]         [int] NOT NULL,
		[isTORTER]         [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [reportId_pk]
		PRIMARY KEY
		CLUSTERED
		([reportId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ReportMaster]
	WITH NOCHECK
	ADD CONSTRAINT [FK8AC1B7B798B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_ReportMaster]
	CHECK CONSTRAINT [FK8AC1B7B798B66EC5]

GO
ALTER TABLE [dbo].[tbl_ReportMaster]
	WITH NOCHECK
	ADD CONSTRAINT [rptTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ReportMaster]
	CHECK CONSTRAINT [rptTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_ReportMaster] SET (LOCK_ESCALATION = TABLE)
GO
