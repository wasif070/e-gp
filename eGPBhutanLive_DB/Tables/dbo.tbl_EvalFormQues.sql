SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalFormQues] (
		[evalQueId]             [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]              [int] NOT NULL,
		[pkgLotId]              [int] NOT NULL,
		[tenderFormId]          [int] NOT NULL,
		[userId]                [int] NOT NULL,
		[question]              [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[quePostDt]             [smalldatetime] NOT NULL,
		[quePostedBy]           [int] NOT NULL,
		[queSentByTec]          [int] NOT NULL,
		[answer]                [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ansDt]                 [smalldatetime] NULL,
		[evalStatus]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[evalDate]              [smalldatetime] NULL,
		[postedByGovUserId]     [int] NULL,
		[evalCount]             [int] NOT NULL,
		CONSTRAINT [evalQueId_PK]
		PRIMARY KEY
		CLUSTERED
		([evalQueId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalFormQues]
	ADD
	CONSTRAINT [DF__tbl_EvalF__evalC__1C0818FF]
	DEFAULT ('0') FOR [evalCount]
GO
ALTER TABLE [dbo].[tbl_EvalFormQues]
	ADD
	CONSTRAINT [DF_tbl_EvalFormQues_queSentByTec]
	DEFAULT ((0)) FOR [queSentByTec]
GO
ALTER TABLE [dbo].[tbl_EvalFormQues] SET (LOCK_ESCALATION = TABLE)
GO
