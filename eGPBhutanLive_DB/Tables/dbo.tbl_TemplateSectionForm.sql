SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TemplateSectionForm] (
		[formId]                [int] IDENTITY(1, 1) NOT NULL,
		[templateId]            [smallint] NOT NULL,
		[sectionId]             [int] NOT NULL,
		[filledBy]              [tinyint] NOT NULL,
		[formName]              [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[formHeader]            [varchar](5000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[formFooter]            [varchar](5000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[noOfTables]            [tinyint] NOT NULL,
		[isMultipleFilling]     [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isEncryption]          [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isPriceBid]            [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[status]                [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isMandatory]           [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[FormType]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [formId_PK]
		PRIMARY KEY
		CLUSTERED
		([formId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TemplateSectionForm]
	ADD
	CONSTRAINT [DF_tbl_TemplateSectionForm_isMandatory]
	DEFAULT ('Yes') FOR [isMandatory]
GO
ALTER TABLE [dbo].[tbl_TemplateSectionForm]
	WITH CHECK
	ADD CONSTRAINT [FK9491FA4E904D9C11]
	FOREIGN KEY ([templateId]) REFERENCES [dbo].[tbl_TemplateMaster] ([templateId])
ALTER TABLE [dbo].[tbl_TemplateSectionForm]
	CHECK CONSTRAINT [FK9491FA4E904D9C11]

GO
ALTER TABLE [dbo].[tbl_TemplateSectionForm]
	WITH CHECK
	ADD CONSTRAINT [FK9491FA4EEC995508]
	FOREIGN KEY ([sectionId]) REFERENCES [dbo].[tbl_TemplateSections] ([sectionId])
ALTER TABLE [dbo].[tbl_TemplateSectionForm]
	CHECK CONSTRAINT [FK9491FA4EEC995508]

GO
ALTER TABLE [dbo].[tbl_TemplateSectionForm]
	WITH CHECK
	ADD CONSTRAINT [templateform_FK2]
	FOREIGN KEY ([templateId]) REFERENCES [dbo].[tbl_TemplateMaster] ([templateId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TemplateSectionForm]
	CHECK CONSTRAINT [templateform_FK2]

GO
ALTER TABLE [dbo].[tbl_TemplateSectionForm]
	WITH CHECK
	ADD CONSTRAINT [templatesectionform_FK1]
	FOREIGN KEY ([sectionId]) REFERENCES [dbo].[tbl_TemplateSections] ([sectionId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TemplateSectionForm]
	CHECK CONSTRAINT [templatesectionform_FK1]

GO
ALTER TABLE [dbo].[tbl_TemplateSectionForm] SET (LOCK_ESCALATION = TABLE)
GO
