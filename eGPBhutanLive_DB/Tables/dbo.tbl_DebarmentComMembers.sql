SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_DebarmentComMembers] (
		[debarMemberId]        [int] IDENTITY(1, 1) NOT NULL,
		[debarCommitteeId]     [int] NOT NULL,
		[userId]               [int] NOT NULL,
		[memberType]           [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_DebarmentComMembers]
		PRIMARY KEY
		CLUSTERED
		([debarMemberId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_DebarmentComMembers]
	WITH CHECK
	ADD CONSTRAINT [debarCommitteId_FK1]
	FOREIGN KEY ([debarCommitteeId]) REFERENCES [dbo].[tbl_DebarmentCommittee] ([debarCommitteeId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_DebarmentComMembers]
	CHECK CONSTRAINT [debarCommitteId_FK1]

GO
ALTER TABLE [dbo].[tbl_DebarmentComMembers]
	WITH CHECK
	ADD CONSTRAINT [FK9B53EA77B9E24B3]
	FOREIGN KEY ([debarCommitteeId]) REFERENCES [dbo].[tbl_DebarmentCommittee] ([debarCommitteeId])
ALTER TABLE [dbo].[tbl_DebarmentComMembers]
	CHECK CONSTRAINT [FK9B53EA77B9E24B3]

GO
ALTER TABLE [dbo].[tbl_DebarmentComMembers] SET (LOCK_ESCALATION = TABLE)
GO
