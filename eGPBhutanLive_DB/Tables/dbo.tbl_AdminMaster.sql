SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_AdminMaster] (
		[adminId]        [smallint] IDENTITY(1, 1) NOT NULL,
		[userId]         [int] NOT NULL,
		[fullName]       [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[nationalId]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[mobileNo]       [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[phoneNo]        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[rollId]         [int] NULL,
		CONSTRAINT [adminId_PK]
		PRIMARY KEY
		CLUSTERED
		([adminId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_AdminMaster]
	WITH CHECK
	ADD CONSTRAINT [FKE3A63B706174DBD1]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
ALTER TABLE [dbo].[tbl_AdminMaster]
	CHECK CONSTRAINT [FKE3A63B706174DBD1]

GO
ALTER TABLE [dbo].[tbl_AdminMaster]
	WITH CHECK
	ADD CONSTRAINT [userId_FK]
	FOREIGN KEY ([userId]) REFERENCES [dbo].[tbl_LoginMaster] ([userId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_AdminMaster]
	CHECK CONSTRAINT [userId_FK]

GO
ALTER TABLE [dbo].[tbl_AdminMaster] SET (LOCK_ESCALATION = TABLE)
GO
