SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_RoleMaster] (
		[rollId]       [int] IDENTITY(1, 1) NOT NULL,
		[rollName]     [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[status]       [int] NOT NULL,
		CONSTRAINT [PK_tbl_RoleMaster]
		PRIMARY KEY
		CLUSTERED
		([rollId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_RoleMaster] SET (LOCK_ESCALATION = TABLE)
GO
