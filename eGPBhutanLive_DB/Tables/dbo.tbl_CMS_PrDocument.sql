SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_PrDocument] (
		[prDocId]            [int] IDENTITY(1, 1) NOT NULL,
		[prRepId]            [int] NOT NULL,
		[documentName]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]     [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedBy]         [int] NOT NULL,
		[uploadedDate]       [datetime] NOT NULL,
		CONSTRAINT [PK_tbl_CMS_PrDocument]
		PRIMARY KEY
		CLUSTERED
		([prDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_PrDocument]
	WITH CHECK
	ADD CONSTRAINT [FK_prRepId_PK_PrMaster]
	FOREIGN KEY ([prRepId]) REFERENCES [dbo].[tbl_CMS_PrMaster] ([progressRepId])
ALTER TABLE [dbo].[tbl_CMS_PrDocument]
	CHECK CONSTRAINT [FK_prRepId_PK_PrMaster]

GO
ALTER TABLE [dbo].[tbl_CMS_PrDocument] SET (LOCK_ESCALATION = TABLE)
GO
