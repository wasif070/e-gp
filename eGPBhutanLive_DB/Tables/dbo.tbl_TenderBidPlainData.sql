SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderBidPlainData] (
		[bidPlainId]        [int] IDENTITY(1, 1) NOT NULL,
		[bidTableId]        [int] NOT NULL,
		[tenderCelId]       [int] NOT NULL,
		[tenderColId]       [int] NOT NULL,
		[tenderTableId]     [int] NOT NULL,
		[cellValue]         [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[rowId]             [int] NOT NULL,
		[tenderFormId]      [int] NULL,
		[cellId]            [int] NULL,
		[submissionDt]      [datetime] NULL,
		CONSTRAINT [bidPlainId_PK]
		PRIMARY KEY
		CLUSTERED
		([bidPlainId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderBidPlainData]
	WITH CHECK
	ADD CONSTRAINT [FKE509DF4CB5C96B11]
	FOREIGN KEY ([bidTableId]) REFERENCES [dbo].[tbl_TenderBidTable] ([bidTableId])
ALTER TABLE [dbo].[tbl_TenderBidPlainData]
	CHECK CONSTRAINT [FKE509DF4CB5C96B11]

GO
ALTER TABLE [dbo].[tbl_TenderBidPlainData]
	WITH CHECK
	ADD CONSTRAINT [plainBidTableId_FK1]
	FOREIGN KEY ([bidTableId]) REFERENCES [dbo].[tbl_TenderBidTable] ([bidTableId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderBidPlainData]
	CHECK CONSTRAINT [plainBidTableId_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderBidPlainData] SET (LOCK_ESCALATION = TABLE)
GO
