SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_CMS_InvRemarks] (
		[invRemarksId]     [int] IDENTITY(1, 1) NOT NULL,
		[invoiceId]        [int] NOT NULL,
		[remarks]          [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdDt]        [datetime] NOT NULL,
		[createdBy]        [int] NOT NULL,
		CONSTRAINT [PK_tbl_CMS_InvRemarks]
		PRIMARY KEY
		CLUSTERED
		([invRemarksId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_InvRemarks]
	WITH CHECK
	ADD CONSTRAINT [FK_InvoiceId_PK_InvoiceMaster]
	FOREIGN KEY ([invoiceId]) REFERENCES [dbo].[tbl_CMS_InvoiceMaster] ([invoiceId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_InvRemarks]
	CHECK CONSTRAINT [FK_InvoiceId_PK_InvoiceMaster]

GO
ALTER TABLE [dbo].[tbl_CMS_InvRemarks] SET (LOCK_ESCALATION = TABLE)
GO
