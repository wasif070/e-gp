SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderDebriefing] (
		[debriefId]       [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]        [int] NOT NULL,
		[quePostedBy]     [int] NOT NULL,
		[postDate]        [datetime] NOT NULL,
		[answeredBy]      [int] NOT NULL,
		[answeredDt]      [datetime] NULL,
		[queryText]       [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[replyText]       [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_TenderDebriefing]
		PRIMARY KEY
		CLUSTERED
		([debriefId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderDebriefing]
	WITH NOCHECK
	ADD CONSTRAINT [FK221911E98B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_TenderDebriefing]
	CHECK CONSTRAINT [FK221911E98B66EC5]

GO
ALTER TABLE [dbo].[tbl_TenderDebriefing]
	WITH NOCHECK
	ADD CONSTRAINT [tenderDeBriefing_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderDebriefing]
	CHECK CONSTRAINT [tenderDeBriefing_FK1]

GO
ALTER TABLE [dbo].[tbl_TenderDebriefing] SET (LOCK_ESCALATION = TABLE)
GO
