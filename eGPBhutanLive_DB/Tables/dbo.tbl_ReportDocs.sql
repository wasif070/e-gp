SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ReportDocs] (
		[reportDocId]        [int] IDENTITY(1, 1) NOT NULL,
		[documentName]       [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]       [smalldatetime] NOT NULL,
		[userId]             [int] NOT NULL,
		[status]             [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_ReportDocs]
		PRIMARY KEY
		CLUSTERED
		([reportDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ReportDocs] SET (LOCK_ESCALATION = TABLE)
GO
