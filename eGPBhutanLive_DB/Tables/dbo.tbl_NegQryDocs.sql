SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_NegQryDocs] (
		[negQueryDocId]      [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]           [int] NOT NULL,
		[userId]             [int] NOT NULL,
		[negQueryId]         [int] NOT NULL,
		[documentName]       [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docDescription]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[docSize]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[uploadedDate]       [smalldatetime] NOT NULL,
		[uploadedBy]         [int] NOT NULL,
		[docType]            [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[negId]              [int] NOT NULL,
		CONSTRAINT [negQueryDocId_Pk]
		PRIMARY KEY
		CLUSTERED
		([negQueryDocId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_NegQryDocs] SET (LOCK_ESCALATION = TABLE)
GO
