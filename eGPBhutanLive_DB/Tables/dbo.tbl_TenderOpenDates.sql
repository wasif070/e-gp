SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_TenderOpenDates] (
		[tenderOpenDtId]     [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]           [int] NOT NULL,
		[openingDt]          [smalldatetime] NOT NULL,
		[entryDt]            [smalldatetime] NOT NULL,
		[envelopeId]         [smallint] NOT NULL,
		CONSTRAINT [PK_tbl_TenderOpenDates]
		PRIMARY KEY
		CLUSTERED
		([tenderOpenDtId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderOpenDates] SET (LOCK_ESCALATION = TABLE)
GO
