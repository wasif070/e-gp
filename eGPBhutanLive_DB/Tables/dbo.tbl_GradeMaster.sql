SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_GradeMaster] (
		[gradeId]        [smallint] IDENTITY(1, 1) NOT NULL,
		[grade]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[gradeLevel]     [tinyint] NOT NULL,
		CONSTRAINT [gradeId_PK]
		PRIMARY KEY
		CLUSTERED
		([gradeId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_GradeMaster] SET (LOCK_ESCALATION = TABLE)
GO
