SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CurrencyMaster] (
		[currencyId]            [int] IDENTITY(1, 1) NOT NULL,
		[currencyName]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[currencySymbol]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[currencyShortName]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [currencyId_PK]
		PRIMARY KEY
		CLUSTERED
		([currencyId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CurrencyMaster] SET (LOCK_ESCALATION = TABLE)
GO
