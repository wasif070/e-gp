SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_NoaAcceptance] (
		[noaAcceptId]         [int] IDENTITY(1, 1) NOT NULL,
		[noaIssueId]          [int] NOT NULL,
		[comments]            [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[userId]              [int] NOT NULL,
		[acceptRejDt]         [datetime] NULL,
		[acceptRejStatus]     [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[acceptRejType]       [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[accountTitle]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[bankName]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[branchName]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[accountNo]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[address]             [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[telephone]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[fax]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[email]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[swiftCode]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [noaAcceptId_PK]
		PRIMARY KEY
		CLUSTERED
		([noaAcceptId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_NoaAcceptance]
	WITH CHECK
	ADD CONSTRAINT [FK3AFE2656866FC17D]
	FOREIGN KEY ([noaIssueId]) REFERENCES [dbo].[tbl_NoaIssueDetails] ([noaIssueId])
ALTER TABLE [dbo].[tbl_NoaAcceptance]
	CHECK CONSTRAINT [FK3AFE2656866FC17D]

GO
ALTER TABLE [dbo].[tbl_NoaAcceptance]
	WITH CHECK
	ADD CONSTRAINT [noaAcceptNoaIssueId_Fk1]
	FOREIGN KEY ([noaIssueId]) REFERENCES [dbo].[tbl_NoaIssueDetails] ([noaIssueId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_NoaAcceptance]
	CHECK CONSTRAINT [noaAcceptNoaIssueId_Fk1]

GO
ALTER TABLE [dbo].[tbl_NoaAcceptance] SET (LOCK_ESCALATION = TABLE)
GO
