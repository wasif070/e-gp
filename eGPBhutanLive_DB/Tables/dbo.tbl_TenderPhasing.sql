SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderPhasing] (
		[tenderPhasingId]      [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]             [int] NOT NULL,
		[phasingRefNo]         [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[phasingOfService]     [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[location]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[indStartDt]           [date] NOT NULL,
		[indEndDt]             [date] NOT NULL,
		CONSTRAINT [PK_tbk_TenderPhasing]
		PRIMARY KEY
		CLUSTERED
		([tenderPhasingId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderPhasing]
	WITH NOCHECK
	ADD CONSTRAINT [FK_tbk_TenderPhasing_tbl_TenderMaster]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderPhasing]
	CHECK CONSTRAINT [FK_tbk_TenderPhasing_tbl_TenderMaster]

GO
ALTER TABLE [dbo].[tbl_TenderPhasing]
	WITH NOCHECK
	ADD CONSTRAINT [FK53F49F2398B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_TenderPhasing]
	CHECK CONSTRAINT [FK53F49F2398B66EC5]

GO
ALTER TABLE [dbo].[tbl_TenderPhasing] SET (LOCK_ESCALATION = TABLE)
GO
