SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_CMS_SrvStaffDaysCnt] (
		[srvRptId]           [int] IDENTITY(1, 1) NOT NULL,
		[srvTCId]            [int] NOT NULL,
		[onSiteCnt]          [int] NOT NULL,
		[offSiteCnt]         [int] NOT NULL,
		[totalCnt]           [int] NOT NULL,
		[tenderId]           [int] NOT NULL,
		[lotId]              [int] NOT NULL,
		[usedOnSiteCnt]      [int] NOT NULL,
		[usedOffSiteCnt]     [int] NOT NULL,
		CONSTRAINT [PK_tbl_CMS_SrvStaffDaysCnt]
		PRIMARY KEY
		CLUSTERED
		([srvRptId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvStaffDaysCnt]
	ADD
	CONSTRAINT [DF_tbl_CMS_SrvStaffDaysCnt_usedOffSiteCnt]
	DEFAULT ((0)) FOR [usedOffSiteCnt]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvStaffDaysCnt]
	ADD
	CONSTRAINT [DF_tbl_CMS_SrvStaffDaysCnt_usedOnSiteCont]
	DEFAULT ((0)) FOR [usedOnSiteCnt]
GO
ALTER TABLE [dbo].[tbl_CMS_SrvStaffDaysCnt] SET (LOCK_ESCALATION = TABLE)
GO
