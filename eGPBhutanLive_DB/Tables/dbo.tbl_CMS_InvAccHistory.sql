SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_InvAccHistory] (
		[invAccHistId]         [int] IDENTITY(1, 1) NOT NULL,
		[invoiceAccDtlId]      [int] NOT NULL,
		[invoiceId]            [int] NOT NULL,
		[advAdjAmt]            [decimal](15, 3) NULL,
		[retentionMDeduct]     [decimal](15, 3) NULL,
		[retentionMAdd]        [decimal](15, 3) NULL,
		[vatAmt]               [decimal](15, 3) NULL,
		[aitAmt]               [decimal](15, 3) NULL,
		[advVatAmt]            [decimal](15, 3) NULL,
		[advAitAmt]            [decimal](15, 3) NULL,
		[bonusAmt]             [decimal](15, 3) NULL,
		[penaltyAmt]           [decimal](15, 3) NULL,
		[ldAmt]                [decimal](15, 3) NULL,
		[grossAmt]             [decimal](15, 3) NULL,
		[generatedDt]          [datetime] NOT NULL,
		[modeOfPayment]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[dateOfPayment]        [datetime] NOT NULL,
		[bankName]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[branchName]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[instrumentNo]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[editedWhen]           [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[advAmt]               [decimal](15, 3) NOT NULL,
		[aitBankName]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[aitBranchName]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[aitINstrumentNo]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[interestonDP]         [decimal](15, 3) NULL,
		[aitDateOfPayment]     [datetime] NULL,
		[aitModeOfPayment]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[vatBankName]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[vatBranchName]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[vatINstrumentNo]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[vatDateOfPayment]     [datetime] NULL,
		[vatModeOfPayment]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_CMS_InvAccHistory]
		PRIMARY KEY
		CLUSTERED
		([invAccHistId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_CMS_InvAccHistory]
	WITH CHECK
	ADD CONSTRAINT [FK_invAccDtlId_PK_InvoiceAccDetails]
	FOREIGN KEY ([invoiceAccDtlId]) REFERENCES [dbo].[tbl_CMS_InvoiceAccDetails] ([invoiceAccDtlId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_CMS_InvAccHistory]
	CHECK CONSTRAINT [FK_invAccDtlId_PK_InvoiceAccDetails]

GO
ALTER TABLE [dbo].[tbl_CMS_InvAccHistory] SET (LOCK_ESCALATION = TABLE)
GO
