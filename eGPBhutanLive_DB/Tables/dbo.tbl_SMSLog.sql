SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_SMSLog] (
		[smsLogId]         [int] IDENTITY(1, 1) NOT NULL,
		[mobileNo]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[sentDate]         [datetime] NOT NULL,
		[smstext]          [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[exceptionLog]     [varchar](5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_SMSLog]
		PRIMARY KEY
		CLUSTERED
		([smsLogId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_SMSLog] SET (LOCK_ESCALATION = TABLE)
GO
