SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalRoundMaster] (
		[roundId]        [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]       [int] NOT NULL,
		[pkgLotId]       [int] NOT NULL,
		[reportType]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[reportId]       [int] NOT NULL,
		[userId]         [int] NOT NULL,
		[createdDt]      [datetime] NOT NULL,
		[createdBy]      [int] NOT NULL,
		[romId]          [int] NULL,
		[evalCount]      [int] NOT NULL,
		CONSTRAINT [PK_tbl_EvalRoundMaster]
		PRIMARY KEY
		CLUSTERED
		([roundId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalRoundMaster]
	ADD
	CONSTRAINT [DF__tbl_EvalR__evalC__7F80E8EA]
	DEFAULT ('0') FOR [evalCount]
GO
ALTER TABLE [dbo].[tbl_EvalRoundMaster]
	ADD
	CONSTRAINT [DF_tbl_EvalRoundMaster_reportType]
	DEFAULT ('L1') FOR [reportType]
GO
ALTER TABLE [dbo].[tbl_EvalRoundMaster]
	WITH NOCHECK
	ADD CONSTRAINT [tenderRoundMaster_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EvalRoundMaster]
	CHECK CONSTRAINT [tenderRoundMaster_FK1]

GO
ALTER TABLE [dbo].[tbl_EvalRoundMaster] SET (LOCK_ESCALATION = TABLE)
GO
