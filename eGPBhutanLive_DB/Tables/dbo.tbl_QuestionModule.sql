SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_QuestionModule] (
		[moduleId]       [int] IDENTITY(1, 1) NOT NULL,
		[moduleName]     [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_QuestionModule]
		PRIMARY KEY
		CLUSTERED
		([moduleId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_QuestionModule] SET (LOCK_ESCALATION = TABLE)
GO
