SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_CMS_ComplaintTypes] (
		[complaintTypeId]     [tinyint] IDENTITY(1, 1) NOT NULL,
		[complaintType]       [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_CMS_ComplaintTypes]
		PRIMARY KEY
		CLUSTERED
		([complaintTypeId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Type Of Complaint information', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintTypes', 'COLUMN', N'complaintType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'System Generated Id stores here ', 'SCHEMA', N'dbo', 'TABLE', N'tbl_CMS_ComplaintTypes', 'COLUMN', N'complaintTypeId'
GO
ALTER TABLE [dbo].[tbl_CMS_ComplaintTypes] SET (LOCK_ESCALATION = TABLE)
GO
