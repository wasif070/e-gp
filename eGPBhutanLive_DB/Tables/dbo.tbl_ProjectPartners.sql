SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_ProjectPartners] (
		[projectPartnerId]     [int] IDENTITY(1, 1) NOT NULL,
		[sBankDevelopId]       [int] NOT NULL,
		[projectId]            [int] NOT NULL,
		CONSTRAINT [PK_tbl_ProjectPartners]
		PRIMARY KEY
		CLUSTERED
		([projectPartnerId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ProjectPartners]
	WITH CHECK
	ADD CONSTRAINT [FK21E7D44380EAD74F]
	FOREIGN KEY ([projectId]) REFERENCES [dbo].[tbl_ProjectMaster] ([projectId])
ALTER TABLE [dbo].[tbl_ProjectPartners]
	CHECK CONSTRAINT [FK21E7D44380EAD74F]

GO
ALTER TABLE [dbo].[tbl_ProjectPartners]
	WITH CHECK
	ADD CONSTRAINT [FK21E7D443815B516A]
	FOREIGN KEY ([sBankDevelopId]) REFERENCES [dbo].[tbl_ScBankDevPartnerMaster] ([sBankDevelopId])
ALTER TABLE [dbo].[tbl_ProjectPartners]
	CHECK CONSTRAINT [FK21E7D443815B516A]

GO
ALTER TABLE [dbo].[tbl_ProjectPartners]
	WITH CHECK
	ADD CONSTRAINT [partnerProjrctId]
	FOREIGN KEY ([projectId]) REFERENCES [dbo].[tbl_ProjectMaster] ([projectId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ProjectPartners]
	CHECK CONSTRAINT [partnerProjrctId]

GO
ALTER TABLE [dbo].[tbl_ProjectPartners]
	WITH CHECK
	ADD CONSTRAINT [projectsBankDevelopId_FK1]
	FOREIGN KEY ([sBankDevelopId]) REFERENCES [dbo].[tbl_ScBankDevPartnerMaster] ([sBankDevelopId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ProjectPartners]
	CHECK CONSTRAINT [projectsBankDevelopId_FK1]

GO
ALTER TABLE [dbo].[tbl_ProjectPartners] SET (LOCK_ESCALATION = TABLE)
GO
