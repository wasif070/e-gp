SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TenderBidDetail] (
		[bidDetailId]       [int] IDENTITY(1, 1) NOT NULL,
		[bidTableId]        [int] NOT NULL,
		[columnId]          [int] NOT NULL,
		[tenderTableId]     [int] NOT NULL,
		[cellValue]         [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[rowId]             [int] NOT NULL,
		[cellId]            [int] NOT NULL,
		[celId]             [int] NULL,
		[tenderCelId]       [int] NULL,
		[tenderColId]       [int] NULL,
		CONSTRAINT [bidDetailId_PK]
		PRIMARY KEY
		CLUSTERED
		([bidDetailId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_TenderBidDetail]
	WITH CHECK
	ADD CONSTRAINT [FK_tbl_TenderBidDetail_tbl_TenderBidTable]
	FOREIGN KEY ([bidTableId]) REFERENCES [dbo].[tbl_TenderBidTable] ([bidTableId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_TenderBidDetail]
	CHECK CONSTRAINT [FK_tbl_TenderBidDetail_tbl_TenderBidTable]

GO
ALTER TABLE [dbo].[tbl_TenderBidDetail]
	WITH CHECK
	ADD CONSTRAINT [FKC3B5BC99B5C96B11]
	FOREIGN KEY ([bidTableId]) REFERENCES [dbo].[tbl_TenderBidTable] ([bidTableId])
ALTER TABLE [dbo].[tbl_TenderBidDetail]
	CHECK CONSTRAINT [FKC3B5BC99B5C96B11]

GO
ALTER TABLE [dbo].[tbl_TenderBidDetail] SET (LOCK_ESCALATION = TABLE)
GO
