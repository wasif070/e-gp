SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ListBoxDetail] (
		[listDetailId]     [int] IDENTITY(1, 1) NOT NULL,
		[listBoxId]        [int] NOT NULL,
		[itemId]           [smallint] NOT NULL,
		[itemValue]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[itemText]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isDefault]        [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [listDetailId_PK]
		PRIMARY KEY
		CLUSTERED
		([listDetailId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ListBoxDetail]
	WITH CHECK
	ADD CONSTRAINT [FKB5765C1DB9654DD7]
	FOREIGN KEY ([listBoxId]) REFERENCES [dbo].[tbl_ListBoxMaster] ([listBoxId])
ALTER TABLE [dbo].[tbl_ListBoxDetail]
	CHECK CONSTRAINT [FKB5765C1DB9654DD7]

GO
ALTER TABLE [dbo].[tbl_ListBoxDetail]
	WITH CHECK
	ADD CONSTRAINT [listboxMaster_FK1]
	FOREIGN KEY ([listBoxId]) REFERENCES [dbo].[tbl_ListBoxMaster] ([listBoxId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_ListBoxDetail]
	CHECK CONSTRAINT [listboxMaster_FK1]

GO
ALTER TABLE [dbo].[tbl_ListBoxDetail] SET (LOCK_ESCALATION = TABLE)
GO
