SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_NoaIssueDetails] (
		[noaIssueId]             [int] IDENTITY(1, 1) NOT NULL,
		[tenderId]               [int] NOT NULL,
		[pkgLotId]               [int] NOT NULL,
		[contractNo]             [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[contractDt]             [datetime] NOT NULL,
		[userId]                 [int] NOT NULL,
		[contractAmt]            [numeric](20, 3) NOT NULL,
		[contractAmtWords]       [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[noaIssueDays]           [smallint] NOT NULL,
		[noaAcceptDt]            [datetime] NOT NULL,
		[perfSecAmt]             [money] NOT NULL,
		[perfSecAmtWords]        [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[perSecSubDays]          [smallint] NOT NULL,
		[perSecSubDt]            [datetime] NULL,
		[contractSignDays]       [smallint] NOT NULL,
		[contractSignDt]         [datetime] NOT NULL,
		[createdBy]              [int] NOT NULL,
		[createdDt]              [datetime] NOT NULL,
		[contractName]           [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[clauseRef]              [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[companyName]            [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[rank]                   [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[roundId]                [int] NOT NULL,
		[advanceContractAmt]     [decimal](15, 3) NULL,
		[isRepeatOrder]          [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[currencyId]             [int] NULL,
		[salvageAmt]             [numeric](20, 3) NULL,
		CONSTRAINT [noaIssueId_PK]
		PRIMARY KEY
		CLUSTERED
		([noaIssueId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_NoaIssueDetails]
	ADD
	CONSTRAINT [DF_tbl_NoaIssueDetails_isRepeatOrder]
	DEFAULT ('no') FOR [isRepeatOrder]
GO
ALTER TABLE [dbo].[tbl_NoaIssueDetails]
	ADD
	CONSTRAINT [DF_tbl_NoaIssueDetails_rank]
	DEFAULT ('1') FOR [rank]
GO
ALTER TABLE [dbo].[tbl_NoaIssueDetails]
	ADD
	CONSTRAINT [DF_tbl_NoaIssueDetails_roundId]
	DEFAULT ((1)) FOR [roundId]
GO
ALTER TABLE [dbo].[tbl_NoaIssueDetails]
	WITH NOCHECK
	ADD CONSTRAINT [FK2E135B6898B66EC5]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_NoaIssueDetails]
	CHECK CONSTRAINT [FK2E135B6898B66EC5]

GO
ALTER TABLE [dbo].[tbl_NoaIssueDetails]
	WITH NOCHECK
	ADD CONSTRAINT [noaDetTenderId_FK1]
	FOREIGN KEY ([tenderId]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_NoaIssueDetails]
	CHECK CONSTRAINT [noaDetTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_NoaIssueDetails] SET (LOCK_ESCALATION = TABLE)
GO
