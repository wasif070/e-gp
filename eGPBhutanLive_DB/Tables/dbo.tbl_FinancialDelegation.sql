SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_FinancialDelegation] (
		[FinancialDeligationId]     [int] IDENTITY(1, 1) NOT NULL,
		[BudgetType]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[TenderType]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ProcurementMethod]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ProcurementType]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ProcurementNature]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[TenderEmergency]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[MinValueBDT]               [numeric](19, 2) NOT NULL,
		[MaxValueBDT]               [numeric](19, 2) NOT NULL,
		[ApprovingAuthority]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[MinProjectValueBDT]        [numeric](19, 2) NULL,
		[MaxProjectValueBDT]        [numeric](19, 2) NULL,
		[IsBoD]                     [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsCorporation]             [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[PEOfficeLevel]             [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_FinancialDelegation]
		PRIMARY KEY
		CLUSTERED
		([FinancialDeligationId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_FinancialDelegation] SET (LOCK_ESCALATION = TABLE)
GO
