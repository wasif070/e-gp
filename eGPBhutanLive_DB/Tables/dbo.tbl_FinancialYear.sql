SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_FinancialYear] (
		[financialYearId]     [int] IDENTITY(1, 1) NOT NULL,
		[financialYear]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isCurrent]           [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_FinancialYear]
		PRIMARY KEY
		CLUSTERED
		([financialYearId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_FinancialYear] SET (LOCK_ESCALATION = TABLE)
GO
