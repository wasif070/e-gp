SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_DesignationMaster] (
		[designationId]       [int] IDENTITY(1, 1) NOT NULL,
		[designationName]     [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[gradeId]             [smallint] NOT NULL,
		[departmentid]        [smallint] NOT NULL,
		[createdBy]           [int] NULL,
		[createdDate]         [smalldatetime] NULL,
		CONSTRAINT [PK_tbl_DesignationMaster]
		PRIMARY KEY
		CLUSTERED
		([designationId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_DesignationMaster]
	WITH CHECK
	ADD CONSTRAINT [DepartmentMaster_fk3]
	FOREIGN KEY ([departmentid]) REFERENCES [dbo].[tbl_DepartmentMaster] ([departmentId])
ALTER TABLE [dbo].[tbl_DesignationMaster]
	CHECK CONSTRAINT [DepartmentMaster_fk3]

GO
ALTER TABLE [dbo].[tbl_DesignationMaster]
	WITH CHECK
	ADD CONSTRAINT [designationGrade_FK2]
	FOREIGN KEY ([gradeId]) REFERENCES [dbo].[tbl_GradeMaster] ([gradeId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_DesignationMaster]
	CHECK CONSTRAINT [designationGrade_FK2]

GO
ALTER TABLE [dbo].[tbl_DesignationMaster]
	WITH CHECK
	ADD CONSTRAINT [FK40A96D83B4622CB]
	FOREIGN KEY ([gradeId]) REFERENCES [dbo].[tbl_GradeMaster] ([gradeId])
ALTER TABLE [dbo].[tbl_DesignationMaster]
	CHECK CONSTRAINT [FK40A96D83B4622CB]

GO
ALTER TABLE [dbo].[tbl_DesignationMaster]
	WITH CHECK
	ADD CONSTRAINT [FK40A96D8E4B351C1]
	FOREIGN KEY ([departmentid]) REFERENCES [dbo].[tbl_DepartmentMaster] ([departmentId])
ALTER TABLE [dbo].[tbl_DesignationMaster]
	CHECK CONSTRAINT [FK40A96D8E4B351C1]

GO
ALTER TABLE [dbo].[tbl_DesignationMaster] SET (LOCK_ESCALATION = TABLE)
GO
