SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_RegFeePayment] (
		[regPaymentId]          [int] IDENTITY(1, 1) NOT NULL,
		[paymentFor]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[userId]                [int] NOT NULL,
		[createdBy]             [int] NOT NULL,
		[bankName]              [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[branchName]            [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[currency]              [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[amount]                [money] NOT NULL,
		[paymentInstType]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[instRefNumber]         [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[issuanceBank]          [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[issuanceBranch]        [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[issuanceDate]          [smalldatetime] NOT NULL,
		[instValidityDt]        [smalldatetime] NOT NULL,
		[dtOfPayment]           [smalldatetime] NOT NULL,
		[comments]              [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[paymentMode]           [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[eSignature]            [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[status]                [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isVerified]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[isLive]                [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[dtOfAction]            [smalldatetime] NULL,
		[validityPeriodRef]     [int] NULL,
		[partTransId]           [int] NULL,
		[onlineTransId]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_regPayment]
		PRIMARY KEY
		CLUSTERED
		([regPaymentId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_RegFeePayment]
	ADD
	CONSTRAINT [DF_tbl_RegFeePayment_currency]
	DEFAULT ('BDT') FOR [currency]
GO
ALTER TABLE [dbo].[tbl_RegFeePayment]
	ADD
	CONSTRAINT [DF_tbl_RegFeePayment_isLive]
	DEFAULT ('yes') FOR [isLive]
GO
ALTER TABLE [dbo].[tbl_RegFeePayment]
	ADD
	CONSTRAINT [DF_tbl_RegFeePayment_isVerified]
	DEFAULT ('no') FOR [isVerified]
GO
ALTER TABLE [dbo].[tbl_RegFeePayment] SET (LOCK_ESCALATION = TABLE)
GO
