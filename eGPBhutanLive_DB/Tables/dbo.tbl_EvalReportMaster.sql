SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_EvalReportMaster] (
		[evalRptId]         [int] IDENTITY(1, 1) NOT NULL,
		[evalRptName]       [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]         [int] NOT NULL,
		[createdDate]       [smalldatetime] NOT NULL,
		[evalRptStatus]     [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[tenderid]          [int] NOT NULL,
		CONSTRAINT [PK_tbl_EvalReportMaster]
		PRIMARY KEY
		CLUSTERED
		([evalRptId])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_EvalReportMaster]
	ADD
	CONSTRAINT [DF_tbl_EvalReportMaster_evalRptStatus]
	DEFAULT ('Pending') FOR [evalRptStatus]
GO
ALTER TABLE [dbo].[tbl_EvalReportMaster]
	ADD
	CONSTRAINT [DF_tbl_EvalReportMaster_tenderid]
	DEFAULT ((0)) FOR [tenderid]
GO
ALTER TABLE [dbo].[tbl_EvalReportMaster]
	WITH NOCHECK
	ADD CONSTRAINT [FK1B6149F398B66EC5]
	FOREIGN KEY ([tenderid]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ALTER TABLE [dbo].[tbl_EvalReportMaster]
	CHECK CONSTRAINT [FK1B6149F398B66EC5]

GO
ALTER TABLE [dbo].[tbl_EvalReportMaster]
	WITH NOCHECK
	ADD CONSTRAINT [rptMasterTenderId_FK1]
	FOREIGN KEY ([tenderid]) REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
	ON DELETE CASCADE
	ON UPDATE CASCADE
ALTER TABLE [dbo].[tbl_EvalReportMaster]
	CHECK CONSTRAINT [rptMasterTenderId_FK1]

GO
ALTER TABLE [dbo].[tbl_EvalReportMaster] SET (LOCK_ESCALATION = TABLE)
GO
