SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_MediaContent] (
		[mediaId]         [int] IDENTITY(1, 1) NOT NULL,
		[contentText]     [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[contentType]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[createdBy]       [int] NOT NULL,
		[createdDate]     [datetime] NOT NULL,
		[contentSub]      [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_tbl_MediaCotent]
		PRIMARY KEY
		CLUSTERED
		([mediaId])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_MediaContent] SET (LOCK_ESCALATION = TABLE)
GO
