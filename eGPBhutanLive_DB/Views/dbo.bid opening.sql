SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[bid opening]
AS
SELECT        dbo.tbl_TOSListing.ListingId
FROM            dbo.tbl_TosRptShare INNER JOIN
                         dbo.tbl_TOSListing ON dbo.tbl_TosRptShare.listingId = dbo.tbl_TOSListing.ListingId INNER JOIN
                         dbo.tbl_TenderMaster ON dbo.tbl_TosRptShare.tenderId = dbo.tbl_TenderMaster.tenderId INNER JOIN
                         dbo.tbl_TosSheetSign ON dbo.tbl_TenderMaster.tenderId = dbo.tbl_TosSheetSign.tenderId INNER JOIN
                         dbo.tbl_TenderOpenDates ON dbo.tbl_TenderMaster.tenderId = dbo.tbl_TenderOpenDates.tenderId INNER JOIN
                         dbo.tbl_TenderOpenExt ON dbo.tbl_TenderMaster.tenderId = dbo.tbl_TenderOpenExt.tenderId AND 
                         dbo.tbl_TenderMaster.tenderId = dbo.tbl_TenderOpenExt.tenderId INNER JOIN
                         dbo.tbl_TenderClose ON dbo.tbl_TenderMaster.tenderId = dbo.tbl_TenderClose.tenderId INNER JOIN
                         dbo.tbl_TosDocs ON dbo.tbl_TenderMaster.tenderId = dbo.tbl_TosDocs.tenderId AND dbo.tbl_TenderMaster.tenderId = dbo.tbl_TosDocs.tenderId INNER JOIN
                         dbo.tbl_TORRptSign ON dbo.tbl_TenderMaster.tenderId = dbo.tbl_TORRptSign.tenderId



GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[88] 4[4] 2[4] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbl_TosRptShare"
            Begin Extent = 
               Top = 10
               Left = 12
               Bottom = 234
               Right = 191
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_TosSheetSign"
            Begin Extent = 
               Top = 261
               Left = 14
               Bottom = 457
               Right = 184
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_TOSListing"
            Begin Extent = 
               Top = 11
               Left = 250
               Bottom = 221
               Right = 429
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_TenderMaster"
            Begin Extent = 
               Top = 260
               Left = 232
               Bottom = 450
               Right = 410
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_TenderOpenDates"
            Begin Extent = 
               Top = 7
               Left = 482
               Bottom = 170
               Right = 674
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_TenderOpenExt"
            Begin Extent = 
               Top = 21
               Left = 870
               Bottom = 293
               Right = 1061
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_TenderClose"
            Begin Extent = 
               Top = 367
               Left = 878
               Bottom = 548
              ', 'SCHEMA', N'dbo', 'VIEW', N'bid opening', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N' Right = 1068
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_TosDocs"
            Begin Extent = 
               Top = 381
               Left = 450
               Bottom = 545
               Right = 626
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_TORRptSign"
            Begin Extent = 
               Top = 193
               Left = 687
               Bottom = 387
               Right = 866
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'bid opening', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'dbo', 'VIEW', N'bid opening', NULL, NULL
GO
