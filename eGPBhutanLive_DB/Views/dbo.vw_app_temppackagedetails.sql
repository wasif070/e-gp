SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_app_temppackagedetails]
AS
SELECT        dbo.tbl_AppMaster.financialYear, dbo.tbl_AppMaster.budgetType, dbo.tbl_AppMaster.projectName, dbo.tbl_TempAppPackages.procurementnature, 
                         dbo.tbl_TempAppPackages.servicesType, dbo.tbl_TempAppPackages.packageNo, dbo.tbl_TempAppPackages.packageDesc, 
                         dbo.tbl_TempAppPackages.allocateBudget, dbo.tbl_TempAppPackages.pkgEstCost, dbo.tbl_TempAppPackages.isPQRequired, 
                         dbo.tbl_TempAppPackages.reoiRfaRequired, dbo.tbl_TempAppPackages.procurementMethodId, dbo.tbl_TempAppPackages.procurementType, 
                         dbo.tbl_TempAppPackages.sourceOfFund, dbo.tbl_TempAppPqTenderDates.advtDt, dbo.tbl_TempAppPqTenderDates.subDt, 
                         dbo.tbl_TempAppPqTenderDates.tenderAdvertDt, dbo.tbl_TempAppPqTenderDates.tenderSubDt, dbo.tbl_TempAppPqTenderDates.tenderOpenDt, 
                         dbo.tbl_TempAppPqTenderDates.tenderNoaIssueDt, dbo.tbl_TempAppPqTenderDates.tenderContractSignDt, dbo.tbl_TempAppPqTenderDates.tenderContractCompDt, 
                         dbo.tbl_EmployeeMaster.employeeName AS PE, dbo.tbl_StateMaster.stateName AS district, dbo.tbl_TempAppPqTenderDates.techSubCmtRptDt, 
                         dbo.tbl_TempAppPqTenderDates.tenderEvalRptDt, dbo.tbl_TempAppPqTenderDates.tenderEvalRptAppDt, dbo.tbl_TempAppPqTenderDates.tenderContractAppDt, 
                         dbo.tbl_TempAppPqTenderDates.reoiReceiptDt, dbo.tbl_TempAppPqTenderDates.rfpTechEvalDt, dbo.tbl_TempAppPqTenderDates.rfpFinancialOpenDt, 
                         dbo.tbl_TempAppPqTenderDates.rfpNegCompDt, dbo.tbl_TempAppPqTenderDates.rfpContractAppDt, dbo.tbl_TempAppPqTenderDates.rfaAdvertDt, 
                         dbo.tbl_TempAppPqTenderDates.rfaReceiptDt, dbo.tbl_TempAppPqTenderDates.rfaEvalDt, dbo.tbl_TempAppPqTenderDates.rfaInterviewDt, 
                         dbo.tbl_TempAppPqTenderDates.rfaFinalSelDt, dbo.tbl_TempAppPqTenderDates.rfaEvalRptSubDt, dbo.tbl_TempAppPqTenderDates.rfaAppConsultantDt, 
                         dbo.tbl_TempAppPqTenderDates.actSubDt, dbo.tbl_TempAppPqTenderDates.actAdvtDt, dbo.tbl_TempAppPqTenderDates.actEvalRptDt, 
                         dbo.tbl_TempAppPqTenderDates.actAppLstDt, dbo.tbl_TempAppPqTenderDates.actTenderAdvertDt, dbo.tbl_TempAppPqTenderDates.actTenderSubDt, 
                         dbo.tbl_TempAppPqTenderDates.actTenderOpenDt, dbo.tbl_TempAppPqTenderDates.actTechSubCmtRptDt, dbo.tbl_TempAppPqTenderDates.actTenderEvalRptDt, 
                         dbo.tbl_TempAppPqTenderDates.acttenderEvalRptAppDt, dbo.tbl_TempAppPqTenderDates.actTenderContractAppDt, 
                         dbo.tbl_TempAppPqTenderDates.actTenderNoaIssueDt, dbo.tbl_TempAppPqTenderDates.actTenderContractSignDt, 
                         dbo.tbl_TempAppPqTenderDates.actTenderContractCompDt, dbo.tbl_TempAppPqTenderDates.actReoiReceiptDt, dbo.tbl_TempAppPqTenderDates.actRfpTechEvalDt, 
                         dbo.tbl_TempAppPqTenderDates.actRfpFinancialOpenDt, dbo.tbl_TempAppPqTenderDates.actRfpNegComDt, dbo.tbl_TempAppPqTenderDates.actRfpContractAppDt, 
                         dbo.tbl_TempAppPqTenderDates.actRfaAdvertDt, dbo.tbl_TempAppPqTenderDates.actRfaReceiptDt, dbo.tbl_TempAppPqTenderDates.actRfaEvalDt, 
                         dbo.tbl_TempAppPqTenderDates.actRfaInterviewDt, dbo.tbl_TempAppPqTenderDates.actRfaFinalSelDt, dbo.tbl_TempAppPqTenderDates.actRfaEvalRptSubDt, 
                         dbo.tbl_TempAppPqTenderDates.actRfaAppConsultantDt, dbo.tbl_TempAppPqTenderDates.evalRptDt, dbo.tbl_TempAppPackages.packageId, 
                         dbo.tbl_TempAppPqTenderDates.appLstDt, dbo.tbl_TempAppPqTenderDates.subDays, dbo.tbl_TempAppPqTenderDates.advtDays, 
                         dbo.tbl_TempAppPqTenderDates.openDays, dbo.tbl_TempAppPqTenderDates.openDt, dbo.tbl_TempAppPqTenderDates.evalRptDays, 
                         dbo.tbl_TempAppPqTenderDates.tenderAdvertDays, dbo.tbl_TempAppPqTenderDates.tenderSubDays, dbo.tbl_TempAppPqTenderDates.tenderOpenDays, 
                         dbo.tbl_TempAppPqTenderDates.techSubCmtRptDays, dbo.tbl_TempAppPqTenderDates.tenderEvalRptdays, 
                         dbo.tbl_TempAppPqTenderDates.tenderEvalRptAppDays, dbo.tbl_TempAppPqTenderDates.tenderContractAppDays, 
                         dbo.tbl_TempAppPqTenderDates.tenderNoaIssueDays, dbo.tbl_TempAppPqTenderDates.tenderContractSignDays, dbo.tbl_TempAppPqTenderDates.rfaAdvertDays, 
                         dbo.tbl_TempAppPqTenderDates.rfaReceiptDays, dbo.tbl_TempAppPqTenderDates.rfaEvalDays, dbo.tbl_TempAppPqTenderDates.rfaInterviewDays, 
                         dbo.tbl_TempAppPqTenderDates.rfaFinalSelDays, dbo.tbl_TempAppPqTenderDates.rfaEvalRptSubDays, dbo.tbl_TempAppPqTenderDates.rfpContractAppDays, 
                         dbo.tbl_TempAppPqTenderDates.rfpNegCompDays, dbo.tbl_TempAppPqTenderDates.rfpFinancialOpenDays, dbo.tbl_TempAppPqTenderDates.rfpTechEvalDays, 
                         dbo.tbl_TempAppPqTenderDates.reoiReceiptDays, dbo.tbl_TempAppPqTenderDates.pqDtId, dbo.tbl_TempAppPackages.appId, dbo.tbl_AppMaster.projectId, 
                         dbo.tbl_TempAppPackages.approvingAuthEmpId, dbo.tbl_TempAppPackages.pkgUrgency, dbo.tbl_TempAppPackages.cpvCode, dbo.tbl_AppMaster.appCode, 
                         dbo.tbl_TempAppPackages.bidderCategory, dbo.tbl_TempAppPackages.workCategory, dbo.tbl_TempAppPackages.depoplanWork, 
                         dbo.tbl_TempAppPackages.entrustingAgency, dbo.tbl_TempAppPackages.timeFrame, dbo.tbl_TempAppPqTenderDates.tenderLetterIntentDt, 
                         dbo.tbl_TempAppPqTenderDates.tnderLetterIntentDays
FROM            dbo.tbl_AppMaster INNER JOIN
                         dbo.tbl_TempAppPackages ON dbo.tbl_AppMaster.appId = dbo.tbl_TempAppPackages.appId INNER JOIN
                         dbo.tbl_TempAppPqTenderDates ON dbo.tbl_AppMaster.appId = dbo.tbl_TempAppPqTenderDates.appid AND 
                         dbo.tbl_TempAppPackages.packageId = dbo.tbl_TempAppPqTenderDates.packageId INNER JOIN
                         dbo.tbl_OfficeMaster ON dbo.tbl_AppMaster.officeId = dbo.tbl_OfficeMaster.officeId INNER JOIN
                         dbo.tbl_StateMaster ON dbo.tbl_OfficeMaster.stateId = dbo.tbl_StateMaster.stateId AND dbo.tbl_OfficeMaster.stateId = dbo.tbl_StateMaster.stateId INNER JOIN
                         dbo.tbl_EmployeeMaster ON dbo.tbl_AppMaster.employeeId = dbo.tbl_EmployeeMaster.employeeId



GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[76] 4[5] 2[5] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -192
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbl_AppMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 243
            End
            DisplayFlags = 280
            TopColumn = 9
         End
         Begin Table = "tbl_TempAppPackages"
            Begin Extent = 
               Top = 142
               Left = 372
               Bottom = 449
               Right = 582
            End
            DisplayFlags = 280
            TopColumn = 19
         End
         Begin Table = "tbl_TempAppPqTenderDates"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 532
               Right = 271
            End
            DisplayFlags = 280
            TopColumn = 72
         End
         Begin Table = "tbl_OfficeMaster"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 531
               Right = 235
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_StateMaster"
            Begin Extent = 
               Top = 6
               Left = 281
               Bottom = 118
               Right = 451
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_EmployeeMaster"
            Begin Extent = 
               Top = 534
               Left = 38
               Bottom = 663
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
     ', 'SCHEMA', N'dbo', 'VIEW', N'vw_app_temppackagedetails', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N' Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vw_app_temppackagedetails', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'dbo', 'VIEW', N'vw_app_temppackagedetails', NULL, NULL
GO
