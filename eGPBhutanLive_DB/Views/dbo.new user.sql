SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[new user]
AS
SELECT        dbo.tbl_LoginMaster.userId, dbo.tbl_LoginMaster.hintQuestion, dbo.tbl_HintQuestionMaster.hintQuestionId, dbo.tbl_TendererMaster.tendererId, 
                         dbo.tbl_CompanyDocuments.companyDocId, dbo.tbl_CompanyDocuments.tendererId AS Expr1, dbo.tbl_CompanyMaster.companyId, 
                         dbo.tbl_MandatoryDoc.mandatoryDocId
FROM            dbo.tbl_LoginMaster INNER JOIN
                         dbo.tbl_TendererMaster ON dbo.tbl_LoginMaster.userId = dbo.tbl_TendererMaster.userId AND 
                         dbo.tbl_LoginMaster.userId = dbo.tbl_TendererMaster.userId INNER JOIN
                         dbo.tbl_CompanyMaster ON dbo.tbl_LoginMaster.userId = dbo.tbl_CompanyMaster.userId AND 
                         dbo.tbl_TendererMaster.companyId = dbo.tbl_CompanyMaster.companyId AND dbo.tbl_TendererMaster.companyId = dbo.tbl_CompanyMaster.companyId INNER JOIN
                         dbo.tbl_CompanyDocuments ON dbo.tbl_TendererMaster.tendererId = dbo.tbl_CompanyDocuments.tendererId AND 
                         dbo.tbl_TendererMaster.tendererId = dbo.tbl_CompanyDocuments.tendererId INNER JOIN
                         dbo.tbl_HintQuestionMaster ON dbo.tbl_LoginMaster.hintQuestion = dbo.tbl_HintQuestionMaster.hintQuestionId INNER JOIN
                         dbo.tbl_MandatoryDoc ON dbo.tbl_CompanyDocuments.companyDocId = dbo.tbl_MandatoryDoc.mandatoryDocId



GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[79] 4[8] 2[7] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbl_LoginMaster"
            Begin Extent = 
               Top = 11
               Left = 11
               Bottom = 387
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "tbl_TendererMaster"
            Begin Extent = 
               Top = 0
               Left = 535
               Bottom = 479
               Right = 701
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_CompanyMaster"
            Begin Extent = 
               Top = 89
               Left = 304
               Bottom = 367
               Right = 484
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_CompanyDocuments"
            Begin Extent = 
               Top = 0
               Left = 886
               Bottom = 237
               Right = 1078
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_HintQuestionMaster"
            Begin Extent = 
               Top = 378
               Left = 292
               Bottom = 482
               Right = 490
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_MandatoryDoc"
            Begin Extent = 
               Top = 308
               Left = 712
               Bottom = 481
               Right = 872
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
     ', 'SCHEMA', N'dbo', 'VIEW', N'new user', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N' Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'new user', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'dbo', 'VIEW', N'new user', NULL, NULL
GO
