SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TOC TEC formation]
AS
SELECT        dbo.tbl_TenderMaster.tenderId
FROM            dbo.tbl_ExternalMemInfo INNER JOIN
                         dbo.tbl_EmployeeMaster ON dbo.tbl_ExternalMemInfo.userId = dbo.tbl_EmployeeMaster.userId CROSS JOIN
                         dbo.tbl_CommitteeMembers INNER JOIN
                         dbo.tbl_Committee ON dbo.tbl_CommitteeMembers.committeeId = dbo.tbl_Committee.committeeId AND 
                         dbo.tbl_CommitteeMembers.committeeId = dbo.tbl_Committee.committeeId INNER JOIN
                         dbo.tbl_CommitteeMembersHistory ON dbo.tbl_Committee.committeeId = dbo.tbl_CommitteeMembersHistory.committeeId INNER JOIN
                         dbo.tbl_CommitteeHistory ON dbo.tbl_Committee.committeeId = dbo.tbl_CommitteeHistory.committeeId AND 
                         dbo.tbl_CommitteeMembers.committeeId = dbo.tbl_CommitteeHistory.committeeId AND 
                         dbo.tbl_CommitteeMembersHistory.committeeId = dbo.tbl_CommitteeHistory.committeeId INNER JOIN
                         dbo.tbl_TenderMaster ON dbo.tbl_Committee.tenderId = dbo.tbl_TenderMaster.tenderId AND dbo.tbl_Committee.tenderId = dbo.tbl_TenderMaster.tenderId



GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[88] 4[4] 2[4] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbl_EmployeeMaster"
            Begin Extent = 
               Top = 11
               Left = 38
               Bottom = 267
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_Committee"
            Begin Extent = 
               Top = 11
               Left = 589
               Bottom = 309
               Right = 774
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_CommitteeMembers"
            Begin Extent = 
               Top = 9
               Left = 867
               Bottom = 281
               Right = 1064
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_CommitteeMembersHistory"
            Begin Extent = 
               Top = 277
               Left = 37
               Bottom = 540
               Right = 253
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "tbl_CommitteeHistory"
            Begin Extent = 
               Top = 284
               Left = 347
               Bottom = 531
               Right = 546
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_ExternalMemInfo"
            Begin Extent = 
               Top = 316
               Left = 873
               Bottom = 538
               Right = 1067
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_TenderMaster"
            Begin Extent = 
               Top = 12
               Left = 290
               Bottom', 'SCHEMA', N'dbo', 'VIEW', N'TOC TEC formation', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N' = 190
               Right = 460
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'TOC TEC formation', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'dbo', 'VIEW', N'TOC TEC formation', NULL, NULL
GO
