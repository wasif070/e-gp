SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Project Creation]
AS
SELECT        dbo.tbl_ProjectMaster.projectId
FROM            dbo.tbl_ProgrammeMaster INNER JOIN
                         dbo.tbl_ProjectMaster ON dbo.tbl_ProgrammeMaster.progId = dbo.tbl_ProjectMaster.progId INNER JOIN
                         dbo.tbl_ProjectRoles ON dbo.tbl_ProjectMaster.projectId = dbo.tbl_ProjectRoles.projectId AND 
                         dbo.tbl_ProjectMaster.projectId = dbo.tbl_ProjectRoles.projectId INNER JOIN
                         dbo.tbl_ProjectPartners ON dbo.tbl_ProjectMaster.projectId = dbo.tbl_ProjectPartners.projectId AND 
                         dbo.tbl_ProjectMaster.projectId = dbo.tbl_ProjectPartners.projectId INNER JOIN
                         dbo.tbl_ProjectOffice ON dbo.tbl_ProjectMaster.projectId = dbo.tbl_ProjectOffice.projectId



GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[80] 4[4] 2[4] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbl_ProgrammeMaster"
            Begin Extent = 
               Top = 70
               Left = 37
               Bottom = 199
               Right = 207
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_ProjectMaster"
            Begin Extent = 
               Top = 17
               Left = 291
               Bottom = 277
               Right = 465
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_ProjectRoles"
            Begin Extent = 
               Top = 319
               Left = 519
               Bottom = 458
               Right = 710
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_ProjectPartners"
            Begin Extent = 
               Top = 168
               Left = 724
               Bottom = 304
               Right = 898
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_ProjectOffice"
            Begin Extent = 
               Top = 9
               Left = 725
               Bottom = 138
               Right = 895
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
     ', 'SCHEMA', N'dbo', 'VIEW', N'Project Creation', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'    Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'Project Creation', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'dbo', 'VIEW', N'Project Creation', NULL, NULL
GO
