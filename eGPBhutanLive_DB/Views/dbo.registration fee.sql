SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[registration fee]
AS
SELECT        dbo.tbl_LoginMaster.userId
FROM            dbo.tbl_TendererMaster INNER JOIN
                         dbo.tbl_LoginMaster ON dbo.tbl_TendererMaster.userId = dbo.tbl_LoginMaster.userId AND dbo.tbl_TendererMaster.userId = dbo.tbl_LoginMaster.userId INNER JOIN
                         dbo.tbl_PartnerAdmin ON dbo.tbl_LoginMaster.userId = dbo.tbl_PartnerAdmin.userId AND dbo.tbl_LoginMaster.userId = dbo.tbl_PartnerAdmin.userId AND 
                         dbo.tbl_LoginMaster.userId = dbo.tbl_PartnerAdmin.userId INNER JOIN
                         dbo.tbl_RegFeePayment ON dbo.tbl_LoginMaster.userId = dbo.tbl_RegFeePayment.userId INNER JOIN
                         dbo.tbl_ScBankDevPartnerMaster ON dbo.tbl_PartnerAdmin.sBankDevelopId = dbo.tbl_ScBankDevPartnerMaster.sBankDevelopId CROSS JOIN
                         dbo.tbl_RegistrationValidity



GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[88] 4[4] 2[4] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbl_TendererMaster"
            Begin Extent = 
               Top = 9
               Left = 11
               Bottom = 545
               Right = 190
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_LoginMaster"
            Begin Extent = 
               Top = 22
               Left = 238
               Bottom = 444
               Right = 423
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_PartnerAdmin"
            Begin Extent = 
               Top = 275
               Left = 462
               Bottom = 496
               Right = 618
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_RegFeePayment"
            Begin Extent = 
               Top = 0
               Left = 917
               Bottom = 476
               Right = 1084
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_RegistrationValidity"
            Begin Extent = 
               Top = 249
               Left = 1299
               Bottom = 546
               Right = 1487
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_ScBankDevPartnerMaster"
            Begin Extent = 
               Top = 209
               Left = 660
               Bottom = 540
               Right = 902
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPan', 'SCHEMA', N'dbo', 'VIEW', N'registration fee', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'e = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'registration fee', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'dbo', 'VIEW', N'registration fee', NULL, NULL
GO
