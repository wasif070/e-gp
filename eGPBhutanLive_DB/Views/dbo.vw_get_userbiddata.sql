SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_get_userbiddata]
AS
SELECT DISTINCT
                      TOP (100) PERCENT dbo.tbl_TenderBidForm.userId, dbo.tbl_TenderBidTable.tenderTableId, dbo.tbl_TenderBidForm.tenderFormId,
                      dbo.tbl_TenderBidForm.bidId, dbo.tbl_TenderBidPlainData.tenderColId AS columnId, dbo.tbl_TenderBidPlainData.cellValue,
                      dbo.tbl_TenderBidPlainData.rowId, dbo.tbl_TenderBidTable.bidTableId
FROM         dbo.tbl_TenderBidTable INNER JOIN
                      dbo.tbl_TenderBidForm ON dbo.tbl_TenderBidTable.bidId = dbo.tbl_TenderBidForm.bidId INNER JOIN
                      dbo.tbl_TenderBidPlainData ON dbo.tbl_TenderBidTable.bidTableId = dbo.tbl_TenderBidPlainData.bidTableId AND
                      dbo.tbl_TenderBidTable.bidTableId = dbo.tbl_TenderBidPlainData.bidTableId INNER JOIN
                      dbo.tbl_FinalSubmission ON dbo.tbl_TenderBidForm.tenderId = dbo.tbl_FinalSubmission.tenderId AND
                      dbo.tbl_TenderBidForm.userId = dbo.tbl_FinalSubmission.userId AND dbo.tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
ORDER BY dbo.tbl_TenderBidForm.userId



GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[56] 4[12] 2[29] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -28
      End
      Begin Tables = 
         Begin Table = "tbl_TenderBidTable"
            Begin Extent = 
               Top = 35
               Left = 336
               Bottom = 190
               Right = 532
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_TenderBidForm"
            Begin Extent = 
               Top = 89
               Left = 871
               Bottom = 269
               Right = 1061
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_TenderBidPlainData"
            Begin Extent = 
               Top = 58
               Left = 32
               Bottom = 267
               Right = 258
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "tbl_FinalSubmission"
            Begin Extent = 
               Top = 217
               Left = 568
               Bottom = 407
               Right = 731
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_CurrencyMaster"
            Begin Extent = 
               Top = 270
               Left = 284
               Bottom = 418
               Right = 479
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane =', 'SCHEMA', N'dbo', 'VIEW', N'vw_get_userbiddata', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N' 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 2475
         Output = 4410
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vw_get_userbiddata', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'dbo', 'VIEW', N'vw_get_userbiddata', NULL, NULL
GO
