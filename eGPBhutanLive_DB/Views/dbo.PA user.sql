SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PA user]
AS
SELECT        dbo.tbl_EmployeeMaster.employeeId
FROM            dbo.tbl_EmployeeMaster INNER JOIN
                         dbo.tbl_EmployeeOffices ON dbo.tbl_EmployeeMaster.employeeId = dbo.tbl_EmployeeOffices.employeeId AND 
                         dbo.tbl_EmployeeMaster.employeeId = dbo.tbl_EmployeeOffices.employeeId INNER JOIN
                         dbo.tbl_EmployeeRoles ON dbo.tbl_EmployeeMaster.employeeId = dbo.tbl_EmployeeRoles.employeeId AND 
                         dbo.tbl_EmployeeMaster.employeeId = dbo.tbl_EmployeeRoles.employeeId INNER JOIN
                         dbo.tbl_DepartmentMaster ON dbo.tbl_EmployeeRoles.departmentId = dbo.tbl_DepartmentMaster.departmentId AND 
                         dbo.tbl_EmployeeRoles.departmentId = dbo.tbl_DepartmentMaster.departmentId INNER JOIN
                         dbo.tbl_DesignationMaster ON dbo.tbl_EmployeeMaster.designationId = dbo.tbl_DesignationMaster.designationId AND 
                         dbo.tbl_DepartmentMaster.departmentId = dbo.tbl_DesignationMaster.departmentid AND 
                         dbo.tbl_DepartmentMaster.departmentId = dbo.tbl_DesignationMaster.departmentid INNER JOIN
                         dbo.tbl_OfficeMaster ON dbo.tbl_EmployeeOffices.officeId = dbo.tbl_OfficeMaster.officeId AND 
                         dbo.tbl_EmployeeOffices.officeId = dbo.tbl_OfficeMaster.officeId INNER JOIN
                         dbo.tbl_ProcurementRole ON dbo.tbl_EmployeeRoles.procurementRoleId = dbo.tbl_ProcurementRole.procurementRoleId AND 
                         dbo.tbl_EmployeeRoles.procurementRoleId = dbo.tbl_ProcurementRole.procurementRoleId INNER JOIN
                         dbo.tbl_EmployeeTrasfer ON dbo.tbl_EmployeeMaster.employeeId = dbo.tbl_EmployeeTrasfer.employeeId



GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[88] 4[4] 2[4] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbl_EmployeeMaster"
            Begin Extent = 
               Top = 3
               Left = 7
               Bottom = 258
               Right = 216
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_EmployeeOffices"
            Begin Extent = 
               Top = 4
               Left = 611
               Bottom = 143
               Right = 794
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_EmployeeRoles"
            Begin Extent = 
               Top = 90
               Left = 393
               Bottom = 219
               Right = 584
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_EmployeeTrasfer"
            Begin Extent = 
               Top = 268
               Left = 244
               Bottom = 541
               Right = 453
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_DepartmentMaster"
            Begin Extent = 
               Top = 337
               Left = 852
               Bottom = 466
               Right = 1055
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_DesignationMaster"
            Begin Extent = 
               Top = 271
               Left = 514
               Bottom = 461
               Right = 697
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_OfficeMaster"
            Begin Extent = 
               Top = 5
               Left = 859
               Bottom = 134', 'SCHEMA', N'dbo', 'VIEW', N'PA user', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'
               Right = 1056
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_ProcurementRole"
            Begin Extent = 
               Top = 197
               Left = 867
               Bottom = 292
               Right = 1058
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'PA user', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'dbo', 'VIEW', N'PA user', NULL, NULL
GO
