SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_get_AppData]
AS
SELECT     am.appId, am.appCode, dm.departmentName, em.employeeName, sm.stateName, ap.procurementnature, ap.packageNo, ap.packageDesc, am.projectName, 
                      am.financialYear, CONVERT(int, dm.departmentId) AS departmentId, em.employeeId, ap.cpvCode, ap.procurementType, ap.estimatedCost, ap.pkgEstCost, am.officeId, 
                      am.budgetType, pm.procurementMethod, om.officeName, ap.appStatus, ap.packageId, ap.workflowStatus, am.projectId, dm.departmentType, 
                      dm.parentDepartmentId
FROM         dbo.tbl_AppMaster AS am INNER JOIN
                      dbo.tbl_AppPackages AS ap ON am.appId = ap.appId INNER JOIN
                      dbo.tbl_ProcurementMethod AS pm ON ap.procurementMethodId = pm.procurementMethodId INNER JOIN
                      dbo.tbl_OfficeMaster AS om ON am.officeId = om.officeId INNER JOIN
                      dbo.tbl_EmployeeMaster AS em ON am.employeeId = em.employeeId INNER JOIN
                      dbo.tbl_DesignationMaster AS ds ON em.designationId = ds.designationId INNER JOIN
                      dbo.tbl_DepartmentMaster AS dm ON ds.departmentid = dm.departmentId INNER JOIN
                      dbo.tbl_StateMaster AS sm ON om.stateId = sm.stateId


GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[59] 4[10] 2[31] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1[49] 4[21] 3) )"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1[44] 3) )"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2) )"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = -288
      End
      Begin Tables = 
         Begin Table = "am"
            Begin Extent = 
               Top = 57
               Left = 690
               Bottom = 195
               Right = 875
            End
            DisplayFlags = 280
            TopColumn = 10
         End
         Begin Table = "ap"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 363
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "pm"
            Begin Extent = 
               Top = 6
               Left = 261
               Bottom = 137
               Right = 448
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "om"
            Begin Extent = 
               Top = 173
               Left = 310
               Bottom = 324
               Right = 486
            End
            DisplayFlags = 280
            TopColumn = 12
         End
         Begin Table = "em"
            Begin Extent = 
               Top = 236
               Left = 684
               Bottom = 388
               Right = 869
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ds"
            Begin Extent = 
               Top = 405
               Left = 424
               Bottom = 595
               Right = 586
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dm"
            Begin Extent = 
               Top = 368
               Left = 145
               Bottom = 668
               Right = 328
            End
            DisplayFlags = 280
       ', 'SCHEMA', N'dbo', 'VIEW', N'vw_get_AppData', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'     TopColumn = 7
         End
         Begin Table = "sm"
            Begin Extent = 
               Top = 438
               Left = 723
               Bottom = 625
               Right = 874
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 28
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 2055
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vw_get_AppData', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'dbo', 'VIEW', N'vw_get_AppData', NULL, NULL
GO
