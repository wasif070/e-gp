SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_app_packagedetails]
AS
SELECT        dbo.tbl_AppMaster.appCode, dbo.tbl_AppMaster.financialYear, dbo.tbl_AppMaster.budgetType, dbo.tbl_AppMaster.projectName, 
                         dbo.tbl_AppPackages.procurementnature, dbo.tbl_AppPackages.servicesType, dbo.tbl_AppPackages.packageNo, dbo.tbl_AppPackages.packageDesc, 
                         dbo.tbl_AppPackages.allocateBudget, dbo.tbl_AppPackages.pkgEstCost, dbo.tbl_AppPackages.isPQRequired, dbo.tbl_AppPackages.reoiRfaRequired, 
                         dbo.tbl_AppPackages.procurementMethodId, dbo.tbl_AppPackages.procurementType, dbo.tbl_AppPackages.sourceOfFund, dbo.tbl_AppPqTenderDates.advtDt, 
                         dbo.tbl_AppPqTenderDates.subDt, dbo.tbl_AppPqTenderDates.tenderAdvertDt, dbo.tbl_AppPqTenderDates.tenderSubDt, dbo.tbl_AppPqTenderDates.tenderOpenDt, 
                         dbo.tbl_AppPqTenderDates.tenderNoaIssueDt, dbo.tbl_AppPqTenderDates.tenderContractSignDt, dbo.tbl_AppPqTenderDates.tenderContractCompDt, 
                         dbo.tbl_EmployeeMaster.employeeName AS PE, dbo.tbl_StateMaster.stateName AS district, dbo.tbl_AppPqTenderDates.techSubCmtRptDt, 
                         dbo.tbl_AppPqTenderDates.tenderEvalRptDt, dbo.tbl_AppPqTenderDates.tenderEvalRptAppDt, dbo.tbl_AppPqTenderDates.tenderContractAppDt, 
                         dbo.tbl_AppPqTenderDates.reoiReceiptDt, dbo.tbl_AppPqTenderDates.rfpTechEvalDt, dbo.tbl_AppPqTenderDates.rfpFinancialOpenDt, 
                         dbo.tbl_AppPqTenderDates.rfpNegCompDt, dbo.tbl_AppPqTenderDates.rfpContractAppDt, dbo.tbl_AppPqTenderDates.rfaAdvertDt, 
                         dbo.tbl_AppPqTenderDates.rfaReceiptDt, dbo.tbl_AppPqTenderDates.rfaEvalDt, dbo.tbl_AppPqTenderDates.rfaInterviewDt, dbo.tbl_AppPqTenderDates.rfaFinalSelDt, 
                         dbo.tbl_AppPqTenderDates.rfaEvalRptSubDt, dbo.tbl_AppPqTenderDates.rfaAppConsultantDt, dbo.tbl_AppPqTenderDates.actSubDt, 
                         dbo.tbl_AppPqTenderDates.actAdvtDt, dbo.tbl_AppPqTenderDates.actEvalRptDt, dbo.tbl_AppPqTenderDates.actAppLstDt, 
                         dbo.tbl_AppPqTenderDates.actTenderAdvertDt, dbo.tbl_AppPqTenderDates.actTenderSubDt, dbo.tbl_AppPqTenderDates.actTenderOpenDt, 
                         dbo.tbl_AppPqTenderDates.actTechSubCmtRptDt, dbo.tbl_AppPqTenderDates.actTenderEvalRptDt, dbo.tbl_AppPqTenderDates.acttenderEvalRptAppDt, 
                         dbo.tbl_AppPqTenderDates.actTenderContractAppDt, dbo.tbl_AppPqTenderDates.actTenderNoaIssueDt, dbo.tbl_AppPqTenderDates.actTenderContractSignDt, 
                         dbo.tbl_AppPqTenderDates.actTenderContractCompDt, dbo.tbl_AppPqTenderDates.actReoiReceiptDt, dbo.tbl_AppPqTenderDates.actRfpTechEvalDt, 
                         dbo.tbl_AppPqTenderDates.actRfpFinancialOpenDt, dbo.tbl_AppPqTenderDates.actRfpNegComDt, dbo.tbl_AppPqTenderDates.actRfpContractAppDt, 
                         dbo.tbl_AppPqTenderDates.actRfaAdvertDt, dbo.tbl_AppPqTenderDates.actRfaReceiptDt, dbo.tbl_AppPqTenderDates.actRfaEvalDt, 
                         dbo.tbl_AppPqTenderDates.actRfaInterviewDt, dbo.tbl_AppPqTenderDates.actRfaFinalSelDt, dbo.tbl_AppPqTenderDates.actRfaEvalRptSubDt, 
                         dbo.tbl_AppPqTenderDates.actRfaAppConsultantDt, dbo.tbl_AppPqTenderDates.evalRptDt, dbo.tbl_AppPackages.packageId, dbo.tbl_AppPqTenderDates.appLstDt, 
                         dbo.tbl_AppPqTenderDates.subDays, dbo.tbl_AppPqTenderDates.advtDays, dbo.tbl_AppPqTenderDates.openDays, dbo.tbl_AppPqTenderDates.openDt, 
                         dbo.tbl_AppPqTenderDates.evalRptDays, dbo.tbl_AppPqTenderDates.tenderAdvertDays, dbo.tbl_AppPqTenderDates.tenderSubDays, 
                         dbo.tbl_AppPqTenderDates.tenderOpenDays, dbo.tbl_AppPqTenderDates.techSubCmtRptDays, dbo.tbl_AppPqTenderDates.tenderEvalRptdays, 
                         dbo.tbl_AppPqTenderDates.tenderEvalRptAppDays, dbo.tbl_AppPqTenderDates.tenderContractAppDays, dbo.tbl_AppPqTenderDates.tenderNoaIssueDays, 
                         dbo.tbl_AppPqTenderDates.tenderContractSignDays, dbo.tbl_AppPqTenderDates.rfaAdvertDays, dbo.tbl_AppPqTenderDates.rfaReceiptDays, 
                         dbo.tbl_AppPqTenderDates.rfaEvalDays, dbo.tbl_AppPqTenderDates.rfaInterviewDays, dbo.tbl_AppPqTenderDates.rfaFinalSelDays, 
                         dbo.tbl_AppPqTenderDates.rfaEvalRptSubDays, dbo.tbl_AppPqTenderDates.rfpContractAppDays, dbo.tbl_AppPqTenderDates.rfpNegCompDays, 
                         dbo.tbl_AppPqTenderDates.rfpFinancialOpenDays, dbo.tbl_AppPqTenderDates.rfpTechEvalDays, dbo.tbl_AppPqTenderDates.reoiReceiptDays, 
                         dbo.tbl_AppPqTenderDates.pqDtId, dbo.tbl_AppPackages.appId, dbo.tbl_AppMaster.projectId, dbo.tbl_AppPackages.approvingAuthEmpId, 
                         dbo.tbl_AppPackages.pkgUrgency, dbo.tbl_AppPackages.cpvCode, dbo.tbl_AppPackages.bidderCategory, dbo.tbl_AppPackages.workCategory, 
                         dbo.tbl_AppPackages.depoplanWork, dbo.tbl_AppPackages.entrustingAgency, dbo.tbl_AppPackages.timeFrame, dbo.tbl_AppPqTenderDates.tenderLetterIntentDt, 
                         dbo.tbl_AppPqTenderDates.tnderLetterIntentDays
FROM            dbo.tbl_AppMaster INNER JOIN
                         dbo.tbl_AppPackages ON dbo.tbl_AppMaster.appId = dbo.tbl_AppPackages.appId INNER JOIN
                         dbo.tbl_AppPqTenderDates ON dbo.tbl_AppMaster.appId = dbo.tbl_AppPqTenderDates.appid AND 
                         dbo.tbl_AppPackages.packageId = dbo.tbl_AppPqTenderDates.packageId INNER JOIN
                         dbo.tbl_OfficeMaster ON dbo.tbl_AppMaster.officeId = dbo.tbl_OfficeMaster.officeId INNER JOIN
                         dbo.tbl_StateMaster ON dbo.tbl_OfficeMaster.stateId = dbo.tbl_StateMaster.stateId AND dbo.tbl_OfficeMaster.stateId = dbo.tbl_StateMaster.stateId INNER JOIN
                         dbo.tbl_EmployeeMaster ON dbo.tbl_AppMaster.employeeId = dbo.tbl_EmployeeMaster.employeeId



GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[73] 4[5] 2[9] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tbl_AppMaster"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 223
            End
            DisplayFlags = 280
            TopColumn = 11
         End
         Begin Table = "tbl_AppPackages"
            Begin Extent = 
               Top = 6
               Left = 261
               Bottom = 288
               Right = 448
            End
            DisplayFlags = 280
            TopColumn = 21
         End
         Begin Table = "tbl_AppPqTenderDates"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 323
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 24
         End
         Begin Table = "tbl_OfficeMaster"
            Begin Extent = 
               Top = 114
               Left = 285
               Bottom = 222
               Right = 461
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_StateMaster"
            Begin Extent = 
               Top = 222
               Left = 38
               Bottom = 315
               Right = 189
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbl_EmployeeMaster"
            Begin Extent = 
               Top = 222
               Left = 227
               Bottom = 330
               Right = 412
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 2', 'SCHEMA', N'dbo', 'VIEW', N'vw_app_packagedetails', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'84
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vw_app_packagedetails', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'dbo', 'VIEW', N'vw_app_packagedetails', NULL, NULL
GO
