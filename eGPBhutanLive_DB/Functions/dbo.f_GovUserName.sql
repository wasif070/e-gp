SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------  
--  
-- Purpose: Returns a trimmed string.  
--  
--  
-- Author: Karan  
-- Date: 16-04-2011  
--  
-- Last Modified:  
-- Modified By:  
-- Date   
-- Modification:  
--------------------------------------------------------------------------------  
  
  
CREATE FUNCTION [dbo].[f_GovUserName](@v_GovUserId_inInt INT, @TableName varchar(50)) RETURNS VARCHAR(MAX)  
AS  
  
BEGIN   
  
  
 Declare @v_GovUserName varchar(500)  
   
 IF @TableName='tbl_PartnerAdminTransfer' -- Bank User Case  
 BEGIN  
  SELECT @v_GovUserName = fullName FROM dbo.tbl_PartnerAdminTransfer Where partTransId=@v_GovUserId_inInt  
 END  
 ELSE IF @TableName='tbl_EmployeeTrasfer'  
 BEGIN  
	SELECT @v_GovUserName = employeeName FROM dbo.tbl_EmployeeTrasfer Where govUserId=@v_GovUserId_inInt    
 END   
 --set @v_GovUserName='abcabc'  
 Return @v_GovUserName  
  
END



GO
