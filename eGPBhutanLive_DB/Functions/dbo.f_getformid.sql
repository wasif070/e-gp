SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Return the formIds
--
--
-- Author: Krishnraj
-- Date: 07-01-2011
--
-- Last Modified:
-- Modified By:
-- Date 
-- Modification:
--------------------------------------------------------------------------------

-- Select dbo.f_getformid(1374, 15)
CREATE FUNCTION [dbo].[f_getformid](@v_tenderId_inIn INT, @v_UserId_inIn INT)
RETURNS @v_Results_outTbl TABLE (formID INT)

AS

BEGIN
    
    DECLARE @v_isTSC VARCHAR(10), @v_tsccnt INT,@v_cnfgType VARCHAR(5), @v_tscMemId INT, @v_tecMemId INT

	SELECT @v_isTSC = isTscReq, @v_cnfgType = configType FROM dbo.tbl_EvalConfig WHERE tenderId = @v_tenderId_inIn

    IF @v_isTSC = 'yes'

	BEGIN

	SELECT @v_tsccnt = count(userId) FROM tbl_Committee tc, tbl_CommitteeMembers tcm 
		WHERE tc.committeeId = tcm.committeeId and tc.tenderId = @v_tenderId_inIn and tc. committeetype = 'TSC' 
		and tc.committeStatus = 'approved' and userid = @v_UserId_inIn  

		  IF @v_tsccnt <> 0 --- TSC Member

				BEGIN

					  IF @v_cnfgType = 'team'

							BEGIN

								  SELECT @v_tscMemId = tscMemberId FROM tbl_EvalConfig WHERE tenderId = @v_tenderId_inIn and tscMemberId = @v_UserId_inIn      

								  IF @v_tscMemId <> 0
										INSERT INTO @v_Results_outTbl (formID)
											SELECT distinct formID FROM tbl_EvalMapForms WHERE tenderId = @v_tenderId_inIn and tscCom = 'yes'                          
								  ELSE
										INSERT INTO @v_Results_outTbl (formID)
											SELECT '0'											
							END

					  ELSE
							INSERT INTO @v_Results_outTbl (formID)
								SELECT distinct formID FROM tbl_EvalMapForms WHERE tenderId = @v_tenderId_inIn and tscCom = 'yes'            

				END

		  ELSE --- TEC Member

		  BEGIN

				IF @v_cnfgType = 'team'

							BEGIN

								  SELECT @v_tecMemId = tecMemberId FROM tbl_EvalConfig WHERE tenderId = @v_tenderId_inIn and tecMemberId = @v_UserId_inIn      

								  IF @v_tecMemId <> 0
										INSERT INTO @v_Results_outTbl (formID)
											SELECT distinct formID FROM tbl_EvalMapForms WHERE tenderId = @v_tenderId_inIn and tecCom = 'yes'                          
								  ELSE
										INSERT INTO @v_Results_outTbl (formID)
											SELECT '0'

							END

					  ELSE

							INSERT INTO @v_Results_outTbl (formID)
								SELECT DISTINCT formID FROM tbl_EvalMapForms WHERE tenderId = @v_tenderId_inIn and tecCom = 'yes'
				END
	END

	ELSE

	BEGIN

		  IF @v_cnfgType = 'team'

			BEGIN

				  SELECT @v_tecMemId = tecMemberId FROM tbl_EvalConfig WHERE tenderId = @v_tenderId_inIn and tecMemberId = @v_UserId_inIn      

				  IF @v_tecMemId <> 0
						INSERT INTO @v_Results_outTbl (formID)
							select tenderFormId from tbl_TenderStd s,tbl_TenderSection st,tbl_TenderForms tf
where s.tenderStdId=st.tenderStdId
and tf.tenderSectionId=st.tenderSectionId and s.tenderId=@v_tenderId_inIn							
						       
				  ELSE
						INSERT INTO @v_Results_outTbl (formID) 
							SELECT '0'

			END

	ELSE
			INSERT INTO @v_Results_outTbl (formID)
					select tenderFormId from tbl_TenderStd s,tbl_TenderSection st,tbl_TenderForms tf
where s.tenderStdId=st.tenderStdId
and tf.tenderSectionId=st.tenderSectionId and s.tenderId=@v_tenderId_inIn					
			
	END
		
    RETURN
END



GO
