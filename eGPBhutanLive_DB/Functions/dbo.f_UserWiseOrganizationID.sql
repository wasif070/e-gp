SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[f_UserWiseOrganizationID] 
(
	@userid int
)
RETURNS int
AS
BEGIN
declare @departmentId int
select @departmentId = departmentId from tbl_OfficeMaster where officeId = dbo.f_OfficeID(@userid)
RETURN @departmentId
END



GO
