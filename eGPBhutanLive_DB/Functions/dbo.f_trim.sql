SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Returns a trimmed string.
--
--
-- Author: Karan
-- Date: 25-10-2010
--
-- Last Modified:
-- Modified By:
-- Date 25-10-2010
-- Modification:
--------------------------------------------------------------------------------


CREATE FUNCTION [dbo].[f_trim](@v_String_inVc VARCHAR(MAX)) RETURNS VARCHAR(MAX)
AS
BEGIN
RETURN dbo.f_trimleft(dbo.f_trimright(@v_String_inVc))
END



GO
