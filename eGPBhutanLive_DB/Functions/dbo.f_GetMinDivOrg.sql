SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Taher
-- Create date: 15/Feb/2012
-- Description:	Ministry,Division and Organisation
-- =============================================
CREATE FUNCTION [dbo].[f_GetMinDivOrg](@v_departmentId_inInt INT) RETURNS VARCHAR(MAX)  
AS
BEGIN
DECLARE @v_departMentId int,@v_departMentType varchar(200),@v_departMentName varchar(200)
select @v_departMentId = parentDepartmentId,@v_departMentType = departmentType,@v_departMentName = departmentName from tbl_DepartmentMaster where departmentId = @v_departmentId_inInt
if @v_departMentType != 'Ministry'          
Begin
 select @v_departMentName  = @v_departMentName +' , ' +(select dbo.f_GetMinDivOrg(@v_departMentId))
End	
RETURN @v_departMentName

END



GO
