SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[f_get_Ministry_new]
(
	-- Add the parameters for the function here
	@v_appIdVc varchar(8000)
)
RETURNS varchar(max)
AS
BEGIN
	declare
  @v_CurrDepartmentId_Int int,
@v_CurrDepartmentType_Vc varchar(15),
@v_CurrDepartmentName_Vc varchar(150),
@v_Ministry_Vc varchar(150),
@v_Division_Vc varchar(150),
@v_Agency_Vc varchar(150),
@v_DeptId_Int Int, @v_OffId_Int Int,
@retContactInformation varchar(max)

SELECT @v_DeptId_Int = departmentId, @v_OffId_Int= officeId FROM [tbl_AppMaster] where appId = @v_appIdVc
  
  Select @v_CurrDepartmentId_Int=departmentId ,@v_CurrDepartmentType_Vc=departmentType, @v_CurrDepartmentName_Vc=departmentName 
					from tbl_DepartmentMaster where departmentId=@v_DeptId_Int
				
				If @v_CurrDepartmentType_Vc='Ministry'
				Begin
					Select @v_Ministry_Vc=@v_CurrDepartmentName_Vc, @v_Division_Vc='',@v_Agency_Vc=''
				End
				Else If @v_CurrDepartmentType_Vc='Division'
				Begin
					Select @v_Ministry_Vc=(select departmentName from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_CurrDepartmentId_Int)), 
					@v_Division_Vc=@v_CurrDepartmentName_Vc,					
					@v_Agency_Vc=''
				End
				Else -- Agency/Organisation Case
				Begin
					Select @v_Ministry_Vc=(select departmentName from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_CurrDepartmentId_Int))), 
					@v_Division_Vc= (select departmentName from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_CurrDepartmentId_Int)),					
					@v_Agency_Vc=@v_CurrDepartmentName_Vc
				End
				
				
				--select @v_Ministry_Vc as FieldValue1, @v_Division_Vc as FieldValue2, @v_Agency_Vc as FieldValue3, officename+' - '+pecode FieldValue4 from tbl_OfficeMaster where officeId = @v_OffId_Int
		
 
        SELECT @retContactInformation = officename+' - '+pecode from tbl_OfficeMaster where officeId = @v_OffId_Int
        return @retContactInformation

END



GO
