SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Returns a table after splitting a string with respect to a specific character. eg; comma(,).
--
--
-- Author: Karan
-- Date: 25-10-2010
--
-- Last Modified:
-- Modified By:
-- Date 25-10-2010
-- Modification:
--------------------------------------------------------------------------------

CREATE FUNCTION [dbo].[f_split](@v_String_inVc varchar(max), @v_Delimiter_inChr char(1))
RETURNS @v_Results_outtbl TABLE (Items varchar(max))
AS

BEGIN
    
    DECLARE @v_IndexInt int,
			@v_SliceVc varchar(max)
    -- HAVE TO SET TO 1 SO IT DOESNT EQUAL ZERO FIRST TIME IN LOOP
    SELECT @v_IndexInt = 1
    
    IF @v_String_inVc IS NULL RETURN
    WHILE @v_IndexInt !=0
        BEGIN	
        	-- GET THE INDEX OF THE FIRST OCCURENCE OF THE SPLIT CHARACTER
        	SELECT @v_IndexInt = CHARINDEX(@v_Delimiter_inChr,@v_String_inVc)
        	-- NOW PUSH EVERYTHING TO THE LEFT OF IT INTO THE SLICE VARIABLE
        	IF @v_IndexInt !=0
        		SELECT @v_SliceVc = LEFT(@v_String_inVc,@v_IndexInt - 1)
        	ELSE
        		SELECT @v_SliceVc = @v_String_inVc
        	-- PUT THE ITEM INTO THE RESULTS SET
        	INSERT INTO @v_Results_outtbl(Items) VALUES(@v_SliceVc)
        	-- CHOP THE ITEM REMOVED OFF THE MAIN STRING
        	SELECT @v_String_inVc = RIGHT(@v_String_inVc,LEN(@v_String_inVc) - @v_IndexInt)
        	-- BREAK OUT IF WE ARE DONE
        	IF LEN(@v_String_inVc) = 0 BREAK
    END
    RETURN
END



GO
