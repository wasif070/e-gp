SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Returns a trimmed string.
--
--
-- Author: Darshan
-- Date: 18-02-2012
--
-- Last Modified:
-- Modified By:
-- Date 
-- Modification:
--------------------------------------------------------------------------------


CREATE FUNCTION [dbo].[f_getTenderEvalCase](@v_TenderId_inInt INT) RETURNS VARCHAR(5)
AS

BEGIN 
DECLARE @v_TenderEvalCase_STR VARCHAR(50)

DECLARE @v_TenderEncCount_Int INT,
		@v_ProcNature_STR VARCHAR(10),
		@v_ProcMethod_STR VARCHAR(10),
		@v_eventType_STR VARCHAR(10)
		
		
SET  @v_TenderEncCount_Int = dbo.f_getTenderEnvCount(@v_TenderId_inInt)

SELECT	@v_ProcNature_STR=procurementNature,
		@v_ProcMethod_STR=procurementMethod,
		@v_eventType_STR=eventType
FROM	tbl_tenderDetails where tenderID = @v_TenderId_inInt
		
SELECT @v_TenderEvalCase_STR = CASE 
WHEN @v_ProcNature_STR='Services' -- For Services
THEN
    CASE 
    WHEN @v_eventType_STR='REOI' AND (@v_ProcMethod_STR='QCBS' OR @v_ProcMethod_STR='LCS' OR @v_ProcMethod_STR='SFB' OR @v_ProcMethod_STR='DC'OR @v_ProcMethod_STR='SBCQ' OR @v_ProcMethod_STR='CSO')
	THEN
		 'CASE1'
	WHEN  ((@v_eventType_STR='RFA' AND (@v_ProcMethod_STR='DC' OR @v_ProcMethod_STR='SBCQ' OR @v_ProcMethod_STR='SSS' OR @v_ProcMethod_STR='IC' OR @v_ProcMethod_STR='CSO')) OR (@v_eventType_STR='RFP' AND (@v_ProcMethod_STR='QCBS' OR @v_ProcMethod_STR='LCS' OR @v_ProcMethod_STR='SFB' OR @v_ProcMethod_STR='DC' OR @v_ProcMethod_STR='SBCQ' OR @v_ProcMethod_STR='CSO' OR @v_ProcMethod_STR='DC' OR @v_ProcMethod_STR='SBCQ' OR @v_ProcMethod_STR='SSS' OR @v_ProcMethod_STR='IC' OR @v_ProcMethod_STR='CSO')))
    THEN
         CASE 
         WHEN @v_TenderEncCount_Int = 2
		 THEN	
            'CASE2'
         ELSE
            'CASE3'
		 END
	END
ELSE
    CASE 
    WHEN (@v_eventType_STR='1 stage-TSTM' OR @v_eventType_STR='PQ') OR (@v_ProcMethod_STR='OSTETM' AND @v_TenderEncCount_Int <> 2)
	THEN
		 'CASE1'
	WHEN @v_ProcMethod_STR='OSTETM' AND @v_TenderEncCount_Int = 2
    THEN
        'CASE2'
    WHEN (@v_eventType_STR='2 stage-TSTM') OR (@v_ProcMethod_STR='OTM' AND @v_eventType_STR<>'PQ') OR @v_ProcMethod_STR='LTM' OR @v_ProcMethod_STR='RFQ' OR @v_ProcMethod_STR='RFQU' OR @v_ProcMethod_STR='RFQL' OR @v_ProcMethod_STR='DPM'
	THEN
       'CASE3'
    END
END

RETURN @v_TenderEvalCase_STR

END



GO
