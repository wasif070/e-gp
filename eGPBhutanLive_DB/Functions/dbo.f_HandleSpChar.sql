SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------  
--  
-- Purpose: Returns String with special handled
--  
-- Author: Taher
-- Date: 16-11-2011  
--  
-- Last Modified:  
-- Modified By:  
-- Date   
-- Modification:  
--------------------------------------------------------------------------------  
  
  
CREATE FUNCTION [dbo].[f_HandleSpChar](@v_xml_String varchar(max)) RETURNS VARCHAR(MAX)  
AS  
  
BEGIN   

 Declare @v_xml_output varchar(MAX)

				 set @v_xml_output=replace(@v_xml_String,'¡','&#161;')
				 set @v_xml_output=replace(@v_xml_output,'¢','&#162;')
				 set @v_xml_output=replace(@v_xml_output,'£','&#163;')
				 set @v_xml_output=replace(@v_xml_output,'¤','&#164;')
				 set @v_xml_output=replace(@v_xml_output,'¥','&#165;')
				 set @v_xml_output=replace(@v_xml_output,'¦','&#166;')
				 set @v_xml_output=replace(@v_xml_output,'§','&#167;')
				 set @v_xml_output=replace(@v_xml_output,'¨','&#168;')
				 set @v_xml_output=replace(@v_xml_output,'©','&#169;')
				 set @v_xml_output=replace(@v_xml_output,'ª','&#170;')
				 set @v_xml_output=replace(@v_xml_output,'«','&#171;')
				 set @v_xml_output=replace(@v_xml_output,'¬','&#172;')
				 set @v_xml_output=replace(@v_xml_output,'®','&#174;')
				 set @v_xml_output=replace(@v_xml_output,'¯','&#175;')
				 set @v_xml_output=replace(@v_xml_output,'°','&#176;')
				 set @v_xml_output=replace(@v_xml_output,'±','&#177;')
				 set @v_xml_output=replace(@v_xml_output,'²','&#178;')
				 set @v_xml_output=replace(@v_xml_output,'³','&#179;')
				 set @v_xml_output=replace(@v_xml_output,'´','&#180;')
				 set @v_xml_output=replace(@v_xml_output,'µ','&#181;')
				 set @v_xml_output=replace(@v_xml_output,'¶','&#182;')
				 set @v_xml_output=replace(@v_xml_output,'·','&#183;')
				 set @v_xml_output=replace(@v_xml_output,'¸','&#184;')
				 set @v_xml_output=replace(@v_xml_output,'¹','&#185;')
				 set @v_xml_output=replace(@v_xml_output,'º','&#186;')
				 set @v_xml_output=replace(@v_xml_output,'»','&#187;')
				 set @v_xml_output=replace(@v_xml_output,'¼','&#188;')
				 set @v_xml_output=replace(@v_xml_output,'½','&#189;')
				 set @v_xml_output=replace(@v_xml_output,'¾','&#190;')
				 set @v_xml_output=replace(@v_xml_output,'¿','&#191;')
				 --added by Nishith

				    --set @v_xml_output=replace(@v_xml_output,'&quote', '&#39;')
					set @v_xml_output=replace(@v_xml_output,'À','&#192;')
					set @v_xml_output=replace(@v_xml_output,'Á','&#193;')
					set @v_xml_output=replace(@v_xml_output,'Â','&#194;')
					set @v_xml_output=replace(@v_xml_output,'Ã','&#195;')
					set @v_xml_output=replace(@v_xml_output,'Ä','&#196;')
					set @v_xml_output=replace(@v_xml_output,'Å','&#197;')
					set @v_xml_output=replace(@v_xml_output,'Æ','&#198;')
					set @v_xml_output=replace(@v_xml_output,'Ç','&#199;')
					set @v_xml_output=replace(@v_xml_output,'È','&#200;')
					set @v_xml_output=replace(@v_xml_output,'É','&#201;')
					set @v_xml_output=replace(@v_xml_output,'Ê','&#202;')
					set @v_xml_output=replace(@v_xml_output,'Ë','&#203;')
					set @v_xml_output=replace(@v_xml_output,'Ì','&#204;')
					set @v_xml_output=replace(@v_xml_output,'Í','&#205;')
					set @v_xml_output=replace(@v_xml_output,'Î','&#206;')
					set @v_xml_output=replace(@v_xml_output,'Ï','&#207;')
					set @v_xml_output=replace(@v_xml_output,'Ð','&#208;')
					set @v_xml_output=replace(@v_xml_output,'Ñ','&#209;')
					set @v_xml_output=replace(@v_xml_output,'Ò','&#210;')
					set @v_xml_output=replace(@v_xml_output,'Ó','&#211;')
					set @v_xml_output=replace(@v_xml_output,'Ô','&#212;')
					set @v_xml_output=replace(@v_xml_output,'Õ','&#213;')
					set @v_xml_output=replace(@v_xml_output,'Ö','&#214;')
					set @v_xml_output=replace(@v_xml_output,'×','&#215;')
					set @v_xml_output=replace(@v_xml_output,'Ø','&#216;')
					set @v_xml_output=replace(@v_xml_output,'Ù','&#217;')
					set @v_xml_output=replace(@v_xml_output,'Ú','&#218;')
					set @v_xml_output=replace(@v_xml_output,'Û','&#219;')
					set @v_xml_output=replace(@v_xml_output,'Ü','&#220;')
					set @v_xml_output=replace(@v_xml_output,'Ý','&#221;')
					set @v_xml_output=replace(@v_xml_output,'Þ','&#222;')
					set @v_xml_output=replace(@v_xml_output,'ß','&#223;')
					set @v_xml_output=replace(@v_xml_output,'à','&#224;')
					set @v_xml_output=replace(@v_xml_output,'á','&#225;')
					set @v_xml_output=replace(@v_xml_output,'â','&#226;')
					set @v_xml_output=replace(@v_xml_output,'ã','&#227;')
					set @v_xml_output=replace(@v_xml_output,'ä','&#228;')
					set @v_xml_output=replace(@v_xml_output,'å','&#229;')
					set @v_xml_output=replace(@v_xml_output,'æ','&#230;')
					set @v_xml_output=replace(@v_xml_output,'ç','&#231;')
					set @v_xml_output=replace(@v_xml_output,'è','&#232;')
					set @v_xml_output=replace(@v_xml_output,'é','&#233;')
					set @v_xml_output=replace(@v_xml_output,'ê','&#234;')
					set @v_xml_output=replace(@v_xml_output,'ë','&#235;')
					set @v_xml_output=replace(@v_xml_output,'ì','&#236;')
					set @v_xml_output=replace(@v_xml_output,'í','&#237;')
					set @v_xml_output=replace(@v_xml_output,'î','&#238;')
					set @v_xml_output=replace(@v_xml_output,'ï','&#239;')
					set @v_xml_output=replace(@v_xml_output,'ð','&#240;')
					set @v_xml_output=replace(@v_xml_output,'ñ','&#241;')
					set @v_xml_output=replace(@v_xml_output,'ò','&#242;')
					set @v_xml_output=replace(@v_xml_output,'ó','&#243;')
					set @v_xml_output=replace(@v_xml_output,'ô','&#244;')
					set @v_xml_output=replace(@v_xml_output,'õ','&#245;')
					set @v_xml_output=replace(@v_xml_output,'ö','&#246;')
					set @v_xml_output=replace(@v_xml_output,'÷','&#247;')
					set @v_xml_output=replace(@v_xml_output,'ø','&#248;')
					set @v_xml_output=replace(@v_xml_output,'ù','&#249;')
					set @v_xml_output=replace(@v_xml_output,'ú','&#250;')
					set @v_xml_output=replace(@v_xml_output,'û','&#251;')
					set @v_xml_output=replace(@v_xml_output,'ü','&#252;')
					set @v_xml_output=replace(@v_xml_output,'ý','&#253;')
					set @v_xml_output=replace(@v_xml_output,'þ','&#254;')
					set @v_xml_output=replace(@v_xml_output,'ÿ','&#255;')
					set @v_xml_output=replace(@v_xml_output,'Œ','&#338;')
					set @v_xml_output=replace(@v_xml_output,'œ','&#339;')
					set @v_xml_output=replace(@v_xml_output,'Š','&#352;')
					set @v_xml_output=replace(@v_xml_output,'š','&#353;')
					set @v_xml_output=replace(@v_xml_output,'Ÿ','&#376;')
					set @v_xml_output=replace(@v_xml_output,'ƒ','&#402;')
				 --added by Nishith done

				 set @v_xml_output=replace(@v_xml_output,'–','&#8211;')
				 set @v_xml_output=replace(@v_xml_output,'—','&#8212;')
				 set @v_xml_output=replace(@v_xml_output,'‘','&#8216;')
				 set @v_xml_output=replace(@v_xml_output,'’','&#8217;')
				 set @v_xml_output=replace(@v_xml_output,'‚','&#8218;')
				 set @v_xml_output=replace(@v_xml_output,'“','&#8220;')
				 set @v_xml_output=replace(@v_xml_output,'”','&#8221;')
				 set @v_xml_output=replace(@v_xml_output,'„','&#8222;')
				 set @v_xml_output=replace(@v_xml_output,'†','&#8224;')
				 set @v_xml_output=replace(@v_xml_output,'‡','&#8225;')
				 set @v_xml_output=replace(@v_xml_output,'•','&#8226;')
				 set @v_xml_output=replace(@v_xml_output,'…','&#8230;')
				 set @v_xml_output=replace(@v_xml_output,'‰','&#8240;')
				 set @v_xml_output=replace(@v_xml_output,'€','&#8364;')
				 set @v_xml_output=replace(@v_xml_output,'™','&#8482;')
				 set @v_xml_output=replace(@v_xml_output,'”','&#8221;')				 
				 --Added by DOHATEC on 21 Jan 2014
                 set @v_xml_output=replace(@v_xml_output,'ˆ','&#710;')
				 --set @v_xml_output=replace(@v_xml_output,'`','&#710;')
				 set @v_xml_output=replace(@v_xml_output,'›','&#62;')
				set @v_xml_output=replace(@v_xml_output,' ','&#32;')
				set @v_xml_output=replace(@v_xml_output,'­','&#173;')
				set @v_xml_output=replace(@v_xml_output,'€˜','&#8364;')		
 Return @v_xml_output  
  
END



GO
