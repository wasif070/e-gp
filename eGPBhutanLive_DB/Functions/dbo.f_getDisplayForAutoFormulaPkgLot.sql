SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		G. M. ROKIBUL HASAN
-- Create date: 31-May-2016
-- Description:	For display Name from N number of forms
-- =============================================
CREATE FUNCTION [dbo].[f_getDisplayForAutoFormulaPkgLot]
(
	@v_tenderID INT,
	@v_PkgLotId INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @v_displayAutoFormula VARCHAR(MAX),@v_displayAutoFormulaForSalvageForms VARCHAR(MAX),
			@v_excludeFormId INT

	SELECT	@v_excludeFormId = tenderFormId
	FROM	tbl_TenderForms tf
			INNER JOIN tbl_TenderSection ts ON ts.tenderSectionId = tf.tenderSectionId
			--INNER JOIN tbl_epwtwobmapping epw2b ON epw2b.formId = tf.templateFormId
			INNER JOIN tbl_epwtwobmapping epw2b ON epw2b.formId = CASE WHEN tf.templateFormId = 0 and tf.FormType IS NOT NULL AND tf.formStatus <> 'createp' THEN  CAST(SUBSTRING(tf.FormType,32,LEN(tf.FormType)) AS INT) WHEN (tf.formStatus IS NULL OR tf.formStatus = 'cp') AND tf.templateFormId > 0 AND tf.FormType IS NULL THEN tf.templateFormId END
			INNER JOIN tbl_TenderStd tstd on tstd.templateId = epw2b.stdId
						AND tstd.tenderStdId = ts.tenderStdId
						AND tstd.tenderId = @v_tenderID
	SELECT	@v_displayAutoFormula =	(	SELECT autoFormulaDisplay + '+'
										FROM (	SELECT	tcs.columnHeader +'_'+RTRIM(LTRIM(CONVERT(VARCHAR(10),TC.rowId))) AS autoFormulaDisplay
												FROM	tbl_TenderCells TC
														INNER JOIN tbl_TenderGrandSumDetail TGS ON TGS.tenderTableId = TC.tenderTableId
																	AND TGS.tenderFormId = CASE WHEN @v_excludeFormId >0 THEN @v_excludeFormId ELSE TGS.tenderFormId END
														INNER JOIN tbl_TenderGrandSum TG ON TG.tenderSumId = TGS.tenderSumId
																	AND tenderId = @v_tenderID AND pkgLotId=@v_PkgLotId
														INNER JOIN tbl_TenderColumns tcs ON tcs.columnId = tc.columnId
																	AND tcs.tenderTableId = TGS.tenderTableId
																	AND dataType IN (3,4,8,13) -- 3=Money Positive,4=Numeric,8=Money All,13=Money Positive(3 digits after decimal)
																	AND filledBy = 3 -- Auto
                                                                                                                                        AND columnType = 7 -- Total Rate Column type
                                                                                                                                         AND TGS.tenderFormId NOT IN (
                                                                                                                                                   select tf.tenderFormId from tbl_TenderForms tf INNER JOIN tbl_TenderGrandSumDetail tgs
                                                                                                                                                   ON tf.tenderFormId=tgs.tenderFormId
                                                                                                                                                   INNER JOIN tbl_TenderGrandSum tg
                                                                                                                                                   ON tg.tenderSumId=tgs.tenderSumId
                                                                                                                                                   where tg.tenderId=@v_tenderID AND tg.pkgLotId=@v_PkgLotId and tf.FormType = 'BOQsalvage'

                                                                                                                                                                 )
												WHERE	tc.templateTableId != 0
											) AS frm
										FOR XML PATH('')
									)

                SET @v_displayAutoFormula=  CASE WHEN  @v_displayAutoFormula IS NULL  THEN '' ELSE '_'+REPLACE(REPLACE(SUBSTRING(@v_displayAutoFormula,1,LEN(@v_displayAutoFormula)-1),'&lt;','<'),'&gt;','>') END
	-- Return the result of the function

         SELECT	@v_displayAutoFormulaForSalvageForms =	(	SELECT autoFormulaDisplay + '+'
										FROM (	SELECT	tcs.columnHeader +'_'+RTRIM(LTRIM(CONVERT(VARCHAR(10),TC.rowId))) AS autoFormulaDisplay
												FROM	tbl_TenderCells TC
														INNER JOIN tbl_TenderGrandSumDetail TGS ON TGS.tenderTableId = TC.tenderTableId
																	AND TGS.tenderFormId = CASE WHEN @v_excludeFormId >0 THEN @v_excludeFormId ELSE TGS.tenderFormId END
														INNER JOIN tbl_TenderGrandSum TG ON TG.tenderSumId = TGS.tenderSumId
																	AND tenderId = @v_tenderID AND pkgLotId=@v_PkgLotId
														INNER JOIN tbl_TenderColumns tcs ON tcs.columnId = tc.columnId
																	AND tcs.tenderTableId = TGS.tenderTableId
																	AND dataType IN (3,4,8,13) -- 3=Money Positive,4=Numeric,8=Money All,13=Money Positive(3 digits after decimal)
																	AND filledBy = 3 -- Auto
                                                                                                                                        AND columnType = 7 -- Total Rate Column type
                                                                                                                                         AND TGS.tenderFormId  IN (
                                                                                                                                                   select tf.tenderFormId from tbl_TenderForms tf INNER JOIN tbl_TenderGrandSumDetail tgs
                                                                                                                                                   ON tf.tenderFormId=tgs.tenderFormId
                                                                                                                                                   INNER JOIN tbl_TenderGrandSum tg
                                                                                                                                                   ON tg.tenderSumId=tgs.tenderSumId
                                                                                                                                                   where tg.tenderId=@v_tenderID AND tg.pkgLotId=@v_PkgLotId and tf.FormType = 'BOQsalvage'

                                                                                                                                                                 )
												WHERE	tc.templateTableId != 0
											) AS frm
										FOR XML PATH('')
									)

         SET @v_displayAutoFormulaForSalvageForms=  CASE WHEN  @v_displayAutoFormulaForSalvageForms IS NULL  THEN '' ELSE '_'+REPLACE(REPLACE(SUBSTRING(@v_displayAutoFormulaForSalvageForms,1,LEN(@v_displayAutoFormulaForSalvageForms)-1),'&lt;','<'),'&gt;','>') END

       SET @v_displayAutoFormula= CASE WHEN @v_displayAutoFormulaForSalvageForms<>'' THEN @v_displayAutoFormula+'-'+@v_displayAutoFormulaForSalvageForms ELSE LTRIM(RTRIM(@v_displayAutoFormula)) END

	RETURN @v_displayAutoFormula

END



GO
