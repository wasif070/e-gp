SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Alter date: <Alter Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[getEmpRoles](@userId int)

RETURNS varchar(200) 
AS
begin
declare @v_FormList_Vc3 varchar(2000)='0'
 
 select @v_FormList_Vc3 = COALESCE(@v_FormList_Vc3+',', ' ') +
  convert(varchar(20), procurementRole)
   from tbl_EmployeeRoles e,tbl_EmployeeMaster em,tbl_procurementrole pm 
    where   e.employeeId=em.employeeId and 
         em.userid=@userId and e.procurementroleid=pm.procurementroleid
	
return replace(@v_FormList_Vc3,'0,','')
	 
end



GO
