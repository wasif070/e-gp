SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		G. M. Rokibul Hasan
-- Create date: 06-August-2015
-- Description:	TO Get Organization , Ministry Or Division ID 
-- =============================================
CREATE FUNCTION [dbo].[OrganizationDivisionMinistry] (	@userid int)
RETURNS @v_Results_outTbl TABLE (officeid INT,Org_ID INT , Division_ID INT, Ministry_ID INT)
AS
BEGIN

declare @officeid as int ,@departmentId as int ,@Division as int , @Ministry as int ;
select top 1 @officeid= officeId from tbl_EmployeeOffices where employeeId in 
(select employeeId from tbl_EmployeeMaster where userId = @userid ) 
order by employeeOfficeId desc

select @departmentId = departmentId from tbl_OfficeMaster where officeId =@officeid
select @Division = departmentId  from tbl_DepartmentMaster where departmentId = (select parentDepartmentId from tbl_DepartmentMaster where departmentId=@departmentId) and departmentType = 'Division'
select @Ministry = ISNULL(departmentId,0)  from tbl_DepartmentMaster where departmentId = (select parentDepartmentId from tbl_DepartmentMaster where departmentId=@departmentId) and departmentType = 'Ministry'
SELECT @Division = ISNULL(@Division,0)
if @Division != 0
select @Ministry = ISNULL(departmentId,0)  from tbl_DepartmentMaster where departmentId = (select parentDepartmentId from tbl_DepartmentMaster where departmentId=@Division) and departmentType = 'Ministry'

INSERT INTO @v_Results_outTbl
select @officeid , @departmentId , @Division , @Ministry 

RETURN
END




GO
