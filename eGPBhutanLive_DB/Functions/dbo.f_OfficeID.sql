SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[f_OfficeID]
(
	@userid int
)
RETURNS int
AS
BEGIN
DECLARE @officeid int =0
select top 1 @officeid= officeId from tbl_EmployeeOffices where employeeId in 
(select employeeId from tbl_EmployeeMaster where userId = @userid ) 
order by employeeOfficeId desc
RETURN @officeid
END



GO
