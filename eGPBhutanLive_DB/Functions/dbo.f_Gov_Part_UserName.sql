SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-------------------------------------------------------------------------------- 
-- 
-- Purpose: Return Goverment/Partment Admin username based on userid and sessionStartTime
-- 
-- 
-- Author: DARSHAN SHAH 
-- Date: 04-May-2012
-- 
-- Last Modified: 
-- Modified By: 
-- Date  
-- Modification: 
-------------------------------------------------------------------------------- 
 
 
CREATE FUNCTION [dbo].[f_Gov_Part_UserName](@v_GovUserId_inInt INT, @sessionStartDT datetime, @userTypeID int) RETURNS VARCHAR(MAX) 
AS 
 
BEGIN  

    Declare @v_GovUserName varchar(500) 

	IF @sessionStartDT IS NULL OR @sessionStartDT <> ''
	BEGIN
		SET @sessionStartDT  = GETDATE()
	END

    IF @userTypeID = 3 OR @userTypeID = 17
    BEGIN
        SELECT @v_GovUserName = employeeName from tbl_employeeMaster where userid = @v_GovUserId_inInt
          
        IF (SELECT COUNT(*) FROM tbl_EmployeeTrasfer where action = 'Transfer' AND userId = @v_GovUserId_inInt) > 0
        BEGIN
                SELECT    @v_GovUserName = employeeName
                FROM    tbl_EmployeeTrasfer   
                WHERE    action = 'Transfer'
                        AND userId = @v_GovUserId_inInt
                        and convert(CHAR(20),@sessionStartDT,121) >= convert(CHAR(20),transferDt,121)
        END
    END
    ELSE IF @userTypeID = 6 OR @userTypeID = 7 OR @userTypeID = 15
    BEGIN
        SELECT @v_GovUserName = fullName from tbl_PartnerAdmin where userid = @v_GovUserId_inInt
          
        IF (SELECT COUNT(*) FROM tbl_PartnerAdminTransfer where userId = @v_GovUserId_inInt) > 0
        BEGIN
                SELECT    @v_GovUserName = fullName
                FROM    tbl_PartnerAdminTransfer   
                WHERE    userId = @v_GovUserId_inInt
                        and convert(CHAR(20),@sessionStartDT,121) >= convert(CHAR(20),transeredDt,121)
        END
    END   
   
Return @v_GovUserName 
 
END



GO
