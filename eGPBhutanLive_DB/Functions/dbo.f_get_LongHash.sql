SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------------------------------------
--
-- Purpose: get HASHBYTES which input VARBINARY length is more than 8000
--
--
-- Author: G. M. Rokibul Hasan
-- Date: 15-12-2014
--
-- Last Modified:
-- Modified By: 
-- Date :
-- Modification: 
--------------------------------------------------------------------------------


CREATE FUNCTION [dbo].[f_get_LongHash](@data VARCHAR(MAX))
RETURNS VARBINARY(MAX)
WITH RETURNS NULL ON NULL INPUT
AS
BEGIN
    DECLARE @res VARBINARY(MAX) = 0x
    DECLARE @position INT = 1, @len INT = DATALENGTH(@data)

    WHILE 1 = 1
    BEGIN
        SET @res = HASHBYTES('SHA1',(@res + HASHBYTES('SHA1', SUBSTRING(@data, @position, 7995))))
        SET @position = @position+7995
        IF @Position > @len 
          BREAK
    END
    
    RETURN @res
END



GO
