SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[f_UserWiseDevisionID]
(
	@userid int
)
RETURNS int
AS
BEGIN
declare @Division int
select @Division = departmentId  from tbl_DepartmentMaster 
where departmentId = (select parentDepartmentId from tbl_DepartmentMaster 
where departmentId= dbo.f_UserWiseOrganizationID(@userid)) and departmentType = 'Division'
RETURN ISNULL(@Division,0)
END



GO
