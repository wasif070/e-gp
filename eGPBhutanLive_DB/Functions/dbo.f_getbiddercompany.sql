SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Returns a trimmed string.
--
--
-- Author: Karan
-- Date: 28-10-2010
--
-- Last Modified:
-- Modified By:
-- Date 
-- Modification:
--------------------------------------------------------------------------------


CREATE FUNCTION [dbo].[f_getbiddercompany](@v_UserId_inInt INT) RETURNS VARCHAR(MAX)
AS

BEGIN 


 Declare @v_CompanyId int, @v_CompanyName varchar(max)
	Select @v_CompanyId = TM.companyId from tbl_LoginMaster LM 
	Inner Join tbl_TendererMaster TM On LM.userId = TM.userId Where LM.userId=@v_UserId_inInt 

	If @v_CompanyId=1
	Begin
		-- // Take the First name and last name from table - tbl_CompanyMaster
		Select @v_CompanyName= (TM.firstName + IsNull(' '+ TM.lastName,'')) from tbl_LoginMaster LM 
			Inner Join tbl_TendererMaster TM On LM.userId = TM.userId
			Where LM.userId=@v_UserId_inInt 
	End
	Else
	Begin
		-- // Take the company name from table - tbl_CompanyMaster
		Select @v_CompanyName=companyName from tbl_LoginMaster LM 
			Inner Join tbl_TendererMaster TM On LM.userId = TM.userId	
			Inner join tbl_CompanyMaster CM On TM.companyId=CM.companyId
			Where LM.userId=@v_UserId_inInt 
	End
	
	Return @v_CompanyName

END



GO
