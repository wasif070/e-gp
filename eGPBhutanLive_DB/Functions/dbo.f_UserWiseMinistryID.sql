SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[f_UserWiseMinistryID]
(
	@userid int
)
RETURNS int
AS
BEGIN
declare @Ministry int

if (dbo.f_UserWiseDevisionID(@userid) = 0 )
BEGIN
select @Ministry = ISNULL(departmentId,0)  from tbl_DepartmentMaster 
where departmentId = (select parentDepartmentId from tbl_DepartmentMaster 
where departmentId=dbo.f_UserWiseOrganizationID(@userid)) and departmentType = 'Ministry'
END
ELSE
BEGIN
select @Ministry = ISNULL(departmentId,0)  from tbl_DepartmentMaster 
where departmentId = (select parentDepartmentId from tbl_DepartmentMaster 
where departmentId=dbo.f_UserWiseDevisionID(@userid)) and departmentType = 'Ministry'
END

RETURN ISNULL(@Ministry,0)
END



GO
