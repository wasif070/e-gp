SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Returns a trimmed string.
--
--
-- Author: Karan
-- Date: 05-01-2011
--
-- Last Modified:
-- Modified By:
-- Date 
-- Modification:
--------------------------------------------------------------------------------


CREATE FUNCTION [dbo].[f_gettendersecuritydate](@v_TenderId_inInt INT) RETURNS DATETIME
AS

BEGIN 
Declare @v_TenderSecurityDt_Dt Datetime
 Select @v_TenderSecurityDt_Dt = Case 
	When (Select Max(tenderSecNewDt) from tbl_TenderValidityExtDate Where tenderId=@v_TenderId_inInt And extStatus='approved' And tenderSecNewDt is not null And tenderSecNewDt>'1900-01-01') is not null
	Then (Select Max(tenderSecNewDt) from tbl_TenderValidityExtDate Where tenderId=@v_TenderId_inInt And extStatus='approved' And tenderSecNewDt is not null And tenderSecNewDt>'1900-01-01')
	When (Select Max(tenderSecLastDt) from tbl_TenderValidityExtDate Where tenderId=@v_TenderId_inInt And extStatus='approved' And tenderSecLastDt is not null And tenderSecLastDt>'1900-01-01') is not null
	Then (Select Max(tenderSecLastDt) from tbl_TenderValidityExtDate Where tenderId=@v_TenderId_inInt And extStatus='approved' And tenderSecLastDt is not null And tenderSecLastDt>'1900-01-01')
	When Exists (Select tenderSecurityDt From tbl_TenderDetails Where tenderId=@v_TenderId_inInt)
	Then (Select tenderSecurityDt From tbl_TenderDetails Where tenderId=@v_TenderId_inInt)	
 End 		 
 
 Return @v_TenderSecurityDt_Dt
	 

END



GO
