SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		G. M. Rokibul Hasan
-- Create date: 9-9-2015
-- Description:	To Compare in Financial Delegation for The Project Cost
-- =============================================
CREATE FUNCTION [dbo].[f_get_ProjectCostWiseCheck]
(
	 @TENDERID INT ,
	 @MinProjectValue [numeric](19, 2),
	 @MaxProjectValue [numeric](19, 2)
)
RETURNS int
AS
BEGIN
DECLARE @ResultVal int
SELECT @ResultVal = (CASE	WHEN
								(SELECT procurementRole FROM tbl_ProcurementRole WHERE procurementRoleId in (
								(select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId =@TENDERID))) != 'PD'
								THEN 1
							ELSE 
								CASE WHEN	(select ISNULL(SUM(projectCost),0) from 
											( select Tenderid, appId from tbl_TenderMaster where tenderId = @TENDERID) tm  
											inner  join (select projectId,appId from tbl_AppMaster) am  on am.appId=tm.appId  
											left join tbl_ProjectMaster pm on pm.projectId = am.projectId) between @MinProjectValue and @MaxProjectValue
											THEN 1
									ELSE 0	
								END
	
					END	)								
	RETURN @ResultVal

END





GO
