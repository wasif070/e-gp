SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Returns a string after trimming it from left side.
--
--
-- Author: Karan
-- Date: 25-10-2010
--
-- Last Modified:
-- Modified By:
-- Date 25-10-2010
-- Modification:
--------------------------------------------------------------------------------

CREATE FUNCTION [dbo].[f_trimleft](@v_String_inVc VARCHAR(MAX)) RETURNS VARCHAR(MAX)
AS
BEGIN
DECLARE 
 @v_trimchars_Vc VARCHAR(10)
 
 SET @v_trimchars_Vc = CHAR(9)+CHAR(10)+CHAR(13)+CHAR(32)
 IF @v_String_inVc LIKE '[' + @v_trimchars_Vc + ']%' 
  SET @v_String_inVc = SUBSTRING(@v_String_inVc, PATINDEX('%[^' + @v_trimchars_Vc + ']%', @v_String_inVc), 8000)

RETURN @v_String_inVc
END



GO
