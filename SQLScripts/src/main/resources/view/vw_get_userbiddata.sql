SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_get_userbiddata]
AS
SELECT DISTINCT
                      TOP (100) PERCENT dbo.tbl_TenderBidForm.userId, dbo.tbl_TenderBidTable.tenderTableId, dbo.tbl_TenderBidForm.tenderFormId,
                      dbo.tbl_TenderBidForm.bidId, dbo.tbl_TenderBidPlainData.tenderColId AS columnId, dbo.tbl_TenderBidPlainData.cellValue,
                      dbo.tbl_TenderBidPlainData.rowId, dbo.tbl_TenderBidTable.bidTableId
FROM         dbo.tbl_TenderBidTable INNER JOIN
                      dbo.tbl_TenderBidForm ON dbo.tbl_TenderBidTable.bidId = dbo.tbl_TenderBidForm.bidId INNER JOIN
                      dbo.tbl_TenderBidPlainData ON dbo.tbl_TenderBidTable.bidTableId = dbo.tbl_TenderBidPlainData.bidTableId AND
                      dbo.tbl_TenderBidTable.bidTableId = dbo.tbl_TenderBidPlainData.bidTableId INNER JOIN
                      dbo.tbl_FinalSubmission ON dbo.tbl_TenderBidForm.tenderId = dbo.tbl_FinalSubmission.tenderId AND
                      dbo.tbl_TenderBidForm.userId = dbo.tbl_FinalSubmission.userId AND dbo.tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
ORDER BY dbo.tbl_TenderBidForm.userId

GO