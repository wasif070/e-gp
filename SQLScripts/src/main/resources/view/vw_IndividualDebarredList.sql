/****** Object:  View [dbo].[vw_IndividualDebarredList]    Script Date: 29-Jun-16 10:55:42 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_IndividualDebarredList]
AS
SELECT DISTINCT 
                         TOP (100) PERCENT dbp.ProcurementCategory, dbp.UserId, dbp.CompanyID, tm.firstName, tm.firstName + tm.lastName AS Name, tm.state AS StateName, 
                         dbp.DebarredUpto, dbp.DebarredFrom, dbp.Comments, dbp.DebarredReason
FROM            dbo.tbl_DebarredBiddingPermission AS dbp LEFT OUTER JOIN
                         dbo.tbl_TendererMaster AS tm ON tm.userId = dbp.UserId AND tm.companyId = dbp.CompanyID
WHERE        (dbp.CompanyID = 1) AND (dbp.IsActive = 1)
ORDER BY dbp.UserId

GO