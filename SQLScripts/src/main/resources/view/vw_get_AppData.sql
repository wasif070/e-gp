SELECT        am.appId, am.appCode, dm.departmentName, em.employeeName, sm.stateName, ap.procurementnature, ap.packageNo, ap.packageDesc, am.projectName, 
                         am.financialYear, CONVERT(int, dm.departmentId) AS departmentId, em.employeeId, ap.cpvCode, ap.procurementType, ap.estimatedCost, ap.pkgEstCost, am.officeId, 
                         am.budgetType, pm.procurementMethod, om.officeName, ap.appStatus, ap.packageId, ap.workflowStatus, am.projectId, dm.departmentType, 
                         dm.parentDepartmentId
FROM            dbo.tbl_AppMaster AS am INNER JOIN
                         dbo.tbl_AppPackages AS ap ON am.appId = ap.appId INNER JOIN
                         dbo.tbl_ProcurementMethod AS pm ON ap.procurementMethodId = pm.procurementMethodId INNER JOIN
                         dbo.tbl_OfficeMaster AS om ON am.officeId = om.officeId INNER JOIN
                         dbo.tbl_EmployeeMaster AS em ON am.employeeId = em.employeeId INNER JOIN
                         dbo.tbl_DesignationMaster AS ds ON em.designationId = ds.designationId INNER JOIN
                         dbo.tbl_DepartmentMaster AS dm ON ds.departmentid = dm.departmentId INNER JOIN
                         dbo.tbl_StateMaster AS sm ON om.stateId = sm.stateId