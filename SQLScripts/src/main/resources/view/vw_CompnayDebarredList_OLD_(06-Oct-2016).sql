/****** Object:  View [dbo].[vw_CompnayDebarredList]    Script Date: 29-Jun-16 10:52:13 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_CompnayDebarredList]
AS
SELECT DISTINCT 
                         TOP (100) PERCENT dbp.ProcurementCategory, dbp.UserId, dbp.CompanyID, cm.companyName, cm.regOffState, dbp.DebarredUpto, dbp.DebarredFrom, 
                         dbp.Comments, dbp.DebarredReason
FROM            dbo.tbl_DebarredBiddingPermission AS dbp LEFT OUTER JOIN
                         dbo.tbl_CompanyMaster AS cm ON cm.userId = dbp.UserId AND cm.companyId = dbp.CompanyID
WHERE        (dbp.CompanyID <> 1) AND (dbp.IsActive = 1)
ORDER BY dbp.UserId

GO