SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_empfinance_power]
AS
SELECT     dbo.tbl_ProcurementNature.procurementNature, dbo.tbl_EmployeeRoles.departmentId, dbo.tbl_ProcurementRole.procurementRoleId,
                      dbo.tbl_ProcurementRole.procurementRole, dbo.tbl_EmployeeRoles.employeeId, dbo.tbl_EmployeeFinancialPower.operator,
                      dbo.tbl_EmployeeFinancialPower.amount, ISNULL
                          ((SELECT     CASE WHEN b.procurementMethodId = 0 THEN '-' ELSE b.procurementMethod END AS procurementMethod
                              FROM         dbo.tbl_ProcurementMethod AS b
                              WHERE     (procurementMethodId = dbo.tbl_EmployeeFinancialPower.procurementMethodId)), '-') AS procurementMethod, dbo.tbl_BudgetType.budgetType,
                      dbo.tbl_EmployeeFinancialPower.budgetTypeId, dbo.tbl_EmployeeFinancialPower.employeeFinPowerId, dbo.tbl_EmployeeRoles.employeeRoleId,
                      dbo.tbl_EmployeeFinancialPower.procurementMethodId, dbo.tbl_EmployeeFinancialPower.procurementNatureId
FROM         dbo.tbl_ProcurementRole INNER JOIN
                      dbo.tbl_EmployeeRoles ON dbo.tbl_ProcurementRole.procurementRoleId = dbo.tbl_EmployeeRoles.procurementRoleId INNER JOIN
                      dbo.tbl_EmployeeFinancialPower ON dbo.tbl_EmployeeRoles.employeeId = dbo.tbl_EmployeeFinancialPower.employeeId INNER JOIN
                      dbo.tbl_ProcurementNature ON dbo.tbl_EmployeeFinancialPower.procurementNatureId = dbo.tbl_ProcurementNature.procurementNatureId INNER JOIN
                      dbo.tbl_BudgetType ON dbo.tbl_EmployeeFinancialPower.budgetTypeId = dbo.tbl_BudgetType.budgetTypeId AND
                      dbo.tbl_EmployeeFinancialPower.budgetTypeId = dbo.tbl_BudgetType.budgetTypeId

GO
