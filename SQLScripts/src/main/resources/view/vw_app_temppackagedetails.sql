SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[vw_app_temppackagedetails]
AS
SELECT        dbo.tbl_AppMaster.financialYear, dbo.tbl_AppMaster.budgetType, dbo.tbl_AppMaster.projectName, dbo.tbl_TempAppPackages.procurementnature,
                         dbo.tbl_TempAppPackages.servicesType, dbo.tbl_TempAppPackages.packageNo, dbo.tbl_TempAppPackages.packageDesc,
                         dbo.tbl_TempAppPackages.allocateBudget, dbo.tbl_TempAppPackages.pkgEstCost, dbo.tbl_TempAppPackages.isPQRequired,
                         dbo.tbl_TempAppPackages.reoiRfaRequired, dbo.tbl_TempAppPackages.procurementMethodId, dbo.tbl_TempAppPackages.procurementType,
                         dbo.tbl_TempAppPackages.sourceOfFund, dbo.tbl_TempAppPqTenderDates.advtDt, dbo.tbl_TempAppPqTenderDates.subDt,
                         dbo.tbl_TempAppPqTenderDates.tenderAdvertDt, dbo.tbl_TempAppPqTenderDates.tenderSubDt, dbo.tbl_TempAppPqTenderDates.tenderOpenDt,
                         dbo.tbl_TempAppPqTenderDates.tenderNoaIssueDt, dbo.tbl_TempAppPqTenderDates.tenderContractSignDt, dbo.tbl_TempAppPqTenderDates.tenderContractCompDt,
                         dbo.tbl_EmployeeMaster.employeeName AS PE, dbo.tbl_StateMaster.stateName AS district, dbo.tbl_TempAppPqTenderDates.techSubCmtRptDt,
                         dbo.tbl_TempAppPqTenderDates.tenderEvalRptDt, dbo.tbl_TempAppPqTenderDates.tenderEvalRptAppDt, dbo.tbl_TempAppPqTenderDates.tenderContractAppDt,
                         dbo.tbl_TempAppPqTenderDates.reoiReceiptDt, dbo.tbl_TempAppPqTenderDates.rfpTechEvalDt, dbo.tbl_TempAppPqTenderDates.rfpFinancialOpenDt,
                         dbo.tbl_TempAppPqTenderDates.rfpNegCompDt, dbo.tbl_TempAppPqTenderDates.rfpContractAppDt, dbo.tbl_TempAppPqTenderDates.rfaAdvertDt,
                         dbo.tbl_TempAppPqTenderDates.rfaReceiptDt, dbo.tbl_TempAppPqTenderDates.rfaEvalDt, dbo.tbl_TempAppPqTenderDates.rfaInterviewDt,
                         dbo.tbl_TempAppPqTenderDates.rfaFinalSelDt, dbo.tbl_TempAppPqTenderDates.rfaEvalRptSubDt, dbo.tbl_TempAppPqTenderDates.rfaAppConsultantDt,
                         dbo.tbl_TempAppPqTenderDates.actSubDt, dbo.tbl_TempAppPqTenderDates.actAdvtDt, dbo.tbl_TempAppPqTenderDates.actEvalRptDt,
                         dbo.tbl_TempAppPqTenderDates.actAppLstDt, dbo.tbl_TempAppPqTenderDates.actTenderAdvertDt, dbo.tbl_TempAppPqTenderDates.actTenderSubDt,
                         dbo.tbl_TempAppPqTenderDates.actTenderOpenDt, dbo.tbl_TempAppPqTenderDates.actTechSubCmtRptDt, dbo.tbl_TempAppPqTenderDates.actTenderEvalRptDt,
                         dbo.tbl_TempAppPqTenderDates.acttenderEvalRptAppDt, dbo.tbl_TempAppPqTenderDates.actTenderContractAppDt,
                         dbo.tbl_TempAppPqTenderDates.actTenderNoaIssueDt, dbo.tbl_TempAppPqTenderDates.actTenderContractSignDt,
                         dbo.tbl_TempAppPqTenderDates.actTenderContractCompDt, dbo.tbl_TempAppPqTenderDates.actReoiReceiptDt, dbo.tbl_TempAppPqTenderDates.actRfpTechEvalDt,
                         dbo.tbl_TempAppPqTenderDates.actRfpFinancialOpenDt, dbo.tbl_TempAppPqTenderDates.actRfpNegComDt, dbo.tbl_TempAppPqTenderDates.actRfpContractAppDt,
                         dbo.tbl_TempAppPqTenderDates.actRfaAdvertDt, dbo.tbl_TempAppPqTenderDates.actRfaReceiptDt, dbo.tbl_TempAppPqTenderDates.actRfaEvalDt,
                         dbo.tbl_TempAppPqTenderDates.actRfaInterviewDt, dbo.tbl_TempAppPqTenderDates.actRfaFinalSelDt, dbo.tbl_TempAppPqTenderDates.actRfaEvalRptSubDt,
                         dbo.tbl_TempAppPqTenderDates.actRfaAppConsultantDt, dbo.tbl_TempAppPqTenderDates.evalRptDt, dbo.tbl_TempAppPackages.packageId,
                         dbo.tbl_TempAppPqTenderDates.appLstDt, dbo.tbl_TempAppPqTenderDates.subDays, dbo.tbl_TempAppPqTenderDates.advtDays,
                         dbo.tbl_TempAppPqTenderDates.openDays, dbo.tbl_TempAppPqTenderDates.openDt, dbo.tbl_TempAppPqTenderDates.evalRptDays,
                         dbo.tbl_TempAppPqTenderDates.tenderAdvertDays, dbo.tbl_TempAppPqTenderDates.tenderSubDays, dbo.tbl_TempAppPqTenderDates.tenderOpenDays,
                         dbo.tbl_TempAppPqTenderDates.techSubCmtRptDays, dbo.tbl_TempAppPqTenderDates.tenderEvalRptdays,
                         dbo.tbl_TempAppPqTenderDates.tenderEvalRptAppDays, dbo.tbl_TempAppPqTenderDates.tenderContractAppDays,
                         dbo.tbl_TempAppPqTenderDates.tenderNoaIssueDays, dbo.tbl_TempAppPqTenderDates.tenderContractSignDays, dbo.tbl_TempAppPqTenderDates.rfaAdvertDays,
                         dbo.tbl_TempAppPqTenderDates.rfaReceiptDays, dbo.tbl_TempAppPqTenderDates.rfaEvalDays, dbo.tbl_TempAppPqTenderDates.rfaInterviewDays,
                         dbo.tbl_TempAppPqTenderDates.rfaFinalSelDays, dbo.tbl_TempAppPqTenderDates.rfaEvalRptSubDays, dbo.tbl_TempAppPqTenderDates.rfpContractAppDays,
                         dbo.tbl_TempAppPqTenderDates.rfpNegCompDays, dbo.tbl_TempAppPqTenderDates.rfpFinancialOpenDays, dbo.tbl_TempAppPqTenderDates.rfpTechEvalDays,
                         dbo.tbl_TempAppPqTenderDates.reoiReceiptDays, dbo.tbl_TempAppPqTenderDates.pqDtId, dbo.tbl_TempAppPackages.appId, dbo.tbl_AppMaster.projectId,
                         dbo.tbl_TempAppPackages.approvingAuthEmpId, dbo.tbl_TempAppPackages.pkgUrgency, dbo.tbl_TempAppPackages.cpvCode,
                         dbo.tbl_TempAppPackages.bidderCategory, dbo.tbl_TempAppPackages.workCategory, dbo.tbl_TempAppPackages.depoplanWork,
                         dbo.tbl_TempAppPackages.entrustingAgency, dbo.tbl_TempAppPackages.timeFrame, dbo.tbl_AppMaster.appCode,
                         dbo.tbl_TempAppPqTenderDates.tenderLetterIntentDt, dbo.tbl_TempAppPqTenderDates.tnderLetterIntentDays
FROM            dbo.tbl_AppMaster INNER JOIN
                         dbo.tbl_TempAppPackages ON dbo.tbl_AppMaster.appId = dbo.tbl_TempAppPackages.appId INNER JOIN
                         dbo.tbl_TempAppPqTenderDates ON dbo.tbl_AppMaster.appId = dbo.tbl_TempAppPqTenderDates.appid AND
                         dbo.tbl_TempAppPackages.packageId = dbo.tbl_TempAppPqTenderDates.packageId INNER JOIN
                         dbo.tbl_OfficeMaster ON dbo.tbl_AppMaster.officeId = dbo.tbl_OfficeMaster.officeId INNER JOIN
                         dbo.tbl_StateMaster ON dbo.tbl_OfficeMaster.stateId = dbo.tbl_StateMaster.stateId AND dbo.tbl_OfficeMaster.stateId = dbo.tbl_StateMaster.stateId INNER JOIN
                         dbo.tbl_EmployeeMaster ON dbo.tbl_AppMaster.employeeId = dbo.tbl_EmployeeMaster.employeeId


GO
