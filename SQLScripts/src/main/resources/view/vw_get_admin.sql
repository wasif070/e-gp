SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER view [dbo].[vw_get_admin] as

select
d.FullName,c.EmailId,d.MobileNo,a.DepartmentName

 from tbl_DepartmentMaster a,tbl_UserTypeMaster b,tbl_LoginMaster c,tbl_AdminMaster d

where

a.ApprovingAuthorityId = c.UserId and
a.ApprovingAuthorityId = d.UserId and
c.UserId = d.UserId and
b.UserTypeId = c.userTyperId and
a.DepartmentType like 'Organization'

GO
