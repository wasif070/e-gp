SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_get_tendercolumndata]
AS
    SELECT      dbo.tbl_TenderCells.rowId,
                dbo.tbl_TenderColumns.filledBy,
                dbo.tbl_TenderCells.columnId,
                dbo.tbl_TenderCells.cellvalue,
                dbo.tbl_TenderColumns.tenderTableId
    FROM        dbo.tbl_TenderCells
                INNER JOIN  dbo.tbl_TenderColumns ON dbo.tbl_TenderCells.columnId = dbo.tbl_TenderColumns.columnId
                            AND dbo.tbl_TenderCells.tenderTableId = dbo.tbl_TenderColumns.tenderTableId

GO