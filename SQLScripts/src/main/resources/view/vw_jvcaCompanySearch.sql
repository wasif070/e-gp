SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_jvcaCompanySearch]
AS
SELECT     l.userId, l.emailId, c.companyId, c.companyName, c.companyRegNumber
FROM         dbo.tbl_LoginMaster AS l INNER JOIN
                      dbo.tbl_TendererMaster AS t ON l.userId = t.userId INNER JOIN
                      dbo.tbl_CompanyMaster AS c ON t.companyId = c.companyId
WHERE     (c.companyName <> '-') AND (l.status = 'Approved')

GO