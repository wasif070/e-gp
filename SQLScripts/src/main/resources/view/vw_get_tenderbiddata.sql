SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_get_tenderbiddata]
AS
SELECT     dbo.tbl_TenderColumns.filledBy, dbo.tbl_TenderColumns.tenderTableId, dbo.tbl_TenderBidPlainData.rowId, dbo.tbl_TenderBidPlainData.bidTableId,
                      dbo.tbl_TenderBidPlainData.cellValue, dbo.tbl_TenderBidPlainData.tenderColId AS columnId, dbo.tbl_TenderColumns.dataType
FROM         dbo.tbl_TenderColumns INNER JOIN
                      dbo.tbl_TenderBidPlainData ON dbo.tbl_TenderColumns.tenderTableId = dbo.tbl_TenderBidPlainData.tenderTableId AND
                      dbo.tbl_TenderColumns.columnId = dbo.tbl_TenderBidPlainData.tenderColId

GO