SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_getSbDevPartner]
AS
SELECT     l.emailId, p.partnerId, p.userId, p.fullName, p.nationalId, p.mobileNo, p.sBankDevelopId, p.isAdmin, s.sbDevelopName, s.partnerType, s.createdBy,
                      s.sBankDevelHeadId, p.isMakerChecker, l.status
FROM         dbo.tbl_LoginMaster AS l INNER JOIN
                      dbo.tbl_PartnerAdmin AS p ON l.userId = p.userId INNER JOIN
                      dbo.tbl_ScBankDevPartnerMaster AS s ON p.sBankDevelopId = s.sBankDevelopId

GO