/****** Object:  UserDefinedFunction [dbo].[GetDepartmentType]    Script Date: 07/12/2012 13:40:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[GetDepartmentType](@DepartmentID int)
RETURNS varchar(50)
AS
BEGIN
Declare @v_departmentidCounter_Vc as varchar(500),@v_tempCounter_Vc as varchar(100),
	@v_departmentid_Vc as varchar(200)

SELECT @v_departmentid_Vc=@DepartmentID
		--set @v_departmentidCounter_Vc=@v_departmentid_Vc
		set @v_tempCounter_Vc=''

		If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Organization'
			Begin
				SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)
				SET @v_departmentidCounter_Vc=cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
			End

		if @v_tempCounter_Vc=''
			BEGIN
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =8) as varchar(10))
					END
			END
		ELSE
			BEGIN

				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_tempCounter_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)) as varchar(10))
					END
			END

		if @v_tempCounter_Vc=''
			BEGIN
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Ministry'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =8) as varchar(10))
					END
			END
		ELSE
			BEGIN

				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_tempCounter_Vc)='Ministry'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)) as varchar(10))
					END
			END
		Declare @MyOutput as varchar(20)
		Select @MyOutput= departmentType from tbl_DepartmentMaster where departmentId=@v_departmentidCounter_Vc
RETURN @MyOutput
END


		--select * from tbl_AppMaster where appStatus='Approved'
