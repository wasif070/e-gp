SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		G. M. Rokibul hasan
-- Create date: 09-August-2015
-- Description:	To get Govt User Wise Office ID
-- =============================================
ALTER FUNCTION [dbo].[f_UserWiseOfficeID]
(
	@userid int
)
RETURNS int
AS
BEGIN
DECLARE @officeid int =0
select top 1 @officeid= officeId from tbl_EmployeeOffices where employeeId in
(select employeeId from tbl_EmployeeMaster where userId = @userid )
order by employeeOfficeId desc
RETURN @officeid
END