/****** Object:  UserDefinedFunction [dbo].[f_get_BoDUserCheck]    Script Date: 09/09/2015 16:26:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		G. M. Rokibul Hasan
-- Create date: 9-9-2015
-- Description:	To Compare in Financial Delegation for The BoD user
-- =============================================
ALTER FUNCTION [dbo].[f_get_BoDUserCheck]
(
	 @TENDERID INT ,
	 @isBoD varchar(3)
)
RETURNS int
AS
BEGIN
DECLARE @ResultVal int
SELECT @ResultVal = (CASE	WHEN @isBoD = 'NA' THEN 1
							WHEN @isBoD = 'No' THEN 1
							ELSE
								CASE WHEN 
												(select ISNULL(COUNT(employeeid),0) from tbl_EmployeeRoles 
												where procurementRoleId = (select procurementRoleId from tbl_ProcurementRole where procurementRole = 'BOD')
												and departmentId in (select departmentId from tbl_DepartmentMaster where departmentType='Organization' 
												and departmentName = (select agency from tbl_TenderDetails where tenderId = @TENDERID)))>0 and @isBoD = 'Yes'
												THEN 1
									else 0 
								end
					END) 
	RETURN @ResultVal

END


GO