/****** Object:  UserDefinedFunction [dbo].[f_gettscmembername]    Script Date: 07/12/2012 13:38:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Returns a trimmed string.
--
--
-- Author: Karan
-- Date: 28-04-2011
--
-- Last Modified:
-- Modified By:
-- Date
-- Modification:
--------------------------------------------------------------------------------


ALTER FUNCTION [dbo].[f_gettscmembername](@v_UserId_inInt INT, @v_GovUserId_inInt INT) RETURNS VARCHAR(MAX)
AS

BEGIN


 Declare @v_GovUserName varchar(500)

 If @v_GovUserId_inInt<>0
 Begin
 SELECT @v_GovUserName = employeeName FROM dbo.tbl_EmployeeTrasfer Where govUserId=@v_GovUserId_inInt
 End
 Else
 Begin
 SELECT @v_GovUserName = fullName FROM dbo.tbl_ExternalMemInfo Where userId=@v_UserId_inInt
 End

 Return @v_GovUserName

END
