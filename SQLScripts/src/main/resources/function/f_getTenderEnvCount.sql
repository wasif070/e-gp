/****** Object:  UserDefinedFunction [dbo].[f_getTenderEnvCount]    Script Date: 07/12/2012 13:36:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Returns a trimmed string.
--
--
-- Author: Darshan
-- Date: 05-01-2011
--
-- Last Modified:
-- Modified By:
-- Date
-- Modification:
--------------------------------------------------------------------------------


ALTER FUNCTION [dbo].[f_getTenderEnvCount](@v_TenderId_inInt INT) RETURNS INT
AS

BEGIN
DECLARE @v_TenderEncCount_Int INT

SELECt	@v_TenderEncCount_Int = TT.noOfEnvelops
FROM	tbl_TenderDetails td
		INNER JOIN (SELECt	CEM.noOfEnvelops,pm.procurementMethod, pn.procurementNature, tt.tenderType
					FROM	tbl_ConfigEvalMethod CEM
							INNER JOIN tbl_ProcurementMethod pm on pm.procurementMethodId = CEM.procurementMethodId
							INNER JOIN tbl_ProcurementNature pn on pn.procurementNatureId = CEM.procurementNatureId
							INNER JOIN tbl_TenderTypes tt on tt.tenderTypeId = CEM.tenderTypeId) as TT ON TT.procurementMethod = td.procurementMethod
							AND TT.procurementNature = td.procurementNature
							AND TT.tenderType = td.eventType
WHERE tenderId = @v_TenderId_inInt

RETURN @v_TenderEncCount_Int

END