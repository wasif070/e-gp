/****** Object:  UserDefinedFunction [dbo].[f_GetAuditOfficeDept]    Script Date: 07/12/2012 13:34:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Returns username based on userTypeId and userId
--
--
-- Author: Karan ,rishita, taher
-- Date: 21-10-2011
--
-- Last Modified:
-- Modified By:
-- Date
-- Modification:
--------------------------------------------------------------------------------


ALTER FUNCTION [dbo].[f_GetAuditOfficeDept](@v_UserId_inInt INT, @v_UserTypeId_inInt INT) RETURNS VARCHAR(MAX)
AS

BEGIN


 Declare @v_DeptOfficeAddr varchar(500)

 IF @v_UserTypeId_inInt = 3
 BEGIN
	select @v_DeptOfficeAddr = dm.departmentName + ' / ' + om.officeName from
	tbl_EmployeeOffices eo inner join tbl_EmployeeMaster em on eo.employeeId = em.employeeId inner join
	tbl_OfficeMaster om on om.officeId=eo.officeId
	inner join tbl_DepartmentMaster dm on dm.departmentId = om.departmentId
	where em.userId=@v_UserId_inInt
 END
 Else IF @v_UserTypeId_inInt = 6 or @v_UserTypeId_inInt = 7 or @v_UserTypeId_inInt = 15
 BEGIN
	  select @v_DeptOfficeAddr = 'NA'
	--select @v_DeptOfficeAddr = sm.sbDevelopName + ' / ' + sm.officeAddress
	--from tbl_ScBankDevPartnerMaster sm
	--inner join tbl_PartnerAdmin pa on pa.sBankDevelopId = sm.sBankDevelopId
	--and pa.userId = @v_UserId_inInt
 END

 Else IF @v_UserTypeId_inInt = 5
 BEGIN
	select @v_DeptOfficeAddr =  dm.departmentName + ' / NA' from
	tbl_AdminMaster am inner join tbl_DepartmentMaster dm on dm.approvingAuthorityId= am.userId
	where am.userId=@v_UserId_inInt
 END

 Else IF @v_UserTypeId_inInt = 4
 BEGIN
	select @v_DeptOfficeAddr = dm.departmentName + ' / ' + om.officeName from
	tbl_OfficeAdmin oa inner join tbl_AdminMaster am on oa.userId = am.userId
	inner join tbl_OfficeMaster om on om.officeId = oa.officeId
	inner join tbl_DepartmentMaster dm on dm.departmentId = om.departmentId
	where am.userId=@v_UserId_inInt
 END

 Else IF @v_UserTypeId_inInt = 1 or @v_UserTypeId_inInt = 8 or @v_UserTypeId_inInt = 12 or @v_UserTypeId_inInt = 14 or @v_UserTypeId_inInt = 16 or @v_UserTypeId_inInt = 17
 BEGIN
	select @v_DeptOfficeAddr = 'NA'
 END

 Return @v_DeptOfficeAddr

END
