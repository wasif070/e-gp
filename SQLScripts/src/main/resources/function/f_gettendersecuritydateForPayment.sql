/****** Object:  UserDefinedFunction [dbo].[f_gettendersecuritydateForPayment]    Script Date: 07/12/2012 13:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Returns a trimmed string.
--
--
-- Author: Karan
-- Date: 16-06-2011
--
-- Last Modified:
-- Modified By:
-- Date
-- Modification:
--------------------------------------------------------------------------------


ALTER FUNCTION [dbo].[f_gettendersecuritydateForPayment](@v_TenderId_inInt INT) RETURNS DATETIME
AS

BEGIN

Declare @v_TenderSecurityDt_Dt Datetime
 Select @v_TenderSecurityDt_Dt = MAX(securityLastDtForPayment) From
	(Select Max(lastValAcceptDt) as securityLastDtForPayment From tbl_TenderValidityExtDate Where tenderId=@v_TenderId_inInt
	Union
	Select securityLastDt as securityLastDtForPayment From tbl_TenderDetails Where tenderId=@v_TenderId_inInt
	)A


 Return @v_TenderSecurityDt_Dt


END
