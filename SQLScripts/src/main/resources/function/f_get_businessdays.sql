/****** Object:  UserDefinedFunction [dbo].[f_get_businessdays]    Script Date: 07/12/2012 13:34:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Calculates business days
--
--
-- Author: Karan
-- Date: 28-10-2010
--
-- Last Modified:
-- Modified By: Kinjal
-- Date
-- Modification: Change logic
--------------------------------------------------------------------------------
ALTER FUNCTION [dbo].[f_get_businessdays]
(
     @v_StartDate_inDT datetime,
     @v_totaldays_inInt int
)
returns datetime
 as
begin

  DECLARE @v_DaysBetween_Int int, @v_BusinessDays_Int int, @v_Cnt_Int int, @v_EvalDate_Dt datetime

  SELECT @v_DaysBetween_Int = 0, @v_BusinessDays_Int = 0, @v_Cnt_Int=0
  --SELECT @v_DaysBetween_Int = datediff(Day,@v_StartDate_inDT,@v_EndDate_inDT) + 1

   WHILE @v_Cnt_Int <= @v_totaldays_inInt
     BEGIN
           SELECT @v_EvalDate_Dt = @v_StartDate_inDT + @v_DaysBetween_Int

           IF NOT EXISTS (Select holidaydate from tbl_HolidayMaster where holidaydate  = @v_EvalDate_Dt )
           BEGIN
                if ((datepart(dw,@v_EvalDate_Dt) <> 6) and (datepart(dw,@v_EvalDate_Dt) <> 7))
                    BEGIN
                         select @v_BusinessDays_Int = @v_BusinessDays_Int + 1
                         SELECT @v_Cnt_Int = @v_Cnt_Int + 1
                    END
            END


           select @v_DaysBetween_Int = @v_DaysBetween_Int +1
        select @v_Cnt_Int = @v_Cnt_Int
    END

 RETURN cast(floor(cast(@v_EvalDate_Dt as float))as smalldatetime)
end
