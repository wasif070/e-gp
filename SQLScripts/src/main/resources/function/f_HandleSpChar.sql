/****** Object:  UserDefinedFunction [dbo].[f_HandleSpChar]    Script Date: 07/12/2012 13:39:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Returns String with special handled
--
-- Author: Taher
-- Date: 16-11-2011
--
-- Last Modified:
-- Modified By:
-- Date
-- Modification:
--------------------------------------------------------------------------------


ALTER FUNCTION [dbo].[f_HandleSpChar](@v_xml_String varchar(max)) RETURNS VARCHAR(MAX)
AS

BEGIN

 Declare @v_xml_output varchar(MAX)

				 set @v_xml_output=replace(@v_xml_String,'�','&#161;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#162;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#163;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#164;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#165;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#166;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#167;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#168;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#169;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#170;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#171;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#172;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#174;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#175;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#176;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#177;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#178;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#179;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#180;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#181;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#182;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#183;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#184;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#185;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#186;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#187;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#188;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#189;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#190;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#191;')
				 --added by Nishith

				    --set @v_xml_output=replace(@v_xml_output,'&quote', '&#39;')
					set @v_xml_output=replace(@v_xml_output,'�','&#192;')
					set @v_xml_output=replace(@v_xml_output,'�','&#193;')
					set @v_xml_output=replace(@v_xml_output,'�','&#194;')
					set @v_xml_output=replace(@v_xml_output,'�','&#195;')
					set @v_xml_output=replace(@v_xml_output,'�','&#196;')
					set @v_xml_output=replace(@v_xml_output,'�','&#197;')
					set @v_xml_output=replace(@v_xml_output,'�','&#198;')
					set @v_xml_output=replace(@v_xml_output,'�','&#199;')
					set @v_xml_output=replace(@v_xml_output,'�','&#200;')
					set @v_xml_output=replace(@v_xml_output,'�','&#201;')
					set @v_xml_output=replace(@v_xml_output,'�','&#202;')
					set @v_xml_output=replace(@v_xml_output,'�','&#203;')
					set @v_xml_output=replace(@v_xml_output,'�','&#204;')
					set @v_xml_output=replace(@v_xml_output,'�','&#205;')
					set @v_xml_output=replace(@v_xml_output,'�','&#206;')
					set @v_xml_output=replace(@v_xml_output,'�','&#207;')
					set @v_xml_output=replace(@v_xml_output,'�','&#208;')
					set @v_xml_output=replace(@v_xml_output,'�','&#209;')
					set @v_xml_output=replace(@v_xml_output,'�','&#210;')
					set @v_xml_output=replace(@v_xml_output,'�','&#211;')
					set @v_xml_output=replace(@v_xml_output,'�','&#212;')
					set @v_xml_output=replace(@v_xml_output,'�','&#213;')
					set @v_xml_output=replace(@v_xml_output,'�','&#214;')
					set @v_xml_output=replace(@v_xml_output,'�','&#215;')
					set @v_xml_output=replace(@v_xml_output,'�','&#216;')
					set @v_xml_output=replace(@v_xml_output,'�','&#217;')
					set @v_xml_output=replace(@v_xml_output,'�','&#218;')
					set @v_xml_output=replace(@v_xml_output,'�','&#219;')
					set @v_xml_output=replace(@v_xml_output,'�','&#220;')
					set @v_xml_output=replace(@v_xml_output,'�','&#221;')
					set @v_xml_output=replace(@v_xml_output,'�','&#222;')
					set @v_xml_output=replace(@v_xml_output,'�','&#223;')
					set @v_xml_output=replace(@v_xml_output,'�','&#224;')
					set @v_xml_output=replace(@v_xml_output,'�','&#225;')
					set @v_xml_output=replace(@v_xml_output,'�','&#226;')
					set @v_xml_output=replace(@v_xml_output,'�','&#227;')
					set @v_xml_output=replace(@v_xml_output,'�','&#228;')
					set @v_xml_output=replace(@v_xml_output,'�','&#229;')
					set @v_xml_output=replace(@v_xml_output,'�','&#230;')
					set @v_xml_output=replace(@v_xml_output,'�','&#231;')
					set @v_xml_output=replace(@v_xml_output,'�','&#232;')
					set @v_xml_output=replace(@v_xml_output,'�','&#233;')
					set @v_xml_output=replace(@v_xml_output,'�','&#234;')
					set @v_xml_output=replace(@v_xml_output,'�','&#235;')
					set @v_xml_output=replace(@v_xml_output,'�','&#236;')
					set @v_xml_output=replace(@v_xml_output,'�','&#237;')
					set @v_xml_output=replace(@v_xml_output,'�','&#238;')
					set @v_xml_output=replace(@v_xml_output,'�','&#239;')
					set @v_xml_output=replace(@v_xml_output,'�','&#240;')
					set @v_xml_output=replace(@v_xml_output,'�','&#241;')
					set @v_xml_output=replace(@v_xml_output,'�','&#242;')
					set @v_xml_output=replace(@v_xml_output,'�','&#243;')
					set @v_xml_output=replace(@v_xml_output,'�','&#244;')
					set @v_xml_output=replace(@v_xml_output,'�','&#245;')
					set @v_xml_output=replace(@v_xml_output,'�','&#246;')
					set @v_xml_output=replace(@v_xml_output,'�','&#247;')
					set @v_xml_output=replace(@v_xml_output,'�','&#248;')
					set @v_xml_output=replace(@v_xml_output,'�','&#249;')
					set @v_xml_output=replace(@v_xml_output,'�','&#250;')
					set @v_xml_output=replace(@v_xml_output,'�','&#251;')
					set @v_xml_output=replace(@v_xml_output,'�','&#252;')
					set @v_xml_output=replace(@v_xml_output,'�','&#253;')
					set @v_xml_output=replace(@v_xml_output,'�','&#254;')
					set @v_xml_output=replace(@v_xml_output,'�','&#255;')
					set @v_xml_output=replace(@v_xml_output,'�','&#338;')
					set @v_xml_output=replace(@v_xml_output,'�','&#339;')
					set @v_xml_output=replace(@v_xml_output,'�','&#352;')
					set @v_xml_output=replace(@v_xml_output,'�','&#353;')
					set @v_xml_output=replace(@v_xml_output,'�','&#376;')
					set @v_xml_output=replace(@v_xml_output,'�','&#402;')
				 --added by Nishith done

				 set @v_xml_output=replace(@v_xml_output,'�','&#8211;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#8212;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#8216;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#8217;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#8218;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#8220;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#8221;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#8222;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#8224;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#8225;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#8226;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#8230;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#8240;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#8364;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#8482;')
				 set @v_xml_output=replace(@v_xml_output,'�','&#8221;')				 
				 --Added by DOHATEC on 21 Jan 2014
                 set @v_xml_output=replace(@v_xml_output,'�','&#710;')
				  --Added by DOHATEC on 07 Dec 2014 to resolve OTID 5909
				 set @v_xml_output=replace(@v_xml_output,'�','&#62;')
				 --Added by DOHATEC on 25 Feb 2015 to resolve OTID 6780
				 set @v_xml_output=replace(@v_xml_output,'�','&#32;')
				 --Added by DOHATEC on 8 Mar 2015 to resolve OTID 11576
				 set @v_xml_output=replace(@v_xml_output,'�','&#173;')
				--Added by DOHATEC on 16 September 2015 to resolve OTID 19458
				set @v_xml_output=replace(@v_xml_output,'�','&#32;')

 Return @v_xml_output

END
