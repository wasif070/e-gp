/****** Object:  UserDefinedFunction [dbo].[f_initcap]    Script Date: 07/12/2012 13:39:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Returns a trimmed string.
--
--
-- Author: Karan
-- Date: 28-10-2010
--
-- Last Modified:
-- Modified By:
-- Date
-- Modification:
--------------------------------------------------------------------------------


ALTER FUNCTION [dbo].[f_initcap](@v_String_inVc VARCHAR(MAX)) RETURNS VARCHAR(MAX)
AS

BEGIN

 RETURN upper(left(@v_String_inVc, 1)) + right(@v_String_inVc, len(@v_String_inVc) - 1)

END
