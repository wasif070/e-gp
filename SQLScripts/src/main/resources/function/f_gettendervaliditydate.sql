/****** Object:  UserDefinedFunction [dbo].[f_gettendervaliditydate]    Script Date: 07/12/2012 13:38:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Returns a trimmed string.
--
--
-- Author: Karan
-- Date: 05-01-2011
--
-- Last Modified:
-- Modified By:
-- Date
-- Modification:
--------------------------------------------------------------------------------


ALTER FUNCTION [dbo].[f_gettendervaliditydate](@v_TenderId_inInt INT) RETURNS DATETIME
AS

BEGIN
Declare @v_TenderValDt_Dt Datetime
 Select @v_TenderValDt_Dt = Case
	When (Select Max(newPropValDt) from tbl_TenderValidityExtDate Where tenderId=@v_TenderId_inInt And extStatus='approved' ) is not null
	Then (Select Max(newPropValDt) from tbl_TenderValidityExtDate Where tenderId=@v_TenderId_inInt And extStatus='approved' )
	When Exists (Select tenderValidityDt From tbl_TenderDetails Where tenderId=@v_TenderId_inInt)
	Then (Select tenderValidityDt From tbl_TenderDetails Where tenderId=@v_TenderId_inInt)
 End

 Return @v_TenderValDt_Dt


END
