SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Darshan Shah
-- Create date: 27-Dec-2012
-- Description:	For Generate Formula from N number of forms
-- =============================================
ALTER FUNCTION [dbo].[f_getDataForAutoFormula]
(
	@v_tenderID INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @v_autoFormula VARCHAR(MAX), @v_autoFormulaForSalvageForms VARCHAR(MAX),
			@v_excludeFormId INT
			
	SELECT	@v_excludeFormId = tenderFormId  
	FROM	tbl_TenderForms tf 
			INNER JOIN tbl_TenderSection ts ON ts.tenderSectionId = tf.tenderSectionId 
			-- INNER JOIN tbl_epwtwobmapping epw2b ON epw2b.formId = tf.templateFormId 
			INNER JOIN tbl_epwtwobmapping epw2b ON epw2b.formId = CASE WHEN tf.templateFormId = 0 and tf.FormType IS NOT NULL AND tf.formStatus <> 'createp' THEN  CAST(SUBSTRING(tf.FormType,32,LEN(tf.FormType)) AS INT) WHEN (tf.formStatus IS NULL OR tf.formStatus = 'cp') AND tf.templateFormId > 0 AND tf.FormType IS NULL THEN tf.templateFormId END
			INNER JOIN tbl_TenderStd tstd on tstd.templateId = epw2b.stdId
						AND tstd.tenderStdId = ts.tenderStdId 
						AND tstd.tenderId = @v_tenderID

	SELECT @v_autoFormula =(SELECT autoFormulaCell + '+'
							FROM (	SELECT	RTRIM(LTRIM(CONVERT(VARCHAR(10),TC.tenderTableId)))+'_'+RTRIM(LTRIM(CONVERT(VARCHAR(10),TC.columnId)))+'_'+RTRIM(LTRIM(CONVERT(VARCHAR(10),TC.cellId))) AS autoFormulaCell,
											tcs.columnHeader +'_'+RTRIM(LTRIM(CONVERT(VARCHAR(10),TC.rowId))) AS autoFormulaDisplay
									FROM	tbl_TenderCells TC
											INNER JOIN tbl_TenderGrandSumDetail TGS ON TGS.tenderTableId = TC.tenderTableId
														AND TGS.tenderFormId = CASE WHEN @v_excludeFormId > 0 THEN @v_excludeFormId ELSE TGS.tenderFormId END
											INNER JOIN tbl_TenderGrandSum TG ON TG.tenderSumId = TGS.tenderSumId
														AND tenderId = @v_tenderID
											INNER JOIN tbl_TenderColumns tcs ON tcs.columnId = tc.columnId
														AND tcs.tenderTableId = TGS.tenderTableId
														AND dataType IN (3,4,8,13) -- 3=Money Positive,4=Numeric,8=Money All,13=Money Positive(3 digits after decimal)
														AND filledBy = 3 -- Auto
                                                                                                                AND columnType = 7 -- Total Rate Column type
                                                                                                                 AND TGS.tenderFormId NOT IN (
                                                                                                                                   select tf.tenderFormId from tbl_TenderForms tf INNER JOIN tbl_TenderGrandSumDetail tgs
																   ON tf.tenderFormId=tgs.tenderFormId
																   INNER JOIN tbl_TenderGrandSum tg
																   ON tg.tenderSumId=tgs.tenderSumId
																   where tg.tenderId=@v_tenderID and tf.FormType = 'BOQsalvage'
														                                                       
                                                                                     ) 
									WHERE	tc.templateTableId != 0
								) AS frm
							FOR XML PATH(''))

                                                       SET @v_autoFormula=  CASE WHEN  @v_autoFormula IS NULL  THEN '' ELSE SUBSTRING(@v_autoFormula,1,LEN(@v_autoFormula)-1) END

                                                SELECT @v_autoFormulaForSalvageForms =(SELECT autoFormulaCell + '-'
							FROM (	SELECT	RTRIM(LTRIM(CONVERT(VARCHAR(10),TC.tenderTableId)))+'_'+RTRIM(LTRIM(CONVERT(VARCHAR(10),TC.columnId)))+'_'+RTRIM(LTRIM(CONVERT(VARCHAR(10),TC.cellId))) AS autoFormulaCell,
											tcs.columnHeader +'_'+RTRIM(LTRIM(CONVERT(VARCHAR(10),TC.rowId))) AS autoFormulaDisplay
									FROM	tbl_TenderCells TC
											INNER JOIN tbl_TenderGrandSumDetail TGS ON TGS.tenderTableId = TC.tenderTableId
														AND TGS.tenderFormId = CASE WHEN @v_excludeFormId > 0 THEN @v_excludeFormId ELSE TGS.tenderFormId END
											INNER JOIN tbl_TenderGrandSum TG ON TG.tenderSumId = TGS.tenderSumId
														AND tenderId = @v_tenderID
											INNER JOIN tbl_TenderColumns tcs ON tcs.columnId = tc.columnId
														AND tcs.tenderTableId = TGS.tenderTableId
														AND dataType IN (3,4,8,13) -- 3=Money Positive,4=Numeric,8=Money All,13=Money Positive(3 digits after decimal)
														AND filledBy = 3 -- Auto
                                                                                                                AND columnType = 7 -- Total Rate Column type
                                                                                                                AND TGS.tenderFormId  IN (
                                                                                                                                   select tf.tenderFormId from tbl_TenderForms tf INNER JOIN tbl_TenderGrandSumDetail tgs
																   ON tf.tenderFormId=tgs.tenderFormId
																   INNER JOIN tbl_TenderGrandSum tg
																   ON tg.tenderSumId=tgs.tenderSumId
																   where tg.tenderId=@v_tenderID and tf.FormType = 'BOQsalvage'

                                                                                     )
									WHERE	tc.templateTableId != 0
								) AS frm
							FOR XML PATH(''))

                            SET @v_autoFormulaForSalvageForms=  CASE WHEN  @v_autoFormulaForSalvageForms IS NULL  THEN '' ELSE SUBSTRING(@v_autoFormulaForSalvageForms,1,LEN(@v_autoFormulaForSalvageForms)-1) END

					 SET @v_autoFormula= CASE WHEN @v_autoFormulaForSalvageForms<>'' THEN @v_autoFormula+'-'+@v_autoFormulaForSalvageForms ELSE LTRIM(RTRIM(@v_autoFormula)) END
		       -- print @v_autoFormula

		RETURN @v_autoFormula

END
