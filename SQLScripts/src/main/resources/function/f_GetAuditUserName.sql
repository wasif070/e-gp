/****** Object:  UserDefinedFunction [dbo].[f_GetAuditUserName]    Script Date: 07/12/2012 13:35:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Returns username based on userTypeId and userId
--
--
-- Author: Karan ,rishita, taher
-- Date: 21-10-2011
--
-- Last Modified:
-- Modified By:
-- Date
-- Modification:
--------------------------------------------------------------------------------


ALTER FUNCTION [dbo].[f_GetAuditUserName](@v_UserId_inInt INT, @v_UserTypeId_inInt INT) RETURNS VARCHAR(MAX)
AS

BEGIN


 Declare @v_UserName varchar(500)

 IF @v_UserTypeId_inInt = 3 or @v_UserTypeId_inInt = 17
 BEGIN
	select @v_UserName = employeeName from tbl_EmployeeMaster where userId =@v_UserId_inInt
 END

 Else IF @v_UserTypeId_inInt = 14
 BEGIN
	select @v_UserName = fullname from tbl_ExternalMemInfo where userId =@v_UserId_inInt
 END


 Else IF @v_UserTypeId_inInt = 6 or @v_UserTypeId_inInt = 7 or @v_UserTypeId_inInt = 15
 BEGIN
	select @v_UserName = fullname from tbl_PartnerAdmin where userId =@v_UserId_inInt
 END

 Else IF @v_UserTypeId_inInt = 4 or @v_UserTypeId_inInt = 5 or @v_UserTypeId_inInt = 8 or @v_UserTypeId_inInt = 12
 BEGIN
	select @v_UserName = fullname from tbl_AdminMaster where userId =@v_UserId_inInt
 END

 Else IF @v_UserTypeId_inInt = 1
 BEGIN
	select @v_UserName = 'eGP-Admin'
 END
 Else IF @v_UserTypeId_inInt = 16
 BEGIN
	select @v_UserName = 'DG-CPTU'
 END
 Return @v_UserName

END
