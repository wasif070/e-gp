/****** Object:  UserDefinedFunction [dbo].[f_get_ScopeOfDebarment]    Script Date: 01/26/2015 15:26:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Get Scope of Debarment
--
--
-- Author:	G. M. Rokibul Hasan
-- Create date: 26-01-2015
--
-- Last Modified:
-- Modified By:
-- Date
-- Modification:
-- =============================================
ALTER FUNCTION [dbo].[f_get_ScopeOfDebarment]
(
	@debarTypeId INT,
	@debarIds VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
DECLARE @ScopeOfDebarment VARCHAR(MAX) = '';
IF (@debarTypeId =1) BEGIN
SELECT  @ScopeOfDebarment = @ScopeOfDebarment + Scope FROM (  SELECT 'Tender/Proposal ID: '+  cast(tenderId as varchar(10))+ ', Ref No.: '+ reoiRfpRefNo + ' ;</br>' AS Scope  from tbl_tenderdetails where  tenderId in (SELECT * FROM dbo.f_split(@debarIds,','))) T
END
IF (@debarTypeId =2) BEGIN
SELECT  @ScopeOfDebarment = @ScopeOfDebarment + Scope FROM (  SELECT 'APP Code: '+ appcode + ', Package No.: '+ packageNo +' ;</br>' AS Scope  from tbl_AppMaster a,tbl_apppackages p where a.appid=p.appid and packageId in (SELECT * FROM dbo.f_split(@debarIds,','))) T
END

IF (@debarTypeId =3) BEGIN
SELECT  @ScopeOfDebarment = @ScopeOfDebarment + Scope FROM (  SELECT 'Project Name: '+ projectName + ', Project Code: '+ projectCode +' ;</br>'AS Scope  from tbl_ProjectMaster p where projectId in (SELECT * FROM dbo.f_split(@debarIds,','))) T
END

IF (@debarTypeId =4) BEGIN
SELECT  @ScopeOfDebarment = @ScopeOfDebarment + Scope FROM (  SELECT 'PE Office Name: '+ officeName +' ;</br>' AS Scope  from tbl_officemaster o where  o.officeId in (SELECT * FROM dbo.f_split(@debarIds,','))) T
END

IF (@debarTypeId =5) BEGIN
SELECT  @ScopeOfDebarment = @ScopeOfDebarment + Scope FROM (  SELECT departmentType +' Name: '+ departmentname + ' ;</br>' AS Scope  from tbl_departmentmaster where  departmentId in (SELECT * FROM dbo.f_split(@debarIds,','))) T
END

IF (@debarTypeId =6) BEGIN
RETURN 'e-GP Portal'
END

SET @ScopeOfDebarment = SUBSTRING(@ScopeOfDebarment,1,LEN(@ScopeOfDebarment)-5)
RETURN @ScopeOfDebarment 

END

GO