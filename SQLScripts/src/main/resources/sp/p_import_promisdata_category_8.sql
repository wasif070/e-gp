USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_import_promisdata_category_8]    Script Date: 4/24/2016 11:13:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- Modified By Dohatec on 10th August 2015 To Segregate Promis data import basis of Indicator Category 
-- =============================================
ALTER PROCEDURE [dbo].[p_import_promisdata_category_8]
AS
BEGIN
UPDATE  [Tbl_PromisIndicator]
SET [DaysReleasePayment] = T.DerivedDaysReleasePayment
           ,[LatePayment] = T.LatePayment
           ,[InterestPaid] = T.InterestPaid
FROM (select tenderId DerivedTenderID,
case
	when -- Dates configured
		(select count(invoiceId) from tbl_CMS_InvoiceMaster cdc where cdc.tenderId = td.tenderId) = 0
	then
		'NA'
	else
(select ISNULL(convert(varchar(20),AVG(abc.noOfDays)),'0') from (select ISNULL(DATEDIFF(d,(im.createdDate),(im.releasedDtNTime)),0) noOfDays
from tbl_CMS_InvoiceMaster im where im.tenderId = td.tenderId) abc where abc.noOfDays != 0) 
end as DerivedDaysReleasePayment,		--done - Added on 24th Jan 2012 
case
	when -- Dates configured
		(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
	then
		'100'
	else
		case
			when 
			(select count(iad.ldAmt) from tbl_CMS_InvoiceAccDetails iad
			where iad.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b 
			where a.wpLotId = b.appPkgLotId and b.tenderId = td.tenderId) and iad.interestonDP > 0) > 0
			then
			--'1'
			CONVERT(varchar,(select count(iad.ldAmt) from tbl_CMS_InvoiceAccDetails iad
			where iad.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b 
			where a.wpLotId = b.appPkgLotId and b.tenderId = td.tenderId) and iad.interestonDP > 0))
			else
			'0'
		end
end	LatePayment,
case
	when -- Dates configured
		(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
	then
		'100'
	else
		case
			when 
			(select count(iad.ldAmt) from tbl_CMS_InvoiceAccDetails iad
			where iad.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b 
			where a.wpLotId = b.appPkgLotId and b.tenderId = td.tenderId) and iad.interestonDP > 0) > 0
			then
			'1'
			else
			'0'
		end
end	InterestPaid

from tbl_TenderDetails td,tbl_DepartmentMaster dm 

where tenderStatus not in('Pending')
and td.departmentId=dm.departmentId
and submissionDt < GETDATE())T
WHERE tenderId = T.DerivedTenderID
and DaysReleasePayment = 'NA'
and AwardPubCPTUWebsite !=100 
END
