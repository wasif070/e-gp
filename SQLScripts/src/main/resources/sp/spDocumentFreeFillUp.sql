USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[spDocumentFreeFillUp]    Script Date: 4/24/2016 11:22:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spDocumentFreeFillUp]
	-- Add the parameters for the stored procedure here
	@TenderID varchar(50),
	@CreatedBy bigint,
	@TendererID bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @DocFeeAmount numeric(18,4)
	Declare @PkgLotNO varchar(50)
	Declare @tenderPaymentId bigint
	
	
	
	Select @DocFeeAmount=pkgDocFees from tbl_TenderDetails Where tenderId=@TenderID
	Set @PkgLotNO=''
    -- Insert statements for procedure here
	INSERT INTO [dbo].[tbl_TenderPayment]
           ([paymentFor]
           ,[paymentInstType]
           ,[instRefNumber]
           ,[amount]
           ,[instDate]
           ,[instValidUpto]
           ,[bankName]
           ,[branchName]
           ,[comments]
           ,[tenderId]
           ,[userId]
           ,[createdBy]
           ,[createdDate]
           ,[pkgLotId]
           ,[eSignature]
           ,[status]
           ,[paymentMode]
           ,[extValidityRef]
           ,[currency]
           ,[issuanceBank]
           ,[issuanceBranch]
           ,[isVerified]
           ,[isLive]
           ,[dtOfAction]
           ,[partTransId]
           ,[OnlineTransId])
     VALUES
           ('Document Fees'
           ,'Cash'
           ,''
           ,@DocFeeAmount
           ,GETDATE()
           ,'1900-01-01 00:00:00'
           ,'Bank of Bangla (BOB)'
           ,'Dhaka (BOB)'
           ,'Payment'
           ,@TenderID
           ,@TendererID
           ,@CreatedBy
           ,GETDATE()
           ,0
           ,''
           ,'paid'
           ,'bank'
           ,0
           ,'BDT'
           ,''
           ,''
           ,'yes'
           ,'Yes'
           ,GETDATE()
           ,552
           ,null)
    set @tenderPaymentId=Ident_Current('dbo.[tbl_TenderPayment]') 
           
    INSERT INTO [dbo].[tbl_TenderPaymentStatus]
           ([tenderPaymentId]
           ,[paymentStatus]
           ,[paymentStatusDt]
           ,[createdBy]
           ,[comments]
           ,[tenderPayRefId]
           ,[refPartTransId])
     VALUES
           (@tenderPaymentId
           ,'paid'
           ,GETDATE()
           ,@CreatedBy
           ,'Document Fees'
           ,0
           ,552)       
END

