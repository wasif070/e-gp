USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[spInserttbl_TenderCells]    Script Date: 4/24/2016 11:24:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROC [dbo].[spInserttbl_TenderCells]
	@tenderTableId int,
	@rowId int,
	@cellDatatype Varchar(100),
	@cellvalue varchar(max),
	@columnId smallint,
	@templateTableId int,
	@templateColumnId smallint,
	@cellId int
As
Begin

	Insert Into tbl_TenderCells	(tenderTableId, rowId, cellDatatype, cellvalue, 
		columnId, templateTableId, templateColumnId, cellId)
	Values(@tenderTableId, @rowId, @cellDatatype, @cellvalue, 
		@columnId, @templateTableId, @templateColumnId, @cellId)

End

