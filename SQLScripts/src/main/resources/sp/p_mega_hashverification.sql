USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_mega_hashverification]    Script Date: 4/24/2016 11:18:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: verify mega mega hash
--
--
-- Author: Krishnraj
-- Date: 18-12-2010
--
-- Last Modified:
-- Modified By: 
-- Date 
-- Modification: 
--------------------------------------------------------------------------------
-- SP Name	   :   p_mega_hashverification
-- Module	   :   Tender Notice
-- Description :   Verifies Mega Hash against the Mega Hash generated for bidder's bid submission for the tender
-- Function	   :   N.A.

-- EXEC [dbo].[p_mega_hashverification]
ALTER PROC [dbo].[p_mega_hashverification]
@v_tenderId_inInt int, 
@v_flag_inVc VARCHAR(6) OUTPUT

AS

BEGIN

	SET NOCOUNT ON;
	
	-- DECLARE REQUIRED VARIABLE
	DECLARE @v_tenderHash_Vc VARCHAR(MAX), @v_megaHash_Vc VARCHAR(MAX)
	
	-- FETCH GENERATED TENDER-HASH
	SELECT @v_megaHash_Vc = megaHash FROM dbo.tbl_TenderMegaHash WHERE tenderId = @v_tenderId_inInt
	
	-- FETCH TENDER-HASH
	SELECT @v_tenderHash_Vc = ISNULL(@v_tenderHash_Vc + '_' + tenderHash, tenderHash) FROM dbo.tbl_FinalSubmission 
	WHERE tenderId = @v_tenderId_inInt and bidSubStatus='finalsubmission'
			
	-- NOW GENERATE SHA1 OF GENERATED HASH
	SELECT @v_tenderHash_Vc = SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('SHA1', @v_tenderHash_Vc)), 3, 41)
			
	-- IF BOTH THE HASH ARE MATCH THEN RETURN TRUE ELSE RETURN FALSE INSERT RECORD INTO tbl_BidTemper TABLE
	IF @v_tenderHash_Vc = @v_megaHash_Vc
		SET @v_flag_inVc = 'true'
	ELSE
	BEGIN
	
		SET @v_flag_inVc = 'false'
		
		INSERT INTO dbo.tbl_BidTemper (tenderId)
			SELECT @v_tenderId_inInt
			
	END
	
END

