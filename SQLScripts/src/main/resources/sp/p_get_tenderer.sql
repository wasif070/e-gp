USE [pppd_bhutan_27092016]
GO
/****** Object:  StoredProcedure [dbo].[p_get_tenderer]    Script Date: 10/20/2016 13:55:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Get Tenderer Search Information
--
--
-- Author: Sachin Patel
-- Date: 3-11-2010
--
-- Last Modified:
-- Modified By:
-- Date 5-11-2010
-- Modification:
--------------------------------------------------------------------------------
-- SP Name	:   p_get_tenderer
-- Module	:	Company Verification (Content Admin)
-- Function	:	N.A.

--
ALTER PROCEDURE [dbo].[p_get_tenderer]


	 @v_Status_inVc Varchar(20)=Null,
	 @v_RegType_inVc Varchar(20)=NULL,
	 @v_CompanyRegNo_inVc Varchar(30)=Null,
	 @v_CompanyName_inVc Varchar(200)=Null,
	 @v_EmailId_inVc Varchar(100)=Null,
	 @v_RegDateFrom_inDt Date=Null,
	 @v_RegDateTo_inDt Date=Null,
	 @v_Page_inN Int =1 ,
	 @v_RecordPerPage_inN INT =10,
	 @v_CompanyId_inN Int=null,
	 @v_OrderBy_inVc varchar(200)=null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;

    -- Set Search Criteria
	DECLARE @v_TempQuery_Vc Varchar(max),@v_IntialQuery_Vc Varchar(1000)

	 set @v_TempQuery_Vc=' '
	IF  @v_EmailId_inVc IS NULL OR @v_EmailId_inVc=''
		BEGIN
			SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
		END
	ELSE
		BEGIN
		   	SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND emailid like ''%'+CAST(@v_EmailId_inVc AS varchar(100))+'%'''
		END

	IF  @v_RegType_inVc IS NULL OR @v_RegType_inVc='all'
		BEGIN
			SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
		END
	ELSE
		BEGIN
		   	SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND registrationType = '''+CAST(@v_RegType_inVc AS varchar(20))+''''
		END

	IF  @v_CompanyRegNo_inVc IS NULL OR @v_CompanyRegNo_inVc=''
		BEGIN
			SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
		END
	ELSE
		BEGIN
		   	SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND companyRegNumber like ''%'+CAST(@v_CompanyRegNo_inVc AS varchar(30))+'%'''
		END

	IF  @v_CompanyName_inVc IS NULL OR @v_CompanyName_inVc=''
		BEGIN
			SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
		END
	ELSE
		BEGIN
		   	SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND companyName like ''%'+CAST(@v_CompanyName_inVc AS varchar(100))+'%'''
		END
	IF  @v_CompanyId_inN IS NULL OR @v_CompanyId_inN=''
		BEGIN
			SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
		END
	ELSE
		BEGIN
		   	SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND convert(varchar(20),cm.companyId) = '''+CAST(@v_CompanyId_inN AS varchar(100))+''''
		END

	IF  @v_Status_inVc IS NULL OR @v_Status_inVc=''
		BEGIN
			SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
		END
	ELSE
		BEGIN
		   	SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND status = '''+CAST(@v_Status_inVc AS varchar(100))+''''
		END
	IF (@v_RegDateFrom_inDt is NULL OR @v_RegDateFrom_inDt='') and (@v_RegDateTo_inDt is NULL OR @v_RegDateTo_inDt='')
	begin
			SET @v_TempQuery_Vc=@v_TempQuery_Vc
	end
ELSE
	BEGIN
		if (@v_TempQuery_Vc='')
			BEGIN
			--Tue Nov 09 00:00:00 GMT+5:30 2010
				SET @v_TempQuery_Vc=' ((CONVERT(VARCHAR(10),[registeredDate],111) >= '''+CONVERT(VARCHAR(10),@v_RegDateFrom_inDt,111)+'''
				AND CONVERT(VARCHAR(10),[registeredDate],111) <= '''+CONVERT(VARCHAR(10),@v_RegDateTo_inDt,111)+''' ))'
			END
		ELSE
			BEGIN
				SET @v_TempQuery_Vc=@v_TempQuery_Vc + ' AND ((CONVERT(VARCHAR(10),[registeredDate],111) >= '''+CONVERT(VARCHAR(10),@v_RegDateFrom_inDt,111)+'''
				AND CONVERT(VARCHAR(10),[registeredDate],111) <= '''+CONVERT(VARCHAR(10),@v_RegDateTo_inDt,111)+''' ))'
			END
	END

 	if @v_CompanyId_inN is null
 	begin
				if @v_Status_inVc='approved'
				Begin
						SET @v_IntialQuery_Vc =' from tbl_loginmaster lm,tbl_TendererMaster tm,tbl_CompanyMaster cm
			where lm.userId=tm.userId and tm.companyId=cm.companyId and isjvca=''No''   and isadmin=''Yes'' and cm.companyId    in
			(select companyId from tbl_TendererMaster t,tbl_loginmaster lm where t.userid=lm.userid and status = '''+CAST(@v_Status_inVc AS varchar(100))+''' and isAdmin='''+'Yes'+''')'
				End
				
				else if @v_Status_inVc='pending'
				Begin

						SET @v_IntialQuery_Vc =' from tbl_loginmaster lm,tbl_TendererMaster tm,tbl_CompanyMaster cm
			where isjvca=''No'' and isadmin=''Yes'' and  lm.userId=tm.userId and tm.companyId=cm.companyId and cm.companyId    in
			(select companyId from tbl_TendererMaster t,tbl_loginmaster lm where t.userid=lm.userid and status = '''+CAST(@v_Status_inVc AS varchar(100))+''' and isAdmin='''+'Yes'+''')'
				End

				
				else
				Begin

						SET @v_IntialQuery_Vc =' from tbl_loginmaster lm,tbl_TendererMaster tm,tbl_CompanyMaster cm, tbl_TendererAuditTrail tat
			where isjvca=''No'' and isadmin=''Yes'' and  lm.userId=tm.userId and tat.userId=lm.userId and tm.companyId=cm.companyId and cm.companyId    in
			(select companyId from tbl_TendererMaster t,tbl_loginmaster lm where t.userid=lm.userid and status = '''+CAST(@v_Status_inVc AS varchar(100))+''' and isAdmin='''+'Yes'+''')'
				End
	end
	else
	begin
			if @v_Status_inVc='approved'
			Begin
					SET @v_IntialQuery_Vc =' from tbl_loginmaster lm,tbl_TendererMaster tm,tbl_CompanyMaster cm
		where isjvca=''No'' and isadmin=''Yes'' and lm.userId=tm.userId and tm.companyId=cm.companyId and cm.companyId    in
		(select companyId from tbl_TendererMaster t,tbl_loginmaster lm where t.userid=lm.userid and status = '''+CAST(@v_Status_inVc AS varchar(100))+''' and isAdmin='''+'No'+''')'
			End
			
			else if @v_Status_inVc='pending'
			Begin

					SET @v_IntialQuery_Vc =' from tbl_loginmaster lm,tbl_TendererMaster tm,tbl_CompanyMaster cm
		where  isjvca=''No'' and isadmin=''Yes'' and lm.userId=tm.userId and tm.companyId=cm.companyId and cm.companyId    in
		(select companyId from tbl_TendererMaster t,tbl_loginmaster lm  where t.userid=lm.userid and status = '''+CAST(@v_Status_inVc AS varchar(100))+''' and isAdmin='''+'No'+''')'
			End

			

			else
			Begin

					SET @v_IntialQuery_Vc =' from tbl_loginmaster lm,tbl_TendererMaster tm,tbl_CompanyMaster cm, tbl_TendererAuditTrail tat
		where  isjvca=''No'' and isadmin=''Yes'' and lm.userId=tm.userId and tat.userId=lm.userId and tm.companyId=cm.companyId and cm.companyId    in
		(select companyId from tbl_TendererMaster t,tbl_loginmaster lm  where t.userid=lm.userid and status = '''+CAST(@v_Status_inVc AS varchar(100))+''' and isAdmin='''+'No'+''')'
			End
	end
	
	-- apply for new procurement category
	
	
	
	--print 'Initial Qry: ' + @v_IntialQuery_Vc
	declare @v_ExecutingQuery_Vc as varchar(max)
	declare @v_reapplyCondition_Vc as varchar(max)
	if @v_Status_inVc = 'reapply'
	begin
	set @v_reapplyCondition_Vc = ''
	
	IF  @v_EmailId_inVc IS NOT NULL OR @v_EmailId_inVc != ''
		SET @v_reapplyCondition_Vc = @v_reapplyCondition_Vc + ' AND emailid like ''%'+CAST(@v_EmailId_inVc AS varchar(100))+'%'''
	IF  @v_RegType_inVc IS NOT NULL AND @v_RegType_inVc != '' AND @v_RegType_inVc != 'all'
		SET @v_reapplyCondition_Vc = @v_reapplyCondition_Vc + ' AND registrationType = '''+CAST(@v_RegType_inVc AS varchar(20))+''''
	IF  @v_CompanyName_inVc IS NOT NULL OR @v_CompanyName_inVc != ''
		SET @v_reapplyCondition_Vc = @v_reapplyCondition_Vc + ' AND companyName like ''%'+CAST(@v_CompanyName_inVc AS varchar(100))+'%'''
	
	IF (@v_RegDateFrom_inDt is NOT NULL AND @v_RegDateFrom_inDt!='') and (@v_RegDateTo_inDt is  NOT NULL AND @v_RegDateTo_inDt!='')	
		SET @v_reapplyCondition_Vc=@v_reapplyCondition_Vc + ' AND ((CONVERT(VARCHAR(10),[registeredDate],111) >= '''+CONVERT(VARCHAR(10),@v_RegDateFrom_inDt,111)+'''
				AND CONVERT(VARCHAR(10),[registeredDate],111) <= '''+CONVERT(VARCHAR(10),@v_RegDateTo_inDt,111)+''' ))'
	set @v_ExecutingQuery_Vc = 'DECLARE @v_Reccountf Int
		DECLARE @v_TotalPagef Int

		SELECT @v_Reccountf=count(*) from (
		SELECT distinct lm.userid as userid, lm.userid as tendererId,lm.registrationType, cm.companyID as companyid, cm.companyName, lm.emailId, 
		cm.regOffCountry as country, cm.regOffState as state, cm.regOffCity as city, lm.registeredDate,''Comments'' as comments FROM  
		tbl_loginmaster lm,tbl_CompanyMaster cm, tbl_BiddingPermission bp, tbl_TendererMaster tm, tbl_CompanyDocuments cd
		WHERE bp.IsReapplied=1 AND bp.CompanyID=cm.companyId AND lm.userId=cm.userId  AND lm.userId=tm.userId AND cd.tendererId=tm.tendererId AND cd.IsReapplied=1 '+@v_reapplyCondition_Vc+') as temp

		SET @v_TotalPagef =CEILING(@v_Reccountf/10)

		SELECT *, @v_Reccountf as TotalRecords, (SELECT ROW_NUMBER() OVER ( order by Reapply.registeredDate desc)) As Rownumber,	
		@v_TotalPagef as TotalPages FROM (
		select distinct lm.userid as userid, tm.tendererId as tendererId, lm.registrationType, cm.companyID as companyid, cm.companyName, lm.emailId, 
		cm.regOffCountry as country, cm.regOffState as state, cm.regOffCity as city, lm.registeredDate as registeredDate, ''Comments'' as comments  from  
		tbl_loginmaster lm,tbl_CompanyMaster cm, tbl_BiddingPermission bp, tbl_TendererMaster tm, tbl_CompanyDocuments cd
		WHERE bp.IsReapplied=1 AND bp.CompanyID=cm.companyId AND lm.userId=cm.userId  AND lm.userId=tm.userId AND cd.tendererId=tm.tendererId AND cd.IsReapplied=1 '+@v_reapplyCondition_Vc+') as Reapply'
	end
		else if @v_Status_inVc = 'rejected'
	begin
    set @v_ExecutingQuery_Vc='DECLARE @v_Reccountf Int
	DECLARE @v_TotalPagef Int
	SELECT @v_Reccountf = Count(*) From (
	SELECT * From (SELECT ROW_NUMBER() OVER ( order by '+@v_OrderBy_inVc+') As Rownumber,lm.userid,registrationType,case when cm.companyid=1
 then ''-''  else companyName end companyName,emailId,tm.country,
 tm.state,registeredDate,tm.companyid,tm.city,tm.tendererId, tat.activity as comments
	'+@v_IntialQuery_Vc+@v_TempQuery_Vc+' ) AS DATA) AS TTT
	SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_RecordPerPage_inN as varchar(10))+')
	SELECT *,@v_TotalPagef as TotalPages,@v_Reccountf as TotalRecords From (SELECT ROW_NUMBER() OVER ( order by '+@v_OrderBy_inVc+') As Rownumber,lm.userid,registrationType,case when cm.companyid=1
 then  ''-'' else companyName end companyName,emailId,tm.country,
 tm.state,registeredDate,tm.companyid,tm.city,tm.tendererId, tat.activity as comments
	'+@v_IntialQuery_Vc+ @v_TempQuery_Vc +' ) AS DATA where Rownumber between '+cast(((@v_Page_inN - 1) * @v_RecordPerPage_inN + 1) as varchar(10))+'
	AND '+cast((@v_Page_inN * @v_RecordPerPage_inN) as varchar(10))+''

	end
	else 
	begin
    set @v_ExecutingQuery_Vc='DECLARE @v_Reccountf Int
	DECLARE @v_TotalPagef Int
	SELECT @v_Reccountf = Count(*) From (
	SELECT * From (SELECT ROW_NUMBER() OVER ( order by '+@v_OrderBy_inVc+') As Rownumber,lm.userid,registrationType,case when cm.companyid=1
 then ''-''  else companyName end companyName,emailId,tm.country, ''Comments'' as comments,
 tm.state,registeredDate,tm.companyid,tm.city,tm.tendererId
	'+@v_IntialQuery_Vc+@v_TempQuery_Vc+' ) AS DATA) AS TTT
	SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_RecordPerPage_inN as varchar(10))+')
	SELECT *,@v_TotalPagef as TotalPages,@v_Reccountf as TotalRecords From (SELECT ROW_NUMBER() OVER ( order by '+@v_OrderBy_inVc+') As Rownumber,lm.userid,registrationType,case when cm.companyid=1
 then  ''-'' else companyName end companyName,emailId,tm.country, ''Comments'' as comments,
 tm.state,registeredDate,tm.companyid,tm.city,tm.tendererId
	'+@v_IntialQuery_Vc+ @v_TempQuery_Vc +' ) AS DATA where Rownumber between '+cast(((@v_Page_inN - 1) * @v_RecordPerPage_inN + 1) as varchar(10))+'
	AND '+cast((@v_Page_inN * @v_RecordPerPage_inN) as varchar(10))+''

	end
	print @v_ExecutingQuery_Vc
	Exec(@v_ExecutingQuery_Vc)
END

