USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_search_appdata]    Script Date: 4/24/2016 11:19:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Search In AppData
--
--
-- Author: Rajesh Singh
-- Date: 1-11-2010
--

-- SP Name: [p_search_appdata]
-- Module: APP
-- Function: Procedure use for search APP Details on the basis of given search criteria.
--------------------------------------------------------------------------------

--p_search_appdata '','financialYear','procurementnature','3','5','cpvcode','eGB','yes','8','appcode','packageno','90','8','3','Between','50','100'
--p_search_appdata '','','','','','','','','','','','','','','','',''
ALTER PROCEDURE [dbo].[p_search_appdata]
                @v_Keyword_inVc Varchar(50)=NULL,
                @v_financialYear_inVc Varchar(50)=NULL,
                @v_procurementnature_inVc Varchar(10)=NULL,
                @v_departmentId_inN int=NULL,
                @v_employeeId_inN int=NULL,
                @v_cpvCode_inVc Varchar(500)=NULL,
                @v_projectName_inVc Varchar(150)=NULL,
                @v_procurementType_inVc Varchar(3)=NULL,
                @v_appid_inN int=NULL,
                @v_appcode_inVc Varchar(50)=NULL,
                @v_packageno_inVc Varchar(50)=NULL,
                @v_pkgEstCost_M money=NULL,
                @v_officeId_N int=NULL,
                @v_budgetType_tint Varchar(20)=NULL,
                @v_operator_inVc Varchar(15)=NULL, -- ">=,>,<,<=,Between"
                @v_value1_inVc money=NULL,
                @v_value2_inVc money=NULL,
                @v_Page_inN Int =1,
                @v_RecordPerPage_inN INT =10,
                @v_status_inVc varchar(50) =Null,
                @v_userId_inInt Int=0

				/*search by budgetcode*/
				--@v_budgetcode_inVc Varchar(50)=NULL

AS
BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @v_TempQuery_Vc Varchar(max),@v_IntialQuery_Vc Varchar(1000)

        --This condition is use to make search of keywords in tbl_AppPkgLots
        IF  @v_Keyword_inVc IS NULL or @v_Keyword_inVc=''
                BEGIN
                        SET @v_TempQuery_Vc=''
                END
        ELSE
                BEGIN
                        SET @v_TempQuery_Vc=' packageDesc like ''%'+@v_Keyword_inVc+'%'''
                END

        IF  @v_userId_inInt IS NULL or @v_userId_inInt='' or @v_userId_inInt=0
                BEGIN
                        print 'df'
                END
        ELSE
                BEGIN
                        SET @v_TempQuery_Vc=' packageId IN (SELECT [packageId] FROM [dbo].[tbl_AppWatchList] where userid ='+CAST(@v_userId_inInt AS varchar(10))+')'
                END
        --This condition is use to make search in tbl_AppMaster
        IF  @v_financialYear_inVc IS NULL OR @v_financialYear_inVc=''
                BEGIN
                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
                END
        ELSE
                BEGIN
                        IF @v_TempQuery_Vc=''
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' financialYear LIKE ''%'+@v_financialYear_inVc+'%'''
                                END
                        ELSE
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND financialYear LIKE ''%'+@v_financialYear_inVc+'%'''
                                END
                END

        IF  @v_status_inVc IS NULL OR @v_status_inVc=''
                BEGIN
                if @v_TempQuery_Vc =''
                begin
                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + '   appstatus in(''Approved'',''BeingRevised'')'
                end
                else
                begin
                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' and appstatus in(''Approved'',''BeingRevised'')'
                end
                END
        ELSE
                BEGIN
                        IF @v_TempQuery_Vc=''
                                BEGIN
                                print @v_status_inVc
                                        if @v_status_inVc='wfpending'
                                                begin
                                                set @v_status_inVc='Pending'
                                                SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' packageid in (select packageid from tbl_apppackages where workflowstatus  LIKE ''%'+@v_status_inVc+'%'')'
                                                end
                                        else if @v_status_inVc='PendingApproved'
                                                BEGIN
                                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc
                                                END
                                        else if @v_status_inVc='Pending,Approved,BeingRevised,Revised'
                                                BEGIN
                                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' packageid in (select packageid from tbl_apppackages where appstatus in(''Pending'',''Approved'',''BeingRevised'',''Revised''))'
                                                END
                                                else if @v_status_inVc='publish'
                                                BEGIN
                                                SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' workflowstatus=''Approved'' and (appstatus <> ''Approved'' or packageid in (select packageid from tbl_apppackages where appId = '+CAST(+@v_appid_inN AS Varchar(10))+' and appstatus  <> ''Approved'') ) '
                                                END
                                                else if @v_status_inVc='isiniappsame'
                                                BEGIN
                                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' workflowstatus=''Pending''   '
                                                END
                                                else if @v_status_inVc='appiniappsame'
                                                BEGIN
                                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' (workflowstatus=''Pending'' or workflowstatus=''Approved'' or workflowstatus=''Conditional Approval'')   '
                                                END
                                        else
                                                begin
                                                SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' packageid in (select packageid from tbl_apppackages where appId = '+CAST(+@v_appid_inN AS Varchar(10))+' and appstatus  in (''BeingRevised'','''+@v_status_inVc+''')  )'
                                                end
                                END
                        ELSE
                                BEGIN
                                        if @v_status_inVc='wfpending'
                                        begin
                                                set @v_status_inVc='Pending'
                                                SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' packageid in (select packageid from tbl_apppackages where appId = '+CAST(+@v_appid_inN AS Varchar(10))+' and workflowstatus  LIKE ''%'+@v_status_inVc+'%'')'
                                        end
                                        else if @v_status_inVc='PendingApproved'
                                                BEGIN
                                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc
                                                END
                                                else if @v_status_inVc='publish'
                                                BEGIN
                                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' workflowstatus=''Approved'' and (appstatus=''Pending'' or packageid in (select packageid from tbl_apppackages where appId = '+CAST(+@v_appid_inN AS Varchar(10))+' and appstatus  = ''Pending'') ) '
                                                END
                                                else if @v_status_inVc='isiniappsame'
                                                BEGIN
                                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' workflowstatus=''Pending''   '
                                                END
                                                else if @v_status_inVc='appiniappsame'
                                                BEGIN
                                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' (workflowstatus=''Pending'' or workflowstatus=''Approved'' or workflowstatus=''Conditional Approval'')   '
                                                END
                                        else
                                                begin
                                                SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' packageid in (select packageid from tbl_apppackages where appId = '+CAST(+@v_appid_inN AS Varchar(10))+' and appstatus  in (''BeingRevised'','''+@v_status_inVc+''')  )'
                                                end
                                END
                END

        --This condition is use to make search in tbl_AppPackages
        IF  @v_procurementnature_inVc IS NULL OR @v_procurementnature_inVc=''
                BEGIN
                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
                END
        ELSE
                BEGIN
                        IF @v_TempQuery_Vc=''
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' procurementnature LIKE ''%'+@v_procurementnature_inVc+'%'''
                                END
                        ELSE
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND procurementnature LIKE ''%'+@v_procurementnature_inVc+'%'''
                                END

                END

        --This condition is use to make search in tbl_OfficeMaster
        IF  @v_departmentId_inN IS NULL OR @v_departmentId_inN=''
                BEGIN
                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
                END
        ELSE
                BEGIN
                        IF @v_TempQuery_Vc=''
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' departmentId = '+CAST(@v_departmentId_inN AS varchar(10))+''
                                END
                        ELSE
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND departmentId = '+CAST(@v_departmentId_inN AS varchar(10))+''
                                END
                END

        --This condition is use to make search in tbl_EmployeeMaster
        IF  @v_employeeId_inN IS NULL OR @v_employeeId_inN=''
                BEGIN
                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
                END
        ELSE
                BEGIN
                        IF @v_TempQuery_Vc=''
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' employeeId = '+CAST(@v_employeeId_inN AS varchar(10))+''
                                END
                        ELSE
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND employeeId = '+CAST(@v_employeeId_inN AS varchar(10))+''
                                END
                END

        --This condition is use to make search in tbl_AppPackages
        IF  @v_cpvCode_inVc IS NULL OR @v_cpvCode_inVc=''
                BEGIN
                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
                END
        ELSE
                BEGIN
                        IF @v_TempQuery_Vc=''
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' cpvCode LIKE ''%'+@v_cpvCode_inVc+'%'''
                                END
                        ELSE
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND cpvCode LIKE ''%'+@v_cpvCode_inVc+'%'''
                                END

                END

        --This condition is use to make search in tbl_AppMaster
        IF  @v_projectName_inVc IS NULL OR @v_projectName_inVc=''
                BEGIN
                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
                END
        ELSE
                BEGIN
                        IF @v_TempQuery_Vc=''
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' projectid = '''+@v_projectName_inVc+''''
                                END
                        ELSE
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND projectid = '''+@v_projectName_inVc+''''
                                END
                END

        --This condition is use to make search in tbl_AppPackages
        IF  @v_procurementType_inVc IS NULL OR @v_procurementType_inVc=''
                BEGIN
                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
                END
        ELSE
                BEGIN
                        IF @v_TempQuery_Vc=''
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' procurementType LIKE ''%'+@v_procurementType_inVc+'%'''
                                END
                        ELSE
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND procurementType LIKE ''%'+@v_procurementType_inVc+'%'''
                                END
                END

        --This condition is use to make search in tbl_AppMaster
        IF  @v_appid_inN IS NULL OR @v_appid_inN=''
                BEGIN
                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
                END
        ELSE
                BEGIN
                        IF @v_TempQuery_Vc=''
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' appId = '+CAST(+@v_appid_inN AS Varchar(10))+''
                                END
                        ELSE
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND appId = '+CAST(+@v_appid_inN AS Varchar(10))+''
                                END
                END

        --This condition is use to make search in tbl_AppMaster
        IF  @v_appcode_inVc IS NULL OR @v_appcode_inVc=''
                BEGIN
                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
                END
        ELSE
                BEGIN
                IF @v_TempQuery_Vc=''
                        BEGIN
                                SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' appCode LIKE ''%'+@v_appcode_inVc+'%'''
                        END
                ELSE
                        BEGIN
                                SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND appCode LIKE ''%'+@v_appcode_inVc+'%'''
                        END
                END

	
        --This condition is use to make search in tbl_AppPackages
        IF  @v_packageno_inVc IS NULL OR @v_packageno_inVc=''
                BEGIN
                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
                END
        ELSE
                BEGIN
                        IF @v_TempQuery_Vc=''
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' packageNo ='''+@v_packageno_inVc+''''
                                END
                        ELSE
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND packageNo ='''+@v_packageno_inVc+''''
                                END
                END

        --This condition is use to make search in tbl_AppPackages
        IF  @v_pkgEstCost_M IS NULL OR @v_pkgEstCost_M=''
                BEGIN
                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
                END
        ELSE
                BEGIN
                        IF @v_TempQuery_Vc=''
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' pkgEstCost ='+CONVERT(varchar(10), @v_pkgEstCost_M)+''
                                END
                        ELSE
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND pkgEstCost ='+CONVERT(varchar(10), @v_pkgEstCost_M)+''
                                END
                END

        --This condition is use to make search in tbl_AppMaster
        IF  @v_officeId_N IS NULL OR @v_officeId_N=''
                BEGIN
                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
                END
        ELSE
                BEGIN
                        IF @v_TempQuery_Vc=''
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' officeId ='+CONVERT(varchar(10), @v_officeId_N)+''
                                END
                        ELSE
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND officeId ='+CONVERT(varchar(10), @v_officeId_N)+''
                                END
                END

        --This condition is use to make search in tbl_AppMaster
        IF  @v_budgetType_tint IS NULL OR @v_budgetType_tint=''
                BEGIN
                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
                END
        ELSE
                BEGIN
                        IF @v_TempQuery_Vc=''
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' CONVERT(varchar(20),budgetType) ='+CONVERT(varchar(20), @v_budgetType_tint)+''
                                END
                        ELSE
                                BEGIN
                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND CONVERT(varchar(20),budgetType) ='+CONVERT(varchar(20), @v_budgetType_tint)+''
                                END
                END

        --This condition is use to make search in tbl_AppPackages
        IF  (@v_operator_inVc IS NULL OR @v_operator_inVc='') AND (@v_value1_inVc IS NULL OR @v_value1_inVc='')
                BEGIN
                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
                END
        ELSE
                BEGIN
                        IF @v_TempQuery_Vc=''
                                BEGIN
                                        IF @v_operator_inVc='='
                                                BEGIN
                                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' estimatedCost = '+CONVERT(varchar(10), @v_value1_inVc)+''
                                                END
                                        ELSE IF @v_operator_inVc='<='
                                                BEGIN
                                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' estimatedCost <= '+CONVERT(varchar(10), @v_value1_inVc)+''
                                                END
                                        ELSE IF @v_operator_inVc='>='
                                                BEGIN
                                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' estimatedCost >= '+CONVERT(varchar(10), @v_value1_inVc)+''
                                                END
                                        ELSE IF @v_operator_inVc='Between'
                                                BEGIN
                                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' (estimatedCost >= '+CONVERT(varchar(10), @v_value1_inVc)+' AND estimatedCost <= '+CONVERT(varchar(10), @v_value2_inVc)+')'
                                                END
                                END
                        ELSE
                                BEGIN
                                        IF @v_operator_inVc='='
                                                BEGIN
                                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND estimatedCost = '+CONVERT(varchar(10), @v_value1_inVc)+''
                                                END
                                        ELSE IF @v_operator_inVc='<='
                                                BEGIN
                                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND estimatedCost <= '+CONVERT(varchar(10), @v_value1_inVc)+''
                                                END
                                        ELSE IF @v_operator_inVc='>='
                                                BEGIN
                                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND estimatedCost >= '+CONVERT(varchar(10), @v_value1_inVc)+''
                                                END
                                        ELSE IF @v_operator_inVc='Between'
                                                BEGIN
                                                        SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND (estimatedCost >= '+CONVERT(varchar(10), @v_value1_inVc)+' AND estimatedCost <= '+CONVERT(varchar(10), @v_value2_inVc)+')'
                                                END
                                END
                END

		----This condition is use to make search in tbl_AppMaster
  --      IF  @v_budgetcode_inVc IS NULL OR @v_budgetcode_inVc=''
  --              BEGIN
  --                      SET @v_TempQuery_Vc= @v_TempQuery_Vc + ''
  --              END
  --      ELSE
  --              BEGIN
  --              IF @v_TempQuery_Vc=''
  --                      BEGIN
  --                              SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' budgetCode LIKE ''%'+@v_budgetcode_inVc+'%'''
  --                      END
  --              ELSE
  --                      BEGIN
  --                              SET @v_TempQuery_Vc= @v_TempQuery_Vc + ' AND budgetCode LIKE ''%'+@v_budgetcode_inVc+'%'''
  --                      END
  --              END		

        --Creating final query and executing final query
        IF @v_TempQuery_Vc=''
                BEGIN
                        SET @v_IntialQuery_Vc='FROM vw_get_AppData'
                END
        ELSE
                BEGIN
                        SET @v_IntialQuery_Vc='FROM vw_get_AppData WHERE'
                END

        --print @v_IntialQuery_Vc + @v_TempQuery_Vc
        --Exec(@v_IntialQuery_Vc + @v_TempQuery_Vc)

        declare @v_ExecutingQuery_Vc as varchar(max)
        set @v_ExecutingQuery_Vc='DECLARE @v_Reccountf FLOAT
        DECLARE @v_TotalPagef FLOAT
        SELECT @v_Reccountf = Count(*) From (
        SELECT * From (SELECT ROW_NUMBER() OVER (order by appid desc) As Rownumber,[appId],[appCode],[departmentName],[employeeName],[stateName],[procurementnature],[packageNo],[packageDesc],[projectName],[financialYear],[departmentId],[employeeId],[cpvCode],[procurementType],[estimatedCost],[pkgEstCost],[officeId],convert(varchar(20),[budgetType]) budgetType,[procurementMethod],[officeName],[packageId],workflowStatus,appstatus status
        '+@v_IntialQuery_Vc+''+@v_TempQuery_Vc+') AS DATA) AS TTT
        SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_RecordPerPage_inN as varchar(10))+')
        SELECT *,@v_TotalPagef as TotalPages,convert(int,@v_Reccountf) as TotalRecords From (SELECT ROW_NUMBER() OVER (order by appid desc) As Rownumber,[appId],[appCode],[departmentName],[employeeName],[stateName],[procurementnature],[packageNo],[packageDesc],[projectName],[financialYear],[departmentId],[employeeId],[cpvCode],[procurementType],[estimatedCost],[pkgEstCost],[officeId],convert(varchar(20),[budgetType]) budgetType,[procurementMethod],[officeName],[packageId],workflowStatus ,appstatus status
        '+@v_IntialQuery_Vc+''+@v_TempQuery_Vc+') AS DATA where Rownumber between '+cast(((@v_Page_inN - 1) * @v_RecordPerPage_inN + 1) as varchar(10))+'
        AND '+cast((@v_Page_inN * @v_RecordPerPage_inN) as varchar(10))+''

        print @v_ExecutingQuery_Vc
        Exec(@v_ExecutingQuery_Vc)


END

