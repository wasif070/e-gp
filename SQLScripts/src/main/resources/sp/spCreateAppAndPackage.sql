USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[spCreateAppAndPackage]    Script Date: 4/24/2016 11:22:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spCreateAppAndPackage]
	-- Add the parameters for the stored procedure here
	
	@v_Procurementnature_inVc varchar(10),
	
	
	@v_IsTenderCreate bit=1,
	-- Input Params
	-- Declare
	@v_TempleteName varchar(150),
	@v_IsDocFillUp  bit=1,
	@v_IsTenderEvaluation  bit=1,
	@v_IsTenderWorflow  bit=1,
	@v_IsOpenComite  bit=1,
	@v_IsCommitteeEncryptionDecryp  bit=1,
	@v_IsNoticeWorkFlow  bit=1,
	@v_IsDocFeePayment  bit=1,
	@v_IsSecqurityPayment  bit=1,
	@CreatedBy nvarchar(100),
	@TOC nvarchar(100),
	@TEC nvarchar(100),
	@Tender1 nvarchar(100),
	@Tender2 nvarchar(100),
	@Tender3 nvarchar(100),
	@ApprovingAuthority nvarchar(100)
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	SET NOCOUNT ON;
	
	Declare @v_BudgetType_intInt tinyint
	Declare @v_ProjectId_inInt tinyint
	Declare @v_AppCode_inVc varchar(50)	
	Declare @v_ServicesType_inVc varchar(100)
	Declare @v_PackageNo_inVc varchar(50)
	Declare @v_ProcurementMethodId_intInt nvarchar(50)
	Declare @v_ProcurementType_inVc varchar(3)
	Declare @v_SourceOfFund_inVc varchar(200)
	Declare @v_Quantity_inVc numeric(18,4)
	Declare @v_Unit_inVc varchar(max)
	Declare @v_LotEstCost_inVc money
	Declare @Status nvarchar(50)
	Declare @advtDt Date 
	
	Declare @v_ReoiRfpRefNo_inVc varchar(50)
				
	Declare @v_EvalType_inVc varchar(20) ----'Package wise','Lot wise'
	Declare @v_DocFeesMode_inVc varchar(20)	
	
	
	-- Input Params for table tbl_TenderLotSecurity
	--declare
	Declare @v_Location_inVc varchar(max)
	Declare @v_DocFess_inVc varchar(max)
	Declare @v_TenderSecurityAmt_inM varchar(max)
	
	-- Input Params for table tbl_TenderPhasing
	Declare @v_PhasingRefNo_inVc varchar(max)
	Declare @v_PhasingOfService_inVc varchar(max)
	Declare @v_PhasingLocation_inVc varchar(max)
	-- Input Params for table tbl_TenderAuditTrail
	
	--Input Params use to compare old value and insert field into tbl_corrignedium
	Declare @v_ContractTypeNew_Vc varchar(20)  -------'Time based','Lump – Sum' if contractType REOI	
	Declare @v_PkgDocFees_Vc varchar(20)  --0.00,
	Declare @v_PassingMarks_Int int	
	Declare @v_PkgDocFeesUSD_Vc varchar(20)
	Declare @v_TenderSecurityAmt_M_USD varchar(20)
	Declare @v_tenderPubDt_Vc datetime
	
	
	
	Declare @v_FinancialYear_inVc nvarchar(50)
	Declare @v_ProjectName_inVc nvarchar(150)
	Declare @v_OfficeId_inInt int
	Declare @v_EmployeeId_inInt int
	Declare @v_TEC_inInt int
	Declare @v_TOC_inInt int
	Declare @v_Tender1_inInt int
	Declare @v_Tender2_inInt int
	Declare @v_Tender3_inInt int
	Declare @v_AAEmployeeId_inInt int
	Declare @v_ApprovedAuthority_inInt int
	Declare @v_CreatedBy_inInt int
	Declare @v_AAProcurementRoleId_intInt int
	Declare @v_DepartmentId_Int int
	
	Declare @v_AllocateBudget_inM money
	Declare @v_CpvCode_inVc varchar(8000)
	
	Declare @v_Emergency nvarchar(150)
	Declare @v_PackageDesc_inVc varchar(2000)
	Declare @v_PkgEstCost_inM money=0
	
	Declare @AppId int
	Declare @PackageId int
	Declare @AppCodeCount int
	Declare @v_TenderType_inVc varchar(50)
	Declare @v_Action_inVc varchar(50)
	Declare @v_EligibilityCriteria_inVc varchar(2000) 
	Declare @v_Deliverables_inVc varchar(2000)
	Declare @v_OtherDetails_inVc varchar(2000)
	Declare @v_ForeignFirm_inVc varchar(20)
	Declare @v_DocAvlMethod_inVc varchar(10)
	Declare @v_SecuritySubOff_inVc varchar(1000)=''
	Declare @v_DocFeesMethod_inVc varchar(20)
	Declare @v_Tenderid_inInt int
	Declare @v_Corrid_inInt int
	Declare @v_DocOfficeAdd_inVc varchar(2000)
	Declare @v_TenderBrief_inVc varchar(2000)
	
	
	
	Declare @v_PreBidStartDt_insDt smalldatetime=Null
	Declare @v_PreBidEndDt_insDt smalldatetime=Null
	Declare @v_SecurityLastDt_inDt smalldatetime=Null
	Declare @v_DocEndDate_insDt smalldatetime=Null
	Declare @v_SubmissionDt_insDt smalldatetime=Null
	Declare @v_OpeningDt_insDt smalldatetime=Null	
	Declare @v_CompletionTime_inVc varchar(max)=''
	Declare @v_StartTime_inVc varchar(max)=''
	Declare @v_IndStartDt_inVc varchar(max)=''
	Declare @v_IndEndDt_inVc varchar(max)=''
	Declare @v_stdTemplateId_Int int
	
	Declare @PDate datetime
	
	----------===========Variable Assign
	
	
	Select  @v_BudgetType_intInt=budgetTypeId from tbl_BudgetType Where budgetType='Revenue'
	
	Set @v_AppCode_inVc=@v_Procurementnature_inVc +' '+CAST(GETDATE() as nvarchar(50))
	
	Set @v_BudgetType_intInt =1
	Set @v_ProjectId_inInt =162
	Set @v_AppCode_inVc =@v_Procurementnature_inVc +' '+CAST(GETDATE() as nvarchar(50))
	Set @v_ServicesType_inVc ='6'
	Set @v_PackageNo_inVc =@v_Procurementnature_inVc +' '+CAST(GETDATE() as nvarchar(50))
	Set @v_ProcurementMethodId_intInt ='2'
	Set @v_ProcurementType_inVc ='NCT'
	Set @v_SourceOfFund_inVc ='Govment'
	Set @v_Quantity_inVc =500
	Set @v_Unit_inVc ='PCS'
	Set @v_LotEstCost_inVc =50000
	Set @Status ='Approved'
	Set @advtDt =GETDATE()
	
	
	
	Set @v_ReoiRfpRefNo_inVc  =@v_Procurementnature_inVc +' '+CAST(GETDATE() as nvarchar(50))
				
	Set @v_EvalType_inVc ='Lot wise' ----'Package wise','Lot wise'
	Set @v_DocFeesMode_inVc ='Bank'
	
	
	-- Input Params for table tbl_TenderLotSecurity
	--declare
	Set @v_Location_inVc ='Dhaka'	
	Set @v_DocFess_inVc ='510'
	Set @v_TenderSecurityAmt_inM ='500'	
	
	-- Input Params for table tbl_TenderPhasing
	Set @v_PhasingRefNo_inVc =''
	Set @v_PhasingOfService_inVc =''
	Set @v_PhasingLocation_inVc ='Dohar'
	-- Input Params for table tbl_TenderAuditTrail
	
	--Input Params use to compare old value and insert field into tbl_corrignedium
	Set @v_ContractTypeNew_Vc =''  -------'Time based','Lump – Sum' if contractType REOI	
	Set @v_PkgDocFees_Vc ='500'  --0.00,
	Set @v_PassingMarks_Int ='0'	
	Set @v_PkgDocFeesUSD_Vc ='550'
	Set @v_TenderSecurityAmt_M_USD ='560'
	Set @v_tenderPubDt_Vc =GETDATE()
	
	
	
	Set @v_AllocateBudget_inM=0
	Set @v_Emergency='Normal'
	set @v_CpvCode_inVc='Electrical machinery, apparatus, equipment and consumables'
	Set @v_PkgEstCost_inM=@v_LotEstCost_inVc
	Set @v_PackageDesc_inVc =@v_PackageNo_inVc + @v_AppCode_inVc
	
	Set @v_TenderType_inVc='Package'
	Set @v_Action_inVc='INSERT'
	Set @v_EligibilityCriteria_inVc='<p><em>Script Generated Tender</em></p> '
	Set @v_Deliverables_inVc='<p>   ok</p>  '
	Set @v_OtherDetails_inVc='<p><em>Script Generated Tender</em></p>'
	Set @v_ForeignFirm_inVc=''
	set @v_DocAvlMethod_inVc='Package'
	Set @v_SecuritySubOff_inVc=''
	Set @v_DocFeesMethod_inVc='Package wise'
	Set @v_Corrid_inInt=0
	Set @v_DocOfficeAdd_inVc=''
	set @v_TenderBrief_inVc='<p> Test</p>  '
	
	-----------------*********** Date Calculate **************--------------
	Set @PDate=@v_tenderPubDt_Vc 
	Set @v_PreBidStartDt_insDt= CAST(dateadd(DAY,2,@PDate) as varchar(50))
	Set @v_PreBidEndDt_insDt= CAST(dateadd(DAY,3,@PDate) as varchar(50))
	Set @v_SecurityLastDt_inDt= CAST(dateadd(DAY,7,@PDate) as varchar(50))
	Set @v_DocEndDate_insDt= CAST(dateadd(DAY,9,@PDate) as varchar(50))
	Set @v_SubmissionDt_insDt= CAST(dateadd(DAY,11,@PDate) as varchar(50))
	Set @v_OpeningDt_insDt= CAST(dateadd(DAY,15,@PDate) as varchar(50))
	Set @v_CompletionTime_inVc= CONVERT(char(10), dateadd(Day,80,@PDate),103)
	Set @v_StartTime_inVc=  CONVERT(char(10), dateadd(Day,40,@PDate),103)
	Set @v_IndStartDt_inVc= CAST(dateadd(DAY,23,@PDate) as varchar(50))
	Set @v_IndEndDt_inVc= CAST(dateadd(Day,24,@PDate) as varchar(50))
	
	
	--Set @v_stdTemplateId_Int=Case When @v_Procurementnature_inVc='Goods' then 344
	--							  When @v_Procurementnature_inVc='Works' then 347 end
	
	----------------*********** ******************************-------------------
	
	
	BEGIN TRANSACTION trans
	BEGIN TRY
	
	Select @AppCodeCount=COUNT(appId) from tbl_AppMaster Where appCode=@v_AppCode_inVc
	if @AppCodeCount >0 
	begin
		raiserror('AppCode Already Exits', 18, 0)
	end
	Select @AppCodeCount=COUNT(packageId) from tbl_AppPackages Where packageNo=@v_PackageNo_inVc
	if @AppCodeCount >0 
	begin
		raiserror('Package No already Exits', 18, 0)
	end
	
	Select @v_stdTemplateId_Int=templateId from tbl_TemplateMaster Where templateName =@v_TempleteName
	if (@v_stdTemplateId_Int <=0 or ISNULL(@v_stdTemplateId_Int,'')='')
	begin
		raiserror('Template Does not Found, Please Correct Template Name', 18, 0)
	end
	---Year
	Select @v_FinancialYear_inVc=financialYear from tbl_FinancialYear Where isCurrent='Yes'
	---Project Name
	Select top 1 @v_ProjectName_inVc=projectName from tbl_ProjectMaster --Where projectId=@v_ProjectId_inInt
	
	--Employee PE
	Declare @empID bigint
	Select @empID=E.userId from tbl_LoginMaster L inner join tbl_employeemaster E
		On L.userId=E.userId Where L.emailId=@CreatedBy
	
	Select @v_EmployeeId_inInt=E.employeeId from tbl_LoginMaster L inner join tbl_employeemaster E
		On L.userId=E.userId Where L.emailId=@CreatedBy
	--OfficeId
	Select  @v_OfficeId_inInt= officeId from tbl_EmployeeOffices Where employeeId=@v_EmployeeId_inInt 
	
	---TEC
	Select @v_TEC_inInt=E.userId from tbl_LoginMaster L inner join tbl_employeemaster E
	On L.userId=E.userId Where L.emailId=@TEC
	--TOC
	Select @v_TOC_inInt=E.userId from tbl_LoginMaster L inner join tbl_employeemaster E
		On L.userId=E.userId Where L.emailId=@TOC
   ----Approve Authority
   Select @v_ApprovedAuthority_inInt=E.userId from tbl_LoginMaster L inner join tbl_employeemaster E
		On L.userId=E.userId Where L.emailId=@ApprovingAuthority
   	
   Select @v_Tender1_inInt=userId from tbl_LoginMaster WHERE emailId=@Tender1  
   Select @v_Tender2_inInt=userId from tbl_LoginMaster WHERE emailId=@Tender2 
   Select @v_Tender3_inInt=userId from tbl_LoginMaster WHERE emailId=@Tender3  

	--User Id PE
	Select  @v_CreatedBy_inInt= userId from tbl_employeemaster Where employeeId=@v_EmployeeId_inInt
	
	--ApproveUserID
	set @v_AAEmployeeId_inInt=@v_EmployeeId_inInt
	
	set @v_AAProcurementRoleId_intInt=1
	--Department ID
	Select @v_DepartmentId_Int=D.departmentId from tbl_OfficeMaster O inner join tbl_DepartmentMaster D
	on O.departmentId=D.departmentId Where O.officeId=@v_OfficeId_inInt

	--Select top 1 @v_DepartmentId_Int= departmentId from tbl_OfficeMaster --Where officeId =355
	
	-----------==============================================-------------
	
    -- Insert statements APP Master
	INSERT INTO [dbo].[tbl_AppMaster]
				   ([financialYear]
				   ,[budgetType]
				   ,[projectId]
				   ,[projectName]
				   ,[officeId]
				   ,[employeeId]
				   ,[aAEmployeeId]
				   ,[appCode]
				   ,[createdBy]
				   ,[departmentId]
				   ,[aAProcurementRoleId]
				   ,[appStatus])
				VALUES
				   (@v_FinancialYear_inVc,
					@v_BudgetType_intInt,
					@v_ProjectId_inInt,
					@v_ProjectName_inVc,
					@v_OfficeId_inInt,
					@v_EmployeeId_inInt,
					@v_AAEmployeeId_inInt,
					@v_AppCode_inVc,
					@v_CreatedBy_inInt,
					@v_DepartmentId_Int,
					@v_AAProcurementRoleId_intInt,
					@Status)
	set @AppId=Ident_Current('dbo.[tbl_AppMaster]') 
	
	
	INSERT INTO [dbo].[tbl_AppPackages]
					   ([appId]
					   ,[procurementnature]
					   ,[servicesType]
					   ,[packageNo]
					   ,[packageDesc]
					   ,[allocateBudget]
					   ,[estimatedCost]
					   ,[cpvCode]
					   ,[pkgEstCost]
					   ,[approvingAuthEmpId]
					   ,[isPQRequired]
					   ,[reoiRfaRequired]
					   ,[procurementMethodId]
					   ,[procurementType]
					   ,[sourceOfFund]
					   ,[appStatus]
					   ,[workflowStatus]
					   ,[noOfStages],[pkgUrgency])
				 VALUES
					   (@AppId ,
						@v_Procurementnature_inVc,
						@v_ServicesType_inVc,
						@v_PackageNo_inVc,
						@v_PackageDesc_inVc,
						@v_AllocateBudget_inM,
						@v_LotEstCost_inVc,
						@v_CpvCode_inVc,
						@v_PkgEstCost_inM,
						1,
						'',
						'' ,
						@v_ProcurementMethodId_intInt,
						@v_ProcurementType_inVc,
						@v_SourceOfFund_inVc,
						@Status,
						@Status,
						0,'Normal')
	set @PackageId=Ident_Current('dbo.[tbl_AppPackages]') 
	
	INSERT INTO [dbo].[tbl_AppPkgLots]
					   ([packageid]
					   ,[appId]
					   ,[lotNo]
					   ,[lotDesc]
					   ,[quantity]
					   ,[unit]
					   ,[lotEstCost])
				VALUES(@PackageId,
					   @AppId,
					   --dbo.f_HandleSpChar(@v_LotDesc_inVc),
					   1,
					   @v_PackageDesc_inVc,
					   @v_Quantity_inVc,
					   @v_Unit_inVc,
					   @v_LotEstCost_inVc)
					   
					   
			Insert Into [dbo].[Tbl_AppPqTenderDates]	
				(appid, packageId, advtDt, advtDays, 
				subDt, subDays, openDt, openDays, evalRptDt, 
				evalRptDays, appLstDt, tenderAdvertDt, tenderAdvertDays, tenderSubDt, 
				tenderSubDays, tenderOpenDt, tenderOpenDays, techSubCmtRptDt, techSubCmtRptDays, 
				tenderEvalRptDt, tenderEvalRptdays, tenderEvalRptAppDt, tenderEvalRptAppDays, tenderContractAppDt, 
				tenderContractAppDays, tenderNoaIssueDt, tenderNoaIssueDays, tenderContractSignDt, tenderContractSignDays, 
				tenderContractCompDt, reoiReceiptDt, reoiReceiptDays, rfpTechEvalDt, rfpTechEvalDays, 
				rfpFinancialOpenDt, rfpFinancialOpenDays, rfpNegCompDt, rfpNegCompDays, rfpContractAppDt, 
				rfpContractAppDays, rfaAdvertDt, rfaAdvertDays, rfaReceiptDt, rfaReceiptDays, 
				rfaEvalDt, rfaEvalDays, rfaInterviewDt, rfaInterviewDays, rfaFinalSelDt, 
				rfaFinalSelDays, rfaEvalRptSubDt, rfaEvalRptSubDays, rfaAppConsultantDt, actAdvtDt, 
				actSubDt, actEvalRptDt, actAppLstDt, actTenderAdvertDt, actTenderSubDt, 
				actTenderOpenDt, actTechSubCmtRptDt, actTenderEvalRptDt, acttenderEvalRptAppDt, actTenderContractAppDt, 
				actTenderNoaIssueDt, actTenderContractSignDt, actTenderContractCompDt, actReoiReceiptDt, actRfpTechEvalDt, 
				actRfpFinancialOpenDt, actRfpNegComDt, actRfpContractAppDt, actRfaAdvertDt, actRfaReceiptDt, 
				actRfaEvalDt, actRfaInterviewDt, actRfaFinalSelDt, actRfaEvalRptSubDt, actRfaAppConsultantDt, 
				pkgPubDate)
			Values(@AppId, @packageId, @advtDt, 1, 
					@advtDt, 2, @advtDt, 3, @advtDt, 
					4, @advtDt, @advtDt, 5, @advtDt, 
					6, @advtDt, 7, @advtDt, 8, 
					@advtDt, 9, @advtDt, 10, @advtDt, 
					11, @advtDt, 12, @advtDt, 13, 
					@advtDt, @advtDt, 16, @advtDt, 17, 
					@advtDt, 18, @advtDt, 19, @advtDt, 
					20, @advtDt, 21, @advtDt, 22, 
					@advtDt, 23, @advtDt, 24, @advtDt, 
					25, @advtDt, 26, @advtDt, @advtDt, 
					@advtDt, @advtDt, @advtDt, @advtDt, @advtDt, 
					@advtDt, @advtDt, @advtDt, @advtDt, @advtDt, 
					@advtDt, @advtDt, @advtDt, @advtDt, @advtDt, 
					@advtDt, @advtDt, @advtDt, @advtDt, @advtDt, 
					@advtDt, @advtDt, @advtDt, @advtDt, @advtDt, 
					null)
	-----********tbl_WorkFlowEventConfig******----------				
	INSERT INTO [dbo].[tbl_WorkFlowEventConfig]
           ([noOfReviewer]
           ,[noOfDays]
           ,[isDonorConReq]
           ,[eventId]
           ,[objectId])
     VALUES
           (0
           ,0
           ,'NO'
           ,1
           ,@AppId)
           
	--------===========Tbl_WorkFlowFileHistory=====---------------
	INSERT INTO [dbo].[tbl_WorkFlowFileHistory]
           ([wfFileOnHandId]
           ,[moduleName]
           ,[eventName]
           ,[activityName]
           ,[activityId]
           ,[childId]
           ,[objectId]
           ,[processDate]
           ,[endDate]
           ,[fromUserId]
           ,[toUserId]
           ,[fileSentFrom]
           ,[fileSentTo]
           ,[wfRoleid]
           ,[fileOnHand]
           ,[action]
           ,[workflowDirection]
           ,[remarks]
           ,[wfTrigger]
           ,[documentId])
     VALUES
           (762
           ,'Annual Procurement Plan (APP)'
           ,'App Approval Workflow'
           ,'App Approval Workflow'
           ,1
           ,@AppId
           ,@PackageId
           ,GETDATE()
           ,GETDATE()
           ,@v_TOC_inInt
           ,@v_TEC_inInt
           ,'PE User New - Procurement Entity'
           ,'HOPE User New - Hope User'
           ,2
           ,'Yes'
           ,'Forward'
           ,'Up'
           ,'<p>   sdf</p>'
           ,'system'
           ,'')
           
     INSERT INTO [dbo].[tbl_WorkFlowFileHistory]
           ([wfFileOnHandId]
           ,[moduleName]
           ,[eventName]
           ,[activityName]
           ,[activityId]
           ,[childId]
           ,[objectId]
           ,[processDate]
           ,[endDate]
           ,[fromUserId]
           ,[toUserId]
           ,[fileSentFrom]
           ,[fileSentTo]
           ,[wfRoleid]
           ,[fileOnHand]
           ,[action]
           ,[workflowDirection]
           ,[remarks]
           ,[wfTrigger]
           ,[documentId])
     VALUES
           (762
           ,'Annual Procurement Plan (APP)'
           ,'App Approval Workflow'
           ,'App Approval Workflow'
           ,1
           ,@AppId
           ,@PackageId
           ,GETDATE()
           ,GETDATE()
           ,@v_TOC_inInt
           ,@v_TEC_inInt
           ,'HOPE User New - Hope User'
           ,'PE User New - Procurement Entity'
           ,1
           ,'Yes'
           ,'Approve'
           ,'Down'
           ,'<p>   sdf</p>'
           ,'system'
           ,'')
     ----------============================--------
     
     -------*********	Tender ***********----------------
     if @v_IsTenderCreate=1 
     begin
		exec  p_add_upd_tendernotice_Dohatech 
		@AppId ,
		@PackageId ,
		@v_TenderType_inVc ,-- Package/Lot
		@v_CreatedBy_inInt ,

		-- Input Params
		-- Declare
		@v_ReoiRfpRefNo_inVc ,
		@v_DocEndDate_insDt ,
		@v_PreBidStartDt_insDt,
		@v_PreBidEndDt_insDt ,
		@v_SubmissionDt_insDt,
		@v_OpeningDt_insDt ,
		@v_EligibilityCriteria_inVc,
		@v_TenderBrief_inVc ,
		@v_Deliverables_inVc,
		@v_OtherDetails_inVc,
		@v_ForeignFirm_inVc ,
		@v_DocAvlMethod_inVc,
		@v_EvalType_inVc ,
		@v_DocFeesMethod_inVc ,
		@v_DocFeesMode_inVc ,
		@v_DocOfficeAdd_inVc,
		@v_SecurityLastDt_inDt,
		@v_SecuritySubOff_inVc ,
		-- Input Params for table tbl_TenderLotSecurity
		--declare
		@v_Location_inVc ,
		@v_DocFess_inVc ,
		@v_TenderSecurityAmt_inM ,
		@v_CompletionTime_inVc ,

		@v_Action_inVc ,
		@v_Tenderid_inInt,
		@v_Corrid_inInt ,
		-- Input Params for table tbl_TenderPhasing
		@v_PhasingRefNo_inVc ,
		@v_PhasingOfService_inVc,
		@v_PhasingLocation_inVc ,
		@v_IndStartDt_inVc ,
		@v_IndEndDt_inVc ,

		-- Input Params for table tbl_TenderAuditTrail
		'',
		'' ,
		'' ,

		--Input Params use to compare old value and insert field into tbl_corrignedium
		@v_ContractTypeNew_Vc ,
		@v_tenderPubDt_Vc ,
		@v_PkgDocFees_Vc ,  --0.00,
		@v_PkgEstCost_inM ,
		@v_PassingMarks_Int,
		@v_StartTime_inVc,
		@v_PkgDocFeesUSD_Vc,	
		@v_TenderSecurityAmt_M_USD 
		
		
		
		set @v_Tenderid_inInt=Ident_Current('dbo.[tbl_TenderMaster]') 
     
		 INSERT INTO [dbo].[tbl_TenderOpenDates]
			   ([tenderId]
			   ,[openingDt]
			   ,[entryDt]
			   ,[envelopeId])
		 VALUES
			   (@v_Tenderid_inInt
			   ,@v_OpeningDt_insDt
			   ,getdate()
			   ,2)


		 INSERT INTO [dbo].[tbl_TenderPostQueConfig]
						   ([tenderId]
						   ,[isQueAnsConfig]
						   ,[createdBy]
						   ,[createdDt]
						   ,[postQueLastDt])
					 VALUES
						   (@v_Tenderid_inInt
						   ,'No'
						   ,@v_CreatedBy_inInt
						   ,GETDATE()
						   ,null)
						   
	   INSERT INTO [dbo].[tbl_TenderEstCost]
			   ([tenderId]
			   ,[pkgLotId]
			   ,[estCost]
			   ,[createdBy]
			   ,[createdDt])
		 VALUES
			   (@v_Tenderid_inInt
			   ,(Select top 1 appPkgLotId from  tbl_TenderLotSecurity Where tenderId=@v_Tenderid_inInt)
			   ,@v_LotEstCost_inVc
			   ,@v_CreatedBy_inInt
			   ,GETDATE())
	           
		Update tbl_TenderDetails Set stdTemplateId= @v_stdTemplateId_Int ,approvingAuthId=@v_ApprovedAuthority_inInt  Where tenderId=@v_Tenderid_inInt  
		    
	    
		exec p_add_upd_tendertemplate
		'insert',       
		@v_Tenderid_inInt,
		@v_stdTemplateId_Int,
		@v_CreatedBy_inInt
	    
	    if @v_IsDocFillUp=1  and @v_IsTenderCreate=1 
	    begin
		exec spTenderDocInsert @v_Tenderid_inInt,@v_CreatedBy_inInt,@v_Procurementnature_inVc
		END
		
		----------////////Tender Evlation Commitee\\\\\\\\\\\---------
		if @v_IsTenderEvaluation=1 and @v_IsDocFillUp=1  and @v_IsTenderCreate=1 
		begin
			exec spInsertComitee @v_Tenderid_inInt,@v_CreatedBy_inInt,6,1,'Evaluation Committee','TEC',@v_TOC_inInt,@v_TEC_inInt
		END
		----------//////Opening Comitee\\\\\\\\\\\\\--------
		If @v_IsOpenComite=1 and @v_IsTenderEvaluation=1 and @v_IsDocFillUp=1  and @v_IsTenderCreate=1 
		Begin
			exec spInsertComitee @v_Tenderid_inInt,@v_CreatedBy_inInt,5,2,'Opening Committee','TOC',@v_TOC_inInt,@v_TEC_inInt
		END	
		
		Declare @v_HashCode nvarchar(50)
		set @v_HashCode= Cast(@v_TOC_inInt as varchar(50))+','+ Cast(@v_TEC_inInt as varchar(50))
		
		--------////////Encription Decription in Notice\\\\\\---------
		if @v_IsCommitteeEncryptionDecryp=1 AND @v_IsOpenComite=1 and @v_IsTenderEvaluation=1 and @v_IsDocFillUp=1  and @v_IsTenderCreate=1 
		begin
			INSERT INTO [dbo].[tbl_TenderHash]
			   ([tenderId]
			   ,[tenderHashUserId]
			   ,[tenderHash]
			   ,[creationDate]
			   ,[createdBy])
			VALUES
			   (@v_Tenderid_inInt
			   , @v_HashCode
			   ,'418158649655880539cf3b092940a06e97a9c017'
			   ,GETDATE()
			   ,@v_CreatedBy_inInt)
        END   
        
        ------//////////Notice Workflow\\\\\\\\\\---------------
		if @v_IsNoticeWorkFlow=1 AND @v_IsCommitteeEncryptionDecryp=1 AND @v_IsOpenComite=1 and @v_IsTenderEvaluation=1 and @v_IsDocFillUp=1  and @v_IsTenderCreate=1 
		Begin
					 INSERT INTO [dbo].[tbl_WorkFlowEventConfig]
						   ([noOfReviewer]
						   ,[noOfDays]
						   ,[isDonorConReq]
						   ,[eventId]
						   ,[objectId])
					 VALUES
						   (0
						   ,0
						   ,'NO'
						   ,2
						   ,@v_Tenderid_inInt)
				     
					 INSERT INTO [dbo].[tbl_WorkFlowLevelConfig]
										([eventId]
										,[moduleId]
										,[wfLevel]
										,[userId]
										,[wfRoleId]
										,[objectId]
										,[actionDate]
										,[activityId]
										,[childId]
										,[fileOnHand]
										,[procurementRoleId])
							Values(2,2,2,@v_CreatedBy_inInt,1,@v_Tenderid_inInt,GETDATE(),2,@v_Tenderid_inInt,'YES',1)			
					
						INSERT INTO [dbo].[tbl_WorkFlowLevelConfig]
										([eventId]
										,[moduleId]
										,[wfLevel]
										,[userId]
										,[wfRoleId]
										,[objectId]
										,[actionDate]
										,[activityId]
										,[childId]
										,[fileOnHand]
										,[procurementRoleId])
							Values(2,2,2,@v_CreatedBy_inInt,2,@v_Tenderid_inInt,GETDATE(),2,@v_Tenderid_inInt,'NO',1)	
						
					  INSERT INTO [dbo].[tbl_TenderAuditTrail]
						   ([tenderId]
						   ,[action]
						   ,[actionBy]
						   ,[actionDate]
						   ,[eSignature]
						   ,[digitalSignature]
						   ,[activity]
						   ,[remarks])
					 VALUES
						   (@v_Tenderid_inInt
						   ,'Approved'
						   ,@v_TOC_inInt
						   ,GETDATE()
						   ,'cef3f8ba1420fced388fa86a0cef0786a4736229'
						   ,''
						   ,'Approved'
						   ,'Script Tender Publisahed')
				           

					Update tbl_TenderDetails Set tenderStatus='Approved' Where tenderId=@v_Tenderid_inInt
						
					   END
						
					 
     
	 END	      
     
     if(@v_IsDocFeePayment=1 AND @v_IsNoticeWorkFlow=1 AND @v_IsCommitteeEncryptionDecryp=1 AND @v_IsOpenComite=1 and @v_IsTenderEvaluation=1 and @v_IsDocFillUp=1  and @v_IsTenderCreate=1 )
     begin
     exec spDocumentFreeFillUp @v_Tenderid_inInt,@v_CreatedBy_inInt,@v_Tender1_inInt
     exec spDocumentFreeFillUp @v_Tenderid_inInt,@v_CreatedBy_inInt,@v_Tender2_inInt
     exec spDocumentFreeFillUp @v_Tenderid_inInt,@v_CreatedBy_inInt,@v_Tender3_inInt
     end
    if(@v_IsSecqurityPayment=1 AND @v_IsDocFeePayment=1 AND @v_IsNoticeWorkFlow=1 AND @v_IsCommitteeEncryptionDecryp=1 AND @v_IsOpenComite=1 and @v_IsTenderEvaluation=1 and @v_IsDocFillUp=1  and @v_IsTenderCreate=1)
    Begin
     exec spTenderSecqurity @v_Tenderid_inInt,@v_CreatedBy_inInt,@v_Tender1_inInt
     exec spTenderSecqurity @v_Tenderid_inInt,@v_CreatedBy_inInt,@v_Tender2_inInt
     exec spTenderSecqurity @v_Tenderid_inInt,@v_CreatedBy_inInt,@v_Tender3_inInt
     
     INSERT INTO [dbo].[tbl_BidConfirmation]
           ([tenderId]
           ,[maskName]
           ,[confirmationDt]
           ,[eSignature]
           ,[digitalSignature]
           ,[userId]
           ,[confirmationStatus])
     VALUES
           (@v_Tenderid_inInt
           ,''
           ,GETDATE()
           ,''
           ,''
           ,@v_Tender1_inInt
           ,'accepted')
      INSERT INTO [dbo].[tbl_BidConfirmation]
           ([tenderId]
           ,[maskName]
           ,[confirmationDt]
           ,[eSignature]
           ,[digitalSignature]
           ,[userId]
           ,[confirmationStatus])
     VALUES
           (@v_Tenderid_inInt
           ,''
           ,GETDATE()
           ,''
           ,''
           ,@v_Tender2_inInt
           ,'accepted')     
     INSERT INTO [dbo].[tbl_BidConfirmation]
           ([tenderId]
           ,[maskName]
           ,[confirmationDt]
           ,[eSignature]
           ,[digitalSignature]
           ,[userId]
           ,[confirmationStatus])
     VALUES
           (@v_Tenderid_inInt
           ,''
           ,GETDATE()
           ,''
           ,''
           ,@v_Tender3_inInt
           ,'accepted')    
           
     --exec spTenderBidInsert    @v_Tenderid_inInt,2272,2255
     --exec spTenderBidInsert    @v_Tenderid_inInt,2272,2256   
     --exec spTenderBidInsert    @v_Tenderid_inInt,2272,2270     
     
    END       
   
     
    Select 'Insert Successful' Massage,  @AppId AppID ,@PackageId PackageID,@v_Tenderid_inInt tenderID
     
	COMMIT TRANSACTION trans
	END TRY
	BEGIN CATCH
		select  ERROR_NUMBER() Number, ERROR_MESSAGE() as Message
		ROLLBACK TRANSACTION;	
	END CATCH;
					
END

