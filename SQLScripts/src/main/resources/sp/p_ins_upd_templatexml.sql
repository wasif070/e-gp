USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_ins_upd_templatexml]    Script Date: 4/24/2016 11:16:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Insert/Update/Delete a specific table 
--
--
-- Author: Karan
-- Date: 01-11-2010
--
-- Last Modified:
-- Modified By: Karan
-- Date 25-11-2010
-- Modification: New Code added for updatetable' case
--------------------------------------------------------------------------------
-- SP Name	   :   p_ins_upd_templatexml
-- Module	   :   N.A.
-- Description :   Performs transaction on single Table.
-- Function	   :   
--				insert - Inserts into Table through XML
--				update - Updates into Table through XML
--				delete - Deletes from Table through XML
--				insdel - Deletes from Table and then Inserts into Table through XML					
--				updatetable - Updates into Table by parametrized query

ALTER PROCEDURE [dbo].[p_ins_upd_templatexml]
@v_action_inVc varchar(50),
@v_tablename_inVc varchar(500),
@v_xml_text_inVc varchar(max),
@v_conditions_inVc varchar(max)=null
AS

BEGIN

SET NOCOUNT ON;

DECLARE  @v_flag_bit bit

Declare @v_InsQry_Vc varchar(max), @v_UpdQry_Vc varchar(max), @v_DelQry_Vc varchar(max),
		@v_NodePath_Vc varchar(500), @v_Columns_Vc varchar(max), @v_PkColumn_Vc varchar(500),
		@v_TblStructure_Vc varchar(max)

	SET @v_xml_text_inVc = dbo.f_HandleSpChar(@v_xml_text_inVc)		
	
	IF @v_action_inVc='insert'
	BEGIN
		--BEGIN TRY
			--BEGIN TRAN	
			PRINT(@v_xml_text_inVc)
				SET @v_NodePath_Vc = '/root/'+@v_tablename_inVc				
				/* START:  THIS CODE IS USING STAR(*) QUERY */
				
				SET @v_InsQry_Vc='
				Declare @i INT, @IdentVal int, @v_Success_bit bit;
				EXEC sp_xml_preparedocument @i OUTPUT, '''+@v_xml_text_inVc+''';
				INSERT INTO ' +@v_tablename_inVc +'	
				SELECT * FROM 
					OPENXML(@i, '''+ @v_NodePath_Vc + ''') With '+@v_tablename_inVc+';
				EXEC sp_xml_removedocument @i;
				Select @IdentVal=SCOPE_IDENTITY();'
				
				SELECT @v_InsQry_Vc=
				'BEGIN TRY
					BEGIN TRAN'
						+@v_InsQry_Vc+
				+'IF @IdentVal is Not Null
				Begin
						Set @v_Success_bit=1
						 Select @v_Success_bit as flag, ''New record(s) inserted into table - '+@v_tablename_inVc+''' as Message, @IdentVal as Id
				End
				Else 
				Begin
					Set @v_Success_bit=0
						Select @v_Success_bit as flag, ''Error while inserting into table - '+@v_tablename_inVc + ''' as Message, 0 as Id				
				End						
					COMMIT TRAN	
				 END TRY 
				 BEGIN CATCH
					BEGIN
						Set @v_Success_bit=0
						Select @v_Success_bit as flag, ''Error while inserting into table - '+@v_tablename_inVc + ''' + Error_Message() as Message, 0 as Id				
						--ROLLBACK TRAN
					END
				 END CATCH'
				
				/* END:  THIS CODE IS USING STAR(*) QUERY */
				PRINT(@v_InsQry_Vc)				
				EXEC(@v_InsQry_Vc) 
				/* END : alter AND EXECUTE INSERT INTO TABLE QUERY*/
				
				--Set @v_flag_bit=1
				--Select @v_flag_bit as flag, 'New record(s) inserted into table - '+@v_tablename_inVc+'.' as Message	
			--COMMIT TRAN	
		--END TRY 
		--BEGIN CATCH
		--		BEGIN
		--			Set @v_flag_bit=0
					 
		--			Select @v_flag_bit as flag, 'Error while inserting into table - '+@v_tablename_inVc+'.' + ERROR_MESSAGE() as Message 
		--			ROLLBACK TRAN
		--		END
		--END CATCH	 	
	END

	ELSE IF @v_action_inVc='update'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
				SET @v_NodePath_Vc = '/root/'+@v_tablename_inVc
				
				/* START CODE: TO GET COLUMN NAMES FROM TABLE */
				-- // GET THE PRIMARY KEY COLUMN
				Set @v_PkColumn_Vc = (select top 1 column_name from information_schema.columns where table_name = @v_tablename_inVc);
				
				select @v_Columns_Vc = Coalesce(@v_Columns_Vc, '') + column_name +' = b.'+column_name+', ' 
				from information_schema.columns where table_name = @v_tablename_inVc
				and column_name not in (select top 1 column_name from information_schema.columns where table_name = @v_tablename_inVc);
				Set @v_Columns_Vc=SUBSTRING(@v_Columns_Vc,0,LEN(@v_Columns_Vc))														
			
				/* END CODE: TO GET COLUMN NAMES FROM TABLE */
			
				/* START CODE: TO GET COLUMN NAMES TO alter STRUCTURE ACCORDING TO THE TABLE */
				select @v_TblStructure_Vc = 
				Coalesce(@v_TblStructure_Vc, '') + column_name + ' ' + DATA_TYPE + ISNULL('('+ CONVERT(varchar(50), CHARACTER_MAXIMUM_LENGTH) +')','')+ ', ' 
				from information_schema.columns where table_name = @v_tablename_inVc
				Set @v_TblStructure_Vc='(' + SUBSTRING(@v_TblStructure_Vc,0,LEN(@v_TblStructure_Vc)) + ')';				
				/* END CODE: TO GET COLUMN NAMES TO alter STRUCTURE ACCORDING TO THE TABLE */
				
				/* START : alter AND EXECUTE UPDATE TABLE QUERY*/
				SET @v_UpdQry_Vc='
				Declare @i INT;
				EXEC sp_xml_preparedocument @i OUTPUT, '''+@v_xml_text_inVc+''';
				UPDATE ' +@v_tablename_inVc + ' SET '+@v_Columns_Vc+' 
				FROM OPENXML(@i, '''+ @v_NodePath_Vc + ''') With '+@v_TblStructure_Vc+' b 
				Where '+@v_tablename_inVc+'.'+@v_PkColumn_Vc+'=b.' +@v_PkColumn_Vc+';
				EXEC sp_xml_removedocument @i;'
				PRINT (@v_UpdQry_Vc)
				EXEC(@v_UpdQry_Vc) 
				/* END : alter AND EXECUTE UPDATE TABLE QUERY*/
				
				Set @v_flag_bit=1
				Select @v_flag_bit as flag, 'Record(s) updated from table - '+@v_tablename_inVc+'.' as Message
			COMMIT TRAN	
		END TRY 
		BEGIN CATCH
				BEGIN
					Set @v_flag_bit=0
					Select @v_flag_bit as flag, 'Error while updating table - '+@v_tablename_inVc+'.'+ ERROR_MESSAGE() as Message
					ROLLBACK TRAN
				END
		END CATCH	 
	END

	ELSE IF @v_action_inVc='delete'
	BEGIN
		BEGIN TRY
			BEGIN TRAN			
					/* START : alter AND EXECUTE DELETE FROM TABLE QUERY*/					
					SET @v_DelQry_Vc='DELETE FROM ' +@v_tablename_inVc + ' WHERE ' + @v_conditions_inVc								
					--PRINT (@v_DelQry_Vc)
					EXEC(@v_DelQry_Vc) 
					/* END : alter AND EXECUTE DELETE FROM TABLE QUERY*/
					
					Set @v_flag_bit=1
					Select @v_flag_bit as flag, 'Record deleted from table - '+@v_tablename_inVc+'.' as Message
				
			COMMIT TRAN	
		END TRY 
		BEGIN CATCH
				BEGIN
					Set @v_flag_bit=0
					Select @v_flag_bit as flag, 'Error while deleting from table - '+@v_tablename_inVc+'.' as Message
					ROLLBACK TRAN
				END
		END CATCH	 
			
	END
	
	ELSE IF @v_action_inVc='insdel'
	BEGIN
		BEGIN TRY
			BEGIN TRAN		
				/* START : alter AND EXECUTE DELETE FROM TABLE QUERY*/
				SET @v_DelQry_Vc='DELETE FROM ' +@v_tablename_inVc + ' WHERE ' + @v_conditions_inVc
				PRINT (@v_DelQry_Vc)
				EXEC(@v_DelQry_Vc) 
				/* END : alter AND EXECUTE DELETE FROM TABLE QUERY*/
				
				SET @v_NodePath_Vc = '/root/'+@v_tablename_inVc
				
				/* START:  THIS CODE IS USING STAR(*) QUERY */
				SET @v_InsQry_Vc='
				Declare @i INT;
				EXEC sp_xml_preparedocument @i OUTPUT, '''+@v_xml_text_inVc+''';
				INSERT INTO ' +@v_tablename_inVc +'	
				SELECT * FROM 
					OPENXML(@i, '''+ @v_NodePath_Vc + ''') With '+@v_tablename_inVc+';
				EXEC sp_xml_removedocument @i'
				/* END:  THIS CODE IS USING STAR(*) QUERY */
				PRINT(@v_InsQry_Vc)				
				EXEC(@v_InsQry_Vc) 
				/* END : alter AND EXECUTE INSERT INTO TABLE QUERY*/				
				
				Set @v_flag_bit=1
				Select @v_flag_bit as flag,ERROR_MESSAGE()+ 'Record deleted and inserted from table - '+@v_tablename_inVc+'.' as Message
			COMMIT TRAN	
		END TRY 
		BEGIN CATCH
			BEGIN
				Set @v_flag_bit=0
				Select @v_flag_bit as flag, 'Error while deleting and inserting from table - '+@v_tablename_inVc+'.' as Message
				ROLLBACK TRAN
			END
		END CATCH	
	END
	
	ELSE IF @v_action_inVc='updatetable'
	BEGIN
		BEGIN TRY
			BEGIN TRAN	
				Select @v_UpdQry_Vc='UPDATE ' + @v_tablename_inVc + ' SET ' + @v_xml_text_inVc + ' WHERE ' + @v_conditions_inVc
				print(@v_UpdQry_Vc)
				EXEC (@v_UpdQry_Vc)
				Set @v_flag_bit=1
				Select @v_flag_bit as flag, 'Record(s) updated from table - '+@v_tablename_inVc+'.' as Message
			COMMIT TRAN	
		END TRY 
		BEGIN CATCH
			BEGIN
				Set @v_flag_bit=0
				 
				Select @v_flag_bit as flag, 'Error while updating table - '+@v_tablename_inVc+'.' + ERROR_MESSAGE() as Message
				ROLLBACK TRAN
			END
		END CATCH		
	END

SET NOCOUNT OFF;

END

