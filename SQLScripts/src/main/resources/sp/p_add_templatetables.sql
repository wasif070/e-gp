USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_add_templatetables]    Script Date: 4/24/2016 10:39:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Inserts into tables related to template data.
--
--
-- Author: Karan
-- Date: 25-01-2011
--

-- SP Name: [p_add_templatetables]
-- Module: STD
-- Function: Store Procedure is use for insert data into tbl_TemplateMaster, tbl_TemplateSections,
--			 tbl_TemplateSectionForm, tbl_templatetables, tbl_templatecolumns, tbl_templatecells,
--			 tbl_templateformulas, tbl_Ittheader, tbl_IttClause, tbl_IttSubClause, tbl_TdsSubClause.

--------------------------------------------------------------------------------
-- INSERT:	Use for perform ADD operation for template tables.
--------------------------------------------------------------------------------
/*

EXEC p_add_templatetables
@v_Action_inVc='INSERT',
@v_TemplateId_inInt=2


*/
ALTER PROCEDURE [dbo].[p_add_templatetables]
@v_Action_inVc varchar(50),
@v_TemplateId_inInt int

AS

BEGIN
SET NOCOUNT ON;

DECLARE @v_flag_bit bit, @v_NuTemplateId_Int int

IF @v_Action_inVc='INSERT'
BEGIN
	BEGIN TRY
		BEGIN TRAN

				/* START CODE: TO INSERT INTO TABLE - tbl_TemplateMaster */
				Insert into tbl_TemplateMaster
				Select templateName,noOfSections,status,procType,stdFor,devPartnerId
				from tbl_TemplateMaster TM
				Where TM.templateId=@v_TemplateId_inInt

				Set @v_NuTemplateId_Int=SCOPE_IDENTITY();
				/* END CODE: TO INSERT INTO TABLE - tbl_TemplateMaster */

				/* START CODE: TO INSERT INTO TABLE - tbl_TemplateSections */
				Insert into tbl_TemplateSections
				Select sectionName,templateId,contentType,isSubSection,status
				from tbl_TemplateSections
				Where templateId=@v_NuTemplateId_Int
				/* END CODE: TO INSERT INTO TABLE - tbl_TemplateSections */

				/* START CODE: TO INSERT INTO TABLE - tbl_TemplateSectionForm */
				Insert into tbl_TemplateSectionForm
				Select TM.templateId,TS.sectionId,filledBy,formName,formHeader,formFooter,noOfTables,
				isMultipleFilling,isEncryption,isPriceBid,TSF.status,isMandatory,NULL
				from tbl_TemplateSectionForm TSF 
				Inner Join tbl_TemplateSections TS On TSF.sectionId=TS.sectionId
				Inner Join tbl_TemplateMaster TM On TS.templateId=TM.templateId
				Where TM.templateId=@v_NuTemplateId_Int
				/* END CODE: TO INSERT INTO TABLE - tbl_TemplateSectionForm */

				/* START CODE: TO INSERT INTO TABLE - tbl_templatetables */
				Insert into tbl_templatetables
				Select TSF.formId,TM.templateId,TS.sectionId,tableName,tableHeader,tableFooter,noOfRows,noOfCols,TSF.isMultipleFilling
				from tbl_templatetables TT
				Inner join tbl_TemplateSectionForm TSF On TT.formId=TSF.formId
				Inner Join tbl_TemplateSections TS On TSF.sectionId=TS.sectionId
				Inner Join tbl_TemplateMaster TM On TS.templateId=TM.templateId
				Where TM.templateId=@v_NuTemplateId_Int
				/* END CODE: TO INSERT INTO TABLE - tbl_templatetables */

				/* START CODE: TO INSERT INTO TABLE - tbl_templatecolumns */
				Insert into tbl_templatecolumns
				Select columnId,columnHeader,dataType,TC.filledBy,columnType,sortOrder,TT.tableId,TSF.formId,TS.sectionId,showorhide
				from tbl_templatecolumns TC
				Inner Join tbl_templatetables TT On TC.tableId=TT.tableId
				Inner join tbl_TemplateSectionForm TSF On TT.formId=TSF.formId
				Inner Join tbl_TemplateSections TS On TSF.sectionId=TS.sectionId
				Inner Join tbl_TemplateMaster TM On TS.templateId=TM.templateId
				Where TM.templateId=@v_NuTemplateId_Int
				/* END CODE: TO INSERT INTO TABLE - tbl_templatecolumns */

				/* START CODE: TO INSERT INTO TABLE - tbl_templatecells */
				Insert into tbl_templatecells
				Select TT.tableId,TC.columnId,cellId,rowId,cellDatatype,cellvalue,colId,TmpC.showOrHide
				from tbl_templatecells TmpC
				Inner Join tbl_templatecolumns TC On TmpC.columnId=TC.columnId
				Inner Join tbl_templatetables TT On TC.tableId=TT.tableId
				Inner join tbl_TemplateSectionForm TSF On TT.formId=TSF.formId
				Inner Join tbl_TemplateSections TS On TSF.sectionId=TS.sectionId
				Inner Join tbl_TemplateMaster TM On TS.templateId=TM.templateId
				Where TM.templateId=@v_NuTemplateId_Int
				/* END CODE: TO INSERT INTO TABLE - tbl_templatecells */

				/* START CODE: TO INSERT INTO TABLE - tbl_templateformulas */
				Insert into tbl_templateformulas
				Select TSF.formId,TT.tableId,TC.columnId,formula,isGrandTotal
				from tbl_templateformulas TmpF
				Inner Join tbl_templatecolumns TC On TmpF.columnId=TC.columnId
				Inner Join tbl_templatetables TT On TC.tableId=TT.tableId
				Inner join tbl_TemplateSectionForm TSF On TT.formId=TSF.formId
				Inner Join tbl_TemplateSections TS On TSF.sectionId=TS.sectionId
				Inner Join tbl_TemplateMaster TM On TS.templateId=TM.templateId
				Where TM.templateId=@v_NuTemplateId_Int
				/* END CODE: TO INSERT INTO TABLE - tbl_templateformulas */

				/* START CODE: TO INSERT INTO TABLE - tbl_Ittheader */
				Insert into tbl_Ittheader
				Select ittHeaderName,TS.sectionId
				from tbl_Ittheader IH
				Inner Join tbl_TemplateSections TS On IH.sectionId=TS.sectionId
				Inner Join tbl_TemplateMaster TM On TS.templateId=TM.templateId
				Where TM.templateId=@v_NuTemplateId_Int
				/* END CODE: TO INSERT INTO TABLE - tbl_Ittheader */

				/* START CODE: TO INSERT INTO TABLE - tbl_IttClause */
				Insert into tbl_IttClause
				Select IH.ittHeaderId,ittClauseName
				from tbl_IttClause IC
				Inner Join tbl_Ittheader IH On IC.ittHeaderId=IH.ittHeaderId
				Inner Join tbl_TemplateSections TS On IH.sectionId=TS.sectionId
				Inner Join tbl_TemplateMaster TM On TS.templateId=TM.templateId
				Where TM.templateId=@v_NuTemplateId_Int
				/* END CODE: TO INSERT INTO TABLE - tbl_IttClause */

				/* START CODE: TO INSERT INTO TABLE - tbl_IttSubClause */
				Insert into tbl_IttSubClause
				Select IC.ittClauseId,ittSubClauseName,isTdsApplicable
				from tbl_IttSubClause ISC
				Inner Join tbl_IttClause IC On ISC.ittClauseId=IC.ittClauseId
				Inner Join tbl_Ittheader IH On IC.ittHeaderId=IH.ittHeaderId
				Inner Join tbl_TemplateSections TS On IH.sectionId=TS.sectionId
				Inner Join tbl_TemplateMaster TM On TS.templateId=TM.templateId
				Where TM.templateId=@v_NuTemplateId_Int
				/* END CODE: TO INSERT INTO TABLE - tbl_IttSubClause */

				/* START CODE: TO INSERT INTO TABLE - tbl_TdsSubClause */
			Insert into tbl_TdsSubClause
			Select ISC.ittSubClauseId,orderNumber,tdsSubClauseName,IH.ittHeaderId
			from tbl_TdsSubClause TdsSC
			Inner Join tbl_IttSubClause ISC On TdsSC.ittReference=ISC.ittSubClauseId
			Inner Join tbl_IttClause IC On ISC.ittClauseId=IC.ittClauseId
			Inner Join tbl_Ittheader IH On IC.ittHeaderId=IH.ittHeaderId
			Inner Join tbl_TemplateSections TS On IH.sectionId=TS.sectionId
			Inner Join tbl_TemplateMaster TM On TS.templateId=TM.templateId
			Where TM.templateId=@v_NuTemplateId_Int

				/* END CODE: TO INSERT INTO TABLE - tbl_TdsSubClause */



		COMMIT TRAN
	END TRY
	BEGIN CATCH
		BEGIN

			Set @v_flag_bit=0
			Select @v_flag_bit as flag, ERROR_MESSAGE() as Msg
			ROLLBACK TRAN
		END
	END CATCH
END

SET NOCOUNT OFF;
END

