USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[SPGetCurrencyDataForICT]    Script Date: 4/24/2016 11:23:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nazmul Islam Bhuiyan, G.M. Salahuddin>
-- Create date: <07-11-2012>
-- Description:	<Retrieves the bid amount in all currencies for a particular tender for an individual tenderer>
-- =============================================s
ALTER PROCEDURE [dbo].[SPGetCurrencyDataForICT]
@tenderID int, @roundID int

AS
Declare @formID int
Declare @tableID int
Declare @cellID int
Declare @rowID int
Declare @userID int
Declare @count int
Declare @flag int

Declare @valueInland float
Declare @valueInlandFinal float
Declare @valueVAT float
Declare @valueVATFinal float
Declare @value float
Declare @value1 float
Declare @value2 float
Declare @value3 float
Declare @value4 float
Declare @valueIndCol float

Declare @exchangeRate float

Declare @currencyID int
Declare @currencyID1 int
Declare @currencyID2 int
Declare @currencyID3 int
Declare @currencyID4 int

Declare @Qty int

Declare @currencyName varchar(20)
Declare @currencyName1 varchar(20)
Declare @currencyName2 varchar(20)
Declare @currencyName3 varchar(20)
Declare @currencyName4 varchar(20)

set @currencyID1=0
set @currencyID2=0
set @currencyID3=0
set @currencyID4=0

set @valueInlandFinal=0
set @valueVATFinal=0
set @valueIndCol=0
set @value=0
set @value1=0
set @value2=0
set @value3=0
set @value4=0
set @Qty=0

Declare @NoOfRows int
Declare @columnID varchar(20)

--select @userID=userId from tbl_BidderRank where tenderId=@tenderID and rank=1

select distinct @userID=userId from tbl_BidderRank where tenderId=@tenderID and
  rank=(select min(rank)from tbl_BidderRank where tenderId=@tenderID
  and roundId=@roundID)


  
DECLARE cursorForm CURSOR FOR  

select distinct tenderTableId, tf.tenderFormId from tbl_TenderForms tf,tbl_TenderBidForm tbf,tbl_TenderBidPlainData tbp where tf.tenderFormId=tbp.tenderFormId and 
 tf.isPriceBid='Yes' and tbf.tenderFormId=tbp.tenderFormId and tenderId=@tenderID and userId=@userID  


OPEN cursorForm   
FETCH NEXT FROM cursorForm INTO @tableID, @formID    

WHILE @@FETCH_STATUS = 0   
BEGIN   
       
		-- 2nd CURSOR STARTS       
       
		DECLARE cursorCell CURSOR FOR  				
		
		select tc.cellId, tc.rowId  from tbl_TenderCells tc 
		where  (cellDatatype=9) and 
		tc.tenderTableId= @tableID and rowId<>(select MAX(rowId)  from tbl_TenderCells tc 
		where  (cellDatatype=9) and 
		tc.tenderTableId= @tableID)

		OPEN cursorCell   
		FETCH NEXT FROM cursorCell INTO @cellID, @rowID    

		WHILE @@FETCH_STATUS = 0   
		BEGIN   																					
			set @Qty=0
			select @Qty=cellValue from tbl_TenderCells 
			where columnId=(SELECT distinct columnId FROM tbl_TenderColumns  where  tenderTableId=@tableID and columnType=2) and rowId=@rowID
			and tenderTableId=@tableID

			set @valueInland = 0
			select @valueInland=cellValue from tbl_TenderBidPlainData 
			where tenderColId=(SELECT distinct columnId FROM tbl_TenderColumns  where  tenderTableId=@tableID and columnType=11) and rowId=@rowID
			and bidTableId=(select bidTableId from tbl_TenderBidTable tbt,tbl_TenderBidForm tbf where tbt.bidId=tbf.bidId and tenderTableId=@tableID and userId=@userID)

			
			select @valueVAT=cellValue from tbl_TenderBidPlainData 
			where tenderColId=(SELECT distinct columnId FROM tbl_TenderColumns  where  tenderTableId=@tableID and columnType=10) and rowId=@rowID
			and bidTableId=(select bidTableId from tbl_TenderBidTable tbt,tbl_TenderBidForm tbf where tbt.bidId=tbf.bidId and tenderTableId=@tableID and userId=@userID)

			SELECT distinct @NoOfRows = count(columnId) FROM tbl_TenderColumns  where  tenderTableId=@tableID and columnType=6
			if @NoOfRows>1
				Begin
					declare @temp float
					set @temp = 0
					-- 3rd CURSOR STARTS              
					DECLARE cursorColumn CURSOR FOR  				
		
					SELECT distinct columnId FROM tbl_TenderColumns  where  tenderTableId=@tableID and columnType=6

					OPEN cursorColumn   
					FETCH NEXT FROM cursorColumn INTO @columnID

					WHILE @@FETCH_STATUS = 0   
					BEGIN
						
						select @valueIndCol=cellValue from tbl_TenderBidPlainData 
						where tenderColId=@columnID and rowId=@rowID
						and bidTableId=(select bidTableId from tbl_TenderBidTable tbt,tbl_TenderBidForm tbf where tbt.bidId=tbf.bidId and tenderTableId=@tableID and userId=@userID)					
					
						set @temp = @temp + @valueIndCol
						FETCH NEXT FROM cursorColumn INTO @columnID
					END   
					set @value = @temp
					CLOSE cursorColumn   
					DEALLOCATE cursorColumn
					
				End
			else
				Begin
					select @value=cellValue from tbl_TenderBidPlainData 
					where tenderColId=(SELECT distinct columnId FROM tbl_TenderColumns  where  tenderTableId=@tableID and columnType=6) and rowId=@rowID
					and bidTableId=(select bidTableId from tbl_TenderBidTable tbt,tbl_TenderBidForm tbf where tbt.bidId=tbf.bidId and tenderTableId=@tableID and userId=@userID)
				End
			
			select @exchangeRate=exchangeRate, @currencyID=tc.currencyId, @currencyName=cm.currencyShortName from tbl_TenderCurrency tc, tbl_CurrencyMaster cm where tenderId=@tenderID and 
			tc.currencyId=(select distinct cellValue from tbl_TenderBidPlainData where tenderCelId=@cellID and cm.currencyId=tc.currencyId and
			bidTableId=(select bidTableId from tbl_TenderBidTable tbt,tbl_TenderBidForm tbf 
			where tbt.bidId=tbf.bidId and tenderTableId=@tableID and userId=@userID))							
						
			set @count=1
			set @flag=1
			
			while @count>=1
			Begin
			--print @valueInland
			if @valueInland>=1
				Begin
					set @valueInlandFinal=@valueInlandFinal+@valueInland
				end
				
				if @valueVAT>=1
				Begin
					set @valueVATFinal=@valueVATFinal+@valueVAT
				end
			
			if @currencyID1=0 or @currencyID1 = @currencyID
				Begin
					if @currencyID1=0
						Begin
							set @currencyName1=@currencyName
							set @currencyID1 = @currencyID								
						End					
					--set @value1 = @value1 + @value/@exchangeRate	
					set @value1 = @value1 + @value*@Qty									
					set @flag=0								
				END						
			
			if (@currencyID2=0 or @currencyID2 = @currencyID) and @flag=1
				Begin
					if @currencyID2=0
						Begin
							set @currencyName2=@currencyName
							set @currencyID2 = @currencyID												
						End
					--set @value2 = @value2 + @value/@exchangeRate	
					set @value2 = @value2 + @value*@Qty						
					set @flag=0		
				END						
			
			if (@currencyID3=0 or @currencyID3 = @currencyID)  and @flag=1
				Begin
					If @currencyID3=0
						Begin
							set @currencyName3=@currencyName
							set @currencyID3 = @currencyID								
						End
					--set @value3 = @value3 + @value/@exchangeRate	
					set @value3 = @value3 + @value*@Qty						
					set @flag=0
				END			
						
			
			if (@currencyID4=0 or @currencyID4 = @currencyID)  and @flag=1
				Begin
					If @currencyID4=0
						Begin
							set @currencyName4=@currencyName
							set @currencyID4 = @currencyID													
						End
					--set @value4 = @value4 + @value/@exchangeRate	
					set @value4 = @value4 + @value*@Qty												
					set @flag=0
				END
			
						
			if @flag=0
				Break
			
			END									

			FETCH NEXT FROM cursorCell INTO @cellID, @rowID
		END   

		CLOSE cursorCell   
		DEALLOCATE cursorCell
       
       -- 2nd CURSOR ENDS                                

    FETCH NEXT FROM cursorForm INTO @tableID, @formID
END   

CLOSE cursorForm   
DEALLOCATE cursorForm

	if @currencyID1=2
		set @value1 = @value1 + @valueVATFinal + @valueInlandFinal
	else if @currencyID2=2
		set @value2 = @value2 + @valueVATFinal + @valueInlandFinal											
	else if @currencyID2=0
		Begin
			set @value2 = @value2 + @valueVATFinal + @valueInlandFinal
			select @currencyName2=currencyShortName from tbl_CurrencyMaster where currencyShortName='BDT' and currencyId=2
			set @currencyID2=2
		End
	else if @currencyID3=2
		set @value3 = @value3 + @valueVATFinal + @valueInlandFinal												
	else if @currencyID3=0
		Begin
			set @value3 = @value3 + @valueVATFinal + @valueInlandFinal
			select @currencyName3=currencyShortName from tbl_CurrencyMaster where currencyShortName='BDT' and currencyId=2
			set @currencyID3=2
		End	
	else
		Begin
			set @value4 = @value4 + @valueVATFinal + @valueInlandFinal
			select @currencyName4=currencyShortName from tbl_CurrencyMaster where currencyShortName='BDT' and currencyId=2
			set @currencyID4=2
		End	
		


select 
@value1 as value1, @currencyName1 as currencyName1,
@value2 as value2, @currencyName2 as currencyName2,
@value3 as value3, @currencyName3 as currencyName3,
@value4 as value4, @currencyName4 as currencyName4,
@valueVATFinal as VAT,
@valueInlandFinal as Inland,
@currencyID1 as currencyID1, 
@currencyID2 as currencyID2,
@currencyID3 as currencyID3,
@currencyID4 as currencyID4

