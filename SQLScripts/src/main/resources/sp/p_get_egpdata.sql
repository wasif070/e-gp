USE [eGPBhutanLive07052018]
GO
/****** Object:  StoredProcedure [dbo].[p_get_egpdata]    Script Date: 24-May-18 2:52:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TaherT
-- alter date: 15-Dec-2010
-- Description:	Proc created for Search Bid data
-- Last Modified
-- Modified BY: Karan
-- =============================================

-- SP Name: [p_get_egpdata]
-- Module: Bid Submission, Evaluation, NOA, Payment, Negotiation, Bank Module
-- Function: Store Procedure is use for fetch data base on given criteria for different Modules.
--------------------------------------------------------------------------------

-- getformDetails: 	Get Tender bid form details for final bid submission or bid submission receipt.
-- getTecAndPe: 	Get Email id of TEC or PEC user.
-- getRejectedTendererEmail:	Get Rejected tenderer's Email id.
-- getCpAndPe:		Get Email id of commitee chair person or PE.
-- getApprovedExtId:	Get approved Tender validity extension id.
-- isAllBOQDecrypted:	Check weather all BOQ forms are decrypted or not.
-- GetTenderEnvCount:	Get Tender's evaluation count.
-- isAvailTenSecAmt:	Check weather tender security amount exist or not.
-- getAllBidderForClariWithCP:
-- getAllBidderClariWithCP:
-- allBidderClariShow: 		Get all bidder clairfication in evaluation process.
-- getCRReportId:	Get Report Id for Report type is TER for given tender id.
-- getTORReportId:	Get Report Id for Report type is TOR for given tender id.
-- getPostCommentsData:	Get Comments Posed by given user id for Eval TSC.
-- getPostQualifyRank:	Get post qualification rank, bidder details for given tender id for report type is 'TER'.
-- GetTORReportForms:	Get TOR reprot forms.
-- TSCUploadReport: 	Check Committee member CP has approved TSC report by given user id.
-- getTecMember:	Get TEC/PEC commitee member's Email ids.
-- getTECReportsList:	Get TEC Report List.
-- getGrandSummaryLinkStatus:		Get Grand SUmmary Status.
-- GetLotPackageDetails:		Get Tender Pakcage Lot Details for given tender id.
-- GetLotPackageDetailsWithContractId:	Get Tender Package Lot details with Contract Id.
-- GetLotPackageDetailsForIssue:	Get Tender Package Lot Details for Issue NOA.
-- GetTECMemberEmail:			Get Email id of TEC/PEC Member.
-- getRegistrationFeePaymentDetail:	Get Tenderer Registration Fee Details for given regPayment id.
-- GetDetailsForNOA:			Get Tender details, submission details for NOA.
-- getLotsForBidderEvalStatusView:	Get Tender lot details for Evaluation.
-- getFormsForBidderEvalStatusView:	Get Tender forms for Evauation Process.
-- isPackageLotForForm:			Check Package Lot exist for given tender id.
-- isFinalizeDoneForLot:	Check bid sub status done as final submission for given package lot id.
-- getDocsforDocHash:		Get Documents Hash details for given tender id.
-- getNameOfTenderer:		Get Tenderer/Consultant detail from NOA ISSUE Details for given tender id.
-- CheckForTEC:
-- CheckIssuedNOA:		Check NOA issued for given tender id.
-- NoaDocInfo:			Get NOA Document Information on the basis of given tender id, pkglotid and user id.
-- getFinalSubComp:		Get Final submission Company details for given tender id for evaluation module.
-- isTERSignedLabel:	Get Lable for TER Sign if already signed then return 'View' else return 'View and
--				        sign' for given round id and user id for given reprot type.
-- isCRFormulaMadeTORTER:	Check if formula made for given report.
-- ContractListingTenderSide:	Get NOA Details for contract listing at tenderer side.
-- ContractListingTenderSideForRO:		Get NOA Details for contract listing at tenderer side for RO.
-- getPENameODER:		Get PENAME for given tender id.
-- getFinalResponseLinkStatus:	Get Final response Link Status for evaluation process.
-- isTERNotified:		Check if TER Nofified for given tender for given report type on the basis of given
--			bid id and package lot id.
-- getTenderAuthorisedUser:	Get AU user for Tender autorised User for given tender id.
-- getEvaluationStatus:		Get Evaluation status for given form id.
-- getEmailIdOfTenderer:	Get Email id of Tenerer/Consultant where final bid submission of given tender is
--			done.
-- getTendererDetails:		Get Tender details on the basis of given tender id and user id and noaissue id.
-- delUnselectedLotBid:		Delete Tender lot security details for unselected lot bid.
-- chkTSCMember:		Check TSC Member exist.
-- GetEvalRptApprovalNOA:	Get report status of evaluation reort on the basis of given tender id for Approval of NOA.
-- getMainPaymentBanksList:	Get Scedule Developer Partner details.
-- getTenderPaymentLisForAdmin:		Get Tender payment details List for Admin user.
-- getTenderPaymentDetailForAdmin:	Get Tender payment details for Admin user on the basis of given tender payment id.
-- chkLotSelectionSubmission:	Check final submission done for tender lot selection.
-- chkMapForAT:
-- chkNOAAcceptDt:
-- reportTORChk:		Check 'TOR2' is signed for given tender id, pcklotid.
-- getUNameTendererIdCmpId:	Get Company details for given tender on the basis of given user id.
-- getTenderPaymentDetail:	Get Tender payment details for given tender id.
-- chkRecordExist:		Check tender payment record exist for given search criteria.
-- getEvalOpenCommonMember:	Get members details for Oppening committee of Evaluation process.
-- getRegistrationFeePaymentDetailForVerify:	Get Registration Fee payment detials for verfication process.
-- GetEvalCPClarificationLinkStatus:	Get CP Clarification status details for Evaluation process.
-- GetEvalCPClarificationExist:		Get CP Clarification details if Clarification exist.
-- getMinistryDetails:			Get ministry details on the basis of given department id and office id.
-- getReTenderingLink:			Get Re tender details for given tender id.
-- getRoundsforEvaluation:		Get Eval round details for Evaluation process for given tender id.
-- FirstRoundAndNoaBiddersChk:
-- roundWiseInsert:			Perform insert operation for tbl_BidderRank , tbl_BidderRankDetails.
-- getLinkForEditViewEvalTenderer:	Get tender details for Edit/View link of Tender Evaluatio process.
-- getAwardedContractsDetails:		Get awarded contract details on the basis of tender id, pkglotid, userid.
-- getAwardedContractsDetailsTwo:	Get awarded contract details on the basis of tender id, pkglotid, userid.
-- getAwardedContractsDetailsThree:	Get awarded contract details on the basis of tender id, pkglotid, userid.
-- chkAwardedContractsDueTime:
-- chkAwardedContractsPerSec:
-- getRoundsforEvaluationForLotId:
-- BidderRoundsforEvaluationChk:
-- PostDisqualifyCRDelete:
-- PostDisqualifyUsers:
-- LowestAmtByRound:
-- getTscMemberCount:
-- PerfSecurityAvailChk:
-- PerfSecurityNaoChk:
-- getLinkForEditViewEvalTenderer:
-- getBidderStatusView:
-- getT1L1RoundWise:
-- getDataForTenderer:
-- getDataForPQIssue:
-- getDataForPQIssueOne:
-- getDataForPerSec:
-- chkMapTendererTender:
-- chkSiteVisitDateNTime:
-- getRegFeePayment_MISReport:
-- getPaymentReportTotal:
-- chkWorkFlowStatusForCancel:
-- reportTORTERChk:
-- checkNegStartPoint:
-- getOnlinePaymentBankForUserReport:
-- getSubContractor:
-- getSucTenderEvalRpt:
--------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_get_egpdata]
	-- Add the parameters for the stored procedure here
	 @v_fieldName1Vc varchar(500)=NULL,		  -- Action
	 @v_fieldName2Vc varchar(500)=NULL,
	 @v_fieldName3Vc varchar(500)=NULL,
	 @v_fieldName4Vc varchar(500)=NULL,
	 @v_fieldName5Vc varchar(500)=NULL,
	 @v_fieldName6Vc varchar(500)=NULL,
	 @v_fieldName7Vc varchar(500)=NULL,
	 @v_fieldName8Vc varchar(500)=NULL,
	 @v_fieldName9Vc varchar(500)=NULL,
	 @v_fieldName10Vc varchar(500)=NULL,
	 @v_fieldName11Vc varchar(500)=NULL,
	 @v_fieldName12Vc varchar(500)=NULL,
	 @v_fieldName13Vc varchar(500)=NULL,
	 @v_fieldName14Vc varchar(500)=NULL,
	 @v_fieldName15Vc varchar(500)=NULL,
	 @v_fieldName16Vc varchar(500)=NULL,
	 @v_fieldName17Vc varchar(500)=NULL,
	 @v_fieldName18Vc varchar(500)=NULL,
	 @v_fieldName19Vc varchar(500)=NULL,
	 @v_fieldName20Vc varchar(500)=NULL
AS

DECLARE @v_FinalQueryVc varchar(Max)=null, @v_Query1Vc varchar(1000)=null,
	@v_ConditionString_Vc varchar(4000),
	@v_CntQry_Vc varchar(max),
	@v_PgCntQry_Vc varchar(max),
	@v_StartAt_Int int, @v_StopAt_Int int,
	@v_Page_inInt int ,
	@v_RecordPerPage_inInt int,
	@FinalQuery as varchar(max),
	@evalCount int

BEGIN
   -- Insert statements for procedure here

   --Start added for discount by dohatec
IF @v_fieldName1Vc = 'getDiscountAmount'
BEGIN
	select top 1 tbpd.cellValue as FieldValue1 from tbl_TenderForms tf inner join
	tbl_TenderSection ts on tf.tenderSectionId=ts.tenderSectionId inner join
	tbl_TenderStd tstd on ts.tenderStdId=tstd.tenderStdId inner join
	tbl_TenderBidForm tbf on tbf.tenderFormId = tf.tenderFormId inner join
	tbl_TenderBidTable tbt on tbf.bidId=tbt.bidId inner join
	tbl_TenderBidPlainData tbpd on tbt.bidTableId=tbpd.bidTableId

	where tstd.tenderId = @v_fieldName2Vc and tf.pkgLotId=@v_fieldName3Vc and tbf.userId=@v_fieldName4Vc and tbpd.tenderColId=3 and formName = 'Discount Form' order by tbpd.rowId desc
END
   --End added for discount by dohatec

IF @v_fieldName1Vc = 'getformDetails'     -- For Finalsubmission / Submisn receipt
BEGIN
select distinct convert(varchar(25),a.tenderFormId )as FieldValue1 ,formName as FieldValue2,
case when b.tenderFormId IS null then 'No' else 'Yes' end FieldValue3,
case when isEncryption='Yes' then case when c.tenderFormId IS null then 'No' else 'Yes' end else '-' END FieldValue4,
case when d.tenderFormId IS null then 'No' else signtext end FieldValue5, case isMandatory when 'yes' then 'Yes' else 'No' end as FieldValue6

 from (select tenderid,packageLotId, t.tenderFormId,formName,isEncryption, isMandatory from tbl_TenderForms t,tbl_TenderSection s,tbl_TenderStd std
where t.tenderSectionId=s.tenderSectionId and s.tenderStdId=std.tenderStdId
and tenderId=@v_fieldName2Vc
and
case when @v_fieldName4Vc <> '0' then case when packageLotId = @v_fieldName4Vc then 1 else 0 end else 1 end =1

)a left outer join
(select distinct tenderFormId from tbl_TenderBidForm where tenderId =@v_fieldName2Vc and userId=@v_fieldName3Vc )b
on a.tenderFormId=b.tenderFormId
left outer join
(select distinct tenderFormId from tbl_FinalSubmission f,tbl_FinalSubDetail fd
where f.finalSubmissionId=fd.finalSubmissionId and  tenderId =@v_fieldName2Vc  and userId=@v_fieldName3Vc)c
on a.tenderFormId=c.tenderFormId
left outer join
(select distinct tenderFormId,signtext from tbl_TenderBidSign s,tbl_TenderBidForm b
 where tenderId =@v_fieldName2Vc and userId=@v_fieldName3Vc and b.bidId=s.bidId )d
on a.tenderFormId=d.tenderFormId

End


IF @v_fieldName1Vc = 'getTecAndPe'
BEGIN
	 select emailId as FieldValue1 from tbl_TenderMaster t,tbl_AppMaster a,tbl_EmployeeMaster e,tbl_LoginMaster l
	 where tenderId=@v_fieldName2Vc and t.appId=a.appId
	 and a.employeeId=e.employeeId
	 and l.userId=e.userId
	 union
	select l.emailId  as FieldValue1 from tbl_Committee c,tbl_CommitteeMembers cm,tbl_LoginMaster l
	where c.committeeId=cm.committeeId and tenderId=@v_fieldName2Vc and
	committeeType in('TEC','PEC')
	and l.userId=cm.userId
END

IF @v_fieldName1Vc = 'getRejectedTendererEmail'
BEGIN
	select emailId as FieldValue1 from tbl_EvalBidderStatus  e,tbl_LoginMaster l
	where e.userId=l.userId and (( bidderStatus ='Technically Unresponsive' and result='') or result='fail') and tenderId=@v_fieldName2Vc
END

IF @v_fieldName1Vc = 'getCpAndPe'
BEGIN
	select emailId as FieldValue1 from tbl_TenderMaster t,tbl_AppMaster a,tbl_EmployeeMaster e,tbl_LoginMaster l
	 where tenderId=@v_fieldName2Vc and t.appId=a.appId
	 and a.employeeId=e.employeeId
	 and l.userId=e.userId
	 union
	select emailId as FieldValue1 from tbl_Committee c,tbl_CommitteeMembers cm,tbl_LoginMaster l
	where c.committeeId=cm.committeeId and tenderId=@v_fieldName2Vc and cm.memberRole='cp' and
	committeeType in('TEC','PEC') and l.userId=cm.userId
END



IF @v_fieldName1Vc = 'getApprovedExtId'
BEGIN
	select top 1 convert(varchar(20),valExtDtId) as FieldValue1 from tbl_TenderValidityExtDate where tenderId=@v_fieldName2Vc and  extStatus='Approved'
order by valExtDtId desc

END

IF @v_fieldName1Vc = 'isAllBOQDecrypted'
BEGIN
	if @v_fieldName3Vc='0'
	Begin
		select @v_fieldName3Vc = appPkgLotId from tbl_TenderLotSecurity where tenderId=@v_fieldName2Vc
	End
	select case when
	 (select Convert(varchar(50), COUNT( distinct tbf.tenderFormId))
		from tbl_TenderBidForm tbf, tbl_FinalSubmission fs, tbl_TenderForms tf
		where tbf.tenderId=fs.tenderId and tbf.tenderId=@v_fieldName2Vc and tbf.tenderFormId=tf.tenderFormId
		and tf.pkgLotId=@v_fieldName3Vc
		and bidSubStatus='finalsubmission' And isPriceBid='Yes' and tf.formStatus != 'c')=
		(
		select Convert(varchar(50), COUNT( distinct tbf.tenderFormId))
		from tbl_tenderbidplaindata bp,tbl_TenderBidForm tbf, tbl_FinalSubmission fs, tbl_TenderForms tf
		where tbf.tenderId=fs.tenderId and bp.tenderFormId=tbf.tenderFormId
		and tf.pkgLotId=@v_fieldName3Vc
		 and tbf.tenderId=@v_fieldName2Vc and tbf.tenderFormId=tf.tenderFormId
		and bidSubStatus='finalsubmission' And isPriceBid='Yes' and tf.formStatus != 'c') then '1' else '0' end as FieldValue1
END
IF @v_fieldName1Vc = 'GetTenderEnvCount'
BEGIN
	select	convert(varchar(5),noOfEnvelops) as FieldValue1,
			convert(varchar(5),evalMethod) as FieldValue2
	from	tbl_ConfigEvalMethod cm ,
			tbl_TenderDetails td,
			tbl_TenderTypes tt
	where	cm.procurementMethodId=td.procurementMethodId
			and cm.tenderTypeId=tt.tenderTypeId
			and tt.tenderType=td.eventType
			and cm.procurementNatureId=td.procurementNatureId
			and tenderId=@v_fieldName2Vc
End

IF @v_fieldName1Vc = 'isAvailTenSecAmt'
BEGIN
	/*	(Old Version)
	select case when (select sum(tenderSecurityAmt) from tbl_TenderLotSecurity where tenderId=@v_fieldName2Vc) > 0
	THEN '1'
	ELSE '0'
	END as FieldValue1 */

	/* Dohatec Start */

	Declare @flag VARCHAR(8)
	SET @flag = ''

	IF (SELECT sum(tenderSecurityAmt) FROM tbl_TenderLotSecurity WHERE tenderId=@v_fieldName2Vc) > 0 AND (SELECT sum(tenderSecurityAmtUSD) FROM tbl_TenderLotSecurity WHERE tenderId=@v_fieldName2Vc) > 0
		SET @flag = '11'
	ELSE IF (SELECT sum(tenderSecurityAmt) FROM tbl_TenderLotSecurity WHERE tenderId=@v_fieldName2Vc) > 0 AND ((SELECT sum(tenderSecurityAmtUSD) FROM tbl_TenderLotSecurity WHERE tenderId=@v_fieldName2Vc) < 1 or (SELECT sum(tenderSecurityAmtUSD) FROM tbl_TenderLotSecurity WHERE tenderId=@v_fieldName2Vc) is null)
		SET @flag = '10'
	ELSE IF (SELECT sum(tenderSecurityAmt) FROM tbl_TenderLotSecurity WHERE tenderId=@v_fieldName2Vc) < 1 AND (SELECT sum(tenderSecurityAmtUSD) FROM tbl_TenderLotSecurity WHERE tenderId=@v_fieldName2Vc) > 0
		SET @flag = '01'
	ELSE
		SET @flag = '00'

	SELECT @flag AS FieldValue1

	/* Security Amount (USD and BDT) must be checked for ICT whether it is 0 or not */
	/* Dohatec End */
End
IF @v_fieldName1Vc = 'getAllBidderForClariWithCP'
BEGIN

select distinct convert(varchar(50),userId) as FieldValue1,dbo.f_getbiddercompany(userId) as FieldValue2
	from tbl_EvalFormQues fq
	where tenderId=@v_fieldName2Vc and evalCount = @v_fieldName4Vc and
	--fq.pkgLotId=@v_fieldName3Vc and
	queSentByTec=1

End
IF @v_fieldName1Vc = 'getAllBidderClariWithCP'
BEGIN
		select convert(varchar(50),tf.tenderFormId) as FieldValue1, tf.formName as FieldValue2, question as FieldValue3 ,
	case when exists (select evalBidderRspId from tbl_EvalBidderResp where tenderId=@v_fieldName2Vc and userId = @v_fieldName4Vc and isClarificationComp='yes')
	then answer else 'Response Pending' end as FieldValue4
	from tbl_EvalFormQues fq inner join tbl_TenderForms tf on fq.tenderFormId = tf.tenderFormId
	where tenderId=@v_fieldName2Vc and fq.pkgLotId=@v_fieldName3Vc and queSentByTec=1 and evalCount = @v_fieldName5Vc
	and fq.userId = @v_fieldName4Vc
	and (tf.formStatus is Null Or tf.formStatus in ('p', 'cp'))
End
IF @v_fieldName1Vc = 'allBidderClariShow'
Begin
 if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
	SET @evalCount = 0
else
	select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

 select case when COUNT(evalQueId)>0 then '1' else '0' end as FieldValue1 from tbl_EvalFormQues where tenderId=@v_fieldName2Vc and queSentByTec=1 and evalCount = @evalCount
end

IF @v_fieldName1Vc = 'getCRReportId'
Begin
	 if @v_fieldName3Vc='0'
	 begin
		select CONVERT(varchar(50),rm.reportId) as FieldValue1,
		case when((select COUNT(bidderRankId) from tbl_BidderRank br where
		br.tenderId=@v_fieldName2Vc and br.reportId=rm.reportId and roundId=@v_fieldName4Vc)=0)
		then '0' else '1' end as FieldValue2
		 from tbl_ReportMaster rm where rm.tenderId=@v_fieldName2Vc and rm.isTORTER='TER'
	 end
	 else
	 begin
	 select CONVERT(varchar(50),rm.reportId) as FieldValue1,
	 case when((select COUNT(bidderRankId) from tbl_BidderRank br
	 where br.tenderId=@v_fieldName2Vc and br.pkgLotId=@v_fieldName3Vc and br.reportId=rm.reportId  and roundId=@v_fieldName4Vc)=0)
		then '0' else '1' end as FieldValue2
	 from tbl_ReportMaster rm,tbl_ReportLots rl
		where rm.tenderId=@v_fieldName2Vc  and rm.reportId = rl.reportId and rl.pkgLotId=@v_fieldName3Vc
		and rm.isTORTER='TER'
	 end
end

IF @v_fieldName1Vc = 'getTORReportId'
Begin
	 if @v_fieldName3Vc='0'
	 begin
		select CONVERT(varchar(50),rm.reportId) as FieldValue1
		 from tbl_ReportMaster rm where rm.tenderId=@v_fieldName2Vc and rm.isTORTER='TOR'
	 end
	 else
	 begin
	 select CONVERT(varchar(50),rm.reportId) as FieldValue1
	 from tbl_ReportMaster rm,tbl_ReportLots rl
		where rm.tenderId=@v_fieldName2Vc  and rm.reportId = rl.reportId and rl.pkgLotId=@v_fieldName3Vc
		and rm.isTORTER='TOR'
	 end
end

IF @v_fieldName1Vc = 'getPostCommentsData'
Begin
	select  CONVERT(varchar(50),etc.commentsId) as FieldValue1, etc.comments as FieldValue2, em.employeeName as FieldValue3, CONVERT(varchar(50),etc.tscUserId) as FieldValue4
	from Tbl_EvalTsccomments as etc,Tbl_EmployeeMaster em
	where etc.tenderId=@v_fieldName2Vc
	and etc.formId=@v_fieldName3Vc and etc.tscUserId = em.userId
	and etc.userId=@v_fieldName4Vc
	union
	select  CONVERT(varchar(50),etc.commentsId) as FieldValue1,etc.comments as FieldValue2,em.fullName as FieldValue3,CONVERT(varchar(50),etc.tscUserId) as FieldValue4
	from Tbl_EvalTsccomments as etc,tbl_ExternalMemInfo em
	where etc.tenderId=@v_fieldName2Vc
	and etc.formId=@v_fieldName3Vc and etc.tscUserId = em.userId
	and etc.userId=@v_fieldName4Vc
end
IF @v_fieldName1Vc = 'getPostQualifyRank'
begin
	select br.rank as FieldValue1,dbo.f_getbiddercompany(tp.userId) as FieldValue2,
	ISNULL(REPLACE(CONVERT(VARCHAR(11), tp.entryDate, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), tp.entryDate,108), 1, 5), 'N.A.')  as FieldValue3,
	CONVERT(varchar(50),tp.postQaulId) as FieldValue4,CONVERT(varchar(50), tp.userId)as FieldValue5,tp.postQualStatus as FieldValue6
		from Tbl_PostQualification tp inner join tbl_BidderRank br on tp.pkgLotId = br.pkgLotId and tp.tenderId = br.tenderId and tp.userId = br.userId
		inner join tbl_ReportMaster rm on br.reportId = rm.reportId
		where tp.pkgLotId=@v_fieldName3Vc and tp.tenderId=@v_fieldName2Vc and rm.isTORTER='TER'
end

IF @v_fieldName1Vc='GetTORReportForms'
BEGIN
	IF @v_fieldName4Vc='yes'
	BEGIN
		SELECT	tf.formName as FieldValue1,
				CONVERT(varchar(20),tenderformid)  as FieldValue2,
				ISNULL(tf.formStatus,'') as FieldValue3
		FROM	tbl_TenderForms tf,
				tbl_TenderSection ts,
				tbl_TenderStd std
		WHERE	tf.tenderSectionId=ts.tenderSectionId
				and ts.tenderStdId=std.tenderStdId
				and tenderId=@v_fieldName2Vc
				and pkgLotId=@v_fieldName3Vc
				and isPriceBid='yes'
	END
	ELSE IF @v_fieldName4Vc='no'
	BEGIN
		IF @v_fieldName3Vc!=0
		BEGIN
			SELECT	tf.formName as FieldValue1,
					CONVERT(varchar(20),tenderformid)  as FieldValue2,
					ISNULL(tf.formStatus,'') as FieldValue3
			FROM	tbl_TenderForms tf,
					tbl_TenderSection ts,
					tbl_TenderStd std
			WHERE	tf.tenderSectionId=ts.tenderSectionId
					and ts.tenderStdId=std.tenderStdId
					and tenderId=@v_fieldName2Vc
					and isPriceBid='no'
					and pkgLotId=@v_fieldName3Vc
		END
		ELSE
		BEGIN
			SELECT	tf.formName as FieldValue1,
					CONVERT(varchar(20),tenderformid)  as FieldValue2,
					ISNULL(tf.formStatus,'') as FieldValue3
			FROM	tbl_TenderForms tf,
					tbl_TenderSection ts,
					tbl_TenderStd std
			WHERE	tf.tenderSectionId=ts.tenderSectionId
					and ts.tenderStdId=std.tenderStdId
					and tenderId=@v_fieldName2Vc
					and isPriceBid='no'
		END
	END
	ELSE
	BEGIN
		IF @v_fieldName3Vc!=0
		BEGIN
			SELECT	tf.formName as FieldValue1,
					CONVERT(varchar(20),tenderformid)  as FieldValue2,
					ISNULL(tf.formStatus,'') as FieldValue3
			FROM	tbl_TenderForms tf,
					tbl_TenderSection ts,
					tbl_TenderStd std
			WHERE	tf.tenderSectionId=ts.tenderSectionId
					and ts.tenderStdId=std.tenderStdId
					and tenderId=@v_fieldName2Vc
					and pkgLotId=@v_fieldName3Vc
		END
		ELSE
		BEGIN
			SELECT	tf.formName as FieldValue1,
					CONVERT(varchar(20),tenderformid)  as FieldValue2,
					ISNULL(tf.formStatus,'')as FieldValue3
			FROM	tbl_TenderForms tf,
					tbl_TenderSection ts,
					tbl_TenderStd std
			WHERE	tf.tenderSectionId=ts.tenderSectionId
					and ts.tenderStdId=std.tenderStdId
					and tenderId=@v_fieldName2Vc
		END
	END
END

IF @v_fieldName1Vc = 'TSCUploadReport'
BEGIN
	SELECT Case When (select cm.userId from tbl_CommitteeMembers cm
	inner join tbl_Committee c On cm.committeeId=c.committeeId
	where c.tenderId=@v_fieldName2Vc and c.committeeType IN ('TSC')
	and c.committeStatus='approved' and memberRole='cp')=@v_fieldName3Vc
	THEN 'yes'
	ELSE 'no'
	END as FieldValue1
END
IF @v_fieldName1Vc = 'getTecMember'
	BEGIN
	select l.emailId  as FieldValue1 from tbl_Committee c,tbl_CommitteeMembers cm,tbl_LoginMaster l
	where c.committeeId=cm.committeeId and tenderId=@v_fieldName2Vc and
	committeeType in('TEC','PEC')
	and l.userId=cm.userId
	END



IF @v_fieldName1Vc='getTECReportsList'
BEGIN
	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName3Vc, @v_RecordPerPage_inInt=@v_fieldName4Vc

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)	;
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */

	/*Start: Dynamic Condition string*/
	select @v_ConditionString_Vc=''

	If @v_fieldName5Vc is not null And @v_fieldName5Vc<>'' --  Tender Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And TD.tenderId='+@v_fieldName5Vc
	End

	If @v_fieldName6Vc is not null And @v_fieldName6Vc<>'' --  Tender Ref. No.
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And TD.reoiRfpRefNo=''' + @v_fieldName6Vc + ''''
	End
	SELECT @FinalQuery='
	 With CTE1 as (
	 Select *
	  from (
		select  Convert(varchar(50), TD.tenderId) as FieldValue1,
		tenderBrief as FieldValue2,
		CASE WHEN TD.agency = '''' AND TD.division = '''' THEN TD.ministry WHEN TD.agency = '''' THEN TD.division ELSE TD.agency END as FieldValue3,
		peOfficeName as FieldValue4,
		TD.reoiRfpRefNo as FieldValue5
		,ROW_NUMBER() Over (Order by max(sentDate) desc) as RowNumber,
		--, Row_Number() Over(evalRptToAAId) as RowNumber
		CASE WHEN ER.rptstatus = ''Approved'' OR ER.rptstatus = ''Rejected / Re-Tendering'' OR ER.rptstatus = ''Rejected'' then ''Processed'' ELSE ''Process'' END as FieldValue9
		from tbl_tenderdetails TD
		inner join tbl_EvalRptSentToAA ER ON TD.tenderId=ER.tenderId
		where ER.userId=' + @v_fieldName2Vc +
		+@v_ConditionString_Vc+
		' GROUP BY td.tenderId, td.tenderBrief, CASE WHEN TD.agency = '''' AND TD.division = '''' THEN TD.ministry WHEN TD.agency = '''' THEN TD.division ELSE TD.agency END, td.peOfficeName, reoiRfpRefNo, ER.rptstatus
	) Tmp
	)

	select *,
		CONVERT(varchar(50), RowNumber) as FieldValue6,
		(select CONVERT(varchar(50), COUNT(*))  from CTE1) as FieldValue7,
		CONVERT(varchar(50),Ceiling(Cast((select COUNT(*)  from CTE1) as float)/' + CONVERT(varchar(50),@v_RecordPerPage_inInt) + ')) as FieldValue8
	from CTE1
	WHERE RowNumber Between  ' + CONVERT(varchar(50), @v_StartAt_Int) + '  And  ' + CONVERT(varchar(50), @v_StopAt_Int) +
	' Order by RowNumber'
	PRINT (@FinalQuery)
	EXEC (@FinalQuery)
END

IF @v_fieldName1Vc='getGrandSummaryLinkStatus'
BEGIN
select Case When
	(
		select COUNT( distinct tbf.tenderFormId)
			from tbl_TenderBidForm tbf, tbl_FinalSubmission fs, tbl_TenderForms tf
			where tbf.tenderId=fs.tenderId
			and tbf.tenderId=@v_fieldName2Vc
			and tf.pkgLotId=@v_fieldName3Vc
			and tbf.tenderFormId=tf.tenderFormId
			and bidSubStatus='finalsubmission' And isPriceBid='Yes'
	)

	=

	(select  Count(distinct tenderFormId) from tbl_TenderBidPlainData
	 Where tenderFormId in
		( Select distinct tenderFormId from tbl_TenderForms
		    Where tenderSectionId in
			(Select distinct tenderSectionId from tbl_TenderSection
				Where tenderStdId in
					(Select distinct tenderStdId from tbl_TenderStd Where tenderId=@v_fieldName2Vc)
			)
			and pkgLotId=@v_fieldName3Vc
			And isPriceBid='Yes'
		)
	)
			Then '1'
			Else '0'
			End as FieldValue1




END

IF @v_fieldName1Vc = 'GetLotPackageDetails'
BEGIN
	IF (select procurementNatureId from tbl_TenderDetails where tenderId=@v_fieldName2Vc) = 3
	BEGIN
		select distinct ttd.packageNo as FieldValue1,ttd.packageDescription as FieldValue2, ttls.lotNo as FieldValue3,ttls.lotDesc as FieldValue4,CONVERT(VARCHAR(50), ttls.appPkgLotId) as FieldValue5
		from tbl_TenderDetails ttd,tbl_TenderLotSecurity ttls
		where ttd.tenderId = ttls.tenderId and ttd.tenderId = @v_fieldName2Vc
	END
	ELSE
	BEGIN
		select distinct ttd.packageNo as FieldValue1,ttd.packageDescription as FieldValue2, ttls.lotNo as FieldValue3,ttls.lotDesc as FieldValue4,CONVERT(VARCHAR(50), ttls.appPkgLotId) as FieldValue5
		from tbl_EvalRptSentToAA tersta , tbl_TenderLotSecurity ttls ,tbl_TenderDetails ttd
		where ttls.tenderId = tersta.tenderId and ttls.appPkgLotId = tersta.pkgLotId and
		ttd.tenderId = ttls.tenderId and ttls.tenderId = @v_fieldName2Vc
	END

END


IF @v_fieldName1Vc = 'GetNOAConfig'
BEGIN

	SELECT CONVERT(varchar(50), configNoaId) as FieldValue1, CONVERT(varchar(50), loiNoa) as FieldValue2
	FROM tbl_ConfigNoa
	WHERE procurementMethodId = @v_fieldName3Vc and procurementNatureId = @v_fieldName4Vc and procurementTypeId = @v_fieldName5Vc
END






IF @v_fieldName1Vc = 'GetLotPackageDetailsWithContractId'
BEGIN
	IF (select procurementNatureId from tbl_TenderDetails where tenderId=@v_fieldName2Vc) = 3
	BEGIN
		select packageNo as FieldValue1,packageDescription as FieldValue2 from tbl_TenderDetails where tenderId = @v_fieldName2Vc
	END
	ELSE
	BEGIN
		select distinct ttd.packageNo as FieldValue1,CONVERT(VARCHAR(50), ts.contractSignId) as FieldValue6, ttd.packageDescription as FieldValue2, ttls.lotNo as FieldValue3,ttls.lotDesc as FieldValue4,CONVERT(VARCHAR(50), ttls.appPkgLotId) as FieldValue5
		from tbl_EvalRptSentToAA tersta , tbl_TenderLotSecurity ttls,
		tbl_TenderDetails ttd,tbl_ContractSign ts,tbl_NoaIssueDetails ns
		where ttls.tenderId = tersta.tenderId and ttls.appPkgLotId = tersta.pkgLotId and
		ts.noaId = ns.noaIssueId and ttls.appPkgLotId = ns.pkgLotId and
		---added for RO
		ns.isRepeatOrder='no' and
		-------------------------
		ttd.tenderId = ttls.tenderId and ttls.tenderId = @v_fieldName2Vc
	END

END
IF @v_fieldName1Vc = 'GetLotPackageDetailsForIssue'
BEGIN
	IF @v_fieldName2Vc = 'contractName'
	BEGIN
		select lotDesc as FieldValue1 from tbl_TenderLotSecurity where tenderId=@v_fieldName3Vc and appPkgLotId = @v_fieldName4Vc
	END
	ELSE IF @v_fieldName2Vc = 'perSecAmt'
	BEGIN
		--select convert(varchar(50),perSecurityAmt) as FieldValue1 from tbl_TenderPerfSecurity where tenderId = @v_fieldName3Vc and pkgLotId = @v_fieldName4Vc and roundId = @v_fieldName5Vc
		/* Dohatec Start */
		select convert(varchar(50),perSecurityAmt) as FieldValue1, convert(varchar(50),lowestAmt) as FieldValue2, convert(varchar(50),currencyId) as FieldValue3 from tbl_TenderPerfSecurity where tenderId = @v_fieldName3Vc and pkgLotId = @v_fieldName4Vc and roundId = @v_fieldName5Vc
		/* Dohatec End */
	END
	ELSE IF @v_fieldName2Vc = 'getLotId'
	BEGIN
		select convert(varchar(50),pkgLotId) as FieldValue1,convert(varchar(50),userId) as FieldValue2,perfSecAmtWords as FieldValue3 from tbl_NoaIssueDetails where noaIssueId = @v_fieldName3Vc
	END
	ELSE IF @v_fieldName2Vc = 'getPEOfficeName'
	BEGIN
		select peOfficeName as FieldValue1 from tbl_TenderDetails where tenderId = @v_fieldName3Vc
	END
	ELSE IF @v_fieldName2Vc = 'getPEName'
	BEGIN
	Declare @transfers as int
	/* Dohatec Start */
		DECLARE @NoaPEUserID as int 
		SET @NoaPEUserID = (select DISTINCT createdBy from tbl_NoaIssueDetails where tenderId = @v_fieldName3Vc and noaIssueId=@v_fieldName4Vc)
		select @transfers=count(*) from tbl_EmployeeTrasfer where userId =@NoaPEUserID and transferDt > (select tenderPubDt from tbl_TenderDetails where tenderId = @v_fieldName3Vc) 
		IF(@transfers > 0)
		BEGIN
			select employeeName as FieldValue1 from tbl_EmployeeTrasfer 
			WHERE 	userId  = @NoaPEUserID AND transferDt = (	SELECT 	MAX(transferDt) FROM
			(select employeeName,transferDt from tbl_EmployeeTrasfer 
			where userId  = @NoaPEUserID
			and transferDt < (select MAX(createdDt) from tbl_NoaIssueDetails where tenderId = @v_fieldName3Vc and noaIssueId=@v_fieldName4Vc))T)
			ORDER BY govUserId DESC
		END
	/* Dohatec End */
		ELSE
		BEGIN
			select peName as FieldValue1 from tbl_TenderDetails where tenderId = @v_fieldName3Vc
		END	
	END
	/* Dohatec Start */
	ELSE IF @v_fieldName2Vc = 'getCurrencyName'
	BEGIN
		select currencyShortName as FieldValue1 from tbl_TenderPerfSecurity, tbl_CurrencyMaster cm where tenderId = @v_fieldName3Vc and cm.currencyId=convert(int,@v_fieldName4Vc)
	END
	ELSE IF @v_fieldName2Vc = 'getAdvPayment'
	BEGIN
		select convert(varchar(50),ISNULL(advanceContractAmt,0)) as FieldValue1 from tbl_NoaIssueDetails where tenderId = @v_fieldName3Vc and roundId = @v_fieldName4Vc
	END
	/* Dohatec End */
END

IF @v_fieldName1Vc = 'GetTECMemberEmail'
BEGIN
print '11'
 select emailId as FieldValue1,CONVERT(varchar(20),l.userId) as FieldValue2  from tbl_LoginMaster l,tbl_CommitteeMembers c,tbl_Committee cm
 where c.userId=l.userId and c.committeeId=cm.committeeId
 and cm.tenderId=@v_fieldName2Vc  and committeeType in('PEC','TEC')
ENd
END

IF @v_fieldName1Vc = 'getRegistrationFeePaymentDetail'
BEGIN

	Select	branchName as FieldValue1,
			ISNULL(dbo.f_GovUserName(RP.partTransId, 'tbl_PartnerAdminTransfer'),'-') as FieldValue2,
			currency as FieldValue3,
			Convert(varchar(50), RP.amount) as FieldValue4,
			paymentInstType as FieldValue5,
			REPLACE(CONVERT(VARCHAR(11),dtOfPayment, 106), ' ', '-') + ' ' + Substring(CONVERT(VARCHAR(5),dtOfPayment,108),1,8)  as FieldValue6,
			comments as FieldValue7,
			isVerified as FieldValue8,
			Convert(varchar(50), validityPeriodRef) as FieldValue9,
			RP.status as FieldValue10,
			(select top 1 dbo.f_GovUserName(verifyPartTransId, 'tbl_PartnerAdminTransfer')
			from	tbl_PaymentVerification where paymentId = RP.regPaymentId) as FieldValue11,
			RP.bankName as  FieldValue12,
			isnull((select TOP 1 REPLACE(CONVERT(VARCHAR(11),verificationDt, 106), ' ', '-') + ' ' + Substring(CONVERT(VARCHAR(5),verificationDt,108),1,8)
					from tbl_PaymentVerification where paymentId=RP.regPaymentId order by verificationDt desc),'')as FieldValue13,
			RP.instRefNumber as FieldValue14,
			RP.onlineTransId as  FieldValue15,
			CASE WHEN RP.paymentInstType='Online' THEN CONVERT(VARCHAR(25),ISNULL(OPD.serviceChargeAmt,'0.00')) ELSE '-' END AS FieldValue16,
			CASE WHEN RP.paymentInstType='Online' THEN CONVERT(VARCHAR(25),ISNULL(OPD.TotalAmount,'0.00')) ELSE '-' END AS FieldValue17
	From	tbl_RegFeePayment RP
			LEFT Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
			LEFT JOIN tbl_OnlinePaymentTransDetails OPD ON OPD.TransId = RP.onlineTransId
						AND OPD.userId = RP.userId
	Where regPaymentId=@v_fieldName2Vc And isLive='yes'
END

If @v_fieldName1Vc='GetDetailsForNOA'
BEGIN
	select procurementnature as FieldValue1,REPLACE(CONVERT(VARCHAR(11),finalSubmissionDt, 106), ' ', '-')  as FieldValue2 from tbl_tenderDetails inner join tbl_finalsubmission on tbl_tenderDetails.tenderId=tbl_finalsubmission.tenderId
	where tbl_finalsubmission.tenderId=@v_fieldName2Vc and tbl_finalsubmission.userid=@v_fieldName3Vc and tbl_finalsubmission.bidSubStatus='finalsubmission'
END


	--select @v_fieldName1Vc ='getLotsForBidderEvalStatusView',		  -- Action
	-- @v_fieldName2Vc =1997,
	-- @v_fieldName3Vc =1363,
	-- @v_fieldName4Vc =931 ,
	-- @v_fieldName5Vc = 'evaluation'



	 IF @v_fieldName1Vc ='getLotsForBidderEvalStatusView'
	 Begin
		select distinct
		Convert(varchar(50),ts.appPkgLotId) as FieldValue1,
		ts.lotNo as FieldValue2,
		ts.lotDesc as FieldValue3
		from tbl_EvalMemStatus ms
		inner join tbl_TenderForms tf on tf.tenderFormId = ms.formId
		inner join tbl_TenderLotSecurity ts On tf.pkgLotId=ts.appPkgLotId
		where ms.tenderId=@v_fieldName2Vc
			and userId=@v_fieldName3Vc
			and evalBy=@v_fieldName4Vc
			and evalStatus=''+@v_fieldName5Vc+''


		--select ms.evalMemStatusId, ms.tenderId, ms.userId, ms.evalBy,ms.formId,
		--ms.isComplied,tf.formName
		--from tbl_EvalMemStatus ms, tbl_TenderForms tf
		--where tenderId=  1997 and userId=1363 and evalBy=931 and evalStatus='evaluation' and tf.tenderFormId = ms.formId
		--and tf.tenderFormId in
		--(
		--	select distinct tenderFormId from tbl_TenderForms where tenderSectionId in
		--		(select tenderSectionId from tbl_TenderSection where tenderStdId in
		--			(select tenderStdId from tbl_TenderStd where tenderId=1997)
		--		)
		--)
	 END

	 IF @v_fieldName1Vc ='getFormsForBidderEvalStatusView'
	 BEGIN
		If @v_fieldName6Vc<>'0' -- Lot Id Case
		Begin
			select tf.formName as FieldValue1,
			ms.isComplied as FieldValue2,
                        -- For TEC member comments view Modified by Dohatec
                        ms.evalNonCompRemarks as FieldValue3,   
                        Convert(varchar(50), tf.tenderFormId) as FieldValue4
                        -- For TEC member comments view Dohatec End
			from tbl_EvalMemStatus ms
			inner join tbl_TenderForms tf on tf.tenderFormId = ms.formId
			inner join tbl_TenderLotSecurity ts On tf.pkgLotId=ts.appPkgLotId
			where ms.tenderId=@v_fieldName2Vc
				and userId=@v_fieldName3Vc
				and evalBy=@v_fieldName4Vc
				and evalStatus=''+@v_fieldName5Vc+''
				and tf.pkgLotId=@v_fieldName6Vc
		End
		Else
		Begin
			select tf.formName as FieldValue1,
			ms.isComplied as FieldValue2,
                       -- For TEC member comments view Modified by Dohatec
                        ms.evalNonCompRemarks as FieldValue3,
                        Convert(varchar(50), tf.tenderFormId) as FieldValue4
                        -- For TEC member comments view Dohatec End
			from tbl_EvalMemStatus ms
			inner join tbl_TenderForms tf on tf.tenderFormId = ms.formId
			where ms.tenderId=@v_fieldName2Vc
				and userId=@v_fieldName3Vc
				and evalBy=@v_fieldName4Vc
				and evalStatus=''+@v_fieldName5Vc+''
			and tf.tenderFormId in
			(
				select distinct tenderFormId from tbl_TenderForms where tenderSectionId in
					(select tenderSectionId from tbl_TenderSection where tenderStdId in
						(select tenderStdId from tbl_TenderStd where tenderId=@v_fieldName2Vc)
					)
			)
		End

	 END
IF @v_fieldName1Vc ='isPackageLotForForm'
BEGIN
	select convert(varchar(50),td.tenderId) as FieldValue1 from
	tbl_TenderDetails td,tbl_TenderLotSecurity tls
	where td.tenderId = @v_fieldName2Vc and td.tenderId = tls.tenderId and
	procurementNatureId !=3 and docAvlMethod ='Package'
	group by td.tenderId
	having  COUNT(tls.appPkgLotId)>1
END
IF @v_fieldName1Vc='isFinalizeDoneForLot'
BEGIN
		Declare @field1 int
	Declare @field2 int
	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
			SET @evalCount = 0
		else
			select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc


	if ((select docAvlMethod from tbl_TenderDetails where tenderId=@v_fieldName2Vc) <> 'Package')
	begin
		select case when
		(
		(select COUNT(f.userId) FROM tbl_FinalSubmission f,tbl_BidderLots bl WHERE f.tenderId=@v_fieldName2Vc and
		  bl.userId=f.userId and bl.tenderId = f.tenderId and bl.pkgLotId = @v_fieldName3Vc	and bidSubStatus='finalsubmission')=
		(SELECT COUNT(evaBidderStatusId) FROM dbo.tbl_EvalBidderStatus WHERE tenderId = @v_fieldName2Vc and evalCount = @v_fieldName4Vc
		  and pkgLotId = @v_fieldName3Vc)
		)
		then '1' else '0' end as FieldValue1
	end
	else
	begin
		IF((select  COUNT(tbl_noaissuedetails.userId)  from tbl_noaacceptance inner join tbl_noaissuedetails 
		on tbl_noaissuedetails.noaissueId = tbl_noaacceptance.noaissueId where tenderId = @v_fieldName2Vc and acceptRejStatus in ('decline','Performance Security not paid')
		) > 0 and @evalCount > 0 )
		BEGIN
		 select @field1 = COUNT(evaBidderStatusId) from tbl_EvalBidderStatus ebs,tbl_FinalSubmission f--,tbl_NoaIssueDetails tnid 
			where ebs.tenderId = f.tenderId and ebs.evalCount = @v_fieldName4Vc and f.tenderId = @v_fieldName2Vc and f.userId = ebs.userId
			and ebs.tenderId  = @v_fieldName2Vc and ebs.userId not in (select userId from tbl_NoaIssueDetails where tenderId = @v_fieldName2Vc)
		select @field2 = count(userId) from tbl_NoaIssueDetails where tenderId = @v_fieldName2Vc
			select case when
			(
						
			(select COUNT(f.userId) FROM tbl_FinalSubmission f WHERE f.tenderId=@v_fieldName2Vc and
			 bidSubStatus='finalsubmission')= @field1 + @field2
			
			  )			 
			
		then '1' else '0' end as FieldValue1
		END
		ELSE 
		BEGIN
			select case when
			(
			(select COUNT(f.userId) FROM tbl_FinalSubmission f WHERE f.tenderId=@v_fieldName2Vc and
			 bidSubStatus='finalsubmission')=
			(SELECT COUNT(evaBidderStatusId) FROM dbo.tbl_EvalBidderStatus WHERE tenderId = @v_fieldName2Vc and evalCount = @evalCount
			  )
			)
		then '1' else '0' end as FieldValue1
		END
	end
END

IF @v_fieldName1Vc='getDocsforDocHash'
BEGIN
	select c.tendererId as FieldValue1,userId as FieldValue2,docHash as FieldValue3,documentName as FieldValue4,companyDocId as FieldValue5 from tbl_CompanyDocuments c,tbl_TendererMaster t
	where c.tendererId=t.tendererId
END
IF @v_fieldName1Vc='getNameOfTenderer'
BEGIN
	select dbo.f_getbiddercompany(userId) as FieldValue1 from tbl_NoaIssueDetails where tenderId = @v_fieldName2Vc and userId=@v_fieldName3Vc and pkgLotId=@v_fieldName4Vc
END

IF @v_fieldName1Vc = 'CheckForTEC'
BEGIN
	SELECT  emailId as FieldValue1,cast(reoiRfpRefNo as varchar(20)) as FieldValue2,
	case when exists (select top 1 tenderId from tbl_EvalTSCNotification where tenderId=@v_fieldName2Vc)
		then 'Yes'
		else 'No'
	end as FieldValue3
	 from tbl_Committee c,tbl_CommitteeMembers cm,tbl_loginmaster lm,tbl_TenderDetails t
		where c.committeeId=cm.committeeId
		and c.tenderId=@v_fieldName2Vc and committeeType in('TEC','PEC')
		and lm.userId=cm.userId and t.tenderId=c.tenderId
		and lm.userId=@v_fieldName3Vc
END

IF @v_fieldName1Vc = 'CheckIssuedNOA'
BEGIN
	select case when
	(select count(tnid.tenderId) from tbl_NoaAcceptance tna, tbl_NoaIssueDetails tnid
	where tnid.noaIssueId = tna.noaIssueId and tnid.tenderId =@v_fieldName2Vc and tnid.pkgLotId=@v_fieldName3Vc and
	tnid.userId = (select top 1 userId from tbl_EvalRoundMaster where tenderId = @v_fieldName2Vc and pkgLotId = @v_fieldName3Vc order by roundId desc)
	and	acceptRejStatus = 'decline') > 0
	THEN
		'yes'
	ELSE
		'no'
	END as FieldValue1,
	case when
	(select count(tnid.tenderId) from tbl_NoaAcceptance tna, tbl_NoaIssueDetails tnid
	where tnid.noaIssueId = tna.noaIssueId and tnid.tenderId =@v_fieldName2Vc and tnid.pkgLotId=@v_fieldName3Vc and

		acceptRejStatus in ('pending','approved')) > 0
	THEN
		'yes'
	ELSE
		'no'
	END as FieldValue2
END
If @v_fieldName1Vc='NoaDocInfo'
   Begin
		Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(noaDocId  as varchar(10)) as FieldValue4,  cast(uploadedBy  as varchar(10)) as FieldValue5,
		CONVERT(varchar(50),roundId) as FieldValue6
		from tbl_NoaDocuments Where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc and userId = @v_fieldName4Vc and roundId=@v_fieldName5Vc
   End

IF @v_fieldName1Vc='getFinalSubComp'
BEGIN

	Declare @noaCount int
	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
			SET @evalCount = 0
		else
			select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc
	select @noaCount = COUNT(tbl_noaissuedetails.userId)  from tbl_noaacceptance inner join tbl_noaissuedetails 
		on tbl_noaissuedetails.noaissueId = tbl_noaacceptance.noaissueId where tenderId = @v_fieldName2Vc and acceptRejStatus in ('decline','Performance Security not paid')

 IF(@noaCount > 0 and @evalCount > 0)
 BEGIN
 SET @evalCount = @evalCount - 1
	SELECT CONVERT(VARCHAR(10),f.tenderid) as FieldValue1,
	CONVERT(VARCHAR(10),l.userid) as FieldValue2,
	CASE WHEN t.companyId=1 THEN firstName+' '+lastname ELSE companyName end  as FieldValue3,
	'-1' as FieldValue4,
	Case
		When (select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='Services'
		Then
			Case When (select COUNT(esFormDetailId) from tbl_EvalSerFormDetail where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc and evalCount = @evalCount) > 0
			Then 'yes'
			Else 'No'
			End
		Else
			Case When (select COUNT(evalMemStatusId) from tbl_EvalMemStatus where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) > 0
			Then 'yes'
			Else 'No'
			End
		End
	as 	FieldValue5,
	Case
		When (select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='Services'
		Then
			--Case When
			--	(select COUNT(esFormDetailId) from tbl_EvalSerFormDetail where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc)
			--	=
			--	(select COUNT(distinct tbf.tenderFormId)
			--		from tbl_TenderBidForm tbf
			--		inner join tbl_TenderForms tf On tbf.tenderFormId=tf.tenderFormId
			--		where tf.isPriceBid='No' and  userId=l.userId And tenderId=@v_fieldName2Vc)
			--Then 'yes'
			--Else 'No'
			--End
			Case When
				(select COUNT(distinct bidId) from tbl_EvalSerFormDetail where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc and evalCount = @evalCount)
				=
				(select COUNT(distinct tbf.bidId)
					from tbl_TenderBidForm tbf
					inner join tbl_TenderForms tf On tbf.tenderFormId=tf.tenderFormId
					and (tf.formStatus is null or tf.formStatus in ('cp','a','p'))  -- modified by dohatec to resolve issue 2087 added 'p'
					where tf.isPriceBid='No' and  userId=l.userId And tenderId=@v_fieldName2Vc)
			Then 'yes'
			Else 'No'
			End
		Else
		Case
			When ((select count(procurementMethodId) from tbl_TenderDetails where tenderId=@v_fieldName2Vc and procurementMethodId in(1,16,17))>=1)
		 Then

			Case When
				(select COUNT(distinct formId) from tbl_EvalMemStatus where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc)
				=
				(select COUNT(distinct tbf.tenderFormId)
					from tbl_TenderBidForm tbf
					inner join tbl_TenderForms tf On tbf.tenderFormId=tf.tenderFormId
					and (tf.formStatus is null or tf.formStatus in ('cp','a','p')) -- modified by dohatec to resolve issue 2087 added 'p'
					where tf.isPriceBid='No' and userId=l.userId And tenderId=@v_fieldName2Vc)
			Then 'yes'
			Else 'No'
			End
			When ((select count(procurementMethodId) from tbl_TenderDetails where tenderId=@v_fieldName2Vc and procurementMethodId in(14))>=1)
		 Then

			Case When
				(select COUNT(distinct formId) from tbl_EvalMemStatus where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc)
				=
				(select COUNT(distinct tbf.tenderFormId)
					from tbl_TenderBidForm tbf
					inner join tbl_TenderForms tf On tbf.tenderFormId=tf.tenderFormId
					and (tf.formStatus is null or tf.formStatus in ('cp','a','p')) -- modified by dohatec to resolve issue 2087 added 'p'
					where  userId=l.userId And tenderId=@v_fieldName2Vc)
			Then 'yes'
			Else 'No'
			End
		Else
			Case When
				(select COUNT(distinct formId) from tbl_EvalMemStatus where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc)
				=
				(select COUNT(distinct tbf.tenderFormId)
					from tbl_TenderBidForm tbf
					inner join tbl_TenderForms tf On tbf.tenderFormId=tf.tenderFormId
					and (tf.formStatus is null or tf.formStatus in ('cp','a','p')) -- modified by dohatec to resolve issue 2087 added 'p'
					where tf.isPriceBid='No' and  userId=l.userId And tenderId=@v_fieldName2Vc)
			Then 'yes'
			Else 'No'
			End
		End
		End
	as 	FieldValue6,
	CONVERT(varchar(50),t.tendererId) as FieldValue7,
	CONVERT(varchar(50),t.companyId) as FieldValue8,
	isnull ((select Case isClarificationComp When 'No' Then 'Clarification Sought' When 'Yes' Then 'Clarification Received' End from tbl_EvalBidderResp where tenderId=@v_fieldName2Vc and userId=l.userId and evalstatus=''+@v_fieldName4Vc+''),'-') as FieldValue9
	FROM dbo.tbl_loginmaster l,tbl_companymaster c,tbl_TendererMaster t,tbl_FinalSubmission f, tbl_EvalBidderStatus ebs
	WHERE l.userId=f.userId and c.companyId=t.companyId
	and t.userid=l.userid and f.tenderId=@v_fieldName2Vc
	and bidSubStatus='finalsubmission' 
	and ebs.tenderId = f.tenderId and ebs.userId = l.userId and ebs.evalCount = @evalCount
	and ebs.userId not in (CASE WHEN ((select TOP 1 DATEDIFF(Minute, ersa.sentDate, tnid.createdDt) from tbl_NoaIssueDetails tnid ,tbl_EvalRptSentToAA  ersa
		where tnid.tenderId = @v_fieldName2Vc and tnid.tenderId = ersa.tenderId and ersa.evalCount != 0 and ersa.evalCount = (select MAX(evalcount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)) <0)		
		THEN (select userId from tbl_NoaIssueDetails where tenderId = @v_fieldName2Vc)
		ELSE '' 
		END)	
	ORDER BY FieldValue3
  END
  ELSE -- for
  BEGIN
    SELECT CONVERT(VARCHAR(10),f.tenderid) as FieldValue1,
	CONVERT(VARCHAR(10),l.userid) as FieldValue2,
	CASE WHEN t.companyId=1 THEN firstName+' '+lastname ELSE companyName end  as FieldValue3,
	'-1' as FieldValue4,
	Case
		When (select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='Services'
		Then
			Case When (select COUNT(esFormDetailId) from tbl_EvalSerFormDetail where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc and evalCount = @evalCount) > 0
			Then 'yes'
			Else 'No'
			End
		Else
			Case When (select COUNT(evalMemStatusId) from tbl_EvalMemStatus where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) > 0
			Then 'yes'
			Else 'No'
			End
		End
	as 	FieldValue5,
	Case
		When (select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='Services'
		Then
			--Case When
			--	(select COUNT(esFormDetailId) from tbl_EvalSerFormDetail where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc)
			--	=
			--	(select COUNT(distinct tbf.tenderFormId)
			--		from tbl_TenderBidForm tbf
			--		inner join tbl_TenderForms tf On tbf.tenderFormId=tf.tenderFormId
			--		where tf.isPriceBid='No' and  userId=l.userId And tenderId=@v_fieldName2Vc)
			--Then 'yes'
			--Else 'No'
			--End
			Case When
				(select COUNT(distinct bidId) from tbl_EvalSerFormDetail where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc and evalCount = @evalCount)
				=
				(select COUNT(distinct tbf.bidId)
					from tbl_TenderBidForm tbf
					inner join tbl_TenderForms tf On tbf.tenderFormId=tf.tenderFormId
					and (tf.formStatus is null or tf.formStatus in ('cp','a','p'))  -- modified by dohatec to resolve issue 2087 added 'p'
					where tf.isPriceBid='No' and  userId=l.userId And tenderId=@v_fieldName2Vc)
			Then 'yes'
			Else 'No'
			End
		Else
		Case
			When ((select count(procurementMethodId) from tbl_TenderDetails where tenderId=@v_fieldName2Vc and procurementMethodId in(1,16,17))>=1)
		 Then

			Case When
				(select COUNT(distinct formId) from tbl_EvalMemStatus where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc)
				=
				(select COUNT(distinct tbf.tenderFormId)
					from tbl_TenderBidForm tbf
					inner join tbl_TenderForms tf On tbf.tenderFormId=tf.tenderFormId
					and (tf.formStatus is null or tf.formStatus in ('cp','a','p')) -- modified by dohatec to resolve issue 2087 added 'p'
					where tf.isPriceBid='No' and userId=l.userId And tenderId=@v_fieldName2Vc)
			Then 'yes'
			Else 'No'
			End
			When ((select count(procurementMethodId) from tbl_TenderDetails where tenderId=@v_fieldName2Vc and procurementMethodId in(14))>=1)
		 Then

			Case When
				(select COUNT(distinct formId) from tbl_EvalMemStatus where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc)
				=
				(select COUNT(distinct tbf.tenderFormId)
					from tbl_TenderBidForm tbf
					inner join tbl_TenderForms tf On tbf.tenderFormId=tf.tenderFormId
					and (tf.formStatus is null or tf.formStatus in ('cp','a','p')) -- modified by dohatec to resolve issue 2087 added 'p'
					where  userId=l.userId And tenderId=@v_fieldName2Vc)
			Then 'yes'
			Else 'No'
			End
		Else
			Case When
				(select COUNT(distinct formId) from tbl_EvalMemStatus where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc)
				=
				(select COUNT(distinct tbf.tenderFormId)
					from tbl_TenderBidForm tbf
					inner join tbl_TenderForms tf On tbf.tenderFormId=tf.tenderFormId
					and (tf.formStatus is null or tf.formStatus in ('cp','a','p')) -- modified by dohatec to resolve issue 2087 added 'p'
					where tf.isPriceBid='No' and  userId=l.userId And tenderId=@v_fieldName2Vc)
			Then 'yes'
			Else 'No'
			End
		End
		End
	as 	FieldValue6,
	CONVERT(varchar(50),t.tendererId) as FieldValue7,
	CONVERT(varchar(50),t.companyId) as FieldValue8,
	isnull ((select Case isClarificationComp When 'No' Then 'Clarification Sought' When 'Yes' Then 'Clarification Received' End from tbl_EvalBidderResp where tenderId=@v_fieldName2Vc and userId=l.userId and evalstatus=''+@v_fieldName4Vc+''),'-') as FieldValue9
	FROM dbo.tbl_loginmaster l,tbl_companymaster c,tbl_TendererMaster t,tbl_FinalSubmission f
	WHERE l.userId=f.userId and c.companyId=t.companyId
	and t.userid=l.userid and f.tenderId=@v_fieldName2Vc
	and bidSubStatus='finalsubmission' 
	--(@evalCount>0)--and
	-- pq.evalCount = @evalCount 
	--and pq.noaStatus = 'pending' and pq.tenderId = @v_fieldName2Vc and pq.tenderId = f.tenderId and l.userId = pq.userId
	ORDER BY FieldValue3
  END	
END
IF @v_fieldName1Vc = 'isTERSignedLabel'
BEGIN

	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
			SET @evalCount = 0
		else
			select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc
			
	select case when COUNT(torSignId)=0 then 'View and Sign' else 'View' end as FieldValue1
	from tbl_TORRptSign where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc and reportType=''+@v_fieldName4Vc+''
	and userId=@v_fieldName5Vc and roundId = @v_fieldName6Vc and evalCount = @evalCount


end
IF @v_fieldName1Vc = 'isCRFormulaMadeTORTER'
BEGIN
	IF @v_fieldName3Vc='0'
		BEGIN
			select case when COUNT(rm.reportId)!=0 then '1' else '0' end as FieldValue1
			from tbl_ReportMaster rm inner join tbl_ReportTableMaster rtm
			on rm.reportId = rtm.reportId inner join tbl_ReportFormulaMaster rfm on rfm.reportTableId = rtm.reportTableId
			where tenderId=@v_fieldName2Vc and isTORTER=''+@v_fieldName4Vc+''
		END
	ELSE
		BEGIN
			select case when COUNT(rm.reportId)!=0 then '1' else '0' end as FieldValue1
			from tbl_ReportMaster rm inner join tbl_ReportTableMaster rtm
			on rm.reportId = rtm.reportId inner join tbl_ReportFormulaMaster rfm on rfm.reportTableId = rtm.reportTableId
			inner join tbl_ReportLots rl on rl.reportId = rm.reportId
			where tenderId=@v_fieldName2Vc and isTORTER=''+@v_fieldName4Vc+'' and rl.pkgLotId=@v_fieldName3Vc
		END
end
If @v_fieldName1Vc='ContractListingTenderSide'
Begin
		select cast(contractNo as varchar(100)) as FieldValue1,cast(contractAmt as varchar(50)) as FieldValue2,
			REPLACE(CONVERT(VARCHAR(11),contractDt, 106), ' ', '-') as FieldValue3,
			ISNULL(REPLACE(CONVERT(VARCHAR(11), acceptRejDt, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), acceptRejDt,108), 1, 5), '-')  as FieldValue4,
			cast(tbl_NoaIssueDetails.noaIssueId as varchar(50)) as FieldValue5,
			acceptRejStatus as FieldValue6,cast(tbl_NoaIssueDetails.tenderId as varchar(50)) as FieldValue7,cast(tbl_NoaIssueDetails.pkgLotId as varchar(50)) as FieldValue8,
			ISNULL(REPLACE(CONVERT(VARCHAR(11),tbl_NoaIssueDetails.noaAcceptDt, 106), ' ', '-'),'-') as FieldValue9,
			cast(advanceContractAmt as varchar(500)) as FieldValue10
			from tbl_NoaIssueDetails inner join tbl_NoaAcceptance
			on tbl_NoaIssueDetails.noaIssueId=tbl_NoaAcceptance.noaIssueId

			--added for repeat order
			where tbl_NoaIssueDetails.isRepeatOrder='no'
			--where
			and tbl_NoaIssueDetails.userId=@v_fieldName2Vc and tbl_NoaIssueDetails.tenderId=@v_fieldName3Vc and tbl_NoaIssueDetails.pkgLotId =@v_fieldName4Vc

		--select cast(contractNo as varchar(20)) as FieldValue1,cast(contractAmt as varchar(50)) as FieldValue2,REPLACE(CONVERT(VARCHAR(11),createdDt, 106), ' ', '-') as FieldValue3,REPLACE(CONVERT(VARCHAR(11),contractDt, 106), ' ', '-') as FieldValue4,cast(tbl_NoaIssueDetails.noaIssueId as varchar(50)) as FieldValue5,acceptRejStatus as FieldValue6,cast(tbl_NoaIssueDetails.tenderId as varchar(50)) as FieldValue7
		--from tbl_NoaIssueDetails inner join tbl_NoaAcceptance on tbl_NoaIssueDetails.noaIssueId=tbl_NoaAcceptance.noaIssueId where tbl_NoaIssueDetails.userId=@v_fieldName2Vc
End
--------------added for Repeat Order-------------
If @v_fieldName1Vc='ContractListingTenderSideForRO'
Begin
		select cast(contractNo as varchar(20)) as FieldValue1,cast(contractAmt as varchar(50)) as FieldValue2,
			REPLACE(CONVERT(VARCHAR(11),contractDt, 106), ' ', '-') as FieldValue3,
			ISNULL(REPLACE(CONVERT(VARCHAR(11), acceptRejDt, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), acceptRejDt,108), 1, 5), '-')  as FieldValue4,
			cast(tbl_NoaIssueDetails.noaIssueId as varchar(50)) as FieldValue5,
			acceptRejStatus as FieldValue6,cast(tbl_NoaIssueDetails.tenderId as varchar(50)) as FieldValue7,cast(tbl_NoaIssueDetails.pkgLotId as varchar(50)) as FieldValue8,
			ISNULL(REPLACE(CONVERT(VARCHAR(11),tbl_NoaIssueDetails.noaAcceptDt, 106), ' ', '-'),'-') as FieldValue9
			from tbl_NoaIssueDetails inner join tbl_NoaAcceptance
			on tbl_NoaIssueDetails.noaIssueId=tbl_NoaAcceptance.noaIssueId

			--added for repeat order
			where tbl_NoaIssueDetails.isRepeatOrder='yes'
			--where
			and tbl_NoaIssueDetails.userId=@v_fieldName2Vc and tbl_NoaIssueDetails.tenderId=@v_fieldName3Vc and tbl_NoaIssueDetails.pkgLotId =@v_fieldName4Vc
			and tbl_NoaIssueDetails.roundId =@v_fieldName5Vc
		--select cast(contractNo as varchar(20)) as FieldValue1,cast(contractAmt as varchar(50)) as FieldValue2,REPLACE(CONVERT(VARCHAR(11),createdDt, 106), ' ', '-') as FieldValue3,REPLACE(CONVERT(VARCHAR(11),contractDt, 106), ' ', '-') as FieldValue4,cast(tbl_NoaIssueDetails.noaIssueId as varchar(50)) as FieldValue5,acceptRejStatus as FieldValue6,cast(tbl_NoaIssueDetails.tenderId as varchar(50)) as FieldValue7
		--from tbl_NoaIssueDetails inner join tbl_NoaAcceptance on tbl_NoaIssueDetails.noaIssueId=tbl_NoaAcceptance.noaIssueId where tbl_NoaIssueDetails.userId=@v_fieldName2Vc
End
If @v_fieldName1Vc='getPENameODER'
BEGIN
	select peName as FieldValue1 from tbl_TenderDetails where tenderId =@v_fieldName2Vc
END


IF @v_fieldName1Vc='getFinalResponseLinkStatus'
BEGIN
	/*
		@v_fieldName2Vc= tenderId
		v_fieldName3Vc= Evaluation Status
	*/

	Select Case ec.configType
		When 'team' Then
			Case
				when exists (select evalClrToCpId from tbl_EvalSentQueToCp where tenderId=td.tenderId and sentBy=(select  en.tecMemberId from tbl_EvalConfig en where en.evalConfigId=ec.evalConfigId) and sentFor='evaluation')
				then 'Yes'
				else 'No'
			end
		When 'ind' Then
			Case
				when
				(
					(select count(distinct sentBy) from tbl_EvalSentQueToCp
						where tenderId=td.tenderId and sentFor='evaluation')
					=
					(select COUNT(distinct cm.userId) from tbl_CommitteeMembers cm
						inner join tbl_Committee c On cm.committeeId=c.committeeId
						and c.tenderId=td.tenderId
						and c.committeeType in ('tec','pec')
						and c.committeStatus='approved'
						and cm.memberRole<>'cp'
					)
				)
				then 'yes'
				else 'No'
			End
		Else 'No'
	End	 as FieldValue1
	from tbl_TenderDetails td
	inner join tbl_EvalConfig ec on td.tenderId=ec.tenderId
	where td.tenderId=@v_fieldName2Vc
	and ec.evalStatus=''+@v_fieldName3Vc+''
END

If @v_fieldName1Vc='isTERNotified'
BEGIN
	select case when count(evalNotId)!=0 then '1' else '0' end as FieldValue1
	from tbl_EvalTERNotifyMember  where  tenderId=@v_fieldName2Vc
	and pkgLotId=@v_fieldName3Vc and reportType=@v_fieldName4Vc
	and roundId = @v_fieldName5Vc
END



If @v_fieldName1Vc='getTenderAuthorisedUser'
BEGIN
	select convert(varchar(20),userId) as FieldValue1 from tbl_TenderDetails t,tbl_EmployeeOffices e,tbl_EmployeeRoles em,tbl_EmployeeMaster epm
	where t.officeId=e.officeId and e.employeeId=em.employeeId and procurementRoleId=5
	and e.employeeId=epm.employeeId
	and t.tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc
END

If @v_fieldName1Vc='getEvaluationStatus'
BEGIN
	select case when etc.evalStatus='Evaluation' then 'Comments Given' else '-' end  as FieldValue1 from tbl_EvalTSCComments etc
	where etc.formId=@v_fieldName2Vc
END

If @v_fieldName1Vc='getEmailIdOfTenderer'
BEGIN
	select emailId as FieldValue1 from tbl_FinalSubmission tfs,tbl_LoginMaster tlm
	where tlm.userId = tfs.userId and
	tfs.tenderId =@v_fieldName2Vc and tfs.bidSubStatus = 'finalsubmission'
END

If @v_fieldName1Vc='getTendererDetails'
BEGIN
	select case when cm.companyId=1 then tm.address1 else cm.regOffAddress end as FieldValue1,
	convert(varchar(20),tm.userId) as FieldValue2,dbo.f_getbiddercompany(tm.userId) as FieldValue3,
	ttd.tenderBrief as FieldValue4,ttd.procurementMethod as FieldValue5
	from tbl_TendererMaster tm, tbl_CompanyMaster cm ,tbl_NoaIssueDetails tnid, tbl_TenderDetails ttd
	where cm.companyId = tm.companyId and tnid.userId = tm.userId and ttd.tenderId = tnid.tenderId
	and tnid.noaIssueId=@v_fieldName2Vc
	--and tm.userId=@v_fieldName2Vc and tnid.tenderId=@v_fieldName3Vc
END
If @v_fieldName1Vc='delUnselectedLotBid'
BEGIN

	BEGIN TRY
		BEGIN TRAN

delete  from tbl_FinalSubDetail where bidId in(
	select bidId 
	 from tbl_TenderBidForm 
	 where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc and tenderFormId in (
		select tenderFormId 
		from tbl_TenderForms 
		where pkgLotId in (
			select appPkgLotId 
			from tbl_TenderLots 
			where tenderId = @v_fieldName2Vc and appPkgLotId not in (
				select pkgLotId 
				from tbl_bidderLots 
				where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc))))


delete  from tbl_BidDocuments where formId in(
	select tenderFormId 
	from tbl_TenderBidForm 
	where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc and tenderFormId in (
		select tenderFormId 
		from tbl_TenderForms 
		where pkgLotId in (
			select appPkgLotId 
			from tbl_TenderLots 
			where tenderId = @v_fieldName2Vc and appPkgLotId not in (
				select pkgLotId 
				from tbl_bidderLots 
				where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc))))


delete from tbl_BidDocuments where formId in(
	select tenderFormId 
	from tbl_TenderBidForm 
	where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc and tenderFormId in (
		select tenderFormId 
		from tbl_TenderForms 
		where pkgLotId in (
			select appPkgLotId 
			from tbl_TenderLots 
			where tenderId = @v_fieldName2Vc and appPkgLotId not in (
				select pkgLotId 
				from tbl_bidderLots 
				where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc))))


delete from tbl_TenderBidSign where bidId in(
	select bidId 
	 from tbl_TenderBidForm 
	 where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc and tenderFormId in (
		select tenderFormId 
		from tbl_TenderForms 
		where pkgLotId in (
			select appPkgLotId 
			from tbl_TenderLots 
			where tenderId = @v_fieldName2Vc and appPkgLotId not in (
				select pkgLotId 
				from tbl_bidderLots 
				where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc))))


delete from tbl_TenderBidEncrypt where bidTableId in(
	select bidTableId 
	from tbl_TenderBidTable 
	where bidId in(
		select bidId 
		 from tbl_TenderBidForm 
		 where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc and tenderFormId in (
			select tenderFormId 
			from tbl_TenderForms 
			where pkgLotId in (
				select appPkgLotId 
				from tbl_TenderLots 
				where tenderId = @v_fieldName2Vc and appPkgLotId not in (
					select pkgLotId 
					from tbl_bidderLots 
					where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc)))))


delete from tbl_TenderBidDetail where bidTableId in(
	select bidTableId 
	from tbl_TenderBidTable 
	where bidId in(
		select bidId 
		 from tbl_TenderBidForm 
		 where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc and tenderFormId in (
			select tenderFormId 
			from tbl_TenderForms 
			where pkgLotId in (
				select appPkgLotId 
				from tbl_TenderLots 
				where tenderId = @v_fieldName2Vc and appPkgLotId not in (
					select pkgLotId 
					from tbl_bidderLots 
					where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc)))))


delete from tbl_TenderBidTable where bidId in(
	select bidId 
	 from tbl_TenderBidForm 
	 where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc and tenderFormId in (
		select tenderFormId 
		from tbl_TenderForms 
		where pkgLotId in (
			select appPkgLotId 
			from tbl_TenderLots 
			where tenderId = @v_fieldName2Vc and appPkgLotId not in (
				select pkgLotId 
				from tbl_bidderLots 
				where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc))))



delete from tbl_TenderBidForm 
	where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc and tenderFormId in (
	select tenderFormId 
	from tbl_TenderForms 
	where pkgLotId in (
		select appPkgLotId 
		from tbl_TenderLots 
		where tenderId = @v_fieldName2Vc and appPkgLotId not in (
			select pkgLotId 
			from tbl_bidderLots 
			where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc)))

 Select '1' as FieldValue1, ERROR_MESSAGE() as FieldValue2
COMMIT TRAN
	END TRY
	BEGIN CATCH
		BEGIN


			Select '0' as FieldValue1, ERROR_MESSAGE() as FieldValue2
			ROLLBACK TRAN
		END
	END CATCH
END


if @v_fieldName1Vc='chkTSCMember'
begin
	if @v_fieldName4Vc = '1'
	begin
		select convert(varchar(50),COUNT(cm.userId))  as FieldValue1 from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
		where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved'  and c.committeeType in ('TSC')
		and cm.userId=@v_fieldName3Vc and cm.memberRole in ('cp')
	end
	if @v_fieldName4Vc = '0'
	begin
		select convert(varchar(50),COUNT(cm.userId))  as FieldValue1 from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
		where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved'  and c.committeeType in ('TSC')
		and cm.userId=@v_fieldName3Vc and cm.memberRole in ('m','ms')
	end
end
IF @v_fieldName1Vc='GetEvalRptApprovalNOA'
BEGIN
if (select procurementNature from tbl_TenderDetails where tenderId = @v_fieldName2Vc) = 'Services'
begin
select rptStatus as FieldValue1  from tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and evalRptToAAId = (select MAX(evalRptToAAId)  from tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc)
end
else
begin
select rptStatus as FieldValue1  from tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId = @v_fieldName3Vc and evalRptToAAId = (select MAX(evalRptToAAId)  from tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId = @v_fieldName3Vc)
end
End

IF @v_fieldName1Vc='getMainPaymentBanksList'
BEGIN

	SELECT	CONVERT(varchar(50), sBankDevelopId) as FieldValue1,
			sbDevelopName as FieldValue2
	FROM	tbl_ScBankDevPartnerMaster
	WHERE	partnerType='ScheduleBank'
			and isBranchOffice='no'
	order by sbDevelopName
END


IF @v_fieldName1Vc ='getTenderPaymentLisForAdmin'
BEGIN

	/*
	Input parameters
	 @fieldName1Vc = Keyword,
	 @v_fieldName2Vc =Page No,
	 @v_fieldName3Vc = Record Offset,
	 @v_fieldName4Vc =Department Id,
	 @v_fieldName5Vc =Office Id,
	 @v_fieldName6Vc =Bank Id,
	 @v_fieldName7Vc =Amount Operator,
	 @v_fieldName8Vc =Amount,
	 @v_fieldName9Vc =Payment Date To,
	 @v_fieldName10Vc =Payment Date From,
	 @v_fieldName11Vc ='Payment For',
	 @v_fieldName12Vc ='',
	 @v_fieldName13Vc ='',
	 @v_fieldName14Vc ='', -- Payment Type
	 @v_fieldName15Vc ='',
	 @v_fieldName16Vc ='',
	 @v_fieldName17Vc ='',
	 @v_fieldName18Vc ='',
	 @v_fieldName19Vc ='',
	 @v_fieldName20Vc =''
	*/

	select @v_ConditionString_Vc=''
	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	IF @v_fieldName3Vc = '0'
	BEGIN
		Select @v_Page_inInt=1,@v_RecordPerPage_inInt=0
		Set @v_StartAt_Int = 1
		Set @v_StopAt_Int= 0
	END
	ELSE
	BEGIN
	Select @v_Page_inInt=@v_fieldName2Vc, @v_RecordPerPage_inInt=@v_fieldName3Vc

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	END
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */

------------------------------------

If @v_fieldName4Vc is not null And @v_fieldName4Vc<>'' --  Department Id
Begin
	Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And td.departmentId='+@v_fieldName4Vc
	/*
	 If (select departmentType from tbl_DepartmentMaster where departmentId=@v_fieldName4Vc)='Organization'
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And td.departmentId='+@v_fieldName4Vc
	End
	Else If (select departmentType from tbl_DepartmentMaster where departmentId=@v_fieldName4Vc)='Division'
	Begin
	Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And td.tenderId in
	(
		select distinct tenderId
	from tbl_TenderDetails td
	where td.departmentId=' + @v_fieldName4Vc + '
		Or td.departmentId in
		(select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=' + @v_fieldName4Vc+')
	)
	'

 End
	Else If (select departmentType from tbl_DepartmentMaster where departmentId=@v_fieldName4Vc)='Ministry'
	Begin
	Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And td.tenderId in
	(
		select distinct tenderId
		from tbl_TenderDetails td
		where td.departmentId='+@v_fieldName4Vc+'
			Or td.departmentId in
				(select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId='+@v_fieldName4Vc+')
			Or td.departmentId in
				(
					select distinct departmentId from tbl_DepartmentMaster
					where parentDepartmentId in
						(
							select distinct departmentId from tbl_DepartmentMaster
							where parentDepartmentId='+@v_fieldName4Vc+' And departmentType=''Division''
						)
				)
	)
	'



 End
*/
End

If @v_fieldName5Vc is not null And @v_fieldName5Vc<>'' And @v_fieldName5Vc<>'0' --  Office Id
Begin
	Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And td.officeId='+@v_fieldName5Vc
End

If @v_fieldName6Vc is not null And @v_fieldName6Vc<>'' And @v_fieldName6Vc<>'0' --  Bank Id
BEGIN
	IF @v_fieldName14Vc != 'Online'
	BEGIN
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tp.tenderPaymentId in
		(
			select distinct tenderPaymentId
			from tbl_TenderPayment where isVerified=''yes'' and isLive=''yes'' AND paymentmode != ''online''
			and createdBy in
			(
				select distinct userId from tbl_PartnerAdmin
				where sBankDevelopId in
					(select sBankDevelopId from tbl_ScBankDevPartnerMaster where sBankDevelHeadId='+@v_fieldName6Vc+')
			)'
	END
	ELSE IF @v_fieldName14Vc = 'Online'
	BEGIN
		SELECT @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tp.tenderPaymentId in
		(
			select distinct tenderPaymentId
			from tbl_TenderPayment where isVerified=''yes'' and isLive=''yes'' AND paymentmode = ''online''
			and bankname = '''+@v_fieldName6Vc+''''
	END
	SELECT @v_ConditionString_Vc=@v_ConditionString_Vc + ')'
END

If @v_fieldName7Vc is not null And @v_fieldName7Vc<>'' And @v_fieldName8Vc is not null And @v_fieldName8Vc<>''  --  Amount
Begin
	Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tp.amount ' + @v_fieldName7Vc + ' ' + @v_fieldName8Vc
End

IF (@v_fieldName9Vc is not null And @v_fieldName9Vc <>'') And (@v_fieldName10Vc is not null And @v_fieldName10Vc <>'')
Begin
	Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tp.createdDate as Float)) as Datetime) >= ''' + @v_fieldName9Vc + ''' And Cast (Floor(Cast (tp.createdDate as Float)) as Datetime) <= ''' + @v_fieldName10Vc + ''''
End
ELSE IF (@v_fieldName9Vc is not null And @v_fieldName9Vc <>'') And (@v_fieldName10Vc is null OR @v_fieldName10Vc='')
Begin
	Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tp.createdDate as Float)) as Datetime) >= ''' + @v_fieldName9Vc + ''''
End
ELSE IF (@v_fieldName9Vc is not null Or @v_fieldName9Vc <>'') And (@v_fieldName10Vc is not null And @v_fieldName10Vc <>'')
Begin
	Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And Cast (Floor(Cast (tp.createdDate as Float)) as Datetime) <= ''' + @v_fieldName10Vc + ''''
End


If @v_fieldName11Vc is not null And @v_fieldName11Vc<>'' --  Payment For
Begin
	Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And paymentFor='''+@v_fieldName11Vc + ''''
End


If @v_fieldName12Vc is not null And @v_fieldName12Vc<>'' --  Tender ID
Begin
	Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And td.tenderId='+@v_fieldName12Vc
End


If @v_fieldName13Vc is not null And @v_fieldName13Vc<>'' --  Branch Id
Begin
	Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tp.tenderPaymentId in
	(
		select distinct tenderPaymentId
		from tbl_TenderPayment where isVerified=''yes'' and isLive=''yes''
		and createdBy in
		(
			select distinct userId from tbl_PartnerAdmin
			where sBankDevelopId  = '+@v_fieldName13Vc+'
		)
	)'
End



If @v_fieldName14Vc is not null And @v_fieldName14Vc<>'' --  Payment Type
Begin
	If @v_fieldName14Vc = 'Online'
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tp.paymentInstType=''online'''
	End
	If @v_fieldName14Vc = 'Offline'
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tp.paymentInstType!=''online'''
	End
End

print (@v_ConditionString_Vc)
-------------------------------------

/* output parameter
 FieldValue1 =Tender Id,
 FieldValue2 =Reference No. ,
	 FieldValue3 =Lot No.,
	 FieldValue4 = Office Name (Procuring Entity),
	 FieldValue5 =Organization,
	 FieldValue6 =Division,
	 FieldValue7 =Ministry,
	 FieldValue8 =Bank Name,
	 FieldValue9 =Branch Name,
	 FieldValue10 =Amount ,
	 FieldValue11 =Payment Date and Time,
	 FieldValue12 =Tenderer / Consultant Name,
	 FieldValue13 ='Tender Payment  Id',
	 FieldValue14 ='Lot Id',
	 FieldValue15 ='Office ID',
	 FieldValue16 ='Department ID',
	 FieldValue17 ='Bidder User ID',
	 FieldValue18 ='Payment For',
	 FieldValue19 ='Row Number',
	 FieldValue20 ='Total Record Count'

*/

	DECLARE @v_countData VARCHAR(MAX)
	SET @v_countData =
	'select COUNT(distinct tenderPaymentId) from tbl_TenderPayment tp
		inner join tbl_TenderDetails td on tp.tenderId=td.tenderId
		inner join tbl_OfficeMaster om on td.officeId=om.officeId
		inner join tbl_DepartmentMaster dm on td.departmentId=dm.departmentId
		where tp.isLive=''yes'' and tp.isVerified=''yes''
		 '+ @v_ConditionString_Vc
	IF	@v_StopAt_Int= 0 AND @v_RecordPerPage_inInt = 0
	BEGIN
		DECLARE	@tblReccount TABLE
			(
				recCount INT
			)
			INSERT INTO @tblReccount (recCount)
			EXEC   (@v_countData)
		SELECT @v_StopAt_Int = recCount ,@v_RecordPerPage_inInt = recCount FROM	 @tblReccount
	END

 Select @FinalQuery='
With CTE1 as (
	select ROW_NUMBER() Over (Order by tenderPaymentId desc) as RowNumber, tenderPaymentId
	From
	(
		select distinct tenderPaymentId from tbl_TenderPayment tp
		inner join tbl_TenderDetails td on tp.tenderId=td.tenderId
		inner join tbl_OfficeMaster om on td.officeId=om.officeId
		inner join tbl_DepartmentMaster dm on td.departmentId=dm.departmentId
		where tp.isLive=''yes'' and tp.isVerified=''yes''
		 '+ @v_ConditionString_Vc +
	') Tmp
)
select
Convert(varchar(50),td.tenderId) as FieldValue1,
td.reoiRfpRefNo as FieldValue2,
IsNull((SELECT lotNo FROM tbl_TenderLotSecurity tls where tls.tenderId=tp.tenderId and tls.appPkgLotId=tp.pkgLotId),'''') as FieldValue3,
om.officeName as FieldValue4,

		Case dm.departmentType
			When ''Organization''
			Then dm.departmentName
			Else ''''
		End as FieldValue5,

		Case dm.departmentType
			When ''Organization''
			Then
				Case
					When ((select departmentType from tbl_DepartmentMaster where departmentId=dm.parentDepartmentId)=''Division'')
					Then (select departmentName from tbl_DepartmentMaster where departmentId=dm.parentDepartmentId)
				Else ''''
				End
			When ''Division''
			Then dm.departmentName
			Else ''''
		End as FieldValue6,

		Case dm.departmentType
			When ''Organization''
			Then
				Case
					When ((select departmentType from tbl_DepartmentMaster where departmentId=dm.parentDepartmentId)=''Ministry'')
					Then (select departmentName from tbl_DepartmentMaster where departmentId=dm.parentDepartmentId)
					When ((select departmentType from tbl_DepartmentMaster where departmentId=dm.parentDepartmentId)=''Division'')
					Then (select departmentName from tbl_DepartmentMaster where departmentId=(select parentDepartmentId from tbl_DepartmentMaster where departmentId=dm.parentDepartmentId))
					Else ''''
				End
			When ''Division''
			Then (select departmentName from tbl_DepartmentMaster where departmentId=dm.parentDepartmentId)
			When ''Ministry''
			Then dm.departmentName
		Else ''''
		End as FieldValue7,

		tp.bankName as FieldValue8,
		CASE WHEN paymentInstType=''Online'' THEN ''Online'' ELSE tp.branchName END as FieldValue9,
		Convert(varchar(50),tp.amount) as FieldValue10,
		REPLACE(CONVERT(VARCHAR(11),tp.createdDate, 106), '' '', ''-'')+ '' '' + Substring(CONVERT(VARCHAR(30),tp.createdDate,108),1,5) as FieldValue11,
		dbo.f_getbiddercompany(tp.userId) as FieldValue12,

		Convert(varchar(50),tp.tenderPaymentId) as FieldValue13,
		Convert(varchar(50),tp.pkgLotId) as FieldValue14,
		Convert(varchar(50),td.officeId) as FieldValue15,
		Convert(varchar(50),td.departmentId) as FieldValue16,
		Convert(varchar(50),tp.userId) as FieldValue17,
		Case paymentFor
			When ''Document Fees''
			Then ''df''
			When ''Tender Security''
			Then ''ts''
			When ''Performance Security''
			Then ''ps''
			Else ''''
		End as FieldValue18,
		CONVERT(varchar(50), RowNumber) as FieldValue19,
		(select CONVERT(varchar(50), COUNT(RowNumber))  from CTE1) as FieldValue20,
		RowNumber
from CTE1
inner join tbl_TenderPayment tp on CTE1.tenderPaymentId=tp.tenderPaymentId
inner join tbl_TenderDetails td on tp.tenderId=td.tenderId
inner join tbl_DepartmentMaster dm on td.departmentId=dm.departmentId
LEFT join tbl_OfficeMaster om on td.officeId=om.officeId
WHERE RowNumber
	Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
+ ' Order by RowNumber'


/*
 Select @FinalQuery='
 With CTE1 as (
 Select * from (
 select
  convert(varchar(50),a.companyDocId ) as FieldValue1,
 convert(varchar(150),documentName) as FieldValue2
      , convert(varchar(150),documentSize) as FieldValue3
      , convert(varchar(250),documentBrief) as FieldValue4
      ,ISNULL(REPLACE(CONVERT(VARCHAR(11), uploadedDate, 106), '' '', ''-'') + '' '' +SUBSTRING(CONVERT(VARCHAR(5), uploadedDate,108), 1, 5), ''-'') as FieldValue5
      , convert(varchar(50),docstatus) as FieldValue6
      , convert(varchar(50),b.companyDocId ) as FieldValue7
       ,IsNull(docHash,'''') as FieldValue13
       , case when b.companyDocId is not null then '''+@v_FormList_Vc3+''' else null end as FieldValue10
       , documentTypeId as FieldValue11
       ,Row_Number() Over('+@v_fieldName7Vc+') as RowNumber
      from (	select c.* from tbl_CompanyDocuments c,
					tbl_TendererMaster t
					where c.tendererId=t.tendererId and userId='+@v_fieldName3Vc+' and docstatus=''archive'' '
				+ @v_ConditionString_Vc +
			') a
left outer join
(select distinct companyDocId  from tbl_biddocuments where  userId='+@v_fieldName3Vc+') b
on a.companyDocId=b.companydocid
) as Tmp
	)
	select *, (select CONVERT(varchar(50), COUNT(*))  from CTE1) as FieldValue12 from CTE1
	WHERE RowNumber
	Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
	+ ' Order by RowNumber'
*/


	--print(@v_ConditionString_Vc)
 print (@FinalQuery)
 Exec (@FinalQuery)

END


IF @v_fieldName1Vc = 'getTenderPaymentDetailForAdmin'
BEGIN

	/*
	FieldValue1: Branch Name
	FieldValue2: Branch Manager (Name of Person who has done payment)
	FieldValue3: Curreny
	FieldValue4: Amount
	FieldValue5: Payment Instrument Type
	FieldValue6: Date Of Payment
	FieldValue7: Comments

	*/
	SELECT	branchName as FieldValue1,
			ISNULL(dbo.f_GovUserName(TP.partTransId, 'tbl_PartnerAdminTransfer'),'-') as FieldValue2,
			currency as FieldValue3,
			Convert(varchar(50), tp.amount) as FieldValue4,
			paymentInstType as FieldValue5,
			REPLACE(CONVERT(VARCHAR(11),createdDate, 106), ' ', '-') + ' ' + Substring(CONVERT(VARCHAR(5),createdDate,108),1,8) as FieldValue6,
			comments as FieldValue7,
			isVerified as FieldValue8,
			Convert(varchar(50), extValidityRef) as FieldValue9,
			dbo.f_initcap(TP.status) as FieldValue10,
			(select	top 1 dbo.f_GovUserName(verifyPartTransId, 'tbl_PartnerAdminTransfer') from	tbl_TenderPaymentVerification where paymentId = TP.tenderPaymentId) as FieldValue11,
			TP.bankName as  FieldValue12,
			LM.emailId as FieldValue13,
			TP.OnlineTransId AS FieldValue14,
			TP.instRefNumber AS FieldValue15,
			CASE WHEN TP.paymentInstType='Online' THEN CONVERT(VARCHAR(25),ISNULL(OPD.serviceChargeAmt,'0.00')) ELSE '-' END AS FieldValue16,
			CASE WHEN TP.paymentInstType='Online' THEN CONVERT(VARCHAR(25),ISNULL(OPD.TotalAmount,'0.00')) ELSE '-' END AS FieldValue17
	FROM	tbl_TenderPayment TP
			Inner join tbl_LoginMaster LM On TP.userId=LM.userId
			LEFT Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
			LEFT JOIN tbl_OnlinePaymentTransDetails OPD ON TP.OnlineTransId = OPD.TransId
					AND TP.userId = OPD.userId
	WHERE	tenderPaymentId=@v_fieldName2Vc
			And isLive='yes'
			And isVerified='yes'
END
--changes for re-evaluation
IF @v_fieldName1Vc = 'chkLotSelectionSubmission'
BEGIN
	IF(select procurementNature from tbl_TenderDetails where tenderId = @v_fieldName2Vc) = 'Services'
	BEGIN
	select case when ((select COUNT(pkgLotId) cnt from tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and rptStatus not in('seek clarification','Pending','Re-evaluation'))=
		 1) and ((select COUNT(tenderid) from tbl_TenderDetails where pqTenderId = @v_fieldName2Vc) <1)
		THEN
			'Yes'
		ELSE
			'No'
		END as FieldValue1
	END
	ELSE
	BEGIN
		select case when ((select COUNT(pkgLotId) cnt from tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and rptStatus not in('seek clarification','Pending'))=
		 (select COUNT(appPkgLotId) cnt from tbl_TenderLotSecurity where tenderId=@v_fieldName2Vc))
		THEN
			'Yes'
		ELSE
			'No'
		END as FieldValue1
	END
END
IF @v_fieldName1Vc = 'chkMapForAT'
BEGIN
	SELECT CASE WHEN
	Exists (select eventType from tbl_TenderDetails where tenderId = @v_fieldName2Vc and eventType in ('2 stage-TSTM','2 Stage-PQ','RFP') and pqTenderId != 0)
	THEN
		'no'
	ELSE
		'yes'
	END as FieldValue1
END

IF @v_fieldName1Vc = 'chkNOAAcceptDt'
BEGIN
	update	tbl_PostQualification
	set		noaStatus = 'Performance Security not paid'
	where	tenderId in	(	select	tenderId
							from	tbl_NoaIssueDetails n,
									tbl_NoaAcceptance na
							where	na.noaIssueId = n.noaIssueId
									and na.acceptRejStatus = 'Approved'
									and Cast(FLOOR( CAST (perSecSubDt as float)) as Datetime) < Cast(FLOOR( CAST (GETDATE() as float)) as Datetime)
									and perfSecAmt > 0
									and  n.noaissueid not in(	select	extValidityRef
																from	tbl_TenderPayment
																where	paymentFor = 'Performance Security'
																		and status IN ('paid','released','forfeited','extended')
																		and extValidityRef is not null
																)
						)
			and pkgLotId in (	select	pkgLotId
								from	tbl_NoaIssueDetails n,
										tbl_NoaAcceptance na
								where	na.noaIssueId = n.noaIssueId
										and na.acceptRejStatus = 'Approved'
										and Cast(FLOOR( CAST (perSecSubDt as float)) as Datetime) < Cast(FLOOR( CAST (GETDATE() as float)) as Datetime)
										and perfSecAmt > 0
										and  n.noaissueid not in(	select	extValidityRef
																	from	tbl_TenderPayment
																	where	paymentFor = 'Performance Security'
																			and status IN ('paid','released','forfeited','extended')
																			and extValidityRef is not null
																)
							)
			and userId  in (	select	n.userId
								from	tbl_NoaIssueDetails n,
										tbl_NoaAcceptance na
								where	na.noaIssueId = n.noaIssueId
										and na.acceptRejStatus = 'Approved'
										and Cast(FLOOR( CAST (perSecSubDt as float)) as Datetime) < Cast(FLOOR( CAST (GETDATE() as float)) as Datetime)
										and perfSecAmt > 0
										and n.noaissueid not in(	select	extValidityRef
																	from	tbl_TenderPayment
																	where	paymentFor = 'Performance Security'
																			and status IN ('paid','released','forfeited','extended')
																			and extValidityRef is not null
																	)
							)

	update	tbl_NoaAcceptance
	set		acceptRejStatus = 'Performance Security not paid',
			acceptRejType = 'By System'
	where	noaIssueId in(	select	n.noaIssueId
							from	tbl_NoaIssueDetails n,
									tbl_NoaAcceptance na
							where	na.noaIssueId = n.noaIssueId
									and na.acceptRejStatus = 'Approved'
									and	Cast(FLOOR( CAST (perSecSubDt as float)) as Datetime) < Cast(FLOOR( CAST (GETDATE() as float)) as Datetime)
									and perfSecAmt > 0
									and  n.noaissueid not in(	select	extValidityRef
																from	tbl_TenderPayment
																where	paymentFor = 'Performance Security'
																		and status IN ('paid','released','forfeited','extended')
																		and extValidityRef is not null
																)
							)

	update	pq
	set		pq.noaStatus='rejected'
	from	tbl_NoaAcceptance n,
			tbl_NoaIssueDetails na,
			tbl_PostQualification pq
	where	acceptRejStatus='Pending'
			and n.noaIssueId=na.noaIssueId
			and pq.userId = na.userId
			and pq.tenderId = na.tenderId
			and pq.pkgLotId = na.pkgLotId
			and convert(date,dateadd(day,1,noaAcceptDt)) < GETDATE()

	update	n
	set		n.acceptRejStatus='decline',
			acceptRejDt=GETDATE(),
			acceptRejType='By System'
	from	tbl_NoaAcceptance n,
			tbl_NoaIssueDetails na
	where	acceptRejStatus='Pending'
			and n.noaIssueId=na.noaIssueId
			and convert(date,dateadd(day,1,noaAcceptDt)) < GETDATE()
END
IF @v_fieldName1Vc = 'reportTORChk'
BEGIN
	if @v_fieldName3Vc is Null
		BEGIN
			select
			Case When COUNT(torSignId) <> 0 Then '1' Else '0' End as FieldValue1
			from tbl_TORRptSign where tenderId=@v_fieldName2Vc and reportType='tor2'
		END
	ELSE
		BEGIN
			select
			Case When COUNT(torSignId) <> 0 Then '1' Else '0' End as FieldValue1
			from tbl_TORRptSign where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc and reportType='tor2'
		END
END

IF @v_fieldName1Vc = 'getUNameTendererIdCmpId'
BEGIN
	select
	case when cm.companyId=1 then tm.firstName+' '+ tm.lastName else cm.companyName end as FieldValue1,
	CONVERT(varchar(50),tm.companyId)  as FieldValue2,CONVERT(varchar(50),tm.tendererId) as FieldValue3
	from tbl_TendererMaster tm inner join tbl_CompanyMaster cm
	on tm.companyId = cm.companyId and tm.userId=@v_fieldName2Vc
End

IF @v_fieldName1Vc = 'getTenderPaymentDetail'
BEGIN

	/*
	FieldValue1: Branch Name
	FieldValue2: Branch Manager (Name of Person who has done payment)
	FieldValue3: Curreny
	FieldValue4: Amount
	FieldValue5: Payment Instrument Type
	FieldValue6: Date Of Payment
	FieldValue7: Comments
	FieldValue8: Varified or not
	FieldValue9: Validity Ref no
	FieldValue10: Status
	FieldValue11: Bakn Name
	FieldValue12: Payment For
	FieldValue13: Online Transaction Number
	FieldValue14: Instrument Referance Number
	FieldValue15: Service charge for online payment
	FieldValue17: BranchMaker UserId
	FieldValue18: BranchChekcer UserId
	*/

	IF @v_fieldName4Vc = 'New Performance Security'
	BEGIN
		select Convert(varchar(50), bgAmt) as FieldValue1, Convert(varchar(50), paymentId) as FieldValue2 from tbl_Cms_NewBankGuarnatee where tenderId= @v_fieldName3Vc order by paymentId
	END

		IF @v_fieldName3Vc = 'Performance Security'
				Begin

				Select	ISNULL(branchName,'-') as FieldValue1,
						ISNULL(dbo.f_GovUserName(TP.partTransId, 'tbl_PartnerAdminTransfer'),'-') as FieldValue2,
						currency as FieldValue3,
						Convert(varchar(50), tp.amount) as FieldValue4,
						paymentInstType as FieldValue5,
						REPLACE(CONVERT(VARCHAR(11),createdDate, 106), ' ', '-')+ ' ' + Substring(CONVERT(VARCHAR(5),createdDate,108),1,8)  as FieldValue6,
						comments as FieldValue7,
						isVerified as FieldValue8,
						Convert(varchar(50), extValidityRef) as FieldValue9,
						dbo.f_initcap(TP.status) as FieldValue10,
						TP.bankName as  FieldValue11,
						TP.paymentFor as FieldValue12,
						ISNULL(TP.OnlineTransId,'-') as FieldValue13,
						TP.instRefNumber  as FieldValue14,
						CASE WHEN TP.paymentInstType='Online' THEN CONVERT(VARCHAR(25),ISNULL(OPD.serviceChargeAmt,'0.00')) ELSE '-' END AS FieldValue15,
						CASE WHEN TP.paymentInstType='Online' THEN CONVERT(VARCHAR(25),ISNULL(OPD.TotalAmount,'0.00')) ELSE '-' END AS FieldValue16,
						Convert(varchar(50), createdBy) as FieldValue17,Convert(varchar(50), ttpv.verifiedBy) as FieldValue18
				from	tbl_TenderPayment TP
						LEFT JOIN tbl_OnlinePaymentTransDetails OPD ON TP.OnlineTransId = OPD.TransId
									AND TP.userId = OPD.userId
						LEFT Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
						Left Join tbl_TenderPaymentVerification ttpv ON TP.tenderPaymentId = ttpv.paymentId
				 Where	TP.paymentFor=@v_fieldName3Vc And isLive='yes' and TP.tenderId=@v_fieldName4Vc
				End
		Else
		Begin--Dohatec ICT End

Select	ISNULL(branchName,'-') as FieldValue1,
		ISNULL(dbo.f_GovUserName(TP.partTransId, 'tbl_PartnerAdminTransfer'),'-') as FieldValue2,
		currency as FieldValue3,
		Convert(varchar(50), tp.amount) as FieldValue4,
		paymentInstType as FieldValue5,
		REPLACE(CONVERT(VARCHAR(11),createdDate, 106), ' ', '-')+ ' ' + Substring(CONVERT(VARCHAR(5),createdDate,108),1,8)  as FieldValue6,
		comments as FieldValue7,
		isVerified as FieldValue8,
		Convert(varchar(50), extValidityRef) as FieldValue9,
		dbo.f_initcap(TP.status) as FieldValue10,
		TP.bankName as  FieldValue11,
		TP.paymentFor as FieldValue12,
		ISNULL(TP.OnlineTransId,'-') as FieldValue13,
		TP.instRefNumber  as FieldValue14,
		CASE WHEN TP.paymentInstType='Online' THEN CONVERT(VARCHAR(25),ISNULL(OPD.serviceChargeAmt,'0.00')) ELSE '-' END AS FieldValue15,
		CASE WHEN TP.paymentInstType='Online' THEN CONVERT(VARCHAR(25),ISNULL(OPD.TotalAmount,'0.00')) ELSE '-' END AS FieldValue16,
		Convert(varchar(50), createdBy) as FieldValue17,Convert(varchar(50), ttpv.verifiedBy) as FieldValue18
from	tbl_TenderPayment TP
		LEFT JOIN tbl_OnlinePaymentTransDetails OPD ON TP.OnlineTransId = OPD.TransId
					AND TP.userId = OPD.userId
		LEFT Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		Left Join tbl_TenderPaymentVerification ttpv ON TP.tenderPaymentId = ttpv.paymentId
 Where	tenderPaymentId=@v_fieldName2Vc And isLive='yes'
			--Dohatec ICT Start
		End
		--Dohatec ICT End
END


IF @v_fieldName1Vc = 'chkRecordExist'
BEGIN
	--IF @v_fieldName1Vc = 'Document Fees'
	--BEGIN
	--	select Convert(varchar(10), COUNT (tenderPaymentId)) as FieldValue1 from tbl_TenderPayment where tenderId = @v_fieldName2Vc and pkgLotId = @v_fieldName3Vc and userId = @v_fieldName4Vc and paymentFor = @v_fieldName5Vc and isLive = 'yes' and isVerified = 'no'
	--END
	IF((select COUNT (tenderPaymentId) from tbl_TenderPayment where tenderId = @v_fieldName2Vc and pkgLotId = @v_fieldName3Vc and userId = @v_fieldName4Vc and paymentFor = @v_fieldName5Vc and isLive = 'yes' and isVerified = 'no' and extValidityRef = @v_fieldName6Vc) > 0)
		BEGIN
			select 'yes' as FieldValue1
		END
	ELSE
		BEGIN
			select 'no' as FieldValue1
		END
END

IF @v_fieldName1Vc = 'getEvalOpenCommonMember'
BEGIN
	select convert(varchar(50),cm.userId) as FieldValue1 from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
	and c.tenderId = @v_fieldName2Vc and memberFrom in ('TEC','PEC') and c.committeStatus='approved' and cm.appStatus='approved'
END

IF @v_fieldName1Vc = 'getEvalCommitteeMember'
BEGIN
	select convert(varchar(50),cm.userId) as FieldValue1 from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
	and c.tenderId = @v_fieldName2Vc and committeeType in ('TEC','PEC') 
END


IF @v_fieldName1Vc = 'getRegistrationFeePaymentDetailForVerify'
BEGIN
	/*
	FieldValue1: Branch Name
	FieldValue2: Branch Manager (Name of Person who has done payment)
	FieldValue3: Curreny
	FieldValue4: Amount
	FieldValue5: Payment Instrument Type
	FieldValue6: Date Of Payment
	FieldValue7: Comments

	*/

	Select branchName as FieldValue1,
		dbo.f_GovUserName(RP.partTransId, 'tbl_PartnerAdminTransfer') as FieldValue2,
		currency as FieldValue3,
		Convert(varchar(50), amount) as FieldValue4,
		paymentInstType as FieldValue5,
		REPLACE(CONVERT(VARCHAR(11),dtOfPayment, 106), ' ', '-') + ' ' + Substring(CONVERT(VARCHAR(5),dtOfPayment,108),1,8)  as FieldValue6,
		comments as FieldValue7,
		isVerified as FieldValue8,
		Convert(varchar(50), validityPeriodRef) as FieldValue9,
		RP.status as FieldValue10,
		RP.bankName as FieldValue11,
		isnull((select TOP 1 REPLACE(CONVERT(VARCHAR(11),verificationDt, 106), ' ', '-') + ' ' + Substring(CONVERT(VARCHAR(5),verificationDt,108),1,8) from tbl_PaymentVerification where paymentId=RP.regPaymentId and verifyPartTransId  is not null order by verificationId asc),'')as FieldValue12,
		isnull((select TOP 1 dbo.f_GovUserName(verifyPartTransId, 'tbl_PartnerAdminTransfer') from tbl_PaymentVerification where paymentId=RP.regPaymentId and verifyPartTransId  is not null order by verificationId asc),'')as FieldValue13,
		Convert(varchar(50),rp.createdBy) as FieldValue14,(select TOP 1 Convert(varchar(50),verifiedBy) from tbl_PaymentVerification where paymentId=RP.regPaymentId and verifyPartTransId  is not null order by verificationId asc) as FieldValue15
	from tbl_RegFeePayment RP Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
Where regPaymentId=@v_fieldName2Vc And isLive='yes'


END

IF @v_fieldName1Vc = 'GetEvalCPClarificationLinkStatus'
BEGIN
if @v_fieldName4Vc<>0
begin
SELECT top 1 a.tenderFormId FROM tbl_EvalCpMemClarification a
inner join tbl_TenderForms b on a.tenderFormId=b.tenderFormId
inner join tbl_TenderSection ts On b.tenderSectionId=ts.tenderSectionId
inner join tbl_TenderStd td On ts.tenderStdId=td.tenderStdId
Where userId=@v_fieldName2Vc and td.tenderId=@v_fieldName3Vc and b.pkgLotId = @v_fieldName4Vc
end
else
begin
SELECT top 1 a.tenderFormId FROM tbl_EvalCpMemClarification a
inner join tbl_TenderForms b on a.tenderFormId=b.tenderFormId
inner join tbl_TenderSection ts On b.tenderSectionId=ts.tenderSectionId
inner join tbl_TenderStd td On ts.tenderStdId=td.tenderStdId
Where userId=@v_fieldName2Vc and td.tenderId=@v_fieldName3Vc
end
END


IF @v_fieldName1Vc = 'GetEvalCPClarificationExist'
BEGIN

		SELECT '1' as FieldValue1 FROM dbo.tbl_EvalCpMemClarification
		WHERE userId = @v_fieldName2Vc and memAnswerBy = @v_fieldName3Vc
		and tenderFormId in (
select t.tenderformId from tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std ,tbl_TenderBidForm tbf
			where std.tenderid = @v_fieldName4Vc and t.tenderSectionId=st.tenderSectionId
			and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId and isPriceBid='No'
			and tbf.userId = @v_fieldName2Vc
			And (t.formStatus is null Or t.formStatus in ('p', 'cp'))
)

END


IF @v_fieldName1Vc = 'getMinistryDetails'
BEGIN
	SELECT
      distinct (CASE dm.departmentType
                        WHEN 'Ministry' THEN dm.departmentName
                        WHEN 'Division' THEN pdm.departmentName
                        WHEN 'Organization' THEN
                         (CASE WHEN pdm.departmentType = 'Division' THEN
                               (SELECT    departmentName
                                    FROM  tbl_DepartmentMaster
                                    WHERE departmentId=pdm.parentDepartmentId )
                                                WHEN pdm.departmentType = 'Ministry' THEN pdm.departmentName
                  ELSE '' END) END )
                  AS FieldValue1,

      (CASE dm.departmentType
                   WHEN 'Division' THEN dm.departmentName
                   WHEN 'Organization' THEN
                      (CASE
                                 WHEN pdm.departmentType = 'Division' THEN pdm.departmentName
                                 ELSE 'N/A' END)
                       ELSE 'N/A' END) AS FieldValue2,

      (CASE dm.departmentType
                   WHEN 'Organization'    THEN dm.departmentName
                   ELSE 'N/A' END) AS FieldValue3

      FROM  tbl_DepartmentMaster dm,
                  tbl_OfficeMaster om,
                  tbl_DepartmentMaster pdm,
                  tbl_StateMaster sm
      WHERE dm.departmentId=om.departmentId
                  AND dm.parentDepartmentId=pdm.departmentId
                  AND om.stateId=sm.stateId
                  and (dm.departmentId = @v_fieldName2Vc or officeId = @v_fieldName3Vc)

END

IF @v_fieldName1Vc = 'getReTenderingLink'
BEGIN
	select case when (select COUNT(userId) from tbl_FinalSubmission where tenderId = @v_fieldName2Vc
	and bidSubStatus = 'finalsubmission') = 0
	THEN
		case when ((select openingDt from tbl_TenderDetails where tenderId = @v_fieldName2Vc and tenderStatus ='approved')< GETDATE())
		THEN
			'yes'
		ELSE
			'no'
		END
	ELSE
		'no'
	END AS FieldValue1
END
IF @v_fieldName1Vc = 'getRoundsforEvaluation'
BEGIN
	if(@v_fieldName5Vc is not null)
		SET @evalCount = @v_fieldName5Vc
	else
	BEGIN
	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
			SET @evalCount = 0
		else
			select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc
	END	
		
	--IF ((select COUNT(tbl_noaissuedetails.userId)  from tbl_noaacceptance inner join tbl_noaissuedetails 
	--on tbl_noaissuedetails.noaissueId = tbl_noaacceptance.noaissueId where tenderId = @v_fieldName2Vc and acceptRejStatus in ('decline','Performance Security not paid')
	--	) > 0 and @evalCount > 0 )
	--BEGIN
	--	select convert(varchar(50),er1.roundId) as FieldValue1,convert(varchar(50),er1.userId) as FieldValue2,
	-- case when
	--    (select COUNT(er2.roundId) from tbl_EvalRoundMaster er2 where er2.tenderId=@v_fieldName2Vc and er2.reportType='T1L1' and er2.roundId > er1.roundId)!=0
	-- Then
	--	dbo.f_getbiddercompany((select top 1 er2.userId from tbl_EvalRoundMaster er2 where er2.tenderId=@v_fieldName2Vc and er2.reportType='T1L1' and er2.roundId > er1.roundId order by er2.roundId asc))
	-- Else
	--  case when
	--	 (select COUNT(er2.roundId) from tbl_EvalRoundMaster er2 where er2.tenderId=@v_fieldName2Vc and er2.reportType='T1' and er2.roundId < er1.roundId)!=0
	--  Then
	--	 dbo.f_getbiddercompany((select top 1 er2.userId from tbl_EvalRoundMaster er2 where er2.tenderId=@v_fieldName2Vc and er2.reportType='T1' and er2.roundId < er1.roundId order by er2.roundId asc))
	--  Else
	--	dbo.f_getbiddercompany(er1.userId)
	--  End
	-- End
	--as FieldValue3
	--from tbl_EvalRoundMaster er1 where er1.tenderId=@v_fieldName2Vc and er1.reportId=@v_fieldName3Vc and er1.reportType=@v_fieldName4Vc and er1.evalCount = @evalCount and 
	--er1.userId not in (select tbl_noaissuedetails.userId  from tbl_noaacceptance inner join tbl_noaissuedetails 
	--on tbl_noaissuedetails.noaissueId = tbl_noaacceptance.noaissueId where tenderId = @v_fieldName2Vc and acceptRejStatus in ('decline','Performance Security not paid'))
	--END
	--ELSE
	BEGIN
	select convert(varchar(50),er1.roundId) as FieldValue1,convert(varchar(50),er1.userId) as FieldValue2,
	 case when
	    (select COUNT(er2.roundId) from tbl_EvalRoundMaster er2 where er2.tenderId=@v_fieldName2Vc and er2.reportType='T1L1' and er2.roundId > er1.roundId)!=0
	 Then
		dbo.f_getbiddercompany((select top 1 er2.userId from tbl_EvalRoundMaster er2 where er2.tenderId=@v_fieldName2Vc and er2.reportType='T1L1' and er2.roundId > er1.roundId order by er2.roundId asc))
	 Else
	  case when
		 (select COUNT(er2.roundId) from tbl_EvalRoundMaster er2 where er2.tenderId=@v_fieldName2Vc and er2.reportType='T1' and er2.roundId < er1.roundId)!=0
	  Then
		 dbo.f_getbiddercompany((select top 1 er2.userId from tbl_EvalRoundMaster er2 where er2.tenderId=@v_fieldName2Vc and er2.reportType='T1' and er2.roundId < er1.roundId order by er2.roundId asc))
	  Else
		dbo.f_getbiddercompany(er1.userId)
	  End
	 End
	as FieldValue3
	from tbl_EvalRoundMaster er1 where er1.tenderId=@v_fieldName2Vc and er1.reportId=@v_fieldName3Vc and er1.reportType=@v_fieldName4Vc and er1.evalCount = @evalCount
    END
END

IF @v_fieldName1Vc = 'FirstRoundAndNoaBiddersChk'
BEGIN
	DECLARE @var_cnt int
	if ((select count(tenderid) from tbl_TenderDetails where tenderId=@v_fieldName2Vc and procurementMethod in('DPM')) >=1  )
	BEGIN
		SET @v_fieldName3Vc='0'
	END

	IF @v_fieldName3Vc!='0'
	BEGIN
		IF(SELECT COUNT(na.userId) FROM tbl_NoaIssueDetails nd inner join tbl_NoaAcceptance na on
			na.noaIssueId = nd.noaIssueId and acceptRejStatus in ('decline','Performance Security not paid') and tenderId =@v_fieldName2Vc
			and (pkgLotId = @v_fieldName3Vc OR pkgLotId=0))=
			(SELECT CASE WHEN COUNT(userId)=0 then 1 else COUNT(userId) end  from tbl_BidderRank where
			roundId in (select MIN(roundId) from tbl_EvalRoundMaster where tenderId=@v_fieldName2Vc
			 and reportId=@v_fieldName4Vc and evalCount = @v_fieldName5Vc))
		BEGIN
			SELECT @var_cnt=0
		END
		ELSE
		BEGIN
			SELECT @var_cnt=1
		END

		 IF @var_cnt=0
		 BEGIN
			 IF(select  case when COUNT(userId)= 0 then 5 else COUNT(userId) end from tbl_PostQualification pq
				where postQualStatus='disqualify' and tenderId = @v_fieldName2Vc and evalCount = @v_fieldName5Vc
				and (pkgLotId = @v_fieldName3Vc or pkgLotId=0 )) =
				(select case when COUNT(userId)=0 then 1 else COUNT(userId) end  from tbl_BidderRank where
				roundId in (select MIN(roundId) from tbl_EvalRoundMaster where tenderId=@v_fieldName2Vc
				 and reportId=@v_fieldName4Vc and evalCount = @v_fieldName5Vc))
			 BEGIN
				SELECT @var_cnt=0
			 END
			 ELSE
			 BEGIN
				SELECT @var_cnt=1
			 END
		 END
		 IF @var_cnt=1
		 Begin
			IF (select count(roundId) from tbl_EvalRoundMaster where tenderId=@v_fieldName2Vc and evalCount = @v_fieldName5Vc)!=0
			BEGIN
				IF((select top 1 userId from tbl_PostQualification where  (postQualStatus='disqualify' or noaStatus='rejected' or noaStatus='Performance Security not paid') and tenderId=@v_fieldName2Vc and evalCount = @v_fieldName5Vc order by postQaulId desc)
				=
			   (select top 1 userId from tbl_EvalRoundMaster where tenderId=@v_fieldName2Vc and evalCount = @v_fieldName5Vc order by roundId desc))
				BEGIN
					SELECT '1' as FieldValue1
				END
				ELSE
				BEGIN
					SELECT '0' as FieldValue1
				END
			END
			ELSE
			BEGIN
				SELECT '1' as FieldValue1
			END
	     END
	END
	ELSE--start for others @v_fieldName3Vc='0'
	BEGIN
		IF(select COUNT(na.userId) from tbl_NoaIssueDetails nd inner join tbl_NoaAcceptance na on
				na.noaIssueId = nd.noaIssueId and acceptRejStatus in ('decline','Performance Security not paid') and tenderId =@v_fieldName2Vc
				and (pkgLotId = @v_fieldName3Vc OR pkgLotId=0))=
				(select case when COUNT(userId)=0 then 1 else COUNT(userId) end  from tbl_BidderRank where
				roundId in (select MIN(roundId) from tbl_EvalRoundMaster where tenderId=@v_fieldName2Vc
				 and reportId=@v_fieldName4Vc and evalCount = @v_fieldName5Vc))
		Begin
			select @var_cnt=0
		END
		ELSE
		BEGIN
			select @var_cnt=1
		End
		IF @var_cnt=1
		Begin
			IF(select count(nn.userId) from tbl_Negotiation tn inner join tbl_NegNotifyTenderer nn on tn.negId = nn.negId
				where negStatus='Failed' and tenderId = @v_fieldName2Vc and evalCount = @v_fieldName5Vc ) =
				(select count(userId)  from tbl_EvalRoundMaster where
				roundId in (select MAX(roundId) from tbl_EvalRoundMaster where tenderId=@v_fieldName2Vc
				 and reportId=@v_fieldName4Vc and evalCount = @v_fieldName5Vc and reportType not in ('N1','T1')))
			BEGIN
				SELECT '1' as FieldValue1
			END
			ELSE
			BEGIN
				SELECT '0' as FieldValue1
			END
		END
		ELSE
		BEGIN
			select '0' as FieldValue1
		END
	END
END

IF @v_fieldName1Vc = 'roundWiseInsert'
BEGIN
	Declare @v_roundId_inInt int=0 ,@v_flag_bit bit
	
BEGIN TRY
/***added By Dohatec for resolving multiple entry start***/
DECLARE @RoundTenderID INT 
SET  @RoundTenderID = (SELECT DISTINCT tenderId from tbl_EvalRoundMaster where roundId = @v_fieldName2Vc)
IF((select COUNT(distinct userId) from tbl_EvalRoundMaster where tenderId = @RoundTenderID)=
(select COUNT(distinct userId) from tbl_PostQualification where tenderId = @RoundTenderID))
BEGIN
/***added By Dohatec for resolving multiple entry end***/
insert into tbl_EvalRoundMaster ([tenderId]
      ,[pkgLotId]
      ,[reportType]
      ,[reportId]
      ,[userId]
      ,[createdDt]
      ,[createdBy]
      ,[evalCount])

 select [tenderId]
      ,[pkgLotId]
      ,[reportType]
      ,[reportId]
      ,@v_fieldName3Vc
      ,[createdDt]
      ,[createdBy]
      ,[evalCount] from tbl_EvalRoundMaster where roundId=@v_fieldName2Vc
 Set @v_roundId_inInt= Ident_Current('dbo.tbl_EvalRoundMaster')

 insert into tbl_BidderRank([tenderId]
      ,[pkgLotId]
      ,[userId]
      ,[createdTime]
      ,[createdBy]
      ,[rank]
      ,[reportId]
      ,[amount]
      ,[rowId]
      ,[tableId]
      ,[companyName]
      ,[roundId])
 select [tenderId]
      ,[pkgLotId]
      ,[userId]
      ,[createdTime]
      ,[createdBy]
      ,[rank]
      ,[reportId]
      ,[amount]
      ,[rowId]
      ,[tableId]
      ,[companyName]
      ,@v_roundId_inInt from tbl_BidderRank where roundId=@v_fieldName2Vc
		and userId not in (select userId from tbl_EvalRoundMaster where roundId=@v_fieldName2Vc) order by rank

 insert into tbl_BidRankDetail([reportColumnId]
      ,[cellId]
      ,[cellValue]
      ,[bidderRankId])
 select [reportColumnId]
      ,[cellId]
      ,[cellValue]
      ,(select b.bidderRankId  from (select * From tbl_BidderRank where roundId=@v_fieldName2Vc)a,
(select * From tbl_BidderRank where roundId=@v_roundId_inInt)b
where a.tenderId=b.tenderId and a.pkgLotId=b.pkgLotId
and a.userId=b.userId and a.bidderRankId=d.bidderRankId) from tbl_BidRankDetail d
       where bidderRankId
  in(select bidderRankId from tbl_BidderRank where roundId=@v_fieldName2Vc and userId not in (select userId from tbl_EvalRoundMaster where roundId=@v_fieldName2Vc))
    /***added By Dohatec for resolving multiple entry start***/
 END
/***added By Dohatec for resolving multiple entry end***/

 	END TRY
		BEGIN CATCH
			BEGIN
				Set @v_flag_bit=0
				Select @v_flag_bit as flag, 'Error while creating Project.' as Message
				--ROLLBACK TRAN
			END
		END CATCH
END

if @v_fieldName1Vc='getLinkForEditViewEvalTenderer'
begin
if (select procurementNature from tbl_TenderDetails where tenderId = @v_fieldName2Vc) = 'Services'
begin
	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
		SET @evalCount = 0
	else
		select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

	select CONVERT(varchar(50),COUNT(esFormDetailId)) as FieldValue1 from tbl_EvalSerFormDetail where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc and evalBy = @v_fieldName4Vc and evalCount = @evalCount
end
else
begin
select CONVERT(varchar(50),COUNT(evalMemStatusId)) as FieldValue1 from tbl_EvalMemStatus where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc and evalBy = @v_fieldName4Vc
end
end


IF @v_fieldName1Vc='getAwardedContractsDetails'
BEGIN
	select	case when ttd.division <> '' then ttd.division else ttd.ministry end as FieldValue1,ttd.agency as FieldValue2,
			ttd.peOfficeName as FieldValue3,ttd.peCode as FieldValue4,ttd.peDistrict as FieldValue5,
			ttd.procurementNature as FieldValue6,ttd.reoiRfpRefNo as FieldValue7, ttd.procurementMethod as FieldValue8,
			sourceOfFund as FieldValue9,ttd.budgetType FieldValue10, devPartners as FieldValue11, 
			case when am.projectName <> '' then am.projectName else am.activityName end as FieldValue12,
			packageNo as FieldValue13,packageDescription as FieldValue14,
			ISNULL(REPLACE(CONVERT(VARCHAR(11), ttd.tenderPubDt, 106), ' ', '-') +' '+SUBSTRING(CONVERT(VARCHAR(5), 
			ttd.tenderPubDt,108), 1, 5), '-') as FieldValue15,
			ISNULL(REPLACE(CONVERT(VARCHAR(11), tnid.createdDt, 106), ' ', '-') , '-') as FieldValue16,
			REPLACE(CONVERT(VARCHAR(11),cs.contractSignDt, 106), ' ', '-') as FieldValue17,
			REPLACE(CONVERT(VARCHAR(11),ttls.completionTime, 106), ' ', '-') as FieldValue18,
			CONVERT(VARCHAR(50), tnid.contractAmt) as FieldValue19 , peDesignation as FieldValue20
	from	tbl_TenderDetails ttd,tbl_NoaIssueDetails tnid, tbl_TenderLotSecurity ttls,tbl_ContractSign cs, tbl_ContractNOARelation cr,
			tbl_AppMaster am, tbl_TenderMaster tm
	where	tnid.tenderId = ttd.tenderId and ttls.tenderId = ttd.tenderId and am.appId=tm.appId
			and ttd.tenderId=tm.tenderId
			and cr.noaid=tnid.noaissueid
			and ttd.tenderId = @v_fieldName2Vc and tnid.pkgLotId = @v_fieldName3Vc and tnid.userId=@v_fieldName4Vc
END
IF @v_fieldName1Vc='getAwardedContractsDetailsTwo'
BEGIN
	select distinct peName as FieldValue1, ttls.lotDesc as FieldValue2,
	dbo.f_getbiddercompany(tnid.userId) as FieldValue3,
	ttls.location as FieldValue4, CONVERT(VARCHAR(50), tnid.noaIssueId) as FieldValue5,
	apl.lotNo as FieldValue6, apl.lotDesc as FieldValue7
	from tbl_TenderDetails ttd,tbl_NoaIssueDetails tnid, tbl_TenderLotSecurity ttls, tbl_AppPkgLots apl
	--from tbl_TenderDetails ttd,tbl_NoaIssueDetails tnid, tbl_TenderLotSecurity ttls
	where tnid.tenderId = ttd.tenderId and ttls.tenderId = ttd.tenderId and apl.appPkgLotId=tnid.pkgLotId
	and ttd.tenderId = @v_fieldName2Vc and tnid.userId=@v_fieldName4Vc
END
IF @v_fieldName1Vc='getAwardedContractsDetailsThree'
BEGIN
	select CONVERT(VARCHAR(50), COUNT(userId)) as FieldValue1 from tbl_EvalBidderStatus where tenderId = @v_fieldName2Vc -- and pkgLotId =@v_fieldName3Vc
	and (bidderStatus = 'Technically Responsive' or result = 'pass')
	and  evalCount  = (select MAX(evalCount) from tbl_EvalBidderStatus where tenderId = @v_fieldName2Vc) 
END
IF @v_fieldName1Vc='getAwardedContractsDetailsFour'
BEGIN
	(select case when cm.companyId=1 then tm.address1 else cm.regOffAddress end as FieldValue1
	from tbl_TendererMaster tm, tbl_CompanyMaster cm ,tbl_NoaIssueDetails tnid
	where cm.companyId = tm.companyId and tnid.userId = tm.userId and tnid.noaissueid = tnid.noaissueid
	and tnid.tenderId = @v_fieldName2Vc and tnid.pkgLotId = @v_fieldName3Vc and tnid.userId=@v_fieldName4Vc
	)
END
IF @v_fieldName1Vc='chkAwardedContractsDueTime'
BEGIN
	select case when exists(select contractSignId from tbl_ContractSign
	where (CONVERT(char(10),lastDtContractDt,111)) >= (CONVERT(char(10),contractSignDt,111)) and noaid=@v_fieldName2Vc)
	then
	'yes'
	else
	'no'
	end as FieldValue1
END
IF @v_fieldName1Vc='chkAwardedContractsPerSec'
BEGIN
	select case when exists(
	select top 1 dtOfAction from tbl_TenderPayment t,tbl_NoaIssueDetails tnid,tbl_NoaAcceptance tna
	where paymentFor = 'performance security' and tnid.tenderId = @v_fieldName2Vc and tnid.pkgLotId = @v_fieldName3Vc
	and tnid.tenderId = t.tenderId and tnid.pkgLotId = t.pkgLotId
	and tna.noaIssueId = tnid.noaIssueId and tnid.tenderId = @v_fieldName2Vc and tnid.pkgLotId=@v_fieldName3Vc
	--and tnid.userId=@v_fieldName4Vc
	and tna.acceptRejStatus ='approved' and isLive = 'yes' and isVerified = 'yes'
	)
	then
		'yes'
	else
		'no'
	end as FieldValue1
END
IF @v_fieldName1Vc = 'getRoundsforEvaluationForLotId'
BEGIN
	if @v_fieldName3Vc = '0'
	begin
		select	convert(varchar(50),roundId) as FieldValue1,
				convert(varchar(50),userId) as FieldValue2,
				(	select	top 1 dbo.f_getbiddercompany(ee.userId)
					from	tbl_EvalRoundMaster ee
					where	ee.tenderId = @v_fieldName2Vc
							and ee.reportType='N1'
					order by ee.roundId desc
				) as FieldValue3
		from	tbl_EvalRoundMaster
		where	tenderId=@v_fieldName2Vc
				and reportType=@v_fieldName4Vc
	end
	else
	begin
		select	convert(varchar(50),roundId) as FieldValue1,
				convert(varchar(50),userId) as FieldValue2,
				dbo.f_getbiddercompany(userId) as FieldValue3
		from	tbl_EvalRoundMaster
		where	tenderId=@v_fieldName2Vc
				and pkgLotId=@v_fieldName3Vc
				and reportType=@v_fieldName4Vc
	end
END

--------------------------------------------------------------------
IF @v_fieldName1Vc = 'BidderRoundsforEvaluationChk'
BEGIN


	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
			SET @evalCount = 0
		else
			select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc
	if ((select count(tenderid) from tbl_TenderDetails where tenderId=@v_fieldName3Vc and procurementMethod in('DPM')) >=1  )
	begin
	set @v_fieldName4Vc='0'
	end
	If @v_fieldName4Vc!='0'
		Begin
			select case when (select COUNT(na.userId) from tbl_NoaIssueDetails nd inner join tbl_NoaAcceptance na on
			na.noaIssueId = nd.noaIssueId and acceptRejStatus in ('decline','Performance Security not paid')
			and roundId = @v_fieldName2Vc)=
			(select COUNT(userId) from tbl_EvalRoundMaster where
			roundId = @v_fieldName2Vc and evalCount = @evalCount)
			then '1'
			when (select COUNT(pq.userId) from tbl_PostQualification pq
			 where postQualStatus='disqualify' and tenderId=@v_fieldName3Vc and (pkgLotId=@v_fieldName4Vc or pkgLotId=0)
			 and userId In (select userId from tbl_EvalRoundMaster where
			roundId = @v_fieldName2Vc) and evalCount = @evalCount
			 )=
			(select COUNT(userId) from tbl_EvalRoundMaster where
			roundId = @v_fieldName2Vc and evalCount = @evalCount)
			then '1'
			 else '0' end as FieldValue1
		End
	Else
		Begin
			select case when (select COUNT(na.userId) from tbl_NoaIssueDetails nd inner join tbl_NoaAcceptance na on
			na.noaIssueId = nd.noaIssueId and acceptRejStatus in ('decline','Performance Security not paid')
			and roundId = @v_fieldName2Vc)=
			(select COUNT(userId) from tbl_EvalRoundMaster where
			roundId = @v_fieldName2Vc)
			then '1'
			when (select count(nn.userId) from tbl_Negotiation tn inner join tbl_NegNotifyTenderer nn on tn.negId = nn.negId
				where negStatus='Failed' and tenderId = @v_fieldName3Vc and userId IN (select userId from tbl_EvalRoundMaster where
			roundId = @v_fieldName2Vc))=
			(select COUNT(userId) from tbl_EvalRoundMaster where
			roundId = @v_fieldName2Vc)
			then '1'
			 else '0' end as FieldValue1
		End
END
IF @v_fieldName1Vc = 'PostDisqualifyCRDelete'
BEGIN
	declare @v_roundId int
	select @v_roundId = roundId from tbl_EvalRoundMaster where tenderId = @v_fieldName2Vc and pkgLotId =@v_fieldName3Vc and userId=@v_fieldName4Vc and reportType='L1'
	delete from tbl_BidderRank where roundId = @v_roundId
	delete from tbl_EvalRoundMaster where roundId = @v_roundId
	if (select count(roundId) from tbl_EvalRoundMaster where tenderId = @v_fieldName2Vc and pkgLotId =@v_fieldName3Vc and userId=@v_fieldName4Vc and reportType='T1L1')!=0
	Begin
		select @v_roundId = roundId from tbl_EvalRoundMaster where tenderId = @v_fieldName2Vc and pkgLotId =@v_fieldName3Vc and userId=@v_fieldName4Vc and reportType='T1L1'
		delete from tbl_BidderRank where roundId = @v_roundId
		delete from tbl_EvalRoundMaster where roundId = @v_roundId
	End
END
IF @v_fieldName1Vc = 'PostDisqualifyUsers'
BEGIN
	IF @v_fieldName5Vc !='true'
	BEGIN
		select CONVERT(varchar(20),userid) as FieldValue1 from tbl_PostQualification where postQualStatus='Disqualify' and  tenderid=@v_fieldName2Vc and pkgLotId in(select pkglotid  from tbl_ReportLots where reportId= @v_fieldName3Vc) and evalCount = @v_fieldName4Vc
	END
END
IF @v_fieldName1Vc = 'LowestAmtByRound'
BEGIN
	select convert(varchar(50),amount) as FieldValue1 from tbl_BidderRank where userId in (select userId from tbl_EvalRoundMaster where roundId=@v_fieldName2Vc)
	and roundId = @v_fieldName2Vc
END

IF @v_fieldName1Vc = 'getTscMemberCount'
BEGIN
	select CONVERT(varchar(20),COUNT(committeeId)) as FieldValue1 from tbl_Committee where committeeType = 'TSC' and committeStatus = 'Approved' and tenderId = @v_fieldName2Vc
END
IF @v_fieldName1Vc = 'PerfSecurityAvailChk'
BEGIN
	select case when (count(perCostLotId)!=0) then '1' else '0' end as FieldValue1
	 from tbl_TenderPerfSecurity where roundId=@v_fieldName2Vc
ENd
IF @v_fieldName1Vc = 'PerfSecurityNaoChk'
BEGIN
	select case when (count(noaIssueId)!=0) then '1' else '0' end as FieldValue1
	 from tbl_NoaIssueDetails where roundId=@v_fieldName2Vc
ENd
--it is repeated
--if @v_fieldName1Vc='getLinkForEditViewEvalTenderer'
--begin
--select CONVERT(varchar(50),COUNT(evalMemStatusId)) as FieldValue1 from tbl_EvalMemStatus where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc
--end

IF @v_fieldName1Vc = 'getBidderStatusView'
BEGIN
if (select procurementNature from tbl_TenderDetails where tenderId = @v_fieldName2Vc) = 'Services'
BEGIN
if (select eventType from tbl_TenderDetails where tenderId = @v_fieldName2Vc) = 'REOI'
Begin

select case when result = 'Pass' then 'Qualified' Else 'Disqualified' END as FieldValue1,convert(varchar(10),pkgLotId) as FieldValue2 from tbl_EvalBidderStatus where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc and pkgLotId =isnull(@v_fieldName4Vc,0) and evalCount = isnull(@v_fieldName5Vc,0)
End
Else
Begin
select result as FieldValue1,convert(varchar(10),pkgLotId) as FieldValue2 from tbl_EvalBidderStatus where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc and pkgLotId =isnull(@v_fieldName4Vc,0) and evalCount = isnull(@v_fieldName5Vc,0)
End

END
else
begin
select bidderStatus as FieldValue1,convert(varchar(10),pkgLotId) as FieldValue2 from tbl_EvalBidderStatus where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc and pkgLotId =isnull(@v_fieldName4Vc,0) and evalCount =@v_fieldName5Vc
end
end
IF @v_fieldName1Vc = 'getT1L1RoundWise'
BEGIN
	select convert(varchar(50),roundId) as FieldValue1,
	 convert(varchar(50),userId) as FieldValue2,
	 dbo.f_getbiddercompany(userId) as FieldValue3
	from tbl_EvalRoundMaster  where tenderId=@v_fieldName2Vc and reportId=0 and reportType=@v_fieldName3Vc
END

IF @v_fieldName1Vc = 'getDataForTenderer'
BEGIN
	select case when preBidStartDt is NULL
	THEN
		'yes'
	else
		'no'
	END as FieldValue1
from tbl_TenderDetails where tenderId = @v_fieldName2Vc
END

IF @v_fieldName1Vc = 'getDataForPQIssue'
BEGIN
	select convert(varchar(50),n.userId) as FieldValue1 from tbl_NoaIssueDetails n, tbl_NoaAcceptance na where
	na.noaIssueId = n.noaIssueId and na.acceptRejStatus = 'Approved' and
	tenderId = @v_fieldName2Vc and pkgLotId =@v_fieldName3Vc
	and Cast(FLOOR( CAST (perSecSubDt as float)) as Datetime) < Cast(FLOOR( CAST (GETDATE() as float)) as Datetime)
	order by roundId desc
END

IF @v_fieldName1Vc = 'getDataForPQIssueOne'
BEGIN
	select status as FieldValue1 from tbl_TenderPayment where tenderId = @v_fieldName2Vc and pkgLotId = @v_fieldName3Vc
	and paymentFor = 'Performance Security' and userId = @v_fieldName4Vc
END
IF @v_fieldName1Vc = 'getDataForPerSec'
BEGIN
	 if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
			SET @evalCount = 0
		else
			select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

	IF ((select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='Services' OR (select procurementMethod from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='DPM')
	Begin
			select dbo.f_getbiddercompany(userId) as FieldValue1 ,convert(varchar(50),roundId) as FieldValue2
		,convert(varchar(10),userId) as FieldValue3
		from tbl_EvalRoundMaster where tenderId = @v_fieldName2Vc and evalCount = @evalCount
		and reportType = 'N1'
		--and userId not in (select userId from tbl_PostQualification where tenderId = @v_fieldName2Vc and pkgLotId = @v_fieldName3Vc and postQualStatus = 'Disqualify')
	End
	Else
	Begin
		select dbo.f_getbiddercompany(userId) as FieldValue1 ,convert(varchar(50),roundId) as FieldValue2
		,convert(varchar(10),userId) as FieldValue3
		from tbl_EvalRoundMaster where tenderId = @v_fieldName2Vc and evalCount = @evalCount
		and reportType = 'L1' and pkgLotId = @v_fieldName3Vc and userId not in(select userId from tbl_PostQualification
		where tenderId = @v_fieldName2Vc and pkgLotId = @v_fieldName3Vc and postQualStatus = 'Disqualify' and evalCount = @evalCount)
	End

	--Declare @v_evalMId int, @v_rptType varchar(5)
 --  select @v_evalMId =  evalMethod
	--from tbl_ConfigEvalMethod cm ,tbl_TenderDetails td, tbl_TenderTypes tt
	--where cm.procurementMethodId=td.procurementMethodId and
	--cm.tenderTypeId=tt.tenderTypeId and tt.tenderType=td.eventType and cm.procurementNatureId=td.procurementNatureId
	--and tenderId=@v_fieldName2Vc

	--select @v_rptType =  Case when @v_evalMId = 1 then 'T1'
	--else
	--case when @v_evalMId = 2 then 'L1'
	--else 'T1L1'
	--end
	--end

	--select dbo.f_getbiddercompany(userId) as FieldValue1 ,convert(varchar(50),roundId) as FieldValue2
	--,convert(varchar(10),userId) as FieldValue3
	--from tbl_EvalRoundMaster where tenderId = @v_fieldName2Vc
	--and reportType = @v_rptType and userId not in(select userId from tbl_PostQualification
	--where tenderId = @v_fieldName2Vc and pkgLotId = @v_fieldName3Vc and postQualStatus = 'Disqualify')
END
IF @v_fieldName1Vc = 'chkMapTendererTender'
BEGIN
	select case when (
	select COUNT(tenderId) from tbl_TenderDetails where tenderId = @v_fieldName2Vc
	and procurementNature = 'Works' and procurementType = 'NCT' and procurementMethod = 'LTM'
	) != 0
	then
		'1'
	else
		'0'
	end as FieldValue1
END

IF @v_fieldName1Vc = 'chkSiteVisitDateNTime'
BEGIN
	declare @v_yesNo varchar(5)
	if (select count(postQaulId) from tbl_PostQualification where tenderId = @v_fieldName2Vc and siteVisitReqDt < GETDATE())  != 0
	begin
		select @v_yesNo = 'Yes'
	end
		else
		begin

		select @v_yesNo = 'No'
	END

	select @v_yesNo as FieldValue1 ,siteVisit as FieldValue2 from tbl_PostQualification where tenderId = @v_fieldName2Vc
END

IF @v_fieldName1Vc = 'getRegFeePayment_MISReport' --Change in SP As per requiremnt Change by darshan
BEGIN

	select @v_ConditionString_Vc=''
	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	IF @v_fieldName3Vc = '0'
	BEGIN
		Select @v_Page_inInt=1,@v_RecordPerPage_inInt=0
		Set @v_StartAt_Int = 1
		Set @v_StopAt_Int= 0
	END
	ELSE
	BEGIN
		Select @v_Page_inInt=@v_fieldName2Vc, @v_RecordPerPage_inInt=@v_fieldName3Vc

		Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
		Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	END
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */

	declare @v_userStatus varchar(50)
	select @v_userStatus = 'Approved'
	If @v_fieldName4Vc is not null And @v_fieldName4Vc<>'' -- Creiteris: Status
	Begin
		If @v_fieldName4Vc='New'
		Begin
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And RP.status=''paid'' And LM.validUpTo >= getdate()'
		End
		Else If @v_fieldName4Vc='Renew'
		Begin
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And RP.status=''renewed'' And LM.validUpTo >= getdate()'
		End
		Else If @v_fieldName4Vc='Expired'
		Begin
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And LM.status Not in (''incomplete'',''pending'') And LM.validUpTo < getdate() AND RP.status != ''freezed'''
		End
		Else If @v_fieldName4Vc='Disabled'
		Begin
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And RP.status=''Freezed'''
		End
	End


	IF (@v_fieldName5Vc is not null And @v_fieldName5Vc <>'') And (@v_fieldName6Vc is not null And @v_fieldName6Vc <>'')
	Begin
		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) >= ''' + @v_fieldName5Vc + ''' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) <= ''' + @v_fieldName6Vc + ''''
	End
	ELSE IF (@v_fieldName5Vc is not null And @v_fieldName5Vc <>'') And (@v_fieldName6Vc is null OR @v_fieldName6Vc='')
	Begin
		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) >= ''' + @v_fieldName5Vc + ''''
	End
	ELSE IF (@v_fieldName5Vc is not null Or @v_fieldName5Vc <>'') And (@v_fieldName6Vc is not null And @v_fieldName6Vc <>'')
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) <= ''' + @v_fieldName6Vc + ''''
	End

	If @v_fieldName7Vc is not null And @v_fieldName7Vc<>'' AND @v_fieldName7Vc <> 'NA' -- Creiteris: Email ID
	Begin
		if @v_fieldName9Vc = '='
		begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And LM.emailId ='''+@v_fieldName7Vc+''''
		end
		else
		begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And LM.emailId like ''%'+@v_fieldName7Vc+'%'''
		end
	End

	If @v_fieldName8Vc is not null And @v_fieldName8Vc<>'' AND @v_fieldName8Vc <> 'NA' -- Creiteris: Company Name
	Begin
	If @v_fieldName10Vc = '='
		begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And dbo.f_getbiddercompany(LM.userId) ='''+@v_fieldName8Vc+''''
		end
		else
		begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And dbo.f_getbiddercompany(LM.userId) like ''%'+@v_fieldName8Vc+'%'''
		end
	End

	IF(@v_fieldName7Vc is not null And @v_fieldName7Vc<>'' AND @v_fieldName7Vc = 'NA') OR (@v_fieldName8Vc is not null And @v_fieldName8Vc<>'' AND @v_fieldName8Vc = 'NA')
	BEGIN
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And LM.emailId IS NULL'
	END

	If @v_fieldName11Vc is not null And @v_fieldName11Vc<>'' -- Creiteris: Bank Name
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And RP.bankName ='''+@v_fieldName11Vc+''''
	End

	If @v_fieldName12Vc is not null And @v_fieldName12Vc<>'' -- Creiteris: Branch Name
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And RP.branchName ='''+@v_fieldName12Vc+''''
	End
	If @v_fieldName13Vc is not null And @v_fieldName13Vc<>'' -- Creiteris: Company Name
	Begin
		IF @v_fieldName13Vc = 'Online'
		Begin
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And RP.paymentInstType =''Online'''
		End
		IF @v_fieldName13Vc = 'Offline'
		Begin
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And RP.paymentInstType !=''Online'''
		End

	End
	--Print(@v_ConditionString_Vc)

	-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT

	Select @v_CntQry_Vc='SELECT Count(Distinct regPaymentId)
	From tbl_RegFeePayment RP
		LEFT join tbl_LoginMaster LM On RP.userId=LM.userId
				AND userTyperId = 2
				AND registrationType != ''Media''
		LEFT join tbl_TendererMaster TM On TM.userId=RP.userId
		--Where   isLive=''yes''
		Where  1 = 1 ' + @v_ConditionString_Vc

	IF	@v_StopAt_Int= 0 AND @v_RecordPerPage_inInt = 0
	BEGIN
		DECLARE	@tbl_Reccount TABLE
			(
				recCount INT
			)
			INSERT INTO @tbl_Reccount (recCount)
			EXEC   (@v_CntQry_Vc)
		SELECT @v_StopAt_Int = recCount ,@v_RecordPerPage_inInt = recCount FROM	 @tbl_Reccount
	END
	Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT( Distinct regPaymentId ) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')
	From tbl_RegFeePayment RP
		LEFT join tbl_LoginMaster LM On RP.userId=LM.userId
				AND userTyperId = 2
					AND registrationType != ''Media''
		LEFT join tbl_TendererMaster TM On TM.userId=RP.userId
		--Where   isLive=''yes''
		Where  1 = 1 ' + @v_ConditionString_Vc

	--PRINT 	'1'+@v_PgCntQry_Vc
	--PRINT 	'2'+@v_CntQry_Vc

			/* START CODE: DYNAMIC QUERY TO GET RECORDS */

			/*
				FieldValue1: Payment Id
				FieldValue2: User Id of Bidder for which Payment is done
				FieldValue3: Email Id of Bidder for which Payment is done
				FieldValue4: Branch Maker - Name of Bank User who has done the Payment
				FieldValue5: Payment Status New Payment/Renewal
				FieldValue6: Date of Payment
			*/


		Select @v_FinalQueryVc=
	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int,@v_status_str VARCHAR(10);
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
		Set @v_status_str = ('''+@v_fieldName4Vc+''');

	Select FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6,FieldValue7, FieldValue8, FieldValue9, FieldValue10, FieldValue11, FieldValue12,FieldValue14,FieldValue15
	From
	(Select distinct Convert(varchar(50), RP.regPaymentId) as FieldValue1,
	Convert(varchar(50), RP.userId) as FieldValue2,
	CASE LM.registrationType
		When ''individualconsultant'' Then ''Individual Consultant''
		When ''govtundertaking'' Then ''Government owned Enterprise''
		--When ''media'' Then ''Media''
		When ''contractor'' Then ''Tenderer / Consultant''
		Else ''Tenderer / Consultant''
		End as FieldValue3,
--	Case RP.status
--		When ''paid'' Then ''New''
--		When ''renewed'' Then ''Renewal''
--		When ''freezed'' Then ''Disabled''
--		End as FieldValue4,
	Case WHEN LM.validUpTo < GETDATE() AND RP.status != ''freezed'' THEN ''Expired''
		ELSE
		CASE RP.status
			When ''paid'' Then ''New''
			When ''renewed'' Then ''Renewal''
			When ''freezed'' Then ''Disabled''
		End
	END as FieldValue4,
	Case
		WHEN RP.paymentInstType = ''Online'' AND RP.status <> ''freezed''  THEN ''NA''
		WHEN RP.status = ''freezed'' THEN ''Freezed''
		When RP.isVerified = ''yes'' Then ''Verified''
		When RP.isVerified = ''no'' Then ''Paid''
		End as FieldValue14,
	IsNUll(dbo.f_getbiddercompany(RP.userId),''NA'') as FieldValue5,
	ISNULL(LM.emailId,''NA'') as FieldValue6,
	ISNULL(TM.country,''NA'') as FieldValue7,
	ISNULL(TM.state,''NA'') as FieldValue8,
	REPLACE(CONVERT(VARCHAR(11),RP.dtOfPayment, 106), '' '', ''-'')+ '' '' + Substring(CONVERT(VARCHAR(30),RP.dtOfPayment,108),1,5) as FieldValue9,
	Case Currency
		When ''BDT''
			Then ''Taka'' + '' '' + CONVERT(varchar(50),RP.amount)
		When ''USD''
			Then ''$'' + '' '' + CONVERT(varchar(50),RP.amount)
			End as FieldValue10,
	CONVERT(varchar(50),@v_TotalPageCnt_Int) as FieldValue11,
	CONVERT(varchar(50),@v_TotalRecordCnt_Int) as FieldValue12,
	CONVERT(varchar(50),Tmp.RowNumber) as FieldValue13,
	CASE WHEN RP.paymentInstType != ''Online'' THEN ''Offline'' ELSE RP.paymentInstType END AS FieldValue15,
	Tmp.regPaymentId
		From
		(Select RowNumber, regPaymentId From
			(
				Select ROW_NUMBER() Over (Order by regPaymentId desc) as RowNumber, regPaymentId From
				(
					SELECT distinct RP.regPaymentId
						From tbl_RegFeePayment RP
						LEFT join tbl_LoginMaster LM On RP.userId=LM.userId
							AND userTyperId = 2
					AND registrationType != ''Media''
						LEFT join tbl_TendererMaster TM On TM.userId=RP.userId
						--Where  isLive=''yes''
						Where  1 = 1'
						+@v_ConditionString_Vc+
				') as InnerTbl
			) B
			WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CASE WHEN @v_StopAt_Int = '0' THEN @v_CntQry_Vc ELSE CONVERT(varchar(50), @v_StopAt_Int) END
		+ ' ) Tmp
		Inner Join tbl_RegFeePayment RP On Tmp.regPaymentId=RP.regPaymentId
			LEFT join tbl_LoginMaster LM On RP.userId=LM.userId
					AND userTyperId = 2
					AND registrationType != ''Media''
			LEFT join tbl_TendererMaster TM On TM.userId=RP.userId
		) as A Order by A.regPaymentId desc
		'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */


		PRINT(@v_FinalQueryVc)
		Exec (@v_FinalQueryVc)

END

IF @v_fieldName1Vc = 'getPaymentReportTotal'
BEGIN
    select @v_ConditionString_Vc=''
    select @v_userStatus = 'Approved'
	If @v_fieldName4Vc is not null And @v_fieldName4Vc<>'' -- Creiteris: Status
	Begin
		If @v_fieldName4Vc='New'
		Begin
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And RP.status=''paid'' And LM.validUpTo >= getdate()'
		End
		Else If @v_fieldName4Vc='Renew'
		Begin
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And RP.status=''renewed'' And LM.validUpTo >= getdate()'
		End
		Else If @v_fieldName4Vc='Expired'
		Begin
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And LM.status Not in (''incomplete'',''pending'') And LM.validUpTo < getdate() AND RP.status != ''freezed'''
		End
		Else If @v_fieldName4Vc='Disabled'
		Begin
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And RP.status=''Freezed'''
		End
	End

    IF (@v_fieldName5Vc is not null And @v_fieldName5Vc <>'') And (@v_fieldName6Vc is not null And @v_fieldName6Vc <>'')
    Begin
        Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) >= ''' + @v_fieldName5Vc + ''' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) <= ''' + @v_fieldName6Vc + ''''
    End
    ELSE IF (@v_fieldName5Vc is not null And @v_fieldName5Vc <>'') And (@v_fieldName6Vc is null OR @v_fieldName6Vc='')
    Begin
        Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) >= ''' + @v_fieldName5Vc + ''''
    End
    ELSE IF (@v_fieldName5Vc is not null Or @v_fieldName5Vc <>'') And (@v_fieldName6Vc is not null And @v_fieldName6Vc <>'')
    Begin
        Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) <= ''' + @v_fieldName6Vc + ''''
    End
    If @v_fieldName7Vc is not null And @v_fieldName7Vc<>'' AND @v_fieldName7Vc <> 'NA' -- Creiteris: Email ID
    Begin
        If @v_fieldName9Vc = '='
        begin
            Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And LM.emailId ='''+@v_fieldName7Vc+''''
        end
        else
        begin
            Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And LM.emailId like ''%'+@v_fieldName7Vc+'%'''
        end
    End

    If @v_fieldName8Vc is not null And @v_fieldName8Vc<>'' AND @v_fieldName8Vc <> 'NA' -- Creiteris: Company Name
    Begin
        If @v_fieldName10Vc = '='
        begin
            Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And dbo.f_getbiddercompany(LM.userId) ='''+@v_fieldName8Vc+''''
        end
        else
        begin
            Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And dbo.f_getbiddercompany(LM.userId) like ''%'+@v_fieldName8Vc+'%'''
        end
    End

    IF(@v_fieldName7Vc is not null And @v_fieldName7Vc<>'' AND @v_fieldName7Vc = 'NA') OR (@v_fieldName8Vc is not null And @v_fieldName8Vc<>'' AND @v_fieldName8Vc = 'NA')
    BEGIN
        Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And LM.emailId IS NULL'
    END
    If @v_fieldName11Vc is not null And @v_fieldName11Vc<>'' -- Creiteris: Bank Name
    Begin
        Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And RP.bankName ='''+@v_fieldName11Vc+''''
    End
    If @v_fieldName12Vc is not null And @v_fieldName12Vc<>'' -- Creiteris: Branch Name
    Begin
        Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And RP.branchName ='''+@v_fieldName12Vc+''''
    End
    If @v_fieldName13Vc is not null And @v_fieldName13Vc<>'' -- Creiteris: Company Name
    Begin
        IF @v_fieldName13Vc = 'Online'
        Begin
           Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And RP.paymentInstType =''Online'''
        End
        IF @v_fieldName13Vc = 'Offline'
        Begin
           Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And RP.paymentInstType !=''Online'''
        End
    End
    If @v_fieldName2Vc is not null And @v_fieldName2Vc<>'' -- Creiteris: verified and paid status
    Begin
        IF @v_fieldName2Vc = 'paid'
        Begin
           Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And RP.isVerified =''no'''
        End
        IF @v_fieldName2Vc = 'verified'
        Begin
           Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And RP.isVerified =''yes'''
        End
    End

    Select @v_FinalQueryVc= '   select  currency as FieldValue1,
                                        CONVERT(varchar(50),SUM(amount)) as FieldValue2
                                from    tbl_RegFeePayment RP
                                        LEFT join tbl_LoginMaster LM On RP.userId=LM.userId
                                        LEFT join tbl_TendererMaster TM On TM.userId=RP.userId
                               Where    1=1'  + @v_ConditionString_Vc
                            + ' group by currency '
    print(@v_FinalQueryVc)
    exec (@v_FinalQueryVc)
END

IF @v_fieldName1Vc = 'chkWorkFlowStatusForCancel'
BEGIN
	select case when ((select COUNT(canTenderId) from tbl_CancelTenderRequest
		where tenderId = @v_fieldName2Vc and workflowStatus in ('Cancelled', 'Approved')) != 0)
	THEN
		'Yes'
	ELSE
		'No'
	END as FieldValue1
END

IF @v_fieldName1Vc = 'reportTORTERChk'
BEGIN
		select
		Case When COUNT(torSignId) <> 0 Then '1' Else '0' End as FieldValue1
		from tbl_TORRptSign where tenderId=@v_fieldName2Vc and reportType=''+@v_fieldName3Vc+''
END
IF @v_fieldName1Vc = 'checkNegStartPoint'
BEGIN
	Declare @v_evalMethod int
	select @v_evalMethod = evalMethod from tbl_ConfigEvalMethod cm ,tbl_TenderDetails td, tbl_TenderTypes tt where cm.procurementMethodId=td.procurementMethodId and cm.tenderTypeId=tt.tenderTypeId and tt.tenderType=td.eventType and cm.procurementNatureId=td.procurementNatureId and tenderId=@v_fieldName3Vc

	IF @v_evalMethod=1
	BEGIN
		select case when (select count(nn.userId) from tbl_Negotiation tn inner join tbl_NegNotifyTenderer nn on tn.negId = nn.negId
				where negStatus='Failed' and tenderId = @v_fieldName3Vc)
				=
				(select count(br.userId) from tbl_EvalRoundMaster er inner join tbl_BidderRank br on er.roundId = br.roundId where er.tenderId=@v_fieldName3Vc and reportType='T1')
		Then
			'1'
		Else
			'0'
		End	as FieldValue1
	End
	Else IF @v_evalMethod=2
	BEGIN
		select case when (select count(nn.userId) from tbl_Negotiation tn inner join tbl_NegNotifyTenderer nn on tn.negId = nn.negId
				where negStatus='Failed' and tenderId = @v_fieldName3Vc)
				=
				(select count(br.userId) from tbl_BidderRank br where br.roundId in (select top 1 er.roundId from tbl_EvalRoundMaster er where er.tenderId=@v_fieldName3Vc and reportType='L1' order by roundId asc))
		Then
			'1'
		Else
			'0'
		End	as FieldValue1
	End
	Else
	BEGIN
		select case when (select count(nn.userId) from tbl_Negotiation tn inner join tbl_NegNotifyTenderer nn on tn.negId = nn.negId
				where negStatus='Failed' and tenderId = @v_fieldName3Vc)
				=
				(select count(br.userId) from tbl_EvalRoundMaster er inner join tbl_BidderRank br on er.roundId = br.roundId where er.tenderId=@v_fieldName3Vc and reportType='T1L1')
		Then
			'1'
		Else
			'0'
		End	as FieldValue1
	End
	--IF (select COUNT(roundId) from tbl_EvalRoundMaster where roundId > convert(int,@v_fieldName2Vc)and reportType='L1'and tenderId=@v_fieldName3Vc)!=0
	--	BEgin
	--		select top 1 @v_fieldName2Vc=roundId from tbl_EvalRoundMaster where roundId > convert(int,@v_fieldName2Vc)and reportType='L1'and tenderId=@v_fieldName3Vc order by roundId desc
	--	END
	--select case when (select count(nn.userId) from tbl_Negotiation tn inner join tbl_NegNotifyTenderer nn on tn.negId = nn.negId
	--			where negStatus='Failed' and tenderId = @v_fieldName3Vc and userId IN (select userId from tbl_EvalRoundMaster where
	--		roundId = @v_fieldName2Vc))=
	--		(select COUNT(userId) from tbl_EvalRoundMaster where
	--		roundId = @v_fieldName2Vc)
	--		then '1'
	--		 else '0' end as FieldValue1
End

-----For get all online payment bank for User Registration--------------------------------------
IF @v_fieldName1Vc = 'getOnlinePaymentBankForUserReport'
BEGIN
	select distinct bankname as FieldValue1 from tbl_RegFeePayment where paymentInstType ='Online'
End
---Get SubContracting Bidders of from NOA issue Details Id
IF @v_fieldName1Vc = 'getSubContractor'
BEGIN
	select dbo.f_getbiddercompany(tsc.invToUserId) as FieldValue1 from tbl_NoaIssueDetails nid
		inner join tbl_SubContracting tsc on tsc.tenderId = nid.tenderId and tsc.invFromUserId = nid.userId
	where nid.noaIssueId=@v_fieldName2Vc and tsc.invAcceptStatus = 'Approved'
End

---When aa approves evalreport then mail to sucessfully tenderer case of reoi
IF @v_fieldName1Vc = 'getSucTenderEvalRpt'
Begin
	select lm.emailId as FieldValue1,td.tenderBrief as FieldValue2,
	td.peOfficeName as FieldValue3,td.eventType as FieldValue4
	from tbl_EvalBidderStatus ebs inner join tbl_TenderDetails td on td.tenderId = ebs.tenderId
	inner join tbl_LoginMaster lm on lm.userId = ebs.userId where ebs.tenderId = @v_fieldName2Vc
	and ebs.result = 'pass' and ebs.evalStatus = 'evaluation'
End

IF @v_fieldName1Vc = 'checkForReTenderOrReject'
Begin
Declare @v_sealbid int
select @v_sealbid = count(userId) from tbl_BidderRank where roundId in (select top 1 roundId from
		tbl_EvalRoundMaster where tenderId=@v_fieldName2Vc and reportType='L1' order by roundId asc)
	IF @v_sealbid=0
	Begin
		select '0' as FieldValue1
	End
	Else
	Begin
		select case when
	  (@v_sealbid)
		=
		(select count(na.userId) from tbl_NoaIssueDetails n inner join tbl_NoaAcceptance na on n.noaIssueId = na.noaIssueId
			where na.acceptRejStatus in ('decline','Performance Security not paid') and tenderId=@v_fieldName2Vc)
	     then '1'
	     Else
			  '0'
	     End as FieldValue1
	End
End
IF @v_fieldName1Vc='getReviewReportsList'
BEGIN
	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName3Vc, @v_RecordPerPage_inInt=@v_fieldName4Vc

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)	;
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */

	/*Start: Dynamic Condition string*/
	select @v_ConditionString_Vc=''

	If @v_fieldName5Vc is not null And @v_fieldName5Vc<>'' --  Tender Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' AND TD.tenderId='+@v_fieldName5Vc
	End

	If @v_fieldName6Vc is not null And @v_fieldName6Vc<>'' --  Tender Ref. No.
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' AND TD.reoiRfpRefNo=''' + @v_fieldName6Vc + ''''
	End
	SELECT @FinalQuery='
	 WITH CTE1 AS (	SELECT	*
					FROM (
							SELECT  CONVERT(VARCHAR(50), TD.tenderId) AS FieldValue1,
									tenderBrief AS FieldValue2,
									CASE WHEN TD.agency = '''' AND TD.division = '''' THEN TD.ministry WHEN TD.agency = '''' THEN TD.division ELSE TD.agency END AS FieldValue3,
									peOfficeName AS FieldValue4,
									TD.reoiRfpRefNo AS FieldValue5,
									CONVERT(VARCHAR(50),ER.evalFFrptId) AS FieldValue9,
									CONVERT(VARCHAR(50),ER.pkglotid) AS FieldValue10,
									CONVERT(VARCHAR(50),ER.roundid) AS FieldValue11,
									ER.rptStatus AS FieldValue12,
									ROW_NUMBER() Over (Order by max(sentDate) desc) AS ROWNUMBER
							FROM	tbl_tenderdetails TD
									INNER JOIN tbl_EvalRptForwardToAA ER ON TD.tenderId=ER.tenderId
							WHERE	ER.actionuserId=' + @v_fieldName2Vc +
									+@v_ConditionString_Vc+
							' GROUP BY td.tenderId, td.tenderBrief, CASE WHEN TD.agency = '''' AND TD.division = '''' THEN TD.ministry WHEN TD.agency = '''' THEN TD.division ELSE TD.agency END ,td.peOfficeName, reoiRfpRefNo,ER.evalFFrptId,ER.pkglotid,ER.roundid,ER.rptStatus
						) Tmp
					)

	SELECT	*,
			CONVERT(VARCHAR(50), RowNumber) AS FieldValue6,
			(SELECT CONVERT(VARCHAR(50), COUNT(*))  from CTE1) AS FieldValue7,
			CONVERT(VARCHAR(50),CEILING(CAST((SELECT COUNT(*)  FROM CTE1) AS float)/' + CONVERT(VARCHAR(50),@v_RecordPerPage_inInt) + ')) AS FieldValue8
	FROM	CTE1
	WHERE	RowNumber BETWEEN  ' + CONVERT(VARCHAR(50), @v_StartAt_Int) + '  AND  ' + CONVERT(VARCHAR(50), @v_StopAt_Int) +
	' ORDER BY RowNumber'
	PRINT (@FinalQuery)
	EXEC (@FinalQuery)
END
If @v_fieldName1Vc='getTenderHopeUser'
BEGIN
	select	convert(varchar(20),userId) as FieldValue1
	from	tbl_TenderDetails t,
			tbl_EmployeeOffices e,
			tbl_EmployeeRoles em,
			tbl_EmployeeMaster epm
	where	t.officeId=e.officeId
			and e.employeeId=em.employeeId
			and procurementRoleId=6
			and e.employeeId=epm.employeeId
			and t.tenderId=@v_fieldName2Vc
			and userId=@v_fieldName3Vc
END
If @v_fieldName1Vc='getTenderSecratory'
BEGIN
	SELECT	CONVERT(VARCHAR(20),et.userId) AS FieldValue1
	FROM	tbl_EvalRptForwardToAA ef
			INNER JOIN tbl_EmployeeTrasfer et on et.govUserId = ef.actionGovUserId
			INNER JOIN tbl_EmployeeRoles ER on ER.employeeId = et.employeeId
			INNER JOIN tbl_ProcurementRole PR on pr.procurementRoleId = ER.procurementRoleId
						AND pr.procurementRole = @v_fieldName4Vc
	WHERE tenderId = @v_fieldName2Vc
/*	SELECT	convert(varchar(20),userId) as FieldValue1
	FROM	tbl_TenderDetails t,
			tbl_EmployeeOffices e,
			tbl_EmployeeRoles em,
			tbl_EmployeeMaster epm
	WHERE	t.officeId=e.officeId
			AND e.employeeId=em.employeeId
			AND procurementRoleId=2
			AND e.employeeId=epm.employeeId
			AND t.tenderId=@v_fieldName2Vc
			AND userId=@v_fieldName3Vc
*/
END
If @v_fieldName1Vc='getTenderAAMinister'
BEGIN
	SELECT	CONVERT(VARCHAR(5),er.procurementRoleId) as FieldValue1,
			CONVERT(VARCHAR(20),t.approvingAuthId) as FieldValue2
	FROM	tbl_TenderDetails t,
            tbl_EmployeeMaster em,
            tbl_EmployeeRoles er
	WHERE	t.approvingAuthId = em.userId
            AND em.employeeId = er.employeeId
            AND procurementRoleId=3
            AND t.tenderId=@v_fieldName2Vc
END
IF @v_fieldName1Vc='getTECMemberRole'
BEGIN
	SELECT	convert(varchar(50),COUNT(cm.userId))  as FieldValue1
	FROM	tbl_Committee c
			INNER JOIN tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
						AND cm.userId=@v_fieldName3Vc
						AND cm.memberRole IN (@v_fieldName4Vc)
	WHERE	c.tenderId=@v_fieldName2Vc
			AND c.committeStatus='approved'
			AND c.committeeType in ('TEC','PEC')
END

IF @v_fieldName1Vc='getSendMAILForMinisterApprove'
BEGIN
    DECLARE	@v_ApproveMailTable TABLE
    (
        emailID VARCHAR(100)
    )

    INSERT INTO @v_ApproveMailTable
    SELECT	emailid
    FROM	tbl_LoginMaster lm
            INNER JOIN tbl_EmployeeMaster em ON em.userId = lm.userId
            INNER JOIN tbl_AppMaster am ON am.employeeId = em.employeeId
            INNER JOIN tbl_TenderMaster tm ON tm.appId = am.appId
                        AND tm.tenderId = @v_fieldName2Vc

    INSERT INTO @v_ApproveMailTable
    SELECT	emailid
    FROM	tbl_LoginMaster lm
            INNER JOIN tbl_CommitteeMembers cm ON cm.userId = lm.userId
            INNER JOIN tbl_Committee c ON c.committeeId = cm.committeeId
                        AND c.committeeType in('TEC','PEC')
                        AND c.tenderId = @v_fieldName2Vc

    INSERT INTO @v_ApproveMailTable
    SELECT	emailid
    FROM	tbl_loginmaster lm
                    INNER JOIN tbl_EvalRptForwardToAA ef ON ef.actionUserId = lm.userId
                                AND ef.actionUserRole = 'Secretary'
                                AND ef.rptStatus = 'Forward'
                                AND tenderId =@v_fieldName2Vc

    INSERT INTO @v_ApproveMailTable
    SELECT	emailid
    FROM	tbl_EmployeeMaster e,
            tbl_loginmaster l
    WHERE	l.userid=e.userid
                    AND employeeId IN(  SELECT  employeeId
                                        FROM    tbl_EmployeeRoles
                                        WHERE   departmentId = (SELECT departmentId
                                                                FROM    tbl_TenderDetails
                                                                WHERE   tenderId=@v_fieldName2Vc
                                                                    )
                    AND procurementRoleId =(SELECT	procurementRoleId
                                            FROM	tbl_ProcurementRole
                                            WHERE	procurementRole='HOPE'
                                            )
                                    )
    SELECT	emailID AS FieldValue1
    FROM	@v_ApproveMailTable
END

IF @v_fieldName1Vc='getSendMAILForMinisterReject'
BEGIN
    DECLARE	@v_RejectMailTable TABLE
    (
            emailID	VARCHAR(100)
    )

    INSERT INTO @v_RejectMailTable
    SELECT	emailid
    FROM	tbl_LoginMaster lm
                    INNER JOIN tbl_EmployeeMaster em ON em.userId = lm.userId
                    INNER JOIN tbl_AppMaster am ON am.employeeId = em.employeeId
                    INNER JOIN tbl_TenderMaster tm ON tm.appId = am.appId
                                AND tm.tenderId = @v_fieldName2Vc

    INSERT INTO @v_RejectMailTable
    SELECT	emailid
    FROM	tbl_LoginMaster lm
                    INNER JOIN tbl_CommitteeMembers cm ON cm.userId = lm.userId
                                AND cm.memberRole='cp'
                    INNER JOIN tbl_Committee c ON c.committeeId = cm.committeeId
                                AND c.committeeType in('TEC','PEC')
                                AND c.tenderId = @v_fieldName2Vc

    INSERT INTO @v_RejectMailTable
    SELECT	emailid
    FROM	tbl_loginmaster lm
                    INNER JOIN tbl_EvalRptForwardToAA ef ON ef.actionUserId = lm.userId
                                AND ef.actionUserRole = 'Secretary'
                                AND ef.rptStatus = 'Forward'
                                AND tenderId =@v_fieldName2Vc

    INSERT INTO @v_RejectMailTable
    SELECT	emailid
    FROM	tbl_EmployeeMaster e,
            tbl_loginmaster l
    WHERE	l.userid=e.userid
                    AND employeeId IN(  SELECT  employeeId
                                        FROM    tbl_EmployeeRoles
                                        WHERE   departmentId = (SELECT  departmentId
                                                                FROM    tbl_TenderDetails
                                                                WHERE   tenderId=@v_fieldName2Vc
                                                                )
                    AND procurementRoleId =(SELECT	procurementRoleId
                                            FROM	tbl_ProcurementRole
                                            WHERE	procurementRole='HOPE'
                                            )
                                    )
    SELECT	emailID AS FieldValue1
    FROM	@v_RejectMailTable
END

-- Dohatec Start : New Added

-- For getting all the currency during Performance Security Payment
IF @v_fieldName1Vc = 'getPerformanceSecurityPaymentDetail'
Begin
	SELECT
		CONVERT(VARCHAR(64), tps.perCostLotId) AS FieldValue1
	   ,CONVERT(VARCHAR(64), tps.tenderId) AS FieldValue2
	   ,CONVERT(VARCHAR(64), tps.pkgLotId) AS FieldValue3
	   ,CONVERT(VARCHAR(64), tps.perSecurityAmt) AS FieldValue4
	   ,CONVERT(VARCHAR(64), tps.createdBy) AS FieldValue5
	   ,CONVERT(VARCHAR(64), tps.createdDt) AS FieldValue6
	   ,CONVERT(VARCHAR(64), tps.roundId) AS FieldValue7
	   ,CONVERT(VARCHAR(64), tps.percentage) AS FieldValue8
	   ,CONVERT(VARCHAR(64), tps.lowestAmt) AS FieldValue9
	   ,CONVERT(VARCHAR(64), tps.currencyId) AS FieldValue10
       ,CONVERT(VARCHAR(64), cm.currencyName) AS FieldValue11
       ,CONVERT(VARCHAR(64), cm.currencyShortName) AS FieldValue12
       ,CONVERT(VARCHAR(64), cm.currencySymbol) AS FieldValue13
	FROM tbl_TenderPerfSecurity AS tps
	INNER JOIN tbl_CurrencyMaster AS cm ON tps.currencyId = cm.currencyId
	WHERE tps.tenderId = @v_fieldName2Vc AND tps.roundId = (SELECT MAX(roundId) FROM tbl_TenderPerfSecurity WHERE tenderId = @v_fieldName2Vc)
	--ORDER BY cm.currencyShortName ASC
End

-- For getting All Performance Security Payment ID for NOA
IF @v_fieldName1Vc = 'getPerSecPaymentIDDetail'
Begin
	SELECT
		CONVERT(VARCHAR(64), tp.tenderPaymentId)  AS FieldValue1
	FROM tbl_TenderPayment AS tp
		INNER JOIN tbl_TenderPaymentStatus AS tps
			ON tp.tenderPaymentId = tps.tenderPaymentId
	WHERE tp.tenderId = @v_fieldName2Vc
		AND tp.userId = @v_fieldName3Vc
		AND tp.paymentFor ='Performance Security'
		AND tp.isLive='YES'
End

-- For getting All New Performance Security Payment for Branch Maker
IF @v_fieldName1Vc = 'getNewPerSecDetail'
Begin
	SELECT
		CONVERT(VARCHAR(64), nbg.bgAmt) AS FieldValue1,
		CONVERT(VARCHAR(64), nbg.currencyName) AS FieldValue2,
		CONVERT(VARCHAR(64), cm.currencySymbol) AS FieldValue3
	FROM tbl_Cms_NewBankGuarnatee AS nbg
		INNER JOIN tbl_CurrencyMaster AS cm
			ON nbg.currencyName = cm.currencyShortName
    WHERE tenderId = @v_fieldName2Vc
		AND pkgLotId = @v_fieldName3Vc
		AND userId = @v_fieldName4Vc
End

-- For getting All New Performance Security Payment ID
IF @v_fieldName1Vc = 'getNewPerSecPaymentIDDetail'
Begin
	SELECT
		CONVERT(VARCHAR(64), tp.tenderPaymentId)  AS FieldValue1
	FROM tbl_TenderPayment AS tp
		INNER JOIN tbl_TenderPaymentStatus AS tps
			ON tp.tenderPaymentId = tps.tenderPaymentId
	WHERE tp.tenderId = @v_fieldName2Vc
		AND tp.pkgLotId = @v_fieldName3Vc
		AND tp.userId = @v_fieldName4Vc
		AND tp.paymentFor ='Bank Guarantee'
		AND tp.isLive='YES'
End

-- For getting WPDetailHistory
IF @v_fieldName1Vc = 'getWPDetailHistory'
Begin
	DECLARE @query VARCHAR(3000)
	SET @query = ''
	SELECT @query = @query +
			'SELECT CONVERT(VARCHAR(64), tcwd.wpSrNo) AS FieldValue1,
			 CONVERT(VARCHAR(64), tcwd.wpDescription) AS FieldValue2,
			 CONVERT(VARCHAR(64), tcwd.wpUom) AS FieldValue3,
			 CONVERT(VARCHAR(64), tcwd.wpQty) AS FieldValue4,
			 CONVERT(VARCHAR(64), tcwdh.wpEndDate, 106) AS FieldValue5,
			 CONVERT(VARCHAR(64), tcwd.groupId) AS FieldValue6,
			 CONVERT(VARCHAR(64), tcwdh.wpStartDate, 106) AS FieldValue7,
			 CONVERT(VARCHAR(64), tcwdh.wpNoOfDays) AS FieldValue8,
			 CONVERT(VARCHAR(64), tcwdh.wpRate) AS FieldValue9,
			 CONVERT(VARCHAR(64), tcwdh.createdDate, 113) AS FieldValue10,
			 CONVERT(VARCHAR(64), tcwdh.currencyName) AS FieldValue11,
			 CONVERT(VARCHAR(64), (ISNULL(tcwdh.supplierVat, 0.000) + ISNULL(tcwdh.InlandOthers, 0.000))) AS FieldValue12
			 FROM tbl_Cms_WpDetailHistory tcwdh, tbl_Cms_WpDetail tcwd
			 WHERE tcwd.wpId=tcwdh.wpId and tcwd.tenderTableId=tcwdh.tenderTableId AND
			 tcwd.wpRowId=tcwdh.wpRowId and tcwdh.wpId = ' + @v_fieldName2Vc + ' AND tcwdh.historyCount = ' + @v_fieldName3Vc + ' '
	IF @v_fieldName4Vc = 'goods'
	Begin
		SELECT @query = @query + ' AND tcwdh.amendmentFlag in (''original'',''originalupdate'')'
	End
	ELSE
	Begin
		SELECT @query = @query + ' AND tcwdh.amendmentFlag in (''originalbytenderer'',''originalupdate'') order BY tcwd.groupId '
	End

	Print(@query)

	execute(@query)

    /*
    FieldName1 = wpSrNo
    FieldName2 = wpDescription
    FieldName3 = wpUom
    FieldName4 = wpQty
    FieldName5 = wpEndDate
    FieldName6 = groupId
    FieldName7 = wpStartDate
    FieldName8 = wpNoOfDays
    FieldName9 = wpRate
    FieldName10 = createDate
    FieldName11 = currency Name
    FieldName12 = VAROthers
    */
End
-- Get all Bank Guarantee Ids for ICT

IF @v_fieldName1Vc = 'getBankGuaranteeIdsICT'
BEGIN
	DECLARE @tenderId VARCHAR(64)
	DECLARE @lotId VARCHAR(64)
	DECLARE @userId VARCHAR(64)

	SELECT @tenderId = tenderId, @lotId = pkgLotId, @userId = userId
	FROM tbl_Cms_NewBankGuarnatee
	WHERE bankGId = @v_fieldName2Vc

	SELECT CONVERT(VARCHAR(64), bankGId) AS FieldValue1
	FROM tbl_Cms_NewBankGuarnatee
	WHERE tenderId = @tenderId AND pkgLotId = @lotId AND userId = @userId AND paymentId = 0
END

IF @v_fieldName1Vc = 'getBankGuaranteeIdsICTBank'
BEGIN
	DECLARE @eTenderId VARCHAR(64)
	DECLARE @eLotId VARCHAR(64)
	DECLARE @eUserId VARCHAR(64)

	IF @v_fieldName2Vc = 'bgId'
	BEGIN
		SELECT @eTenderId = tenderId, @eLotId = pkgLotId, @eUserId = userId
		FROM tbl_Cms_NewBankGuarnatee WHERE bankGId = @v_fieldName3Vc
	END
	ELSE
	BEGIN
		SELECT TOP 1 @eTenderId = tenderId, @eLotId = pkgLotId, @eUserId = userId
		FROM tbl_Cms_NewBankGuarnatee WHERE tenderId = @v_fieldName3Vc
	END

	SELECT CONVERT(VARCHAR(64), bankGId) AS FieldValue1
	FROM tbl_Cms_NewBankGuarnatee
	WHERE tenderId = @eTenderId AND pkgLotId = @eLotId AND userId = @eUserId
END

-- Dohatec Start
IF @v_fieldName1Vc = 'getPe'
BEGIN
	 select emailId as FieldValue1 from tbl_TenderMaster t,tbl_AppMaster a,tbl_EmployeeMaster e,tbl_LoginMaster l
	 where tenderId=@v_fieldName2Vc and t.appId=a.appId
	 and a.employeeId=e.employeeId
	 and l.userId=e.userId
END

IF @v_fieldName1Vc = 'AllBidderDisqualified'
BEGIN
if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
	SET @evalCount = 0
 else
	select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc
	
SELECT CASE WHEN(SELECT COUNT(DISTINCT CONVERT(varchar(20),userid)) as FieldValue1
from tbl_PostQualification where postQualStatus='Disqualify' and
tenderid=@v_fieldName2Vc and evalCount = @evalCount) =
(SELECT COUNT (DISTINCT convert(varchar(20), tbl_FinalSubmission.userId)) as FieldValue1
FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
  and tenderid = @v_fieldName2Vc 
   and userId not in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and evalCount = @evalCount and
  ((isnull(result,'pass')='fail') or (isnull(bidderStatus,'Technically Responsive')='Technically Unresponsive')))
  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc))
 THEN '1' ELSE '0' END AS FieldValue1
 END

 IF @v_fieldName1Vc = 'AllBidderDisqualifiedAfterNOA'
BEGIN
if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
	SET @evalCount = 0
 else
	select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

Declare @totalCount varchar(50)
set @totalCount = (SELECT COUNT(DISTINCT CONVERT(varchar(20),userid)) as FieldValue1 
from tbl_PostQualification where postQualStatus='Disqualify' and 
tenderid=@v_fieldName2Vc and evalCount = @evalCount) + (SELECT COUNT(DISTINCT CONVERT(varchar(20),userid)) as FieldValue1 
from tbl_PostQualification where tenderid=@v_fieldName2Vc and evalCount = @evalCount and
noaStatus in ('rejected','Performance Security not paid') )

IF @totalCount  =
(SELECT COUNT (DISTINCT convert(varchar(20), tbl_FinalSubmission.userId)) as FieldValue1
FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
  and tenderid = @v_fieldName2Vc
   and userId not in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and evalCount = @evalCount and
  ((isnull(result,'pass')='fail') or (isnull(bidderStatus,'Technically Responsive')='Technically Unresponsive')))
  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc))
	Begin
		select '1' AS FieldValue1
	END
 ELSE
	Begin
		select '0' AS FieldValue1
	END

END
-- Dohatec End

-- Dohatec Start for eventum issue 4297 (JVCA Registration Expiry Problem)
IF @v_fieldName1Vc = 'getJVCAExpiryDate'
select CONVERT(VARCHAR(19),ISNULL(MIN(validUpTo),DATEADD(D,-1,GETDATE())),120)  AS FieldValue1 
from tbl_loginmaster 
WHERE userId IN (
SELECT userId FROM Tbl_Jvcapartners WHERE JVID IN 
(Select JVId FROM Tbl_JointVenture where newJVUserId = @v_fieldName2Vc))
-- Dohatec End for eventum issue 4297 (JVCA Registration Expiry Problem)

-- Dohatec Start for Get Report Sign Status
If @v_fieldName1Vc='getTenderReportSignStatus'
Begin	 		
	 
     DECLARE @v_CountCommitteeMembers int
		 DECLARE @v_CountSignMembers int    
		 
		 if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
			SET @evalCount = 0
		else
			select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc
    
		 If @v_fieldName5Vc='LTM'
			Begin 
			SET @v_CountCommitteeMembers=(select count(distinct userId) from tbl_Committee tc 
			inner join tbl_CommitteeMembers tcm on tc.committeeId = tcm.committeeId
			where tc.tenderId = @v_fieldName2Vc and tc.committeeType  = @v_fieldName3Vc)

			SET @v_CountSignMembers= (select count(distinct userId) from tbl_TORRptSign 
			where tenderId=@v_fieldName2Vc and reportType = @v_fieldName4Vc and roundId=@v_fieldName6Vc and evalCount = @evalCount )

				if(@v_CountCommitteeMembers=@v_CountSignMembers)
       				select 'Signed' AS FieldValue1
					else  	select '0' AS FieldValue1
		   END 
		 else 
			Begin
			  SET @v_CountCommitteeMembers=(select count(distinct userId) from tbl_Committee tc 
			  inner join tbl_CommitteeMembers tcm on tc.committeeId = tcm.committeeId
			  where tc.tenderId = @v_fieldName2Vc and tc.committeeType  = @v_fieldName3Vc)

			  SET @v_CountSignMembers= (select count(distinct userId) from tbl_TORRptSign 
			  where tenderId=@v_fieldName2Vc and reportType = @v_fieldName4Vc and roundId=@v_fieldName6Vc and evalCount = @evalCount)

			  if(@v_CountCommitteeMembers=@v_CountSignMembers)
       			select 'Signed' AS FieldValue1
			  else  select '0' AS FieldValue1
			END
          	
END
-- Dohatec End for Get Report Sign Status

-- Dohatec Start for Get Report Sign Status with Lot
If @v_fieldName1Vc='getTenderReportSignStatusWithLot'
Begin	 		
	 
     DECLARE @v_CountCommitteeMembersL int
		 DECLARE @v_CountSignMembersL int    
		 
		 if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
			SET @evalCount = 0
		else
			select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc
    
		 If @v_fieldName5Vc='LTM'
			Begin 
			SET @v_CountCommitteeMembersL=(select count(distinct userId) from tbl_Committee tc 
			inner join tbl_CommitteeMembers tcm on tc.committeeId = tcm.committeeId
			where tc.tenderId = @v_fieldName2Vc and tc.committeeType  = @v_fieldName3Vc)

			SET @v_CountSignMembersL= (select count(distinct userId) from tbl_TORRptSign 
			where tenderId=@v_fieldName2Vc and reportType = @v_fieldName4Vc and roundId=@v_fieldName6Vc and evalCount = @evalCount )

				if(@v_CountCommitteeMembersL=@v_CountSignMembersL)
       				select 'Signed' AS FieldValue1
					else  	select '0' AS FieldValue1
		   END 
		 else 
			Begin
			  SET @v_CountCommitteeMembersL=(select count(distinct userId) from tbl_Committee tc 
			  inner join tbl_CommitteeMembers tcm on tc.committeeId = tcm.committeeId
			  where tc.tenderId = @v_fieldName2Vc and tc.committeeType  = @v_fieldName3Vc)

			  SET @v_CountSignMembersL= (select count(distinct userId) from tbl_TORRptSign 
			  where tenderId=@v_fieldName2Vc and pkgLotId = @v_fieldName5Vc and reportType = @v_fieldName4Vc and roundId=@v_fieldName6Vc and evalCount = @evalCount)

			  if(@v_CountCommitteeMembersL=@v_CountSignMembersL)
       			select 'Signed' AS FieldValue1
			  else  select '0' AS FieldValue1
			END
          	
END
-- Dohatec End for Get Report Sign Status


--Code Start By Proshanto Kumar Saha,Dohatec
IF @v_fieldName1Vc = 'GetLotPackageDetailsWithoutContractId'
BEGIN
	IF (select procurementNatureId from tbl_TenderDetails where tenderId=@v_fieldName2Vc) = 3
	BEGIN
		select packageNo as FieldValue1,packageDescription as FieldValue2 from tbl_TenderDetails where tenderId = @v_fieldName2Vc
	END
	ELSE
	BEGIN
		select distinct ttd.packageNo as FieldValue1,ttd.packageDescription as FieldValue2, ttls.lotNo as FieldValue3,ttls.lotDesc as FieldValue4,CONVERT(VARCHAR(50), ttls.appPkgLotId) as FieldValue5
		from tbl_EvalRptSentToAA tersta , tbl_TenderLotSecurity ttls,
		tbl_TenderDetails ttd,tbl_NoaIssueDetails ns
		where ttls.tenderId = tersta.tenderId and ttls.appPkgLotId = tersta.pkgLotId and
		 ttls.appPkgLotId = ns.pkgLotId and
		---added for RO
		ns.isRepeatOrder='no' and
		-------------------------
		ttd.tenderId = ttls.tenderId and ttls.tenderId = @v_fieldName2Vc
	END

END
--Code End by Proshanto Kumar Saha,Dohatec


