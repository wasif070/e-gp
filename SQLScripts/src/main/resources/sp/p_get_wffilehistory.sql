USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_get_wffilehistory]    Script Date: 4/24/2016 11:02:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Get Work Flow File Information.
--
--
-- Author: Karan
-- Date: 09-10-2010
--
-- Last Modified:
-- Modified By: Karan
-- Date: 10-11-2010
-- Modification: Changes done for retriving data for Processed and Pending case
--------------------------------------------------------------------------------

/*

-- // SAMPLE TO EXECUTE THE PROCEDURE
exec p_get_wffilehistory 
@v_ViewType_inVc='processed',
@v_ActivityId_inInt =1, 
@v_ObjectId_inInt=1, 
@v_ChildId_inInt =1, 
@v_UserId_inInt =94,
@v_Page_inInt =1, 
@v_RecordPerPage_inInt =3

*/


ALTER PROCEDURE [dbo].[p_get_wffilehistory]

@v_ViewType_inVc varchar(50),
@v_ActivityId_inInt int=null,
@v_ObjectId_inInt int=null,
@v_ChildId_inInt int=null,
@v_UserId_inInt int=null,
@v_Page_inInt int =1,
@v_RecordPerPage_inInt int =10,
@v_columnNameVc varchar(20),
@v_orderTypeVc varchar(10),
@v_ModuleName varchar(50)=null,
@v_EventName varchar(50)=null,
@v_ProcessedBy varchar(400)=null,
@v_ObjectId int=null,
@v_FromProcessDate varchar(50)=null,
@v_FromProcessTo varchar(50)=null


AS

BEGIN
SET NOCOUNT ON;




DECLARE @v_StartAt_Int int, @v_StopAt_Int int,@v_TotalCnt_Int int, @v_Qry_Vc varchar(max),
@v_Condition_Vc varchar(4000), @v_CntQry_Vc varchar(4000), @v_Table_Vc varchar(max), @v_PgCntQry_Vc varchar(4000),@v_docColinVc varchar(500) 
/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */

	/* START CODE: TO SET WHERE CONDITION AND TABLE NAME */
	IF @v_ViewType_inVc='history'
	BEGIN
		if @v_ActivityId_inInt=1   
		begin
		set @v_ChildId_inInt=@v_ObjectId_inInt
		end
		if @v_ActivityId_inInt=4  
		begin
		Set @v_Condition_Vc= ' wf.activityId='+ CONVERT(varchar(50), @v_ActivityId_inInt) 
							   
							+ ' And objectId=' + CONVERT(varchar(50), @v_ObjectId_inInt) 
		end
		else
		Begin
		if @v_ActivityId_inInt=1   
		Begin
		Set @v_Condition_Vc= ' wf.activityId='+ CONVERT(varchar(50), @v_ActivityId_inInt) 
							+ ' And objectId=' + CONVERT(varchar(50), Case when @v_ObjectId_inInt=0 then @v_ObjectId else @v_ObjectId_inInt end)
		End
		else
		Begin
		Set @v_Condition_Vc= ' wf.activityId='+ CONVERT(varchar(50), @v_ActivityId_inInt) 
							+' And childId=' + CONVERT(varchar(50), @v_ChildId_inInt)  
							+ ' And objectId=' + CONVERT(varchar(50), Case when @v_ObjectId_inInt=0 then @v_ObjectId else @v_ObjectId_inInt end)
		End
		End					 
		Set @v_Table_Vc='tbl_WorkFlowFileHistory wf Inner Join (select activityid,eventid  from tbl_activitymaster) em On wf.activityid=em.activityid  '					
	END
	ELSE IF @v_ViewType_inVc='pkghistory'
	BEGIN
		 
		Set @v_Condition_Vc= ' wf.activityId='+ CONVERT(varchar(50), @v_ActivityId_inInt) 
							+' And childId=' + CONVERT(varchar(50), @v_ChildId_inInt)  
							+ ' And objectId=' + CONVERT(varchar(50), @v_ObjectId_inInt)
		 					 
		Set @v_Table_Vc='tbl_WorkFlowFileHistory wf Inner Join (select activityid,eventid  from tbl_activitymaster) em On wf.activityid=em.activityid  '					
	END
	ELSE IF @v_ViewType_inVc='processed'
	BEGIN
		Set @v_Condition_Vc='fromUserId='+ CONVERT(varchar(50), @v_UserId_inInt) 
		Set @v_Table_Vc='tbl_WorkFlowFileHistory wf Inner Join (select activityid,eventid  from tbl_activitymaster) em On wf.activityid=em.activityid  '	
	END
	ELSE IF @v_ViewType_inVc='pending'
	BEGIN
		Set @v_Condition_Vc='toUserId='+ CONVERT(varchar(50), @v_UserId_inInt) 
		Set @v_Table_Vc='tbl_WorkFlowFileOnHand wf Inner Join tbl_EventMaster em  On wf.eventId=em.eventId Inner Join tbl_ModuleMaster mm On wf.moduleId=mm.moduleId'	
	END
	ELSE  
	BEGIN
		--Set @v_Condition_Vc=' UserId='+ CONVERT(varchar(50), @v_UserId_inInt) + ' group by eventName, moduleName,wf.activityId,childId,objectId,em.eventId ,wf.actiondate ' 
		Set @v_Condition_Vc=' UserId='+ CONVERT(varchar(50), @v_UserId_inInt) 
		
		
		Set @v_Table_Vc='tbl_WorkFlowLevelConfig wf Inner Join tbl_EventMaster em  On wf.eventId=em.eventId Inner Join tbl_ModuleMaster mm On wf.moduleId=mm.moduleId'	
	END
	
	
	----------SEARCH CHANGES
	IF @v_ModuleName is not null And @v_ModuleName<>''
	BEGIN
		Set @v_Condition_Vc = @v_Condition_Vc + ' And moduleName=''' + @v_ModuleName + ''''
	END
	
	IF @v_EventName is not null And @v_EventName<>''
	BEGIN
		Set @v_Condition_Vc = @v_Condition_Vc + ' And eventName=''' + @v_EventName + ''''
	END
	
	IF @v_ProcessedBy is not null And @v_ProcessedBy<>''
	BEGIN
		IF @v_ViewType_inVc='processed'
			Set @v_Condition_Vc = @v_Condition_Vc + ' And fileSentto like ''%'+@v_ProcessedBy+'%'''
		ELSE IF @v_ViewType_inVc='pending'	
			Set @v_Condition_Vc = @v_Condition_Vc + ' And fileSentfrom like ''%'+@v_ProcessedBy+'%'''
	END
	
	IF @v_ObjectId is not null And @v_ObjectId<>''  And @v_ObjectId<>'0' 
	BEGIN
		Set @v_Condition_Vc = @v_Condition_Vc + ' And objectId=' + CONVERT(varchar(50), @v_ObjectId)
	END
	
	
	--IF (@v_FromProcessDate is not null And @v_FromProcessDate <>'') And (@v_FromProcessTo is not null And @v_FromProcessTo <>'')
	--Begin
	--	Select @v_Condition_Vc =@v_Condition_Vc + ' And Cast (Floor(Cast (processdate as Float)) as Datetime) >= ''' + @v_FromProcessDate + ''' And Cast (Floor(Cast (processdate as Float)) as Datetime) <= ''' + @v_FromProcessTo + ''''
	--End
	--ELSE IF (@v_FromProcessDate is not null And @v_FromProcessDate <>'') And (@v_FromProcessTo is null OR @v_FromProcessTo='')
	--Begin
	--	Select @v_Condition_Vc =@v_Condition_Vc + ' And Cast (Floor(Cast (processdate as Float)) as Datetime) >= ''' + @v_FromProcessDate + ''''
	--End
	--ELSE IF (@v_FromProcessDate is not null Or @v_FromProcessDate <>'') And (@v_FromProcessTo is not null And @v_FromProcessTo <>'')
	--Begin
	--	Select @v_Condition_Vc = @v_Condition_Vc + ' And Cast (Floor(Cast (processdate as Float)) as Datetime) <= ''' + @v_FromProcessTo + ''''
	--End
	
	
		IF (@v_FromProcessDate is not null And @v_FromProcessDate <>'') And (@v_FromProcessTo is not null And @v_FromProcessTo <>'')
	Begin
		Select @v_Condition_Vc =@v_Condition_Vc + ' And processdate >= ''' + @v_FromProcessDate + ''' And processdate <= ''' + @v_FromProcessTo + ''''
	End
	ELSE IF (@v_FromProcessDate is not null And @v_FromProcessDate <>'') And (@v_FromProcessTo is null OR @v_FromProcessTo='')
	Begin
		Select @v_Condition_Vc =@v_Condition_Vc + ' And processdate >= ''' + @v_FromProcessDate + ''''
	End
	ELSE IF (@v_FromProcessDate is not null Or @v_FromProcessDate <>'') And (@v_FromProcessTo is not null And @v_FromProcessTo <>'')
	Begin
		Select @v_Condition_Vc = @v_Condition_Vc + ' And processdate <= ''' + @v_FromProcessTo + ''''
	End
	
	/* END CODE: TO SET WHERE CONDITION AND TABLE NAME */
	 
	 
	 
	 If @v_ViewType_inVc = N'defaultworkflow'
	 Begin
		Set @v_Condition_Vc = @v_Condition_Vc + ' group by eventName, moduleName,wf.activityId,childId,objectId,em.eventId ,wf.actiondate ' 
	 End
	 
	print('Condition Query') 
	print(@v_Condition_Vc)
	 
	 IF @v_ViewType_inVc !='defaultworkflow'
	 BEGIN
	-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
			Set @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT(wf.activityId) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+') FROM '+@v_Table_Vc+' WHERE ' + @v_Condition_Vc
			Set @v_CntQry_Vc='SELECT COUNT(wf.activityId) FROM '+@v_Table_Vc+' WHERE ' + @v_Condition_Vc
	 END

	ELSE
	BEGIN
	
	/* STRAT CODE (SACHIN): CODE FOR GETTING COUNT OF TOTOAL RECORDS AND PAGES */
		DECLARE @cntDetail TABLE (activityid int)
		declare @v_tblInsert_vc varchar(max)
		--Set @v_tblInsert_vc = '	SELECT  activityId FROM '+@v_Table_Vc+' WHERE  UserId='+ CONVERT(varchar(50), @v_UserId_inInt) +' group by eventName, moduleName,wf.activityId,childId,objectId,em.eventId ,wf.actiondate '
		
		
		Set @v_tblInsert_vc = '	SELECT activityId FROM '+@v_Table_Vc+' WHERE '+ @v_Condition_Vc 
		
		
		insert into  @cntDetail  exec(@v_tblInsert_vc)
		Set @v_PgCntQry_Vc= (SELECT Ceiling(Cast(COUNT(activityId) as float)/Convert(varchar(50),@v_RecordPerPage_inInt)) FROM @cntDetail )
		Set @v_CntQry_Vc=(SELECT COUNT(activityId) FROM @cntDetail)
	
	/*END CODE (SACHIN)*/
END

If  @v_ViewType_inVc ='history' or @v_ViewType_inVc ='pkghistory'
Begin
set @v_docColinVc='convert(varchar(500),documentId) as docId,convert(varchar(10),wfHistoryId) as wfHistoryId'
End	
Else
Begin
set @v_docColinVc='convert(varchar(500),0) as docId,convert(varchar(10),0) as wfHistoryId' 
End 

 print @v_ViewType_inVc
	/* START CODE: DYNAMIC QUERY TO GET RECORDS */	
	 IF @v_ViewType_inVc !='defaultworkflow'
	 Begin
		Set @v_Qry_Vc=
		'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int; 
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
		SELECT  CONVERT(varchar(20),A.RowNumber) RowNumber, fromUserId, fileSentFrom, processDate, action, remarks, toUserId, fileSentTo, 
		eventName, moduleName, @v_TotalPageCnt_Int as TotalPages, @v_TotalRecordCnt_Int as totalRecords,activityId,childId,objectId,eventId,docId,wfHistoryId   FROM
		(SELECT Row_Number() Over (Order by '+@v_columnNameVc+ ' ' +@v_orderTypeVc+') as RowNumber, fromUserId, fileSentFrom, processDate,action,remarks, toUserId, fileSentTo, eventName, moduleName,wf.activityId,childId,objectId,em.eventId eventid,
		  '+@v_docColinVc+' 
		FROM '+@v_Table_Vc+' WHERE '+ @v_Condition_Vc +') as A
		WHERE A.RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) 
		+ ' And ' + CONVERT(varchar(50), @v_StopAt_Int) 
		+ ' Order By ' + @v_columnNameVc + ' ' +@v_orderTypeVc					
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */
	End
	Else
	Begin
	 
	--Set @v_Qry_Vc=
	--'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int; 
	--Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
	--Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
	--SELECT  CONVERT(varchar(20),A.RowNumber) RowNumber,   
	--eventName, moduleName, @v_TotalPageCnt_Int as TotalPages, @v_TotalRecordCnt_Int as totalRecords,activityId,childId,objectId,eventId, actiondate processdate   FROM
	--(SELECT Row_Number() Over (Order by actiondate ' +@v_orderTypeVc+') as RowNumber , eventName, moduleName,wf.activityId,childId,objectId,em.eventId eventid ,actiondate    
		IF @v_columnNameVc IS NULL OR @v_columnNameVc = 'processDate' 
		BEGIN
			SET	@v_columnNameVc = 'wf.actiondate'
		END
	
		Set @v_Qry_Vc= 	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int; 
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
		SELECT  CONVERT(varchar(20),A.RowNumber) RowNumber,   
		eventName, moduleName, @v_TotalPageCnt_Int as TotalPages, @v_TotalRecordCnt_Int as totalRecords,activityId,childId,objectId,eventId, actiondate, actiondate   FROM
		(SELECT Row_Number() Over (Order by '+@v_columnNameVc+ ' ' +@v_orderTypeVc+') as RowNumber , eventName, moduleName,wf.activityId,childId,objectId,em.eventId eventid ,wf.actiondate
		FROM '+@v_Table_Vc+' WHERE '+ @v_Condition_Vc +') as A
		WHERE A.RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) 
		+ ' And ' + CONVERT(varchar(50), @v_StopAt_Int) 
		+ ' Order By A.actionDate  '  +@v_orderTypeVc	
	End
	print @v_Qry_Vc 
	
	EXEC (@v_Qry_Vc) -- // Execute query to get records
	
	SET NOCOUNT OFF;
END

