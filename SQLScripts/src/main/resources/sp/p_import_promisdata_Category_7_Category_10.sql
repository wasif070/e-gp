USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_import_promisdata_Category_7_Category_10]    Script Date: 4/24/2016 11:12:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- Modified By Dohatec on 10th August 2015 To Segregate Promis data import basis of Indicator Category 
-- =============================================
ALTER PROCEDURE [dbo].[p_import_promisdata_Category_7_Category_10]
AS
BEGIN

UPDATE [Tbl_PromisIndicator]
SET  [DeliveredInTime] = T.DerivedDeliveredInTime
           ,[LiquidatedDamage] = T.LiquidatedDamage
           ,[FullyCompleted] = T.FullyCompleted
           ,[IsContAmended] = T.IsContAmended
           
FROM (select tenderId DerivedTenderId,
case
	when td.procurementNatureId = 3	--Services
	then
		case 
			when td.contractType = 'Lump - Sum'  --Services Lump - Sum
			then
				case
					when
						(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
					then
						'100'
					else
						case
							when	-- check payment schedule fully completed or not
								(select COUNT(srvPSId) from tbl_CMS_SrvPaymentSch sps, tbl_CMS_SrvFormMap sfm 
								where sps.srvFormMapId = sfm.srvFormMapId and sps.status = 'pending'
								and sfm.srvBoqId = 12 and sfm.tenderId = td.tenderId) = 0
							then
								case
									when -- check milestone complted on time or not
									(select COUNT(srvPSId) from tbl_CMS_SrvPaymentSch sps, tbl_CMS_SrvFormMap sfm 
									where sps.srvFormMapId = sfm.srvFormMapId and CAST(completedDt as DATE) > CAST(peenddate as DATE)
									and sfm.srvBoqId = 12 and sfm.tenderId = td.tenderId) > 0
									then
									'0'
									else
									'1'
								end		
							else
								case when (select distinct paymentTerms from tbl_ContractSign cs inner join tbl_NoaIssueDetails nid 
									on cs.noaId=nid.noaIssueId and nid.tenderId = td.tenderId) = 'allitem100p'
									then
									'100'
									when (select distinct paymentTerms from tbl_ContractSign cs inner join tbl_NoaIssueDetails nid 
									on cs.noaId=nid.noaIssueId and nid.tenderId = td.tenderId) = 'itemwise100p'
									then	
												
												case
													when	-- check payment schedule fully completed or not
														(select COUNT(srvPSId) from tbl_CMS_SrvPaymentSch sps, tbl_CMS_SrvFormMap sfm 
														where sps.srvFormMapId = sfm.srvFormMapId and sps.status = 'completed'
														and sfm.srvBoqId = 12 and sfm.tenderId = td.tenderId) > 0
													then
														case
															when -- check milestone complted on time or not
															(select COUNT(srvPSId) from tbl_CMS_SrvPaymentSch sps, tbl_CMS_SrvFormMap sfm 
															where sps.srvFormMapId = sfm.srvFormMapId and sps.status = 'completed' and CAST(completedDt as DATE) > CAST(peenddate as DATE)
															and sfm.srvBoqId = 12 and sfm.tenderId = td.tenderId) > 0
															then
															'0'
															else
															'1'
														end	
												
												else 
												'100'
												end
										else 
										case
															when -- check milestone complted on time or not
															(select COUNT(srvPSId) from tbl_CMS_SrvPaymentSch sps, tbl_CMS_SrvFormMap sfm 
															where sps.srvFormMapId = sfm.srvFormMapId and CAST(completedDt as DATE) > CAST(peenddate as DATE)
															and sfm.srvBoqId = 12 and sfm.tenderId = td.tenderId) > 0
															then
															'0'
															else
															'1'
														end	
												
									end
						end
				end
			else	--Services Time Based
				case
					when
						(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
					then
						'100'
					else	-- in service timebased checking for reimbusable expences
						case
							when -- check is fully completed or not (reimbusable expences)
							(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
							where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b 
							where a.wpLotId = b.appPkgLotId 
							and b.tenderId = td.tenderId) and wd.isRepeatOrder = 'no' 
							and wd.wpItemStatus = 'pending') = 0
							then
								case
									when  -- check items delivered on time or not
									(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
									where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b 
									where a.wpLotId = b.appPkgLotId 
									and b.tenderId = td.tenderId) and wd.isRepeatOrder = 'no' 
									and CAST (wd.completedDtNTime as date) > CAST (wpenddate as date)
									and wd.completedDtNTime  IS NOT NULL) > 0
									then
									'0'
									else
									'1'
								end
							else
							case when (select distinct paymentTerms from tbl_ContractSign cs inner join tbl_NoaIssueDetails nid 
							on cs.noaId=nid.noaIssueId and nid.tenderId = td.tenderId) = 'allitem100p'
							then
							'100'
							when (select distinct paymentTerms from tbl_ContractSign cs inner join tbl_NoaIssueDetails nid 
							on cs.noaId=nid.noaIssueId and nid.tenderId = td.tenderId) = 'itemwise100p'
							then	
							case
								when -- check is fully completed or not (reimbusable expences)
								(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
								where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b 
								where a.wpLotId = b.appPkgLotId 
								and b.tenderId = td.tenderId) and wd.isRepeatOrder = 'no' 
								and wd.wpItemStatus = 'completed') > 0
								then
								case
									when  -- check items delivered on time or not
									(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
									where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b 
									where a.wpLotId = b.appPkgLotId 
									and b.tenderId = td.tenderId)  and wpItemStatus = 'completed' 
									and wd.isRepeatOrder = 'no' 
									and CAST (wd.completedDtNTime as date) > CAST (wpenddate as date)
									and wd.completedDtNTime  IS NOT NULL) > 0
									then
									'0'
									else
									'1'
								end
							end
							else
									
										case
											when  -- check items delivered on time or not
											(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
											where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b 
											where a.wpLotId = b.appPkgLotId 
											and b.tenderId = td.tenderId)   
											and wd.isRepeatOrder = 'no' 
											and CAST (wd.completedDtNTime as date) > CAST (wpenddate as date)
											and wd.completedDtNTime  IS NOT NULL) > 0
											then
											'0'
											else
											'1'
										end								
								
									end
							
							
						end	
				end	
		end
	else	--Goods and Works
	case
		when
			(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
		then
			'100'
		else
			case
				when -- check is fully completed or not
				(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
				where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b 
				where a.wpLotId = b.appPkgLotId 
				and b.tenderId = td.tenderId) and wd.isRepeatOrder = 'no' 
				and wd.wpItemStatus = 'pending') = 0
				then
					case
						when  -- check items delivered on time or not
						(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
						where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b 
						where a.wpLotId = b.appPkgLotId 
						and b.tenderId = td.tenderId) and wd.isRepeatOrder = 'no' 
						and CAST (wd.completedDtNTime as date) > CAST (wpenddate as date)
						and wd.completedDtNTime  IS NOT NULL) > 0
						then
						'0'
						else
						'1'
					end
				else
						case when (select distinct paymentTerms from tbl_ContractSign cs inner join tbl_NoaIssueDetails nid 
							on cs.noaId=nid.noaIssueId and nid.tenderId = td.tenderId) = 'allitem100p'
							then
							'100'
							when (select distinct paymentTerms from tbl_ContractSign cs inner join tbl_NoaIssueDetails nid 
							on cs.noaId=nid.noaIssueId and nid.tenderId = td.tenderId) = 'itemwise100p'
							then	
							case
								when -- check is fully completed or not (reimbusable expences)
								(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
								where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b 
								where a.wpLotId = b.appPkgLotId 
								and b.tenderId = td.tenderId) and wd.isRepeatOrder = 'no' 
								and wd.wpItemStatus = 'completed') > 0
								then
								case
									when  -- check items delivered on time or not
									(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
									where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b 
									where a.wpLotId = b.appPkgLotId 
									and b.tenderId = td.tenderId)  and wpItemStatus = 'completed' 
									and wd.isRepeatOrder = 'no' 
									and CAST (wd.completedDtNTime as date) > CAST (wpenddate as date)
									and wd.completedDtNTime  IS NOT NULL) > 0
									then
									'0'
									else
									'1'
								end
								else 
								'100'
								
							end
							else
									
										case
											when  -- check items delivered on time or not
											(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
											where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b 
											where a.wpLotId = b.appPkgLotId 
											and b.tenderId = td.tenderId)   
											and wd.isRepeatOrder = 'no' 
											and CAST (wd.completedDtNTime as date) > CAST (wpenddate as date)
											and wd.completedDtNTime  IS NOT NULL) > 0
											then
											'0'
											else
											'1'
										end								
								
									end
							
			end	
	end
end DerivedDeliveredInTime,
case
	when -- Dates configured
		(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
	then
		'100'
	else
		case
			when 
			(select count(iad.ldAmt) from tbl_CMS_InvoiceAccDetails iad
			where iad.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b 
			where a.wpLotId = b.appPkgLotId 
			and b.tenderId = td.tenderId) and iad.ldAmt > 0) > 0
			then
			'1'
			else
			'0'
		end	
end LiquidatedDamage,
case
	when td.procurementNatureId = 3		--Services
	then
		case 
			when td.contractType = 'Lump - Sum'  --Services Lump - Sum
			then
			case
				when
					(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
				then
					'100'
				else
					case
						when
						(select COUNT(srvPSId) from tbl_CMS_SrvPaymentSch sps, tbl_CMS_SrvFormMap sfm 
						where sps.srvFormMapId = sfm.srvFormMapId and sps.status = 'pending'
						and sfm.srvBoqId = 12 and sfm.tenderId = td.tenderId) = 0
						then
						'1'
						else
						'0'
					end 
			end
			else	--Services Time Based
				case
					when
						(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
					then
						'100'
					else
						case
							when -- check is fully completed or not (reimbusable expences)
							(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
							where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b 
							where a.wpLotId = b.appPkgLotId 
							and b.tenderId = td.tenderId) and wd.isRepeatOrder = 'no' 
							and wd.wpItemStatus = 'pending') = 0
							then
								'1'
							else
								'0'
						end
				end
		end
	else	--Goods and Works
		case
			when
				(select count(actContractDtId) from tbl_CMS_DateConfig cdc where cdc.tenderId = td.tenderId) = 0
			then
				'100'
			else
				case
					when 
					(select COUNT(wpItemStatus) from tbl_CMS_WpDetail wd
					where wd.wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b 
					where a.wpLotId = b.appPkgLotId 
					and b.tenderId = td.tenderId) and wd.isRepeatOrder = 'no' 
					and wd.wpItemStatus = 'pending') = 0
					then
					'1'
					else
					'0'
				end	
		end
end FullyCompleted,
case
when eventType in('REOI','PQ','1 stage-TSTM')
	then '0'
else
	case when td.procurementNatureId = 1
	then
	'0'
	else
		case 
			when 
				td.procurementNatureId = 2
			then
				case
					when
						(select COUNT(variOrdId) from tbl_CMS_VariationOrder 
						where wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b 
						where a.wpLotId = b.appPkgLotId and b.tenderId = td.tenderId) and variOrdStatus = 'accepted') > 0
					then
						'1'
					else
						case
							when 
								(select COUNT(variOrdId) from tbl_CMS_VariationOrder 
								where wpId in (select wpId from tbl_CMS_WpMaster a, tbl_TenderLotSecurity b 
								where a.wpLotId = b.appPkgLotId and b.tenderId = td.tenderId) and variOrdStatus != 'accepted') > 0
							then
								'100'
							else
								'0'
						end	
				end
			else
				case
					when 
						(select COUNT(variOrdId) from tbl_CMS_VariationOrder 
						where tenderId = td.tenderId and variOrdStatus = 'accepted') > 0
					then
						'1'
					else
						case
							when 
								(select COUNT(variOrdId) from tbl_CMS_VariationOrder 
								where tenderId = td.tenderId and variOrdStatus != 'accepted') > 0
							then
								'100'
							else
								'0'
						end	
				end	
		end
		end
end IsContAmended
from tbl_TenderDetails td,tbl_DepartmentMaster dm 

where tenderStatus not in('Pending')
and td.departmentId=dm.departmentId
and submissionDt < GETDATE())T
WHERE  tenderid = T.DerivedTenderId 
and DeliveredInTime = 100
and AwardPubCPTUWebsite !=100 

END
