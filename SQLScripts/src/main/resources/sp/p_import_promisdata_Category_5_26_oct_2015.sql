USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_import_promisdata_Category_5_26_oct_2015]    Script Date: 4/24/2016 11:11:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- Modified By Dohatec on 10th August 2015 To Segregate Promis data import basis of Indicator Category 
-- =============================================
ALTER PROCEDURE [dbo].[p_import_promisdata_Category_5_26_oct_2015]
AS
BEGIN

UPDATE [Tbl_PromisIndicator]
SET 
[DaysBtwnTenEvalTenApp] = T.DerivedDaysBtwnTenEvalTenApp
           ,[IsApprovedByProperAA] = T.IsApprovedByProperAA
           ,[SubofEvalReportAA] = T.SubofEvalReportAA           
           ,[TERReviewByOther] = T.TERReviewByOther
           ,[ContractAppByHighAuth] = T.ContractAppByHighAuth
FROM      
(  select tenderId DerivedTenderID,
case when (select count(distinct envelopeId) from tbl_TenderEnvelopes where tenderId = td.tenderid)=1 
then ISNULL( convert(varchar(20),DATEDIFF(d,
	(case	when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and tc.committeeType in ('TEC','PEC'))=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					)then 
						(	SELECT ISNULL((select MIN(processDate) 
							from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
							on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	and wfl.wfRoleId = 1							
							where  [action] = 'Forward' and wfl.activityId = 13 and wfh.objectId = td.tenderid) , 
							(	select MIN(sentDate) from tbl_EvalRptSentToAA era where era.tenderId=td.tenderid 
								and roundId = (select MIN(roundId) from tbl_TORRptSign 
								where tenderId = td.tenderid and reportType = 'TER4')
								group by tenderId))
						)	
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and td.procurementNatureId != 3 
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					) then 
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc 
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
							where tc.tenderId = td.tenderid and td.procurementNatureId != 3 
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
							where trs.tenderId = td.tenderid and trs.reportType = 'TER2' and td.procurementNatureId != 3 
							and roundId =(select MIN(roundId) from tbl_TORRptSign 
							where tenderId = td.tenderid and reportType = 'TER2')
						) then 
						(	case  when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))>0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))										
									when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = 0)>0 and (select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))=0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 
									and roundId = 0)	
							end										
						)	end
					) 			
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and td.procurementNatureId = 3 
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					) then 
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc 
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
							where tc.tenderId = td.tenderid and td.procurementNatureId = 3 
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
							where trs.tenderId = td.tenderid and trs.reportType = 'TER1' 
							and roundId =(select MIN(roundId) from tbl_TORRptSign 
							where tenderId = td.tenderid and reportType = 'TER1')
						) then 
						(	case  when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER1'))>0
									then
									(	SELECT ISNULL((select MIN(processDate) 
										from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
										on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	
										and wfl.wfRoleId = 1 where  [action] = 'Forward' and wfl.activityId = 13 
										and wfh.objectId = td.tenderid) , 							
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER1'))))	
							end										
						)	end
					)							
					end
	),(select MIN(rptApproveDt) from tbl_EvalRptSentToAA era
where era.tenderId=td.tenderId
group by tenderId ))),'NA') 
when (select count(distinct envelopeId) from tbl_TenderEnvelopes where tenderId = td.tenderid)=2 
then
(case when (select count(*) from tbl_EvalRptSentToAA where tenderId = td.tenderId and envelopeId =0 )>0
then
(case when (select count(*) from tbl_EvalRptSentToAA where tenderId = td.tenderId and envelopeId =1 )>0
then
 ISNULL( convert(varchar(20),(DATEDIFF(d,
	(case	when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and tc.committeeType in ('TEC','PEC'))=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					)
					then 
						(	SELECT ISNULL((select MIN(processDate) from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
							on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	and wfl.wfRoleId = 1							
							where  [action] = 'Forward' and wfl.activityId = 13 and wfh.processDate > (select MIN(rptApproveDt) from tbl_EvalRptSentToAA era
							where era.tenderId=td.tenderId and envelopeId = 0 group by tenderId) and wfh.objectId = td.tenderid) , 
							(	select MIN(sentDate) from tbl_EvalRptSentToAA era where era.tenderId=td.tenderid  and envelopeId = 1
								and roundId = (select MIN(roundId) from tbl_TORRptSign where tenderId = td.tenderid and reportType = 'TER4')
								group by tenderId))
						)
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc inner join tbl_CommitteeMembers tcm 
					ON tc.committeeId = tcm.committeeId where tc.tenderId = td.tenderid and td.procurementNatureId != 3 
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign where tenderId = td.tenderid and reportType = 'TER4')
					) then 
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc inner join tbl_CommitteeMembers tcm 
							ON tc.committeeId = tcm.committeeId where tc.tenderId = td.tenderid and td.procurementNatureId != 3 
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
							where trs.tenderId = td.tenderid and trs.reportType = 'TER2' and td.procurementNatureId != 3 
							and roundId =(select MIN(roundId) from tbl_TORRptSign 
							where tenderId = td.tenderid and reportType = 'TER2')
						) then 
						(	case  when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA tersta 
									where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))>0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 and envelopeId =1
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))										
									when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = 0)>0 and (select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))=0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 and envelopeId = 1
									and roundId = 0)	
							end											
						)	end
					) 			
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm 
					ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and td.procurementNatureId = 3 
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					) then 
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc 
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
							where tc.tenderId = td.tenderid and td.procurementNatureId = 3 
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
							where trs.tenderId = td.tenderid and trs.reportType = 'TER1' 
							and roundId =(select MIN(roundId) from tbl_TORRptSign 
							where tenderId = td.tenderid and reportType = 'TER1')
						) then 
						(	case  when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER1'))>0
									then
									(	SELECT ISNULL((select MIN(processDate) 
										from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
										on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	
										and wfl.wfRoleId = 1 where  [action] = 'Forward' and wfl.activityId = 13 and wfh.processDate > (select MIN(rptApproveDt) from tbl_EvalRptSentToAA era
										where era.tenderId=td.tenderId and envelopeId = 0 group by tenderId)
										and wfh.objectId = td.tenderid) , 							
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3 and tersta.envelopeId = 1
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER1'))))	
							end											
						)	end
					)										
					end
	),(select MIN(rptApproveDt) from tbl_EvalRptSentToAA era
where era.tenderId=td.tenderId and era.envelopeId = 1
group by tenderId )))
+  
(DATEDIFF(d,
	(case	when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and tc.committeeType in ('TEC','PEC'))=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					)
					then 
						(	SELECT ISNULL((select MIN(processDate) 
							from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
							on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	and wfl.wfRoleId = 1							
							where  [action] = 'Forward' and wfl.activityId = 13 and wfh.objectId = td.tenderid) , 
							(	select MIN(sentDate) from tbl_EvalRptSentToAA era
								where era.tenderId=td.tenderid  and era.envelopeId=0
								and roundId = (select MIN(roundId) from tbl_TORRptSign 
								where tenderId = td.tenderid and reportType = 'TER4')
								group by tenderId))
						)									
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and td.procurementNatureId != 3 
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					) then 
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc 
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
							where tc.tenderId = td.tenderid and td.procurementNatureId != 3 
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
							where trs.tenderId = td.tenderid and trs.reportType = 'TER2' and td.procurementNatureId != 3 
							and roundId =(select MIN(roundId) from tbl_TORRptSign 
							where tenderId = td.tenderid and reportType = 'TER2')
						) then 
						(	case  when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))>0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 and tersta.envelopeId =0
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))										
									when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = 0)>0 and (select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))=0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 and tersta.envelopeId =0 
									and roundId = 0)	
							end										
						)	end
					) 			
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and td.procurementNatureId = 3 
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					) then 
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc 
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
							where tc.tenderId = td.tenderid and td.procurementNatureId = 3 
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
							where trs.tenderId = td.tenderid and trs.reportType = 'TER1' 
							and roundId =(select MIN(roundId) from tbl_TORRptSign 
							where tenderId = td.tenderid and reportType = 'TER1')
						) then 
						(	case  when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER1'))>0
									then
									(	SELECT ISNULL((select MIN(processDate) 
										from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
										on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	
										and wfl.wfRoleId = 1 where  [action] = 'Forward' and wfl.activityId = 13 
										and wfh.objectId = td.tenderid) , 							
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3 and tersta.envelopeId =0
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER1'))))	
							end											
						)	end
					)										
					end
	),(select MIN(rptApproveDt) from tbl_EvalRptSentToAA era
where era.tenderId=td.tenderId and era.envelopeId = 0
group by tenderId )))
),'NA')
else 
ISNULL( convert(varchar(20),DATEDIFF(d,
	(case	when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and tc.committeeType in ('TEC','PEC'))=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					)
					then 
						(	SELECT ISNULL((select MIN(processDate) 
							from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
							on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	and wfl.wfRoleId = 1							
							where  [action] = 'Forward' and wfl.activityId = 13 and wfh.objectId = td.tenderid) , 
							(	select MIN(sentDate) from tbl_EvalRptSentToAA era where era.tenderId=td.tenderid  and era.envelopeId=0
								and roundId = (select MIN(roundId) from tbl_TORRptSign 
								where tenderId = td.tenderid and reportType = 'TER4')
								group by tenderId))
						)									
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and td.procurementNatureId != 3 
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					) then 
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc 
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
							where tc.tenderId = td.tenderid and td.procurementNatureId != 3 
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
							where trs.tenderId = td.tenderid and trs.reportType = 'TER2' and td.procurementNatureId != 3 
							and roundId =(select MIN(roundId) from tbl_TORRptSign 
							where tenderId = td.tenderid and reportType = 'TER2')
						) then 
						(	case  when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))>0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 and tersta.envelopeId =0
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))										
									when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = 0)>0 and (select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))=0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 and tersta.envelopeId =0 
									and roundId = 0)	
							end												
						)	end
					) 			
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and td.procurementNatureId = 3 
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					) then 
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc 
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
							where tc.tenderId = td.tenderid and td.procurementNatureId = 3 
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
							where trs.tenderId = td.tenderid and trs.reportType = 'TER1' 
							and roundId =(select MIN(roundId) from tbl_TORRptSign 
							where tenderId = td.tenderid and reportType = 'TER1')
						) then 
						(	case  when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER1'))>0
									then
									(	SELECT ISNULL((select MIN(processDate) 
										from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
										on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	
										and wfl.wfRoleId = 1 where  [action] = 'Forward' and wfl.activityId = 13 
										and wfh.objectId = td.tenderid) , 							
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3 and tersta.envelopeId =0
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER1'))))	
							end											
						)	end
					)										
					end
	),(select MIN(rptApproveDt) from tbl_EvalRptSentToAA era
where era.tenderId=td.tenderId and era.envelopeId = 0
group by tenderId ))),'NA') 
end
)
else 'NA'
end )
else 'NA'
end as DerivedDaysBtwnTenEvalTenApp, --Modified By Dohatec
---- Financial Delegation
(case
       WHEN
		(SELECT COUNT(tenderId) FROM tbl_EvalRptSentToAA ea WHERE ea.tenderId = td.tenderId) = 0
	THEN
		'0'
	ELSE
		CASE WHEN
			(SELECT COUNT(*) FROM tbl_WorkFlowFileHistory wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13) = 0
		THEN
			'1'
		ELSE
					(CASE WHEN (select count(*) from tbl_DepartmentMaster where organizationType != 'yes' and
								departmentId in (select distinct departmentId from tbl_tenderdetails where tenderId = td.tenderid))>0
					THEN
						---Corporation Not Applicable
							(CASE WHEN td.projectName IS NOT NULL			
								THEN
									(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =7) or
													(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=7) and 
														((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
									THEN --- CCGP Approver
									(CASE WHEN
									(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
									where (select distinct amount from tbl_BidderRank
									where userId = (select userId from tbl_EvalRoundMaster
									where tenderId = td.tenderId and reportType = 'L1'
									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
									where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
									where tenderId = td.tenderId and reportType = 'L1'))
									>= fd.MinValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
									and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
									and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
									and ApprovingAuthority ='CCGP') T) > 0
										THEN
											'1'
										ELSE
											'0'
										END)
									
									
									
									ELSE 
															(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =3) or
																		(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=3) and 
																			((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																		THEN --- Minister Approver 
																				(CASE WHEN (select ISNULL(COUNT(employeeid),0) from tbl_EmployeeRoles 
																							where procurementRoleId = (select procurementRoleId from tbl_ProcurementRole where procurementRole = 'BOD')
																							and departmentId in (select departmentId from tbl_DepartmentMaster where departmentType='Organization' 
																							and departmentName = (select agency from tbl_TenderDetails where tenderId = td.tenderId)))>0
																							THEN
																								(CASE WHEN
																								(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																								where (select distinct amount from tbl_BidderRank
																								where userId = (select userId from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1'
																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1'))
																								between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																								and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																								and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																								and IsBoD = 'yes'
																								and ApprovingAuthority = 'Minister') T) > 0
																									THEN
																										'1'
																									ELSE
																										'0'																							
																									END)
																				ELSE
																						(CASE WHEN
																								(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																								where (select distinct amount from tbl_BidderRank
																								where userId = (select userId from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1'
																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1'))
																								between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																								and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																								and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																								and IsBoD != 'yes'
																								and ApprovingAuthority = 'Minister') T) > 0
																									THEN
																										'1'
																									ELSE
																										'0'
																									END)
																				END)
																				
																	ELSE
																
																										(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =6) or
																										(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=6) and 
																											((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																										THEN -- HOPE Approver
																												(CASE WHEN (select ISNULL(COUNT(employeeid),0) from tbl_EmployeeRoles 
																															where procurementRoleId = (select procurementRoleId from tbl_ProcurementRole where procurementRole = 'BOD')
																															and departmentId in (select departmentId from tbl_DepartmentMaster where departmentType='Organization' 
																															and departmentName = (select agency from tbl_TenderDetails where tenderId = td.tenderId)))>0
																															THEN
																															(CASE WHEN (SELECT procurementnatureid from tbl_tenderdetails where tenderId =td.tenderId) =2
																																THEN
																																(CASE WHEN
																																(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																where (select distinct amount from tbl_BidderRank
																																where userId = (select userId from tbl_EvalRoundMaster
																																where tenderId = td.tenderId and reportType = 'L1'
																																and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																where tenderId = td.tenderId and reportType = 'L1'))
																																between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																																and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																																and IsBoD = 'yes'
																																and ApprovingAuthority = 'HOPE') T) > 0
																																	THEN
																																		'1'
																																	ELSE
																																		'0'																							
																																	END)
																																	ELSE
																																			(CASE WHEN
																																			(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																			where (select distinct amount from tbl_BidderRank
																																			where userId = (select userId from tbl_EvalRoundMaster
																																			where tenderId = td.tenderId and reportType = 'L1'
																																			and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																			where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																			and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																			where tenderId = td.tenderId and reportType = 'L1'))
																																			between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																																			and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																			and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																																			and IsBoD != 'yes'
																																			and ApprovingAuthority = 'HOPE') T) > 0
																																				THEN
																																					'1'
																																				ELSE
																																					'0'																							
																																				END)																						
																																	END)
																												ELSE
																														(CASE WHEN
																																(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																where (select distinct amount from tbl_BidderRank
																																where userId = (select userId from tbl_EvalRoundMaster
																																where tenderId = td.tenderId and reportType = 'L1'
																																and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																where tenderId = td.tenderId and reportType = 'L1'))
																																between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																																and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																																and IsBoD != 'yes'
																																and ApprovingAuthority = 'HOPE') T) > 0
																																	THEN
																																		'1'
																																	ELSE
																																		'0'
																																	END)
																												END)
																												
																									ELSE
																																---- BOD Approver
																																				(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =4) or
																																				(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=4) and 
																																					((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																																				THEN 																																			
																																						(CASE WHEN
																																							(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																							where (select distinct amount from tbl_BidderRank
																																							where userId = (select userId from tbl_EvalRoundMaster
																																							where tenderId = td.tenderId and reportType = 'L1'
																																							and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																							where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																							and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																							where tenderId = td.tenderId and reportType = 'L1'))
																																							between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																																							and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																							and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																																							and ApprovingAuthority = 'BoD') T) > 0
																																									THEN
																																												'1'
																																									ELSE
																																												'0'																							
																																									END)
																																			ELSE
																																					-- PD Approver
																																							(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =19) or
																																									(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=19) and 
																																										((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																																									THEN 																																			
																																											(CASE WHEN
																																												(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																												where (select distinct amount from tbl_BidderRank
																																												where userId = (select userId from tbl_EvalRoundMaster
																																												where tenderId = td.tenderId and reportType = 'L1'
																																												and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																												where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																												and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																												where tenderId = td.tenderId and reportType = 'L1'))
																																												between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																																												and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																												and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no' 
																																												and dbo.f_get_ProjectCostWiseCheck(td.tenderId,fd.MinProjectValueBDT,fd.MaxProjectValueBDT)=1
																																												and ApprovingAuthority ='PD') T) > 0
																																														THEN
																																																	'1'
																																														ELSE
																																																	'0'																							
																																														END)
																																								ELSE
																																										'0'	
																																							END)
																																					-- PD approver
																																		END)
																																----
																								END)
																END)
										
									END)
						ELSE
							-- Revenue Budget
							(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =7) or
													(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=7) and 
														((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
									THEN --- CCGP Approver
									(CASE WHEN
									(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
									where (select distinct amount from tbl_BidderRank
									where userId = (select userId from tbl_EvalRoundMaster
									where tenderId = td.tenderId and reportType = 'L1'
									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
									where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
									where tenderId = td.tenderId and reportType = 'L1'))
									>= fd.MinValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
									and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
									and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
									and ApprovingAuthority ='CCGP') T) > 0
										THEN
											'1'
										ELSE
											'0'
										END)
									
									
									
									ELSE 
															(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =3) or
																		(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=3) and 
																			((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																		THEN --- Minister Approver 
																				(CASE WHEN (select ISNULL(COUNT(employeeid),0) from tbl_EmployeeRoles 
																							where procurementRoleId = (select procurementRoleId from tbl_ProcurementRole where procurementRole = 'BOD')
																							and departmentId in (select departmentId from tbl_DepartmentMaster where departmentType='Organization' 
																							and departmentName = (select agency from tbl_TenderDetails where tenderId = td.tenderId)))>0
																							THEN
																								(CASE WHEN
																								(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																								where (select distinct amount from tbl_BidderRank
																								where userId = (select userId from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1'
																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1'))
																								between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																								and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																								and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																								and IsBoD = 'yes'
																								and ApprovingAuthority = 'Minister') T) > 0
																									THEN
																										'1'
																									ELSE
																										'0'																							
																									END)
																				ELSE
																						(CASE WHEN
																								(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																								where (select distinct amount from tbl_BidderRank
																								where userId = (select userId from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1'
																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																								where tenderId = td.tenderId and reportType = 'L1'))
																								between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																								and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																								and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																								and IsBoD != 'yes'
																								and ApprovingAuthority = 'Minister') T) > 0
																									THEN
																										'1'
																									ELSE
																										'0'
																									END)
																				END)
																				
																	ELSE
																
																										(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =6) or
																										(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=6) and 
																											((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																										THEN -- HOPE Approver																									
																															(CASE WHEN
																																(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																where (select distinct amount from tbl_BidderRank
																																where userId = (select userId from tbl_EvalRoundMaster
																																where tenderId = td.tenderId and reportType = 'L1'
																																and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																where tenderId = td.tenderId and reportType = 'L1'))
																																between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																																and ApprovingAuthority = 'HOPE') T) > 0
																																	THEN
																																		'1'
																																	ELSE
																																		'0'																							
																																	END)
																									ELSE
																																---- BOD Approver
																																				(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =4) or
																																				(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=4) and 
																																					((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																																				THEN 																																			
																																						(CASE WHEN
																																							(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																							where (select distinct amount from tbl_BidderRank
																																							where userId = (select userId from tbl_EvalRoundMaster
																																							where tenderId = td.tenderId and reportType = 'L1'
																																							and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																							where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																							and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																							where tenderId = td.tenderId and reportType = 'L1'))
																																							between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																							and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																							and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																																							and ApprovingAuthority = 'BoD') T) > 0
																																									THEN
																																												'1'
																																									ELSE
																																												'0'																							
																																									END)
																																			ELSE
																																					-- Grade 5 Approver
																																							(CASE WHEN ((select gradeId from tbl_designationmaster where designationid in 
																																										(select designationId from tbl_employeemaster where userid in
																																										(select distinct userId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId and worfklowId = 
																																											(SELECT MAX(worfklowId) FROM tbl_WorkFlowLevelConfig wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13)))) <=5)  and 
																																										((select procurementNatureId from tbl_TenderDetails where  tenderId = td.tenderId) =3) and
																																										((select COUNT(*) from tbl_TenderDetails where  tenderId = td.tenderId and td.procurementMethod in ('IC','SSS','IC')) >0)
																																										THEN 																																			
																																											----Single Source And Individual Consultant
																																											(CASE WHEN
																																																	(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																																	where (select distinct amount from tbl_BidderRank
																																																	where userId = (select userId from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1'
																																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1'))
																																																	between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																																	and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																																	and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																																																	and ApprovingAuthority = 'HOPE') T) > 0
																																																		THEN
																																																			'1'
																																																		ELSE
																																																			'0'																							
																																																		END)
																																											----Single Source And Individual Consultant
																																									WHEN ((select gradeId from tbl_designationmaster where designationid in 
																																										(select designationId from tbl_employeemaster where userid in
																																										(select distinct userId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId and worfklowId = 
																																											(SELECT MAX(worfklowId) FROM tbl_WorkFlowLevelConfig wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13)))) <=5) and 
																																										((select procurementNatureId from tbl_TenderDetails where  tenderId = td.tenderId) =3) and
																																										((select COUNT(*) from tbl_TenderDetails where  tenderId = td.tenderId and td.procurementMethod not in ('IC','SSS','IC')) >0)
																																										THEN 
																																										----Without Single Source And Individual Consultant
																																										(CASE WHEN
																																																	(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																																	where (select distinct amount from tbl_BidderRank
																																																	where userId = (select userId from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1'
																																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1'))
																																																	< fd.MinValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																																	and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																																	and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																																																	and ApprovingAuthority = 'HOPE') T) > 0
																																																		THEN
																																																			'1'
																																																		ELSE
																																																			'0'																							
																																																		END)
																																										----Without Single Source And Individual Consultant
																																										WHEN ((select gradeId from tbl_designationmaster where designationid in 
																																										(select designationId from tbl_employeemaster where userid in
																																										(select distinct userId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId and worfklowId = 
																																											(SELECT MAX(worfklowId) FROM tbl_WorkFlowLevelConfig wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13)))) <=5)  and 
																																										((select procurementNatureId from tbl_TenderDetails where  tenderId = td.tenderId) !=3) and
																																										((select COUNT(*) from tbl_TenderDetails where  tenderId = td.tenderId and td.procurementMethod  in ('RFQ','RFQU','RFQL','DP','LTM')) >0)
																																										THEN 
																																										---For ('RFQ','RFQU','RFQL','DP','LTM')
																																															(CASE WHEN
																																																	(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																																	where (select distinct amount from tbl_BidderRank
																																																	where userId = (select userId from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1'
																																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1'))
																																																	between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																																	and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																																	and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																																																	and ApprovingAuthority = 'HOPE') T) > 0
																																																		THEN
																																																			'1'
																																																		ELSE
																																																			'0'																							
																																																		END)
																																															---For ('RFQ','RFQU','RFQL','DP','LTM')
																																										WHEN ((select gradeId from tbl_designationmaster where designationid in 
																																										(select designationId from tbl_employeemaster where userid in
																																										(select distinct userId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId and worfklowId = 
																																											(SELECT MAX(worfklowId) FROM tbl_WorkFlowLevelConfig wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13)))) <=5) and 
																																										((select procurementNatureId from tbl_TenderDetails where  tenderId = td.tenderId) !=3) and
																																										((select COUNT(*) from tbl_TenderDetails where  tenderId = td.tenderId and td.procurementMethod not in ('RFQ','RFQU','RFQL','DP','LTM')) >0)
																																										
																																										THEN 
																																										---Without ('RFQ','RFQU','RFQL','DP','LTM')
																																															(CASE WHEN
																																																	(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																																	where (select distinct amount from tbl_BidderRank
																																																	where userId = (select userId from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1'
																																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																	where tenderId = td.tenderId and reportType = 'L1'))
																																																	< fd.MinValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																																	and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																																	and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='no'
																																																	and ApprovingAuthority = 'HOPE') T) > 0
																																																		THEN
																																																			'1'
																																																		ELSE
																																																			'0'																							
																																																		END)
																																															---Without ('RFQ','RFQU','RFQL','DP','LTM')
																																								ELSE
																																										'0'	
																																							END)
																																					-- Grade 5 Approver
																																		END)
																																----
																								END)
																END)
										
									END)
							-- Revenue Budget
						END )
						---Corporation Not Applicable
					ELSE 	
						----Corporation Applicable
								(CASE WHEN td.projectName IS NOT NULL			
									THEN
										(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =7) or
														(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=7) and 
															((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
										THEN --- CCGP Approver
										(CASE WHEN
										(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
										where (select distinct amount from tbl_BidderRank
										where userId = (select userId from tbl_EvalRoundMaster
										where tenderId = td.tenderId and reportType = 'L1'
										and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
										where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
										and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
										where tenderId = td.tenderId and reportType = 'L1'))
										>= fd.MinValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
										and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
										and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
										and ApprovingAuthority ='CCGP') T) > 0
											THEN
												'1'
											ELSE
												'0'
											END)
										
										
										
										ELSE 
																(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =3) or
																			(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=3) and 
																				((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																			THEN --- Minister Approver 
																					(CASE WHEN (select ISNULL(COUNT(employeeid),0) from tbl_EmployeeRoles 
																								where procurementRoleId = (select procurementRoleId from tbl_ProcurementRole where procurementRole = 'BOD')
																								and departmentId in (select departmentId from tbl_DepartmentMaster where departmentType='Organization' 
																								and departmentName = (select agency from tbl_TenderDetails where tenderId = td.tenderId)))>0
																								THEN
																									(CASE WHEN
																									(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																									where (select distinct amount from tbl_BidderRank
																									where userId = (select userId from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1'
																									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1'))
																									between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																									and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																									and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																									and IsBoD = 'yes'
																									and ApprovingAuthority = 'Minister') T) > 0
																										THEN
																											'1'
																										ELSE
																											'0'																							
																										END)
																					ELSE
																							(CASE WHEN
																									(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																									where (select distinct amount from tbl_BidderRank
																									where userId = (select userId from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1'
																									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1'))
																									between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																									and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																									and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																									and IsBoD != 'yes'
																									and ApprovingAuthority = 'Minister') T) > 0
																										THEN
																											'1'
																										ELSE
																											'0'
																										END)
																					END)
																					
																		ELSE
																	
																											(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =6) or
																											(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=6) and 
																												((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																											THEN -- HOPE Approver
																													(CASE WHEN (select ISNULL(COUNT(employeeid),0) from tbl_EmployeeRoles 
																																where procurementRoleId = (select procurementRoleId from tbl_ProcurementRole where procurementRole = 'BOD')
																																and departmentId in (select departmentId from tbl_DepartmentMaster where departmentType='Organization' 
																																and departmentName = (select agency from tbl_TenderDetails where tenderId = td.tenderId)))>0
																																THEN
																																(CASE WHEN (SELECT procurementnatureid from tbl_tenderdetails where tenderId =td.tenderId) =2
																																	THEN
																																	(CASE WHEN
																																	(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																	where (select distinct amount from tbl_BidderRank
																																	where userId = (select userId from tbl_EvalRoundMaster
																																	where tenderId = td.tenderId and reportType = 'L1'
																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																	where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																	where tenderId = td.tenderId and reportType = 'L1'))
																																	between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																																	and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																	and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																																	and IsBoD = 'yes'
																																	and ApprovingAuthority = 'HOPE') T) > 0
																																		THEN
																																			'1'
																																		ELSE
																																			'0'																							
																																		END)
																																		ELSE
																																				(CASE WHEN
																																				(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																				where (select distinct amount from tbl_BidderRank
																																				where userId = (select userId from tbl_EvalRoundMaster
																																				where tenderId = td.tenderId and reportType = 'L1'
																																				and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																				where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																				and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																				where tenderId = td.tenderId and reportType = 'L1'))
																																				between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																																				and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																				and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																																				and IsBoD != 'yes'
																																				and ApprovingAuthority = 'HOPE') T) > 0
																																					THEN
																																						'1'
																																					ELSE
																																						'0'																							
																																					END)																						
																																		END)
																													ELSE
																															(CASE WHEN
																																	(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																	where (select distinct amount from tbl_BidderRank
																																	where userId = (select userId from tbl_EvalRoundMaster
																																	where tenderId = td.tenderId and reportType = 'L1'
																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																	where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																	where tenderId = td.tenderId and reportType = 'L1'))
																																	between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																																	and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																	and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																																	and IsBoD != 'yes'
																																	and ApprovingAuthority = 'HOPE') T) > 0
																																		THEN
																																			'1'
																																		ELSE
																																			'0'
																																		END)
																													END)
																													
																										ELSE
																																	---- BOD Approver
																																					(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =4) or
																																					(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=4) and 
																																						((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																																					THEN 																																			
																																							(CASE WHEN
																																								(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																								where (select distinct amount from tbl_BidderRank
																																								where userId = (select userId from tbl_EvalRoundMaster
																																								where tenderId = td.tenderId and reportType = 'L1'
																																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																								where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																								where tenderId = td.tenderId and reportType = 'L1'))
																																								between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																																								and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																								and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																																								and ApprovingAuthority = 'BoD') T) > 0
																																										THEN
																																													'1'
																																										ELSE
																																													'0'																							
																																										END)
																																				ELSE
																																						-- PD Approver
																																								(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =19) or
																																										(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=19) and 
																																											((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																																										THEN 																																			
																																												(CASE WHEN
																																													(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																													where (select distinct amount from tbl_BidderRank
																																													where userId = (select userId from tbl_EvalRoundMaster
																																													where tenderId = td.tenderId and reportType = 'L1'
																																													and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																													where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																													and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																													where tenderId = td.tenderId and reportType = 'L1'))
																																													between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Development' and fd.ProcurementMethod = td.procurementMethod
																																													and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																													and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes' 
																																													and dbo.f_get_ProjectCostWiseCheck(td.tenderId,fd.MinProjectValueBDT,fd.MaxProjectValueBDT)=1
																																													and ApprovingAuthority ='PD') T) > 0
																																															THEN
																																																		'1'
																																															ELSE
																																																		'0'																							
																																															END)
																																									ELSE
																																											'0'	
																																								END)
																																						-- PD approver
																																			END)
																																	----
																									END)
																	END)
											
										END)
							ELSE
								-- Revenue Budget
								(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =7) or
														(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=7) and 
															((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
										THEN --- CCGP Approver
										(CASE WHEN
										(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
										where (select distinct amount from tbl_BidderRank
										where userId = (select userId from tbl_EvalRoundMaster
										where tenderId = td.tenderId and reportType = 'L1'
										and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
										where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
										and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
										where tenderId = td.tenderId and reportType = 'L1'))
										>= fd.MinValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
										and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
										and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
										and ApprovingAuthority ='CCGP') T) > 0
											THEN
												'1'
											ELSE
												'0'
											END)
										
										
										
										ELSE 
																(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =3) or
																			(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=3) and 
																				((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																			THEN --- Minister Approver 
																					(CASE WHEN (select ISNULL(COUNT(employeeid),0) from tbl_EmployeeRoles 
																								where procurementRoleId = (select procurementRoleId from tbl_ProcurementRole where procurementRole = 'BOD')
																								and departmentId in (select departmentId from tbl_DepartmentMaster where departmentType='Organization' 
																								and departmentName = (select agency from tbl_TenderDetails where tenderId = td.tenderId)))>0
																								THEN
																									(CASE WHEN
																									(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																									where (select distinct amount from tbl_BidderRank
																									where userId = (select userId from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1'
																									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1'))
																									between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																									and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																									and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																									and IsBoD = 'yes'
																									and ApprovingAuthority = 'Minister') T) > 0
																										THEN
																											'1'
																										ELSE
																											'0'																							
																										END)
																					ELSE
																							(CASE WHEN
																									(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																									where (select distinct amount from tbl_BidderRank
																									where userId = (select userId from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1'
																									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																									and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																									where tenderId = td.tenderId and reportType = 'L1'))
																									between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																									and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																									and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																									and IsBoD != 'yes'
																									and ApprovingAuthority = 'Minister') T) > 0
																										THEN
																											'1'
																										ELSE
																											'0'
																										END)
																					END)
																					
																		ELSE
																	
																											(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =6) or
																											(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=6) and 
																												((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																											THEN -- HOPE Approver																									
																																(CASE WHEN
																																	(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																	where (select distinct amount from tbl_BidderRank
																																	where userId = (select userId from tbl_EvalRoundMaster
																																	where tenderId = td.tenderId and reportType = 'L1'
																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																	where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																	and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																	where tenderId = td.tenderId and reportType = 'L1'))
																																	between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																	and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																	and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																																	and ApprovingAuthority = 'HOPE') T) > 0
																																		THEN
																																			'1'
																																		ELSE
																																			'0'																							
																																		END)
																										ELSE
																																	---- BOD Approver
																																					(CASE WHEN ((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =4) or
																																					(	((select approvingAuthEmpId from tbl_AppPackages where packageId=(select packageId from tbl_TenderMaster where tenderId = td.tenderId))=4) and 
																																						((select distinct procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId) =21))
																																					THEN 																																			
																																							(CASE WHEN
																																								(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																								where (select distinct amount from tbl_BidderRank
																																								where userId = (select userId from tbl_EvalRoundMaster
																																								where tenderId = td.tenderId and reportType = 'L1'
																																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																								where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																								and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																								where tenderId = td.tenderId and reportType = 'L1'))
																																								between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																								and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																								and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																																								and ApprovingAuthority = 'BoD') T) > 0
																																										THEN
																																													'1'
																																										ELSE
																																													'0'																							
																																										END)
																																				ELSE
																																						-- Grade 5 Approver
																																								(CASE WHEN ((select gradeId from tbl_designationmaster where designationid in 
																																											(select designationId from tbl_employeemaster where userid in
																																											(select distinct userId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId and worfklowId = 
																																												(SELECT MAX(worfklowId) FROM tbl_WorkFlowLevelConfig wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13)))) <=5)  and 
																																											((select procurementNatureId from tbl_TenderDetails where  tenderId = td.tenderId) =3) and
																																											((select COUNT(*) from tbl_TenderDetails where  tenderId = td.tenderId and td.procurementMethod in ('IC','SSS','IC')) >0)
																																											THEN 																																			
																																												----Single Source And Individual Consultant
																																												(CASE WHEN
																																																		(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																																		where (select distinct amount from tbl_BidderRank
																																																		where userId = (select userId from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1'
																																																		and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																																		and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1'))
																																																		between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																																		and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																																		and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																																																		and ApprovingAuthority = 'HOPE') T) > 0
																																																			THEN
																																																				'1'
																																																			ELSE
																																																				'0'																							
																																																			END)
																																												----Single Source And Individual Consultant
																																										WHEN ((select gradeId from tbl_designationmaster where designationid in 
																																											(select designationId from tbl_employeemaster where userid in
																																											(select distinct userId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId and worfklowId = 
																																												(SELECT MAX(worfklowId) FROM tbl_WorkFlowLevelConfig wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13)))) <=5) and 
																																											((select procurementNatureId from tbl_TenderDetails where  tenderId = td.tenderId) =3) and
																																											((select COUNT(*) from tbl_TenderDetails where  tenderId = td.tenderId and td.procurementMethod not in ('IC','SSS','IC')) >0)
																																											THEN 
																																											----Without Single Source And Individual Consultant
																																											(CASE WHEN
																																																		(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																																		where (select distinct amount from tbl_BidderRank
																																																		where userId = (select userId from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1'
																																																		and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																																		and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1'))
																																																		< fd.MinValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																																		and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																																		and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																																																		and ApprovingAuthority = 'HOPE') T) > 0
																																																			THEN
																																																				'1'
																																																			ELSE
																																																				'0'																							
																																																			END)
																																											----Without Single Source And Individual Consultant
																																											WHEN ((select gradeId from tbl_designationmaster where designationid in 
																																											(select designationId from tbl_employeemaster where userid in
																																											(select distinct userId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId and worfklowId = 
																																												(SELECT MAX(worfklowId) FROM tbl_WorkFlowLevelConfig wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13)))) <=5)  and 
																																											((select procurementNatureId from tbl_TenderDetails where  tenderId = td.tenderId) !=3) and
																																											((select COUNT(*) from tbl_TenderDetails where  tenderId = td.tenderId and td.procurementMethod  in ('RFQ','RFQU','RFQL','DP','LTM')) >0)
																																											THEN 
																																											---For ('RFQ','RFQU','RFQL','DP','LTM')
																																																(CASE WHEN
																																																		(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																																		where (select distinct amount from tbl_BidderRank
																																																		where userId = (select userId from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1'
																																																		and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																																		and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1'))
																																																		between fd.MinValueBDT and fd.MaxValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																																		and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																																		and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																																																		and ApprovingAuthority = 'HOPE') T) > 0
																																																			THEN
																																																				'1'
																																																			ELSE
																																																				'0'																							
																																																			END)
																																																---For ('RFQ','RFQU','RFQL','DP','LTM')
																																											WHEN ((select gradeId from tbl_designationmaster where designationid in 
																																											(select designationId from tbl_employeemaster where userid in
																																											(select distinct userId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId and worfklowId = 
																																												(SELECT MAX(worfklowId) FROM tbl_WorkFlowLevelConfig wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13)))) <=5) and 
																																											((select procurementNatureId from tbl_TenderDetails where  tenderId = td.tenderId) !=3) and
																																											((select COUNT(*) from tbl_TenderDetails where  tenderId = td.tenderId and td.procurementMethod not in ('RFQ','RFQU','RFQL','DP','LTM')) >0)
																																											
																																											THEN 
																																											---Without ('RFQ','RFQU','RFQL','DP','LTM')
																																																(CASE WHEN
																																																		(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
																																																		where (select distinct amount from tbl_BidderRank
																																																		where userId = (select userId from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1'
																																																		and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
																																																		and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
																																																		where tenderId = td.tenderId and reportType = 'L1'))
																																																		< fd.MinValueBDT  and fd.BudgetType = 'Revenue' and fd.ProcurementMethod = td.procurementMethod
																																																		and fd.ProcurementNature = td.ProcurementNature 	and fd.TenderType = td.eventType
																																																		and fd.TenderEmergency = td.pkgUrgency and fd.IsCorporation='yes'
																																																		and ApprovingAuthority = 'HOPE') T) > 0
																																																			THEN
																																																				'1'
																																																			ELSE
																																																				'0'																							
																																																			END)
																																																---Without ('RFQ','RFQU','RFQL','DP','LTM')
																																									ELSE
																																											'0'	
																																								END)
																																						-- Grade 5 Approver
																																			END)
																																	----
																									END)
																	END)
											
										END)
								-- Revenue Budget
							END )
						----Corporation Applicable
					END
					
					)
		END 	
end)
----- Financial deligation
IsApprovedByProperAA, --done /* Modified by Dohatec */
case	when (select COUNT(distinct wfFileOnHandId)  from tbl_WorkFlowFileHistory where activityId = 13 
			and wfFileOnHandId =(select MAX(wfFileOnHandId) from tbl_WorkFlowFileHistory 
			where activityId = 13 and objectId = td.tenderId)) = 0
			then case when ((select count(tenderId) from tbl_EvalRptSentToAA ea where ea.tenderId=td.tenderId and rptApproveDt is not  null) > 0) 
			then	'1'	else	'0' end
		 when (select COUNT(distinct wfFileOnHandId)  from tbl_WorkFlowFileHistory where activityId = 13 
			and wfFileOnHandId =(select MAX(wfFileOnHandId) from tbl_WorkFlowFileHistory 
			where activityId = 13 and objectId = td.tenderId)) > 0 
			then 
				case	when (select count(distinct wfRoleid) from tbl_WorkFlowFileHistory wfl 
						inner join tbl_EvalRptSentToAA ersa on wfl.objectId = ersa.tenderId and activityId= 13 and rptStatus !='Pending' and rptApproveDt is not  null
						where   roundId = ( select MAX(roundId) from tbl_EvalRptSentToAA where rptStatus != 'Pending' and rptApproveDt is not  null and tenderId = td.tenderId)
						and tenderId = td.tenderId	GROUP BY objectId)<3 then '1'							
						when (select count(distinct wfRoleid) from tbl_WorkFlowFileHistory wfl 
						inner join tbl_EvalRptSentToAA ersa on wfl.objectId = ersa.tenderId and activityId= 13 and rptStatus !='Pending' and rptApproveDt is not  null
						where   roundId = ( select MAX(roundId) from tbl_EvalRptSentToAA where rptStatus != 'Pending' and rptApproveDt is not  null and tenderId = td.tenderId)
						and tenderId = td.tenderId	GROUP BY objectId)=3 then
							case	when (select COUNT(worfklowId) from tbl_WorkFlowLevelConfig where wfRoleId = 2 and activityId = 13 and procurementRoleId in (3,7) and objectId = td.tenderId) >0 then '1' 
									else
									case when (select COUNT(distinct procurementRoleId) from tbl_WorkFlowLevelConfig where wfRoleId = 3 and activityId = 13 and procurementRoleId not in (7)  and objectId = td.tenderId) > 0  then '0' else '1' end 
							end
				else		
					'0'		
				end 
else	'0' end SubofEvalReportAA, --Modified By Dohatec
case	when
			(select COUNT(distinct wfFileOnHandId)  from tbl_WorkFlowFileHistory where activityId = 13 
			and wfFileOnHandId =(select MAX(wfFileOnHandId) from tbl_WorkFlowFileHistory 
			where activityId = 13 and objectId = td.tenderId)) > 0 
							then 
							case  when	(select count(distinct wfRoleid) from tbl_WorkFlowFileHistory wfl 
										inner join tbl_EvalRptSentToAA ersa on wfl.objectId = ersa.tenderId 
										and activityId= 13 and rptStatus !='Pending'
										where   roundId = ( select MAX(roundId) from tbl_EvalRptSentToAA where rptStatus != 'Pending' and tenderId = td.tenderId)
										and tenderId = td.tenderId	GROUP BY objectId)=3
										then 
											case when (select COUNT(worfklowId) from tbl_WorkFlowLevelConfig where wfRoleId = 2 and activityId = 13 and procurementRoleId in (3,7) and objectId = td.tenderId) >0 then '0' 
											else case when  (select COUNT(distinct procurementRoleId) from tbl_WorkFlowLevelConfig where wfRoleId = 3 and activityId = 13 and procurementRoleId not in (7)  and objectId = td.tenderId) > 0 
															then '1' 
															else '0' 
														end 
											end 	
										else	'0'	end 
else	'0' end as TERReviewByOther,  --done /* Modified By Dohatec*/
case
	WHEN
		(SELECT COUNT(tenderId) FROM tbl_EvalRptSentToAA ea WHERE ea.tenderId = td.tenderId) = 0
	THEN
		'0'
	ELSE
		CASE WHEN
			(SELECT COUNT(*) FROM tbl_WorkFlowFileHistory wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13) = 0
		THEN
			'0'
		ELSE			
			(CASE 
				WHEN 
					--	Actual AA Rank = 
					(select RankId from tbl_DelegationDesignationRank 
					where ApprovingAuthority = (SELECT procurementRole FROM tbl_ProcurementRole
					WHERE procurementRoleId in (select procurementRoleId from tbl_WorkFlowLevelConfig 
					where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId)))
					
					>		
			
					-- Desire AA Rank = 
					(select MIN(RankId) from tbl_DelegationDesignationRank 
					where ApprovingAuthority in (select fd.ApprovingAuthority from tbl_FinancialDelegation fd
					where (select distinct(amount) from tbl_BidderRank 
					where userId = (select userId from tbl_EvalRoundMaster 
					where tenderId = td.tenderId and reportType = 'L1' 
					and roundId = (select MAX(roundId) from tbl_EvalRoundMaster 
					where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId and roundId = (select MAX(roundId) from tbl_EvalRoundMaster where tenderId = td.tenderId and reportType = 'L1'))  
					between fd.MinValueBDT and fd.MaxValueBDT and fd.BudgetType = td.budgetType and fd.ProcurementMethod = td.procurementMethod
					and fd.ProcurementNature = fd.ProcurementNature and fd.ProcurementType = td.procurementType	and fd.TenderType = td.eventType
					and fd.TenderEmergency = td.pkgUrgency
					and dbo.f_get_BoDUserCheck(td.tenderId,fd.IsBoD)=1
					and dbo.f_get_ProjectCostWiseCheck(td.tenderId,fd.MinProjectValueBDT,fd.MaxProjectValueBDT)=1))
									
				THEN '1'
				ELSE '0'
				END)
		END
end ContractAppByHighAuth

from tbl_TenderDetails td,tbl_DepartmentMaster dm 

where tenderStatus not in('Pending')
and td.departmentId=dm.departmentId
and submissionDt < GETDATE())T
WHERE tenderId = T.DerivedTenderID
and EvaluationTime !='NA'
and DaysBtwnContappNOA ='NA'
;


-- Indicator 22
WITH CTE2 AS (select tp.tenderId,
case when DaysBtwnTenEvalTenApp = 'NA'   Then '0'
	else (case	when (select count(worfklowId) from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = tp.tenderId)>0
				then ( ----------------- Contract Approval Workflow is Created
					case	when (SELECT procurementRole FROM tbl_ProcurementRole WHERE procurementRoleId in ( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = tp.tenderId))='CCGP'
							then (case when (select ISNULL(MIN(rptApproveDt),(select MIN(rptApproveDt) from tbl_EvalRptSentToAA where tenderId = tp.tenderId and envelopeId = 0 )) from tbl_EvalRptSentToAA where tenderId = tp.tenderId and envelopeId = 1)<=convert(date,dbo.f_gettendervaliditydate(tp.tenderId))
										then '1'	else '0'	end	)  ----------------- APPROVING AUTHORITY IS CCGP
							else ( ----------------- APPROVING AUTHORITY IS NOT CCGP
									case	when (select COUNT(committeeId) from tbl_Committee where committeeType = 'TSC' and committeStatus ='approved' and tenderId = tp.tenderid) >0 
											then ( 	case when (select ISNULL(NoOfDaysForApproval,0) from tbl_ProcurementApprovalTimeline pat inner join 
															( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in 
															( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = tp.tenderId))T
															on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 0)>=DaysBtwnTenEvalTenApp 
													then 	'1' else '0' -- 'TSC REQUIRED'
												end)											 
											else  (	case when (select ISNULL( NoOfDaysForApproval,0) from tbl_ProcurementApprovalTimeline pat inner join 
															( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in 
															( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = tp.tenderId))T
															on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC = 0) >=DaysBtwnTenEvalTenApp
													then '1'	else '0' -- 'TSC NOT REQUIRED'
												end)	
										end ) ----------------- APPROVING AUTHORITY IS NOT CCGP						
				end )----------------- Contract Approval Workflow is Created	
				else (----------------- Contract Approval Workflow is Not Created
					case	when (SELECT procurementRole FROM tbl_ProcurementRole WHERE procurementRoleId in ( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in ( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = tp.tenderid)))='CCGP'
							then (case when (select ISNULL(MIN(rptApproveDt),(select MIN(rptApproveDt) from tbl_EvalRptSentToAA where tenderId = tp.tenderId and envelopeId = 0 )) from tbl_EvalRptSentToAA where tenderId = tp.tenderId and envelopeId = 1)<=convert(date,dbo.f_gettendervaliditydate(tp.tenderId))
								then '1'	else '0'	end	) ----------------- APPROVING AUTHORITY IS CCGP
							else (	 ----------------- APPROVING AUTHORITY IS NOT CCGP						
									case	when (select COUNT(committeeId) from tbl_Committee where committeeType = 'TSC' and committeStatus ='approved' and tenderId = tp.tenderid) >0
											then ( 	case when	( SELECT ISNULL(NoOfDaysForApproval,0) from tbl_ProcurementApprovalTimeline pat inner join 
																( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in ( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in 
																( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = tp.tenderid)))T	on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 0) >=DaysBtwnTenEvalTenApp
														then '1'	else '0' -- 'TSC  REQUIRED'
											end)
									
									else 
									(  case when	(select ISNULL(NoOfDaysForApproval,0) from tbl_ProcurementApprovalTimeline pat inner join 
													( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in 
													( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in 
													( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = tp.tenderid)))T
														on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC = 0)>=DaysBtwnTenEvalTenApp
													then '1'	else '0' --'TSC NOT REQUIRED'
									end	)
							end) ----------------- APPROVING AUTHORITY IS NOT CCGP									
					end )----------------- Contract Approval Workflow is Not Created
	end )
end AS ApprovalByAAInTime
from Tbl_PromisIndicator tp inner join tbl_TenderDetails td on tp.tenderId=td.tenderId)

UPDATE Tbl_PromisIndicator SET AppofContractByAAInTimr = ApprovalByAAInTime FROM CTE2 WHERE CTE2.tenderId = Tbl_PromisIndicator.tenderId;
END

