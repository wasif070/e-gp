USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_view_mailmessage]    Script Date: 4/24/2016 11:21:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: View Message Box.
--
--
-- Author: Rajesh
-- Date: 25-10-2010
--
--
-- SP Name:  p_view_mailmessage
-- Module:   Message Box
-- Function: Store Procedure is use for get message details for current user's Message Box.
--------------------------------------------------------------------------------
-- Inbox:	Get Message list from Inbox.
-- Sent:	Get Message list from Send Items.
-- Draft:	Get Message list from Draf Messages.
-- Trash:	Get Message list from Trash Messages.
--------------------------------------------------------------------------------

--[p_ViewMessageBox] 'Inbox','1','0','Active'
ALTER PROCEDURE [dbo].[p_view_mailmessage] --'Inbox','1','0','Active'
	-- Add the parameters for the stored procedure here
	@v_MsgBoxTypeVc varchar(20),
	@v_UserIdN int, 
	@v_FolderIdN int,
	@v_msgStatusVc varchar(10),  --(archive,Live,trash)
	@v_PagePerRecordN int=10,
	@v_PageN int=1,
	@v_TotalPages int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF @v_FolderIdN=0
		BEGIN
			IF @v_MsgBoxTypeVc='Inbox'
				BEGIN
					
					DECLARE @v_Reccountf FLOAT
					DECLARE @v_TotalPagef FLOAT
					SELECT @v_Reccountf = Count(*) From (
					SELECT * From (SELECT ROW_NUMBER() OVER (order by m.[msgId] desc) As Rownumber,m.[msgId]
					FROM tbl_Message m,tbl_MessageInBox mi 
					WHERE m.msgId=mi.msgId AND msgToUserId=@v_UserIdN AND mi.msgStatus=@v_msgStatusVc AND folderId= 0) AS DATA) AS TTT
					
					SET @v_TotalPagef =CEILING(@v_Reccountf/@v_PagePerRecordN)
					SELECT *,@v_TotalPagef as TotalPages,@v_Reccountf as TotalRecords From (SELECT ROW_NUMBER() OVER (order by m.[msgId] desc) As Rownumber,m.[msgId],m.[subject]
							,m.[msgText],mi.[msgBoxId],mi.[msgToUserId],mi.[msgFromUserId]
							,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail 
							,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
							,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
							,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded],mi.[processUrl]
					FROM tbl_Message m,tbl_MessageInBox mi 
					WHERE m.msgId=mi.msgId AND msgToUserId=@v_UserIdN AND mi.msgStatus=@v_msgStatusVc AND folderId= 0) AS DATA where Rownumber between  (@v_PageN - 1) * @v_PagePerRecordN + 1 
							AND @v_PageN * @v_PagePerRecordN
					
				END
			ElSE IF @v_MsgBoxTypeVc='Sent'
				BEGIN
				
					SELECT @v_Reccountf = Count(*) From (
					SELECT * From (SELECT ROW_NUMBER() OVER (order by m.[msgId] desc) As Rownumber,m.[msgId]
					FROM tbl_Message m,tbl_MessageSent mi
					WHERE m.msgId=mi.msgId AND msgFromUserId=@v_UserIdN AND mi.msgStatus=@v_msgStatusVc AND folderId= 0) AS DATA) AS TTT
					
					SET @v_TotalPagef =CEILING(@v_Reccountf/@v_PagePerRecordN)
					
					SELECT *,@v_TotalPagef as TotalPages,@v_Reccountf as TotalRecords From (SELECT ROW_NUMBER() OVER (order by m.[msgId] desc) As Rownumber,m.[msgId]
							,m.[subject],m.[msgText],mi.[msgSentId],mi.[msgToUserId],mi.[msgFromUserId]
							,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail 
							,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
							,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate],mi.[folderId],mi.[isPriority]
							,mi.[isMsgRead],mi.[isDocUploaded]
					FROM tbl_Message m,tbl_MessageSent mi 
					WHERE m.msgId=mi.msgId AND msgFromUserId=@v_UserIdN AND mi.msgStatus=@v_msgStatusVc AND folderId= 0) AS DATA where Rownumber between  (@v_PageN - 1) * @v_PagePerRecordN + 1 
							AND @v_PageN * @v_PagePerRecordN
					
				END
			ELSE IF @v_MsgBoxTypeVc='Draft'
				BEGIN
					SELECT @v_Reccountf = Count(*) From (
					SELECT * From (SELECT ROW_NUMBER() OVER (order by m.[msgId] desc) As Rownumber,m.[msgId]
					FROM tbl_Message m,tbl_MessageDraft mi  
					WHERE m.msgId=mi.msgId AND msgToUserId=@v_UserIdN AND mi.msgStatus=@v_msgStatusVc AND folderId=0) AS DATA) AS TTT
					
					SET @v_TotalPagef =CEILING(@v_Reccountf/@v_PagePerRecordN)
					
					SELECT *,@v_TotalPagef as TotalPages,@v_Reccountf as TotalRecords From (SELECT ROW_NUMBER() OVER (order by m.[msgId] desc) As Rownumber,m.[msgId]
							,m.[subject],m.[msgText],mi.[msgDraftId],mi.[msgToUserId],mi.[msgFromUserId]
							,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail 
							,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
							,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate],mi.[folderId],mi.[isPriority]
							,mi.[isMsgRead],mi.[isDocUploaded]
					FROM tbl_Message m,tbl_MessageDraft mi 
					WHERE m.msgId=mi.msgId AND msgToUserId=@v_UserIdN AND mi.msgStatus=@v_msgStatusVc AND folderId=0) AS DATA where Rownumber between  (@v_PageN - 1) * @v_PagePerRecordN + 1 
							AND @v_PageN * @v_PagePerRecordN
				
				END
			ELSE IF @v_MsgBoxTypeVc='Trash'
				BEGIN
					SELECT @v_Reccountf = Count(*) From (
					SELECT * From (SELECT ROW_NUMBER() OVER (order by m.[msgId] desc) As Rownumber,m.[msgId]
					FROM tbl_Message m,tbl_MessageTrash mt  
					WHERE m.msgId=mt.msgId AND msgToUserId=@v_UserIdN AND mt.msgStatus=@v_msgStatusVc AND folderId=0) AS DATA) AS TTT
					
					SET @v_TotalPagef =CEILING(@v_Reccountf/@v_PagePerRecordN)
					
					SELECT *,@v_TotalPagef as TotalPages,@v_Reccountf as TotalRecords From (SELECT ROW_NUMBER() OVER (order by m.[msgId] desc) As Rownumber,m.[msgId]
							,m.[subject],m.[msgText],mt.[msgTrashId],mt.[msgToUserId],mt.[msgFromUserId]
							,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mt.[msgToUserId]) as ToEmail 
							,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mt.[msgFromUserId]) as FromEmail
							,mt.[msgToCCReply],mt.[msgStatus],mt.[creationDate],mt.[folderId],mt.[isPriority]
							,mt.[isMsgRead],mt.[isDocUploaded]
					FROM tbl_Message m,tbl_MessageTrash mt 
					WHERE m.msgId=mt.msgId AND msgToUserId=@v_UserIdN AND mt.msgStatus=@v_msgStatusVc AND folderId=0) AS DATA where Rownumber between  (@v_PageN - 1) * @v_PagePerRecordN + 1 
							AND @v_PageN * @v_PagePerRecordN
				END
		END
	ELSE
		BEGIN
					
					SELECT @v_Reccountf = Count(*) From (
					SELECT * From (SELECT ROW_NUMBER() OVER (order by m.[msgId] desc) As Rownumber,m.[msgId]
					FROM tbl_Message m,tbl_MessageInBox mi 
					WHERE m.msgId=mi.msgId AND msgToUserId=@v_UserIdN AND mi.msgStatus=@v_msgStatusVc AND mi.[folderId]=@v_FolderIdN) AS DATA) AS TTT
					
					SET @v_TotalPagef =CEILING(@v_Reccountf/@v_PagePerRecordN)
					
					SELECT *,@v_TotalPagef as TotalPages,@v_Reccountf as TotalRecords From (SELECT ROW_NUMBER() OVER (order by m.[msgId] desc) As Rownumber,m.[msgId],m.[subject]
							,m.[msgText],mi.[msgBoxId],mi.[msgToUserId],mi.[msgFromUserId]
							,(select emailid  from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgToUserId]) as ToEmail 
							,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userid=mi.[msgFromUserId]) as FromEmail
							,mi.[msgToCCReply],mi.[msgStatus],mi.[creationDate]
							,mi.[folderId],mi.[isPriority],mi.[isMsgRead],mi.[isDocUploaded],mi.[processUrl]
					FROM tbl_Message m,tbl_MessageInBox mi 
					WHERE m.msgId=mi.msgId AND msgToUserId=@v_UserIdN AND mi.msgStatus=@v_msgStatusVc AND mi.[folderId]=@v_FolderIdN) AS DATA where Rownumber between  (@v_PageN - 1) * @v_PagePerRecordN + 1 
							AND @v_PageN * @v_PagePerRecordN
			
		END
END

