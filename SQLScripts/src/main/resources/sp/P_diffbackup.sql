USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[P_diffbackup]    Script Date: 4/24/2016 10:45:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER    proc [dbo].[P_diffbackup]
as
set nocount on
DECLARE @DBName varchar(255)
DECLARE @DS VARCHAR(50)
DECLARE @Path VARCHAR(255)
DECLARE DiffBackup 
CURSOR FOR SELECT name from sys.databases WHERE name  IN ('egp_stg')AND state = 0 --Exclude offline databases, they won't backup if they offline anywayAND Source_database_id is null -- Removes snapshots from the databases returned, these can't be backed up eith
OPEN DiffBackup
FETCH NEXT FROM DiffBackup
INTO @DBName-- Check @@FETCH_STATUS to see if there are any more rows to fetch.
WHILE @@FETCH_STATUS = 0
BEGIN--Set the filename values of the backup files
SET @DS = REPLACE(CONVERT(VARCHAR(10), GETDATE(), 111), '/', '') + '_'    + REPLACE(CONVERT(VARCHAR(8), GETDATE(), 108), ':', '')
SET @Path = 'd:\backup\egp_stgdiff\'SET @Path = @path + @DBNAME + '_' + @DS + '.diffbak'--Take the backup
BACKUP DATABASE @DBNAME TO DISK = @Path  WITH   Differential, INIT,  SKIP, NOREWIND,    NOUNLOAD, STATS = 10    
FETCH NEXT FROM DiffBackup    INTO @DBName
END
CLOSE DiffBackup
DEALLOCATE DiffBackup


---exec P_diffbackup

