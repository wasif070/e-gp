USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_import_promisdata_category_1_category_2_category_3]    Script Date: 4/24/2016 11:04:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- Modified By Dohatec on 10th August 2015 To Segregate Promis data import basis of Indicator Category 
-- =============================================
ALTER PROCEDURE [dbo].[p_import_promisdata_category_1_category_2_category_3]
AS
BEGIN
 
INSERT INTO [Tbl_PromisIndicator]
           ([officeId]
           ,[PEOffice_Name]
           ,[tenderId]
           ,[PublishNewspaper]
           ,[PublishCPTUWebsite]
	   ,[GoBProcRules]
	   ,[DPRules]
           ,[MultiLocSubmission]
           ,[DaysBetTenPubTenderClosing]
           ,[SuffTenderSubTime]
           ,[TenderDocSold]
           ,[TenderersPartCount]
           ,[TenderSubVSDocSold]
           ,[TOCMemberFromTEC]
           ,[TECFormedByAA]
           ,[TECExternalMembers]
           ,[EvaluationTime]
           ,[EvalCompletedInTime]
           ,[ResponsiveTenderersCount]
           ,[IsReTendered]
           ,[IsTenderCancelled]
           ,[DaysBtwnTenEvalTenApp]
           ,[IsApprovedByProperAA]
           ,[SubofEvalReportAA]
           ,[AppofContractByAAInTimr]
           ,[TERReviewByOther]
           ,[ContractAppByHighAuth]
           ,[DaysBtwnContappNOA]
           ,[DaysBtwnTenOpenNOA]
           ,[DaysBtwnIFTNOA]
           ,[AwardPubCPTUWebsite]
           ,[AwardedInTenValidityPeriod]
           ,Is_Progress,Is_TenderOnline, modify_date,
           Ministry_ID,Division_ID,Org_ID,
           [prmsPECode]
           --- Added 20th Jan 2012 yagnesh ---
           ,[DeliveredInTime]
           ,[LiquidatedDamage]
           ,[FullyCompleted]
           ,[DaysReleasePayment]
           ,[LatePayment]
           ,[InterestPaid]
           ,[ComplaintReceived]
           ,[ResolAwardModification]
           ,[ComplaintResolved]
           ,[RevPanelDecUpheld]
           ,[IsContAmended]
           ,[ContUnresolvDispute]
           ,[DetFraudCorruption]
           --- Added 20th Jan 2012 yagnesh ---
           )
           
select officeId,peOfficeName,tenderId,
case
	when
	--(select COUNT(ta.tenderId) from tbl_TenderAdvertisement ta where ta.tenderId = td.tenderId) != 0
	(select 1 as cnt) = 0
	then
	'100'	--Added 100 conditon on 23rdJan2012 
	else
	case 
		when 
		(select COUNT(tenderAdvtId) from tbl_TenderAdvertisement ta
		where newsPaperPubDt is not null and ta.tenderId=td.tenderId ) = 0
		then 
		'0'
		else
		'1'
	end
end PublishNewspaper,	--done
1 PublishCPTUWebsite,	--done
case
	when
	(select COUNT(ts.templateId) from tbl_TenderStd ts where ts.tenderId = td.tenderId) = 0
	then
	'100'	--Added 100 conditon on 23rdJan2012
	else
		case
			when 
			(select count(tm.stdFor) from tbl_TenderStd ts, tbl_TemplateMaster tm where ts.templateId = tm.templateId 
			and ts.tenderId = td.tenderId and tm.stdFor = 'Government of Bangladesh') > 0
			then
			'1'
			else 
			'0'
		end 
end GoBProcRules,		-- done on 23rdJan2012 yagnesh
case
	when
	(select COUNT(ts.templateId) from tbl_TenderStd ts where ts.tenderId = td.tenderId) = 0
	then
	'100'
	else
		case
			when 
			(select count(tm.stdFor) from tbl_TenderStd ts, tbl_TemplateMaster tm where ts.templateId = tm.templateId 
			and ts.tenderId = td.tenderId and tm.stdFor = 'Development Partner') > 0
			then
			'1'
			else 
			'0'
		end
end DPRules,			-- done on 23rdJan2012 yagnesh
0 MultiLocSubmission,   --done
DATEDIFF(D,tenderPubDt,submissionDt) DaysBetTenPubTenderClosing,	--done
1 SuffTenderSubTime,	--done
(select COUNT(userId) from tbl_TenderPayment tp 
where paymentFor='Document Fees' and  status='paid' and isLive='yes' and isVerified='yes' 
and tp.tenderId=td.tenderId ) TenderDocSold, --done
(select COUNT(userId) from tbl_FinalSubmission ta
where ta.bidSubStatus='finalsubmission' and ta.tenderId=td.tenderId )
 TenderersPartCount,	--done
convert(varchar(20),(select COUNT(userId) from tbl_FinalSubmission ta
where ta.bidSubStatus='finalsubmission' and ta.tenderId=td.tenderId ) )
+':'+convert(varchar(20),(select COUNT(userId) from tbl_TenderPayment tp where paymentFor='Document Fees' and  status='paid' and isLive='yes' and isVerified='yes' and tp.tenderId=td.tenderId )) TenderSubVSDocSold, --done
1 TOCMemberFromTEC, --done
----------	Commented By Dohatec for accurate result	1 TECFormedByAA,	--done
case when (select COUNT(*) from tbl_ProcurementRole where procurementRoleId = 
(select approvingAuthEmpId from tbl_AppPackages where packageId  = (
select packageId from tbl_TenderMaster  where tenderId = td.tenderId)) 
and procurementRole in ('Minister','CCGP'))> 0 
then 
case WHEN	(select COUNT(*) from  		(select distinct objectId,userId from tbl_WorkFlowLevelConfig 
		where moduleId = 2 and eventId=6 and activityId = 6 and wfRoleId = 2 and objectId in
		(select distinct tenderId from tbl_Committee tc where committeeType IN ( 'TEC','PEC' ) and isCurrent = 'YES'
		and tenderId = td.tenderId)) T where T.objectId=td.tenderId and T.userId = td.approvingAuthId)=1
		THEN '1'
	WHEN	(select COUNT(*) from  (select distinct objectId,userId from tbl_WorkFlowLevelConfig 
		where moduleId = 2 and eventId=6 and activityId = 6 and wfRoleId = 2 and procurementRoleId in (2,3) and objectId in
		(select distinct tenderId from tbl_Committee tc where committeeType IN ( 'TEC','PEC' ) and isCurrent = 'YES'
		and tenderId = td.tenderId)) T where T.objectId=td.tenderId)=1
		THEN '1'
ELSE
	'0'
 end
else 
case WHEN	(select COUNT(*) from  
		(select distinct objectId,userId from tbl_WorkFlowLevelConfig 
		where moduleId = 2 and eventId=6 and activityId = 6 
		and wfRoleId = 2 and objectId in
		(select distinct tenderId from tbl_Committee tc 
		where committeeType IN ( 'TEC','PEC' ) and isCurrent = 'YES'
		and tenderId = td.tenderId)) T 
		where T.objectId=td.tenderId and T.userId = td.approvingAuthId)=1
		THEN '1'
ELSE
	'0'
 end
 end TECFormedByAA , -- Modified By Dohatec
case
	when
		(selecT COUNT(cm.comMemberId) from tbl_Committee c, tbl_CommitteeMembers cm 
			where c.committeeId = cm.committeeId 
			and cm.memberFrom = 'Other PE' and tenderId = td.tenderId and (c.committeeType = 'TEC' or c.committeeType = 'PEC')) >= 2 
	then
		'1'	
	else
		'0'
end TECExternalMembers, --done
'NA' as EvaluationTime,  --done, 
'0' as EvalCompletedInTime , --end  -- Modified By Dohatec 
'NA' as ResponsiveTenderersCount,	--done
--Edited by Palash, Dohatec
0	as IsReTendered,	--done
0 as IsTenderCancelled,	--done
'NA' as DaysBtwnTenEvalTenApp, --Modified By Dohatec	
0 as IsApprovedByProperAA, --done /* Modified by Dohatec */
'0' as SubofEvalReportAA, --Modified By Dohatec
'0' as AppofContractByAAInTimr, --done --Modified By Dohatec
'0' as TERReviewByOther,  --done /* Modified By Dohatec*/
'0' as ContractAppByHighAuth,	--done
'NA' as DaysBtwnContappNOA,	--done /* Modified By Dohatec*/
'NA' as DaysBtwnTenOpenNOA,	--done /* Modified By Dohatec*/
'NA' as DaysBtwnIFTNOA, --done /* Modified By Dohatec*/
100 as AwardPubCPTUWebsite,		--Added on 24th Jan 2012 yagnesh  /* Modified By Dohatec*/
100 as AwardedInTenValidityPeriod,  -- done  /* Modified By Dohatec*/
'N' as Is_Progress,	-- done on 9th Feb 2012
'Y' as Is_TenderOnline,
getdate() modify_date,

case 
	when 
		departmentType='Ministry'
	then 
		dm.departmentId
	when 
		departmentType='division'
		then 
		(select isnull(departmentid,0) from tbl_departmentmaster where departmentid in(select parentDepartmentId from tbl_departmentmaster where departmentId= dm.departmentId)  )
		else
		case 
		when (select td1.departmentType  from tbl_departmentmaster  td1 where departmentid in(select parentDepartmentId from tbl_departmentmaster where departmentid in(select parentDepartmentId from tbl_departmentmaster where departmentId= dm.departmentId))) like 'Cabinet%'
			then
				(select isnull(departmentid,0) from tbl_departmentmaster where departmentid in(select parentDepartmentId from tbl_departmentmaster where departmentId= dm.departmentId)  )
			else
				(select isnull(departmentid,0)  from tbl_departmentmaster where departmentid in(select parentDepartmentId from tbl_departmentmaster where departmentid in(select parentDepartmentId from tbl_departmentmaster where departmentId= dm.departmentId)))
			end
end as ministry_id,
case 
	when 
		departmentType='division'
	then 
		dm.departmentId
	else
		case 
		when (select td1.departmentType  from tbl_departmentmaster  td1 where departmentid in (select parentDepartmentId from tbl_departmentmaster where departmentId= dm.departmentId)) like 'Division' 
		then
		(select isnull(departmentid,0) from tbl_departmentmaster where departmentid in(select parentDepartmentId from tbl_departmentmaster where departmentId= dm.departmentId) )
		else
		'0'
		end
end as division_id,
case 
	when 
		departmentType='organization'
	then 
		dm.departmentId
	else
		0
end as org_id,
(select prmsPECode from tbl_OfficeMaster om where om.officeName = td.peOfficeName
  and om.departmentId = td.departmentId) as prmsPECode,

100 as DeliveredInTime,
100 as LiquidatedDamage,
100 as FullyCompleted,
'NA' as DaysReleasePayment,		--done - Added on 24th Jan 2012 
100 as LatePayment,
100 as InterestPaid,
'0' as ComplaintReceived,
'0' as ResolAwardModification, --done
100 as ComplaintResolved,	--done on 9th Feb 2012 if at level 4 rejected then resolved
100 as RevPanelDecUpheld,
100 as IsContAmended,
100 as ContUnresolvDispute,	--Added on 23rdJan2012 yagnesh
'0' DetFraudCorruption		--Default No
--- Added 20th Jan 2012 Yagnesh -------

from tbl_TenderDetails td,tbl_DepartmentMaster dm 

where tenderStatus not in('Pending')
and td.departmentId=dm.departmentId
and submissionDt < GETDATE()
and tenderId not in (select distinct tenderId from Tbl_PromisIndicator)


END
