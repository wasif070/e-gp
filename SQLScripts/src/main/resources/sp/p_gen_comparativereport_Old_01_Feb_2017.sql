USE [pppd_bhutan_27092016]
GO
/****** Object:  StoredProcedure [dbo].[p_gen_comparativereport]    Script Date: 10/31/2016 10:14:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- alter date: <alter Date,,>
-- Description:	<Description,,>
-- =============================================
-- SP Name	   :   p_gen_Comparativereport
-- Module	   :   Opening/Evaluation (TOC/TEC or POC/PEC User)
-- Description :   After the Tender has opened and all the Price Bid Forms are
--                 Decrypted by Opening/Evaluation Committee CP Comparative is Generated.
-- Function	   :   N.A.

-- exec [dbo].[p_gen_Comparativereport]  11675
ALTER PROCEDURE [dbo].[p_gen_comparativereport]
	-- Add the parameters for the stored procedure here
	@v_formId_inInt INT,
	@v_userId_inInt INT=0

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets FROM interfering with SELECT statements.
	SET NOCOUNT ON;
	---- PARAMETERS DECLARATION ----------
DECLARE  @v_IsPriceBid varchar(10)='',@v_FormHeader varchar(2000)='',@v_FormFooter varchar(2000)='',@v_TableNames_Vc VARCHAR(8000),  @v_FormName VARCHAR(1500), @v_TableId_Int int, @v_FormTableHeader VARCHAR(max),  @v_RowId_Int int,  @v_RowCellValue VARCHAR(max), @v_CellHtml_Vc VARCHAR(max),  @v_RowHtml_Vc VARCHAR(max),  @v_colfill_Int int, @v_FormfinalTableHeader VARCHAR(max), @v_columnhead_Vc VARCHAR(max),@v_columnhead2_Vc VARCHAR(max),@v_ColId_int Int, @v_TableNa_Vc varchar(5000), @v_MainHeader_Vc varchar(max), @v_CmpName_Vc varchar(5000), @v_MainColspan_vc varchar(10), @v_ListBoxName_Vc varchar(100), @v_IsProcurementTypeIct bit = 0, @v_CellId_int int, @v_datatype_Int int,@v_TotRowId_Int int=0,@v_TotColId_Int int=0,@v_ColSpn_Int int=0,@v_TotDispColId int=0,@v_tId int=0,@v_dformId int


declare @v_temptable table
(
	[tenderColId] [int]  NOT NULL,
	[tenderTableId] [int] NOT NULL,
	[columnId] [smallint] NOT NULL,
	[columnHeader] [varchar](500) NOT NULL,
	[dataType] [tinyint] NOT NULL,
	[filledBy] [tinyint] NOT NULL,
	[columnType] [varchar](15) NOT NULL,
	[sortOrder] [smallint] NOT NULL,
	[showorhide] [varchar](4) NOT NULL,
	[templateTableId] [int] NOT NULL,
	tenderformid int
)


select @v_tId=tenderId from tbl_TenderBidForm where tenderFormId=@v_formId_inInt
	/**** SELECT FORMNAME ******/

			--Dohatec: ICT Start
	IF((SELECT COUNT(tenderId) from tbl_tenderdetails WHERE tenderId = convert(int,@v_tId) and procurementType='ICT') != 0)
		Begin
			set @v_IsProcurementTypeIct = 1 --true
		End
	--Dohatec: ICT End

	SELECT @v_FormName = formName,@v_FormHeader=formHeader,@v_FormFooter=formFooter,@v_IsPriceBid=isPriceBid  FROM [tbl_TenderForms] WHERE tenderFormId = @v_formId_inInt
	Set @v_MainHeader_Vc = '<table border="0" cellpadding="10" cellspacing="0" width="100%" id="mtable1" class="tableList_1"><tr><th> '+@v_FormName+'</th></tr><tr><th> '+@v_FormHeader+'</th></tr><tr><td>'

	Set @v_FormTableHeader=''

	/**** SELECT TABLES IN FORMS ******/

	SELECT @v_TableNames_Vc=  COALESCE(@v_TableNames_Vc+', ', ' ') + convert(VARCHAR(20),tenderTableId)FROM [tbl_TenderTables] WHERE tenderFormId = @v_formId_inInt

	--SELECT @v_TableNames_Vc


	Set @v_CellHtml_Vc = ''
	set @v_RowHtml_Vc =''


	---added by dtec
	---determine whether the form is a discount form or not
	
	select @v_dformId = tenderFormId from tbl_TenderForms where formName = 'Discount Form' and tenderFormId = @v_formId_inInt

/**** FETCH DATA TABLE WISE  ******/

DECLARE cur_tblNames cursor FAST_FORWARD FOR SELECT items  FROM dbo.f_split(@v_TableNames_Vc,',')
			open cur_tblNames
			fetch next FROM cur_tblNames into @v_TableId_Int

if(@v_dformId = @v_formId_inInt)
Begin
	INSERT INTO @v_temptable
	select  top 2 t.*,
								f.tenderformid
						FROM	tbl_TenderColumns t,
								tbl_Tendertables f
						WHERE	t.tenderTableId=f.tendertableid
								and  tenderFormId  = @v_formId_inInt
								and t.tenderTableId = @v_TableId_Int
								
								and filledby not in(1)

end
else
begin
	INSERT INTO @v_temptable
	select   t.*,
								f.tenderformid
						FROM	tbl_TenderColumns t,
								tbl_Tendertables f
						WHERE	t.tenderTableId=f.tendertableid
								and  tenderFormId  = @v_formId_inInt
								and t.tenderTableId = @v_TableId_Int
								
								and filledby not in(1)
end

			print 'STEP 1 : First table Cursor'
			While @@Fetch_status = 0
			BEGIN

					print @v_TableId_Int
			/******* SET FIRST HEADER ************/
			SELECT  @v_TableNa_vc = tablename FROM	tbl_TenderTables WHERE tenderFormId = @v_formId_inInt and tendertableId = @v_TableId_Int
/* old brings all data

			SELECT @v_columnhead_Vc=  COALESCE(@v_columnhead_Vc+'', ' ') + item FROM
(SELECT CASE convert(VARCHAR(50),filledby )WHEN '1' THEN (SELECT '<td><b>'+columnHeader+'</b></td>')ELSE (SELECT '<td colspan="'+ convert(VARCHAR(500),(SELECT count(DISTINCT(bidid)) FROM tbl_TenderBidForm WHERE convert(VARCHAR(50), tenderFormId) = @v_formId_inInt))+'"><b>'+columnHeader+'</b></td>') END AS item FROM tbl_TenderColumns WHERE tendertableId = @v_TableId_Int)AS aa

*/
 If (select noOfEnvelops
		from tbl_ConfigEvalMethod cm ,tbl_TenderDetails td, tbl_TenderTypes tt
		where cm.procurementMethodId=td.procurementMethodId and
		cm.tenderTypeId=tt.tenderTypeId and tt.tenderType=td.eventType and cm.procurementNatureId=td.procurementNatureId
		and tenderId=@v_tId)=2 and @v_IsPriceBid='Yes'
	Begin
SELECT @v_ColSpn_Int=count(DISTINCT(tb.bidtableid)) FROM tbl_TenderBidForm tbf,
tbl_finalsubmission fs,tbl_TenderBidTable tb WHERE tbf.bidid=tb.bidid and  convert(VARCHAR(50), tbf.tenderFormId) = @v_formId_inInt and  tendertableId = @v_TableId_Int and  tbf.tenderId = fs.tenderId and fs.userId = tbf.userId and bidSubStatus = 'finalsubmission'
and fs.userId not in(select userId from tbl_EvalBidderStatus where tenderId=@v_tId and (bidderStatus='Technically Unresponsive' or ISNULL(result,'Pass')='Fail'))
end
else
begin
SELECT @v_ColSpn_Int=count(DISTINCT(tb.bidtableid)) FROM tbl_TenderBidForm tbf,
tbl_finalsubmission fs,tbl_TenderBidTable tb WHERE tbf.bidid=tb.bidid and  convert(VARCHAR(50), tbf.tenderFormId) = @v_formId_inInt and  tendertableId = @v_TableId_Int and  tbf.tenderId = fs.tenderId and fs.userId = tbf.userId and bidSubStatus = 'finalsubmission'

end

--if section added by dtec
if(@v_dformId = @v_formId_inInt)
BEGIN
  SELECT @v_columnhead_Vc=  COALESCE(@v_columnhead_Vc+'', ' ') + item FROM
	(SELECT top 2 columnId, CASE convert(VARCHAR(50),filledby )WHEN '1' THEN (SELECT '<th><b>'+columnHeader+'</b></th>')
	ELSE (SELECT '<th colspan="'+ convert(VARCHAR(500),@v_ColSpn_Int)+'"><b>'+columnHeader+'</b></th>') END
	AS item FROM tbl_TenderColumns WHERE tendertableId = @v_TableId_Int order by columnId desc)AS aa order by columnId 
END
ELSE
BEGIN
	SELECT @v_columnhead_Vc=  COALESCE(@v_columnhead_Vc+'', ' ') + item FROM
	(SELECT columnId, CASE convert(VARCHAR(50),filledby )WHEN '1' THEN (SELECT '<th><b>'+columnHeader+'</b></th>')
	ELSE (SELECT '<th colspan="'+ convert(VARCHAR(500),@v_ColSpn_Int)+'"><b>'+columnHeader+'</b></th>') END
	AS item FROM tbl_TenderColumns WHERE tendertableId = @v_TableId_Int)AS aa order by columnId -- changed by dtec 26/10/2016

END
Set @v_columnhead_Vc = '<tr>'+@v_columnhead_Vc+'</tr>'

Print 'First Header ' + @v_columnhead_Vc

			/******* END FIRST HEADER ************/

			/******* SET SECOND HEADER ************/

--SELECT @v_columnhead2_Vc=  COALESCE(@v_columnhead2_Vc+'', ' ') + item FROM
--(
--SELECT DISTINCT CASE WHEN tc.filledBy = 1 THEN '<td></td>' ELSE '<td><b>'+(SELECT distinct firstname FROM tbl_tendererMaster tm, tbl_TenderBidForm tf WHERE  tm.userId = tf.userId and tf.tenderFormId = @v_formId_inInt and tm.userid = vwd.userid)+'</b></td>' END AS item, tc.columnid FROM vw_get_userbiddata vwd, tbl_TenderColumns tc WHERE tc.tenderTableId = vwd.tendertableid
--)AS aa ORDER BY aa.columnId


/** -- OLD which brings all bidders' detail

SELECT @v_columnhead2_Vc=  COALESCE(@v_columnhead2_Vc+'', ' ') + Item FROM (SELECT '<td><b>'+'</b></td>' item,0 userid,sortOrder  FROM tbl_TenderColumns t,tbl_Tendertables f WHERE t.tenderTableId=f.tendertableid and  tenderFormId  = @v_formId_inInt and t.tenderTableId = @v_TableId_Int and filledby in(1)
   UNION all
 SELECT '<td><b>'+CASE WHEN companyname ='-' THEN firstname ELSE companyName END +'</b></td>' ,
 t.userId,sortorder FROM tbl_TenderBidForm t,tbl_TendererMaster tm,tbl_CompanyMaster cm, (select t.*,f.tenderformid  FROM tbl_TenderColumns t,tbl_Tendertables f WHERE t.tenderTableId=f.tendertableid and  tenderFormId  = @v_formId_inInt and t.tenderTableId = @v_TableId_Int and filledby not in(1)) tf WHERE t.userid=tm.userId and tf.tenderformid=t.tenderformid and tm.companyId=cm.companyid and   tf.tenderFormId  = @v_formId_inInt)a ORDER BY sortOrder, userId

 **/
  If (select noOfEnvelops
		from tbl_ConfigEvalMethod cm ,tbl_TenderDetails td, tbl_TenderTypes tt
		where cm.procurementMethodId=td.procurementMethodId and
		cm.tenderTypeId=tt.tenderTypeId and tt.tenderType=td.eventType and cm.procurementNatureId=td.procurementNatureId
		and tenderId=@v_tId)=2 and @v_IsPriceBid='Yes'
	Begin
			SELECT @v_columnhead2_Vc=  COALESCE(@v_columnhead2_Vc+'', ' ') + Item FROM (SELECT '<td><b>'+'</b></td>' item,0 userid,sortOrder  FROM tbl_TenderColumns t,tbl_Tendertables f WHERE t.tenderTableId=f.tendertableid and  tenderFormId  = @v_formId_inInt and t.tenderTableId = @v_TableId_Int and filledby in(1)
		    UNION all
			SELECT '<td><b>'+CASE WHEN companyname ='-' THEN firstname ELSE companyName END +'</b></td>' ,
			t.userId,sortorder FROM tbl_TenderBidForm t,tbl_TenderBidTable tb,tbl_TendererMaster tm,tbl_CompanyMaster cm, tbl_finalsubmission fs,(select t.*,f.tenderformid  FROM tbl_TenderColumns t,tbl_Tendertables f WHERE t.tenderTableId=f.tendertableid and  tenderFormId  = @v_formId_inInt and t.tenderTableId = @v_TableId_Int and filledby not in(1)) tf WHERE t.userid=tm.userId and tf.tenderformid=t.tenderformid and tm.companyId=cm.companyid and   tf.tenderFormId  = @v_formId_inInt
			and t.tenderId = fs.tenderId and t.bidId=tb.bidId  and   tb.tenderTableId=@v_TableId_Int
			and fs.userId = t.userId and bidSubStatus = 'finalsubmission'
			and fs.userId not in(select userId from tbl_EvalBidderStatus where tenderId=@v_tId and (bidderStatus='Technically Unresponsive' or ISNULL(result,'Pass')='Fail'))
			)a ORDER BY sortOrder, userId
	End
	Else
	Begin
			SELECT	@v_columnhead2_Vc=  COALESCE(@v_columnhead2_Vc+'', ' ') + Item
			FROM	(SELECT '<td><b>'+'</b></td>' item,
							0 userid,
							sortOrder
					FROM	tbl_TenderColumns t,
							tbl_Tendertables f
					WHERE	t.tenderTableId=f.tendertableid
							and  tenderFormId  = @v_formId_inInt
							and t.tenderTableId = @v_TableId_Int
							and filledby in(1)
			UNION all
			SELECT	'<td><b>'+CASE WHEN companyname ='-' THEN firstname ELSE companyName END +'</b></td>' AS item,
					t.userId,
					sortorder
			FROM	tbl_TenderBidForm t,
					tbl_TenderBidTable tb,
					tbl_TendererMaster tm,
					tbl_CompanyMaster cm,
					tbl_finalsubmission fs,
					@v_temptable tf
					
			WHERE	t.userid=tm.userId
					and tf.tenderformid=t.tenderformid
					and tm.companyId=cm.companyid
					and tf.tenderFormId  = @v_formId_inInt
					and t.tenderId = fs.tenderId
					and t.bidId=tb.bidId
					and  tb.tenderTableId=@v_TableId_Int
					and fs.userId = t.userId
					and bidSubStatus = 'finalsubmission')a
			ORDER BY sortOrder,userid,item
	End


Set @v_columnhead2_Vc = '<tr>'+@v_columnhead2_Vc+'</tr>'
Print '2nd Header ' + @v_columnhead2_Vc

			/******* END SECOND HEADER ************/

	/****BEGIN:*** SET COLSPAN FOR MAIN HEADER ***********/
	If (select noOfEnvelops
		from tbl_ConfigEvalMethod cm ,tbl_TenderDetails td, tbl_TenderTypes tt
		where cm.procurementMethodId=td.procurementMethodId and
		cm.tenderTypeId=tt.tenderTypeId and tt.tenderType=td.eventType and cm.procurementNatureId=td.procurementNatureId
		and tenderId=@v_tId)=2 and @v_IsPriceBid='Yes'
	Begin
		SELECT @v_MainColspan_vc = count(items) FROM(
		SELECT 0 Items  FROM tbl_TenderColumns t,tbl_Tendertables f WHERE t.tenderTableId=f.tendertableid and  tenderFormId  = @v_formId_inInt and t.tenderTableId = @v_TableId_Int and filledby in(1)
		UNION all
		SELECT t.userId as items FROM tbl_TenderBidForm t,tbl_TenderBidTable tb,tbl_TendererMaster tm,tbl_CompanyMaster cm, tbl_finalsubmission fs, (select t.columnId,f.tenderformid  FROM tbl_TenderColumns t,tbl_Tendertables f WHERE t.tenderTableId=f.tendertableid and  tenderFormId  = @v_formId_inInt and t.tenderTableId = @v_TableId_Int and filledby not in(1)) tf WHERE t.userid=tm.userId and tf.tenderformid=t.tenderformid and tm.companyId=cm.companyid and   tf.tenderFormId  = @v_formId_inInt
		and t.tenderId = fs.tenderId and fs.userId = t.userId and t.bidId=tb.bidId and bidSubStatus = 'finalsubmission'
		and fs.userId not in(select userId from tbl_EvalBidderStatus where tenderId=@v_tId and (bidderStatus='Technically Unresponsive' or ISNULL(result,'Pass')='Fail'))
		)a
	End
	Else
	Begin
		SELECT @v_MainColspan_vc = count(items) FROM(
		SELECT 0 Items  FROM tbl_TenderColumns t,tbl_Tendertables f WHERE t.tenderTableId=f.tendertableid and  tenderFormId  = @v_formId_inInt and t.tenderTableId = @v_TableId_Int and filledby in(1)
		UNION all
		SELECT t.userId as items FROM tbl_TenderBidForm t,tbl_TenderBidTable tb,tbl_TendererMaster tm,tbl_CompanyMaster cm, tbl_finalsubmission fs, (select t.columnId,f.tenderformid  FROM tbl_TenderColumns t,tbl_Tendertables f WHERE t.tenderTableId=f.tendertableid and  tenderFormId  = @v_formId_inInt and t.tenderTableId = @v_TableId_Int and filledby not in(1)) tf WHERE t.userid=tm.userId and tf.tenderformid=t.tenderformid and tm.companyId=cm.companyid and   tf.tenderFormId  = @v_formId_inInt
		and t.tenderId = fs.tenderId and fs.userId = t.userId and t.bidId=tb.bidId and bidSubStatus = 'finalsubmission'
		)a
	End

	/****END:*** SET COLSPAN FOR MAIN HEADER ***********/


			print '@v_MainColspan_vc'+@v_MainColspan_vc
	IF @v_FormfinalTableHeader <> ''
			BEGIN
			print 'in <>'
--					SET @v_FormTableHeader= @v_FormfinalTableHeader + '</table><table border="0" cellpadding="10" cellspacing="0" width="100%" id="table1" class="tableList_1"><tr><td colspan="'+(SELECT convert(VARCHAR(20), ((count(DISTINCT userId) * count (DISTINCT(columnid)))+ (SELECT count(DISTINCT(columnid)) FROM tbl_TenderColumns WHERE tenderTableId = @v_TableId_Int and filledBy = 1)))
--FROM vw_get_userbiddata ubd  WHERE ubd.tenderTableId = @v_TableId_Int) +'">'+@v_TableNa_vc+'</td></tr><tr>'

					SET @v_FormTableHeader= @v_FormfinalTableHeader + '</table><table border="0" cellpadding="10" cellspacing="0" width="100%" id="table1" class="tableList_1"><tr><td colspan="'+@v_MainColspan_vc +'">'+@v_TableNa_vc+'</td></tr><tr>'

	END

			ELSE
			BEGIN
			print 'in else <>'

					SET @v_FormTableHeader= @v_FormTableHeader + '<table border="0" cellpadding="10" cellspacing="0" width="100%" id="table1" class="tableList_1"><tr><td colspan="'+@v_MainColspan_vc+'">'+@v_TableNa_vc+'</td></tr>'
		END

			SET @v_FormfinalTableHeader=  @v_FormTableHeader + @v_columnhead_Vc + @v_columnhead2_Vc

			print @v_FormfinalTableHeader
			print 'TableID: ' + CONVERT(VARCHAR(200),@v_TableId_Int)


	/******** FETCH DATA ROW WISE *************/
	DECLARE cur_rowValues CURSOR FAST_FORWARD FOR
		SELECT distinct rowid FROM tbl_tenderbiddetail  WHERE [tenderTableId] =@v_TableId_Int  ORDER BY rowid
			OPEN cur_rowValues
			FETCH NEXT FROM cur_rowValues into @v_rowId_Int

			print 'STEP 2 : 2nd Row Cursor'
			print 'in rowid: '+ CONVERT(VARCHAR(20),@v_rowId_Int)

			WHILE @@FETCH_STATUS = 0
			BEGIN

				SET @v_RowHtml_Vc = @v_RowHtml_Vc + '<tr>'

 			print 'taking columns FROM [tbl_TenderColumns]'

 				/******** TAKE COLUMN ROW WISE ***********/

 			DECLARE cur_columnId CURSOR FAST_FORWARD FOR
 				SELECT DISTINCT columnid, filledby,datatype FROM tbl_TenderColumns WHERE [tenderTableId] =@v_TableId_Int order by columnid
 				OPEN cur_columnId
				FETCH NEXT FROM cur_columnId into @v_ColId_int , @v_colfill_Int,@v_datatype_Int

 				WHILE @@FETCH_STATUS = 0
			BEGIN

				print 'Rowid: ' + CONVERT(VARCHAR(50),@v_rowId_Int)
				print 'Colid: ' + CONVERT(VARCHAR(50),@v_ColId_int)
				print 'Filled By : ' + CONVERT(VARCHAR(50),@v_colfill_Int)

				IF @v_colfill_Int = 1
				BEGIN
					print 'taking data from tender cells'
					SELECT   @v_RowCellValue = case when cellvalue = 'null' then '' else isnull(cellvalue,'') end
					FROM vw_get_tendercolumndata WHERE tenderTableId =@v_TableId_Int and rowid = @v_rowId_Int and columnid = @v_ColId_int  order by rowid, columnid
					select  @v_TotRowId_Int=MAX(rowId)  ,@v_TotColId_Int=MAX(t.columnId)  from tbl_TenderFormula t,tbl_TenderCells c where
					formula like '%TOTAL%' and t.tenderTableId=@v_TableId_Int
					and t.tenderTableId=c.tenderTableId
					and t.columnId=c.columnId group by t.tenderTableId

			select top 1 @v_TotDispColId= sortOrder from tbl_TenderColumns
			 where tenderTableId=@v_TableId_Int and filledBy=2
			  order by sortOrder
			if ((select COUNT(tenderTableId) from tbl_TenderFormula where tenderTableId =@v_TableId_Int and columnId= @v_TotDispColId-1)>1)
			begin
				set @v_TotDispColId=@v_TotDispColId-1
			end
			print '@v_TotDispColId'+convert(varchar(20),@v_TotDispColId)
			print '@v_ColId_int'+convert(varchar(20),@v_ColId_int)
			if @v_TotDispColId-1=@v_ColId_int and @v_TotRowId_Int !=0 and @v_TotRowId_Int=@v_rowId_Int
			begin
			set @v_RowCellValue =  '<td style="white-space:nowrap;text-align:right;"><b>Grand Total</b></td>'
			end
			else
			begin
			if @v_datatype_Int =3 or  @v_datatype_Int =8 or  @v_datatype_Int =13 or @v_datatype_Int =11 or @v_datatype_Int =4
						begin
			if @v_RowCellValue !=''
			begin
			set @v_RowCellValue =  '<td style=''text-align:right;''>'+convert(varchar(8000),cast(@v_RowCellValue as decimal(18,3)))+'</td>'
			end
			else
			begin
			set @v_RowCellValue =  '<td style=''text-align:right;''>'+convert(varchar(8000),@v_RowCellValue )+'</td>'
			end
			end
			else
			begin
			set @v_RowCellValue =  '<td>'+@v_RowCellValue+'</td>'
			end
			end

				END


				ELSE
				BEGIN
				select @v_datatype_Int=  max(celldatatype) from tbl_TenderCells where
				tenderTableId=@v_TableId_Int and columnId=@v_ColId_int and rowId=@v_rowId_Int

					print 'taking data from other'
					if @v_datatype_Int=9 or @v_datatype_Int=10
					begin

						--Dohatec: ICT Start
							select top 1 @v_CellId_int = cellId from tbl_TenderCells
							where tenderTableId=@v_TableId_Int and columnId=@v_ColId_int and rowId=@v_rowId_Int
							print 'CellId: ' + CONVERT(VARCHAR(50),@v_CellId_int)

							--get list box (combo box) name (e.g. Currency)
							set @v_ListBoxName_Vc = ''

							select top 1 @v_ListBoxName_Vc = tlb.listBoxName from tbl_TenderListCellDetail tlcd, tbl_TenderListBox tlb
							where tlcd.tenderTableId=@v_TableId_Int and tlcd.columnId = @v_ColId_int and tlcd.cellId = @v_CellId_int and tlcd.tenderListId = tlb.tenderListId
							print 'ListBoxName: ' + @v_ListBoxName_Vc
							--Dohatec: ICT End
					
							-- DOHATEC START
							--If(@v_IsProcurementTypeIct = 1 and @v_ListBoxName_Vc = 'Currency')
							If(@v_IsProcurementTypeIct = 1)
										/*BEGIN
											SELECT @v_RowCellValue=  COALESCE(@v_RowCellValue+'</td><td>', ' ') + 
												case 
													when cellvalue = 'null' 
														then '' 
													else CONVERT(VARCHAR(8000), currencyShortName) 
												end 
											from vw_get_userbiddata v,(select  tbpd.bidTableId,currencyShortName 
																		from tbl_TenderListCellDetail tlcd, tbl_TenderBidPlainData tbpd, tbl_CurrencyMaster cm
																		where tlcd.tenderTableId=tbpd.tenderTableId and tlcd.columnId=tbpd.tenderColId and
																		tlcd.cellId=tbpd.tenderCelId and tlcd.location=tbpd.rowId
																		and tlcd.tenderTableId=@v_TableId_Int and tbpd.tenderTableId =@v_TableId_Int  
																		and rowid =@v_rowId_Int and columnid =@v_ColId_int and tbpd.cellValue != 'null' and tbpd.cellValue = cm.currencyId) p
											WHERE v.bidTableId=p.bidTableId and  v.tenderTableId =@v_TableId_Int 
											and v.rowId = @v_rowId_Int and v.columnId = @v_ColId_int  
											ORDER BY userid,v.bidTableId
										END
									Else*/
									begin	
						
						/*SELECT @v_RowCellValue=  COALESCE(@v_RowCellValue+'</td><td>', ' ') + case when cellvalue = 'null' then ''  else CONVERT(VARCHAR(8000),
						itemtext) end from vw_get_userbiddata v,(select  p.bidTableId,itemText from
						tbl_TenderListCellDetail l,tbl_TenderListDetail ld,
						 tbl_TenderBidPlainData p

						where
						l.tenderTableId=p.tenderTableId and
						 l.columnId=p.tenderColId and
						 l.cellId=p.tenderCelId and l.location=p.rowId and
						l.tenderListId=ld.tenderListId and l.tenderTableId=@v_TableId_Int
						and ld.itemValue=p.cellValue and p.tenderTableId =@v_TableId_Int
						  and rowid =@v_rowId_Int
												and columnid =@v_ColId_int) p
						 WHERE v.bidTableId=p.bidTableId
						 and  v.tenderTableId =@v_TableId_Int and v.rowId = @v_rowId_Int
						  and v.columnId = @v_ColId_int  ORDER BY userid,v.bidTableId*/
						  
						  						  						  
						  SELECT @v_RowCellValue=  COALESCE( @v_RowCellValue +'</td><td style=''text-align:right;''>', ' ') + case when cellvalue = 'null' then '' else CONVERT(VARCHAR(8000),case when @v_TotRowId_Int = @v_RowId_Int and @v_TotColId_Int=@v_ColId_int then '<br/>'+convert(varchar(8000),cast(cellValue as decimal(18,3))) else convert(varchar(8000),cast(cellValue as decimal(18,3))) end) end FROM vw_get_userbiddata WHERE tenderTableId =@v_TableId_Int and rowid = @v_rowId_Int and columnid = @v_ColId_int ORDER BY userid,bidTableId
						  
						  -- DOHATEC END
								--Dohatec: ICT Start
								end
								--Dohatec: ICT End
						  if @v_RowCellValue is null or @v_RowCellValue=''
						  begin
						  set @v_RowCellValue=''
						  end
						  print 'IN If loop 9 or 10'+@v_RowCellValue

					end
					else
					--Dohatec: ICT Start
					begin
					--Dohatec: ICT End


					select  @v_TotRowId_Int=MAX(rowId)  ,@v_TotColId_Int=MAX(t.columnId)  from tbl_TenderFormula t,tbl_TenderCells c where
					formula like '%TOTAL%' and t.tenderTableId=@v_TableId_Int
					and t.tenderTableId=c.tenderTableId
					and t.columnId=c.columnId group by t.tenderTableId
					print @v_TotRowId_Int
					print @v_TotColId_Int
					print @v_TableId_Int
					print @v_ColId_int
					print @v_rowId_Int
						if @v_datatype_Int =3 or  @v_datatype_Int =8 or  @v_datatype_Int =13 or @v_datatype_Int =11 or @v_datatype_Int =4
						begin
						print  'kkkkkkkkk'+ @v_RowCellValue
						SELECT @v_RowCellValue=  COALESCE( @v_RowCellValue +'</td><td style=''text-align:right;''>', ' ') + case when cellvalue = 'null' then '' else CONVERT(VARCHAR(8000),case when @v_TotRowId_Int = @v_RowId_Int and @v_TotColId_Int=@v_ColId_int then '<b></b><br/>'+convert(varchar(8000),cast(cellValue as decimal(18,3))) else convert(varchar(8000),cast(cellValue as decimal(18,3))) end) end FROM vw_get_userbiddata WHERE tenderTableId =@v_TableId_Int and rowid = @v_rowId_Int and columnid = @v_ColId_int ORDER BY userid,bidTableId
						end
						else
						begin
						SELECT @v_RowCellValue=  COALESCE( @v_RowCellValue   +'</td><td >', ' ') + case when cellvalue = 'null' then '' else CONVERT(VARCHAR(8000),cellValue) end FROM vw_get_userbiddata WHERE tenderTableId =@v_TableId_Int and rowid = @v_rowId_Int and columnid = @v_ColId_int ORDER BY userid,bidTableId
						end
					end

						if @v_datatype_Int =3 or  @v_datatype_Int =13 or  @v_datatype_Int =8 or @v_datatype_Int =11 or @v_datatype_Int =4
			 			begin
			 			print  'rrrrrrrrrrrrrr'+ @v_RowCellValue
 						SET @v_RowCellValue =  '<td style=''text-align:right;''>'+@v_RowCellValue+'</td>'
 						end
 						else
 						begin
 						SET @v_RowCellValue =  '<td>'+@v_RowCellValue+'</td>'
 						end

				END
				print 'print'
				set  @v_RowCellValue=isnull(@v_RowCellValue,'')

				SET @v_CellHtml_vc = @v_CellHtml_vc + @v_RowCellValue
				print 'Cell Value: ' + @v_CellHtml_vc

				SET @v_RowCellValue = null



			FETCH NEXT FROM cur_columnId INTO @v_ColId_int, @v_colfill_Int,@v_datatype_Int
			END

			SET @v_RowHtml_Vc = @v_RowHtml_Vc + @v_CellHtml_vc + '</tr>'
			CLOSE cur_columnId
			DEALLOCATE cur_columnId

			print 'row HTML' + @v_RowHtml_Vc
 			print 'row fetch'

 			set @v_CellHtml_vc = ''

 --			set @v_CellFinalHtml_vc = @v_CellFinalHtml_vc + '<tr>'+@v_CellHtml_vc+'</tr>'
 --			SELECT @v_CellFinalHtml_vc
 			FETCH NEXT FROM cur_rowValues INTO @v_rowId_Int
 			   SET @v_FormfinalTableHeader = @v_FormfinalTableHeader + @v_RowHtml_Vc
 			   SET @v_RowHtml_Vc = ''
			END

			CLOSE cur_rowValues
			DEALLOCATE cur_rowValues


			--set @v_RowHtml_Vc = @v_RowHtml_Vc + '</table>'
					SET @v_columnhead_Vc =''
					SET @v_columnhead2_Vc =''
			FETCH NEXT FROM cur_tblNames INTO @v_TableId_Int

			END

			CLOSE cur_tblNames
			DEALLOCATE cur_tblNames


print ' lAST ST </BR > ' + isnull (@v_MainHeader_Vc + @v_FormfinalTableHeader + '</table></td></tr><tr><th> '+@v_FormFooter+'</th></tr></table>','') 

	SELECT isnull (@v_MainHeader_Vc + @v_FormfinalTableHeader + '</table></td></tr><tr><th> '+@v_FormFooter+'</th></tr></table>','') as reportHtml

END
