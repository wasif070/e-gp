USE [egpbhutan24jan2017]
GO
/****** Object:  StoredProcedure [dbo].[p_generateTOR_TER]    Script Date: 1/26/2017 12:40:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	G. M. Rokibul Hasan
-- Create date: 31-May-2016
-- Description:	For Generate Tender Opening Report and Tender Evaluation Report
-- =============================================
ALTER PROCEDURE [dbo].[p_generateTOR_TER]
	@v_tenderID VARCHAR(10),
	@v_reportType VARCHAR(10)
AS
BEGIN
    SET NOCOUNT ON;
	BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @v_pkgLotID VARCHAR(10),
					@v_reportID INT,
					@v_reportTableID INT,
					@v_reportColNO INT,
					@v_offEstCost VARCHAR(25),
					@v_reportName VARCHAR(50),
					@errorCode VARCHAR(MAX),
					@v_reportgenerated int

			

			IF @v_reportType = 'TOR'
			BEGIN
					SET @v_reportName = 'Opening'
					SET @v_reportColNO = 3
			END
			ELSE IF @v_reportType = 'TER'
			BEGIN
					SET @v_reportName = 'Evaluation'
					SET @v_reportColNO = 6
			END
			
			SELECT @v_reportgenerated = reportid from tbl_ReportMaster where tenderid = @v_tenderID and reportType= @v_reportType --- **** Added By Dohatec For Double Entry Check **** ---
			IF  @v_reportgenerated IS NULL ----------**** FOR CHECK ALLREADY INSERTED DATA By Dohatec to restrict Double Entry Check ***---------------
			BEGIN
			/************ REPORT MASTER TABLE **************/
			INSERT INTO tbl_ReportMaster VALUES (@v_reportName + ' Report For Tender '+@v_tenderID, @v_reportName+' Report Header', @v_reportName+' Report Footer', 'l1', @v_tenderID, @v_reportType)			
			SELECT  @v_reportID = MAX(reportID) FROM tbl_ReportMaster where tenderId = @v_tenderID and isTORTER = @v_reportType
						
						DECLARE PkgLotId_Cursor CURSOR FAST_FORWARD FOR
						SELECT	DISTINCT appPkgLotId FROM tbl_TenderLots tl WHERE tenderId = @v_tenderID;
						OPEN PkgLotId_Cursor;
						FETCH NEXT FROM PkgLotId_Cursor INTO @v_PkgLotId;
						WHILE @@FETCH_STATUS = 0
						   BEGIN			
										/************ REPORT LOTS TABLE **************/
										--INSERT INTO tbl_ReportLots VALUES (@v_reportID,@v_pkgLotID)
										-- GET TENDER PACKAGE LOTID
										INSERT INTO tbl_ReportLots (pkgLotId,reportId)	SELECT	DISTINCT @v_PkgLotId,@v_reportID
										
										
										-- GET ESTIMATED COST
										SELECT @v_offEstCost = 0
										SELECT	@v_offEstCost = @v_offEstCost + estCost
										FROM	tbl_TenderEstCost -- by dohatec in place of tbl_TenderDetails to solve issue # 1623 
										WHERE	tenderId = @v_tenderID and pkgLotId=@v_PkgLotId

										/************ REPORT FORMS TABLE **************/
										INSERT INTO tbl_ReportForms (tenderFormId,reportId)
										SELECT	tenderFormId,	@v_reportID
										FROM	tbl_TenderGrandSumDetail TGS
														INNER JOIN tbl_TenderGrandSum TG ON TG.tenderSumId = TGS.tenderSumId
																AND tenderId = @v_tenderID AND pkgLotId = @v_PkgLotId

										/************ REPORT TABLE MASTER TABLE **************/
										INSERT INTO tbl_ReportTableMaster VALUES (@v_reportID,@v_reportColNO,@v_reportName+' Report Header', @v_reportName+' Report Footer',@v_PkgLotId)
										--SET @v_reportTableID = IDENT_CURRENT('tbl_ReportTableMaster') -- Commented BY DOHATEC 29 Dec 2014
										SELECT @v_reportTableID = MAX(reportTableID) FROM tbl_ReportTableMaster where reportId = @v_reportID and rptTableHeader = @v_reportName+' Report Header';
										
										IF @v_reportType = 'TOR' ----------------*************** FOR TOR ***************----------------
										BEGIN
												/************ REPORT COLUMN MASTER TABLE **************/
												INSERT INTO tbl_ReportColumnMaster VALUES (@v_reportTableID,1,'S. No',3,1,'no')
												INSERT INTO tbl_ReportColumnMaster VALUES (@v_reportTableID,2,'Name of Tenderer',1,2,'no')
												INSERT INTO tbl_ReportColumnMaster VALUES (@v_reportTableID,3,'Quoted Amount (in BDT)',2,3,'yes')
												/************ REPORT FORMULA MASTER TABLE **************/
												INSERT INTO tbl_ReportFormulaMaster VALUES (@v_reportTableID,dbo.f_getDataForAutoFormulaPkgLot(@v_tenderID,@v_PkgLotId),3,dbo.f_getDisplayForAutoFormulaPkgLot(@v_tenderID,@v_PkgLotId))
										END
										ELSE IF @v_reportType = 'TER' ----------------*************** FOR TER ***************----------------
										BEGIN
												/************ REPORT COLUMN MASTER TABLE **************/
												INSERT INTO tbl_ReportColumnMaster VALUES (@v_reportTableID,1,'Rank',3,1,'no')
												INSERT INTO tbl_ReportColumnMaster VALUES (@v_reportTableID,2,'Name of Tenderer',1,2,'no')
												INSERT INTO tbl_ReportColumnMaster VALUES (@v_reportTableID,3,'Official Est Cost',4,3,'no')
												INSERT INTO tbl_ReportColumnMaster VALUES (@v_reportTableID,4,'Quoted Amount (in BDT)',2,4,'yes')
												INSERT INTO tbl_ReportColumnMaster VALUES (@v_reportTableID,5,'Deviation Amount',2,5,'no')
												INSERT INTO tbl_ReportColumnMaster VALUES (@v_reportTableID,6,'% of Deviaiton',2,6,'no')
												/************ REPORT FORMULA MASTER TABLE **************/
												INSERT INTO tbl_ReportFormulaMaster VALUES (@v_reportTableID,dbo.f_getDataForAutoFormulaPkgLot(@v_tenderID,@v_PkgLotId),4,dbo.f_getDisplayForAutoFormulaPkgLot(@v_tenderID,@v_PkgLotId))
												INSERT INTO tbl_ReportFormulaMaster VALUES (@v_reportTableID,+'('+dbo.f_getDataForAutoFormulaPkgLot(@v_tenderID,@v_PkgLotId)+')-'+@v_offEstCost,5,'('+dbo.f_getDisplayForAutoFormulaPkgLot(@v_tenderID,@v_PkgLotId)+')-Est_Cost')
												INSERT INTO tbl_ReportFormulaMaster VALUES (@v_reportTableID,'-(N100-('+dbo.f_getDataForAutoFormulaPkgLot(@v_tenderID,@v_PkgLotId)+')*N100/'+@v_offEstCost+')',6,'-(100-('+dbo.f_getDisplayForAutoFormulaPkgLot(@v_tenderID,@v_PkgLotId)+')*100/Est_Cost)')
										END
							FETCH NEXT FROM PkgLotId_Cursor INTO @v_PkgLotId;
							END;
						CLOSE PkgLotId_Cursor;
						DEALLOCATE PkgLotId_Cursor;
			END	--- **** Added By Dohatec For Double Entry Check **** ---

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SET @errorCode = 'Some error occured while generating reports.'
				--PRINT @errorCode 
		RAISERROR(@errorCode , 12, 1)
	END CATCH
		
    SET NOCOUNT OFF;
END
