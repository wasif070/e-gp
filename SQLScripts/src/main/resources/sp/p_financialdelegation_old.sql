USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_financialdelegation_old]    Script Date: 4/24/2016 10:47:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Dohatec,,Istiak>
-- Create date: <14.Jun.15>
-- Description:	<Storeprocedure for new functionality of Financial Delegation>
-- =============================================
ALTER PROCEDURE [dbo].[p_financialdelegation_old]
	 @v_fieldName1Vc VARCHAR(MAX)=NULL,		  -- Action
	 @v_fieldName2Vc VARCHAR(MAX)=NULL,
	 @v_fieldName3Vc VARCHAR(MAX)=NULL,
	 @v_fieldName4Vc VARCHAR(MAX)=NULL,
	 @v_fieldName5Vc VARCHAR(MAX)=NULL,
	 @v_fieldName6Vc VARCHAR(MAX)=NULL,
	 @v_fieldName7Vc VARCHAR(MAX)=NULL,
	 @v_fieldName8Vc VARCHAR(MAX)=NULL,
	 @v_fieldName9Vc VARCHAR(MAX)=NULL,
	 @v_fieldName10Vc VARCHAR(MAX)=NULL,
	 @v_fieldName11Vc VARCHAR(MAX)=NULL,
	 @v_fieldName12Vc VARCHAR(MAX)=NULL,
	 @v_fieldName13Vc VARCHAR(MAX)=NULL,
	 @v_fieldName14Vc VARCHAR(MAX)=NULL,
	 @v_fieldName15Vc VARCHAR(MAX)=NULL
AS
BEGIN
	IF @v_fieldName1Vc = 'ViewTimeline'
	BEGIN

		DECLARE @v_query VARCHAR(MAX)
		SET @v_query = 'SELECT CONVERT(VARCHAR(128), FinancialDeligationId) AS FieldValue1
						  ,BudgetType AS FieldValue2
						  ,TenderType AS FieldValue3
						  ,ProcurementMethod AS FieldValue4
						  ,ProcurementType AS FieldValue5
						  ,ProcurementNature AS FieldValue6
						  ,TenderEmergency AS FieldValue7
						  ,CONVERT(VARCHAR(128), MinValueBDT) AS FieldValue8
						  ,CONVERT(VARCHAR(128), MaxValueBDT) AS FieldValue9
						  ,ApprovingAuthority AS FieldValue10
						FROM tbl_FinancialDelegation'

		IF @v_fieldName2Vc = 'single'
			SET @v_query = @v_query + ' WHERE FinancialDeligationId = ' + @v_fieldName3Vc

		print @v_query
		exec (@v_query)
	END

	ELSE IF @v_fieldName1Vc = 'DeleteRows'
	BEGIN
		SET @v_query = 'DELETE
			FROM tbl_FinancialDelegation
			WHERE FinancialDeligationId in (' +  @v_fieldName2Vc + ')'

		print @v_query
		exec (@v_query)

		IF @@ROWCOUNT > 0
		BEGIN
			SELECT 'true' AS FieldValue1
		END
		ELSE
		BEGIN
			SELECT 'false' AS FieldValue1
		END
	END

	ELSE IF @v_fieldName1Vc = 'CountRules'
	BEGIN
		SELECT CONVERT(VARCHAR(10), COUNT(*)) AS FieldValue1  FROM tbl_FinancialDelegation
	END

	ELSE IF @v_fieldName1Vc = 'InsertOrUpdate'
	BEGIN
		BEGIN TRY
			DECLARE @TransactionName VARCHAR(50)
			SET @TransactionName = 'InsertOrUpdate'
			BEGIN TRANSACTION @TransactionName
				PRINT  ' ' + @v_fieldName2Vc + ' ' + @v_fieldName3Vc
				EXEC (' ' + @v_fieldName2Vc + ' ' + @v_fieldName3Vc)
			COMMIT TRANSACTION @TransactionName
			SELECT 'true' as FieldValue1
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION @TransactionName
			SELECT 'false' AS FieldValue1
		END CATCH
	END

	ELSE IF @v_fieldName1Vc = 'getBudgetType'
	BEGIN
		SELECT
			CONVERT(VARCHAR(8), budgetTypeId) AS FieldValue1
			,budgetType AS FieldValue2
		FROM tbl_BudgetType
	END

	ELSE IF @v_fieldName1Vc = 'getTenderType'
	BEGIN
		SELECT
			CONVERT(VARCHAR(8), tenderTypeId) AS FieldValue1
			,tenderType AS FieldValue2
		FROM tbl_TenderTypes
	END

	ELSE IF @v_fieldName1Vc = 'getProcurementMethod'
	BEGIN
		SELECT
			CONVERT(VARCHAR(8), procurementMethodId) AS FieldValue1
			,procurementMethod AS FieldValue2
		FROM tbl_ProcurementMethod
	END

	ELSE IF @v_fieldName1Vc = 'getProcurementType'
	BEGIN
		SELECT
			CONVERT(VARCHAR(8), procurementTypeId) AS FieldValue1
			,procurementType AS FieldValue2
		FROM tbl_ProcurementTypes
	END

	ELSE IF @v_fieldName1Vc = 'getProcurementNature'
	BEGIN
		SELECT
			CONVERT(VARCHAR(8), procurementNatureId) AS FieldValue1
			,procurementNature AS FieldValue2
		FROM tbl_ProcurementNature
	END

	ELSE IF @v_fieldName1Vc = 'getApprovingAuthority'
	BEGIN
		SELECT
			CONVERT(VARCHAR(8), procurementRoleId) AS FieldValue1
			,procurementRole AS FieldValue2
		FROM tbl_ProcurementRole
		WHERE procurementRole IN ('PE', 'Secretary', 'Minister', 'BOD', 'HOPE', 'CCGP', 'AO', 'PD', 'PMC')
	END

	ELSE IF @v_fieldName1Vc = 'CountRanks'
	BEGIN
		SELECT CONVERT(VARCHAR(10), COUNT(*)) AS FieldValue1  FROM tbl_DelegationDesignationRank
	END

	ELSE IF @v_fieldName1Vc = 'ShowRanksTable'
	BEGIN
		SELECT
			CONVERT(VARCHAR(50), RankId) AS FieldValue1
			,ApprovingAuthority AS FieldValue2
		FROM tbl_DelegationDesignationRank
	END

	ELSE IF @v_fieldName1Vc = 'DeleteRanks'
	BEGIN
		SET @v_query = 'DELETE
			FROM tbl_DelegationDesignationRank
			WHERE RankId in (' +  @v_fieldName2Vc + ')'

		print @v_query
		exec (@v_query)

		IF @@ROWCOUNT > 0
		BEGIN
			SELECT 'true' AS FieldValue1
		END
		ELSE
		BEGIN
			SELECT 'false' AS FieldValue1
		END
	END

	ELSE IF @v_fieldName1Vc = 'getAllProcurementRole'
	BEGIN
		SELECT
			CONVERT(VARCHAR(8), procurementRoleId) AS FieldValue1
			,procurementRole AS FieldValue2
		FROM tbl_ProcurementRole
	END

	ELSE IF @v_fieldName1Vc = 'RanksAddUpdate'
	BEGIN
		BEGIN TRY
			DECLARE @TransName VARCHAR(50)
			SET @TransName = @v_fieldName1Vc
			BEGIN TRANSACTION @TransName
				PRINT  ' ' + @v_fieldName2Vc + ' ' + @v_fieldName3Vc
				EXEC (' ' + @v_fieldName2Vc + ' ' + @v_fieldName3Vc)
			COMMIT TRANSACTION @TransName
			SELECT 'true' as FieldValue1
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION @TransName
			SELECT 'false' AS FieldValue1
		END CATCH
	END
END

