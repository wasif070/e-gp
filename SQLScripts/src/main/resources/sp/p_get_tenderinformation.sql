USE [eGPBhutanLive]
GO
/****** Object:  StoredProcedure [dbo].[p_get_tenderinformation]    Script Date: 29-Nov-17 12:18:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Get Tender Information.
--
--
-- Author: Karan
-- Date: 12-11-2010
--
-- Last Modified:
-- Modified By:
-- Date:
-- Modification:
--------------------------------------------------------------------------------
-- SP Name	   :   p_get_tenderinformation
-- Module	   :   Tender Notice
-- Description :   Tender(s) Listing in PE's and Tenderer's Tender Menu
-- Function	   :
--				get details - Get All Tender Details for Listing.
--				get mytenders - Get Tender Details for Listing created by PE.
--				get alltenders - Get All Tender Details for Listing.
--				get watchlisttenders - Get All Tender Details for Listing that is being watched by Bidder.
--				get tenderermytenders - Get All Tender Details for Listing in which bidder had given Declaration.
--				get tendererlimitedtenders - Get All Tender Details for Listing in which Bidders are Mapped by PE.
--				generalsearch - Get All Tender Details for Listing based on search criteria.
--				get alltendersbytype - Get All Tender Details for Listing on search by Tender Type for bidder.
--				get officeralltendersbytype - Get All Tender Details for Listing on search by Tender Type for officer.
--				get rightsalltendersbytype - Get All Tender Details for Listing.
--				get rightssearchtenders - Get All Tender Details for Listing.



ALTER PROCEDURE [dbo].[p_get_tenderinformation]
@v_Action_inVc varchar(50),
@v_ViewType_inVc varchar(50)=null, -- Tender/Phase/Lot
@v_TenderId_Int int=null,
@v_AppId_inInt int=null,
--
@v_UserId_inInt int=null,
@v_procurementNatureId_inInt int=null,
@v_procurementType_inVc varchar(50)=null,
@v_procurementMethod_inInt int=null,
@v_reoiRfpRefNo_inVc varchar(50)=null,
@v_tenderPubDtFrom_inVc varchar(25)=null,
@v_tenderPubDtTo_inVc varchar(25)=null,
@v_Status_inVc varchar(50)=null,
--
@v_Ministry_inVc varchar(150)=null,
@v_Division_inVc varchar(150)=null,
@v_Agency_inVc varchar(150)=null,
@v_PeOfficeName_inVc varchar(150)=null,
@v_SubmissionDtFrom_inVc varchar(25)=null,
@v_SubmissionDtTo_inVc varchar(25)=null,
@v_Page_inInt int =1,
@v_RecordPerPage_inInt int =10,

/*added 2 parameters district and thana by Proshanto Kumar Saha*/
@v_district_inVc varchar(100)=null,
@v_thana_inVc varchar(100)=null

AS

BEGIN
SET NOCOUNT ON;

DECLARE
@v_Qry_Vc varchar(max),
@v_CntQry_Vc varchar(max),
@v_PgCntQry_Vc varchar(max),
@v_ConditionString_Vc varchar(4000),
@v_StartAt_Int int, @v_StopAt_Int int,
@v_SortParam_Vc varchar(50),
@v_roundID INT


SET @v_roundID=0

/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */

--Start Code to set @v_Action_inVc and @v_filter for filtering owncategory tenders for a bidder (aprojit)
declare @v_filter varchar(max) = ''

if (@v_Action_inVc like '%ownCategory%')
begin
set @v_filter = SUBSTRING(@v_Action_inVc,1,11)
set @v_Action_inVc = SUBSTRING(@v_Action_inVc,12,LEN(@v_Action_inVc))
end
--End Code to set @v_Action_inVc and @v_filter for filtering owncategory tenders for a bidder

IF(Select procurementNature from tbl_TenderDetails where tenderId=@v_TenderId_Int)='Services'
Begin
	SET @v_roundID = (SELECt TOP 1 roundId from tbl_EvalRoundMaster ea where tenderId = @v_TenderId_Int and reportType='N1')
End
Else
Begin
	SET @v_roundID = (SELECt	TOP 1 roundId from tbl_EvalRptSentToAA ea where tenderId = @v_TenderId_Int order by evalRptToAAId desc)
End

SET @v_ConditionString_Vc=''

	IF @v_Action_inVc='get details'
	BEGIN
		If @v_ViewType_inVc='tender'
		Begin
			-- // GET INFO. FOR TENDER
			SELECT  tenderDtlId, ministry, division, agency, peOfficeName, peCode, peDistrict, procurementNature, procurementType,eventType,reoiTenderId,
					invitationFor, reoiRfpFor, contractType, reoiRfpRefNo, procurementMethod, budgetType, sourceOfFund, devPartners,
					projectCode, projectName, packageNo, packageDescription, cpvCode, tenderPubDt, docEndDate, preBidStartDt,
					/* Dohatec Start for showing re-tendered id*/reTenderId, /* Dohatec End */
					preBidEndDt, submissionDt, openingDt, eligibilityCriteria, tenderBrief, deliverables, otherDetails, foreignFirm,
					docAvlMethod, evalType, docFeesMethod, pkgDocFees, docFeesMode, docOfficeAdd, securityLastDt, securitySubOff,
					peName, peDesignation, peAddress, peContactDetails,convert(int,procurementMethodId) procurementMethodId,pqTenderId, tenderStatus,
					/* Dohatec Start */ pkgDocFeesUSD, /* Dohatec End */
					tenderSecurityDt, Cast(tenderValidityDt AS SMALLDATETIME) as tenderValidityDt,estCost,passingMarks,stdTemplateId,
					--case when ((select COUNT(*) from tbl_TenderDetails where reTenderId=@v_TenderId_Int )> 0)then 'Re-Tendered' when ((select TOP 1 tcs.contractSignId from tbl_NoaIssueDetails tnid, tbl_ContractSign tcs
					--where tnid.noaIssueId = tcs.noaId and tnid.tenderId = @v_TenderId_Int order by tnid.noaIssueId DESC) > 0 ) THEN 'Awarded'  else 'Not' end as tenderevalstatus
					CASE
					--WHEN ((select COUNT(*) from tbl_TenderDetails where reTenderId=@v_TenderId_Int )> 0) OR ((select TOP 1 rptstatus from tbl_EvalRptSentToAA ea where tenderId = @v_TenderId_Int order by evalRptToAAId desc) = 'Rejected / Re-Tendering') OR ((select TOP 1 rptstatus from tbl_EvalRptSentToAA ea where tenderId = @v_TenderId_Int order by evalRptToAAId desc) = 'Rejected')
					WHEN ((select COUNT(tenderid) from tbl_TenderDetails ted where reTenderId = @v_TenderId_Int)>0)
					THEN 'ReTendered'
					WHEN ((select TOP 1 rptstatus from tbl_EvalRptSentToAA ea where tenderId = @v_TenderId_Int order by evalRptToAAId desc) = 'Rejected / Re-Tendering')
					THEN 'Re-Tendered'
					WHEN ((select TOP 1 rptstatus from tbl_EvalRptSentToAA ea where tenderId = @v_TenderId_Int order by evalRptToAAId desc) = 'Rejected')
					THEN 'Rejected'
					when ((select TOP 1 tcs.ContractNOAId from tbl_NoaIssueDetails tnid, tbl_ContractNOARelation tcs
                            where tnid.noaIssueId = tcs.NOAID and tnid.tenderId = @v_TenderId_Int order by tnid.noaIssueId DESC) > 0 )
                    THEN 'Awarded'
                    WHEN (select TOP 1 rptstatus from tbl_EvalRptSentToAA ea where tenderId = @v_TenderId_Int order by evalRptToAAId desc) = 'Approved'	OR ((SELECT dbo.f_getTenderEvalCase(@v_TenderId_Int)) = 'CASE2' AND @v_roundID = 0)
					THEN
						CASE
							WHEN (((select TOP 1 rptstatus from tbl_EvalRptSentToAA ea where tenderId = @v_TenderId_Int order by evalRptToAAId desc) = 'Approved') AND
								(SELECT COUNT(*) FROM (select TOP 1 tna.acceptRejStatus FROM tbl_NoaIssueDetails tnid, tbl_NoaAcceptance tna
								where tnid.noaIssueId = tna.noaIssueId and tnid.tenderId = @v_TenderId_Int and tnid.roundId = @v_roundID AND tna.acceptRejStatus = 'pending' order by tna.noaAcceptId  DESC) RES) >= 0)
							THEN 'Approved'
							WHEN ((select TOP 1 tna.acceptRejStatus FROM tbl_NoaIssueDetails tnid, tbl_NoaAcceptance tna,tbl_EvalRptSentToAA ea
								where ea.tenderId =@v_TenderId_Int and  tnid.noaIssueId = tna.noaIssueId and tnid.tenderId = ea.tenderId and tnid.roundId = ea.roundId AND tna.acceptRejStatus='decline' order by tna.noaAcceptId  DESC) = 'decline')
							THEN 'NOA declined'
							WHEN    (((SELECT COUNT(*) FROM (select TOP 1 tna.acceptRejStatus FROM tbl_NoaIssueDetails tnid, tbl_NoaAcceptance tna
								where tnid.noaIssueId = tna.noaIssueId and tnid.tenderId = @v_TenderId_Int and tnid.roundId = @v_roundID AND tna.acceptRejStatus != 'approved' and tna.acceptRejStatus != 'pending' order by tna.noaAcceptId  DESC) RES) > 0) AND
								(select TOP 1 rptstatus from tbl_EvalRptSentToAA ea where tenderId = @v_TenderId_Int order by evalRptToAAId desc) = 'Re-evaluation')
							THEN  'Being Re-evaluated'
							WHEN ((SELECT COUNT(*) FROM (select TOP 1 tna.acceptRejStatus FROM tbl_NoaIssueDetails tnid, tbl_NoaAcceptance tna
								where tnid.noaIssueId = tna.noaIssueId and tnid.tenderId = @v_TenderId_Int and tnid.roundId = @v_roundID AND tna.acceptRejStatus != 'approved' and tna.acceptRejStatus != 'pending' order by tna.noaAcceptId  DESC) RES) > 0)
							THEN 'Being evaluated'
						END
					WHEN (select TOP 1 rptstatus  from tbl_EvalRptSentToAA ea where tenderId = @v_TenderId_Int order by evalRptToAAId desc) ='Seek Clarification'
					THEN 'Clarification Requested'
					WHEN ((select count(tenderId) from tbl_TosRptShare where tenderId = @v_TenderId_Int)>=1 and (select TOP 1 rptstatus from tbl_EvalRptSentToAA ea where tenderId = @v_TenderId_Int order by evalRptToAAId desc) = 'Re-evaluation')
					THEN 'Being Re-evaluated'
					WHEN (select count(tenderId) from tbl_TosRptShare where tenderId = @v_TenderId_Int) >= 1
					THEN 'Being evaluated'
					WHEN (select COUNT(tenderId) from tbl_TenderClose where tenderId=@v_TenderId_Int) != 0
					THEN 'Opening Completed'					
					ELSE  'Not'
					END as tenderevalstatus
				FROM dbo.tbl_TenderDetails WHERE tenderid=@v_TenderId_Int
		End
		Else If @v_ViewType_inVc='phase'
		Begin
			-- // GET INFO. FOR PHASE
			SELECT phasingRefNo,tenderPhasingId, phasingOfService, location, cast(indStartDt as smalldatetime) indStartDt,cast(indEndDt as smalldatetime) indEndDt
				FROM dbo.tbl_TenderPhasing WHERE tenderId=@v_TenderId_Int
		End
		Else If @v_ViewType_inVc='lot'
		Begin
			-- // GET INFO. FOR LOT
			/*SELECT tenderLotSecId, lotNo, lotDesc, location as locationSec, docFess, tenderSecurityAmt, completionTime ,appPkgLotId,starttime
				FROM dbo.tbl_TenderLotSecurity WHERE tenderId=@v_TenderId_Int*/
			SELECT tenderLotSecId, lotNo, lotDesc, location as locationSec, docFess, tenderSecurityAmt, completionTime ,appPkgLotId,starttime,
			-- Dohatec Start
				tenderSecurityAmtUSD, BidSecurityType
			-- Dohatec End
				FROM dbo.tbl_TenderLotSecurity WHERE tenderId=@v_TenderId_Int
		End
		Else If @v_ViewType_inVc='REOITenderInfoForRFP'
		Begin
			-- // GET REOI tender for from which RFP tender is created.
			select b.contractType,b.foreignFirm from tbl_TenderDetails a
			inner join tbl_tenderdetails b
			on a.reoiTenderId = b.tenderId
			where a.reoiTenderId!= 0 and a.eventType='RFP' and b.eventType='REOI'
			and a.tenderId=@v_TenderId_Int
		End
	END


	ELSE IF @v_Action_inVc='get mytenders'
	BEGIN
		-- // GET MY TENDERS - DEFAULT/SEARCH INCLUDED

		/* START CODE: TO SET WHERE CONDITION */
		Set @v_ConditionString_Vc='tenderId in
			(Select tenderId From dbo.tbl_TenderDetails Where tenderId in (select tenderId from tbl_Committee where committeeId in (select committeeId from tbl_CommitteeMembers where userId =  ''' + convert (varchar(20),@v_UserId_inInt) +''' )) or officeId in (SELECT officeid FROM tbl_EmployeeOffices WHERE employeeId in (SELECT employeeId FROM tbl_EmployeeMaster WHERE userId =  ''' + convert (varchar(20),@v_UserId_inInt) +''') union all
 select officeid from tbl_ProjectOffice where  userId =''' + convert (varchar(20),@v_UserId_inInt) +''' )
			)'


		IF @v_procurementNatureId_inInt is not null and @v_procurementNatureId_inInt<>''
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementNatureId=' + Convert(varchar(50), @v_procurementNatureId_inInt)
		IF @v_procurementType_inVc is not null and @v_procurementType_inVc<>''
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementType=''' + @v_procurementType_inVc + ''''
		IF @v_procurementMethod_inInt is not null and @v_procurementMethod_inInt<>''
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementMethodId=' + Convert(varchar(50), @v_procurementMethod_inInt)
		IF @v_reoiRfpRefNo_inVc is not null and @v_reoiRfpRefNo_inVc<>''
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And reoiRfpRefNo=''' + @v_reoiRfpRefNo_inVc + ''''
		IF @v_TenderId_Int is not null and @v_TenderId_Int<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderId=' + Convert(varchar(50), @v_TenderId_Int)


		IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtFrom_inVc !='' and @v_tenderPubDtTo_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime)  between ''' + @v_tenderPubDtFrom_inVc + ''' And ''' + @v_tenderPubDtTo_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is null and @v_tenderPubDtFrom_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime)  >= ''' + @v_tenderPubDtFrom_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtTo_inVc =''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime)  <= ''' + @v_tenderPubDtTo_inVc + ''''
		End


		IF @v_Status_inVc is not null and @v_Status_inVc<>''
			BEGIN
				IF @v_ViewType_inVc='Under Preparation'
				BEGIN
					Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And ((tenderStatus = ''Approved'' And tenderPubDt > getdate()) Or tenderStatus = ''Pending'')'
				END
				ELSE
				BEGIN
					Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderStatus=''' + @v_Status_inVc +''''
				END
			END


		IF @v_ViewType_inVc is not null and @v_ViewType_inVc<>''
		Begin
			IF @v_ViewType_inVc='Live'
			Begin
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And SubmissionDt >= getdate() And tenderPubDt < getdate()'
				select @v_SortParam_Vc = 'tenderPubDt'
			End
			Else IF @v_ViewType_inVc='Archive'
			Begin
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And SubmissionDt < getdate()
				And tenderId in (select distinct tenderId from tbl_NoaIssueDetails where noaIssueId in
										(select distinct noaId from tbl_ContractNOARelation)
									) AND tenderId not in( select reTenderId from tbl_TenderDetails)
				'
				select @v_SortParam_Vc = 'tenderPubDt'
			End
			Else IF @v_ViewType_inVc='Cancelled'
			Begin
				select @v_SortParam_Vc = 'tenderPubDt'
			End
			Else IF @v_ViewType_inVc='Processing'
			Begin
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And SubmissionDt < getdate()
				And tenderId not in (select distinct tenderId from tbl_NoaIssueDetails where noaIssueId in
										(select distinct noaId from tbl_ContractNOARelation)
									)
				'
				PRINT 'Processing' + @v_Status_inVc
				select @v_SortParam_Vc = 'tenderPubDt'
			End
			Else IF @v_ViewType_inVc='Under Preparation'
			Begin
				--Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderPubDt > getdate()'
				select @v_SortParam_Vc = 'tenderId'
			End
		End
		Else
		Begin
			select @v_SortParam_Vc = 'tenderId'
		End


		/* END CODE: TO SET WHERE CONDITION */

		-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
		Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT(tenderDtlId) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+') FROM dbo.tbl_TenderDetails  WHERE ' + @v_ConditionString_Vc
		Select @v_CntQry_Vc='SELECT Count(tenderDtlId) FROM dbo.tbl_TenderDetails WHERE ' + @v_ConditionString_Vc

		/* START CODE: DYNAMIC QUERY TO GET RECORDS */
		Select @v_Qry_Vc=
		'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
			SELECT tmp.RowNumber, A.tenderId tenderDtlId, A.reoiRfpRefNo, A.procurementNature, A.tenderBrief, A.ministry,
			A.division, A.agency,	A.peOfficeName, A.procurementType, A.procurementMethod,   A.tenderPubDt ,
			A.submissionDt, A.openingDt, @v_TotalPageCnt_Int as TotalPages, @v_TotalRecordCnt_Int as totalRecords,tenderId,tenderbrief, A.tenderStatus,
			--case when ((select COUNT(*) from tbl_TenderDetails where reTenderId=a.tenderId )> 0) then ''Re-Tendered'' when ((select TOP 1 tcs.contractSignId from tbl_NoaIssueDetails tnid, tbl_ContractSign tcs
			--where tnid.noaIssueId = tcs.noaId and tnid.tenderId = a.tenderId order by tnid.noaIssueId DESC) > 0 ) THEN ''Awarded''  else ''Not'' end as tenderevalstatus
		CASE
--			WHEN ((select COUNT(*) from tbl_TenderDetails where reTenderId=a.tenderId )> 0) OR ((select TOP 1 rptstatus from tbl_EvalRptSentToAA ea where tenderId = a.tenderId order by evalRptToAAId desc) = ''Rejected / Re-Tendering'') OR ((select TOP 1 rptstatus from tbl_EvalRptSentToAA ea where tenderId = a.tenderId order by evalRptToAAId desc) = ''Rejected'')
			WHEN ((select COUNT(*) from tbl_TenderDetails where reTenderId=a.tenderId )> 0)
			THEN ''Re-Tender''
			WHEN ((select TOP 1 rptstatus from tbl_EvalRptSentToAA ea where tenderId = a.tenderId order by evalRptToAAId desc) = ''Rejected / Re-Tendering'')
			THEN ''Re-Tendered''
			WHEN ((select TOP 1 rptstatus from tbl_EvalRptSentToAA ea where tenderId = a.tenderId order by evalRptToAAId desc) = ''Rejected'')
			THEN ''Rejected''
			WHEN ((select TOP 1 tcs.ContractNOAId from tbl_NoaIssueDetails tnid, tbl_ContractNOARelation tcs
					where tnid.noaIssueId = tcs.noaId and tnid.tenderId = a.tenderId order by tnid.noaIssueId DESC) > 0 )
			THEN ''Awarded''
			WHEN (select TOP 1 rptstatus from tbl_EvalRptSentToAA ea where tenderId = a.tenderId order by evalRptToAAId desc) = ''Approved''
			THEN
				/*CASE
					WHEN (SELECT COUNT(*) FROM (select TOP 1 tna.acceptRejStatus FROM tbl_NoaIssueDetails tnid, tbl_NoaAcceptance tna ,tbl_EvalRptSentToAA ea
						 where ea.tenderId = a.tenderId and  tnid.noaIssueId = tna.noaIssueId and tnid.tenderId = ea.tenderId and tnid.roundId = ea.roundId AND tna.acceptRejStatus = ''Approved'' order by tna.noaAcceptId  DESC) RES) >= 0
					THEN ''Approved''
					WHEN ((select TOP 1 tna.acceptRejStatus FROM tbl_NoaIssueDetails tnid, tbl_NoaAcceptance tna,tbl_EvalRptSentToAA ea
						 where ea.tenderId = a.tenderId and  tnid.noaIssueId = tna.noaIssueId and tnid.tenderId = ea.tenderId and tnid.roundId = ea.roundId order by tna.noaAcceptId  DESC) = ''decline'')
					THEN ''NOA declined''
					WHEN (SELECT COUNT(*) FROM (select TOP 1 tna.acceptRejStatus FROM tbl_NoaIssueDetails tnid, tbl_NoaAcceptance tna ,tbl_EvalRptSentToAA ea
						 where ea.tenderId = a.tenderId and  tnid.noaIssueId = tna.noaIssueId and tnid.tenderId = ea.roundId and tnid.roundId =  ea.roundId AND tna.acceptRejStatus != ''Approved'' and tna.acceptRejStatus != ''pending'' order by tna.noaAcceptId  DESC) RES) > 0
					THEN ''Being evaluated''
				END*/
				CASE WHEN A.procurementNature<>''Services'' THEN
					CASE
						WHEN (((select TOP 1 rptstatus from tbl_EvalRptSentToAA ea where tenderId = A.tenderId order by evalRptToAAId desc) = ''Approved'') AND
							(SELECT COUNT(*) FROM (select TOP 1 tna.acceptRejStatus FROM tbl_NoaIssueDetails tnid, tbl_NoaAcceptance tna ,tbl_EvalRptSentToAA ea
							 where ea.tenderId = a.tenderId and  tnid.noaIssueId = tna.noaIssueId and tnid.tenderId = ea.tenderId and tnid.roundId = ea.roundId AND tna.acceptRejStatus = ''pending'' order by tna.noaAcceptId  DESC) RES) >= 0) OR ((SELECT dbo.f_getTenderEvalCase(A.tenderId)) = ''CASE1'') OR ((SELECT dbo.f_getTenderEvalCase(A.tenderId)) = ''CASE3'' AND (select TOP 1 roundId from tbl_EvalRptSentToAA ea where tenderId = a.tenderId and rptstatus=''approved'' order by evalRptToAAId desc) > 0)
						THEN ''Approved''
						WHEN ((select TOP 1 tna.acceptRejStatus FROM tbl_NoaIssueDetails tnid, tbl_NoaAcceptance tna,tbl_EvalRptSentToAA ea
							 where ea.tenderId = a.tenderId and  tnid.noaIssueId = tna.noaIssueId and tnid.tenderId = ea.tenderId and tnid.roundId = ea.roundId order by tna.noaAcceptId  DESC) = ''decline'')
						THEN ''NOA declined''
						WHEN (SELECT COUNT(*) FROM (select TOP 1 tna.acceptRejStatus FROM tbl_NoaIssueDetails tnid, tbl_NoaAcceptance tna ,tbl_EvalRptSentToAA ea
							 where ea.tenderId = a.tenderId and  tnid.noaIssueId = tna.noaIssueId and tnid.tenderId = ea.roundId and tnid.roundId =  ea.roundId AND tna.acceptRejStatus = ''Approved'' order by tna.noaAcceptId  DESC) RES) <= 0
						THEN ''Being evaluated''
					END
				ELSE
					CASE
						WHEN (((select TOP 1 rptstatus from tbl_EvalRptSentToAA ea where tenderId = A.tenderId order by evalRptToAAId desc) = ''Approved'') AND
							(SELECT COUNT(*) FROM (select TOP 1 tna.acceptRejStatus FROM tbl_NoaIssueDetails tnid, tbl_NoaAcceptance tna ,tbl_EvalRoundMaster ea
							 where ea.tenderId = A.tenderId and  tnid.noaIssueId = tna.noaIssueId and ea.reportType=''N1'' AND tnid.tenderId = ea.tenderId and tnid.roundId = ea.roundId AND tna.acceptRejStatus = ''pending'' order by tna.noaAcceptId  DESC) RES) >= 0) OR (SELECT dbo.f_getTenderEvalCase(A.tenderId)) = ''CASE1'' OR ((SELECT dbo.f_getTenderEvalCase(A.tenderId)) = ''CASE3'' AND (select TOP 1 roundId from tbl_EvalRptSentToAA ea where tenderId = a.tenderId and rptstatus=''approved'' order by evalRptToAAId desc) > 0)
						THEN ''Approved''
						WHEN ((select TOP 1 tna.acceptRejStatus FROM tbl_NoaIssueDetails tnid, tbl_NoaAcceptance tna,tbl_EvalRoundMaster ea
							 where ea.tenderId = a.tenderId and  tnid.noaIssueId = tna.noaIssueId and ea.reportType=''N1'' AND tnid.tenderId = ea.tenderId and tnid.roundId = ea.roundId order by tna.noaAcceptId  DESC) = ''decline'')
						THEN ''NOA declined''
						WHEN (SELECT COUNT(*) FROM (select TOP 1 tna.acceptRejStatus FROM tbl_NoaIssueDetails tnid, tbl_NoaAcceptance tna ,tbl_EvalRoundMaster ea
							 where ea.tenderId = a.tenderId and  tnid.noaIssueId = tna.noaIssueId and ea.reportType=''N1'' AND tnid.tenderId = ea.roundId and tnid.roundId = ea.roundId AND tna.acceptRejStatus = ''Approved'' order by tna.noaAcceptId  DESC) RES) <= 0
						THEN ''Being evaluated''
					END
				END
			WHEN (select TOP 1 rptstatus  from tbl_EvalRptSentToAA ea where tenderId = a.tenderId order by evalRptToAAId desc) =''Seek Clarification''
			THEN ''Clarification Requested''
			WHEN ((select count(tenderId) from tbl_TosRptShare where tenderId = a.tenderId)>=1 and (select TOP 1 rptstatus from tbl_EvalRptSentToAA ea where tenderId = a.tenderId order by evalRptToAAId desc) = ''Re-evaluation'')
			THEN ''Being Re-evaluated''
			WHEN (select count(tenderId) from tbl_TosRptShare where tenderId = a.tenderId) >= 1
			THEN ''Being evaluated''
			WHEN (select COUNT(tenderId) from tbl_TenderClose where tenderId=a.tenderId) != 0
			THEN  ''Opening Completed''
			ELSE  ''Not''
			END as tenderevalstatus
		FROM dbo.tbl_TenderDetails A
		Inner join
		(Select RowNumber, tenderDtlId From
		(SELECT ROW_NUMBER() Over (Order by '+@v_SortParam_Vc+' Desc) as RowNumber, tenderDtlId, tenderPubDt,tenderId
		FROM dbo.tbl_TenderDetails
		WHERE '	+ @v_ConditionString_Vc
		+') B
		WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+') Tmp
		On A.tenderDtlId=tmp.tenderDtlId ORDER BY A.'+@v_SortParam_Vc+' DESC'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */

		-- // PRINT QUERY HERE
		--print(@v_ConditionString_Vc)
		Print(@v_Qry_Vc)
		-- // EXECUTE QUERY
		Exec(@v_Qry_Vc)
	END
	ELSE IF @v_Action_inVc='get alltenders'
	BEGIN
		set @v_Status_inVc='Approved'
		-- // GET ALL TENDERS - DEFAULT/SEARCH INCLUDED
		Set @v_ConditionString_Vc='  eventtype not in (''2 Stage-PQ'',''RFP'',''LTM'',''2STM'',''2 stage-TSTM'') and docaccess=''Open'' and procurementMethodId not in(1,3) '
		/* START CODE: TO SET WHERE CONDITION */
		IF @v_procurementNatureId_inInt is not null and @v_procurementNatureId_inInt<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementNatureId=' + Convert(varchar(50), @v_procurementNatureId_inInt)
		IF @v_Ministry_inVc is not null and @v_Ministry_inVc<>''	and @v_Ministry_inVc<> '0'
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tbl_tenderDetails.departmentId=''' + Convert(varchar(50),@v_Ministry_inVc) + ''''
	--	IF @v_Division_inVc is not null and @v_Division_inVc<>''
		--		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And Division=''' + @v_Division_inVc + ''''
		IF @v_Agency_inVc is not null and @v_Agency_inVc<>''
	 	Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And cpvcode like ''%' + @v_Agency_inVc + '%'''
		IF @v_PeOfficeName_inVc is not null and @v_PeOfficeName_inVc<>'' and @v_PeOfficeName_inVc<>0
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And officeId=''' + Convert(varchar(50),@v_PeOfficeName_inVc) + ''''
		IF @v_procurementType_inVc is not null and @v_procurementType_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementType=''' + @v_procurementType_inVc + ''''
		IF @v_procurementMethod_inInt is not null and @v_procurementMethod_inInt<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementMethodid=' + Convert(varchar(50), @v_procurementMethod_inInt)
		IF @v_TenderId_Int is not null and @v_TenderId_Int<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderId=' + Convert(varchar(50), @v_TenderId_Int)
		IF @v_reoiRfpRefNo_inVc is not null and @v_reoiRfpRefNo_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And reoiRfpRefNo=''' + @v_reoiRfpRefNo_inVc + ''''



		IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtTo_inVc !='' and @v_tenderPubDtFrom_inVc!=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime) between ''' + @v_tenderPubDtFrom_inVc + ''' And ''' + @v_tenderPubDtTo_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is null and @v_tenderPubDtFrom_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime) >= ''' + @v_tenderPubDtFrom_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtTo_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime) <= ''' + @v_tenderPubDtTo_inVc + ''''
		End


		IF @v_SubmissionDtFrom_inVc is not null And @v_SubmissionDtTo_inVc is not null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) between ''' + @v_SubmissionDtFrom_inVc + ''' And ''' + @v_SubmissionDtTo_inVc + ''''
		End
		ELSE IF @v_SubmissionDtFrom_inVc is not null And @v_SubmissionDtTo_inVc is null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) >= ''' + @v_SubmissionDtFrom_inVc + ''''
		End
		ELSE IF @v_SubmissionDtFrom_inVc is null And @v_SubmissionDtTo_inVc is not null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) <= ''' + @v_SubmissionDtTo_inVc + ''''
		End

		IF @v_Status_inVc is not null and @v_Status_inVc<>''
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderStatus=''' + @v_Status_inVc +''''



		--IF @v_Status_inVc is not null and @v_Status_inVc<>''
		--Begin
		--		Select @v_ConditionString_Vc=
		--		Case When (@v_ConditionString_Vc='')
		--			 Then 	+ ' tenderStatus=''' + @v_Status_inVc + ''''
		--			 Else (@v_ConditionString_Vc + ' And tenderStatus=''' + @v_Status_inVc+'''')
		--		End
		--End
		/* END CODE: TO SET WHERE CONDITION */

		-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
		Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT(tenderDtlId) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+') FROM dbo.tbl_TenderDetails WHERE' + @v_ConditionString_Vc
		Select @v_CntQry_Vc='SELECT Count(tenderDtlId) FROM dbo.tbl_TenderDetails WHERE' + @v_ConditionString_Vc

		SeLECT @v_PgCntQry_Vc=REPLACE(@v_PgCntQry_Vc,'WHERE AND','WHERE'),
				@v_CntQry_Vc=REPLACE(@v_CntQry_Vc,'WHERE AND','WHERE')

		/* START CODE: DYNAMIC QUERY TO GET RECORDS */
		Select @v_Qry_Vc=
		'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
		SELECT tmp.RowNumber, A.tenderId tenderDtlId, A.reoiRfpRefNo, A.procurementNature, A.tenderBrief, A.ministry,
			A.division, A.agency,	A.peOfficeName, A.procurementType, A.procurementMethod, A.tenderPubDt,
			A.submissionDt, A.openingDt, @v_TotalPageCnt_Int as TotalPages, @v_TotalRecordCnt_Int as totalRecords,tenderId,tenderbrief,A.tenderStatus,
--                      case when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
--                                    where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected'') OR ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
--                                    where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected / Re-Tendering'')
                        case when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
                                    where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected / Re-Tendering'')
                            then ''Re-Tendered''
                            when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
                                    where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected'')
                            then ''Rejected''
                            when ((select TOP 1 tcs.ContractNOAId from tbl_NoaIssueDetails tnid, tbl_ContractNOARelation tcs
                                    where tnid.noaIssueId = tcs.NOAID and tnid.tenderId = a.tenderId order by tnid.noaIssueId DESC) > 0 )
                            THEN ''Awarded''
                            else ''Not'' end as tenderevalstatus
		FROM dbo.tbl_TenderDetails A
		Inner Join
		(Select RowNumber, tenderDtlId From
			(SELECT ROW_NUMBER() Over (Order by tenderPubDt Desc) as RowNumber, tenderDtlId, tenderPubDt
				FROM dbo.tbl_TenderDetails
				WHERE'	+ @v_ConditionString_Vc
			+' ) B
			WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+') Tmp
		On A.tenderDtlId=tmp.tenderDtlId ORDER BY A.tenderid DESC'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */

		-- // PRINT QUERY HERE
		Set @v_Qry_Vc=REPLACE(@v_Qry_Vc,'WHERE AND','WHERE')
		 Print(@v_Qry_Vc)
		-- // EXECUTE QUERY
		Exec(@v_Qry_Vc)

	END

	ELSE IF @v_Action_inVc='get watchlisttenders'
	BEGIN
	set @v_Status_inVc='Approved'
		-- // GET ALL TENDERS - DEFAULT/SEARCH INCLUDED
		Set @v_ConditionString_Vc=' tenderId in
			(Select tenderId From dbo.tbl_TenderWatchList where userid ='+Convert(varchar(50), @v_UserId_inInt)+')'
		/* START CODE: TO SET WHERE CONDITION */
		IF @v_procurementNatureId_inInt is not null and @v_procurementNatureId_inInt<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementNatureId=' + Convert(varchar(50), @v_procurementNatureId_inInt)
		IF @v_Ministry_inVc is not null and @v_Ministry_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tbl_tenderDetails.departmentId=''' + Convert(varchar(50),@v_Ministry_inVc) + ''''
	--	IF @v_Division_inVc is not null and @v_Division_inVc<>''
		--		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And Division=''' + @v_Division_inVc + ''''
	--	IF @v_Agency_inVc is not null and @v_Agency_inVc<>''
		--		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And Agency=''' + @v_Agency_inVc + ''''
		IF @v_PeOfficeName_inVc is not null and @v_PeOfficeName_inVc<>''	 and @v_PeOfficeName_inVc<>0
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And officeId=''' + Convert(varchar(50),@v_PeOfficeName_inVc) + ''''
		IF @v_procurementType_inVc is not null and @v_procurementType_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementType=''' + @v_procurementType_inVc + ''''
		IF @v_procurementMethod_inInt is not null and @v_procurementMethod_inInt<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementMethodid=' + Convert(varchar(50), @v_procurementMethod_inInt)
		IF @v_TenderId_Int is not null and @v_TenderId_Int<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderId=' + Convert(varchar(50), @v_TenderId_Int)
		IF @v_reoiRfpRefNo_inVc is not null and @v_reoiRfpRefNo_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And reoiRfpRefNo=''' + @v_reoiRfpRefNo_inVc + ''''


		IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtTo_inVc!=''  and @v_tenderPubDtFrom_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime) between ''' + @v_tenderPubDtFrom_inVc + ''' And ''' + @v_tenderPubDtTo_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is null and @v_tenderPubDtFrom_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime) >= ''' + @v_tenderPubDtFrom_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtTo_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime) <= ''' + @v_tenderPubDtTo_inVc + ''''
		End


		IF @v_SubmissionDtFrom_inVc is not null And @v_SubmissionDtTo_inVc is not null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) between ''' + @v_SubmissionDtFrom_inVc + ''' And ''' + @v_SubmissionDtTo_inVc + ''''
		End
		ELSE IF @v_SubmissionDtFrom_inVc is not null And @v_SubmissionDtTo_inVc is null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) >= ''' + @v_SubmissionDtFrom_inVc + ''''
		End
		ELSE IF @v_SubmissionDtFrom_inVc is null And @v_SubmissionDtTo_inVc is not null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) <= ''' + @v_SubmissionDtTo_inVc + ''''
		End

		IF @v_Status_inVc is not null and @v_Status_inVc<>''
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderStatus=''' + @v_Status_inVc +''''

		--IF @v_Status_inVc is not null and @v_Status_inVc<>''
		--Begin
		--		Select @v_ConditionString_Vc=
		--		Case When (@v_ConditionString_Vc='')
		--			 Then 	+ ' tenderStatus=''' + @v_Status_inVc + ''''
		--			 Else (@v_ConditionString_Vc + ' And tenderStatus=''' + @v_Status_inVc+'''')
		--		End
		--End
		/* END CODE: TO SET WHERE CONDITION */

		-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
		Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT(tenderDtlId) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+') FROM dbo.tbl_TenderDetails WHERE' + @v_ConditionString_Vc
		Select @v_CntQry_Vc='SELECT Count(tenderDtlId) FROM dbo.tbl_TenderDetails WHERE' + @v_ConditionString_Vc

		SeLECT @v_PgCntQry_Vc=REPLACE(@v_PgCntQry_Vc,'WHERE AND','WHERE'),
				@v_CntQry_Vc=REPLACE(@v_CntQry_Vc,'WHERE AND','WHERE')

		/* START CODE: DYNAMIC QUERY TO GET RECORDS */
		Select @v_Qry_Vc=
		'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
		SELECT	tmp.RowNumber, A.tenderId tenderDtlId, A.reoiRfpRefNo, A.procurementNature, A.tenderBrief, A.ministry,
				A.division, A.agency,	A.peOfficeName, A.procurementType, A.procurementMethod, A.tenderPubDt,
				A.submissionDt, @v_TotalPageCnt_Int as TotalPages, @v_TotalRecordCnt_Int as totalRecords,tenderId,tenderbrief ,A.tenderStatus ,
--				case when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
--                                            where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected'') OR ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
--                                            where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected / Re-Tendering'') then ''Re-Tendered''
				case when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
                                            where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected / Re-Tendering'')
                                    then ''Re-Tendered''
                                    when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
                                            where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected'')
                                    then ''Rejected''
                                    when ((select TOP 1 tcs.ContractNOAId from tbl_NoaIssueDetails tnid, tbl_ContractNOARelation tcs
                                            where tnid.noaIssueId = tcs.NOAID and tnid.tenderId = a.tenderId order by tnid.noaIssueId DESC) > 0 )
                                    THEN ''Awarded''
                                    else ''Not'' end as tenderevalstatus
		FROM dbo.tbl_TenderDetails A
		Inner Join
		(Select RowNumber, tenderDtlId From
			(SELECT ROW_NUMBER() Over (Order by tenderPubDt Desc) as RowNumber, tenderDtlId, tenderPubDt
				FROM dbo.tbl_TenderDetails
				WHERE'	+ @v_ConditionString_Vc
			+' ) B
			WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+' ) Tmp
		On A.tenderDtlId=tmp.tenderDtlId ORDER BY A.tenderid DESC'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */

		-- // PRINT QUERY HERE
		Set @v_Qry_Vc=REPLACE(@v_Qry_Vc,'WHERE AND','WHERE')
		 Print(@v_Qry_Vc)
		-- // EXECUTE QUERY
		Exec(@v_Qry_Vc)
	END

	ELSE IF @v_Action_inVc='get tenderermytenders'
	BEGIN
		-- // GET ALL TENDERS - DEFAULT/SEARCH INCLUDED
		Set @v_ConditionString_Vc=' tenderId in
			(Select tenderId From dbo.tbl_BidConfirmation where userid ='+Convert(varchar(50), @v_UserId_inInt)+')'
		/* START CODE: TO SET WHERE CONDITION */
		IF @v_procurementNatureId_inInt is not null and @v_procurementNatureId_inInt<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementNatureId=' + Convert(varchar(50), @v_procurementNatureId_inInt)
		IF @v_Ministry_inVc is not null and @v_Ministry_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tbl_tenderDetails.departmentId=''' + Convert(varchar(50),@v_Ministry_inVc) + ''''
	--	IF @v_Division_inVc is not null and @v_Division_inVc<>''
		--		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And Division=''' + @v_Division_inVc + ''''
	--	IF @v_Agency_inVc is not null and @v_Agency_inVc<>''
		--		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And Agency=''' + @v_Agency_inVc + ''''
		IF @v_PeOfficeName_inVc is not null and @v_PeOfficeName_inVc<>'' and @v_PeOfficeName_inVc<>0
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And officeId=''' + Convert(varchar(50),@v_PeOfficeName_inVc) + ''''
		IF @v_procurementType_inVc is not null and @v_procurementType_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementType=''' + @v_procurementType_inVc + ''''
		IF @v_procurementMethod_inInt is not null and @v_procurementMethod_inInt<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementMethodId=' + Convert(varchar(50), @v_procurementMethod_inInt)
		IF @v_TenderId_Int is not null and @v_TenderId_Int<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderId=' + Convert(varchar(50), @v_TenderId_Int)
		IF @v_reoiRfpRefNo_inVc is not null and @v_reoiRfpRefNo_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And reoiRfpRefNo=''' + @v_reoiRfpRefNo_inVc + ''''
		IF @v_Agency_inVc is not null and @v_Agency_inVc<>''
	 	Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And cpvcode like ''%' + @v_Agency_inVc + '%'''



		IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtTo_inVc !='' and  @v_tenderPubDtFrom_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime)   between ''' + @v_tenderPubDtFrom_inVc + ''' And ''' + @v_tenderPubDtTo_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is null and @v_tenderPubDtFrom_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime)   >= ''' + @v_tenderPubDtFrom_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtTo_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime)  <= ''' + @v_tenderPubDtTo_inVc + ''''
		End


		IF @v_SubmissionDtFrom_inVc is not null And @v_SubmissionDtTo_inVc is not null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) between ''' + @v_SubmissionDtFrom_inVc + ''' And ''' + @v_SubmissionDtTo_inVc + ''''
		End
		ELSE IF @v_SubmissionDtFrom_inVc is not null And @v_SubmissionDtTo_inVc is null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) >= ''' + @v_SubmissionDtFrom_inVc + ''''
		End
		ELSE IF @v_SubmissionDtFrom_inVc is null And @v_SubmissionDtTo_inVc is not null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) <= ''' + @v_SubmissionDtTo_inVc + ''''
		End

		IF @v_Status_inVc is not null and @v_Status_inVc<>''
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderStatus=''' + @v_Status_inVc +''''

		IF @v_ViewType_inVc is not null and @v_ViewType_inVc<>''
		Begin
			IF @v_ViewType_inVc='Live'
			Begin
				select @v_SortParam_Vc = 'tenderPubDt'
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And SubmissionDt >= getdate() And tenderPubDt < getdate()'
			End

			--add 11 feb 2016

			IF @v_ViewType_inVc='Progress'
			Begin
				select @v_SortParam_Vc = 'tenderPubDt'
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And SubmissionDt >= getdate() And tenderPubDt < getdate()'
			End

			--ebd 11 feb 2016

			Else IF @v_ViewType_inVc='Archive'
			Begin
				select @v_SortParam_Vc = 'tenderPubDt'
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And SubmissionDt < getdate() '
			End
			Else IF @v_ViewType_inVc='Cancelled'
			Begin
				select @v_SortParam_Vc = 'tenderPubDt'
			End
		End
		Else
		Begin
			select @v_SortParam_Vc = 'tenderid'
		End

		--IF @v_Status_inVc is not null and @v_Status_inVc<>''
		--Begin
		--		Select @v_ConditionString_Vc=
		--		Case When (@v_ConditionString_Vc='')
		--			 Then 	+ ' tenderStatus=''' + @v_Status_inVc + ''''
		--			 Else (@v_ConditionString_Vc + ' And tenderStatus=''' + @v_Status_inVc+'''')
		--		End
		--End
		/* END CODE: TO SET WHERE CONDITION */

		-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
		Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT(tenderDtlId) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+') FROM dbo.tbl_TenderDetails WHERE' + @v_ConditionString_Vc
		Select @v_CntQry_Vc='SELECT Count(tenderDtlId) FROM dbo.tbl_TenderDetails WHERE' + @v_ConditionString_Vc

		SeLECT @v_PgCntQry_Vc=REPLACE(@v_PgCntQry_Vc,'WHERE AND','WHERE'),
				@v_CntQry_Vc=REPLACE(@v_CntQry_Vc,'WHERE AND','WHERE')

		/* START CODE: DYNAMIC QUERY TO GET RECORDS */
		Select @v_Qry_Vc=
		'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
		SELECT	tmp.RowNumber, A.tenderId tenderDtlId, A.reoiRfpRefNo, A.procurementNature, A.tenderBrief, A.ministry,
			A.division, A.agency,	A.peOfficeName, A.procurementType, A.procurementMethod, tenderPubDt,
			submissionDt, openingDt, @v_TotalPageCnt_Int as TotalPages, @v_TotalRecordCnt_Int as totalRecords,tenderId,tenderbrief,A.tenderStatus,
--			case when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
--                                where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected'') OR ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
--				where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected / Re-Tendering'') then ''Re-Tendered''
			case WHEN ((select COUNT(*) from tbl_TenderDetails where reTenderId=a.tenderId )> 0)
			THEN ''Re-Tender''
			when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
                                    where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected'') 
                            THEN ''Rejected''
                            WHEN ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
				where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected / Re-Tendering'') 
                            then ''Re-Tendered''
                            when ((select TOP 1 tcs.ContractNOAId from tbl_NoaIssueDetails tnid, tbl_ContractNOARelation tcs
                                    where tnid.noaIssueId = tcs.NOAID and tnid.tenderId = a.tenderId order by tnid.noaIssueId DESC) > 0 )
                            THEN ''Awarded''
                            else ''Not'' end as tenderevalstatus
		FROM	dbo.tbl_TenderDetails A
		Inner Join
		(Select RowNumber, tenderDtlId From
			(SELECT ROW_NUMBER() Over (Order by ' + @v_SortParam_Vc + ' Desc) as RowNumber, tenderDtlId, tenderPubDt
				FROM dbo.tbl_TenderDetails
				WHERE'	+ @v_ConditionString_Vc
			+'  ) B
			WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+') Tmp
		On A.tenderDtlId=tmp.tenderDtlId ORDER BY A.'+ @v_SortParam_Vc +' DESC'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */

		-- // PRINT QUERY HERE
		Set @v_Qry_Vc=REPLACE(@v_Qry_Vc,'WHERE AND','WHERE')
		Print(@v_Qry_Vc)
		-- // EXECUTE QUERY
		Exec(@v_Qry_Vc)
	END
		ELSE IF @v_Action_inVc='get tendererlimitedtenders'
	BEGIN
		-- // GET ALL TENDERS - DEFAULT/SEARCH INCLUDED
		if((select companyid from tbl_TendererMaster where userId=@v_UserId_inInt)!=1)
		begin
		Set @v_ConditionString_Vc=' tenderid in (select tenderid from  tbl_LimitedTenders l ,tbl_TendererMaster cm
		where l.userId=cm.userId and cm.companyId in(select companyId from tbl_TendererMaster where userId='+Convert(varchar(50), @v_UserId_inInt)+' ) )'
		End
		else
		begin
		Set @v_ConditionString_Vc=' tenderid in (select tenderid from  tbl_LimitedTenders l  where userId='+Convert(varchar(50), @v_UserId_inInt)+'  )'
		end
		/* START CODE: TO SET WHERE CONDITION */
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + '  And tenderPubDt < getdate() '

		--Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And tenderPubDt < getdate() '

		IF @v_procurementNatureId_inInt is not null and @v_procurementNatureId_inInt<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementNatureId=' + Convert(varchar(50), @v_procurementNatureId_inInt)
		IF @v_Ministry_inVc is not null and @v_Ministry_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tbl_tenderDetails.departmentId=''' + Convert(varchar(50),@v_Ministry_inVc) + ''''
	--	IF @v_Division_inVc is not null and @v_Division_inVc<>''
		--		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And Division=''' + @v_Division_inVc + ''''
	--	IF @v_Agency_inVc is not null and @v_Agency_inVc<>''
		--		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And Agency=''' + @v_Agency_inVc + ''''
		IF @v_PeOfficeName_inVc is not null and @v_PeOfficeName_inVc<>''	and @v_PeOfficeName_inVc<>0
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And officeId=''' + Convert(varchar(50),@v_PeOfficeName_inVc) + ''''
		IF @v_procurementType_inVc is not null and @v_procurementType_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementType=''' + @v_procurementType_inVc + ''''
		IF @v_procurementMethod_inInt is not null and @v_procurementMethod_inInt<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementMethodid=' + Convert(varchar(50), @v_procurementMethod_inInt)
		IF @v_TenderId_Int is not null and @v_TenderId_Int<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderId=' + Convert(varchar(50), @v_TenderId_Int)
		IF @v_reoiRfpRefNo_inVc is not null and @v_reoiRfpRefNo_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And reoiRfpRefNo=''' + @v_reoiRfpRefNo_inVc + ''''

		IF @v_Agency_inVc is not null and @v_Agency_inVc<>''
	 	Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And cpvcode like ''%' + @v_Agency_inVc + '%'''


		IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtFrom_inVc !='' and @v_tenderPubDtTo_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime)   between ''' + @v_tenderPubDtFrom_inVc + ''' And ''' + @v_tenderPubDtTo_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is null and @v_tenderPubDtFrom_inVc!=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime)   >= ''' + @v_tenderPubDtFrom_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtTo_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime)  <= ''' + @v_tenderPubDtTo_inVc + ''''
		End


		IF @v_SubmissionDtFrom_inVc is not null And @v_SubmissionDtTo_inVc is not null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) between ''' + @v_SubmissionDtFrom_inVc + ''' And ''' + @v_SubmissionDtTo_inVc + ''''
		End
		ELSE IF @v_SubmissionDtFrom_inVc is not null And @v_SubmissionDtTo_inVc is null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) >= ''' + @v_SubmissionDtFrom_inVc + ''''
		End
		ELSE IF @v_SubmissionDtFrom_inVc is null And @v_SubmissionDtTo_inVc is not null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) <= ''' + @v_SubmissionDtTo_inVc + ''''
		End
		IF @v_TenderId_Int is not null and @v_TenderId_Int<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderId=' + Convert(varchar(50), @v_TenderId_Int)
		IF @v_Status_inVc is not null and @v_Status_inVc<>''
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderStatus=''' + @v_Status_inVc +''''

		--IF @v_Status_inVc is not null and @v_Status_inVc<>''
		--Begin
		--		Select @v_ConditionString_Vc=
		--		Case When (@v_ConditionString_Vc='')
		--			 Then 	+ ' tenderStatus=''' + @v_Status_inVc + ''''
		--			 Else (@v_ConditionString_Vc + ' And tenderStatus=''' + @v_Status_inVc+'''')
		--		End
		--End
		--print (@v_ConditionString_Vc)

		/* END CODE: TO SET WHERE CONDITION */

		-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
		Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT(tenderDtlId) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+') FROM dbo.tbl_TenderDetails WHERE' + @v_ConditionString_Vc
		Select @v_CntQry_Vc='SELECT Count(tenderDtlId) FROM dbo.tbl_TenderDetails WHERE' + @v_ConditionString_Vc

		SeLECT @v_PgCntQry_Vc=REPLACE(@v_PgCntQry_Vc,'WHERE AND','WHERE'),
				@v_CntQry_Vc=REPLACE(@v_CntQry_Vc,'WHERE AND','WHERE')

		/* START CODE: DYNAMIC QUERY TO GET RECORDS */
		Select @v_Qry_Vc=
		'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
		SELECT	tmp.RowNumber, A.tenderId tenderDtlId, A.reoiRfpRefNo, A.procurementNature, A.tenderBrief, A.ministry,
				A.division, A.agency,	A.peOfficeName, A.procurementType, A.procurementMethod, A.tenderPubDt,
				A.submissionDt, A.openingDt, @v_TotalPageCnt_Int as TotalPages, @v_TotalRecordCnt_Int as totalRecords,tenderId,tenderbrief,A.tenderStatus,
--				case when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
--                              where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected'') OR ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
--				where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected / Re-Tendering'') then ''Re-Tendered''
				case WHEN ((select COUNT(*) from tbl_TenderDetails where reTenderId=a.tenderId )> 0) THEN ''Re-Tender''
				when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
                                                where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected'') 
                                THEN  ''Rejected''
                                WHEN  ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
                                        where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected / Re-Tendering'') 
                                then ''Re-Tendered'' 
                                when ((select TOP 1 tcs.ContractNOAId from tbl_NoaIssueDetails tnid, tbl_ContractNOARelation tcs
                                        where tnid.noaIssueId = tcs.NOAID and tnid.tenderId = a.tenderId order by tnid.noaIssueId DESC) > 0 )
                                THEN ''Awarded''
                                else ''Not'' end as tenderevalstatus
		FROM	dbo.tbl_TenderDetails A
		Inner Join
		(Select RowNumber, tenderDtlId From
			(SELECT ROW_NUMBER() Over (Order by tenderPubDt Desc) as RowNumber, tenderDtlId, tenderPubDt
				FROM dbo.tbl_TenderDetails
				WHERE'	+ @v_ConditionString_Vc
			+'   ) B
			WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+') Tmp
		On A.tenderDtlId=tmp.tenderDtlId ORDER BY A.tenderPubDt DESC'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */

		-- // PRINT QUERY HERE
		Set @v_Qry_Vc=REPLACE(@v_Qry_Vc,'WHERE AND','WHERE')
		Print(@v_Qry_Vc)
		-- // EXECUTE QUERY
		Exec(@v_Qry_Vc)
	END

        ELSE IF @v_Action_inVc='get tendererlimitedrfp'
	BEGIN
		-- // GET ALL TENDERS - DEFAULT/SEARCH INCLUDED
		if((select companyid from tbl_TendererMaster where userId=@v_UserId_inInt)!=1)
		begin
		Set @v_ConditionString_Vc=' tenderid in (select tenderid from  tbl_LimitedTenders l ,tbl_TendererMaster cm
		where l.userId=cm.userId and cm.companyId in(select companyId from tbl_TendererMaster where userId='+Convert(varchar(50), @v_UserId_inInt)+' ) and eventType = ''RFP'' )'
		End
		else
		begin
		Set @v_ConditionString_Vc=' tenderid in (select tenderid from  tbl_LimitedTenders l  where userId='+Convert(varchar(50), @v_UserId_inInt)+'  )'
		end
		/* START CODE: TO SET WHERE CONDITION */
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + '  And tenderPubDt < getdate() '

		IF @v_procurementNatureId_inInt is not null and @v_procurementNatureId_inInt<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementNatureId=' + Convert(varchar(50), @v_procurementNatureId_inInt)
		IF @v_Ministry_inVc is not null and @v_Ministry_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tbl_tenderDetails.departmentId=''' + Convert(varchar(50),@v_Ministry_inVc) + ''''
		IF @v_PeOfficeName_inVc is not null and @v_PeOfficeName_inVc<>''	and @v_PeOfficeName_inVc<>0
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And officeId=''' + Convert(varchar(50),@v_PeOfficeName_inVc) + ''''
		IF @v_procurementType_inVc is not null and @v_procurementType_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementType=''' + @v_procurementType_inVc + ''''
		IF @v_procurementMethod_inInt is not null and @v_procurementMethod_inInt<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementMethodid=' + Convert(varchar(50), @v_procurementMethod_inInt)
		IF @v_TenderId_Int is not null and @v_TenderId_Int<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderId=' + Convert(varchar(50), @v_TenderId_Int)
		IF @v_reoiRfpRefNo_inVc is not null and @v_reoiRfpRefNo_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And reoiRfpRefNo=''' + @v_reoiRfpRefNo_inVc + ''''

		IF @v_Agency_inVc is not null and @v_Agency_inVc<>''
	 	Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And cpvcode like ''%' + @v_Agency_inVc + '%'''


		IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtFrom_inVc !='' and @v_tenderPubDtTo_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime)   between ''' + @v_tenderPubDtFrom_inVc + ''' And ''' + @v_tenderPubDtTo_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is null and @v_tenderPubDtFrom_inVc!=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime)   >= ''' + @v_tenderPubDtFrom_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtTo_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime)  <= ''' + @v_tenderPubDtTo_inVc + ''''
		End


		IF @v_SubmissionDtFrom_inVc is not null And @v_SubmissionDtTo_inVc is not null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) between ''' + @v_SubmissionDtFrom_inVc + ''' And ''' + @v_SubmissionDtTo_inVc + ''''
		End
		ELSE IF @v_SubmissionDtFrom_inVc is not null And @v_SubmissionDtTo_inVc is null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) >= ''' + @v_SubmissionDtFrom_inVc + ''''
		End
		ELSE IF @v_SubmissionDtFrom_inVc is null And @v_SubmissionDtTo_inVc is not null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) <= ''' + @v_SubmissionDtTo_inVc + ''''
		End
		IF @v_TenderId_Int is not null and @v_TenderId_Int<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderId=' + Convert(varchar(50), @v_TenderId_Int)
		IF @v_Status_inVc is not null and @v_Status_inVc<>''
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderStatus=''' + @v_Status_inVc +''''

		/* END CODE: TO SET WHERE CONDITION */

		-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
		Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT(tenderDtlId) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+') FROM dbo.tbl_TenderDetails WHERE' + @v_ConditionString_Vc
		Select @v_CntQry_Vc='SELECT Count(tenderDtlId) FROM dbo.tbl_TenderDetails WHERE' + @v_ConditionString_Vc

		SeLECT @v_PgCntQry_Vc=REPLACE(@v_PgCntQry_Vc,'WHERE AND','WHERE'),
				@v_CntQry_Vc=REPLACE(@v_CntQry_Vc,'WHERE AND','WHERE')

		/* START CODE: DYNAMIC QUERY TO GET RECORDS */
		Select @v_Qry_Vc=
		'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
		SELECT	tmp.RowNumber, A.tenderId tenderDtlId, A.reoiRfpRefNo, A.procurementNature, A.tenderBrief, A.ministry,
				A.division, A.agency,	A.peOfficeName, A.procurementType, A.procurementMethod, A.tenderPubDt,
				A.submissionDt, A.openingDt, @v_TotalPageCnt_Int as TotalPages, @v_TotalRecordCnt_Int as totalRecords,tenderId,tenderbrief,A.tenderStatus,
--				case when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
--                              where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected'') OR ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
--				where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected / Re-Tendering'') then ''Re-Tendered''
				case WHEN ((select COUNT(*) from tbl_TenderDetails where reTenderId=a.tenderId )> 0) THEN ''Re-Tender''
				when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
                                                where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected'') 
                                THEN  ''Rejected''
                                WHEN  ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
                                        where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected / Re-Tendering'') 
                                then ''Re-Tendered'' 
                                when ((select TOP 1 tcs.ContractNOAId from tbl_NoaIssueDetails tnid, tbl_ContractNOARelation tcs
                                        where tnid.noaIssueId = tcs.NOAID and tnid.tenderId = a.tenderId order by tnid.noaIssueId DESC) > 0 )
                                THEN ''Awarded''
                                else ''Not'' end as tenderevalstatus
		FROM	dbo.tbl_TenderDetails A
		Inner Join
		(Select RowNumber, tenderDtlId From
			(SELECT ROW_NUMBER() Over (Order by tenderPubDt Desc) as RowNumber, tenderDtlId, tenderPubDt
				FROM dbo.tbl_TenderDetails
				WHERE'	+ @v_ConditionString_Vc
			+'   ) B
			WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+') Tmp
		On A.tenderDtlId=tmp.tenderDtlId ORDER BY A.tenderPubDt DESC'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */

		-- // PRINT QUERY HERE
		Set @v_Qry_Vc=REPLACE(@v_Qry_Vc,'WHERE AND','WHERE')
		Print(@v_Qry_Vc)
		-- // EXECUTE QUERY
		Exec(@v_Qry_Vc)
	END
	
	ELSE IF @v_Action_inVc='get tendererlimitedtender'
	BEGIN
		-- // GET ALL TENDERS - DEFAULT/SEARCH INCLUDED
		if((select companyid from tbl_TendererMaster where userId=@v_UserId_inInt)!=1)
		begin
		Set @v_ConditionString_Vc=' tenderid in (select tenderid from  tbl_LimitedTenders l ,tbl_TendererMaster cm
		where l.userId=cm.userId and cm.companyId in(select companyId from tbl_TendererMaster where userId='+Convert(varchar(50), @v_UserId_inInt)+' ) and eventType <> ''RFP'' )'
		End
		else
		begin
		Set @v_ConditionString_Vc=' tenderid in (select tenderid from  tbl_LimitedTenders l  where userId='+Convert(varchar(50), @v_UserId_inInt)+'  )'
		end
		/* START CODE: TO SET WHERE CONDITION */
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + '  And tenderPubDt < getdate() '

		IF @v_procurementNatureId_inInt is not null and @v_procurementNatureId_inInt<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementNatureId=' + Convert(varchar(50), @v_procurementNatureId_inInt)
		IF @v_Ministry_inVc is not null and @v_Ministry_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tbl_tenderDetails.departmentId=''' + Convert(varchar(50),@v_Ministry_inVc) + ''''
		IF @v_PeOfficeName_inVc is not null and @v_PeOfficeName_inVc<>''	and @v_PeOfficeName_inVc<>0
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And officeId=''' + Convert(varchar(50),@v_PeOfficeName_inVc) + ''''
		IF @v_procurementType_inVc is not null and @v_procurementType_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementType=''' + @v_procurementType_inVc + ''''
		IF @v_procurementMethod_inInt is not null and @v_procurementMethod_inInt<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementMethodid=' + Convert(varchar(50), @v_procurementMethod_inInt)
		IF @v_TenderId_Int is not null and @v_TenderId_Int<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderId=' + Convert(varchar(50), @v_TenderId_Int)
		IF @v_reoiRfpRefNo_inVc is not null and @v_reoiRfpRefNo_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And reoiRfpRefNo=''' + @v_reoiRfpRefNo_inVc + ''''

		IF @v_Agency_inVc is not null and @v_Agency_inVc<>''
	 	Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And cpvcode like ''%' + @v_Agency_inVc + '%'''


		IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtFrom_inVc !='' and @v_tenderPubDtTo_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime)   between ''' + @v_tenderPubDtFrom_inVc + ''' And ''' + @v_tenderPubDtTo_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is null and @v_tenderPubDtFrom_inVc!=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime)   >= ''' + @v_tenderPubDtFrom_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtTo_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime)  <= ''' + @v_tenderPubDtTo_inVc + ''''
		End


		IF @v_SubmissionDtFrom_inVc is not null And @v_SubmissionDtTo_inVc is not null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) between ''' + @v_SubmissionDtFrom_inVc + ''' And ''' + @v_SubmissionDtTo_inVc + ''''
		End
		ELSE IF @v_SubmissionDtFrom_inVc is not null And @v_SubmissionDtTo_inVc is null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) >= ''' + @v_SubmissionDtFrom_inVc + ''''
		End
		ELSE IF @v_SubmissionDtFrom_inVc is null And @v_SubmissionDtTo_inVc is not null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) <= ''' + @v_SubmissionDtTo_inVc + ''''
		End
		IF @v_TenderId_Int is not null and @v_TenderId_Int<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderId=' + Convert(varchar(50), @v_TenderId_Int)
		IF @v_Status_inVc is not null and @v_Status_inVc<>''
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderStatus=''' + @v_Status_inVc +''''

		/* END CODE: TO SET WHERE CONDITION */

		-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
		Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT(tenderDtlId) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+') FROM dbo.tbl_TenderDetails WHERE' + @v_ConditionString_Vc
		Select @v_CntQry_Vc='SELECT Count(tenderDtlId) FROM dbo.tbl_TenderDetails WHERE' + @v_ConditionString_Vc

		SeLECT @v_PgCntQry_Vc=REPLACE(@v_PgCntQry_Vc,'WHERE AND','WHERE'),
				@v_CntQry_Vc=REPLACE(@v_CntQry_Vc,'WHERE AND','WHERE')

		/* START CODE: DYNAMIC QUERY TO GET RECORDS */
		Select @v_Qry_Vc=
		'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
		SELECT	tmp.RowNumber, A.tenderId tenderDtlId, A.reoiRfpRefNo, A.procurementNature, A.tenderBrief, A.ministry,
				A.division, A.agency,	A.peOfficeName, A.procurementType, A.procurementMethod, A.tenderPubDt,
				A.submissionDt, A.openingDt, @v_TotalPageCnt_Int as TotalPages, @v_TotalRecordCnt_Int as totalRecords,tenderId,tenderbrief,A.tenderStatus,
--				case when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
--                              where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected'') OR ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
--				where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected / Re-Tendering'') then ''Re-Tendered''
				case WHEN ((select COUNT(*) from tbl_TenderDetails where reTenderId=a.tenderId )> 0) THEN ''Re-Tender''
				when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
                                                where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected'') 
                                THEN  ''Rejected''
                                WHEN  ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
                                        where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected / Re-Tendering'') 
                                then ''Re-Tendered'' 
                                when ((select TOP 1 tcs.ContractNOAId from tbl_NoaIssueDetails tnid, tbl_ContractNOARelation tcs
                                        where tnid.noaIssueId = tcs.NOAID and tnid.tenderId = a.tenderId order by tnid.noaIssueId DESC) > 0 )
                                THEN ''Awarded''
                                else ''Not'' end as tenderevalstatus
		FROM	dbo.tbl_TenderDetails A
		Inner Join
		(Select RowNumber, tenderDtlId From
			(SELECT ROW_NUMBER() Over (Order by tenderPubDt Desc) as RowNumber, tenderDtlId, tenderPubDt
				FROM dbo.tbl_TenderDetails
				WHERE'	+ @v_ConditionString_Vc
			+'   ) B
			WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+') Tmp
		On A.tenderDtlId=tmp.tenderDtlId ORDER BY A.tenderPubDt DESC'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */

		-- // PRINT QUERY HERE
		Set @v_Qry_Vc=REPLACE(@v_Qry_Vc,'WHERE AND','WHERE')
		Print(@v_Qry_Vc)
		-- // EXECUTE QUERY
		Exec(@v_Qry_Vc)
	END

	ELSE IF @v_Action_inVc='generalsearch'
	BEGIN
		-- // GET MY TENDERS - DEFAULT/SEARCH INCLUDED

		/* START CODE: TO SET WHERE CONDITION */
		IF @v_Status_inVc IS NOT NULL AND @v_Status_inVc<>''
			SELECT @v_ConditionString_Vc=@v_ConditionString_Vc + ' docaccess=''Open'' AND tenderPubDt < getdate() AND tenderbrief LIKE ''%' + @v_ViewType_inVc +'%'' AND (tenderStatus=''' + @v_Status_inVc +''' OR tenderStatus=''cancelled'' ) '
		/* END CODE: TO SET WHERE CONDITION */

		-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
			SELECT @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT(tenderDtlId) as float)/'+CONVERT(VARCHAR(50),@v_RecordPerPage_inInt)+') FROM dbo.tbl_TenderDetails  WHERE ' + @v_ConditionString_Vc
			SELECT @v_CntQry_Vc= 'SELECT Count(tenderDtlId) FROM dbo.tbl_TenderDetails WHERE ' + @v_ConditionString_Vc

		/* START CODE: DYNAMIC QUERY TO GET RECORDS */
			SELECT @v_Qry_Vc=
			'DECLARE @v_TotalPageCnt_Int INT, @v_TotalRecordCnt_Int INT;
			SET @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
			SET @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
			SELECT	tmp.RowNumber, A.tenderId tenderDtlId, A.reoiRfpRefNo, A.procurementNature, A.tenderBrief, A.ministry,
				A.division, A.agency,	A.peOfficeName, A.procurementType, A.procurementMethod, A.tenderPubDt,
				A.submissionDt, @v_TotalPageCnt_Int as TotalPages, @v_TotalRecordCnt_Int as totalRecords,tenderId,tenderbrief,tenderStatus,
--				CASE WHEN ((    SELECT top 1 rptstatus FROM tbl_EvalRptSentToAA ea
--                                        	WHERE rptstatus LIKE ''reject%'' AND  ea.tenderId=a.tenderId ORDER BY evalRptToAAId DESC  )=''Rejected'') OR((    SELECT TOP 1 rptstatus FROM tbl_EvalRptSentToAA ea
--                                                WHERE rptstatus LIKE ''reject%'' AND  ea.tenderId=a.tenderId ORDER BY evalRptToAAId DESC  )=''Rejected / Re-Tendering'')
				CASE WHEN ((select COUNT(*) from tbl_TenderDetails where reTenderId=a.tenderId )> 0) THEN ''Re-Tender''
				WHEN ((SELECT TOP 1 rptstatus FROM tbl_EvalRptSentToAA ea
                                            WHERE rptstatus LIKE ''reject%'' AND  ea.tenderId=a.tenderId ORDER BY evalRptToAAId DESC  )=''Rejected / Re-Tendering'')
                                THEN ''Re-Tendered''
				WHEN ((	SELECT top 1 rptstatus FROM tbl_EvalRptSentToAA ea
                                      WHERE rptstatus like ''reject%'' AND  ea.tenderId=a.tenderId ORDER BY evalRptToAAId DESC  )=''Rejected'')
                                THEN ''Rejected''
                                when ((select TOP 1 tcs.ContractNOAId from tbl_NoaIssueDetails tnid, tbl_ContractNOARelation tcs
                                        where tnid.noaIssueId = tcs.NOAID and tnid.tenderId = a.tenderId order by tnid.noaIssueId DESC) > 0 )
                                THEN ''Awarded''
                                ELSE ''Not'' END AS tenderevalstatus
			FROM	dbo.tbl_TenderDetails A
				INNER JOIN (SELECT RowNumber, tenderDtlId FROM
                                            (SELECT ROW_NUMBER() OVER (Order by tenderPubDt Desc) AS RowNumber, tenderDtlId, tenderPubDt,tenderId
                                                    FROM dbo.tbl_TenderDetails WHERE '	+ @v_ConditionString_Vc +' ) B
                                                    WHERE RowNumber BETWEEN ' + CONVERT(VARCHAR(50), @v_StartAt_Int) + ' AND ' + CONVERT(VARCHAR(50), @v_StopAt_Int)
			+') Tmp
			ON A.tenderDtlId=tmp.tenderDtlId ORDER BY A.tenderPubDt DESC'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */

		-- // PRINT QUERY HERE
		Print(@v_Qry_Vc)
		-- // EXECUTE QUERY
		Exec(@v_Qry_Vc)
	END
	ELSE IF @v_Action_inVc='get alltendersbytype'
	BEGIN
	declare @v_filterPageignation varchar(300)

		set @v_Status_inVc='Approved'
		-- // GET ALL TENDERS - DEFAULT/SEARCH INCLUDED
		Set @v_ConditionString_Vc='       docaccess=''Open'' '

		IF @v_filter = 'ownCategory'
				SET @v_filterPageignation= ' '
		ELSE
			SET @v_filterPageignation= ' WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) +' And '+ CONVERT(varchar(50), @v_StopAt_Int)

		/* START CODE: TO SET WHERE CONDITION */
		IF @v_procurementNatureId_inInt is not null and @v_procurementNatureId_inInt<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementNatureId=' + Convert(varchar(50), @v_procurementNatureId_inInt)
		IF @v_Ministry_inVc is not null and @v_Ministry_inVc<>''	and @v_Ministry_inVc<> '0'
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tbl_OfficeMaster.departmentId=''' + Convert(varchar(50),@v_Ministry_inVc) + ''''
	--	IF @v_Division_inVc is not null and @v_Division_inVc<>''
		--		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And Division=''' + @v_Division_inVc + ''''
		IF @v_Agency_inVc is not null and @v_Agency_inVc<>''
	 	Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And cpvcode like ''%' + @v_Agency_inVc + '%'''
		IF @v_PeOfficeName_inVc is not null and @v_PeOfficeName_inVc<>'' and @v_PeOfficeName_inVc<>0
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tbl_TenderDetails.officeId=''' + Convert(varchar(50),@v_PeOfficeName_inVc) + ''''
		IF @v_procurementType_inVc is not null and @v_procurementType_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementType=''' + @v_procurementType_inVc + ''''
		IF @v_procurementMethod_inInt is not null and @v_procurementMethod_inInt<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementMethodid=' + Convert(varchar(50), @v_procurementMethod_inInt)
		IF @v_TenderId_Int is not null and @v_TenderId_Int<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderId=' + Convert(varchar(50), @v_TenderId_Int)
		IF @v_reoiRfpRefNo_inVc is not null and @v_reoiRfpRefNo_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And reoiRfpRefNo=''' + @v_reoiRfpRefNo_inVc + ''''


		IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtTo_inVc !='' and @v_tenderPubDtFrom_inVc!=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime) between ''' + @v_tenderPubDtFrom_inVc + ''' And ''' + @v_tenderPubDtTo_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is null and @v_tenderPubDtFrom_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime) >= ''' + @v_tenderPubDtFrom_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtTo_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime) <= ''' + @v_tenderPubDtTo_inVc + ''''
		End


		IF @v_SubmissionDtFrom_inVc is not null And @v_SubmissionDtTo_inVc is not null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) between ''' + @v_SubmissionDtFrom_inVc + ''' And ''' + @v_SubmissionDtTo_inVc + ''''
		End
		ELSE IF @v_SubmissionDtFrom_inVc is not null And @v_SubmissionDtTo_inVc is null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) >= ''' + @v_SubmissionDtFrom_inVc + ''''
		End
		ELSE IF @v_SubmissionDtFrom_inVc is null And @v_SubmissionDtTo_inVc is not null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) <= ''' + @v_SubmissionDtTo_inVc + ''''
		End
		/*code by Proshanto*/

		IF @v_district_inVc is not null and @v_district_inVc<>'' and @v_district_inVc!='0'
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And peDistrict=''' + CAST(@v_district_inVc AS VARCHAR) + ''''
		
         IF @v_thana_inVc is not null and @v_thana_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And  upjilla = ''' + CAST(@v_thana_inVc AS VARCHAR) + ''' '

         /*code end by Proshanto*/

		IF @v_ViewType_inVc is not null and @v_ViewType_inVc<>''
		Begin
			IF @v_ViewType_inVc='Live'
			Begin
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And SubmissionDt >= getdate() And tenderPubDt < getdate()'
					Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderstatus in(''Approved'') '
			End
			Else IF @v_ViewType_inVc='Archive'
			Begin
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And SubmissionDt < getdate() '
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderstatus in(''Approved'') '
			End
		End

		IF @v_ViewType_inVc is not null and @v_ViewType_inVc<>''
		Begin
			IF @v_ViewType_inVc='Cancelled'
			Begin

			IF @v_Status_inVc is not null and @v_Status_inVc<>''
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' and  tenderStatus=''Cancelled'''


			End
		End
		else
		begin


		IF @v_Status_inVc is not null and @v_Status_inVc<>''
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderPubDt < getdate() And tenderstatus in(''Cancelled'',''Approved'')'


		end

		--IF @v_Status_inVc is not null and @v_Status_inVc<>''
		--Begin
		--		Select @v_ConditionString_Vc=
		--		Case When (@v_ConditionString_Vc='')
		--			 Then 	+ ' tenderStatus=''' + @v_Status_inVc + ''''
		--			 Else (@v_ConditionString_Vc + ' And tenderStatus=''' + @v_Status_inVc+'''')
		--		End
		--End
		/* END CODE: TO SET WHERE CONDITION */

		-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
		Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT(tenderDtlId) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+') FROM dbo.tbl_TenderDetails INNER JOIN tbl_OfficeMaster
ON tbl_TenderDetails.officeId= tbl_OfficeMaster.officeId WHERE' + @v_ConditionString_Vc
		Select @v_CntQry_Vc='SELECT Count(tenderDtlId) FROM dbo.tbl_TenderDetails INNER JOIN tbl_OfficeMaster
ON tbl_TenderDetails.officeId= tbl_OfficeMaster.officeId WHERE' + @v_ConditionString_Vc

		SeLECT @v_PgCntQry_Vc=REPLACE(@v_PgCntQry_Vc,'WHERE AND','WHERE'),
				@v_CntQry_Vc=REPLACE(@v_CntQry_Vc,'WHERE AND','WHERE')

		/* START CODE: DYNAMIC QUERY TO GET RECORDS */
		Select @v_Qry_Vc=
		'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
		SELECT	tmp.RowNumber, * FROM 
		(SELECT A.tenderId tenderDtlId, A.reoiRfpRefNo, A.procurementNature, A.tenderBrief, A.ministry,
				A.division, A.agency,	A.peOfficeName, A.procurementType, A.procurementMethod, A.tenderPubDt,
				A.submissionDt, A.openingDt, @v_TotalPageCnt_Int as TotalPages, @v_TotalRecordCnt_Int as totalRecords,tenderId, A.tenderStatus,
--				case when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
--			where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected'') OR ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
--				where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected / Re-Tendering'') then ''Re-Tendered''
				case WHEN ((select COUNT(*) from tbl_TenderDetails where reTenderId=a.tenderId )> 0) THEN ''Re-Tender''
				when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
                                        where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected'')
                                    THEN ''Rejected''
                                    WHEN ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
                                            where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected / Re-Tendering'')
                                    then ''Re-Tendered''
                                    when ((select TOP 1 tcs.ContractNOAId from tbl_NoaIssueDetails tnid, tbl_ContractNOARelation tcs
                                            where tnid.noaIssueId = tcs.NOAID and tnid.tenderId = a.tenderId order by tnid.noaIssueId DESC) > 0 )
                                    THEN ''Awarded''
                                    else ''Not'' end as tenderevalstatus, A.officeId,om.upjilla
		FROM	dbo.tbl_TenderDetails A Inner Join tbl_OfficeMaster om ON A.officeId= om.officeId) A
		Inner Join
		(Select RowNumber, tenderDtlId From
			(SELECT ROW_NUMBER() Over (Order by tenderPubDt Desc) as RowNumber, tenderDtlId, tenderPubDt
				FROM dbo.tbl_TenderDetails INNER JOIN tbl_OfficeMaster
ON tbl_TenderDetails.officeId= tbl_OfficeMaster.officeId
				WHERE'	+ @v_ConditionString_Vc
			+' ) B
			'+ @v_filterPageignation+') Tmp
		On A.tenderDtlId=tmp.tenderDtlId ORDER BY A.tenderPubDt DESC'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */

		-- // PRINT QUERY HERE
		Set @v_Qry_Vc=REPLACE(@v_Qry_Vc,'WHERE AND','WHERE')
		 Print(@v_Qry_Vc)
		-- // EXECUTE QUERY
		Exec(@v_Qry_Vc)

	END
	ELSE IF @v_Action_inVc='get officeralltendersbytype'
	BEGIN
		set @v_Status_inVc='Approved'
		-- // GET ALL TENDERS - DEFAULT/SEARCH INCLUDED

		/* START CODE: TO SET WHERE CONDITION */
		IF @v_procurementNatureId_inInt is not null and @v_procurementNatureId_inInt<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementNatureId=' + Convert(varchar(50), @v_procurementNatureId_inInt)
		IF @v_Ministry_inVc is not null and @v_Ministry_inVc<>''	and @v_Ministry_inVc<> '0'
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tbl_tenderDetails.departmentId=''' + Convert(varchar(50),@v_Ministry_inVc) + ''''
	--	IF @v_Division_inVc is not null and @v_Division_inVc<>''
		--		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And Division=''' + @v_Division_inVc + ''''
		IF @v_Agency_inVc is not null and @v_Agency_inVc<>''
	 	Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And cpvcode like ''%' + @v_Agency_inVc + '%'''
		IF @v_PeOfficeName_inVc is not null and @v_PeOfficeName_inVc<>'' and @v_PeOfficeName_inVc<>0
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And officeId=''' + Convert(varchar(50),@v_PeOfficeName_inVc) + ''''
		IF @v_procurementType_inVc is not null and @v_procurementType_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementType=''' + @v_procurementType_inVc + ''''
		IF @v_procurementMethod_inInt is not null and @v_procurementMethod_inInt<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementMethodid=' + Convert(varchar(50), @v_procurementMethod_inInt)
		IF @v_TenderId_Int is not null and @v_TenderId_Int<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderId=' + Convert(varchar(50), @v_TenderId_Int)
		IF @v_reoiRfpRefNo_inVc is not null and @v_reoiRfpRefNo_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And reoiRfpRefNo=''' + @v_reoiRfpRefNo_inVc + ''''


		IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtTo_inVc !='' and @v_tenderPubDtFrom_inVc!=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime) between ''' + @v_tenderPubDtFrom_inVc + ''' And ''' + @v_tenderPubDtTo_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is null and @v_tenderPubDtFrom_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime) >= ''' + @v_tenderPubDtFrom_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtTo_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime) <= ''' + @v_tenderPubDtTo_inVc + ''''
		End


		IF @v_SubmissionDtFrom_inVc is not null And @v_SubmissionDtTo_inVc is not null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) between ''' + @v_SubmissionDtFrom_inVc + ''' And ''' + @v_SubmissionDtTo_inVc + ''''
		End
		ELSE IF @v_SubmissionDtFrom_inVc is not null And @v_SubmissionDtTo_inVc is null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) >= ''' + @v_SubmissionDtFrom_inVc + ''''
		End
		ELSE IF @v_SubmissionDtFrom_inVc is null And @v_SubmissionDtTo_inVc is not null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) <= ''' + @v_SubmissionDtTo_inVc + ''''
		End

		/*code by Proshanto Kumar Saha*/

		IF @v_district_inVc is not null and @v_district_inVc<>'' and @v_district_inVc!='0'
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And peDistrict=''' + CAST(@v_district_inVc AS VARCHAR) + ''''
		
         IF @v_thana_inVc is not null and @v_thana_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And  upjilla = ''' + CAST(@v_thana_inVc AS VARCHAR) + ''' '

         /*code end by Proshanto Kumar Saha*/

		IF @v_ViewType_inVc is not null and @v_ViewType_inVc<>''
		Begin
			IF @v_ViewType_inVc='Live'
			Begin
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And SubmissionDt >= getdate() And tenderPubDt < getdate()'
					Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderstatus in(''Approved'') '
			End
			Else IF @v_ViewType_inVc='Archive'
			Begin
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And SubmissionDt < getdate() '
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderstatus in(''Approved'') '
			End
		End

		IF @v_ViewType_inVc is not null and @v_ViewType_inVc<>''
		Begin
			IF @v_ViewType_inVc='Cancelled'
			Begin

			IF @v_Status_inVc is not null and @v_Status_inVc<>''
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' and  tenderStatus=''Cancelled'''


			End
		End
		else
		begin


		IF @v_Status_inVc is not null and @v_Status_inVc<>''
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderPubDt < getdate() And tenderstatus in(''Cancelled'',''Approved'')'


		end

		--IF @v_Status_inVc is not null and @v_Status_inVc<>''
		--Begin
		--		Select @v_ConditionString_Vc=
		--		Case When (@v_ConditionString_Vc='')
		--			 Then 	+ ' tenderStatus=''' + @v_Status_inVc + ''''
		--			 Else (@v_ConditionString_Vc + ' And tenderStatus=''' + @v_Status_inVc+'''')
		--		End
		--End
		/* END CODE: TO SET WHERE CONDITION */

		-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
		Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT(tenderDtlId) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+') FROM dbo.tbl_TenderDetails INNER JOIN tbl_OfficeMaster
ON tbl_TenderDetails.officeId= tbl_OfficeMaster.officeId WHERE' + @v_ConditionString_Vc
		Select @v_CntQry_Vc='SELECT Count(tenderDtlId) FROM dbo.tbl_TenderDetails INNER JOIN tbl_OfficeMaster
ON tbl_TenderDetails.officeId= tbl_OfficeMaster.officeId WHERE' + @v_ConditionString_Vc

		SeLECT @v_PgCntQry_Vc=REPLACE(@v_PgCntQry_Vc,'WHERE AND','WHERE'),
				@v_CntQry_Vc=REPLACE(@v_CntQry_Vc,'WHERE AND','WHERE')

		/* START CODE: DYNAMIC QUERY TO GET RECORDS */
		Select @v_Qry_Vc=
		'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
		SELECT	tmp.RowNumber,* FROM ( SELECT A.tenderId tenderDtlId, A.reoiRfpRefNo, A.procurementNature, A.tenderBrief, A.ministry,
				A.division, A.agency,	A.peOfficeName, A.procurementType, A.procurementMethod, A.tenderPubDt,
				A.submissionDt, A.openingDt, @v_TotalPageCnt_Int as TotalPages, @v_TotalRecordCnt_Int as totalRecords,tenderId,A.tenderStatus,
--				case when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
--                              where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected'') OR ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
--				where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected / Re-Tendering'') then ''Re-Tendered''
				case WHEN ((select COUNT(*) from tbl_TenderDetails where reTenderId=a.tenderId )> 0) THEN ''Re-Tender''
				when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
                                            where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected'')
                                    THEN ''Rejected''
                                    WHEN ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
                                        where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected / Re-Tendering'')
                                    then ''Re-Tendered''
                                    when ((select TOP 1 tcs.ContractNOAId from tbl_NoaIssueDetails tnid, tbl_ContractNOARelation tcs
                                            where tnid.noaIssueId = tcs.NOAID and tnid.tenderId = a.tenderId order by tnid.noaIssueId DESC) > 0 )
                                    THEN ''Awarded''
                                    else ''Not'' end as tenderevalstatus, A.officeId,om.upjilla
		FROM	dbo.tbl_TenderDetails A Inner Join tbl_OfficeMaster om ON A.officeId= om.officeId) A
		Inner Join
		(Select RowNumber, tenderDtlId From
			(SELECT ROW_NUMBER() Over (Order by tenderPubDt Desc) as RowNumber, tenderDtlId, tenderPubDt
				FROM dbo.tbl_TenderDetails INNER JOIN tbl_OfficeMaster
ON tbl_TenderDetails.officeId= tbl_OfficeMaster.officeId
				WHERE'	+ @v_ConditionString_Vc
			+' ) B
			WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+') Tmp
		On A.tenderDtlId=tmp.tenderDtlId ORDER BY A.tenderPubDt DESC'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */

		-- // PRINT QUERY HERE
		Set @v_Qry_Vc=REPLACE(@v_Qry_Vc,'WHERE AND','WHERE')
		 Print(@v_Qry_Vc)
		-- // EXECUTE QUERY
		Exec(@v_Qry_Vc)

	END
	ELSE IF @v_Action_inVc='get rightsalltendersbytype'
	BEGIN
		set @v_Status_inVc='Approved'
		-- // GET ALL TENDERS - DEFAULT/SEARCH INCLUDED
		Set @v_ConditionString_Vc='       (docaccess=''Open'' or tenderid in (select tenderid from  tbl_LimitedTenders l ,tbl_TendererMaster cm
		where l.userId=cm.userId and cm.companyId in(select companyId from tbl_TendererMaster where userId='+Convert(varchar(50), @v_UserId_inInt)+' ) )) '
		/* START CODE: TO SET WHERE CONDITION */
		IF @v_procurementNatureId_inInt is not null and @v_procurementNatureId_inInt<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementNatureId=' + Convert(varchar(50), @v_procurementNatureId_inInt)
		IF @v_Ministry_inVc is not null and @v_Ministry_inVc<>''	and @v_Ministry_inVc<> '0'
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tbl_tenderDetails.departmentId=''' + Convert(varchar(50),@v_Ministry_inVc) + ''''
	--	IF @v_Division_inVc is not null and @v_Division_inVc<>''
		--		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And Division=''' + @v_Division_inVc + ''''
		IF @v_Agency_inVc is not null and @v_Agency_inVc<>''
	 	Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And cpvcode like ''%' + @v_Agency_inVc + '%'''
		IF @v_PeOfficeName_inVc is not null and @v_PeOfficeName_inVc<>'' and @v_PeOfficeName_inVc<>0
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And officeId=''' + Convert(varchar(50),@v_PeOfficeName_inVc) + ''''
		IF @v_procurementType_inVc is not null and @v_procurementType_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementType=''' + @v_procurementType_inVc + ''''
		IF @v_procurementMethod_inInt is not null and @v_procurementMethod_inInt<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementMethodid=' + Convert(varchar(50), @v_procurementMethod_inInt)
		IF @v_TenderId_Int is not null and @v_TenderId_Int<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderId=' + Convert(varchar(50), @v_TenderId_Int)
		IF @v_reoiRfpRefNo_inVc is not null and @v_reoiRfpRefNo_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And reoiRfpRefNo=''' + @v_reoiRfpRefNo_inVc + ''''


		IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtTo_inVc !='' and @v_tenderPubDtFrom_inVc!=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime) between ''' + @v_tenderPubDtFrom_inVc + ''' And ''' + @v_tenderPubDtTo_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is null and @v_tenderPubDtFrom_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime) >= ''' + @v_tenderPubDtFrom_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtTo_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime) <= ''' + @v_tenderPubDtTo_inVc + ''''
		End


		IF @v_SubmissionDtFrom_inVc is not null And @v_SubmissionDtTo_inVc is not null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) between ''' + @v_SubmissionDtFrom_inVc + ''' And ''' + @v_SubmissionDtTo_inVc + ''''
		End
		ELSE IF @v_SubmissionDtFrom_inVc is not null And @v_SubmissionDtTo_inVc is null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) >= ''' + @v_SubmissionDtFrom_inVc + ''''
		End
		ELSE IF @v_SubmissionDtFrom_inVc is null And @v_SubmissionDtTo_inVc is not null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) <= ''' + @v_SubmissionDtTo_inVc + ''''
		End

		IF @v_ViewType_inVc is not null and @v_ViewType_inVc<>''
		Begin
			IF @v_ViewType_inVc='Live'
			Begin
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And SubmissionDt >= getdate() '
			End
			Else IF @v_ViewType_inVc='Archive'
			Begin
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And SubmissionDt < getdate() '
			End
		End

		IF @v_ViewType_inVc is not null and @v_ViewType_inVc<>''
		Begin
			IF @v_ViewType_inVc='Cancelled'
			Begin

			IF @v_Status_inVc is not null and @v_Status_inVc<>''
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' and  tenderStatus=''Cancelled'''


			End
		End
		else
		begin


		IF @v_Status_inVc is not null and @v_Status_inVc<>''
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderStatus=''' + @v_Status_inVc +''''


		end

		--IF @v_Status_inVc is not null and @v_Status_inVc<>''
		--Begin
		--		Select @v_ConditionString_Vc=
		--		Case When (@v_ConditionString_Vc='')
		--			 Then 	+ ' tenderStatus=''' + @v_Status_inVc + ''''
		--			 Else (@v_ConditionString_Vc + ' And tenderStatus=''' + @v_Status_inVc+'''')
		--		End
		--End
		/* END CODE: TO SET WHERE CONDITION */

		-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
		Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT(tenderDtlId) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+') FROM dbo.tbl_TenderDetails WHERE' + @v_ConditionString_Vc
		Select @v_CntQry_Vc='SELECT Count(tenderDtlId) FROM dbo.tbl_TenderDetails WHERE' + @v_ConditionString_Vc

		SeLECT @v_PgCntQry_Vc=REPLACE(@v_PgCntQry_Vc,'WHERE AND','WHERE'),
				@v_CntQry_Vc=REPLACE(@v_CntQry_Vc,'WHERE AND','WHERE')

		/* START CODE: DYNAMIC QUERY TO GET RECORDS */
		Select @v_Qry_Vc=
		'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
		SELECT	tmp.RowNumber, A.tenderId tenderDtlId, A.reoiRfpRefNo, A.procurementNature, A.tenderBrief, A.ministry,
				A.division, A.agency,	A.peOfficeName, A.procurementType, A.procurementMethod, A.tenderPubDt,
				A.submissionDt, A.openingDt, @v_TotalPageCnt_Int as TotalPages, @v_TotalRecordCnt_Int as totalRecords,tenderId,tenderbrief,A.tenderStatus,
--				case when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
--                              where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected'') OR ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
--				where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected / Re-Tendering'') then ''Re-Tendered''
				case when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
                                            where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected'') 
                                    THEN ''Rejected'' 
                                    WHEN ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
                                            where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected / Re-Tendering'') 
                                    then ''Re-Tendered'' 
                                    when ((select TOP 1 tcs.ContractNOAId from tbl_NoaIssueDetails tnid, tbl_ContractNOARelation tcs
                                            where tnid.noaIssueId = tcs.NOAID and tnid.tenderId = a.tenderId order by tnid.noaIssueId DESC) > 0 )
                                    THEN ''Awarded''
                                    else ''Not'' end as tenderevalstatus
		FROM	dbo.tbl_TenderDetails A
		Inner Join
		(Select RowNumber, tenderDtlId From
			(SELECT ROW_NUMBER() Over (Order by tenderPubDt Desc) as RowNumber, tenderDtlId, tenderPubDt
				FROM dbo.tbl_TenderDetails
				WHERE'	+ @v_ConditionString_Vc
			+' ) B
			WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+') Tmp
		On A.tenderDtlId=tmp.tenderDtlId ORDER BY A.tenderPubDt DESC'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */

		-- // PRINT QUERY HERE
		Set @v_Qry_Vc=REPLACE(@v_Qry_Vc,'WHERE AND','WHERE')
		 Print(@v_Qry_Vc)
		-- // EXECUTE QUERY
		Exec(@v_Qry_Vc)

	END

	ELSE IF @v_Action_inVc='get rightssearchtenders'
	BEGIN
		set @v_Status_inVc='Approved'
		-- // GET ALL TENDERS - DEFAULT/SEARCH INCLUDED
		Set @v_ConditionString_Vc='    (  docaccess=''Open'' or tenderid in (select tenderid from  tbl_LimitedTenders l ,tbl_TendererMaster cm
		where l.userId=cm.userId and cm.companyId in(select companyId from tbl_TendererMaster where userId='+Convert(varchar(50), @v_UserId_inInt)+' ) )) '
		/* START CODE: TO SET WHERE CONDITION */
		IF @v_procurementNatureId_inInt is not null and @v_procurementNatureId_inInt<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementNatureId=' + Convert(varchar(50), @v_procurementNatureId_inInt)
		IF @v_Ministry_inVc is not null and @v_Ministry_inVc<>''	and @v_Ministry_inVc<> '0'
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tbl_tenderDetails.departmentId=''' + Convert(varchar(50),@v_Ministry_inVc) + ''''
	--	IF @v_Division_inVc is not null and @v_Division_inVc<>''
		--		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And Division=''' + @v_Division_inVc + ''''
		IF @v_Agency_inVc is not null and @v_Agency_inVc<>''
	 	Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And cpvcode like ''%' + @v_Agency_inVc + '%'''
		IF @v_PeOfficeName_inVc is not null and @v_PeOfficeName_inVc<>'' and @v_PeOfficeName_inVc<>0
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And officeId=''' + Convert(varchar(50),@v_PeOfficeName_inVc) + ''''
		IF @v_procurementType_inVc is not null and @v_procurementType_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementType=''' + @v_procurementType_inVc + ''''
		IF @v_procurementMethod_inInt is not null and @v_procurementMethod_inInt<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And procurementMethodid=' + Convert(varchar(50), @v_procurementMethod_inInt)
		IF @v_TenderId_Int is not null and @v_TenderId_Int<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderId=' + Convert(varchar(50), @v_TenderId_Int)
		IF @v_reoiRfpRefNo_inVc is not null and @v_reoiRfpRefNo_inVc<>''
				Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And reoiRfpRefNo=''' + @v_reoiRfpRefNo_inVc + ''''



		IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtTo_inVc !='' and @v_tenderPubDtFrom_inVc!=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime) between ''' + @v_tenderPubDtFrom_inVc + ''' And ''' + @v_tenderPubDtTo_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is not null And @v_tenderPubDtTo_inVc is null and @v_tenderPubDtFrom_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime) >= ''' + @v_tenderPubDtFrom_inVc + ''''
		End
		ELSE IF @v_tenderPubDtFrom_inVc is null And @v_tenderPubDtTo_inVc is not null and @v_tenderPubDtTo_inVc !=''
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (tenderPubDt as Float)) as Datetime) <= ''' + @v_tenderPubDtTo_inVc + ''''
		End


		IF @v_SubmissionDtFrom_inVc is not null And @v_SubmissionDtTo_inVc is not null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) between ''' + @v_SubmissionDtFrom_inVc + ''' And ''' + @v_SubmissionDtTo_inVc + ''''
		End
		ELSE IF @v_SubmissionDtFrom_inVc is not null And @v_SubmissionDtTo_inVc is null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) >= ''' + @v_SubmissionDtFrom_inVc + ''''
		End
		ELSE IF @v_SubmissionDtFrom_inVc is null And @v_SubmissionDtTo_inVc is not null
		Begin
			Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (SubmissionDt as Float)) as Datetime) <= ''' + @v_SubmissionDtTo_inVc + ''''
		End

		IF @v_Status_inVc is not null and @v_Status_inVc<>''
			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderStatus=''' + @v_Status_inVc +''''

		--IF @v_Status_inVc is not null and @v_Status_inVc<>''
		--Begin
		--		Select @v_ConditionString_Vc=
		--		Case When (@v_ConditionString_Vc='')
		--			 Then 	+ ' tenderStatus=''' + @v_Status_inVc + ''''
		--			 Else (@v_ConditionString_Vc + ' And tenderStatus=''' + @v_Status_inVc+'''')
		--		End
		--End
		/* END CODE: TO SET WHERE CONDITION */

		-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
		Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT(tenderDtlId) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+') FROM dbo.tbl_TenderDetails WHERE' + @v_ConditionString_Vc
		Select @v_CntQry_Vc='SELECT Count(tenderDtlId) FROM dbo.tbl_TenderDetails WHERE' + @v_ConditionString_Vc

		SeLECT @v_PgCntQry_Vc=REPLACE(@v_PgCntQry_Vc,'WHERE AND','WHERE'),
				@v_CntQry_Vc=REPLACE(@v_CntQry_Vc,'WHERE AND','WHERE')

		/* START CODE: DYNAMIC QUERY TO GET RECORDS */
		Select @v_Qry_Vc=
		'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
		SELECT	tmp.RowNumber, A.tenderId tenderDtlId, A.reoiRfpRefNo, A.procurementNature, A.tenderBrief, A.ministry,
				A.division, A.agency,	A.peOfficeName, A.procurementType, A.procurementMethod, A.tenderPubDt,
				A.submissionDt, @v_TotalPageCnt_Int as TotalPages, @v_TotalRecordCnt_Int as totalRecords,tenderId,tenderbrief,A.tenderStatus,
--				case when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
--			where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected'') OR ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
--				where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected / Re-Tendering'') then ''Re-Tendered''
				case when ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
                                            where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected'')
                                    THEN ''Rejected''
                                    WHEN  ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
                                                    where rptstatus like ''reject%'' and  ea.tenderId=a.tenderId order by evalRptToAAId desc  )=''Rejected / Re-Tendering'')
                                    then ''Re-Tendered''
                                    when ((select TOP 1 tcs.ContractNOAId from tbl_NoaIssueDetails tnid, tbl_ContractNOARelation tcs
                                            where tnid.noaIssueId = tcs.NOAID and tnid.tenderId = a.tenderId order by tnid.noaIssueId DESC) > 0 )
                                    THEN ''Awarded''
                                    else ''Not'' end as tenderevalstatus
		FROM	dbo.tbl_TenderDetails A
		Inner Join
		(Select RowNumber, tenderDtlId From
			(SELECT ROW_NUMBER() Over (Order by tenderPubDt Desc) as RowNumber, tenderDtlId, tenderPubDt
				FROM dbo.tbl_TenderDetails
				WHERE'	+ @v_ConditionString_Vc
			+' ) B
			WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+') Tmp
		On A.tenderDtlId=tmp.tenderDtlId ORDER BY A.tenderPubDt DESC'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */

		-- // PRINT QUERY HERE
		Set @v_Qry_Vc=REPLACE(@v_Qry_Vc,'WHERE AND','WHERE')
		 Print(@v_Qry_Vc)
		-- // EXECUTE QUERY
		Exec(@v_Qry_Vc)

	END
SET NOCOUNT OFF;
END


