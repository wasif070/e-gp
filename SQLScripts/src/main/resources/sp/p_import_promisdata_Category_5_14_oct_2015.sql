USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_import_promisdata_Category_5_14_oct_2015]    Script Date: 4/24/2016 11:10:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- Modified By Dohatec on 10th August 2015 To Segregate Promis data import basis of Indicator Category 
-- =============================================
ALTER PROCEDURE [dbo].[p_import_promisdata_Category_5_14_oct_2015]
AS
BEGIN

UPDATE [Tbl_PromisIndicator]
SET 
[DaysBtwnTenEvalTenApp] = T.DerivedDaysBtwnTenEvalTenApp
           ,[IsApprovedByProperAA] = T.IsApprovedByProperAA
           ,[SubofEvalReportAA] = T.SubofEvalReportAA           
           ,[TERReviewByOther] = T.TERReviewByOther
           ,[ContractAppByHighAuth] = T.ContractAppByHighAuth
FROM      
(  select tenderId DerivedTenderID,
case when (select count(distinct envelopeId) from tbl_TenderEnvelopes where tenderId = td.tenderid)=1 
then ISNULL( convert(varchar(20),DATEDIFF(d,
	(case	when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and tc.committeeType in ('TEC','PEC'))=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					)then 
						(	SELECT ISNULL((select MIN(processDate) 
							from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
							on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	and wfl.wfRoleId = 1							
							where  [action] = 'Forward' and wfl.activityId = 13 and wfh.objectId = td.tenderid) , 
							(	select MIN(sentDate) from tbl_EvalRptSentToAA era where era.tenderId=td.tenderid 
								and roundId = (select MIN(roundId) from tbl_TORRptSign 
								where tenderId = td.tenderid and reportType = 'TER4')
								group by tenderId))
						)	
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and td.procurementNatureId != 3 
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					) then 
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc 
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
							where tc.tenderId = td.tenderid and td.procurementNatureId != 3 
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
							where trs.tenderId = td.tenderid and trs.reportType = 'TER2' and td.procurementNatureId != 3 
							and roundId =(select MIN(roundId) from tbl_TORRptSign 
							where tenderId = td.tenderid and reportType = 'TER2')
						) then 
						(	case  when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))>0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))										
									when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = 0)>0 and (select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))=0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 
									and roundId = 0)	
							end										
						)	end
					) 			
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and td.procurementNatureId = 3 
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					) then 
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc 
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
							where tc.tenderId = td.tenderid and td.procurementNatureId = 3 
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
							where trs.tenderId = td.tenderid and trs.reportType = 'TER1' 
							and roundId =(select MIN(roundId) from tbl_TORRptSign 
							where tenderId = td.tenderid and reportType = 'TER1')
						) then 
						(	case  when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER1'))>0
									then
									(	SELECT ISNULL((select MIN(processDate) 
										from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
										on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	
										and wfl.wfRoleId = 1 where  [action] = 'Forward' and wfl.activityId = 13 
										and wfh.objectId = td.tenderid) , 							
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER1'))))	
							end										
						)	end
					)							
					end
	),(select MIN(rptApproveDt) from tbl_EvalRptSentToAA era
where era.tenderId=td.tenderId
group by tenderId ))),'NA') 
when (select count(distinct envelopeId) from tbl_TenderEnvelopes where tenderId = td.tenderid)=2 
then
(case when (select count(*) from tbl_EvalRptSentToAA where tenderId = td.tenderId and envelopeId =0 )>0
then
(case when (select count(*) from tbl_EvalRptSentToAA where tenderId = td.tenderId and envelopeId =1 )>0
then
 ISNULL( convert(varchar(20),(DATEDIFF(d,
	(case	when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and tc.committeeType in ('TEC','PEC'))=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					)
					then 
						(	SELECT ISNULL((select MIN(processDate) from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
							on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	and wfl.wfRoleId = 1							
							where  [action] = 'Forward' and wfl.activityId = 13 and wfh.processDate > (select MIN(rptApproveDt) from tbl_EvalRptSentToAA era
							where era.tenderId=td.tenderId and envelopeId = 0 group by tenderId) and wfh.objectId = td.tenderid) , 
							(	select MIN(sentDate) from tbl_EvalRptSentToAA era where era.tenderId=td.tenderid  and envelopeId = 1
								and roundId = (select MIN(roundId) from tbl_TORRptSign where tenderId = td.tenderid and reportType = 'TER4')
								group by tenderId))
						)
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc inner join tbl_CommitteeMembers tcm 
					ON tc.committeeId = tcm.committeeId where tc.tenderId = td.tenderid and td.procurementNatureId != 3 
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign where tenderId = td.tenderid and reportType = 'TER4')
					) then 
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc inner join tbl_CommitteeMembers tcm 
							ON tc.committeeId = tcm.committeeId where tc.tenderId = td.tenderid and td.procurementNatureId != 3 
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
							where trs.tenderId = td.tenderid and trs.reportType = 'TER2' and td.procurementNatureId != 3 
							and roundId =(select MIN(roundId) from tbl_TORRptSign 
							where tenderId = td.tenderid and reportType = 'TER2')
						) then 
						(	case  when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA tersta 
									where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))>0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 and envelopeId =1
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))										
									when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = 0)>0 and (select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))=0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 and envelopeId = 1
									and roundId = 0)	
							end											
						)	end
					) 			
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm 
					ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and td.procurementNatureId = 3 
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					) then 
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc 
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
							where tc.tenderId = td.tenderid and td.procurementNatureId = 3 
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
							where trs.tenderId = td.tenderid and trs.reportType = 'TER1' 
							and roundId =(select MIN(roundId) from tbl_TORRptSign 
							where tenderId = td.tenderid and reportType = 'TER1')
						) then 
						(	case  when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER1'))>0
									then
									(	SELECT ISNULL((select MIN(processDate) 
										from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
										on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	
										and wfl.wfRoleId = 1 where  [action] = 'Forward' and wfl.activityId = 13 and wfh.processDate > (select MIN(rptApproveDt) from tbl_EvalRptSentToAA era
										where era.tenderId=td.tenderId and envelopeId = 0 group by tenderId)
										and wfh.objectId = td.tenderid) , 							
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3 and tersta.envelopeId = 1
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER1'))))	
							end											
						)	end
					)										
					end
	),(select MIN(rptApproveDt) from tbl_EvalRptSentToAA era
where era.tenderId=td.tenderId and era.envelopeId = 1
group by tenderId )))
+  
(DATEDIFF(d,
	(case	when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and tc.committeeType in ('TEC','PEC'))=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					)
					then 
						(	SELECT ISNULL((select MIN(processDate) 
							from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
							on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	and wfl.wfRoleId = 1							
							where  [action] = 'Forward' and wfl.activityId = 13 and wfh.objectId = td.tenderid) , 
							(	select MIN(sentDate) from tbl_EvalRptSentToAA era
								where era.tenderId=td.tenderid  and era.envelopeId=0
								and roundId = (select MIN(roundId) from tbl_TORRptSign 
								where tenderId = td.tenderid and reportType = 'TER4')
								group by tenderId))
						)									
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and td.procurementNatureId != 3 
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					) then 
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc 
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
							where tc.tenderId = td.tenderid and td.procurementNatureId != 3 
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
							where trs.tenderId = td.tenderid and trs.reportType = 'TER2' and td.procurementNatureId != 3 
							and roundId =(select MIN(roundId) from tbl_TORRptSign 
							where tenderId = td.tenderid and reportType = 'TER2')
						) then 
						(	case  when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))>0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 and tersta.envelopeId =0
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))										
									when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = 0)>0 and (select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))=0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 and tersta.envelopeId =0 
									and roundId = 0)	
							end										
						)	end
					) 			
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and td.procurementNatureId = 3 
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					) then 
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc 
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
							where tc.tenderId = td.tenderid and td.procurementNatureId = 3 
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
							where trs.tenderId = td.tenderid and trs.reportType = 'TER1' 
							and roundId =(select MIN(roundId) from tbl_TORRptSign 
							where tenderId = td.tenderid and reportType = 'TER1')
						) then 
						(	case  when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER1'))>0
									then
									(	SELECT ISNULL((select MIN(processDate) 
										from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
										on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	
										and wfl.wfRoleId = 1 where  [action] = 'Forward' and wfl.activityId = 13 
										and wfh.objectId = td.tenderid) , 							
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3 and tersta.envelopeId =0
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER1'))))	
							end											
						)	end
					)										
					end
	),(select MIN(rptApproveDt) from tbl_EvalRptSentToAA era
where era.tenderId=td.tenderId and era.envelopeId = 0
group by tenderId )))
),'NA')
else 
ISNULL( convert(varchar(20),DATEDIFF(d,
	(case	when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and tc.committeeType in ('TEC','PEC'))=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					)
					then 
						(	SELECT ISNULL((select MIN(processDate) 
							from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
							on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	and wfl.wfRoleId = 1							
							where  [action] = 'Forward' and wfl.activityId = 13 and wfh.objectId = td.tenderid) , 
							(	select MIN(sentDate) from tbl_EvalRptSentToAA era where era.tenderId=td.tenderid  and era.envelopeId=0
								and roundId = (select MIN(roundId) from tbl_TORRptSign 
								where tenderId = td.tenderid and reportType = 'TER4')
								group by tenderId))
						)									
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and td.procurementNatureId != 3 
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					) then 
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc 
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
							where tc.tenderId = td.tenderid and td.procurementNatureId != 3 
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
							where trs.tenderId = td.tenderid and trs.reportType = 'TER2' and td.procurementNatureId != 3 
							and roundId =(select MIN(roundId) from tbl_TORRptSign 
							where tenderId = td.tenderid and reportType = 'TER2')
						) then 
						(	case  when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))>0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 and tersta.envelopeId =0
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))										
									when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = 0)>0 and (select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))=0
									then
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3 and tersta.envelopeId =0 
									and roundId = 0)	
							end												
						)	end
					) 			
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and td.procurementNatureId = 3 
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MIN(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					) then 
					(	case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc 
							inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
							where tc.tenderId = td.tenderid and td.procurementNatureId = 3 
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
							where trs.tenderId = td.tenderid and trs.reportType = 'TER1' 
							and roundId =(select MIN(roundId) from tbl_TORRptSign 
							where tenderId = td.tenderid and reportType = 'TER1')
						) then 
						(	case  when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3 
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER1'))>0
									then
									(	SELECT ISNULL((select MIN(processDate) 
										from tbl_WorkFlowFileHistory wfh	inner join tbl_WorkFlowLevelConfig wfl
										on wfh.objectId = wfl.objectId	and wfh.activityId = wfl.activityId	
										and wfl.wfRoleId = 1 where  [action] = 'Forward' and wfl.activityId = 13 
										and wfh.objectId = td.tenderid) , 							
									(select MIN(distinct sentDate) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId = 3 and tersta.envelopeId =0
									and roundId = (select MIN(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER1'))))	
							end											
						)	end
					)										
					end
	),(select MIN(rptApproveDt) from tbl_EvalRptSentToAA era
where era.tenderId=td.tenderId and era.envelopeId = 0
group by tenderId ))),'NA') 
end
)
else 'NA'
end )
else 'NA'
end as DerivedDaysBtwnTenEvalTenApp, --Modified By Dohatec
case
       WHEN
		(SELECT COUNT(tenderId) FROM tbl_EvalRptSentToAA ea WHERE ea.tenderId = td.tenderId) = 0
	THEN
		'0'
	ELSE
		CASE WHEN
			(SELECT COUNT(*) FROM tbl_WorkFlowFileHistory wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13) = 0
		THEN
			'1'
		ELSE
			(CASE WHEN
				(select COUNT(*) from (select ApprovingAuthority from tbl_FinancialDelegation fd
				where (select distinct amount from tbl_BidderRank
				where userId = (select userId from tbl_EvalRoundMaster
				where tenderId = td.tenderId and reportType = 'L1'
				and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
				where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId
				and roundId = (select MAX(roundId) from tbl_EvalRoundMaster
				where tenderId = td.tenderId and reportType = 'L1'))
				between fd.MinValueBDT and fd.MaxValueBDT and fd.BudgetType = td.budgetType and fd.ProcurementMethod = td.procurementMethod
				and fd.ProcurementNature = fd.ProcurementNature and fd.ProcurementType = td.procurementType	and fd.TenderType = td.eventType
				and fd.TenderEmergency = td.pkgUrgency
				and ApprovingAuthority in (SELECT procurementRole FROM tbl_ProcurementRole
				WHERE procurementRoleId in (select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId))) T) > 0
			THEN
				'1'
			ELSE
				'0'
			END)
		END 	
end IsApprovedByProperAA, --done /* Modified by Dohatec */
case	when (select COUNT(distinct wfFileOnHandId)  from tbl_WorkFlowFileHistory where activityId = 13 
			and wfFileOnHandId =(select MAX(wfFileOnHandId) from tbl_WorkFlowFileHistory 
			where activityId = 13 and objectId = td.tenderId)) = 0
			then case when ((select count(tenderId) from tbl_EvalRptSentToAA ea where ea.tenderId=td.tenderId and rptApproveDt is not  null) > 0) 
			then	'1'	else	'0' end
		 when (select COUNT(distinct wfFileOnHandId)  from tbl_WorkFlowFileHistory where activityId = 13 
			and wfFileOnHandId =(select MAX(wfFileOnHandId) from tbl_WorkFlowFileHistory 
			where activityId = 13 and objectId = td.tenderId)) > 0 
			then 
				case	when (select count(distinct wfRoleid) from tbl_WorkFlowFileHistory wfl 
						inner join tbl_EvalRptSentToAA ersa on wfl.objectId = ersa.tenderId and activityId= 13 and rptStatus !='Pending' and rptApproveDt is not  null
						where   roundId = ( select MAX(roundId) from tbl_EvalRptSentToAA where rptStatus != 'Pending' and rptApproveDt is not  null and tenderId = td.tenderId)
						and tenderId = td.tenderId	GROUP BY objectId)<3 then '1'							
						when (select count(distinct wfRoleid) from tbl_WorkFlowFileHistory wfl 
						inner join tbl_EvalRptSentToAA ersa on wfl.objectId = ersa.tenderId and activityId= 13 and rptStatus !='Pending' and rptApproveDt is not  null
						where   roundId = ( select MAX(roundId) from tbl_EvalRptSentToAA where rptStatus != 'Pending' and rptApproveDt is not  null and tenderId = td.tenderId)
						and tenderId = td.tenderId	GROUP BY objectId)=3 then
							case	when (select COUNT(worfklowId) from tbl_WorkFlowLevelConfig where wfRoleId = 2 and activityId = 13 and procurementRoleId in (3,7) and objectId = td.tenderId) >0 then '1' 
									else
									case when (select COUNT(distinct procurementRoleId) from tbl_WorkFlowLevelConfig where wfRoleId = 3 and activityId = 13 and procurementRoleId not in (7)  and objectId = td.tenderId) > 0  then '0' else '1' end 
							end
				else		
					'0'		
				end 
else	'0' end SubofEvalReportAA, --Modified By Dohatec
case	when 
			(select COUNT(distinct wfFileOnHandId)  from tbl_WorkFlowFileHistory where activityId = 13 
			and wfFileOnHandId =(select MAX(wfFileOnHandId) from tbl_WorkFlowFileHistory 
			where activityId = 13 and objectId = td.tenderId)) > 0 
							then 
							case  when	(select count(distinct wfRoleid) from tbl_WorkFlowFileHistory wfl 
										inner join tbl_EvalRptSentToAA ersa on wfl.objectId = ersa.tenderId 
										and activityId= 13 and rptStatus !='Pending'
										where   roundId = ( select MAX(roundId) from tbl_EvalRptSentToAA where rptStatus != 'Pending' and tenderId = td.tenderId)
										and tenderId = td.tenderId	GROUP BY objectId)=3
										then 
											case when (select COUNT(worfklowId) from tbl_WorkFlowLevelConfig where wfRoleId = 2 and activityId = 13 and procurementRoleId in (3,7) and objectId = td.tenderId) >0 then '0' 
											else case when  (select COUNT(distinct procurementRoleId) from tbl_WorkFlowLevelConfig where wfRoleId = 3 and activityId = 13 and procurementRoleId not in (7)  and objectId = td.tenderId) > 0 
															then '1' 
															else '0' 
														end 
											end 	
										else	'0'	end 
else	'0' end as TERReviewByOther,  --done /* Modified By Dohatec*/
case
	WHEN
		(SELECT COUNT(tenderId) FROM tbl_EvalRptSentToAA ea WHERE ea.tenderId = td.tenderId) = 0
	THEN
		'0'
	ELSE
		CASE WHEN
			(SELECT COUNT(*) FROM tbl_WorkFlowFileHistory wfh WHERE wfh.objectId = td.tenderId AND wfh.activityId = 13) = 0
		THEN
			'0'
		ELSE			
			(CASE 
				WHEN 
					--	Actual AA Rank = 
					(select RankId from tbl_DelegationDesignationRank 
					where ApprovingAuthority = (SELECT procurementRole FROM tbl_ProcurementRole
					WHERE procurementRoleId in (select procurementRoleId from tbl_WorkFlowLevelConfig 
					where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId)))
					
					>		
			
					-- Desire AA Rank = 
					(select MAX(RankId) from tbl_DelegationDesignationRank 
					where ApprovingAuthority in (select fd.ApprovingAuthority from tbl_FinancialDelegation fd
					where (select distinct(amount) from tbl_BidderRank 
					where userId = (select userId from tbl_EvalRoundMaster 
					where tenderId = td.tenderId and reportType = 'L1' 
					and roundId = (select MAX(roundId) from tbl_EvalRoundMaster 
					where tenderId = td.tenderId and reportType = 'L1')) and tenderId = td.tenderId and roundId = (select MAX(roundId) from tbl_EvalRoundMaster where tenderId = td.tenderId and reportType = 'L1'))  
					between fd.MinValueBDT and fd.MaxValueBDT and fd.BudgetType = td.budgetType and fd.ProcurementMethod = td.procurementMethod
					and fd.ProcurementNature = fd.ProcurementNature and fd.ProcurementType = td.procurementType	and fd.TenderType = td.eventType
					and fd.TenderEmergency = td.pkgUrgency))
									
				THEN '1'
				ELSE '0'
				END)
		END
end ContractAppByHighAuth

from tbl_TenderDetails td,tbl_DepartmentMaster dm 

where tenderStatus not in('Pending')
and td.departmentId=dm.departmentId
and submissionDt < GETDATE())T
WHERE tenderId = T.DerivedTenderID
and EvaluationTime !='NA'
and DaysBtwnContappNOA ='NA'
;


-- Indicator 22
WITH CTE2 AS (select tp.tenderId,
case when DaysBtwnTenEvalTenApp = 'NA'   Then '0'
	else (case	when (select count(worfklowId) from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = tp.tenderId)>0
				then ( ----------------- Contract Approval Workflow is Created
					case	when (SELECT procurementRole FROM tbl_ProcurementRole WHERE procurementRoleId in ( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = tp.tenderId))='CCGP'
							then (case when (select ISNULL(MIN(rptApproveDt),(select MIN(rptApproveDt) from tbl_EvalRptSentToAA where tenderId = tp.tenderId and envelopeId = 0 )) from tbl_EvalRptSentToAA where tenderId = tp.tenderId and envelopeId = 1)<=convert(date,dbo.f_gettendervaliditydate(tp.tenderId))
										then '1'	else '0'	end	)  ----------------- APPROVING AUTHORITY IS CCGP
							else ( ----------------- APPROVING AUTHORITY IS NOT CCGP
									case	when (select COUNT(committeeId) from tbl_Committee where committeeType = 'TSC' and committeStatus ='approved' and tenderId = tp.tenderid) >0 
											then ( 	case when (select ISNULL(NoOfDaysForApproval,0) from tbl_ProcurementApprovalTimeline pat inner join 
															( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in 
															( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = tp.tenderId))T
															on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 0)>=DaysBtwnTenEvalTenApp 
													then 	'1' else '0' -- 'TSC REQUIRED'
												end)											 
											else  (	case when (select ISNULL( NoOfDaysForApproval,0) from tbl_ProcurementApprovalTimeline pat inner join 
															( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in 
															( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = tp.tenderId))T
															on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC = 0) >=DaysBtwnTenEvalTenApp
													then '1'	else '0' -- 'TSC NOT REQUIRED'
												end)	
										end ) ----------------- APPROVING AUTHORITY IS NOT CCGP						
				end )----------------- Contract Approval Workflow is Created	
				else (----------------- Contract Approval Workflow is Not Created
					case	when (SELECT procurementRole FROM tbl_ProcurementRole WHERE procurementRoleId in ( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in ( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = tp.tenderid)))='CCGP'
							then (case when (select ISNULL(MIN(rptApproveDt),(select MIN(rptApproveDt) from tbl_EvalRptSentToAA where tenderId = tp.tenderId and envelopeId = 0 )) from tbl_EvalRptSentToAA where tenderId = tp.tenderId and envelopeId = 1)<=convert(date,dbo.f_gettendervaliditydate(tp.tenderId))
								then '1'	else '0'	end	) ----------------- APPROVING AUTHORITY IS CCGP
							else (	 ----------------- APPROVING AUTHORITY IS NOT CCGP						
									case	when (select COUNT(committeeId) from tbl_Committee where committeeType = 'TSC' and committeStatus ='approved' and tenderId = tp.tenderid) >0
											then ( 	case when	( SELECT ISNULL(NoOfDaysForApproval,0) from tbl_ProcurementApprovalTimeline pat inner join 
																( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in ( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in 
																( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = tp.tenderid)))T	on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 0) >=DaysBtwnTenEvalTenApp
														then '1'	else '0' -- 'TSC  REQUIRED'
											end)
									
									else 
									(  case when	(select ISNULL(NoOfDaysForApproval,0) from tbl_ProcurementApprovalTimeline pat inner join 
													( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in 
													( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in 
													( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = tp.tenderid)))T
														on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC = 0)>=DaysBtwnTenEvalTenApp
													then '1'	else '0' --'TSC NOT REQUIRED'
									end	)
							end) ----------------- APPROVING AUTHORITY IS NOT CCGP									
					end )----------------- Contract Approval Workflow is Not Created
	end )
end AS ApprovalByAAInTime
from Tbl_PromisIndicator tp inner join tbl_TenderDetails td on tp.tenderId=td.tenderId)

UPDATE Tbl_PromisIndicator SET AppofContractByAAInTimr = ApprovalByAAInTime FROM CTE2 WHERE CTE2.tenderId = Tbl_PromisIndicator.tenderId;



END


