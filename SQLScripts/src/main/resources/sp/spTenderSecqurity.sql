USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[spTenderSecqurity]    Script Date: 4/24/2016 11:25:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spTenderSecqurity]
	-- Add the parameters for the stored procedure here
	@TenderID varchar(50),
	@CreatedBy bigint,
	@TendererID bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @DocFeeAmount numeric(18,4)
	Declare @PkgLotNO varchar(50)
	Declare @tenderPaymentId bigint
	Declare @pkgLotId bigint
	
	
	
	Select @DocFeeAmount=tenderSecurityAmt,@pkgLotId=appPkgLotId from tbl_TenderLotSecurity Where tenderId=@TenderID
	Set @PkgLotNO=''
    -- Insert statements for procedure here
	INSERT INTO [dbo].[tbl_TenderPayment]
           ([paymentFor]
           ,[paymentInstType]
           ,[instRefNumber]
           ,[amount]
           ,[instDate]
           ,[instValidUpto]
           ,[bankName]
           ,[branchName]
           ,[comments]
           ,[tenderId]
           ,[userId]
           ,[createdBy]
           ,[createdDate]
           ,[pkgLotId]
           ,[eSignature]
           ,[status]
           ,[paymentMode]
           ,[extValidityRef]
           ,[currency]
           ,[issuanceBank]
           ,[issuanceBranch]
           ,[isVerified]
           ,[isLive]
           ,[dtOfAction]
           ,[partTransId]
           ,[OnlineTransId])
     VALUES
           ('Tender Security'
           ,'Pay Order'
           ,'4435'
           ,@DocFeeAmount
           ,GETDATE()
           ,GETDATE()
           ,'Bank of Bangla (BOB)'
           ,'Dhaka (BOB)'
           ,'Payment'
           ,@TenderID
           ,@TendererID
           ,@CreatedBy
           ,GETDATE()
           ,@pkgLotId
           ,''
           ,'paid'
           ,'bank'
           ,0
           ,'BDT'
           ,'Brac Bank'
           ,'Wari'
           ,'yes'
           ,'Yes'
           ,GETDATE()
           ,552
           ,null)
    set @tenderPaymentId=Ident_Current('dbo.[tbl_TenderPayment]') 
           
    INSERT INTO [dbo].[tbl_TenderPaymentStatus]
           ([tenderPaymentId]
           ,[paymentStatus]
           ,[paymentStatusDt]
           ,[createdBy]
           ,[comments]
           ,[tenderPayRefId]
           ,[refPartTransId])
     VALUES
           (@tenderPaymentId
           ,'paid'
           ,GETDATE()
           ,@CreatedBy
           ,'Document Fees'
           ,0
           ,552)       
END

