USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[SPGetUpdateTenderInfoForICTGoods]    Script Date: 4/24/2016 11:24:21 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- =============================================
-- Author:		<Author,,Name>
-- alter date: <alter Date,,>
-- Description:	<Description,,>
-- =============================================
-- SP Name	   :   SPGetUpdateTenderInfoForICTGoods
-- Module	   :   Opening (TOC chairperson/others who can verify Mega Mega Hash)
-- Description :   After the Tender has opened and Currency Exchange rate added by TOC Chairperson, when one of the TOC member
--				   verify Mega mega hash, at the same time Total Amount of different priceBid form will be updated with Currency exchange rate.
 
-- exec [dbo].[spUpdateFormAmountWithCurrencyExchangeRate]  3284,2270

ALTER PROCEDURE [dbo].[SPGetUpdateTenderInfoForICTGoods] 

@v_actionName_inVC varchar(500),
@v_tenderId_inInt int

AS

Declare @tenderFormId as integer
If @v_actionName_inVC = 'getTenderFormsId'
	begin
		 select distinct tbl_TenderBidForm.tenderFormId
		 from tbl_TenderBidForm 
		 inner join tbl_TenderForms 
		 on tbl_TenderForms.tenderFormId = tbl_TenderBidForm.tenderFormId
		 where tbl_TenderBidForm.tenderId = @v_tenderId_inInt and tbl_TenderForms.isPriceBid = 'yes';	
		 
		--Select  bidTableId, tenderFormId,tenderTableId from tbl_TenderBidPlainData where tenderFormId = @tenderFormId
			
	end
	
	


