USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_search_myapp]    Script Date: 4/24/2016 11:20:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Search In AppData
--
--
-- Author: Rajesh Singh; Kinjal Shah
-- Date: 3-11-2010

-- SP Name: [p_search_myapp]
-- Module: APP
-- Function: Procedure use for get APP List for 'MYAPP', SP return list of APP details of logg in user
--           on the basis of given search criteria.
--------------------------------------------------------------------------------

-- exec [p_search_myapp] null, 1, 'test1', 'pending',1,10,31
ALTER PROCEDURE [dbo].[p_search_myapp]

	 @v_financialYear_inVc Varchar(40)=NULL,
	 @v_budgetType_tint varchar(20) =NULL,
	 @v_projectName_inVc Varchar(150)=NULL,
	 @v_status_inVc Varchar(150)=NULL,
	 @v_Page_inN Int =1,
	 @v_RecordPerPage_inN INT =10,
	 @v_UserId_inN INT =0,
	 @v_AppId_inN INT=0,
	 @v_AppCode_inN varchar(150)='',
	 @v_CreatedBy_inN INT=0,
	 @v_SortingColumn_InVc varchar(200)='',
	 @v_SortingType_InVc varchar(20)=''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @v_TempQuery_Vc Varchar(max),@v_IntialQuery_Vc Varchar(1000)

	--This condition is used to make search with status parameter
	IF @v_status_inVc IS NULL OR @v_status_inVc =''
		BEGIN
			SET @v_TempQuery_Vc =  ''
		END
	ELSE
		BEGIN
			SET @v_TempQuery_Vc =  ' appstatus = ''' + @v_status_inVc +''''
		END

	--This condition is use to make search with financial Year parameter
	IF @v_financialYear_inVc IS NULL OR @v_financialYear_inVc = ''
		BEGIN
			SET @v_TempQuery_Vc = @v_TempQuery_Vc + ''
		END
	ELSE
		BEGIN
			IF @v_TempQuery_Vc =  ''
				BEGIN
					SET @v_TempQuery_Vc = @v_TempQuery_Vc + ' financialYear = '''+@v_financialYear_inVc +''''
				END
			ELSE
				BEGIN
					SET @v_TempQuery_Vc = @v_TempQuery_Vc + ' and financialYear = '''+@v_financialYear_inVc +''''
				END
		END

		IF @v_AppId_inN IS NULL OR @v_AppId_inN = '' or @v_AppId_inN=0
		BEGIN
			SET @v_TempQuery_Vc = @v_TempQuery_Vc + ''
		END
	ELSE
		BEGIN
			IF @v_TempQuery_Vc =  ''
				BEGIN
					SET @v_TempQuery_Vc = @v_TempQuery_Vc + ' appId =  '+ convert (varchar(20),@v_AppId_inN)
				END
			ELSE
				BEGIN
					SET @v_TempQuery_Vc = @v_TempQuery_Vc + ' and appId = '+ convert (varchar(20),@v_AppId_inN)
				END
		END

	IF @v_AppCode_inN IS NULL OR @v_AppCode_inN = ''
		BEGIN
			SET @v_TempQuery_Vc = @v_TempQuery_Vc + ''
		END
	ELSE
		BEGIN
			IF @v_TempQuery_Vc =  ''
				BEGIN
					SET @v_TempQuery_Vc = @v_TempQuery_Vc + ' convert (varchar(200),appcode) = '''+convert (varchar(200),@v_AppCode_inN) +''''
				END
			ELSE
				BEGIN
					SET @v_TempQuery_Vc = @v_TempQuery_Vc + ' and convert(varchar(200),appcode) = '''+convert(varchar(200),@v_AppCode_inN) +''''
				END
		END

        --To find App created by this user
	IF @v_CreatedBy_inN IS NULL OR @v_CreatedBy_inN = '' or @v_CreatedBy_inN=0
		BEGIN
			SET @v_TempQuery_Vc = @v_TempQuery_Vc + ''
		END
	ELSE
		BEGIN
			IF @v_TempQuery_Vc =  ''
				BEGIN
					SET @v_TempQuery_Vc = @v_TempQuery_Vc + ' createdBy =  '+ convert (varchar(20),@v_CreatedBy_inN)
				END
			ELSE
				BEGIN
					SET @v_TempQuery_Vc = @v_TempQuery_Vc + ' and createdBy = '+ convert (varchar(20),@v_CreatedBy_inN)
				END
		END

	--This condition is use to make search with budgetType parameter
	IF @v_budgetType_tint IS NULL OR @v_budgetType_tint =''
		BEGIN
			SET @v_TempQuery_Vc = @v_TempQuery_Vc + ''
		END
	ELSE
		BEGIN
			IF @v_TempQuery_Vc =  ''
				BEGIN
					SET @v_TempQuery_Vc = @v_TempQuery_Vc + ' CONVERT(varchar(20),budgetType) = ' +  CONVERT(varchar(20),@v_budgetType_tint)
				END
			ELSE
				BEGIN
					SET @v_TempQuery_Vc = @v_TempQuery_Vc + ' and CONVERT(varchar(20),budgetType) = ' +  CONVERT(varchar(20),@v_budgetType_tint)
				END
		END

	--This condition is use to make search with projectName Parameter
	IF @v_projectName_inVc IS NULL OR @v_projectName_inVc =''
		BEGIN
			SET @v_TempQuery_Vc = @v_TempQuery_Vc + ''
		END
	ELSE
		BEGIN
			IF @v_TempQuery_Vc =  ''
				BEGIN
					SET @v_TempQuery_Vc = @v_TempQuery_Vc + ' projectName = ''' + @v_projectName_inVc +''''
				END
			ELSE
				BEGIN
					SET @v_TempQuery_Vc = @v_TempQuery_Vc + ' and projectName = ''' + @v_projectName_inVc +''''
				END
		END

		
	IF @v_UserId_inN <> 0
		BEGIN
		IF @v_TempQuery_Vc =  ''
				BEGIN

			SET @v_TempQuery_Vc =  @v_TempQuery_Vc + ' officeId in (SELECT officeid FROM tbl_EmployeeOffices WHERE employeeId in (SELECT employeeId FROM tbl_EmployeeMaster WHERE userId =  ''' + convert (varchar(20),@v_UserId_inN) +''') union all
 select officeid from tbl_ProjectOffice where  userId =''' + convert (varchar(20),@v_UserId_inN) +''' )'
			END
	ELSE
		BEGIN
			SET @v_TempQuery_Vc =  @v_TempQuery_Vc + ' and  officeId in (SELECT officeid FROM tbl_EmployeeOffices WHERE employeeId in (SELECT employeeId FROM tbl_EmployeeMaster WHERE userId =  ''' + convert (varchar(20),@v_UserId_inN) +''') union all
 select officeid from tbl_ProjectOffice where  userId =''' + convert (varchar(20),@v_UserId_inN) +''' )'
		END
	End

	IF @v_TempQuery_Vc=''
		BEGIN
			SET @v_IntialQuery_Vc ='FROM tbl_AppMaster'
		END
	ELSE
		BEGIN
			SET @v_IntialQuery_Vc= 'FROM tbl_AppMaster WHERE '
		END


	declare @v_ExecutingQuery_Vc as varchar(max)
	declare @v_OrderByQuery_Vc as varchar(max)
	--Budget Type Changed as Development to Capital and Revenue to Recurrent by Proshanto Kumar Saha
    set @v_ExecutingQuery_Vc='DECLARE @v_Reccountf Int
	DECLARE @v_TotalPagef Int
	SELECT @v_Reccountf = Count(*) From (
	SELECT * From (SELECT ROW_NUMBER() OVER (order by appId desc) As Rownumber,appId, appCode,CONVERT(varchar(20),case when budgetType=1 then ''Capital'' when budgetType=2 then ''Recurrent'' else ''Own Fund'' End) budgetType, projectName
	'+@v_IntialQuery_Vc+''+@v_TempQuery_Vc+') AS DATA) AS TTT
	SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_RecordPerPage_inN as varchar(10))+')
	SELECT *,@v_TotalPagef as TotalPages,@v_Reccountf as TotalRecords From (SELECT ROW_NUMBER() OVER (order by appId desc) As Rownumber,appId, appCode, CONVERT(varchar(20),case when budgetType=1 then ''Capital'' when budgetType=2 then ''Recurrent'' else ''Own Fund'' End) budgetType, projectName
	'+@v_IntialQuery_Vc+''+@v_TempQuery_Vc+') AS DATA where Rownumber between '+cast(((@v_Page_inN - 1) * @v_RecordPerPage_inN + 1) as varchar(10))+'
	AND '+cast((@v_Page_inN * @v_RecordPerPage_inN) as varchar(10))+''

	if @v_SortingColumn_InVc <> ''
	set @v_ExecutingQuery_Vc = @v_ExecutingQuery_Vc + ' order by '+@v_SortingColumn_InVc + ' ' + @v_SortingType_InVc

	print @v_ExecutingQuery_Vc

	Exec(@v_ExecutingQuery_Vc)
END

