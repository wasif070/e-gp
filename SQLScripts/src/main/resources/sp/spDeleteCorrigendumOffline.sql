USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[spDeleteCorrigendumOffline]    Script Date: 4/24/2016 11:22:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spDeleteCorrigendumOffline]
--@corrigendumID int
@tenderOLID int
AS
DECLARE @flag_bit bit
	Begin
		DELETE FROM tbl_CorrigendumDetailOffline
		WHERE tenderOfflineId = @tenderOLID and CorrigendumStatus = 'Pending'
		Set @flag_bit=1
		Select @flag_bit as flag, 'Corrigendum has been deleted.' as Message
	End

