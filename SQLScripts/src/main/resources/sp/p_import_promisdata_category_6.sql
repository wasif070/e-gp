USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_import_promisdata_category_6]    Script Date: 4/24/2016 11:12:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- Modified By Dohatec on 10th August 2015 To Segregate Promis data import basis of Indicator Category 
-- =============================================
ALTER PROCEDURE [dbo].[p_import_promisdata_category_6]
AS
BEGIN

UPDATE [Tbl_PromisIndicator]
SET [DaysBtwnContappNOA] = T.DaysBtwnContappNOA
           ,[DaysBtwnTenOpenNOA] = T.DaysBtwnTenOpenNOA
           ,[DaysBtwnIFTNOA] = T.DaysBtwnIFTNOA
           ,[AwardPubCPTUWebsite] = T.DerivedAwardPubCPTUWebsite
           ,[AwardedInTenValidityPeriod] = T.AwardedInTenValidityPeriod
           ,[Is_Progress] = T.Is_Progress
FROM (              
select tenderId DerivedTenderID,

ISNULL( convert(varchar(20),DATEDIFF(d,(
select MIN(rptApproveDt) from tbl_EvalRptSentToAA era
where era.tenderId=td.tenderId group by tenderId),
 (select MIN(createdDt) from tbl_NoaIssueDetails noi
where noi.tenderId=td.tenderId group by tenderId  ))),'NA') as DaysBtwnContappNOA,	--done /* Modified By Dohatec*/
ISNULL( convert(varchar(20),DATEDIFF(d,(
select max(openingDt) from tbl_TenderOpenDates tod where tod.tenderId=td.tenderId group by tenderId),
 (select MIN(createdDt) from tbl_NoaIssueDetails noi
where noi.tenderId=td.tenderId group by tenderId  ))),'NA') as DaysBtwnTenOpenNOA,	--done /* Modified By Dohatec*/
ISNULL( convert(varchar(20),DATEDIFF(d,(
td.tenderPubDt),
 (select MIN(createdDt) from tbl_NoaIssueDetails noi
where noi.tenderId=td.tenderId group by tenderId  ))),'NA') as DaysBtwnIFTNOA, --done /* Modified By Dohatec*/
case 
	when 
	eventType in('REOI','PQ','1 stage-TSTM')
	then
	'100'
	else
	case
		when
			(selecT COUNT(cs.contractSignId) from tbl_ContractSign cs, tbl_NoaIssueDetails nd, tbl_NoaAcceptance na
			where nd.tenderId = td.tenderId and na.acceptRejStatus = 'approved' 
			and nd.noaIssueId = na.noaIssueId and nd.noaIssueId = cs.noaId) = 0
		then
		'100'
		else
		case
			when
			(selecT COUNT(cs.contractSignId) from tbl_ContractSign cs, tbl_NoaIssueDetails nd, tbl_NoaAcceptance na
			where cs.isPubAggOnWeb = 'yes' and nd.tenderId = td.tenderId and na.acceptRejStatus = 'approved' 
			and nd.noaIssueId = na.noaIssueId and nd.noaIssueId = cs.noaId) > 0
			then
			'1'
			else
			'0'
		end 	
	end
end DerivedAwardPubCPTUWebsite,		--Added on 24th Jan 2012 yagnesh  /* Modified By Dohatec*/
case 
	when 
	eventType in('REOI','PQ','1 stage-TSTM')
	then
	'100'
	else
	case
		when
			(selecT COUNT(cs.contractSignId) from tbl_ContractSign cs, tbl_NoaIssueDetails nd, tbl_NoaAcceptance na
			where nd.tenderId = td.tenderId and na.acceptRejStatus = 'approved' 
			and nd.noaIssueId = na.noaIssueId and nd.noaIssueId = cs.noaId) = 0
		then
		'100'
		else
		case
			when
			(select count(tenderId) from tbl_TenderValidityExtDate tve
			where tve.tenderId = td.tenderId and tve.extStatus = 'Approved') > 0
			then
			'0'
			else
			'1'
		end 	
	end
end AwardedInTenValidityPeriod,  -- done  /* Modified By Dohatec*/
case when eventType in('REOI','PQ','1 stage-TSTM')
	then 
		case 
			when 
				((select count(evalRptId) from tbl_EvalRptSentToAA ea 
				where rptStatus not in('Pending','Seek Clarification') and ea.tenderid=td.tenderId)>=1)
			then
				'Y'
			else
				'N'
		end
	else
		case 
			when 
				((select count(contractSignId) 
				from tbl_CMS_WcCertificate wc, tbl_ContractSign c, tbl_NoaIssueDetails na 
				where c.contractSignId = wc.contractId and na.noaIssueId=c.noaId 
				and na.tenderId=td.tenderId and wc.isWorkComplete = 'yes')>=1)
			then
				'N'
			else
				'Y'
		end
		
end Is_Progress	-- done on 9th Feb 2012

from tbl_TenderDetails td,tbl_DepartmentMaster dm 

where tenderStatus not in('Pending')
and td.departmentId=dm.departmentId
and submissionDt < GETDATE())T
WHERE tenderId = T.DerivedTenderID
and DaysBtwnTenEvalTenApp != 'NA'
and AwardPubCPTUWebsite =100

END
