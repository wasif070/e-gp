USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_ins_upd_apppkglot]    Script Date: 4/24/2016 11:15:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Insert/Update/Delete table tbl_AppPkgLots
--
--
-- Author: Karan
-- Date: 30-10-2010
--

-- SP Name: [p_ins_upd_apppkglot]
-- Module: APP Package
-- Function: Store Procedure is use for perform insert/update/delete operation for APP Package Lot Details.

--------------------------------------------------------------------------------
-- Create:	Use for perform Insert operation for tbl_AppPkgLots.
-- Update:  Use for perform Update operation for tbl_AppPkgLots.
-- Delete:  Use for perform Delete operation for tbl_AppPkgLots.
--------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_ins_upd_apppkglot]
	
@v_packageid_inInt int,
@v_appId_inInt int,
@v_lotNo_inInt int,
@v_lotDesc_inVc varchar(2000),
@v_quantity_inInt int,
@v_unit_inVc varchar(50),
@v_Action_inVc varchar(50)=null,
@v_AppPkgLotId_inInt int=null
	
AS

BEGIN
DECLARE @v_flag_bit bit	

IF @v_Action_inVc='Create'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
				/* START CODE : TO INSERT INTO TABLE - tbl_AppPkgLots */	
				INSERT INTO [dbo].[tbl_AppPkgLots]
					   ([packageid]
					   ,[appId]
					   ,[lotNo]
					   ,[lotDesc]
					   ,[quantity]
					   ,[unit])
				 VALUES
					   (@v_packageid_inInt,
						@v_appId_inInt,
						@v_lotNo_inInt,
						@v_lotDesc_inVc,
						@v_quantity_inInt,
						@v_unit_inVc)
				/* END CODE : TO INSERT INTO TABLE - tbl_AppPkgLots */	
					
				Set @v_flag_bit=1			
				Select @v_flag_bit as flag, 'APP Package Lot created.' as Message
			COMMIT TRAN	
		END TRY 
		BEGIN CATCH
				BEGIN
					Set @v_flag_bit=0
					Select @v_flag_bit as flag, 'Error while creating APP Package Lot.' as Message
					ROLLBACK TRAN
				END
		END CATCH	 
	END

ELSE IF @v_Action_inVc='Update'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
					UPDATE [dbo].[tbl_AppPkgLots]
					   SET [packageid] = @v_packageid_inInt,
						  [appId] = @v_appId_inInt,
						  [lotNo] = @v_lotNo_inInt,
						  [lotDesc] = @v_lotDesc_inVc,
						  [quantity] = @v_quantity_inInt,
						  [unit] = @v_unit_inVc
					 WHERE appPkgLotId= @v_AppPkgLotId_inInt				 
					 /* END CODE : TO UPDATE TABLE - tbl_AppPkgLots */
					 
					 Set @v_flag_bit=1
					 Select @v_flag_bit as flag, 'APP Package Lot updated.' as Message
					 COMMIT TRAN	
			END TRY 
			BEGIN CATCH
					BEGIN
						Set @v_flag_bit=0
						Select @v_flag_bit as flag, 'Error while updating APP Package Lot.' as Message
						ROLLBACK TRAN
					END
			END CATCH	 
	END

ELSE IF @v_Action_inVc='Delete'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
					/* START CODE : TO DELETE FROM TABLE - tbl_AppPkgLots */
					DELETE FROM [dbo].[tbl_AppPkgLots]      
					 WHERE appPkgLotId= @v_AppPkgLotId_inInt				 
					 /* END CODE : TO DELETE FROM TABLE - tbl_AppPkgLots */
					 
					 Set @v_flag_bit=1
					 Select @v_flag_bit as flag, 'APP Package Lot deleted.' as Message
					 COMMIT TRAN	
			END TRY 
			BEGIN CATCH
					BEGIN
						Set @v_flag_bit=0
						Select @v_flag_bit as flag, 'Error while deleting APP Package Lot.' as Message
						ROLLBACK TRAN
					END
			END CATCH	 
	END	

END

