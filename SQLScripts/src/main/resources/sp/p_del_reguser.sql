USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_del_reguser]    Script Date: 4/24/2016 10:44:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Inserts/Updates tables related to tenders.
--
--
-- Author: Karan
-- Date: 13-12-2010
--
-- Last Modified:
-- Modified By:
-- Date:
-- Modification:
--------------------------------------------------------------------------------

-- SP Name	:   p_del_reguser
-- Module	:	New User Registration (For DB JOB Delete user who had not done payment with 7 days or mail is not verified in with in 3 days)
-- Funftion	:	N.A.


ALTER PROCEDURE [dbo].[p_del_reguser]
AS

BEGIN

SET NOCOUNT ON;

DECLARE @v_flag_bit bit

	BEGIN TRY
		BEGIN TRAN
			DECLARE	@v_totalDeleteRecord INT,
                    @v_delMailID varchar(MAX)

            SET @v_delMailID = ''
			SET @v_totalDeleteRecord = 0
			/* START CODE: TO DELETE DATA FROM TEMPORARY TABLES */
			DELETE FROM tbl_TempTendererEsignature WHERE tendererId in
			(SELECT  tendererid FROM tbl_TempTendererMaster WHERE userid in
				(	select	userId
					from	tbl_LoginMaster
					Where	registrationType!='media'
							AND userTyperId=2
							AND userId NOT IN(	select	userId from tbl_RegFeePayment
													where	isLive='yes'
															AND isVerified='yes'
												)
							AND (isEmailVerified='no' AND datediff(HOUR, registeredDate, GETDATE()) > 72)

				)
			)

			SET @v_totalDeleteRecord = @v_totalDeleteRecord +@@ROWCOUNT

			DELETE FROM [dbo].[tbl_TempCompanyDocuments] WHERE tendererId in
			(SELECT tendererid FROM tbl_TempTendererMaster WHERE userid in
				(	select	userId
					from	tbl_LoginMaster
					Where	registrationType!='media'
							AND userTyperId=2
							AND userId NOT IN(	select	userId from tbl_RegFeePayment
													where	isLive='yes'
															AND isVerified='yes'
												)
							AND (isEmailVerified='no' AND datediff(HOUR, registeredDate, GETDATE()) > 72)
				)
			)

			SET @v_totalDeleteRecord = @v_totalDeleteRecord +@@ROWCOUNT

			DELETE FROM dbo.tbl_TempCompanyJointVenture WHERE companyId in
			(	SELECT companyId FROM tbl_TempCompanyMaster WHERE userid in
				(	select	userId
					from	tbl_LoginMaster
					Where	registrationType!='media'
							AND userTyperId=2
							AND userId NOT IN(	select	userId from tbl_RegFeePayment
													where	isLive='yes'
															AND isVerified='yes'
												)
							AND (isEmailVerified='no' AND datediff(HOUR, registeredDate, GETDATE()) > 72)
				)
			)

			SET @v_totalDeleteRecord = @v_totalDeleteRecord +@@ROWCOUNT

			DELETE FROM tbl_tempTendererMaster WHERE userId In
			(	select	userId
				from	tbl_LoginMaster
				Where	registrationType!='media'
						AND userTyperId=2
						AND userId NOT IN(	select	userId from tbl_RegFeePayment
												where	isLive='yes'
														AND isVerified='yes'
											)
						AND (isEmailVerified='no' AND datediff(HOUR, registeredDate, GETDATE()) > 72)
			)

			SET @v_totalDeleteRecord = @v_totalDeleteRecord +@@ROWCOUNT

			DELETE FROM [dbo].[tbl_TempCompanyMaster] WHERE userId In
			(	select	userId
				from	tbl_LoginMaster
				Where	registrationType!='media'
						AND userTyperId=2
						AND userId NOT IN(	select	userId from tbl_RegFeePayment
												where	isLive='yes'
														AND isVerified='yes'
											)
						AND (isEmailVerified='no' AND datediff(HOUR, registeredDate, GETDATE()) > 72)
			)

			SET @v_totalDeleteRecord = @v_totalDeleteRecord +@@ROWCOUNT

			/* END CODE: TO DELETE DATA FROM TEMPORARY TABLES */


			/* START CODE: TO DELETE FROM LOGIN MASTER TABLE - tbl_LoginMaster*/
			DELETE FROM tbl_MessageInBox WHERE msgToUserId In
			(	select	userId
				from	tbl_LoginMaster
				Where	registrationType!='media'
						AND userTyperId=2
						AND userId NOT IN(	select	userId from tbl_RegFeePayment
												where	isLive='yes'
														AND isVerified='yes'
											)
						AND (isEmailVerified='no' AND datediff(HOUR, registeredDate, GETDATE()) > 72)
			)

			SET @v_totalDeleteRecord = @v_totalDeleteRecord +@@ROWCOUNT

			DELETE FROM tbl_MessageSent WHERE msgFromUserId In
			(	select	userId
				from	tbl_LoginMaster
				Where	registrationType!='media'
						AND userTyperId=2
                                                AND userId NOT IN(  select  userId from tbl_RegFeePayment
                                                                    where   isLive='yes'
                                                                            AND isVerified='yes'
                                                                  )
						AND (isEmailVerified='no' AND datediff(HOUR, registeredDate, GETDATE()) > 72)
			)

			SET @v_totalDeleteRecord = @v_totalDeleteRecord +@@ROWCOUNT

            SELECT	@v_delMailID = @v_delMailID + emailId +','
            FROM	tbl_LoginMaster
			WHERE	registrationType!='media'
					and userTyperId=2
					and userID not in ( select  userId 
										from tbl_RegFeePayment
                                        where   isLive='yes' 
												and isVerified='yes'
                                       )
					AND (isEmailVerified='no' AND datediff(HOUR, registeredDate, GETDATE()) > 72)
            
			DELETE	FROM	tbl_LoginMaster
			WHERE	registrationType!='media'
					and userTyperId=2
					and userID not in ( select  userId 
										from tbl_RegFeePayment
                                        where   isLive='yes' 
												and isVerified='yes'
                                       )
					AND (isEmailVerified='no' AND datediff(HOUR, registeredDate, GETDATE()) > 72)
			/* END CODE: TO DELETE FROM LOGIN MASTER TABLE - tbl_LoginMaster*/

			SET @v_totalDeleteRecord = @v_totalDeleteRecord +@@ROWCOUNT

			Set @v_flag_bit=1
			Select @v_flag_bit as flag, 'Records deleted.' as Msg
			INSERT INTO tbl_deleteUserJob VALUES (CONVERT(VARCHAR(5),@v_totalDeleteRecord) +' records deleted successfully (After 72 Hours). ['+@v_delMailID+']',GETDATE())
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		BEGIN
			Set @v_flag_bit=0
			Select @v_flag_bit as flag, 'Records not deleted. Error: ' + ERROR_MESSAGE() as Msg
						INSERT INTO tbl_deleteUserJob VALUES ('Unable to delete record due to '+ERROR_MESSAGE()+' (After 72 Hours).',GETDATE())
			ROLLBACK TRAN
		END
	END CATCH


SET NOCOUNT OFF;

END

