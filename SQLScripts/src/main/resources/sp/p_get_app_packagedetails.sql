USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_get_app_packagedetails]    Script Date: 4/24/2016 10:54:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Select information regarding app package
--
--
-- Author: Karan
-- Date: 03-11-2010
--
-- SP Name: [p_get_app_packagedetails]
-- Module: APP , Tender
-- Function: Store Procedure is use for fetch APP Package data.
--------------------------------------------------------------------------------
-- Package:		Get APP Package Details.
-- AppPackage:	Get APP Details.
-- Lot:			Get Package Lot Details.
--------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_get_app_packagedetails]

@v_AppId_inInt int,
@v_PackageId_inInt int,
@v_PackageType_inVc varchar(50)

AS

BEGIN

SET NOCOUNT ON;

	IF @v_PackageType_inVc='Package'
	BEGIN
		--SELECT  appId,appCode,financialYear,
		--convert(varchar(20),case when budgettype=1 then 'Development' when budgettype=2 then 'Budget' when budgettype=3 then 'Own Fund' end ) budgetType,
		--projectName,procurementnature,
		--servicesType,packageNo,packageDesc,
		--allocateBudget,pkgEstCost,isPQRequired,reoiRfaRequired,
		--procurementMethodId,procurementType,sourceOfFund,
		--IsNull(convert(varchar(15),advtDt, 103),'') as advtDt,
		--IsNull(convert(varchar(15),subDt, 103),'') as subDt,
		--IsNull(convert(varchar(15),tenderAdvertDt, 103),'') as tenderAdvertDt,
		--IsNull(convert(varchar(15),tenderSubDt, 103),'') as tenderSubDt,
		--IsNull(convert(varchar(15),tenderOpenDt, 103),'') as tenderOpenDt,
		--IsNull(convert(varchar(15),tenderNoaIssueDt, 103),'') as tenderNoaIssueDt,
		--IsNull(convert(varchar(15),tenderContractSignDt, 103),'') as tenderContractSignDt,
		--IsNull(convert(varchar(15),tenderContractCompDt, 103),'') as tenderContractCompDt,
		--PE,district,
		--IsNull(convert(varchar(15),techSubCmtRptDt, 103),'') as  techSubCmtRptDt,
		-- IsNull(convert(varchar(15),tenderEvalRptDt, 103),'') as tenderEvalRptDt,
		-- IsNull(convert(varchar(15),tenderEvalRptAppDt, 103),'') as tenderEvalRptAppDt,
		-- IsNull(convert(varchar(15),tenderContractAppDt, 103),'') as tenderContractAppDt,
		-- IsNull(convert(varchar(15),reoiReceiptDt, 103),'') as reoiReceiptDt,
		-- IsNull(convert(varchar(15),rfpTechEvalDt, 103),'') as rfpTechEvalDt,
		-- IsNull(convert(varchar(15),rfpFinancialOpenDt, 103),'') as rfpFinancialOpenDt,
		-- IsNull(convert(varchar(15),rfpNegCompDt, 103),'') as rfpNegCompDt,
		-- IsNull(convert(varchar(15),rfpContractAppDt, 103),'') as rfpContractAppDt,
		-- IsNull(convert(varchar(15),rfaAdvertDt, 103),'') as rfaAdvertDt,
		-- IsNull(convert(varchar(15),rfaReceiptDt, 103),'') as rfaReceiptDt,
		-- IsNull(convert(varchar(15),rfaEvalDt, 103),'') as rfaEvalDt,
		-- IsNull(convert(varchar(15),rfaInterviewDt, 103),'') as rfaInterviewDt,
		-- IsNull(convert(varchar(15),rfaFinalSelDt, 103),'') as rfaFinalSelDt,
		-- IsNull(convert(varchar(15),rfaEvalRptSubDt, 103),'') as rfaEvalRptSubDt,
		-- IsNull(convert(varchar(15),rfaAppConsultantDt, 103),'') as rfaAppConsultantDt,
		-- IsNull(convert(varchar(15),actSubDt, 103),'') as actSubDt,
		-- IsNull(convert(varchar(15),actAdvtDt, 103),'') as actAdvtDt,
		-- IsNull(convert(varchar(15),actEvalRptDt, 103),'') as actEvalRptDt,
		-- IsNull(convert(varchar(15),actAppLstDt, 103),'') as actAppLstDt,
		-- IsNull(convert(varchar(15),actTenderAdvertDt, 103),'') as actTenderAdvertDt,
		-- IsNull(convert(varchar(15),actTenderSubDt, 103),'') as actTenderSubDt,
		-- IsNull(convert(varchar(15),actTenderOpenDt, 103),'') as actTenderOpenDt,
		-- IsNull(convert(varchar(15),actTechSubCmtRptDt, 103),'') as actTechSubCmtRptDt,
		-- IsNull(convert(varchar(15),actTenderEvalRptDt, 103),'') as actTenderEvalRptDt,
		-- IsNull(convert(varchar(15),acttenderEvalRptAppDt, 103),'') as acttenderEvalRptAppDt,
		-- IsNull(convert(varchar(15),actTenderContractAppDt, 103),'') as actTenderContractAppDt,
		-- IsNull(convert(varchar(15),actTenderNoaIssueDt, 103),'') as actTenderNoaIssueDt,
		-- IsNull(convert(varchar(15),actTenderContractSignDt, 103),'') as actTenderContractSignDt,
		-- IsNull(convert(varchar(15),actTenderContractCompDt, 103),'') as actTenderContractCompDt,
		-- IsNull(convert(varchar(15),actReoiReceiptDt, 103),'') as actReoiReceiptDt,
		-- IsNull(convert(varchar(15),actRfpTechEvalDt, 103),'') as actRfpTechEvalDt,
		-- IsNull(convert(varchar(15),actRfpFinancialOpenDt, 103),'') as actRfpFinancialOpenDt,
		-- IsNull(convert(varchar(15),actRfpNegComDt, 103),'') as actRfpNegComDt,
		-- IsNull(convert(varchar(15),actRfpContractAppDt, 103),'') as actRfpContractAppDt,
		-- IsNull(convert(varchar(15),actRfaAdvertDt, 103),'') as actRfaAdvertDt,
		-- IsNull(convert(varchar(15),actRfaReceiptDt, 103),'') as actRfaReceiptDt,
		-- IsNull(convert(varchar(15),actRfaEvalDt, 103),'') as actRfaEvalDt,
		-- IsNull(convert(varchar(15),actRfaInterviewDt, 103),'') as actRfaInterviewDt,
		-- IsNull(convert(varchar(15),actRfaFinalSelDt, 103),'') as actRfaFinalSelDt,
		-- IsNull(convert(varchar(15),actRfaEvalRptSubDt, 103),'') as actRfaEvalRptSubDt,
		-- IsNull(convert(varchar(15),actRfaAppConsultantDt, 103),'') as actRfaAppConsultantDt,
		-- IsNull(convert(varchar(15),evalRptDt, 103),'') as evalRptDt,
		-- packageId,
		-- IsNull(convert(varchar(15),appLstDt, 103),'') as appLstDt,
		-- subDays, advtDays,openDays,
		-- IsNull(convert(varchar(15),openDt, 103),'') as openDt,
  --       evalRptDays,tenderAdvertDays,tenderSubDays,
  --       tenderOpenDays,techSubCmtRptDays,tenderEvalRptdays,
  --       tenderEvalRptAppDays,tenderContractAppDays,
  --       tenderNoaIssueDays,tenderContractSignDays,rfaAdvertDays,
  --       rfaReceiptDays,rfaEvalDays,rfaInterviewDays,
  --       rfaFinalSelDays,rfaEvalRptSubDays,rfpContractAppDays,
  --       rfpNegCompDays,rfpFinancialOpenDays,rfpTechEvalDays,
  --       reoiReceiptDays,pqDtId
	SELECT  appId,appCode,financialYear,
	--Changed Budget Type Development to Capital and Revenue to Recurrent by Proshanto Kumarr Saha
		convert(varchar(20),case when budgettype=1 then 'Capital' when budgettype=2 then 'Recurrent' when budgettype=3 then 'Own Fund' end ) budgetType,
		projectName,procurementnature,
		servicesType,packageNo,packageDesc,bidderCategory,workCategory,depoplanWork,entrustingAgency,timeFrame,
		allocateBudget,pkgEstCost,isPQRequired,reoiRfaRequired,pkgUrgency,replace(cpvCode,';','; ') cpvCode,
		procurementMethodId,procurementType,replace(sourceOfFund,',',', ') sourceOfFund,
		replace(IsNull(REPLACE(CONVERT(VARCHAR(11),advtDt, 106), ' ', '-'),''),'01-Jan-1900','-') as advtDt,
		replace(IsNull(REPLACE(CONVERT(VARCHAR(11),subDt, 106), ' ', '-'),''),'01-Jan-1900','-') as subDt,
		replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderAdvertDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderAdvertDt,
		replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderSubDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderSubDt,
		replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderOpenDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderOpenDt,
		--Code by Proshanto
		replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderLetterIntentDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderLetterIntentDt,
		replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderNoaIssueDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderNoaIssueDt,
		replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderContractSignDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderContractSignDt,
		replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderContractCompDt, 106), ' ', '-'),''),'01-Jan-1900','-')  as tenderContractCompDt,
		PE,district,
		replace(IsNull(REPLACE(CONVERT(VARCHAR(11),techSubCmtRptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as  techSubCmtRptDt,
		replace( IsNull(REPLACE(CONVERT(VARCHAR(11),tenderEvalRptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderEvalRptDt,
		replace( IsNull(REPLACE(CONVERT(VARCHAR(11),tenderEvalRptAppDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderEvalRptAppDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderContractAppDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderContractAppDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),reoiReceiptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as reoiReceiptDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfpTechEvalDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfpTechEvalDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfpFinancialOpenDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfpFinancialOpenDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfpNegCompDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfpNegCompDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfpContractAppDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfpContractAppDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaAdvertDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaAdvertDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaReceiptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaReceiptDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaEvalDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaEvalDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaInterviewDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaInterviewDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaFinalSelDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaFinalSelDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaEvalRptSubDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaEvalRptSubDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaAppConsultantDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaAppConsultantDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actSubDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actSubDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actAdvtDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actAdvtDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actEvalRptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actEvalRptDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actAppLstDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actAppLstDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderAdvertDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderAdvertDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderSubDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderSubDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderOpenDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderOpenDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTechSubCmtRptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTechSubCmtRptDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderEvalRptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderEvalRptDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),acttenderEvalRptAppDt, 106), ' ', '-'),''),'01-Jan-1900','-') as acttenderEvalRptAppDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderContractAppDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderContractAppDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderNoaIssueDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderNoaIssueDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderContractSignDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderContractSignDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderContractCompDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderContractCompDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actReoiReceiptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actReoiReceiptDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfpTechEvalDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfpTechEvalDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfpFinancialOpenDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfpFinancialOpenDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfpNegComDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfpNegComDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfpContractAppDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfpContractAppDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaAdvertDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaAdvertDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaReceiptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaReceiptDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaEvalDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaEvalDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaInterviewDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaInterviewDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaFinalSelDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaFinalSelDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaEvalRptSubDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaEvalRptSubDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaAppConsultantDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaAppConsultantDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),evalRptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as evalRptDt,
		 packageId,
		 IsNull(REPLACE(CONVERT(VARCHAR(11),appLstDt, 106), ' ', '-'),'') as appLstDt,
		 subDays, advtDays,openDays,
		 IsNull(REPLACE(CONVERT(VARCHAR(11),openDt, 106), ' ', '-'),'') as openDt,
         evalRptDays,tenderAdvertDays,tenderSubDays,
         tenderOpenDays,techSubCmtRptDays,tenderEvalRptdays,
		 --added tnderLetterIntentDays by Proshanto Kumar Saha
         tenderEvalRptAppDays,tenderContractAppDays,tnderLetterIntentDays,
         tenderNoaIssueDays,tenderContractSignDays,rfaAdvertDays,
         rfaReceiptDays,rfaEvalDays,rfaInterviewDays,
         rfaFinalSelDays,rfaEvalRptSubDays,rfpContractAppDays,
         rfpNegCompDays,rfpFinancialOpenDays,rfpTechEvalDays,
         reoiReceiptDays,pqDtId,projectId,approvingAuthEmpId as aAEmployeeId
		FROM vw_app_packagedetails WHERE appId=@v_AppId_inInt And packageId=@v_PackageId_inInt
	END
        ELSE IF @v_PackageType_inVc='RevisePackage'
	BEGIN
		SELECT  appId,appCode,financialYear,
		--Changed Budget Type Development to Capital and Revenue to Recurrent by Proshanto Kumarr Saha
		convert(varchar(20),case when budgettype=1 then 'Capital' when budgettype=2 then 'Recurrent' when budgettype=3 then 'Own Fund' end ) budgetType,
		projectName,procurementnature,
		servicesType,packageNo,packageDesc,bidderCategory,workCategory,depoplanWork, entrustingAgency, timeFrame,
		allocateBudget,pkgEstCost,isPQRequired,reoiRfaRequired,pkgUrgency,replace(cpvCode,';','; ') cpvCode,
		procurementMethodId,procurementType,replace(sourceOfFund,',',', ') sourceOfFund,
		replace(IsNull(REPLACE(CONVERT(VARCHAR(11),advtDt, 106), ' ', '-'),''),'01-Jan-1900','-') as advtDt,
		replace(IsNull(REPLACE(CONVERT(VARCHAR(11),subDt, 106), ' ', '-'),''),'01-Jan-1900','-') as subDt,
		replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderAdvertDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderAdvertDt,
		replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderSubDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderSubDt,
		replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderOpenDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderOpenDt,
		--Code by Proshanto
		replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderLetterIntentDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderLetterIntentDt,
		replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderNoaIssueDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderNoaIssueDt,
		replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderContractSignDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderContractSignDt,
		replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderContractCompDt, 106), ' ', '-'),''),'01-Jan-1900','-')  as tenderContractCompDt,
		PE,district,
		replace(IsNull(REPLACE(CONVERT(VARCHAR(11),techSubCmtRptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as  techSubCmtRptDt,
		replace( IsNull(REPLACE(CONVERT(VARCHAR(11),tenderEvalRptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderEvalRptDt,
		replace( IsNull(REPLACE(CONVERT(VARCHAR(11),tenderEvalRptAppDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderEvalRptAppDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),tenderContractAppDt, 106), ' ', '-'),''),'01-Jan-1900','-') as tenderContractAppDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),reoiReceiptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as reoiReceiptDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfpTechEvalDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfpTechEvalDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfpFinancialOpenDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfpFinancialOpenDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfpNegCompDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfpNegCompDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfpContractAppDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfpContractAppDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaAdvertDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaAdvertDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaReceiptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaReceiptDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaEvalDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaEvalDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaInterviewDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaInterviewDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaFinalSelDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaFinalSelDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaEvalRptSubDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaEvalRptSubDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),rfaAppConsultantDt, 106), ' ', '-'),''),'01-Jan-1900','-') as rfaAppConsultantDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actSubDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actSubDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actAdvtDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actAdvtDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actEvalRptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actEvalRptDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actAppLstDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actAppLstDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderAdvertDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderAdvertDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderSubDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderSubDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderOpenDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderOpenDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTechSubCmtRptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTechSubCmtRptDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderEvalRptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderEvalRptDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),acttenderEvalRptAppDt, 106), ' ', '-'),''),'01-Jan-1900','-') as acttenderEvalRptAppDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderContractAppDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderContractAppDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderNoaIssueDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderNoaIssueDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderContractSignDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderContractSignDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actTenderContractCompDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actTenderContractCompDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actReoiReceiptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actReoiReceiptDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfpTechEvalDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfpTechEvalDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfpFinancialOpenDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfpFinancialOpenDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfpNegComDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfpNegComDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfpContractAppDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfpContractAppDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaAdvertDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaAdvertDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaReceiptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaReceiptDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaEvalDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaEvalDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaInterviewDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaInterviewDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaFinalSelDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaFinalSelDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaEvalRptSubDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaEvalRptSubDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),actRfaAppConsultantDt, 106), ' ', '-'),''),'01-Jan-1900','-') as actRfaAppConsultantDt,
		 replace(IsNull(REPLACE(CONVERT(VARCHAR(11),evalRptDt, 106), ' ', '-'),''),'01-Jan-1900','-') as evalRptDt,
		 packageId,
		 IsNull(REPLACE(CONVERT(VARCHAR(11),appLstDt, 106), ' ', '-'),'') as appLstDt,
		 subDays, advtDays,openDays,
		 IsNull(REPLACE(CONVERT(VARCHAR(11),openDt, 106), ' ', '-'),'') as openDt,
         evalRptDays,tenderAdvertDays,tenderSubDays,
         tenderOpenDays,techSubCmtRptDays,tenderEvalRptdays,
		 --added tnderLetterIntentDays by Proshanto Kumar Saha
         tenderEvalRptAppDays,tenderContractAppDays,tnderLetterIntentDays,
         tenderNoaIssueDays,tenderContractSignDays,rfaAdvertDays,
         rfaReceiptDays,rfaEvalDays,rfaInterviewDays,
         rfaFinalSelDays,rfaEvalRptSubDays,rfpContractAppDays,
         rfpNegCompDays,rfpFinancialOpenDays,rfpTechEvalDays,
         reoiReceiptDays,pqDtId,projectId,approvingAuthEmpId as aAEmployeeId
		FROM vw_app_temppackagedetails WHERE appId=@v_AppId_inInt And packageId=@v_PackageId_inInt
	END
	ELSE IF @v_PackageType_inVc='AppPackage'
	BEGIN
	--Changed Budget Type Development to Capital and Revenue to Recurrent by Proshanto Kumarr Saha
		SELECT  am.appId,appCode,financialYear,convert(varchar(20),case when budgettype=1 then 'Capital' when budgettype=2 then 'Recurrent' when budgettype=3 then 'Own Fund' end ) budgetType,projectName,procurementnature,servicesType,pkgUrgency,packageNo,packageDesc,bidderCategory,workCategory,depoplanWork, ap.entrustingAgency, timeFrame,allocateBudget,pkgEstCost,isPQRequired,reoiRfaRequired,procurementMethodId,procurementType,sourceOfFund,approvingAuthEmpId as aAEmployeeId,cpvCode,projectId,pkgUrgency,cpvCode
			FROM tbl_AppPackages ap,tbl_AppMaster am WHERE  am.appId=ap.appId and ap.appId=@v_AppId_inInt And packageId=@v_PackageId_inInt
	END
	ELSE IF @v_PackageType_inVc='Lot'
	BEGIN
		SELECT  appPkgLotId, l.packageId, l.appId, lotNo, lotDesc, quantity, unit, lotEstCost,p.procurementMethodId,pkgUrgency,p.cpvCode
			FROM tbl_AppPkgLots l,tbl_apppackages p
			 WHERE l.packageId=p.packageId and l.appId=@v_AppId_inInt And l.packageId=@v_PackageId_inInt
	END
        ELSE IF @v_PackageType_inVc='ReviseLot'
	BEGIN
		SELECT  appPkgLotId, l.packageId, l.appId, lotNo, lotDesc, quantity, unit, lotEstCost,p.procurementMethodId,pkgUrgency,p.cpvCode
			FROM tbl_TempAppPkgLots l,tbl_TempApppackages p
			 WHERE l.packageId=p.packageId and l.appId=@v_AppId_inInt And l.packageId=@v_PackageId_inInt
	END

SET NOCOUNT OFF;

END

