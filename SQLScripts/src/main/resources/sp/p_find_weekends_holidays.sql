USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_find_weekends_holidays]    Script Date: 4/24/2016 10:47:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: View Message Box.
--
--
-- Author: Rajesh
-- Date: 31-10-2010
--
-- Last Modified:
-- Modified By:
-- Date 31-10-2010
-- Modification:
--------------------------------------------------------------------------------
-- p_find_weekends_holidays '2010/10/3','5'
--
-- SP Name	:	p_find_weekends_holidays
-- Module	:	APP, Tender
-- Function	:	N.A.


ALTER PROCEDURE [dbo].[p_find_weekends_holidays] 
	@v_Getdated as DATETIME,
	@v_DaysN as INT=''
AS
BEGIN	
	-- FINd Working day on given dates
	SET NOCOUNT ON;
	DECLARE @v_flag_bit bit	
	
        -- Edited by nafiul to show holyday description
	if (exists(select holidayId from tbl_HolidayMaster where holidayDate=(@v_Getdated + @v_DaysN)))
		BEGIN
			Set @v_flag_bit=0
			Select @v_flag_bit as flag, DateName(WEEKDAY,(@v_Getdated + @v_DaysN)) +', ' + (select [Description] from tbl_HolidayMaster where holidayDate=(@v_Getdated + @v_DaysN)) + '!' as flgMessage,(@v_Getdated + @v_DaysN) as holidayDate
		END
	ELSE IF(Exists(select weekendid from tbl_WeekEnd where weekDayName=DateName(WEEKDAY,(@v_Getdated + @v_DaysN))))
		BEGIN
			Set @v_flag_bit=0
			Select @v_flag_bit as flag, DateName(WEEKDAY,(@v_Getdated + @v_DaysN)) +', Weekend' +  '!' as flgMessage, (@v_Getdated + @v_DaysN) as holidayDate
		END
	ELSE
		BEGIN
			Set @v_flag_bit=1
			Select @v_flag_bit as flag,'Working Day' as flgMessage,(@v_Getdated + @v_DaysN) as holidayDate
		END

    
END

