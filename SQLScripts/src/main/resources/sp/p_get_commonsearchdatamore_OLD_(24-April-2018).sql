USE [egpgovbt_15112016]
GO
/****** Object:  StoredProcedure [dbo].[p_get_commonsearchdatamore]    Script Date: 12/1/2016 3:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kinjal Shah
-- alter date: 15-Dec-2010
-- Description:	Proc created for Search Bid data
-- Last Modified
-- Modified BY: Kinjal, Krish,  Rajesh
-- =============================================

ALTER PROCEDURE [dbo].[p_get_commonsearchdatamore]
	-- Add the parameters for the stored procedure here
	 @v_fieldName1Vc varchar(500)=NULL,		  -- Action
	 @v_fieldName2Vc varchar(500)=NULL,
	 @v_fieldName3Vc varchar(500)=NULL,
	 @v_fieldName4Vc varchar(500)=NULL,
	 @v_fieldName5Vc varchar(500)=NULL,
	 @v_fieldName6Vc varchar(500)=NULL,
	 @v_fieldName7Vc varchar(500)=NULL,
	 @v_fieldName8Vc varchar(500)=NULL,
	 @v_fieldName9Vc varchar(500)=NULL,
	 @v_fieldName10Vc varchar(500)=NULL,
	 @v_fieldName11Vc varchar(500)=NULL,
	 @v_fieldName12Vc varchar(500)=NULL,
	 @v_fieldName13Vc varchar(500)=NULL,
	 @v_fieldName14Vc varchar(500)=NULL,
	 @v_fieldName15Vc varchar(500)=NULL,
	 @v_fieldName16Vc varchar(500)=NULL,
	 @v_fieldName17Vc varchar(500)=NULL,
	 @v_fieldName18Vc varchar(500)=NULL,
	 @v_fieldName19Vc varchar(500)=NULL,
	 @v_fieldName20Vc varchar(500)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_FinalQueryVc varchar(Max)=null, @v_Query1Vc varchar(1000)=null,
	@v_ConditionString_Vc varchar(4000),
	@v_CntQry_Vc varchar(max),
	@v_PgCntQry_Vc varchar(max),
	@v_StartAt_Int int, @v_StopAt_Int int,
	@v_Page_inInt int ,
	@v_RecordPerPage_inInt int,
	@FinalQuery as varchar(max),
	@v_FormList_Vc varchar(2000),
	@v_FormList_Vc1 varchar(2000),
	@v_FormList_Vc3 varchar(2000)

	Declare @v_BankUserRole_Vc varchar(50), @v_BankBranchId int, @v_BankId int
	Declare @evalCount int
	Declare
@Field1 varchar(500),
@Field2 varchar(500),
@Field3 varchar(500),
@Field4 varchar(500),
@Field5 varchar(500),
@Field6 varchar(500),
@Field7 varchar(500),
@Field8 varchar(500),
@Field9 varchar(500),
@Field10 varchar(500),
@Field11 varchar(500),
@Field12 varchar(500),
@Field13 varchar(500),
@Field14 varchar(500),
@Field15 varchar(500),
@Field16 varchar(500),
@Field17 varchar(500),
@Field18 varchar(500),
@Field19 varchar(500),
@Field20 varchar(500)

-------


    -- Insert statements for procedure here

--IF @v_fieldName1Vc = 'getRegFeePayment_MISReport'
--BEGIN

--	select @v_ConditionString_Vc=''
--	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
--	Select @v_Page_inInt=@v_fieldName2Vc, @v_RecordPerPage_inInt=@v_fieldName3Vc

--	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
--	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
--	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */


--	If @v_fieldName4Vc is not null And @v_fieldName4Vc<>'' -- Creiteris: Status
--	Begin
--		If @v_fieldName4Vc='New'
--		Begin
--			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And RP.status=''paid'''
--		End
--		Else If @v_fieldName4Vc='Renew'
--		Begin
--			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And RP.status=''renewed'''
--		End
--		Else If @v_fieldName4Vc='Expired'
--		Begin
--			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And LM.validUpTo < getdate()'
--		End
--	End


--	IF (@v_fieldName5Vc is not null And @v_fieldName5Vc <>'') And (@v_fieldName6Vc is not null And @v_fieldName6Vc <>'')
--	Begin
--		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) >= ''' + @v_fieldName5Vc + ''' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) <= ''' + @v_fieldName6Vc + ''''
--	End
--	ELSE IF (@v_fieldName5Vc is not null And @v_fieldName5Vc <>'') And (@v_fieldName6Vc is null OR @v_fieldName6Vc='')
--	Begin
--		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) >= ''' + @v_fieldName5Vc + ''''
--	End
--	ELSE IF (@v_fieldName5Vc is not null Or @v_fieldName5Vc <>'') And (@v_fieldName6Vc is not null And @v_fieldName6Vc <>'')
--	Begin
--		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) <= ''' + @v_fieldName6Vc + ''''
--	End

--	If @v_fieldName7Vc is not null And @v_fieldName7Vc<>'' -- Creiteris: Email ID
--	Begin
--		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And LM.emailId ='''+@v_fieldName7Vc+''''
--	End

--	If @v_fieldName8Vc is not null And @v_fieldName8Vc<>'' -- Creiteris: Company Name
--	Begin
--		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And dbo.f_getbiddercompany(LM.userId) ='''+@v_fieldName8Vc+''''
--	End


--	--Print(@v_ConditionString_Vc)

--	-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
--	Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT( Distinct regPaymentId ) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')
--	From tbl_RegFeePayment RP
--		Inner join tbl_LoginMaster LM On RP.userId=LM.userId
--		Inner join tbl_TendererMaster TM On TM.userId=LM.userId
--		Where isLive=''yes'' and isVerified=''yes'' ' + @v_ConditionString_Vc

--	Select @v_CntQry_Vc='SELECT Count(Distinct regPaymentId)
--	From tbl_RegFeePayment RP
--		Inner join tbl_LoginMaster LM On RP.userId=LM.userId
--		Inner join tbl_TendererMaster TM On TM.userId=LM.userId
--		Where isLive=''yes'' and isVerified=''yes'' ' + @v_ConditionString_Vc


--			/* START CODE: DYNAMIC QUERY TO GET RECORDS */

--			/*
--				FieldValue1: Payment Id
--				FieldValue2: User Id of Bidder for which Payment is done
--				FieldValue3: Email Id of Bidder for which Payment is done
--				FieldValue4: Branch Maker - Name of Bank User who has done the Payment
--				FieldValue5: Payment Status New Payment/Renewal
--				FieldValue6: Date of Payment
--			*/

--		Select @v_FinalQueryVc=
--	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
--		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
--		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');

--	Select FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6,FieldValue7, FieldValue8, FieldValue9, FieldValue10, FieldValue11, FieldValue12
--	From
--	(Select distinct Convert(varchar(50), RP.regPaymentId) as FieldValue1,
--	Convert(varchar(50), LM.userId) as FieldValue2,
--	CASE LM.registrationType
--		When ''individualconsultant'' Then ''Individual Consultant''
--		When ''govtundertaking'' Then ''Government owned Enterprise''
--		When ''media'' Then ''Media''
--		When ''contractor'' Then ''Tenderer / Consultant''
--		Else ''''
--		End as FieldValue3,
--	Case RP.status
--		When ''paid'' Then ''New''
--		When ''renewed'' Then ''Renewal''
--		End as FieldValue4,
--	IsNUll(dbo.f_getbiddercompany(LM.userId),'''') as FieldValue5,
--	LM.emailId as FieldValue6,
--	TM.country as FieldValue7,
--	TM.state as FieldValue8,
--	REPLACE(CONVERT(VARCHAR(11),RP.dtOfPayment, 106), '' '', ''-'')+ '' '' + Substring(CONVERT(VARCHAR(30),RP.dtOfPayment,108),1,5) as FieldValue9,
--	Case Currency
--		When ''BDT''
--			Then ''Taka'' + '' '' + CONVERT(varchar(50),RP.amount)
--		When ''USD''
--			Then ''$'' + '' '' + CONVERT(varchar(50),RP.amount)
--			End as FieldValue10,
--	CONVERT(varchar(50),@v_TotalPageCnt_Int) as FieldValue11,
--	CONVERT(varchar(50),@v_TotalRecordCnt_Int) as FieldValue12,
--	CONVERT(varchar(50),Tmp.RowNumber) as FieldValue13,
--	Tmp.regPaymentId
--		From
--		(Select RowNumber, regPaymentId From
--			(
--				Select ROW_NUMBER() Over (Order by regPaymentId desc) as RowNumber, regPaymentId From
--				(
--					SELECT distinct RP.regPaymentId
--						From tbl_RegFeePayment RP
--						Inner join tbl_LoginMaster LM On RP.userId=LM.userId
--						Inner join tbl_TendererMaster TM On TM.userId=LM.userId
--						Where isLive=''yes'' and isVerified=''yes'' '
--						+@v_ConditionString_Vc+
--				') as InnerTbl
--			) B
--			WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
--		+ ' ) Tmp
--		Inner Join tbl_RegFeePayment RP On Tmp.regPaymentId=RP.regPaymentId
--			Inner join tbl_LoginMaster LM On RP.userId=LM.userId
--			Inner join tbl_TendererMaster TM On TM.userId=LM.userId
--		) as A Order by A.regPaymentId desc
--		'
--		/* END CODE: DYNAMIC QUERY TO GET RECORDS */


--		PRINT(@v_FinalQueryVc)
--		Exec (@v_FinalQueryVc)

--END

-- start nafiul
If @v_fieldName1Vc='GetDebaredBiddingPermissionInfo'     
Begin
select top 1 ProcurementCategory as FieldValue1, cast(dbp.CompanyID as varchar) as FieldValue2, cast(dbp.UserId as varchar) as FieldValue3, cast(DebarredFrom as varchar) as FieldValue4, cast(DebarredUpto as varchar) as FieldValue5, dbp.Comments as FieldValue6, cast(DebarredBy as varchar) as FieldValue7, DebarredReason as FieldValue8, emailId as FieldValue9, companyName as FieldValue10, firstName as FieldValue11, middleName as FieldValue12, lastName as FieldValue13 from tbl_DebarredBiddingPermission dbp inner join tbl_LoginMaster lm on dbp.UserId = lm.userId inner join tbl_TendererMaster tm on lm.userId = tm.userId left join tbl_CompanyMaster cm on cm.companyId = dbp.CompanyID  where dbp.UserId = @v_fieldName2Vc and ProcurementCategory = @v_fieldName3Vc and dbp.isActive = 1 
End
--end nafiul

IF @v_fieldName1Vc='getTenderOpeningRpt'
BEGIN
	If @v_fieldName3Vc='0' -- If Lot Id
	Begin
		SELECT ministry as FieldValue1, division as FieldValue2, agency as FieldValue3, peofficename as FieldValue4,
		packageNo as FieldValue5, packageDescription as FieldValue6,
		procurementType as FieldValue7,  budgetType as FieldValue8, procurementFullName as FieldValue9,
		ISNULL(REPLACE(CONVERT(VARCHAR(11), tenderPubDt, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), tenderPubDt,108), 1, 5), '-')  as FieldValue10,
		 ISNULL(REPLACE(CONVERT(VARCHAR(11), submissionDt, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), submissionDt,108), 1, 5), '-')  as FieldValue11,
		'' as FieldValue12,
		'' as FieldValue13,
		budgetType as FieldValue14,
		case when sourceOfFund='Government' then 'GOB' when sourceOfFund='own fund'  then 'own fund' else  IsNull(devPartners,'-') end as  FieldValue15,
		ISNULL(REPLACE(CONVERT(VARCHAR(11), tod.openingDt, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), tod.openingDt,108), 1, 5), '-')  as FieldValue16,
		ISNULL(REPLACE(CONVERT(VARCHAR(11), tenderValidityDt, 106), ' ', '-'),'-')  as FieldValue17,
		ISNULL(REPLACE(CONVERT(VARCHAR(11), tenderSecurityDt, 106), ' ', '-'), '-')  as FieldValue18,
		--employeeName as FieldValue19
		'<table width="100%" border="0" cellspacing="0">
            <tr>
                <td class="ff" width="20%"  style="border: none;">Name: </td>
                <td width="80%"  style="border: none;">'+et.employeeName+'</td>
            </tr>
            <tr>
                <td class="ff"  style="border: none;">Designation: </td>
                <td  style="border: none;">'+dm.designationName+'</td>
            </tr>
            <tr>
                <td class="ff"  style="border: none;">Role: </td>
                <td  style="border: none;">'+ Replace (pr.procurementRole,'PE','PA') + '</td>
            </tr>
            <tr>
                <td class="ff"  style="border: none;">Office Name: </td>
                <td  style="border: none;">'+om.officeName +'</td>
            </tr>
            <tr>
                <td class="ff"  style="border: none;">'+dpm.departmentType+': </td>
                <td  style="border: none;">'+dpm.departmentName+'</td>
            </tr>
        </table>' as FieldValue19,isNull(convert(varchar(15),tenderValidityDt,103),'null') as FieldValue20
            FROM dbo.tbl_TenderDetails
                    Inner Join dbo.tbl_TenderLotSecurity ON dbo.tbl_TenderDetails.tenderId = dbo.tbl_TenderLotSecurity.tenderId
                    --Inner Join tbl_TenderOpenDates tod On tbl_TenderDetails.tenderId=tod.tenderId
                    Inner Join (SELECT distinct tenderId,openingDt from tbl_TenderOpenDates ) tod On tbl_TenderDetails.tenderId=tod.tenderId
                                AND tbl_TenderDetails.openingDt = tod.openingDt
                    inner join tbl_EmployeeTrasfer et on tbl_TenderDetails.approvingAuthId=et.userId and et.isCurrent='yes'
                    inner join tbl_ProcurementMethod tpm on tbl_TenderDetails.procurementMethod = tpm.procurementMethod
                    inner join tbl_EmployeeMaster em On et.userId=em.userId
                    inner join tbl_DesignationMaster dm On em.designationId=dm.designationId
                    inner join tbl_EmployeeRoles er On em.employeeId=er.employeeId
                    inner join tbl_ProcurementRole pr On er.procurementRoleId=pr.procurementRoleId
                    inner join tbl_OfficeMaster om On dbo.tbl_TenderDetails.officeId=om.officeId
                    inner join tbl_DepartmentMaster dpm On om.departmentId=dpm.departmentId
            WHERE dbo.tbl_TenderDetails.tenderId = @v_fieldName2Vc
	End
	Else
	Begin
		SELECT ministry as FieldValue1, division as FieldValue2, agency as FieldValue3, peofficename as FieldValue4,
                    packageNo as FieldValue5, packageDescription as FieldValue6,
                    procurementType as FieldValue7, budgetType as FieldValue8, procurementFullName as FieldValue9,
                    ISNULL(REPLACE(CONVERT(VARCHAR(11), tenderPubDt, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), tenderPubDt,108), 1, 5), '-')  as FieldValue10,
                     ISNULL(REPLACE(CONVERT(VARCHAR(11), submissionDt, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), submissionDt,108), 1, 5), '-')  as FieldValue11,
                    lotNo as FieldValue12,
                    lotDesc as FieldValue13, budgetType as FieldValue14,
                    case when sourceOfFund='Government' then 'GOB' when sourceOfFund='own fund'  then 'own fund' else  IsNull(devPartners,'-') end as  FieldValue15,
                    ISNULL(REPLACE(CONVERT(VARCHAR(11), tod.openingDt, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), tod.openingDt,108), 1, 5), '-')  as FieldValue16,
                    ISNULL(REPLACE(CONVERT(VARCHAR(11), tenderValidityDt, 106), ' ', '-'),'-')  as FieldValue17,
                    ISNULL(REPLACE(CONVERT(VARCHAR(11), tenderSecurityDt, 106), ' ', '-'), '-')  as FieldValue18,
                    --employeeName as FieldValue19
                    '<table width="100%" border="0" cellspacing="0">
                <tr>
                    <td class="ff" width="20%"   style="border: none;">Name: </td>
                    <td width="80%"  style="border: none;">'+et.employeeName+'</td>
                </tr>
                <tr>
                    <td class="ff"  style="border: none;">Designation: </td>
                    <td  style="border: none;">'+dm.designationName+'</td>
                </tr>
                <tr>
                    <td class="ff"  style="border: none;">Role: </td>
                    <td  style="border: none;">'+ Replace (pr.procurementRole,'PE','PA') + '</td>
                </tr>
                <tr>
                    <td class="ff"  style="border: none;">Office Name: </td>
                    <td  style="border: none;">'+om.officeName +'</td>
                </tr>
                <tr>
                    <td class="ff"  style="border: none;">'+dpm.departmentType+': </td>
                    <td  style="border: none;">'+dpm.departmentName+'</td>
                </tr>
            </table>'
                    as FieldValue19,isNull(convert(varchar(15),tenderValidityDt,103),'null') as FieldValue20
            FROM dbo.tbl_TenderDetails 
                Inner Join dbo.tbl_TenderLotSecurity ON dbo.tbl_TenderDetails.tenderId = dbo.tbl_TenderLotSecurity.tenderId
                --Inner Join tbl_TenderOpenDates tod On tbl_TenderDetails.tenderId=tod.tenderId
		Inner Join (SELECT distinct tenderId,openingDt from tbl_TenderOpenDates ) tod On tbl_TenderDetails.tenderId=tod.tenderId
                            AND tbl_TenderDetails.openingDt = tod.openingDt
		inner join tbl_EmployeeTrasfer et on tbl_TenderDetails.approvingAuthId=et.userId and et.isCurrent='yes'
		inner join tbl_ProcurementMethod tpm on tbl_TenderDetails.procurementMethod = tpm.procurementMethod
		inner join tbl_EmployeeMaster em On et.userId=em.userId
		inner join tbl_DesignationMaster dm On em.designationId=dm.designationId
		inner join tbl_EmployeeRoles er On em.employeeId=er.employeeId
		inner join tbl_ProcurementRole pr On er.procurementRoleId=pr.procurementRoleId
		inner join tbl_OfficeMaster om On dbo.tbl_TenderDetails.officeId=om.officeId
		inner join tbl_DepartmentMaster dpm On om.departmentId=dpm.departmentId
            WHERE dbo.tbl_TenderDetails.tenderId = @v_fieldName2Vc And tbl_TenderLotSecurity.appPkgLotId=@v_fieldName3Vc
	End
END

IF @v_fieldName1Vc='getBidderPostQualification'
BEGIN
 select postQualStatus as FieldValue1,convert(varchar(50),userId) as FieldValue2 from tbl_PostQualification where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
End
IF @v_fieldName1Vc='getAdvertisementDetail'
BEGIN

	SELECT newsPaper as FieldValue1, ISNULL(REPLACE(CONVERT(VARCHAR(11), newsPaperPubDt, 106), ' ', '-'),' ') as FieldValue2,
	webSite as FieldValue3 ,
	case when webSiteAdvtDt='1900-01-01' then
		''
	else
		ISNULL(REPLACE(CONVERT(VARCHAR(11), webSiteAdvtDt, 106), ' ', '-'),' ')
	end  as FieldValue4
	FROM
	dbo.tbl_TenderAdvertisement WHERE tenderId = @v_fieldName2Vc

END

IF @v_fieldName1Vc='getOpeningDateAndTime'
BEGIN

	SELECT DISTINCT ISNULL(REPLACE(CONVERT(VARCHAR(11), openingdt, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), openingdt,108), 1, 5), 'N.A.')
	as FieldValue1 from dbo.tbl_TenderOpenDates WHERE tenderid = @v_fieldName2Vc

END

IF @v_fieldName1Vc='getTenderDocument'
BEGIN

	Declare @v_RecvCount_Int INT,
	@v_WithdrawlCount_Int INT,
	@v_DownloadCount_Int INT,
	@v_SubstitutedCount_Int INT,
	@v_isDocPaid int


	Select @v_isDocPaid =
		Case docFeesMethod
		When 'Package wise'
		Then
			Case
				When (pkgDocFees > 0)
				Then 1
				Else 0
			End
		When 'Lot wise'
		Then
			Case
				When ((select Sum(docFess) from tbl_TenderLotSecurity Where tenderId=@v_fieldName2Vc) > 0)
				Then 1
				Else 0
			End
		Else 0
		End
		From tbl_TenderDetails where tenderId=@v_fieldName2Vc




	if @v_fieldName3Vc='0'
		BEGIN
			SELECT @v_RecvCount_Int = COUNT(DISTINCT fs.finalSubmissionId) From tbl_FinalSubmission fs
			Where fs.bidSubStatus ='FinalSubmission'  and fs.tenderId = @v_fieldName2Vc

			SELECT @v_WithdrawlCount_Int = COUNT(DISTINCT fs.bidModId) From tbl_BidModification fs
			Where fs.bidStatus ='Withdrawal'  and fs.tenderId = @v_fieldName2Vc

			SELECT @v_SubstitutedCount_Int = COUNT(distinct fs.userId) From tbl_BidModification fs
			Where fs.bidStatus ='modify'  and fs.tenderId = @v_fieldName2Vc
		END
	ELSE
		BEGIN
			SELECT @v_RecvCount_Int = COUNT(DISTINCT fs.finalSubmissionId) From tbl_FinalSubmission fs
			inner join tbl_BidderLots bl on fs.tenderId = bl.tenderId and fs.userId = bl.userId and bl.pkgLotId = @v_fieldName3Vc
			Where fs.bidSubStatus ='FinalSubmission'  and fs.tenderId = @v_fieldName2Vc

			SELECT @v_WithdrawlCount_Int = COUNT(DISTINCT fs.bidModId) From tbl_BidModification fs
			inner join tbl_BidderLots bl on fs.tenderId = bl.tenderId and fs.userId = bl.userId and bl.pkgLotId = @v_fieldName3Vc
			Where fs.bidStatus ='Withdrawal'  and fs.tenderId = @v_fieldName2Vc

			SELECT @v_SubstitutedCount_Int = COUNT(distinct fs.userId) From tbl_BidModification fs
			inner join tbl_BidderLots bl on fs.tenderId = bl.tenderId and fs.userId = bl.userId and bl.pkgLotId = @v_fieldName3Vc
			Where fs.bidStatus ='modify'  and fs.tenderId = @v_fieldName2Vc
		END



	IF @v_isDocPaid = 1 -- // Documents are Payable
	BEGIN
		If (select docAvlMethod from tbl_TenderDetails Where tenderId=@v_fieldName2Vc)='Package'
		Begin
			Select @v_DownloadCount_Int =
			Count (distinct userId) from tbl_TenderPayment tp
			where tenderId=@v_fieldName2Vc
					and paymentFor='Document Fees'
					and isVerified='Yes'
		End
		Else If (select docAvlMethod from tbl_TenderDetails Where tenderId=@v_fieldName2Vc)='Lot'
		Begin
			Select @v_DownloadCount_Int =
			Count (distinct userId) from tbl_TenderPayment tp
			where tenderId=@v_fieldName2Vc And pkgLotId=@v_fieldName3Vc
					and paymentFor='Document Fees'
					and isVerified='Yes'
		End
	END
	ELSE IF @v_isDocPaid = 0 -- // Documents are Free
	BEGIN
		--If (select docAvlMethod from tbl_TenderDetails Where tenderId=@v_fieldName2Vc)='Package'
		--Begin
			Select @v_DownloadCount_Int =
			COUNT(distinct userId) from tbl_BidConfirmation where tenderId=@v_fieldName2Vc
			and confirmationStatus='accepted'
		--End
	END

	--If (select docAvlMethod from tbl_TenderDetails Where tenderId=@v_fieldName2Vc)='Package'
	--Begin
	--	Select @v_DownloadCount_Int= Count(distinct(docStatsId)) From tbl_TenderDocStatistics Where tenderId=@v_fieldName2Vc
	--End
	--Else If (select docAvlMethod from tbl_TenderDetails Where tenderId=@v_fieldName2Vc)='Lot'
	--Begin
	--	Select @v_DownloadCount_Int= Count(distinct(docStatsId)) From tbl_TenderDocStatistics Where tenderId=@v_fieldName2Vc And pkgLotId=@v_fieldName3Vc
	--End

	SELECT CONVERT(VARCHAR(10), @v_RecvCount_Int) as FieldValue1,
			CONVERT(VARCHAR(10), @v_WithdrawlCount_Int) as FieldValue2,
			CONVERT(VARCHAR(10), @v_DownloadCount_Int) as FieldValue3,
			CONVERT(VARCHAR(10), @v_SubstitutedCount_Int) as FieldValue4

END

IF @v_fieldName1Vc='getTOCMembers'
BEGIN

	--SELECT employeeName as FieldValue1,cm.memberRole as FieldValue2, dsm.designationName as FieldValue3,om.officeName as FieldValue4
	--from tbl_Committee c,tbl_CommitteeMembers cm,tbl_EmployeeMaster em,tbl_EmployeeOffices eo,
	--tbl_OfficeMaster om,tbl_DepartmentMaster  dm,tbl_DesignationMaster dsm
	--WHERE c.committeeId=cm.committeeId and cm.userId=em.userId
	--and em.employeeId=eo.employeeId and eo.officeId=om.officeId  and dsm.designationId=em.designationId
	--and om.departmentId=dm.departmentId and dm.departmentId=dsm.departmentid
	--and tenderId = @v_fieldName2Vc and committeeType in('TOC','POC')

	If @v_fieldName3Vc='0'
	Begin
		SELECT	--employeeName as FieldValue1,
				--(SELECT dbo.f_Gov_Part_UserName(lm.userId,(SELECT signedDate from tbl_TORRptSign trs Where trs.userId=cm.userId And tenderId=c.tenderId and trs.reportType=@v_fieldName4Vc),lm.userTyperId)) as FieldValue1,  -- Commented by Dohatec for Transfer Employee
				(SELECT dbo.f_Gov_Part_Transfer_UserName(lm.userId,(SELECT signedDate from tbl_TORRptSign trs Where trs.userId=cm.userId And tenderId=c.tenderId and trs.reportType=@v_fieldName4Vc),lm.userTyperId)) as FieldValue1, -- Modified by dohatec for transfer employee
				cm.memberRole as FieldValue2,
				dsm.designationName as FieldValue3,
				om.officeName as FieldValue4,
				CONVERT(varchar(50), cm.userId) as FieldValue5,
				IsNull((select CONVERT(varchar(50), signedDate, 106) + ' ' + Substring(CONVERT(VARCHAR(30),signedDate,108),1,5)  from tbl_TORRptSign trs Where trs.userId=cm.userId And tenderId=c.tenderId and trs.reportType=@v_fieldName4Vc),'-') as FieldValue6,
				CONVERT(varchar(50), c.committeeId) as FieldValue7,
				IsNull((select trs.comments from tbl_TORRptSign trs Where trs.userId=cm.userId And tenderId=c.tenderId and trs.reportType=@v_fieldName4Vc),'-') as FieldValue8
		from	tbl_Committee c,
				tbl_CommitteeMembers cm,
				tbl_EmployeeMaster em,
				tbl_EmployeeOffices eo,
				tbl_OfficeMaster om,
				tbl_DepartmentMaster  dm,
				tbl_DesignationMaster dsm,
				tbl_loginmaster lm
		WHERE	c.committeeId=cm.committeeId
				and cm.userId=em.userId
				and lm.userid = em.userid
				and em.employeeId=eo.employeeId
				and eo.officeId=om.officeId
				and dsm.designationId=em.designationId
				and om.departmentId=dm.departmentId
				and dm.departmentId=dsm.departmentid
				and c.tenderId = @v_fieldName2Vc
				and committeeType in('TOC','POC')
				And committeStatus='Approved'

	End
	Else
	Begin
		SELECT	--employeeName as FieldValue1,
				--(SELECT dbo.f_Gov_Part_UserName(lm.userId,(SELECT signedDate from tbl_TORRptSign trs Where trs.userId=cm.userId And tenderId=c.tenderId and trs.reportType=@v_fieldName4Vc),lm.userTyperId)) as FieldValue1,-- Commented by Dohatec for Transfer Employee
				(SELECT dbo.f_Gov_Part_Transfer_UserName(lm.userId,(SELECT signedDate from tbl_TORRptSign trs Where trs.userId=cm.userId And tenderId=c.tenderId  And trs.pkgLotId=@v_fieldName3Vc and trs.reportType=@v_fieldName4Vc),lm.userTyperId)) as FieldValue1, -- Modified by dohatec for transfer employee
				cm.memberRole as FieldValue2,
				dsm.designationName as FieldValue3,
				om.officeName as FieldValue4,
				CONVERT(varchar(50), cm.userId) as FieldValue5,
				IsNull((select CONVERT(varchar(50), signedDate, 106)+ ' ' + Substring(CONVERT(VARCHAR(30),signedDate,108),1,5)  from tbl_TORRptSign trs Where trs.userId=cm.userId And tenderId=c.tenderId And trs.pkgLotId=@v_fieldName3Vc  and trs.reportType=@v_fieldName4Vc),'-') as FieldValue6,
				CONVERT(varchar(50), c.committeeId) as FieldValue7,
				IsNull((select trs.comments from tbl_TORRptSign trs Where trs.userId=cm.userId And tenderId=c.tenderId And trs.pkgLotId=@v_fieldName3Vc  and trs.reportType=@v_fieldName4Vc),'-') as FieldValue8
		FROM	tbl_Committee c,
				tbl_CommitteeMembers cm,
				tbl_EmployeeMaster em,
				tbl_EmployeeOffices eo,
				tbl_OfficeMaster om,
				tbl_DepartmentMaster  dm,
				tbl_DesignationMaster dsm,
				tbl_loginmaster lm
		WHERE	c.committeeId=cm.committeeId
				and cm.userId=em.userId
				and lm.userId=em.userId
				and em.employeeId=eo.employeeId
				and eo.officeId=om.officeId
				and dsm.designationId=em.designationId
				and om.departmentId=dm.departmentId
				and dm.departmentId=dsm.departmentid
				and c.tenderId = @v_fieldName2Vc
				and committeeType in('TOC','POC')
				And committeStatus='Approved'

	End
END

IF @v_fieldName1Vc = 'getTenderPaymentListing'
BEGIN

	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName2Vc, @v_RecordPerPage_inInt=@v_fieldName3Vc

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */


	SET @v_ConditionString_Vc=' And isLive=''yes'''
	Select @v_BankUserRole_Vc=isMakerChecker,
		   @v_BankBranchId = sBankDevelopId
	From tbl_PartnerAdmin Where userId=@v_fieldName4Vc

	Select @v_BankId = Case sBankDevelHeadId
	When 0 Then @v_BankBranchId Else sBankDevelHeadId End
	From tbl_ScBankDevPartnerMaster Where sBankDevelopId=@v_BankBranchId



	If @v_BankUserRole_Vc='BranchChecker'
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And tenderPaymentId in (Select tenderPaymentId From tbl_TenderPayment Where createdBy in (select distinct userId from tbl_PartnerAdmin where sBankDevelopId= '+Convert(varchar(50), @v_BankBranchId)+'))'
	End
	Else If @v_BankUserRole_Vc='BankChecker'
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And tenderPaymentId in (Select tenderPaymentId From tbl_TenderPayment Where createdBy in (select distinct userId from tbl_PartnerAdmin where sBankDevelopId in (select distinct sBankDevelopId from tbl_ScBankDevPartnerMaster Where sBankDevelHeadId='+Convert(varchar(50), @v_BankId)+')))'
	End

	If @v_fieldName5Vc<>''
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And emailId=''' + @v_fieldName5Vc +''''
	End

	If @v_fieldName6Vc<>'' -- Verified status
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And isVerified=''' + @v_fieldName6Vc +''''
	End

	If @v_fieldName7Vc<>'' -- Branch Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderPaymentId in (Select tenderPaymentId From tbl_TenderPayment Where createdBy in (select distinct userId from tbl_PartnerAdmin where sBankDevelopId='+@v_fieldName7Vc+'))'
	End

	If @v_fieldName8Vc<>'' -- Branch Member Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And createdBy=''' + @v_fieldName8Vc +''''
	End

	--Print (@v_fieldName9Vc)
	--Print (@v_fieldName10Vc)
	IF (@v_fieldName9Vc is not null And @v_fieldName9Vc <>'') And (@v_fieldName10Vc is not null And @v_fieldName10Vc <>'')
	Begin
		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (createdDate as Float)) as Datetime) >= ''' + @v_fieldName9Vc + ''' And Cast (Floor(Cast (createdDate as Float)) as Datetime) <= ''' + @v_fieldName10Vc + ''''
	End
	ELSE IF (@v_fieldName9Vc is not null And @v_fieldName9Vc <>'') And (@v_fieldName10Vc is null OR @v_fieldName10Vc='')
	Begin
		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (createdDate as Float)) as Datetime) >= ''' + @v_fieldName9Vc + ''''
	End
	ELSE IF (@v_fieldName9Vc is not null Or @v_fieldName9Vc <>'') And (@v_fieldName10Vc is not null And @v_fieldName10Vc <>'')
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And Cast (Floor(Cast (createdDate as Float)) as Datetime) <= ''' + @v_fieldName10Vc + ''''
	End


	If @v_fieldName11Vc is Not Null And @v_fieldName11Vc<>'' -- Tender Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderId=''' + @v_fieldName11Vc +''''
	End

	If @v_fieldName12Vc is Not Null And  @v_fieldName12Vc<>'' -- Tenderer Name
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And dbo.f_getbiddercompany(LM.userId)=''' + @v_fieldName12Vc +''''
	End

	If @v_fieldName13Vc is Not Null And  @v_fieldName13Vc<>'' And @v_fieldName13Vc<>'All' -- Payment For
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And paymentFor=''' + @v_fieldName13Vc +''''
	End

	Print('Condition String: ')
	Print(@v_ConditionString_Vc)

	-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
	Select @v_PgCntQry_Vc= 'Ceiling(Cast(COUNT( Distinct tenderPaymentId ) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')
	From tbl_TenderPayment TP
	Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		WHERE tenderPaymentId is Not Null ' + @v_ConditionString_Vc

	Select @v_CntQry_Vc='Count(Distinct tenderPaymentId)
	From tbl_TenderPayment TP
	Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		WHERE tenderPaymentId is Not Null ' + @v_ConditionString_Vc


			/* START CODE: DYNAMIC QUERY TO GET RECORDS */

			/*
				FieldValue1: Payment Id
				FieldValue2: User Id of Bidder for which Payment is done
				FieldValue3: Email Id of Bidder for which Payment is done
				FieldValue4: Branch Maker - Name of Bank User who has done the Payment
				FieldValue5: Payment Status New Payment/Renewal
				FieldValue6: Date of Payment
			*/

	Print ('PageCount')
	Print (@v_PgCntQry_Vc)

	Print ('RowCount')
	Print (@v_CntQry_Vc)

		Select @v_FinalQueryVc=
	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');

	Select FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6,FieldValue7, FieldValue8,
	FieldValue9, FieldValue10, FieldValue11, FieldValue12, FieldValue13, FieldValue14, FieldValue15, FieldValue16
	From
	(Select Distinct
		Convert(varchar(50),Tmp.tenderPaymentId) as FieldValue1,
			Convert(varchar(50),LM.userId) as FieldValue2,
			LM.emailId as FieldValue3,
			PA.fullName as FieldValue4,
			TP.status as FieldValue5,
			REPLACE(CONVERT(VARCHAR(11),createdDate, 106), '' '', ''-'')+ '' '' + Substring(CONVERT(VARCHAR(30),createdDate,108),1,5) as FieldValue6,
			CONVERT(varchar(50),@v_TotalPageCnt_Int) as FieldValue7,
			CONVERT(varchar(50),@v_TotalRecordCnt_Int) as FieldValue8,
			CONVERT(varchar(50),Tmp.RowNumber) as FieldValue9,
			isVerified as FieldValue10,
			dbo.f_getbiddercompany(LM.userId) as FieldValue11,
			paymentFor as FieldValue12,
			Convert(varchar(50),amount) as FieldValue13,
			Convert(varchar(50),tenderId) as FieldValue14,
			Convert(varchar(50),pkgLotId) as FieldValue15,
			Case paymentFor
				When ''Document Fees'' Then ''df''
				When ''Tender Security'' Then ''ts''
				When ''Performance Security'' Then ''ps''
				End as FieldValue16,
			Tmp.tenderPaymentId
		From
		(Select RowNumber, tenderPaymentId From
			(SELECT ROW_NUMBER() Over (Order by tenderPaymentId desc) as RowNumber, tenderPaymentId
				From tbl_TenderPayment TP
				Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
				Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		WHERE tenderPaymentId is Not Null '
					+@v_ConditionString_Vc+
			') B

		WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+ ' ) Tmp
		Inner Join tbl_TenderPayment TP On Tmp.tenderPaymentId=TP.tenderPaymentId
			Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
			Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		) as A Order by A.tenderPaymentId desc
		'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */


		PRINT(@v_FinalQueryVc)
		Exec (@v_FinalQueryVc)

END


IF @v_fieldName1Vc = 'getNOAIssuedListing'
BEGIN

	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName2Vc, @v_RecordPerPage_inInt=@v_fieldName3Vc

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */


	SET @v_ConditionString_Vc=' And NA.acceptRejStatus=''approved'''

	If @v_fieldName5Vc is Not Null And @v_fieldName5Vc<>'' -- Tender Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And NID.tenderId=''' + @v_fieldName5Vc +''''
	End

	If @v_fieldName6Vc<>''
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And LM.emailId=''' + @v_fieldName6Vc +''''
	End

	If @v_fieldName7Vc is Not Null And  @v_fieldName7Vc<>'' -- Tenderer Name
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And dbo.f_getbiddercompany(LM.userId)=''' + @v_fieldName7Vc +''''
	End

	Print('Condition String: ')
	Print(@v_ConditionString_Vc)

	-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
	Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT( Distinct NA.noaAcceptId ) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')
		From tbl_NoaIssueDetails NID
	Inner Join tbl_NoaAcceptance NA On NID.noaIssueId=NA.noaIssueId
	Inner Join tbl_LoginMaster LM ON NID.userId=LM.userId
	Where NID.perfSecAmt > 0 and NA.noaAcceptId is Not Null ' + @v_ConditionString_Vc

	Select @v_CntQry_Vc='SELECT Count(Distinct NA.noaAcceptId )
	From tbl_NoaIssueDetails NID
	Inner Join tbl_NoaAcceptance NA On NID.noaIssueId=NA.noaIssueId
	Inner Join tbl_LoginMaster LM ON NID.userId=LM.userId
	Where NID.perfSecAmt > 0 and NA.noaAcceptId is Not Null ' + @v_ConditionString_Vc


			/* START CODE: DYNAMIC QUERY TO GET RECORDS */

			/*

			*/

	Print ('PageCount')
	Print (@v_PgCntQry_Vc)

	Print ('RowCount')
	Print (@v_CntQry_Vc)

		Select @v_FinalQueryVc=
	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');

	Select FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6,FieldValue7, FieldValue8,
	FieldValue9, FieldValue10, FieldValue11, FieldValue12, FieldValue13, FieldValue14, FieldValue15, FieldValue16, FieldValue17,
	FieldValue18, FieldValue19
	From
	(Select distinct
	CONVERT(varchar(50),  NA.noaAcceptId) as FieldValue1,
	CONVERT(varchar(50),  NID.noaIssueId) as FieldValue2,
	CONVERT(varchar(50),  NID.tenderId) as FieldValue3,
	(select tenderStatus from tbl_TenderDetails where tenderId = NID.tenderId) as FieldValue19,
	CONVERT(varchar(50),  LM.userId) as FieldValue4,
	LM.emailId as FieldValue5,
	dbo.f_getbiddercompany(LM.userId) as FieldValue6,
	NID.contractNo as FieldValue7,
	REPLACE(CONVERT(VARCHAR(11),NID.contractDt, 106), '' '', ''-'') as FieldValue8,
	Case When NID.perSecSubDt >= Cast(Floor(Cast(GETDATE() as Float)) as Datetime) Then ''Yes'' Else ''No'' End as FieldValue9,
	IsNull((Select top 1 dbo.f_initcap(TP.status) From tbl_TenderPayment TP Where TP.tenderId=NID.tenderId And TP.userId=NID.userId And TP.paymentFor=''Performance Security'' And isLive=''Yes'' And TP.extValidityRef=NID.noaIssueId), ''Pending'') as FieldValue10,
	CONVERT(varchar(50),@v_TotalPageCnt_Int) as FieldValue11,
	CONVERT(varchar(50),@v_TotalRecordCnt_Int) as FieldValue12,
	CONVERT(varchar(50),Tmp.RowNumber) as FieldValue13,
	Case
		When Exists (Select top 1 TP.createdby From tbl_TenderPayment TP Where TP.tenderId=NID.tenderId And TP.userId=NID.userId And TP.paymentFor=''Performance Security'' And isLive=''Yes'' And TP.extValidityRef=NID.noaIssueId)
		Then (Select top 1 Convert(varchar(50), TP.createdby) From tbl_TenderPayment TP Where TP.tenderId=NID.tenderId And TP.userId=NID.userId And TP.paymentFor=''Performance Security'' And isLive=''Yes'' And TP.extValidityRef=NID.noaIssueId)
		Else ''0''
		End as FieldValue14,
	IsNull(Convert(varchar(50), NID.pkgLotId),''0'') as FieldValue15,
	Case
		When Exists (Select top 1 TP.tenderPaymentId From tbl_TenderPayment TP Where TP.tenderId=NID.tenderId And TP.userId=NID.userId And TP.paymentFor=''Performance Security'' And isLive=''Yes'' And TP.extValidityRef=NID.noaIssueId)
		Then (Select top 1 Convert(varchar(50), TP.tenderPaymentId) From tbl_TenderPayment TP Where TP.tenderId=NID.tenderId And TP.userId=NID.userId And TP.paymentFor=''Performance Security'' And isLive=''Yes'' And TP.extValidityRef=NID.noaIssueId)
		Else ''0''
		End as FieldValue16,
	Case
		When Exists (Select top 1 TP.tenderPaymentId From tbl_TenderPayment TP Where TP.tenderId=NID.tenderId And TP.userId=NID.userId And TP.paymentFor=''Performance Security'' And isLive=''Yes'' And isVerified=''Yes'' And TP.extValidityRef=NID.noaIssueId)
		Then ''Yes''
		Else ''No''
		End as FieldValue17,

		Case
			When
				(
					(Select top 1 sBankDevelopId
					From tbl_TenderPayment TP
					Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
					Where TP.tenderId=NID.tenderId
					And TP.userId=NID.userId
					And TP.paymentFor=''Performance Security''
					And isLive=''Yes''
					And TP.extValidityRef=NID.noaIssueId)
				=
					(select top 1 sBankDevelopId from tbl_PartnerAdmin where userId='+@v_fieldName4Vc+')
				)
			Then ''Yes''
			Else ''No''
		End	as FieldValue18,

	Tmp.noaAcceptId
		From
		(Select RowNumber, noaAcceptId From
			(SELECT ROW_NUMBER() Over (Order by  NA.noaAcceptId desc) as RowNumber, noaAcceptId
				From tbl_NoaIssueDetails NID
	Inner Join tbl_NoaAcceptance NA On NID.noaIssueId=NA.noaIssueId
	Inner Join tbl_LoginMaster LM ON NID.userId=LM.userId
	Where NID.perfSecAmt > 0 and NA.noaAcceptId is Not Null ' + @v_ConditionString_Vc +
			') B

		WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+ ' ) Tmp
		Inner Join tbl_NoaAcceptance NA On Tmp.noaAcceptId=NA.noaAcceptId
			Inner Join tbl_NoaIssueDetails NID On NA.noaIssueId=NId.noaIssueId
			Inner Join tbl_LoginMaster LM ON NID.userId=LM.userId
		) as A Order by A.noaAcceptId desc
		'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */


		PRINT(@v_FinalQueryVc)
		Exec (@v_FinalQueryVc)

END

   IF @v_fieldName1Vc = 'getTenderPaymentListingWithSorting'
BEGIN
	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName2Vc, @v_RecordPerPage_inInt=@v_fieldName3Vc

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */



	SET @v_ConditionString_Vc=' And isLive=''yes'''
	Select @v_BankUserRole_Vc=isMakerChecker,
		   @v_BankBranchId = sBankDevelopId
	From tbl_PartnerAdmin Where userId=@v_fieldName4Vc

	Select @v_BankId = Case sBankDevelHeadId
	When 0 Then @v_BankBranchId Else sBankDevelHeadId End
	From tbl_ScBankDevPartnerMaster Where sBankDevelopId=@v_BankBranchId



	If @v_BankUserRole_Vc='BranchChecker'
	Begin
		--Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And tenderPaymentId in (Select tenderPaymentId From tbl_TenderPayment Where createdBy in (select distinct userId from tbl_PartnerAdmin where sBankDevelopId= '+Convert(varchar(50), @v_BankBranchId)+'))'
		--dohatec start
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And tenderPaymentId in (Select tenderPaymentId From tbl_TenderPayment Where branchName in (select sbDevelopName from tbl_ScBankDevPartnerMaster where sBankDevelopId= '+Convert(varchar(50), @v_BankBranchId)+') and bankName in (select sbDevelopName from tbl_ScBankDevPartnerMaster where sBankDevelopId = (select sBankDevelHeadId from tbl_ScBankDevPartnerMaster where sBankDevelopId = '+Convert(varchar(50), @v_BankBranchId)+')))'
		--dohatec end

	End
	Else If @v_BankUserRole_Vc='BankChecker'
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And tenderPaymentId in (Select tenderPaymentId From tbl_TenderPayment Where createdBy in (select distinct userId from tbl_PartnerAdmin where sBankDevelopId in (select distinct sBankDevelopId from tbl_ScBankDevPartnerMaster Where sBankDevelHeadId='+Convert(varchar(50), @v_BankId)+')))'
	End

	If @v_fieldName5Vc<>''
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And emailId=''' + @v_fieldName5Vc +''''
	End

	If @v_fieldName6Vc<>'' -- Verified status
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And isVerified=''' + @v_fieldName6Vc +''''
	End

	If @v_fieldName7Vc<>'' -- Branch Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderPaymentId in (Select tenderPaymentId From tbl_TenderPayment Where createdBy in (select distinct userId from tbl_PartnerAdmin where sBankDevelopId='+@v_fieldName7Vc+'))'
	End

	If @v_fieldName8Vc<>'' -- Branch Member Id
	Begin
		--Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And createdBy=''' + @v_fieldName8Vc +''''
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And partTransId=''' + @v_fieldName8Vc +''''
	End

	--Print (@v_fieldName9Vc)
	--Print (@v_fieldName10Vc)
	IF (@v_fieldName9Vc is not null And @v_fieldName9Vc <>'') And (@v_fieldName10Vc is not null And @v_fieldName10Vc <>'')
	Begin
		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (createdDate as Float)) as Datetime) >= ''' + @v_fieldName9Vc + ''' And Cast (Floor(Cast (createdDate as Float)) as Datetime) <= ''' + @v_fieldName10Vc + ''''
	End
	ELSE IF (@v_fieldName9Vc is not null And @v_fieldName9Vc <>'') And (@v_fieldName10Vc is null OR @v_fieldName10Vc='')
	Begin
		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (createdDate as Float)) as Datetime) >= ''' + @v_fieldName9Vc + ''''
	End
	ELSE IF (@v_fieldName9Vc is not null Or @v_fieldName9Vc <>'') And (@v_fieldName10Vc is not null And @v_fieldName10Vc <>'')
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And Cast (Floor(Cast (createdDate as Float)) as Datetime) <= ''' + @v_fieldName10Vc + ''''
	End


	If @v_fieldName11Vc is Not Null And @v_fieldName11Vc<>'' -- Tender Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderId=''' + @v_fieldName11Vc +''''
	End

	If @v_fieldName12Vc is Not Null And  @v_fieldName12Vc<>'' -- Tenderer Name
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And dbo.f_getbiddercompany(LM.userId)=''' + @v_fieldName12Vc +''''
	End

	If @v_fieldName13Vc is Not Null And  @v_fieldName13Vc<>'' And @v_fieldName13Vc<>'All' -- Payment For
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And paymentFor=''' + @v_fieldName13Vc +''''
	End

	--Print('Condition String: ')
	--Print(@v_ConditionString_Vc)

	-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
	Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT( Distinct tenderPaymentId ) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')
	From tbl_TenderPayment TP
	Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		WHERE tenderPaymentId is Not Null ' + @v_ConditionString_Vc

	Select @v_CntQry_Vc='SELECT Count(Distinct tenderPaymentId)
	From tbl_TenderPayment TP
	Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		WHERE tenderPaymentId is Not Null ' + @v_ConditionString_Vc


			/* START CODE: DYNAMIC QUERY TO GET RECORDS */

			/*
				FieldValue1: Payment Id
				FieldValue2: User Id of Bidder for which Payment is done
				FieldValue3: Email Id of Bidder for which Payment is done
				FieldValue4: Branch Maker - Name of Bank User who has done the Payment
				FieldValue5: Payment Status New Payment/Renewal
				FieldValue6: Date of Payment
			*/

	Print ('PageCount')
	Print (@v_PgCntQry_Vc)

	Print ('RowCount')
	Print (@v_CntQry_Vc)

		Select @v_FinalQueryVc=
	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');

	Select FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6,FieldValue7, FieldValue8,
	FieldValue9, FieldValue10, FieldValue11, FieldValue12, FieldValue13, FieldValue14, FieldValue15, FieldValue16,
	FieldValue17
	From
	(Select Distinct
		Convert(varchar(50),Tmp.tenderPaymentId) as FieldValue1,
			Convert(varchar(50),LM.userId) as FieldValue2,
			LM.emailId as FieldValue3,
			PA.fullName as FieldValue4,
			TP.status as FieldValue5,
			REPLACE(CONVERT(VARCHAR(11),createdDate, 106), '' '', ''-'')+ '' '' + Substring(CONVERT(VARCHAR(30),createdDate,108),1,5) as FieldValue6,
			CONVERT(varchar(50),@v_TotalPageCnt_Int) as FieldValue7,
			CONVERT(varchar(50),@v_TotalRecordCnt_Int) as FieldValue8,
			CONVERT(varchar(50),Tmp.RowNumber) as FieldValue9,
			isVerified as FieldValue10,
			dbo.f_getbiddercompany(LM.userId) as FieldValue11,
			paymentFor as FieldValue12,
			Convert(varchar(50),amount) as FieldValue13,
			Convert(varchar(50),tenderId) as FieldValue14,
			Convert(varchar(50),pkgLotId) as FieldValue15,
			Case paymentFor
				When ''Document Fees'' Then ''df''
				When ''Tender Security'' Then ''ts''
				When ''Performance Security'' Then ''ps''
				When ''Bank Guarantee'' Then ''bg''
				End as FieldValue16,
			IsNull((select top 1 ('' ,<br/> '' + lotNo) from tbl_TenderLotSecurity where tenderId=TP.tenderId and appPkgLotId=TP.pkgLotId ),'''') as FieldValue17,
			Tmp.tenderPaymentId,
			sortParam
		From
		(Select RowNumber, tenderPaymentId, sortParam From
			(SELECT ROW_NUMBER() Over (Order by '+@v_fieldName14Vc+ ' ' +@v_fieldName15Vc+') as RowNumber, tenderPaymentId,' +@v_fieldName14Vc+' as sortParam
				From tbl_TenderPayment TP
				Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
				Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		WHERE tenderPaymentId is Not Null '
					+@v_ConditionString_Vc+
			') B

		WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+ ' ) Tmp
		Inner Join tbl_TenderPayment TP On Tmp.tenderPaymentId=TP.tenderPaymentId
			Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
			Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		) as A Order By sortParam '+@v_fieldName15Vc+'
		--Order by A.tenderPaymentId desc
		'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */


		PRINT(@v_FinalQueryVc)
		Exec (@v_FinalQueryVc)

END




if @v_fieldName1Vc='getOpeningCommitteeCPId'
BEGIN
select distinct CONVERT(varchar(50), userId) as FieldValue1 from 
tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId 
where committeeType='TOC' and committeStatus='approved' and isCurrent='yes' 
and memberRole='cp' and tenderid = @v_fieldName2Vc
END


IF @v_fieldName1Vc = 'getSearchRegUserListing'
BEGIN

	--Declare @v_BankUserRole_Vc varchar(50), @v_BankBranchId int, @v_BankId int

	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName2Vc, @v_RecordPerPage_inInt=@v_fieldName3Vc

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */



	SET @v_ConditionString_Vc=' And isLive=''yes'''
	Select @v_BankUserRole_Vc=isMakerChecker,
		   @v_BankBranchId = sBankDevelopId
	From tbl_PartnerAdmin Where userId=@v_fieldName4Vc

	Select @v_BankId = Case sBankDevelHeadId
	When 0 Then @v_BankBranchId Else sBankDevelHeadId End
	From tbl_ScBankDevPartnerMaster Where sBankDevelopId=@v_BankBranchId



	If @v_BankUserRole_Vc='BranchChecker'
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And regPaymentId in (Select regPaymentId From tbl_RegFeePayment Where createdBy in (select distinct userId from tbl_PartnerAdmin where sBankDevelopId= '+Convert(varchar(50), @v_BankBranchId)+'))'
	End
	Else If @v_BankUserRole_Vc='BankChecker'
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And regPaymentId in (Select regPaymentId From tbl_RegFeePayment Where createdBy in (select distinct userId from tbl_PartnerAdmin
		where (
				sBankDevelopId= '+Convert(varchar(50), @v_BankBranchId)+
				' OR
				sBankDevelopId in (select distinct sBankDevelopId from tbl_ScBankDevPartnerMaster Where sBankDevelHeadId='+Convert(varchar(50), @v_BankId)+')
			)
		))'
	End

	If @v_fieldName5Vc<>''
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And emailId=''' + @v_fieldName5Vc +''''
	End

	If @v_fieldName6Vc<>'' -- Verified status
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And isVerified=''' + @v_fieldName6Vc +''''
	End

	If @v_fieldName7Vc<>'' -- Branch Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And regPaymentId in (Select regPaymentId From tbl_RegFeePayment Where createdBy in (select distinct userId from tbl_PartnerAdmin where sBankDevelopId='+@v_fieldName7Vc+'))'
	End

	If @v_fieldName8Vc<>'' -- Branch Member Id
	Begin
		--Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And createdBy=''' + @v_fieldName8Vc +''''
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And partTransId=''' + @v_fieldName8Vc +''''
	End

	Print (@v_fieldName9Vc)
	Print (@v_fieldName10Vc)
	IF (@v_fieldName9Vc is not null And @v_fieldName9Vc <>'') And (@v_fieldName10Vc is not null And @v_fieldName10Vc <>'')
	Begin
		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) >= ''' + @v_fieldName9Vc + ''' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) <= ''' + @v_fieldName10Vc + ''''
	End
	ELSE IF (@v_fieldName9Vc is not null And @v_fieldName9Vc <>'') And (@v_fieldName10Vc is null OR @v_fieldName10Vc='')
	Begin
		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) >= ''' + @v_fieldName9Vc + ''''
	End
	ELSE IF (@v_fieldName9Vc is not null Or @v_fieldName9Vc <>'') And (@v_fieldName10Vc is not null And @v_fieldName10Vc <>'')
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) <= ''' + @v_fieldName10Vc + ''''
	End
	Print(@v_ConditionString_Vc)

	-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
	Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT( Distinct regPaymentId ) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')
	From tbl_RegFeePayment RP
	Inner Join tbl_LoginMaster LM On RP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
		WHERE regPaymentId is Not Null ' + @v_ConditionString_Vc

	Select @v_CntQry_Vc='SELECT Count(Distinct regPaymentId)
	From tbl_RegFeePayment RP
	Inner Join tbl_LoginMaster LM On RP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
		WHERE regPaymentId is Not Null ' + @v_ConditionString_Vc

	Print ('Count Query')
	Print(@v_CntQry_Vc)

			/* START CODE: DYNAMIC QUERY TO GET RECORDS */

			/*
				FieldValue1: Payment Id
				FieldValue2: User Id of Bidder for which Payment is done
				FieldValue3: Email Id of Bidder for which Payment is done
				FieldValue4: Branch Maker - Name of Bank User who has done the Payment
				FieldValue5: Payment Status New Payment/Renewal
				FieldValue6: Date of Payment
			*/

		Select @v_FinalQueryVc=
	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');

	Select FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6,FieldValue7, FieldValue8, FieldValue9, FieldValue10
	From
	(Select Distinct
		Convert(varchar(50),Tmp.regPaymentId) as FieldValue1,
			Convert(varchar(50),LM.userId) as FieldValue2,
			LM.emailId as FieldValue3,
			dbo.f_GovUserName(RP.partTransId, ''tbl_PartnerAdminTransfer'') as FieldValue4,
			Case RP.status When ''paid'' Then ''New Registration''
						   When ''freezed'' Then ''New Registration''
						   When ''renewed'' Then ''Renewal''
				 End as FieldValue5,
			REPLACE(CONVERT(VARCHAR(11),dtOfPayment, 106), '' '', ''-'')+ '' '' + Substring(CONVERT(VARCHAR(30),dtOfPayment,108),1,5) as FieldValue6,
			CONVERT(varchar(50),@v_TotalPageCnt_Int) as FieldValue7,
			CONVERT(varchar(50),@v_TotalRecordCnt_Int) as FieldValue8,
			CONVERT(varchar(50),Tmp.RowNumber) as FieldValue9,
			isVerified as FieldValue10,
			Tmp.regPaymentId,
			sortParam
		From
		(Select RowNumber, regPaymentId, sortParam From
			(SELECT ROW_NUMBER() Over (Order by '+@v_fieldName11Vc+ ' ' +@v_fieldName12Vc+') as RowNumber, regPaymentId, ' +@v_fieldName11Vc+' as sortParam
				From tbl_RegFeePayment RP
				Inner Join tbl_LoginMaster LM On RP.userId=LM.userId
				Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
		WHERE regPaymentId is Not Null '
					+@v_ConditionString_Vc+
			') B

		WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+ ' ) Tmp
		Inner Join tbl_RegFeePayment RP On Tmp.regPaymentId=RP.regPaymentId
			Inner Join tbl_LoginMaster LM On RP.userId=LM.userId
			Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
		) as A Order By sortParam '+@v_fieldName12Vc+'
		--Order by A.regPaymentId desc
		'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */


		PRINT(@v_FinalQueryVc)
		Exec (@v_FinalQueryVc)

END

IF @v_fieldName1Vc = 'GeteGPBankInfo'
BEGIN
	If @v_fieldName2Vc='ScheduleBank'
	Begin
		--select distinct sbDevelopName as FieldValue1,
		--(select convert(varchar(50),count(sBankDevelopId)) from tbl_ScBankDevPartnerMaster
		--Where partnerType='ScheduleBank' And sBankDevelHeadId=A.sBankDevelopId And isBranchOffice='no') as FieldValue2
		--from tbl_ScBankDevPartnerMaster A
		--Where partnerType='ScheduleBank'And sBankDevelHeadId=0 	And isBranchOffice='no'

		select distinct sbDevelopName as FieldValue1,
		(select convert(varchar(50),count(sBankDevelopId)) from tbl_ScBankDevPartnerMaster
		Where partnerType='ScheduleBank' And sBankDevelHeadId=A.sBankDevelopId) as FieldValue2,
		convert(varchar(50),sBankDevelopId) as FieldValue3
		from tbl_ScBankDevPartnerMaster A
		Where partnerType='ScheduleBank' And isBranchOffice='no'
	End
	Else If @v_fieldName2Vc='Development'
	Begin
		--select distinct sbDevelopName as FieldValue1, '' as FieldValue2 from tbl_ScBankDevPartnerMaster
		--Where partnerType='Development' And sBankDevelHeadId=0	And isBranchOffice='no'

		select distinct sbDevelopName as FieldValue1,
		(select convert(varchar(50),count(aa.sBankDevelopId)) from tbl_ScBankDevPartnerMaster aa,
		tbl_PartnerAdmin bb,tbl_LoginMaster cc
		Where aa.partnerType='Development' And aa.sBankDevelHeadId=A.sBankDevelopId
		and aa.sBankDevelopId=bb.sBankDevelopId
		and bb.userId=cc.userId
		and cc.status!='deactive') as FieldValue2,
		convert(varchar(50),sBankDevelopId) as FieldValue3
		from tbl_ScBankDevPartnerMaster A
		Where partnerType='Development' And isBranchOffice='no'
	End
END

IF @v_fieldName1Vc = 'GetPEOfficeInfo' -- Ministry / Division / Organization wise PE office details page
BEGIN
	Select distinct	convert(varchar(50),count(dm.departmentId)) as FieldValue1,
	dm.departmentName as FieldValue2,
	(Select convert(varchar(50),count(officeId)) from tbl_OfficeMaster om where om.departmentId=dm.departmentId) as FieldValue3
	from tbl_DepartmentMaster dm Where departmentType=@v_fieldName2Vc
	Group By departmentId, departmentName
	Order by departmentName
END


IF @v_fieldName1Vc = 'GetMDOTenderCount' -- Ministry / Division / Organization Total Count
BEGIN
--	-- // Annual Procurement Plans (Tenders) Count Published By Ministry
---- Published
--select @Field1 = convert(varchar(50),count(tenderId)) from tbl_TenderDetails Where tenderStatus='approved'
--and (
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry')
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Division' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry') )
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry') )
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Division' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry')) )
--	)

---- Processed
--select @Field2 = convert(varchar(50),count(tenderId)) from tbl_TenderDetails Where tenderStatus='approved'
--and (
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry')
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Division' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry') )
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry') )
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Division' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry')) )
--	)
----and tenderId Not In (select distinct tenderId from tbl_NoaIssueDetails)
--and tenderId Not In (select distinct tenderId from tbl_NoaIssueDetails nid
--							INNER JOIN tbl_NoaAcceptance na on na.noaIssueId = nid.noaIssueId where acceptRejStatus ='approved')
--/* and tenderId in
--(select distinct c.tenderId
--from tbl_Committee c
--inner join tbl_CommitteeMembers cm ON c.committeeId=cm.committeeId
--where
--c.committeeType in ('TOC', 'POC')
--and c.committeStatus='approved'
--and cm.appStatus='approved'
--) */


---- Finalized
--select @Field3 = convert(varchar(50),count(tenderId)) from tbl_TenderDetails Where tenderStatus='approved'
--and (
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry')
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Division' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry') )
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry') )
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Division' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry')) )
--	)
--and tenderId In (select distinct tenderId from tbl_NoaIssueDetails NID
--					INNER JOIN tbl_ContractSign cs ON cs.noaId = NID.noaIssueId)

---- // Annual Procurement Plans (Tenders) Count Published By Division
---- Published
--select @Field4 = convert(varchar(50),count(tenderId)) from tbl_TenderDetails Where tenderStatus='approved'
--and (
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='division')
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='division') )
--	)

---- Processed
--select @Field5 = convert(varchar(50),count(tenderId)) from tbl_TenderDetails Where tenderStatus='approved'
--and (
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='division')
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='division') )
--	)
----and tenderId Not In (select distinct tenderId from tbl_NoaIssueDetails)
--and tenderId Not In (select distinct tenderId from tbl_NoaIssueDetails nid
--							INNER JOIN tbl_NoaAcceptance na on na.noaIssueId = nid.noaIssueId where acceptRejStatus ='approved')

---- Finalized
--select @Field6 = convert(varchar(50),count(tenderId)) from tbl_TenderDetails Where tenderStatus='approved'
--and (
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='division')
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='division') )
--	)
--and tenderId In (select distinct tenderId from tbl_NoaIssueDetails NID
--					INNER JOIN tbl_ContractSign cs ON cs.noaId = NID.noaIssueId)

---- // Annual Procurement Plans (Tenders) Count Published By Organization
---- Published
--select @Field7 = convert(varchar(50),count(tenderId)) from tbl_TenderDetails Where tenderStatus='approved'
--and departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Organization')

---- Processed
--select @Field8 = convert(varchar(50),count(tenderId)) from tbl_TenderDetails Where tenderStatus='approved'
--and departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Organization')
----and tenderId Not In (select distinct tenderId from tbl_NoaIssueDetails)
--and tenderId Not In (select distinct tenderId from tbl_NoaIssueDetails nid
--							INNER JOIN tbl_NoaAcceptance na on na.noaIssueId = nid.noaIssueId where acceptRejStatus ='approved')

---- Finalized
--select @Field9 = convert(varchar(50),count(tenderId)) from tbl_TenderDetails Where tenderStatus='approved'
--and departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Organization')
--and tenderId In (select distinct tenderId from tbl_NoaIssueDetails NID
--					INNER JOIN tbl_ContractSign cs ON cs.noaId = NID.noaIssueId AND isPubAggOnWeb = 'yes')

--Dohatec Start
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='#Temp_TenderCount') 
		   drop table  #Temp_TenderCount;
    
 CREATE TABLE #Temp_TenderCount
    (    id        nvarchar(50) COLLATE DATABASE_DEFAULT NOT NULL    );
 
 WITH CTE1 AS (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry'),
 CTE2 AS (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Division' and parentDepartmentId in (SELECT CTE1.departmentId FROM CTE1)),
 CTE3 AS (select distinct departmentId,parentDepartmentId from tbl_DepartmentMaster dm Where dm.departmentType='Organization')
 
 insert into #Temp_TenderCount(id)
 select convert(varchar(50),tenderId) as P from tbl_TenderDetails Where tenderStatus='approved'
and (
		departmentId in (select departmentId from CTE1)
		Or
		departmentId in (select departmentId from CTE2 )
		Or
		departmentId in (select departmentId from CTE3 WHERE parentDepartmentId in (select distinct departmentId from CTE1) )
		Or
		departmentId in (select departmentId from CTE3 WHERE parentDepartmentId in (select distinct departmentId from CTE2) )
	) 
---- Published
 select @Field1 = count(id) from #Temp_TenderCount
 ---- Processed
 select @Field2 = count(id) from #Temp_TenderCount where id Not In (select distinct tenderId from tbl_NoaIssueDetails nid
							INNER JOIN tbl_NoaAcceptance na on na.noaIssueId = nid.noaIssueId where acceptRejStatus ='approved')
--Finalized
 select @Field3 = count(id) from #Temp_TenderCount where id In (select distinct tenderId from tbl_NoaIssueDetails NID
					INNER JOIN tbl_ContractSign cs ON cs.noaId = NID.noaIssueId)
truncate table #Temp_TenderCount


insert into #Temp_TenderCount(id)
select convert(varchar(50),tenderId) from tbl_TenderDetails Where tenderStatus='approved'
and (
		departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='division')
		Or
		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='division') )
	)
---- Published
select @Field4 = count(id) from #Temp_TenderCount
---- Processed
select @Field5 = count(id) from #Temp_TenderCount where id not in (select distinct tenderId from tbl_NoaIssueDetails nid
							INNER JOIN tbl_NoaAcceptance na on na.noaIssueId = nid.noaIssueId where acceptRejStatus ='approved')
--Finalized
select @Field6 = count(id) from #Temp_TenderCount where id In (select distinct tenderId from tbl_NoaIssueDetails NID
					INNER JOIN tbl_ContractSign cs ON cs.noaId = NID.noaIssueId)
truncate table #Temp_TenderCount

insert into #Temp_TenderCount(id)
select convert(varchar(50),tenderId) from tbl_TenderDetails Where tenderStatus='approved'
and departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Organization')
---- Published
select @Field7 = count(id) from #Temp_TenderCount
---- Processed
select @Field8 = count(id) from #Temp_TenderCount where id Not In (select distinct tenderId from tbl_NoaIssueDetails nid
							INNER JOIN tbl_NoaAcceptance na on na.noaIssueId = nid.noaIssueId where acceptRejStatus ='approved')
---- Finalized
select @Field9 = count(id) from #Temp_TenderCount where id In (select distinct tenderId from tbl_NoaIssueDetails NID
					INNER JOIN tbl_ContractSign cs ON cs.noaId = NID.noaIssueId AND isPubAggOnWeb = 'yes') 
drop table  #Temp_TenderCount
--Dohatec End

Select @Field1 as FieldValue1,
 @Field2 as FieldValue2,
 @Field3 as FieldValue3,
 @Field4 as FieldValue4,
 @Field5 as FieldValue5,
 @Field6 as FieldValue6,
 @Field7 as FieldValue7,
 @Field8 as FieldValue8,
 @Field9 as FieldValue9

END

IF @v_fieldName1Vc = 'GetMDOTenderCountList' -- View [Ministry / Division / Organization] specific [processed / published / Finalized]  Tenders count


BEGIN
	If @v_fieldName2Vc = 'Organization'
	Begin
		if @v_fieldName3Vc = 'Published'
		begin
			select distinct convert(varchar(50),count(departmentId))  as FieldValue1, departmentName as FieldValue2,
			(Select convert(varchar(50),count(tenderId))
				from tbl_TenderDetails
				Where tenderStatus='approved'
				and departmentId =dm.departmentId
			) as FieldValue3
			from tbl_DepartmentMaster dm
			Where departmentType='Organization'
			Group By departmentId, departmentName
			Order by departmentName
		end
		else if @v_fieldName3Vc = 'Processed'
		begin
			select distinct convert(varchar(50),count(departmentId)) as FieldValue1, departmentName as FieldValue2,
			(Select convert(varchar(50),count(tenderId))
				from tbl_TenderDetails
				Where tenderStatus='approved'
				and departmentId =dm.departmentId
			--and tenderId Not In (select distinct tenderId from tbl_NoaIssueDetails)
			 and tenderId Not In (select distinct tenderId from tbl_NoaIssueDetails nid inner join tbl_NoaAcceptance na on na.noaIssueId = nid.noaIssueId where acceptRejStatus ='approved')
			) as FieldValue3
			from tbl_DepartmentMaster dm
			Where departmentType='Organization'
			Group By departmentId, departmentName
			Order by departmentName
		end
		else if @v_fieldName3Vc = 'Finalized'
		begin
			select distinct convert(varchar(50),count(departmentId)) as FieldValue1, departmentName as FieldValue2,
			(Select convert(varchar(50),count(tenderId))
				from tbl_TenderDetails
				Where tenderStatus='approved'
				and departmentId =dm.departmentId
				and tenderId In (select distinct tenderId from tbl_NoaIssueDetails  NID
					INNER JOIN tbl_ContractSign cs ON cs.noaId = NID.noaIssueId)
			) as FieldValue3
			from tbl_DepartmentMaster dm
			Where departmentType='Organization'
			Group By departmentId, departmentName
			Order by departmentName
		end
	End
	Else If @v_fieldName2Vc = 'Division'
	Begin
		if @v_fieldName3Vc = 'Published'
		begin
			select distinct convert(varchar(50),count(departmentId)) as FieldValue1, departmentName as FieldValue2,
			(Select convert(varchar(50),count(tenderId))
				from tbl_TenderDetails
				Where tenderStatus='approved'
				and (
						departmentId = dm.departmentId
						Or
						departmentId in (select distinct departmentId from tbl_DepartmentMaster a Where a.departmentType='Organization' and parentDepartmentId=dm.departmentId)
					)
				--and tenderId Not In (select distinct tenderId from tbl_NoaIssueDetails)

			) as FieldValue3
			from tbl_DepartmentMaster dm
			Where departmentType='Division'
			Group By departmentId, departmentName
			Order by departmentName
		end
		else if @v_fieldName3Vc = 'Processed'
		begin
			select distinct convert(varchar(50),count(departmentId)) as FieldValue1, departmentName as FieldValue2,
			(Select convert(varchar(50),count(tenderId))
				from tbl_TenderDetails
				Where tenderStatus='approved'
				and (
						departmentId = dm.departmentId
						Or
						departmentId in (select distinct departmentId from tbl_DepartmentMaster a Where a.departmentType='Organization' and parentDepartmentId=dm.departmentId)
					)
				--and tenderId Not In (select distinct tenderId from tbl_NoaIssueDetails)
				and tenderId Not In (select distinct tenderId from tbl_NoaIssueDetails nid inner join tbl_NoaAcceptance na on na.noaIssueId = nid.noaIssueId where acceptRejStatus ='approved')
			) as FieldValue3
			from tbl_DepartmentMaster dm
			Where departmentType='Division'
			Group By departmentId, departmentName
			Order by departmentName
		end
		else if @v_fieldName3Vc = 'Finalized'
		begin
			select distinct convert(varchar(50),count(departmentId)) as FieldValue1, departmentName as FieldValue2,
			(Select convert(varchar(50),count(tenderId))
				from tbl_TenderDetails
				Where tenderStatus='approved'
				and (
						departmentId = dm.departmentId
						Or
						departmentId in (select distinct departmentId from tbl_DepartmentMaster a Where a.departmentType='Organization' and parentDepartmentId=dm.departmentId)
					)
				and tenderId In (select distinct tenderId from tbl_NoaIssueDetails  NID
					INNER JOIN tbl_ContractSign cs ON cs.noaId = NID.noaIssueId)

			) as FieldValue3
			from tbl_DepartmentMaster dm
			Where departmentType='Division'
			Group By departmentId, departmentName
			Order by departmentName
		end
	End
	Else If @v_fieldName2Vc = 'Ministry'
	Begin
		if @v_fieldName3Vc = 'Published'
		begin
			select distinct convert(varchar(50),count(departmentId)) as FieldValue1, departmentName as FieldValue2,
			(Select convert(varchar(50),count(tenderId))
				from tbl_TenderDetails
				Where tenderStatus='approved'
				and (
						departmentId = dm.departmentId
						Or
						departmentId in (select distinct departmentId from tbl_DepartmentMaster a Where a.departmentType in ('Division','Organization') and parentDepartmentId=dm.departmentId)
						Or
						departmentId in (select distinct departmentId from tbl_DepartmentMaster a Where a.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster b Where b.departmentType in ('Division') and parentDepartmentId=dm.departmentId))
					)
			) as FieldValue3
			from tbl_DepartmentMaster dm
			Where departmentType='Ministry'
			Group By departmentId, departmentName
			Order by departmentName
		end
		else if @v_fieldName3Vc = 'Processed'
		begin
			select distinct convert(varchar(50),count(departmentId)) as FieldValue1, departmentName as FieldValue2,
			(Select convert(varchar(50),count(tenderId))
				from tbl_TenderDetails
				Where tenderStatus='approved'
				and (
						departmentId = dm.departmentId
						Or
						departmentId in (select distinct departmentId from tbl_DepartmentMaster a Where a.departmentType in ('Division','Organization') and parentDepartmentId=dm.departmentId)
						Or
						departmentId in (select distinct departmentId from tbl_DepartmentMaster a Where a.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster b Where b.departmentType in ('Division') and parentDepartmentId=dm.departmentId))
					)
				--and tenderId Not In (select distinct tenderId from tbl_NoaIssueDetails)
				and tenderId Not In (select distinct tenderId from tbl_NoaIssueDetails nid inner join tbl_NoaAcceptance na on na.noaIssueId = nid.noaIssueId where acceptRejStatus ='approved')
			) as FieldValue3
			from tbl_DepartmentMaster dm
			Where departmentType='Ministry'
			Group By departmentId, departmentName
			Order by departmentName
		end
		else if @v_fieldName3Vc = 'Finalized'
		begin
			select distinct convert(varchar(50),count(departmentId)) as FieldValue1, departmentName as FieldValue2,
			(Select convert(varchar(50),count(tenderId))
				from tbl_TenderDetails
				Where tenderStatus='approved'
				and (
						departmentId = dm.departmentId
						Or
						departmentId in (select distinct departmentId from tbl_DepartmentMaster a Where a.departmentType in ('Division','Organization') and parentDepartmentId=dm.departmentId)
						Or
						departmentId in (select distinct departmentId from tbl_DepartmentMaster a Where a.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster b Where b.departmentType in ('Division') and parentDepartmentId=dm.departmentId))
					)
				and tenderId In (select distinct tenderId from tbl_NoaIssueDetails  NID
					INNER JOIN tbl_ContractSign cs ON cs.noaId = NID.noaIssueId)
			) as FieldValue3
			from tbl_DepartmentMaster dm
			Where departmentType='Ministry'
			Group By departmentId, departmentName
			Order by departmentName
		end
	End
END

IF @v_fieldName1Vc = 'GetAppCount' -- Annual Procurement Plans
BEGIN
select @Field1 = convert(varchar(50),count(appId)) from tbl_AppMaster Where appStatus='pending'
select @Field2 = convert(varchar(50),count(appId)) from tbl_AppMaster Where appStatus='approved'

Select @Field1 as FieldValue1,
 @Field2 as FieldValue2
END


IF @v_fieldName1Vc = 'GetMISTwo'
BEGIN

-- // National Tenderer/Consultant Count
--select @Field1 =  convert(varchar(50),count(userId)) from tbl_LoginMaster where status='Approved' and userTyperId=2
--and registrationType='contractor' and nationality='Bhutani'
/*
SELECT	@Field1 =  convert(varchar(50),count(LM.userId))
FROM	tbl_CompanyMaster CM
		INNER JOIN tbl_TendererMaster TM ON TM.companyId = CM.companyId
		INNER JOIN tbl_LoginMaster LM ON LM.userId = TM.userId
					AND registrationType='contractor'
					AND status='Approved'
					AND userTyperId=2
WHERE	CM.regOffCountry = 'Bhutan'
*/
SELECT	@Field1 = convert(varchar(50),count(LM.userId))
FROM	tbl_CompanyMaster CM
		INNER JOIN tbl_LoginMaster LM ON LM.userId = CM.userId
					AND registrationType='contractor'
					AND status='Approved'
					AND userTyperId=2
					AND isJvca = 'no'
		INNER JOIN tbl_TendererMaster TM ON TM.userId = LM.userId
					AND tm.companyId = CM.companyId
WHERE	CM.regOffCountry = 'Bhutan'

-- // International Tenderer/Consultant Count
--select @Field2 =  convert(varchar(50),count(userId)) from tbl_LoginMaster where status='Approved' and userTyperId=2
--and registrationType='contractor' and nationality<>'Bhutani'
/*
SELECT	@Field2 =  convert(varchar(50),count(LM.userId))
FROM	tbl_CompanyMaster CM
		INNER JOIN tbl_TendererMaster TM ON TM.companyId = CM.companyId
		INNER JOIN tbl_LoginMaster LM ON LM.userId = TM.userId
					AND registrationType='contractor'
					AND status='Approved'
					AND userTyperId=2
WHERE	CM.regOffCountry <> 'Bhutan'
*/

SELECT	@Field2 = convert(varchar(50),count(LM.userId))
FROM	tbl_CompanyMaster CM
		INNER JOIN tbl_LoginMaster LM ON LM.userId = CM.userId
					AND registrationType='contractor'
					AND status='Approved'
					AND userTyperId=2
					AND isJvca = 'no'
		INNER JOIN tbl_TendererMaster TM ON TM.userId = LM.userId
					AND tm.companyId = CM.companyId
WHERE	CM.regOffCountry <> 'Bhutan'

-- // National Individual Consultant Count
select @Field3 =  convert(varchar(50),count(userId)) from tbl_LoginMaster where status='Approved' and userTyperId=2
and registrationType='individualconsultant' and nationality='Bhutani' AND isJvca = 'no'

-- // International Individual Consultant Count
select @Field4 =  convert(varchar(50),count(userId)) from tbl_LoginMaster where status='Approved' and userTyperId=2
and registrationType='individualconsultant' and nationality<>'Bhutani' AND isJvca = 'no'

-- // Ministry/Division/Organization Count With PE Offices Count
select @Field5 = convert(varchar(50),count(departmentId)) from tbl_DepartmentMaster Where departmentType='Ministry'
select @Field6 = convert(varchar(50), count(officeId) ) from tbl_OfficeMaster where departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry')
select @Field7 = convert(varchar(50),count(departmentId)) from tbl_DepartmentMaster Where departmentType='Division'
select @Field8 = convert(varchar(50), count(officeId)) from tbl_OfficeMaster where departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Division')
select @Field9 = convert(varchar(50),count(departmentId)) from tbl_DepartmentMaster Where departmentType='Organization'
select @Field10 = convert(varchar(50),count(officeId)) from tbl_OfficeMaster where departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Organization')

---- // Annual Procurement Plans (Tenders) Count
----select @Field10 = convert(varchar(50),count(tenderId)) from tbl_TenderDetails Where tenderStatus='pending'
----select @Field11 = convert(varchar(50),count(tenderId)) from tbl_TenderDetails Where tenderStatus='approved
--select @Field10 = convert(varchar(50),count(appId)) from tbl_AppMaster Where appStatus='pending'
--select @Field11 = convert(varchar(50),count(appId)) from tbl_AppMaster Where appStatus='approved'

---- // Annual Procurement Plans (Tenders) Count Published By Ministry
---- Published
--select @Field12 = convert(varchar(50),count(tenderId)) from tbl_TenderDetails Where tenderStatus='approved'
--and (
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry')
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Division' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry') )
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry') )
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Division' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry')) )
--	)

---- Processed
--select @Field13 = convert(varchar(50),count(tenderId)) from tbl_TenderDetails Where tenderStatus='approved'
--and (
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry')
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Division' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry') )
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry') )
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Division' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry')) )
--	)
--and tenderId Not In (select distinct tenderId from tbl_NoaIssueDetails)

---- Finalized
--select @Field14 = convert(varchar(50),count(tenderId)) from tbl_TenderDetails Where tenderStatus='approved'
--and (
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry')
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Division' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry') )
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry') )
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Division' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Ministry')) )
--	)
--and tenderId In (select distinct tenderId from tbl_NoaIssueDetails)

---- // Annual Procurement Plans (Tenders) Count Published By Division
---- Published
--select @Field15 = convert(varchar(50),count(tenderId)) from tbl_TenderDetails Where tenderStatus='approved'
--and (
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='division')
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='division') )
--	)

---- Processed
--select @Field16 = convert(varchar(50),count(tenderId)) from tbl_TenderDetails Where tenderStatus='approved'
--and (
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='division')
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='division') )
--	)
--and tenderId Not In (select distinct tenderId from tbl_NoaIssueDetails)

---- Finalized
--select @Field17 = convert(varchar(50),count(tenderId)) from tbl_TenderDetails Where tenderStatus='approved'
--and (
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='division')
--		Or
--		departmentId in (select distinct departmentId from tbl_DepartmentMaster dm Where dm.departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='division') )
--	)
--and tenderId In (select distinct tenderId from tbl_NoaIssueDetails)

---- // Annual Procurement Plans (Tenders) Count Published By Organization
--select @Field18 = convert(varchar(50),count(tenderId)) from tbl_TenderDetails Where tenderStatus='approved' and departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Organization') -- Published
--select @Field19 = convert(varchar(50),count(tenderId)) from tbl_TenderDetails Where tenderStatus='approved' and departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Organization') and tenderId Not In (select distinct tenderId from tbl_NoaIssueDetails)-- Processed
--select @Field20 = convert(varchar(50),count(tenderId)) from tbl_TenderDetails Where tenderStatus='approved' and departmentId in (select distinct departmentId from tbl_DepartmentMaster Where departmentType='Organization') and tenderId In (select distinct tenderId from tbl_NoaIssueDetails) -- Finalized

Select @Field1 as FieldValue1,
 @Field2 as FieldValue2,
 @Field3 as FieldValue3,
 @Field4 as FieldValue4,
 @Field5 as FieldValue5,
 @Field6 as FieldValue6,
 @Field7 as FieldValue7,
 @Field8 as FieldValue8,
 @Field9 as FieldValue9,
 @Field10 as FieldValue10
 --@Field11 as FieldValue11,
 --@Field12 as FieldValue12,
 --@Field13 as FieldValue13,
 --@Field14 as FieldValue14,
 --@Field15 as FieldValue15,
 --@Field16 as FieldValue16,
 --@Field17 as FieldValue17,
 --@Field18 as FieldValue18,
 --@Field19 as FieldValue19,
 --@Field20 as FieldValue20

END


IF @v_fieldName1Vc = 'getTodaysRegUserListing' -- for
BEGIN

	--Declare @v_BankUserRole_Vc varchar(50), @v_BankBranchId int, @v_BankId int

	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName2Vc, @v_RecordPerPage_inInt=@v_fieldName3Vc

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */


		SET @v_ConditionString_Vc=' And isLive=''yes'''
	Select @v_BankUserRole_Vc=isMakerChecker,
		   @v_BankBranchId = sBankDevelopId
	From tbl_PartnerAdmin Where userId=@v_fieldName4Vc

	Select @v_BankId = Case sBankDevelHeadId
	When 0 Then @v_BankBranchId Else sBankDevelHeadId End
	From tbl_ScBankDevPartnerMaster Where sBankDevelopId=@v_BankBranchId


	If @v_BankUserRole_Vc='BranchChecker'
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And regPaymentId in (Select regPaymentId From tbl_RegFeePayment Where createdBy in (select distinct userId from tbl_PartnerAdmin where sBankDevelopId= '+Convert(varchar(50), @v_BankBranchId)+'))'
	End
	Else If @v_BankUserRole_Vc='BankChecker'
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And regPaymentId in (Select regPaymentId From tbl_RegFeePayment Where createdBy in (select distinct userId from tbl_PartnerAdmin
		where (
				sBankDevelopId= '+Convert(varchar(50), @v_BankBranchId)+
				' OR
				sBankDevelopId in (select distinct sBankDevelopId from tbl_ScBankDevPartnerMaster Where sBankDevelHeadId='+Convert(varchar(50), @v_BankId)+')
			)
		))'
	End
	Else If @v_BankUserRole_Vc='BranchMaker'
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And createdBy = ' + @v_fieldName4Vc
	End

	If @v_fieldName5Vc<>''
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And emailId=''' + @v_fieldName5Vc +''''
	End

	If @v_fieldName6Vc<>'' -- Verified status
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And isVerified=''' + @v_fieldName6Vc +''''
	End

	If @v_fieldName7Vc<>'' -- Branch Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And regPaymentId in (Select regPaymentId From tbl_RegFeePayment Where createdBy in (select distinct userId from tbl_PartnerAdmin where sBankDevelopId='+@v_fieldName7Vc+'))'
	End

	If @v_fieldName8Vc<>'' -- Branch Member Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And createdBy=''' + @v_fieldName8Vc +''''
	End

	Print (@v_fieldName9Vc)
	Print (@v_fieldName10Vc)

	If (@v_fieldName9Vc='0' And @v_fieldName10Vc='0')-- By default show for todays records
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) = Cast (Floor(Cast (Getdate() as Float)) as Datetime)'
	End
	Else IF (@v_fieldName9Vc is not null And @v_fieldName9Vc <>'') And (@v_fieldName10Vc is not null And @v_fieldName10Vc <>'')
	Begin
		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) >= ''' + @v_fieldName9Vc + ''' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) <= ''' + @v_fieldName10Vc + ''''
	End
	ELSE IF (@v_fieldName9Vc is not null And @v_fieldName9Vc <>'') And (@v_fieldName10Vc is null OR @v_fieldName10Vc='')
	Begin
		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) >= ''' + @v_fieldName9Vc + ''''
	End
	ELSE IF (@v_fieldName9Vc is not null Or @v_fieldName9Vc <>'') And (@v_fieldName10Vc is not null And @v_fieldName10Vc <>'')
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) <= ''' + @v_fieldName10Vc + ''''
	End



	Print(@v_ConditionString_Vc)

	-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
	Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT( Distinct regPaymentId ) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')
	From tbl_RegFeePayment RP
	Inner Join tbl_LoginMaster LM On RP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
		WHERE regPaymentId is Not Null ' + @v_ConditionString_Vc

	Select @v_CntQry_Vc='SELECT Count(Distinct regPaymentId)
	From tbl_RegFeePayment RP
	Inner Join tbl_LoginMaster LM On RP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
		WHERE regPaymentId is Not Null ' + @v_ConditionString_Vc

	Print ('Count Query')
	Print(@v_CntQry_Vc)

			/* START CODE: DYNAMIC QUERY TO GET RECORDS */

			/*
				FieldValue1: Payment Id
				FieldValue2: User Id of Bidder for which Payment is done
				FieldValue3: Email Id of Bidder for which Payment is done
				FieldValue4: Branch Maker - Name of Bank User who has done the Payment
				FieldValue5: Payment Status New Payment/Renewal
				FieldValue6: Date of Payment
			*/

		Select @v_FinalQueryVc=
	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');

	Select FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6,FieldValue7, FieldValue8, FieldValue9, FieldValue10
	From
	(Select Distinct
		Convert(varchar(50),Tmp.regPaymentId) as FieldValue1,
			Convert(varchar(50),LM.userId) as FieldValue2,
			LM.emailId as FieldValue3,
			dbo.f_GovUserName(RP.partTransId, ''tbl_PartnerAdminTransfer'') as FieldValue4,
			Case RP.status When ''paid'' Then ''New Registration ''
						   When ''renewed'' Then ''Renewal''
						   When ''freezed'' Then ''New Registration ''
				 End as FieldValue5,
			REPLACE(CONVERT(VARCHAR(11),dtOfPayment, 106), '' '', ''-'')+ '' '' + Substring(CONVERT(VARCHAR(30),dtOfPayment,108),1,5) as FieldValue6,
			CONVERT(varchar(50),@v_TotalPageCnt_Int) as FieldValue7,
			CONVERT(varchar(50),@v_TotalRecordCnt_Int) as FieldValue8,
			CONVERT(varchar(50),Tmp.RowNumber) as FieldValue9,
			isVerified as FieldValue10,
			Tmp.regPaymentId,
			sortParam
		From
		(Select RowNumber, regPaymentId, sortParam From
			(SELECT ROW_NUMBER() Over (Order by '+@v_fieldName11Vc+ ' ' +@v_fieldName12Vc+') as RowNumber, regPaymentId, ' +@v_fieldName11Vc+' as sortParam
				From tbl_RegFeePayment RP
				Inner Join tbl_LoginMaster LM On RP.userId=LM.userId
				Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
		WHERE regPaymentId is Not Null '
					+@v_ConditionString_Vc+
			') B

		WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+ ' ) Tmp
		Inner Join tbl_RegFeePayment RP On Tmp.regPaymentId=RP.regPaymentId
			Inner Join tbl_LoginMaster LM On RP.userId=LM.userId
			Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
		) as A Order By sortParam '+@v_fieldName12Vc+'
		--Order by A.regPaymentId desc
		'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */


		PRINT(@v_FinalQueryVc)
		Exec (@v_FinalQueryVc)

END


IF @v_fieldName1Vc = 'getTodaysVerifiedRegUserListing' -- for
BEGIN

	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName2Vc, @v_RecordPerPage_inInt=@v_fieldName3Vc

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */


	SET @v_ConditionString_Vc=' And isLive=''yes'''

	Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And Cast (Floor(Cast (verificationDt as Float)) as Datetime) = Cast (Floor(Cast (Getdate() as Float)) as Datetime)'

	Select @v_BankUserRole_Vc=isMakerChecker,
		   @v_BankBranchId = sBankDevelopId
	From tbl_PartnerAdmin Where userId=@v_fieldName4Vc

	Select @v_BankId = Case sBankDevelHeadId
	When 0 Then @v_BankBranchId Else sBankDevelHeadId End
	From tbl_ScBankDevPartnerMaster Where sBankDevelopId=@v_BankBranchId


	If @v_BankUserRole_Vc='BranchChecker'
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And verifiedBy = ' + @v_fieldName4Vc
	End


	Print(@v_ConditionString_Vc)

	-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
	Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT( Distinct regPaymentId ) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')
	From tbl_RegFeePayment RP
	Inner Join tbl_LoginMaster LM On RP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
	Inner Join tbl_PaymentVerification PV On RP.regPaymentId=PV.paymentId
		WHERE regPaymentId is Not Null ' + @v_ConditionString_Vc

	Select @v_CntQry_Vc='SELECT Count(Distinct regPaymentId)
	From tbl_RegFeePayment RP
	Inner Join tbl_LoginMaster LM On RP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
	Inner Join tbl_PaymentVerification PV On RP.regPaymentId=PV.paymentId
		WHERE regPaymentId is Not Null ' + @v_ConditionString_Vc

	Print ('Count Query')
	Print(@v_CntQry_Vc)

			/* START CODE: DYNAMIC QUERY TO GET RECORDS */

			/*
				FieldValue1: Payment Id
				FieldValue2: User Id of Bidder for which Payment is done
				FieldValue3: Email Id of Bidder for which Payment is done
				FieldValue4: Branch Maker - Name of Bank User who has done the Payment
				FieldValue5: Payment Status New Payment/Renewal
				FieldValue6: Date of Payment
			*/

		Select @v_FinalQueryVc=
	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');

	Select FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6,FieldValue7, FieldValue8, FieldValue9, FieldValue10
	From
	(Select Distinct
		Convert(varchar(50),Tmp.regPaymentId) as FieldValue1,
			Convert(varchar(50),LM.userId) as FieldValue2,
			LM.emailId as FieldValue3,
			dbo.f_GovUserName(RP.partTransId, ''tbl_PartnerAdminTransfer'') as FieldValue4,
			Case RP.status When ''paid'' Then ''New Registration ''
						   When ''renewed'' Then ''Renewal''
				 End as FieldValue5,
			REPLACE(CONVERT(VARCHAR(11),dtOfPayment, 106), '' '', ''-'')+ '' '' + Substring(CONVERT(VARCHAR(30),dtOfPayment,108),1,5) as FieldValue6,
			CONVERT(varchar(50),@v_TotalPageCnt_Int) as FieldValue7,
			CONVERT(varchar(50),@v_TotalRecordCnt_Int) as FieldValue8,
			CONVERT(varchar(50),Tmp.RowNumber) as FieldValue9,
			isVerified as FieldValue10,
			Tmp.regPaymentId,
			sortParam
		From
		(Select RowNumber, regPaymentId, sortParam From
			(SELECT ROW_NUMBER() Over (Order by '+@v_fieldName5Vc+ ' ' +@v_fieldName6Vc+') as RowNumber, regPaymentId, ' +@v_fieldName5Vc+' as sortParam
				From tbl_RegFeePayment RP
				Inner Join tbl_LoginMaster LM On RP.userId=LM.userId
				Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
				Inner Join tbl_PaymentVerification PV On RP.regPaymentId=PV.paymentId
		WHERE regPaymentId is Not Null '
					+@v_ConditionString_Vc+
			') B

		WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+ ' ) Tmp
		Inner Join tbl_RegFeePayment RP On Tmp.regPaymentId=RP.regPaymentId
			Inner Join tbl_LoginMaster LM On RP.userId=LM.userId
			Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
			Inner Join tbl_PaymentVerification PV On RP.regPaymentId=PV.paymentId

		) as A Order By sortParam '+@v_fieldName6Vc+'
		--Order by A.regPaymentId desc
		'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */


		PRINT(@v_FinalQueryVc)
		Exec (@v_FinalQueryVc)

END


IF @v_fieldName1Vc = 'getTodaysVerifiedTenPaymentListing'
BEGIN
	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName2Vc, @v_RecordPerPage_inInt=@v_fieldName3Vc

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */


	SET @v_ConditionString_Vc=' And isLive=''yes'''

	Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And Cast (Floor(Cast (verificationDt as Float)) as Datetime) = Cast (Floor(Cast (Getdate() as Float)) as Datetime)'

	Select @v_BankUserRole_Vc=isMakerChecker,
		   @v_BankBranchId = sBankDevelopId
	From tbl_PartnerAdmin Where userId=@v_fieldName4Vc

	Select @v_BankId = Case sBankDevelHeadId
	When 0 Then @v_BankBranchId Else sBankDevelHeadId End
	From tbl_ScBankDevPartnerMaster Where sBankDevelopId=@v_BankBranchId


	If @v_BankUserRole_Vc='BranchChecker'
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And verifiedBy = ' + @v_fieldName4Vc
	End


	Print('Condition String: ')
	Print(@v_ConditionString_Vc)

	-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
	Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT( Distinct tenderPaymentId ) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')
	From tbl_TenderPayment TP
	Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
	Inner Join tbl_TenderPaymentVerification TPV On TP.tenderPaymentId=TPV.paymentId
		WHERE tenderPaymentId is Not Null ' + @v_ConditionString_Vc

	Select @v_CntQry_Vc='SELECT Count(Distinct tenderPaymentId)
	From tbl_TenderPayment TP
	Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
	Inner Join tbl_TenderPaymentVerification TPV On TP.tenderPaymentId=TPV.paymentId
		WHERE tenderPaymentId is Not Null ' + @v_ConditionString_Vc


			/* START CODE: DYNAMIC QUERY TO GET RECORDS */

			/*
				FieldValue1: Payment Id
				FieldValue2: User Id of Bidder for which Payment is done
				FieldValue3: Email Id of Bidder for which Payment is done
				FieldValue4: Branch Maker - Name of Bank User who has done the Payment
				FieldValue5: Payment Status New Payment/Renewal
				FieldValue6: Date of Payment
			*/

	Print ('PageCount')
	Print (@v_PgCntQry_Vc)

	Print ('RowCount')
	Print (@v_CntQry_Vc)

		Select @v_FinalQueryVc=
	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');

	Select FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6,FieldValue7, FieldValue8,
	FieldValue9, FieldValue10, FieldValue11, FieldValue12, FieldValue13, FieldValue14, FieldValue15, FieldValue16,
	FieldValue17
	From
	(Select Distinct
		Convert(varchar(50),Tmp.tenderPaymentId) as FieldValue1,
			Convert(varchar(50),LM.userId) as FieldValue2,
			LM.emailId as FieldValue3,
			dbo.f_GovUserName(TP.partTransId, ''tbl_PartnerAdminTransfer'') as FieldValue4,
			TP.status as FieldValue5,
			REPLACE(CONVERT(VARCHAR(11),createdDate, 106), '' '', ''-'')+ '' '' + Substring(CONVERT(VARCHAR(30),createdDate,108),1,5) as FieldValue6,
			CONVERT(varchar(50),@v_TotalPageCnt_Int) as FieldValue7,
			CONVERT(varchar(50),@v_TotalRecordCnt_Int) as FieldValue8,
			CONVERT(varchar(50),Tmp.RowNumber) as FieldValue9,
			isVerified as FieldValue10,
			dbo.f_getbiddercompany(LM.userId) as FieldValue11,
			paymentFor as FieldValue12,
			Convert(varchar(50),amount) as FieldValue13,
			Convert(varchar(50),tenderId) as FieldValue14,
			Convert(varchar(50),pkgLotId) as FieldValue15,
			Case paymentFor
				When ''Document Fees'' Then ''df''
				When ''Tender Security'' Then ''ts''
				When ''Performance Security'' Then ''ps''
				End as FieldValue16,
			IsNull((select top 1 ('' ,<br/> '' + lotNo) from tbl_TenderLotSecurity where tenderId=TP.tenderId and appPkgLotId=TP.pkgLotId ),'''') as FieldValue17,
			Tmp.tenderPaymentId,
			sortParam
		From
		(Select RowNumber, tenderPaymentId, sortParam From
			(SELECT ROW_NUMBER() Over (Order by '+@v_fieldName5Vc+ ' ' +@v_fieldName6Vc+') as RowNumber, tenderPaymentId,' +@v_fieldName5Vc+' as sortParam
				From tbl_TenderPayment TP
				Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
				Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
				Inner Join tbl_TenderPaymentVerification TPV On TP.tenderPaymentId=TPV.paymentId
		WHERE tenderPaymentId is Not Null '
					+@v_ConditionString_Vc+
			') B

		WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+ ' ) Tmp
		Inner Join tbl_TenderPayment TP On Tmp.tenderPaymentId=TP.tenderPaymentId
			Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
			Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
			Inner Join tbl_TenderPaymentVerification TPV On TP.tenderPaymentId=TPV.paymentId
		) as A Order By sortParam '+@v_fieldName6Vc+'
		--Order by A.tenderPaymentId desc
		'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */


		PRINT(@v_FinalQueryVc)
		Exec (@v_FinalQueryVc)

END


   IF @v_fieldName1Vc = 'getTodaysTenderPaymentListing'
BEGIN
	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName2Vc, @v_RecordPerPage_inInt=@v_fieldName3Vc

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */


	SET @v_ConditionString_Vc=' And isLive=''yes'''
	Select @v_BankUserRole_Vc=isMakerChecker,
		   @v_BankBranchId = sBankDevelopId
	From tbl_PartnerAdmin Where userId=@v_fieldName4Vc

	Select @v_BankId = Case sBankDevelHeadId
	When 0 Then @v_BankBranchId Else sBankDevelHeadId End
	From tbl_ScBankDevPartnerMaster Where sBankDevelopId=@v_BankBranchId


	If @v_BankUserRole_Vc='BranchChecker'
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And tenderPaymentId in (Select tenderPaymentId From tbl_TenderPayment Where createdBy in (select distinct userId from tbl_PartnerAdmin where sBankDevelopId= '+Convert(varchar(50), @v_BankBranchId)+'))'
	End
	Else If @v_BankUserRole_Vc='BankChecker'
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And tenderPaymentId in (Select tenderPaymentId From tbl_TenderPayment Where createdBy in (select distinct userId from tbl_PartnerAdmin where sBankDevelopId in (select distinct sBankDevelopId from tbl_ScBankDevPartnerMaster Where sBankDevelHeadId='+Convert(varchar(50), @v_BankId)+')))'
	End
	Else If @v_BankUserRole_Vc='BranchMaker'
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And createdBy = ' + @v_fieldName4Vc
	End

	If @v_fieldName5Vc<>''
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And emailId=''' + @v_fieldName5Vc +''''
	End

	If @v_fieldName6Vc<>'' -- Verified status
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And isVerified=''' + @v_fieldName6Vc +''''
	End

	If @v_fieldName7Vc<>'' -- Branch Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderPaymentId in (Select tenderPaymentId From tbl_TenderPayment Where createdBy in (select distinct userId from tbl_PartnerAdmin where sBankDevelopId='+@v_fieldName7Vc+'))'
	End

	If @v_fieldName8Vc<>'' -- Branch Member Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And createdBy=''' + @v_fieldName8Vc +''''
	End

	--Print (@v_fieldName9Vc)
	--Print (@v_fieldName10Vc)

	IF  (@v_fieldName9Vc='0' And @v_fieldName10Vc='0')-- By default show for todays records
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And Cast (Floor(Cast (createdDate as Float)) as Datetime) = Cast (Floor(Cast (Getdate() as Float)) as Datetime)'
	End

	ELSE IF (@v_fieldName9Vc is not null And @v_fieldName9Vc <>'') And (@v_fieldName10Vc is not null And @v_fieldName10Vc <>'')
	Begin
		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (createdDate as Float)) as Datetime) >= ''' + @v_fieldName9Vc + ''' And Cast (Floor(Cast (createdDate as Float)) as Datetime) <= ''' + @v_fieldName10Vc + ''''
	End
	ELSE IF (@v_fieldName9Vc is not null And @v_fieldName9Vc <>'') And (@v_fieldName10Vc is null OR @v_fieldName10Vc='')
	Begin
		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (createdDate as Float)) as Datetime) >= ''' + @v_fieldName9Vc + ''''
	End
	ELSE IF (@v_fieldName9Vc is not null Or @v_fieldName9Vc <>'') And (@v_fieldName10Vc is not null And @v_fieldName10Vc <>'')
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And Cast (Floor(Cast (createdDate as Float)) as Datetime) <= ''' + @v_fieldName10Vc + ''''
	End


	If @v_fieldName11Vc is Not Null And @v_fieldName11Vc<>'' -- Tender Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderId=''' + @v_fieldName11Vc +''''
	End

	If @v_fieldName12Vc is Not Null And  @v_fieldName12Vc<>'' -- Tenderer Name
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And dbo.f_getbiddercompany(LM.userId)=''' + @v_fieldName12Vc +''''
	End

	If @v_fieldName13Vc is Not Null And  @v_fieldName13Vc<>'' And @v_fieldName13Vc<>'All' -- Payment For
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And paymentFor=''' + @v_fieldName13Vc +''''
	End

	Print('Condition String: ')
	Print(@v_ConditionString_Vc)

	-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
	Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT( Distinct tenderPaymentId ) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')
	From tbl_TenderPayment TP
	Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		WHERE tenderPaymentId is Not Null ' + @v_ConditionString_Vc

	Select @v_CntQry_Vc='SELECT Count(Distinct tenderPaymentId)
	From tbl_TenderPayment TP
	Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		WHERE tenderPaymentId is Not Null ' + @v_ConditionString_Vc


			/* START CODE: DYNAMIC QUERY TO GET RECORDS */

			/*
				FieldValue1: Payment Id
				FieldValue2: User Id of Bidder for which Payment is done
				FieldValue3: Email Id of Bidder for which Payment is done
				FieldValue4: Branch Maker - Name of Bank User who has done the Payment
				FieldValue5: Payment Status New Payment/Renewal
				FieldValue6: Date of Payment
			*/

	Print ('PageCount')
	Print (@v_PgCntQry_Vc)

	Print ('RowCount')
	Print (@v_CntQry_Vc)

		Select @v_FinalQueryVc=
	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');

	Select FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6,FieldValue7, FieldValue8,
	FieldValue9, FieldValue10, FieldValue11, FieldValue12, FieldValue13, FieldValue14, FieldValue15, FieldValue16
	From
	(Select Distinct
		Convert(varchar(50),Tmp.tenderPaymentId) as FieldValue1,
			Convert(varchar(50),LM.userId) as FieldValue2,
			LM.emailId as FieldValue3,
			dbo.f_GovUserName(TP.partTransId, ''tbl_PartnerAdminTransfer'') as FieldValue4,
			TP.status as FieldValue5,
			REPLACE(CONVERT(VARCHAR(11),createdDate, 106), '' '', ''-'')+ '' '' + Substring(CONVERT(VARCHAR(30),createdDate,108),1,5) as FieldValue6,
			CONVERT(varchar(50),@v_TotalPageCnt_Int) as FieldValue7,
			CONVERT(varchar(50),@v_TotalRecordCnt_Int) as FieldValue8,
			CONVERT(varchar(50),Tmp.RowNumber) as FieldValue9,
			isVerified as FieldValue10,
			dbo.f_getbiddercompany(LM.userId) as FieldValue11,
			paymentFor as FieldValue12,
			Convert(varchar(50),amount) as FieldValue13,
			Convert(varchar(50),tenderId) as FieldValue14,
			Convert(varchar(50),pkgLotId) as FieldValue15,
			Case paymentFor
				When ''Document Fees'' Then ''df''
				When ''Tender Security'' Then ''ts''
				When ''Performance Security'' Then ''ps''
				End as FieldValue16,
			Tmp.tenderPaymentId,
			sortParam
		From
		(Select RowNumber, tenderPaymentId, sortParam From
			(SELECT ROW_NUMBER() Over (Order by '+@v_fieldName14Vc+ ' ' +@v_fieldName15Vc+') as RowNumber, tenderPaymentId,' +@v_fieldName14Vc+' as sortParam
				From tbl_TenderPayment TP
				Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
				Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		WHERE tenderPaymentId is Not Null '
					+@v_ConditionString_Vc+
			') B

		WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+ ' ) Tmp
		Inner Join tbl_TenderPayment TP On Tmp.tenderPaymentId=TP.tenderPaymentId
			Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
			Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		) as A Order By sortParam '+@v_fieldName15Vc+'
		--Order by A.tenderPaymentId desc
		'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */


		PRINT(@v_FinalQueryVc)
		Exec (@v_FinalQueryVc)

END


IF @v_fieldName1Vc = 'getTodaysReceivedTenderPaymentListing'
BEGIN
	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName2Vc, @v_RecordPerPage_inInt=@v_fieldName3Vc

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */


	SET @v_ConditionString_Vc=' And isLive=''yes'''
	Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And Cast (Floor(Cast (createdDate as Float)) as Datetime) = Cast (Floor(Cast (Getdate() as Float)) as Datetime)'

	Select @v_BankUserRole_Vc=isMakerChecker,
		   @v_BankBranchId = sBankDevelopId
	From tbl_PartnerAdmin Where userId=@v_fieldName4Vc

	Select @v_BankId = Case sBankDevelHeadId
	When 0 Then @v_BankBranchId Else sBankDevelHeadId End
	From tbl_ScBankDevPartnerMaster Where sBankDevelopId=@v_BankBranchId

	If @v_BankUserRole_Vc='BranchMaker'
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And createdBy = ' + @v_fieldName4Vc
	End

	Print('Condition String: ')
	Print(@v_ConditionString_Vc)

	-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
	Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT( Distinct tenderPaymentId ) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')
	From tbl_TenderPayment TP
	Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		WHERE tenderPaymentId is Not Null ' + @v_ConditionString_Vc

	Select @v_CntQry_Vc='SELECT Count(Distinct tenderPaymentId)
	From tbl_TenderPayment TP
	Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		WHERE tenderPaymentId is Not Null ' + @v_ConditionString_Vc


			/* START CODE: DYNAMIC QUERY TO GET RECORDS */

			/*
				FieldValue1: Payment Id
				FieldValue2: User Id of Bidder for which Payment is done
				FieldValue3: Email Id of Bidder for which Payment is done
				FieldValue4: Branch Maker - Name of Bank User who has done the Payment
				FieldValue5: Payment Status New Payment/Renewal
				FieldValue6: Date of Payment
			*/

	Print ('PageCount')
	Print (@v_PgCntQry_Vc)

	Print ('RowCount')
	Print (@v_CntQry_Vc)

		Select @v_FinalQueryVc=
	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');

	Select FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6,FieldValue7, FieldValue8,
	FieldValue9, FieldValue10, FieldValue11, FieldValue12, FieldValue13, FieldValue14, FieldValue15, FieldValue16
	From
	(Select Distinct
		Convert(varchar(50),Tmp.tenderPaymentId) as FieldValue1,
			Convert(varchar(50),LM.userId) as FieldValue2,
			LM.emailId as FieldValue3,
			dbo.f_GovUserName(TP.partTransId, ''tbl_PartnerAdminTransfer'') as FieldValue4,
			TP.status as FieldValue5,
			REPLACE(CONVERT(VARCHAR(11),createdDate, 106), '' '', ''-'')+ '' '' + Substring(CONVERT(VARCHAR(30),createdDate,108),1,5) as FieldValue6,
			CONVERT(varchar(50),@v_TotalPageCnt_Int) as FieldValue7,
			CONVERT(varchar(50),@v_TotalRecordCnt_Int) as FieldValue8,
			CONVERT(varchar(50),Tmp.RowNumber) as FieldValue9,
			isVerified as FieldValue10,
			dbo.f_getbiddercompany(LM.userId) as FieldValue11,
			paymentFor as FieldValue12,
			Convert(varchar(50),amount) as FieldValue13,
			Convert(varchar(50),tenderId) as FieldValue14,
			Convert(varchar(50),pkgLotId) as FieldValue15,
			Case paymentFor
				When ''Document Fees'' Then ''df''
				When ''Tender Security'' Then ''ts''
				When ''Performance Security'' Then ''ps''
				End as FieldValue16,
			Tmp.tenderPaymentId,
			sortParam
		From
		(Select RowNumber, tenderPaymentId, sortParam From
			(SELECT ROW_NUMBER() Over (Order by '+@v_fieldName5Vc+ ' ' +@v_fieldName6Vc+') as RowNumber, tenderPaymentId,' +@v_fieldName5Vc+' as sortParam
				From tbl_TenderPayment TP
				Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
				Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		WHERE tenderPaymentId is Not Null '
					+@v_ConditionString_Vc+
			') B

		WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+ ' ) Tmp
		Inner Join tbl_TenderPayment TP On Tmp.tenderPaymentId=TP.tenderPaymentId
			Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
			Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		) as A Order By sortParam '+@v_fieldName6Vc+'
		'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */


		PRINT(@v_FinalQueryVc)
		Exec (@v_FinalQueryVc)

END


 IF @v_fieldName1Vc = 'getTOR2BidderList'
 BEGIN

 If @v_fieldName3Vc='0'
 Begin
	Select top 1 @v_fieldName3Vc = Convert(varchar(50), appPkgLotId) from tbl_TenderLotSecurity Where tenderId=@v_fieldName2Vc
 End

	IF (Select SUM(tenderSecurityAmt) from tbl_TenderLotSecurity where tenderId=@v_fieldName2Vc) > 0
Begin
	IF (Select docAvlMethod from tbl_TenderDetails where tenderId=@v_fieldName2Vc) ='Package'
	BEGIN
                if @v_fieldName3Vc in (select appPkgLotId from tbl_TenderLotSecurity where tenderId = @v_fieldName2Vc and BidSecurityType = 2)   
		   BEGIN
                        select
                                '0' as FieldValue1,
                                CONVERT(varchar(50), finalSubmissionId) as FieldValue2,
                                CONVERT(varchar(50), fs.userId) as FieldValue3,
                                dbo.f_getbiddercompany(fs.userId) as FieldValue4,
                                ISNULL(REPLACE(CONVERT(VARCHAR(11), fs.finalSubmissionDt , 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), fs.finalSubmissionDt ,108), 1, 5), '-') as FieldValue5,
                                convert(varchar(50),fs.userId) as FieldValue14,
                                convert(varchar(50),tm.tendererId) as FieldValue15,
                                convert(varchar(50),tm.companyId) as FieldValue16,
                                'Yes' as FieldValue17,
                                ISNULL(REPLACE(CONVERT(VARCHAR(11), bd.CreatedDate , 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), bd.CreatedDate ,108), 1, 5), '-') as FieldValue18

                                from tbl_FinalSubmission fs
                                Inner Join tbl_TenderDetails td On fs.tenderId=td.tenderId
                                Inner Join tbl_TendererMaster tm On tm.userId=fs.userId
                                Inner Join tbl_BIdDeclaration bd on fs.userId = bd.UserId
                                Where td.tenderId=@v_fieldName2Vc
                                and bd.LotId = @v_fieldName3Vc
                                And bidSubStatus='finalsubmission'
                                order by FieldValue4	   
                    END
		select
		'1' as FieldValue1,
		CONVERT(varchar(50), finalSubmissionId) as FieldValue2,
		CONVERT(varchar(50), fs.userId) as FieldValue3,
		dbo.f_getbiddercompany(fs.userId) as FieldValue4,
		ISNULL(REPLACE(CONVERT(VARCHAR(11), fs.finalSubmissionDt , 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), fs.finalSubmissionDt ,108), 1, 5), '-') as FieldValue5,
		CONVERT(varchar(50), tp.tenderPaymentId) as FieldValue6 ,
		tp.paymentInstType as FieldValue7,
		CONVERT(varchar(50), tp.amount) as FieldValue8 ,
		tp.bankName as FieldValue9,
		tp.branchName as FieldValue10,
		ISNULL(REPLACE(CONVERT(VARCHAR(11), tp.createdDate , 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), tp.createdDate ,108), 1, 5), '-') as FieldValue11,
		ISNULL(REPLACE(CONVERT(VARCHAR(11), td.tenderSecurityDt	, 106), ' ', '-'), '-') as FieldValue12,
		ISNULL(REPLACE(CONVERT(VARCHAR(11), td.tenderValidityDt	, 106), ' ', '-'),'-') as FieldValue13,
		convert(varchar(50),fs.userId) as FieldValue14,
		convert(varchar(50),tm.tendererId) as FieldValue15,
		convert(varchar(50),tm.companyId) as FieldValue16


		from tbl_FinalSubmission fs
		Inner Join tbl_TenderDetails td On fs.tenderId=td.tenderId
		Inner Join tbl_TendererMaster tm On tm.userId=fs.userId
		Inner Join tbl_TenderPayment tp On fs.userId=tp.userId And fs.tenderId=tp.tenderId
		Where td.tenderId=@v_fieldName2Vc
		And tp.pkgLotId=@v_fieldName3Vc
		And bidSubStatus='finalsubmission'
		And tp.paymentFor='Tender Security'
		And tp.isLive='yes' And tp.isVerified='yes'
		ORDER BY FieldValue4
		END
	Else
		BEGIN
			select
			'1' as FieldValue1,
			CONVERT(varchar(50), finalSubmissionId) as FieldValue2,
			CONVERT(varchar(50), fs.userId) as FieldValue3,
			dbo.f_getbiddercompany(fs.userId) as FieldValue4,
			ISNULL(REPLACE(CONVERT(VARCHAR(11), fs.finalSubmissionDt , 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), fs.finalSubmissionDt ,108), 1, 5), '-') as FieldValue5,
			CONVERT(varchar(50), tp.tenderPaymentId) as FieldValue6 ,
			tp.paymentInstType as FieldValue7,
			CONVERT(varchar(50), tp.amount) as FieldValue8 ,
			tp.bankName as FieldValue9,
			tp.branchName as FieldValue10,
			ISNULL(REPLACE(CONVERT(VARCHAR(11), tp.createdDate , 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), tp.createdDate ,108), 1, 5), '-') as FieldValue11,
			ISNULL(REPLACE(CONVERT(VARCHAR(11), td.tenderSecurityDt	, 106), ' ', '-'), '-') as FieldValue12,
			ISNULL(REPLACE(CONVERT(VARCHAR(11), td.tenderValidityDt	, 106), ' ', '-'),'-') as FieldValue13,
			convert(varchar(50),fs.userId) as FieldValue14,
			convert(varchar(50),tm.tendererId) as FieldValue15,
			convert(varchar(50),tm.companyId) as FieldValue16


			from tbl_FinalSubmission fs
			Inner Join tbl_TenderDetails td On fs.tenderId=td.tenderId
			Inner Join tbl_BidderLots bl on bl.tenderId = td.tenderId
			Inner Join tbl_TendererMaster tm On tm.userId=fs.userId
			Inner Join tbl_TenderPayment tp On fs.userId=tp.userId And fs.tenderId=tp.tenderId
			Where td.tenderId=@v_fieldName2Vc
			And tp.pkgLotId=@v_fieldName3Vc
			And bl.pkgLotId = tp.pkgLotId
			And tp.tenderId = bl.tenderId
			and tp.userId=bl.userId
			And bidSubStatus='finalsubmission'
			And tp.paymentFor='Tender Security'
			And tp.isLive='yes' And tp.isVerified='yes'
			ORDER BY FieldValue4
		END
End
Else
Begin

	IF (Select docAvlMethod from tbl_TenderDetails where tenderId=@v_fieldName2Vc) ='Package'
	BEGIN
	select
	'0' as FieldValue1,
	CONVERT(varchar(50), finalSubmissionId) as FieldValue2,
	CONVERT(varchar(50), fs.userId) as FieldValue3,
	dbo.f_getbiddercompany(fs.userId) as FieldValue4,
	ISNULL(REPLACE(CONVERT(VARCHAR(11), fs.finalSubmissionDt , 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), fs.finalSubmissionDt ,108), 1, 5), '-') as FieldValue5,
	convert(varchar(50),fs.userId) as FieldValue14,
	convert(varchar(50),tm.tendererId) as FieldValue15,
	convert(varchar(50),tm.companyId) as FieldValue16

	from tbl_FinalSubmission fs
	Inner Join tbl_TenderDetails td On fs.tenderId=td.tenderId
	Inner Join tbl_TendererMaster tm On tm.userId=fs.userId
	--Inner Join tbl_TenderPayment tp On fs.userId=tp.userId And fs.tenderId=tp.tenderId
	Where td.tenderId=@v_fieldName2Vc
	--And tp.pkgLotId=@v_fieldName3Vc
	And bidSubStatus='finalsubmission'
	--And tp.paymentFor='Tender Security'
	--And tp.isLive='yes' And tp.isVerified='yes'
	order by FieldValue4
	end
	else
	begin
		select
	'0' as FieldValue1,
	CONVERT(varchar(50), finalSubmissionId) as FieldValue2,
	CONVERT(varchar(50), fs.userId) as FieldValue3,
	dbo.f_getbiddercompany(fs.userId) as FieldValue4,
	ISNULL(REPLACE(CONVERT(VARCHAR(11), fs.finalSubmissionDt , 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), fs.finalSubmissionDt ,108), 1, 5), '-') as FieldValue5,
	convert(varchar(50),fs.userId) as FieldValue14,
	convert(varchar(50),tm.tendererId) as FieldValue15,
	convert(varchar(50),tm.companyId) as FieldValue16

	from tbl_FinalSubmission fs
	Inner Join tbl_TenderDetails td On fs.tenderId=td.tenderId
	Inner Join tbl_TendererMaster tm On tm.userId=fs.userId
	--Inner Join tbl_TenderPayment tp On fs.userId=tp.userId And fs.tenderId=tp.tenderId
	Where td.tenderId=@v_fieldName2Vc
	--And tp.pkgLotId=@v_fieldName3Vc
	And bidSubStatus='finalsubmission'
	--And tp.paymentFor='Tender Security'
	--And tp.isLive='yes' And tp.isVerified='yes'
	and fs.userId in(select userId from tbl_BidderLots where tenderid=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc)
	order by FieldValue4
	end
End


 END
   IF @v_fieldName1Vc = 'getBidderMarksMem'
	BEGIN
	select convert(varchar(20),esd.tenderFormId) as FieldValue1,convert(varchar(1000),formname) as FieldValue2,
	convert(varchar(100),subcriteria) as FieldValue3,convert(varchar(20),esd.subCriteriaId) as FieldValue4,
	convert(varchar(20),avg(convert(float, esd.maxMarks))) as FieldValue5,
	convert(varchar(20),avg(convert(float, esd.ratedScore))) as FieldValue6,convert(varchar(20),avg(convert(float, esd.actualMarks))) as FieldValue7,
	convert(varchar(500),esc.mainCriteria) as FieldValue8
            from Tbl_EvalSerFormDetail esd,Tbl_TenderForms tf,Tbl_EvalServiceCriteria esc
            where esd.tenderFormId=tf.tenderFormId
            and esd.subCriteriaId=esc.subCriteriaId and
            esd.tenderId=@v_fieldName2Vc and esd.evalBy=@v_fieldName5Vc and esd.userId=@v_fieldName4Vc
            and tf.tenderFormId=@v_fieldName3Vc group by esd.tenderFormId,subCriteria,formName,esd.subCriteriaId,mainCriteria

	End
	--Modified By Dohatec For Reevaluation
	IF @v_fieldName1Vc = 'getBidderMarksMemWithCrit'
	BEGIN
	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
		SET @evalCount = 0
	else
		select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

	select convert(varchar(20),avg(convert(float, esd.ratedScore))) as FieldValue1,convert(varchar(20),avg(convert(float, esd.actualMarks))) as FieldValue2,
			ratingWeightage  as FieldValue3
            from Tbl_EvalSerFormDetail esd
            where
            esd.tenderId=@v_fieldName2Vc and esd.evalBy=@v_fieldName5Vc and esd.userId=@v_fieldName4Vc
            and esd.tenderFormId=@v_fieldName3Vc and esd.subCriteriaId=@v_fieldName6Vc and esd.evalCount = @evalCount
            group by esd.tenderFormId,esd.subCriteriaId,ratingWeightage

	End
	--Modified By Dohatec For Reevaluation
	IF @v_fieldName1Vc = 'getTotal4ComMemReport'
	BEGIN
	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
		SET @evalCount = 0
	else
		select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc
		
	select convert(varchar(20),avg(convert(float, esd.actualMarks))) as FieldValue1
            from Tbl_EvalSerFormDetail esd
            where
            esd.tenderId=@v_fieldName2Vc and esd.evalBy=@v_fieldName4Vc and esd.userId=@v_fieldName3Vc and esd.evalCount = @evalCount
            group by esd.tenderFormId,esd.subCriteriaId

	End
	IF @v_fieldName1Vc = 'seekClariLinkTextLinkSer'
	BEGIN
		select memAnswer as FieldValue1 from tbl_EvalCpMemClarification
		where tenderformid=@v_fieldName2Vc and evalMemStatusId=@v_fieldName3Vc and cpQuestionBy=@v_fieldName4Vc
		and memAnswerBy=@v_fieldName5Vc and userId=@v_fieldName6Vc
	End



	IF @v_fieldName1Vc = 'GetAppMIS'
	BEGIN

			IF @v_fieldName2Vc = 'GetAppCount' -- Annual Procurement Plans
			BEGIN
				/*
					FieldValue1=Total No. of APP prepared
					FieldValue2=Total No. of APP approved
				*/

				If @v_fieldName3Vc is Not Null And @v_fieldName3Vc<>'0' -- Office Case
				Begin
					select @Field1 = convert(varchar(50),count(appId)) from tbl_AppMaster Where officeId=@v_fieldName3Vc and appStatus='pending'
					select @Field2 = convert(varchar(50),count(appId)) from tbl_AppMaster Where officeId=@v_fieldName3Vc and appStatus='approved'
				End
				Else If @v_fieldName4Vc is Not Null And @v_fieldName4Vc<>'0' -- Deparatment Case
				Begin
					if (select departmentType from tbl_DepartmentMaster Where departmentId=@v_fieldName4Vc) = 'Ministry'
					begin
						select @Field1 = convert(varchar(50),count(appId)) from tbl_AppMaster
						Where appStatus='pending'
						and (
								departmentId=@v_fieldName4Vc
								Or
								departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								Or
								departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
							)


						select @Field2 = convert(varchar(50),count(appId)) from tbl_AppMaster
						Where appStatus='approved'
						and (
								departmentId=@v_fieldName4Vc
								Or
								departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								Or
								departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
							)
					end

					else if (select departmentType from tbl_DepartmentMaster Where departmentId=@v_fieldName4Vc) = 'Division'
					begin
						select @Field1 = convert(varchar(50),count(appId)) from tbl_AppMaster
						Where appStatus='pending'
						and (
								departmentId=@v_fieldName4Vc
								Or
								departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
							)

						select @Field2 = convert(varchar(50),count(appId)) from tbl_AppMaster
						Where appStatus='approved'
						and (
								departmentId=@v_fieldName4Vc
								Or
								departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
							)
					end

					else if (select departmentType from tbl_DepartmentMaster Where departmentId=@v_fieldName4Vc) = 'Organization'
					begin
						select @Field1 = convert(varchar(50),count(appId)) from tbl_AppMaster
						Where appStatus='pending'
						and departmentId=@v_fieldName4Vc

						select @Field2 = convert(varchar(50),count(appId)) from tbl_AppMaster
						Where appStatus='approved'
						and departmentId=@v_fieldName4Vc
					end

				End



			Select @Field1 as FieldValue1,
			 @Field2 as FieldValue2
			END

			ELSE IF @v_fieldName2Vc = 'GetAppClassifiedCount_Approved'
			BEGIN
				/*
					FieldValue1=Total No. of APP Packages approved Of Goods Procurement Nature
					FieldValue2=Total No. of APP Packages approved Of Works Procurement Nature
					FieldValue3=Total No. of APP Packages approved Of Services Procurement Nature
					FieldValue4=Total No. of APP Packages approved
					FieldValue5=Total Amount of APP Packages approved Of Goods Procurement Nature
					FieldValue6=Total Amount of APP Packages approved Of Works Procurement Nature
					FieldValue7=Total Amount of APP Packages approved Of Services Procurement Nature
					FieldValue8=Total Amount of APP Packages approved

				*/

				If @v_fieldName3Vc is Not Null And @v_fieldName3Vc<>'0' -- Office Case
				Begin
						select @Field1 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								Where officeId=@v_fieldName3Vc and am.appStatus='approved'
								and ap.procurementnature='Goods'

						select @Field2 = convert(varchar(50),count(distinct ap.packageId))
											from tbl_AppMaster am
											inner join tbl_AppPackages ap ON am.appId=ap.appId
											Where officeId=@v_fieldName3Vc and am.appStatus='approved'
											and ap.procurementnature='Works'

						select @Field3 = convert(varchar(50),count(distinct ap.packageId))
											from tbl_AppMaster am
											inner join tbl_AppPackages ap ON am.appId=ap.appId
											Where officeId=@v_fieldName3Vc and am.appStatus='approved'
											and ap.procurementnature='Services'

						select @Field4 = convert(varchar(50),count(distinct ap.packageId))
											from tbl_AppMaster am
											inner join tbl_AppPackages ap ON am.appId=ap.appId
											Where officeId=@v_fieldName3Vc and am.appStatus='approved'


						select @Field5 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
											from tbl_AppMaster am
											inner join tbl_AppPackages ap ON am.appId=ap.appId
											Where officeId=@v_fieldName3Vc and am.appStatus='approved'
											and ap.procurementnature='Goods'

						select @Field6 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
											from tbl_AppMaster am
											inner join tbl_AppPackages ap ON am.appId=ap.appId
											Where officeId=@v_fieldName3Vc and am.appStatus='approved'
											and ap.procurementnature='Works'

						select @Field7 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
											from tbl_AppMaster am
											inner join tbl_AppPackages ap ON am.appId=ap.appId
											Where officeId=@v_fieldName3Vc and am.appStatus='approved'
											and ap.procurementnature='Services'

						select @Field8 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
											from tbl_AppMaster am
											inner join tbl_AppPackages ap ON am.appId=ap.appId
											Where officeId=@v_fieldName3Vc and am.appStatus='approved'
				End
				Else If @v_fieldName4Vc is Not Null And @v_fieldName4Vc<>'0' -- Department Case
				Begin
					if (select departmentType from tbl_DepartmentMaster Where departmentId=@v_fieldName4Vc) = 'Ministry'
					begin
						select @Field1 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='approved'
								and ap.procurementnature='Goods'
								and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
									)

						select @Field2 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='approved'
								and ap.procurementnature='Works'
								and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
									)

						select @Field3 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='approved'
								and ap.procurementnature='Services'
								and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
									)

						select @Field4 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='approved'
								and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
									)

						select @Field5 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='approved'
									and ap.procurementnature='Goods'
									and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
									)

						select @Field6 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='approved'
									and ap.procurementnature='Works'
									and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
									)

						select @Field7 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='approved'
									and ap.procurementnature='Services'
									and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
									)

						select @Field8 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='approved'
									and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
									)
					end

					else if (select departmentType from tbl_DepartmentMaster Where departmentId=@v_fieldName4Vc) = 'Division'
					begin
						select @Field1 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='approved'
								and ap.procurementnature='Goods'
								and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									)

						select @Field2 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='approved'
								and ap.procurementnature='Works'
								and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									)

						select @Field3 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='approved'
								and ap.procurementnature='Services'
								and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									)

						select @Field4 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='approved'
								and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									)

						select @Field5 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='approved'
									and ap.procurementnature='Goods'
									and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									)

						select @Field6 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='approved'
									and ap.procurementnature='Works'
									and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									)

						select @Field7 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='approved'
									and ap.procurementnature='Services'
									and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									)

						select @Field8 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='approved'
									and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									)
					end

					else if (select departmentType from tbl_DepartmentMaster Where departmentId=@v_fieldName4Vc) = 'Organization'
					begin
						select @Field1 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='approved'
								and ap.procurementnature='Goods'
								and departmentId=@v_fieldName4Vc

						select @Field2 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='approved'
								and ap.procurementnature='Works'
								and departmentId=@v_fieldName4Vc

						select @Field3 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='approved'
								and ap.procurementnature='Services'
								and departmentId=@v_fieldName4Vc

						select @Field4 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='approved'
								and departmentId=@v_fieldName4Vc

						select @Field5 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='approved'
									and ap.procurementnature='Goods'
									and departmentId=@v_fieldName4Vc

						select @Field6 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='approved'
									and ap.procurementnature='Works'
									and departmentId=@v_fieldName4Vc

						select @Field7 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='approved'
									and ap.procurementnature='Services'
									and departmentId=@v_fieldName4Vc

						select @Field8 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='approved'
									and departmentId=@v_fieldName4Vc

					end
				End




			Select @Field1 as FieldValue1,
			 @Field2 as FieldValue2,
			 @Field3 as FieldValue3,
			 @Field4 as FieldValue4,
			 @Field5 as FieldValue5,
			 @Field6 as FieldValue6,
			 @Field7 as FieldValue7,
			 @Field8 as FieldValue8


			END

			ELSE IF @v_fieldName2Vc = 'GetAppClassifiedCount_Pending'
			BEGIN
				/*
					FieldValue1=Total No. of APP Packages pending Of Goods Procurement Nature
					FieldValue2=Total No. of APP Packages pending Of Works Procurement Nature
					FieldValue3=Total No. of APP Packages pending Of Services Procurement Nature
					FieldValue4=Total No. of APP Packages pending
					FieldValue5=Total Amount of APP Packages pending Of Goods Procurement Nature
					FieldValue6=Total Amount of APP Packages pending Of Works Procurement Nature
					FieldValue7=Total Amount of APP Packages pending Of Services Procurement Nature
					FieldValue8=Total Amount of APP Packages pending

				*/

				If @v_fieldName3Vc is Not Null And @v_fieldName3Vc<>'0' -- Office Case
				Begin
						select @Field1 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								Where officeId=@v_fieldName3Vc and am.appStatus='pending'
								and ap.procurementnature='Goods'

						select @Field2 = convert(varchar(50),count(distinct ap.packageId))
											from tbl_AppMaster am
											inner join tbl_AppPackages ap ON am.appId=ap.appId
											Where officeId=@v_fieldName3Vc and am.appStatus='pending'
											and ap.procurementnature='Works'

						select @Field3 = convert(varchar(50),count(distinct ap.packageId))
											from tbl_AppMaster am
											inner join tbl_AppPackages ap ON am.appId=ap.appId
											Where officeId=@v_fieldName3Vc and am.appStatus='pending'
											and ap.procurementnature='Services'

						select @Field4 = convert(varchar(50),count(distinct ap.packageId))
											from tbl_AppMaster am
											inner join tbl_AppPackages ap ON am.appId=ap.appId
											Where officeId=@v_fieldName3Vc and am.appStatus='pending'


						select @Field5 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
											from tbl_AppMaster am
											inner join tbl_AppPackages ap ON am.appId=ap.appId
											Where officeId=@v_fieldName3Vc and am.appStatus='pending'
											and ap.procurementnature='Goods'

						select @Field6 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
											from tbl_AppMaster am
											inner join tbl_AppPackages ap ON am.appId=ap.appId
											Where officeId=@v_fieldName3Vc and am.appStatus='pending'
											and ap.procurementnature='Works'

						select @Field7 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
											from tbl_AppMaster am
											inner join tbl_AppPackages ap ON am.appId=ap.appId
											Where officeId=@v_fieldName3Vc and am.appStatus='pending'
											and ap.procurementnature='Services'

						select @Field8 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
											from tbl_AppMaster am
											inner join tbl_AppPackages ap ON am.appId=ap.appId
											Where officeId=@v_fieldName3Vc and am.appStatus='pending'
				End
				Else If @v_fieldName4Vc is Not Null And @v_fieldName4Vc<>'0' -- Department Case
				Begin
					if (select departmentType from tbl_DepartmentMaster Where departmentId=@v_fieldName4Vc) = 'Ministry'
					begin
						select @Field1 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='pending'
								and ap.procurementnature='Goods'
								and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
									)

						select @Field2 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='pending'
								and ap.procurementnature='Works'
								and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
									)

						select @Field3 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='pending'
								and ap.procurementnature='Services'
								and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
									)

						select @Field4 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='pending'
								and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
									)

						select @Field5 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='pending'
									and ap.procurementnature='Goods'
									and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
									)

						select @Field6 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='pending'
									and ap.procurementnature='Works'
									and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
									)

						select @Field7 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='pending'
									and ap.procurementnature='Services'
									and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
									)

						select @Field8 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='pending'
									and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
									)
					end

					else if (select departmentType from tbl_DepartmentMaster Where departmentId=@v_fieldName4Vc) = 'Division'
					begin
						select @Field1 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='pending'
								and ap.procurementnature='Goods'
								and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									)

						select @Field2 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='pending'
								and ap.procurementnature='Works'
								and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									)

						select @Field3 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='pending'
								and ap.procurementnature='Services'
								and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									)

						select @Field4 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='pending'
								and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									)

						select @Field5 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='pending'
									and ap.procurementnature='Goods'
									and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									)

						select @Field6 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='pending'
									and ap.procurementnature='Works'
									and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									)

						select @Field7 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='pending'
									and ap.procurementnature='Services'
									and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									)

						select @Field8 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='pending'
									and
									(
										departmentId=@v_fieldName4Vc
										Or
										departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									)
					end

					else if (select departmentType from tbl_DepartmentMaster Where departmentId=@v_fieldName4Vc) = 'Organization'
					begin
						select @Field1 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='pending'
								and ap.procurementnature='Goods'
								and departmentId=@v_fieldName4Vc

						select @Field2 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='pending'
								and ap.procurementnature='Works'
								and departmentId=@v_fieldName4Vc

						select @Field3 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='pending'
								and ap.procurementnature='Services'
								and departmentId=@v_fieldName4Vc

						select @Field4 = convert(varchar(50),count(distinct ap.packageId))
								from tbl_AppMaster am
								inner join tbl_AppPackages ap ON am.appId=ap.appId
								and am.appStatus='pending'
								and departmentId=@v_fieldName4Vc

						select @Field5 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='pending'
									and ap.procurementnature='Goods'
									and departmentId=@v_fieldName4Vc

						select @Field6 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='pending'
									and ap.procurementnature='Works'
									and departmentId=@v_fieldName4Vc

						select @Field7 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='pending'
									and ap.procurementnature='Services'
									and departmentId=@v_fieldName4Vc

						select @Field8 = convert(varchar(50), IsNull(sum(ap.pkgEstCost),0 ))
									from tbl_AppMaster am
									inner join tbl_AppPackages ap ON am.appId=ap.appId
									Where am.appStatus='pending'
									and departmentId=@v_fieldName4Vc

					end
				End




			Select @Field1 as FieldValue1,
			 @Field2 as FieldValue2,
			 @Field3 as FieldValue3,
			 @Field4 as FieldValue4,
			 @Field5 as FieldValue5,
			 @Field6 as FieldValue6,
			 @Field7 as FieldValue7,
			 @Field8 as FieldValue8


			END
			ELSE IF @v_fieldName2Vc = 'GetTenderClassifiedCount'
			BEGIN

				If @v_fieldName3Vc is Not Null And @v_fieldName3Vc<>'0' -- Office Case
				Begin
					select @Field1 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where officeId=@v_fieldName3Vc and tenderStatus='approved' and procurementNature='Goods' and reTenderId=0

					select @Field2 = convert(varchar(50),count(distinct tenderId))
								from tbl_TenderDetails
								Where officeId=@v_fieldName3Vc and tenderStatus='approved' and procurementNature='Works' and reTenderId=0

					select @Field3 = convert(varchar(50),count(distinct tenderId))
								from tbl_TenderDetails
								Where officeId=@v_fieldName3Vc and tenderStatus='approved' and procurementNature='Services' and reTenderId=0

					select @Field4 = convert(varchar(50),count(distinct tenderId))
								from tbl_TenderDetails
								Where officeId=@v_fieldName3Vc and tenderStatus='approved' and reTenderId=0


					select @Field5 = convert(varchar(50),count(distinct tenderId))
								from tbl_TenderDetails
								Where officeId=@v_fieldName3Vc and tenderStatus='approved' and procurementNature='Goods' and reTenderId=1

					select @Field6 = convert(varchar(50),count(distinct tenderId))
								from tbl_TenderDetails
								Where officeId=@v_fieldName3Vc and tenderStatus='approved' and procurementNature='Works' and reTenderId=1

					select @Field7 = convert(varchar(50),count(distinct tenderId))
								from tbl_TenderDetails
								Where officeId=@v_fieldName3Vc and tenderStatus='approved' and procurementNature='Services' and reTenderId=1

					select @Field8 = convert(varchar(50),count(distinct tenderId))
								from tbl_TenderDetails
								Where officeId=@v_fieldName3Vc and tenderStatus='approved' and reTenderId=1

					select @Field9 = convert(varchar(50),IsNull(sum(estCost),0 ))
								from tbl_TenderDetails
								Where officeId=@v_fieldName3Vc and tenderStatus='approved' and procurementNature='Goods' and reTenderId=0

					select @Field10 = convert(varchar(50),IsNull(sum(estCost),0 ))
								from tbl_TenderDetails
								Where officeId=@v_fieldName3Vc and tenderStatus='approved' and procurementNature='Works' and reTenderId=0

					select @Field11 = convert(varchar(50),IsNull(sum(estCost),0 ))
								from tbl_TenderDetails
								Where officeId=@v_fieldName3Vc and tenderStatus='approved' and procurementNature='Services' and reTenderId=0

					select @Field12 = convert(varchar(50),IsNull(sum(estCost),0 ))
								from tbl_TenderDetails
								Where officeId=@v_fieldName3Vc and tenderStatus='approved' and reTenderId=0
				End
				Else If @v_fieldName4Vc is Not Null And @v_fieldName4Vc<>'0' -- Department Case
				Begin
					if (select departmentType from tbl_DepartmentMaster Where departmentId=@v_fieldName4Vc) = 'Ministry'
					begin
						select @Field1 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and procurementNature='Goods' and reTenderId=0
							and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
								)

						select @Field2 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and procurementNature='Works' and reTenderId=0
							and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
								)

						select @Field3 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and procurementNature='Services' and reTenderId=0
							and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
								)

						select @Field4 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and reTenderId=0
							and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
								)

						select @Field5 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and procurementNature='Goods' and reTenderId=1
							and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
								)

						select @Field6 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and procurementNature='Works' and reTenderId=1
							and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
								)

						select @Field7 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and procurementNature='Services' and reTenderId=1
							and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
								)

						select @Field8 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and reTenderId=1
							and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
								)

						select @Field9 = convert(varchar(50),IsNull(sum(estCost),0 ))
								from tbl_TenderDetails
								Where tenderStatus='approved' and procurementNature='Goods' and reTenderId=0
								and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
								)

						select @Field10 = convert(varchar(50),IsNull(sum(estCost),0 ))
								from tbl_TenderDetails
								Where tenderStatus='approved' and procurementNature='Works' and reTenderId=0
								and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
								)

						select @Field11 = convert(varchar(50),IsNull(sum(estCost),0 ))
								from tbl_TenderDetails
								Where tenderStatus='approved' and procurementNature='Services' and reTenderId=0
								and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
								)

						select @Field12 = convert(varchar(50),IsNull(sum(estCost),0 ))
								from tbl_TenderDetails
								Where tenderStatus='approved' and reTenderId=0
								and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
								)

					end

					else if (select departmentType from tbl_DepartmentMaster Where departmentId=@v_fieldName4Vc) = 'Division'
					begin
						select @Field1 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and procurementNature='Goods' and reTenderId=0
							and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								)

						select @Field2 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and procurementNature='Works' and reTenderId=0
							and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								)

						select @Field3 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and procurementNature='Services' and reTenderId=0
							and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								)

						select @Field4 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and reTenderId=0
							and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								)

						select @Field5 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and procurementNature='Goods' and reTenderId=1
							and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								)

						select @Field6 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and procurementNature='Works' and reTenderId=1
							and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								)

						select @Field7 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and procurementNature='Services' and reTenderId=1
							and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								)

						select @Field8 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and reTenderId=1
							and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								)

						select @Field9 = convert(varchar(50),IsNull(sum(estCost),0 ))
								from tbl_TenderDetails
								Where tenderStatus='approved' and procurementNature='Goods' and reTenderId=0
								and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								)

						select @Field10 = convert(varchar(50),IsNull(sum(estCost),0 ))
								from tbl_TenderDetails
								Where tenderStatus='approved' and procurementNature='Works' and reTenderId=0
								and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								)

						select @Field11 = convert(varchar(50),IsNull(sum(estCost),0 ))
								from tbl_TenderDetails
								Where tenderStatus='approved' and procurementNature='Services' and reTenderId=0
								and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								)

						select @Field12 = convert(varchar(50),IsNull(sum(estCost),0 ))
								from tbl_TenderDetails
								Where tenderStatus='approved' and reTenderId=0
								and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								)


					end

					else if (select departmentType from tbl_DepartmentMaster Where departmentId=@v_fieldName4Vc) = 'Organization'
					begin
						select @Field1 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and procurementNature='Goods' and reTenderId=0
							and departmentId=@v_fieldName4Vc

						select @Field2 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and procurementNature='Works' and reTenderId=0
							and departmentId=@v_fieldName4Vc

						select @Field3 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and procurementNature='Services' and reTenderId=0
							and departmentId=@v_fieldName4Vc

						select @Field4 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and reTenderId=0
							and departmentId=@v_fieldName4Vc

						select @Field5 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and procurementNature='Goods' and reTenderId=1
							and departmentId=@v_fieldName4Vc

						select @Field6 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and procurementNature='Works' and reTenderId=1
							and departmentId=@v_fieldName4Vc

						select @Field7 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and procurementNature='Services' and reTenderId=1
							and departmentId=@v_fieldName4Vc

						select @Field8 = convert(varchar(50),count(distinct tenderId))
							from tbl_TenderDetails
							Where tenderStatus='approved' and reTenderId=1
							and departmentId=@v_fieldName4Vc

						select @Field9 = convert(varchar(50),IsNull(sum(estCost),0 ))
								from tbl_TenderDetails
								Where tenderStatus='approved' and procurementNature='Goods' and reTenderId=0
								and departmentId=@v_fieldName4Vc

						select @Field10 = convert(varchar(50),IsNull(sum(estCost),0 ))
								from tbl_TenderDetails
								Where tenderStatus='approved' and procurementNature='Works' and reTenderId=0
								and departmentId=@v_fieldName4Vc

						select @Field11 = convert(varchar(50),IsNull(sum(estCost),0 ))
								from tbl_TenderDetails
								Where tenderStatus='approved' and procurementNature='Services' and reTenderId=0
								and departmentId=@v_fieldName4Vc

						select @Field12 = convert(varchar(50),IsNull(sum(estCost),0 ))
								from tbl_TenderDetails
								Where tenderStatus='approved' and reTenderId=0
								and departmentId=@v_fieldName4Vc
					end

				End



				Select @Field1 as FieldValue1,
			 @Field2 as FieldValue2,
			 @Field3 as FieldValue3,
			 @Field4 as FieldValue4,
			 @Field5 as FieldValue5,
			 @Field6 as FieldValue6,
			 @Field7 as FieldValue7,
			 @Field8 as FieldValue8,
			 @Field9 as FieldValue9,
			 @Field10 as FieldValue10,
			 @Field11 as FieldValue11,
			 @Field12 as FieldValue12

			END

			ELSE IF @v_fieldName2Vc = 'GetTenderContractInfo'
			BEGIN
				If @v_fieldName3Vc is Not Null And @v_fieldName3Vc<>'0' -- Office Case
				Begin

					select @Field1 = convert(varchar(50),count(distinct contractSignId))
					from tbl_ContractSign cs
					inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
					inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
					Where tenderStatus='approved' and procurementNature='Goods'
					and td.officeId=@v_fieldName3Vc

					select @Field2 = convert(varchar(50),count(distinct contractSignId))
					from tbl_ContractSign cs
					inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
					inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
					Where tenderStatus='approved' and procurementNature='Works'
					and td.officeId=@v_fieldName3Vc

					select @Field3 = convert(varchar(50),count(distinct contractSignId))
					from tbl_ContractSign cs
					inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
					inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
					Where tenderStatus='approved' and procurementNature='Services'
					and td.officeId=@v_fieldName3Vc

					select @Field4 = convert(varchar(50),count(distinct contractSignId))
					from tbl_ContractSign cs
					inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
					inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
					Where tenderStatus='approved'
					and td.officeId=@v_fieldName3Vc

					select @Field5 = convert(varchar(50),IsNull(sum(nid.contractAmt),0 ))
					from tbl_ContractSign cs
					inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
					inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
					Where tenderStatus='approved' and procurementNature='Goods'
					and td.officeId=@v_fieldName3Vc

					select @Field6 = convert(varchar(50),IsNull(sum(nid.contractAmt),0 ))
					from tbl_ContractSign cs
					inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
					inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
					Where tenderStatus='approved' and procurementNature='Works'
					and td.officeId=@v_fieldName3Vc

					select @Field7 = convert(varchar(50),IsNull(sum(nid.contractAmt),0 ))
					from tbl_ContractSign cs
					inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
					inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
					Where tenderStatus='approved' and procurementNature='Services'
					and td.officeId=@v_fieldName3Vc

					select @Field8 = convert(varchar(50),IsNull(sum(nid.contractAmt),0 ))
					from tbl_ContractSign cs
					inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
					inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
					Where tenderStatus='approved'
					and td.officeId=@v_fieldName3Vc

				End

				Else If @v_fieldName4Vc is Not Null And @v_fieldName4Vc<>'0' -- Department Case
				Begin
					if (select departmentType from tbl_DepartmentMaster Where departmentId=@v_fieldName4Vc) = 'Ministry'
					begin
						select @Field1 = convert(varchar(50),count(distinct contractSignId))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved' and procurementNature='Goods'
						and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
								)

						select @Field2 = convert(varchar(50),count(distinct contractSignId))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved' and procurementNature='Works'
						and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
								)


						select @Field3 = convert(varchar(50),count(distinct contractSignId))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved' and procurementNature='Services'
						and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
								)

						select @Field4 = convert(varchar(50),count(distinct contractSignId))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved'
						and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
								)

						select @Field5 = convert(varchar(50),IsNull(sum(nid.contractAmt),0 ))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved' and procurementNature='Goods'
						and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
								)

						select @Field6 = convert(varchar(50),IsNull(sum(nid.contractAmt),0 ))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved' and procurementNature='Works'
						and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
								)

						select @Field7 = convert(varchar(50),IsNull(sum(nid.contractAmt),0 ))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved' and procurementNature='Services'
						and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
								)

						select @Field8 = convert(varchar(50),IsNull(sum(nid.contractAmt),0 ))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved'
						and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Organization' and parentDepartmentId in (select distinct departmentId from tbl_DepartmentMaster where departmentType='Division' and parentDepartmentId=@v_fieldName4Vc))
								)


					end

					else if (select departmentType from tbl_DepartmentMaster Where departmentId=@v_fieldName4Vc) = 'Division'
					begin
						select @Field1 = convert(varchar(50),count(distinct contractSignId))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved' and procurementNature='Goods'
						and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								)

						select @Field2 = convert(varchar(50),count(distinct contractSignId))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved' and procurementNature='Works'
						and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								)

						select @Field3 = convert(varchar(50),count(distinct contractSignId))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved' and procurementNature='Services'
						and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								)

						select @Field4 = convert(varchar(50),count(distinct contractSignId))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved'
						and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								)

						select @Field5 = convert(varchar(50),IsNull(sum(nid.contractAmt),0 ))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved' and procurementNature='Goods'
						and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								)

						select @Field6 = convert(varchar(50),IsNull(sum(nid.contractAmt),0 ))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved' and procurementNature='Works'
						and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								)

						select @Field7 = convert(varchar(50),IsNull(sum(nid.contractAmt),0 ))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved' and procurementNature='Services'
						and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								)

						select @Field8 = convert(varchar(50),IsNull(sum(nid.contractAmt),0 ))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved'
						and
								(
									departmentId=@v_fieldName4Vc
									Or
									departmentId in (select distinct departmentId from tbl_DepartmentMaster where parentDepartmentId=@v_fieldName4Vc)
								)
					end

					else if (select departmentType from tbl_DepartmentMaster Where departmentId=@v_fieldName4Vc) = 'Organization'
					begin
						select @Field1 = convert(varchar(50),count(distinct contractSignId))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved' and procurementNature='Goods'
						and departmentId=@v_fieldName4Vc

						select @Field2 = convert(varchar(50),count(distinct contractSignId))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved' and procurementNature='Works'
						and departmentId=@v_fieldName4Vc

						select @Field3 = convert(varchar(50),count(distinct contractSignId))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved' and procurementNature='Services'
						and departmentId=@v_fieldName4Vc

						select @Field4 = convert(varchar(50),count(distinct contractSignId))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved'
						and departmentId=@v_fieldName4Vc

						select @Field5 = convert(varchar(50),IsNull(sum(nid.contractAmt),0 ))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved' and procurementNature='Goods'
						and departmentId=@v_fieldName4Vc

						select @Field6 = convert(varchar(50),IsNull(sum(nid.contractAmt),0 ))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved' and procurementNature='Works'
						and departmentId=@v_fieldName4Vc

						select @Field7 = convert(varchar(50),IsNull(sum(nid.contractAmt),0 ))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved' and procurementNature='Services'
						and departmentId=@v_fieldName4Vc

						select @Field8 = convert(varchar(50),IsNull(sum(nid.contractAmt),0 ))
						from tbl_ContractSign cs
						inner join tbl_NoaIssueDetails nid ON cs.noaId=nid.noaIssueId
						inner join tbl_TenderDetails td ON nid.tenderId=td.tenderId
						Where tenderStatus='approved'
						and departmentId=@v_fieldName4Vc
					end
				End

				Select @Field1 as FieldValue1,
			 @Field2 as FieldValue2,
			 @Field3 as FieldValue3,
			 @Field4 as FieldValue4,
			 @Field5 as FieldValue5,
			 @Field6 as FieldValue6,
			 @Field7 as FieldValue7,
			 @Field8 as FieldValue8
			END

	END
End
IF @v_fieldName1Vc = 'getReportFormDetail'
Begin
		select COnvert(varchar(50),a.appPkgLotId ) as FieldValue1,a.lotNo as FieldValue2,a.lotDesc as FieldValue3
		,case when b.pkgLotId IS not null then 'Yes' else 'No' end FieldValue4,
		case when c.pkgLotId IS not null then 'Yes' else 'No' end FieldValue5
		from (select ls.appPkgLotId,ls.lotNo,ls.lotDesc
		from Tbl_TenderDetails tm,Tbl_TenderLotSecurity ls
		where tm.tenderId=ls.tenderId and tm.tenderId=@v_fieldName2Vc )a
		left outer join(select distinct pkgLotId from tbl_ReportLots trl,tbl_ReportMaster trm where trl.reportId=trm.reportId and
		trm.tenderId=@v_fieldName2Vc  and trm.isTORTER = 'TOR' ) b
		on a.appPkgLotId=b.pkgLotId
		left outer join(select distinct pkgLotId from tbl_ReportLots trl,tbl_ReportMaster trm where trl.reportId=trm.reportId and
		trm.tenderId=@v_fieldName2Vc  and trm.isTORTER = 'TER' ) c
		on a.appPkgLotId=c.pkgLotId
End


IF @v_fieldName1Vc = 'searchBidder'
BEGIN

declare @bidQuery varchar(Max), @v_CpvList1_Vc varchar(max),@procN varchar(10),@procMethod varchar(50),@procType varchar(50),@v_tendertype varchar(20),@v_fieldSet varchar(50)
SELECT @v_CpvList1_Vc = cpvcode, @procN = cast(procurementNature as varchar), @procMethod = procurementMethod,@procType = procurementType,@v_tendertype=eventType from tbl_TenderDetails where tenderId = @v_fieldName2Vc
print @procType
IF(@v_tendertype='RFA') OR (@v_tendertype='RFP' And @procMethod='IC')
Begin
	set @v_fieldSet = ' and lm.registrationType=''individualconsultant'' '
End
Else
Begin
	set @v_fieldSet = ''
End
IF (select reoiTenderId from tbl_TenderDetails where tenderId=@v_fieldName2Vc and procurementNature='Services')!=0
Begin
	set @v_fieldName5Vc='0'
End
IF(@procMethod!='IC')
Begin
	if (@v_fieldName5Vc ='1' and @procType != 'ICT')
		begin
		Set @bidQuery =
		'Select distinct ct.CompanyName as FieldValue1, ct.companyRegNumber FieldValue2, ct.EmailID FieldValue3,  ct.Country FieldValue4,  ct.State as FieldValue5,  ct.RegistrationType FieldValue6 ,  ct.LegalStatus FieldValue7, convert(varchar(30),ct.userId )as FieldValue8, case when lt.userId IS NULL  then ''UnMapped'' else ''Mapped'' end as FieldValue9  from
		(
		Select case CompanyName when ''-'' then tm.firstName+'' ''+ tm.lastName else companyName end as companyName, companyRegNumber,  EmailID, case CompanyName when ''-'' then  Country else regOffCountry end as Country, case CompanyName when ''-'' then State else regOffState  end as State,  RegistrationType,  LegalStatus , lm.userId
		from tbl_LoginMaster lm, tbl_TendererMaster tm , tbl_CompanyMaster tc, tbl_BiddingPermission bp
		where tm.isAdmin = ''Yes'' and lm.userId = tm.userId and tm.companyid=tc.companyid and tc.companyid=bp.companyid and bp.ProcurementCategory= '''+@v_fieldName6Vc+''' and lm.registrationType!=''media'' '+@v_fieldSet+'  and  (tc.regOffcountry=''Bhutan'' or(tm.country=''Bhutan'' and tm.companyid=1))   
		
		and status=''Approved'' and tc.companyid = tm.companyId '+case when @v_fieldName3Vc is not null then +' and ' + @v_fieldName3Vc else '' end +'
		) as ct
			left outer join (Select userId from tbl_LimitedTenders where tenderId = '+@v_fieldName2Vc+' and pkgLotId= '+@v_fieldName4Vc+') lt
			on ct.userid = lt.userId order by ct.CompanyName '

		end
		else
		begin
		Set @bidQuery =
		'Select distinct ct.CompanyName as FieldValue1, ct.companyRegNumber FieldValue2, ct.EmailID FieldValue3,  ct.Country FieldValue4,  ct.State as FieldValue5,  ct.RegistrationType FieldValue6 ,  ct.LegalStatus FieldValue7, convert(varchar(30),ct.userId )as FieldValue8, case when lt.userId IS NULL  then ''UnMapped'' else ''Mapped'' end as FieldValue9  from
		(
		Select case CompanyName when ''-'' then tm.firstName+'' ''+ tm.lastName else companyName end as companyName, companyRegNumber,  EmailID,  case CompanyName when ''-'' then  Country else regOffCountry end as Country,  case CompanyName when ''-'' then State else regOffState  end as State,  RegistrationType,  LegalStatus , lm.userId
		from tbl_LoginMaster lm, tbl_TendererMaster tm , tbl_CompanyMaster tc, tbl_BiddingPermission bp
		where tm.isAdmin = ''Yes'' and lm.userId = tm.userId and tm.companyid=tc.companyid and tc.companyid=bp.companyid and bp.ProcurementCategory= '''+@v_fieldName6Vc+''' and lm.registrationType!=''media'' '+@v_fieldSet+'     and
		  status=''Approved'' and tc.companyid = tm.companyId '+case when @v_fieldName3Vc is not null then +' and ' + @v_fieldName3Vc else '' end +'
		) as ct
			left outer join (Select userId from tbl_LimitedTenders where tenderId = '+@v_fieldName2Vc+' and pkgLotId= '+@v_fieldName4Vc+') lt
			on ct.userid = lt.userId  order by ct.CompanyName '

		end
End
Else
Begin
		if @v_fieldName5Vc ='1'
			begin
			Set @bidQuery =
			'Select distinct ct.CompanyName as FieldValue1, ct.companyRegNumber FieldValue2, ct.EmailID FieldValue3,  ct.Country FieldValue4,  ct.State as FieldValue5,  ct.RegistrationType FieldValue6 ,  ct.LegalStatus FieldValue7, convert(varchar(30),ct.userId )as FieldValue8, case when lt.userId IS NULL  then ''UnMapped'' else ''Mapped'' end as FieldValue9  from
			(
			Select case CompanyName when ''-'' then tm.firstName+'' ''+ tm.lastName else companyName end as companyName, companyRegNumber,  EmailID, case CompanyName when ''-'' then  Country else regOffCountry end as Country, case CompanyName when ''-'' then State else regOffState  end as State,  RegistrationType,  LegalStatus , lm.userId
			from tbl_LoginMaster lm, tbl_TendererMaster tm , tbl_CompanyMaster tc, tbl_BiddingPermission bp
			where tm.isAdmin = ''Yes'' and lm.userId = tm.userId and lm.registrationType=''individualconsultant'' and tm.companyid=tc.companyid and tc.companyid=bp.companyid and bp.ProcurementCategory= '''+@v_fieldName6Vc+''' and lm.registrationType!=''media'' and  (tc.regOffcountry=''Bhutan'' or(tm.country=''Bhutan'' and tm.companyid=1))  and
			 status=''Approved'' and tc.companyid = tm.companyId '+case when @v_fieldName3Vc is not null then +' and ' + @v_fieldName3Vc else '' end +'
			) as ct
				left outer join (Select userId from tbl_LimitedTenders where tenderId = '+@v_fieldName2Vc+' and pkgLotId= '+@v_fieldName4Vc+') lt
				on ct.userid = lt.userId  order by ct.CompanyName '

			end
			else
			begin
			Set @bidQuery =
			'Select distinct ct.CompanyName as FieldValue1, ct.companyRegNumber FieldValue2, ct.EmailID FieldValue3,  ct.Country FieldValue4,  ct.State as FieldValue5,  ct.RegistrationType FieldValue6 ,  ct.LegalStatus FieldValue7, convert(varchar(30),ct.userId )as FieldValue8, case when lt.userId IS NULL  then ''UnMapped'' else ''Mapped'' end as FieldValue9  from
			(
			Select case CompanyName when ''-'' then tm.firstName+'' ''+ tm.lastName else companyName end as companyName, companyRegNumber,  EmailID,  case CompanyName when ''-'' then  Country else regOffCountry end as Country,  case CompanyName when ''-'' then State else regOffState  end as State,  RegistrationType,  LegalStatus , lm.userId
			from tbl_LoginMaster lm, tbl_TendererMaster tm , tbl_CompanyMaster tc, tbl_BiddingPermission bp
			where tm.isAdmin = ''Yes'' and lm.userId = tm.userId and lm.registrationType=''individualconsultant'' and tm.companyid=tc.companyid and tc.companyid=bp.companyid and bp.ProcurementCategory= '''+@v_fieldName6Vc+''' and lm.registrationType!=''media''    and
			  status=''Approved'' and tc.companyid = tm.companyId '+case when @v_fieldName3Vc is not null then +' and ' + @v_fieldName3Vc else '' end +'
			) as ct
				left outer join (Select userId from tbl_LimitedTenders where tenderId = '+@v_fieldName2Vc+' and pkgLotId= '+@v_fieldName4Vc+') lt
				on ct.userid = lt.userId  order by ct.CompanyName '

			end
End
print(@bidQuery)
	exec(@bidQuery)
END

--change by dohatec for re-evaluation
IF @v_fieldName1Vc = 'isEvalReportSigned'
BEGIN
	select convert(varchar(50),COUNT(torSignId)) as FieldValue1 from
	tbl_TORRptSign where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc and reportType=''+@v_fieldName4Vc+''
	and roundId = @v_fieldName5Vc and evalCount = @v_fieldName6Vc
end

--change by dohatec for re-evaluation
IF @v_fieldName1Vc='getTECMembers'
BEGIN

--SELECT employeeName as FieldValue1,cm.memberRole as FieldValue2, dsm.designationName as FieldValue3,om.officeName as FieldValue4
	--from tbl_Committee c,tbl_CommitteeMembers cm,tbl_EmployeeMaster em,tbl_EmployeeOffices eo,
	--tbl_OfficeMaster om,tbl_DepartmentMaster  dm,tbl_DesignationMaster dsm
	--WHERE c.committeeId=cm.committeeId and cm.userId=em.userId
	--and em.employeeId=eo.employeeId and eo.officeId=om.officeId  and dsm.designationId=em.designationId
	--and om.departmentId=dm.departmentId and dm.departmentId=dsm.departmentid
	--and tenderId = @v_fieldName2Vc and committeeType in('TOC','POC')
			
	If @v_fieldName3Vc='0'
	Begin
			SELECT	--employeeName as FieldValue1,
					--(SELECT dbo.f_Gov_Part_UserName(lm.userId,(SELECT signedDate from tbl_TORRptSign trs Where trs.userId=cm.userId And tenderId=c.tenderId and trs.reportType=@v_fieldName4Vc	 and trs.roundId=@v_fieldName5Vc),lm.userTyperId)) as FieldValue1,    -- commented out by dohatec to get actual transfered Employee name 
					(SELECT dbo.f_Gov_Part_Transfer_UserName(lm.userId,(SELECT signedDate from tbl_TORRptSign trs Where trs.userId=cm.userId And tenderId=c.tenderId and trs.reportType=@v_fieldName4Vc	 and trs.evalCount = @v_fieldName6Vc and trs.roundId=@v_fieldName5Vc),lm.userTyperId)) as FieldValue1,    -- Modified by dohatec to get actual transfered Employee name
					cm.memberRole as FieldValue2,
					dsm.designationName as FieldValue3,
					om.officeName as FieldValue4,
					CONVERT(varchar(50), cm.userId) as FieldValue5,
					IsNull((select CONVERT(varchar(50), signedDate, 106) + ' ' + Substring(CONVERT(VARCHAR(30),signedDate,108),1,5) from tbl_TORRptSign trs Where trs.userId=cm.userId And tenderId=c.tenderId and trs.reportType=@v_fieldName4Vc and trs.evalCount = @v_fieldName6Vc and (trs.roundId=@v_fieldName5Vc)),'-') as FieldValue6,
					CONVERT(varchar(50), c.committeeId) as FieldValue7,
					IsNull((select trs.comments from tbl_TORRptSign trs Where trs.userId=cm.userId And tenderId=c.tenderId and trs.reportType=@v_fieldName4Vc and trs.evalCount = @v_fieldName6Vc and (trs.roundId=@v_fieldName5Vc)),'-') as FieldValue8
			from	tbl_Committee c,
					tbl_CommitteeMembers cm,
					tbl_EmployeeMaster em,
					tbl_EmployeeOffices eo,
					tbl_OfficeMaster om,
					tbl_DepartmentMaster  dm,
					tbl_DesignationMaster dsm,
					tbl_LoginMaster lm
			WHERE	c.committeeId=cm.committeeId
					and cm.userId=em.userId
					and lm.userId = em.userId
					and em.employeeId=eo.employeeId
					and eo.officeId=om.officeId
					and dsm.designationId=em.designationId
					and om.departmentId=dm.departmentId
					and dm.departmentId=dsm.departmentid
					and c.tenderId = @v_fieldName2Vc
					and committeeType in('TEC','PEC')
					And committeStatus='Approved'
	End
	Else
	Begin
			SELECT	--employeeName as FieldValue1,
					--(SELECT dbo.f_Gov_Part_UserName(lm.userId,(SELECT signedDate from tbl_TORRptSign trs Where trs.userId=cm.userId And tenderId=c.tenderId and trs.reportType=@v_fieldName4Vc	 and trs.roundId=@v_fieldName5Vc),lm.userTyperId)) as FieldValue1, -- commented out by dohatec to get actual transfered Employee name 
					(SELECT dbo.f_Gov_Part_Transfer_UserName(lm.userId,(SELECT signedDate from tbl_TORRptSign trs Where trs.userId=cm.userId And tenderId=c.tenderId and trs.reportType=@v_fieldName4Vc	 and trs.evalCount = @v_fieldName6Vc and trs.roundId=@v_fieldName5Vc),lm.userTyperId)) as FieldValue1,     -- Modified by dohatec to get actual transfered Employee name
					cm.memberRole as FieldValue2,
					dsm.designationName as FieldValue3,
					om.officeName as FieldValue4,
					CONVERT(varchar(50), cm.userId) as FieldValue5,
					IsNull((select CONVERT(varchar(50), signedDate, 106) + ' ' + Substring(CONVERT(VARCHAR(30),signedDate,108),1,5) from tbl_TORRptSign trs Where trs.userId=cm.userId And tenderId=c.tenderId And trs.pkgLotId=@v_fieldName3Vc  and trs.reportType=@v_fieldName4Vc and trs.evalCount = @v_fieldName6Vc and (trs.roundId=@v_fieldName5Vc)),'-') as FieldValue6,
					CONVERT(varchar(50), c.committeeId) as FieldValue7,
					IsNull((select trs.comments from tbl_TORRptSign trs Where trs.userId=cm.userId And tenderId=c.tenderId And trs.pkgLotId=@v_fieldName3Vc  and trs.reportType=@v_fieldName4Vc and trs.evalCount = @v_fieldName6Vc and (trs.roundId=@v_fieldName5Vc)),'-') as FieldValue8
			from	tbl_Committee c,
					tbl_CommitteeMembers cm,
					tbl_EmployeeMaster em,
					tbl_EmployeeOffices eo,
					tbl_OfficeMaster om,
					tbl_DepartmentMaster  dm,
					tbl_DesignationMaster dsm,
					tbl_LoginMaster lm
			WHERE	c.committeeId=cm.committeeId
					and cm.userId=em.userId
					and lm.userId = em.userId
					and em.employeeId=eo.employeeId
					and eo.officeId=om.officeId
					and dsm.designationId=em.designationId
					and om.departmentId=dm.departmentId
					and dm.departmentId=dsm.departmentid
					and c.tenderId = @v_fieldName2Vc
					and committeeType in('TEC','PEC')
					And committeStatus='Approved'
	END
END
--change by dohatec for re-evaluation
if @v_fieldName1Vc='isTERSignedByAll'
begin
	if @v_fieldName4Vc ='4' -- TER's count
	begin
		IF @v_fieldName3Vc='0'
		BEGIN
			  select (case
					when

						(select COUNT(cm.userId)*convert(int,@v_fieldName4Vc) from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
						where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved'  and c.committeeType in ('TEC','PEC'))
						=
						(select COUNT(trs.torSignId) from tbl_TORRptSign trs
						Where tenderId=@v_fieldName2Vc and trs.evalCount = @v_fieldName7Vc and
						(
						(trs.reportType in ('ter1') and (roundId=0))
							or
						(trs.reportType in ('ter2','ter3','ter4') and (roundId=@v_fieldName6Vc))
						)
						and pkgLotId=@v_fieldName3Vc)

				then '1' else '0' end) as FieldValue1
		END
		ELSE
		BEGIN
					select case
					when
					(
						(select COUNT(cm.userId)*convert(int,@v_fieldName4Vc) from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
						where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved'  and c.committeeType in ('TEC','PEC'))
						=
						(select COUNT(trs.torSignId) from tbl_TORRptSign trs
						Where tenderId=@v_fieldName2Vc and trs.evalCount = @v_fieldName7Vc and
						(
						(trs.reportType in ('ter1','ter2') and (roundId=0))
							or
						(trs.reportType in ('ter3','ter4') and (roundId=@v_fieldName6Vc))
						)
						and pkgLotId=@v_fieldName3Vc)
					)
				then '1' else '0' end as FieldValue1
		END
END
else if @v_fieldName4Vc ='3' -- TER's count
	begin
		IF @v_fieldName3Vc='0'
		BEGIN
			select case
				when
				(
				(select COUNT(cm.userId)*convert(int,@v_fieldName4Vc) from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
				where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved'  and c.committeeType in ('TEC','PEC'))
				 =
				(select COUNT(trs.torSignId) from tbl_TORRptSign trs Where tenderId=@v_fieldName2Vc and trs.evalCount = @v_fieldName7Vc and 
				(
					(trs.reportType in ('ter1') and (roundId=0))
						or
					(trs.reportType in ('ter2','ter3') and (roundId=@v_fieldName6Vc))
				)
				and pkgLotId=@v_fieldName3Vc)
				 )
				then '1' else '0' end as FieldValue1
		END
		ELSE
		BEGIN
				select case
				when
				(
				(select COUNT(cm.userId)*convert(int,@v_fieldName4Vc) from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
				where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved'  and c.committeeType in ('TEC','PEC'))
				 =
				(select COUNT(trs.torSignId) from tbl_TORRptSign trs Where tenderId=@v_fieldName2Vc and trs.evalCount = @v_fieldName7Vc and
				(
					(trs.reportType in ('ter1','ter2') and (roundId=0))
						or
					(trs.reportType in ('ter3') and (roundId=@v_fieldName6Vc))
				)
				and pkgLotId=@v_fieldName3Vc)
				 )
			then '1' else '0' end as FieldValue1
		END
end
else if @v_fieldName4Vc ='2' -- TER's count
	begin
		IF @v_fieldName3Vc='0'
		BEGIN
			select case
				when
				(
				(select COUNT(cm.userId)*convert(int,@v_fieldName4Vc) from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
				where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved' and c.committeeType in ('TEC','PEC'))
				=
				(select COUNT(trs.torSignId) from tbl_TORRptSign trs Where tenderId=@v_fieldName2Vc and trs.evalCount = @v_fieldName7Vc and
				(
					(trs.reportType in ('ter1') and (roundId=0))
						or
					(trs.reportType in ('ter2') and (roundId=@v_fieldName6Vc))
				)
				and pkgLotId=@v_fieldName3Vc)
				)
			then '1' else '0' end as FieldValue1
		END
		ELSE
		begin
				select case
				when
				(
				(select COUNT(cm.userId)*convert(int,@v_fieldName4Vc) from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
				where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved' and c.committeeType in ('TEC','PEC'))
				=
				(select COUNT(trs.torSignId) from tbl_TORRptSign trs Where tenderId=@v_fieldName2Vc and trs.reportType in ('ter1','ter2') and trs.evalCount = @v_fieldName7Vc 
				 and pkgLotId=@v_fieldName3Vc and roundId=@v_fieldName6Vc)
				)
			then '1' else '0' end as FieldValue1
		END

end
else if @v_fieldName4Vc ='1' -- TER's count
	begin
		select case
	when
		(
		(select COUNT(cm.userId)*convert(int,@v_fieldName4Vc) from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
		where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved' and c.committeeType in ('TEC','PEC'))
		=
		(
		  select COUNT(trs.torSignId) from tbl_TORRptSign trs Where tenderId=@v_fieldName2Vc and trs.reportType in ('ter1') and pkgLotId=@v_fieldName3Vc and trs.evalCount = @v_fieldName7Vc)
		)
	then '1' else '0' end as FieldValue1
end
end

--change by dohatec for re-evaluation
if @v_fieldName1Vc='isTERSentToAA'
begin
	if (@v_fieldName6Vc = null or @v_fieldName6Vc = '')
	BEGIN
		select convert(varchar(50),COUNT(evalRptToAAId)) as FieldValue1 from
		tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
		and envelopeId=@v_fieldName4Vc and roundId = @v_fieldName5Vc
	END
	else
	BEGIN
		select convert(varchar(50),COUNT(evalRptToAAId)) as FieldValue1 from
		tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
		and envelopeId=@v_fieldName4Vc and roundId = @v_fieldName5Vc and evalCount = @v_fieldName6Vc
	END
end
if @v_fieldName1Vc='isTERSentToAA1s2eafter'
begin
if exists (select convert(varchar(50),COUNT(evalRptToAAId)) as FieldValue1 from
			tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
			and envelopeId=@v_fieldName4Vc and roundId = @v_fieldName5Vc and rptStatus in ('Approved','Pending') and evalCount = @v_fieldName6Vc
)
	BEGIN
	if ((select evalRptToAAId from
			tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
			and envelopeId <> 0 and roundId <> 0 ) > (select max(evalRptToAAId) from
			tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
			and envelopeId=@v_fieldName4Vc and roundId = @v_fieldName5Vc) 
			)
			
			select '0' as FieldValue1
	else
			select '1' as FieldValue1
	END
else
	select '0' as FieldValue1

end
--added by dohatec for re-evauation
if @v_fieldName1Vc='isTERSentToAA1s2e'
begin
	if (@v_fieldName6Vc = null or @v_fieldName6Vc = '')
	BEGIN
		select convert(varchar(50),COUNT(evalRptToAAId)) as FieldValue1 from
		tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
		and envelopeId=@v_fieldName4Vc and roundId = @v_fieldName5Vc
	END
	else
	BEGIN

	select convert(varchar(50),COUNT(evalRptToAAId)) as FieldValue1 from
			tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
			and envelopeId=@v_fieldName4Vc and roundId = @v_fieldName5Vc and rptStatus in ('Approved','Pending') and evalCount = @v_fieldName6Vc
	END
end
--added by dohatec for re-evauation
if @v_fieldName1Vc='isTERSentToAAService'
begin
	select convert(varchar(50),COUNT(evalRptToAAId)) as FieldValue1 from
			tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
			and envelopeId=@v_fieldName4Vc and roundId = @v_fieldName5Vc and rptStatus in ('Approved','Pending')

end

if @v_fieldName1Vc='isTERSentToAAApp'
begin
	select convert(varchar(50),COUNT(evalRptToAAId)) as FieldValue1 from
	tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
	and envelopeId=@v_fieldName4Vc and roundId = @v_fieldName5Vc and rptStatus='Approved'
end
if @v_fieldName1Vc='isClarificationAsked'
begin
	select  top 1 convert(varchar(50),er.evalRptToAAId) as FieldValue1,
                er.answer AS FieldValue2
	from    tbl_EvalReportClarification er inner join tbl_EvalRptSentToAA es on  er.evalRptToAAId = es.evalRptToAAId
	where   es.tenderId=@v_fieldName2Vc and es.pkgLotId=@v_fieldName3Vc and es.roundId = @v_fieldName4Vc
	order by evalRptClariId desc
end
if @v_fieldName1Vc='evaluationMemberCheck'
begin
	if @v_fieldName4Vc = '1'
	begin
		select convert(varchar(50),COUNT(cm.userId))  as FieldValue1 from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
		where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved'  and c.committeeType in ('TEC','PEC')
		and cm.userId=@v_fieldName3Vc and cm.memberRole in ('cp')
	end
	if @v_fieldName4Vc = '0'
	begin
		select convert(varchar(50),COUNT(cm.userId))  as FieldValue1 from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
		where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved'  and c.committeeType in ('TEC','PEC')
		and cm.userId=@v_fieldName3Vc and cm.memberRole in ('m','ms')
	end
end

IF @v_fieldName1Vc = 'GetDocumentsWithPaging'
BEGIN

	--declare @v_FormList_Vc1 varchar(2000)

	/*
	 Start: Input Params
	 @v_fieldName1Vc =keyword,
	 @v_fieldName2Vc =FolderId,
	 @v_fieldName3Vc =UserId (Session id),
	 @v_fieldName4Vc =PageNo,
	 @v_fieldName5Vc =RecordSize,
	 @v_fieldName6Vc =SearchText,
	 @v_fieldName7Vc =SOrtVal
	 End: Input Params
	 */

	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName4Vc, @v_RecordPerPage_inInt=@v_fieldName5Vc

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */


	/*Start: Dynamic Condition string*/
	SET @v_ConditionString_Vc=''
	If @v_fieldName6Vc is not null And @v_fieldName6Vc<>''
	Begin
		Set @v_ConditionString_Vc = ' And ' + @v_fieldName6Vc
	End
	/*End: Dynamic Condition string*/

	If (select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1 -- Company Case
	Begin
		 select @v_FormList_Vc1 = COALESCE(@v_FormList_Vc1+',', ' ') + convert(varchar(20), formid)
		 from tbl_biddocuments where userId=@v_fieldName3Vc
		 --print    @v_FormList_Vc1
		 set @v_FormList_Vc1=ISNULL(@v_FormList_Vc1,'')
		 Select @FinalQuery='
		 With CTE1 as (
		 Select * from (
		 select
		  convert(varchar(50),a.companyDocId ) as FieldValue1,
		  convert(varchar(150),documentName) as FieldValue2
			  , convert(varchar(150),documentSize) as FieldValue3
			  , convert(varchar(250),documentBrief) as FieldValue4
			  ,ISNULL(REPLACE(CONVERT(VARCHAR(11), uploadedDate, 106), '' '', ''-'') + '' '' +SUBSTRING(CONVERT(VARCHAR(5), uploadedDate,108), 1, 5), ''-'') as FieldValue5
			  , convert(varchar(50),docstatus) as FieldValue6
			  , convert(varchar(50),b.companyDocId ) as FieldValue7
			  ,IsNull(docHash,'''') as FieldValue13
			  , convert(varchar(50),documentTypeId )  as FieldValue11
			  ,case when b.companyDocId is not null then '''+@v_FormList_Vc1+''' else null end as FieldValue10
			  ,Row_Number() Over('+@v_fieldName7Vc+') as RowNumber

			  from (select c.*
					from tbl_CompanyDocuments c,
						tbl_TendererMaster t
					where c.tendererId=t.tendererId
							and t.userId in
								(
									select userId from tbl_TendererMaster
										where companyId =
											(select companyId from tbl_TendererMaster where userId='+@v_fieldName3Vc+' and companyId!=1)
								)

							and folderId='+@v_fieldName2Vc+' and docStatus!=''archive'''
							+ @v_ConditionString_Vc +
				   ') a
			left outer join
				(select distinct companyDocId  from tbl_biddocuments
					where userId in
								(
									select userId from tbl_TendererMaster
										where companyId =
											(select companyId from tbl_TendererMaster where userId='+@v_fieldName3Vc+' and companyId!=1)
								)
				) b
			on a.companyDocId=b.companydocid
			) as Tmp
	)
	select *, (select CONVERT(varchar(50), COUNT(*)) from CTE1) as FieldValue12 from CTE1
	WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
	+ ' Order by RowNumber'

	End
	Else -- Individual Consultant Case
	Begin
		 select @v_FormList_Vc1 = COALESCE(@v_FormList_Vc1+',', ' ') + convert(varchar(20), formid)
		 from tbl_biddocuments where userId=@v_fieldName3Vc
		 --print    @v_FormList_Vc1

	 Select @FinalQuery='
	  With CTE1 as (
	 Select * from (
	 select
		  convert(varchar(50),a.companyDocId ) as FieldValue1,
		 convert(varchar(150),documentName) as FieldValue2
			  , convert(varchar(150),documentSize) as FieldValue3
			  , convert(varchar(250),documentBrief) as FieldValue4
			  ,ISNULL(REPLACE(CONVERT(VARCHAR(11), uploadedDate, 106), '' '', ''-'') + '' '' +SUBSTRING(CONVERT(VARCHAR(5), uploadedDate,108), 1, 5), ''-'') as FieldValue5
			  , convert(varchar(50),docstatus) as FieldValue6
			  , convert(varchar(50),b.companyDocId ) as FieldValue7
			  ,IsNull(docHash,'''') as FieldValue13
			  , documentTypeId as FieldValue11
			  ,case when b.companyDocId is not null then '''+@v_FormList_Vc1+''' else null end as FieldValue10
			  ,Row_Number() Over('+@v_fieldName7Vc+') as RowNumber

			  from (select c.* from tbl_CompanyDocuments c,
								tbl_TendererMaster t
					where c.tendererId=t.tendererId
							and userId='+@v_fieldName3Vc+'
						and folderId='+@v_fieldName2Vc+' and docStatus!=''archive'''
						+ @v_ConditionString_Vc +
					') a
		left outer join
		(select distinct companyDocId  from tbl_biddocuments where userId='+@v_fieldName3Vc+') b
		on a.companyDocId=b.companydocid
			) as Tmp
	)
	select *, (select CONVERT(varchar(50), COUNT(*)) from CTE1) as FieldValue12 from CTE1
	WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
	+ ' Order by RowNumber'


	End

	 print (@FinalQuery)
	 Exec (@FinalQuery)


	 --print @v_FormList_Vc1
END

IF @v_fieldName1Vc = 'GetFoldersWithPaging'
BEGIN

	/*
	 Start: Input Params
	 @v_fieldName1Vc =keyword,
	 @v_fieldName2Vc =FolderId,
	 @v_fieldName3Vc =UserId (Session id),
	 @v_fieldName4Vc =PageNo,
	 @v_fieldName5Vc =RecordSize,
	 @v_fieldName6Vc =SearchText,
	 @v_fieldName7Vc =SOrtVal
	 End: Input Params
	 */

	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName4Vc, @v_RecordPerPage_inInt=@v_fieldName5Vc

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */


	/*Start: Dynamic Condition string*/
	SET @v_ConditionString_Vc=''
	If @v_fieldName6Vc is not null And @v_fieldName6Vc<>''
	Begin
		Set @v_ConditionString_Vc = ' And ' + @v_fieldName6Vc
	End

	If (select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1
	Begin
		Select @FinalQuery='
		With CTE1 as (
		Select * from (
		select
		convert(varchar(50),folderId ) as FieldValue1,
		convert(varchar(150),folderName) as FieldValue2,
		Row_Number() Over('+@v_fieldName7Vc+') as RowNumber
		from tbl_TendererFolderMaster c,
			 tbl_TendererMaster t
		where c.tendererId=t.tendererId
			and userId in (
						select userId from tbl_TendererMaster
								where companyId =
								(select companyId from tbl_TendererMaster where userId='+@v_fieldName3Vc+' and companyId!=1)
						)'
				+ @v_ConditionString_Vc +
		' ) as Tmp
	)
	select *, (select CONVERT(varchar(50), COUNT(*)) from CTE1) as FieldValue12 from CTE1
	WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
	+ ' Order by RowNumber'

	End
	Else
	Begin
	Select @FinalQuery='
	With CTE1 as (
	Select * from (
	select
		convert(varchar(50),folderId ) as FieldValue1,
		convert(varchar(150),folderName) as FieldValue2,
		Row_Number() Over('+@v_fieldName7Vc+') as RowNumber
		from tbl_TendererFolderMaster c,tbl_TendererMaster t
		where c.tendererId=t.tendererId and userId=' + @v_fieldName3Vc
		+ @v_ConditionString_Vc +
			' ) as Tmp
	)
	select *, (select CONVERT(varchar(50), COUNT(*)) from CTE1) as FieldValue12 from CTE1
	WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
	+ ' Order by RowNumber'


	End
	 print (@FinalQuery)
	 Exec (@FinalQuery)



END

IF @v_fieldName1Vc = 'GetAllDocumentsWithPaging'
BEGIN

/*
	 Start: Input Params
	 @v_fieldName1Vc =keyword,
	 @v_fieldName2Vc =FolderId,
	 @v_fieldName3Vc =UserId (Session id),
	 @v_fieldName4Vc =PageNo,
	 @v_fieldName5Vc =RecordSize,
	 @v_fieldName6Vc =SearchText,
	 @v_fieldName7Vc =SOrtVal
	 End: Input Params
	 */

	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName4Vc, @v_RecordPerPage_inInt=@v_fieldName5Vc

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */

	/*Start: Dynamic Condition string*/
	SET @v_ConditionString_Vc=''
	If @v_fieldName6Vc is not null And @v_fieldName6Vc<>''
	Begin
		Set @v_ConditionString_Vc = ' And ' + @v_fieldName6Vc
	End

 If (select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1
 Begin

  select @v_FormList_Vc = COALESCE(@v_FormList_Vc+',', ' ') + convert(varchar(20), formid)
   from tbl_biddocuments where userId=@v_fieldName3Vc
   set @v_FormList_Vc=ISNULL(@v_FormList_Vc,'')
	-- , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), '' '', ''-'')   as FieldValue5
	Select @FinalQuery='
	With CTE1 as (
	Select * from (
  select
  convert(varchar(50),a.companyDocId ) as FieldValue1,
 convert(varchar(150),documentName) as FieldValue2
      , convert(varchar(150),documentSize) as FieldValue3
      , convert(varchar(250),documentBrief) as FieldValue4
      ,ISNULL(REPLACE(CONVERT(VARCHAR(11), uploadedDate, 106), '' '', ''-'') + '' '' +SUBSTRING(CONVERT(VARCHAR(5), uploadedDate,108), 1, 5), ''-'') as FieldValue5
      , convert(varchar(50),docstatus) as FieldValue6
      , convert(varchar(50),b.companyDocId ) as FieldValue7
      ,convert(varchar(50),a.folderId ) as FieldValue8
       , convert(varchar(50),isnull(c.foldername,''-'') ) as FieldValue9
        ,documentTypeId as FieldValue11
        ,IsNull(docHash,'''') as FieldValue13
        ,case when b.companyDocId is not null then '''+@v_FormList_Vc+''' else null end  as FieldValue10
        ,Row_Number() Over('+@v_fieldName7Vc+') as RowNumber

		  from (select c.* from tbl_CompanyDocuments c,
					tbl_TendererMaster t
					where c.tendererId=t.tendererId and
					userId in (
						select userId from tbl_TendererMaster
						where companyId =
							(select companyId from tbl_TendererMaster where userId='+@v_fieldName3Vc+' and companyId!=1)
						)'
						+ @v_ConditionString_Vc +

		'and docstatus!=''archive'' ) a
	left outer join
	(
		select distinct companyDocId from tbl_biddocuments where
		userId in (
					select userId from tbl_TendererMaster
					where companyId =
					(select companyId from tbl_TendererMaster where userId='+@v_fieldName3Vc+' and companyId!=1)

					)
	) b
	on a.companyDocId=b.companydocid
	 left outer join
	(
		select distinct t.tendererId,folderName,folderid from tbl_TendererFolderMaster t,tbl_tenderermaster m
			where t.tendererId=m.tendererId
			and  m.userId in (
				select userId from tbl_TendererMaster
					where companyId =
					(select companyId from tbl_TendererMaster where userId='+@v_fieldName3Vc+' and companyId!=1)

			)
		) c
	on a.tendererId=c.tendererId and a.folderId=c.folderId
	) as Tmp
	)
	select *, (select CONVERT(varchar(50), COUNT(*)) from CTE1) as FieldValue12 from CTE1
	WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
	+ ' Order by RowNumber'

 End
 Else
 Begin
	 select @v_FormList_Vc = COALESCE(@v_FormList_Vc+',', ' ') + convert(varchar(20), formid)
 from tbl_biddocuments where userId=@v_fieldName3Vc

   Select @FinalQuery='
    With CTE1 as (
   Select * from (
 select
  convert(varchar(50),a.companyDocId ) as FieldValue1,
 convert(varchar(150),documentName) as FieldValue2
      , convert(varchar(150),documentSize) as FieldValue3
      , convert(varchar(250),documentBrief) as FieldValue4
      ,ISNULL(REPLACE(CONVERT(VARCHAR(11), uploadedDate, 106), '' '', ''-'') + '' '' +SUBSTRING(CONVERT(VARCHAR(5), uploadedDate,108), 1, 5), ''-'') as FieldValue5
      , convert(varchar(50),docstatus) as FieldValue6
      , convert(varchar(50),b.companyDocId ) as FieldValue7
      ,convert(varchar(50),a.folderId ) as FieldValue8
        ,convert(varchar(50),isnull(c.foldername,''-'') ) as FieldValue9
        ,documentTypeId as FieldValue11
        ,IsNull(docHash,'''') as FieldValue13
          ,case when b.companyDocId is not null then '''+@v_FormList_Vc+''' else null end  as FieldValue10
          ,Row_Number() Over('+@v_fieldName7Vc+') as RowNumber

      from (select c.* from tbl_CompanyDocuments c,
				tbl_TendererMaster t
					where c.tendererId=t.tendererId and userId='+@v_fieldName3Vc+' and docstatus!=''archive'' '
					+ @v_ConditionString_Vc +
			') a
left outer join
(select distinct companyDocId from tbl_biddocuments where userId='+@v_fieldName3Vc+') b
on a.companyDocId=b.companydocid
 left outer join
(select distinct t.tendererId,folderName,folderid from tbl_TendererFolderMaster t,tbl_tenderermaster m
 where t.tendererId=m.tendererId and  m.userId='+@v_fieldName3Vc+' ) c
on a.tendererId=c.tendererId and a.folderId=c.folderId
) as Tmp
	)
	select *, (select CONVERT(varchar(50), COUNT(*)) from CTE1) as FieldValue12 from CTE1
	WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
	+ ' Order by RowNumber'


 End

 print (@FinalQuery)
	 Exec (@FinalQuery)

END

IF @v_fieldName1Vc = 'GetArchiveDocumentsWithPaging'
BEGIN
--declare @v_FormList_Vc3 varchar(2000)

/*
	 Start: Input Params
	 @v_fieldName1Vc =keyword,
	 @v_fieldName2Vc =FolderId,
	 @v_fieldName3Vc =UserId (Session id),
	 @v_fieldName4Vc =PageNo,
	 @v_fieldName5Vc =RecordSize,
	 @v_fieldName6Vc =SearchText,
	 @v_fieldName7Vc =SOrtVal
	 End: Input Params
	 */

	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName4Vc, @v_RecordPerPage_inInt=@v_fieldName5Vc

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */

	/*Start: Dynamic Condition string*/
	SET @v_ConditionString_Vc=''
	If @v_fieldName6Vc is not null And @v_fieldName6Vc<>''
	Begin
		Set @v_ConditionString_Vc = ' And ' + @v_fieldName6Vc
	End

	If (select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1
	Begin
					 select @v_FormList_Vc3 = COALESCE(@v_FormList_Vc3+',', ' ') + convert(varchar(20), formid)
		 from tbl_biddocuments where userId=@v_fieldName3Vc
		 set @v_FormList_Vc3=isnull(@v_FormList_Vc3,'')

		 Select @FinalQuery='
		 With CTE1 as (
		 Select * from (
	 select
		  convert(varchar(50),a.companyDocId ) as FieldValue1,
		 convert(varchar(150),documentName) as FieldValue2
			  , convert(varchar(150),documentSize) as FieldValue3
			  , convert(varchar(250),documentBrief) as FieldValue4
			  ,ISNULL(REPLACE(CONVERT(VARCHAR(11), uploadedDate, 106), '' '', ''-'') + '' '' +SUBSTRING(CONVERT(VARCHAR(5), uploadedDate,108), 1, 5), ''-'') as FieldValue5
			  , convert(varchar(50),docstatus) as FieldValue6
			  , convert(varchar(50),b.companyDocId ) as FieldValue7
			  ,IsNull(docHash,'''') as FieldValue13
			   , case when b.companyDocId is not null then '''+@v_FormList_Vc3+''' else null end as FieldValue10
			   , documentTypeId as FieldValue11,
			   Row_Number() Over('+@v_fieldName7Vc+') as RowNumber
			  from (
					select c.* from tbl_CompanyDocuments c,
							tbl_TendererMaster t
					where c.tendererId=t.tendererId
					and docstatus=''archive''
					and userId in (
							select userId from tbl_TendererMaster
								where companyId =
								(select companyId from tbl_TendererMaster where userId='+@v_fieldName3Vc+' and companyId!=1)
							)'
							+ @v_ConditionString_Vc +

					') a
		left outer join
		(
			select distinct companyDocId  from tbl_biddocuments where
			userId in (
						select userId from tbl_TendererMaster
							where companyId =
							(select companyId from tbl_TendererMaster where userId='+@v_fieldName3Vc+' and companyId!=1)

					)
		) b
		on a.companyDocId=b.companydocid

		) as Tmp
	)
	select *, (select CONVERT(varchar(50), COUNT(*)) from CTE1) as FieldValue12 from CTE1
	WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
	+ ' Order by RowNumber'

	End
	Else
	Begin
			 select @v_FormList_Vc3 = COALESCE(@v_FormList_Vc3+',', ' ') + convert(varchar(20), formid)
 from tbl_biddocuments where userId=@v_fieldName3Vc

 Select @FinalQuery='
 With CTE1 as (
 Select * from (
 select
  convert(varchar(50),a.companyDocId ) as FieldValue1,
 convert(varchar(150),documentName) as FieldValue2
      , convert(varchar(150),documentSize) as FieldValue3
      , convert(varchar(250),documentBrief) as FieldValue4
      ,ISNULL(REPLACE(CONVERT(VARCHAR(11), uploadedDate, 106), '' '', ''-'') + '' '' +SUBSTRING(CONVERT(VARCHAR(5), uploadedDate,108), 1, 5), ''-'') as FieldValue5
      , convert(varchar(50),docstatus) as FieldValue6
      , convert(varchar(50),b.companyDocId ) as FieldValue7
       ,IsNull(docHash,'''') as FieldValue13
       , case when b.companyDocId is not null then '''+@v_FormList_Vc3+''' else null end as FieldValue10
       , documentTypeId as FieldValue11
       ,Row_Number() Over('+@v_fieldName7Vc+') as RowNumber
      from (	select c.* from tbl_CompanyDocuments c,
					tbl_TendererMaster t
					where c.tendererId=t.tendererId and userId='+@v_fieldName3Vc+' and docstatus=''archive'' '
				+ @v_ConditionString_Vc +
			') a
left outer join
(select distinct companyDocId  from tbl_biddocuments where  userId='+@v_fieldName3Vc+') b
on a.companyDocId=b.companydocid
) as Tmp
	)
	select *, (select CONVERT(varchar(50), COUNT(*))  from CTE1) as FieldValue12 from CTE1
	WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
	+ ' Order by RowNumber'

	End

	 print (@FinalQuery)
	 Exec (@FinalQuery)
END

IF @v_fieldName1Vc = 'GetBidderDocumentsWithPaging'
BEGIN

/*
	 Start: Input Params
	 @v_fieldName1Vc =keyword,
	 @v_fieldName2Vc =FolderId,
	 @v_fieldName3Vc =UserId (Session id),
	 @v_fieldName4Vc =PageNo,
	 @v_fieldName5Vc =RecordSize,
	 @v_fieldName6Vc =SearchText,
	 @v_fieldName7Vc =SOrtVal
	 End: Input Params
	 */

	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName4Vc, @v_RecordPerPage_inInt=@v_fieldName5Vc

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */

	/*Start: Dynamic Condition string*/
	SET @v_ConditionString_Vc=''
	If @v_fieldName6Vc is not null And @v_fieldName6Vc<>''
	Begin
		Set @v_ConditionString_Vc = ' And ' + @v_fieldName6Vc
	End

Select @FinalQuery='
select
  convert(varchar(50),a.companyDocId ) as FieldValue1,
 convert(varchar(150),documentName) as FieldValue2
      , convert(varchar(150),documentSize) as FieldValue3
      , convert(varchar(250),documentBrief) as FieldValue4
      ,ISNULL(REPLACE(CONVERT(VARCHAR(11), uploadedDate, 106), '' '', ''-'') + '' '' +SUBSTRING(CONVERT(VARCHAR(5), uploadedDate,108), 1, 5), ''-'') as FieldValue5
      , convert(varchar(50),docstatus) as FieldValue6
       ,  convert(varchar(50),formId)  as FieldValue10
        , documentTypeId as FieldValue11
        ,case manDocId
			when 0 then ''-''
			else (select documentName from tbl_TenderMandatoryDoc tmd where tmd.tenderFormDocId=manDocId)
        end as FieldValue12
      ,IsNull(docHash,'''') as FieldValue13
      from
		(select c.*,formid,b.manDocId from tbl_CompanyDocuments c,
				tbl_TendererMaster t,tbl_biddocuments b
				where c.tendererId=t.tendererId and t.userId='+@v_fieldName2Vc+'
				and b.companyDocId=c.companyDocId
			and formId='+@v_fieldName3Vc+''
			+ @v_ConditionString_Vc +
			') a'

	 print (@FinalQuery)
	 Exec (@FinalQuery)

END

IF @v_fieldName1Vc = 'GetFolders'
BEGIN
	If (select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1
	Begin
		select
		convert(varchar(50),folderId ) as FieldValue1,
		convert(varchar(150),folderName) as FieldValue2
		from tbl_TendererFolderMaster c,tbl_TendererMaster t
		where c.tendererId=t.tendererId
		and userId in (
			select userId from tbl_TendererMaster
				where companyId =
				(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)

		)
	End
	Else
	Begin
		select
		convert(varchar(50),folderId ) as FieldValue1,
		convert(varchar(150),folderName) as FieldValue2
		from tbl_TendererFolderMaster c,tbl_TendererMaster t
		where c.tendererId=t.tendererId and userId=@v_fieldName3Vc
	End


END


if @v_fieldName1Vc='isAllBidderEvaluated'

begin
declare @v_docAvlMethod_Vc varchar(30)

	
SELECT @v_docAvlMethod_Vc = docAvlMethod FROM dbo.tbl_TenderDetails WHERE tenderId = @v_fieldName2Vc
if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
	SET @evalCount = 0
else
	select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

	IF @v_docAvlMethod_Vc = 'Lot'
	BEGIN

		select case when
	(select COUNT(userId) from tbl_EvalBidderStatus  where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc and evalCount =@evalCount)=
	(select COUNT(distinct f.userId) from tbl_FinalSubmission f,tbl_BidderLots b where f.tenderId=@v_fieldName2Vc
	and f.tenderId=b.tenderId and f.userId=b.userid and pkgLotId =@v_fieldName3Vc
	and bidSubStatus='finalsubmission') then '1' else '0' end as FieldValue1

	END
	ELSE
	BEGIN
	IF((select  COUNT(tbl_noaissuedetails.userId)  from tbl_noaacceptance inner join tbl_noaissuedetails 
		on tbl_noaissuedetails.noaissueId = tbl_noaacceptance.noaissueId where tenderId = @v_fieldName2Vc and acceptRejStatus in ('decline','Performance Security not paid')
		) > 0 and @evalCount >0 )
		BEGIN
		 select @field1 = COUNT(evaBidderStatusId) from tbl_EvalBidderStatus ebs,tbl_FinalSubmission f--,tbl_NoaIssueDetails tnid 
			where ebs.tenderId = f.tenderId and ebs.evalCount = @evalCount and f.tenderId = @v_fieldName2Vc and f.userId = ebs.userId
			and ebs.tenderId  = @v_fieldName2Vc and ebs.userId not in (select userId from tbl_NoaIssueDetails where tenderId = @v_fieldName2Vc)
		select @field2 = count(userId) from tbl_NoaIssueDetails where tenderId = @v_fieldName2Vc

		select case when
		(Convert(int,@field1)+Convert(int,@field2))=
		(select COUNT(userId) from tbl_FinalSubmission where tenderId=@v_fieldName2Vc
		and bidSubStatus='finalsubmission') then '1' else '0' end as FieldValue1
		END
	else
		BEGIN
		select case when
		(select COUNT(userId) from tbl_EvalBidderStatus where tenderId=@v_fieldName2Vc and evalCount =@evalCount)=
		(select COUNT(userId) from tbl_FinalSubmission where tenderId=@v_fieldName2Vc
		and bidSubStatus='finalsubmission') then '1' else '0' end as FieldValue1
		END
	END

end
if @v_fieldName1Vc='getNoteofDissent'
begin
	select em.employeeName  as FieldValue1,ts.comments as FieldValue2 from
	tbl_TORRptSign ts inner join tbl_EmployeeMaster  em on em.userId = ts.userId
	where
	ts.tenderId=@v_fieldName2Vc and ts.pkgLotId=@v_fieldName3Vc and ts.aggDisAggStatus='I Disagree' and ts.reportType=@v_fieldName4Vc
end

IF @v_fieldName1Vc = 'GetBidderDocuments'
BEGIN

-- select
--  convert(varchar(50),a.companyDocId ) as FieldValue1,
-- convert(varchar(150),documentName) as FieldValue2
--      , convert(varchar(150),documentSize) as FieldValue3
--      , convert(varchar(250),documentBrief) as FieldValue4
--      , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), ' ', '-')   as FieldValue5
--      , convert(varchar(50),docstatus) as FieldValue6
--       ,  convert(varchar(50),formId)  as FieldValue10
--        , documentTypeId as FieldValue11
--      from (select c.*,formid from tbl_CompanyDocuments c,tbl_TendererMaster t,tbl_biddocuments b
--where c.tendererId=t.tendererId and t.userId=@v_fieldName2Vc  and b.companyDocId=c.companyDocId
--and formId=@v_fieldName3Vc ) a

select
  convert(varchar(50),a.companyDocId ) as FieldValue1,
 convert(varchar(150),documentName) as FieldValue2
      , convert(varchar(150),documentSize) as FieldValue3
      , convert(varchar(250),documentBrief) as FieldValue4
      , ISNULL(REPLACE(CONVERT(VARCHAR(11), uploadedDate, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), uploadedDate,108), 1, 5), '-')   as FieldValue5
      , convert(varchar(50),docstatus) as FieldValue6
      ,IsNull(docHash,'') as FieldValue7
       ,  convert(varchar(50),formId)  as FieldValue10
        , documentTypeId as FieldValue11 ,IsNull(docHash,'''') as FieldValue13
        ,case manDocId
        when 0 then 'Other'
        else (select documentName from tbl_TenderMandatoryDoc tmd where tmd.tenderFormDocId=manDocId)
        end as FieldValue12
      from (select c.*,formid,b.manDocId from tbl_CompanyDocuments c,tbl_TendererMaster t,tbl_biddocuments b
where c.tendererId=t.tendererId and t.userId=@v_fieldName2Vc  and b.companyDocId=c.companyDocId
and formId=@v_fieldName3Vc) a


END
--change by dohatec for re-evaluation
if @v_fieldName1Vc='getPostQualifiedBidders'
begin
select case when tm.companyId!=1 then companyname else  title+' '+tm.firstName+' '+tm.lastName end as FieldValue1,postQualStatus as FieldValue2
	 from tbl_PostQualification pq,tbl_CompanyMaster cm,tbl_TendererMaster tm
	 where tm.companyId=cm.companyId and tm.userId=pq.userId and tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
	 and pq.postQualStatus!='Initiated' and pq.userId in (select userId from tbl_EvalRoundMaster where roundId = @v_fieldName4Vc) and pq.evalCount = @v_fieldName5Vc
END

--change by dohatec for re-evaluation
if @v_fieldName1Vc='getQualifiedBidders'
begin
	IF ((@v_fieldName4Vc = 'Services') OR (select procurementMethod from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='DPM')
	Begin
			select dbo.f_getbiddercompany(nt.userId) as FieldValue1
			from tbl_Negotiation n inner join tbl_NegNotifyTenderer nt on n.negId = nt.negId
			where tenderId=@v_fieldName2Vc and n.negStatus='Successful' and n.evalCount = @v_fieldName5Vc
	End
	Else
	Begin
			select case when tm.companyId!=1 then companyname else  title+' '+tm.firstName+' '+tm.lastName end as FieldValue1
			 from tbl_PostQualification pq,tbl_CompanyMaster cm,tbl_TendererMaster tm
			 where tm.companyId=cm.companyId and tm.userId=pq.userId and tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
			 and postQualStatus='Qualify' and pq.evalCount = @v_fieldName5Vc --and noaStatus = 'pending'
	End
END


if @v_fieldName1Vc='isTERSignedByAllMem'
BEGIN
 select case when ((select COUNT(cm.userId) from tbl_Committee c inner join tbl_CommitteeMembers cm on
c.committeeId = cm.committeeId and c.tenderId=@v_fieldName2Vc and c.committeStatus='approved' and c.committeeType in ('TEC','PEC'))
=
(select COUNT(userId) from tbl_TORRptSign where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc and reportType=@v_fieldName4Vc))
then '1' else '0' end as FieldValue1
END

if @v_fieldName1Vc='checkDuplicateDocument'
BEGIN
	SELECt	CD.documentName as FieldValue1,
			CONVERT(VARCHAR(15),bd.companyDocId) as FieldValue2
	FROM	tbl_BidDocuments bd
			INNER JOIN tbl_TendererMaster TM on tM.userId = bd.userId
			INNER JOIN tbl_CompanyDocuments CD on CD.tendererId = TM.tendererId
						AND CD.companyDocId = bd.companyDocId
	where	bd.tenderId = CONVERT(INT,@v_fieldName2Vc) and
			bd.userId = CONVERT(INT,@v_fieldName3Vc) and
			bd.formId = CONVERT(INT,@v_fieldName4Vc) and
			bd.companyDocId = CONVERT(INT,@v_fieldName5Vc)
	order by bidDocId desc

END

IF @v_fieldName1Vc='GetPMethodEstCostTender'
BEGIN
	select FieldValue1,FieldValue2 FROM
	(select tenderId,procurementMethod as FieldValue1 from tbl_TenderDetails where tenderId=@v_fieldName2Vc) T1 INNER JOIN 
	(select tenderId,convert(varchar(20),estCost) as FieldValue2 from tbl_TenderEstCost where tenderId=@v_fieldName2Vc) T2
	ON T1.tenderId = T2.tenderId
END
IF @v_fieldName1Vc='getBidderPostQualification'
BEGIN
 select postQualStatus as FieldValue1,convert(varchar(50),userId) as FieldValue2 from tbl_PostQualification where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
End
IF @v_fieldName1Vc='getMiscPaymentReport'
BEGIN
	declare @fnlqry varchar(max);
	declare @conditionqry varchar(max);
	declare @paginationQry varchar(max);
	DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;

	set @fnlqry = ''
	set @conditionqry = ''
		/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=cast(@v_fieldName2Vc as int), @v_RecordPerPage_inInt=cast(@v_fieldName3Vc as int)

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	set @paginationQry = ') as vtable
	where RowNum between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	--@v_fieldName1Vc : getMiscPaymentReport",
	--@v_fieldName2Vc : strPageNo, strOffset, emailId,sortEmailId, paymentDate, payeeName, payeeNameSearch, Amount,amountSearch

	if (@v_fieldName4Vc != '')
	begin
		if(@v_fieldName5Vc = '=')
		begin
			set @conditionqry = @conditionqry + ' where optran.emailId' + @v_fieldName5Vc + '''' + @v_fieldName4Vc + ''''
		end
		if(@v_fieldName5Vc = 'Like')
		begin
			set @conditionqry = @conditionqry + ' where optran.emailId ' + @v_fieldName5Vc + '''%' + @v_fieldName4Vc + '%'''
		end
	end

	if(@v_fieldName6Vc != '' )
	begin
		if(@v_fieldName4Vc != '')
		begin
			set @conditionqry = @conditionqry + ' and Cast (Floor(Cast (regvas.dtofPayment as Float)) as Datetime) = ' + '''' + @v_fieldName6Vc + ''''
		end
		else
		begin
			set @conditionqry = @conditionqry + ' where Cast (Floor(Cast (regvas.dtofPayment as Float)) as Datetime) = ' + '''' + @v_fieldName6Vc + ''''
		end
	end
	if(@v_fieldName7Vc != '' )
	begin
		if(@v_fieldName4Vc != '' AND @v_fieldName6Vc != '' )
		begin
			set @conditionqry = @conditionqry + ' and regvas. payeeName '+ @v_fieldName8Vc + '''%' + @v_fieldName7Vc + '%'''
		end
		else
		begin
			set @conditionqry = @conditionqry + ' where regvas. payeeName '+ @v_fieldName8Vc + '''%' + @v_fieldName7Vc + '%'''
		end
	end
	if(@v_fieldName9Vc != '')
	begin
		if(@v_fieldName4Vc != '' AND @v_fieldName6Vc != '' AND @v_fieldName7Vc != '')
		begin
			set @conditionqry = @conditionqry + ' and optran.Amount '+ @v_fieldName10Vc + @v_fieldName9Vc
		end
		else
		begin
			set @conditionqry = @conditionqry + ' where optran.Amount '+ @v_fieldName10Vc + @v_fieldName9Vc
		end
	end

	Select @v_PgCntQry_Vc= 'SELECT Ceiling(COUNT( Distinct paymnetId )/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')
	from tbl_OnlinePaymentTransDetails optran
	inner join tbl_RegVasfeePayment regvas on regvas.transId = optran.TransId
	and regvas.cardNo is not null and regvas.dtofPayment is not null ' + @conditionqry

	Select @v_CntQry_Vc='SELECT Count(Distinct paymnetId)
	from tbl_OnlinePaymentTransDetails optran
	inner join tbl_RegVasfeePayment regvas on regvas.transId = optran.TransId
	and regvas.cardNo is not null and regvas.dtofPayment is not null ' + @conditionqry



	select @fnlqry =
	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
		select FieldValue1,FieldValue2,FieldValue3,FieldValue4,FieldValue5,FieldValue6,FieldValue7,
	FieldValue8,FieldValue9,FieldValue11,FieldValue12 from
(
	select
	CONVERT(VARCHAR(20),regvas.paymnetId) as  FieldValue1,
	CONVERT(VARCHAR(20),optran.userId) as FieldValue2,
	CONVERT(VARCHAR(20),regvas.dtofPayment,103) as FieldValue3,
	regvas.payeeName as FieldValue4,
	regvas. payeeAddress as FieldValue5,
	optran.Description as FieldValue6,
	optran.emailId as FieldValue7,
	CONVERT(VARCHAR(20),optran.Amount) as FieldValue8,
	regvas.remarks as  FieldValue9,
	CONVERT(VARCHAR(20),@v_TotalPageCnt_Int) as FieldValue11,
	CONVERT(VARCHAR(20),@v_TotalRecordCnt_Int) as FieldValue12,
	ROW_NUMBER() OVER (ORDER BY regvas.paymnetId DESC) AS RowNum


	from

	tbl_OnlinePaymentTransDetails optran
	inner join tbl_RegVasfeePayment regvas on regvas.transId = optran.TransId
	and regvas.cardNo is not null and regvas.dtofPayment is not null '

--	set @fnlQry = 'with virtualtable as ( ' + @fnlQry + @conditionqry + @paginationQry
set @fnlQry =  @fnlQry + @conditionqry + @paginationQry
--optran.PaymentFor = 'fromvas' and optran.TransactionStatus = 'Successful'
		 print (@fnlQry)
		 exec (@fnlQry)
END
IF @v_fieldName1Vc='getMiscPaymentReportDetails'
BEGIN

	select regvas.payeeName as FieldValue1,
	regvas. payeeAddress as FieldValue2,
	optran.emailId as FieldValue3,
	optran.Description as FieldValue4,
	regvas.bankName as FieldValue5,
	CONVERT(VARCHAR(20),regvas.cardNo) as FieldValue6,
	CONVERT(VARCHAR(20),regvas.transId) as FieldValue7,
	regvas.currancy as FieldValue8,
	CONVERT(VARCHAR(20),optran.Amount) as FieldValue9,
	CONVERT(VARCHAR(20),optran.serviceChargeAmt) as FieldValue10,
	CONVERT(VARCHAR(20),optran.TotalAmount) as FieldValue11,
	regvas.paymentType as FieldValue12,
	CONVERT(VARCHAR(20),regvas.dtofPayment,103) as FieldValue13,
	regvas.remarks as  FieldValue14,
	CONVERT(VARCHAR(20),regvas.paymnetId) as  FieldValue15,
	CONVERT(VARCHAR(20),optran.userId) as FieldValue16

	from tbl_OnlinePaymentTransDetails optran
	inner join tbl_RegVasfeePayment regvas on regvas.transId = optran.TransId
	and regvas.cardNo is not null and regvas.dtofPayment is not null
	where regvas.paymnetId = @v_fieldName2Vc
END
if @v_fieldName1Vc='isTERSentForReview'
begin
	select convert(varchar(50),COUNT(evalFFRptId)) as FieldValue1 from 
	tbl_EvalRptForwardToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc 
	and roundId = @v_fieldName4Vc
end

if @v_fieldName1Vc='isEvaluationWorkFlowCreated'
begin
	SELECT     worfklowId FROM         tbl_WorkFlowLevelConfig
	WHERE     (eventId = 12) AND (moduleId = 2) AND (activityId = 13) AND 
	(objectId = @v_fieldName2Vc) 
end

-- by dohatec for checking upload document
If @v_fieldName1Vc='getTendererDocumentNameByDocumentId'
Begin	 		
	 select documentName  AS FieldValue1 , documentSize AS FieldValue2  
	 from tbl_CompanyDocuments where companyDocId = @v_fieldName2Vc 
     and tendererId = ( select tendererId from tbl_TendererMaster tm 
     where tm.userId = @v_fieldName3Vc )	           	
END
IF @v_fieldName1Vc = 'getTenderPaymentListingForReport'
	BEGIN
	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName2Vc, @v_RecordPerPage_inInt=@v_fieldName3Vc-1

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */



	SET @v_ConditionString_Vc=' And isLive=''yes'''
	Select @v_BankUserRole_Vc=isMakerChecker,
		   @v_BankBranchId = sBankDevelopId
	From tbl_PartnerAdmin Where userId=@v_fieldName4Vc

	Select @v_BankId = Case sBankDevelHeadId
	When 0 Then @v_BankBranchId Else sBankDevelHeadId End
	From tbl_ScBankDevPartnerMaster Where sBankDevelopId=@v_BankBranchId



	--If @v_BankUserRole_Vc='BranchChecker'
	--Begin
	--	--Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And tenderPaymentId in (Select tenderPaymentId From tbl_TenderPayment Where createdBy in (select distinct userId from tbl_PartnerAdmin where sBankDevelopId= '+Convert(varchar(50), @v_BankBranchId)+'))'
	--	--dohatec start
	--	Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And tenderPaymentId in (Select tenderPaymentId From tbl_TenderPayment Where branchName in (select sbDevelopName from tbl_ScBankDevPartnerMaster where sBankDevelopId= '+Convert(varchar(50), @v_BankBranchId)+') and bankName in (select sbDevelopName from tbl_ScBankDevPartnerMaster where sBankDevelopId = (select sBankDevelHeadId from tbl_ScBankDevPartnerMaster where sBankDevelopId = '+Convert(varchar(50), @v_BankBranchId)+')))'
	--	--dohatec end

	--End
	--Else If @v_BankUserRole_Vc='BankChecker'
	--Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And tenderPaymentId in (Select tenderPaymentId From tbl_TenderPayment Where createdBy in (select distinct userId from tbl_PartnerAdmin where sBankDevelopId in (select distinct sBankDevelopId from tbl_ScBankDevPartnerMaster Where sBankDevelHeadId='+Convert(varchar(50), @v_BankId)+')))'
	--End

	If @v_fieldName5Vc<>''
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And emailId=''' + @v_fieldName5Vc +''''
	End

	If @v_fieldName6Vc<>'' -- Verified status
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And isVerified=''' + @v_fieldName6Vc +''''
	End

	If @v_fieldName7Vc<>'' -- Branch Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderPaymentId in (Select tenderPaymentId From tbl_TenderPayment Where createdBy in (select distinct userId from tbl_PartnerAdmin where sBankDevelopId='+@v_fieldName7Vc+'))'
	End

	If @v_fieldName8Vc<>'' -- Branch Member Id
	Begin
		--Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And createdBy=''' + @v_fieldName8Vc +''''
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And partTransId=''' + @v_fieldName8Vc +''''
	End

	--Print (@v_fieldName9Vc)
	--Print (@v_fieldName10Vc)
	IF (@v_fieldName9Vc is not null And @v_fieldName9Vc <>'') And (@v_fieldName10Vc is not null And @v_fieldName10Vc <>'')
	Begin
		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (createdDate as Float)) as Datetime) >= ''' + @v_fieldName9Vc + ''' And Cast (Floor(Cast (createdDate as Float)) as Datetime) <= ''' + @v_fieldName10Vc + ''''
	End
	ELSE IF (@v_fieldName9Vc is not null And @v_fieldName9Vc <>'') And (@v_fieldName10Vc is null OR @v_fieldName10Vc='')
	Begin
		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (createdDate as Float)) as Datetime) >= ''' + @v_fieldName9Vc + ''''
	End
	ELSE IF (@v_fieldName9Vc is not null Or @v_fieldName9Vc <>'') And (@v_fieldName10Vc is not null And @v_fieldName10Vc <>'')
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And Cast (Floor(Cast (createdDate as Float)) as Datetime) <= ''' + @v_fieldName10Vc + ''''
	End


	If @v_fieldName11Vc is Not Null And @v_fieldName11Vc<>'' -- Tender Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And tenderId=''' + @v_fieldName11Vc +''''
	End

	If @v_fieldName12Vc is Not Null And  @v_fieldName12Vc<>'' -- Tenderer Name
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And dbo.f_getbiddercompany(LM.userId)=''' + @v_fieldName12Vc +''''
	End

	If @v_fieldName13Vc is Not Null And  @v_fieldName13Vc<>'' And @v_fieldName13Vc<>'All' -- Payment For
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And paymentFor=''' + @v_fieldName13Vc +''''
	End

	--Print('Condition String: ')
	--Print(@v_ConditionString_Vc)

	-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
	Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT( Distinct tenderPaymentId ) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')
	From tbl_TenderPayment TP
	Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		WHERE tenderPaymentId is Not Null ' + @v_ConditionString_Vc

	Select @v_CntQry_Vc='SELECT Count(Distinct tenderPaymentId)
	From tbl_TenderPayment TP
	Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		WHERE tenderPaymentId is Not Null ' + @v_ConditionString_Vc


			/* START CODE: DYNAMIC QUERY TO GET RECORDS */

			/*
				FieldValue1: Payment Id
				FieldValue2: User Id of Bidder for which Payment is done
				FieldValue3: Email Id of Bidder for which Payment is done
				FieldValue4: Branch Maker - Name of Bank User who has done the Payment
				FieldValue5: Payment Status New Payment/Renewal
				FieldValue6: Date of Payment
			*/

	Print ('PageCount')
	Print (@v_PgCntQry_Vc)

	Print ('RowCount')
	Print (@v_CntQry_Vc)

		Select @v_FinalQueryVc=
	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
	--With CTE as(
	Select FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6,FieldValue7, FieldValue8,
	FieldValue9, FieldValue10, FieldValue11, FieldValue12, FieldValue13, FieldValue14, FieldValue15, FieldValue16,
	FieldValue17--,tenderPaymentId
	From
	(Select Distinct
		Convert(varchar(50),Tmp.tenderPaymentId) as FieldValue1,
			Convert(varchar(50),LM.userId) as FieldValue2,
			currency as FieldValue3,
			PA.fullName as FieldValue4,
			TP.status as FieldValue5,
			REPLACE(CONVERT(VARCHAR(11),createdDate, 106), '' '', ''-'')+ '' '' + Substring(CONVERT(VARCHAR(30),createdDate,108),1,5) as FieldValue6,
			CONVERT(varchar(50),@v_TotalPageCnt_Int) as FieldValue7,
			CONVERT(varchar(50),@v_TotalRecordCnt_Int) as FieldValue8,
			CONVERT(varchar(50),Tmp.RowNumber) as FieldValue9,
			isVerified as FieldValue10,
			branchName as FieldValue11,
			paymentFor as FieldValue12,
			Convert(varchar(50),amount) as FieldValue13,
			Convert(varchar(50),tenderId) as FieldValue14,
			Convert(varchar(50),pkgLotId) as FieldValue15,
			Case paymentFor
				When ''Document Fees'' Then ''df''
				When ''Tender Security'' Then ''ts''
				When ''Performance Security'' Then ''ps''
				When ''Bank Guarantee'' Then ''bg''
				End as FieldValue16,
			IsNull((select top 1 ('' , '' + lotNo) from tbl_TenderLotSecurity where tenderId=TP.tenderId and appPkgLotId=TP.pkgLotId ),'''') as FieldValue17,
			Tmp.tenderPaymentId,
			sortParam
		From
		(Select RowNumber, tenderPaymentId, sortParam From
			(SELECT ROW_NUMBER() OVER (Order by tenderPaymentId desc) as RowNumber, tenderPaymentId,tenderPaymentId as sortParam
				From tbl_TenderPayment TP
				Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
				Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		WHERE tenderPaymentId is Not Null '
					+@v_ConditionString_Vc+
			') B

		WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+ ' ) Tmp
		Inner Join tbl_TenderPayment TP On Tmp.tenderPaymentId=TP.tenderPaymentId
			Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
			Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
		) as A --)
		--SELECT T.FieldValue1,T.FieldValue2,T.FieldValue3,T.FieldValue4,T.FieldValue5, 
		--T.FieldValue6,T.FieldValue7,T.FieldValue8,T.FieldValue9,T.FieldValue10,
		--T.FieldValue11,T.FieldValue12,T.FieldValue13,T.FieldValue14,T.FieldValue15,
		--T.FieldValue16,T.FieldValue17
		--FROM
		--(select * from CTE 
		--union all
		--select '''' as FieldValue1,'''' as  FieldValue2,'''' as  FieldValue3, '''' as FieldValue4, '''' as FieldValue5, '''' as FieldValue6,
		--'''' as FieldValue7, '''' as FieldValue8,'''' as FieldValue9, '''' as FieldValue10, '''' as FieldValue11, 
		--'+'''Total'''+' as FieldValue12, ISNULL(Convert(Varchar(50),SUM(Cast(FieldValue13 as decimal(10,0)))),0)  as FieldValue13, '''' as FieldValue14, '''' as FieldValue15, '''' as FieldValue16,
	 --   '''' as FieldValue17,0 as FieldValue18 from CTE
		--)T
		--order by T.tenderPaymentId desc
		order by tenderPaymentId desc
		'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */


		PRINT(@v_FinalQueryVc)
		Exec (@v_FinalQueryVc)

END

--By Dohatec for Bank Admin report
IF @v_fieldName1Vc = 'getSearchRegUserListingForReport'
BEGIN

	--Declare @v_BankUserRole_Vc varchar(50), @v_BankBranchId int, @v_BankId int

	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName2Vc, @v_RecordPerPage_inInt=@v_fieldName3Vc-1

	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */



	SET @v_ConditionString_Vc=' And isLive=''yes'''
	Select @v_BankUserRole_Vc=isMakerChecker,
		   @v_BankBranchId = sBankDevelopId
	From tbl_PartnerAdmin Where userId=@v_fieldName4Vc

	Select @v_BankId = Case sBankDevelHeadId
	When 0 Then @v_BankBranchId Else sBankDevelHeadId End
	From tbl_ScBankDevPartnerMaster Where sBankDevelopId=@v_BankBranchId



	--If @v_BankUserRole_Vc='BranchChecker'
	--Begin
	--	Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And regPaymentId in (Select regPaymentId From tbl_RegFeePayment Where createdBy in (select distinct userId from tbl_PartnerAdmin where sBankDevelopId= '+Convert(varchar(50), @v_BankBranchId)+'))'
	--End
	--Else If @v_BankUserRole_Vc='BankChecker'
	--Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And regPaymentId in (Select regPaymentId From tbl_RegFeePayment Where createdBy in (select distinct userId from tbl_PartnerAdmin
		where (
				sBankDevelopId= '+Convert(varchar(50), @v_BankBranchId)+
				' OR
				sBankDevelopId in (select distinct sBankDevelopId from tbl_ScBankDevPartnerMaster Where sBankDevelHeadId='+Convert(varchar(50), @v_BankId)+')
			)
		))'
	--End

	If @v_fieldName5Vc<>''
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And emailId=''' + @v_fieldName5Vc +''''
	End

	If @v_fieldName6Vc<>'' -- Verified status
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And isVerified=''' + @v_fieldName6Vc +''''
	End

	If @v_fieldName7Vc<>'' -- Branch Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And regPaymentId in (Select regPaymentId From tbl_RegFeePayment Where createdBy in (select distinct userId from tbl_PartnerAdmin where sBankDevelopId='+@v_fieldName7Vc+'))'
	End

	If @v_fieldName8Vc<>'' -- Branch Member Id
	Begin
		--Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And createdBy=''' + @v_fieldName8Vc +''''
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And partTransId=''' + @v_fieldName8Vc +''''
	End

	Print (@v_fieldName9Vc)
	Print (@v_fieldName10Vc)
	IF (@v_fieldName9Vc is not null And @v_fieldName9Vc <>'') And (@v_fieldName10Vc is not null And @v_fieldName10Vc <>'')
	Begin
		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) >= ''' + @v_fieldName9Vc + ''' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) <= ''' + @v_fieldName10Vc + ''''
	End
	ELSE IF (@v_fieldName9Vc is not null And @v_fieldName9Vc <>'') And (@v_fieldName10Vc is null OR @v_fieldName10Vc='')
	Begin
		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) >= ''' + @v_fieldName9Vc + ''''
	End
	ELSE IF (@v_fieldName9Vc is not null Or @v_fieldName9Vc <>'') And (@v_fieldName10Vc is not null And @v_fieldName10Vc <>'')
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) <= ''' + @v_fieldName10Vc + ''''
	End
	Print(@v_ConditionString_Vc)

	-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
	Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT( Distinct regPaymentId ) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')
	From tbl_RegFeePayment RP
	Inner Join tbl_LoginMaster LM On RP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
		WHERE regPaymentId is Not Null ' + @v_ConditionString_Vc

	Select @v_CntQry_Vc='SELECT Count(Distinct regPaymentId)
	From tbl_RegFeePayment RP
	Inner Join tbl_LoginMaster LM On RP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
		WHERE regPaymentId is Not Null ' + @v_ConditionString_Vc

	Print ('Count Query')
	Print(@v_CntQry_Vc)

			/* START CODE: DYNAMIC QUERY TO GET RECORDS */

			/*
				FieldValue1: Payment Id
				FieldValue2: User Id of Bidder for which Payment is done
				FieldValue3: Email Id of Bidder for which Payment is done
				FieldValue4: Branch Maker - Name of Bank User who has done the Payment
				FieldValue5: Payment Status New Payment/Renewal
				FieldValue6: Date of Payment
			*/

		Select @v_FinalQueryVc=
	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int;
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');

	Select FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6,FieldValue7, FieldValue8, FieldValue9, FieldValue10, FieldValue11, FieldValue12, FieldValue13,FieldValue14
	From
	(Select Distinct
		Convert(varchar(50),Tmp.regPaymentId) as FieldValue1,
			Convert(varchar(50),LM.userId) as FieldValue2,
			LM.emailId as FieldValue3,
			RP.branchName as FieldValue14,
			dbo.f_GovUserName(RP.partTransId, ''tbl_PartnerAdminTransfer'') as FieldValue4,
			Case RP.status When ''paid'' Then ''New Registration''
						   When ''freezed'' Then ''New Registration''
						   When ''renewed'' Then ''Renewal''
				 End as FieldValue5,
			REPLACE(CONVERT(VARCHAR(11),dtOfPayment, 106), '' '', ''-'')+ '' '' + Substring(CONVERT(VARCHAR(30),dtOfPayment,108),1,5) as FieldValue6,
			CONVERT(varchar(50),@v_TotalPageCnt_Int) as FieldValue7,
			CONVERT(varchar(50),@v_TotalRecordCnt_Int) as FieldValue8,
			CONVERT(varchar(50),Tmp.RowNumber) as FieldValue9,
			isVerified as FieldValue10,
			CONVERT(varchar(50),RP.paymentInstType) as FieldValue11,
			CONVERT(varchar(50),RP.amount) as FieldValue12,
			CONVERT(varchar(50),RP.currency) as FieldValue13,
			Tmp.regPaymentId,
			sortParam
		From
		(Select RowNumber, regPaymentId, sortParam From
			(SELECT ROW_NUMBER() Over (Order by '+@v_fieldName11Vc+ ' ' +@v_fieldName12Vc+') as RowNumber, regPaymentId, ' +@v_fieldName11Vc+' as sortParam
				From tbl_RegFeePayment RP
				Inner Join tbl_LoginMaster LM On RP.userId=LM.userId
				Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
		WHERE regPaymentId is Not Null '
					+@v_ConditionString_Vc+
			') B

		WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)
		+ ' ) Tmp
		Inner Join tbl_RegFeePayment RP On Tmp.regPaymentId=RP.regPaymentId
			Inner Join tbl_LoginMaster LM On RP.userId=LM.userId
			Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
		) as A Order By sortParam '+@v_fieldName12Vc+'
		--Order by A.regPaymentId desc
		'
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */


		PRINT(@v_FinalQueryVc)
		Exec (@v_FinalQueryVc)

END
if @v_fieldName1Vc='reTenderRecommendetion'
BEGIN
select reTenderRecommendetion as FieldValue1 from tbl_PostQualification where tenderId=@v_fieldName2Vc and evalCount = @v_fieldName3Vc and postQualStatus = 'Qualify'
END


if @v_fieldName1Vc='getAdminUserId'
BEGIN

declare @compnay varchar(50)

select @compnay = companyId from tbl_TendererMaster where userId=@v_fieldName2Vc
if	@compnay = '1'
begin
select cast(userId as varchar) as FieldValue1 from tbl_TendererMaster where userId=@v_fieldName2Vc
end
else
begin
select cast(userId as varchar) as FieldValue1 from tbl_TendererMaster where companyId = @compnay and isAdmin='yes'
end
END


IF @v_fieldName1Vc = 'getMobNoForSendVerifySMS'
BEGIN
select @Field1 = userId , @Field2 = userTyperId, @Field3 = countResetSMS from tbl_LoginMaster where emailId = @v_fieldName2Vc
IF(@Field2 = '2')
	BEGIN
		select convert(varchar(10),userid) as FieldValue1, 
		case when (select mobileNo from tbl_TendererMaster where userId = @Field1) = ''  
		then '0'
		else (convert(varchar(20),'+880'+right(mobileNo,10)) ) end as FieldValue2,
		convert(varchar(10),@Field3) as FieldValue3 from tbl_TendererMaster where userId = @Field1
	END
ELSE IF(@Field2 = '3')
	BEGIN
		select convert(varchar(10),userid) as FieldValue1, 
		case when (select mobileNo from tbl_EmployeeMaster where userId = @Field1) = ''  
		then '0'
		else (convert(varchar(20),'+880'+right(mobileNo,10)) ) end as FieldValue2,
		--convert(varchar(20),'+880'+right(mobileNo,10)) as FieldValue2,
		convert(varchar(10),@Field3) as FieldValue3 from tbl_EmployeeMaster where userId = @Field1
	END
ELSE IF(@Field2 = '4' or @Field2 = '5' or @Field2 = '8' or @Field2 = '19' or @Field2 = '20' or @Field2 = '21')
	BEGIN
		select convert(varchar(10),userid) as FieldValue1, 
		case when (select mobileNo from tbl_AdminMaster where userId = @Field1) = ''  
		then '0'
		else (convert(varchar(20),'+880'+right(mobileNo,10)) ) end as FieldValue2,
		--convert(varchar(20),'+880'+right(mobileNo,10)) as FieldValue2,
		convert(varchar(10),@Field3) as FieldValue3 from tbl_AdminMaster where userId = @Field1
	END
ELSE IF(@Field2 = '6' or @Field2 = '7' or @Field2 = '15')
	BEGIN
		select convert(varchar(10),userid) as FieldValue1, 
		case when (select mobileNo from tbl_PartnerAdmin where userId = @Field1) = ''  
		then '0'
		else (convert(varchar(20),'+880'+right(mobileNo,10)) ) end as FieldValue2,
		--convert(varchar(20),'+880'+right(mobileNo,10)) as FieldValue2,
		convert(varchar(10),@Field3) as FieldValue3 from tbl_PartnerAdmin where userId = @Field1
	
	END
ELSE
	BEGIN
		select '0' as FieldValue1, 
		'0' as FieldValue2,
	    '0' as FieldValue3 
	END
END

IF @v_fieldName1Vc='GetPMethodEstCostTenderLot'
BEGIN
	SELECT FieldValue1,FieldValue2 FROM
	(select tenderId,procurementMethod as FieldValue1 from tbl_TenderDetails  where tenderId=@v_fieldName2Vc)T
	INNER JOIN
	(select tenderId,convert(varchar(20),estCost) as FieldValue2 from tbl_TenderEstCost where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc)T1
	ON T.tenderId=T1.tenderId
END

IF @v_fieldName1Vc='GetWinnerInfo'
BEGIN

	select convert(varchar(200),userId) as FieldValue1 from tbl_EvalRoundMaster  where tenderId=@v_fieldName2Vc and pkgLotId = 0 /* For Service tender pkgLotId = 0 */
	
END

