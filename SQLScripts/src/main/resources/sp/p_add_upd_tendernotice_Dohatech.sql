USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_add_upd_tendernotice_Dohatech]    Script Date: 4/24/2016 10:41:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[p_add_upd_tendernotice_Dohatech]

@v_AppId_inInt int,
@v_PackageId_inInt int,
@v_TenderType_inVc varchar(50),-- Package/Lot
@v_CreatedBy_inVc varchar(400),

-- Input Params
-- Declare
@v_ReoiRfpRefNo_inVc varchar(50)='',
@v_DocEndDate_insDt smalldatetime=Null,
@v_PreBidStartDt_insDt smalldatetime=Null,
@v_PreBidEndDt_insDt smalldatetime=Null,
@v_SubmissionDt_insDt smalldatetime=Null,
@v_OpeningDt_insDt smalldatetime=Null,
@v_EligibilityCriteria_inVc varchar(2000) = '',
@v_TenderBrief_inVc varchar(2000)='',
@v_Deliverables_inVc varchar(2000)='',
@v_OtherDetails_inVc varchar(2000)='',
@v_ForeignFirm_inVc varchar(20)='',
@v_DocAvlMethod_inVc varchar(10)='',
@v_EvalType_inVc varchar(20)='',
@v_DocFeesMethod_inVc varchar(20)='',
@v_DocFeesMode_inVc varchar(20)='',
@v_DocOfficeAdd_inVc varchar(2000)='',
@v_SecurityLastDt_inDt smalldatetime=Null,
@v_SecuritySubOff_inVc varchar(1000)='',
-- Input Params for table tbl_TenderLotSecurity
--declare
@v_Location_inVc varchar(max)='',
@v_DocFess_inVc varchar(max)='',
@v_TenderSecurityAmt_inM varchar(max)='',
@v_CompletionTime_inVc varchar(max)='',

@v_Action_inVc varchar(50),
@v_Tenderid_inInt int=0,
@v_Corrid_inInt int=0,
-- Input Params for table tbl_TenderPhasing
@v_PhasingRefNo_inVc varchar(max)='',
@v_PhasingOfService_inVc varchar(max)='',
@v_PhasingLocation_inVc varchar(max)='',
@v_IndStartDt_inVc varchar(max)='',
@v_IndEndDt_inVc varchar(max)='',

-- Input Params for table tbl_TenderAuditTrail
@v_ESignature_inVc varchar(max)='',
@v_DigitalSignature_inVc varchar(max)='',
@v_TenderLotSecId_inVc varchar(max)='',

--Input Params use to compare old value and insert field into tbl_corrignedium
@v_ContractTypeNew_Vc varchar(20)='',
@v_tenderPubDt_Vc smalldatetime=NULL,
@v_PkgDocFees_Vc varchar(20)=null,  --0.00,
@v_EstCost_inM money=0.00 ,
@v_PassingMarks_Int int=0,
@v_StartTime_inVc varchar(max)=''
,@v_PkgDocFeesUSD_Vc varchar(20)='',	
@v_TenderSecurityAmt_M_USD varchar(20)=''	
AS

BEGIN

SET NOCOUNT ON;

DECLARE @v_flag_bit bit,
@v_FinancialYear_Vc varchar(10),
@v_TenderId_Int int,
@v_DepartmentId_Int int,
@v_EmployeeName_Vc varchar(200),
@v_Pecode_Vc varchar(15),
@v_StateName_Vc varchar(100),
@v_EventType_Vc varchar(100),
@v_AppPkgLotIdCnt_Int int,
@v_InvitationFor varchar(100),
@v_CurrDepartmentId_Int int,
@v_CurrDepartmentType_Vc varchar(15),
@v_CurrDepartmentName_Vc varchar(150),
@v_Ministry_Vc varchar(150),
@v_Division_Vc varchar(150),
@v_Agency_Vc varchar(150),
@v_Reoirfpfor_Vc varchar(50),

@v_Procurementnature_Vc varchar(10),
@v_IsPQrequired_Vc varchar(10),
@v_ReoiRfaRequired_Vc varchar(10),
@v_ProcurementMethodId_tInt tinyint,

@v_ContractType_Vc varchar(20),
@v_ProcurementMethod_Vc varchar(30),
@v_BudgetTypeId_tInt tinyint,
@v_BudgetType_Vc varchar(20),
@v_SourceOfFund varchar(200),
@v_SbDevelopName varchar(300),
@v_ProjectId_Int int,
@v_ProjectName_Vc varchar(150),
@v_ProjectCode_Vc varchar(150),
@v_PackageNo_Vc varchar(50),
@v_PackageDescription varchar(2000),
@v_CpvCode_Vc varchar(8000),
@v_OfficeName_Vc varchar(150),
@v_OfficeId_Int int,
@v_ProcurementType_Vc varchar(10),
@v_DesignationId_Int int,
@v_DesignationName_Vc varchar(100),
@v_OfficeAddress_Vc varchar(5000),
@v_ContactDetails_Vc varchar(1000),
@v_ProcurementNatureId_tInt tinyint,
@v_PkgUrgency_Vc varchar(20),
@v_ServicesType_Vc varchar(100)

IF @v_Action_inVc='INSERT'
BEGIN
	BEGIN TRY
			BEGIN TRAN
				/* START CODE: TO INSERT INTO TABLE - tbl_TenderMaster */
				INSERT INTO [dbo].[tbl_TenderMaster]
						([appId],[packageId],[createdBy],[creationDate],[financialYear],[tenderType])
				SELECT	@v_AppId_inInt,@v_PackageId_inInt,@v_CreatedBy_inVc,GETDATE(),(select financialyear from tbl_appmaster where appid=@v_AppId_inInt), @v_TenderType_inVc
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderMaster */

				Set @v_TenderId_Int=IDENT_CURRENT('dbo.tbl_TenderMaster')

				/* START CODE: TO INSERT INTO TABLE - tbl_TenderLots */
				INSERT INTO [dbo].[tbl_TenderLots]
						([tenderId],[appPkgLotId])
				SELECT 	@v_TenderId_Int, appPkgLotId
					FROM tbl_AppPkgLots WHERE appId=@v_AppId_inInt and packageId=@v_PackageId_inInt
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderLots */

				/* START CODE: TO INSERT INTO TABLE - tbl_TenderCurrency */
				INSERT INTO  [dbo].[tbl_TenderCurrency]
				([tenderId],[currencyId],[exchangeRate],[createdBy],[createdDate])
				VALUES (@v_TenderId_Int,2,1,@v_CreatedBy_inVc,GETDATE())
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderCurrency */

				Select @v_AppPkgLotIdCnt_Int= COUNT(appPkgLotId) from tbl_AppPkgLots where appId=@v_AppId_inInt and packageId=@v_PackageId_inInt

				Select @v_DepartmentId_Int = departmentId from tbl_AppMaster where appid=@v_AppId_inInt

				/* START CODE: TO FETCH COLUMN VALUES FROM tbl_AppMaster */
				Select @v_FinancialYear_Vc=financialYear,
						@v_BudgetTypeId_tInt=budgetType,
						@v_ProjectId_Int=projectId,
						@v_OfficeId_Int=officeId
					From tbl_AppMaster Where appId=@v_AppId_inInt
				/* END CODE: TO FETCH COLUMN VALUES FROM tbl_AppMaster */

				/* START CODE: TO FETCH COLUMN VALUES FROM tbl_ProjectMaster */
				Select @v_ProjectCode_Vc = ProjectCode, @v_ProjectName_Vc=projectName
					from tbl_ProjectMaster Where projectId=@v_ProjectId_Int
				/* END CODE: TO FETCH COLUMN VALUES FROM tbl_ProjectMaster */


				/* START CODE: TO FETCH COLUMN VALUES FROM tbl_AppPackages */
				Select @v_procurementnature_Vc= procurementnature,
					   @v_IsPQrequired_Vc=isPQRequired,
					   @v_ReoiRfaRequired_Vc=reoiRfaRequired,
					   @v_ProcurementMethodId_tInt=procurementMethodId,
					   @v_SourceOfFund=sourceOfFund,
					   @v_PackageNo_Vc=packageNo,
					   @v_PackageDescription=PackageDesc,
					   @v_CpvCode_Vc=cpvCode,
					   @v_ProcurementType_Vc=ProcurementType,
					   @v_EstCost_inM=pkgEstCost,
					   @v_PkgUrgency_Vc=pkgUrgency,
					   @v_ServicesType_Vc=servicesType
				from tbl_AppPackages
				where appId=@v_AppId_inInt and packageId=@v_PackageId_inInt
				/* END CODE: TO FETCH COLUMN VALUES FROM tbl_AppPackages */

				--If @v_ProcurementMethodId_tInt=8 or @v_ProcurementMethodId_tInt=3  -- For SFB ProcurementMethod Case
				--Begin
				--	Select @v_EstCost_inM= allocateBudget from tbl_AppPackages
				--			where appId=@v_AppId_inInt and packageId=@v_PackageId_inInt
				--End

				Select @v_ProcurementNatureId_tInt=procurementNatureId from tbl_procurementnature where procurementNature=@v_procurementnature_Vc


				/* START CODE: TO SET VALUES OF Ministry, Division and Agency */
				Select @v_CurrDepartmentId_Int=departmentId,@v_CurrDepartmentType_Vc=departmentType, @v_CurrDepartmentName_Vc=departmentName
					from tbl_DepartmentMaster where departmentId=@v_DepartmentId_Int

				If @v_CurrDepartmentType_Vc='Ministry'
				Begin
					Select @v_Ministry_Vc=@v_CurrDepartmentName_Vc, @v_Division_Vc='',@v_Agency_Vc=''
				End
				Else If @v_CurrDepartmentType_Vc='Division'
				Begin
					Select @v_Ministry_Vc=(select departmentName from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_CurrDepartmentId_Int)),
					@v_Division_Vc=@v_CurrDepartmentName_Vc,
					@v_Agency_Vc=''
				End
				Else -- Agency/Organisation Case
				Begin
					If (select departmentType from tbl_DepartmentMaster where departmentId=(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_CurrDepartmentId_Int))='Ministry'
					Begin
						Select @v_Ministry_Vc=(select departmentName from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_CurrDepartmentId_Int)),
						@v_Division_Vc= '',
						@v_Agency_Vc=@v_CurrDepartmentName_Vc
					End
					Else
					Begin
						Select @v_Ministry_Vc=(select departmentName from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_CurrDepartmentId_Int))),
						@v_Division_Vc= (select departmentName from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_CurrDepartmentId_Int)),
						@v_Agency_Vc=@v_CurrDepartmentName_Vc
					End

					--Select @v_Ministry_Vc=(select departmentName from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_CurrDepartmentId_Int))),
					--@v_Division_Vc= (select departmentName from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_CurrDepartmentId_Int)),
					--@v_Agency_Vc=@v_CurrDepartmentName_Vc
				End
				/* END CODE: TO SET VALUES OF Ministry, Division and Agency */

				/* START CODE: TO SET VALUE OF peName, peCode, peDistrict */
					/*
						peName = employeeName from tbl_EmployeeMaster
						peCode = peCode from tbl_officemaster
						peDistrict = stateName from tbl_StateMaster
					*/
				Select @v_EmployeeName_Vc = employeeName, @v_DesignationId_Int=designationId from tbl_EmployeeMaster where employeeId=(select employeeId from tbl_AppMaster where appid=@v_AppId_inInt )

				Select @v_Pecode_Vc = peCode,
						@v_OfficeName_Vc=officeName,
						@v_OfficeAddress_Vc ='
						<table width="100%" border="0"><tr><td width="30%"><b>Address</b></td><td width="70%">: '+Address +'</td></tr>'
						+'<tr><td><b>City</b></td><td>: '+ city+'</td></tr><tr><td><b>Thana</b></td><td>: '+ upjilla+'</td></tr><tr><td><b>District</b></td><td>: '
						+(select stateName from tbl_StateMaster where stateId=OM.stateId)+' - '+postCode+'</td></tr><tr><td><b>Country</b></td><td>: '
						+(select countryName from tbl_CountryMaster where countryId=OM.countryId)+'</td></tr></table>',
						@v_ContactDetails_Vc = '<table width="100%" border="0">
						<tr><td width="30%"><b>Phone No</b></td><td width="70%">: ' + phoneNo +'</td></tr>
						<tr><td><b>Fax No</b></td><td>: ' + faxNo + '</td></tr></table>'
						from tbl_officemaster OM where officeId=@v_OfficeId_Int

				Select @v_StateName_Vc = stateName from tbl_StateMaster where stateId=(select stateId from tbl_officemaster where officeId=@v_OfficeId_Int)

				Select @v_designationName_Vc = designationName from tbl_DesignationMaster where designationId=@v_DesignationId_Int
				/* END CODE: TO SET VALUE OF peName, peCode, peDistrict */

				/* START CODE: TO SET VALUE OF EventType */
				Select @v_EventType_Vc=Case
							When (@v_IsPQrequired_Vc='yes')
							Then 'PQ'
							When (@v_IsPQrequired_Vc='no' And @v_ProcurementMethodId_tInt!=5 And @v_ProcurementMethodId_tInt!=1 And @v_ProcurementMethodId_tInt!=16 And @v_ProcurementMethodId_tInt!=17)
							Then 'Tender'
							When (@v_ProcurementMethodId_tInt=1 OR @v_ProcurementMethodId_tInt=16 OR @v_ProcurementMethodId_tInt=17)
							Then 'RFQ'
							When (@v_IsPQrequired_Vc='' And @v_ReoiRfaRequired_Vc<>'' And @v_ReoiRfaRequired_Vc is not null)
							Then @v_ReoiRfaRequired_Vc
							When (@v_ProcurementMethodId_tInt=5)
							Then '1 stage-TSTM'
							When (@v_ProcurementMethodId_tInt!=5)
							Then 'Tender'
							End
					--Case
					--	When @v_IsPQrequired_Vc='yes'
					--	Then 'PQ'
					--	When @v_IsPQrequired_Vc='no' and @v_ProcurementMethodId_tInt!=5
					--	Then 'Tender'
					--	When @v_IsPQrequired_Vc='' And @v_ReoiRfaRequired_Vc <> '' And @v_ReoiRfaRequired_Vc is not null
					--	Then @v_ReoiRfaRequired_Vc
					--	When @v_ProcurementMethodId_tInt=5
					--	Then '1 stage-TSTM'
					--	When @v_ProcurementMethodId_tInt!=5
					--	Then 'Tender'
						 --(Select procurementMethod from tbl_procurementmethod where procurementMethodId=@v_ProcurementMethodId_tInt)
					--End
				/* END CODE: TO SET VALUE OF EventType */

				/* START CODE: TO SET VALUE OF InvitationFor */
				Select @v_InvitationFor=
					Case
						When @v_EventType_Vc='PQ'
						Then
							Case
								When @v_AppPkgLotIdCnt_Int=1
								Then 'PQ - Single Lot'
								When @v_AppPkgLotIdCnt_Int>1
								Then 'PQ - Multiple Lot'
							End

						When @v_EventType_Vc<>'PQ'
						Then
							Case
								When @v_AppPkgLotIdCnt_Int=1
								Then 'Tender - Single Lot'
								When @v_AppPkgLotIdCnt_Int>1
								Then 'Tender - Multiple Lot'
							End
					End
				/* END CODE: TO SET VALUE OF InvitationFor */

				/* START CODE: TO SET VALUE OF Reoirfpfor */
				Select @v_Reoirfpfor_Vc=
					Case
						When @v_procurementnature_Vc='Services'
						Then
							Case
								When @v_EventType_Vc='RFA'
								Then 'Individual consultant'
								Else 'Consulting firm'
							End
						Else
							''
					End
				/* END CODE: TO SET VALUE OF Reoirfpfor */

				-- // Currently ContractType is Set To Blank
				--Select @v_ContractType_Vc=''

				Select @v_ProcurementMethod_Vc=procurementMethod from tbl_procurementmethod where procurementMethodId=@v_ProcurementMethodId_tInt

				Select @v_BudgetType_Vc=budgetType from tbl_BudgetType where budgetTypeId=@v_BudgetTypeId_tInt

				-- devPartners=v_SbDevelopName
				Select @v_SbDevelopName= Coalesce (@v_SbDevelopName, '') + sbDevelopName + ', ' from tbl_ScBankDevPartnerMaster where sBankDevelopId in (select sBankDevelopId from tbl_projectpartners where projectId=@v_ProjectId_Int)
				Select @v_SbDevelopName = SUBSTRING(@v_SbDevelopName,0,LEN(@v_SbDevelopName))
				
				
					/* START CODE: TO INSERT INTO TABLE - tbl_TenderDetails */
				INSERT INTO [dbo].[tbl_TenderDetails]
				([departmentId],[tenderId],[ministry],[division],[agency],[peOfficeName],
				[peCode],[peDistrict],[eventType],[invitationFor],[reoiRfpFor],[contractType],
				[reoiRfpRefNo],[procurementMethod],[budgetType],[sourceOfFund],[devPartners],
				[projectCode],[projectName],[packageNo],[packageDescription],[cpvCode],
				[docEndDate],[preBidStartDt],[preBidEndDt],[submissionDt],[openingDt],
				[eligibilityCriteria],[tenderBrief],[deliverables],[otherDetails],[foreignFirm],
				[docAvlMethod],[evalType],[docFeesMethod],[docFeesMode],[docOfficeAdd],[securityLastDt],
				[securitySubOff],[procurementNature],[procurementType],
				[modeOfTender],[reTenderId],[pqTenderId],[reoiTenderId],[tenderStatus],[tenderPubDt],
				[peName],[peDesignation],[peAddress],[peContactDetails],
				[estCost],[approvingAuthId],[stdTemplateId],
				[docAccess],[tenderValDays],[tenderValidityDt],[tenderSecurityDays],[tenderSecurityDt],
				[procurementNatureId],[procurementMethodId],[officeId],[budgetTypeId],
				[workflowStatus],[pkgDocFees],pkgUrgency,servicesType)
				SELECT
				@v_DepartmentId_Int,@v_TenderId_Int,@v_Ministry_Vc,@v_Division_Vc,@v_Agency_Vc,@v_OfficeName_Vc,
				@v_Pecode_Vc,@v_StateName_Vc,@v_EventType_Vc,@v_InvitationFor,@v_Reoirfpfor_Vc,@v_ContractTypeNew_Vc,
				@v_ReoiRfpRefNo_inVc,@v_ProcurementMethod_Vc,@v_BudgetType_Vc,@v_SourceOfFund,@v_SbDevelopName,
				@v_ProjectCode_Vc,@v_ProjectName_Vc,@v_PackageNo_Vc,@v_PackageDescription,@v_CpvCode_Vc,
				@v_DocEndDate_insDt,@v_PreBidStartDt_insDt,@v_PreBidEndDt_insDt,@v_SubmissionDt_insDt,@v_OpeningDt_insDt,
				@v_EligibilityCriteria_inVc,@v_TenderBrief_inVc,@v_Deliverables_inVc,@v_OtherDetails_inVc,@v_ForeignFirm_inVc,
				@v_DocAvlMethod_inVc,@v_EvalType_inVc,@v_DocFeesMethod_inVc,@v_DocFeesMode_inVc,@v_DocOfficeAdd_inVc,@v_SecurityLastDt_inDt,
				@v_SecuritySubOff_inVc,@v_Procurementnature_Vc,@v_ProcurementType_Vc,
				'Online',0,0,0,'pending',@v_tenderPubDt_Vc,
				@v_EmployeeName_Vc,@v_DesignationName_Vc,@v_OfficeAddress_Vc,@v_ContactDetails_Vc,
				@v_EstCost_inM,@v_AppId_inInt,@v_PackageId_inInt,
				'Open',15,Null,54,@v_SubmissionDt_insDt,
				@v_ProcurementNatureId_tInt,@v_ProcurementMethodId_tInt,@v_OfficeId_Int,@v_BudgetTypeId_tInt,
				'pending',@v_PkgDocFees_Vc, @v_PkgUrgency_Vc, @v_ServicesType_Vc
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderDetails */



				/* START CODE: TO INSERT INTO TABLE - tbl_TenderOpenDates */
				--INSERT INTO [tbl_TenderOpenDates]
				--	([tenderId],[openingDt],[entryDt],[envelopeId])
				-- VALUES
				--	   (@v_TenderId_Int,@v_OpeningDt_insDt,GetDate(),1)
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderOpenDates */

				/* START CODE: TO INSERT INTO TABLE - tbl_TenderLotSecurity */

				INSERT INTO [dbo].[tbl_TenderLotSecurity]
					([tenderId],[appPkgLotId],[lotNo],[lotDesc],[location],[docFess],[tenderSecurityAmt],[completionTime],[startTime]) 
				SELECT @v_TenderId_Int,appPkgLotId,lotNo,lotDesc,@v_Location_inVc,@v_DocFess_inVc,@v_TenderSecurityAmt_inM,@v_CompletionTime_inVc,@v_StartTime_inVc
					FROM tbl_AppPkgLots
					WHERE appId=@v_AppId_inInt and packageId=@v_PackageId_inInt


				--INSERT INTO [dbo].[tbl_TenderLotSecurity]
				--	([tenderId],[appPkgLotId],[lotNo],[lotDesc],[location],[docFess],[tenderSecurityAmt],[completionTime])
				--SELECT @v_TenderId_Int,appPkgLotId,lotNo,lotDesc,@v_Location_inVc,@v_DocFess_inM,@v_TenderSecurityAmt_inM,@v_CompletionTime_inVc
				--	FROM tbl_AppPkgLots
				--	WHERE appId=@v_AppId_inInt and packageId=@v_PackageId_inInt
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderLotSecurity */
			
				INSERT INTO [dbo].[tbl_TenderPostQueConfig]
					   ([tenderId]
					   ,[isQueAnsConfig]
					   ,[createdBy]
					   ,[createdDt]
					   ,[postQueLastDt])
				 VALUES
					   (@v_TenderId_Int
					   ,'No'
					   ,@v_CreatedBy_inVc
					   ,GETDATE()
					   ,null)

				Set @v_flag_bit=1
				--Select @v_flag_bit as flag, 'Record inserted.' as Message,IDENT_CURRENT('dbo.tbl_TenderMaster') id
				Select @v_TenderId_Int as Id,@v_flag_bit as flag, 'Record inserted.' as Msg

			COMMIT TRAN
		END TRY
	BEGIN CATCH
		BEGIN
			Set @v_flag_bit=0
			Select 0 as Id, @v_flag_bit as flag, ERROR_MESSAGE() as Msg
			ROLLBACK TRAN
		END
	END CATCH
END



ELSE IF @v_Action_inVc='UPDATE'
BEGIN
	BEGIN TRY
		BEGIN TRAN
			/* START CODE: TO UPDATE TABLE - tbl_TenderDetails */
			select @v_SecurityLastDt_inDt

			UPDATE [dbo].[tbl_TenderDetails]
		    SET
			  [reoiRfpRefNo] = @v_ReoiRfpRefNo_inVc
			  ,[docEndDate] = @v_DocEndDate_insDt
			  ,[preBidStartDt] = @v_PreBidStartDt_insDt
			  ,[preBidEndDt] = @v_PreBidEndDt_insDt
			  ,[submissionDt] = @v_SubmissionDt_insDt
			  ,[openingDt] = @v_OpeningDt_insDt
			  ,[eligibilityCriteria] = @v_EligibilityCriteria_inVc
			  ,[tenderBrief] = @v_TenderBrief_inVc
			  ,[deliverables] = @v_Deliverables_inVc
			  ,[otherDetails] = @v_OtherDetails_inVc
			  ,[foreignFirm] = @v_ForeignFirm_inVc
			  ,[docAvlMethod] = @v_DocAvlMethod_inVc
			  ,[evalType] = @v_EvalType_inVc
			  ,[docFeesMethod] = @v_DocFeesMethod_inVc
			  ,[docFeesMode] = @v_DocFeesMode_inVc
			  ,[docOfficeAdd] = @v_DocOfficeAdd_inVc
			  ,[securityLastDt] = @v_SecurityLastDt_inDt
			  ,[securitySubOff] = @v_SecuritySubOff_inVc
			  ,[tenderPubDt] = @v_tenderPubDt_Vc,
			  contracttype=@v_ContractTypeNew_Vc,
			  pkgDocFees=@v_PkgDocFees_Vc,
			  passingMarks=@v_PassingMarks_Int
			  ,pkgDocFeesUSD=@v_PkgDocFeesUSD_Vc	
			WHERE  tenderId = @v_Tenderid_inInt
			/* END CODE: TO UPDATE TABLE - tbl_TenderDetails */

			/* START CODE: TO UPDATE TABLE - tbl_TenderOpenDates */
			--UPDATE [tbl_TenderOpenDates]
			--	SET openingDt=@v_OpeningDt_insDt,
			--		entryDt=gETDATE()
			-- WHERE tenderId = @v_Tenderid_inInt
			/* END CODE: TO UPDATE TABLE - tbl_TenderOpenDates */

			Select @v_ProcurementNatureId_tInt=procurementNatureId
			from tbl_TenderDetails where tenderId=@v_Tenderid_inInt

			IF @v_ProcurementNatureId_tInt=1 OR @v_ProcurementNatureId_tInt=2
			BEGIN
				/* START: REPLACE '@$' WITH ',' */
				Select @v_TenderLotSecId_inVc=REPLACE(@v_TenderLotSecId_inVc,'@$',','),
					   @v_Location_inVc=REPLACE(@v_Location_inVc,'@$',','),
					   @v_DocFess_inVc=REPLACE(@v_DocFess_inVc,'@$',','),
					   @v_TenderSecurityAmt_inM=REPLACE(@v_TenderSecurityAmt_inM,'@$',','),
					   @v_CompletionTime_inVc=REPLACE(@v_CompletionTime_inVc,'@$',',')
					   ,@v_StartTime_inVc=REPLACE(@v_StartTime_inVc,'@$',',')
					   ,@v_TenderSecurityAmt_M_USD=REPLACE(@v_TenderSecurityAmt_M_USD,'@$',',')	
				/* END: REPLACE '@$' WITH ',' */
print @v_StartTime_inVc
				/* START CODE: TO UPDATE TABLE - tbl_TenderLotSecurity */
				UPDATE [dbo].[tbl_TenderLotSecurity]
					  SET
					  [location] = Tmp.Location
					  ,[docFess] = Tmp.DocFess
					  ,[tenderSecurityAmt] = Tmp.TenderSecurityAmt
					  ,[completionTime] = Tmp.CompletionTime
					  ,[startTime] = Tmp.StartTime
					  ,[tenderSecurityAmtUSD] = Tmp.TenderSecurityAmtUSD	

				FROM
				(
					SELECT dbo.f_trim(A.items) as TenderLotSecId,
						   dbo.f_trim(B.items) as Location,
						   dbo.f_trim(C.items) as DocFess,
						   dbo.f_trim(D.items) as TenderSecurityAmt,
						   dbo.f_trim(E.Items) as CompletionTime
						  ,dbo.f_trim(F.Items) as StartTime
						   ,dbo.f_trim(G.items) as TenderSecurityAmtUSD
					FROM dbo.f_splitX(@v_TenderLotSecId_inVc,',') A
						Inner join	dbo.f_splitX(@v_Location_inVc,',') B On A.ID=B.ID
						Inner Join dbo.f_splitX(@v_DocFess_inVc,',') C On A.Id=C.Id
						Inner Join dbo.f_splitX(@v_TenderSecurityAmt_inM,',') D On A.Id=D.Id
						Inner Join dbo.f_splitX(@v_CompletionTime_inVc,',') E On A.Id=E.Id
						Inner Join dbo.f_splitX(@v_StartTime_inVc,',') F On A.Id=F.Id
						Inner Join dbo.f_splitX(@v_TenderSecurityAmt_M_USD,',') G On A.Id=G.Id	
				) Tmp
				WHERE tbl_TenderLotSecurity.tenderLotSecId=Tmp.TenderLotSecId



				/* END CODE: TO UPDATE TABLE - tbl_TenderLotSecurity */
			END
			ELSE
			BEGIN
				-- // DELETE FROM TABLE tbl_TenderPhasing
				DELETE FROM tbl_TenderPhasing WHERE tenderId=@v_Tenderid_inInt

				/* START: REPLACE '@$' WITH ',' */
				Select @v_PhasingRefNo_inVc=REPLACE(@v_PhasingRefNo_inVc,'@$',','),
					   @v_PhasingOfService_inVc=REPLACE(@v_PhasingOfService_inVc,'@$',','),
					   @v_PhasingLocation_inVc=REPLACE(@v_PhasingLocation_inVc,'@$',','),
					   @v_IndStartDt_inVc=REPLACE(@v_IndStartDt_inVc,'@$',','),
					   @v_IndEndDt_inVc=REPLACE(@v_IndEndDt_inVc,'@$',',')
				/* END: REPLACE '@$' WITH ',' */

				/* START CODE: TO INSERT INTO TABLE - tbl_TenderPhasing */
				INSERT INTO [dbo].[tbl_TenderPhasing]
					   ([tenderId]
					   ,[phasingRefNo]
					   ,[phasingOfService]
					   ,[location]
					   ,[indStartDt]
					   ,[indEndDt])
				SELECT @v_Tenderid_inInt AS TenderId,
					   dbo.f_trim(A.items) as PhasingRefNo,
					   dbo.f_trim(B.items) as PhasingOfService,
					   dbo.f_trim(C.items) as PhasingLocation,
					   dbo.f_trim(D.items) as IndStartDt,
					   dbo.f_trim(E.Items) as IndEndDt
				FROM dbo.f_splitX(@v_PhasingRefNo_inVc,',') A
					Inner join	dbo.f_splitX(@v_PhasingOfService_inVc,',') B On A.ID=B.ID
					Inner Join dbo.f_splitX(@v_PhasingLocation_inVc,',') C On A.Id=C.Id
					Inner Join dbo.f_splitX(@v_IndStartDt_inVc ,',') D On A.Id=D.Id
					Inner Join dbo.f_splitX(@v_IndEndDt_inVc,',') E On A.Id=E.Id
				/* END CODE: TO INSERT INTO TABLE - tbl_TenderPhasing */
			END

			Set @v_flag_bit=1
			Select 0 as Id, @v_flag_bit as flag,@v_CompletionTime_inVc+'Record Updated' as Msg
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		BEGIN
			Set @v_flag_bit=0
			Select @v_TenderId_Int as Id, @v_flag_bit as flag, ERROR_MESSAGE() as Msg
			ROLLBACK TRAN
		END
	END CATCH
END


ELSE IF @v_Action_inVc='UPDATECONFIG'
BEGIN
	BEGIN TRY
		BEGIN TRAN
			/* START CODE: TO UPDATE TABLE - tbl_TenderDetails */
			UPDATE [dbo].[tbl_TenderDetails]
		    SET
			  [approvingAuthId] = @v_AppId_inInt
			  ,[stdTemplateId] = @v_PackageId_inInt
			  ,[tenderValDays] = @v_TenderType_inVc
			  ,[tenderSecurityDays] = @v_CreatedBy_inVc
			WHERE  tenderId = @v_Tenderid_inInt
			/* END CODE: TO UPDATE TABLE - tbl_TenderDetails */


			Set @v_flag_bit=1
			Select @v_TenderId_Int as Id, @v_flag_bit as flag, 'Record updated.' as Msg
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		BEGIN
			Set @v_flag_bit=0
			Select @v_TenderId_Int as Id, @v_flag_bit as flag, ERROR_MESSAGE() as Msg
			ROLLBACK TRAN
		END
	END CATCH
END



ELSE IF @v_Action_inVc='CORRIGENDUM'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
				DECLARE @v_contractTypeOld_Vc varchar(20),@v_tenderPubDtOld_Vc varchar(20),@v_docEndDateOld_Vc varchar(20),
						@v_preBidStartDtOld_Vc varchar(20),@v_preBidEndDtOld_Vc varchar(20),@v_submissionDtOld_Vc varchar(20),
						@v_openingDtOld_Vc varchar(20),@v_pkgDocFeesOld_Vc varchar(20),@v_securityLastDtOld_Vc varchar(20),
						@v_tenderSecurityAmtOld_Vc varchar(20),@v_docFessOld_Vc varchar(20)

				SELECT @v_contractTypeOld_Vc=tbl_TenderDetails.contractType,
					   @v_tenderPubDtOld_Vc=tbl_TenderDetails.tenderPubDt,
					   @v_docEndDateOld_Vc=tbl_TenderDetails.docEndDate,
					   @v_preBidStartDtOld_Vc=tbl_TenderDetails.preBidStartDt,
					   @v_preBidEndDtOld_Vc=tbl_TenderDetails.preBidEndDt,
					   @v_submissionDtOld_Vc=tbl_TenderDetails.submissionDt,
					   @v_openingDtOld_Vc=tbl_TenderDetails.openingDt,
					   @v_pkgDocFeesOld_Vc=tbl_TenderDetails.pkgDocFees,
					   @v_securityLastDtOld_Vc=tbl_TenderDetails.securityLastDt,
					   @v_tenderSecurityAmtOld_Vc=tbl_TenderLotSecurity.tenderSecurityAmt,
					   @v_docFessOld_Vc=tbl_TenderLotSecurity.docFess
				FROM   tbl_TenderDetails INNER JOIN
					   tbl_CorrigendumMaster ON tbl_TenderDetails.tenderId = tbl_CorrigendumMaster.tenderId INNER JOIN
					   tbl_TenderLotSecurity ON tbl_CorrigendumMaster.tenderId = tbl_TenderLotSecurity.tenderId
				WHERE  tbl_CorrigendumMaster.corrigendumId=@v_Corrid_inInt


				--Comparing Old value with new value, if change is found then insert into table tbl_CorrigendumDetail
				--Finding change in field 'contractType'
				IF @v_contractTypeOld_Vc <> @v_ContractTypeNew_Vc
					BEGIN
						INSERT INTO tbl_CorrigendumDetail VALUES (@v_Corrid_inInt,'contractType',@v_contractTypeOld_Vc,@v_ContractTypeNew_Vc)
					END

				--Finding change in field 'tenderPubDt'
				IF cast(floor(cast(@v_tenderPubDtOld_Vc as float))AS datetime) <> cast(floor(cast(@v_tenderPubDt_Vc as float))AS datetime)
					BEGIN
						INSERT INTO tbl_CorrigendumDetail VALUES (@v_Corrid_inInt,'tenderPubDt',@v_tenderPubDtOld_Vc,@v_tenderPubDt_Vc)
					END

				--Finding change in field 'docEndDate'
				IF cast(floor(cast(@v_docEndDateOld_Vc as float))AS datetime) <> cast(floor(cast(@v_DocEndDate_insDt as float))AS datetime)
					BEGIN
						INSERT INTO tbl_CorrigendumDetail VALUES (@v_Corrid_inInt,'docEndDate',@v_docEndDateOld_Vc,@v_DocEndDate_insDt)
					END

				--Finding change in field 'preBidStartDt'
				IF cast(floor(cast(@v_preBidStartDtOld_Vc as float))AS datetime) <> cast(floor(cast(@v_PreBidStartDt_insDt as float))AS datetime)
					BEGIN
						INSERT INTO tbl_CorrigendumDetail VALUES (@v_Corrid_inInt,'preBidStartDt',@v_preBidStartDtOld_Vc,@v_PreBidStartDt_insDt)
					END

				--Finding change in field 'preBidEndDt'
				IF cast(floor(cast(@v_preBidEndDtOld_Vc as float))AS datetime) <> cast(floor(cast(@v_PreBidEndDt_insDt as float))AS datetime)
					BEGIN
						INSERT INTO tbl_CorrigendumDetail VALUES (@v_Corrid_inInt,'preBidEndDt',@v_preBidEndDtOld_Vc,@v_PreBidEndDt_insDt)
					END

				--Finding change in field 'submissionDt'
				IF cast(floor(cast(@v_submissionDtOld_Vc as float))AS datetime) <> cast(floor(cast(@v_SubmissionDt_insDt as float))AS datetime)
					BEGIN
						INSERT INTO tbl_CorrigendumDetail VALUES (@v_Corrid_inInt,'submissionDt',@v_submissionDtOld_Vc,@v_SubmissionDt_insDt)
					END

				--Finding change in field 'openingDt'
				IF cast(floor(cast(@v_openingDtOld_Vc as float))AS datetime) <> cast(floor(cast(@v_OpeningDt_insDt as float))AS datetime)
					BEGIN
						INSERT INTO tbl_CorrigendumDetail VALUES (@v_Corrid_inInt,'openingDt',@v_openingDtOld_Vc,@v_OpeningDt_insDt)
					END

				--Finding change in field 'pkgDocFees'
				IF @v_pkgDocFeesOld_Vc <> @v_PkgDocFees_Vc
					BEGIN
						INSERT INTO tbl_CorrigendumDetail VALUES (@v_Corrid_inInt,'pkgDocFees',@v_pkgDocFeesOld_Vc,@v_PkgDocFees_Vc)
					END

				--Finding change in field 'securityLastDt'
				IF @v_securityLastDtOld_Vc <> @v_SecurityLastDt_inDt
					BEGIN
						INSERT INTO tbl_CorrigendumDetail VALUES (@v_Corrid_inInt,'securityLastDt',@v_securityLastDtOld_Vc,@v_SecurityLastDt_inDt)
					END

				--Finding change in field 'tenderSecurityAmt'
				IF @v_tenderSecurityAmtOld_Vc <> @v_SecurityLastDt_inDt
					BEGIN
						INSERT INTO tbl_CorrigendumDetail VALUES (@v_Corrid_inInt,'tenderSecurityAmt',@v_tenderSecurityAmtOld_Vc,@v_SecurityLastDt_inDt)
					END

				--Finding change in field 'docFess'
				IF @v_docFessOld_Vc <> @v_DocFess_inVc
					BEGIN
						INSERT INTO tbl_CorrigendumDetail VALUES (@v_Corrid_inInt,'docFess',@v_docFessOld_Vc,@v_DocFess_inVc)
					END


				--SELECT * FROM tbl_TenderDetails
				Set @v_flag_bit=1
				Select @v_flag_bit as flag, 'Record inserted.' as Message
			COMMIT TRAN
		END TRY
	BEGIN CATCH
		BEGIN
			Set @v_flag_bit=0
			Select @v_flag_bit as flag, 'Record not inserted.' as Message
			ROLLBACK TRAN
		END
	END CATCH
	END

SET NOCOUNT OFF;
END

