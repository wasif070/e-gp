USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_get_commonsearchdata_before_megahash_modify]    Script Date: 4/24/2016 10:55:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Kinjal Shah
-- alter date: 15-Dec-2010
-- Description:	Proc created for Search Bid data
-- Last Modified
-- Modified BY: Kinjal, Krish,  Rajesh
-- ===================eva==========================
----------------------------------------------------------------------------
-- SP Name	:   p_get_commonsearchdata
-- Module	:	
-- Function	:	purpose
--GetEmployee - get list of employess
--EvalClariDocs - get list of eval clari docs
--GetBidformInfo - get list of bidder from data
--getformnamebytenderidandlotid - get form name
--GetBidderLots - get bidder lot data
--GetBidderLotsForClarification - get bidder lot data 
--GetBidderPkgLots - get bidder package lot data
--getlotorpackagebytenderid - get lot or package data 
--getEncryptStatus - get encrypt status data
--getGrandSum - get grand summary data
--getformDetails - get form details data
--gettenderlotbytenderid - get tender lot data 
--getTenderDoc - get tender document data 
--getTenderMegaHash - get tender mega hash data 
--getTenderMegaMegaHash - get tender mega hash data 
--getTenderEncData - get tender enc data
--doFinalSubmission - by using this final submission data inserted
--chkBidWithdrawal - get bidder withdrwal data
--chkSubmissionDt - get submission date data
--SearchPendingProject - get pending project data 
--SearchApproveProject - get approved project data 
--LotPaymentListing - get lot payment listing data 
--SearchCommittee - get search committee data 
--DumpCommittee - get dump committee data 
--TenderOpenExt - get tender open ext data
--FinalSubDtIP - get final submission date and ip address
--GetTenderForms - get tender forms data 
--GetTenderFormsByUserId - get tender forms data by user id
--GetAppDetailByOfficeId - get app details data by office id
--GetAppDetailByPackageDtl - get app details data by package date id
--GetTenderFormsByLotId - get tender forms data 
--GetNegTenderFormsByLotId - get negotiation tender forms data
--ProcureNOA - get procure noa data 
--findRankerInfo - get ranker info data 
--getBusinessDays - get business days data 
--GetEvalComMemForBidderClari - 
--GetEvalComMem - get eval committee menber data 
--GetEvalComMemQuestion - get eval committee menber question data 
--GetEvalComMemQuestionByMem - get eval committee menber question data by member
--searchSubContractBidder - 
--evalBidderStatus
--evalComMember
--evalComMemberForFinalEvaluation
--EvalMemberRole
--EvalMemberStatus
--AAFillComboAPP
--pd
--pe
--hope
--Authorized Officer or AO
--BOD
--Secretary
--Minister
--CCGP
--Authorized User or au
--Development Partner or dp
--SeekClarTECCP
--getProcurementMethod
--getClarQuesPostedByCP
--GetQuesPostedByCP
--chkTECTSCComMember
--chkTOSComMember
--getEvalMemberStatus
--checkEvalServiceFrom
--GetComMem
--chkConfigStatus
--getPostedQueForms
--GetTenderFormsforTCP
--GetTenderFormsForSeekClari
--GetEvalFormByFormId
--GetDebaredData
--getCommListing
--FormsList
--GetCPQueTenderForms
--GetCPTenderForm
--GetEmployeeDetails
--GetNegBidderFormsByLotId
--getselectedformDetails
--Eval_isTscReq_Info
--chkTSCCommitteeStatus
--getTECChairPersonUserId
--GetManageUsersStatisticsOne
--GetManageUsersStatisticsTwo
--ValidityExtensionRequestListingPending
--ValidityExtensionRequestListingApproved
--getFormsForEvlauationMapping
--getSearchRegUserListing
--GetLoginReport
--EvalchkPostQuali
--showhidesubmissionbutton
--getBidderStatus
--GetTenderFormsByLotIdUid
--GetDocuments
--GetBidderDocuments
--GetFolders
--GetArchiveDocuments
--GetAllDocuments
--getIsSentToPEStatus
--GetQuesPostedByCPforMem
--getPostedQueFormsForBidder
--getPostedQueFormsForBidderWithoutBids
--getPostedQueFormsForBidderWithBids
--EvalClariDocsCP
--getRegFeePaymentAmount
--getMobNoforSMS
--GetFormsforTSC
--SearchTenPayment
--GetDebarmentUserStatus
--search_Procurementforum
--getPreTenderPaymentConditions
--getSeekClariLinkText
--getCronJob
--getExpiredRegCustList
--getLapsedRegCustList
--getPrebidEmpMobNo
--getTenderOpenEmpMobNo
--EvalSerFormsList
--getJVCACompanyPay
--getformnamebytenderidandlotid_ForLotPrep
--getlotid_lotdesc
--ChkTodoFinalSubmission
--getPriceBidfromLotId
--getLoIdLotDesc
--get2EnvLoIdLotDesc
--search_MediaContent
--getTenderEvalEmpMobNo
--getGovUserIdFromUserId
--GetComMemforEvalPost
--getClarificationToCPLink
--getFlagStatus
--GetDocumentsWithPaging
--GetFoldersWithPaging
--GetAllDocumentsWithPaging
--GetArchiveDocumentsWithPaging
--GetBidderDocumentsWithPaging
--getCancelledTenders
--getAppProject
--getEncryptStatusLink
--getTenderRightsName
--evalComMemberForReport
--GetEvalBidderClarificationStatus
--GetEvalCPClarificationExist
--getmarqueelisting
--getmarqueelistWithPaging
--ChkTenderAccess
--getDocRelatedInfo
--getFormNameByTenderId
--gettenderlotbytenderidForViewPayment
--getBidModificationInfo
--getTenSecExtLinkStatusMore
--getTenSecLastDt
--getGrandSumForOfficer
--getGrandSummaryLink
--getPaidTendererListForPE
--getCancelPaymentLinkStatus
--getTenderDocForIndi
--SubContractInvAppReq
--SubContract
--SubContractApprovedReq
--GetTenderAAHopeAo
--getTenderPays
--getGrandSumOpening
--getUserForFinalSubmission
--getComparativeGrandSumOpening
--getFormIdforCompReport
--evalBidderStatusRpt
--GetEvalFormFinalQuesCount
--getEvaluationStatus
--isSubContractingAccepted
--getAppEmailIdForPublishPKGCron
--getEmailIdForPublishTenderCron
--GetCriteria4FormId
--GetNegTenderFormsList
--GetNegBidderFormsData
--GetGrandSumNego
--GetNegFormDtl
--OpenCommitteeDumpCheck
--getFinalSubUser
				
----------------------------------------------------------------------------
-- Exec p_get_commonsearchdata 'GetAppDetailByOfficeId', '2', '3', '10', '1'
--Exec p_get_commonsearchdata 'GetLoginReport','10','2'
ALTER PROCEDURE [dbo].[p_get_commonsearchdata_before_megahash_modify] 
	-- Add the parameters for the stored procedure here
	 @v_fieldName1Vc varchar(500)=NULL,		  -- Action 
	 @v_fieldName2Vc varchar(500)=NULL,
	 @v_fieldName3Vc varchar(500)=NULL, 
	 @v_fieldName4Vc varchar(500)=NULL,
	 @v_fieldName5Vc varchar(500)=NULL,
	 @v_fieldName6Vc varchar(500)=NULL,
	 @v_fieldName7Vc varchar(500)=NULL,
	 @v_fieldName8Vc varchar(500)=NULL,
	 @v_fieldName9Vc varchar(500)=NULL,
	 @v_fieldName10Vc varchar(500)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @v_FinalQueryVc varchar(Max)=null, 
			@v_Query1Vc varchar(1000)=null,
			@v_ConditionString_Vc varchar(4000),
			@v_CntQry_Vc varchar(max),
			@v_PgCntQry_Vc varchar(max),
			@v_StartAt_Int int, @v_StopAt_Int int,
			@v_Page_inInt int ,
			@v_RecordPerPage_inInt int,
			@v_DocFeesMethod_Vc varchar(500),
			@v_ProcurementMethod_Vc varchar(50)
	
	DECLARE @v_finalSubId_int Int, 
			@v_signTxtList_Vc varchar(Max), 
			@v_jvcaCmpId int, 
			@v_cntjvcaCmpUser int, 
			@v_tenderMndCnt int, 
			@v_userMndCnt int, 
			@v_tenderEncCnt int, 
			@v_userEncCnt int,  
			@v_userfilCnt int, 
			@v_tenderEncfilCnt int, 
			@v_userEncfilCnt int,
			@v_userLotSelectedCnt int,
			@v_userLotTenSecPayCnt int,
			@v_isTenSecPayable bit,
			@v_cntSameCmpUser int,
			@v_EnvelopeCnt_Int int,
			@v_BidTenderManDocCnt_Int int,
			@v_BiderFilledTenderManDocCnt_Int int

			DECLARE @v_BankUserRole_Vc varchar(50), 
					@v_BankBranchId int, 
					@v_BankId int
			DECLARE @v_PagePerRecordN int

			----
			Declare @FinalQuery as varchar(max),
					@v_IntialQuery_Vc Varchar(1000)
			DECLARE @v_FormList_Vc varchar(2000)
			DECLARE @v_FormList_Vc1 varchar(2000)
			DECLARE @v_FormList_Vc3 varchar(2000)
    -- Insert statements for procedure here
IF @v_fieldName1Vc = 'GetEmployee'  --Get all rows from table tbl_FinancialYear
BEGIN
	set @v_Query1Vc = ''
	if @v_fieldName2Vc = 4
	begin
		set @v_Query1Vc = ' and  employeeId in (select employeeId from tbl_EmployeeOffices where officeId in(select officeId from tbl_OfficeAdmin where userId='+ @v_fieldName3Vc+'))'
	End
	else if @v_fieldName2Vc = 5
	begin
		set @v_Query1Vc = ' and  ( employeeId in (select employeeId from tbl_EmployeeOffices where officeId in(select officeId from tbl_OfficeMaster where departmentId in(select departmentId from tbl_DepartmentMaster where approvingAuthorityId='+@v_fieldName3Vc+'))) or  createdBy='+ @v_fieldName3Vc+' )'
	End
--	set @v_FinalQueryVc = 
--'select CONVERT(VARCHAR(10),a.employeeId) FieldValue1, FieldValue2, FieldValue3,CONVERT(VARCHAR(10),registereddate,103) FieldValue4, FieldValue5 ,convert(varchar(20),isnull(b.employeeId,0)) FieldValue6  from (select  e.employeeId  ,employeeName as FieldValue2,emailId as FieldValue3,registereddate   ,status as FieldValue5  from tbl_EmployeeMaster e,tbl_LoginMaster l where
--e.userId=l.userId ' + @v_Query1Vc + case when @v_fieldName4Vc IS not null then +' and '+ @v_fieldName4Vc else '' end  +')a left outer join (select distinct employeeid from tbl_employeeroles)b on a.employeeid=b.employeeid  order by registereddate desc '
--rajesh
	set @v_FinalQueryVc = 'select	CONVERT(VARCHAR(10),a.employeeId) FieldValue1, 
									FieldValue2, 
									FieldValue3,
									REPLACE(CONVERT(VARCHAR(11),registereddate, 106), '' '', ''-'') FieldValue4, 
									FieldValue5 ,
									convert(varchar(20),isnull(b.employeeId,0)) FieldValue6 ,
									isnull(departmentname,'''') as FieldValue7, 
									case when FieldValue8 =''0'' then ''''  else convert(varchar(100),FieldValue8) end FieldValue8  
							from	(select	e.employeeId  ,
											employeeName as FieldValue2,
											emailId as FieldValue3,
											registereddate   ,
											status as FieldValue5,
											(select		dbo.getEmpRoles(e.userid)) as FieldValue8  
											from		tbl_EmployeeMaster e,
														tbl_LoginMaster l 
											where		e.userId=l.userId ' + @v_Query1Vc + 
														case 
															when @v_fieldName7Vc IS not null and @v_fieldName7Vc<>''  then +' and '+ @v_fieldName4Vc 
															when @v_fieldName4Vc IS not null And @v_fieldName6Vc='EQ' then +' and '+ @v_fieldName4Vc + ' = ''' + @v_fieldName5Vc + ''''
															when @v_fieldName4Vc IS not null And @v_fieldName6Vc='CN' then +' and '+ @v_fieldName4Vc + ' like''%' + @v_fieldName5Vc + '%'''
														else '' 
														end  +'
											)a 
									left outer join (select distinct employeeid,
															departmentname  
													 from	tbl_employeeroles er,
															tbl_departmentmaster dm 
													where	er.departmentId=dm.departmentId
													)b on a.employeeid=b.employeeid  
							order by FieldValue2   '

print @v_FinalQueryVc

exec(@v_FinalQueryVc)
END 




If @v_fieldName1Vc='EvalClariDocs'
BEGIN
  -- Begin
	--	Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(evalClrDocId  as varchar(10)) as FieldValue4 ,doctype as FieldValue5,
	--	cast(uploadedBy  as varchar(10)) as FieldValue6
--		from tbl_EvalClariDocs Where tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc and   formId=@v_fieldName4Vc
 --  End
--IF @v_fieldName1Vc = 'GetTest'  --Get all rows from table tbl_FinancialYear
--BEGIN
--select 
-- convert(varchar(50),folderId ) as FieldValue1,
-- convert(varchar(150),folderName) as FieldValue2         
 --     from tbl_TendererFolderMaster c,tbl_TendererMaster t
--where c.tendererId=t.tendererId and t.tendererId = 22
--END
		If @v_fieldName5Vc is not null And @v_fieldName5Vc<>''
		Begin
		-- // Officer Wise Condition
			Select documentName as FieldValue1, docDescription as FieldValue2, 
			docSize as FieldValue3,cast(evalClrDocId  as varchar(10)) as FieldValue4 ,
			doctype as FieldValue5,	cast(uploadedBy  as varchar(10)) as FieldValue6,
			Case doctype 
			When 'Officer'
			Then Convert(varchar(50),uploadedBy) + '_' + Convert(varchar(50),userId)+ '_' + Convert(varchar(50),formId) 
			When 'Tenderer'
			Then Convert(varchar(50),userId) + '_' + Convert(varchar(50),formId) 
			End
			as FieldValue7
			from tbl_EvalClariDocs 
			Where tenderId=@v_fieldName2Vc 
				  and 
				  formId=@v_fieldName3Vc
				  And 
					(
						( userId=@v_fieldName4Vc and  Uploadedby=@v_fieldName4Vc )			  
					 Or (userId=@v_fieldName4Vc and  Uploadedby=@v_fieldName5Vc )
				   )
		End
		Else 
		Begin
			-- // Bidder Wise Condition
			Select documentName as FieldValue1, docDescription as FieldValue2, 
			docSize as FieldValue3,cast(evalClrDocId  as varchar(10)) as FieldValue4 ,
			doctype as FieldValue5,	cast(uploadedBy  as varchar(10)) as FieldValue6,
			Case doctype 
				When 'Officer'
				Then Convert(varchar(50),uploadedBy) + '_' + Convert(varchar(50),userId)+ '_' + Convert(varchar(50),formId) 
				When 'Tenderer'
				Then Convert(varchar(50),userId) + '_' + Convert(varchar(50),formId) 
				End
			as FieldValue7
			from tbl_EvalClariDocs 
			Where tenderId=@v_fieldName2Vc 		
				and formId=@v_fieldName3Vc
				and userId=@v_fieldName4Vc		
		End

END
IF @v_fieldName1Vc = 'GetBidformInfo'  --Get all rows from table tbl_FinancialYear
BEGIN

SELECT convert(varchar(20),bidid) as FieldValue1 FROM tbl_TenderBidForm 
WHERE tenderId = @v_fieldName2Vc and tenderformId = @v_fieldName3Vc and userId = @v_fieldName4Vc

END


IF @v_fieldName1Vc = 'getformnamebytenderidandlotid'     -- For tenderer/BidPreperation.jsp -- Kinjal
BEGIN
		
	IF @v_fieldName3Vc <> '0' --- LOT CASE
	BEGIN
	
		SELECT formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,tf.formStatus as FieldValue6, 
		'' as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9
		FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 
		Where td.tenderId = @v_fieldName2Vc and packageLotId = @v_fieldName3Vc
		Order By isPriceBid	
			
	END
	ELSE --- PACKAGE CASE
	BEGIN
		SELECT formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,tf.formStatus as FieldValue6, 
		'' as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9
		FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 
		Where td.tenderId = @v_fieldName2Vc
		Order By isPriceBid
	END
		
END

IF @v_fieldName1Vc = 'GetBidderLots'     -- For tenderer/BidPreperation.jsp
BEGIN
		
	IF @v_fieldName3Vc = 'Lot'
	BEGIN
		SELECT DISTINCT lotNo as FieldValue1, lotDesc as FieldValue2, Convert(Varchar,appPkgLotId) as  FieldValue3
		FROM  dbo.tbl_TenderLotSecurity Inner Join dbo.tbl_TenderForms On 
		dbo.tbl_TenderLotSecurity.appPkgLotId = dbo.tbl_TenderForms.pkgLotId
		Inner Join tbl_TenderBidForm tbf on tbf.tenderFormId = dbo.tbl_TenderForms.tenderFormId
		WHERE dbo.tbl_TenderLotSecurity.tenderId = @v_fieldName2Vc
		AND tbf.userId = @v_fieldName4Vc
		
		
	END
	ELSE
	BEGIN
		
		SELECT packageNo as FieldValue1, packageDescription as FieldValue2 FROM 
			dbo.tbl_TenderDetails WHERE tenderId = @v_fieldName2Vc
		
	END
		
END


IF @v_fieldName1Vc = 'GetBidderLotsForClarification'     
BEGIN
		
	IF @v_fieldName3Vc = 'Lot'
	BEGIN
		SELECT DISTINCT lotNo as FieldValue1, lotDesc as FieldValue2, Convert(Varchar,appPkgLotId) as  FieldValue3
		FROM  dbo.tbl_TenderLotSecurity Inner Join dbo.tbl_TenderForms On 
		dbo.tbl_TenderLotSecurity.appPkgLotId = dbo.tbl_TenderForms.pkgLotId
		Inner Join tbl_TenderBidForm tbf on tbf.tenderFormId = dbo.tbl_TenderForms.tenderFormId
		WHERE dbo.tbl_TenderLotSecurity.tenderId = @v_fieldName2Vc
		AND tbf.userId = @v_fieldName4Vc
		and dbo.tbl_TenderLotSecurity.appPkgLotId in
		(
			select distinct pkgLotId from tbl_EvalFormQues 
			where tenderId=@v_fieldName2Vc 
			and userId=@v_fieldName4Vc 
			and tenderFormId 			
			in (Select distinct  tenderFormId 
				from tbl_EvalFormQues 
				where tenderId = @v_fieldName2Vc 
				and userId = @v_fieldName4Vc
				and 
				( 
					quePostedBy=@v_fieldName5Vc
					Or 
					quePostedBy in (select distinct sentBy from tbl_EvalSentQueToCp where tenderId =@v_fieldName2Vc and sentFor='question')
				)
			) 			
		)
	END
	ELSE
	BEGIN
		
		SELECT packageNo as FieldValue1, packageDescription as FieldValue2 FROM 
			dbo.tbl_TenderDetails WHERE tenderId = @v_fieldName2Vc
		
	END
		
END

IF @v_fieldName1Vc = 'GetBidderPkgLots'     -- For tenderer/BidPreperation.jsp
BEGIN
		
	IF @v_fieldName3Vc = 'Lot'
	BEGIN
		
		SELECT DISTINCT lotNo as FieldValue1, lotDesc as FieldValue2, Convert(Varchar,appPkgLotId) as  FieldValue3
		FROM  dbo.tbl_TenderLotSecurity Inner Join dbo.tbl_TenderBidForm On 
		dbo.tbl_TenderLotSecurity.appPkgLotId = dbo.tbl_TenderBidForm.pkgLotId 
		WHERE dbo.tbl_TenderLotSecurity.tenderId = @v_fieldName2Vc 
		AND dbo.tbl_TenderBidForm.userId = @v_fieldName4Vc and 
		dbo.tbl_TenderLotSecurity.appPkgLotId=@v_fieldName5Vc
		
	END
	ELSE
	BEGIN
		
		SELECT packageNo as FieldValue1, packageDescription as FieldValue2 FROM 
			dbo.tbl_TenderDetails WHERE tenderId = @v_fieldName2Vc
		
	END
		
END

IF @v_fieldName1Vc = 'getlotorpackagebytenderid'     -- For tenderer/BidPreperation.jsp
BEGIN
		
	IF @v_fieldName3Vc = 'Lot'
	BEGIN		
		SELECT lotNo as FieldValue1, lotDesc as FieldValue2 
		FROM  dbo.tbl_TenderLotSecurity 
		WHERE tenderId = @v_fieldName2Vc AND appPkgLotId = @v_fieldName4Vc	
	END
	ELSE
	BEGIN		
		SELECT packageNo as FieldValue1, packageDescription as FieldValue2 FROM 
			dbo.tbl_TenderDetails WHERE tenderId = @v_fieldName2Vc		
	END
		
END

IF @v_fieldName1Vc = 'getEncryptStatus'     -- For tenderer/BidPreperation.jsp
BEGIN

Select CONVERT(varchar(20),tf.finalSubmissionId) as FieldValue1  from  tbl_FinalSubmission tf,tbl_FinalSubDetail tfd where  
tf.finalSubmissionId = tfd.finalSubmissionId and tenderId = @v_fieldName2Vc and
tenderformId = @v_fieldName3Vc and  userId = @v_fieldName4Vc and bidId = @v_fieldName5Vc
END

IF @v_fieldName1Vc = 'getGrandSum'     -- For tenderer/BidPreperation.jsp
BEGIN
		if @v_fieldName4Vc !=0
		begin
				select CONVERT(varchar(2000),formName) as FieldValue1,cellValue as FieldValue2  from (select tg.cellId,tg.tendertableid,formName from tbl_TenderGrandSum g,tbl_TenderGrandSumDetail tg,tbl_TenderForms tf
				where g.tenderSumId=tg.tenderSumId  and tenderId=@v_fieldName2Vc
				and tf.tenderFormId=tg.tenderFormId and g.pkgLotId=@v_fieldName4Vc)a
				inner join (
				select tb.tenderTableId,b.cellId,cellValue from tbl_TenderBidDetail b,tbl_TenderBidTable tb,tbl_tenderbidform bf
				where bf.bidId=tb.bidId and tb.bidTableId=b.bidTableId 
				and tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc )b
				on a.tenderTableId=b.tenderTableId and a.cellId=b.cellId  
		end
		else
		begin
		
						select CONVERT(varchar(2000),formName) as FieldValue1,cellValue as FieldValue2  from (select tg.cellId,tg.tendertableid,formName from tbl_TenderGrandSum g,tbl_TenderGrandSumDetail tg,tbl_TenderForms tf
				where g.tenderSumId=tg.tenderSumId  and tenderId=@v_fieldName2Vc
				and tf.tenderFormId=tg.tenderFormId)a
				inner join (
				select tb.tenderTableId,b.cellId,cellValue from tbl_TenderBidDetail b,tbl_TenderBidTable tb,tbl_tenderbidform bf
				where bf.bidId=tb.bidId and tb.bidTableId=b.bidTableId 
				and tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc )b
				on a.tenderTableId=b.tenderTableId and a.cellId=b.cellId  
		
		end
		
END

IF @v_fieldName1Vc = 'getformDetails'     -- For Finalsubmission / Submisn receipt
BEGIN
select distinct convert(varchar(25),a.tenderFormId )as FieldValue1 ,formName as FieldValue2,
case when b.tenderFormId IS null then 'No' else 'Yes' end FieldValue3,
case when isEncryption='Yes' then case when c.tenderFormId IS null then 'No' else 'Yes' end else '-' END FieldValue4,
case when d.tenderFormId IS null then 'No' else signtext end FieldValue5, case isMandatory when 'yes' then 'Yes' else 'No' end as FieldValue6,newFormCombinedHash as FieldValue7

 from (select tenderid,packageLotId, t.tenderFormId,formName,isEncryption, isMandatory from tbl_TenderForms t,tbl_TenderSection s,tbl_TenderStd std
where t.tenderSectionId=s.tenderSectionId and s.tenderStdId=std.tenderStdId
and tenderId=@v_fieldName2Vc 
and 
case when @v_fieldName4Vc <> '0' then case when packageLotId = @v_fieldName4Vc then 1 else 0 end else 1 end =1

)a left outer join 
(select distinct tenderFormId from tbl_TenderBidForm where tenderId =@v_fieldName2Vc and userId=@v_fieldName3Vc )b
on a.tenderFormId=b.tenderFormId
left outer join 
(select distinct tenderFormId,newFormCombinedHash from tbl_FinalSubmission f,tbl_FinalSubDetail fd 
where f.finalSubmissionId=fd.finalSubmissionId and  tenderId =@v_fieldName2Vc  and userId=@v_fieldName3Vc)c
on a.tenderFormId=c.tenderFormId
left outer join 
(select distinct tenderFormId,signtext from tbl_TenderBidSign s,tbl_TenderBidForm b
 where tenderId =@v_fieldName2Vc and userId=@v_fieldName3Vc and b.bidId=s.bidId )d
on a.tenderFormId=d.tenderFormId

End


IF @v_fieldName1Vc = 'gettenderlotbytenderid'     -- For LotTendPrep.jsp / get lotInfo by tenderid
BEGIN
		
		SELECT convert(varchar(20),lotNo) as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3 		FROM  dbo.tbl_TenderLotSecurity 
		WHERE tenderId = @v_fieldName2Vc  
	
END


IF @v_fieldName1Vc = 'getTenderDoc'     -- For LotTendPrep.jsp : get docs based on FormId, usrid, tenderid
BEGIN

select convert(varchar(20),c.companyDocId) as FieldValue1,documentName as FieldValue2, documentSize as FieldValue3, documentBrief as FieldValue4 from tbl_companydocuments c,tbl_biddocuments b
where c.companyDocId=b.companyDocId 
and tenderId=@v_fieldName2Vc and formId=@v_fieldName3Vc and userId = @v_fieldName4Vc

END
IF @v_fieldName1Vc = 'getTenderMegaHash'     -- For LotTendPrep.jsp : get docs based on FormId, usrid, tenderid
BEGIN

 select distinct convert(varchar(200),tenderhash) as FieldValue1 from tbl_FinalSubmission 
 where tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc

END
IF @v_fieldName1Vc = 'getTenderMegaMegaHash'     -- For LotTendPrep.jsp : get docs based on FormId, usrid, tenderid
BEGIN

 select distinct convert(varchar(200),megaHash) as FieldValue1 from tbl_TenderMegaHash where tenderId=@v_fieldName2Vc

END
IF @v_fieldName1Vc = 'getTenderEncData'     -- For LotTendPrep.jsp
BEGIN

If (Select isPriceBid from tbl_TenderForms Where tenderFormId=@v_fieldName3Vc)='Yes'  -- For Price bid Forms
Begin		
	Select @v_EnvelopeCnt_Int= tcm.noOfEnvelops
	from tbl_TenderDetails ttd,tbl_TenderTypes ttt,tbl_ConfigEvalMethod tcm
	 where ttd.eventType =ttt.tenderType
	 and ttd.procurementMethodId =tcm.procurementMethodId 
	 and ttt.tenderTypeId=tcm.tenderTypeId  
	 and ttd.procurementNatureId =tcm.procurementNatureId 
	 and ttd.tenderId=@v_fieldName2Vc
	 
	 if @v_EnvelopeCnt_Int>1
	 begin
			select  convert(varchar(20),tbt.bidTableId) as FieldValue1
		  , convert(varchar(20),tendercolumnid) as FieldValue2
		  , convert(varchar(20),f.[tenderTableId]) as FieldValue3
		  , convert(varchar(max),[cellValue]) as FieldValue4
		  , convert(varchar(20),[rowId]) as FieldValue5
		  , convert(varchar(20),[cellId]) as FieldValue6 
		  from tbl_TenderBidEncrypt t,tbl_TenderTables f, tbl_TenderBidTable tbt, tbl_TenderBidForm tbf
		  where f.tenderFormId=@v_fieldName3Vc and f.tenderTableId=t.tenderTableId 
		  and 
		  tbf.bidId=tbt.bidId and tbt.bidTableId=t.bidTableId and f.tenderTableId=tbt.tenderTableId
		  --and t.formId not in (select tenderFormId from tbl_TenderBidPlainData tbpd where tbpd.tenderFormId = @v_fieldName3Vc)
		  And tbf.userId in
					(select distinct userId from Tbl_EvalBidderStatus where tenderId=@v_fieldName2Vc and (bidderStatus='Technically Responsive'or result='pass'))
		 end
	 else
	 begin
			select  convert(varchar(20),[bidTableId]) as FieldValue1
      , convert(varchar(20),tendercolumnid) as FieldValue2
      , convert(varchar(20),f.[tenderTableId]) as FieldValue3
      , convert(varchar(max),[cellValue]) as FieldValue4
      , convert(varchar(20),[rowId]) as FieldValue5
      , convert(varchar(20),[cellId]) as FieldValue6 from tbl_TenderBidEncrypt t,tbl_TenderTables f
      where   f.tenderFormId=@v_fieldName3Vc and f.tenderTableId=t.tenderTableId 
      --and t.formId not in (select tenderFormId from tbl_TenderBidPlainData tbpd where tbpd.tenderFormId = @v_fieldName3Vc)
	 end
End
Else -- For Technical Forms
Begin
	select  convert(varchar(20),[bidTableId]) as FieldValue1
      , convert(varchar(20),tendercolumnid) as FieldValue2
      , convert(varchar(20),f.[tenderTableId]) as FieldValue3
      , convert(varchar(max),[cellValue]) as FieldValue4
      , convert(varchar(20),[rowId]) as FieldValue5
      , convert(varchar(20),[cellId]) as FieldValue6 from tbl_TenderBidEncrypt t,tbl_TenderTables f
      where f.tenderFormId=@v_fieldName3Vc and f.tenderTableId=t.tenderTableId 
      --and t.formId not in (select tenderFormId from tbl_TenderBidPlainData tbpd where tbpd.tenderFormId = @v_fieldName3Vc)
End




--select  convert(varchar(20),[bidTableId]) as FieldValue1
--      , convert(varchar(20),tendercolumnid) as FieldValue2
--      , convert(varchar(20),f.[tenderTableId]) as FieldValue3
--      , convert(varchar(max),[cellValue]) as FieldValue4
--      , convert(varchar(20),[rowId]) as FieldValue5
--      , convert(varchar(20),[cellId]) as FieldValue6 from tbl_TenderBidEncrypt t,tbl_TenderTables f
--      where   f.tenderFormId=@v_fieldName3Vc and f.tenderTableId=t.tenderTableId  

 

END

IF @v_fieldName1Vc = 'doFinalSubmission'     -- For LotTendPrep.jsp : Final Sub Button Condition
BEGIN
	BEGIN TRY
		BEGIN TRAN
/***
@v_fieldName1Vc: Method Name
@v_fieldName2Vc: TenderId
@v_fieldName3Vc: UserId
@v_fieldName4Vc: IP ADDRESS

**/
--DECLARE @v_finalSubId_int Int, @v_signTxtList_Vc varchar(Max), @v_jvcaCmpId int, @v_cntjvcaCmpUser int, @v_tenderMndCnt int, @v_userMndCnt int, @v_tenderEncCnt int, @v_userEncCnt int,  @v_userfilCnt int, @v_tenderEncfilCnt int, @v_userEncfilCnt int
set   @v_userEncCnt=0

DECLARE @v_tenderHash_Vc VARCHAR(MAX) -- Dohatec Add for resolve verify mega mega hash problem

/* Start : Code To Check Bid Submission for Same Company*/
select @v_cntSameCmpUser = count(finalSubmissionId) 
FROM tbl_FinalSubmission tfs 
INNER JOIN tbl_TendererMaster tm ON tfs.userId = tm.userId 
and tenderId = @v_fieldName2Vc 
and bidSubStatus = 'finalsubmission' 
And (tfs.userId in (
	select userId from tbl_TendererMaster 
		where companyId =
		(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1))
		or tfs.userId =@v_fieldName3Vc)
 
/* End */

-- Count of Lots Filled by Bidder
Select @v_userLotSelectedCnt=Count(tls.appPkgLotId) 
from tbl_TenderLotSecurity tls
Inner Join tbl_bidderLots tbl On tls.tenderId=tbl.tenderId And tls.appPkgLotId=tbl.pkgLotId
Where tbl.tenderId=@v_fieldName2Vc And tbl.userId=@v_fieldName3Vc

-- Check Whether Tender Security is Payable OR Not
select @v_isTenSecPayable=
Case When SUM(tenderSecurityAmt) > 0 Then 1 Else 0 End 
from tbl_TenderLotSecurity 
where tenderId=@v_fieldName2Vc

-- Count of Lots for which Tender Security has been paid by Bidder
Select @v_userLotTenSecPayCnt=Count(tls.appPkgLotId) 
from tbl_TenderLotSecurity tls
Inner Join tbl_bidderLots tbl On tls.tenderId=tbl.tenderId And tls.appPkgLotId=tbl.pkgLotId
Inner Join tbl_TenderPayment tp On tbl.tenderId=tp.tenderId And tbl.pkgLotId=tp.pkgLotId
Where tbl.tenderId=@v_fieldName2Vc And 
(tbl.userId in (select distinct userId from tbl_TendererMaster 
											where companyId =(select companyId from tbl_TendererMaster 
											where userId=@v_fieldName3Vc and companyId!=1) ) or tbl.userId=@v_fieldName3Vc )
And (tp.userId in 
(select distinct userId from tbl_TendererMaster 
											where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1) ) or tp.userId=@v_fieldName3Vc ) 
And paymentFor='Tender Security'
And isVerified='yes'



select @v_BidTenderManDocCnt_Int = count(distinct tenderFormDocId)
from tbl_TenderMandatoryDoc tmd
inner join tbl_TenderBidForm bd on tmd.tenderFormId=bd.tenderFormId and tmd.tenderId=bd.tenderId
where bd.tenderId=@v_fieldName2Vc
      and bd.userId=@v_fieldName3Vc	
      
select @v_BiderFilledTenderManDocCnt_Int = count(distinct manDocId)
from tbl_BidDocuments
where manDocId<>0
	and tenderId=@v_fieldName2Vc
	and userId=@v_fieldName3Vc

IF (SELECT TOP 1 tenderStatus FROM tbl_TenderDetails WHERE tenderid = @v_fieldName2Vc) != 'cancelled'
BEGIN
IF EXISTS (SELECT TOP 1 submissionDt FROM tbl_TenderDetails WHERE tenderid = @v_fieldName2Vc and submissiondt >= convert(smalldatetime, GETDATE()))
-- SUBMISSION Time NOT over
BEGIN

/** BEGIN Code : CHECK CONDITION FOR JVCA PARTNER **/

	
--SELECT @v_jvcaCmpId = companyId
--FROM tbl_LoginMaster lm INNER JOIN
--     tbl_TendererMaster tm ON lm.userId = tm.userId AND lm.userId = tm.userId
--     Where isjvca = 'yes' and lm.userId  = @v_fieldName3Vc and lm.status = 'approved'
--print @v_jvcaCmpId
--SELECT @v_cntjvcaCmpUser = count(finalSubmissionId) FROM tbl_FinalSubmission tfs INNER JOIN tbl_TendererMaster tm  ON tfs.userId = tm.userId and tenderId = @v_fieldName2Vc and bidSubStatus = 'finalsubmission' and companyId in 
--     ( SELECT jvCompanyId FROM tbl_CompanyJointVenture WHERE companyId =@v_jvcaCmpId )
--/** END Code : CHECK CONDITION FOR JVCA PARTNER  **/
--print @v_cntjvcaCmpUser
--IF (SELECT TOP 1 tenderStatus FROM tbl_TenderDetails WHERE tenderid = @v_fieldName2Vc) != 'cancelled'
--BEGIN
--select @v_cntjvcaCmpUser = count(finalSubmissionId) 
--FROM tbl_FinalSubmission tfs 
--INNER JOIN tbl_TendererMaster tm 
-- ON tfs.userId = tm.userId and tenderId = @v_fieldName2Vc 
-- and bidSubStatus = 'finalsubmission' 
-- and companyId in (select companyId from tbl_JVCAPartners where JVId in( select jv.JVId from tbl_JointVenture j,tbl_JVCAPartners jv
--where j.JVId=jv.JVId and JVStatus='Approved' and userId=@v_fieldName3Vc))
--End
set @v_cntjvcaCmpUser=0
IF (SELECT isJvca  FROM tbl_FinalSubmission f,tbl_LoginMaster l WHERE 
 f.userId=l.userId and 
tenderid = @v_fieldName2Vc and f.userId= @v_fieldName3Vc 
and bidSubStatus = 'finalsubmission'   ) = 'yes'
BEGIN
print '11'
select     @v_cntjvcaCmpUser = count(finalSubmissionId) 
FROM tbl_FinalSubmission tfs 
INNER JOIN tbl_TendererMaster tm 
 ON tfs.userId = tm.userId and tenderId = @v_fieldName2Vc 
 and bidSubStatus = 'finalsubmission' 
 
 And tfs.userId in (select userid from tbl_TendererMaster where companyId in( select distinct lm.companyId from tbl_JVCAPartners jp
														inner join tbl_TendererMaster lm On jp.userId=lm.userId
														where JVId in( select jv.JVId from tbl_JointVenture j,tbl_JVCAPartners jv where j.JVId=jv.JVId and JVStatus='Approved' and userId=@v_fieldName3Vc)
												  )
												union
									select userId from tbl_TendererMaster where companyId in( select companyId from tbl_tenderermaster where userid in(select newJVUserId from tbl_JointVenture j,tbl_JVCAPartners jv where j.JVId=jv.JVId and JVStatus='Approved' and userId=@v_fieldName3Vc))			    
												  ) 
 
End
else
begin
print '12'
IF (SELECT isJvca  FROM  tbl_LoginMaster l WHERE 
 
  userId= @v_fieldName3Vc 
   ) = 'yes'
begin
select @v_cntjvcaCmpUser = count(finalSubmissionId) 
									from tbl_FinalSubmission 
									where bidSubStatus = 'finalsubmission' 
									And userId in (select userid from tbl_tenderermaster where companyid in(select distinct lm.companyId from tbl_JVCAPartners jp
														inner join tbl_TendererMaster lm On jp.userId=lm.userId	 
														where JVId in( select jv.JVId from tbl_JointVenture j,tbl_JVCAPartners jv where j.JVId=jv.JVId and JVStatus='Approved' and newJVUserId=@v_fieldName3Vc)
														
												  )
												  			union
									select userId from tbl_TendererMaster where companyId in( select companyId from tbl_tenderermaster where userid in(select newJVUserId from tbl_JointVenture j,tbl_JVCAPartners jv where j.JVId=jv.JVId and JVStatus='Approved' and newJVUserId=@v_fieldName3Vc))			    
												  ) 
									and tenderId=@v_fieldName2Vc
end
else
begin
	select     @v_cntjvcaCmpUser = count(finalSubmissionId) 
FROM tbl_FinalSubmission tfs 
INNER JOIN tbl_TendererMaster tm 
 ON tfs.userId = tm.userId and tenderId = @v_fieldName2Vc 
 and bidSubStatus = 'finalsubmission' 
 
 And tfs.userId in ( 
									select userId from tbl_TendererMaster where companyId in( select companyId from tbl_tenderermaster where userid in(select newJVUserId from tbl_JointVenture j,tbl_JVCAPartners jv where j.JVId=jv.JVId and JVStatus='Approved' and userId=@v_fieldName3Vc))			    
												  )
end									
end
print @v_cntjvcaCmpUser
IF @v_cntjvcaCmpUser = 0 --- IF NO JVCA PARTNER HAS DONE

BEGIN

print 'JVCA LOOP'
/** BEGIN Code : GET (_) saperated HASH values **/
	
	Select @v_signTxtList_Vc = COALESCE(@v_signTxtList_Vc+'_' , '') + signText
from tbl_tenderbidsign tbs, tbl_TenderBidForm tbf 
where tbs.bidId = tbf.bidId and tenderId = @v_fieldName2Vc and userId =@v_fieldName3Vc
print @v_signTxtList_Vc
/** END Code : GET (_) saperated HASH values **/

--Tender Mandatory Count
SELECT  @v_tenderMndCnt = count(distinct tbl_TenderForms.tenderFormId) 
FROM         tbl_TenderSection INNER JOIN
                      tbl_TenderStd ON tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId AND 
                      tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId INNER JOIN
                      tbl_TenderForms ON tbl_TenderSection.tenderSectionId = tbl_TenderForms.tenderSectionId
WHERE tenderId = @v_fieldName2Vc and isMandatory='Yes' and isnull(formStatus,'a') !='c'
And (tbl_TenderForms.pkgLotId=0 Or tbl_TenderForms.pkgLotId in 
		(select distinct pkgLotId from tbl_bidderLots Where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc)
    )                   


IF @v_tenderMndCnt > 0
BEGIN

--Bidder Filled Mandatory Count 	
		SELECT @v_userMndCnt = count(distinct tbl_TenderForms.tenderFormId) 
		FROM  tbl_TenderSection INNER JOIN
                      tbl_TenderStd ON tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId AND 
                      tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId INNER JOIN
                      tbl_TenderForms ON tbl_TenderSection.tenderSectionId = tbl_TenderForms.tenderSectionId
                      inner join tbl_TenderBidForm on tbl_TenderBidForm.tenderId=tbl_TenderStd.tenderId
                      and tbl_TenderBidForm.tenderFormId=tbl_TenderForms.tenderFormId and isnull(formStatus,'a') !='c'						
		WHERE tbl_TenderBidForm.tenderId = @v_fieldName2Vc  and tbl_TenderBidForm.userId=@v_fieldName3Vc  and isMandatory='Yes'
		And (tbl_TenderForms.pkgLotId=0 Or tbl_TenderForms.pkgLotId in 
		(select distinct pkgLotId from tbl_bidderLots Where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc)
    )   
		
		IF @v_userMndCnt>= @v_tenderMndCnt  --- compare tender mndCnt and user mndcount
		BEGIN
		---Tender Encryption forms count
			SELECT  @v_tenderEncCnt = count(distinct tbl_TenderForms.tenderFormId) 
			FROM	tbl_TenderSection INNER JOIN
                    tbl_TenderStd ON tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId AND 
                    tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId INNER JOIN
                    tbl_TenderForms ON tbl_TenderSection.tenderSectionId = tbl_TenderForms.tenderSectionId						inner join tbl_TenderBidForm on tbl_TenderBidForm.tenderId=tbl_TenderStd.tenderId
                      and tbl_TenderBidForm.tenderFormId=tbl_TenderForms.tenderFormId and isnull(formStatus,'a') !='c'	
					WHERE tbl_TenderBidForm.tenderId = @v_fieldName2Vc and tbl_TenderBidForm.userId=@v_fieldName3Vc    and isEncryption='Yes' 
					And (tbl_TenderForms.pkgLotId=0 Or tbl_TenderForms.pkgLotId in 
		(select distinct pkgLotId from tbl_bidderLots Where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc)
    )   
				--print 'TENDER ENC CNT : ' + convert(varchar(10),@v_tenderEncCnt	)
					
			IF @v_tenderEncCnt > 0
			BEGIN
			--Bidder Encrypted forms count
				SELECT @v_userEncCnt= count(distinct tbl_TenderForms.tenderFormId) 
				FROM  tbl_TenderSection INNER JOIN
                      tbl_TenderStd ON tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId AND 
                      tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId INNER JOIN
                      tbl_TenderForms ON tbl_TenderSection.tenderSectionId = tbl_TenderForms.tenderSectionId and isnull(formStatus,'a') !='c' 
                      inner join tbl_TenderBidForm on tbl_TenderBidForm.tenderId=tbl_TenderStd.tenderId
                      and tbl_TenderBidForm.tenderFormId=tbl_TenderForms.tenderFormId 
                      INNER JOIN tbl_FinalSubmission on tbl_FinalSubmission.tenderId=tbl_TenderBidForm.tenderId and tbl_FinalSubmission.userid=tbl_TenderBidForm.userid inner join tbl_FinalSubDetail 
                      on tbl_FinalSubDetail.finalSubmissionId=tbl_FinalSubmission.finalSubmissionId		
                       and tbl_FinalSubDetail.tenderFormid=tbl_TenderBidForm.tenderFormId
      WHERE tbl_TenderBidForm.tenderId = @v_fieldName2Vc and tbl_TenderBidForm.userId=@v_fieldName3Vc
       and isEncryption='Yes'
       And (tbl_TenderForms.pkgLotId=0 Or tbl_TenderForms.pkgLotId in 
		(select distinct pkgLotId from tbl_bidderLots Where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc)
    )   
			END
		 
		--print 'BIDDER ENC CNT : ' + convert(varchar(10),@v_userEncCnt	)
		 
			IF  @v_userEncCnt >= @v_tenderEncCnt --- compare tender ENC Cnt and user ENC count
			BEGIN
				/** BEGIN ----------------------- FINAL SUBMISSION --------------------------********/
    	--print 'IN @v_userEncCnt >= @v_tenderEncCnt LOOP'
    	
    	IF @v_BiderFilledTenderManDocCnt_Int >= @v_BidTenderManDocCnt_Int 
    	BEGIN
    		/********************* Chk ENTRY Exists / Not In tbl_FinalSubmission ****************************/
			IF EXISTS(SELECT finalSubmissionId FROM tbl_FinalSubmission WHERE tenderId = @v_fieldName2Vc  and userId = @v_fieldName3Vc)
			BEGIN --- Exists
			print 'FInal 1 : '
			
			If @v_cntSameCmpUser>0
			Begin
				Select '0' as FieldValue1, 'Final Submission already done for this Company' as FieldValue2
			End
			Else
			Begin	
				If (@v_isTenSecPayable = 1) And (@v_userLotTenSecPayCnt < @v_userLotSelectedCnt)
				Begin
					Select '0' as FieldValue1, 'Tender Security Payment is Pending' as FieldValue2							
				End
				Else
				Begin
					Select @v_finalSubId_int = finalSubmissionId FROM tbl_FinalSubmission WHERE tenderId = @v_fieldName2Vc  and userId = @v_fieldName3Vc
					/********************* ENTRY Exists/Not IN tbl_FinalSubDetail ****************************/
					--IF EXISTS (SELECT tenderformId FROM tbl_TenderBidForm WHERE tenderId = @v_fieldName2Vc  and userId = @v_fieldName3Vc and tenderformId not in (SELECT tenderformid FROM tbl_FinalSubDetail WHERE finalsubmissionid = @v_finalSubId_int) )
					--BEGIN --IF ENTRY Not IN tbl_FinalSubDetail then Insert in it and Update tbl_FinalSubmission
						
						INSERT INTO tbl_FinalSubDetail(tenderFormid,ipAddress,finalSubmissionId, bidid)
						SELECT tenderformId,  @v_fieldName4Vc, @v_finalSubId_int, bidid 
						FROM tbl_TenderBidForm WHERE tenderId = @v_fieldName2Vc  and userId = @v_fieldName3Vc and tenderformId not in (SELECT tenderformid FROM tbl_FinalSubDetail WHERE finalsubmissionid = @v_finalSubId_int) 
			
						
						UPDATE tbl_FinalSubmission 
						SET bidSubStatus = 'finalsubmission',
						tenderHash = SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('SHA1', @v_signTxtList_Vc)), 3, 41), receiptNo = SUBSTRING(convert(varchar(300),NEWID()),1,8), finalSubmissionDt = convert(smalldatetime,GETDATE()),ipaddress = 	@v_fieldName4Vc	
						WHERE tenderId =@v_fieldName2Vc and userId = @v_fieldName3Vc
						
						/* Dohatec Add for resolve verify mega mega hash problem Start*/
											-- FETCH TENDER-HASH
						
						SELECT @v_tenderHash_Vc = ISNULL(@v_tenderHash_Vc + '_' + tenderHash, tenderHash) FROM dbo.tbl_FinalSubmission 
						WHERE tenderId = @v_fieldName2Vc AND bidSubStatus = 'finalsubmission'
					
						-- NOW GENERATE SHA1 OF GENERATED HASH
						SELECT @v_tenderHash_Vc = SUBSTRING(MASTER.dbo.fn_varbintohexstr(HASHBYTES('SHA1', @v_tenderHash_Vc)), 3, 41)
						delete from tbl_TenderMegaHash WHERE tenderId = @v_fieldName2Vc 
						-- NOW INSERT THIS GENERATED HASH INTO tbl_TenderMegaHash
						INSERT INTO dbo.tbl_TenderMegaHash (tenderId, megaHash, megaHashDt)
							SELECT @v_fieldName2Vc, @v_tenderHash_Vc, GETDATE()
					
						-- RESET THE @v_tenderHash_Vc VARIABLE TO NULL
						SET @v_tenderHash_Vc = NULL
						/* Dohatec Add for resolve verify mega mega hash problem Ends*/
					
						
						Select '1' as FieldValue1, 'Final Submission Completed Successfully.' as FieldValue2
				End
			End
				
			--END			
		END
			ELSE --- NOT EXISTS in tbl_FinalSubmission, insert in it
			BEGIN
			If @v_cntSameCmpUser>0
			Begin
				Select '0' as FieldValue1, 'Final Submission already done for this Company' as FieldValue2
			End
			Else
				Begin	
					If (@v_isTenSecPayable = 1) And (@v_userLotTenSecPayCnt < @v_userLotSelectedCnt)
					Begin
						Select '0' as FieldValue1, 'Tender Security Payment is Pending' as FieldValue2							
					End
				Else
				Begin
					INSERT INTO tbl_FinalSubmission(tenderId,finalSubmissionDt,userId,ipAddress,
					bidSubStatus, receiptNo,tenderHash)
					SELECT @v_fieldName2Vc, Convert(smalldatetime, GETDATE()),@v_fieldName3Vc,@v_fieldName4Vc,'finalsubmission',SUBSTRING(convert(varchar(300),NEWID()),1,8), SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('SHA1', @v_signTxtList_Vc)), 3, 41)			
					
					SET @v_finalSubId_int=Ident_Current('dbo.tbl_FinalSubmission') 
					
					--IF EXISTS (SELECT tenderformId FROM tbl_TenderBidForm WHERE tenderId = @v_fieldName2Vc  and userId = @v_fieldName3Vc and tenderformId not in (SELECT tenderformid FROM tbl_FinalSubDetail WHERE finalsubmissionid = @v_finalSubId_int) )
					--BEGIN --- IF ENTRY Not FOUND IN tbl_FinalSubDetail
						
						INSERT INTO tbl_FinalSubDetail(tenderFormid,ipAddress,finalSubmissionId, bidid)
						SELECT tenderformId,  @v_fieldName4Vc, @v_finalSubId_int, bidid FROM tbl_TenderBidForm WHERE tenderId = @v_fieldName2Vc  and userId = @v_fieldName3Vc and tenderformId not in (SELECT tenderformid FROM tbl_FinalSubDetail WHERE finalsubmissionid = @v_finalSubId_int) 
			
						UPDATE tbl_FinalSubmission 
						SET bidSubStatus = 'finalsubmission',
						tenderHash = SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('SHA1', @v_signTxtList_Vc)), 3, 41), receiptNo = SUBSTRING(convert(varchar(300),NEWID()),1,8), finalSubmissionDt = convert(smalldatetime,GETDATE()),	ipaddress = @v_fieldName4Vc			
						WHERE tenderId =@v_fieldName2Vc and userId = @v_fieldName3Vc
						Select '1' as FieldValue1, 'Final Submission Completed Successfully.' as FieldValue2
					--END	
					
						/* Dohatec Add for resolve verify mega mega hash problem Start*/
											-- FETCH TENDER-HASH
						
						SELECT @v_tenderHash_Vc = ISNULL(@v_tenderHash_Vc + '_' + tenderHash, tenderHash) FROM dbo.tbl_FinalSubmission 
						WHERE tenderId = @v_fieldName2Vc AND bidSubStatus = 'finalsubmission'
					
						-- NOW GENERATE SHA1 OF GENERATED HASH
						SELECT @v_tenderHash_Vc = SUBSTRING(MASTER.dbo.fn_varbintohexstr(HASHBYTES('SHA1', @v_tenderHash_Vc)), 3, 41)
						delete from tbl_TenderMegaHash WHERE tenderId = @v_fieldName2Vc 
						-- NOW INSERT THIS GENERATED HASH INTO tbl_TenderMegaHash
						INSERT INTO dbo.tbl_TenderMegaHash (tenderId, megaHash, megaHashDt)
							SELECT @v_fieldName2Vc, @v_tenderHash_Vc, GETDATE()
					
						-- RESET THE @v_tenderHash_Vc VARIABLE TO NULL
						SET @v_tenderHash_Vc = NULL
						/* Dohatec Add for resolve verify mega mega hash problem Ends*/
						
					
				End
			End
		
					
		END			
			END
		ELSE
		BEGIN
			SELECT '0' as FieldValue1,'Please Fill All Mandatory Forms' as FieldValue2	
		END    	
    	/** END ----------------------- FINAL SUBMISSION --------------------------********/
		END
			else
					begin
						SELECT '0' as FieldValue1,'Filled BOQ/Price Bid Form Not Encrypted Using Buyer''s Hash.' as FieldValue2
					end								
		END
		
		Else
		Begin
		SELECT  '0' as FieldValue1, 'Please fill Mandatory forms and Map Supporting / Reference Documents as requested' as FieldValue2
		End
END
ELSE
BEGIN
	print 'Not mandatory 1 : '	
	-- User has filled up forms
	SELECT @v_userfilCnt = count(distinct tbl_TenderForms.tenderFormId) 
		FROM  tbl_TenderSection INNER JOIN
                      tbl_TenderStd ON tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId AND 
                      tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId INNER JOIN
                      tbl_TenderForms ON tbl_TenderSection.tenderSectionId = tbl_TenderForms.tenderSectionId and isnull(formStatus,'a') !='c'
                      inner join tbl_TenderBidForm on tbl_TenderBidForm.tenderId=tbl_TenderStd.tenderId
                      and tbl_TenderBidForm.tenderFormId=tbl_TenderForms.tenderFormId						
		WHERE tbl_TenderBidForm.tenderId = @v_fieldName2Vc and tbl_TenderBidForm.userId=@v_fieldName3Vc		
		And (tbl_TenderForms.pkgLotId=0 Or tbl_TenderForms.pkgLotId in 
		(select distinct pkgLotId from tbl_bidderLots Where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc)
    )   
-- Filled Forms Count > 1
IF @v_userfilCnt >= 1
BEGIN

	--Count of encrypted filled forms by User
	SELECT @v_tenderEncfilCnt = count(distinct tbl_TenderForms.tenderFormId) 
		FROM  tbl_TenderSection INNER JOIN
                      tbl_TenderStd ON tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId AND 
                      tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId INNER JOIN
                      tbl_TenderForms ON tbl_TenderSection.tenderSectionId = tbl_TenderForms.tenderSectionId and isnull(formStatus,'a') !='c'
                      inner join tbl_TenderBidForm on tbl_TenderBidForm.tenderId=tbl_TenderStd.tenderId
                      and tbl_TenderBidForm.tenderFormId=tbl_TenderForms.tenderFormId						
		WHERE tbl_TenderBidForm.tenderId = @v_fieldName2Vc and tbl_TenderBidForm.userId=@v_fieldName3Vc and isEncryption='Yes'		
		And (tbl_TenderForms.pkgLotId=0 Or tbl_TenderForms.pkgLotId in 
		(select distinct pkgLotId from tbl_bidderLots Where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc)
    )   
		
		
     print   @v_tenderEncfilCnt  
	-- count of bidder encrypted forms	-- 
	SELECT @v_userEncfilCnt= count(distinct tbl_TenderForms.tenderFormId) 
		FROM  tbl_TenderSection INNER JOIN
                      tbl_TenderStd ON tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId AND 
                      tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId INNER JOIN
                      tbl_TenderForms ON tbl_TenderSection.tenderSectionId = tbl_TenderForms.tenderSectionId and isnull(formStatus,'a') !='c'
                      inner join tbl_TenderBidForm on tbl_TenderBidForm.tenderId=tbl_TenderStd.tenderId
                      and tbl_TenderBidForm.tenderFormId=tbl_TenderForms.tenderFormId
                      INNER JOIN tbl_FinalSubmission on tbl_FinalSubmission.tenderId=tbl_TenderBidForm.tenderId and tbl_FinalSubmission.userid=tbl_TenderBidForm.userid inner join tbl_FinalSubDetail 
                      on tbl_FinalSubDetail.finalSubmissionId=tbl_FinalSubmission.finalSubmissionId	
                       and tbl_FinalSubDetail.tenderFormid=tbl_TenderBidForm.tenderFormId
       WHERE tbl_TenderBidForm.tenderId = @v_fieldName2Vc and tbl_TenderBidForm.userId=@v_fieldName3Vc  and isEncryption='Yes'	
       And (tbl_TenderForms.pkgLotId=0 Or tbl_TenderForms.pkgLotId in 
		(select distinct pkgLotId from tbl_bidderLots Where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc)
    )   
                 
    IF @v_userEncfilCnt >= @v_tenderEncfilCnt
    	/** BEGIN ----------------------- FINAL SUBMISSION --------------------------********/
    	
    	/********************* Chk ENTRY Exists / Not In tbl_FinalSubmission ****************************/
	    IF @v_BiderFilledTenderManDocCnt_Int >= @v_BidTenderManDocCnt_Int    
	    BEGIN	    
	    
			IF EXISTS(SELECT finalSubmissionId FROM tbl_FinalSubmission WHERE tenderId = @v_fieldName2Vc  and userId = @v_fieldName3Vc)
			BEGIN --- Exists
		print 'tbl_FinalSubmission 2 : '
		
			If @v_cntSameCmpUser>0
			Begin
				Select '0' as FieldValue1, 'Final Submission already done for this Company' as FieldValue2
			End
			Else
			Begin
					If (@v_isTenSecPayable = 1) And (@v_userLotTenSecPayCnt < @v_userLotSelectedCnt)
					Begin
						Select '0' as FieldValue1, 'Tender Security Payment is Pending' as FieldValue2							
					End
					Else
					Begin
						Select @v_finalSubId_int = finalSubmissionId FROM tbl_FinalSubmission WHERE tenderId = @v_fieldName2Vc  and userId = @v_fieldName3Vc
						/********************* ENTRY Exists/Not IN tbl_FinalSubDetail ****************************/
						--IF EXISTS (SELECT tenderformId FROM tbl_TenderBidForm WHERE tenderId = @v_fieldName2Vc  and userId = @v_fieldName3Vc and tenderformId not in (SELECT tenderformid FROM tbl_FinalSubDetail WHERE finalsubmissionid = @v_finalSubId_int) )
						--BEGIN --IF ENTRY Not IN tbl_FinalSubDetail then Insert in it and Update tbl_FinalSubmission
							
							INSERT INTO tbl_FinalSubDetail(tenderFormid,ipAddress,finalSubmissionId, bidid)
							SELECT tenderformId,  @v_fieldName4Vc, @v_finalSubId_int, bidid FROM tbl_TenderBidForm WHERE tenderId = @v_fieldName2Vc  and userId = @v_fieldName3Vc and tenderformId not in (SELECT tenderformid FROM tbl_FinalSubDetail WHERE finalsubmissionid = @v_finalSubId_int) 
				
							
							UPDATE tbl_FinalSubmission 
							SET bidSubStatus = 'finalsubmission',
							tenderHash = SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('SHA1', @v_signTxtList_Vc)), 3, 41), receiptNo = SUBSTRING(convert(varchar(300),NEWID()),1,8), finalSubmissionDt = convert(smalldatetime,GETDATE()), ipaddress = @v_fieldName4Vc	
							WHERE tenderId =@v_fieldName2Vc and userId = @v_fieldName3Vc
							Select '1' as FieldValue1, 'Final Submission Completed Successfully.' as FieldValue2
							
						--END	
						/* Dohatec Add for resolve verify mega mega hash problem Start*/
								-- FETCH TENDER-HASH
						
						SELECT @v_tenderHash_Vc = ISNULL(@v_tenderHash_Vc + '_' + tenderHash, tenderHash) FROM dbo.tbl_FinalSubmission 
						WHERE tenderId = @v_fieldName2Vc AND bidSubStatus = 'finalsubmission'
					
						-- NOW GENERATE SHA1 OF GENERATED HASH
						SELECT @v_tenderHash_Vc = SUBSTRING(MASTER.dbo.fn_varbintohexstr(HASHBYTES('SHA1', @v_tenderHash_Vc)), 3, 41)
						delete from tbl_TenderMegaHash WHERE tenderId = @v_fieldName2Vc 
						-- NOW INSERT THIS GENERATED HASH INTO tbl_TenderMegaHash
						INSERT INTO dbo.tbl_TenderMegaHash (tenderId, megaHash, megaHashDt)
							SELECT @v_fieldName2Vc, @v_tenderHash_Vc, GETDATE()
					
						-- RESET THE @v_tenderHash_Vc VARIABLE TO NULL
						SET @v_tenderHash_Vc = NULL
						/* Dohatec Add for resolve verify mega mega hash problem Ends*/
						
				End	
			End
			
		
			
		END
			ELSE --- NOT EXISTS in tbl_FinalSubmission, insert in it
			BEGIN
		
		print 'NOT IN tbl_FinalSubmission 2 : '	
			If @v_cntSameCmpUser>0
			Begin
				Select '0' as FieldValue1, 'Final Submission already done for this Company' as FieldValue2
			End
			Else
			Begin
				If (@v_isTenSecPayable = 1) And (@v_userLotTenSecPayCnt < @v_userLotSelectedCnt)
				Begin
					Select '0' as FieldValue1, 'Tender Security Payment is Pending' as FieldValue2							
				End
				Else
				Begin
					INSERT INTO tbl_FinalSubmission(tenderId,finalSubmissionDt,userId,ipAddress,bidSubStatus, receiptNo,tenderHash)
					SELECT @v_fieldName2Vc, Convert(smalldatetime, GETDATE()),@v_fieldName3Vc,@v_fieldName4Vc,'finalsubmission',SUBSTRING(convert(varchar(300),NEWID()),1,8), SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('SHA1', @v_signTxtList_Vc)), 3, 41)			
						
					SET @v_finalSubId_int=Ident_Current('dbo.tbl_FinalSubmission') 
					
					--IF EXISTS (SELECT tenderformId FROM tbl_TenderBidForm WHERE tenderId = @v_fieldName2Vc  and userId = @v_fieldName3Vc and tenderformId not in (SELECT tenderformid FROM tbl_FinalSubDetail WHERE finalsubmissionid = @v_finalSubId_int) )
					--BEGIN --- IF ENTRY Not FOUND IN tbl_FinalSubDetail
						
						INSERT INTO tbl_FinalSubDetail(tenderFormid,ipAddress,finalSubmissionId, bidId)
						SELECT tenderformId,  @v_fieldName4Vc, @v_finalSubId_int, bidid FROM tbl_TenderBidForm WHERE tenderId = @v_fieldName2Vc  and userId = @v_fieldName3Vc and tenderformId not in (SELECT tenderformid FROM tbl_FinalSubDetail WHERE finalsubmissionid = @v_finalSubId_int) 
			
						UPDATE tbl_FinalSubmission 
						SET bidSubStatus = 'finalsubmission',
						tenderHash = SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('SHA1', @v_signTxtList_Vc)), 3, 41), receiptNo = SUBSTRING(convert(varchar(300),NEWID()),1,8), finalSubmissionDt = convert(smalldatetime,GETDATE()),ipaddress = 	@v_fieldName4Vc	
						WHERE tenderId =@v_fieldName2Vc and userId = @v_fieldName3Vc
						Select '1' as FieldValue1, 'Final Submission Completed Successfully.' as FieldValue2
					--END
					/* Dohatec Add for resolve verify mega mega hash problem Start*/
											-- FETCH TENDER-HASH
						
						SELECT @v_tenderHash_Vc = ISNULL(@v_tenderHash_Vc + '_' + tenderHash, tenderHash) FROM dbo.tbl_FinalSubmission 
						WHERE tenderId = @v_fieldName2Vc AND bidSubStatus = 'finalsubmission'
					
						-- NOW GENERATE SHA1 OF GENERATED HASH
						SELECT @v_tenderHash_Vc = SUBSTRING(MASTER.dbo.fn_varbintohexstr(HASHBYTES('SHA1', @v_tenderHash_Vc)), 3, 41)
						delete from tbl_TenderMegaHash WHERE tenderId = @v_fieldName2Vc 
						-- NOW INSERT THIS GENERATED HASH INTO tbl_TenderMegaHash
						INSERT INTO dbo.tbl_TenderMegaHash (tenderId, megaHash, megaHashDt)
							SELECT @v_fieldName2Vc, @v_tenderHash_Vc, GETDATE()
					
						-- RESET THE @v_tenderHash_Vc VARIABLE TO NULL
						SET @v_tenderHash_Vc = NULL
					/* Dohatec Add for resolve verify mega mega hash problem Ends*/
				End
			End
			
		END	
		
		END
		ELSE
		BEGIN
			SELECT '0' as FieldValue1,'Please Fill All Mandatory Documents' as FieldValue2
		END
    	
    	/** END ----------------------- FINAL SUBMISSION --------------------------********/
    ELSE
		SELECT '0' as FieldValue1,'Filled BOQ/Price Bid Form Not Encrypted Using Buyer''s Hash.' as FieldValue2
		
END

ELSE
	SELECT  '0' as FieldValue1, 'Please Fill Atleast One Form To Do Final Submission.You Can''t Do Final Submission In This Tender. ' as FieldValue2


END
	
	
END
ELSE
BEGIN
	Select '0' as FieldValue1, 'Your JVCA Partner Has Already Completed Final Submission' as FieldValue2
END			
END


ELSE
--- SUBMISSION TIME OVER
BEGIN
	Select '0' as FieldValue1, 'Final Submission Time is Over' as FieldValue2
END
END
ELSE
BEGIN
	Select '0' as FieldValue1, 'Tender has been cancelled.' as FieldValue2	
END	
	
COMMIT TRAN	
	END TRY 
	BEGIN CATCH
			BEGIN
				Select '0' as FieldValue1, 'Final Submission Not Completed Successfully' as FieldValue2
			ROLLBACK TRAN
			END
	END CATCH	 

--Update tbl_TenderDetails 
--set submissionDt = '2010-12-19 15:51:00' --2010-12-16 12:15:00
--where tenderid = 345

END


IF @v_fieldName1Vc = 'chkBidWithdrawal'     -- For BidPrep / check final bid status
BEGIN

SELECT bidSubStatus as FieldValue1 FROM tbl_FinalSubmission WHERE tenderId =@v_fieldName2Vc and userId = @v_fieldName3Vc
				

END

IF @v_fieldName1Vc = 'chkSubmissionDt'     -- For LotTendPrep /Final Submission Get last submission dt
BEGIN

SELECT case when submissiondt >= convert(smalldatetime, GETDATE()) then 'true' else 'false' end as FieldValue1 FROM tbl_TenderDetails WHERE tenderid = @v_fieldName2Vc 
				

END


IF @v_fieldName1Vc='SearchPendingProject'
BEGIN
	If (select userTyperId from tbl_LoginMaster where userId=@v_fieldName7Vc)=1
	Begin -- eGP Admin Case
			set @v_FinalQueryVc = 
		'SELECT convert(varchar(400),[projectId]) AS FieldValue1,[projectName] AS FieldValue2,
			[projectCode] AS FieldValue3,
			convert(varchar(400), Convert(decimal,(round([projectCost], 0)),0) ) AS FieldValue4,REPLACE(CONVERT(VARCHAR(11),projectStartDate, 106), '' '', ''-'') AS FieldValue5,REPLACE(CONVERT(VARCHAR(11),projectEndDate, 106), '' '', ''-'') AS FieldValue6 FROM tbl_ProjectMaster
			where projectstatus = ''pending''' +  
		case 
			when @v_fieldName4Vc IS not null And @v_fieldName6Vc='EQ'
			then ' and ' + @v_fieldName4Vc + ' = ''' + @v_fieldName5Vc + ''''
			when @v_fieldName4Vc IS not null And @v_fieldName6Vc='CN'
			then ' and ' + @v_fieldName4Vc + ' like''%' + @v_fieldName5Vc + '%'''
		else '' 
		end  	
	End
	Else
	Begin
	
			set @v_FinalQueryVc = 
		'SELECT convert(varchar(400),[projectId]) AS FieldValue1,[projectName] AS FieldValue2,
			[projectCode] AS FieldValue3,
			convert(varchar(400), Convert(decimal,(round([projectCost], 0)),0) ) AS FieldValue4,REPLACE(CONVERT(VARCHAR(11),projectStartDate, 106), '' '', ''-'') AS FieldValue5,REPLACE(CONVERT(VARCHAR(11),projectEndDate, 106), '' '', ''-'') AS FieldValue6 FROM tbl_ProjectMaster
			where 
			createdby = ' + @v_fieldName7Vc + '
			
			and 
			projectstatus = ''pending''' +  
		case 
			when @v_fieldName4Vc IS not null And @v_fieldName6Vc='EQ'
			then ' and ' + @v_fieldName4Vc + ' = ''' + @v_fieldName5Vc + ''''
			when @v_fieldName4Vc IS not null And @v_fieldName6Vc='CN'
			then ' and ' + @v_fieldName4Vc + ' like''%' + @v_fieldName5Vc + '%'''
		else '' 
		end  	
	
	End

exec(@v_FinalQueryVc)
END

IF @v_fieldName1Vc='SearchApproveProject'
BEGIN
	If (select userTyperId from tbl_LoginMaster where userId=@v_fieldName7Vc)=1
	Begin -- eGP Admin Case
	set @v_FinalQueryVc = 
		'SELECT convert(varchar(400),[projectId]) AS FieldValue1,[projectName] AS FieldValue2,
	[projectCode] AS FieldValue3,
	convert(varchar(400), Convert(decimal,(round([projectCost], 0)),0) ) AS FieldValue4,REPLACE(CONVERT(VARCHAR(11),projectStartDate, 106), '' '', ''-'') AS FieldValue5,REPLACE(CONVERT(VARCHAR(11),projectEndDate, 106), '' '', ''-'') AS FieldValue6 FROM tbl_ProjectMaster
	where projectstatus = ''approved''' +  
		case 
			when @v_fieldName4Vc IS not null And @v_fieldName6Vc='EQ'
			then ' and ' + @v_fieldName4Vc + ' = ''' + @v_fieldName5Vc + ''''
			when @v_fieldName4Vc IS not null And @v_fieldName6Vc='CN'
			then ' and ' + @v_fieldName4Vc + ' like''%' + @v_fieldName5Vc + '%'''
		else '' 
		end  
	End
	Else
	Begin
		set @v_FinalQueryVc = 
		'SELECT convert(varchar(400),[projectId]) AS FieldValue1,[projectName] AS FieldValue2,
	[projectCode] AS FieldValue3,
	convert(varchar(400), Convert(decimal,(round([projectCost], 0)),0) ) AS FieldValue4,REPLACE(CONVERT(VARCHAR(11),projectStartDate, 106), '' '', ''-'') AS FieldValue5,REPLACE(CONVERT(VARCHAR(11),projectEndDate, 106), '' '', ''-'') AS FieldValue6 FROM tbl_ProjectMaster
	where createdby=' + @v_fieldName7Vc + '
	
	and projectstatus = ''approved''' +  
		case 
			when @v_fieldName4Vc IS not null And @v_fieldName6Vc='EQ'
			then ' and ' + @v_fieldName4Vc + ' = ''' + @v_fieldName5Vc + ''''
			when @v_fieldName4Vc IS not null And @v_fieldName6Vc='CN'
			then ' and ' + @v_fieldName4Vc + ' like''%' + @v_fieldName5Vc + '%'''
		else '' 
		end  
	print @v_FinalQueryVc
	End	

--print @v_FinalQueryVc
exec(@v_FinalQueryVc)
	

END

IF @v_fieldName1Vc='LotPaymentListing'
	BEGIN
		select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue1,
		tp.paymentFor as FieldValue2,
		tp.paymentInstType as FieldValue3,
		replace(CONVERT(VARCHAR(11),tp.instValidUpto,106),' ','-') as FieldValue4,
		cast(tp.instRefNumber as varchar(50)) as FieldValue5,
		convert(varchar(20),tp.amount) as FieldValue6,
		replace(CONVERT(VARCHAR(12),tp.instDate,106),' ','-') as FieldValue7,
		tp.bankName as FieldValue8,
		tp.branchName as FieldValue9,
		(select fullName from tbl_partneradmin where userId=tp.createdBy
		union select  case CompanyName when '-' then firstName+' '+lastName else companyName end companynamee  from tbl_loginmaster li,tbl_TendererMaster ti,tbl_CompanyMaster ci,tbl_TenderPayment tpi
		where li.userId=ti.userId and ti.companyId=c.companyId and tpi.userId=li.userId and li.userId=tp.createdBy) as FieldValue10,
		tp.comments as FieldValue11,
		tp.eSignature as FieldValue12
		from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c,tbl_TenderPayment tp
		where l.userId=t.userId and t.companyId=c.companyId and tp.userId=l.userId
		and paymentFor='Tender Security' and tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
	END
IF @v_fieldName1Vc='SearchCommittee'
	BEGIN
	
	DECLARE @v_MQry varchar(Max), @v_condition varchar(8000)
	
	
	SET @v_condition =''		
	IF @v_fieldName4Vc !=''	
		SET @v_condition = @v_condition + ' AND t.tenderId = '+ @v_fieldName4Vc
	IF @v_fieldName5Vc !=''
		SET @v_condition = @v_condition + 'AND committeeName = '''+@v_fieldName5Vc+''''
	
	
	SELECT @v_MQry = 
	'select convert(varchar(150),committeeId)  as FieldValue1,convert(varchar(20),t.tenderId)  as FieldValue2,convert(varchar(150),committeeName)  as FieldValue3 from tbl_TenderDetails t,tbl_Committee c where t.tenderId=c.tenderId 
	and t.officeid in(select  eo.officeId from tbl_EmployeeOffices eo inner join tbl_EmployeeMaster em on eo.employeeId=em.employeeId where  em.userId= '+@v_fieldName6Vc+' )
	and committeStatus=''Approved''	and committeeType in('+@v_fieldName3Vc+') and procurementNatureId in(select procurementNatureId from tbl_TenderDetails where tenderId='+@v_fieldName2Vc+')	' + @v_condition	
	print @v_MQry
	exec (@v_MQry)
	
	END
	
	
IF @v_fieldName1Vc = 'DumpCommittee'  --
BEGIN

 
BEGIN TRY
Declare @v_committeeId int
			--Dumping data for evaluation committee with respect to committee ID
			if(select count(committeeId) from tbl_Committee where tenderId=@v_fieldName3Vc and committeeType in ('TEC','PEC'))=0
			begin
			insert into 
			 [tbl_Committee]
			(tenderid,[committeeName] ,[createdBy],[workflowStatus],[committeStatus],[minMembers]
				  ,[maxMembers] ,[minMemOutSide],[committeeType],[isCurrent],[remarks],[createdDate]
				  ,[publishDate] ,[minMemFromPe],[minMemFromTec])

			SELECT  
				  @v_fieldName3Vc,[committeeName],@v_fieldName4Vc,'Pending','Pending'
				  ,[minMembers],[maxMembers],[minMemOutSide],[committeeType],'dump'
				  ,'',GETDATE(),null,[minMemFromPe],[minMemFromTec]
			  FROM  [tbl_Committee] where committeeId=@v_fieldName2Vc
			  set @v_committeeId= Ident_Current('dbo.tbl_Committee') 
			  
			 insert into [tbl_CommitteeMembers]([committeeId]
				  ,[userId],[createdDate] ,[appStatus],[appDate],[remarks]
				  ,[eSignature],[digitalSignature],[memberRole],[memberFrom],govUserId) 
			SELECT  @v_committeeId,[userId],GETDATE(),'Pending'
				  ,'','','',''
				  ,[memberRole],[memberFrom],govUserId
			  FROM [tbl_CommitteeMembers] where [committeeId]=@v_fieldName2Vc
			 end
			
			--Start- Dumping data for opening committee with respect to TenderID  --Done by Rajesh
			if(select count(committeeId) from tbl_Committee where tenderId=@v_fieldName3Vc and committeeType in ('TOC','POC'))=0
			begin
			 insert into [tbl_Committee]
				(tenderid,[committeeName] ,[createdBy],[workflowStatus],[committeStatus],[minMembers]
				 ,[maxMembers] ,[minMemOutSide],[committeeType],[isCurrent],[remarks],[createdDate]
				 ,[publishDate] ,[minMemFromPe],[minMemFromTec])

			 SELECT  
				 @v_fieldName3Vc,[committeeName],@v_fieldName4Vc,'Pending','Pending'
				 ,[minMembers],[maxMembers],[minMemOutSide],[committeeType],'dump'
				 ,'',GETDATE(),null,[minMemFromPe],[minMemFromTec]
			 FROM  [tbl_Committee] where tenderId=(SELECT  tenderId
													FROM  [tbl_Committee] 
													where committeeId=@v_fieldName2Vc) and 
													committeeType in('TOC','POC')
													and committeStatus='approved'
			 
			 set @v_committeeId= Ident_Current('dbo.tbl_Committee') 
			  
			 insert into [tbl_CommitteeMembers]([committeeId]
				  ,[userId],[createdDate] ,[appStatus],[appDate],[remarks]
				  ,[eSignature],[digitalSignature],[memberRole],[memberFrom],govUserId) 
			 SELECT  @v_committeeId,[userId],GETDATE(),'Pending'
				  ,'','','','',[memberRole],[memberFrom],govUserId
			 FROM [tbl_CommitteeMembers] 
			 where [committeeId]=(select committeeId from tbl_committee where tenderId=(SELECT  tenderId
									FROM  [tbl_Committee] where committeeId=@v_fieldName2Vc) and 
									committeeType in('TOC','POC')
									and committeStatus='approved')
			--End- Dumping data with respect to TenderID  --Done by Rajesh			 
			end
			
			--Start- Dumping data for TSC committee with respect to TenderID  --Done by Rajesh
			if((@v_fieldName5Vc='3') and (select count(committeeId) from tbl_Committee where tenderId=@v_fieldName3Vc and committeeType in ('TSC'))=0)
			begin
			 insert into [tbl_Committee]
				(tenderid,[committeeName] ,[createdBy],[workflowStatus],[committeStatus],[minMembers]
				 ,[maxMembers] ,[minMemOutSide],[committeeType],[isCurrent],[remarks],[createdDate]
				 ,[publishDate] ,[minMemFromPe],[minMemFromTec])

			 SELECT  
				 @v_fieldName3Vc,[committeeName],@v_fieldName4Vc,'Pending','Pending'
				 ,[minMembers],[maxMembers],[minMemOutSide],[committeeType],'dump'
				 ,'',GETDATE(),null,[minMemFromPe],[minMemFromTec]
			 FROM  [tbl_Committee] where tenderId=(SELECT  tenderId
													FROM  [tbl_Committee] 
													where committeeId=@v_fieldName2Vc) and 
													committeeType in('TSC')
													and committeStatus='approved'
			 
			 set @v_committeeId= Ident_Current('dbo.tbl_Committee') 
			  
			 insert into [tbl_CommitteeMembers]([committeeId]
				  ,[userId],[createdDate] ,[appStatus],[appDate],[remarks]
				  ,[eSignature],[digitalSignature],[memberRole],[memberFrom],govUserId) 
			 SELECT  @v_committeeId,[userId],GETDATE(),'Pending'
				  ,'','','','',[memberRole],[memberFrom],govUserId
			 FROM [tbl_CommitteeMembers] 
			 where [committeeId]=(select committeeId from tbl_committee where tenderId=(SELECT  tenderId
									FROM  [tbl_Committee] where committeeId=@v_fieldName2Vc) and 
									committeeType in('TSC')
									and committeStatus='approved')
			--End- Dumping data with respect to TenderID  --Done by Rajesh			 
			end
			
			Select '1'   as FieldValue1, 'Committee created.' as FieldValue2, Ident_Current('dbo.tbl_Committee') as committeId 
 END TRY 
  BEGIN CATCH
   BEGIN
     Select '0'   as FieldValue1, 'Error while creating Project.' as FieldValue2
     ROLLBACK TRAN
	END 
	END CATCH
 END	 
  
 
	
	
IF @v_fieldName1Vc = 'TenderOpenExt'
BEGIN

	Declare @v_TeampQueryVc Varchar(max)
	Declare @v_PageN int=1
	Declare	@v_TotalPages int

	--[p_get_commonsearchdata]'TenderOpenExt','16','10'

	--@v_fieldName1Vc=Function Name
	--@v_fieldName2Vc=UserId
	--@v_fieldName3Vc=Total pagging 
	--@v_fieldName4Vc=

	set @v_TeampQueryVc=
	'DECLARE @v_Reccountf int
	DECLARE @v_TotalPagef int
	SELECT @v_Reccountf = Count(*) From (
	SELECT * From (SELECT ROW_NUMBER() OVER (order by e.extReqDt) As Rownumber,t.tenderId
	from tbl_TenderOpenExt e,tbl_TenderDetails t
	where userid='+cast(@v_fieldName2Vc as varchar(10))
	+' and e.extStatus='''+@v_fieldName5Vc+''' and e.tenderId=t.tenderId) AS DATA) AS TTT

	SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_fieldName3Vc as varchar(10))+')
	SELECT *,cast(@v_Reccountf as varchar(20)) as FieldValue6 From (SELECT cast(ROW_NUMBER() OVER (order by t.tenderId) as varchar(20)) As FieldValue8,cast(t.tenderId as varchar(20)) as FieldValue1,cast(t.reoiRfpRefNo as varchar(20)) as FieldValue2,t.tenderBrief as FieldValue3,t.peofficename as FieldValue4,CONVERT(VARCHAR(10),e.newOpenDt,103) as FieldValue5,cast(e.ExtDtId as varchar(20)) as FieldValue7
	from tbl_TenderOpenExt e,tbl_TenderDetails t where 
	e.extStatus='''+@v_fieldName5Vc+'''
	and userid='+cast(@v_fieldName2Vc as varchar(10))+' 
	and e.tenderId=t.tenderId) AS DATA where FieldValue8 between  ('+cast(@v_fieldName4Vc as varchar(10))+' - 1) * '+cast(@v_fieldName3Vc as varchar(10))+' + 1 AND '+cast(@v_fieldName4Vc as varchar(10))+' * '+cast(@v_fieldName3Vc as varchar(10))+''
									
	print @v_TeampQueryVc
	exec(@v_TeampQueryVc)
				

END	
	
	
IF @v_fieldName1Vc = 'FinalSubDtIP'
BEGIN

	Select  CONVERT(VARCHAR(10),finalsubmissiondt,103) +' ' +Substring(CONVERT(VARCHAR(30),finalsubmissiondt,108),1,5)   as FieldValue1, ipaddress  as FieldValue2 from tbl_FinalSubmission where tenderid = @v_fieldName2Vc and  userId =@v_fieldName3Vc
END	
	
	
IF @v_fieldName1Vc = 'GetTenderForms'
BEGIN

	Select  CONVERT(VARCHAR(10),tenderformId)   as FieldValue1, formname  as FieldValue2 from 
	tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std
	where tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
	and std.tenderStdId =st.tenderStdId and isPriceBid='No'
	  
END	

IF @v_fieldName1Vc = 'GetTenderFormsByUserId'   -- By: Krish - For Getting Formname by UserId - for "Evaluation" module
BEGIN

	SELECT CONVERT(VARCHAR(10),t.tenderformId)   as FieldValue1, formname  as FieldValue2 from 
	dbo.tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std, 
	dbo.tbl_TenderBidForm tbf
	WHERE std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
	and std.tenderStdId =st.tenderStdId and tbf.tenderFormId = t.tenderFormId and isPriceBid='No' 
	and tbf.userId = @v_fieldName3Vc
	  
END	

IF @v_fieldName1Vc = 'GetAppDetailByOfficeId'   -- By: Krish - For Getting App Detail
BEGIN

	;With AppData(FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6, FieldValue9)
	As
	(
		SELECT ROW_NUMBER() Over(ORDER BY ap.packageId DESC) as FieldValue1, CONVERT(VARCHAR(20), am.appId) AS FieldValue2, appCode AS FieldValue3, 
		ap.procurementnature + ', <br>' + am.projectName as FieldValue4, ap.packageNo + ', <br>' + ap.packageDesc as FieldValue5, 
		CONVERT(VARCHAR(20), ap.estimatedCost) + ', <br>' + pm.procurementMethod AS FieldValue6, CONVERT(VARCHAR(20), ap.packageId) as FieldValue9 FROM  
		dbo.tbl_AppMaster am INNER JOIN
		dbo.tbl_AppPackages ap ON am.appId = ap.appId INNER JOIN
		dbo.tbl_ProcurementMethod pm ON ap.procurementMethodId = pm.procurementMethodId 
		WHERE am.budgetType = @v_fieldName2Vc AND am.officeId = @v_fieldName3Vc and ap.appStatus = 'Approved'  
	), 
	CTE_Total As (Select COUNT(*) AS TotalRowCount from AppData) 
	SELECT CONVERT(VARCHAR(20), ad.FieldValue1) as FieldValue1, ad.FieldValue2, ad.FieldValue3, ad.FieldValue4, ad.FieldValue5, ad.FieldValue6, 
	CONVERT(VARCHAR(30), t.TotalRowCount) as FieldValue7, CONVERT(VARCHAR(30), CEILING(t.TotalRowCount / @v_fieldName4Vc)) as FieldValue8, ad.FieldValue9  
	FROM AppData ad CROSS JOIN CTE_Total t 
	WHERE  ad.FieldValue1 >= ((CONVERT(INT, @v_fieldName4Vc) * CONVERT(INT, @v_fieldName5Vc)) - (CONVERT(INT, @v_fieldName4Vc) - CONVERT(INT, @v_fieldName5Vc)) - (CONVERT(INT, @v_fieldName5Vc) - 1)) AND 
	ad.FieldValue1 <= CONVERT(INT, @v_fieldName4Vc) * CONVERT(INT, @v_fieldName5Vc) 
	ORDER BY ad.FieldValue1 ASC
	  
END	

IF @v_fieldName1Vc = 'GetAppDetailByPackageDtl'   -- By: Krish - For Getting App Detail
BEGIN

	;With AppData(FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6, FieldValue9)
	As
	(
		SELECT ROW_NUMBER() Over(ORDER BY ap.packageId DESC) as FieldValue1, CONVERT(VARCHAR(20), am.appId) AS FieldValue2, appCode AS FieldValue3, 
		ap.procurementnature + ', <br>' + am.projectName as FieldValue4, ap.packageNo + ', <br>' + ap.packageDesc as FieldValue5, 
		CONVERT(VARCHAR(20), ap.estimatedCost) + ', <br>' + pm.procurementMethod AS FieldValue6, CONVERT(VARCHAR(20), ap.packageId) as FieldValue9 FROM  
		dbo.tbl_AppMaster am INNER JOIN
		dbo.tbl_AppPackages ap ON am.appId = ap.appId INNER JOIN
		dbo.tbl_ProcurementMethod pm ON ap.procurementMethodId = pm.procurementMethodId 
		WHERE ap.packageDesc Like '%' + @v_fieldName2Vc + '%' and ap.appStatus='Approved'
	), 
	CTE_Total As (Select COUNT(*) AS TotalRowCount from AppData) 
	SELECT CONVERT(VARCHAR(20), ad.FieldValue1) as FieldValue1, ad.FieldValue2, ad.FieldValue3, ad.FieldValue4, ad.FieldValue5, ad.FieldValue6, 
	CONVERT(VARCHAR(30), t.TotalRowCount) as FieldValue7, CONVERT(VARCHAR(30), CEILING(t.TotalRowCount / @v_fieldName3Vc)) as FieldValue8, ad.FieldValue9  
	FROM AppData ad CROSS JOIN CTE_Total t 
	WHERE  ad.FieldValue1 >= ((CONVERT(INT, @v_fieldName3Vc) * CONVERT(INT, @v_fieldName4Vc)) - (CONVERT(INT, @v_fieldName3Vc) - CONVERT(INT, @v_fieldName4Vc)) - (CONVERT(INT, @v_fieldName4Vc) - 1)) AND 
	ad.FieldValue1 <= CONVERT(INT, @v_fieldName3Vc) * CONVERT(INT, @v_fieldName4Vc) 
	ORDER BY ad.FieldValue1 ASC
	  
END	

IF @v_fieldName1Vc = 'GetTenderFormsByLotId'    -- By: Krish - For Getting Formname by lotid or packageid - for "Evaluation" module
BEGIN
	If @v_fieldName7Vc = 'Evaluation'
	Begin	
		Select @v_ProcurementMethod_Vc = procurementMethod from tbl_TenderDetails  where tenderID= @v_fieldName2Vc
		--Do change By Nishith only method of dpm contains procebid forms
		If @v_fieldName9Vc = '1' And (@v_fieldName8Vc='Goods' Or @v_fieldName8Vc='Works') And (@v_ProcurementMethod_Vc='DPM')
		Begin
			-- // If Goods / Works and procurement method RFQ / RFQU / RFQL / DPM And Tender Envelope Count=1 

			IF @v_fieldName3Vc = '1' 
			BEGIN					
				Select distinct CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1, 
				IsNull((select lotNo  + ' - ' from tbl_TenderLotSecurity where appPkgLotId = t.pkgLotId),'') + formname  as FieldValue2, 
				CONVERT(VARCHAR(10),tbf.bidId) AS FieldValue3, 
				Case When @v_fieldName8Vc='Services'
					Then 
						Case 
							When Exists (select top 1 esFormDetailId from tbl_EvalSerFormDetail where tenderFormId=t.tenderFormId and userId=@v_fieldName5Vc and evalBy= @v_fieldName6Vc)
							Then 'Completed'
							Else 'Pending'
						End
					Else	
					isnull(( select top 1 isComplied from tbl_EvalMemStatus where formId = t.tenderFormId and userId=@v_fieldName5Vc and evalBy= @v_fieldName6Vc),'Pending')					
				End
				as FieldValue4
				from 
				tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std  ,tbl_TenderBidForm tbf
				where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
				and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId 				
				and std.packageLotId = @v_fieldName4Vc 
				and tbf.userId = @v_fieldName5Vc 
				And (t.formStatus is null Or t.formStatus in ('p', 'cp', 'a'))			
			END
			ELSE
			BEGIN
				Select distinct CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1,
				IsNull((select lotNo  + ' - ' from tbl_TenderLotSecurity where appPkgLotId = t.pkgLotId),'') + formname  as FieldValue2, 
				CONVERT(VARCHAR(10),tbf.bidId) AS FieldValue3, 			
				Case When @v_fieldName8Vc='Services'
					Then 
						Case 
							When Exists (select top 1 esFormDetailId from tbl_EvalSerFormDetail where tenderFormId=t.tenderFormId and userId=@v_fieldName5Vc and evalBy= @v_fieldName6Vc)
							Then 'Completed'
							Else 'Pending'
						End
					Else	
					isnull(( select  top 1  isComplied from tbl_EvalMemStatus where formId = t.tenderFormId and userId=@v_fieldName5Vc and evalBy= @v_fieldName6Vc),'Pending')					
				End
				as FieldValue4
				from 
				tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std ,tbl_TenderBidForm tbf
				where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
				and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId 				
				and tbf.userId = @v_fieldName5Vc 
				And (t.formStatus is null Or t.formStatus in ('p', 'cp', 'a'))
		END
		End
		
		Else
		Begin
			IF @v_fieldName3Vc = '1' 
			BEGIN					 
				Select distinct CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1, 
				formname as FieldValue2, 
				CONVERT(VARCHAR(10),tbf.bidId) AS FieldValue3, 
				Case When @v_fieldName8Vc='Services'
					Then 
						Case 
							When Exists (select top 1 esFormDetailId from tbl_EvalSerFormDetail where tenderFormId=t.tenderFormId and userId=@v_fieldName5Vc and evalBy= @v_fieldName6Vc)
							Then 'Completed'
							Else 'Pending'
						End
					Else	
					isnull(( select  top 1 isComplied from tbl_EvalMemStatus where formId = t.tenderFormId and userId=@v_fieldName5Vc and evalBy= @v_fieldName6Vc),'Pending')					
				End
				as FieldValue4			
				from 
				tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std  ,tbl_TenderBidForm tbf
				where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId								
				and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId and isPriceBid='No' 
				and std.packageLotId = @v_fieldName4Vc 
				and tbf.userId = @v_fieldName5Vc 
				And (t.formStatus is null Or t.formStatus in ('p', 'cp', 'a'))				
			END
			ELSE
			BEGIN
			
				Select distinct CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1, formname  as FieldValue2,
				CONVERT(VARCHAR(10),tbf.bidId) AS FieldValue3,  			
				Case When @v_fieldName8Vc='Services'
					Then 
						Case 
							When Exists (select top 1 esFormDetailId from tbl_EvalSerFormDetail where tenderFormId=t.tenderFormId and userId=@v_fieldName5Vc and evalBy= @v_fieldName6Vc)
							Then 'Completed'
							Else 'Pending'
						End
					Else	
					isnull(( select top 1  isComplied from tbl_EvalMemStatus where formId = t.tenderFormId and userId=@v_fieldName5Vc and evalBy= @v_fieldName6Vc),'Pending')					
				End
				as FieldValue4				
				from 
				tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std ,tbl_TenderBidForm tbf
				where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
				and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId and isPriceBid='No'
				and tbf.userId = @v_fieldName5Vc 
				And (t.formStatus is null Or t.formStatus in ('p', 'cp', 'a'))
			END
		End
		

	End
	
	Else
	Begin
		IF @v_fieldName3Vc = '1'
		BEGIN
			Select distinct CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1, formname  as FieldValue2, 
			CONVERT(VARCHAR(10),tbf.bidId) AS FieldValue3, 
			--isnull(( select isComplied from tbl_EvalMemStatus where formId = t.tenderFormId and userId=@v_fieldName5Vc and evalBy= @v_fieldName6Vc),'Pending')
			--as FieldValue4
			Case When @v_fieldName8Vc='Services'
				Then 
					Case 
						When Exists (select top 1 esFormDetailId from tbl_EvalSerFormDetail where tenderFormId=t.tenderFormId and userId=@v_fieldName5Vc and evalBy= @v_fieldName6Vc)
						Then 'Completed'
						Else 'Pending'
					End
				Else	
				isnull(( select  top 1  isComplied from tbl_EvalMemStatus where formId = t.tenderFormId and userId=@v_fieldName5Vc and evalBy= @v_fieldName6Vc),'Pending')					
			End
			as FieldValue4
			from 
			tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std  ,tbl_TenderBidForm tbf
			where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
			and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId and isPriceBid='No' 
			and std.packageLotId = @v_fieldName4Vc 
			and tbf.userId = @v_fieldName5Vc 
			And (t.formStatus is null Or t.formStatus in ('p', 'cp', 'a'))
			--AND t.tenderformId IN (SELECT formID FROM dbo.f_getformid(@v_fieldName2Vc, @v_fieldName6Vc))
		END
		ELSE
		BEGIN	
			Select distinct CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1, formname  as FieldValue2, 
			CONVERT(VARCHAR(10),tbf.bidId) AS FieldValue3,
			--isnull(( select isComplied from tbl_EvalMemStatus where formId = t.tenderFormId and userId=@v_fieldName5Vc and evalBy= @v_fieldName6Vc),'Pending')
			--as FieldValue4
			Case When @v_fieldName8Vc='Services'
				Then 
					Case 
						When Exists (select top 1 esFormDetailId from tbl_EvalSerFormDetail where tenderFormId=t.tenderFormId and userId=@v_fieldName5Vc and evalBy= @v_fieldName6Vc)
						Then 'Completed'
						Else 'Pending'
					End
				Else	
				isnull(( select top 1 isComplied from tbl_EvalMemStatus where formId = t.tenderFormId and userId=@v_fieldName5Vc and evalBy= @v_fieldName6Vc),'Pending')					
			End
			as FieldValue4
			from 
			tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std ,tbl_TenderBidForm tbf
			where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
			and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId and isPriceBid='No'
			and tbf.userId = @v_fieldName5Vc 
			And (t.formStatus is null Or t.formStatus in ('p', 'cp', 'a'))
			--AND t.tenderformId IN (SELECT formID FROM dbo.f_getformid(@v_fieldName2Vc, @v_fieldName6Vc))
			
		END
	End

		
	  
END	

IF @v_fieldName1Vc = 'GetNegTenderFormsByLotId'    -- By: Krish - For Getting Formname by lotid or packageid - for "Evaluation" module
BEGIN

	IF @v_fieldName3Vc = '1'
	BEGIN
		IF ((SELECT procurementNature FROM tbl_TenderDetails WHERE tenderId = @v_fieldName2Vc) = 'Services')
			BEGIN
				selecT CONVERT(VARCHAR(10),b.tenderFormId) as FieldValue1, CONVERT(VARCHAR(10),a.srvBoqId) as FieldValue3, b.formName as FieldValue2,c.srvBoqType as FieldValue4
				from tbl_CMS_TemplateSrvBoqDetail a, tbl_TenderForms b, tbl_CMS_SrvBoqMaster c
				where a.templateFormId = replace(b.templateFormId,'-','') and a.srvBoqId = c.srvBoqId and b.tenderFormId
				in (selecT tenderFormId from tbl_TenderBidForm where tenderId = @v_fieldName2Vc and userId = @v_fieldName5Vc)--order by c.srvBoqType asc
				order by b.isPriceBid 
			END
		ELSE
			BEGIN
				select  * from (Select  CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1, formname  as FieldValue2,CONVERT(VARCHAR(10),tbf.bidid) AS FieldValue3, CONVERT(VARCHAR(10),tbf.tenderid) AS FieldValue4  from 
				tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std  ,tbl_TenderBidForm tbf
				where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
				and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId
				 and isPriceBid='Yes' and std.packageLotId = @v_fieldName4Vc 
				and tbf.userId = @v_fieldName5Vc )a left outer join (
				select distinct CONVERT(VARCHAR(10),tbf.bidId) as FieldValue6  from tbl_NegotiatedBid b,tbl_TenderBidTable tb,tbl_TenderBidForm tbf
				where b.bidtableId=tb.bidTableId and tbf.bidId=tb.bidId and tenderid=@v_fieldName2Vc and userid=@v_fieldName5Vc and negid=@v_fieldName7Vc			
				and tbf.tenderFormId in (select formId from tbl_ReportForms where reportId in 
				(select reportId from tbl_EvalRoundMaster where tenderId=@v_fieldName2Vc))
						 )b
						on a.FieldValue3=b.FieldValue6
						 left outer join (
				select distinct CONVERT(VARCHAR(10),tbf.bidId) as FieldValue9  from tbl_NegotiatedBidderBid b,tbl_TenderBidTable tb,tbl_TenderBidForm tbf
				where b.bidtableId=tb.bidTableId and tbf.bidId=tb.bidId and tenderid=@v_fieldName2Vc and userid=@v_fieldName5Vc and negid=@v_fieldName7Vc			
				
						 )c
						on a.FieldValue3=c.FieldValue9
						
		END 
	END
	ELSE
	BEGIN
	IF ((SELECT procurementNature FROM tbl_TenderDetails WHERE tenderId = @v_fieldName2Vc) = 'Services')
			BEGIN
				selecT CONVERT(VARCHAR(10),b.tenderFormId) as FieldValue1, CONVERT(VARCHAR(10),a.srvBoqId) as FieldValue3, b.formName as FieldValue2,c.srvBoqType as FieldValue4
				from tbl_CMS_TemplateSrvBoqDetail a, tbl_TenderForms b, tbl_CMS_SrvBoqMaster c
				where a.templateFormId = replace(b.templateFormId,'-','') and a.srvBoqId = c.srvBoqId and b.tenderFormId
				in (selecT tenderFormId from tbl_TenderBidForm where tenderId = @v_fieldName2Vc and userId = @v_fieldName5Vc)
				--order by c.srvBoqType asc
				order by b.isPriceBid
			END
	ELSE
		BEGIN
			select * from (Select  CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1, formname  as FieldValue2,CONVERT(VARCHAR(10),tbf.bidId)  as FieldValue3,CONVERT(VARCHAR(10),tbf.tenderid)  as FieldValue4 from 
				tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std ,tbl_TenderBidForm tbf
				where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
				and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId
				 and isPriceBid='Yes'and tbf.userId = @v_fieldName5Vc)	a  left outer join (
				select distinct CONVERT(VARCHAR(10),tbf.bidId) as FieldValue6  from tbl_NegotiatedBid b,tbl_TenderBidTable tb,tbl_TenderBidForm tbf
				where b.bidtableId=tb.bidTableId and tbf.bidId=tb.bidId and tenderid=@v_fieldName2Vc and userid=@v_fieldName5Vc	and negid=@v_fieldName7Vc		
				and tbf.tenderFormId in (select formId from tbl_ReportForms where reportId in 
				(select reportId from tbl_EvalRoundMaster where tenderId=@v_fieldName2Vc))
						 )b
						on a.FieldValue3=b.FieldValue6 
						left outer join (
				select distinct CONVERT(VARCHAR(10),tbf.bidId) as FieldValue9  from tbl_NegotiatedBidderBid b,tbl_TenderBidTable tb,tbl_TenderBidForm tbf
				where b.bidtableId=tb.bidTableId and tbf.bidId=tb.bidId and tenderid=@v_fieldName2Vc and userid=@v_fieldName5Vc	and negid=@v_fieldName7Vc		
				
						 )c
						on a.FieldValue3=c.FieldValue9 
			 	
		END
	END	
	  
END	

	
IF @v_fieldName1Vc = 'ProcureNOA'
BEGIN	

Declare @v_ProcNatureId int, @v_ProcMethodId int, @v_ProctypeId int, @v_Estcost money, @v_tenderMode_Vc Varchar(50)
  
Select @v_ProcNatureId =a.procurementNatureId ,@v_ProcMethodId = a.procurementMethodId, @v_ProctypeId =
  --(Select procurementTypeId from tbl_ProcurementTypes Where procurementType = a.procurementType ),@v_Estcost = a.estCost, @v_tenderMode_Vc = docfeesmode from tbl_TenderDetails a where tenderId = @v_fieldName2Vc
   (Select procurementTypeId from tbl_ProcurementTypes Where procurementType = a.procurementType ),
    @v_Estcost = (select estCost from tbl_TenderEstCost where tenderId = @v_fieldName2Vc ),
    @v_tenderMode_Vc = docfeesmode from tbl_TenderDetails a where tenderId = @v_fieldName2Vc
 
SELECT convert(varchar(30),noaAcceptDays) as FieldValue1, convert(varchar(30),persecsubdays) as FieldValue2, 
convert(varchar(30),consigndays )as FieldValue3, @v_tenderMode_Vc as FieldValue4 
  FROM tbl_ConfigNoa where procurementnatureid = @v_ProcNatureId and procurementmethodid = @v_ProcMethodId and procurementtypeid = @v_ProctypeId and @v_Estcost between minvalue and maxvalue
  
	
END	

IF @v_fieldName1Vc = 'findRankerInfo'
BEGIN	
DECLARE @v_rankCnt INT, @v_counter INT, @v_winUserId int
set @v_counter = 1
Set @v_winUserId = 0
SELECT @v_rankCnt = COUNT(bidderRankId) FROM tbl_BidderRank WHERE tenderId = @v_fieldName2Vc and pkgLotId =@v_fieldName3Vc
WHILE (@v_counter <=@v_rankCnt)
BEGIN
--PRINT @v_counter

SELECT @v_winUserId = na.userid
FROM  tbl_NoaAcceptance na INNER JOIN tbl_NoaIssueDetails nid ON na.noaIssueId = nid.noaIssueId AND na.noaIssueId = nid.noaIssueId
WHERE tenderId = @v_fieldName2Vc
 and pkgLotId = @v_fieldName3Vc and na.acceptRejStatus = 'approved' and na.userId in (SELECT userId FROM tbl_BidderRank WHERE tenderId = @v_fieldName2Vc and pkgLotId = @v_fieldName3Vc and rank = @v_counter)

IF @v_winUserId <> 0
BREAK;
END
Select convert(varchar(20),companyId) as FieldValue1, companyName as FieldValue2 from tbl_CompanyMaster where userId = @v_winUserId

END

IF @v_fieldName1Vc = 'getBusinessDays'
BEGIN	
	Select convert(varchar(50),dbo.f_get_businessdays(@v_fieldName2Vc, @v_fieldName3Vc),103) as FieldValue1
END

------------Evaluation Functions --------------------


IF @v_fieldName1Vc = 'GetEvalComMemForBidderClari'
BEGIN	
	
	SELECT distinct '' as FieldValue1,
	dbo.f_GovUserName(postedByGovUserId, 'tbl_EmployeeTrasfer') as FieldValue3,
	convert(varchar(50),[quePostedBy]) as FieldValue2
	FROM  [tbl_EvalFormQues] e 
	where tenderid=@v_fieldName2Vc 
		and tenderFormId=@v_fieldName3Vc
		and 
		(
			quePostedBy = @v_fieldName4Vc
			Or 
			quePostedBy in (select distinct sentBy from tbl_EvalSentQueToCp where tenderId =@v_fieldName2Vc and sentFor='question')
		)
	ORDER BY FieldValue2 
	
ENd

IF @v_fieldName1Vc = 'GetEvalComMem'
BEGIN	
	
	SELECT distinct '' as FieldValue1,
	dbo.f_GovUserName(postedByGovUserId, 'tbl_EmployeeTrasfer') as FieldValue3,
	convert(varchar(50),[quePostedBy]) as FieldValue2
	FROM  [tbl_EvalFormQues] e 
	where tenderid=@v_fieldName2Vc 
		and tenderFormId=@v_fieldName3Vc		
	ORDER BY FieldValue2 
	
ENd
  
IF @v_fieldName1Vc = 'GetEvalComMemQuestion'
BEGIN
 SELECT convert(varchar(50), evalQueId) as FieldValue1, question AS FieldValue3, CONVERT(VARCHAR(50), quePostDt, 103) as FieldValue2, 
 answer AS FieldValue4, CONVERT(VARCHAR(50),[quePostedBy]) as FieldValue5
 FROM  [tbl_EvalFormQues] e ,tbl_EmployeeMaster em,tbl_EvalSentQueToCp es
 WHERE [quePostedBy] = es.sentBy and e.tenderId = es.tenderId and [quePostedBy]=em.userid and e.tenderid=@v_fieldName2Vc   and tenderFormId=@v_fieldName3Vc
 and [quePostedBy]=@v_fieldName4Vc and e.userId =@v_fieldName5Vc  ORDER BY FieldValue5 
End

IF @v_fieldName1Vc = 'GetEvalComMemQuestionByMem' --By nishith
BEGIN
	If @v_fieldName6Vc = 'officer'
	Begin		
		If @v_fieldName7Vc = 'all'
		Begin
			SELECT convert(varchar(50), evalQueId) as FieldValue1, question AS FieldValue3, CONVERT(VARCHAR(50), quePostDt, 103) as FieldValue2, 
		  Case 
			When Exists (select top 1 evalBidderRspId from tbl_EvalBidderResp where tenderId=@v_fieldName2Vc and userId=@v_fieldName5Vc  and isClarificationComp='Yes')
			Then answer Else '' End AS FieldValue4,		 
		 CONVERT(VARCHAR(50),[quePostedBy]) as FieldValue5
		 FROM  [tbl_EvalFormQues] e ,tbl_EmployeeMaster em
		 WHERE   [quePostedBy]=em.userid and e.tenderid=@v_fieldName2Vc  
		  and tenderFormId=@v_fieldName3Vc
		 and [quePostedBy]=@v_fieldName4Vc and e.userId =@v_fieldName5Vc 
		 --and queSentByTec=1
		 ORDER BY FieldValue5 	
		End
		Else
		Begin
			SELECT convert(varchar(50), evalQueId) as FieldValue1, question AS FieldValue3, CONVERT(VARCHAR(50), quePostDt, 103) as FieldValue2, 
		  Case 
			When Exists (select top 1 evalBidderRspId from tbl_EvalBidderResp where tenderId=@v_fieldName2Vc and userId=@v_fieldName5Vc  and isClarificationComp='Yes')
			Then answer Else '' End AS FieldValue4,		 
		 CONVERT(VARCHAR(50),[quePostedBy]) as FieldValue5
		 FROM  [tbl_EvalFormQues] e ,tbl_EmployeeMaster em
		 WHERE   [quePostedBy]=em.userid and e.tenderid=@v_fieldName2Vc  
		  and tenderFormId=@v_fieldName3Vc
		 and [quePostedBy]=@v_fieldName4Vc and e.userId =@v_fieldName5Vc 
		 and queSentByTec=1
		 ORDER BY FieldValue5 	
		End
		
		 
	End
	Else
	Begin		
		 SELECT convert(varchar(50), evalQueId) as FieldValue1, question AS FieldValue3, CONVERT(VARCHAR(50), quePostDt, 103) as FieldValue2, 
		 answer AS FieldValue4, CONVERT(VARCHAR(50),[quePostedBy]) as FieldValue5
		 FROM  [tbl_EvalFormQues] e ,tbl_EmployeeMaster em
		 WHERE   [quePostedBy]=em.userid and e.tenderid=@v_fieldName2Vc  
		  and tenderFormId=@v_fieldName3Vc
		 and [quePostedBy]=@v_fieldName4Vc and e.userId =@v_fieldName5Vc  
		 ORDER BY FieldValue5 		
	End

 --SELECT convert(varchar(50), evalQueId) as FieldValue1, question AS FieldValue3, CONVERT(VARCHAR(50), quePostDt, 103) as FieldValue2, 
 --answer AS FieldValue4, CONVERT(VARCHAR(50),[quePostedBy]) as FieldValue5
 --FROM  [tbl_EvalFormQues] e ,tbl_EmployeeMaster em
 --WHERE   [quePostedBy]=em.userid and e.tenderid=@v_fieldName2Vc  
 -- and tenderFormId=@v_fieldName3Vc
 --and [quePostedBy]=@v_fieldName4Vc and e.userId =@v_fieldName5Vc  ORDER BY FieldValue5 
End
--------------------------------Eval Function End----------------------

IF @v_fieldName1Vc = 'searchSubContractBidder'  
BEGIN

declare @bidQuery1 varchar(Max)

Set @bidQuery1 = 
'if((select COUNT(*) from tbl_FinalSubmission where userId in (select userId from tbl_LoginMaster where '+@v_fieldName3Vc+') and bidSubStatus   in(''finalsubmission'',''withdrawal'') and tenderId='+@v_fieldName2Vc+')=0)
	begin
Select ct.CompanyName as FieldValue1, ct.companyRegNumber FieldValue2, 
ct.EmailID FieldValue3,  ct.Country FieldValue4,  ct.State as FieldValue5,
  ct.RegistrationType FieldValue6 ,  legalStatus FieldValue7,
  convert(varchar(30),ct.userId )as FieldValue8, 
  ct.city as FieldValue9 from 
(
Select case when tm.companyid=1 then  tm.firstname+'' ''+IsNull(tm.lastname,'''')  else CompanyName end CompanyName, companyRegNumber,  EmailID,  Country,  State, 
 RegistrationType,  LegalStatus , lm.userId, 
 case when tm.companyid=1 then  tm.city  else tc.regOffCity end as city
from tbl_LoginMaster lm, tbl_TendererMaster tm , tbl_CompanyMaster tc
where lm.userId = tm.userId and lm.isjvca =''no'' and lm.userid !='+@v_fieldName4Vc+' and  lm.userid not in(select userid from tbl_tenderermaster where companyid in(select companyid from tbl_tenderermaster where userid='+@v_fieldName4Vc+'))  and lm.userid not in(select invTouserid from tbl_subcontracting where invFromuserid='+@v_fieldName4Vc+' and  tenderid='+@v_fieldName2Vc+' and pkgLotId='+@v_fieldName5Vc+') and
 status=''Approved''   and 
tc.companyid = tm.companyId '+case when @v_fieldName3Vc is not null then +' 
and ' + @v_fieldName3Vc else '' end +'
) as ct
end	  '
print @bidQuery1	
	exec(@bidQuery1)
END		



IF @v_fieldName1Vc = 'evalBidderStatus'  
BEGIN
	SELECT distinct  case cm.CompanyName when '-' then tm.firstName+' '+tm.lastName else companyName end as FieldValue1, ebs.bidderStatus  as FieldValue2
	FROM         tbl_CompanyMaster cm INNER JOIN
						  tbl_TendererMaster tm ON cm.companyId = tm.companyId AND 
						  cm.companyId = tm.companyId INNER JOIN
						  tbl_EvalBidderStatus ebs ON tm.userId = ebs.userId
	WHERE ebs.tenderId = @v_fieldName2Vc
END


IF @v_fieldName1Vc = 'evalComMember'  
BEGIN
	SELECT distinct em.employeeName as FieldValue1, convert(varchar(25),em.userId) as FieldValue2
	FROM   tbl_EmployeeMaster em INNER JOIN
                      tbl_EvalMemStatus ems ON em.userId = ems.evalBy
                      Where tenderid = @v_fieldName2Vc and ems.userId = @v_fieldName3Vc
END


IF @v_fieldName1Vc = 'evalComMemberForFinalEvaluation'  
BEGIN
	if((select configType from tbl_EvalConfig where tenderId=@v_fieldName2Vc)='ind')
begin
	SELECT distinct em.employeeName as FieldValue1, convert(varchar(25),em.userId) as FieldValue2
	FROM   tbl_EmployeeMaster em 
	INNER JOIN tbl_EvalMemStatus ems ON em.userId = ems.evalBy
    Where tenderid = @v_fieldName2Vc 
      and ems.userId = @v_fieldName3Vc
      and 
      ( 
		ems.evalBy = @v_fieldName4Vc
		Or 
		ems.evalBy in (select distinct sentBy from tbl_EvalSentQueToCp where tenderId=@v_fieldName2Vc and sentFor='evaluation')                      
      )
 end
 else
 begin
 SELECT distinct em.employeeName as FieldValue1, convert(varchar(25),em.userId) as FieldValue2
	FROM   tbl_EmployeeMaster em 
	INNER JOIN tbl_EvalMemStatus ems ON em.userId = ems.evalBy
    Where tenderid = @v_fieldName2Vc 
      and ems.userId = @v_fieldName3Vc
       and
		ems.evalBy =( select tecMemberId from tbl_EvalConfig where tenderId=@v_fieldName2Vc)
 end     
END

IF @v_fieldName1Vc = 'EvalMemberRole'  
BEGIN
select distinct convert(varchar(20),userid) as FieldValue1,memberRole as FieldValue2, committeeType as FieldValue3  from tbl_committee c,tbl_CommitteeMembers cm
 where tenderId=@v_fieldName2Vc and c.committeeId=cm.committeeId 
 and committeeType in('TEC','TSC','PEC') and userid=@v_fieldName3Vc and committeStatus = 'Approved'
END

IF @v_fieldName1Vc = 'EvalMemberStatus'  
BEGIN
--select CONVERT(varchar(20),evalBy) as FieldValue1, case isComplied when 'no' then 'Non Complied' else 'Complied' end as FieldValue2, evalNonCompRemarks as FieldValue3, convert(varchar(20),e.evalMemStatusId) as FieldValue4,  convert(varchar(20),t.tenderFormId) as FieldValue5  from tbl_EvalMemStatus e,tbl_tenderforms t,tbl_TenderBidForm tbd 
-- where e.tenderId=tbd.tenderid and tbd.tenderformid=t.tenderformid and tbd.userId=@v_fieldName3Vc and e.userId=@v_fieldName3Vc
-- and e.formid=tbd.tenderformid and e.formId = @v_fieldName4Vc
--   and e.tenderId=@v_fieldName2Vc and  e.evalBy=@v_fieldName5Vc  order by evalBy

select CONVERT(varchar(20),evalBy) as FieldValue1,
dbo.f_initcap(isComplied) as FieldValue2, 
evalNonCompRemarks as FieldValue3, convert(varchar(20),e.evalMemStatusId) as FieldValue4,  convert(varchar(20),t.tenderFormId) as FieldValue5  from tbl_EvalMemStatus e,tbl_tenderforms t,tbl_TenderBidForm tbd 
 where e.tenderId=tbd.tenderid and tbd.tenderformid=t.tenderformid and tbd.userId=@v_fieldName3Vc and e.userId=@v_fieldName3Vc
 and e.formid=tbd.tenderformid and e.formId = @v_fieldName4Vc
   and e.tenderId=@v_fieldName2Vc and  e.evalBy=@v_fieldName5Vc  order by evalBy

END


IF @v_fieldName1Vc = 'AAFillComboAPP'
BEGIN
	/****
	@v_fieldName1Vc: Method Name
	@v_fieldName2Vc: objectId
	@v_fieldName3Vc: Procurement Role
	@v_fieldName4Vc: workflow Role
	@v_fieldName5Vc: Activity Id
	@v_fieldName6Vc: Child Id

	****/
	DECLARE @v_roleNameVc varchar(100), @v_roleIdInt int, @v_appIdInt int, @v_projectIdInt int, @v_departmentid_Vc as varchar(200),@v_departmentidCounter_Vc as varchar(500),@v_tempCounter_Vc as varchar(100),@v_departmentidlist_Vc VARCHAR(MAX),@v_designationIdlist_Vc VARCHAR(MAX),@v_employeeIdlist_Vc VARCHAR(MAX),
	@v_FinalemployeeIdlist_Vc VARCHAR(MAX), @v_sBnkDevIdInt int


	IF @v_fieldName5Vc =  '1' or @v_fieldName5Vc = '7'
	BEGIN
--print 'IN cond'
Select @v_roleIdInt= procurementRoleId from tbl_ProcurementRole where procurementRole = @v_fieldName3Vc

IF @v_fieldName3Vc = 'PD'
	BEGIN
		
		--SELECT @v_appIdInt = appid FROM tbl_TenderMaster WHERE tenderId = @v_fieldName2Vc		
	SELECT @v_projectIdInt = projectId FROM tbl_AppMaster WHERE appId = @v_fieldName2Vc
	
	Select cast(lm.userid as varchar(10)) as FieldValue1, 
	(employeeName + ',[' + designationName + ' at ' +om.officeName+ ']') as FieldValue2 
	from tbl_EmployeeMaster EM 
	inner join tbl_DesignationMaster DM ON EM.designationId=DM.designationId
	INNER JOIN tbl_EmployeeOffices eo ON eo.employeeId = em.employeeId
	INNER JOIN tbl_OfficeMaster OM ON OM.officeId = eo.officeId
	inner join tbl_loginmaster lm on em.userid=lm.userid and lm.status='Approved'
	where lm.userId in
	(SELECT userid FROM tbl_ProjectRoles where ProjectId = @v_projectIdInt and procurementRoleId = @v_roleIdInt)

	END
	
	
IF @v_fieldName3Vc = 'PE'
	BEGIN		
	select cast(e.userid as varchar(10)) as FieldValue1, 
	(e.employeeName + ', [' + designationName + ' at ' +om.officeName+ ']' ) as FieldValue2 
	from	tbl_AppMaster a,tbl_employeemaster e,tbl_DesignationMaster d,
	tbl_loginmaster lm ,tbl_EmployeeOffices eo , 
	tbl_OfficeMaster OM ,
	tbl_EmployeeTrasfer et
	where  a.employeeid=e.employeeId 
		and d.designationId=e.designationId
		and eo.employeeId = e.employeeId
		and OM.officeId = eo.officeId
		and lm.userId=e.userId and lm.status='Approved'
		and a.appId = @v_fieldName2Vc	
		AND et.employeeid = e.employeeId
		 AND et.isCurrent = 'yes'	
	END
	
	

IF @v_fieldName3Vc = 'HOPE'
	BEGIN
		select cast(lm.userId as varchar(10)) as FieldValue1, 
		(employeeName + ', [' + designationName + ' at ' +om.officeName+ ']') as FieldValue2 
		from tbl_EmployeeMaster EM 
		inner join tbl_DesignationMaster DM ON EM.designationId=DM.designationId 
		INNER JOIN tbl_EmployeeOffices eo ON eo.employeeId = em.employeeId
		INNER JOIN tbl_OfficeMaster OM ON OM.officeId = eo.officeId
		inner join tbl_loginmaster lm on em.userid=lm.userid and lm.status='Approved'
		where EM.employeeId in(select tbl_EmployeeRoles.employeeId from tbl_EmployeeRoles where departmentId= 
			(select departmentId from tbl_AppMaster where appid=@v_fieldName2Vc)
			and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='HOPE'))
END

IF @v_fieldName3Vc = 'Authorized Officer' or @v_fieldName3Vc = 'AO'
	BEGIN
		select cast(lm.userId as varchar(10)) as FieldValue1, 
		(employeeName + ', [' + designationName + ' at ' +om.officeName+ ']') as FieldValue2 
		from tbl_EmployeeMaster EM
		inner join tbl_DesignationMaster DM ON EM.designationId=DM.designationId  
		INNER JOIN tbl_EmployeeOffices eo ON eo.employeeId = em.employeeId
		INNER JOIN tbl_OfficeMaster OM ON OM.officeId = eo.officeId
		inner join tbl_loginmaster lm on em.userid=lm.userid and lm.status='Approved'
		where EM.employeeId in(select tbl_EmployeeRoles.employeeId from tbl_EmployeeRoles where departmentId= 
			(select departmentId from tbl_AppMaster where appid=@v_fieldName2Vc)
			and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='AO'))
END


IF @v_fieldName3Vc = 'BOD'
	BEGIN
		select cast(lm.userId as varchar(10)) as FieldValue1, 
		(employeeName + ', [' + designationName + ' at ' +om.officeName+ ']') as FieldValue2 
		from tbl_EmployeeMaster EM
		inner join tbl_DesignationMaster DM ON EM.designationId=DM.designationId 
		INNER JOIN tbl_EmployeeOffices eo ON eo.employeeId = em.employeeId
		INNER JOIN tbl_OfficeMaster OM ON OM.officeId = eo.officeId
		inner join tbl_loginmaster lm on em.userid=lm.userid and lm.status='Approved'
		where EM.employeeId in(select employeeId from tbl_EmployeeRoles where departmentId= 
			(select departmentId from tbl_AppMaster where appid=@v_fieldName2Vc)
			and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='BOD'))
END

IF @v_fieldName3Vc = 'Secretary'
	BEGIN
		SELECT @v_departmentid_Vc=departmentId
		 FROM tbl_AppMaster where appid=@v_fieldName2Vc
		set @v_departmentidCounter_Vc=@v_departmentid_Vc
		set @v_tempCounter_Vc=''
		
		If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Organization'
			Begin
				SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)
				SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
			End		
		
		if @v_tempCounter_Vc=''
			BEGIN
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
					END
			END
		ELSE
			BEGIN
		
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_tempCounter_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)) as varchar(10))
					END
			END
		
		
		SELECT @v_employeeIdlist_Vc = COALESCE(@v_employeeIdlist_Vc+',' , '') + cast(employeeId as varchar(10))
		FROM tbl_EmployeeRoles WHERE departmentId in (select dbo.f_trim(items) from dbo.f_split(@v_departmentidCounter_Vc,',')) 
		and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='Secretary')
		
		select cast(lm.userId as varchar(10))  as FieldValue1, 
		(employeeName + ', [' + designationName + ' at ' +om.officeName+ ']') as FieldValue2 
		from tbl_EmployeeMaster EM 
		inner join tbl_DesignationMaster DM On EM.designationId=DM.designationId
		INNER JOIN tbl_EmployeeOffices eo ON eo.employeeId = em.employeeId
		INNER JOIN tbl_OfficeMaster OM ON OM.officeId = eo.officeId
		inner join tbl_loginmaster lm on em.userid=lm.userid and lm.status='Approved'
		where EM.employeeId in (select dbo.f_trim(items) from dbo.f_split(@v_employeeIdlist_Vc,','))
	END

IF @v_fieldName3Vc = 'Minister'
	BEGIN
		SELECT @v_departmentid_Vc=departmentId FROM tbl_AppMaster where appid=@v_fieldName2Vc
		set @v_departmentidCounter_Vc=@v_departmentid_Vc
		set @v_tempCounter_Vc=''
		
		If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Organization'
			Begin
				SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)
				SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
			End		
		
		if @v_tempCounter_Vc=''
			BEGIN
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
					END
			END
		ELSE
			BEGIN
		
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_tempCounter_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)) as varchar(10))
					END
			END
		
		
		SELECT @v_employeeIdlist_Vc = COALESCE(@v_employeeIdlist_Vc+',' , '') + cast(employeeId as varchar(10))
		FROM tbl_EmployeeRoles WHERE departmentId in (select dbo.f_trim(items) from dbo.f_split(@v_departmentidCounter_Vc,',')) 
		and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='Minister')
		
		select cast(lm.userId as varchar(10))  as FieldValue1, 
		(employeeName + ', [' + designationName + ' at ' +om.officeName+ ']') as FieldValue2 
		from tbl_EmployeeMaster EM
		inner join tbl_DesignationMaster DM ON EM.designationId=DM.designationId
		INNER JOIN tbl_EmployeeOffices eo ON eo.employeeId = em.employeeId
		INNER JOIN tbl_OfficeMaster OM ON OM.officeId = eo.officeId
		inner join tbl_loginmaster lm on em.userid=lm.userid and lm.status='Approved'
		where EM.employeeId in (select dbo.f_trim(items) from dbo.f_split(@v_employeeIdlist_Vc,','))
	END


IF @v_fieldName3Vc = 'CCGP'
	BEGIN	
		select cast(lm.userId as varchar(10)) as FieldValue1,
		(employeeName + ', [' + designationName + ' at ' +om.officeName+ ']') as FieldValue2 
		from tbl_EmployeeMaster EM
		inner join tbl_DesignationMaster DM ON EM.designationId=DM.designationId
		INNER JOIN tbl_EmployeeOffices eo ON eo.employeeId = em.employeeId
		INNER JOIN tbl_OfficeMaster OM ON OM.officeId = eo.officeId
		inner join tbl_loginmaster lm on em.userid=lm.userid and lm.status='Approved'		
		where EM.employeeId in (select employeeId from tbl_EmployeeRoles where procurementRoleId in (select procurementRoleId from tbl_ProcurementRole where procurementRole = 'CCGP'))
	END

IF @v_fieldName3Vc = 'Authorized User' or @v_fieldName3Vc = 'AU'
	BEGIN
	print 'in AU'
	 	 select cast(lm.userId as varchar(10)) as FieldValue1, 
	 	 (employeeName + ', [' + designationName + ' at ' +om.officeName+ ']') as FieldValue2 	 	 
		 from tbl_EmployeeMaster EM
		inner join tbl_DesignationMaster DM ON EM.designationId=DM.designationId
		INNER JOIN tbl_EmployeeOffices eo ON eo.employeeId = em.employeeId
		INNER JOIN tbl_OfficeMaster OM ON OM.officeId = eo.officeId
		inner join tbl_loginmaster lm on em.userid=lm.userid and lm.status='Approved'
		where EM.employeeId in(select e.employeeId from tbl_employeeoffices e,tbl_EmployeeRoles er
			 where e.employeeId=er.employeeId and  officeid= 
			(select officeId FROM tbl_AppMaster where appid=@v_fieldName2Vc)
			and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='Authorized User' or procurementRole='AU'))
	END
	

IF @v_fieldName3Vc = 'Development Partner' or @v_fieldName3Vc = 'DP'
	BEGIN
	print 'in DP'
		
	--SELECT @v_appIdInt = appid FROM tbl_TenderMaster WHERE tenderId = @v_fieldName2Vc		
	SELECT @v_projectIdInt = projectId FROM tbl_AppMaster WHERE appId = @v_fieldName2Vc

	Select convert(varchar(20),lm.userid) as FieldValue1, 
	fullName + '-'+ designation + ' at ' + sbDevelopName as FieldValue2  from
	 tbl_PartnerAdmin inner join
	 tbl_ScBankDevPartnerMaster on tbl_PartnerAdmin.sBankDevelopId = tbl_ScBankDevPartnerMaster.sBankDevelopId
	 inner join tbl_loginmaster lm on tbl_PartnerAdmin.userid=lm.userid and lm.status='Approved' and isAdmin='no'
	 where  tbl_PartnerAdmin.sBankDevelopId in 
	( select sBankDevelopId from tbl_ScBankDevPartnerMaster where sBankDevelHeadId in
		(Select sBankDevelopId from tbl_projectpartners where projectId =@v_projectIdInt) and partnerType = 'Development')
	
	END


END
ELSE
	BEGIN

Select @v_roleNameVc= procurementRole, @v_roleIdInt= procurementRoleId from tbl_ProcurementRole where procurementRoleId in (
Select approvingAuthEmpId from tbl_AppPackages where packageId in (
Select packageId from tbl_TenderMaster where tenderId = @v_fieldName2Vc ))
print 'ddf'	
IF @v_fieldName3Vc = 'PD'
	BEGIN
		
		SELECT @v_appIdInt = appid FROM tbl_TenderMaster WHERE tenderId = @v_fieldName2Vc		
		SELECT @v_projectIdInt = projectId FROM tbl_AppMaster WHERE appId = @v_appIdInt
	IF @v_roleNameVc ='PD'
	BEGIN
	Select cast(lm.userId as varchar(20)) as FieldValue1, 
	(employeeName + ', [' + designationName + ' at ' +om.officeName+ ']') as FieldValue2
	 from tbl_EmployeeMaster EM
		inner join tbl_DesignationMaster DM ON EM.designationId=DM.designationId
		inner join tbl_loginmaster lm on EM.userid=lm.userid and lm.status='Approved'
		INNER JOIN tbl_EmployeeOffices eo ON eo.employeeId = em.employeeId
		INNER JOIN tbl_OfficeMaster OM ON OM.officeId = eo.officeId					
	where lm.userId in	
	(SELECT userid FROM tbl_ProjectRoles where ProjectId = @v_projectIdInt and procurementRoleId = @v_roleIdInt)
	END
	ELSE
	--- TO Resolve ISSUE 4032 By Dohatec
	BEGIN
	Select cast(lm.userId as varchar(20)) as FieldValue1, 
	(employeeName + ', [' + designationName + ' at ' +om.officeName+ ']') as FieldValue2
	 from tbl_EmployeeMaster EM
		inner join tbl_DesignationMaster DM ON EM.designationId=DM.designationId
		inner join tbl_loginmaster lm on EM.userid=lm.userid and lm.status='Approved'
		INNER JOIN tbl_EmployeeOffices eo ON eo.employeeId = em.employeeId
		INNER JOIN tbl_OfficeMaster OM ON OM.officeId = eo.officeId					
	where lm.userId in	
	(SELECT userid FROM tbl_ProjectRoles 
	where ProjectId = @v_projectIdInt 
	and procurementRoleId = (SELECT procurementRoleId from tbl_ProcurementRole 
	WHERE procurementRole = 'PD'))	
	END
	END
	
	IF @v_fieldName3Vc = 'PE'
	BEGIN
		--select cast(employeeId as varchar(10)) as FieldValue1,employeeName as FieldValue2 from tbl_EmployeeMaster 
		--	where employeeId in(select employeeId from tbl_EmployeeRoles where departmentId= 
		--	(select departmentId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
		--	and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='PE'))
		select cast(e.userid as varchar(10)) as FieldValue1,
		(employeeName + ', [' + designationName + ' at ' +om.officeName+ ']') as FieldValue2 
		from tbl_tendermaster  t,tbl_AppMaster a,tbl_employeemaster e, 		
		tbl_EmployeeOffices  eo,
		tbl_OfficeMaster om
		,tbl_DesignationMaster dm,tbl_LoginMaster lm 
		where t.appId=a.appId 
		and e.designationId=dm.designationId
		AND e.employeeId = eo.employeeId		
		AND eo.officeId = om.officeId
		and tenderid=@v_fieldName2Vc and lm.userId=e.userid and lm.status='Approved'
		and a.employeeid=e.employeeId;		
	END
	
	IF @v_fieldName3Vc = 'HOPE'
	BEGIN
		select cast(em.userId as varchar(10)) as FieldValue1, 
		(employeeName + ', [' + designationName + ' at ' +om.officeName+ ']')  as FieldValue2 
		from tbl_EmployeeMaster EM 
		inner join tbl_DesignationMaster DM ON EM.designationId=DM.designationId
		INNER JOIN tbl_EmployeeOffices eo ON eo.employeeId = em.employeeId
		INNER JOIN tbl_OfficeMaster OM ON OM.officeId = eo.officeId
		inner join tbl_loginmaster lm on EM.userid=lm.userid and lm.status='Approved'
			where EM.employeeId in(select employeeId from tbl_EmployeeRoles where departmentId= 
			(select departmentId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
			and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='HOPE'))
	END
	
	IF @v_fieldName3Vc = 'Authorized Officer' or @v_fieldName3Vc = 'AO'
	BEGIN
		select cast(lm.userId as varchar(10)) as FieldValue1, 
		(employeeName + ', [' + designationName + ' at ' +om.officeName+ ']')  as FieldValue2 
		from tbl_EmployeeMaster EM 
		inner join tbl_DesignationMaster DM ON EM.designationId=DM.designationId
			INNER JOIN tbl_EmployeeOffices eo ON eo.employeeId = em.employeeId
		INNER JOIN tbl_OfficeMaster OM ON OM.officeId = eo.officeId
			inner join tbl_loginmaster lm on EM.userid=lm.userid and lm.status='Approved'
			where EM.employeeId in(select employeeId from tbl_EmployeeRoles where departmentId= 
			(select departmentId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
			and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='AO'))
	END
	
	IF @v_fieldName3Vc = 'BOD'
	BEGIN
		select cast(lm.userId as varchar(10)) as FieldValue1, 
		(employeeName + ', [' + designationName + ' at ' +om.officeName+ ']')  as FieldValue2 
		from tbl_EmployeeMaster EM 
		inner join tbl_DesignationMaster DM ON EM.designationId=DM.designationId
		INNER JOIN tbl_EmployeeOffices eo ON eo.employeeId = em.employeeId
		INNER JOIN tbl_OfficeMaster OM ON OM.officeId = eo.officeId
		inner join tbl_loginmaster lm on EM.userid=lm.userid and lm.status='Approved'
			where EM.employeeId in(select employeeId from tbl_EmployeeRoles where departmentId= 
			(select departmentId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
			and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='BOD'))
	END
	
	IF @v_fieldName3Vc = 'Secretary'
	BEGIN
		SELECT @v_departmentid_Vc=departmentId FROM tbl_TenderDetails WHERE tenderid=@v_fieldName2Vc
		set @v_departmentidCounter_Vc=@v_departmentid_Vc
		set @v_tempCounter_Vc=''
		
		If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Organization'
			Begin
				SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)
				SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
			End		
		
		if @v_tempCounter_Vc=''
			BEGIN
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
					END
			END
		ELSE
			BEGIN
		
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_tempCounter_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)) as varchar(10))
					END
			END
		
		
		SELECT @v_employeeIdlist_Vc = COALESCE(@v_employeeIdlist_Vc+',' , '') + cast(employeeId as varchar(10))
		FROM tbl_EmployeeRoles WHERE departmentId in (select dbo.f_trim(items) from dbo.f_split(@v_departmentidCounter_Vc,',')) 
		and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='Secretary')
		
		select cast(lm.userId as varchar(10))  as FieldValue1, 
		(employeeName + ', [' + designationName + ' at ' +om.officeName+ ']') as FieldValue2 
		from tbl_EmployeeMaster EM
		inner join tbl_DesignationMaster DM On EM.designationId=DM.designationId 
		INNER JOIN tbl_EmployeeOffices eo ON eo.employeeId = em.employeeId
		INNER JOIN tbl_OfficeMaster OM ON OM.officeId = eo.officeId
		inner join tbl_loginmaster lm on EM.userid=lm.userid and lm.status='Approved'
		where EM.employeeId in (select dbo.f_trim(items) from dbo.f_split(@v_employeeIdlist_Vc,','))
	END


IF @v_fieldName3Vc = 'Minister'
	BEGIN
		SELECT @v_departmentid_Vc=departmentId FROM tbl_TenderDetails WHERE tenderid=@v_fieldName2Vc
		set @v_departmentidCounter_Vc=@v_departmentid_Vc
		set @v_tempCounter_Vc=''
		
		If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Organization'
			Begin
				SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)
				SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
			End		
		
		if @v_tempCounter_Vc=''
			BEGIN
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
					END
			END
		ELSE
			BEGIN
		
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_tempCounter_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)) as varchar(10))
					END
			END
		
		
		SELECT @v_employeeIdlist_Vc = COALESCE(@v_employeeIdlist_Vc+',' , '') + cast(employeeId as varchar(10))
		FROM tbl_EmployeeRoles WHERE departmentId in (select dbo.f_trim(items) from dbo.f_split(@v_departmentidCounter_Vc,',')) 
		and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='Minister')
		
		select cast(lm.userId as varchar(10))  as FieldValue1, 
		(employeeName + ', [' + designationName + ' at ' +om.officeName+ ']') as FieldValue2 
		from tbl_EmployeeMaster EM
		inner join tbl_DesignationMaster DM ON EM.designationId=DM.designationId
		INNER JOIN tbl_EmployeeOffices eo ON eo.employeeId = em.employeeId
		INNER JOIN tbl_OfficeMaster OM ON OM.officeId = eo.officeId
		inner join tbl_loginmaster lm on EM.userid=lm.userid and lm.status='Approved'
		where EM.employeeId in (select dbo.f_trim(items) from dbo.f_split(@v_employeeIdlist_Vc,','))
	END


IF @v_fieldName3Vc = 'CCGP'
	BEGIN	
		select cast(lm.userId as varchar(10)) as FieldValue1,
		(employeeName + ', [' + designationName + ' at ' +om.officeName+ ']')  as FieldValue2 
		from tbl_EmployeeMaster EM
		inner join tbl_DesignationMaster DM ON EM.designationId=DM.designationId
		inner join tbl_loginmaster lm on EM.userid=lm.userid and lm.status='Approved'
		INNER JOIN tbl_EmployeeOffices eo ON eo.employeeId = em.employeeId
		INNER JOIN tbl_OfficeMaster OM ON OM.officeId = eo.officeId
		where EM.employeeId in (select employeeId from tbl_EmployeeRoles where procurementRoleId in (select procurementRoleId from tbl_ProcurementRole where procurementRole = 'CCGP'))
	END
	
	IF @v_fieldName3Vc = 'Authorized User' or @v_fieldName3Vc = 'AU'
	BEGIN
		select cast(lm.userId as varchar(10)) as FieldValue1, 
		(employeeName + ', [' + designationName + ' at ' +om.officeName+ ']')  as FieldValue2 
		from tbl_EmployeeMaster EM
		inner join tbl_DesignationMaster DM ON EM.designationId=DM.designationId
		INNER JOIN tbl_EmployeeOffices eo ON eo.employeeId = em.employeeId
		INNER JOIN tbl_OfficeMaster OM ON OM.officeId = eo.officeId
		inner join tbl_loginmaster lm on EM.userid=lm.userid and lm.status='Approved'
			where EM.employeeId in(select employeeId from tbl_EmployeeRoles where departmentId= 
			(select departmentId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
			and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='Authorized User' or procurementRole='AU'))
END

	IF @v_fieldName3Vc = 'Development Partner' or @v_fieldName3Vc = 'DP'
	BEGIN
	print 'in DP'
	SELECT @v_appIdInt = appid FROM tbl_TenderMaster WHERE tenderId = @v_fieldName2Vc
	print @v_appIdInt
	SELECT @v_projectIdInt = projectId FROM tbl_AppMaster WHERE appId = @v_appIdInt

	Select convert(varchar(20),lm.userid) as FieldValue1, 
	fullName + '-'+ designation + ' at ' + sbDevelopName  as FieldValue2  from
	 tbl_PartnerAdmin inner join tbl_ScBankDevPartnerMaster on tbl_PartnerAdmin.sBankDevelopId = tbl_ScBankDevPartnerMaster.sBankDevelopId
	 inner join tbl_loginmaster lm on tbl_PartnerAdmin.userid=lm.userid and lm.status='Approved' and isAdmin='no'
	 where  tbl_PartnerAdmin.sBankDevelopId in 
	( select sBankDevelopId from tbl_ScBankDevPartnerMaster where sBankDevelHeadId in
		(Select sBankDevelopId from tbl_projectpartners where projectId =@v_projectIdInt) and partnerType = 'Development')
	END
	
	IF @v_fieldName3Vc = 'TEC/PEC'
	BEGIN
		select 
		convert(varchar(20),em.userId ) as FieldValue1, 
		(employeeName + ', [' + designationName + ' at ' +om.officeName+ ']') as FieldValue2 
		from tbl_EmployeeMaster em, tbl_DesignationMaster dm ,				
		tbl_EmployeeOffices  eo,
		tbl_OfficeMaster om
		where em.designationId=dm.designationId  
		and userId = em.userid
		AND em.employeeId = eo.employeeId
		AND eo.officeId = om.officeId 
		and userId =(SELECT userId  FROM tbl_Committee tc,tbl_CommitteeMembers tcm where tenderId=@v_fieldName2Vc 
		and (committeeType='TEC' or committeeType='PEC') 
		and tc.committeeId=tcm.committeeId and memberRole='cp')
	END
END
END



IF @v_fieldName1Vc = 'SeekClarTECCP'
BEGIN

SELECT 		
	cpQuestion as FieldValue1, 
	memAnswer as FieldValue2, 
	CONVERT(VARCHAR(10),cpQuestionDt,103) +' ' +Substring(CONVERT(VARCHAR(30),cpQuestionDt,108),1,5) as FieldValue3,
	CONVERT(VARCHAR(10),memAnwserDt,103) +' ' +Substring(CONVERT(VARCHAR(30),memAnwserDt,108),1,5) as FieldValue4, 
	CONVERT(varchar(20),evalCpQueId)as FieldValue5, 
	CONVERT(varchar(20),noteOfDescent)as FieldValue6,CONVERT(varchar(500),cpcomments)as FieldValue8,
	dbo.f_GovUserName(tbl_EvalCpMemClarification.memGovUserId, 'tbl_EmployeeTrasfer') as FieldValue7
FROM tbl_EvalCpMemClarification 
where tenderFormId = @v_fieldName2Vc 
	and userId = @v_fieldName3Vc
	
END


IF @v_fieldName1Vc = 'getProcurementMethod'
BEGIN
	SELECT procurementMethod as FieldValue1 FROM tbl_TenderDetails where tenderId = @v_fieldName2Vc
END


IF @v_fieldName1Vc = 'getClarQuesPostedByCP'
BEGIN
	SELECT cpQuestion as FieldValue1, convert(varchar(10),evalCpQueId) as FieldValue2, memAnswer as FieldValue3  FROM tbl_EvalCpMemClarification where tenderformId = @v_fieldName2Vc and userid = @v_fieldName3Vc and memAnswerBy = @v_fieldName4Vc
END

IF @v_fieldName1Vc = 'GetQuesPostedByCP'
BEGIN
	IF @v_fieldName5Vc = 'cp'
	BEGIN
		SELECT cpQuestion as FieldValue1, convert(varchar(10),evalCpQueId) as FieldValue2, memAnswer as FieldValue3  FROM tbl_EvalCpMemClarification where tenderformId = @v_fieldName2Vc and userid = @v_fieldName3Vc and cpQuestionBy = @v_fieldName4Vc
	END
	ELSE
	BEGIN		
		SELECT cpQuestion as FieldValue1, convert(varchar(10),evalCpQueId) as FieldValue2, memAnswer as FieldValue3  FROM tbl_EvalCpMemClarification where tenderformId = @v_fieldName2Vc and userid = @v_fieldName3Vc and memAnswerBy = @v_fieldName4Vc
	END	
	
END


IF @v_fieldName1Vc = 'chkTECTSCComMember'
BEGIN

	Select convert(varchar(20),userId) as FieldValue1, committeeName as FieldValue2 from tbl_Committee tc, tbl_CommitteeMembers tcm where tc.committeeId = tcm.committeeId and tc.tenderId = @v_fieldName2Vc and tc. committeetype in ('TEC','PEC') and tc.committeStatus = 'approved' and userid = @v_fieldName3Vc
	union 
	Select userId, committeeName from tbl_Committee tc, tbl_CommitteeMembers tcm where tc.committeeId = tcm.committeeId and tc.tenderId = @v_fieldName2Vc and tc. committeetype = 'TSC' and tc.committeStatus = 'approved' and userid = @v_fieldName3Vc
	
	
END

IF @v_fieldName1Vc = 'chkTOSComMember'
BEGIN

	Select convert(varchar(20),userId) as FieldValue1, committeeName as FieldValue2 from tbl_Committee tc, tbl_CommitteeMembers tcm where tc.committeeId = tcm.committeeId and tc.tenderId = @v_fieldName2Vc and tc. committeetype  in ('TOC','POC') and tc.committeStatus = 'approved' and userid = @v_fieldName3Vc
	
	
END

IF @v_fieldName1Vc = 'getEvalMemberStatus'
BEGIN
SELECT isComplied as FieldValue1,CONVERT(varchar(20),evalMemStatusId) as FieldValue2,
 CONVERT(varchar(20),evalBy )as FieldValue3,evalNonCompRemarks as FieldValue4,actualMarks as FieldValue5
  FROM [tbl_EvalMemStatus] where  
   tenderid=@v_fieldName2Vc and userid=@v_fieldName3Vc and formid=@v_fieldName4Vc and evalBy = @v_fieldName5Vc
END
IF @v_fieldName1Vc = 'checkEvalServiceFrom'
BEGIN
 select convert(varchar(50),maxMarks) as FieldValue1 from tbl_EvalServiceForms where tenderformid=@v_fieldName2Vc
END


IF @v_fieldName1Vc = 'GetComMem'
BEGIN	
	
	SELECT distinct convert(varchar(50), em.employeeId) as FieldVlue1,em.employeeName as FieldValue2, 
	convert(varchar(50), em.userId) as FieldValue3
	FROM  tbl_EvalCpMemClarification e ,tbl_EmployeeMaster em
	where e.memAnswerBy=em.userid and tenderformId=@v_fieldName2Vc 
	
END

IF @v_fieldName1Vc = 'chkConfigStatus'
BEGIN	
	Select convert(varchar(50), tenderId) as FieldValue1, configType  as FieldValue2 ,evalCommittee  as FieldValue3,convert(varchar(20),configBy)  as FieldValue4,isPostQualReq  as FieldValue5,convert(varchar(20),tecMemberId)  as FieldValue6,convert(varchar(20),tscMemberId)  as FieldValue7, isTscReq  as FieldValue8 from tbl_EvalConfig where tenderId = @v_fieldName2Vc
END



IF @v_fieldName1Vc = 'getPostedQueForms'
BEGIN	
	Select  CONVERT(VARCHAR(10),tenderformId)   as FieldValue1, formname  as FieldValue2 from 
	tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std
	where tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
	and std.tenderStdId =st.tenderStdId and isPriceBid='No' and tenderformId in (Select tenderFormId from tbl_EvalFormQues where tenderId =  @v_fieldName2Vc)
END


IF @v_fieldName1Vc = 'GetTenderFormsforTCP'    -- By: Krish - For Getting Formname by lotid or packageid - for "Evaluation" module : EvalClariPostBidder
BEGIN

	IF @v_fieldName3Vc = '1'
	BEGIN
		Select  CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1, formname  as FieldValue2, 
		CONVERT(VARCHAR(10),tbf.bidId) AS FieldValue3 from 
		tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std  ,tbl_TenderBidForm tbf
		where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
		and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId and isPriceBid='No' and std.packageLotId = @v_fieldName4Vc 
		and tbf.userId = @v_fieldName5Vc AND 
		t.tenderformId IN (SELECT formID FROM dbo.f_getformid(@v_fieldName2Vc, @v_fieldName6Vc)) and t.tenderformId in (Select tenderFormId from tbl_EvalFormQues where tenderId = @v_fieldName2Vc and userId = @v_fieldName5Vc)
	END
	ELSE
	BEGIN
	
		Select  CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1, formname  as FieldValue2, 
		CONVERT(VARCHAR(10),tbf.bidId) AS FieldValue3 from 
		tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std ,tbl_TenderBidForm tbf
		where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
		and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId and isPriceBid='No'
		and tbf.userId = @v_fieldName5Vc AND 
		t.tenderformId IN (SELECT formID FROM dbo.f_getformid(@v_fieldName2Vc, @v_fieldName6Vc)) and t.tenderformId in (Select tenderFormId from tbl_EvalFormQues where tenderId = @v_fieldName2Vc and userId = @v_fieldName5Vc)
		
	END	
	  
END	

IF @v_fieldName1Vc = 'GetTenderFormsForSeekClari'    -- By: Krish - For Getting Formname by lotid or packageid - for "Evaluation" module : EvalClariPostBidder
BEGIN

	IF @v_fieldName3Vc = '1'
	BEGIN
		Select  CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1, formname  as FieldValue2, 
		CONVERT(VARCHAR(10),tbf.bidId) AS FieldValue3 from 
		tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std  ,tbl_TenderBidForm tbf
		where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
		and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId and isPriceBid='No' and std.packageLotId = @v_fieldName4Vc 
		and tbf.userId = @v_fieldName5Vc AND 
		t.tenderformId 
		in (Select distinct  tenderFormId from tbl_EvalFormQues 
				where tenderId = @v_fieldName2Vc and userId = @v_fieldName5Vc
				and 
				( 
					quePostedBy=@v_fieldName6Vc
					Or 
					quePostedBy in (select distinct sentBy from tbl_EvalSentQueToCp where tenderId =@v_fieldName2Vc and sentFor='question')
				)
			)
	END
	ELSE
	BEGIN
	
		Select  CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1, formname  as FieldValue2, 
		CONVERT(VARCHAR(10),tbf.bidId) AS FieldValue3 from 
		tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std ,tbl_TenderBidForm tbf
		where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
		and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId and isPriceBid='No'
		and tbf.userId = @v_fieldName5Vc AND 
		t.tenderformId 
		in (
				Select distinct tenderFormId from tbl_EvalFormQues 
				where tenderId = @v_fieldName2Vc and userId = @v_fieldName5Vc
				and 
				( 
					quePostedBy=@v_fieldName6Vc
					Or 
					quePostedBy in (select distinct sentBy from tbl_EvalSentQueToCp where tenderId =@v_fieldName2Vc and sentFor='question')
				)
			)
		
	END	
	  
END	

IF @v_fieldName1Vc = 'GetEvalFormByFormId'    -- By: Krish - For Getting Formname by lotid or packageid - for "Evaluation" module
BEGIN

	If @v_fieldName7Vc='showbids'
	Begin
		Select  CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1, formname  as FieldValue2, 
		CONVERT(VARCHAR(10),tbf.bidId) AS FieldValue3 from 
		tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std ,tbl_TenderBidForm tbf
		where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
		and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId 
		--and isPriceBid='No'
		and tbf.userId = @v_fieldName5Vc AND 
		t.tenderformId =@v_fieldName6Vc and (t.formStatus is null or t.formStatus in ('a','cp'))  --replaced p (which was by mistake mentioned) with a
	End
	Else
	Begin
		If (Select procurementNature from tbl_TenderDetails Where tenderId=@v_fieldName2Vc) = 'Services'
	Begin	
		Select  CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1, formname  as FieldValue2, 
		CONVERT(VARCHAR(10),tbf.bidId) AS FieldValue3 from 
		tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std ,tbl_TenderBidForm tbf
		where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
		and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId 
		--and isPriceBid='No'
		and tbf.userId = @v_fieldName5Vc AND 
		--t.tenderformId =@v_fieldName6Vc and (t.formStatus is null or t.formStatus in ('a','cp'))  --replaced p (which was by mistake mentioned) with a
		t.tenderformId =@v_fieldName6Vc and (t.formStatus is null or t.formStatus in ('a','cp','p')) -- modified by dohatec to resolve issue 2087
	End
	Else
	Begin
		Select distinct CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1, formname  as FieldValue2
		--,CONVERT(VARCHAR(10),tbf.bidId) AS FieldValue3 
		from 
		tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std ,
		tbl_TenderBidForm tbf
		where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
		and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId 
		and isPriceBid='No' -- uncommented by dohatec to resolve issue 2087
		and tbf.userId = @v_fieldName5Vc AND 
		--t.tenderformId =@v_fieldName6Vc and (t.formStatus is null or t.formStatus in ('a','cp'))  --replaced p (which was by mistake mentioned) with a
		t.tenderformId =@v_fieldName6Vc and (t.formStatus is null or t.formStatus in ('a','cp','p')) -- modified by dohatec to resolve issue 2087
	End	
	End
	
		
	 	
	  
END	


IF @v_fieldName1Vc = 'GetDebaredData'    -- By: Karan 
BEGIN

	/*
		@v_fieldName2Vc: for CompanyName/FirstName+ '' + LastName 
		@v_fieldName3Vc: for DISTRICT (actually state)
		@v_fieldName4Vc: for debarStartDt
		@v_fieldName5Vc: for debarEndDt
		@v_fieldName6Vc: for Page Int
		@v_fieldName7Vc: for Record Per Page Count		
	*/	
	
/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
Select @v_Page_inInt=@v_fieldName6Vc, @v_RecordPerPage_inInt=@v_fieldName7Vc
Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)
/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	
	--print(@v_StartAt_Int)
	--print(@v_StopAt_Int)
	
	
	SET @v_ConditionString_Vc=''
	
	/* START: alter THE WHERE CONDITION STRING */
	IF @v_fieldName2Vc is not null and @v_fieldName2Vc<>''
	BEGIN
			Select @v_ConditionString_Vc=@v_ConditionString_Vc +  ' And 
				(
					CM.companyName like ''%' + Convert(varchar(50), @v_fieldName2Vc) + '%''
					OR (TM.firstname+'' ''+IsNull(TM.lastname,''''))  like ''%' + Convert(varchar(50), @v_fieldName2Vc) + '%''
				)'	
	END
	IF @v_fieldName3Vc is not null and @v_fieldName3Vc<>''
	BEGIN
		Select @v_ConditionString_Vc=@v_ConditionString_Vc +  ' And State Like ''%' + @v_fieldName3Vc +  '%'''
	END
	
	
	IF (@v_fieldName4Vc is not null And @v_fieldName4Vc <>'') And (@v_fieldName5Vc is not null And @v_fieldName5Vc <>'')
	Begin
		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And debarStartDt >= ''' + @v_fieldName4Vc + ''' And debarEndDt <= ''' + @v_fieldName5Vc + ''''
	End
	ELSE IF (@v_fieldName4Vc is not null And @v_fieldName4Vc <>'') And (@v_fieldName5Vc is null OR @v_fieldName5Vc='')
	Begin
		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And debarStartDt >= ''' + @v_fieldName4Vc + ''''
	End
	ELSE IF (@v_fieldName4Vc is not null Or @v_fieldName4Vc <>'') And (@v_fieldName5Vc is not null And @v_fieldName5Vc <>'')
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And debarEndDt <= ''' + @v_fieldName5Vc + ''''
	End
	
	/* END: alter THE WHERE CONDITION STRING */
				
				
	-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
	Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT(debarListId) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')From tbl_DebarmentList DL
		Inner Join tbl_TendererMaster TM
		On DL.userId=TM.userId
		Inner Join tbl_CompanyMaster CM On TM.companyId=CM.companyId
		Inner Join tbl_EmployeeMaster EM
		On DL.debarredBy=EM.userId WHERE debarListId is Not Null ' + @v_ConditionString_Vc
		
	Select @v_CntQry_Vc='SELECT Count(debarListId) From tbl_DebarmentList DL
		Inner Join tbl_TendererMaster TM
		On DL.userId=TM.userId
		Inner Join tbl_CompanyMaster CM On TM.companyId=CM.companyId
		Inner Join tbl_EmployeeMaster EM
		On DL.debarredBy=EM.userId  WHERE debarListId is Not Null ' + @v_ConditionString_Vc
				
	
	/* START CODE: DYNAMIC QUERY TO GET RECORDS */
			
		Select @v_FinalQueryVc=
	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int; 
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
	Select 
		Convert(varchar(50),DL.debarListId) as FieldValue1,
			case 
				when TM.companyid=1 
				then TM.firstname+'' ''+IsNull(TM.lastname,'''') 
				else CM.companyName 
			End as FieldValue2,
			TM.state as FieldValue3,
			convert(varchar(15), debarStartDt, 103)+ '' '' + convert(varchar(5), debarStartDt, 108) 
			+ '' - ''
			+ convert(varchar(15), debarEndDt, 103)+ '' '' + convert(varchar(5), debarEndDt, 108) as FieldValue4, 
			EM.employeeName as FieldValue5,
			DL.remarks as FieldValue6,
			CONVERT(varchar(50),@v_TotalPageCnt_Int) as FieldValue7, 
			CONVERT(varchar(50),@v_TotalRecordCnt_Int) as FieldValue8,
			CONVERT(varchar(50),Tmp.RowNumber) as FieldValue9
		
		From
		(Select RowNumber, debarListId From
			(SELECT ROW_NUMBER() Over (Order by debarListId Desc) as RowNumber, debarListId
				From tbl_DebarmentList DL
					Inner Join tbl_TendererMaster TM On DL.userId=TM.userId
					Inner Join tbl_CompanyMaster CM On TM.companyId=CM.companyId
					Inner Join tbl_EmployeeMaster EM On DL.debarredBy=EM.userId
			WHERE debarListId is not null'   
					+@v_ConditionString_Vc+
			') B 				
		
		WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int) 		
		+ ' ) Tmp
		Inner Join tbl_DebarmentList DL On Tmp.debarListId=DL.debarListId
		Inner Join tbl_TendererMaster TM On DL.userId=TM.userId
		Inner Join tbl_CompanyMaster CM On TM.companyId=CM.companyId
		Inner Join tbl_EmployeeMaster EM On DL.debarredBy=EM.userId
		Order by Tmp.debarListId desc
		'		
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */
	
	-- // PRINT VALUES			
	--pRINT('PAge Count qUERY')
	--PRINT(@v_PgCntQry_Vc)
	--pRINT('Count Query')
	--PRINT(@v_CntQry_Vc)
	
	
	--pRINT('cONDITION sTRING')
	--pRINT (@v_ConditionString_Vc)
	PRINT(@v_FinalQueryVc)
	Exec (@v_FinalQueryVc)
END


END


IF @v_fieldName1Vc = 'getCommListing' -- for  /officer/CommListing.jsp
BEGIN

	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName4Vc, @v_RecordPerPage_inInt=@v_fieldName5Vc
	
	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)	
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	
	SET @v_ConditionString_Vc=''
	If @v_fieldName2Vc='TSC'
	Begin
		Select @v_ConditionString_Vc=' And c.committeeId=cm.committeeId 
					And c.committeStatus=''approved'' And committeeType in (''TSC'') And userId=' + @v_fieldName3Vc 
	End
	Else If @v_fieldName2Vc='TEC'
	Begin
		Select @v_ConditionString_Vc=' And c.committeeId=cm.committeeId 
					And c.committeStatus=''approved'' And committeeType in (''TEC'',''PEC'') And userId=' + @v_fieldName3Vc
	End
	Else If @v_fieldName2Vc='TOC'
	Begin
		Select @v_ConditionString_Vc=' And c.committeeId=cm.committeeId 
					And c.committeStatus=''approved'' And committeeType in (''TOC'',''POC'') And userId=' + @v_fieldName3Vc
	End
	
	If @v_fieldName6Vc is not null And @v_fieldName6Vc<>'' -- Department Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And t.departmentId=' + @v_fieldName6Vc
	End
	
	If @v_fieldName7Vc is not null And @v_fieldName7Vc<>'' -- Office Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And t.officeId=' + @v_fieldName7Vc
	End
	
	If @v_fieldName8Vc is not null And @v_fieldName8Vc<>'' -- Tender Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And t.tenderId=' + @v_fieldName8Vc
	End
	
	If @v_fieldName9Vc is not null And @v_fieldName9Vc<>'' -- reoiRfpRefNo / Ref No.
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And t.reoiRfpRefNo=''' + @v_fieldName9Vc + ''''
	End
	
	If @v_fieldName10Vc is not null And @v_fieldName10Vc<>'' -- Opening Date and Time
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And Cast (Floor(Cast (tod.openingDt as Float)) as Datetime) = ''' + @v_fieldName10Vc + ''''
		
		
	End
	
	
	
	-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
	Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT( Distinct cm.committeeId ) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')
	From tbl_CommitteeMembers cm 
		Inner Join	tbl_Committee c On cm.committeeId =c.committeeId
		Inner Join tbl_TenderDetails t On c.tenderid=t.tenderid
                --Inner Join tbl_TenderOpenDates tod On t.tenderId=tod.tenderId
		Inner Join (SELECT distinct tenderId,openingDt from tbl_TenderOpenDates ) tod On t.tenderId=tod.tenderId
                    AND convert(datetime,tod.openingDt,103) = CASE WHEN convert(datetime,tod.openingDt,103) = convert(datetime,t.openingDt,103) THEN convert(datetime,t.openingDt,103) ELSE (SELECT convert(datetime,MAX(finalOpenDt),103)  from tbl_TenderOpenExt where tenderId = tod.tenderId and extStatus =''Approved'') END
		WHERE cm.committeeId is Not Null ' + @v_ConditionString_Vc
		
	Select @v_CntQry_Vc='SELECT Count(Distinct cm.committeeId) 
	From tbl_CommitteeMembers cm 
		Inner Join	tbl_Committee c On cm.committeeId =c.committeeId
		Inner Join tbl_TenderDetails t On c.tenderid=t.tenderid
                --Inner Join tbl_TenderOpenDates tod On t.tenderId=tod.tenderId
		Inner Join (SELECT distinct tenderId,openingDt from tbl_TenderOpenDates ) tod On t.tenderId=tod.tenderId
                     AND convert(datetime,tod.openingDt,103) = CASE WHEN convert(datetime,tod.openingDt,103) = convert(datetime,t.openingDt,103) THEN convert(datetime,t.openingDt,103) ELSE (SELECT convert(datetime,MAX(finalOpenDt),103)  from tbl_TenderOpenExt where tenderId = tod.tenderId and extStatus =''Approved'') END
		WHERE cm.committeeId is Not Null ' + @v_ConditionString_Vc
		
		
			/* START CODE: DYNAMIC QUERY TO GET RECORDS */
			
		Select @v_FinalQueryVc=
	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int; 
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
	
	Select FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6, FieldValue7, FieldValue8, FieldValue9, Convert(varchar(50),RECORDID) as FieldValue10 From
	(
		Select FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6, FieldValue7, FieldValue8, FieldValue9,
		 ROW_NUMBER() Over (Order by openingDt desc) as RECORDID From
		(
			Select Distinct Convert(varchar(50), t.tenderId) as FieldValue1,
			Convert(varchar(50), c.committeeId) as FieldValue2 ,
			Convert(varchar(250), tenderBrief) as FieldValue3,
			isnull(REPLACE(CONVERT(VARCHAR(11),tod.openingDt, 106), '' '', ''-'') +'' '' +Substring(CONVERT(VARCHAR(5),tod.openingDt,108),1,5),''N.A.'') as FieldValue4,
			reoiRfpRefNo as FieldValue5,
			agency as FieldValue6,
			peOfficeName as FieldValue7,
			CONVERT(varchar(50),@v_TotalPageCnt_Int) as FieldValue8, 
			CONVERT(varchar(50),@v_TotalRecordCnt_Int) as FieldValue9,
			t.tenderid,tod.openingDt
			
			From
			(Select distinct RowNumber, Curr_CommitteeId From
				(SELECT distinct ROW_NUMBER() Over (Order by Curr_CommitteeId) as RowNumber, Curr_CommitteeId From
				(SELECT  Distinct cm.committeeId as Curr_CommitteeId
					From tbl_CommitteeMembers cm 
					Inner Join	tbl_Committee c On cm.committeeId =c.committeeId
					Inner Join tbl_TenderDetails t On c.tenderid=t.tenderid
                                        --Inner Join tbl_TenderOpenDates tod On t.tenderId=tod.tenderId
                                        Inner Join (SELECT distinct tenderId,openingDt from tbl_TenderOpenDates ) tod On t.tenderId=tod.tenderId
													AND convert(datetime,tod.openingDt,103) = CASE WHEN convert(datetime,tod.openingDt,103) = convert(datetime,t.openingDt,103) THEN convert(datetime,t.openingDt,103) ELSE (SELECT convert(datetime,MAX(finalOpenDt),103)  from tbl_TenderOpenExt where tenderId = tod.tenderId and extStatus =''Approved'') END
					WHERE cm.committeeId is Not Null '   
						+@v_ConditionString_Vc+
				') iBase) B 				
			) Tmp
			Inner Join tbl_CommitteeMembers cm On Tmp.Curr_CommitteeId=cm.committeeId
			Inner Join tbl_Committee c On cm.committeeId =c.committeeId
			Inner Join tbl_TenderDetails t On c.tenderid=t.tenderid
                        --Inner Join tbl_TenderOpenDates tod On t.tenderId=tod.tenderId
			Inner Join (SELECT distinct tenderId,openingDt from tbl_TenderOpenDates ) tod On t.tenderId=tod.tenderId
						AND convert(datetime,tod.openingDt,103) = CASE WHEN convert(datetime,tod.openingDt,103) = convert(datetime,t.openingDt,103) THEN convert(datetime,t.openingDt,103) ELSE (SELECT convert(datetime,MAX(finalOpenDt),103)  from tbl_TenderOpenExt where tenderId = tod.tenderId and extStatus =''Approved'') END
			) as A 
		)as FinalQuery WHERE RECORDID Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int) 		
				
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */
		
	
		PRINT(@v_FinalQueryVc)
		Exec (@v_FinalQueryVc)

END

IF @v_fieldName1Vc = 'FormsList'
BEGIN
	select distinct convert(varchar(25),a.tenderFormId )as FieldValue1 ,formName as FieldValue2,
	case isMandatory when 'yes' then 'Yes' else 'No' end as FieldValue3
	,cast((select esFormId from tbl_EvalServiceForms where 
	tenderFormId=a.tenderFormId and configBy=@v_fieldName3Vc) as varchar(20)) as FieldValue4
	,cast((select maxMarks from tbl_EvalServiceForms where 
	tenderFormId=a.tenderFormId and configBy=@v_fieldName3Vc) as varchar(20)) as FieldValue5
	from (select tenderid,packageLotId, t.tenderFormId,formName,isEncryption, isMandatory from 
	tbl_TenderForms t,tbl_TenderSection s,tbl_TenderStd std
	where t.tenderSectionId=s.tenderSectionId and s.tenderStdId=std.tenderStdId
	and tenderId=@v_fieldName2Vc and t.tenderformid in(select tenderformid from tbl_EvalServiceForms where 
	tenderId=@v_fieldName2Vc and configBy=@v_fieldName3Vc)
	and 
	case when @v_fieldName4Vc <> '0' then case when packageLotId = @v_fieldName4Vc then 1 else 0 end else 1 end =1
	)a 
END



IF @v_fieldName1Vc = 'GetCPQueTenderForms'
BEGIN

	Select  CONVERT(VARCHAR(10),tenderformId)   as FieldValue1, formname  as FieldValue2 from 
	tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std
	where tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
	and std.tenderStdId =st.tenderStdId and isPriceBid='No' and tenderformId in (	
	Select tenderformId from tbl_EvalCpMemClarification where memAnswerBy = @v_fieldName3Vc and userId = @v_fieldName4Vc)
	  
END	

IF @v_fieldName1Vc = 'GetCPTenderForm'
BEGIN

	IF @v_fieldName5Vc = 'cp'
	BEGIN
	
		if @v_fieldName6Vc <>0
		begin
		select a.*,
		'' as FieldValue5, 
		CONVERT(varchar(20),b.bidid) as FieldValue3,
		CONVERT(varchar(20),b.pkgLotId) as FieldValue4 from 
		(Select  CONVERT(VARCHAR(10),tenderformId) as FieldValue1, 
		formname  as FieldValue2
		
		from 
		tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std
		where tenderid = @v_fieldName2Vc and t.pkgLotId = @v_fieldName6Vc and t.tenderSectionId=st.tenderSectionId
		and std.tenderStdId =st.tenderStdId and isPriceBid='No' and 
		(t.formStatus is null or t.formStatus in ('p','cp')) and
		tenderformId in (	
		Select tenderformId from tbl_EvalCpMemClarification where cpQuestionBy = @v_fieldName3Vc and userId = @v_fieldName4Vc)
)a
	  left outer join (
	  
	  select distinct  bidId,tenderFormId,pkgLotId from tbl_TenderBidForm 
	  where tenderId=@v_fieldName2Vc and userid=@v_fieldName4Vc )b
	  on a.FieldValue1=b.tenderFormId
	  end
	  else
	  begin
	  select a.*,
		'' as FieldValue5, 
		CONVERT(varchar(20),b.bidid) as FieldValue3,
		CONVERT(varchar(20),b.pkgLotId) as FieldValue4 from 
		(Select  CONVERT(VARCHAR(10),tenderformId) as FieldValue1, 
		formname  as FieldValue2
		
		from 
		tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std
		where tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
		and std.tenderStdId =st.tenderStdId and isPriceBid='No' and 
		(t.formStatus is null or t.formStatus in ('p','cp')) and
		tenderformId in (	
		Select tenderformId from tbl_EvalCpMemClarification where cpQuestionBy = @v_fieldName3Vc and userId = @v_fieldName4Vc)
)a
	  left outer join (
	  
	  select distinct  bidId,tenderFormId,pkgLotId from tbl_TenderBidForm 
	  where tenderId=@v_fieldName2Vc and userid=@v_fieldName4Vc )b
	  on a.FieldValue1=b.tenderFormId
	  end
	END
	ELSE
	BEGIN
		select a.*,
		Case 
			When Exists   (Select top 1 torSignId from tbl_TORRptSign where tenderId=@v_fieldName2Vc And pkgLotId=b.pkgLotId And reportType='ter2')
			Then 'No'
			Else 'Yes'
		End as FieldValue5,
		CONVERT(varchar(20),b.bidid) as FieldValue3,
		CONVERT(varchar(20),b.pkgLotId) as FieldValue4 
		from (	Select  CONVERT(VARCHAR(10),tenderformId)   as FieldValue1, 
				formname as FieldValue2
				
				from 
		tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std
		where tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId 
		and (t.formStatus is null or t.formStatus in ('p','cp')) 
		and std.tenderStdId =st.tenderStdId and isPriceBid='No' and tenderformId in (	
		Select tenderformId from tbl_EvalCpMemClarification where
		 memAnswerBy = @v_fieldName3Vc and userId = @v_fieldName4Vc)
)a
	  left outer join (
	  
	  select distinct  bidId,tenderFormId,pkgLotId from tbl_TenderBidForm 
	  where tenderId=@v_fieldName2Vc and userid=@v_fieldName4Vc )b
	  on a.FieldValue1=b.tenderFormId
	END
	  
END	

--Exec p_get_commonsearchdata 'GetEmployeeDetails', '1','1','FieldValue4','asc'
IF @v_fieldName1Vc = 'GetEmployeeDetails'  --Get all rows from table tbl_FinancialYear
BEGIN
	set @v_Query1Vc = ''
	if @v_fieldName2Vc = 4
		begin
		set @v_Query1Vc = ' and  (employeeId in (select employeeId from tbl_EmployeeOffices where officeId in(select officeId from tbl_OfficeAdmin where userId='+ @v_fieldName3Vc+')) or createdBy='+ @v_fieldName3Vc+')'
		End
	else if @v_fieldName2Vc = 5
	begin
		set @v_Query1Vc = ' and  ( employeeId in (select employeeId from tbl_EmployeeOffices where officeId in(select officeId from tbl_OfficeMaster where departmentId in(select departmentId from tbl_DepartmentMaster where approvingAuthorityId='+@v_fieldName3Vc+'))) or  createdBy='+ @v_fieldName3Vc+' )'
	End
	
	set @v_FinalQueryVc = 
'select CONVERT(VARCHAR(10),a.employeeId) FieldValue1, FieldValue2, FieldValue3,REPLACE(CONVERT(VARCHAR(11),registereddate, 106), '' '', ''-'') FieldValue4, FieldValue5 ,convert(varchar(20),isnull(b.employeeId,0)) FieldValue6,isnull(departmentname,'''') as FieldValue7, case when FieldValue8 =''0'' then ''''  else convert(varchar(100),FieldValue8) end FieldValue8  from (select  e.employeeId  ,employeeName as FieldValue2,emailId as FieldValue3,registereddate   ,status as FieldValue5, convert(varchar(100),dbo.getEmpRoles(e.userid)) as FieldValue8  from tbl_EmployeeMaster e,tbl_LoginMaster l where
e.userId=l.userId ' + @v_Query1Vc +')a left outer join (select distinct employeeid,departmentname  from tbl_employeeroles er,tbl_departmentmaster dm where er.departmentId=dm.departmentId)b on a.employeeid=b.employeeid  order by '+@v_fieldName4Vc+' '+@v_fieldName5Vc+''
 

 print @v_FinalQueryVc
exec(@v_FinalQueryVc)
END 


IF @v_fieldName1Vc = 'GetNegBidderFormsByLotId'    -- By: Krish - For Getting Formname by lotid or packageid - for "Evaluation" module
BEGIN

	IF @v_fieldName3Vc = '1'
	BEGIN
	
		select  * from (Select  CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1, formname  as FieldValue2,CONVERT(VARCHAR(10),tbf.bidid) AS FieldValue3, CONVERT(VARCHAR(10),tbf.tenderid) AS FieldValue4  from 
		tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std  ,tbl_TenderBidForm tbf
		where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
		and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId
		 and isPriceBid='Yes' and std.packageLotId = @v_fieldName4Vc 
		and tbf.userId = @v_fieldName5Vc )a left outer join (
		select distinct CONVERT(VARCHAR(10),tbf.bidId) as FieldValue6  from tbl_NegotiatedBidderBid b,tbl_TenderBidTable tb,tbl_TenderBidForm tbf
	where b.bidtableId=tb.bidTableId and tbf.bidId=tb.bidId and tenderid=@v_fieldName2Vc and userid=@v_fieldName5Vc			
		
				 )b
				on a.FieldValue3=b.FieldValue6
	 
	END
	ELSE
	BEGIN
	select * from (Select  CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1, formname  as FieldValue2,CONVERT(VARCHAR(10),tbf.bidId)  as FieldValue3,CONVERT(VARCHAR(10),tbf.tenderid)  as FieldValue4 from 
		tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std ,tbl_TenderBidForm tbf
		where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
		and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId
		 and isPriceBid='Yes'and tbf.userId = @v_fieldName5Vc)	a  left outer join (
		select distinct CONVERT(VARCHAR(10),tbf.bidId) as FieldValue6  from tbl_NegotiatedBidderBid b,tbl_TenderBidTable tb,tbl_TenderBidForm tbf
	where b.bidtableId=tb.bidTableId and tbf.bidId=tb.bidId and tenderid=@v_fieldName2Vc and userid=@v_fieldName5Vc			
		
				 )b
				on a.FieldValue3=b.FieldValue6 
		 	
	END	
	  
END	


IF @v_fieldName1Vc = 'getselectedformDetails' 
BEGIN
select distinct convert(varchar(25),a.tenderFormId )as FieldValue1 ,formName as FieldValue2,
case (select count(esFormId) from tbl_EvalServiceForms where 
tenderFormId=a.tenderFormId and esFormId in (select esFormId from tbl_EvalServiceForms)) when 0 then 'no' else 'yes' end FieldValue3
from (select tenderid,packageLotId, t.tenderFormId,formName,isEncryption, isMandatory from tbl_TenderForms t,tbl_TenderSection s,tbl_TenderStd std
where t.tenderSectionId=s.tenderSectionId and s.tenderStdId=std.tenderStdId
and tenderId=@v_fieldName2Vc 
and 
case when @v_fieldName4Vc <> '0' then case when packageLotId = @v_fieldName4Vc then 1 else 0 end else 1 end =1

)a left outer join 
(select distinct tenderFormId from tbl_TenderBidForm where tenderId =@v_fieldName2Vc and userId=@v_fieldName3Vc )b
on a.tenderFormId=b.tenderFormId
left outer join 
(select distinct tenderFormId from tbl_FinalSubmission f,tbl_FinalSubDetail fd 
where f.finalSubmissionId=fd.finalSubmissionId and  tenderId =@v_fieldName2Vc  and userId=@v_fieldName3Vc)c
on a.tenderFormId=c.tenderFormId
left outer join 
(select distinct tenderFormId,signtext from tbl_TenderBidSign s,tbl_TenderBidForm b
 where tenderId =@v_fieldName2Vc and userId=@v_fieldName3Vc and b.bidId=s.bidId )d
on a.tenderFormId=d.tenderFormId

End


IF @v_fieldName1Vc = 'Eval_isTscReq_Info'
BEGIN

	Select Case 
		When Exists (Select tenderId from tbl_EvalConfig Where tenderId=@v_fieldName2Vc and isTscReq='yes') 
		Then 'yes'
		Else 'no'
		End as FieldValue1,
		Case @v_fieldName3Vc 
			When 'Packagewise' Then
				Case When Exists (select tenderId from tbl_EvalMapForms Where tenderId=@v_fieldName2Vc)
				Then 'mapped'
				Else 'pending'
				End 
			Else
				Case When ((Select count(distinct appPkgLotId) From tbl_TenderLotSecurity Where tenderId=@v_fieldName2Vc)=(Select count(distinct pkgLotId) From tbl_EvalMapForms Where tenderId=@v_fieldName2Vc))	
				Then 'mapped'
				Else 'pending'
				End 
		   End as FieldValue2,
		Case 
		When Exists (Select tenderId from tbl_EvalConfig Where tenderId=@v_fieldName2Vc) 
		Then 'yes'
		Else 'no'
		End as FieldValue3,
		Case 
		When Exists (Select tenderId from tbl_EvalConfig Where tenderId=@v_fieldName2Vc and isPostQualReq='yes') 
		Then 'yes'
		Else 'no'
		End as FieldValue4	  
END	

IF @v_fieldName1Vc = 'chkTSCCommitteeStatus'
BEGIN	
	Select Case 
			when Exists (select tenderId from tbl_EvalConfig where tenderId=@v_fieldName2Vc)
			Then 'yes'
			Else 'no'
			End as FieldValue1,
		Case 
			when Exists (select tenderId from tbl_EvalConfig where tenderId=@v_fieldName2Vc and isTscReq='yes')
			Then 'yes'
			Else 'no'
			End as FieldValue2,
		Case 
			when Exists (select tenderId from tbl_Committee where tenderId=@v_fieldName2Vc and committeeType='TSC' and committeStatus='approved')
			Then 'yes'
			Else 'no'
			End as FieldValue3				
END


IF @v_fieldName1Vc = 'getTECChairPersonUserId'
BEGIN	
	
	select convert(varchar(20),cm.userid) as FieldValue1 		
			from tbl_committee c 
			inner join tbl_CommitteeMembers cm on c.committeeId=cm.committeeId 
					where tenderId=@v_fieldName2Vc and committeeType in('TEC','PEC')  
					and memberRole='cp' and committeStatus='approved'
	
END


IF @v_fieldName1Vc = 'GetManageUsersStatisticsOne' -- By: Taher and Rishita - For Getting count for  Statistics
BEGIN
	declare @Field1 varchar(50),@Field2 varchar(50),@Field3 varchar(50),@Field4 varchar(50),@Field5 varchar(50),@Field6 varchar(50),@Field7 varchar(50),@Field8 varchar(50),@Field9 varchar(50),@Field10 varchar(50),@Field11 varchar(50),@Field12 varchar(50)
	
	select @Field1=convert(varchar(50),count(departmentId)) from tbl_DepartmentMaster where departmentType = 'Ministry' --count No of Ministries:
	
	select @Field2=convert(varchar(50),count(tdm.departmentId)) from tbl_DepartmentMaster tdm, Tbl_OfficeMaster tom where tdm.departmentId = tom.departmentId and tdm.departmentType like 'Ministry'--count No. of PE Offices:

	select @Field3=convert(varchar(50),count(tdm.departmentId)) from Tbl_LoginMaster tlm,Tbl_AdminMaster tam,Tbl_OfficeAdmin oa,Tbl_OfficeMaster om,Tbl_DepartmentMaster tdm 
	where tlm.userId = tam.userId and oa.officeId = om.officeId  and om.departmentId = tdm.departmentId and oa.userId = tlm.userId and tlm.userTyperId = 4 
	and tdm.departmentType = 'Ministry'  -- count No. of PE Admin Users:

	select @Field4=convert(varchar(50),count(employeeid)) from tbl_employeemaster e,tbl_LoginMaster l where employeeid in(select employeeId from tbl_employeeoffices where officeid in(select officeid from tbl_officemaster where departmentid in 
	(select departmentId from tbl_departmentmaster where departmentType='Ministry')))
	and l.userId=e.userId and status='Approved' -- count No. of Government Users:
	
	select @Field5=convert(varchar(50),count(departmentId)) from tbl_DepartmentMaster where departmentType = 'Division' -- count No. of Divisions:
	
	select @Field6=convert(varchar(50),count(tdm.departmentId)) from tbl_DepartmentMaster tdm, Tbl_OfficeMaster tom where tdm.departmentId = tom.departmentId and tdm.departmentType like 'Division' -- count No. of PE Offices:

	select @Field7=convert(varchar(50),count(tdm.departmentId)) from Tbl_LoginMaster tlm,Tbl_AdminMaster tam,Tbl_OfficeAdmin oa,Tbl_OfficeMaster om,Tbl_DepartmentMaster tdm 
	where tlm.userId = tam.userId and oa.officeId = om.officeId  and om.departmentId = tdm.departmentId and oa.userId = tlm.userId and tlm.userTyperId = 4 
	and tdm.departmentType = 'Division'  -- count No. of PE Admin Users:

	select @Field8=convert(varchar(50),count(employeeid)) from tbl_employeemaster e,tbl_LoginMaster l where employeeid in(select employeeId from tbl_employeeoffices where officeid in(select officeid from tbl_officemaster where departmentid in 
	(select departmentId from tbl_departmentmaster where departmentType='Division')))
	and l.userId=e.userId and status='Approved' -- count No. of Government Users:
	
	select @Field9=convert(varchar(50),count(departmentId)) from tbl_DepartmentMaster where departmentType = 'Organization' -- count No. of Organizations:
	
	select @Field10=convert(varchar(50),count(tdm.departmentId)) from tbl_DepartmentMaster tdm, Tbl_OfficeMaster tom where tdm.departmentId = tom.departmentId and tdm.departmentType like 'Organization' -- count No. of PE Offices:

	select @Field11=convert(varchar(50),count(tdm.departmentId)) from Tbl_LoginMaster tlm,Tbl_AdminMaster tam,Tbl_OfficeAdmin oa,Tbl_OfficeMaster om,Tbl_DepartmentMaster tdm 
	where tlm.userId = tam.userId and oa.officeId = om.officeId  and om.departmentId = tdm.departmentId and oa.userId = tlm.userId and tlm.userTyperId = 4 
	and tdm.departmentType = 'Organization' -- count No. of PE Admin Users:

	select @Field12=convert(varchar(50),count(employeeid)) from tbl_employeemaster e,tbl_LoginMaster l where employeeid in(select employeeId from tbl_employeeoffices where officeid in(select officeid from tbl_officemaster where departmentid in 
	(select departmentId from tbl_departmentmaster where departmentType='Organization')))
	and l.userId=e.userId and status='Approved' --count No. of Government Users:
	
	select @Field1 as FieldValue1,@Field2 as FieldValue2,@Field3 as FieldValue3,@Field4 as FieldValue4,@Field5 as FieldValue5,@Field6 as FieldValue6,@Field7 as FieldValue7,@Field8 as FieldValue8,@Field9 as FieldValue9,@Field10 as FieldValue10,@Field11 as FieldValue11,@Field12 as FieldValue12
END 


IF @v_fieldName1Vc = 'GetManageUsersStatisticsTwo' -- By: Rishita - For Getting count for  Statistics
BEGIN
	declare @FieldV1 varchar(50),@FieldV2 varchar(50),@FieldV3 varchar(50),@FieldV4 varchar(50),@FieldV5 varchar(50),@FieldV6 varchar(50),@FieldV7 varchar(50),@FieldV8 varchar(50)
	
	select @FieldV1=convert(varchar(50),count(tsbdpm.sBankDevelopId)) from tbl_ScBankDevPartnerMaster tsbdpm where tsbdpm.partnerType like 'Development' and tsbdpm.isBranchOffice like 'No' -- count No. of Development Partners
	
	select @FieldV2=convert(varchar(50),count(scdpMaster.sBankDevelopId)) from tbl_ScBankDevPartnerMaster scdpMaster where scdpMaster.partnerType LIKE 'Development' AND scdpMaster.isBranchOffice LIKE 'Yes' -- count No. of Development Partners’ Regional/Country Offices
	
	select @FieldV3=convert(varchar(50),count(partnerId)) from vw_getSbDevPartner where partnerType='Development' and isAdmin='Yes' -- count No. of Development Partner Admin Users:
	
	select @FieldV4=convert(varchar(50),count(partnerId)) from vw_getSbDevPartner where partnerType='Development' and isAdmin='No' -- count No. of Development Partner Users
	
	select @FieldV5=convert(varchar(50),count(sBankDevelopId)) from Tbl_ScBankDevPartnerMaster where partnerType = 'ScheduleBank' AND isBranchOffice LIKE 'No' -- count No. of Scheduled Banks:
	
	select @FieldV6=convert(varchar(50),count(sBankDevelopId)) from Tbl_ScBankDevPartnerMaster where partnerType = 'ScheduleBank' AND isBranchOffice LIKE 'Yes' -- count No. of Scheduled Bank Branches
	
	select @FieldV7=convert(varchar(50),count(partnerId)) from vw_getSbDevPartner where partnerType='ScheduleBank' and isAdmin='Yes' -- count No. of Scheduled Bank Admin Users:
	
	select @FieldV8=convert(varchar(50),count(partnerId)) from vw_getSbDevPartner where partnerType='ScheduleBank' and isAdmin='No' -- count No. of Scheduled Bank Users:
	
	select @FieldV1 as FieldValue1,@FieldV2 as FieldValue2,@FieldV3 as FieldValue3,@FieldV4 as FieldValue4,@FieldV5 as FieldValue5,@FieldV6 as FieldValue6,@FieldV7 as FieldValue7,@FieldV8 as FieldValue8
END 

--Exec p_get_commonsearchdata 'ValidityExtensionRequestListingPending','10','1','2562'
IF @v_fieldName1Vc = 'ValidityExtensionRequestListingPending'   ----Done by Rajesh for, officer/TenderExtReqListing.jsp
BEGIN
	
	--Declare @v_PagePerRecordN int
	set @v_PagePerRecordN =@v_fieldName2Vc
	
	set @v_PageN=@v_fieldName3Vc
	
	set @v_TeampQueryVc=
	'DECLARE @v_Reccountf int
	DECLARE @v_TotalPagef int
	SELECT @v_Reccountf = Count(*) From (
	SELECT * From (SELECT ROW_NUMBER() OVER (order by tbl_TenderDetails.tenderid) As Rownumber
	from tbl_TenderDetails inner join tbl_TenderValidityExtDate
	on tbl_TenderDetails.tenderId=tbl_TenderValidityExtDate.tenderId 
	where tbl_TenderValidityExtDate.extSentBy='+cast(@v_fieldName4Vc as varchar(10))+' and tbl_TenderValidityExtDate.extStatus=''Pending''
	) AS DATA) AS TTT
	
	SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
	SELECT *,cast(@v_TotalPagef as varchar(20)) as FieldValue7,cast(cast(@v_Reccountf as int) as varchar(20)) as FieldValue8 From (SELECT cast(ROW_NUMBER() OVER (order by tbl_TenderDetails.tenderid) as varchar(20)) As FieldValue9,cast(tbl_TenderDetails.tenderid as varchar(20)) As FieldValue1,CONVERT(varchar(10),reoiRfpRefNo) as FieldValue2,
	(select DepartmentName from tbl_DepartmentMaster where DepartmentId=tbl_TenderDetails.departmentId ) as FieldValue3, peOfficeName  as FieldValue4,
	tenderBrief as FieldValue5,CONVERT(varchar(10),tbl_TenderValidityExtDate.valExtDtId) as FieldValue6,cast(tbl_TenderValidityExtDate.extStatus as varchar(20)) as FieldValue10
	from tbl_TenderDetails inner join tbl_TenderValidityExtDate
	on tbl_TenderDetails.tenderId=tbl_TenderValidityExtDate.tenderId 
	where tbl_TenderValidityExtDate.extSentby='+cast(@v_fieldName4Vc as varchar(10))+' and tbl_TenderValidityExtDate.extStatus=''Pending''
	) AS DATA where FieldValue9 between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1 
	AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
	
	print @v_TeampQueryVc								
	exec(@v_TeampQueryVc)
	
END	

IF @v_fieldName1Vc = 'ValidityExtensionRequestListingApproved'   ----Done by Rajesh for, officer/TenderExtReqListing.jsp
BEGIN

	set @v_PagePerRecordN =@v_fieldName2Vc
	
	set @v_PageN=@v_fieldName3Vc
	
	set @v_TeampQueryVc=
	'DECLARE @v_Reccountf int
	DECLARE @v_TotalPagef int
	SELECT @v_Reccountf = Count(*) From (
	SELECT * From (SELECT ROW_NUMBER() OVER (order by tbl_TenderDetails.tenderid) As Rownumber
	from tbl_TenderDetails inner join tbl_TenderValidityExtDate
	on tbl_TenderDetails.tenderId=tbl_TenderValidityExtDate.tenderId 
	where tbl_TenderValidityExtDate.extSentby='+cast(@v_fieldName4Vc as varchar(10))+' and tbl_TenderValidityExtDate.extStatus=''Approved'' Or tbl_TenderValidityExtDate.extStatus=''Reject''
	) AS DATA) AS TTT

	SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
	SELECT *,cast(@v_TotalPagef as varchar(20)) as FieldValue7,cast(cast(@v_Reccountf as int) as varchar(20)) as FieldValue8 From (SELECT cast(ROW_NUMBER() OVER (order by tbl_TenderDetails.tenderid) as varchar(20)) As FieldValue9,cast(tbl_TenderDetails.tenderid as varchar(20)) As FieldValue1,CONVERT(varchar(10),reoiRfpRefNo) as FieldValue2,
	(select DepartmentName from tbl_DepartmentMaster where DepartmentId=tbl_TenderDetails.departmentId ) as FieldValue3, peOfficeName  as FieldValue4,
	tenderBrief as FieldValue5,CONVERT(varchar(10),tbl_TenderValidityExtDate.valExtDtId) as FieldValue6,cast(tbl_TenderValidityExtDate.extStatus  as varchar(20))as FieldValue10
	from tbl_TenderDetails inner join tbl_TenderValidityExtDate
	on tbl_TenderDetails.tenderId=tbl_TenderValidityExtDate.tenderId 
	where tbl_TenderValidityExtDate.extSentby='+cast(@v_fieldName4Vc as varchar(10))+' and (tbl_TenderValidityExtDate.extStatus=''Approved'' Or tbl_TenderValidityExtDate.extStatus=''Reject'')
	) AS DATA where FieldValue9 between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1 
	AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
	
	print @v_TeampQueryVc	
						
	exec(@v_TeampQueryVc)
	


END

--Exec p_get_commonsearchdata 'getFormsForEvlauationMapping','1421','435'
IF @v_fieldName1Vc = 'getFormsForEvlauationMapping'     -- For officer/MapEvalForms.jsp
BEGIN
		
	IF @v_fieldName3Vc <> '0'
	BEGIN	
		
		SELECT 
		CONVERT(varchar(50), tf.tenderFormId) as FieldValue1,
		formName as FieldValue2
		FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 		
		Where tf.isPriceBid='no' And td.tenderId = @v_fieldName2Vc and packageLotId = @v_fieldName3Vc
		Order By isPriceBid		
	END
	ELSE
	BEGIN
		SELECT 
		CONVERT(varchar(50), tf.tenderFormId) as FieldValue1,
		formName as FieldValue2
		FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 		
		Where tf.isPriceBid='no' And td.tenderId = @v_fieldName2Vc
		Order By isPriceBid		
		
	END
		
END


IF @v_fieldName1Vc = 'getSearchRegUserListing' -- for  /officer/CommListing.jsp
BEGIN

	
	
	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName2Vc, @v_RecordPerPage_inInt=@v_fieldName3Vc
	
	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)	
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	
	
	
	SET @v_ConditionString_Vc=' And isLive=''yes'''
	Select @v_BankUserRole_Vc=isMakerChecker,
		   @v_BankBranchId = sBankDevelopId		
	From tbl_PartnerAdmin Where userId=@v_fieldName4Vc
	
	Select @v_BankId = Case sBankDevelHeadId 
	When 0 Then @v_BankBranchId Else sBankDevelHeadId End 
	From tbl_ScBankDevPartnerMaster Where sBankDevelopId=@v_BankBranchId
	
	
	
	If @v_BankUserRole_Vc='BranchChecker'
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And regPaymentId in (Select regPaymentId From tbl_RegFeePayment Where createdBy in (select distinct userId from tbl_PartnerAdmin where sBankDevelopId= '+Convert(varchar(50), @v_BankBranchId)+'))'
	End
	Else If @v_BankUserRole_Vc='BankChecker'
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And regPaymentId in (Select regPaymentId From tbl_RegFeePayment Where createdBy in (select distinct userId from tbl_PartnerAdmin 
		where (
				sBankDevelopId= '+Convert(varchar(50), @v_BankBranchId)+
				' OR
				sBankDevelopId in (select distinct sBankDevelopId from tbl_ScBankDevPartnerMaster Where sBankDevelHeadId='+Convert(varchar(50), @v_BankId)+')
			)
		))'
	End
	
	If @v_fieldName5Vc<>''
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And emailId=''' + @v_fieldName5Vc +''''
	End	
	
	If @v_fieldName6Vc<>'' -- Verified status
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And isVerified=''' + @v_fieldName6Vc +''''
	End	
	
	If @v_fieldName7Vc<>'' -- Branch Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And regPaymentId in (Select regPaymentId From tbl_RegFeePayment Where createdBy in (select distinct userId from tbl_PartnerAdmin where sBankDevelopId='+@v_fieldName7Vc+'))'
	End	
	
	If @v_fieldName8Vc<>'' -- Branch Member Id
	Begin
		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And createdBy=''' + @v_fieldName8Vc +''''
	End	
	
	Print (@v_fieldName9Vc)
	Print (@v_fieldName10Vc)
	IF (@v_fieldName9Vc is not null And @v_fieldName9Vc <>'') And (@v_fieldName10Vc is not null And @v_fieldName10Vc <>'')
	Begin
		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) >= ''' + @v_fieldName9Vc + ''' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) <= ''' + @v_fieldName10Vc + ''''
	End
	ELSE IF (@v_fieldName9Vc is not null And @v_fieldName9Vc <>'') And (@v_fieldName10Vc is null OR @v_fieldName10Vc='')
	Begin
		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) >= ''' + @v_fieldName9Vc + ''''
	End
	ELSE IF (@v_fieldName9Vc is not null Or @v_fieldName9Vc <>'') And (@v_fieldName10Vc is not null And @v_fieldName10Vc <>'')
	Begin
		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) <= ''' + @v_fieldName10Vc + ''''
	End
	Print(@v_ConditionString_Vc)
	
	-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
	Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT( Distinct regPaymentId ) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')
	From tbl_RegFeePayment RP
	Inner Join tbl_LoginMaster LM On RP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
		WHERE regPaymentId is Not Null ' + @v_ConditionString_Vc
		
	Select @v_CntQry_Vc='SELECT Count(Distinct regPaymentId) 
	From tbl_RegFeePayment RP
	Inner Join tbl_LoginMaster LM On RP.userId=LM.userId
	Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
		WHERE regPaymentId is Not Null ' + @v_ConditionString_Vc
		
	Print ('Count Query')	
	Print(@v_CntQry_Vc)
		
			/* START CODE: DYNAMIC QUERY TO GET RECORDS */
			
			/*
				FieldValue1: Payment Id
				FieldValue2: User Id of Bidder for which Payment is done
				FieldValue3: Email Id of Bidder for which Payment is done
				FieldValue4: Branch Maker - Name of Bank User who has done the Payment
				FieldValue5: Payment Status New Payment/Renewal
				FieldValue6: Date of Payment
			*/
			
		Select @v_FinalQueryVc=
	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int; 
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
	
	Select FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6,FieldValue7, FieldValue8, FieldValue9, FieldValue10 
	From
	(Select Distinct
		Convert(varchar(50),Tmp.regPaymentId) as FieldValue1,
			Convert(varchar(50),LM.userId) as FieldValue2, 
			LM.emailId as FieldValue3, 
			PA.fullName as FieldValue4,
			Case RP.status When ''paid'' Then ''New Registration ''
						   When ''renewed'' Then ''Renewal''
				 End as FieldValue5,
			REPLACE(CONVERT(VARCHAR(11),dtOfPayment, 106), '' '', ''-'')+ '' '' + Substring(CONVERT(VARCHAR(30),dtOfPayment,108),1,5) as FieldValue6,
			CONVERT(varchar(50),@v_TotalPageCnt_Int) as FieldValue7, 
			CONVERT(varchar(50),@v_TotalRecordCnt_Int) as FieldValue8,
			CONVERT(varchar(50),Tmp.RowNumber) as FieldValue9,
			isVerified as FieldValue10,
			Tmp.regPaymentId
		From
		(Select RowNumber, regPaymentId From
			(SELECT ROW_NUMBER() Over (Order by regPaymentId desc) as RowNumber, regPaymentId
				From tbl_RegFeePayment RP
				Inner Join tbl_LoginMaster LM On RP.userId=LM.userId
				Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
		WHERE regPaymentId is Not Null '   
					+@v_ConditionString_Vc+
			') B 				
		
		WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int) 		
		+ ' ) Tmp
		Inner Join tbl_RegFeePayment RP On Tmp.regPaymentId=RP.regPaymentId
			Inner Join tbl_LoginMaster LM On RP.userId=LM.userId
			Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
		) as A Order by A.regPaymentId desc
		'		
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */
		
	
		PRINT(@v_FinalQueryVc)
		Exec (@v_FinalQueryVc)

END


IF @v_fieldName1Vc = 'GetLoginReport'   ----Done by Rajesh for, officer/TenderExtReqListing.jsp
BEGIN

	set @v_PagePerRecordN =@v_fieldName2Vc
	set @v_PageN=@v_fieldName3Vc
	set @v_TeampQueryVc=
	'DECLARE @v_Reccountf int
	DECLARE @v_TotalPagef int
	SELECT @v_Reccountf = Count(*) From (
	SELECT * From (SELECT ROW_NUMBER() OVER (order by sessionId) As Rownumber
	from tbl_SessionMaster) AS DATA) AS TTT
	SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
	SELECT *,cast(@v_TotalPagef as varchar(20)) as FieldValue8,cast(cast(@v_Reccountf as int) as varchar(20)) as FieldValue9 From (SELECT cast(ROW_NUMBER() OVER (order by sessionId) as varchar(20)) As FieldValue1,cast(sessionId as varchar(20)) As FieldValue2,CONVERT(varchar(10),userId) as FieldValue3,
	(select emailid from tbl_loginmaster where tbl_loginmaster.userid=tbl_SessionMaster.userId ) as FieldValue4, CONVERT(VARCHAR(10),sessionStartDt,103)  as FieldValue5,
	CONVERT(VARCHAR(10),sessionEndDt,103) as FieldValue6,CONVERT(varchar(10),ipAddress) as FieldValue7
	from tbl_SessionMaster
	) AS DATA where FieldValue1 between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1 
	AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
	
	print @v_TeampQueryVc								
	exec(@v_TeampQueryVc)

END

IF @v_fieldName1Vc = 'EvalchkPostQuali'   ----Done by Rajesh for, officer/TenderExtReqListing.jsp
BEGIN
	Select evalConfigId from tbl_EvalConfig where tenderId = @v_fieldName2Vc and isPostQualReq = 'yes'
END

IF @v_fieldName1Vc = 'showhidesubmissionbutton'   ----Done by Rajesh for, officer/TenderExtReqListing.jsp
BEGIN
	Select userType as FieldValue1 from tbl_UserTypeMaster where userTypeId=@v_fieldName2Vc
END

IF @v_fieldName1Vc = 'getBidderStatus' 
BEGIN
	select bidderStatus as FieldValue1,convert(varchar(10),pkgLotId) as FieldValue2 from tbl_EvalBidderStatus where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc and pkgLotId =isnull(@v_fieldName4Vc,0)
END


IF @v_fieldName1Vc = 'GetTenderFormsByLotIdUid'    -- By: Krish - For Getting Formname by lotid or packageid - for "Evaluation" module
BEGIN
		Select  CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1, formname  as FieldValue2, 
		CONVERT(VARCHAR(10),tbf.bidId) AS FieldValue3 from 
		tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std  ,tbl_TenderBidForm tbf
		where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
		and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId and isPriceBid='No'
		and tbf.userId = @v_fieldName5Vc 
		and tbf.pkgLotId=@v_fieldName4Vc


		--Select  CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1, formname  as FieldValue2, 
		--CONVERT(VARCHAR(10),tbf.bidId) AS FieldValue3 from 
		--tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std  ,tbl_TenderBidForm tbf
		--where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
		--and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId and isPriceBid='No' and std.packageLotId = @v_fieldName4Vc 
		--and tbf.userId = @v_fieldName5Vc 
		--AND t.tenderformId IN (SELECT formID FROM dbo.f_getformid(@v_fieldName2Vc, @v_fieldName6Vc))
	END

IF @v_fieldName1Vc = 'GetDocuments'
BEGIN
 
	
	
	If (select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1
	Begin
		 select @v_FormList_Vc1 = COALESCE(@v_FormList_Vc1+',', ' ') + convert(varchar(20), formid) 
		 from tbl_biddocuments where userId=@v_fieldName3Vc
		 --print    @v_FormList_Vc1 
		 select  
		  convert(varchar(50),a.companyDocId ) as FieldValue1,
		 convert(varchar(150),documentName) as FieldValue2
			  , convert(varchar(150),documentSize) as FieldValue3
			  , convert(varchar(250),documentBrief) as FieldValue4
			  , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), ' ', '-')   as FieldValue5
			  , convert(varchar(50),docstatus) as FieldValue6
			  , convert(varchar(50),b.companyDocId ) as FieldValue7
			  , documentTypeId as FieldValue11
			  ,case when b.companyDocId is not null then @v_FormList_Vc1 else null end as FieldValue10
		       
			  from (select c.* from tbl_CompanyDocuments c,tbl_TendererMaster t
						where c.tendererId=t.tendererId 
							and t.userId in 
								(
									select userId from tbl_TendererMaster 
										where companyId =
											(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)	
								)												
							 
							and folderId=@v_fieldName2Vc and docStatus!='archive' 
				   ) a
			left outer join
				(select distinct companyDocId  from tbl_biddocuments 
					where userId in 
								(
									select userId from tbl_TendererMaster 
										where companyId =
											(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)	
								)			
				) b
			on a.companyDocId=b.companydocid 
	End
	Else
	Begin
		 select @v_FormList_Vc1 = COALESCE(@v_FormList_Vc1+',', ' ') + convert(varchar(20), formid) 
		 from tbl_biddocuments where userId=@v_fieldName3Vc
		 --print    @v_FormList_Vc1 
		 select  
		  convert(varchar(50),a.companyDocId ) as FieldValue1,
		 convert(varchar(150),documentName) as FieldValue2
			  , convert(varchar(150),documentSize) as FieldValue3
			  , convert(varchar(250),documentBrief) as FieldValue4
			  , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), ' ', '-')   as FieldValue5
			  , convert(varchar(50),docstatus) as FieldValue6
			  , convert(varchar(50),b.companyDocId ) as FieldValue7
			  , documentTypeId as FieldValue11
			  ,case when b.companyDocId is not null then @v_FormList_Vc1 else null end as FieldValue10
		       
			  from (select c.* from tbl_CompanyDocuments c,tbl_TendererMaster t
		where c.tendererId=t.tendererId and userId=@v_fieldName3Vc and folderId=@v_fieldName2Vc and docStatus!='archive' ) a
		left outer join
		(select distinct companyDocId  from tbl_biddocuments where userId=@v_fieldName3Vc) b
		on a.companyDocId=b.companydocid 
	End
	 
	
	 --print @v_FormList_Vc1
END


IF @v_fieldName1Vc = 'GetBidderDocuments'
BEGIN
 
-- select  
--  convert(varchar(50),a.companyDocId ) as FieldValue1,
-- convert(varchar(150),documentName) as FieldValue2
--      , convert(varchar(150),documentSize) as FieldValue3
--      , convert(varchar(250),documentBrief) as FieldValue4
--      , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), ' ', '-')   as FieldValue5
--      , convert(varchar(50),docstatus) as FieldValue6 
--       ,  convert(varchar(50),formId)  as FieldValue10
--        , documentTypeId as FieldValue11
--      from (select c.*,formid from tbl_CompanyDocuments c,tbl_TendererMaster t,tbl_biddocuments b
--where c.tendererId=t.tendererId and t.userId=@v_fieldName2Vc  and b.companyDocId=c.companyDocId
--and formId=@v_fieldName3Vc ) a

select  
  convert(varchar(50),a.companyDocId ) as FieldValue1,
 convert(varchar(150),documentName) as FieldValue2
      , convert(varchar(150),documentSize) as FieldValue3
      , convert(varchar(250),documentBrief) as FieldValue4
      , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), ' ', '-')   as FieldValue5
      , convert(varchar(50),docstatus) as FieldValue6
      ,IsNull(docHash,'') as FieldValue7 
       ,  convert(varchar(50),formId)  as FieldValue10
        , documentTypeId as FieldValue11    
        ,case manDocId
        when 0 then '-' 
        else (select documentName from tbl_TenderMandatoryDoc tmd where tmd.tenderFormDocId=manDocId)  
        end as FieldValue12     
      from (select c.*,formid,b.manDocId from tbl_CompanyDocuments c,tbl_TendererMaster t,tbl_biddocuments b
where c.tendererId=t.tendererId and t.userId=@v_fieldName2Vc  and b.companyDocId=c.companyDocId
and formId=@v_fieldName3Vc and b.userId=t.userId) a
 
 
END
IF @v_fieldName1Vc = 'GetFolders'
BEGIN
	If (select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1
	Begin
		select  
		convert(varchar(50),folderId ) as FieldValue1,
		convert(varchar(150),folderName) as FieldValue2
		from tbl_TendererFolderMaster c,tbl_TendererMaster t
		where c.tendererId=t.tendererId 
		and userId in (
			select userId from tbl_TendererMaster 
				where companyId =
				(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
				
		) 
	End 
	Else
	Begin
		select  
		convert(varchar(50),folderId ) as FieldValue1,
		convert(varchar(150),folderName) as FieldValue2
		from tbl_TendererFolderMaster c,tbl_TendererMaster t
		where c.tendererId=t.tendererId and userId=@v_fieldName3Vc 
	End

  
END

IF @v_fieldName1Vc = 'GetArchiveDocuments'
BEGIN


	If (select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1
	Begin
					 select @v_FormList_Vc3 = COALESCE(@v_FormList_Vc3+',', ' ') + convert(varchar(20), formid) 
		 from tbl_biddocuments where userId=@v_fieldName3Vc
		 select  
		  convert(varchar(50),a.companyDocId ) as FieldValue1,
		 convert(varchar(150),documentName) as FieldValue2
			  , convert(varchar(150),documentSize) as FieldValue3
			  , convert(varchar(250),documentBrief) as FieldValue4
			  , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), ' ', '-')   as FieldValue5
			  , convert(varchar(50),docstatus) as FieldValue6
			  , convert(varchar(50),b.companyDocId ) as FieldValue7
			   , case when b.companyDocId is not null then @v_FormList_Vc3 else null end as FieldValue10
			   , documentTypeId as FieldValue11
			  from (
				select c.* from tbl_CompanyDocuments c,tbl_TendererMaster t
					where c.tendererId=t.tendererId 
					and docstatus='archive' 
					and userId in (
							select userId from tbl_TendererMaster 
								where companyId =
								(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
								
						)
					
					) a
		left outer join
		(
			select distinct companyDocId  from tbl_biddocuments where 
			userId in (
						select userId from tbl_TendererMaster 
							where companyId =
							(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
							
					)
		) b
		on a.companyDocId=b.companydocid
	End
	Else
	Begin
			 select @v_FormList_Vc3 = COALESCE(@v_FormList_Vc3+',', ' ') + convert(varchar(20), formid) 
 from tbl_biddocuments where userId=@v_fieldName3Vc
 select  
  convert(varchar(50),a.companyDocId ) as FieldValue1,
 convert(varchar(150),documentName) as FieldValue2
      , convert(varchar(150),documentSize) as FieldValue3
      , convert(varchar(250),documentBrief) as FieldValue4
      , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), ' ', '-')   as FieldValue5
      , convert(varchar(50),docstatus) as FieldValue6
      , convert(varchar(50),b.companyDocId ) as FieldValue7
       , case when b.companyDocId is not null then @v_FormList_Vc3 else null end as FieldValue10
       , documentTypeId as FieldValue11
      from (select c.* from tbl_CompanyDocuments c,tbl_TendererMaster t
where c.tendererId=t.tendererId and userId=@v_fieldName3Vc and docstatus='archive' ) a
left outer join
(select distinct companyDocId  from tbl_biddocuments where  userId=@v_fieldName3Vc) b
on a.companyDocId=b.companydocid
	End
 
 
 
END

IF @v_fieldName1Vc = 'GetAllDocuments'
BEGIN

 
 If (select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1
 Begin
	 select @v_FormList_Vc = COALESCE(@v_FormList_Vc+',', ' ') + convert(varchar(20), formid) 
 from tbl_biddocuments where userId=@v_fieldName3Vc
  select  
  convert(varchar(50),a.companyDocId ) as FieldValue1,
 convert(varchar(150),documentName) as FieldValue2
      , convert(varchar(150),documentSize) as FieldValue3
      , convert(varchar(250),documentBrief) as FieldValue4
      , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), ' ', '-')   as FieldValue5
      , convert(varchar(50),docstatus) as FieldValue6
      , convert(varchar(50),b.companyDocId ) as FieldValue7,convert(varchar(50),a.folderId ) as FieldValue8,
        convert(varchar(50),isnull(c.foldername,'-') ) as FieldValue9 , documentTypeId as FieldValue11 ,
          case when b.companyDocId is not null then @v_FormList_Vc else null end  as FieldValue10
       
		  from (select c.* from tbl_CompanyDocuments c,tbl_TendererMaster t
		where c.tendererId=t.tendererId and 
		userId in (
				select userId from tbl_TendererMaster 
					where companyId =
					(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
					
			) 
		
		and docstatus!='archive' ) a
	left outer join
	(
		select distinct companyDocId from tbl_biddocuments where 
		userId in (
				select userId from tbl_TendererMaster 
					where companyId =
					(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
					
			)
	) b
	on a.companyDocId=b.companydocid 
	 left outer join
	(
		select distinct t.tendererId,folderName,folderid from tbl_TendererFolderMaster t,tbl_tenderermaster m
			where t.tendererId=m.tendererId 
			and  m.userId in (
				select userId from tbl_TendererMaster 
					where companyId =
					(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
					
			)
		) c
	on a.tendererId=c.tendererId and a.folderId=c.folderId
 End
 Else
 Begin
	 select @v_FormList_Vc = COALESCE(@v_FormList_Vc+',', ' ') + convert(varchar(20), formid) 
 from tbl_biddocuments where userId=@v_fieldName3Vc
  select  
  convert(varchar(50),a.companyDocId ) as FieldValue1,
 convert(varchar(150),documentName) as FieldValue2
      , convert(varchar(150),documentSize) as FieldValue3
      , convert(varchar(250),documentBrief) as FieldValue4
      , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), ' ', '-')   as FieldValue5
      , convert(varchar(50),docstatus) as FieldValue6
      , convert(varchar(50),b.companyDocId ) as FieldValue7,convert(varchar(50),a.folderId ) as FieldValue8,
        convert(varchar(50),isnull(c.foldername,'-') ) as FieldValue9 , documentTypeId as FieldValue11 ,
          case when b.companyDocId is not null then @v_FormList_Vc else null end  as FieldValue10
       
      from (select c.* from tbl_CompanyDocuments c,tbl_TendererMaster t
where c.tendererId=t.tendererId and userId=@v_fieldName3Vc    and docstatus!='archive' ) a
left outer join
(select distinct companyDocId from tbl_biddocuments where userId=@v_fieldName3Vc) b
on a.companyDocId=b.companydocid 
 left outer join
(select distinct t.tendererId,folderName,folderid from tbl_TendererFolderMaster t,tbl_tenderermaster m
 where t.tendererId=m.tendererId and  m.userId=@v_fieldName3Vc ) c
on a.tendererId=c.tendererId and a.folderId=c.folderId
 End
 


END


IF @v_fieldName1Vc = 'getIsSentToPEStatus'
BEGIN
Select Case When Exists (Select tenderId from tbl_TOSListing where tenderId=@v_fieldName2Vc) 
Then 'yes' Else 'no' End as  FieldValue1 
END

IF @v_fieldName1Vc = 'GetQuesPostedByCPforMem'
BEGIN
	IF @v_fieldName5Vc = 'cp'
	BEGIN
		SELECT cpQuestion as FieldValue1, convert(varchar(10),evalCpQueId) as FieldValue2, memAnswer as FieldValue3  FROM tbl_EvalCpMemClarification where tenderformId = @v_fieldName2Vc and userid = @v_fieldName3Vc and cpQuestionBy = @v_fieldName4Vc and memAnswerBy = @v_fieldName6Vc
	END
	ELSE
	BEGIN		
		SELECT cpQuestion as FieldValue1, convert(varchar(10),evalCpQueId) as FieldValue2, memAnswer as FieldValue3  FROM tbl_EvalCpMemClarification where tenderformId = @v_fieldName2Vc and userid = @v_fieldName3Vc and memAnswerBy = @v_fieldName4Vc
	END	
	
END

IF @v_fieldName1Vc = 'getPostedQueFormsForBidder'
BEGIN	
select a.*,CONVERT(varchar(20),b.bidid) as FieldValue3,CONVERT(varchar(20),b.pkgLotId) as FieldValue4 
from 
(Select  CONVERT(VARCHAR(10),tenderformId)   as FieldValue1, 
		formname  as FieldValue2 from 
		tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std
		where tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
		and std.tenderStdId =st.tenderStdId and isPriceBid='No'
		and tenderformId in 
		(Select tenderFormId from tbl_EvalFormQues where tenderId =  @v_fieldName2Vc)
		And (t.formStatus is Null Or t.formStatus in ('p', 'cp'))
)a
left outer join (	  
	  select distinct  bidId,tenderFormId,pkgLotId 
	  from tbl_TenderBidForm 
	  where tenderId=@v_fieldName2Vc 
	  and userid=@v_fieldName3Vc 
)b
	 
	  on a.FieldValue1=b.tenderFormId
	 
END

IF @v_fieldName1Vc = 'getPostedQueFormsForBidderWithoutBids'
BEGIN	
select distinct a.*,CONVERT(varchar(20),b.pkgLotId) as FieldValue4 
from 
(Select  CONVERT(VARCHAR(10),tenderformId)   as FieldValue1, 
		formname  as FieldValue2 from 
		tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std
		where tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
		and std.tenderStdId =st.tenderStdId and isPriceBid='No'
		and tenderformId in 
		(Select tenderFormId from tbl_EvalFormQues where tenderId =  @v_fieldName2Vc)
		And (t.formStatus is Null Or t.formStatus not in ('createp', 'c'))
		
)a
left outer join (	  
	  select distinct  bidId,tenderFormId,pkgLotId 
	  from tbl_TenderBidForm 
	  where tenderId=@v_fieldName2Vc 
	  and userid=@v_fieldName3Vc
)b
	 
	  on a.FieldValue1=b.tenderFormId
	  
	 
END



IF @v_fieldName1Vc = 'getPostedQueFormsForBidderWithBids'
BEGIN	
select a.*,CONVERT(varchar(20),b.bidid) as FieldValue3,CONVERT(varchar(20),b.pkgLotId) as FieldValue4 
from 
(Select  CONVERT(VARCHAR(10),tenderformId)   as FieldValue1, 
		formname  as FieldValue2 from 
		tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std
		where tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
		and std.tenderStdId =st.tenderStdId and isPriceBid='No'
		and tenderformId in 
		(Select tenderFormId from tbl_EvalFormQues where tenderId =  @v_fieldName2Vc)
		And (t.formStatus is Null Or t.formStatus not in ('createp', 'c'))
		And tenderFormId =  @v_fieldName4Vc
)a
left outer join (	  
	  select distinct  bidId,tenderFormId,pkgLotId 
	  from tbl_TenderBidForm 
	  where tenderId=@v_fieldName2Vc 
	  and userid=@v_fieldName3Vc 
	  And tenderFormId =  @v_fieldName4Vc
)b
	 
	  on a.FieldValue1=b.tenderFormId
	
	 
END





If @v_fieldName1Vc='EvalClariDocsCP'
BEGIN
  		
			Select documentName as FieldValue1, docDescription as FieldValue2, 
			docSize as FieldValue3,cast(evalClrDocId  as varchar(10)) as FieldValue4 ,
			doctype as FieldValue5,	cast(uploadedBy  as varchar(10)) as FieldValue6,
			Case doctype 
			When 'Officer'
			Then Convert(varchar(50),uploadedBy) + '_' + Convert(varchar(50),userId)+ '_' + Convert(varchar(50),formId) 
			When 'Tenderer'
			Then Convert(varchar(50),userId) + '_' + Convert(varchar(50),formId) 
			End
			as FieldValue7
			from tbl_EvalClariDocs 
			Where tenderId=@v_fieldName2Vc 
				  and 
				  formId=@v_fieldName3Vc
				  And  userId=@v_fieldName4Vc --and  Uploadedby=@v_fieldName4Vc 
End
	
	
--This fuction is now in egpData	
--IF @v_fieldName1Vc = 'getRegFeePayment_MISReport' 
--BEGIN
	
--	select @v_ConditionString_Vc=''
--	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
--	Select @v_Page_inInt=@v_fieldName2Vc, @v_RecordPerPage_inInt=@v_fieldName3Vc
	
--	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
--	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)	
--	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */		
	
	
--	If @v_fieldName4Vc is not null And @v_fieldName4Vc<>'' -- Creiteris: Status
--	Begin
--		If @v_fieldName4Vc='New'
--		Begin
--			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And RP.status=''paid'''
--		End
--		Else If @v_fieldName4Vc='Renew'
--		Begin
--			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And RP.status=''renewed'''
--		End
--		Else If @v_fieldName4Vc='Expired'
--		Begin
--			Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And LM.status Not in (''incomplete'',''pending'') And LM.validUpTo < getdate()'
--		End		
--	End		
	
	
--	IF (@v_fieldName5Vc is not null And @v_fieldName5Vc <>'') And (@v_fieldName6Vc is not null And @v_fieldName6Vc <>'')
--	Begin
--		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) >= ''' + @v_fieldName5Vc + ''' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) <= ''' + @v_fieldName6Vc + ''''
--	End
--	ELSE IF (@v_fieldName5Vc is not null And @v_fieldName5Vc <>'') And (@v_fieldName6Vc is null OR @v_fieldName6Vc='')
--	Begin
--		Select @v_ConditionString_Vc =@v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) >= ''' + @v_fieldName5Vc + ''''
--	End
--	ELSE IF (@v_fieldName5Vc is not null Or @v_fieldName5Vc <>'') And (@v_fieldName6Vc is not null And @v_fieldName6Vc <>'')
--	Begin
--		Select @v_ConditionString_Vc = @v_ConditionString_Vc + ' And Cast (Floor(Cast (dtOfPayment as Float)) as Datetime) <= ''' + @v_fieldName6Vc + ''''
--	End
	
--	If @v_fieldName7Vc is not null And @v_fieldName7Vc<>'' -- Creiteris: Email ID
--	Begin
--		if @v_fieldName9Vc = '='
--		begin
--		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And LM.emailId ='''+@v_fieldName7Vc+''''
--		end
--		else
--		begin
--		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And LM.emailId like ''%'+@v_fieldName7Vc+'%'''
--		end
--	End
	
--	If @v_fieldName8Vc is not null And @v_fieldName8Vc<>'' -- Creiteris: Company Name
--	Begin
--	if @v_fieldName10Vc = '='
--		begin
--		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And dbo.f_getbiddercompany(LM.userId) ='''+@v_fieldName8Vc+''''
--		end
--		else
--		begin
--		Select @v_ConditionString_Vc=@v_ConditionString_Vc + ' And dbo.f_getbiddercompany(LM.userId) like ''%'+@v_fieldName8Vc+'%'''
--		end
--	End

	
--	--Print(@v_ConditionString_Vc)
	
--	-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
--	Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT( Distinct regPaymentId ) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')
--	From tbl_RegFeePayment RP
--		Inner join tbl_LoginMaster LM On RP.userId=LM.userId
--		Inner join tbl_TendererMaster TM On TM.userId=LM.userId
--		Where isLive=''yes'' and isVerified=''yes'' ' + @v_ConditionString_Vc
		
--	Select @v_CntQry_Vc='SELECT Count(Distinct regPaymentId) 
--	From tbl_RegFeePayment RP
--		Inner join tbl_LoginMaster LM On RP.userId=LM.userId
--		Inner join tbl_TendererMaster TM On TM.userId=LM.userId
--		Where isLive=''yes'' and isVerified=''yes'' ' + @v_ConditionString_Vc
				
		
--			/* START CODE: DYNAMIC QUERY TO GET RECORDS */
			
--			/*
--				FieldValue1: Payment Id
--				FieldValue2: User Id of Bidder for which Payment is done
--				FieldValue3: Email Id of Bidder for which Payment is done
--				FieldValue4: Branch Maker - Name of Bank User who has done the Payment
--				FieldValue5: Payment Status New Payment/Renewal
--				FieldValue6: Date of Payment
--			*/
			
--		Select @v_FinalQueryVc=
--	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int; 
--		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
--		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
	
--	Select FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6,FieldValue7, FieldValue8, FieldValue9, FieldValue10, FieldValue11, FieldValue12
--	From
--	(Select distinct Convert(varchar(50), RP.regPaymentId) as FieldValue1, 
--	Convert(varchar(50), LM.userId) as FieldValue2, 
--	CASE LM.registrationType 
--		When ''individualconsultant'' Then ''Individual Consultant''
--		When ''govtundertaking'' Then ''Government owned Enterprise''
--		When ''media'' Then ''Media''
--		When ''contractor'' Then ''Tenderer / Consultant''
--		Else ''''
--		End as FieldValue3,
--	Case RP.status	
--		When ''paid'' Then ''New''
--		When ''renewed'' Then ''Renewal''
--		End as FieldValue4,
--	IsNUll(dbo.f_getbiddercompany(LM.userId),'''') as FieldValue5,
--	LM.emailId as FieldValue6,
--	TM.country as FieldValue7,
--	TM.state as FieldValue8,
--	REPLACE(CONVERT(VARCHAR(11),RP.dtOfPayment, 106), '' '', ''-'')+ '' '' + Substring(CONVERT(VARCHAR(30),RP.dtOfPayment,108),1,5) as FieldValue9,
--	Case Currency 
--		When ''BDT''
--			Then ''Taka'' + '' '' + CONVERT(varchar(50),RP.amount) 
--		When ''USD''	
--			Then ''$'' + '' '' + CONVERT(varchar(50),RP.amount) 
--			End as FieldValue10,	
--	CONVERT(varchar(50),@v_TotalPageCnt_Int) as FieldValue11, 
--	CONVERT(varchar(50),@v_TotalRecordCnt_Int) as FieldValue12,
--	CONVERT(varchar(50),Tmp.RowNumber) as FieldValue13,	
--	Tmp.regPaymentId
--		From
--		(Select RowNumber, regPaymentId From
--			(
--				Select ROW_NUMBER() Over (Order by regPaymentId desc) as RowNumber, regPaymentId From
--				(
--					SELECT distinct RP.regPaymentId
--						From tbl_RegFeePayment RP
--						Inner join tbl_LoginMaster LM On RP.userId=LM.userId
--						Inner join tbl_TendererMaster TM On TM.userId=LM.userId
--						Where isLive=''yes'' and isVerified=''yes'' '   
--						+@v_ConditionString_Vc+
--				') as InnerTbl
--			) B 				
--			WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int) 		
--		+ ' ) Tmp
--		Inner Join tbl_RegFeePayment RP On Tmp.regPaymentId=RP.regPaymentId			
--			Inner join tbl_LoginMaster LM On RP.userId=LM.userId
--			Inner join tbl_TendererMaster TM On TM.userId=LM.userId
--		) as A Order by A.regPaymentId desc
--		'		
--		/* END CODE: DYNAMIC QUERY TO GET RECORDS */
		
	
--		PRINT(@v_FinalQueryVc)
--		Exec (@v_FinalQueryVc)

--END

IF @v_fieldName1Vc = 'getRegFeePaymentAmount' 
BEGIN

	If @v_fieldName2Vc='renewal'
	Begin
		-- // RENEWAL PAYMENT
		
		If @v_fieldName3Vc='BDT'	
		Begin
			If ((Select validUpTo from tbl_LoginMaster Where userId=@v_fieldName5Vc)	>= GETDATE())
			Begin
				Select CONVERT(varchar(50), renewAmtDBT) as FieldValue1 From tbl_RegistrationValidity Where validityId=@v_fieldName4Vc 	
			End
			Else
			Begin
				Select CONVERT(varchar(50), renewLateAmtBDT) as FieldValue1 From tbl_RegistrationValidity Where validityId=@v_fieldName4Vc
			End			
		End
		Else If @v_fieldName3Vc='USD'	
		Begin
			If ((Select validUpTo from tbl_LoginMaster Where userId=@v_fieldName5Vc) >= GETDATE())
			Begin
				Select CONVERT(varchar(50), renewAmtUSD) as FieldValue1 From tbl_RegistrationValidity Where validityId=@v_fieldName4Vc 	
			End
			Else
			Begin
				Select CONVERT(varchar(50), renewLateAmtUSD) as FieldValue1 From tbl_RegistrationValidity Where validityId=@v_fieldName4Vc
			End			
		End
	End
	Else 
	Begin
		-- // FIRST TIME PAYMENT
		If @v_fieldName3Vc='BDT'	
		Begin
			Select CONVERT(varchar(50), amtBDT) as FieldValue1 From tbl_RegistrationValidity Where validityId=@v_fieldName4Vc 	
		End
		Else If @v_fieldName3Vc='USD'	
		Begin
			Select CONVERT(varchar(50), amtUSD) as FieldValue1 From tbl_RegistrationValidity Where validityId=@v_fieldName4Vc 		
		End
	End

END
IF @v_fieldName1Vc = 'getMobNoforSMS' 
BEGIN
SELECT replace('+880'+convert(varchar(30),emp.mobileNo),'+8800','+880') as FieldValue1, 
mm.modulename as FieldValue2, em.eventName as FieldValue3,
 convert(varchar(30),objectId )as FieldValue4 
FROM tbl_ActivityMaster am INNER JOIN tbl_EventMaster em 
ON am.eventId = em.eventId INNER JOIN tbl_WorkFlowFileOnHand  wf
 ON am.activityId = wf.activityId AND em.eventId = wf.eventId 
 INNER JOIN tbl_EmployeeMaster emp ON wf.toUserId = emp.userId
  INNER JOIN tbl_ModuleMaster mm ON em.moduleId = mm.moduleId 
  and DATEDIFF (day ,wf.processdate ,GETDATE() ) = 2
 union
 SELECT  convert(varchar(30),emp.mobileNo) as FieldValue1, 
mm.modulename as FieldValue2, em.eventName as FieldValue3,
 convert(varchar(30),objectId )as FieldValue4 
FROM tbl_ActivityMaster am INNER JOIN tbl_EventMaster em 
ON am.eventId = em.eventId INNER JOIN tbl_WorkFlowFileOnHand  wf
 ON am.activityId = wf.activityId AND em.eventId = wf.eventId 
 INNER JOIN tbl_PartnerAdmin emp ON wf.toUserId = emp.userId
  INNER JOIN tbl_ModuleMaster mm ON em.moduleId = mm.moduleId 
  and DATEDIFF (day ,wf.processdate ,GETDATE() ) = 2 

End

IF @v_fieldName1Vc = 'GetFormsforTSC'    -- By: Krish - For Getting Formname by lotid or packageid - for "Evaluation" module
BEGIN

	IF @v_fieldName3Vc = '1'
	BEGIN
		Select  CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1, formname  as FieldValue2, 
		CONVERT(VARCHAR(10),tbf.bidId) AS FieldValue3 from 
		tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std  ,tbl_TenderBidForm tbf
		where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
		and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId and isPriceBid='No' and std.packageLotId = @v_fieldName4Vc 
		and tbf.userId = @v_fieldName5Vc 
		-- AND t.tenderformId IN (SELECT formID FROM tbl_EvalMapForms where tenderId = @v_fieldName2Vc and tscCom='yes')
		And (t.formStatus is null OR t.formStatus in ('p', 'cp'))
	END
	ELSE
	BEGIN
	
		Select  CONVERT(VARCHAR(10),t.tenderformId) AS FieldValue1, formname  as FieldValue2, 
		CONVERT(VARCHAR(10),tbf.bidId) AS FieldValue3 from 
		tbl_TenderForms t,tbl_TenderSection st,tbl_TenderStd std ,tbl_TenderBidForm tbf
		where std.tenderid = @v_fieldName2Vc and t.tenderSectionId=st.tenderSectionId
		and tbf.tenderFormId = t.tenderFormId and std.tenderStdId =st.tenderStdId and isPriceBid='No'
		and tbf.userId = @v_fieldName5Vc 
		--AND t.tenderformId IN (SELECT formID FROM tbl_EvalMapForms where tenderId = @v_fieldName2Vc and tscCom='yes')
		And (t.formStatus is null OR t.formStatus in ('p', 'cp','a'))
		
	END	
	  
END	

--exec p_get_commonsearchdata 'SearchTenPayment','','kosha/54',1,10
--IF @v_fieldName1Vc = 'SearchTenPayment' -- for/partner/SearchTenPayment.jsp
--BEGIN

--	set @v_PagePerRecordN =@v_fieldName5Vc
	
--	set @v_PageN=@v_fieldName4Vc
	
--	IF @v_fieldName2Vc != '' and @v_fieldName3Vc != ''
--	BEGIN
--		set @v_TeampQueryVc=
--		'DECLARE @v_Reccountf int
--		DECLARE @v_TotalPagef int
--		SELECT @v_Reccountf = Count(*) From (
--		SELECT * From (SELECT ROW_NUMBER() OVER (order by tenderId) As Rownumber
--		from tbl_TenderDetails WHERE tenderId='''+@v_fieldName2Vc+''' and reoiRfpRefNo='''+@v_fieldName3Vc+''' and tbl_TenderDetails.tenderStatus=''Approved''
--		) AS DATA) AS TTT
		
--		SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
--		SELECT *,cast(@v_TotalPagef as varchar(20)) as FieldValue8,cast(cast(@v_Reccountf as int) as varchar(20)) as FieldValue9 From (SELECT cast(ROW_NUMBER() OVER (order by tenderId) as varchar(20)) As FieldValue7,(cast(case when tenderId is null then ''N.A'' else tenderId end as varchar(10))+'',<br/>''+cast(case when reoiRfpRefNo ='''' then ''N.A'' else reoiRfpRefNo end as varchar(100))) as FieldValue1,(case when procurementNature  ='''' then ''N.A'' else procurementNature  end+'',<br/>''+case when tenderBrief ='''' then ''N.A'' else tenderBrief end) as FieldValue2,
--			(case when ministry  ='''' then ''N.A'' else ministry  end +'',<br/>''+case when division  ='''' then ''N.A'' else division  end+'',<br/>''+case when agency  ='''' then ''N.A'' else agency  end+'',<br/>''+case when peOfficeName  ='''' then ''N.A'' else peOfficeName  end) as FieldValue3,(procurementType +'',<br/>''+procurementMethod) as FieldValue4,(isnull(cast(REPLACE(CONVERT(VARCHAR(11),tenderPubDt, 106), '' '', ''-'') as varchar(100)),''N.A'') +'',''+isnull(cast(REPLACE(CONVERT(VARCHAR(11),submissionDt, 106), '' '', ''-'') as varchar(100)),''N.A'')) as FieldValue5,docAvlMethod as FieldValue6
--		from tbl_TenderDetails WHERE tenderId='''+@v_fieldName2Vc+''' and reoiRfpRefNo='''+@v_fieldName3Vc+''' and tbl_TenderDetails.tenderStatus=''Approved''
--		) AS DATA where FieldValue7 between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1 
--		AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
--	END
--	ELSE IF (@v_fieldName2Vc = '' or @v_fieldName2Vc is NULL) and @v_fieldName3Vc != ''
--	BEGIN
--		set @v_TeampQueryVc=
--		'DECLARE @v_Reccountf int
--		DECLARE @v_TotalPagef int
--		SELECT @v_Reccountf = Count(*) From (
--		SELECT * From (SELECT ROW_NUMBER() OVER (order by tenderId) As Rownumber
--		from tbl_TenderDetails WHERE reoiRfpRefNo='''+@v_fieldName3Vc+'''  and tbl_TenderDetails.tenderStatus=''Approved''
--		) AS DATA) AS TTT
		
--		SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
--		SELECT *,cast(@v_TotalPagef as varchar(20)) as FieldValue8,cast(cast(@v_Reccountf as int) as varchar(20)) as FieldValue9 From (SELECT cast(ROW_NUMBER() OVER (order by tenderId) as varchar(20)) As FieldValue7,(cast(case when tenderId is null then ''N.A'' else tenderId end as varchar(10))+'',<br/>''+cast(case when reoiRfpRefNo ='''' then ''N.A'' else reoiRfpRefNo end as varchar(100))) as FieldValue1,(case when procurementNature  ='''' then ''N.A'' else procurementNature  end+'',<br/>''+case when tenderBrief ='''' then ''N.A'' else tenderBrief end) as FieldValue2,
--			(case when ministry  ='''' then ''N.A'' else ministry  end +'',<br/>''+case when division  ='''' then ''N.A'' else division  end+'',<br/>''+case when agency  ='''' then ''N.A'' else agency  end+'',<br/>''+case when peOfficeName  ='''' then ''N.A'' else peOfficeName  end) as FieldValue3,(procurementType +'',<br/>''+procurementMethod) as FieldValue4,(isnull(cast(REPLACE(CONVERT(VARCHAR(11),tenderPubDt, 106), '' '', ''-'') as varchar(100)),''N.A'') +'',''+isnull(cast(REPLACE(CONVERT(VARCHAR(11),submissionDt, 106), '' '', ''-'') as varchar(100)),''N.A'')) as FieldValue5,docAvlMethod as FieldValue6
--		from tbl_TenderDetails WHERE reoiRfpRefNo='''+@v_fieldName3Vc+'''  and tbl_TenderDetails.tenderStatus=''Approved''
--		) AS DATA where FieldValue7 between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1 
--		AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
--	END
--	ELSE IF @v_fieldName2Vc != '' and (@v_fieldName3Vc = '' OR @v_fieldName3Vc is NULL)
--	BEGIN
--		set @v_TeampQueryVc=
--		'DECLARE @v_Reccountf int
--		DECLARE @v_TotalPagef int
--		SELECT @v_Reccountf = Count(*) From (
--		SELECT * From (SELECT ROW_NUMBER() OVER (order by tenderId) As Rownumber
--		from tbl_TenderDetails WHERE tenderId='''+@v_fieldName2Vc+'''  and tbl_TenderDetails.tenderStatus=''Approved''
--		) AS DATA) AS TTT
		
--		SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
--		SELECT *,cast(@v_TotalPagef as varchar(20)) as FieldValue8,cast(cast(@v_Reccountf as int) as varchar(20)) as FieldValue9 From (SELECT cast(ROW_NUMBER() OVER (order by tenderId) as varchar(20)) As FieldValue7,(cast(case when tenderId is null then ''N.A'' else tenderId end as varchar(10))+'',<br/>''+cast(case when reoiRfpRefNo ='''' then ''N.A'' else reoiRfpRefNo end as varchar(100))) as FieldValue1,(case when procurementNature  ='''' then ''N.A'' else procurementNature  end+'',<br/>''+case when tenderBrief ='''' then ''N.A'' else tenderBrief end) as FieldValue2,
--			(case when ministry  ='''' then ''N.A'' else ministry  end +'',<br/>''+case when division  ='''' then ''N.A'' else division  end+'',<br/>''+case when agency  ='''' then ''N.A'' else agency  end+'',<br/>''+case when peOfficeName  ='''' then ''N.A'' else peOfficeName  end) as FieldValue3,(procurementType +'',<br/>''+procurementMethod) as FieldValue4,(isnull(cast(REPLACE(CONVERT(VARCHAR(11),tenderPubDt, 106), '' '', ''-'') as varchar(100)),''N.A'') +'',''+isnull(cast(REPLACE(CONVERT(VARCHAR(11),submissionDt, 106), '' '', ''-'') as varchar(100)),''N.A'')) as FieldValue5,docAvlMethod as FieldValue6
--		from tbl_TenderDetails WHERE tenderId='''+@v_fieldName2Vc+'''  and tbl_TenderDetails.tenderStatus=''Approved''
--		) AS DATA where FieldValue7 between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1 
--		AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
--	END
--	ELSE IF (@v_fieldName2Vc = '' OR @v_fieldName2Vc is NULL) and (@v_fieldName3Vc = '' or @v_fieldName3Vc is NULL)
--	BEGIN
--		set @v_TeampQueryVc=
--		'DECLARE @v_Reccountf int
--		DECLARE @v_TotalPagef int
--		SELECT @v_Reccountf = Count(*) From (
--		SELECT * From (SELECT ROW_NUMBER() OVER (order by tenderId) As Rownumber
--		FROM tbl_TenderDetails where tbl_TenderDetails.tenderStatus=''Approved''
--		) AS DATA) AS TTT
		
--		SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
--		SELECT *,cast(@v_TotalPagef as varchar(20)) as FieldValue8,cast(cast(@v_Reccountf as int) as varchar(20)) as FieldValue9 From (SELECT cast(ROW_NUMBER() OVER (order by tenderId) as varchar(20)) As FieldValue7,(cast(case when tenderId is null then ''N.A'' else tenderId end as varchar(10))+'',<br/>''+cast(case when reoiRfpRefNo ='''' then ''N.A'' else reoiRfpRefNo end as varchar(100))) as FieldValue1,(case when procurementNature  ='''' then ''N.A'' else procurementNature  end+'',<br/>''+case when tenderBrief ='''' then ''N.A'' else tenderBrief end) as FieldValue2,
--			(case when ministry  ='''' then ''N.A'' else ministry  end +'',<br/>''+case when division  ='''' then ''N.A'' else division  end+'',<br/>''+case when agency  ='''' then ''N.A'' else agency  end+'',<br/>''+case when peOfficeName  ='''' then ''N.A'' else peOfficeName  end) as FieldValue3,(procurementType +'',<br/>''+procurementMethod) as FieldValue4,(isnull(cast(REPLACE(CONVERT(VARCHAR(11),tenderPubDt, 106), '' '', ''-'') as varchar(100)),''N.A'') +'',''+isnull(cast(REPLACE(CONVERT(VARCHAR(11),submissionDt, 106), '' '', ''-'') as varchar(100)),''N.A'')) as FieldValue5,docAvlMethod as FieldValue6
--		FROM tbl_TenderDetails where tbl_TenderDetails.tenderStatus=''Approved''
--		) AS DATA where FieldValue7 between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1 
--		AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
--	END
	
--	--Set @v_TeampQueryVc = @v_TeampQueryVc + ' Order by tenderPubDt Desc'
--	print @v_TeampQueryVc								
--	exec(@v_TeampQueryVc)
--END
IF @v_fieldName1Vc = 'SearchTenPayment' -- for/partner/SearchTenPayment.jsp
BEGIN

	set @v_PagePerRecordN =@v_fieldName5Vc
	
	set @v_PageN=@v_fieldName4Vc
	
	IF @v_fieldName2Vc != '' and @v_fieldName3Vc != ''
	BEGIN
		set @v_TeampQueryVc=
		'DECLARE @v_Reccountf int
		DECLARE @v_TotalPagef int
		SELECT @v_Reccountf = Count(*) From (
		SELECT * From (SELECT ROW_NUMBER() OVER (order by tenderPubDt desc) As Rownumber
		from tbl_TenderDetails WHERE tenderId='''+@v_fieldName2Vc+''' and reoiRfpRefNo='''+@v_fieldName3Vc+''' and tbl_TenderDetails.tenderStatus<>''Pending'' and tenderPubDt <= getdate()
		) AS DATA) AS TTT
		
		SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
		SELECT *,cast(@v_TotalPagef as varchar(20)) as FieldValue8,cast(cast(@v_Reccountf as int) as varchar(20)) as FieldValue9 From (SELECT tenderPubDt,cast(ROW_NUMBER() OVER (order by tenderPubDt  desc) as varchar(20)) As FieldValue7,(cast(case when tenderId is null then ''N.A'' else tenderId end as varchar(10))+'',<br/>''+cast(case when reoiRfpRefNo ='''' then ''N.A'' else reoiRfpRefNo end as varchar(100))) as FieldValue1,(case when procurementNature  ='''' then ''N.A'' else procurementNature  end+'',<br/>''+case when tenderBrief ='''' then ''N.A'' else tenderBrief end) as FieldValue2,
			(case when ministry  ='''' then ''N.A'' else ministry  end +'',<br/>''+case when division  ='''' then ''N.A'' else division  end+'',<br/>''+case when agency  ='''' then ''N.A'' else agency  end+'',<br/>''+case when peOfficeName  ='''' then ''N.A'' else peOfficeName  end) as FieldValue3,(procurementType +'',<br/>''+procurementMethod) as FieldValue4,(isnull(cast(REPLACE(CONVERT(VARCHAR(11),tenderPubDt, 106), '' '', ''-'') as varchar(100)),''N.A'') +'',''+isnull(cast(REPLACE(CONVERT(VARCHAR(11),submissionDt, 106), '' '', ''-'') as varchar(100)),''N.A'')) as FieldValue5,docAvlMethod as FieldValue6
		from tbl_TenderDetails WHERE tenderId='''+@v_fieldName2Vc+''' and reoiRfpRefNo='''+@v_fieldName3Vc+''' and tbl_TenderDetails.tenderStatus<>''Pending'' and tenderPubDt <= getdate()
		) AS DATA where FieldValue7 between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1 
		AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
	END
	ELSE IF (@v_fieldName2Vc = '' or @v_fieldName2Vc is NULL) and @v_fieldName3Vc != ''
	BEGIN
		set @v_TeampQueryVc=
		'DECLARE @v_Reccountf int
		DECLARE @v_TotalPagef int
		SELECT @v_Reccountf = Count(*) From (
		SELECT * From (SELECT ROW_NUMBER() OVER (order by tenderPubDt  desc) As Rownumber
		from tbl_TenderDetails WHERE reoiRfpRefNo='''+@v_fieldName3Vc+'''  and tbl_TenderDetails.tenderStatus<>''Pending'' and tenderPubDt <= getdate()
		) AS DATA) AS TTT
		
		SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
		SELECT *,cast(@v_TotalPagef as varchar(20)) as FieldValue8,cast(cast(@v_Reccountf as int) as varchar(20)) as FieldValue9 From (SELECT tenderPubDt, cast(ROW_NUMBER() OVER (order by tenderPubDt  desc) as varchar(20)) As FieldValue7,(cast(case when tenderId is null then ''N.A'' else tenderId end as varchar(10))+'',<br/>''+cast(case when reoiRfpRefNo ='''' then ''N.A'' else reoiRfpRefNo end as varchar(100))) as FieldValue1,(case when procurementNature  ='''' then ''N.A'' else procurementNature  end+'',<br/>''+case when tenderBrief ='''' then ''N.A'' else tenderBrief end) as FieldValue2,
			(case when ministry  ='''' then ''N.A'' else ministry  end +'',<br/>''+case when division  ='''' then ''N.A'' else division  end+'',<br/>''+case when agency  ='''' then ''N.A'' else agency  end+'',<br/>''+case when peOfficeName  ='''' then ''N.A'' else peOfficeName  end) as FieldValue3,(procurementType +'',<br/>''+procurementMethod) as FieldValue4,(isnull(cast(REPLACE(CONVERT(VARCHAR(11),tenderPubDt, 106), '' '', ''-'') as varchar(100)),''N.A'') +'',''+isnull(cast(REPLACE(CONVERT(VARCHAR(11),submissionDt, 106), '' '', ''-'') as varchar(100)),''N.A'')) as FieldValue5,docAvlMethod as FieldValue6
		from tbl_TenderDetails WHERE reoiRfpRefNo='''+@v_fieldName3Vc+'''  and tbl_TenderDetails.tenderStatus<>''Pending'' and tenderPubDt <= getdate()
		) AS DATA where FieldValue7 between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1 
		AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
	END
	ELSE IF @v_fieldName2Vc != '' and (@v_fieldName3Vc = '' OR @v_fieldName3Vc is NULL)
	BEGIN
		set @v_TeampQueryVc=
		'DECLARE @v_Reccountf int
		DECLARE @v_TotalPagef int
		SELECT @v_Reccountf = Count(*) From (
		SELECT * From (SELECT ROW_NUMBER() OVER (order by tenderPubDt  desc) As Rownumber
		from tbl_TenderDetails WHERE tenderId='''+@v_fieldName2Vc+'''  and tbl_TenderDetails.tenderStatus<>''Pending'' and tenderPubDt <= getdate()
		) AS DATA) AS TTT
		
		SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
		SELECT *,cast(@v_TotalPagef as varchar(20)) as FieldValue8,cast(cast(@v_Reccountf as int) as varchar(20)) as FieldValue9 From (SELECT tenderPubDt, cast(ROW_NUMBER() OVER (order by tenderPubDt  desc) as varchar(20)) As FieldValue7,(cast(case when tenderId is null then ''N.A'' else tenderId end as varchar(10))+'',<br/>''+cast(case when reoiRfpRefNo ='''' then ''N.A'' else reoiRfpRefNo end as varchar(100))) as FieldValue1,(case when procurementNature  ='''' then ''N.A'' else procurementNature  end+'',<br/>''+case when tenderBrief ='''' then ''N.A'' else tenderBrief end) as FieldValue2,
			(case when ministry  ='''' then ''N.A'' else ministry  end +'',<br/>''+case when division  ='''' then ''N.A'' else division  end+'',<br/>''+case when agency  ='''' then ''N.A'' else agency  end+'',<br/>''+case when peOfficeName  ='''' then ''N.A'' else peOfficeName  end) as FieldValue3,(procurementType +'',<br/>''+procurementMethod) as FieldValue4,(isnull(cast(REPLACE(CONVERT(VARCHAR(11),tenderPubDt, 106), '' '', ''-'') as varchar(100)),''N.A'') +'',''+isnull(cast(REPLACE(CONVERT(VARCHAR(11),submissionDt, 106), '' '', ''-'') as varchar(100)),''N.A'')) as FieldValue5,docAvlMethod as FieldValue6
		from tbl_TenderDetails WHERE tenderId='''+@v_fieldName2Vc+'''  and tbl_TenderDetails.tenderStatus<>''Pending'' and tenderPubDt <= getdate()
		) AS DATA where FieldValue7 between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1 
		AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
	END
	ELSE IF (@v_fieldName2Vc = '' OR @v_fieldName2Vc is NULL) and (@v_fieldName3Vc = '' or @v_fieldName3Vc is NULL)
	BEGIN
		set @v_TeampQueryVc=
		'DECLARE @v_Reccountf int
		DECLARE @v_TotalPagef int
		SELECT @v_Reccountf = Count(*) From (
		SELECT * From (SELECT ROW_NUMBER() OVER (order by tenderPubDt  desc) As Rownumber
		FROM tbl_TenderDetails where tbl_TenderDetails.tenderStatus<>''Pending'' and tenderPubDt <= getdate()
		) AS DATA) AS TTT
		
		SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
		SELECT *,cast(@v_TotalPagef as varchar(20)) as FieldValue8,cast(cast(@v_Reccountf as int) as varchar(20)) as FieldValue9 From (SELECT tenderPubDt,cast(ROW_NUMBER() OVER (order by tenderPubDt  desc) as varchar(20)) As FieldValue7,(cast(case when tenderId is null then ''N.A'' else tenderId end as varchar(10))+'',<br/>''+cast(case when reoiRfpRefNo ='''' then ''N.A'' else reoiRfpRefNo end as varchar(100))) as FieldValue1,(case when procurementNature  ='''' then ''N.A'' else procurementNature  end+'',<br/>''+case when tenderBrief ='''' then ''N.A'' else tenderBrief end) as FieldValue2,
			(case when ministry  ='''' then ''N.A'' else ministry  end +'',<br/>''+case when division  ='''' then ''N.A'' else division  end+'',<br/>''+case when agency  ='''' then ''N.A'' else agency  end+'',<br/>''+case when peOfficeName  ='''' then ''N.A'' else peOfficeName  end) as FieldValue3,(procurementType +'',<br/>''+procurementMethod) as FieldValue4,(isnull(cast(REPLACE(CONVERT(VARCHAR(11),tenderPubDt, 106), '' '', ''-'') as varchar(100)),''N.A'') +'',''+isnull(cast(REPLACE(CONVERT(VARCHAR(11),submissionDt, 106), '' '', ''-'') as varchar(100)),''N.A'')) as FieldValue5,docAvlMethod as FieldValue6
		FROM tbl_TenderDetails where tbl_TenderDetails.tenderStatus<>''Pending'' and tenderPubDt <= getdate()
		) AS DATA where FieldValue7 between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1 
		AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
	END
	
	Set @v_TeampQueryVc = @v_TeampQueryVc + ' Order by tenderPubDt Desc'
	print @v_TeampQueryVc								
	exec(@v_TeampQueryVc)
END


IF @v_fieldName1Vc = 'GetDebarmentUserStatus'    
Begin
Declare @debarId varchar(20),@debarTypeId varchar(20),@hopeId int,@query varchar(8000),@dtFrom smalldatetime, @dtTo smalldatetime, @debarComments varchar(max),@rowcnt int
DECLARE @debardetail TABLE (tenderId varchar(200))

DECLARE cur_tblNames CURSOR FAST_FORWARD FOR  
select CASE WHEN tdd.debarTypeId = 6 then  @v_fieldName2Vc 
 ELSE debarIds END AS debarIds,tdr.debarTypeId,debarStartDt,debarEdnDt,comments,hopeId
from tbl_DebarmentReq tdr,tbl_DebarmentDetails tdd
where (userId in (select userId from tbl_TendererMaster where companyId in(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc) and companyId!=1) or userId=@v_fieldName3Vc)
and tdr.debarmentId=tdd.debarmentId and tdd.debarStatus='byhope' and 
(@v_fieldName4Vc='0' OR (Cast(FLOOR(CAST (debarStartDt as float)) as Datetime) <= Cast(FLOOR(CAST (GETDATE() as float)) as Datetime) and Cast(FLOOR(CAST (debarEdnDt as float)) as Datetime) >= Cast(FLOOR(CAST (GETDATE() as float)) as Datetime)))
 and tdr.debarmentStatus in ('appdebarcomhope','appdebaregp','appdebarhope')
OPEN cur_tblNames        
FETCH NEXT FROM cur_tblNames INTO @debarId,@debarTypeId,@dtFrom,@dtTo ,@debarComments,@hopeId
WHILE @@FETCH_STATUS = 0        
BEGIN 
if @debarTypeId = 1
begin 
set @query ='select tenderId from tbl_TenderMaster where convert(varchar(20),tenderId) in('+@debarId+') and tenderId= '+@v_fieldName2Vc
end

if @debarTypeId = 2
begin 
set @query ='select tenderId from tbl_TenderMaster where convert(varchar(20),packageid) in('+@debarId+') and tenderId= '+@v_fieldName2Vc
end

if @debarTypeId = 3
begin 
set @query ='select tenderId from tbl_TenderMaster where convert(varchar(20),tenderid) in ( select tenderId from tbl_TenderMaster t,tbl_AppMaster a where t.appId=a.appId and projectid in('+@debarId+')) and tenderId= '+@v_fieldName2Vc
end 

if @debarTypeId = 4
begin 
set @query ='select t.tenderId from tbl_TenderMaster t,tbl_tenderdetails td where t.tenderid=td.tenderid and convert(varchar(20),officeid)in('+@debarId+') and tenderId= '+@v_fieldName2Vc
end


if @debarTypeId = 5
begin 
set @query ='select t.tenderId from tbl_TenderMaster t,tbl_tenderdetails td  where t.tenderid=td.tenderid and convert(varchar(20),departmentid) in('+@debarId+') and tenderId= '+@v_fieldName2Vc
end

--print @query
insert into @debardetail 
exec(@query)

set @rowcnt= @@rowcount
if @rowcnt >= 1
begin
Select case when @rowcnt = 0 then 'No' else 'yes' end as FieldValue1, @debarId as FieldValue2 ,
@debarTypeId as FieldValue3,REPLACE(CONVERT(VARCHAR(11),@dtFrom, 106), ' ', '-') as FieldValue4, REPLACE(CONVERT(VARCHAR(11),@dtTo, 106), ' ', '-') as FieldValue5, 
@debarComments as FieldValue6,case when @debarTypeId = 6 then 'eGP-Admin' else (select em.employeeName from tbl_EmployeeMaster em where em.userId=@hopeId) end as FieldValue7
break
end
else
begin
	SELECT 'NO' as FieldValue1
end
FETCH NEXT FROM cur_tblNames INTO @debarId,@debarTypeId,@dtFrom,@dtTo ,@debarComments,@hopeId

END
CLOSE cur_tblNames        
DEALLOCATE cur_tblNames

end


IF @v_fieldName1Vc = 'SearchCPV' -- 
BEGIN
--select * from tbl_CpvClassification c,(
--select SUBSTRING(CpvCode,1,2) scode from tbl_CpvClassification where CpvDescription like '%'+@v_fieldName2Vc+'%')cd
--where SUBSTRING(CpvCode,1,2)=scode 

IF @v_fieldName2Vc is not null And @v_fieldName2Vc<>''
Begin
	--select Distinct CONVERT(varchar(50), CpvId) as FieldValue1, 
	--CpvDescription as FieldValue2, 
	--CpvCode as FieldValue3, 
	--CONVERT(varchar(50), CpvDivision) as FieldValue4, 
	--CONVERT(varchar(50), CpvGroup) as FieldValue5 

	--from tbl_CpvClassification c,(
	--select SUBSTRING(CpvCode,1,2) scode from tbl_CpvClassification where CpvDescription like '%'+@v_fieldName2Vc+'%')cd
	--where SUBSTRING(CpvCode,1,2)=scode 
	
	select Distinct CONVERT(varchar(50), CpvId) as FieldValue1, 
	CpvDescription as FieldValue2, 
	CpvCode as FieldValue3, 
	CONVERT(varchar(50), CpvDivision) as FieldValue4, 
	CONVERT(varchar(50), CpvGroup) as FieldValue5,CpvId  from (select c.* from (select * from tbl_CpvClassification 
where CpvDescription like '%'+@v_fieldName2Vc+'%' and CpvDivision=1)a,tbl_CpvClassification c
where substring(a.CpvCode,1,2)=substring(c.CpvCode,1,2)
union
select c.* from (select * from tbl_CpvClassification 
where CpvDescription like '%'+@v_fieldName2Vc+'%' and CpvGroup=1)a,tbl_CpvClassification c
where substring(a.CpvCode,1,2)=substring(c.CpvCode,1,2)
union
select c.* from (select * from tbl_CpvClassification 
where CpvDescription like '%'+@v_fieldName2Vc+'%' and CpvClass=1)a,tbl_CpvClassification c
where substring(a.CpvCode,1,2)=substring(c.CpvCode,1,2))a 
where CpvCategory=''+@v_fieldName4Vc+'' 
order by cpvid
 
End

Else IF @v_fieldName3Vc is not null And @v_fieldName3Vc<>''
Begin
	--select Distinct CONVERT(varchar(50), CpvId) as FieldValue1, 
	--CpvDescription as FieldValue2, 
	--CpvCode as FieldValue3, 
	--CONVERT(varchar(50), CpvDivision) as FieldValue4, 
	--CONVERT(varchar(50), CpvGroup) as FieldValue5 

	--from tbl_CpvClassification c,(
	--select SUBSTRING(CpvCode,1,2) scode from tbl_CpvClassification where CpvCode =''+@v_fieldName3Vc+'')cd
	--where SUBSTRING(CpvCode,1,2)=scode 
	
	select Distinct CONVERT(varchar(50), CpvId) as FieldValue1, 
	CpvDescription as FieldValue2, 
	CpvCode as FieldValue3, 
	CONVERT(varchar(50), CpvDivision) as FieldValue4, 
	CONVERT(varchar(50), CpvGroup) as FieldValue5,CpvId,CpvCategory  from (select c.* from (select * from tbl_CpvClassification 
where CpvCode =''+@v_fieldName3Vc+'' and CpvDivision=1)a,tbl_CpvClassification c
where substring(a.CpvCode,1,2)=substring(c.CpvCode,1,2)
union
select c.* from (select * from tbl_CpvClassification 
where CpvCode =''+@v_fieldName3Vc+'' and CpvGroup=1)a,tbl_CpvClassification c
where substring(a.CpvCode,1,2)=substring(c.CpvCode,1,2)
union
select c.* from (select * from tbl_CpvClassification 
where CpvCode =''+@v_fieldName3Vc+'' and CpvClass=1)a,tbl_CpvClassification c
where substring(a.CpvCode,1,2)=substring(c.CpvCode,1,2))a
where CpvCategory=''+@v_fieldName4Vc+'' 
 order by cpvid
	
End
Else
Begin
select Distinct CONVERT(varchar(50), CpvId) as FieldValue1, 
	CpvDescription as FieldValue2, 
	CpvCode as FieldValue3, 
	CONVERT(varchar(50), CpvDivision) as FieldValue4, 
	CONVERT(varchar(50), CpvGroup) as FieldValue5,CpvId,CpvCategory from tbl_CpvClassification where CpvCategory=''+@v_fieldName4Vc+''
End



End

-- @v_fieldName1Vc -- search_Procurementforum
-- @v_fieldName2Vc -- @v_Tab
-- @v_fieldName3Vc -- @v_Keyword
-- @v_fieldName4Vc -- @v_PostedBy 
-- @v_fieldName5Vc -- @v_DateofPosting
-- @v_fieldName6Vc -- @v_UserId 
-- @v_fieldName7Vc -- @v_Page 
-- @v_fieldName8Vc -- @v_PagePerRecord

--Exec p_get_commonsearchdata 'search_Procurementforum', 'MYREPLIED', '', '', '','9','1','10',''
IF @v_fieldName1Vc = 'search_Procurementforum'
BEGIN	
	
	--Search By Keywords
	IF @v_fieldName3Vc !=NULL OR @v_fieldName3Vc <>''
	BEGIN
		Set @FinalQuery='and (subject LIKE ''%'+@v_fieldName3Vc+'%'' OR description LIKE ''%'+@v_fieldName3Vc+'%'') '		
	END
	ELSE
	BEGIN
		Set @FinalQuery=''
	END
	
	--Search By Posted By
	IF @v_fieldName4Vc !=NULL OR @v_fieldName4Vc <>''
	BEGIN
		IF @FinalQuery=''
		BEGIN
			Set @FinalQuery=@FinalQuery+'and fullName LIKE ''%'+@v_fieldName4Vc+'%'' '		
		END
		ELSE
		BEGIN
			Set @FinalQuery=@FinalQuery+'and fullName LIKE ''%'+@v_fieldName4Vc+'%'' '		
		END
	END
		
	--Search By DateofPosting
	IF @v_fieldName5Vc !=NULL OR @v_fieldName5Vc <>''
	BEGIN
		IF @FinalQuery=''
			BEGIN
				Set @FinalQuery=@FinalQuery+'and (( CAST(floor(cast([postDate] as float))as datetime)) = '''+convert(varchar(30),CAST(floor(cast(convert(datetime,@v_fieldName5Vc,105) as float))as datetime),120)+''') '
			END
		ELSE
			BEGIN
				Set @FinalQuery=@FinalQuery+'and (( CAST(floor(cast([postDate] as float))as datetime)) = '''+convert(varchar(30),CAST(floor(cast(convert(datetime,@v_fieldName5Vc,105) as float))as datetime),120)+''') '
			END
	END
		
	--Search By Status
	IF ((@v_fieldName9Vc !=NULL OR @v_fieldName9Vc <>'') and  @v_fieldName2Vc<>'MYTOPIC' and  @v_fieldName2Vc<>'MYREPLIED')
	BEGIN
		IF @FinalQuery=''
		BEGIN
			Set @FinalQuery=@FinalQuery+'and status='''+@v_fieldName9Vc+''' '
		END
		ELSE
		BEGIN
			Set @FinalQuery=@FinalQuery+'and status='''+@v_fieldName9Vc+''' '
		END
	END	
	-- new addition	
	ELSE IF ( @v_fieldName2Vc='MYREPLIED' And (@v_fieldName9Vc !=NULL OR @v_fieldName9Vc <>''))
	BEGIN
		Set @FinalQuery=@FinalQuery+'and status='''+@v_fieldName9Vc+''' '
	END
	
	ELSE 
	BEGIN
		Set @FinalQuery=@FinalQuery+' '
	END
		
	set @v_ConditionString_Vc=''	
	
	If @v_fieldName2Vc='MYTOPIC' 
	Begin
		set @v_ConditionString_Vc=' pf.ppfParentId=tbl_PublicProcureForum.ppfid '
	End	
	Else If @v_fieldName2Vc='MYREPLIED' 
	Begin
		set @v_ConditionString_Vc=' pf.ppfParentId=tbl_PublicProcureForum.ppfid  and status=''Accepted''  '
	End	
	Else
	begin
		set @v_ConditionString_Vc=' pf.ppfParentId=tbl_PublicProcureForum.ppfid   and status=''Accepted'' '
	end
		
	--Search By UserID
	IF @v_fieldName6Vc !=NULL OR @v_fieldName6Vc <>''
	BEGIN
		IF @v_fieldName2Vc='MYTOPIC' OR @v_fieldName2Vc=''
		BEGIN
			
			IF @v_fieldName2Vc='MYTOPIC'
			BEGIN
				IF @FinalQuery=''
				BEGIN
					Set @FinalQuery=@FinalQuery+'and postedBy='''+cast(@v_fieldName6Vc as varchar(20))+''' and ppfParentId =''0'' '
				END
				ELSE
				BEGIN
					Set @FinalQuery=@FinalQuery+'and postedBy='''+cast(@v_fieldName6Vc as varchar(20))+''' and ppfParentId =''0'' '
				END
			END
			ELSE
			BEGIN
				IF @FinalQuery=''
				BEGIN
					Set @FinalQuery=@FinalQuery+' and ppfParentId =''0'' '
				END
				ELSE
				BEGIN
					Set @FinalQuery=@FinalQuery+' and ppfParentId =''0'' '
				END
			END		
		END	
			
		ELSE IF @v_fieldName2Vc='MYREPLIED'
		BEGIN
			IF @FinalQuery=''
			BEGIN
				IF (@v_fieldName9Vc='' Or @v_fieldName9Vc is NULL)
				BEGIN
					--Set @FinalQuery=@FinalQuery+' and ppfParentId <>''0'''
					Set @FinalQuery=@FinalQuery+'and postedBy='''+cast(@v_fieldName6Vc as varchar(20))+''' and ppfParentId <>''0'' '
				END
				ELSE
				BEGIN
					Set @FinalQuery=@FinalQuery+'and postedBy='''+cast(@v_fieldName6Vc as varchar(20))+'''   '
				END
			END
			ELSE
			BEGIN
				IF (@v_fieldName9Vc='' Or @v_fieldName9Vc is NULL)
				BEGIN
					--Set @FinalQuery=@FinalQuery+' and ppfParentId <>''0'' '
					Set @FinalQuery=@FinalQuery+'and postedBy='''+cast(@v_fieldName6Vc as varchar(20))+''' and ppfParentId <>''0'' '
				END
				ELSE
				BEGIN
					Set @FinalQuery=@FinalQuery+'and postedBy='''+cast(@v_fieldName6Vc as varchar(20))+'''  '
				END				
			END
		END
		
		ELSE 
		BEGIN
			Set @FinalQuery=@FinalQuery+' OR ppfParentId =''0'' '
		END	
			
	END
	ELSE -- new addition
	BEGIN
		IF @v_fieldName2Vc='MYTOPIC' OR @v_fieldName2Vc=''
		BEGIN
			Set @FinalQuery=@FinalQuery+' and ppfParentId =''0'' '
		END	
			
		ELSE IF @v_fieldName2Vc='MYREPLIED'
		BEGIN
			Set @FinalQuery=@FinalQuery+' and ppfParentId <>''0'' '
		END
		
		ELSE 
		BEGIN
			Set @FinalQuery=@FinalQuery+' OR ppfParentId =''0'' '
		END	
	END
	
	
	
	IF @FinalQuery=''
	BEGIN
		SET @v_IntialQuery_Vc ='from tbl_PublicProcureForum where ppfId=tbl_PublicProcureForum.ppfId '		
	END
	ELSE
	BEGIN
		SET @v_IntialQuery_Vc= 'from tbl_PublicProcureForum where ppfId=tbl_PublicProcureForum.ppfId '
	END
	
	declare @v_ExecutingQuery_Vc as varchar(max)
			
	set @v_ExecutingQuery_Vc='DECLARE @v_Reccountf Int
			DECLARE @v_TotalPagef Int
			SELECT @v_Reccountf = Count(*) From (
			SELECT * From (SELECT ROW_NUMBER() OVER (order by ppfId desc) As Rownumber
			'+@v_IntialQuery_Vc+''+@FinalQuery+') AS DATA) AS TTT
			SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_fieldName8Vc as varchar(10))+')
			SELECT *,cast(@v_TotalPagef as varchar(20)) as FieldValue7,cast(@v_Reccountf as varchar(20)) as FieldValue8 From (SELECT cast(ROW_NUMBER() OVER (order by ppfId desc) as varchar(20)) As FieldValue1,cast([ppfId] as varchar(20)) as FieldValue2,cast([ppfparentId] as varchar(20)) as FieldValue10,substring(description,1,50) as FieldValue11,[subject] as FieldValue3,fullName as FieldValue4,cast(isnull((select  COUNT(*)  from tbl_PublicProcureForum pf where  '+@v_ConditionString_Vc+'  group by ppfParentId),0) as varchar(20)) as FieldValue5,
			--ISNULL(''By ''+ (SELECT TOP 1 fullName FROM tbl_PublicProcureForum tpf where tpf.ppfParentId = tbl_PublicProcureForum.ppfid  and status=''Accepted'' order by ppfId desc) +''  On  ''+cast((SELECT TOP 1 postDate FROM tbl_PublicProcureForum tpf where tpf.ppfParentId = tbl_PublicProcureForum.ppfid  and status=''Accepted'' order by ppfId desc) AS varchar(20))+'''',''-'') as FieldValue6,
			ISNULL(''By ''+ fullName+ '' On ''+cast(postDate AS varchar(20)),''-'') as FieldValue6,
			status as FieldValue9
			'+@v_IntialQuery_Vc+''+@FinalQuery+') AS DATA where FieldValue1 between '+cast(((cast(@v_fieldName7Vc as int) - 1) * cast(@v_fieldName8Vc as int) + 1) as varchar(10))+' 
			AND '+cast((cast(@v_fieldName7Vc as int) * cast(@v_fieldName8Vc as int)) as varchar(10))+''
			
		print @v_ExecutingQuery_Vc
		Exec(@v_ExecutingQuery_Vc)
		
		-- FieldValue1 - RowNumber
		-- FieldValue2 - ppfId
		-- FieldValue3 - subject
		-- FieldValue4 - fullName
		-- FieldValue5 - ppfParentId
		-- FieldValue6 - fullName + postDate
		-- FieldValue7 - TotalPages
		-- FieldValue8 - TotalRecords
		
END


IF @v_fieldName1Vc = 'getPreTenderPaymentConditions' 
BEGIN	

	If @v_fieldName2Vc='Document Fees'
	Begin
		--print ('df')
		Select @v_DocFeesMethod_Vc=docFeesMethod from tbl_TenderDetails where tenderId=@v_fieldName3Vc
		
		If @v_DocFeesMethod_Vc='Package wise'
		Begin
			Set @FinalQuery='Select IsNull(Convert(varchar(50), A.pkgDocFees),''0.00'') as FieldValue1,
			Case 
				When A.docEndDate > = getdate() Then ''Yes''
				Else ''No''
				End as FieldValue2,
				'''' as FieldValue3,
				'''' as FieldValue4	
			From tbl_TenderDetails A Where A.tenderId=' + @v_fieldName3Vc
		End
		Else If @v_DocFeesMethod_Vc='Lot wise'
		Begin
			Set @FinalQuery='Select IsNull(Convert(varchar(50), A.docFess ),''0.00'') as FieldValue1,
			Case 
				When B.docEndDate > = getdate() Then ''Yes''
				Else ''No''
				End as FieldValue2,
				'''' as FieldValue3,
				'''' as FieldValue4			
			From tbl_TenderLotSecurity A 
			Inner join tbl_TenderDetails B On A.TenderId=B.TenderId
			Where A.tenderId=' + @v_fieldName3Vc + ' And A.appPkgLotId='+ @v_fieldName4Vc 
		End
	End
	Else If @v_fieldName2Vc='Tender Security'
	Begin
		--print ('ts')
		--f_gettendersecuritydate
		Set @FinalQuery='Select IsNull(Convert(varchar(50), A.tenderSecurityAmt ),''0.00'') as FieldValue1,
		Case 
			When dbo.f_gettendersecuritydateForPayment(A.TenderId) > = getdate() Then ''Yes''
			Else ''no''
			End as FieldValue2,
			Case B.docFeesMethod
				When ''Package wise'' Then IsNull(Convert(varchar(50), B.pkgDocFees ),''0.00'')
				When ''Lot wise'' Then (select IsNull(Convert(varchar(50), sum(docFess)),''0.00'') From tbl_TenderLotSecurity TLS Where TLS.TenderID=B.TenderId)	 
				Else ''0.00''
				End as FieldValue3,			 
				Case B.docFeesMethod
					When ''Package wise'' Then 
						Case When Exists 
						(select tenderPaymentId from tbl_TenderPayment TP Where TP.tenderId=A.tenderId And paymentFor=''Document Fees'' And isVerified=''yes'' 
							And 
							(
								TP.userId in 								
								(
										select distinct userId from tbl_TendererMaster 
											where companyId =(select companyId from tbl_TendererMaster where userId='+@v_fieldName5Vc+' and companyId!=1)
								)  								
								Or 
								TP.userId = '+@v_fieldName5Vc+'
							)
						)	
						Then ''Paid''
						Else ''Pending''
						End
					When ''Lot wise'' Then 	
						Case When Exists 
						(select tenderPaymentId from tbl_TenderPayment TP Where TP.tenderId=A.tenderId And TP.pkgLotId=A.appPkgLotId And paymentFor=''Document Fees'' And isVerified=''yes'' 
							And 
							(
								TP.userId in 
								(select distinct userId from tbl_TendererMaster 
											where companyId =(select companyId from tbl_TendererMaster where userId='+@v_fieldName5Vc+' and companyId!=1)
								)
								Or 
								TP.userId = '+@v_fieldName5Vc+'
							)  
						)
						Then ''Paid''
						Else ''Pending''
						End
					Else ''0.00''	
				End as FieldValue4,
				IsNull(CONVERT(varchar(10), dbo.f_gettendersecuritydate('+@v_fieldName3Vc+'), 103),''null'') as FieldValue5,
				IsNull(Replace(CONVERT(varchar(20), dbo.f_gettendersecuritydate('+@v_fieldName3Vc+'), 106),'' '',''-''),''null'') as FieldValue6    
			From tbl_TenderLotSecurity A
			Inner join tbl_TenderDetails B On A.TenderId=B.TenderId			
			Where A.tenderId=' + @v_fieldName3Vc + ' And A.appPkgLotId='+ @v_fieldName4Vc 
	End
	Else If @v_fieldName2Vc='Performance Security'
	Begin
		--print ('ps')
		Set @FinalQuery='select top 1 IsNull(Convert(varchar(50), A.perfSecAmt),''0.00'') as FieldValue1,
		Case 
			When perSecSubDt > = getdate() Then ''Yes''
			Else ''No''
			End as FieldValue2,
		B.acceptRejStatus as FieldValue3,
		'''' as FieldValue4 	  			
		from tbl_noaissuedetails A 
		Inner Join tbl_NoaAcceptance B On A.noaIssueId=B.noaIssueId
		Where A.tenderId=' + @v_fieldName3Vc + ' And A.pkgLotId='+@v_fieldName4Vc+' 
		And 
		(
			A.userId in 
			(select distinct userId from tbl_TendererMaster 
											where companyId =(select companyId from tbl_TendererMaster where userId='+@v_fieldName5Vc+' and companyId!=1)
			)
			Or 
			A.userId = '+@v_fieldName5Vc+'  
		)
		Order by B.noaAcceptId desc, A.noaIssueId Desc'
		
	End
				
	--print (@FinalQuery)
	Exec (@FinalQuery)
END 


IF @v_fieldName1Vc = 'getSeekClariLinkText' 
BEGIN
	Select 
	cpQuestion as FieldValue1,
	memAnswer as FieldValue2
	From 
	tbl_EvalCpMemClarification
	Where 
	
	tenderFormId=@v_fieldName2Vc And memAnswerBy=@v_fieldName3Vc and cpQuestionBy=@v_fieldName4Vc and userId=@v_fieldName5Vc
END

If @v_fieldName1Vc='getCronJob'
begin
SELECT  [cronJobName] as FieldValue1
      ,convert(varchar(20),[cronJobStartTime],120) FieldValue2
      ,convert(varchar(20),[cronJobEndTime],120) FieldValue3
      ,[cronJobStatus] FieldValue4
      ,[cronJobMsg] FieldValue5
  FROM [tbl_CronJobs] where cronJobName=@v_fieldName2Vc and SUBSTRING(convert(varchar(10),cronJobStartTime,103),1,10) = CONVERT(varchar(10),getdate(),103)
end

If @v_fieldName1Vc='getExpiredRegCustList'
begin
select emailId as FieldValue1 from tbl_LoginMaster where 
SUBSTRING(convert(varchar(10),validUpTo,120),1,10)=@v_fieldName2Vc
end

If @v_fieldName1Vc='getLapsedRegCustList'
begin
select emailId as FieldValue1, SUBSTRING(convert(varchar(10),validUpTo,120),1,10)  from tbl_LoginMaster where
SUBSTRING(convert(varchar(10),DATEADD(year,01,validUpTo),120),1,10)=@v_fieldName2Vc
end

If @v_fieldName1Vc='getPrebidEmpMobNo'
begin
select '+880'+mobileNo as FieldValue1,CONVERT(varchar(10),t.tenderId) as FieldValue2 ,
emailId as FieldValue3,reoiRfpRefNo as FieldValue4,
REPLACE(CONVERT(VARCHAR(11),preBidStartDt, 106), ' ', '-') +' ' +Substring(CONVERT(VARCHAR(30),preBidStartDt,108),1,5)   as FieldValue5

 from tbl_TenderMaster t,tbl_AppMaster a,tbl_TenderDetails tm,tbl_EmployeeMaster em,tbl_LoginMaster lm
  where lm.userId=em.userId and 
a.appId=t.appId and t.tenderId=tm.tenderId and tenderStatus='Approved' and 
--SUBSTRING(convert(varchar(10),preBidStartDt,120),1,10)=@v_fieldName2Vc
convert(varchar(10),preBidStartDt,103)=convert(varchar(10),GETDATE()+1,103)
  and em.employeeId=a.employeeId
 end 
  
If @v_fieldName1Vc='getTenderOpenEmpMobNo'
begin
select distinct '+880'+mobileNo as FieldValue1,CONVERT(varchar(10),t.tenderId) as FieldValue2 ,
emailId as FieldValue3,reoiRfpRefNo as FieldValue4,
REPLACE(CONVERT(VARCHAR(11),t.openingDt, 106), ' ', '-') +' ' +Substring(CONVERT(VARCHAR(30),t.openingDt,108),1,5)   as FieldValue5
from tbl_TenderOpenDates t,tbl_Committee c ,tbl_CommitteeMembers cm,tbl_EmployeeMaster em,
tbl_TenderDetails td,tbl_LoginMaster lm
where t.tenderId=c.tenderId and em.userId=lm.userId and cm.userId=lm.userId
and c.committeeId=cm.committeeId and cm.userId=em.userId and
t.tenderid=td.tenderId  and committeeType in('toc','poc') and 
convert(varchar(10),t.openingDt,103)=convert(varchar(10),GETDATE()+1,103)
and tenderStatus='Approved'
 end 
  



IF @v_fieldName1Vc = 'EvalSerFormsList'
BEGIN
	
	IF @v_fieldName4Vc='yes'
	Begin 
			select distinct convert(varchar(25),a.tenderFormId )as FieldValue1 ,formName as FieldValue2,
			case isMandatory when 'yes' then 'Yes' else 'No' end as FieldValue3,convert(varchar(25),a.packageLotId )as FieldValue4
				
			from (select tenderid,packageLotId, t.tenderFormId,formName,isEncryption, isMandatory from 
			tbl_TenderForms t,tbl_TenderSection s,tbl_TenderStd std
			where t.tenderSectionId=s.tenderSectionId and s.tenderStdId=std.tenderStdId
			and tenderId=@v_fieldName2Vc 
			and (t.formStatus is Null or t.formStatus not in ('cp','c'))
			and isPriceBid='No' and s.contentType !='TOR'
			
			)a 
	End
	Else 
	Begin
			select distinct convert(varchar(25),a.tenderFormId )as FieldValue1 ,formName as FieldValue2,
			case isMandatory when 'yes' then 'Yes' else 'No' end as FieldValue3,convert(varchar(25),a.packageLotId )as FieldValue4
				
			from (select tenderid,packageLotId, t.tenderFormId,formName,isEncryption, isMandatory from 
			tbl_TenderForms t,tbl_TenderSection s,tbl_TenderStd std
			where t.tenderSectionId=s.tenderSectionId and s.tenderStdId=std.tenderStdId
			and tenderId=@v_fieldName2Vc 
			and (t.formStatus is Null or t.formStatus not in ('createp','c'))
			and isPriceBid='No' and s.contentType !='TOR'
			
			)a 
	End	
END  


--IF @v_fieldName1Vc = 'getJVCACompanyPay' 
--BEGIN
--		Select Case
--			When Exists 
--			(	select CONVERT(varchar(20),userid) as FieldValue1 
--				from tbl_TenderPayment 
--				where paymentFor=@v_fieldName2Vc And isLive='Yes' And 
--				 userId in (select userId from tbl_JVCAPartners 
--				where JVId in( select jv.JVId from tbl_JointVenture j,tbl_JVCAPartners jv
--				where j.JVId=jv.JVId and JVStatus='Approved' and userId=@v_fieldName3Vc)) 
--				and tenderId=@v_fieldName4Vc
--			)
--			Then 'Yes'
--			Else 'No'
--			End as FieldValue1,
--	 Case When @v_fieldName5Vc is Not Null And @v_fieldName5Vc<>'' And @v_fieldName5Vc<>'0'
--			Then 
--				Case				
--					When ((select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1) -- Company Case
--					Then 
--						Case 
--							When Exists		
--							(select tenderPaymentId from tbl_TenderPayment where 
--								paymentFor=@v_fieldName2Vc And isLive='Yes' And 
--								userId in (select userId from tbl_TendererMaster 
--											where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1) )  
--											and tenderId=@v_fieldName4Vc And pkgLotId=@v_fieldName5Vc
--							) 	
--							Then 'Yes'
--							Else 'No'
--							End
--						Else 'No'	
--					End
--			Else 	
--				Case				
--					When ((select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1) -- Company Case
--					Then 
--						Case 
--							When Exists		
--							(select tenderPaymentId from tbl_TenderPayment where 
--								paymentFor=@v_fieldName2Vc And isLive='Yes' And 
--								userId in (select userId from tbl_TendererMaster 
--											where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1) )  
--											and tenderId=@v_fieldName4Vc
--							) 	
--							Then 'Yes'
--							Else 'No'
--							End
--						Else 'No'	
--					End		
	   
--		End as 	FieldValue2	
--END


IF @v_fieldName1Vc = 'getJVCACompanyPay' 
BEGIN
	/*
		FieldValue1: Yes-JVCA Partner has already paid for Tender Document/Tender Security
		FieldValue2: Yes-Tender Document/Tender Security Payment has already been done for this Company / Bidder
		FieldValue2: Yes-Current User is a JVCP User
	*/
		Declare @v_isJVCA_Vc varchar(50)
		Select @v_isJVCA_Vc = isJvca from tbl_LoginMaster where userId=@v_fieldName3Vc

		Select 
			Case 
				When @v_fieldName5Vc is Not Null And @v_fieldName5Vc<>'' And @v_fieldName5Vc<>'0' -- Lot Id Case
				Then 
					Case 
						When @v_isJVCA_Vc='Yes' -- Current User is a JVCA User
						Then 
							Case
								When Exists 
								(	select CONVERT(varchar(20),userid) as FieldValue1 
									from tbl_TenderPayment 
									where paymentFor=@v_fieldName2Vc And isLive='Yes' 
									And userId in (select distinct lm.userId from tbl_JVCAPartners jp
														inner join tbl_LoginMaster lm On jp.userId=lm.userId
														where JVId in( select jv.JVId from tbl_JointVenture j,tbl_JVCAPartners jv where j.JVId=jv.JVId and JVStatus='Approved' and userId=@v_fieldName3Vc)
												  ) 
									and tenderId=@v_fieldName4Vc And pkgLotId=@v_fieldName5Vc
								)
								Then 'Yes'
								Else 'No'
							End 
						When @v_isJVCA_Vc='No' -- Current User is not a JVCA User
						Then 
							Case
								When Exists 
								(	select CONVERT(varchar(20),userid) as FieldValue1 
									from tbl_TenderPayment 
									where paymentFor=@v_fieldName2Vc And isLive='Yes' 
									And userId in (select distinct lm.userId from tbl_JVCAPartners jp
														inner join tbl_LoginMaster lm On jp.userId=lm.userId	 
														where JVId in( select jv.JVId from tbl_JointVenture j,tbl_JVCAPartners jv where j.JVId=jv.JVId and JVStatus='Approved' and userId=@v_fieldName3Vc)
														and lm.isJvca='Yes'
												  ) 
									and tenderId=@v_fieldName4Vc And pkgLotId=@v_fieldName5Vc
								)
								Then 'Yes'
								Else 'No'
							End
						End	 	
				Else -- No Lot Id Case
					Case 
						When @v_isJVCA_Vc='Yes' -- Current User is a JVCA User
						Then
							Case
								When Exists 
								(	select CONVERT(varchar(20),userid) as FieldValue1 
									from tbl_TenderPayment 
									where paymentFor=@v_fieldName2Vc And isLive='Yes' 
									And userId in (select distinct lm.userId from tbl_JVCAPartners jp 
														inner join tbl_LoginMaster lm On jp.userId=lm.userId	 
														where JVId in( select jv.JVId from tbl_JointVenture j,tbl_JVCAPartners jv where j.JVId=jv.JVId and JVStatus='Approved' and userId=@v_fieldName3Vc)
												) 
									and tenderId=@v_fieldName4Vc 
								)
								Then 'Yes'
								Else 'No'
							End 
						When @v_isJVCA_Vc='No' -- Current User is not a JVCA User
						Then 		
							Case
								When Exists 
								(	select CONVERT(varchar(20),userid) as FieldValue1 
									from tbl_TenderPayment 
									where paymentFor=@v_fieldName2Vc And isLive='Yes' 
									And userId in (select distinct lm.userId from tbl_JVCAPartners jp 
														inner join tbl_LoginMaster lm On jp.userId=lm.userId	 
														where JVId in( select jv.JVId from tbl_JointVenture j,tbl_JVCAPartners jv where j.JVId=jv.JVId and JVStatus='Approved' and userId=@v_fieldName3Vc)
														and lm.isJvca='Yes'
												)		
									and tenderId=@v_fieldName4Vc 
								)
								Then 'Yes'
								Else 'No'
							End 
					End	
			End as FieldValue1,
	 Case When @v_fieldName5Vc is Not Null And @v_fieldName5Vc<>'' And @v_fieldName5Vc<>'0'
			Then 
				Case				
					When ((select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1) -- Company Case
					Then 
						Case 
							When Exists		
							(select tenderPaymentId from tbl_TenderPayment where 
								paymentFor=@v_fieldName2Vc And isLive='Yes' And 
								userId in (select distinct userId from tbl_TendererMaster 
											where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1) )  
											and tenderId=@v_fieldName4Vc And pkgLotId=@v_fieldName5Vc
							) 	
							Then 'Yes'
							Else 'No'
							End
						Else 'No'	
					End
			Else 	
				Case				
					When ((select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1) -- Company Case
					Then 
						Case 
							When Exists		
							(select tenderPaymentId from tbl_TenderPayment where 
								paymentFor=@v_fieldName2Vc And isLive='Yes' And 
								userId in (select distinct userId from tbl_TendererMaster 
											where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1) )  
											and tenderId=@v_fieldName4Vc
							) 	
							Then 'Yes'
							Else 'No'
							End
						Else 'No'	
					End		
	   
		End as 	FieldValue2,
		@v_isJVCA_Vc as FieldValue3	
END



IF @v_fieldName1Vc = 'getformnamebytenderidandlotid_ForLotPrep'   -- For tenderer/BidPreperation.jsp -- Kinjal
BEGIN

	IF @v_fieldName3Vc <> '0' --- LOT CASE
	BEGIN
		if @v_fieldName4Vc='Technical'
		begin
			SELECT formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,formStatus as FieldValue6,
			'' as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9
			FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
			ON tf.tenderSectionId = ts.tenderSectionId Inner Join
			dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId
			Where td.tenderId = @v_fieldName2Vc and packageLotId = @v_fieldName3Vc
			And isPriceBid='No' and (tf.formStatus is Null or tf.formStatus not in ('createp'))
			Order By isPriceBid
		end
		else if @v_fieldName4Vc='Pricebid'
		begin
			If (select COUNT(tenderLotId) from tbl_TenderLots where tenderId=@v_fieldName2Vc) > 1 -- Multiple Lot Case
			Begin
				SELECT formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,formStatus as FieldValue6, 
				'' as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9, FormType as FieldValue11
				FROM dbo.tbl_TenderForms tf 
				Inner Join dbo.tbl_TenderSection ts	ON tf.tenderSectionId = ts.tenderSectionId 
				Inner Join dbo.tbl_TenderStd td On ts.tenderStdId = td.tenderStdId
				--Inner Join tbl_TenderLotSecurity tls On td.tenderId=tls.tenderId
				Inner Join dbo.tbl_bidderLots bl On bl.tenderId=td.tenderId And bl.pkgLotId=tf.pkgLotId
				Where td.tenderId = @v_fieldName2Vc and packageLotId = @v_fieldName3Vc
				And isPriceBid='Yes' and (tf.formStatus is Null or tf.formStatus not in ('createp'))
				And bl.userId=@v_fieldName5Vc
				And tf.pkgLotId = @v_fieldName6Vc
				Order By isPriceBid	
			End
			Else If (select COUNT(tenderLotId) from tbl_TenderLots where tenderId=@v_fieldName2Vc) = 1
			Begin
				SELECT formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,formStatus as FieldValue6, 
				'' as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9, FormType as FieldValue11
				FROM dbo.tbl_TenderForms tf 
				Inner Join dbo.tbl_TenderSection ts	ON tf.tenderSectionId = ts.tenderSectionId 
				Inner Join dbo.tbl_TenderStd td On ts.tenderStdId = td.tenderStdId				
				Where td.tenderId = @v_fieldName2Vc and packageLotId = @v_fieldName3Vc
				And isPriceBid='Yes' and (tf.formStatus is Null or tf.formStatus not in ('createp'))				
				Order By isPriceBid	
			End
			
		end	
	END
	ELSE --- PACKAGE CASE
	BEGIN
		if @v_fieldName4Vc='Technical'
		begin
			--SELECT formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,formStatus as FieldValue6, 
			--'' as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9
			--FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
			--ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
			--dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 
			--Where td.tenderId = @v_fieldName2Vc
			--And isPriceBid='No'
			--Order By isPriceBid
			
			
									select a.*,isEncryption as FieldValue2,
						case when b.tenderFormId IS null then 'No' else 'Yes' end FieldValue7,
						case when isEncryption='Yes' then case when c.tenderFormId IS null then 'No' else 'Yes' end else '-' END as  FieldValue11,
						newFormCombinedHash as FieldValue10
						 from 
						(SELECT formName as FieldValue1, isEncryption  , isPriceBid as FieldValue3, 
						isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,formStatus as FieldValue6, 
										  isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9
										FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
									ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
									dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 
									Where td.tenderId = @v_fieldName2Vc
									And isPriceBid='No'  and (tf.formStatus is Null or tf.formStatus not in ('createp'))
										)a left outer join 
						(select distinct tenderFormId from tbl_TenderBidForm where tenderId =@v_fieldName2Vc and userId=@v_fieldName5Vc )b
						on a.FieldValue5=b.tenderFormId
						left outer join 
						(select distinct tenderFormId,case when @v_fieldName7Vc='Bid' then '0' else newFormCombinedHash end as newFormCombinedHash from tbl_FinalSubmission f,tbl_FinalSubDetail fd 
						where f.finalSubmissionId=fd.finalSubmissionId and  tenderId =@v_fieldName2Vc  and userId=@v_fieldName5Vc)c
						on a.FieldValue5=c.tenderFormId
						left outer join 
						(select distinct tenderFormId  from tbl_TenderBidSign s,tbl_TenderBidForm b
						 where tenderId =@v_fieldName2Vc and userId=@v_fieldName5Vc and b.bidId=s.bidId )d
						on a.FieldValue5=d.tenderFormId				
										Order By FieldValue5
		end
		else if @v_fieldName4Vc='Pricebid'
		begin
			If (select COUNT(tenderLotId) from tbl_TenderLots where tenderId=@v_fieldName2Vc) > 1 -- Multiple Lot Case
			Begin
--				SELECT formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,formStatus as FieldValue6, 
--				'' as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9
--				FROM dbo.tbl_TenderForms tf 
--				Inner Join dbo.tbl_TenderSection ts	ON tf.tenderSectionId = ts.tenderSectionId 
--				Inner Join dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 
----				Inner Join tbl_TenderLotSecurity tls On td.tenderId=tls.tenderId
--				Inner Join dbo.tbl_bidderLots bl On bl.tenderId=td.tenderId And bl.pkgLotId=tf.pkgLotId
--				Where td.tenderId = @v_fieldName2Vc
--				And isPriceBid='Yes'
--				And bl.userId=@v_fieldName5Vc
--				And tf.pkgLotId = @v_fieldName6Vc
--				Order By isPriceBid

							select a.*,isEncryption as FieldValue2,
							case when b.tenderFormId IS null then 'No' else 'Yes' end FieldValue7,
							case when isEncryption='Yes' then case when c.tenderFormId IS null then 'No' else 'Yes' end else '-' END as FieldValue11,
							newFormCombinedHash as FieldValue10, /* Dohatec Start */ FormType as FieldValue12 /* Dohatec End */
							 from 
							(SELECT formName as FieldValue1, isEncryption  , isPriceBid as FieldValue3, 
							isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,formStatus as FieldValue6, 
											  isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9,FormType
											FROM dbo.tbl_TenderForms tf
											Inner Join dbo.tbl_TenderSection ts	ON tf.tenderSectionId = ts.tenderSectionId
											Inner Join dbo.tbl_TenderStd td On ts.tenderStdId = td.tenderStdId
											--Inner Join tbl_TenderLotSecurity tls On td.tenderId=tls.tenderId
											Inner Join dbo.tbl_bidderLots bl On bl.tenderId=td.tenderId And bl.pkgLotId=tf.pkgLotId
											Where td.tenderId = @v_fieldName2Vc
											And isPriceBid='Yes'  and (tf.formStatus is Null or tf.formStatus not in ('createp'))
											And bl.userId=@v_fieldName5Vc
											And tf.pkgLotId = @v_fieldName6Vc
											)a left outer join 
							(select distinct tenderFormId from tbl_TenderBidForm where tenderId =@v_fieldName2Vc and userId=@v_fieldName5Vc )b
							on a.FieldValue5=b.tenderFormId
							left outer join 
							(select distinct tenderFormId,case when @v_fieldName7Vc='Bid' then '0' else newFormCombinedHash end as newFormCombinedHash from tbl_FinalSubmission f,tbl_FinalSubDetail fd 
							where f.finalSubmissionId=fd.finalSubmissionId and  tenderId =@v_fieldName2Vc  and userId=@v_fieldName5Vc)c
							on a.FieldValue5=c.tenderFormId
							left outer join 
							(select distinct tenderFormId from tbl_TenderBidSign s,tbl_TenderBidForm b
							 where tenderId =@v_fieldName2Vc and userId=@v_fieldName5Vc and b.bidId=s.bidId )d
							on a.FieldValue5=d.tenderFormId				
											Order By FieldValue5
			End
			Else If (select COUNT(tenderLotId) from tbl_TenderLots where tenderId=@v_fieldName2Vc) = 1
			Begin
			
								select a.*,isEncryption as FieldValue2,
					case when b.tenderFormId IS null then 'No' else 'Yes' end FieldValue7,
					case when isEncryption='Yes' then case when c.tenderFormId IS null then 'No' else 'Yes' end else '-' END as FieldValue11,
					newFormCombinedHash as FieldValue10, /* Dohatec Start */ FormType as FieldValue12 /* Dohatec End */
					 from 
					(SELECT formName as FieldValue1, isEncryption  , isPriceBid as FieldValue3, 
					isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,formStatus as FieldValue6, 
									  isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9,FormType
									FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
								ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
								dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 
								Where td.tenderId = @v_fieldName2Vc
								And isPriceBid='Yes'  and (tf.formStatus is Null or tf.formStatus not in ('createp'))
									)a left outer join 
					(select distinct tenderFormId from tbl_TenderBidForm where tenderId =@v_fieldName2Vc and userId=@v_fieldName5Vc )b
					on a.FieldValue5=b.tenderFormId
					left outer join 
					(select distinct tenderFormId,case when @v_fieldName7Vc='Bid' then '0' else newFormCombinedHash end as newFormCombinedHash from tbl_FinalSubmission f,tbl_FinalSubDetail fd 
					where f.finalSubmissionId=fd.finalSubmissionId and  tenderId =@v_fieldName2Vc  and userId=@v_fieldName5Vc)c
					on a.FieldValue5=c.tenderFormId
					left outer join 
					(select distinct tenderFormId from tbl_TenderBidSign s,tbl_TenderBidForm b
					 where tenderId =@v_fieldName2Vc and userId=@v_fieldName5Vc and b.bidId=s.bidId )d
					on a.FieldValue5=d.tenderFormId				
									Order By FieldValue5
				--SELECT formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,formStatus as FieldValue6, 
				--'' as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9
				--FROM dbo.tbl_TenderForms tf 
				--Inner Join dbo.tbl_TenderSection ts ON tf.tenderSectionId = ts.tenderSectionId 
				--Inner Join dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 				
				--Where td.tenderId = @v_fieldName2Vc
				--And isPriceBid='Yes'
				--Order By isPriceBid
			End
		
		
		end
		
	END
		
END

IF @v_fieldName1Vc = 'getlotid_lotdesc'     -- For tenderer/BidPreperation.jsp -- Kinjal
BEGIN
	If (select COUNT(tenderLotId) from tbl_TenderLots where tenderId=@v_fieldName2Vc) > 1 
	begin
		select CONVERT(varchar(50),ttl.appPkgLotId) as FieldValue1,ttl.lotDesc as FieldValue2,'multiple' as FieldValue3 ,ttl.lotNo as FieldValue4, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue5 from tbl_TenderLotSecurity ttl,tbl_BidderLots tbl
		where ttl.tenderId=tbl.tenderId and ttl.tenderId=@v_fieldName2Vc
		and ttl.appPkgLotId=tbl.pkgLotId and tbl.userId=@v_fieldName3Vc
		end
	else
	begin 
		select CONVERT(varchar(50),ttl.appPkgLotId) as FieldValue1,ttl.lotDesc as FieldValue2,'single' as FieldValue3,ttl.lotNo as FieldValue4,CONVERT(VARCHAR(50), appPkgLotId) as FieldValue5 from tbl_TenderLotSecurity ttl
		where  ttl.tenderId=@v_fieldName2Vc	
	end
	
end

IF @v_fieldName1Vc = 'ChkTodoFinalSubmission'     -- For LotTendPrep.jsp : Final Sub Button Condition
BEGIN
	BEGIN TRY
		BEGIN TRAN
/***
@v_fieldName1Vc: Method Name
@v_fieldName2Vc: TenderId
@v_fieldName3Vc: UserId
@v_fieldName4Vc: IP ADDRESS

**/

set   @v_userEncCnt=0

--select @v_userLotSelectedCnt=0, @v_userLotTenSecPayCnt=0

/* Start : Code To Check Bid Submission for Same Company*/
select @v_cntSameCmpUser = count(finalSubmissionId) 
FROM tbl_FinalSubmission tfs 
INNER JOIN tbl_TendererMaster tm ON tfs.userId = tm.userId 
and tenderId = @v_fieldName2Vc 
and bidSubStatus = 'finalsubmission' 
And (tfs.userId in (
	select userId from tbl_TendererMaster 
		where companyId =
		(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1))
		or tfs.userId =@v_fieldName3Vc)
		 
 
/* End */

-- Count of Lots Filled by Bidder
Select @v_userLotSelectedCnt=Count(tls.appPkgLotId) 
from tbl_TenderLotSecurity tls
Inner Join tbl_bidderLots tbl On tls.tenderId=tbl.tenderId And tls.appPkgLotId=tbl.pkgLotId
Where tbl.tenderId=@v_fieldName2Vc And tbl.userId=@v_fieldName3Vc

-- Check Whether Tender Security is Payable OR Not
select @v_isTenSecPayable=
Case When SUM(tenderSecurityAmt) > 0 Then 1 Else 0 End 
from tbl_TenderLotSecurity 
where tenderId=@v_fieldName2Vc

-- Count of Lots for which Tender Security has been paid by Bidder
Select @v_userLotTenSecPayCnt=Count(tls.appPkgLotId) 
from tbl_TenderLotSecurity tls
Inner Join tbl_bidderLots tbl On tls.tenderId=tbl.tenderId And tls.appPkgLotId=tbl.pkgLotId
Inner Join tbl_TenderPayment tp On tbl.tenderId=tp.tenderId And tbl.pkgLotId=tp.pkgLotId
Where tbl.tenderId=@v_fieldName2Vc 
And (tbl.userId in 
(select distinct userId from tbl_TendererMaster 
											where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1) ) or  tbl.userId=@v_fieldName3Vc    )
And (tp.userId in 
(select distinct userId from tbl_TendererMaster 
											where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1) ) or  tp.userId=@v_fieldName3Vc   )

And paymentFor='Tender Security'
And isVerified='yes'



select @v_BidTenderManDocCnt_Int = count(distinct tenderFormDocId)
from tbl_TenderMandatoryDoc tmd
inner join tbl_TenderBidForm bd on tmd.tenderFormId=bd.tenderFormId and tmd.tenderId=bd.tenderId
where bd.tenderId=@v_fieldName2Vc
      and bd.userId=@v_fieldName3Vc	
      
select @v_BiderFilledTenderManDocCnt_Int = count(distinct manDocId)
from tbl_BidDocuments
where manDocId<>0
	and companyDocId<>0
	and tenderId=@v_fieldName2Vc
	and userId=@v_fieldName3Vc


IF (SELECT TOP 1 tenderStatus FROM tbl_TenderDetails WHERE tenderid = @v_fieldName2Vc)!='cancelled'
BEGIN

IF EXISTS (SELECT TOP 1 submissionDt FROM tbl_TenderDetails WHERE tenderid = @v_fieldName2Vc and submissiondt >= convert(smalldatetime, GETDATE()))
-- SUBMISSION Time NOT over
BEGIN

/** BEGIN Code : CHECK CONDITION FOR JVCA PARTNER **/

	
--SELECT @v_jvcaCmpId = companyId
--FROM tbl_LoginMaster lm INNER JOIN
--     tbl_TendererMaster tm ON lm.userId = tm.userId AND lm.userId = tm.userId
--     Where isjvca = 'yes' and lm.userId  = @v_fieldName3Vc and lm.status = 'approved'
--print @v_jvcaCmpId
--SELECT @v_cntjvcaCmpUser = count(finalSubmissionId) FROM tbl_FinalSubmission tfs INNER JOIN tbl_TendererMaster tm  ON tfs.userId = tm.userId and tenderId = @v_fieldName2Vc and bidSubStatus = 'finalsubmission' and companyId in 
--     ( SELECT jvCompanyId FROM tbl_CompanyJointVenture WHERE companyId =@v_jvcaCmpId )
--/** END Code : CHECK CONDITION FOR JVCA PARTNER  **/
--print @v_cntjvcaCmpUser
set @v_cntjvcaCmpUser=0
IF (SELECT isJvca  FROM tbl_FinalSubmission f,tbl_LoginMaster l WHERE 
 f.userId=l.userId and 
tenderid = @v_fieldName2Vc and f.userId= @v_fieldName3Vc 
and bidSubStatus = 'finalsubmission'   ) = 'yes'
BEGIN
print '11'
select     @v_cntjvcaCmpUser = count(finalSubmissionId) 
FROM tbl_FinalSubmission tfs 
INNER JOIN tbl_TendererMaster tm 
 ON tfs.userId = tm.userId and tenderId = @v_fieldName2Vc 
 and bidSubStatus = 'finalsubmission' 
 
 And tfs.userId in (select userid from tbl_TendererMaster where companyId in( select distinct lm.companyId from tbl_JVCAPartners jp
														inner join tbl_TendererMaster lm On jp.userId=lm.userId
														where JVId in( select jv.JVId from tbl_JointVenture j,tbl_JVCAPartners jv where j.JVId=jv.JVId and JVStatus='Approved' and userId=@v_fieldName3Vc)
												  )
												  			union
									select userId from tbl_TendererMaster where companyId in( select companyId from tbl_tenderermaster where userid in(select newJVUserId from tbl_JointVenture j,tbl_JVCAPartners jv where j.JVId=jv.JVId and JVStatus='Approved' and userId=@v_fieldName3Vc))			    
												  
												  ) 
 
End
else
begin
print '12'
IF (SELECT isJvca  FROM  tbl_LoginMaster l WHERE 
 
  userId= @v_fieldName3Vc 
   ) = 'yes'
begin
select @v_cntjvcaCmpUser = count(finalSubmissionId) 
									from tbl_FinalSubmission 
									where bidSubStatus = 'finalsubmission' 
									And userId in (select userid from tbl_tenderermaster where companyid in(select distinct lm.companyId from tbl_JVCAPartners jp
														inner join tbl_TendererMaster lm On jp.userId=lm.userId	 
														where JVId in( select jv.JVId from tbl_JointVenture j,tbl_JVCAPartners jv where j.JVId=jv.JVId and JVStatus='Approved' and newJVUserId=@v_fieldName3Vc)
														
												  )
												union
									select userId from tbl_TendererMaster where companyId in( select companyId from tbl_tenderermaster where userid in(select newJVUserId from tbl_JointVenture j,tbl_JVCAPartners jv where j.JVId=jv.JVId and JVStatus='Approved' and newJVUserId=@v_fieldName3Vc))			    			  
												  
												  
												  ) 
									and tenderId=@v_fieldName2Vc
end
else
begin
select     @v_cntjvcaCmpUser = count(finalSubmissionId) 
FROM tbl_FinalSubmission tfs 
INNER JOIN tbl_TendererMaster tm 
 ON tfs.userId = tm.userId and tenderId = @v_fieldName2Vc 
 and bidSubStatus = 'finalsubmission' 
 
 And tfs.userId in ( 
									select userId from tbl_TendererMaster where companyId in( select companyId from tbl_tenderermaster where userid in(select newJVUserId from tbl_JointVenture j,tbl_JVCAPartners jv where j.JVId=jv.JVId and JVStatus='Approved' and userId=@v_fieldName3Vc))			    
												  
												  ) 
end									
end
print @v_cntjvcaCmpUser
IF @v_cntjvcaCmpUser = 0 --- IF NO JVCA PARTNER HAS DONE

BEGIN

print 'JVCA LOOP'
/** BEGIN Code : GET (_) saperated HASH values **/
	
	Select @v_signTxtList_Vc = COALESCE(@v_signTxtList_Vc+'_' , '') + signText
from tbl_tenderbidsign tbs, tbl_TenderBidForm tbf 
where tbs.bidId = tbf.bidId and tenderId = @v_fieldName2Vc and userId =@v_fieldName3Vc
print @v_signTxtList_Vc
/** END Code : GET (_) saperated HASH values **/


	
--Tender Mandatory Count
SELECT  @v_tenderMndCnt = count(distinct tbl_TenderForms.tenderFormId) 
FROM tbl_TenderSection 
INNER JOIN tbl_TenderStd ON tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId 
	AND tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId 
INNER JOIN tbl_TenderForms ON tbl_TenderSection.tenderSectionId = tbl_TenderForms.tenderSectionId and isnull(formStatus,'a') !='c'
WHERE tenderId = @v_fieldName2Vc and isMandatory='Yes'
And (tbl_TenderForms.pkgLotId=0 Or tbl_TenderForms.pkgLotId in 
		(select distinct pkgLotId from tbl_bidderLots Where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc)
	)              
print 'mancount'+convert(varchar(20),@v_tenderMndCnt)
IF @v_tenderMndCnt > 0
BEGIN
--Bidder Filled Mandatory Count 	
		SELECT @v_userMndCnt = count(distinct tbl_TenderForms.tenderFormId) 
		FROM tbl_TenderSection 
		INNER JOIN tbl_TenderStd ON tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId 
			AND tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId 
         INNER JOIN tbl_TenderForms ON tbl_TenderSection.tenderSectionId = tbl_TenderForms.tenderSectionId and isnull(formStatus,'a') !='c'
         inner join tbl_TenderBidForm on tbl_TenderBidForm.tenderId=tbl_TenderStd.tenderId
                      and tbl_TenderBidForm.tenderFormId=tbl_TenderForms.tenderFormId						
		WHERE tbl_TenderBidForm.tenderId = @v_fieldName2Vc and tbl_TenderBidForm.userId=@v_fieldName3Vc 
		and isMandatory='Yes'
		And (tbl_TenderForms.pkgLotId=0 Or tbl_TenderForms.pkgLotId in 
		(select distinct pkgLotId from tbl_bidderLots Where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc)
	)   
		print @v_userMndCnt
		IF @v_userMndCnt>= @v_tenderMndCnt  --- compare tender mndCnt and user mndcount
		BEGIN
		---Tender Encryption forms count
			SELECT  @v_tenderEncCnt = count(distinct tbl_TenderForms.tenderFormId) 
			FROM tbl_TenderSection 
			INNER JOIN tbl_TenderStd ON tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId 
			AND tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId 
			INNER JOIN tbl_TenderForms ON tbl_TenderSection.tenderSectionId = tbl_TenderForms.tenderSectionId	and isnull(formStatus,'a') !='c'					inner join tbl_TenderBidForm on tbl_TenderBidForm.tenderId=tbl_TenderStd.tenderId
                      and tbl_TenderBidForm.tenderFormId=tbl_TenderForms.tenderFormId	
			WHERE tbl_TenderBidForm.tenderId = @v_fieldName2Vc and tbl_TenderBidForm.userId=@v_fieldName3Vc 
			and isEncryption='Yes' 
			And (tbl_TenderForms.pkgLotId=0 Or tbl_TenderForms.pkgLotId in 
					(select distinct pkgLotId from tbl_bidderLots Where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc)
				)   
				--print 'TENDER ENC CNT : ' + convert(varchar(10),@v_tenderEncCnt	)
			print @v_tenderEncCnt		
			IF @v_tenderEncCnt > 0
			BEGIN
			--Bidder Encrypted forms count
				SELECT @v_userEncCnt= count(distinct tbl_TenderForms.tenderFormId) 
				FROM  tbl_TenderSection INNER JOIN
                      tbl_TenderStd ON tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId AND 
                      tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId INNER JOIN
                      tbl_TenderForms ON tbl_TenderSection.tenderSectionId = tbl_TenderForms.tenderSectionId and isnull(formStatus,'a') !='c'
                      inner join tbl_TenderBidForm on tbl_TenderBidForm.tenderId=tbl_TenderStd.tenderId
                      and tbl_TenderBidForm.tenderFormId=tbl_TenderForms.tenderFormId
                      INNER JOIN tbl_FinalSubmission on tbl_FinalSubmission.tenderId=tbl_TenderBidForm.tenderId and tbl_FinalSubmission.userid=tbl_TenderBidForm.userid inner join tbl_FinalSubDetail 
                      on tbl_FinalSubDetail.finalSubmissionId=tbl_FinalSubmission.finalSubmissionId		
                       and tbl_FinalSubDetail.tenderFormid=tbl_TenderBidForm.tenderFormId
				  WHERE tbl_TenderBidForm.tenderId = @v_fieldName2Vc and tbl_TenderBidForm.userId=@v_fieldName3Vc
				   and isEncryption='Yes'
						And (tbl_TenderForms.pkgLotId=0 Or tbl_TenderForms.pkgLotId in 
							(select distinct pkgLotId from tbl_bidderLots Where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc)
						)   
			END
		 
		--print 'BIDDER ENC CNT : ' + convert(varchar(10),@v_userEncCnt	)
		print  @v_userEncCnt
			IF  @v_userEncCnt >= @v_tenderEncCnt --- compare tender ENC Cnt and user ENC count
			BEGIN
				/** BEGIN ----------------------- FINAL SUBMISSION --------------------------********/
    	--print 'IN @v_userEncCnt >= @v_tenderEncCnt LOOP'
    	/********************* Chk ENTRY Exists / Not In tbl_FinalSubmission ****************************/
	
		IF @v_BiderFilledTenderManDocCnt_Int >= @v_BidTenderManDocCnt_Int  
		BEGIN
		IF EXISTS(SELECT finalSubmissionId FROM tbl_FinalSubmission WHERE tenderId = @v_fieldName2Vc  and userId = @v_fieldName3Vc)
		BEGIN --- Exists
			print 'FInal 1 : '	
			--Select @v_finalSubId_int = finalSubmissionId 
			--FROM tbl_FinalSubmission WHERE tenderId = @v_fieldName2Vc  and userId = @v_fieldName3Vc
			/********************* ENTRY Exists/Not IN tbl_FinalSubDetail ****************************/			
				If @v_cntSameCmpUser>0
				Begin
					Select '0' as FieldValue1, 'Final Submission already done for this Company' as FieldValue2
				End
				Else
				Begin
					If (@v_isTenSecPayable=1) And (@v_userLotTenSecPayCnt < @v_userLotSelectedCnt)
					Begin
						Select '0' as FieldValue1, 'Tender Security Payment is Pending' as FieldValue2							
					End
					Else
					Begin
						Select '1' as FieldValue1, 'Allow Final Submission' as FieldValue2							
					End
				End
				
				
				--Select '1' as FieldValue1, 'Allow Final Submission' as FieldValue2							
		END
		ELSE --- NOT EXISTS in tbl_FinalSubmission, insert in it
		BEGIN
			
			If @v_cntSameCmpUser>0
			Begin
				Select '0' as FieldValue1, 'Final Submission already done for this Company' as FieldValue2
			End
			Else
			Begin					
				If (@v_isTenSecPayable=1) And (@v_userLotTenSecPayCnt < @v_userLotSelectedCnt)
				Begin
					Select '0' as FieldValue1, 'Tender Security Payment is Pending' as FieldValue2							
				End
				Else
				Begin
					Select '1' as FieldValue1, 'Allow Final Submission' as FieldValue2
				End
			End
		
		END					
    	/** END ----------------------- FINAL SUBMISSION --------------------------********/
    	
    	END
    	ELSE
    	BEGIN
    		SELECT '0' as FieldValue1,'Please Fill All Mandatory Documents' as FieldValue2
    	END
    	
			END
			else
					begin
						SELECT '0' as FieldValue1,'Filled BOQ/Price Bid Form Not Encrypted Using Buyer''s Hash.' as FieldValue2
					end								
		END
		
		Else
		Begin
		SELECT  '0' as FieldValue1, 'Please fill Mandatory forms and Map Supporting / Reference
Documents as requested' as FieldValue2
		End
END
ELSE
BEGIN
	print 'Not mandatory 1 : '	
	-- User has filled up forms
	SELECT @v_userfilCnt = count(distinct tbl_TenderForms.tenderFormId) 
		FROM tbl_TenderSection 
		INNER JOIN tbl_TenderStd ON tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId 
			AND tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId 
		INNER JOIN tbl_TenderForms ON tbl_TenderSection.tenderSectionId = tbl_TenderForms.tenderSectionId and isnull(formStatus,'a') !='c'
        inner join tbl_TenderBidForm on tbl_TenderBidForm.tenderId=tbl_TenderStd.tenderId
                and tbl_TenderBidForm.tenderFormId=tbl_TenderForms.tenderFormId						
		WHERE tbl_TenderBidForm.tenderId = @v_fieldName2Vc and tbl_TenderBidForm.userId=@v_fieldName3Vc	
		And (tbl_TenderForms.pkgLotId=0 Or tbl_TenderForms.pkgLotId in 
				(select distinct pkgLotId from tbl_bidderLots Where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc)
			)   	
-- Filled Forms Count > 1
IF @v_userfilCnt >= 1
BEGIN

	--Count of encrypted filled forms by User
	SELECT @v_tenderEncfilCnt = count(distinct tbl_TenderForms.tenderFormId) 
		FROM  tbl_TenderSection 
		INNER JOIN tbl_TenderStd ON tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId 
			AND tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId 
		INNER JOIN tbl_TenderForms ON tbl_TenderSection.tenderSectionId = tbl_TenderForms.tenderSectionId and isnull(formStatus,'a') !='c'
        inner join tbl_TenderBidForm on tbl_TenderBidForm.tenderId=tbl_TenderStd.tenderId
            and tbl_TenderBidForm.tenderFormId=tbl_TenderForms.tenderFormId						
		WHERE tbl_TenderBidForm.tenderId = @v_fieldName2Vc and tbl_TenderBidForm.userId=@v_fieldName3Vc and isEncryption='Yes'		
		And (tbl_TenderForms.pkgLotId=0 Or tbl_TenderForms.pkgLotId in 
				(select distinct pkgLotId from tbl_bidderLots Where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc)
			)  
		
		
     print   @v_tenderEncfilCnt  
	-- count of bidder encrypted forms	-- 
	SELECT @v_userEncfilCnt= count(distinct tbl_TenderForms.tenderFormId) 
		FROM  tbl_TenderSection 
		INNER JOIN tbl_TenderStd ON tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId 
			AND tbl_TenderSection.tenderStdId = tbl_TenderStd.tenderStdId 
		INNER JOIN tbl_TenderForms ON tbl_TenderSection.tenderSectionId = tbl_TenderForms.tenderSectionId and isnull(formStatus,'a') !='c'
        inner join tbl_TenderBidForm on tbl_TenderBidForm.tenderId=tbl_TenderStd.tenderId
            and tbl_TenderBidForm.tenderFormId=tbl_TenderForms.tenderFormId
        INNER JOIN tbl_FinalSubmission on tbl_FinalSubmission.tenderId=tbl_TenderBidForm.tenderId 
			and tbl_FinalSubmission.userid=tbl_TenderBidForm.userid 
		inner join tbl_FinalSubDetail on tbl_FinalSubDetail.finalSubmissionId=tbl_FinalSubmission.finalSubmissionId	
            and tbl_FinalSubDetail.tenderFormid=tbl_TenderBidForm.tenderFormId
       WHERE tbl_TenderBidForm.tenderId = @v_fieldName2Vc and tbl_TenderBidForm.userId=@v_fieldName3Vc  and isEncryption='Yes'	
        And (tbl_TenderForms.pkgLotId=0 Or tbl_TenderForms.pkgLotId in 
				(select distinct pkgLotId from tbl_bidderLots Where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc)
			)  
                 
    IF @v_userEncfilCnt >= @v_tenderEncfilCnt
    	/** BEGIN ----------------------- FINAL SUBMISSION --------------------------********/
    	
    	IF @v_BiderFilledTenderManDocCnt_Int >= @v_BidTenderManDocCnt_Int 
    	BEGIN
    	
    	/********************* Chk ENTRY Exists / Not In tbl_FinalSubmission ****************************/
	    IF EXISTS(SELECT finalSubmissionId FROM tbl_FinalSubmission WHERE tenderId = @v_fieldName2Vc  and userId = @v_fieldName3Vc)
		BEGIN --- Exists
		print 'tbl_FinalSubmission 2 : '	
			Select @v_finalSubId_int = finalSubmissionId FROM tbl_FinalSubmission WHERE tenderId = @v_fieldName2Vc  and userId = @v_fieldName3Vc
			/********************* ENTRY Exists/Not IN tbl_FinalSubDetail ****************************/
				
			If @v_cntSameCmpUser>0
			Begin
				Select '0' as FieldValue1, 'Final Submission already done for this Company' as FieldValue2
			End
			Else
			Begin		
				If (@v_isTenSecPayable=1) And (@v_userLotTenSecPayCnt < @v_userLotSelectedCnt)
				Begin
					Select '0' as FieldValue1, 'Tender Security Payment is Pending' as FieldValue2							
				End
				Else
				Begin
					Select '1' as FieldValue1, 'Allow Final Submission' as FieldValue2
				End	
			End
				
				
				
			--END			
		END
		ELSE --- NOT EXISTS in tbl_FinalSubmission, insert in it
		BEGIN
			If @v_cntSameCmpUser>0
			Begin
				Select '0' as FieldValue1, 'Final Submission already done for this Company' as FieldValue2
			End
			Else
			Begin		
				If (@v_isTenSecPayable=1) And (@v_userLotTenSecPayCnt < @v_userLotSelectedCnt)
				Begin
					Select '0' as FieldValue1, 'Tender Security Payment is Pending' as FieldValue2							
				End
				Else
				Begin		    
					Select '1' as FieldValue1, 'Allow Final Submission' as FieldValue2		
				End	
			End
		
			
		END	
    	/** END ----------------------- FINAL SUBMISSION --------------------------********/
    	
    	END
    	ELSE
    	BEGIN
    		SELECT '0' as FieldValue1,'Please Fill All Mandatory Documents' as FieldValue2
    	END
    	
    ELSE
		SELECT '0' as FieldValue1,'Filled BOQ/Price Bid Form Not Encrypted Using Buyer''s Hash.' as FieldValue2
		
END

ELSE
	SELECT  '0' as FieldValue1, 'Please Fill Atleast One Form To Do Final Submission.You Can''t Do Final Submission In This Tender. ' as FieldValue2


END
	
	
END
ELSE
BEGIN
	Select '0' as FieldValue1, 'Your JVCA Partner Has Already Completed Final Submission' as FieldValue2
END			
END


ELSE
--- SUBMISSION TIME OVER
BEGIN
	Select '0' as FieldValue1, 'Final Submission Time is Over' as FieldValue2
END
	
END
ELSE
BEGIN
	Select '0' as FieldValue1, 'Tender has been cancelled' as FieldValue2
END	
COMMIT TRAN	
	END TRY 
	BEGIN CATCH
			BEGIN
				Select '0' as FieldValue1, 'Final Submission Not Completed Successfully' as FieldValue2
			ROLLBACK TRAN
			END
	END CATCH	 

END

IF @v_fieldName1Vc = 'getPriceBidfromLotId' 
BEGIN
	SELECT formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,'' as FieldValue6, 
	'' as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9
	FROM dbo.tbl_TenderForms tf 
	Inner Join dbo.tbl_TenderSection ts ON tf.tenderSectionId = ts.tenderSectionId 
	Inner Join dbo.tbl_TenderStd td On ts.tenderStdId = td.tenderStdId
	Inner Join tbl_TenderLotSecurity tls On tls.appPkgLotId = tf.pkgLotId and tls.tenderId=td.tenderId
	Where td.tenderId = @v_fieldName2Vc and tls.appPkgLotId = @v_fieldName3Vc
	And isPriceBid='Yes'                                        
	Order By isPriceBid 
END	
	
IF @v_fieldName1Vc = 'getLoIdLotDesc' 
BEGIN
select convert(varchar(50),appPkgLotId ) as FieldValue1,lotDesc as FieldValue2,lotNo as FieldValue3 from 
tbl_TenderLotSecurity where tenderId=@v_fieldName2Vc 
END

IF @v_fieldName1Vc = 'get2EnvLoIdLotDesc' 
BEGIN
	If (select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc)!='Services'
	Begin
		select convert(varchar(50),appPkgLotId ) as FieldValue1,lotDesc as FieldValue2,lotNo as FieldValue3 from 
		tbl_TenderLotSecurity where tenderId=@v_fieldName2Vc 
		--and  appPkgLotId in(select pkgLotId from tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and rptStatus='Approved')
	End	
	Else
	Begin
	
		select convert(varchar(50),appPkgLotId ) as FieldValue1,lotDesc as FieldValue2,lotNo as FieldValue3 from 
		tbl_TenderLotSecurity t,tbl_TenderDetails td where t.tenderId=td.tenderId and  td.tenderId=@v_fieldName2Vc 
	End	
END

-- @v_fieldName1Vc -- search_MediaContent
-- @v_fieldName2Vc -- @v_Content
-- @v_fieldName3Vc -- @v_PostedBy 
-- @v_fieldName4Vc -- @v_Page 
-- @v_fieldName5Vc -- @v_PagePerRecord
-- @v_fieldName6Vc -- @contentType

--Exec p_get_commonsearchdata 'search_MediaContent', 'ketan','','1','10','Complaint'

--select * from tbl_MediaContent
IF @v_fieldName1Vc = 'search_MediaContent'
BEGIN
	
	--Search By Content
	IF @v_fieldName2Vc !=NULL OR @v_fieldName2Vc <>''
		BEGIN
			Set @FinalQuery='and (contentSub LIKE ''%'+@v_fieldName2Vc+'%'')'		
		END
	ELSE
		BEGIN
			Set @FinalQuery=''
		END
	
	--Search By DateofPosting
	IF @v_fieldName3Vc !=NULL OR @v_fieldName3Vc <>''
		BEGIN
			IF @FinalQuery=''
				BEGIN
					Set @FinalQuery=@FinalQuery+'and (( CAST(floor(cast([createdDate] as float))as datetime)) = '''+convert(varchar(30),CAST(floor(cast(convert(datetime,@v_fieldName3Vc,105) as float))as datetime),120)+''') '
				END
			ELSE
				BEGIN
					Set @FinalQuery=@FinalQuery+'and (( CAST(floor(cast([createdDate] as float))as datetime)) = '''+convert(varchar(30),CAST(floor(cast(convert(datetime,@v_fieldName3Vc,105) as float))as datetime),120)+''') '
				END
		END
	
		
		IF @FinalQuery=''
			BEGIN
				SET @v_IntialQuery_Vc ='from tbl_MediaContent where contentType= '''+	@v_fieldName6Vc	+''''
			END
		ELSE
			BEGIN
				SET @v_IntialQuery_Vc= 'from tbl_MediaContent where contentType= '''+	@v_fieldName6Vc+''''	
			END
		
		
		set @v_ExecutingQuery_Vc='DECLARE @v_Reccountf Int
			DECLARE @v_TotalPagef Int
			SELECT @v_Reccountf = Count(*) From (
			SELECT * From (SELECT ROW_NUMBER() OVER (order by mediaId desc) As Rownumber
			'+@v_IntialQuery_Vc+''+@FinalQuery+') AS DATA) AS TTT
			SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_fieldName5Vc as varchar(10))+')
			SELECT *,cast(@v_TotalPagef as varchar(20)) as FieldValue6,cast(@v_Reccountf as varchar(20)) as FieldValue7 From (SELECT cast(ROW_NUMBER() OVER (order by mediaId desc) as varchar(20)) As FieldValue1,cast([mediaId] as varchar(20)) as FieldValue2,[contentType] as FieldValue3,cast(createdBy as varchar(20)) as FieldValue4,Replace(CONVERT(VARCHAR(11), createdDate , 113),'' '',''-'') +'' ''+convert(varchar(5), createdDate , 114) as FieldValue5,contentSub as FieldValue8
			'+@v_IntialQuery_Vc+''+@FinalQuery+') AS DATA where FieldValue1 between '+cast(((cast(@v_fieldName4Vc as int) - 1) * cast(@v_fieldName5Vc as int) + 1) as varchar(10))+' 
			AND '+cast((cast(@v_fieldName4Vc as int) * cast(@v_fieldName5Vc as int)) as varchar(10))+''
			
		print @v_ExecutingQuery_Vc
		Exec(@v_ExecutingQuery_Vc)
		
		-- FieldValue1 - RowNumber

		-- FieldValue2 - mediaId

		-- FieldValue3 - contentType

		-- FieldValue4 - createdBy

		-- FieldValue5 - createdDate

		-- FieldValue6 - TotalPages

		-- FieldValue7 - TotalRecords


		
END
	
If @v_fieldName1Vc='getTenderEvalEmpMobNo'
begin
select '+880'+mobileNo as FieldValue1,CONVERT(varchar(10),t.tenderId) as FieldValue2 ,
emailId as FieldValue3,reoiRfpRefNo as FieldValue4,
REPLACE(CONVERT(VARCHAR(11),t.openingDt, 106), ' ', '-') +' ' +Substring(CONVERT(VARCHAR(30),t.openingDt,108),1,5)   as FieldValue5
from tbl_TenderOpenDates t,tbl_Committee c ,tbl_CommitteeMembers cm,tbl_EmployeeMaster em,
tbl_TenderDetails td,tbl_LoginMaster lm
where t.tenderId=c.tenderId and em.userId=lm.userId and cm.userId=lm.userId
and c.committeeId=cm.committeeId and cm.userId=em.userId and
t.tenderid=td.tenderId  and committeeType in('tec','pec') and 
convert(varchar(10),t.openingDt,103)=convert(varchar(10),GETDATE()+1,103)
and tenderStatus='Approved'
 end	
	

IF @v_fieldName1Vc='getGovUserIdFromUserId'
BEGIN
	Select Convert(varchar(50),govUserId) as FieldValue1 
	from tbl_EmployeeTrasfer et Where  et.userId=@v_fieldName2Vc and isCurrent='Yes'
END


IF @v_fieldName1Vc='GetComMemforEvalPost'
BEGIN
	If @v_fieldName3Vc='cp'
	Begin
		--SELECT distinct convert(varchar(50), em.employeeId) as FieldValue1,
		--em.employeeName as FieldValue2,
		--convert(varchar(50),[quePostedBy]) as FieldValue3	
		--FROM  [tbl_EvalFormQues] e ,
		--tbl_EmployeeMaster em
		--where [quePostedBy]=em.userId 		
		--and tenderFormId=@v_fieldName2Vc
		--ORDER BY FieldValue2 
		
		SELECT distinct convert(varchar(50), em.employeeId) as FieldValue1,
		em.employeeName as FieldValue2,
		convert(varchar(50),[em].userId) as FieldValue3
		FROM  
		tbl_EmployeeMaster em
		inner join tbl_CommitteeMembers cm on em.userId=cm.userId
		inner join tbl_Committee c on cm.committeeId=c.committeeId
		where c.tenderId=@v_fieldName5Vc
		And cm.memberRole<>'cp'
			--And em.userId=@v_fieldName4Vc
		ORDER BY FieldValue2 	
	End
	Else
	Begin
		--SELECT distinct convert(varchar(50), em.employeeId) as FieldValue1,
		--em.employeeName as FieldValue2,
		--convert(varchar(50),[quePostedBy]) as FieldValue3
		--FROM  [tbl_EvalFormQues] e ,
		--tbl_EmployeeMaster em
		--where [quePostedBy]=em.userId 		
		--and tenderFormId=@v_fieldName2Vc
		--And e.quePostedBy=@v_fieldName4Vc
		--ORDER BY FieldValue2 	
		
		SELECT distinct convert(varchar(50), em.employeeId) as FieldValue1,
		em.employeeName as FieldValue2,
		convert(varchar(50),[em].userId) as FieldValue3
		FROM  
		tbl_EmployeeMaster em
		inner join tbl_CommitteeMembers cm on em.userId=cm.userId
		inner join tbl_Committee c on cm.committeeId=c.committeeId
		where c.tenderId=@v_fieldName5Vc
			And em.userId=@v_fieldName4Vc
		ORDER BY FieldValue2 	
		
		
	End

END

IF @v_fieldName1Vc='getClarificationToCPLink' 
BEGIN
	Select top 1 convert(varchar(50), evaBidderStatusId) as FieldValue1
	From tbl_EvalBidderStatus
	Where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc and evalStatus=@v_fieldName4Vc
END


IF @v_fieldName1Vc='getFlagStatus' 
BEGIN
	/*
	@v_fieldName2Vc = Activity ID
	@v_fieldName3Vc = Object ID
	@v_fieldName4Vc = Child ID	
	*/
	If @v_fieldName2Vc='1'
	Begin
		If @v_fieldName3Vc=@v_fieldName4Vc
		Begin
			Select Case 
				 
				When Exists (Select top 1 packageId from tbl_apppackages where appid=@v_fieldName3Vc and appStatus='Pending' )
				Then '0'
				Else '1'
				End as FieldValue1	
		End
		Else
		Begin
			Select Case 
				When Exists (Select top 1 packageId from tbl_apppackages where packageId=@v_fieldName4Vc and appStatus='approved' )
				Then '1' 
				Else '0'
				End as FieldValue1	
			
		End	
	End
	
	Else If @v_fieldName2Vc='2'
	Begin
		Select Case 
				When Exists (Select top 1 tenderDtlId from tbl_tenderdetails where tenderid=@v_fieldName3Vc and tenderstatus='approved' )
				Then '1' 
				Else '0'
				End as FieldValue1	
	End
	
	Else If @v_fieldName2Vc='3'
	Begin
		Select Case 
				When Exists (Select top 1 preTenderMetDocId from tbl_PretenderMetdocs where tenderid=@v_fieldName3Vc and prebidstatus='approved' )
				Then '1' 
				Else '0'
				End as FieldValue1	
	End

	Else If @v_fieldName2Vc='4'
	Begin
		Select Case 
				When Exists (Select top 1 corrigendumId from tbl_corrigendummaster where tenderid=@v_fieldName3Vc and corrigendumId=@v_fieldName4Vc and corrigendumstatus='approved')
				Then '1' 
				Else '0'
				End as FieldValue1	
	End
	
	Else If @v_fieldName2Vc='5' Or @v_fieldName2Vc='6' Or @v_fieldName2Vc='8'
	Begin
		IF @v_fieldName2Vc = '5'
		Begin
				Select Case 
				When Exists (Select top 1 committeeId from tbl_committee where tenderid=@v_fieldName3Vc and committeeType in ('TOC','POC') and committeStatus='approved')
				Then '1' 
				Else '0'
				End as FieldValue1	
		
		End
		Else IF @v_fieldName2Vc = '6'
		Begin
				Select Case 
				When Exists (Select top 1 committeeId from tbl_committee where tenderid=@v_fieldName3Vc and committeeType in ('TEC','PEC') and committeStatus='approved')
				Then '1' 
				Else '0'
				End as FieldValue1	
		End
		Else IF @v_fieldName2Vc = '8'
		Begin
				Select Case 
				When Exists (Select top 1 committeeId from tbl_committee where tenderid=@v_fieldName3Vc and committeeType='TSC' and committeStatus='approved')
				Then '1' 
				Else '0'
				End as FieldValue1	
		End
	
	
	End


End


IF @v_fieldName1Vc = 'GetDocumentsWithPaging'
BEGIN
 
	--declare @v_FormList_Vc1 varchar(2000)
	
	/* 
	 Start: Input Params
	 @v_fieldName1Vc =keyword,		 
	 @v_fieldName2Vc =FolderId,
	 @v_fieldName3Vc =UserId (Session id), 
	 @v_fieldName4Vc =PageNo,
	 @v_fieldName5Vc =RecordSize,	 
	 @v_fieldName6Vc =SearchText,	 
	 @v_fieldName7Vc =SOrtVal
	 End: Input Params 
	 */
	
	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName4Vc, @v_RecordPerPage_inInt=@v_fieldName5Vc
	
	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)	
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	
	
	/*Start: Dynamic Condition string*/
	SET @v_ConditionString_Vc=''
	If @v_fieldName6Vc is not null And @v_fieldName6Vc<>''
	Begin
		Set @v_ConditionString_Vc = ' And ' + @v_fieldName6Vc
	End
	/*End: Dynamic Condition string*/
	
	If (select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1 -- Company Case
	Begin
		 select @v_FormList_Vc1 = COALESCE(@v_FormList_Vc1+',', ' ') + convert(varchar(20), formid) 
		 from tbl_biddocuments where userId=@v_fieldName3Vc
		 --print    @v_FormList_Vc1 
		 set @v_FormList_Vc1=ISNULL(@v_FormList_Vc1,'')
		 Select @FinalQuery='	
		 With CTE1 as (		
		 Select * from (
		 select  
		  convert(varchar(50),a.companyDocId ) as FieldValue1,
		  convert(varchar(150),documentName) as FieldValue2
			  , convert(varchar(150),documentSize) as FieldValue3
			  , convert(varchar(250),documentBrief) as FieldValue4
			  , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), '' '', ''-'')   as FieldValue5
			  , convert(varchar(50),docstatus) as FieldValue6
			  , convert(varchar(50),b.companyDocId ) as FieldValue7,
			  IsNull(docHash,'''') as FieldValue8
			  , convert(varchar(50),documentTypeId )  as FieldValue11
			  ,case when b.companyDocId is not null then '''+@v_FormList_Vc1+''' else null end as FieldValue10,
			  Row_Number() Over('+@v_fieldName7Vc+') as RowNumber
		       
			  from (select c.* 
					from tbl_CompanyDocuments c,
						tbl_TendererMaster t
					where c.tendererId=t.tendererId 
							and t.userId in 
								(
									select userId from tbl_TendererMaster 
										where companyId =
											(select companyId from tbl_TendererMaster where userId='+@v_fieldName3Vc+' and companyId!=1)	
								)												
							 
							and folderId='+@v_fieldName2Vc+' and docStatus!=''archive''' 
							+ @v_ConditionString_Vc +
				   ') a
			left outer join
				(select distinct companyDocId  from tbl_biddocuments 
					where userId in 
								(
									select userId from tbl_TendererMaster 
										where companyId =
											(select companyId from tbl_TendererMaster where userId='+@v_fieldName3Vc+' and companyId!=1)	
								)			
				) b
			on a.companyDocId=b.companydocid
			) as Tmp
	)
	select *, (select CONVERT(varchar(50), COUNT(*)) from CTE1) as FieldValue12 from CTE1 
	WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int) 					
	+ ' Order by RowNumber'
			 
	
	
	
		
		 --select  
		 -- convert(varchar(50),a.companyDocId ) as FieldValue1,
		 --convert(varchar(150),documentName) as FieldValue2
			--  , convert(varchar(150),documentSize) as FieldValue3
			--  , convert(varchar(250),documentBrief) as FieldValue4
			--  , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), ' ', '-')   as FieldValue5
			--  , convert(varchar(50),docstatus) as FieldValue6
			--  , convert(varchar(50),b.companyDocId ) as FieldValue7
			--  , documentTypeId as FieldValue11
			--  ,case when b.companyDocId is not null then @v_FormList_Vc1 else null end as FieldValue10
		       
			--  from (select c.* from tbl_CompanyDocuments c,tbl_TendererMaster t
			--			where c.tendererId=t.tendererId 
			--				and t.userId in 
			--					(
			--						select userId from tbl_TendererMaster 
			--							where companyId =
			--								(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)	
			--					)												
							 
			--				and folderId=@v_fieldName2Vc and docStatus!='archive' 
			--	   ) a
			--left outer join
			--	(select distinct companyDocId  from tbl_biddocuments 
			--		where userId in 
			--					(
			--						select userId from tbl_TendererMaster 
			--							where companyId =
			--								(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)	
			--					)			
			--	) b
			--on a.companyDocId=b.companydocid 
	End
	Else -- Individual Consultant Case
	Begin
		 select @v_FormList_Vc1 = COALESCE(@v_FormList_Vc1+',', ' ') + convert(varchar(20), formid) 
		 from tbl_biddocuments where userId=@v_fieldName3Vc
		 --print    @v_FormList_Vc1 
		
	 Select @FinalQuery='
	  With CTE1 as (		
	 Select * from (
	 select  
		  convert(varchar(50),a.companyDocId ) as FieldValue1,
		 convert(varchar(150),documentName) as FieldValue2
			  , convert(varchar(150),documentSize) as FieldValue3
			  , convert(varchar(250),documentBrief) as FieldValue4
			  , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), '' '', ''-'')   as FieldValue5
			  , convert(varchar(50),docstatus) as FieldValue6
			  , convert(varchar(50),b.companyDocId ) as FieldValue7
			  , documentTypeId as FieldValue11
			  ,case when b.companyDocId is not null then '''+@v_FormList_Vc1+''' else null end as FieldValue10,
			  Row_Number() Over('+@v_fieldName7Vc+') as RowNumber
		       
			  from (select c.* from tbl_CompanyDocuments c,
								tbl_TendererMaster t
					where c.tendererId=t.tendererId 
							and userId='+@v_fieldName3Vc+' 
						and folderId='+@v_fieldName2Vc+' and docStatus!=''archive''' 
						+ @v_ConditionString_Vc +
					') a
		left outer join
		(select distinct companyDocId  from tbl_biddocuments where userId='+@v_fieldName3Vc+') b
		on a.companyDocId=b.companydocid
			) as Tmp
	)
	select *, (select CONVERT(varchar(50), COUNT(*)) from CTE1) as FieldValue12 from CTE1 
	WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int) 					
	+ ' Order by RowNumber' 	 
		 
		 
		-- select  
		--  convert(varchar(50),a.companyDocId ) as FieldValue1,
		-- convert(varchar(150),documentName) as FieldValue2
		--	  , convert(varchar(150),documentSize) as FieldValue3
		--	  , convert(varchar(250),documentBrief) as FieldValue4
		--	  , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), ' ', '-')   as FieldValue5
		--	  , convert(varchar(50),docstatus) as FieldValue6
		--	  , convert(varchar(50),b.companyDocId ) as FieldValue7
		--	  , documentTypeId as FieldValue11
		--	  ,case when b.companyDocId is not null then @v_FormList_Vc1 else null end as FieldValue10
		       
		--	  from (select c.* from tbl_CompanyDocuments c,tbl_TendererMaster t
		--where c.tendererId=t.tendererId and userId=@v_fieldName3Vc and folderId=@v_fieldName2Vc and docStatus!='archive' ) a
		--left outer join
		--(select distinct companyDocId  from tbl_biddocuments where userId=@v_fieldName3Vc) b
		--on a.companyDocId=b.companydocid 
	End
	
	 print (@FinalQuery)
	 Exec (@FinalQuery)
	 
	
	 --print @v_FormList_Vc1
END

IF @v_fieldName1Vc = 'GetFoldersWithPaging'
BEGIN

	/* 
	 Start: Input Params
	 @v_fieldName1Vc =keyword,		 
	 @v_fieldName2Vc =FolderId,
	 @v_fieldName3Vc =UserId (Session id), 
	 @v_fieldName4Vc =PageNo,
	 @v_fieldName5Vc =RecordSize,	 
	 @v_fieldName6Vc =SearchText,	 
	 @v_fieldName7Vc =SOrtVal
	 End: Input Params 
	 */
	
	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName4Vc, @v_RecordPerPage_inInt=@v_fieldName5Vc
	
	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)	
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	
	
	/*Start: Dynamic Condition string*/
	SET @v_ConditionString_Vc=''
	If @v_fieldName6Vc is not null And @v_fieldName6Vc<>''
	Begin
		Set @v_ConditionString_Vc = ' And ' + @v_fieldName6Vc
	End

	If (select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1
	Begin
		Select @FinalQuery='
		With CTE1 as (		
		Select * from (
		select  
		convert(varchar(50),folderId ) as FieldValue1,
		convert(varchar(150),folderName) as FieldValue2,
		Row_Number() Over('+@v_fieldName7Vc+') as RowNumber
		from tbl_TendererFolderMaster c,
			 tbl_TendererMaster t
		where c.tendererId=t.tendererId 
			and userId in (
						select userId from tbl_TendererMaster 
								where companyId =
								(select companyId from tbl_TendererMaster where userId='+@v_fieldName3Vc+' and companyId!=1)				
						)'
				+ @v_ConditionString_Vc +
		' ) as Tmp
	)
	select *, (select CONVERT(varchar(50), COUNT(*)) from CTE1) as FieldValue12 from CTE1 
	WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int) 					
	+ ' Order by RowNumber' 					 
			
		--select  
		--convert(varchar(50),folderId ) as FieldValue1,
		--convert(varchar(150),folderName) as FieldValue2
		--from tbl_TendererFolderMaster c,
		--	 tbl_TendererMaster t
		--where c.tendererId=t.tendererId 
		--and userId in (
		--	select userId from tbl_TendererMaster 
		--		where companyId =
		--		(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
				
		--) 
	End 
	Else
	Begin
	Select @FinalQuery='
	With CTE1 as (		
	Select * from (
	select  
		convert(varchar(50),folderId ) as FieldValue1,
		convert(varchar(150),folderName) as FieldValue2,
		Row_Number() Over('+@v_fieldName7Vc+') as RowNumber
		from tbl_TendererFolderMaster c,tbl_TendererMaster t
		where c.tendererId=t.tendererId and userId=' + @v_fieldName3Vc 
		+ @v_ConditionString_Vc +
			' ) as Tmp
	)
	select *, (select CONVERT(varchar(50), COUNT(*)) from CTE1) as FieldValue12 from CTE1 
	WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int) 					
	+ ' Order by RowNumber'  
		 
	
		--select  
		--convert(varchar(50),folderId ) as FieldValue1,
		--convert(varchar(150),folderName) as FieldValue2
		--from tbl_TendererFolderMaster c,tbl_TendererMaster t
		--where c.tendererId=t.tendererId and userId=@v_fieldName3Vc 
	End
	 print (@FinalQuery)
	 Exec (@FinalQuery)
	

  
END


IF @v_fieldName1Vc = 'GetAllDocumentsWithPaging'
BEGIN

/* 
	 Start: Input Params
	 @v_fieldName1Vc =keyword,		 
	 @v_fieldName2Vc =FolderId,
	 @v_fieldName3Vc =UserId (Session id), 
	 @v_fieldName4Vc =PageNo,
	 @v_fieldName5Vc =RecordSize,	 
	 @v_fieldName6Vc =SearchText,	 
	 @v_fieldName7Vc =SOrtVal
	 End: Input Params 
	 */
	
	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName4Vc, @v_RecordPerPage_inInt=@v_fieldName5Vc
	
	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)	
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	
	/*Start: Dynamic Condition string*/
	SET @v_ConditionString_Vc=''
	If @v_fieldName6Vc is not null And @v_fieldName6Vc<>''
	Begin
		Set @v_ConditionString_Vc = ' And ' + @v_fieldName6Vc
	End
 
 If (select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1
 Begin
 
  select @v_FormList_Vc = COALESCE(@v_FormList_Vc+',', ' ') + convert(varchar(20), formid) 
   from tbl_biddocuments where userId=@v_fieldName3Vc
   set @v_FormList_Vc=ISNULL(@v_FormList_Vc,'')
	Select @FinalQuery='
	With CTE1 as (	
	Select * from (
  select  
  convert(varchar(50),a.companyDocId ) as FieldValue1,
 convert(varchar(150),documentName) as FieldValue2
      , convert(varchar(150),documentSize) as FieldValue3
      , convert(varchar(250),documentBrief) as FieldValue4
      , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), '' '', ''-'')   as FieldValue5
      , convert(varchar(50),docstatus) as FieldValue6
      , convert(varchar(50),b.companyDocId ) as FieldValue7,
      convert(varchar(50),a.folderId ) as FieldValue8,
        convert(varchar(50),isnull(c.foldername,''-'') ) as FieldValue9 , 
        documentTypeId as FieldValue11 ,
        case when b.companyDocId is not null then '''+@v_FormList_Vc+''' else null end  as FieldValue10,
        Row_Number() Over('+@v_fieldName7Vc+') as RowNumber
       
		  from (select c.* from tbl_CompanyDocuments c,
					tbl_TendererMaster t
					where c.tendererId=t.tendererId and 
					userId in (
						select userId from tbl_TendererMaster 
						where companyId =
							(select companyId from tbl_TendererMaster where userId='+@v_fieldName3Vc+' and companyId!=1)					
						)'
						+ @v_ConditionString_Vc + 
		
		'and docstatus!=''archive'' ) a
	left outer join
	(
		select distinct companyDocId from tbl_biddocuments where 
		userId in (
					select userId from tbl_TendererMaster 
					where companyId =
					(select companyId from tbl_TendererMaster where userId='+@v_fieldName3Vc+' and companyId!=1)
					
					)
	) b
	on a.companyDocId=b.companydocid 
	 left outer join
	(
		select distinct t.tendererId,folderName,folderid from tbl_TendererFolderMaster t,tbl_tenderermaster m
			where t.tendererId=m.tendererId 
			and  m.userId in (
				select userId from tbl_TendererMaster 
					where companyId =
					(select companyId from tbl_TendererMaster where userId='+@v_fieldName3Vc+' and companyId!=1)
					
			)
		) c
	on a.tendererId=c.tendererId and a.folderId=c.folderId
	) as Tmp
	)
	select *, (select CONVERT(varchar(50), COUNT(*)) from CTE1) as FieldValue12 from CTE1 
	WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int) 					
	+ ' Order by RowNumber'
	 
	 
	-- select @v_FormList_Vc = COALESCE(@v_FormList_Vc+',', ' ') + convert(varchar(20), formid) 
 --  from tbl_biddocuments where userId=@v_fieldName3Vc
 -- select  
 -- convert(varchar(50),a.companyDocId ) as FieldValue1,
 --convert(varchar(150),documentName) as FieldValue2
 --     , convert(varchar(150),documentSize) as FieldValue3
 --     , convert(varchar(250),documentBrief) as FieldValue4
 --     , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), ' ', '-')   as FieldValue5
 --     , convert(varchar(50),docstatus) as FieldValue6
 --     , convert(varchar(50),b.companyDocId ) as FieldValue7,convert(varchar(50),a.folderId ) as FieldValue8,
 --       convert(varchar(50),isnull(c.foldername,'-') ) as FieldValue9 , documentTypeId as FieldValue11 ,
 --         case when b.companyDocId is not null then @v_FormList_Vc else null end  as FieldValue10
       
	--	  from (select c.* from tbl_CompanyDocuments c,tbl_TendererMaster t
	--	where c.tendererId=t.tendererId and 
	--	userId in (
	--			select userId from tbl_TendererMaster 
	--				where companyId =
	--				(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
					
	--		) 
		
	--	and docstatus!='archive' ) a
	--left outer join
	--(
	--	select distinct companyDocId from tbl_biddocuments where 
	--	userId in (
	--			select userId from tbl_TendererMaster 
	--				where companyId =
	--				(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
					
	--		)
	--) b
	--on a.companyDocId=b.companydocid 
	-- left outer join
	--(
	--	select distinct t.tendererId,folderName,folderid from tbl_TendererFolderMaster t,tbl_tenderermaster m
	--		where t.tendererId=m.tendererId 
	--		and  m.userId in (
	--			select userId from tbl_TendererMaster 
	--				where companyId =
	--				(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
					
	--		)
	--	) c
	--on a.tendererId=c.tendererId and a.folderId=c.folderId
	
	
 End
 Else
 Begin
	 select @v_FormList_Vc = COALESCE(@v_FormList_Vc+',', ' ') + convert(varchar(20), formid) 
 from tbl_biddocuments where userId=@v_fieldName3Vc
 
   Select @FinalQuery='
    With CTE1 as (		
   Select * from (
 select  
  convert(varchar(50),a.companyDocId ) as FieldValue1,
 convert(varchar(150),documentName) as FieldValue2
      , convert(varchar(150),documentSize) as FieldValue3
      , convert(varchar(250),documentBrief) as FieldValue4
      , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), '' '', ''-'')   as FieldValue5
      , convert(varchar(50),docstatus) as FieldValue6
      , convert(varchar(50),b.companyDocId ) as FieldValue7,convert(varchar(50),a.folderId ) as FieldValue8,
        convert(varchar(50),isnull(c.foldername,''-'') ) as FieldValue9 , documentTypeId as FieldValue11 ,
          case when b.companyDocId is not null then '''+@v_FormList_Vc+''' else null end  as FieldValue10,
          Row_Number() Over('+@v_fieldName7Vc+') as RowNumber
       
      from (select c.* from tbl_CompanyDocuments c,
				tbl_TendererMaster t
					where c.tendererId=t.tendererId and userId='+@v_fieldName3Vc+' and docstatus!=''archive'' '
					+ @v_ConditionString_Vc +
			') a
left outer join
(select distinct companyDocId from tbl_biddocuments where userId='+@v_fieldName3Vc+') b
on a.companyDocId=b.companydocid 
 left outer join
(select distinct t.tendererId,folderName,folderid from tbl_TendererFolderMaster t,tbl_tenderermaster m
 where t.tendererId=m.tendererId and  m.userId='+@v_fieldName3Vc+' ) c
on a.tendererId=c.tendererId and a.folderId=c.folderId
) as Tmp
	)
	select *, (select CONVERT(varchar(50), COUNT(*)) from CTE1) as FieldValue12 from CTE1 
	WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int) 					
	+ ' Order by RowNumber'
 
 
--  select  
--  convert(varchar(50),a.companyDocId ) as FieldValue1,
-- convert(varchar(150),documentName) as FieldValue2
--      , convert(varchar(150),documentSize) as FieldValue3
--      , convert(varchar(250),documentBrief) as FieldValue4
--      , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), ' ', '-')   as FieldValue5
--      , convert(varchar(50),docstatus) as FieldValue6
--      , convert(varchar(50),b.companyDocId ) as FieldValue7,convert(varchar(50),a.folderId ) as FieldValue8,
--        convert(varchar(50),isnull(c.foldername,'-') ) as FieldValue9 , documentTypeId as FieldValue11 ,
--          case when b.companyDocId is not null then @v_FormList_Vc else null end  as FieldValue10
       
--      from (select c.* from tbl_CompanyDocuments c,tbl_TendererMaster t
--where c.tendererId=t.tendererId and userId=@v_fieldName3Vc    and docstatus!='archive' ) a
--left outer join
--(select distinct companyDocId from tbl_biddocuments where userId=@v_fieldName3Vc) b
--on a.companyDocId=b.companydocid 
-- left outer join
--(select distinct t.tendererId,folderName,folderid from tbl_TendererFolderMaster t,tbl_tenderermaster m
-- where t.tendererId=m.tendererId and  m.userId=@v_fieldName3Vc ) c
--on a.tendererId=c.tendererId and a.folderId=c.folderId


 End
 
 print (@FinalQuery)
	 Exec (@FinalQuery)

END

IF @v_fieldName1Vc = 'GetArchiveDocumentsWithPaging'
BEGIN
--declare @v_FormList_Vc3 varchar(2000)

/* 
	 Start: Input Params
	 @v_fieldName1Vc =keyword,		 
	 @v_fieldName2Vc =FolderId,
	 @v_fieldName3Vc =UserId (Session id), 
	 @v_fieldName4Vc =PageNo,
	 @v_fieldName5Vc =RecordSize,	 
	 @v_fieldName6Vc =SearchText,	 
	 @v_fieldName7Vc =SOrtVal
	 End: Input Params 
	 */
	
	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName4Vc, @v_RecordPerPage_inInt=@v_fieldName5Vc
	
	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)	
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	
	/*Start: Dynamic Condition string*/
	SET @v_ConditionString_Vc=''
	If @v_fieldName6Vc is not null And @v_fieldName6Vc<>''
	Begin
		Set @v_ConditionString_Vc = ' And ' + @v_fieldName6Vc
	End

	If (select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1
	Begin
					 select @v_FormList_Vc3 = COALESCE(@v_FormList_Vc3+',', ' ') + convert(varchar(20), formid) 
		 from tbl_biddocuments where userId=@v_fieldName3Vc
		 set @v_FormList_Vc3=isnull(@v_FormList_Vc3,'')
		 
		 Select @FinalQuery='
		 With CTE1 as (		
		 Select * from (
	 select  
		  convert(varchar(50),a.companyDocId ) as FieldValue1,
		 convert(varchar(150),documentName) as FieldValue2
			  , convert(varchar(150),documentSize) as FieldValue3
			  , convert(varchar(250),documentBrief) as FieldValue4
			  , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), '' '', ''-'')   as FieldValue5
			  , convert(varchar(50),docstatus) as FieldValue6
			  , convert(varchar(50),b.companyDocId ) as FieldValue7,
			  IsNull(docHash,'''') as FieldValue8,
			   , case when b.companyDocId is not null then '''+@v_FormList_Vc3+''' else null end as FieldValue10
			   , documentTypeId as FieldValue11,
			   Row_Number() Over('+@v_fieldName7Vc+') as RowNumber
			  from (
					select c.* from tbl_CompanyDocuments c,
							tbl_TendererMaster t
					where c.tendererId=t.tendererId 
					and docstatus=''archive'' 
					and userId in (
							select userId from tbl_TendererMaster 
								where companyId =
								(select companyId from tbl_TendererMaster where userId='+@v_fieldName3Vc+' and companyId!=1)								
							)'
							+ @v_ConditionString_Vc + 
					
					') a
		left outer join
		(
			select distinct companyDocId  from tbl_biddocuments where 
			userId in (
						select userId from tbl_TendererMaster 
							where companyId =
							(select companyId from tbl_TendererMaster where userId='+@v_fieldName3Vc+' and companyId!=1)
							
					)
		) b
		on a.companyDocId=b.companydocid
		
		) as Tmp
	)
	select *, (select CONVERT(varchar(50), COUNT(*)) from CTE1) as FieldValue12 from CTE1 
	WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int) 					
	+ ' Order by RowNumber' 
		 
		-- select  
		--  convert(varchar(50),a.companyDocId ) as FieldValue1,
		-- convert(varchar(150),documentName) as FieldValue2
		--	  , convert(varchar(150),documentSize) as FieldValue3
		--	  , convert(varchar(250),documentBrief) as FieldValue4
		--	  , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), ' ', '-')   as FieldValue5
		--	  , convert(varchar(50),docstatus) as FieldValue6
		--	  , convert(varchar(50),b.companyDocId ) as FieldValue7
		--	   , case when b.companyDocId is not null then @v_FormList_Vc3 else null end as FieldValue10
		--	   , documentTypeId as FieldValue11
		--	  from (
		--		select c.* from tbl_CompanyDocuments c,tbl_TendererMaster t
		--			where c.tendererId=t.tendererId 
		--			and docstatus='archive' 
		--			and userId in (
		--					select userId from tbl_TendererMaster 
		--						where companyId =
		--						(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
								
		--				)
					
		--			) a
		--left outer join
		--(
		--	select distinct companyDocId  from tbl_biddocuments where 
		--	userId in (
		--				select userId from tbl_TendererMaster 
		--					where companyId =
		--					(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
							
		--			)
		--) b
		--on a.companyDocId=b.companydocid
	End
	Else
	Begin
			 select @v_FormList_Vc3 = COALESCE(@v_FormList_Vc3+',', ' ') + convert(varchar(20), formid) 
 from tbl_biddocuments where userId=@v_fieldName3Vc
 
 Select @FinalQuery='
 With CTE1 as (		
 Select * from (
 select  
  convert(varchar(50),a.companyDocId ) as FieldValue1,
 convert(varchar(150),documentName) as FieldValue2
      , convert(varchar(150),documentSize) as FieldValue3
      , convert(varchar(250),documentBrief) as FieldValue4
      , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), '' '', ''-'')   as FieldValue5
      , convert(varchar(50),docstatus) as FieldValue6
      , convert(varchar(50),b.companyDocId ) as FieldValue7
       , case when b.companyDocId is not null then '''+@v_FormList_Vc3+''' else null end as FieldValue10
       , documentTypeId as FieldValue11.
       Row_Number() Over('+@v_fieldName7Vc+') as RowNumber
      from (	select c.* from tbl_CompanyDocuments c,
					tbl_TendererMaster t
					where c.tendererId=t.tendererId and userId='+@v_fieldName3Vc+' and docstatus=''archive'' '
				+ @v_ConditionString_Vc + 
			') a
left outer join
(select distinct companyDocId  from tbl_biddocuments where  userId='+@v_fieldName3Vc+') b
on a.companyDocId=b.companydocid
) as Tmp
	)
	select *, (select CONVERT(varchar(50), COUNT(*))  from CTE1) as FieldValue12 from CTE1 
	WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int) 					
	+ ' Order by RowNumber'  
-- select  
--  convert(varchar(50),a.companyDocId ) as FieldValue1,
-- convert(varchar(150),documentName) as FieldValue2
--      , convert(varchar(150),documentSize) as FieldValue3
--      , convert(varchar(250),documentBrief) as FieldValue4
--      , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), ' ', '-')   as FieldValue5
--      , convert(varchar(50),docstatus) as FieldValue6
--      , convert(varchar(50),b.companyDocId ) as FieldValue7
--       , case when b.companyDocId is not null then @v_FormList_Vc3 else null end as FieldValue10
--       , documentTypeId as FieldValue11
--      from (select c.* from tbl_CompanyDocuments c,tbl_TendererMaster t
--where c.tendererId=t.tendererId and userId=@v_fieldName3Vc and docstatus='archive' ) a
--left outer join
--(select distinct companyDocId  from tbl_biddocuments where  userId=@v_fieldName3Vc) b
--on a.companyDocId=b.companydocid

	End
	
	 print (@FinalQuery)
	 Exec (@FinalQuery)
END

IF @v_fieldName1Vc = 'GetBidderDocumentsWithPaging'
BEGIN

/* 
	 Start: Input Params
	 @v_fieldName1Vc =keyword,		 
	 @v_fieldName2Vc =FolderId,
	 @v_fieldName3Vc =UserId (Session id), 
	 @v_fieldName4Vc =PageNo,
	 @v_fieldName5Vc =RecordSize,	 
	 @v_fieldName6Vc =SearchText,	 
	 @v_fieldName7Vc =SOrtVal
	 End: Input Params 
	 */
	
	/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName4Vc, @v_RecordPerPage_inInt=@v_fieldName5Vc
	
	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)	
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	
	/*Start: Dynamic Condition string*/
	SET @v_ConditionString_Vc=''
	If @v_fieldName6Vc is not null And @v_fieldName6Vc<>''
	Begin
		Set @v_ConditionString_Vc = ' And ' + @v_fieldName6Vc
	End
	
Select @FinalQuery='
select  
  convert(varchar(50),a.companyDocId ) as FieldValue1,
 convert(varchar(150),documentName) as FieldValue2
      , convert(varchar(150),documentSize) as FieldValue3
      , convert(varchar(250),documentBrief) as FieldValue4
      , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), '' '', ''-'')   as FieldValue5
      , convert(varchar(50),docstatus) as FieldValue6 
       ,  convert(varchar(50),formId)  as FieldValue10
        , documentTypeId as FieldValue11    
        ,case manDocId
        when 0 then ''-'' 
        else (select documentName from tbl_TenderMandatoryDoc tmd where tmd.tenderFormDocId=manDocId)  
        end as FieldValue12     
      from (select c.*,formid,b.manDocId from tbl_CompanyDocuments c,
				tbl_TendererMaster t,tbl_biddocuments b
				where c.tendererId=t.tendererId and t.userId='+@v_fieldName2Vc+' 
				and b.companyDocId=c.companyDocId
			and formId='+@v_fieldName3Vc+''
			+ @v_ConditionString_Vc +
			') a'
			
	 print (@FinalQuery)
	 Exec (@FinalQuery)

--select  
--  convert(varchar(50),a.companyDocId ) as FieldValue1,
-- convert(varchar(150),documentName) as FieldValue2
--      , convert(varchar(150),documentSize) as FieldValue3
--      , convert(varchar(250),documentBrief) as FieldValue4
--      , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), ' ', '-')   as FieldValue5
--      , convert(varchar(50),docstatus) as FieldValue6 
--       ,  convert(varchar(50),formId)  as FieldValue10
--        , documentTypeId as FieldValue11    
--        ,case manDocId
--        when 0 then '-' 
--        else (select documentName from tbl_TenderMandatoryDoc tmd where tmd.tenderFormDocId=manDocId)  
--        end as FieldValue12     
--      from (select c.*,formid,b.manDocId from tbl_CompanyDocuments c,tbl_TendererMaster t,tbl_biddocuments b
--where c.tendererId=t.tendererId and t.userId=@v_fieldName2Vc  and b.companyDocId=c.companyDocId
--and formId=@v_fieldName3Vc) a
 
 
END

IF @v_fieldName1Vc = 'getCancelledTenders'
BEGIN
	select CONVERT(varchar(50), tctr.tenderId ) as FieldValue1 
	from tbl_CancelTenderRequest tctr , tbl_TenderDetails ttd
where ttd.tenderId = tctr.tenderId and ttd.tenderStatus = 'Cancelled'
and Cast(Floor(CAST( tctr.reqDateTime as Float)) as Datetime) = Cast(Floor(CAST( GETDATE()-1 as Float)) as Datetime)
END


IF @v_fieldName1Vc = 'getAppProject'  --Get all rows from table tbl_ProjectMaster
BEGIN
	-- For fixing CR #4278 
	--IF	@v_fieldName3Vc = 'Aid or Grant' 
	--BEGIN
	--	SET @v_fieldName3Vc = 'Aid or Grant, Government'
	--END
	-- Change end for #4278
	
	Select @v_ConditionString_Vc =''
	
	Select @v_ConditionString_Vc +=' And projectid IN(	SELECT  projectId 
								FROM [dbo].[tbl_ProjectRoles] pr 
								WHERE pr.userId='+@v_fieldName2Vc+') '
							
	
	
	select @v_ConditionString_Vc += Case 
			When @v_fieldName3Vc = 'Aid or Grant' Then
			' And (sourceoffund like ''%Aid or Grant%'' Or sourceoffund like ''%Government%'')'
			When @v_fieldName3Vc = 'Government' Then						
			'And (sourceoffund like ''%Government%'')'
			When @v_fieldName3Vc = 'own fund' Then						
			'And (sourceoffund like ''%own fund%'')'
			End

    -- For Eventum Issue #842
	--select @v_ConditionString_Vc += 'AND ((year(projectEndDate) > '''+@v_fieldName4Vc+''' )) '
	Declare @JuneLastDay VARCHAR(8) = '-06-30'
	select @v_ConditionString_Vc += 'AND ((projectEndDate > CONVERT(Date,'''+@v_fieldName4Vc+@JuneLastDay +''',102) )) '
	select @v_ConditionString_Vc += ' AND ((projectStartDate <= CONVERT(Date,'''+@v_fieldName5Vc+@JuneLastDay +''',102) )) '

	set @v_FinalQueryVc='
	
	SELECT	CONVERT(VARCHAR(400),[projectId]) AS FieldValue1,
			[projectName] AS FieldValue2,
			[projectCode] AS FieldValue3,
			''Project'' AS FieldValue4,
			projectEndDate  FieldValue5,
			'''' AS FieldValue6 
	FROM	tbl_ProjectMaster
	WHERE	projectStatus=''Approved'' ' 
	+  @v_ConditionString_Vc
	
		
		
		print(@v_FinalQueryVc)
		exec (@v_FinalQueryVc)
			
			
			--AND sourceoffund like ('%'+@v_fieldName3Vc+'%')
			
			--AND ((year(projectEndDate) > @v_fieldName4Vc )
			--		--Or	And @v_fieldName5Vc		 (projectStartDate between @v_fieldName4Vc And @v_fieldName5Vc)
			--	)
	   -- and projectid not in (select projectid from tbl_appmaster)
END

IF @v_fieldName1Vc = 'getEncryptStatusLink'     -- For tenderer/BidPreperation.jsp
BEGIN

Select CONVERT(varchar(20),tf.finalSubmissionId) as FieldValue1  from  tbl_FinalSubmission tf,tbl_FinalSubDetail tfd where  
tf.finalSubmissionId = tfd.finalSubmissionId and tenderId = @v_fieldName2Vc and
tenderformId = @v_fieldName3Vc and  userId = @v_fieldName4Vc and bidId = @v_fieldName5Vc
END

If @v_fieldName1Vc='getTenderRightsName' 
Begin
	select top 1 * from (

select 1 as id ,title+' '+firstName+' '+lastName FieldValue1,convert(varchar(20),tm.userid) FieldValue2 from 
tbl_TenderRights t,tbl_TendererMaster tm
 where tenderId=@v_fieldName3Vc and t.userid=tm.userid 
 and t.userid in(select userid from tbl_tenderermaster where
  (companyid=(select companyId from tbl_TendererMaster where userId=@v_fieldName2Vc) ))
and isCurrent='yes'
union
select 2 as id ,title+' '+firstName+' '+lastName FieldValue1,convert(varchar(20),userid) FieldValue2 from tbl_tenderermaster where
userId=@v_fieldName2Vc
and isAdmin='yes'

 

)a order by id 
End


IF @v_fieldName1Vc = 'evalComMemberForReport'  
BEGIN
	--SELECT distinct em.employeeName as FieldValue1, convert(varchar(25),em.userId) as FieldValue2
	--FROM   tbl_EmployeeMaster em INNER JOIN
 --                     tbl_EvalMemStatus ems ON em.userId = ems.evalBy
 --                     Where tenderid = @v_fieldName2Vc and ems.userId = @v_fieldName3Vc
 
 if((select configType from tbl_EvalConfig where tenderId=@v_fieldName2Vc)='ind')
begin
 SELECT distinct em.employeeName  as FieldValue1, convert(varchar(25),em.userId) as FieldValue2
	FROM tbl_EmployeeMaster em INNER JOIN
                      tbl_EvalMemStatus ems ON em.userId = ems.evalBy
                      Where tenderid = @v_fieldName2Vc and ems.userId = @v_fieldName3Vc
	and evalBy in (select sentBy from tbl_EvalSentQueToCp where tenderId=@v_fieldName2Vc and sentFor='evaluation')
	
 Union
	SELECT distinct em.employeeName as FieldValue1, convert(varchar(25),em.userId) as FieldValue2
	FROM tbl_EmployeeMaster em 
	INNER JOIN tbl_EvalMemStatus ems ON em.userId = ems.evalBy
	INNER JOIN tbl_Committee c On c.tenderId=ems.tenderId
	INNER JOIN tbl_CommitteeMembers cm On c.committeeId=cm.committeeId                      
    Where 
		c.committeStatus='approved' And c.committeeType in ('TEC','PEC')
		And cm.memberRole='cp' and cm.userId=ems.evalBy
        And c.tenderId = @v_fieldName2Vc and ems.userId = @v_fieldName3Vc
end
else
begin
 SELECT distinct em.employeeName  as FieldValue1, convert(varchar(25),em.userId) as FieldValue2
	FROM tbl_EmployeeMaster em INNER JOIN
                      tbl_EvalMemStatus ems ON em.userId = ems.evalBy
                      Where tenderid = @v_fieldName2Vc and
                       ems.userId = @v_fieldName3Vc
	and evalBy in (select sentBy from tbl_EvalSentQueToCp
	 where tenderId=@v_fieldName2Vc and sentFor='evaluation')
	 and evalBy=(select tecMemberId from tbl_EvalConfig where tenderId=@v_fieldName2Vc)
	
end 


 
END


IF @v_fieldName1Vc = 'GetEvalBidderClarificationStatus'
BEGIN
	select isClarificationComp as FieldValue1 
	from dbo.tbl_EvalBidderResp 
	where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc
		and evalstatus=@v_fieldName4Vc
		--SELECT CONVERT(VARCHAR(20), tenderId) as FieldValue1 FROM dbo.tbl_EvalBidderResp 
		--WHERE tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc
END


IF @v_fieldName1Vc = 'GetEvalCPClarificationExist' -- For EvalClari.jsp --> Evaluation module     -> Krish
BEGIN
		
		SELECT '1' as FieldValue1 FROM tbl_EvalCpMemClarification a
		inner join tbl_TenderForms b on a.tenderFormId=b.tenderFormId
		inner join tbl_TenderSection ts On b.tenderSectionId=ts.tenderSectionId
		inner join tbl_TenderStd td On ts.tenderStdId=td.tenderStdId 
		Where a.userId=@v_fieldName2Vc and td.tenderId=@v_fieldName3Vc
		and a.memAnswerBy=@v_fieldName4Vc
		
END

IF @v_fieldName1Vc = 'getmarqueelisting'
BEGIN	
	
	--set @v_PagePerRecordN =10	
	set @v_RecordPerPage_inInt=10	
	--set @v_PageN=@v_fieldName2Vc
	set @v_Page_inInt=@v_fieldName2Vc
	
	/*Start: Dynamic Condition String*/
	set @v_ConditionString_Vc=''
	
	Select @v_ConditionString_Vc = 
	Case @v_fieldName4Vc
		When '0' Then ' And locationId=1'
		When '1' Then ''		
		Else ' And userTypeId=' + @v_fieldName4Vc
	End
	/*End: Dynamic Condition String*/
	
	print (@v_ConditionString_Vc)
	
	set @v_FinalQueryVc=
	'DECLARE @v_Reccountf int
	DECLARE @v_TotalPagef int
	SELECT @v_Reccountf = Count(*) From (
	SELECT * From (SELECT ROW_NUMBER() OVER (order by marqueeId desc) As Rownumber
	FROM tbl_MarqueeMaster Where marqueeId is not null '+@v_ConditionString_Vc+' 
	) AS DATA) AS TTT
	
	SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_RecordPerPage_inInt as varchar(10))+')
	SELECT *,cast(@v_TotalPagef as varchar(20)) as FieldValue5,cast(cast(@v_Reccountf as int) as varchar(20)) as FieldValue6 From (SELECT cast(ROW_NUMBER() OVER (order by marqueeId desc) as varchar(20)) As FieldValue1,cast(marqueeId as varchar(20)) as FieldValue2, marqueeText as FieldValue3, 
	REPLACE(CONVERT(VARCHAR(20), createdTime, 106), '' '', ''-'') +'' '' +Substring(CONVERT(VARCHAR(20),createdTime,108),1,5) as FieldValue4,
	REPLACE(CONVERT(VARCHAR(20), marqueeStartDt, 106), '' '', ''-'') +'' '' +Substring(CONVERT(VARCHAR(20),marqueeStartDt,108),1,5) as FieldValue7,
	REPLACE(CONVERT(VARCHAR(20), marqueeEndDt, 106), '' '', ''-'') +'' '' +Substring(CONVERT(VARCHAR(20),marqueeEndDt,108),1,5) as FieldValue8,
	CONVERT(varchar(50),userTypeId) as FieldValue9,
	CONVERT(varchar(50),locationId) as FieldValue10
	FROM tbl_MarqueeMaster Where marqueeId is not null '+@v_ConditionString_Vc+' 
	) AS DATA where FieldValue1 between  ('+cast(@v_Page_inInt as varchar(10))+' - 1) * '+cast(@v_RecordPerPage_inInt as varchar(10))+' + 1 
	AND '+cast(@v_Page_inInt as varchar(10))+' * '+cast(@v_RecordPerPage_inInt as varchar(10))+''
	
	print @v_FinalQueryVc								
	exec(@v_FinalQueryVc)

END

IF @v_fieldName1Vc = 'getmarqueelistWithPaging' -- for MarqueeListing.jsp
begin

/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName2Vc, @v_RecordPerPage_inInt=@v_fieldName3Vc
	
	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)	
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	
	
	/*Start: Dynamic Condition String*/
	set @v_ConditionString_Vc=''
	
	Select @v_ConditionString_Vc = 
	Case @v_fieldName4Vc
		When '0' Then ' And locationId=1'
		When '1' Then ''		
		Else ' And userTypeId=' + @v_fieldName4Vc
	End
	/*End: Dynamic Condition String*/

-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
	Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT( Distinct tm.marqueeId ) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')
	 FROM tbl_MarqueeMaster tm where marqueeId is not null ' + @v_ConditionString_Vc
	
		
	Select @v_CntQry_Vc='SELECT Count(Distinct tm.marqueeId) 
	FROM tbl_MarqueeMaster tm where marqueeId is not null ' + @v_ConditionString_Vc
		
		
	Print ('Count Query')	
	Print(@v_CntQry_Vc)
		
			/* START CODE: DYNAMIC QUERY TO GET RECORDS */
			
			/*
				FieldValue1: Payment Id
				FieldValue2: User Id of Bidder for which Payment is done
				FieldValue3: Email Id of Bidder for which Payment is done
				FieldValue4: Branch Maker - Name of Bank User who has done the Payment
				FieldValue5: Payment Status New Payment/Renewal
				FieldValue6: Date of Payment
			*/
			
		Select @v_FinalQueryVc=
	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int; 
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
	
	Select FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6, FieldValue7, FieldValue8, RowNumber
	From
	(Select Distinct
		convert(varchar(20),Tmp.marqueeId) as FieldValue1, 
		marqueeText as FieldValue2, 
		(select tl.locationName from tbl_locationmaster tl where tl.locationId = tm.locationId)as FieldValue3,
			CONVERT(varchar(50),@v_TotalPageCnt_Int) as FieldValue4, 
			CONVERT(varchar(50),@v_TotalRecordCnt_Int) as FieldValue5,
			CONVERT(varchar(50),Tmp.RowNumber) as FieldValue6,
			CONVERT(varchar(50),userTypeId) as FieldValue7,
			CONVERT(varchar(50),locationId) as FieldValue8,
			RowNumber
		From
		(Select RowNumber, marqueeId From
			(SELECT ROW_NUMBER() Over (Order by marqueeId desc) as RowNumber, marqueeId
				FROM tbl_MarqueeMaster tm where marqueeId is not null '+@v_ConditionString_Vc+'
		) B 				
		
		WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int) 		
		+ ' ) Tmp
		Inner Join 
		tbl_MarqueeMaster tm On Tmp.marqueeId=tm.marqueeId		
		) as A Order by RowNumber
		'		
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */
		
	
		PRINT(@v_FinalQueryVc)
		Exec (@v_FinalQueryVc)
  
end


IF @v_fieldName1Vc = 'ChkTenderAccess' -- to check whether tender is accessible or not
BEGIN	
	If (select docAccess from tbl_TenderDetails where tenderId=@v_fieldName3Vc)	='Limited'
	Begin
		-- // Limited Tender Case
			If (select docAvlMethod from tbl_TenderDetails where tenderId=@v_fieldName3Vc)='Package'
			Begin			
				-- // Package (tender) wise mapping
				Select Case 
					When Exists (
									select pkgLotId from tbl_LimitedTenders 
									where tenderId=@v_fieldName3Vc 									
									and (
											userId in
											(
												select distinct userId from tbl_TendererMaster 
												where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName5Vc and companyId!=1)
											)  								
											Or 
											userId = @v_fieldName5Vc
										)	
								)
					Then 'Yes'
					Else 'No'
					End as FieldValue1	
			End
			Else If (select docAvlMethod from tbl_TenderDetails where tenderId=@v_fieldName3Vc)='Lot'
			Begin
				-- // Lot wise mapping
				Select Case 
					When Exists (
									select pkgLotId from tbl_LimitedTenders 
									where tenderId=@v_fieldName3Vc and 
									pkgLotId = @v_fieldName4Vc
									and (
											userId in
											(
												select distinct userId from tbl_TendererMaster 
												where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName5Vc and companyId!=1)
											)  								
											Or 
											userId = @v_fieldName5Vc
										)	
								)
					Then 'Yes'
					Else 'No'
					End as FieldValue1	
			End
	End
	Else If (select docAccess from tbl_TenderDetails where tenderId=@v_fieldName3Vc) ='Open'
	Begin
		-- // Open Tender Case
		Select 'Yes' as FieldValue1
	End
END

IF @v_fieldName1Vc = 'getDocRelatedInfo' 
BEGIN	
select documentName as FieldValue1, isnull(manDocumentName,'') as FieldValue2,docHash as FieldValue3  
from  
(select cd.*,bd.formId,bd.manDocId from tbl_BidDocuments bd,tbl_CompanyDocuments cd
where bd.companyDocId=cd.companyDocId and tenderId=@v_fieldName2Vc and bd.formId =@v_fieldName3Vc and userId=@v_fieldName4Vc )a
left outer join( select tenderFormId,tenderFormDocId,documentName as manDocumentName from tbl_TenderMandatoryDoc where tenderFormId =@v_fieldName3Vc) b
on a.formId=b.tenderFormId and a.manDocId=b.tenderFormDocId  
END

IF @v_fieldName1Vc = 'getFormNameByTenderId' 
BEGIN	
select distinct tf.formName as FieldValue1,convert(varchar(50),tf.tenderFormId) as FieldValue2 from tbl_TenderBidForm tbf,tbl_TenderForms tf 
where tbf.tenderFormId = tf.tenderFormId and tbf.tenderId = @v_fieldName2Vc
END


IF @v_fieldName1Vc = 'gettenderlotbytenderidForViewPayment'
BEGIN
	
	Select @v_fieldName10Vc = 
		Case
			When (@v_fieldName3Vc='df')
			Then 'Document Fees'
			When (@v_fieldName3Vc='ts')
			Then 'Tender Security'
			When (@v_fieldName3Vc='ps')
			Then 'Performance Security'
		End	
	
	If @v_fieldName3Vc='ts' Or @v_fieldName3Vc='ps'
	Begin
		SELECT lotNo as FieldValue1, 
		lotDesc as FieldValue2, 
		CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
		CONVERT(VARCHAR(50), tp.tenderPaymentId) as FieldValue4,
		dbo.f_initcap(tp.status) as FieldValue5,
		tp.tenderPaymentId
			FROM  dbo.tbl_TenderLotSecurity tls 
			inner join tbl_TenderPayment tp On tls.tenderId=tp.tenderId And tls.appPkgLotId=tp.pkgLotId
			WHERE tls.tenderId = @v_fieldName2Vc
			And tp.userId=@v_fieldName4Vc
			And tp.paymentFor=@v_fieldName10Vc 				
			And isVerified='Yes'
			And isLive='Yes'
		Order by tp.tenderPaymentId Desc	
	End
	Else
	Begin
		--SELECT @v_docAvlMethod_Vc = docAvlMethod FROM dbo.tbl_TenderDetails WHERE tenderId = @v_fieldName2Vc
		
		IF (select docAvlMethod FROM dbo.tbl_TenderDetails WHERE tenderId = @v_fieldName2Vc) = 'Lot'
		BEGIN		
			SELECT lotNo as FieldValue1, 
			lotDesc as FieldValue2, 
			CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
			CONVERT(VARCHAR(50), tp.tenderPaymentId) as FieldValue4,
			dbo.f_initcap(tp.status) as FieldValue5,
			tp.tenderPaymentId  
			FROM  dbo.tbl_TenderLotSecurity tls 
			inner join tbl_TenderPayment tp On tls.tenderId=tp.tenderId And tls.appPkgLotId=tp.pkgLotId
			WHERE tls.tenderId = @v_fieldName2Vc
			And tp.userId=@v_fieldName4Vc
			And tp.paymentFor=@v_fieldName10Vc 				
			And isVerified='Yes'
			And isLive='Yes'
			Order by tp.tenderPaymentId Desc
		END
		ELSE
		BEGIN		
			SELECT 'Package' as FieldValue1,
			'' as FieldValue2,
			'' as FieldValue3, 
			CONVERT(VARCHAR(50), tp.tenderPaymentId) as FieldValue4,
			dbo.f_initcap(tp.status) as FieldValue5,
			tp.tenderPaymentId  
			FROM  tbl_TenderPayment tp 
			WHERE tp.tenderId = @v_fieldName2Vc
			And tp.userId=@v_fieldName4Vc
			And tp.paymentFor=@v_fieldName10Vc 				
			And isVerified='Yes'
			And isLive='Yes'
			Order by tp.tenderPaymentId Desc
			
		END

	End

	
END

IF @v_fieldName1Vc = 'getBidModificationInfo'
BEGIN
Select CONVERT(varchar(50), bidModId) as FieldValue1, 
bidStatus as FieldValue2, 
comments as FieldValue3, 
REPLACE(CONVERT(VARCHAR(20),bidModDt , 106), ' ', '-') + ' ' + Substring(CONVERT(VARCHAR(20),bidModDt,108),1,5) as FieldValue4
from tbl_BidModification
where bidStatus=@v_fieldName2Vc  and tenderId=@v_fieldName3Vc and userId=@v_fieldName4Vc 
END


IF @v_fieldName1Vc = 'getTenSecExtLinkStatusMore'     
BEGIN
	/*
		@v_fieldName2Vc = Tender Id
		@v_fieldName3Vc = UserId (Bidder User Id) 
		@v_fieldName4Vc = Payment Id
	*/
	
	--If Exists (select tenderValAcceptId from tbl_TenderValAcceptance 
	--	where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc And status='approved' )	
	--Begin
	--	-- Extension Exists Case
	--	If (Select extValidityRef from tbl_TenderPayment Where tenderPaymentId=@v_fieldName4Vc) 
	--		= 
	--	  (select top 1 valExtDtId from tbl_TenderValAcceptance where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc And status='approved' Order by tenderValAcceptDt desc )
	--	Begin
	--		-- Payment already made for Extension
	--		Select 'No' as FieldValue1
	--	End
	--	Else
	--	Begin
	--		-- Payment pending for new Extension : show Extension link
						
	--		select top 1 'Yes' as FieldValue1, 
	--		Convert(varchar(50),valExtDtId) as  FieldValue2
	--		from tbl_TenderValAcceptance 
	--		where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc And status='approved' 
	--		Order by tenderValAcceptDt desc
			
	--	End
	--End
	--Else
	--Begin
	--	-- No Extension Case
	--	Select 'No' as FieldValue1
	--End
	
	
	--If Exists (select tenderValAcceptId from tbl_TenderValAcceptance where tenderId=@v_fieldName4Vc And userId=@v_fieldName5Vc And status='approved' )	
	If Exists (select tenderValAcceptId from tbl_TenderValAcceptance where valExtDtId=@v_fieldName3Vc And tenderId=@v_fieldName4Vc And userId=@v_fieldName5Vc And status='approved' )	
	Begin
		-- Extension Exists Case
		If @v_fieldName2Vc = 0
		Begin
		If	(Select lastValAcceptDt 
				from tbl_TenderValidityExtDate 
					Where valExtDtId=@v_fieldName3Vc 
					And extStatus='Approved' 
					And tenderSecNewDt is not null 
					and tenderSecNewDt <>'1900-01-01'
					)
			
			>
			(select securityLastDt from tbl_TenderDetails where tenderId=@v_fieldName4Vc)
			--(select tenderSecurityDt from tbl_TenderDetails where tenderId=@v_fieldName4Vc)
			
		Begin
			select 'Yes' as FieldValue1
		End		
		Else 
		Begin
			select 'No' as FieldValue1
		End
	 End
		Else If @v_fieldName2Vc > 0
		Begin
	 
		If	(Select lastValAcceptDt 
				from tbl_TenderValidityExtDate 
					Where valExtDtId=@v_fieldName3Vc 
					And extStatus='Approved' 
					And tenderSecNewDt is not null 
					and tenderSecNewDt <>'1900-01-01'
					)
			
			>
			
			(Select lastValAcceptDt 
				from tbl_TenderValidityExtDate 
					Where valExtDtId=@v_fieldName2Vc 
					And extStatus='Approved' 
					And tenderSecNewDt is not null 
					and tenderSecNewDt <>'1900-01-01'
					)
		Begin
			select 'Yes' as FieldValue1
		End		
		Else 
		Begin
			select 'No' as FieldValue1
		End
	 
	 End
	End
	Else
	Begin
		-- No Extension Case
		Select 'No' as FieldValue1
	End
	
END 

IF @v_fieldName1Vc='getTenSecLastDt'
BEGIN
	-- // To Get the Tender Security Last Date
	
	/*
		@v_fieldName2Vc= Action : Edit/Extend
		@v_fieldName3Vc= Tender Id
		@v_fieldName4Vc= Lot Id
		@v_fieldName5Vc=UserId Bidder UserId)
		@v_fieldName6Vc=Payment Id (in case of Edit)
		@v_fieldName7Vc=Tender Security Extension Id in case of Extension Payment
		
	*/
	
	If @v_fieldName2Vc='edit' And @v_fieldName6Vc is not null And @v_fieldName6Vc<>'' And @v_fieldName6Vc<>'0'
	Begin
		Select 
		IsNull(CONVERT(varchar(10), tp.instValidUpto, 103),'null') as FieldValue1,
		IsNull(Replace(CONVERT(varchar(20), tp.instValidUpto, 106),' ','-'),'null') as FieldValue2			
		From tbl_TenderPayment tp Where tenderPaymentId=@v_fieldName6Vc	
	End
	Else
	Begin
	
		If Exists (Select tenderValAcceptId from tbl_TenderValAcceptance where tenderId=@v_fieldName3Vc And userId=@v_fieldName5Vc And valExtDtId=@v_fieldName7Vc)
		Begin
			-- // Extension Case
			Select 
			IsNull(CONVERT(varchar(10), dbo.f_gettendersecuritydate(@v_fieldName3Vc), 103),'null') as FieldValue1,
			IsNull(Replace(CONVERT(varchar(20), dbo.f_gettendersecuritydate(@v_fieldName3Vc), 106),' ','-'),'null') as FieldValue2			
			From tbl_TenderLotSecurity A
			Inner join tbl_TenderDetails B On A.TenderId=B.TenderId			
			Where A.tenderId=@v_fieldName3Vc And A.appPkgLotId=@v_fieldName4Vc 
		End
		Else
		Begin
			Select 
			IsNull(CONVERT(varchar(10), dbo.f_gettendersecuritydate(@v_fieldName3Vc), 103),'null') as FieldValue1,
			IsNull(Replace(CONVERT(varchar(20), dbo.f_gettendersecuritydate(@v_fieldName3Vc), 106),' ','-'),'null') as FieldValue2			
			From tbl_TenderLotSecurity A
			Inner join tbl_TenderDetails B On A.TenderId=B.TenderId			
			Where A.tenderId=@v_fieldName3Vc And A.appPkgLotId=@v_fieldName4Vc 			
		
		End
	
		--Select 
		--IsNull(CONVERT(varchar(10), dbo.f_gettendersecuritydate(@v_fieldName3Vc), 103),'null') as FieldValue1,
		--IsNull(Replace(CONVERT(varchar(20), dbo.f_gettendersecuritydate(@v_fieldName3Vc), 106),' ','-'),'null') as FieldValue2			
		--From tbl_TenderLotSecurity A
		--	Inner join tbl_TenderDetails B On A.TenderId=B.TenderId			
		--	Where A.tenderId=@v_fieldName3Vc And A.appPkgLotId=@v_fieldName4Vc 			
	End
	
	
END

IF @v_fieldName1Vc = 'getGrandSumForOfficer'     -- For tenderer/BidPreperation.jsp
BEGIN

select CONVERT(varchar(2000),formName) as FieldValue1,cellValue as FieldValue2  from (select tg.cellId,tg.tendertableid,formName from tbl_TenderGrandSum g,tbl_TenderGrandSumDetail tg,tbl_TenderForms tf
where g.tenderSumId=tg.tenderSumId  and tenderId=@v_fieldName2Vc
and tf.tenderFormId=tg.tenderFormId)a
inner join (
select tb.tenderTableId,b.cellId,cellValue from tbl_TenderBidPlainData b,tbl_TenderBidTable tb,tbl_tenderbidform bf
where bf.bidId=tb.bidId and tb.bidTableId=b.bidTableId 
and tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc )b
on a.tenderTableId=b.tenderTableId and a.cellId=b.cellId  

END 

IF @v_fieldName1Vc = 'getGrandSummaryLink'   
BEGIN
select Convert(varchar(50),COUNT(*)) as FieldValue1
from tbl_TenderGrandSumDetail tsg, tbl_TenderForms tf 
where tsg.tenderFormId = tf.tenderFormId
and tf.pkgLotId = @v_fieldName2Vc 
END


IF @v_fieldName1Vc = 'getPaidTendererListForPE'   
BEGIN

	If @v_fieldName3Vc is not null And @v_fieldName3Vc<>'' And @v_fieldName3Vc<>'0'
	Begin
		SELECT Distinct Convert(varchar(50),tenderPaymentId) as FieldValue1, 
				dbo.f_getbiddercompany(LM.userId) as FieldValue2,
				Convert(varchar(50),lm.userId) as FieldValue3, 				
				Convert(varchar(50),TM.tenderId) as FieldValue4, 
				Convert(varchar(50),pkgLotId) as FieldValue5, 
				Case paymentFor When 'Document Fees' Then 'df'
				When 'Tender Security' Then 'ts'
				When 'Performance Security' Then 'ps'
				When 'Bank Guarantee' Then 'bg'
				End as FieldValue6,
				paymentFor as FieldValue7,
				dbo.f_initCap(tp.status) as FieldValue8,
				tenderPaymentId,
				Convert(varchar(50),tp.extValidityRef) as FieldValue9,
				LM.registrationType as FieldValue10
			From tbl_TenderPayment TP
			Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
			Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
			Inner Join tbl_TenderPaymentVerification TPV On TP.tenderPaymentId=TPV.paymentId
			Inner Join tbl_TenderMaster TM On TP.tenderId=TM.tenderId
			Inner Join tbl_AppMaster APP On TM.appId=APP.appId
			Inner Join tbl_EmployeeMaster EM On APP.employeeId=EM.employeeId
		WHERE 
			TP.tenderId=@v_fieldName2Vc
			And TP.pkgLotId=@v_fieldName3Vc
			And TP.paymentFor=@v_fieldName4Vc
			And EM.userId=@v_fieldName5Vc 
			And isVerified='Yes'
			And isLive='Yes'
			and TP.tenderId in 
				(
				select distinct tenderId from tbl_Committee c
				inner join tbl_CommitteeMembers cm on c.committeeId=cm.committeeId
				where c.committeeType in ('toc','poc')
				and c.committeStatus='approved'
				and cm.appStatus='approved'			
				)	
		Order by tenderPaymentId Desc
	End
	Else
	Begin
		SELECT Distinct Convert(varchar(50),tenderPaymentId) as FieldValue1, 
				dbo.f_getbiddercompany(LM.userId) as FieldValue2,
				Convert(varchar(50),lm.userId) as FieldValue3, 				
				Convert(varchar(50),TM.tenderId) as FieldValue4, 
				Convert(varchar(50),pkgLotId) as FieldValue5, 
				Case paymentFor When 'Document Fees' Then 'df'
				When 'Tender Security' Then 'ts'
				When 'Performance Security' Then 'ps'
				When 'Bank Guarantee' Then 'bg'
				End as FieldValue6,
				paymentFor as FieldValue7,
				dbo.f_initCap(tp.status) as FieldValue8,
				tenderPaymentId,
				Convert(varchar(50),tp.extValidityRef) as FieldValue9
			From tbl_TenderPayment TP
			Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
			Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
			Inner Join tbl_TenderPaymentVerification TPV On TP.tenderPaymentId=TPV.paymentId
			Inner Join tbl_TenderMaster TM On TP.tenderId=TM.tenderId
			Inner Join tbl_AppMaster APP On TM.appId=APP.appId
			Inner Join tbl_EmployeeMaster EM On APP.employeeId=EM.employeeId
		WHERE 
			TP.tenderId=@v_fieldName2Vc
			And TP.paymentFor=@v_fieldName4Vc
			And EM.userId=@v_fieldName5Vc 
			And isVerified='Yes'
			And isLive='Yes'
			and TP.tenderId in 
				(
				select distinct tenderId from tbl_Committee c
				inner join tbl_CommitteeMembers cm on c.committeeId=cm.committeeId
				where c.committeeType in ('toc','poc')
				and c.committeStatus='approved'
				and cm.appStatus='approved'			
				)	
		Order by tenderPaymentId Desc
	End



END


IF @v_fieldName1Vc = 'getCancelPaymentLinkStatus'   
BEGIN
	Select Case When td.submissionDt < GETDATE() Then 'Yes' Else 'No' End as FieldValue1,
	Case When Exists (select top 1 finalSubmissionId from tbl_FinalSubmission where tenderId=@v_fieldName4Vc and userId=@v_fieldName3Vc and bidSubStatus='finalsubmission') Then 'Yes' Else 'No' End as FieldValue2
	from tbl_TenderDetails td inner join
	tbl_TenderPayment tp on td.tenderId=tp.tenderId
	Where td.tenderId=@v_fieldName4Vc
	And tp.tenderPaymentId=@v_fieldName6Vc
END

IF @v_fieldName1Vc = 'getTenderDocForIndi'     -- TaherT
BEGIN	
select  isnull(manDocumentName,'-') as FieldValue1,documentName as FieldValue2,documentSize as FieldValue3,convert(varchar(20),companyDocId ) as FieldValue4 , convert(varchar(20),userId) as FieldValue5
from  
(select cd.*,bd.formId,bd.manDocId,bd.userId,bd.bidDocId from tbl_BidDocuments bd,tbl_CompanyDocuments cd
where bd.companyDocId=cd.companyDocId and tenderId=@v_fieldName2Vc and bd.formId =@v_fieldName3Vc and userId=@v_fieldName4Vc )a
left outer join( select tenderFormId,tenderFormDocId,documentName as manDocumentName from tbl_TenderMandatoryDoc where tenderFormId =@v_fieldName3Vc) b
on a.formId=b.tenderFormId and a.manDocId=b.tenderFormDocId  
order by bidDocId
END



IF @v_fieldName1Vc = 'SubContractInvAppReq' -- Nishith Raval
BEGIN

select  distinct 
       convert(varchar(50),t.tenderId)  as FieldValue1
      ,convert(varchar(250),cm.companyName) as FieldValue2
      ,REPLACE(CONVERT(VARCHAR(11),lastAcceptDt, 106), ' ', '-') as FieldValue3
      ,convert(varchar(50),[remarks]) as FieldValue4
       ,REPLACE(CONVERT(VARCHAR(11),[invSentDt], 106), ' ', '-')  as FieldValue5
      , convert(varchar(250),cmm.companyName)  as FieldValue6
     ,convert(varchar(20),invAcceptStatus)  as FieldValue7,acceptComments  as FieldValue9,convert(varchar(20),invFromUserId)  as FieldValue10 
      from tbl_subcontracting t,tbl_TendererMaster tm,tbl_CompanyMaster cm,
tbl_TendererMaster ttm,tbl_CompanyMaster cmm
where t.invFromuserid=tm.userId and tm.companyId=cm.companyId
and tenderid=@v_fieldName2Vc and (invFromuserid=@v_fieldName3Vc or  invTouserid=@v_fieldName3Vc)
and ttm.userid=t.invFromuserid and ttm.companyid=cmm.companyId 
and invAcceptStatus='Approved'
 and pkgLotId =
(
Select Case 
	When @v_fieldName4Vc !='0'
	Then @v_fieldName4Vc
	else  @v_fieldName4Vc
	 
	End
 
)

END

IF @v_fieldName1Vc = 'SubContract' -- Nishith Raval
BEGIN

select  distinct 
       convert(varchar(50),t.tenderId)  as FieldValue1
      ,[dbo].[f_getbiddercompany](invToUserId) as FieldValue2
      , REPLACE(CONVERT(VARCHAR(11),lastAcceptDt, 106), ' ', '-')  as FieldValue3
    
      ,convert(varchar(1000),[remarks]) as FieldValue4
       ,  REPLACE(CONVERT(VARCHAR(11),[invSentDt], 106), ' ', '-') 
        
      , convert(varchar(250),cmm.companyName)  as FieldValue6
     ,convert(varchar(20),invAcceptStatus)  as FieldValue7 ,
     acceptComments  as FieldValue9
      from tbl_subcontracting t,tbl_TendererMaster tm,tbl_CompanyMaster cm,
tbl_TendererMaster ttm,tbl_CompanyMaster cmm
where t.invToUserId=tm.userId and tm.companyId=cm.companyId
and tenderid=@v_fieldName2Vc and invFromUserId=@v_fieldName3Vc
and ttm.userid=t.invFromuserid and ttm.companyid=cmm.companyId 
and pkgLotId =
(
Select Case 
	When @v_fieldName4Vc !='0'
	Then @v_fieldName4Vc
	else  @v_fieldName4Vc
	End
 
) 

END

IF @v_fieldName1Vc = 'SubContractApprovedReq' -- Nishith Raval
BEGIN

select  distinct 
       convert(varchar(50),t.tenderId)  as FieldValue1
      ,[dbo].[f_getbiddercompany](invToUserId) as FieldValue2
      ,REPLACE(CONVERT(VARCHAR(11),lastAcceptDt, 106), ' ', '-') as FieldValue3
      ,convert(varchar(1000),[remarks]) as FieldValue4
       ,REPLACE(CONVERT(VARCHAR(11),[invSentDt], 106), ' ', '-')  as FieldValue5
      , convert(varchar(250),cmm.companyName)  as FieldValue6
     ,convert(varchar(20),invAcceptStatus)  as FieldValue7,
     convert(varchar(20),subConId )  as FieldValue8
     ,acceptComments  as FieldValue9,lm.emailid  as FieldValue10
      from tbl_subcontracting t,tbl_TendererMaster tm,tbl_CompanyMaster cm,
tbl_TendererMaster ttm,tbl_CompanyMaster cmm,tbl_loginmaster lm
where t.invToUserId=tm.userId and tm.companyId=cm.companyId and lm.userId=ttm.userId
and tenderid=@v_fieldName2Vc and (invTouserid=@v_fieldName3Vc )
and ttm.userid=t.invFromuserid and ttm.companyid=cmm.companyId 
and pkgLotId =
(
Select Case 
	When @v_fieldName4Vc !='0'
	Then @v_fieldName4Vc
	else  @v_fieldName4Vc
	 
	End
 
) 

END


IF @v_fieldName1Vc = 'GetTenderAAHopeAo' -- Nishith Raval
BEGIN
	IF @v_fieldName2Vc='AA'
	BEGIN
/*	
            SELECT  et.employeeName+', ['+dm.designationName+' at '+om.officeName+']' as FieldValue1,
                    convert(varchar(20),et.userId) FieldValue2,
                    convert(varchar(20),et.govUserId) FieldValue3
            FROM    tbl_TenderDetails td,
                    tbl_EmployeeTrasfer et,
                    tbl_EmployeeRoles er,
                    tbl_EmployeeOffices eo,
                    tbl_OfficeMaster om,
                    tbl_DesignationMaster dm
            WHERE   td.approvingAuthId = et.userId
                    and er.employeeId = et.employeeId
                    and eo.employeeId = er.employeeId
                    and eo.officeId = om.officeId
                    and er.departmentid = dm.departmentid
                    and isCurrent='yes'
                    and td.tenderId = @v_fieldName3Vc
*/
	   SELECT  et.employeeName+', ['+dm.designationName+' at '+om.officeName+']' as FieldValue1,
                    convert(varchar(20),et.userId) FieldValue2,
                    convert(varchar(20),et.govUserId) FieldValue3
            FROM    tbl_TenderDetails td
					INNER JOIN tbl_EmployeeTrasfer et ON et.userId = td.approvingAuthId
								AND isCurrent = 'yes'
					INNER JOIN tbl_EmployeeOffices eo ON eo.employeeId  = et.employeeId 
					INNER JOIN tbl_DesignationMaster dm on dm.designationId =  eo.designationId
								--AND dm.departmentid = td.departmentId
					INNER JOIN tbl_OfficeMaster om ON om.departmentId = dm.departmentId
								AND om.officeId = eo.officeId 
            WHERE	tenderId = @v_fieldName3Vc
                    AND et.govUserId = (SELECT MAX (govUserId) FROM tbl_EmployeeTrasfer where employeeId = eo.employeeId)                  
	END
-- Secretary
	IF @v_fieldName2Vc='Secretary'
	BEGIN
/* Only for Minister Secretary
		SELECT	et.employeeName AS FieldValue1,
				CONVERT(VARCHAR(20),et.userId) FieldValue2,
				CONVERT(VARCHAR(20),et.govUserId) FieldValue3
		FROM	tbl_EmployeeRoles er
				INNER JOIN tbl_EmployeeTrasfer et on et.employeeId = er.employeeId
						AND isCurrent='yes'
				INNER JOIN tbl_EmployeeOffices eo on eo.employeeId = er.employeeId
						AND eo.officeId IN	(	SELECT	officeId
												FROM	tbl_EmployeeOffices
												WHERE	employeeId IN (	SELECT employeeId
																		FROM	tbl_EmployeeMaster
																		WHERE	userId IN (	SELECT	approvingAuthId
																							FROM	tbl_TenderDetails
																							WHERE	tenderId = @v_fieldName3Vc
																						)
																	)
											)
		WHERE	er.procurementRoleId = 2
*/
-- TO get Secretary From all Division as well as Ministry Level
		SELECT	et.employeeName+', ['+dm.designationName +' at '+om.officeName+']' AS FieldValue1,
                        CONVERT(VARCHAR(20),et.userId) AS FieldValue2,
                        CONVERT(VARCHAR(20),et.govUserId) AS FieldValue3
		FROM	tbl_EmployeeRoles er
                        INNER JOIN tbl_EmployeeTrasfer et on et.employeeId = er.employeeId
                                        AND isCurrent='yes'
                        INNER JOIN tbl_EmployeeOffices eo on eo.employeeId = er.employeeId
                        INNER JOIN tbl_DesignationMaster dm on dm.designationId = eo.designationId
                        INNER JOIN tbl_OfficeMaster OM ON om.officeId = eo.officeId
                                   AND om.departmentId IN (    SELECT  dm.departmentId
																FROM    tbl_DepartmentMaster dm
																		INNER JOIN tbl_TenderDetails td on td.tenderId = @v_fieldName3Vc
																					AND (	dm.departmentName = td.ministry OR
																							dm.departmentName = td.division OR
																							td.departmentId IN (dm.departmentId,dm.parentDepartmentId )				
																						)
															)
		WHERE	er.procurementRoleId = 2
	END
--Hope
	if @v_fieldName2Vc='HOPE'
	begin
/*			
			select  et.employeeName+', ['+ d.designationName+' at '+om.officeName+']' as FieldValue1,
                    convert(varchar(20),et.userId) FieldValue2,
                    convert(varchar(20),et.govUserId) FieldValue3
            from    tbl_TenderDetails td,
                    tbl_EmployeeOffices dm,
                    tbl_EmployeeTrasfer et ,
                    tbl_EmployeeRoles er,
                    tbl_OfficeMaster om,
                    tbl_DesignationMaster d
            where   td.officeId=dm.officeId
                    and dm.employeeId=et.employeeId
                    and om.officeId = dm.officeId
                    and td.tenderId = @v_fieldName3Vc
                    and et.employeeId=er.employeeId
                    and d.designationId = dm.designationId
                    and er.procurementRoleId=6
                    and isCurrent='yes'
*/
		SELECT	 et.employeeName+', ['+ dm.designationName+' at '+om.officeName+']' as FieldValue1,
				CONVERT(VARCHAR(20),et.userId) AS FieldValue2,
				CONVERT(VARCHAR(20),et.govUserId) AS FieldValue3
		FROM	tbl_TenderDetails td		
				INNER JOIN tbl_EmployeeRoles er ON er.departmentId= td.departmentId
							AND procurementRoleId = 6
				INNER JOIN tbl_EmployeeTrasfer et ON et.employeeId = er.employeeId
							AND isCurrent = 'Yes'
				INNER JOIN tbl_EmployeeOffices eo ON eo.employeeId = er.employeeId
				INNER JOIN tbl_DesignationMaster dm on dm.designationId = eo.designationId
				INNER JOIN tbl_OfficeMaster om ON om.officeId = eo.officeId
		WHERE td.tenderId = @v_fieldName3Vc
	end
--AO
	if @v_fieldName2Vc='AO'
	begin
/*			
			select  et.employeeName+', ['+ d.designationName+' at '+om.officeName+']' as FieldValue1,
                    convert(varchar(20),et.userId) FieldValue2,
                    convert(varchar(20),et.govUserId) FieldValue3
            from    tbl_TenderDetails td,
                    tbl_EmployeeOffices dm,
                    tbl_EmployeeTrasfer et ,
                    tbl_EmployeeRoles er,
                    tbl_OfficeMaster om,
                    tbl_DesignationMaster d
            where   td.officeId=dm.officeId
                    and  dm.employeeId=et.employeeId
                    and om.officeId = dm.officeId
                    and td.tenderId = @v_fieldName3Vc
                    and et.employeeId=er.employeeId
                    and d.designationId = dm.designationId
                    and er.procurementRoleId=18
                    and isCurrent='yes'
*/                    
		SELECT	et.employeeName+', ['+ dm.designationName+' at '+om.officeName+']' as FieldValue1,
				CONVERT(VARCHAR(20),et.userId) FieldValue2,
				CONVERT(VARCHAR(20),et.govUserId) FieldValue3
		FROM	tbl_TenderDetails td
				INNER JOIN tbl_EmployeeRoles er ON er.departmentId = td.departmentid
							AND procurementRoleId IN (	SELECT	procurementRoleId
														FROM	tbl_ProcurementRole
														WHERE	procurementRole IN ('Authorized Officer','AO')
													)
				INNER JOIN tbl_EmployeeTrasfer et ON et.employeeId = er.employeeId
							AND et.govUserId = (	SELECT	MAX (govUserId)	
													FROM	tbl_EmployeeTrasfer 
													WHERE	employeeId = er.employeeId
												)
				INNER JOIN tbl_EmployeeOffices eo ON eo.employeeId = er.employeeId
				--INNER JOIN tbl_OfficeMaster om ON om.officeId = td.officeId	
				--			AND om.departmentId = td.departmentId
				INNER JOIN tbl_OfficeMaster om ON om.departmentId = td.departmentId
							AND om.officeId = eo.officeId				
				INNER JOIN tbl_DesignationMaster dm ON dm.designationId= eo.designationId
							AND dm.departmentid = om.departmentId
						--AND transferDt IN (SELECT MAX (transferDt) FROM tbl_EmployeeTrasfer WHERE employeeId = er.employeeid)
		WHERE	td.tenderId =  @v_fieldName3Vc
		ORDER BY et.employeeId 
	END
END

IF @v_fieldName1Vc='getTenderPays'
BEGIN
--print('start')
If @v_fieldName4Vc is Not Null And @v_fieldName4Vc <>'0' And @v_fieldName4Vc <> '0'
Begin
	Select Distinct
		Convert(varchar(50),TP.tenderPaymentId) as FieldValue1,
			Convert(varchar(50),LM.userId) as FieldValue2, 
			LM.emailId as FieldValue3,
			TP.isVerified as FieldValue11,
			dbo.f_getbiddercompany(LM.userId) as  FieldValue4,			
			dbo.f_initcap(TP.status) as FieldValue5,
			REPLACE(CONVERT(VARCHAR(11),createdDate, 106), ' ', '-')+ ' ' + Substring(CONVERT(VARCHAR(30),createdDate,108),1,5) as FieldValue6,						
			paymentFor as FieldValue7,			
			Convert(varchar(50),tenderId) as FieldValue8,
			Convert(varchar(50),pkgLotId) as FieldValue9,
			Case paymentFor 
				When 'Document Fees' Then 'df' 
				When 'Tender Security' Then 'ts' 
				When 'Performance Security' Then 'ps' 
				End as FieldValue10
			
			From tbl_TenderPayment TP
			inner join tbl_LoginMaster LM ON TP.userID=LM.userID
			Where TP.tenderId=@v_fieldName3Vc And TP.pkgLotId=@v_fieldName4Vc
			And isLive='Yes' And TP.createdBy= @v_fieldName5Vc
			And TP.paymentFor=@v_fieldName2Vc
End
Else
Begin

	Select Distinct
		Convert(varchar(50),TP.tenderPaymentId) as FieldValue1,
			Convert(varchar(50),LM.userId) as FieldValue2, 
			LM.emailId as FieldValue3,
			dbo.f_getbiddercompany(LM.userId) as  FieldValue4,			
			dbo.f_initcap(TP.status) as FieldValue5,
			TP.isVerified as FieldValue11,
			REPLACE(CONVERT(VARCHAR(11),createdDate, 106), ' ', '-')+ ' ' + Substring(CONVERT(VARCHAR(30),createdDate,108),1,5) as FieldValue6,						
			paymentFor as FieldValue7,			
			Convert(varchar(50),tenderId) as FieldValue8,
			Convert(varchar(50),pkgLotId) as FieldValue9,
			Case paymentFor 
				When 'Document Fees' Then 'df' 
				When 'Tender Security' Then 'ts' 
				When 'Performance Security' Then 'ps' 
				End as FieldValue10
			From tbl_TenderPayment TP
			inner join tbl_LoginMaster LM ON TP.userID=LM.userID
			Where TP.tenderId=@v_fieldName3Vc
			And isLive='Yes' And TP.createdBy= @v_fieldName5Vc
			And TP.paymentFor=@v_fieldName2Vc

End		
END



IF @v_fieldName1Vc = 'getGrandSumOpening'     -- For tenderer/BidPreperation.jsp
BEGIN

select CONVERT(varchar(2000),formName) as FieldValue1,cellValue as FieldValue2  from (
select tg.cellId,tg.tendertableid,formName from tbl_TenderGrandSum g,tbl_TenderGrandSumDetail tg,tbl_TenderForms tf
where g.tenderSumId=tg.tenderSumId  and tenderId=@v_fieldName2Vc
and tf.tenderFormId=tg.tenderFormId and g.pkgLotId=@v_fieldName4Vc)a
inner join (
select DISTINCT  tb.tenderTableId,b.tenderCelId,cellValue from tbl_TenderBidPlainData b,tbl_TenderBidTable tb,tbl_tenderbidform bf
where bf.bidId=tb.bidId and tb.bidTableId=b.bidTableId 
and tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc )b
on a.tenderTableId=b.tenderTableId and a.cellId=b.tenderCelId

 
END

IF @v_fieldName1Vc = 'getUserForFinalSubmission'     -- For tenderer/BidPreperation.jsp
BEGIN
select case when 
tm.companyId!=1 then companyname else  title+' '+tm.firstName+' '+tm.lastName end as FieldValue1,Convert(varchar(50),fs.userId) as FieldValue2 from tbl_FinalSubmission fs, tbl_TendererMaster tm,tbl_CompanyMaster cm
where fs.userId = tm.userId and bidSubstatus = 'finalsubmission' and tenderId = @v_fieldName2Vc
and tm.companyId=cm.companyId
END

IF @v_fieldName1Vc = 'getComparativeGrandSumOpening'     -- For tenderer/BidPreperation.jsp
BEGIN

select CONVERT(varchar(2000),formName) as FieldValue1,cellValue as FieldValue2  from (
select tg.cellId,tg.tendertableid,formName from tbl_TenderGrandSum g,tbl_TenderGrandSumDetail tg,tbl_TenderForms tf
where g.tenderSumId=tg.tenderSumId  and tenderId=@v_fieldName2Vc
and tf.tenderFormId=tg.tenderFormId and g.pkgLotId=@v_fieldName4Vc and tf.tenderFormId = @v_fieldName5Vc)a 
inner join (
select tb.tenderTableId,b.tenderCelId,cellValue from tbl_TenderBidPlainData b,tbl_TenderBidTable tb,tbl_tenderbidform bf
where bf.bidId=tb.bidId and tb.bidTableId=b.bidTableId 
and tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc and bf.tenderFormId = @v_fieldName5Vc)b
on a.tenderTableId=b.tenderTableId and a.cellId=b.tenderCelId

 
END

IF @v_fieldName1Vc = 'getFormIdforCompReport'     -- For tenderer/BidPreperation.jsp
BEGIN

select CONVERT(varchar(2000),tenderFormId) as FieldValue1,CONVERT(varchar(2000),formName) as FieldValue2 from (
select tg.tenderFormId,tf.formName from tbl_TenderGrandSum g,tbl_TenderGrandSumDetail tg,tbl_TenderForms tf
where g.tenderSumId=tg.tenderSumId  and tenderId=@v_fieldName2Vc
and tf.tenderFormId=tg.tenderFormId and g.pkgLotId=@v_fieldName4Vc) a

END

IF @v_fieldName1Vc = 'evalBidderStatusRpt'  
BEGIN
	IF @v_fieldName3Vc='0'
	BEGIN
		SELECT dbo.f_getbiddercompany(ebs.userId) as FieldValue1, ebs.bidderStatus  as FieldValue2,ebs.bidderMarks as FieldValue3,ebs.passingMarks as FieldValue4,ebs.result as FieldValue5
		,(select convert(varchar(20),COUNT(em.roundId)) from tbl_EvalRoundMaster em where em.tenderId=@v_fieldName2Vc and em.reportType='L1') as FieldValue6		
		FROM  tbl_EvalBidderStatus ebs
		WHERE ebs.tenderId = @v_fieldName2Vc and ebs.pkgLotId = @v_fieldName3Vc
		order by convert(float,ebs.bidderMarks) desc
	END
	ELSE
	BEGIN
		SELECT dbo.f_getbiddercompany(ebs.userId) as FieldValue1, ebs.bidderStatus  as FieldValue2
		FROM  tbl_EvalBidderStatus ebs
		WHERE ebs.tenderId = @v_fieldName2Vc and ebs.pkgLotId = @v_fieldName3Vc
	END	
END

IF @v_fieldName1Vc = 'GetEvalFormFinalQuesCount' -- For EvalClari.jsp --> Evaluation module     -> Krish
BEGIN
		
		SELECT ISNULL(CONVERT(VARCHAR(15), COUNT(evalQueId)), '0') as FieldValue1 
		from dbo.tbl_EvalFormQues 
		where 				
	  tenderId = @v_fieldName2Vc 
	  and userId = @v_fieldName3Vc
		 and 
	  (
			quePostedBy = @v_fieldName5Vc
			Or 
			quePostedBy in (select distinct sentBy from tbl_EvalSentQueToCp 
			where tenderId=@v_fieldName2Vc and sentFor='question')
		)
		
END

If @v_fieldName1Vc='getEvaluationStatus' --Nishith
BEGIN
	select case when etc.evalStatus='Evaluation' then 'Comments Given' else 'Comments Pending' end  as FieldValue1 from tbl_EvalTSCComments etc
	where etc.formId=@v_fieldName2Vc and tenderId = @v_fieldName3Vc and pkgLotId = @v_fieldName4Vc and userId = @v_fieldName5Vc and tscUserId = @v_fieldName6Vc
END 

If @v_fieldName1Vc='isSubContractingAccepted'
BEGIN
select convert(varchar(20),COUNT(invToUserId)) as FieldValue1 from tbl_SubContracting where
 tenderId=@v_fieldName2Vc and 
 (invToUserId in(select userId from tbl_tenderermaster 
 where companyid in(select companyId from 
 tbl_TendererMaster where userId=@v_fieldName3Vc)) or invToUserId=@v_fieldName3Vc) and  invAcceptStatus='Approved'
End

IF @v_fieldName1Vc = 'getAppEmailIdForPublishPKGCron' 
BEGIN
	DECLARE	 @v_MailCronTable Table
			(
				email	VARCHAR(100),
				PackageNo VARCHAR(50),
				PackageDESC VARCHAR(2000),
				appID INT,
				packageID INT
			)

	DECLARE	@v_PackageDetail Table
			(
				rowID INT IDENTITY,
				PackageID INT,
				appID INT, 
				PackageNo VARCHAR(50),
				PackageDESC VARCHAR(2000),
				CPVCode VARCHAR(8000)		
			)

	DECLARE	@v_CPVCode VARCHAR(max),
			@v_RowCount INT,
			@v_Row INT,
			@v_PackageID INT,
			@v_appID INT, 
			@v_PackageNo VARCHAR(50),
			@v_PackageDESC VARCHAR(2000),
			@v_statement VARCHAR(MAX)
			
	INSERT @v_PackageDetail
	SELECT	distinct AAT.childId, AAT.appId,AP.packageNo,AP.packageDesc,AP.cpvCode
	FROM	tbl_AppAuditTrial AAT
			INNER JOIN	tbl_AppMaster AM ON AM.appId = AAT.appId
			INNER JOIN	tbl_AppPackages AP ON AP.appId = AM.appId
	WHERE	AAT.childId = AP.packageId
						AND (CONVERT(char(10),AAT.actionDate,111)) = (CONVERT(char(10),GETDATE()-1 ,111))
			
	SET @v_RowCount = @@ROWCOUNT
	SET @v_Row = 1				

	WHILE @v_Row <= @v_RowCount
	BEGIN 
						
		SELECT	@v_CPVCode = CPVCode,
				@v_PackageID = PackageID,
				@v_appID = appID, 
				@v_PackageNo = PackageNo,
				@v_PackageDESC = PackageDESC
		FROM	@v_PackageDetail 
		WHERE	rowID = @v_Row
		
		--PRINT 'Org '+@v_CPVCode	
		SET @v_CPVCode = REPLACE(@v_CPVCode,';','%'' or specialization like ''%')
		SET @v_CPVCode = '''%'+ @v_CPVCode  + '%'''

		--	PRINT '---> '+REPLACE(@v_CPVCode,';','%'' or specialization like ''%')
		
		
		SET @v_statement = 'SELECT	LM.emailId,'+ CONVERT(VARCHAR(1000),@v_appID) +',' + CONVERT(VARCHAR(1000),@v_PackageID) +' 
		FROM	tbl_TendererMaster TM
				INNER JOIN tbl_LoginMaster LM ON LM.userId = TM.userId
						AND	LM.status = ''approved''
						AND	LM.userTyperId=2
						AND LM.registrationType <> ''media''
						AND LM.registrationType=''individualconsultant''
		WHERE	specialization like ' + @v_CPVCode
		
		INSERT @v_MailCronTable (email, appID,packageID)
		EXEC (@v_statement)
		
		SET @v_statement = 'SELECT	LM.emailId,'+ CONVERT(VARCHAR(1000),@v_appID) +',' + CONVERT(VARCHAR(1000),@v_PackageID) +' 
		FROM	tbl_CompanyMaster CM
				INNER JOIN tbl_LoginMaster LM ON LM.userId = CM.userId
						AND	LM.status = ''approved''
						AND	LM.userTyperId=2
						AND LM.registrationType <> ''media''
						AND LM.registrationType=''contractor''
		WHERE	CM.specialization like '+ @v_CPVCode

		INSERT @v_MailCronTable	(email, appID,packageID)
		EXEC (@v_statement)
		
		UPDATE	@v_MailCronTable
		SET	PackageNo = @v_PackageNo,
			PackageDESC = @v_PackageDESC
		WHERE	appID = @v_appID
				AND PackageID = @v_PackageID

		SET @v_Row = @v_Row + 1
	END
	
	SELECT	email AS FieldValue1, PackageNo AS FieldValue2, PackageDESC AS FieldValue3, CONVERT(varchar(2000),appID) AS FieldValue4, CONVERT(varchar(2000),packageID) AS FieldValue5 FROM @v_MailCronTable ORDER BY email
End

IF @v_fieldName1Vc = 'getEmailIdForPublishTenderCron' 
BEGIN
	DECLARE	@v_TenderMailCronTable TABLE
			(
				email	VARCHAR(100),
				tenderID INT,
				refNo VARCHAR(50),
				closingDate DATETIME,
				tenderDesc VARCHAR(200),
				PeOfficeName VARCHAR(200),
				procurementMethod VARCHAR(50),
				companyName VARCHAR(200),
				eventType VARCHAR(50),
				TenderType VARCHAR(50)
			)

	DECLARE	@v_TenderDetail Table
			(
				rowID INT IDENTITY,
				tenderID INT,
				refNo VARCHAR(50),
				closingDate DATETIME,
				tenderDesce VARCHAR(200),
				PeOfficeName VARCHAR(200),
				CPVCode VARCHAR(max),
				procurementMethod VARCHAR(50),
				eventType VARCHAR(50)
			)

	DECLARE	@v_closingDate DATETIME,
			@v_PEOffice VARCHAR(200),
			@v_tenderID INT,
			@v_refNo VARCHAR(50),
			@v_tenderDesce VARCHAR(200),
			@v_procurementMethod VARCHAR(50),
			@v_eventType VARCHAR(50)
			
	INSERT @v_TenderDetail
	SELECT	TD.tenderId,TD.reoiRfpRefNo, TD.submissionDt, TD.tenderBrief,TD.peOfficeName,Replace(TD.cpvCode, 's''', 's'),TD.procurementMethod,TD.eventType
	FROM	tbl_TenderAuditTrail TAT
			INNER JOIN tbl_TenderDetails TD ON TD.tenderId = TAT.tenderId
	WHERE	(CONVERT(char(10),TAT.actionDate,111)) = (CONVERT(char(10),GETDATE()-1,111))

	SET @v_RowCount = @@ROWCOUNT
	SET @v_Row = 1				

	WHILE @v_Row <= @v_RowCount
	BEGIN 
						
		SELECT	@v_CPVCode = CPVCode,
				@v_PEOffice = PeOfficeName,
				@v_tenderID = tenderId, 
				@v_refNo = refNo,
				@v_closingDate = closingDate,
				@v_tenderDesce = tenderDesce,
				@v_procurementMethod = procurementMethod,
				@v_eventType = eventType
		FROM	@v_TenderDetail 
		WHERE	rowID = @v_Row

		IF exists (Select tenderId from tbl_LimitedTenders where tenderId = @v_tenderID)
		BEGIN
			 -- INCASE OF LTM TENDER 
			INSERT @v_TenderMailCronTable
			SELECT	LM.emailId, @v_tenderID, @v_refNo, @v_closingDate, @v_tenderDesce, @v_PEOffice, @v_procurementMethod, dbo.f_getbiddercompany(TM.userId),@v_eventType,'Limited'
			FROM	tbl_LimitedTenders LTM
					INNER JOIN	tbl_TendererMaster TM ON TM.userId = LTM.userId
					INNER JOIN	tbl_LoginMaster LM ON LM.userId = LTM.userId
								AND	LM.status = 'approved'
								AND	LM.userTyperId=2
					INNER JOIN tbl_CountryMaster CM ON CM.countryName = TM.country
			WHERE	LTM.tenderId =  @v_tenderID
			
						
			INSERT @v_TenderMailCronTable
			SELECT	LM.emailId, @v_tenderID, @v_refNo, @v_closingDate, @v_tenderDesce, @v_PEOffice, @v_procurementMethod,dbo.f_getbiddercompany(CompMst.userId) , @v_eventType,'Limited'
			FROM	tbl_LimitedTenders LTM
					INNER JOIN	tbl_CompanyMaster CompMst ON CompMst.userId = LTM.userId
					INNER JOIN	tbl_LoginMaster LM ON LM.userId = LTM.userId
								AND	LM.status = 'approved'
								AND	LM.userTyperId=2
								AND LM.emailId NOT IN (SELECT emailId FROM @v_TenderMailCronTable)
					INNER JOIN tbl_CountryMaster CM ON CM.countryName = CompMst.regOffCountry
			WHERE	LTM.tenderId =  @v_tenderID
			
			--INSERT @v_TenderMailCronTable (email, tenderID,companyName)
			--EXEC (@v_statement)
			--SET @v_tenderDesce = @v_tenderDesce
			
		END
		ELSE
		BEGIN
			/******** INCASE OF NON LTM TENDER ********/
			--PRINT 'Org '+@v_CPVCode	
			SET @v_CPVCode = REPLACE(@v_CPVCode,';','%'' or specialization like ''%')
			SET @v_CPVCode = '''%'+ @v_CPVCode  + '%'''

			--PRINT '---> '+REPLACE(@v_CPVCode,';','%'' or specialization like ''%')
			/******** INCASE OF RFA/RFP IC TENDER ********/
			IF exists (Select tenderId from tbl_TenderDetails where tenderId = @v_tenderID AND procurementMethod = 'IC') -- FOR RFA/RFP IC
			BEGIN
				SET @v_statement = 'SELECT	LM.emailId,'+ CONVERT(VARCHAR(1000),@v_tenderID) +', dbo.f_getbiddercompany(TM.userId),''open''
									FROM	tbl_TendererMaster TM
											INNER JOIN tbl_LoginMaster LM ON LM.userId = TM.userId
													AND	LM.status = ''approved''
													AND	LM.userTyperId=2
													AND LM.registrationType <> ''media''
													AND LM.registrationType=''individualconsultant''
									WHERE	TM.specialization <> '''' and TM.specialization like ' + @v_CPVCode

				INSERT @v_TenderMailCronTable (email, tenderID,companyName,TenderType)
				EXEC (@v_statement)
			END
			ELSE /******** INCASE OF OTHER THEN RFA/RFP IC TENDER ********/
			BEGIN
				SET @v_statement = 'SELECT	LM.emailId,'+ CONVERT(VARCHAR(1000),@v_tenderID) +', dbo.f_getbiddercompany(TM.userId),''open''
				FROM	tbl_TendererMaster TM
						INNER JOIN tbl_LoginMaster LM ON LM.userId = TM.userId
								AND	LM.status = ''approved''
								AND	LM.userTyperId=2
								AND LM.registrationType <> ''media''
								AND LM.registrationType=''individualconsultant''
				WHERE	TM.specialization <> '''' and TM.specialization like ' + @v_CPVCode

				INSERT @v_TenderMailCronTable (email, tenderID,companyName,TenderType)
				EXEC (@v_statement)
				
				SET @v_statement = 'SELECT	LM.emailId,'+ CONVERT(VARCHAR(1000),@v_tenderID) +', dbo.f_getbiddercompany(CM.userId),''open'' 
				FROM	tbl_CompanyMaster CM
						INNER JOIN tbl_LoginMaster LM ON LM.userId = CM.userId
								AND	LM.status = ''approved''
								AND	LM.userTyperId=2
								AND LM.registrationType <> ''media''
								AND LM.registrationType=''contractor''
				WHERE	CM.specialization like '+ @v_CPVCode
				
				INSERT @v_TenderMailCronTable (email, tenderID, companyName,TenderType)
				EXEC (@v_statement)
			END
		END		

		UPDATE	@v_TenderMailCronTable
		SET		tenderId = @v_tenderID , 
				refNo = @v_refNo,
				closingDate = @v_closingDate,
				tenderDesc = @v_tenderDesce,
				PeOfficeName = @v_PEOffice,
				procurementMethod= @v_procurementMethod,
				eventType = @v_eventType
		WHERE	tenderID = @v_tenderID
		
		SET @v_Row = @v_Row + 1
	END

	DELETE FROM @v_TenderMailCronTable WHERE PeOfficeName IS NULL 
	SELECT	email AS FieldValue1, CONVERT(varchar(2000),tenderID) AS FieldValue2, refNo AS FieldValue3, CONVERT(CHAR(17),closingDate,113) AS FieldValue4, tenderDesc AS FieldValue5, PeOfficeName AS FieldValue6, procurementMethod AS FieldValue7, companyName as FieldValue8, eventType AS FieldValue9, TenderType AS FieldValue10 FROM @v_TenderMailCronTable ORDER BY tenderID
END

IF @v_fieldName1Vc='GetCriteria4FormId'
Begin
	IF Exists (select top 1 esFormDetailId from tbl_EvalSerFormDetail where tenderFormId=@v_fieldName2Vc and userId=@v_fieldName3Vc and evalBy= @v_fieldName4Vc)
		Begin		
				select esc.mainCriteria as FieldValue1,esc.subCriteria as FieldValue2,tfd.ratingWeightage as FieldValue3,tfd.actualMarks as FieldValue4,convert(varchar(20),maxMarks) as FieldValue5
					from tbl_EvalSerFormDetail tfd inner join tbl_EvalServiceCriteria esc on tfd.subCriteriaId = esc.subCriteriaId
					 where tfd.tenderFormId=@v_fieldName2Vc and userId=@v_fieldName3Vc and tfd.evalBy= @v_fieldName4Vc																
		End
	Else
		Begin 
				select esc.mainCriteria as FieldValue1,esc.subCriteria as FieldValue2,'-' as FieldValue3,'-' as FieldValue4,convert(varchar(20),maxMarks) as FieldValue5
				from Tbl_EvalServiceForms esf
					inner join tbl_EvalServiceCriteria esc on esf.subCrietId = esc.subCriteriaId
				    and esf.tenderFormId = @v_fieldName2Vc
		End	
End

IF @v_fieldName1Vc = 'GetNegTenderFormsList'    -- By: Dipal - For Getting Form List at tenderere side for Negotiation
Begin
		select  CONVERT(VARCHAR(10),c.formId) as FieldValue1, d.formName as FieldValue2,
		 CONVERT(VARCHAR(10),f.srvBoqId) as FieldValue3, f.srvBoqType as FieldValue4,
		 g.contractType as FieldValue5,d.isMultipleFilling as FieldValue6,
		 d.isMandatory as FieldValue7
		from tbl_Negotiation a
		inner join tbl_NegNotifyTenderer b on a.negId = b.negId
		inner join tbl_NegotiationForms c on a.negId = c.negId  
		inner join tbl_TenderForms d on c.formId=d.tenderFormId 
		inner join tbl_CMS_TemplateSrvBoqDetail e on replace(d.templateFormId,'-','') = e.templateFormId
		inner join tbl_CMS_SrvBoqMaster f on f.srvBoqId = e.srvBoqId
		inner join tbl_TenderDetails g on g.tenderId = a.tenderId
		where 
		a.tenderId=@v_fieldName2Vc and b.userId = @v_fieldName3Vc
		order by f.sortOrder asc
ENd

IF @v_fieldName1Vc = 'GetNegBidderFormsData'    -- By: Dipal - For Getting Form List at tenderere side for Negotiation
Begin
/*		
		select distinct  CONVERT(VARCHAR(10),c.formId) as FieldValue1, d.formName FieldValue2,
		case when g.bidId IS NOT null then
				CONVERT(VARCHAR(10),g.bidId)  
		else
				CONVERT(VARCHAR(10),e.bidId)
		end as FieldValue3 ,
		CONVERT(VARCHAR(10),g.negBidFormId) as FieldValue4,g.negBidFormId , CONVERT(VARCHAR(10),a.negId) as FieldValue5
		from tbl_Negotiation a
		inner join tbl_NegNotifyTenderer b on a.negId = b.negId
		inner join tbl_NegotiationForms c on a.negId = c.negId  
		inner join tbl_TenderForms d on c.formId=d.tenderFormId
		inner join tbl_TenderBidForm e on d.tenderFormId = e.tenderFormId and a.tenderId = e.tenderId and e.userId=b.userId
		left join tbl_NegBidForm g on (g.negId=a.negId  and g.tenderFormId= d.tenderFormId)  
		where
		a.tenderId=@v_fieldName2Vc and b.userId = @v_fieldName3Vc and c.formId=@v_fieldName4Vc
		order by g.negBidFormId desc
		*/
		select FieldValue1,FieldValue2,FieldValue3,FieldValue4,negBidFormId,FieldValue5 from
		(
			select  CONVERT(VARCHAR(10),c.formId) as FieldValue1, d.formName FieldValue2,
					CONVERT(VARCHAR(10),e.bidId)  
			 as FieldValue3 ,
			NULL as FieldValue4,NULL as negBidFormId , CONVERT(VARCHAR(10),a.negId) as FieldValue5
			from tbl_Negotiation a
			inner join tbl_NegNotifyTenderer b on a.negId = b.negId
			inner join tbl_NegotiationForms c on a.negId = c.negId  
			inner join tbl_TenderForms d on c.formId=d.tenderFormId
			inner join tbl_TenderBidForm e on d.tenderFormId = e.tenderFormId and a.tenderId = e.tenderId and e.userId=b.userId
			where
			a.tenderId=@v_fieldName2Vc and b.userId = @v_fieldName3Vc and c.formId=@v_fieldName4Vc
			and e.bidId not in (select bidId from tbl_NegBidForm where tenderFormId=@v_fieldName4Vc and userId=@v_fieldName3Vc)
			union
			select   CONVERT(VARCHAR(10),c.formId) as FieldValue1, d.formName FieldValue2,
					CONVERT(VARCHAR(10),g.bidId) as FieldValue3 ,
			CONVERT(VARCHAR(10),g.negBidFormId) as FieldValue4,g.negBidFormId ,
			 CONVERT(VARCHAR(10),a.negId) as FieldValue5
			from tbl_Negotiation a
			inner join tbl_NegNotifyTenderer b on a.negId = b.negId
			inner join tbl_NegotiationForms c on a.negId = c.negId  
			inner join tbl_TenderForms d on c.formId=d.tenderFormId
			inner join tbl_NegBidForm g on (g.negId=a.negId  and g.tenderFormId= d.tenderFormId)  
			where
			a.tenderId=@v_fieldName2Vc and b.userId = @v_fieldName3Vc and c.formId=@v_fieldName4Vc
		)x
		order by convert(int,FieldValue3) 
		
		
ENd


IF @v_fieldName1Vc = 'GetGrandSumNego'    -- By: Dipal - For Getting detail for negotiation Grand Summary.
Begin
		
		select CONVERT(varchar(2000),formName) as FieldValue1,cellValue as FieldValue2  
		from (select tg.cellId,tg.tendertableid,formName ,tf.tenderFormId
		from tbl_TenderGrandSum g,tbl_TenderGrandSumDetail tg,tbl_TenderForms tf
		where g.tenderSumId=tg.tenderSumId  and tenderId=@v_fieldName2Vc
		and tf.tenderFormId=tg.tenderFormId) a
		inner join (
		select tb.tenderTableId,b.cellId,cellValue,bf.tenderFormId from tbl_NegBidDtlSrv b,tbl_NegBidTable tb,tbl_NegBidForm bf,
		tbl_Negotiation n
		where bf.negBidFormId=tb.negBidFormId and tb.negBidTableId = b.negBidTableId and bf.negId=n.negId 
		and n.tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc)b
		on a.tenderTableId=b.tenderTableId and a.cellId=b.cellId  and a.tenderFormId = b.tenderFormId

ENd


IF @v_fieldName1Vc = 'GetNegFormDtl'    -- By: Dipal - For Getting Negotiation Form Details
Begin
		
		
select distinct 
		CONVERT(VARCHAR(10),c.negBidFormId) as FieldValue1, CONVERT(VARCHAR(10),a.negId) as FieldValue2
		from tbl_Negotiation a
		inner join tbl_NegNotifyTenderer b on a.negId = b.negId
		inner join tbl_NegBidForm c on c.negId= a.negId
		where a.tenderId=@v_fieldName2Vc and b.userId = @v_fieldName3Vc and c.tenderFormId=@v_fieldName4Vc
ENd
If @v_fieldName1Vc='OpenCommitteeDumpCheck'
Begin

		select convert(varchar(20),cm.tenderId) as FieldValue1 from tbl_CommitteeMembers c,tbl_Committee cm
		where c.committeeId=cm.committeeId
		and cm.committeeType in('TOC','POC')
		and cm.tenderId in (select items from dbo.f_split(@v_fieldName2Vc,',')) and c.memberFrom in('TEC','PEC')
		and c.userId in (select c1.userId from tbl_CommitteeMembers c1,tbl_Committee cm1
		where c1.committeeId=cm1.committeeId
		and cm1.committeeType in('TEC','PEC')
		and cm1.tenderId=@v_fieldName3Vc)

	--select case when 
	--	(select count(cm.tenderId) from tbl_CommitteeMembers c,tbl_Committee cm
	--	where c.committeeId=cm.committeeId
	--	and committeeType in('TOC','POC')
	--	and tenderId = @v_fieldName2Vc and memberFrom in('TEC','PEC')
	--	and userId in (select userId from tbl_CommitteeMembers c,tbl_Committee cm
	--	where c.committeeId=cm.committeeId
	--	and committeeType in('TEC','PEC')
	--	and tenderId=@v_fieldName3Vc))!=0
	--Then
	--	'1'
	--Else
	--	'0'
	--End as FieldValue1			
End
If @v_fieldName1Vc='getFinalSubUser'
Begin
	select convert(varchar(20),userId) as FieldValue1,dbo.f_getbiddercompany(userId) as FieldValue2 from tbl_FinalSubmission where tenderId=@v_fieldName2Vc and bidSubStatus='finalsubmission'
End

If @v_fieldName1Vc='InsertEvalSerFormDetail4Zero'
Begin
	Begin try
	begin tran
	IF(select COUNT(esFormDetailId) from tbl_EvalSerFormDetail es where es.tenderId=@v_fieldName2Vc and es.evalBy=@v_fieldName5Vc and es.userId=@v_fieldName4Vc and es.tenderFormId in (select tenderFormId from 
				(select tenderFormId,SUM(maxMarks) sm from tbl_EvalServiceForms where tenderId=@v_fieldName2Vc 
				group by tenderFormId)a where sm=0)) = 0
	Begin	
			insert into tbl_EvalSerFormDetail ([tenderFormId]
				  ,[userId]
				  ,[maxMarks]
				  ,[actualMarks]
				  ,[evalBy]
				  ,[evalDate]
				  ,[evalStatus]
				  ,[evalStatusDt]
				  ,[ratingWeightage]
				  ,[ratedScore]
				  ,[bidId]
				  ,[subCriteriaId]
				  ,[tenderId]
				  ,[isMemberDropped]
				  ,[remarks])
			SELECT tb.tenderformid,userid,0,0.00,@v_fieldName5Vc,GETDATE(),'evaluation',Null,'Excellent',1,bidId,subCrietId,@v_fieldName2Vc,'no','' FROM tbl_EvalServiceForms E,tbl_TenderBidForm TB
			WHERE E.tenderFormId=TB.tenderFormId AND TB.tenderId=@v_fieldName2Vc
			 and tb.userId=@v_fieldName4Vc and tb.tenderFormId in( 
			select tenderFormId from 
				(select tenderFormId,SUM(maxMarks) sm from tbl_EvalServiceForms where tenderId=@v_fieldName2Vc 
				group by tenderFormId)a where sm=0)		
	END
select 'Success' as Col1
	commit tran		
	end try
	begin catch
		select 'Error' as Col1, ERROR_MESSAGE() as Col2
		rollback tran
	end catch
End
/*
START
*/
IF @v_fieldName1Vc = 'geteGPSaveCalc'
BEGIN
	DECLARE	@v_saveCalcDetail Table
				(
					PEName VARCHAR(200),
					totalAward VARCHAR(50), 
					totalOffCost VARCHAR(50),
					totalContrCost VARCHAR(50),
					totalSaved VARCHAR(50),
					totalPerSave VARCHAR(50),
					totalGoodsContact VARCHAR(50), 
					totalGoodsCost VARCHAR(50), 
					totalGoodsSaved VARCHAR(50),
					totalGoodsPerSave VARCHAR(50),
					totalWorksContact VARCHAR(50), 
					totalWorksCost VARCHAR(50), 
					totalWorksSaved VARCHAR(50),
					totalWorksPerSave VARCHAR(50),
					totalSrvsContact VARCHAR(50), 
					totalSrvsCost VARCHAR(50), 
					totalSrvsSaved VARCHAR(50),
					totalSrvsPerSave VARCHAR(50),
					totalContrCompleted VARCHAR(50)
--					procurementType  VARCHAR(50)
				)

	DECLARE	@v_procNatureDetail Table
				(		
					--rowID INT IDENTITY,
					procNatPEName VARCHAR(200),
					procNatAward VARCHAR(50), 
					procNatCost VARCHAR(50), 
					procNatSaved VARCHAR(50),
					procNatPerSave VARCHAR(50)				
				)

	-- MAIN DATA SELECT AND STORE IN TEMP_TABLE
	INSERT INTO @v_saveCalcDetail 
	SELECT	--em.employeeName,		
			td.peName,
			convert(varchar(50),COUNT(cs.noaId)) AS AwardContract,
			convert(varchar(50),SUM(td.estCost)) AS estimateCost,
			ISNULL(convert(varchar(50),SUM(nd.contractAmt)),'N.A.') AS ContractAmt,
			ISNULL(convert(varchar(50),SUM(td.estCost) - SUM(nd.contractAmt)),'N.A.') AS savedAmt,
			ISNULL(convert(varchar(50),(100 *SUM(td.estCost) - SUM(nd.contractAmt))/SUM(td.estCost)),'N.A.') AS savedPer,
			'N.A.',
			'N.A.',
			'N.A.',
			'N.A.',	
			'N.A.',
			'N.A.',
			'N.A.',
			'N.A.',	
			'N.A.',
			'N.A.',
			'N.A.',
			'N.A.',	
			convert(varchar(50),COUNT(wc.contractId)) AS execContract
--			td.procurementType
	FROM	tbl_TenderDetails td
			--INNER JOIN tbl_EmployeeOffices eo on eo.officeId = td.officeId
			--INNER JOIN tbl_EmployeeMaster em on em.employeeId = eo.employeeId
			--INNER JOIN tbl_EmployeeRoles er ON er.employeeId= em.employeeId
			--			AND er.procurementRoleId IN (select procurementRoleId from tbl_ProcurementRole WHERE procurementRole = 'PE')					
			LEFT JOIN tbl_NoaIssueDetails nd on nd.tenderId=td.tenderId 
			LEFT JOIN tbl_NoaAcceptance  na ON na.noaIssueId  = nd.noaIssueId
						and acceptRejStatus='approved'  
			LEFT JOIN tbl_ContractSign cs on cs.noaId = na.noaIssueId
			LEFT JOIN tbl_CMS_WcCertificate wc on wc.contractId = cs.contractSignId
						AND wc.isWorkComplete = 'yes'
	WHERE	td.tenderStatus = 'approved'
			AND td.departmentId IN (CASE WHEN @v_fieldName2Vc = '' OR @v_fieldName2Vc IS NULL THEN td.departmentId ELSE @v_fieldName2Vc END)
			AND  td.officeId =  CASE WHEN @v_fieldName3Vc = '' OR @v_fieldName3Vc IS NULL THEN td.officeId ELSE CAST(@v_fieldName3Vc AS INT) END
			AND td.procurementNature = CASE WHEN @v_fieldName4Vc = '' OR @v_fieldName4Vc IS NULL THEN td.procurementNature ELSE @v_fieldName4Vc END
			AND (td.budgetTypeId = CASE WHEN @v_fieldName5Vc = '' OR  @v_fieldName5Vc IS NULL THEN td.budgetTypeId ELSE LEFT(@v_fieldName5Vc, 1) END OR 
				td.budgetTypeId = CASE WHEN @v_fieldName5Vc = '' OR  @v_fieldName5Vc IS NULL THEN td.budgetTypeId ELSE RIGHT(@v_fieldName5Vc, 1) END) 
			AND td.peDistrict like CASE WHEN @v_fieldName6Vc = '' OR  @v_fieldName6Vc IS NULL THEN '%'+td.peDistrict+'%' ELSE '%'+@v_fieldName6Vc+'%' END
			AND td.cpvCode like CASE WHEN @v_fieldName7Vc = '' OR  @v_fieldName7Vc IS NULL THEN '%'+td.cpvCode+'%' ELSE '%'+@v_fieldName7Vc+'%' END
			AND td.procurementType = CASE WHEN @v_fieldName8Vc = '' OR @v_fieldName8Vc IS NULL THEN ''+td.procurementType+'' ELSE ''+@v_fieldName8Vc+'' END
			AND td.procurementMethodId = CASE WHEN @v_fieldName9Vc = '' OR @v_fieldName9Vc IS NULL THEN td.procurementMethodId ELSE @v_fieldName9Vc END
	GROUP by td.peName--em.employeeName,td.procurementType
	ORDER BY td.peName--em.employeeName,td.procurementType

	-- GOODS DATA SELECT AND STORE IN TEMP_TABLE
	IF @v_fieldName4Vc = 'Goods' OR @v_fieldName4Vc = ''
	BEGIN
				
		INSERT INTO @v_procNatureDetail
		SELECT	--em.employeeName,		
				td.peName,
				convert(varchar(50),COUNT(cs.noaId)) AS AwardContract,
				ISNULL(convert(varchar(50),SUM(nd.contractAmt)),'N.A.') AS ContractAmt,
				ISNULL(convert(varchar(50),SUM(td.estCost) - SUM(nd.contractAmt)),'N.A.') AS savedAmt,
				ISNULL(convert(varchar(50),(100 *SUM(td.estCost) - SUM(nd.contractAmt))/SUM(td.estCost)),'N.A.') AS savedPer
		FROM	tbl_TenderDetails td
				--INNER JOIN tbl_EmployeeOffices eo on eo.officeId = td.officeId
				--INNER JOIN tbl_EmployeeMaster em on em.employeeId = eo.employeeId
				--INNER JOIN tbl_EmployeeRoles er ON er.employeeId= em.employeeId
				--			AND er.procurementRoleId IN (select procurementRoleId from tbl_ProcurementRole WHERE procurementRole = 'PE')					
				LEFT JOIN tbl_NoaIssueDetails nd on nd.tenderId=td.tenderId 
				left JOIN tbl_NoaAcceptance  na ON na.noaIssueId  = nd.noaIssueId
							and acceptRejStatus='approved'  
				left JOIN tbl_ContractSign cs on cs.noaId = na.noaIssueId
		WHERE	td.tenderStatus = 'approved' 
				AND td.procurementNature = 'Goods'
				AND td.departmentId IN (CASE WHEN @v_fieldName2Vc = '' OR @v_fieldName2Vc IS NULL THEN td.departmentId ELSE @v_fieldName2Vc END)
				AND  td.officeId =  CASE WHEN @v_fieldName3Vc = '' OR @v_fieldName3Vc IS NULL THEN td.officeId ELSE CAST(@v_fieldName3Vc AS INT) END
				AND (td.budgetTypeId = CASE WHEN @v_fieldName5Vc = '' OR  @v_fieldName5Vc IS NULL THEN td.budgetTypeId ELSE LEFT(@v_fieldName5Vc, 1) END OR 
					td.budgetTypeId = CASE WHEN @v_fieldName5Vc = '' OR  @v_fieldName5Vc IS NULL THEN td.budgetTypeId ELSE RIGHT(@v_fieldName5Vc, 1) END) 
				AND td.peDistrict like CASE WHEN @v_fieldName6Vc = '' OR  @v_fieldName6Vc IS NULL THEN '%'+td.peDistrict+'%' ELSE '%'+@v_fieldName6Vc+'%' END
				AND td.cpvCode like CASE WHEN @v_fieldName7Vc = '' OR  @v_fieldName7Vc IS NULL THEN '%'+td.cpvCode+'%' ELSE '%'+@v_fieldName7Vc+'%' END
				AND td.procurementType = CASE WHEN @v_fieldName8Vc = '' OR @v_fieldName8Vc IS NULL THEN ''+td.procurementType+'' ELSE ''+@v_fieldName8Vc+'' END
				AND td.procurementMethodId = CASE WHEN @v_fieldName9Vc = '' OR @v_fieldName9Vc IS NULL THEN td.procurementMethodId ELSE @v_fieldName9Vc END
		GROUP by td.peName --em.employeeName
		ORDER BY td.peName --em.employeeName
			
		UPDATE	@v_saveCalcDetail 
		SET		totalGoodsContact = pnd.procNatAward, 
				totalGoodsCost = pnd.procNatCost, 
				totalGoodsSaved = pnd.procNatSaved,
				totalGoodsPerSave = pnd.procNatPerSave
		FROM	@v_saveCalcDetail savCalc
				INNER JOIN @v_procNatureDetail pnd ON pnd.procNatPEName = savCalc.PEName

		DELETE FROM @v_procNatureDetail	
	END

	-- WORKS DATA SELECT AND STORE IN TEMP_TABLE
	IF @v_fieldName4Vc = 'Works' OR @v_fieldName4Vc = ''
	BEGIN
				
		INSERT INTO @v_procNatureDetail
		SELECT	--em.employeeName,
				td.pename,
				convert(varchar(50),COUNT(cs.noaId)) AS AwardContract,
				ISNULL(convert(varchar(50),SUM(nd.contractAmt)),'N.A.') AS ContractAmt,
				ISNULL(convert(varchar(50),SUM(td.estCost) - SUM(nd.contractAmt)),'N.A.') AS savedAmt,
				ISNULL(convert(varchar(50),(100 *SUM(td.estCost) - SUM(nd.contractAmt))/SUM(td.estCost)),'N.A.')  AS savedPer
		FROM	tbl_TenderDetails td
				--INNER JOIN tbl_EmployeeOffices eo on eo.officeId = td.officeId
				--INNER JOIN tbl_EmployeeMaster em on em.employeeId = eo.employeeId
				--INNER JOIN tbl_EmployeeRoles er ON er.employeeId= em.employeeId
				--			AND er.procurementRoleId IN (select procurementRoleId from tbl_ProcurementRole WHERE procurementRole = 'PE')					
				LEFT JOIN tbl_NoaIssueDetails nd on nd.tenderId=td.tenderId 
				left JOIN tbl_NoaAcceptance  na ON na.noaIssueId  = nd.noaIssueId
							and acceptRejStatus='approved'  
				left JOIN tbl_ContractSign cs on cs.noaId = na.noaIssueId
		WHERE	td.tenderStatus = 'approved' 
				AND td.procurementNature = 'works'
				AND td.departmentId IN (CASE WHEN @v_fieldName2Vc = '' OR @v_fieldName2Vc IS NULL THEN td.departmentId ELSE @v_fieldName2Vc END)
				AND  td.officeId =  CASE WHEN @v_fieldName3Vc = '' OR @v_fieldName3Vc IS NULL THEN td.officeId ELSE CAST(@v_fieldName3Vc AS INT) END
				AND (td.budgetTypeId = CASE WHEN @v_fieldName5Vc = '' OR  @v_fieldName5Vc IS NULL THEN td.budgetTypeId ELSE LEFT(@v_fieldName5Vc, 1) END OR 
					td.budgetTypeId = CASE WHEN @v_fieldName5Vc = '' OR  @v_fieldName5Vc IS NULL THEN td.budgetTypeId ELSE RIGHT(@v_fieldName5Vc, 1) END) 
				AND td.peDistrict like CASE WHEN @v_fieldName6Vc = '' OR  @v_fieldName6Vc IS NULL THEN '%'+td.peDistrict+'%' ELSE '%'+@v_fieldName6Vc+'%' END
				AND td.cpvCode like CASE WHEN @v_fieldName7Vc = '' OR  @v_fieldName7Vc IS NULL THEN '%'+td.cpvCode+'%' ELSE '%'+@v_fieldName7Vc+'%' END
				AND td.procurementType = CASE WHEN @v_fieldName8Vc = '' OR @v_fieldName8Vc IS NULL THEN ''+td.procurementType+'' ELSE ''+@v_fieldName8Vc+'' END
				AND td.procurementMethodId = CASE WHEN @v_fieldName9Vc = '' OR @v_fieldName9Vc IS NULL THEN td.procurementMethodId ELSE @v_fieldName9Vc END
		GROUP by td.pename -- em.employeeName
		ORDER BY td.peName -- em.employeeName
			
		UPDATE	@v_saveCalcDetail
		SET		totalWorksContact = pnd.procNatAward, 
				totalWorksCost = pnd.procNatCost, 
				totalWorksSaved = pnd.procNatSaved,
				totalWorksPerSave = pnd.procNatPerSave
		FROM	@v_saveCalcDetail savCalc
				INNER JOIN @v_procNatureDetail pnd ON pnd.procNatPEName = savCalc.PEName
		
		DELETE FROM @v_procNatureDetail			
	END

	-- SERVICE DATA SELECT AND STORE IN TEMP_TABLE
	IF @v_fieldName4Vc = 'Service' OR @v_fieldName4Vc = ''
	BEGIN
				
		INSERT INTO @v_procNatureDetail
		SELECT	--em.employeeName,
				td.pename,
				convert(varchar(50),COUNT(cs.noaId)) AS AwardContract,
				ISNULL(convert(varchar(50),SUM(nd.contractAmt)),'N.A.') AS ContractAmt,
				ISNULL(convert(varchar(50),SUM(td.estCost) - SUM(nd.contractAmt)),'N.A.') AS savedAmt,
				ISNULL(convert(varchar(50),(100 *SUM(td.estCost) - SUM(nd.contractAmt))/SUM(td.estCost)),'N.A.') AS savedPer
		FROM	tbl_TenderDetails td
				--INNER JOIN tbl_EmployeeOffices eo on eo.officeId = td.officeId
				--INNER JOIN tbl_EmployeeMaster em on em.employeeId = eo.employeeId
				--INNER JOIN tbl_EmployeeRoles er ON er.employeeId= em.employeeId
				--			AND er.procurementRoleId IN (select procurementRoleId from tbl_ProcurementRole WHERE procurementRole = 'PE')					
				LEFT JOIN tbl_NoaIssueDetails nd on nd.tenderId=td.tenderId 
				left JOIN tbl_NoaAcceptance  na ON na.noaIssueId  = nd.noaIssueId
							and acceptRejStatus='approved'  
				left JOIN tbl_ContractSign cs on cs.noaId = na.noaIssueId
		WHERE	td.tenderStatus = 'approved' 
				AND td.procurementNature = 'services'
				AND td.departmentId IN (CASE WHEN @v_fieldName2Vc = '' OR @v_fieldName2Vc IS NULL THEN td.departmentId ELSE @v_fieldName2Vc END)
				AND  td.officeId =  CASE WHEN @v_fieldName3Vc = '' OR @v_fieldName3Vc IS NULL THEN td.officeId ELSE CAST(@v_fieldName3Vc AS INT) END
				AND (td.budgetTypeId = CASE WHEN @v_fieldName5Vc = '' OR  @v_fieldName5Vc IS NULL THEN td.budgetTypeId ELSE LEFT(@v_fieldName5Vc, 1) END OR 
					td.budgetTypeId = CASE WHEN @v_fieldName5Vc = '' OR  @v_fieldName5Vc IS NULL THEN td.budgetTypeId ELSE RIGHT(@v_fieldName5Vc, 1) END) 
				AND td.peDistrict like CASE WHEN @v_fieldName6Vc = '' OR  @v_fieldName6Vc IS NULL THEN '%'+td.peDistrict+'%' ELSE '%'+@v_fieldName6Vc+'%' END
				AND td.cpvCode like CASE WHEN @v_fieldName7Vc = '' OR  @v_fieldName7Vc IS NULL THEN '%'+td.cpvCode+'%' ELSE '%'+@v_fieldName7Vc+'%' END
				AND td.procurementType = CASE WHEN @v_fieldName8Vc = '' OR @v_fieldName8Vc IS NULL THEN ''+td.procurementType+'' ELSE ''+@v_fieldName8Vc+'' END
				AND td.procurementMethodId = CASE WHEN @v_fieldName9Vc = '' OR @v_fieldName9Vc IS NULL THEN td.procurementMethodId ELSE @v_fieldName9Vc END
		GROUP by td.peName -- em.employeeName
		ORDER BY td.peName --em.employeeName
			
		UPDATE	srvCalc
		SET		totalSrvsContact = pnd.procNatAward, 
				totalSrvsCost = pnd.procNatCost, 
				totalSrvsSaved = pnd.procNatSaved,
				totalSrvsPerSave = pnd.procNatPerSave
		FROM	@v_saveCalcDetail srvCalc
				INNER JOIN @v_procNatureDetail pnd ON pnd.procNatPEName = srvCalc.PEName
				
		DELETE FROM @v_procNatureDetail
	END
	
	SELECT	PEName AS FieldValue1,
			totalAward AS FieldValue2, 
			totalOffCost AS FieldValue3,
			totalContrCost AS FieldValue4,
			totalSaved AS FieldValue5,
			totalPerSave AS FieldValue6,
			totalGoodsContact AS FieldValue7, 
			totalGoodsCost AS FieldValue8, 
			totalGoodsSaved AS FieldValue9,
			totalGoodsPerSave AS FieldValue10,
			totalWorksContact AS FieldValue11, 
			totalWorksCost AS FieldValue12, 
			totalWorksSaved AS FieldValue13,
			totalWorksPerSave AS FieldValue14,
			totalSrvsContact AS FieldValue15, 
			totalSrvsCost AS FieldValue16, 
			totalSrvsSaved AS FieldValue17,
			totalSrvsPerSave AS FieldValue18,
			totalContrCompleted AS FieldValue19 
	FROM	@v_saveCalcDetail
END

IF @v_fieldName1Vc = 'tenderWisePartRpt'  
BEGIN

	SELECT	td.peName AS FieldValue1,
			td.packageDescription AS FieldValue2,
			ISNULL(convert(varchar(50),COUNT(bd.tenderId)),'0') AS FieldValue3,
			ISNULL(convert(varchar(50),COUNT(fs.tenderId)),'0') AS FieldValue4,
			CASE	WHEN td.tenderStatus = 'Cancelled' THEN 'Cancelled' 
					WHEN es.rptStatus = 'Rejected / Re-Tendering' OR es.rptStatus = 'Rejected/Re-Tendering' THEN 'Retendered' 
					WHEN COUNT(cs.contractSignId) > 0 THEN 'Awarded'
					ELSE 'N.A.'
			END AS FieldValue5,
			CASE WHEN  COUNT(cs.contractSignId) > 0 THEN nd.companyName ELSE 'N.A.' END AS FieldValue6,
			CASE	WHEN COUNT(wc.contractId) > 0 THEN 'Completed'
					WHEN COUNT(ct.contractId) > 0 THEN 'Terminated'
					WHEN COUNT(cs.contractSignId) > 0 THEN 'Undergoing'
					ELSE 'N.A.'
			END AS FieldValue7
	FROM	tbl_TenderDetails td
			LEFT JOIN	tbl_BidConfirmation bd on bd.tenderId = td.tenderId
						and bd.confirmationStatus = 'accepted'
			LEFT JOIN	tbl_TenderPayment tp on tp.tenderId = bd.tenderId
						AND tp.paymentFor = 'Document Fees'
						AND tp.tenderPaymentId IN (select paymentId from tbl_TenderPaymentVerification)	
			LEFT JOIN	tbl_FinalSubmission fs on fs.tenderId = td.tenderId
						AND fs.bidSubStatus = 'finalsubmission' 
			LEFT JOIN	tbl_EvalRptSentToAA es on es.tenderId = fs.tenderId
			LEFT JOIN	tbl_NoaIssueDetails nd on nd.tenderId = td.tenderId 
						AND nd.noaIssueId IN (	SELECT	noaIssueId 
												FROM	tbl_NoaAcceptance 
												WHERE	acceptRejStatus = 'approved'
											)
			LEFT JOIN	tbl_NoaAcceptance na on na.noaIssueId = nd.noaIssueId
						AND na.acceptRejStatus = 'approved'
			LEFT JOIN	tbl_ContractSign cs on cs.noaId = na.noaIssueId
			LEFT JOIN	tbl_CMS_WcCertificate wc on wc.contractId = cs.contractSignId
						AND wc.isWorkComplete = 'yes'
			LEFT JOIN	tbl_CMS_ContractTermination ct on ct.contractId = cs.contractSignId
						AND ct.status = 'approved'
	WHERE td.tenderStatus = 'Approved'
			AND td.departmentId IN (CASE WHEN @v_fieldName2Vc = '' OR @v_fieldName2Vc IS NULL THEN td.departmentId ELSE @v_fieldName2Vc END)
			AND  td.officeId =  CASE WHEN @v_fieldName3Vc = '' OR @v_fieldName3Vc IS NULL THEN td.officeId ELSE CAST(@v_fieldName3Vc AS INT) END
			AND td.procurementNature = CASE WHEN @v_fieldName4Vc = '' OR @v_fieldName4Vc IS NULL THEN td.procurementNature ELSE @v_fieldName4Vc END
			AND (td.budgetTypeId = CASE WHEN @v_fieldName5Vc = '' OR  @v_fieldName5Vc IS NULL THEN td.budgetTypeId ELSE LEFT(@v_fieldName5Vc, 1) END OR 
				td.budgetTypeId = CASE WHEN @v_fieldName5Vc = '' OR  @v_fieldName5Vc IS NULL THEN td.budgetTypeId ELSE RIGHT(@v_fieldName5Vc, 1) END) 
			AND td.peDistrict like CASE WHEN @v_fieldName6Vc = '' OR  @v_fieldName6Vc IS NULL THEN '%'+td.peDistrict+'%' ELSE '%'+@v_fieldName6Vc+'%' END
			AND td.cpvCode like CASE WHEN @v_fieldName7Vc = '' OR  @v_fieldName7Vc IS NULL THEN '%'+td.cpvCode+'%' ELSE '%'+@v_fieldName7Vc+'%' END
			AND td.procurementMethodId = CASE WHEN @v_fieldName8Vc = '' OR @v_fieldName9Vc IS NULL THEN td.procurementMethodId ELSE @v_fieldName8Vc END	
	GROUP by td.peName, td.packageDescription, td.tenderStatus, es.rptStatus, nd.companyName
	ORDER BY td.peName, td.packageDescription, td.tenderStatus, es.rptStatus, nd.companyName
END
/*
END
*/
/*	Dohatec Start	*/
IF @v_fieldName1Vc = 'getPaidTendererListForPEICT'   
BEGIN	
If @v_fieldName3Vc is not null And @v_fieldName3Vc<>'' And @v_fieldName3Vc<>'0'
	Begin
		SELECT -- Distinct Convert(varchar(50),tenderPaymentId) as FieldValue1, 
				Distinct dbo.f_getbiddercompany(LM.userId) as FieldValue2,
				Convert(varchar(50),lm.userId) as FieldValue3, 				
				Convert(varchar(50),TM.tenderId) as FieldValue4, 
				Convert(varchar(50),pkgLotId) as FieldValue5, 
				Case paymentFor When 'Document Fees' Then 'df'
				When 'Tender Security' Then 'ts'
				When 'Performance Security' Then 'ps'
				When 'Bank Guarantee' Then 'bg'
				End as FieldValue6,
				paymentFor as FieldValue7,
				dbo.f_initCap(tp.status) as FieldValue8,
				Convert(varchar(50),tp.extValidityRef) as FieldValue9,
				LM.registrationType as FieldValue10
			From tbl_TenderPayment TP
			Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
			Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
			Inner Join tbl_TenderPaymentVerification TPV On TP.tenderPaymentId=TPV.paymentId
			Inner Join tbl_TenderMaster TM On TP.tenderId=TM.tenderId
			Inner Join tbl_AppMaster APP On TM.appId=APP.appId
			Inner Join tbl_EmployeeMaster EM On APP.employeeId=EM.employeeId
		WHERE 
			TP.tenderId=@v_fieldName2Vc
			And TP.pkgLotId=@v_fieldName3Vc
			And TP.paymentFor=@v_fieldName4Vc
			And EM.userId=@v_fieldName5Vc 
			And isVerified='Yes'
			And isLive='Yes'
			and TP.tenderId in 
				(
				select distinct tenderId from tbl_Committee c
				inner join tbl_CommitteeMembers cm on c.committeeId=cm.committeeId
				where c.committeeType in ('toc','poc')
				and c.committeStatus='approved'
				and cm.appStatus='approved'			
				)	
		-- Order by tenderPaymentId Desc
	End
	Else
	Begin
		SELECT -- Distinct Convert(varchar(50),tenderPaymentId) as FieldValue1, 
				Distinct dbo.f_getbiddercompany(LM.userId) as FieldValue2,
				Convert(varchar(50),lm.userId) as FieldValue3, 				
				Convert(varchar(50),TM.tenderId) as FieldValue4, 
				Convert(varchar(50),pkgLotId) as FieldValue5, 
				Case paymentFor When 'Document Fees' Then 'df'
				When 'Tender Security' Then 'ts'
				When 'Performance Security' Then 'ps'
				When 'Bank Guarantee' Then 'bg'
				End as FieldValue6,
				paymentFor as FieldValue7,
				dbo.f_initCap(tp.status) as FieldValue8,
				-- tenderPaymentId,
				Convert(varchar(50),tp.extValidityRef) as FieldValue9
			From tbl_TenderPayment TP
			Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
			Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
			Inner Join tbl_TenderPaymentVerification TPV On TP.tenderPaymentId=TPV.paymentId
			Inner Join tbl_TenderMaster TM On TP.tenderId=TM.tenderId
			Inner Join tbl_AppMaster APP On TM.appId=APP.appId
			Inner Join tbl_EmployeeMaster EM On APP.employeeId=EM.employeeId
		WHERE 
			TP.tenderId=@v_fieldName2Vc
			And TP.paymentFor=@v_fieldName4Vc
			And EM.userId=@v_fieldName5Vc 
			And isVerified='Yes'
			And isLive='Yes'
			and TP.tenderId in 
				(
				select distinct tenderId from tbl_Committee c
				inner join tbl_CommitteeMembers cm on c.committeeId=cm.committeeId
				where c.committeeType in ('toc','poc')
				and c.committeStatus='approved'
				and cm.appStatus='approved'			
				)	
		-- Order by tenderPaymentId Desc
	End
END


If @v_fieldName1Vc='getCellValueForNOAOfICT'
Begin	 	
Declare @cell int
Declare @qty int
Declare @exchange money
Declare @cell6Rate varchar(20)
Declare @cell10 varchar(20)
Declare @cell11 varchar(20)
Declare @curName varchar(50)
Declare @rowC as int
		--Commited By Nazmul on 02 dec 2012    		
		--select @qty = cellvalue from tbl_TenderCells where rowId=convert(int,@v_fieldName4Vc) and columnId=convert(int,@v_fieldName5Vc)and tenderTableId=convert(int,@v_fieldName3Vc)
		select @cell = tc.cellId  from tbl_TenderCells tc 
		where  (cellDatatype=9 or cellDatatype=10) and 
		tc.tenderTableId= (select distinct tenderTableId from tbl_TenderListBox tl,tbl_TenderListCellDetail tlc where 
		tl.tenderListId=tlc.tenderListId  and  listBoxName='Currency' and tenderTableId= convert(int,@v_fieldName3Vc)) and rowId<>(select MAX(rowId)  from tbl_TenderCells tc 
		where  (cellDatatype=9 or cellDatatype=10) and 
		tc.tenderTableId= (select distinct tenderTableId from tbl_TenderListBox tl,tbl_TenderListCellDetail tlc where 
		tl.tenderListId=tlc.tenderListId  and  listBoxName='Currency' and tenderTableId= convert(int,@v_fieldName3Vc))) and rowId=convert(int,@v_fieldName4Vc)
		select @exchange = exchangeRate,@curName = cm.currencyShortName from tbl_TenderCurrency tc, tbl_CurrencyMaster cm where tenderId=convert(int,@v_fieldName2Vc) and 
		tc.currencyId=(select distinct cellValue from tbl_TenderBidPlainData where tenderCelId=@cell and cm.currencyId=tc.currencyId and
		bidTableId=(select bidTableId from tbl_TenderBidTable tbt,tbl_TenderBidForm tbf 
		where tbt.bidId=tbf.bidId and tenderTableId=convert(int,@v_fieldName3Vc) and userId=convert(int,@v_fieldName6Vc)))	
		select @cell10 = tbpd.cellValue from tbl_TenderBidPlainData tbpd,tbl_TenderColumns tc, tbl_TenderBidForm tbf, tbl_TenderBidTable tbt
        where tbf.tenderId = convert(int,@v_fieldName2Vc) and tbf.userId=convert(int,@v_fieldName6Vc) and tbf.tenderFormId = convert(int,@v_fieldName7Vc) and tbf.bidId = tbt.bidId and tbt.bidTableId = tbpd.bidTableId
        and tc.columnType = 10 and tbpd.tenderTableId = tc.tenderTableId and tbpd.tenderColId = tc.columnId and rowId = convert(int,@v_fieldName4Vc) order by tbpd.rowId asc 	
         --Added and Commited By Nazmul on 02 dec 2012    	
        select @cell11 = Cast(replace(tbpd.cellValue, ',','') as float) from tbl_TenderBidPlainData tbpd,tbl_TenderColumns tc, tbl_TenderBidForm tbf, tbl_TenderBidTable tbt
        where tbf.tenderId = convert(int,@v_fieldName2Vc) and tbf.userId=convert(int,@v_fieldName6Vc) and tbf.tenderFormId = convert(int,@v_fieldName7Vc) and tbf.bidId = tbt.bidId and tbt.bidTableId = tbpd.bidTableId
        and tc.columnType = 11 and tbpd.tenderTableId = tc.tenderTableId and tbpd.tenderColId = tc.columnId and rowId = convert(int,@v_fieldName4Vc) --order by tbpd.rowId asc 	
       
        select @rowC = @@rowcount
	
		IF(@rowC = 0)
		SET @cell11 = 0.00     
		
        select  @cell6Rate=sum(Cast(replace(tbpd.cellValue, ',','') as float))  from tbl_TenderBidPlainData tbpd,tbl_TenderColumns tc, tbl_TenderBidForm tbf, tbl_TenderBidTable tbt
        where tbf.tenderId = convert(int,@v_fieldName2Vc) and tbf.userId=convert(int,@v_fieldName6Vc) and tbf.tenderFormId = convert(int,@v_fieldName7Vc) and tbf.bidId = tbt.bidId and tbt.bidTableId = tbpd.bidTableId
        and tc.columnType = 6 and tbpd.tenderTableId = tc.tenderTableId and tbpd.tenderColId = tc.columnId and rowId = convert(int,@v_fieldName4Vc) group by tc.columnType	
              
		--select convert(varchar(20),@exchange*@qty) as FieldValue1,convert(varchar(20),@cell10) as FieldValue2,convert(varchar(20),@curName) as FieldValue3,convert(varchar(20),@cell11) as FieldValue4
		
		select convert(varchar(20),@cell6Rate) as FieldValue1,convert(varchar(20),@cell10) as FieldValue2,convert(varchar(20),@curName) as FieldValue3,convert(varchar(20),@cell11) as FieldValue4
ENd

If @v_fieldName1Vc='getTenderFormIdForICT'
Begin	 	
	
	select convert(varchar(20),tbl_tenderBidForm.tenderFormId) as FieldValue1 from tbl_tenderBidForm inner join tbl_tenderForms 
	on tbl_tenderBidForm.tenderFormId = tbl_tenderForms.tenderFormId
	where tbl_tenderBidForm.userId = convert(int,@v_fieldName3Vc) and tbl_tenderBidForm.tenderId = convert(int,@v_fieldName2Vc) and tbl_tenderForms.pkgLotId  = convert(int,@v_fieldName4Vc) 

	           	
ENd


IF @v_fieldName1Vc =  'getFormTypeForAlertMessage'
BEGIN
		SELECT	formName AS FieldValue1, isEncryption AS FieldValue2 , 
				isPriceBid AS FieldValue3, isMandatory AS FieldValue4, 
				convert(varchar, tf.tenderFormId) AS FieldValue5,
				formStatus AS FieldValue6, isEncryption AS FieldValue8 ,
				isMultipleFilling AS FieldValue9,FormType AS FieldValue11 
				
		FROM	dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
				ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
				dbo.tbl_TenderStd td ON ts.tenderStdId = td.tenderStdId 
				WHERE	td.tenderId = @v_fieldName2Vc	AND 
						tf.tenderSectionId = @v_fieldName3Vc AND 
						tf.tenderFormId = @v_fieldName4Vc
END

IF @v_fieldName1Vc = 'getPaidTendererListForPETenderCancel'   
BEGIN

	If @v_fieldName3Vc is not null And @v_fieldName3Vc<>'' And @v_fieldName3Vc<>'0'
	Begin
		SELECT Distinct Convert(varchar(50),tenderPaymentId) as FieldValue1, 
				dbo.f_getbiddercompany(LM.userId) as FieldValue2,
				Convert(varchar(50),lm.userId) as FieldValue3, 				
				Convert(varchar(50),TM.tenderId) as FieldValue4, 
				Convert(varchar(50),pkgLotId) as FieldValue5, 
				Case paymentFor When 'Document Fees' Then 'df'
				When 'Tender Security' Then 'ts'
				When 'Performance Security' Then 'ps'
				When 'Bank Guarantee' Then 'bg'
				End as FieldValue6,
				paymentFor as FieldValue7,
				dbo.f_initCap(tp.status) as FieldValue8,
				tenderPaymentId,
				Convert(varchar(50),tp.extValidityRef) as FieldValue9,
				LM.registrationType as FieldValue10
			From tbl_TenderPayment TP
			Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
			Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
			Inner Join tbl_TenderPaymentVerification TPV On TP.tenderPaymentId=TPV.paymentId
			Inner Join tbl_TenderMaster TM On TP.tenderId=TM.tenderId
			Inner Join tbl_AppMaster APP On TM.appId=APP.appId
			Inner Join tbl_EmployeeMaster EM On APP.employeeId=EM.employeeId
		WHERE 
			TP.tenderId=@v_fieldName2Vc
			And TP.pkgLotId=@v_fieldName3Vc
			And TP.paymentFor=@v_fieldName4Vc
			And EM.userId=@v_fieldName5Vc 
			And isVerified='Yes'
			And isLive='Yes'
			and TP.tenderId in 
				(
				select distinct tenderId from tbl_Committee c
				inner join tbl_CommitteeMembers cm on c.committeeId=cm.committeeId
				where c.committeeType in ('toc','poc')
				and c.committeStatus='approved'	
				)	
		Order by tenderPaymentId Desc
	End
	Else
	Begin
		SELECT Distinct Convert(varchar(50),tenderPaymentId) as FieldValue1, 
				dbo.f_getbiddercompany(LM.userId) as FieldValue2,
				Convert(varchar(50),lm.userId) as FieldValue3, 				
				Convert(varchar(50),TM.tenderId) as FieldValue4, 
				Convert(varchar(50),pkgLotId) as FieldValue5, 
				Case paymentFor When 'Document Fees' Then 'df'
				When 'Tender Security' Then 'ts'
				When 'Performance Security' Then 'ps'
				When 'Bank Guarantee' Then 'bg'
				End as FieldValue6,
				paymentFor as FieldValue7,
				dbo.f_initCap(tp.status) as FieldValue8,
				tenderPaymentId,
				Convert(varchar(50),tp.extValidityRef) as FieldValue9
			From tbl_TenderPayment TP
			Inner Join tbl_LoginMaster LM On TP.userId=LM.userId
			Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
			Inner Join tbl_TenderPaymentVerification TPV On TP.tenderPaymentId=TPV.paymentId
			Inner Join tbl_TenderMaster TM On TP.tenderId=TM.tenderId
			Inner Join tbl_AppMaster APP On TM.appId=APP.appId
			Inner Join tbl_EmployeeMaster EM On APP.employeeId=EM.employeeId
		WHERE 
			TP.tenderId=@v_fieldName2Vc
			And TP.paymentFor=@v_fieldName4Vc
			And EM.userId=@v_fieldName5Vc 
			And isVerified='Yes'
			And isLive='Yes'
			and TP.tenderId in 
				(
				select distinct tenderId from tbl_Committee c
				inner join tbl_CommitteeMembers cm on c.committeeId=cm.committeeId
				where c.committeeType in ('toc','poc')
				and c.committeStatus='approved'	
				)	
		Order by tenderPaymentId Desc
	End



END

IF @v_fieldName1Vc='getBidderMobNoForFinalSub' 
BEGIN

with cte as (
select distinct tenderId from tbl_tenderpayment where tenderId in (select tenderid from tbl_tenderdetails
where datediff (day,submissionDt,getdate()) =-2) and paymentFor = 'Tender Security')
select convert(varchar(50),tp.userid) as FieldValue1,
 convert(varchar(50),tp.tenderId) as FieldValue2,
 convert(varchar(50),'+880'+right(tm.mobileNo,10)) as FieldValue3, 
 convert(varchar(50),lm.emailid) as FieldValue4, 
 convert(varchar(50),td.submissionDt) as FieldValue5,
 convert(varchar(50),td.peOfficeName) as FieldValue6,
 convert(varchar(50),td.reoiRfpRefNo) as FieldValue7,
 convert(varchar(50),td.packageNo) as FieldValue8,
 convert(varchar(50),td.packageDescription) as FieldValue9
from tbl_tenderpayment tp , tbl_tenderermaster tm ,tbl_loginmaster lm, tbl_tenderdetails td where 
 tm.userId = tp.userId  
and tm.userId = lm.userId
and td.tenderId in (select tenderId from cte)
and tp.paymentFor = 'Tender Security' and tp.tenderId in (select tenderId from cte)
and tp.userId not in 
(select userId from tbl_FinalSubmission 
where tenderId in (select tenderId from cte) and bidSubStatus = 'finalsubmission')

END

IF @v_fieldName1Vc='getExtensionRequestFromPE' 
BEGIN
SELECT CONVERT(varchar(50),par.requestId) AS FieldValue1 
FROM tbl_PaymentExtensionActionRequest par
INNER JOIN tbl_TenderPayment tp 
on par.paymentId = tp.tenderPaymentId
WHERE paymentFor = 'Performance Security'
and tp.[status] = 'paid'
and tp.isVerified = 'yes'
and tp.isLive = 'Yes'
--and tp.userId = @v_fieldName3Vc 
and tenderId = @v_fieldName2Vc 
and requestTo = @v_fieldName3Vc
ORDER BY requestId DESC
END
--Dohatec End


IF @v_fieldName1Vc='SearchTenderDocument'
	BEGIN
	IF EXISTS (
			SELECT T.eventType,T.invitationFor,T.procurementMethod,
		T.budgetType,T.docAvlMethod,T.procurementNature,
		T.procurementType,T.officeId	FROM
		(SELECT	td.eventType,td.invitationFor,td.procurementMethod,
		td.budgetType,td.docAvlMethod,td.procurementNature,
		td.procurementType,td.officeId 
		FROM tbl_TenderDetails td where tenderId = @v_fieldName2Vc)T,
		(SELECT	td.eventType,td.invitationFor,td.procurementMethod,
		td.budgetType,td.docAvlMethod,td.procurementNature,
		td.procurementType,td.officeId 
		FROM tbl_TenderDetails td where tenderStatus !='Pending' and 
		tenderId = @v_fieldName3Vc)T1
		WHERE	T.eventType			=	T1.eventType 
			AND T.invitationFor		=	T1.invitationFor
			AND T.procurementMethod =	T1.procurementMethod 
			AND T.budgetType		=	T1.budgetType 
			AND T.docAvlMethod		=	T1.docAvlMethod 
			AND T.procurementNature =	T1.procurementNature 
			AND T.procurementType	=	T1.procurementType 
			AND T.officeId			=	T1.officeId
			AND T.officeId IN (select  eo.officeId from tbl_EmployeeOffices eo 
								inner join tbl_EmployeeMaster em on eo.employeeId=em.employeeId 
								where  em.userId= @v_fieldName4Vc ))
	BEGIN
	SELECT	CONVERT(varchar(50),approvingAuthId)   as FieldValue1 ,
			dbo.f_Gov_part_username(approvingAuthId,'',3) as FieldValue2,
			CONVERT(varchar(50),stdTemplateId)  as FieldValue3 ,
			CONVERT(varchar(50),Tm.templateName) as FieldValue4,
			CONVERT(varchar(50),tenderValDays)  as FieldValue5,
			CONVERT(varchar(50),tenderSecurityDays)  as FieldValue6,
			CONVERT(varchar(50),estcost)  as FieldValue7,
			CONVERT(varchar(50),tenderId) as FieldValue8
	FROM (SELECT tenderId,approvingAuthId ,stdTemplateId ,tenderValDays ,tenderSecurityDays , estcost
			FROM tbl_TenderDetails WHERE  tenderId = @v_fieldName3Vc)T, 
			tbl_TemplateMaster Tm
	WHERE T.stdTemplateId = Tm.templateId
	END
	
	END
	
	IF @v_fieldName1Vc='DumpExistingSTD'
	BEGIN
	BEGIN TRAN
	BEGIN TRY
		
	DECLARE @DomesticPref VARCHAR(3) = 'No',  
			@domesticPercent AS money = 0.0,
			@NewActionTenderID VARCHAR(100) = '',			
			@v_TenderStdId_Int int
	
	--SET @NewActionTenderID = 'INSERT_'+@v_fieldName3Vc
	SET @NewActionTenderID = 'DocumentDump'
	
	
	select @DomesticPref = domesticPref,@domesticPercent = domesticPercent 
	from tbl_TenderDetails where tenderId = @v_fieldName2Vc
	
	UPDATE tbl_TenderDetails set domesticPref = @DomesticPref,
	domesticPercent = @domesticPercent where tenderId = @v_fieldName3Vc
	
	UPDATE tbl_TenderDetails SET approvingAuthId = @v_fieldName5Vc
	,stdTemplateId = @v_fieldName6Vc,tenderValDays = @v_fieldName7Vc
	,tenderSecurityDays = @v_fieldName8Vc WHERE  tenderId = @v_fieldName3Vc
	
	EXEC dbo.[p_add_upd_tendertemplate] @NewActionTenderID,@v_fieldName2Vc,@v_fieldName3Vc,@v_fieldName4Vc
	
	SELECT @v_TenderStdId_Int = MAX(tenderStdId) FROM tbl_TenderStd where tenderId = @v_fieldName3Vc

	Select '1' as FieldValue1, 'Record inserted.' as FieldValue2, @v_TenderStdId_Int as Id
		COMMIT TRAN	
	END TRY
	
	BEGIN CATCH
		BEGIN		
			Select  '0' as FieldValue1, ERROR_MESSAGE() as FieldValue2
			ROLLBACK TRAN
		END
	END CATCH
	
	END

