USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_import_promisdata_Category_4]    Script Date: 4/24/2016 11:04:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- Modified By Dohatec on 10th August 2015 To Segregate Promis data import basis of Indicator Category 
-- =============================================
ALTER PROCEDURE [dbo].[p_import_promisdata_Category_4]
AS
BEGIN
 
UPDATE [Tbl_PromisIndicator]
SET [EvaluationTime] = T.DerivedEvaluationTime
,[ResponsiveTenderersCount] = T.ResponsiveTenderersCount
,[IsReTendered] = T.IsReTendered
,[IsTenderCancelled] = T.IsTenderCancelled
FROM (          
select tenderId DerivedTenderID,
ISNULL( convert(varchar(20),DATEDIFF(d,(
select  max(sentDate)from tbl_TosRptShare trs where trs.tenderId=td.tenderId group by tenderId),
	(case	when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid 
					and tc.committeeType in ('TEC','PEC'))=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
						and roundId =(select MAX(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					) then 
						(	select MAX(signedDate) from tbl_TORRptSign trs 
							where trs.tenderId = td.tenderid and trs.reportType = 'TER4' 
							and roundId = (select MAX(roundId) from tbl_TORRptSign 
							where tenderId = td.tenderid and reportType = 'TER4')
						)
		
			when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm 
					ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and td.procurementNatureId != 3
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' and td.procurementNatureId != 3
						and roundId =(select MAX(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					) then 
					(	
						case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc 
							inner join tbl_CommitteeMembers tcm 
							ON tc.committeeId = tcm.committeeId 
							where tc.tenderId = td.tenderid and td.procurementNatureId != 3
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
							where trs.tenderId = td.tenderid and trs.reportType = 'TER2' and td.procurementNatureId != 3
							and roundId =(select MAX(roundId) from tbl_TORRptSign 
							where tenderId = td.tenderid and reportType = 'TER2')
						) then 
						(	
							case  when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid 
									and roundId = (select MAX(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))>0
									then
									(select MAX(signedDate) from tbl_TORRptSign trs 
									where trs.tenderId = td.tenderid and trs.reportType = 'TER2' 
									and roundId = (select MAX(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))										
									when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid and td.procurementNatureId != 3
									and roundId = 0)>0 and (select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid 
									and roundId = (select MAX(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER2'))=0
									then
									(select MAX(signedDate) from tbl_TORRptSign trs 
									where trs.tenderId = td.tenderid and trs.reportType = 'TER2' 
									and roundId = 0)
											
							end
						
						
						)	end
					) 
				
				when	(select COUNT(distinct tcm.userId) from tbl_Committee tc 
					inner join tbl_CommitteeMembers tcm 
					ON tc.committeeId = tcm.committeeId 
					where tc.tenderId = td.tenderid and td.procurementNatureId = 3
					and tc.committeeType in ('TEC','PEC')) !=
					(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
						where trs.tenderId = td.tenderid and trs.reportType = 'TER4' and td.procurementNatureId = 3
						and roundId =(select MAX(roundId) from tbl_TORRptSign 
						where tenderId = td.tenderid and reportType = 'TER4')
					) then 
					(	
						case when
						(select COUNT(distinct tcm.userId) from tbl_Committee tc 
							inner join tbl_CommitteeMembers tcm 
							ON tc.committeeId = tcm.committeeId 
							where tc.tenderId = td.tenderid and td.procurementNatureId = 3
							and tc.committeeType in ('TEC','PEC'))=
						(select COUNT(distinct ISNULL(userId,0)) from tbl_TORRptSign trs 
							where trs.tenderId = td.tenderid and trs.reportType = 'TER1' and td.procurementNatureId = 3
							and roundId =(select MAX(roundId) from tbl_TORRptSign 
							where tenderId = td.tenderid and reportType = 'TER1')
						) then 
						(	
							case  when 
									(select count(distinct tenderId) from tbl_EvalRptSentToAA
									tersta where tersta.tenderId = td.tenderid 
									and roundId = (select MAX(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER1'))>0
									then
									(select MAX(signedDate) from tbl_TORRptSign trs 
									where trs.tenderId = td.tenderid and trs.reportType = 'TER1' 
									and roundId = (select MAX(roundId) from tbl_TORRptSign 
									where tenderId = td.tenderid and reportType = 'TER1'))		
							end
						
						
						)	end
					) 	
					
					end
					
					
	))),'NA') as DerivedEvaluationTime,  --done, -- Modified By Dohatec
'0' AS EvalCompletedInTime , --end  -- Modified By Dohatec 
case
	when
	(select COUNT(userId) from tbl_EvalBidderStatus eval 
	where eval.tenderId=td.tenderId) = 0
	then
	'NA'
	else
	(select convert(varchar(20),COUNT(userId)) from tbl_EvalBidderStatus eval 
	where eval.tenderId=td.tenderId and (bidderStatus ='Technically Responsive' 
	or result='Pass') and eval.evalCount = (Select MAX(evalCount) from tbl_EvalBidderStatus where tenderId = td.tenderId) )	
end ResponsiveTenderersCount,	--done
--Edited by Palash, Dohatec
case
	when
	(select count(tenderId) from tbl_PostQualification pq where pq.tenderId=td.tenderId and reTenderRecommendetion='yes' 
	and pq.evalCount = (Select MAX(evalCount) from tbl_PostQualification where tenderId = td.tenderId) ) = 1
	then
	'1'
	else
	'0'	
end IsReTendered,	--done
case when tenderStatus='Cancelled'
then 1 else 0 end IsTenderCancelled
from tbl_TenderDetails td,tbl_DepartmentMaster dm 
where tenderStatus not in('Pending')
and td.departmentId=dm.departmentId
and submissionDt < GETDATE() ) T
WHERE tenderId = T.DerivedTenderID
and DaysBtwnContappNOA ='NA'
and tenderId in (select distinct tenderId from tbl_TosRptShare)
;

 
--- INDICATOR 15
WITH CTE1 AS (select tp.tenderid , CASE when EvaluationTime  ='NA' then '0'
else 
case when EvaluationTime <=
(case  when (select count(worfklowId) from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId)>0
then (case when (select COUNT(committeeId) from tbl_Committee where committeeType = 'TSC' and committeStatus ='approved' and tenderId = td.tenderid) >0
	then ( case when (SELECT procurementRole FROM tbl_ProcurementRole WHERE procurementRoleId in ( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId))='CCGP'
	then (case when (select DATEDIFF (D, (select max(publishDate) from tbl_Committee where committeeType = 'TSC' and committeStatus ='approved' and tenderId = 2235),(select max(sentDt) from tbl_EvalTSCNotification where tenderId = 2235)))>21
		then
		(select ISNULL( MAX(TotalNoOfDaysForEvaluation),-2) from tbl_ProcurementApprovalTimeline pat inner join
		( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
		( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId))T
			on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 21) ---- Complex TSC REQURED
		else
		(select ISNULL( MIN(TotalNoOfDaysForEvaluation),-2) from tbl_ProcurementApprovalTimeline pat inner join
		( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
		( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId))T
			on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 0) ---- Simple TSC REQURED
		end	)	-- CCGP Approver
	else
	( case when
	(select count(TotalNoOfDaysForEvaluation) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId))T
		on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 0)>0
	then
	(select ISNULL( TotalNoOfDaysForEvaluation,-2) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId))T
		on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 0)
	 else 0 -- 'TSC REQUIRED BUT APPROVING AUTHORITY IS NOT CONFIGURED Contract Approval Workflow is Created'
	 end ) 	 -- CCGP NOT Approver
	end) --TSC REQUIRED 
	
	else
	( case when (SELECT procurementRole FROM tbl_ProcurementRole WHERE procurementRoleId in ( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId))='CCGP'
	then
	(select ISNULL(MIN(TotalNoOfDaysForEvaluation),-2) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId))T
		on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC = 0) -- CCGP APPROVER
	else
	( case when (select count( TotalNoOfDaysForEvaluation) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId))T
		on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC = 0) >0
	then
	(select ISNULL(TotalNoOfDaysForEvaluation,-2) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( select procurementRoleId from tbl_WorkFlowLevelConfig where activityid = 13 and wfRoleId = 2 and objectId = td.tenderId))T
		on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC = 0)
	else 0-- 'TSC NOT REQUIRED BUT APPROVING AUTHORITY IS NOT CONFIGURED  Contract Approval Workflow is Created'
	end) -- CCGP NOT APPROVER
	end) --TSC NOT REQUIRED 
	end)	-- Contract Approval Workflow is Created
else
 (case when (select COUNT(committeeId) from tbl_Committee where committeeType = 'TSC' and committeStatus ='approved' and tenderId = td.tenderid) >0
	then ( case when (SELECT procurementRole FROM tbl_ProcurementRole WHERE procurementRoleId in ( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in ( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = td.tenderid)))='CCGP'
	then
	( case when  (select DATEDIFF (D, (select max(publishDate) from tbl_Committee where committeeType = 'TSC' and committeStatus ='approved' and tenderId = 2235),(select max(sentDt) from tbl_EvalTSCNotification where tenderId = 2235)))>21
	then (select ISNULL( MAX(TotalNoOfDaysForEvaluation),-2) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in
	( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = td.tenderid)))T
	on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 0)
	else
	(select ISNULL( MIN(TotalNoOfDaysForEvaluation),-2) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in
	( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = td.tenderid)))T
		on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 0)
	 end )
	when (SELECT procurementRole FROM tbl_ProcurementRole WHERE procurementRoleId in ( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in ( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = td.tenderid)))!='CCGP'
	then
	(case when  (select COUNT(TotalNoOfDaysForEvaluation) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in ( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in
	( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = td.tenderid)))T	on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 0) >0
	then
	(select ISNULL( TotalNoOfDaysForEvaluation,-2) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in
	( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = td.tenderid)))T
		on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC > 0)
	else 0 -- 'TSC  REQUIRED BUT APPROVING AUTHORITY IS NOT CONFIGURED  Contract Approval Workflow is NOt Created'
	end)
	end) -- 'TSC  REQUIRED
	else
	( case when (SELECT procurementRole FROM tbl_ProcurementRole WHERE procurementRoleId in ( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in ( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = td.tenderid)))='CCGP'
	then	(select ISNULL( MIN(TotalNoOfDaysForEvaluation),-2) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in
	( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = td.tenderid)))T
		on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC = 0) -- CCGP APPROVER
	else
	( case when
	(select COUNT(TotalNoOfDaysForEvaluation) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in
	( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = td.tenderid)))T
		on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC = 0)>0
	then
	(select ISNULL( TotalNoOfDaysForEvaluation,-2) from tbl_ProcurementApprovalTimeline pat inner join
	( SELECT procurementRole FROM tbl_ProcurementRole	WHERE	procurementRoleId in
	( SELECT approvingAuthEmpId FROM tbl_AppPackages WHERE	packageId in
	( SELECT	packageId FROM	tbl_TenderMaster WHERE	tenderId = td.tenderid)))T
		on pat.ApprovalAuthority = T.procurementRole and pat.ProcurementNature = td.ProcurementNature and pat.NoOfDaysForTSC = 0)
	else 0 --'TSC  NOT REQUIRED BUT APPROVING AUTHORITY IS NOT CONFIGURED  Contract Approval Workflow is NOt Created'
	end	)  -- CCGP NOT APPROVER
	end) -- TSC NOT REQUIRED
	end) ---Contract workflow not created 
end) then '1'
else '0'
end
end AS EvalCompletedInTimeLine 
from Tbl_PromisIndicator tp inner join tbl_TenderDetails td on tp.tenderId=td.tenderId)
UPDATE Tbl_PromisIndicator SET EvalCompletedInTime = CTE1.EvalCompletedInTimeLine FROM CTE1 WHERE CTE1.tenderId = Tbl_PromisIndicator.tenderId;;


END
