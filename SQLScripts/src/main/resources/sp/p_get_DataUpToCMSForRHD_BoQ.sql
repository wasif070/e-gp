USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_get_DataUpToCMSForRHD_BoQ]    Script Date: 4/24/2016 10:56:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[p_get_DataUpToCMSForRHD_BoQ]
@v_tenderId_inN	 INT	=NULL

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select 
cwd.groupId,
cwd.wpSrNo,
cwd.wpDescription,
cwd.wpUom,
cwd.wpQty,
cwd.wpStartDate,
cwd.wpEndDate,
cwd.wpNoOfDays,
cwd.wpRate
from tbl_CMS_WpDetail cwd
where cwd.wpId in (selecT cwm.wpId From tbl_CMS_WpMaster cwm 
where cwm.wpLotId in (select noad.pkgLotId from tbl_NoaIssueDetails noad where noad.tenderId = @v_tenderId_inN))
--where cwm.wpLotId in (select noad.pkgLotId from tbl_NoaIssueDetails noad, tbl_TenderDetails td, tbl_noaacceptance na 
--where noad.tenderId = @v_tenderId_inN and noad.tenderId = td.tenderId and noad.noaissueid = na.noaissueid and na.acceptRejStatus ='approved'
--and td.departmentId in(10)))
-- We need to add Road and Highway Department id.

END

