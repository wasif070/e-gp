USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_generateTOR_TER_old]    Script Date: 4/24/2016 10:53:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Darshah Shah
-- Create date: 28-Dec-2012
-- Description:	For Generate Tender Opening Report and Tender Evaluation Report
-- =============================================
ALTER PROCEDURE [dbo].[p_generateTOR_TER_old]
	@v_tenderID VARCHAR(10),
	@v_reportType VARCHAR(10)
AS
BEGIN
    SET NOCOUNT ON;
	BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @v_pkgLotID VARCHAR(10),
					@v_reportID INT,
					@v_reportTableID INT,
					@v_reportColNO INT,
					@v_offEstCost VARCHAR(25),
					@v_reportName VARCHAR(50),
					@errorCode VARCHAR(MAX),
					@v_reportgenerated int

			-- GET ESTIMATED COST
			SELECT	@v_offEstCost = estCost
			FROM	tbl_TenderEstCost -- by dohatec in place of tbl_TenderDetails to solve issue # 1623 
			WHERE	tenderId = @v_tenderID

			-- GET TENDER PACKAGE LOTID
			SELECT	@v_pkgLotID = appPkgLotId
			FROM	tbl_TenderLots tl
			WHERE	tenderId = @v_tenderID

			IF @v_reportType = 'TOR'
			BEGIN
					SET @v_reportName = 'Opening'
					SET @v_reportColNO = 3
			END
			ELSE IF @v_reportType = 'TER'
			BEGIN
					SET @v_reportName = 'Evaluation'
					SET @v_reportColNO = 6
			END
			
			SELECT @v_reportgenerated = reportid from tbl_ReportMaster where tenderid = @v_tenderID and reportType= @v_reportType --- **** Added By Dohatec For Double Entry Check **** ---
			IF  @v_reportgenerated IS NULL ----------**** FOR CHECK ALLREADY INSERTED DATA By Dohatec to restrict Double Entry Check ***---------------
			BEGIN
			
			/************ REPORT MASTER TABLE **************/
			INSERT INTO tbl_ReportMaster VALUES (@v_reportName + ' Report For Tender '+@v_tenderID, @v_reportName+' Report Header', @v_reportName+' Report Footer', 'l1', @v_tenderID, @v_reportType)
			SET @v_reportID = IDENT_CURRENT('tbl_ReportMaster')

			/************ REPORT LOTS TABLE **************/
			INSERT INTO tbl_ReportLots VALUES (@v_reportID,@v_pkgLotID)

			/************ REPORT FORMS TABLE **************/
			INSERT INTO tbl_ReportForms
			SELECT	tenderFormId,
							@v_reportID
			FROM	tbl_TenderGrandSumDetail TGS
							INNER JOIN tbl_TenderGrandSum TG ON TG.tenderSumId = TGS.tenderSumId
									AND tenderId = @v_tenderID

			/************ REPORT TABLE MASTER TABLE **************/
			INSERT INTO tbl_ReportTableMaster VALUES (@v_reportID,@v_reportColNO,@v_reportName+' Report Header', @v_reportName+' Report Footer')
			SET @v_reportTableID = IDENT_CURRENT('tbl_ReportTableMaster')

			IF @v_reportType = 'TOR' ----------------*************** FOR TOR ***************----------------
			BEGIN
					/************ REPORT COLUMN MASTER TABLE **************/
					INSERT INTO tbl_ReportColumnMaster VALUES (@v_reportTableID,1,'S. No',3,1,'no')
					INSERT INTO tbl_ReportColumnMaster VALUES (@v_reportTableID,2,'Name of Tenderer',1,2,'no')
					INSERT INTO tbl_ReportColumnMaster VALUES (@v_reportTableID,3,'Quoted Amount (in BDT)',2,3,'yes')
					/************ REPORT FORMULA MASTER TABLE **************/
					INSERT INTO tbl_ReportFormulaMaster VALUES (@v_reportTableID,dbo.f_getDataForAutoFormula(@v_tenderID),3,dbo.f_getDisplayForAutoFormula(@v_tenderID))
			END
			ELSE IF @v_reportType = 'TER' ----------------*************** FOR TER ***************----------------
			BEGIN
					/************ REPORT COLUMN MASTER TABLE **************/
					INSERT INTO tbl_ReportColumnMaster VALUES (@v_reportTableID,1,'Rank',3,1,'no')
					INSERT INTO tbl_ReportColumnMaster VALUES (@v_reportTableID,2,'Name of Tenderer',1,2,'no')
					INSERT INTO tbl_ReportColumnMaster VALUES (@v_reportTableID,3,'Official Est Cost',4,3,'no')
					INSERT INTO tbl_ReportColumnMaster VALUES (@v_reportTableID,4,'Quoted Amount (in BDT)',2,4,'yes')
					INSERT INTO tbl_ReportColumnMaster VALUES (@v_reportTableID,5,'Deviation Amount',2,5,'no')
					INSERT INTO tbl_ReportColumnMaster VALUES (@v_reportTableID,6,'% of Deviaiton',2,6,'no')
					/************ REPORT FORMULA MASTER TABLE **************/
					INSERT INTO tbl_ReportFormulaMaster VALUES (@v_reportTableID,dbo.f_getDataForAutoFormula(@v_tenderID),4,dbo.f_getDisplayForAutoFormula(@v_tenderID))
					INSERT INTO tbl_ReportFormulaMaster VALUES (@v_reportTableID,@v_offEstCost+'-('+dbo.f_getDataForAutoFormula(@v_tenderID)+')',5,'Est_Cost-('+dbo.f_getDisplayForAutoFormula(@v_tenderID)+')')
					INSERT INTO tbl_ReportFormulaMaster VALUES (@v_reportTableID,'N100-('+dbo.f_getDataForAutoFormula(@v_tenderID)+')*N100/'+@v_offEstCost,6,'100-('+dbo.f_getDisplayForAutoFormula(@v_tenderID)+')*100/Est_Cost')
			END
			END	--- **** Added By Dohatec For Double Entry Check **** ---

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION
		END
		
		SET @errorCode = 'Some error occured while generating reports.'
				--PRINT @errorCode 
		RAISERROR(@errorCode , 12, 1)
	END CATCH
		
    SET NOCOUNT OFF;
END

