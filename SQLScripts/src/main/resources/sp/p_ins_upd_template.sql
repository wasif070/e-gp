USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_ins_upd_template]    Script Date: 4/24/2016 11:16:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Insert/Update/Delete a specific table 
--
--
-- Author: Karan
-- Date: 31-10-2010
--
-- Last Modified:
-- Modified By: 
-- Date 
-- Modification: 
--------------------------------------------------------------------------------
-- SP Name	   :   p_ins_upd_template
-- Module	   :   N.A.
-- Description :   Performs transaction on single Table.
-- Function	   :   
--				insert - Inserts into Table by parametrized query
--				update - Updates into Table by parametrized query
--				delete - Deletes from Table by parametrized query
ALTER PROCEDURE [dbo].[p_ins_upd_template]

@v_fields_inVc varchar(max),
@v_values_inVc varchar(max),
@v_action_inVc varchar(50),
@v_tablename_inVc varchar(500),
@v_conditions_inVc varchar(max)
	
AS

BEGIN

SET NOCOUNT ON;

Declare @v_Qry_Vc varchar(max), @v_FieldCnt_Int int, @v_Index_Int int, 
		@v_CurrField_Vc varchar(500), @v_CurrValue_Vc varchar(max), @StrColumn varchar(max)

seT @v_fields_inVc= REPLACE(@v_fields_inVc,'@$',',')
--PRINT (@v_fields_inVc)
Set @v_values_inVc=REPLACE(@v_values_inVc,'@$',',')
--PRINT (@v_values_inVc)

Select @v_FieldCnt_Int = Count(items)from dbo.f_split(@v_fields_inVc,',')
--print (@v_FieldCnt_Int)

	IF @v_action_inVc='insert'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
				SET @v_Qry_Vc = 'INSERT INTO ' + @v_tablename_inVc
									+ ' ('+@v_fields_inVc+')'
						+' VALUES '	+ '('+@v_values_inVc+')'	
				--PRINT (@v_Qry_Vc)		
				EXEC (@v_Qry_Vc)
				Select 1 as flag, 'New record inserted into table - '+@v_tablename_inVc+'.' as Message	
			COMMIT TRAN	
		END TRY 
		BEGIN CATCH
				BEGIN
					Select 0 as flag, 'Error while inserting into table - '+@v_tablename_inVc+'.' as Message
					ROLLBACK TRAN
				END
		END CATCH	 	
	END

	ELSE IF @v_action_inVc='update'
	BEGIN				
		Select @StrColumn = Coalesce (@StrColumn,'')+ Val + ', '
		 From
		(Select FieldName + '=' + FieldValue as Val From
		(Select id, items as FieldName from dbo.f_splitX(@v_fields_inVc,',')) A
		Inner Join
		(Select id, items as FieldValue from dbo.f_splitX(@v_values_inVc,',')) B
		On A.id=B.id)C

		select @StrColumn=Substring(@StrColumn,0,Len(@StrColumn))
		
		Select @v_Qry_Vc='UPDATE ' + @v_tablename_inVc + ' SET ' + @StrColumn + ' WHERE ' + @v_conditions_inVc
		print( @v_Qry_Vc)
		EXEC (@v_Qry_Vc)
	END

	ELSE IF @v_action_inVc='delete'
	BEGIN
		BEGIN TRY
			BEGIN TRAN
				IF @v_conditions_inVc Is Not Null And @v_conditions_inVc <> ''
				Begin
					SET @v_Qry_Vc = 'DELETE FROM ' + @v_tablename_inVc + ' WHERE ' + @v_conditions_inVc
					--PRINT (@v_Qry_Vc)
					EXEC (@v_Qry_Vc)
					Select 1 as flag, 'Record deleted from table - '+@v_tablename_inVc+'.' as Message
				End
			COMMIT TRAN	
		END TRY 
		BEGIN CATCH
				BEGIN
					Select 0 as flag, 'Error while deleting from table - '+@v_tablename_inVc+'.' as Message
					ROLLBACK TRAN
				END
		END CATCH	 
			
	END


SET NOCOUNT OFF;

END

