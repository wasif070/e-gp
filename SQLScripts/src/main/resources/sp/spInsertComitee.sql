USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[spInsertComitee]    Script Date: 4/24/2016 11:24:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spInsertComitee]
	@TenderID int,
	@v_CreatedBy_inInt int,
	@v_appStatus_inInt int,
	@v_minMembers_inInt int,
	@v_Status varchar(150),
	@v_ComiteType varchar(150),
	@v_TOC_inInt int,
	@v_TEC_inInt int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @v_committeeId_Int int
    -- Insert statements for procedure here
	INSERT INTO [dbo].[tbl_Committee]
           ([tenderId]
           ,[committeeName]
           ,[createdBy]
           ,[workflowStatus]
           ,[committeStatus]
           ,[minMembers]
           ,[maxMembers]
           ,[minMemOutSide]
           ,[committeeType]
           ,[isCurrent]
           ,[remarks]
           ,[createdDate]
           ,[publishDate]
           ,[minMemFromPe]
           ,[minMemFromTec])
     VALUES
           (@TenderID
           ,@v_Status          ---'Evaluation Committee'
           ,@v_CreatedBy_inInt
           ,'Approved'
           ,'Approved'
           ,@v_minMembers_inInt
           ,2
           ,0
           ,@v_ComiteType
           ,'yes'
           ,''
           ,GETDATE()
           ,GETDATE()
           ,1
           ,0)
set @v_committeeId_Int=Ident_Current('dbo.[tbl_Committee]') 



INSERT INTO [dbo].[tbl_CommitteeMembers]
           ([committeeId]
           ,[userId]
           ,[createdDate]
           ,[appStatus]
           ,[appDate]
           ,[remarks]
           ,[eSignature]
           ,[digitalSignature]
           ,[memberRole]
           ,[memberFrom]
           ,[govUserId])
     VALUES
           (@v_committeeId_Int
           ,@v_TOC_inInt
           ,GETDATE()
           ,'pending'
           ,'1900-01-01 00:00:00'
           ,''
           ,''
           ,''
           ,'cp'
           ,'Same PE'
           ,@v_TOC_inInt)

Declare @memberFrom varchar(150)
Set @memberFrom= 'TEC'--case When @v_appStatus_inInt=5 then 'Same PE' When @v_appStatus_inInt=6 then 'TEC' Else 'Same PE' END
INSERT INTO [dbo].[tbl_CommitteeMembers]
           ([committeeId]
           ,[userId]
           ,[createdDate]
           ,[appStatus]
           ,[appDate]
           ,[remarks]
           ,[eSignature]
           ,[digitalSignature]
           ,[memberRole]
           ,[memberFrom]
           ,[govUserId])
     VALUES
           (@v_committeeId_Int
           ,@v_TEC_inInt
           ,GETDATE()
           ,'pending'
           ,GETDATE()
           ,''
           ,''
           ,''
           ,'ms'
           ,@memberFrom
           ,@v_TEC_inInt)
           
           --Update tbl_Committee Set committeeType='TEC', committeStatus='approved' Where tenderId=
     INSERT INTO [dbo].[tbl_WorkFlowEventConfig]
           ([noOfReviewer]
           ,[noOfDays]
           ,[isDonorConReq]
           ,[eventId]
           ,[objectId])
     VALUES
           (0
           ,0
           ,'NO'
           ,@v_appStatus_inInt---6
           ,@TenderID)
     
     INSERT INTO [dbo].[tbl_WorkFlowLevelConfig]
						([eventId]
						,[moduleId]
						,[wfLevel]
						,[userId]
						,[wfRoleId]
						,[objectId]
						,[actionDate]
						,[activityId]
						,[childId]
						,[fileOnHand]
						,[procurementRoleId])
			Values(@v_appStatus_inInt,2,2,@v_CreatedBy_inInt,1,@TenderID,GETDATE(),@v_appStatus_inInt,@TenderID,'YES',1)			
	
	    INSERT INTO [dbo].[tbl_WorkFlowLevelConfig]
						([eventId]
						,[moduleId]
						,[wfLevel]
						,[userId]
						,[wfRoleId]
						,[objectId]
						,[actionDate]
						,[activityId]
						,[childId]
						,[fileOnHand]
						,[procurementRoleId])
			Values(@v_appStatus_inInt--6
			,2,2,@v_CreatedBy_inInt,2,@TenderID,GETDATE(),@v_appStatus_inInt,@TenderID,'NO',1)					
						
			 Insert Into tbl_WorkFlowLevelConfigHist
								(eventid, moduleId, wfLevel, userId, action, wfRoleId, objectId, childId, actionDate, actionBy, fileOnHand, workflowId, procurementRoleId )
						Select eventid, moduleId, wfLevel, userId, 'Create' action , wfRoleId, objectId, childId, actionDate, @v_CreatedBy_inInt actionBy, fileOnHand, worfklowId, procurementRoleId 
						From tbl_WorkFlowLevelConfig 
						Where activityId=@v_appStatus_inInt And objectId=@v_appStatus_inInt And childId=@TenderID 	
				
     ------===========Tbl_WorkFlowFileHistory=====---------------
	INSERT INTO [dbo].[tbl_WorkFlowFileHistory]
           ([wfFileOnHandId]
           ,[moduleName]
           ,[eventName]
           ,[activityName]
           ,[activityId]
           ,[childId]
           ,[objectId]
           ,[processDate]
           ,[endDate]
           ,[fromUserId]
           ,[toUserId]
           ,[fileSentFrom]
           ,[fileSentTo]
           ,[wfRoleid]
           ,[fileOnHand]
           ,[action]
           ,[workflowDirection]
           ,[remarks]
           ,[wfTrigger]
           ,[documentId])
     VALUES
           (@TenderID
           ,'Tender'
           ,@v_Status +' Formation Workflow'
           ,@v_Status + ' Formation Workflow'
           ,@v_appStatus_inInt---6
           ,@TenderID
           ,@TenderID
           ,GETDATE()
           ,GETDATE()
           ,@v_CreatedBy_inInt
           ,@v_TOC_inInt
           ,'PE User New - Procurement Entity'
           ,'HOPE User New - Hope User'
           ,2
           ,'Yes'
           ,'Forward'
           ,'Up'
           ,'<p>   sdf</p>'
           ,'system'
           ,'')
      
				
     INSERT INTO [dbo].[tbl_WorkFlowFileHistory]
           ([wfFileOnHandId]
           ,[moduleName]
           ,[eventName]
           ,[activityName]
           ,[activityId]
           ,[childId]
           ,[objectId]
           ,[processDate]
           ,[endDate]
           ,[fromUserId]
           ,[toUserId]
           ,[fileSentFrom]
           ,[fileSentTo]
           ,[wfRoleid]
           ,[fileOnHand]
           ,[action]
           ,[workflowDirection]
           ,[remarks]
           ,[wfTrigger]
           ,[documentId])
     VALUES
           (@TenderID
           ,'Tender'
           ,@v_Status +' Formation Workflow'
           ,@v_Status +' Formation Workflow'
           ,@v_appStatus_inInt---6
           ,@TenderID
           ,@TenderID
           ,GETDATE()
           ,GETDATE()
           ,@v_TOC_inInt
           ,@v_TEC_inInt
           ,'HOPE User New - Hope User'
           ,'PE User New - Procurement Entity'
           ,1
           ,'Yes'
           ,'pending'
           ,'Down'
           ,'<p>   sdf</p>'
           ,'system'
           ,'')
END

