USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_search_LoginReport]    Script Date: 4/24/2016 11:20:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Search In tbl_SessionMaster
--
--
-- Author: Rajesh Singh
-- Date: 4-02-2010
--
-- SP Name:  p_search_LoginReport
-- Module:   EGP ADMIN -> Login Report
-- Function: Store Procedure is use for search user details for Login report.
--------------------------------------------------------------------------------
--'08/02/2011' -- 1,5,4,6,7
-- Exec [p_search_LoginReport] 0, NULL, NULL, NULL,NULL, 1 ,1 ,0,10
ALTER PROCEDURE [dbo].[p_search_LoginReport]
		@v_Typeofuser_inVc varchar(100)=NULL,   --All--0,Tenderer--2,DP--6,Schedule Bank User--7,Admin User--1,4,5,8,Government Users--3
		@v_Searchfordate_inVc varchar(20)=NULL,
		@v_DateFrom_inVc varchar(20)=NULL,
		@v_DateTo_inVc varchar(20)=NULL,
		@v_EmailId_inVc varchar(100)=NULL,
		@v_UserId_inVc int=NULL,
		@v_UserTypeId_inVc int=NULL,

		@v_ipaddress_inVc varchar(20)=NULL,
		@v_PageId_inVc int=1,
		@v_TotalPage_inVc Int=10
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	DECLARE @v_TempQuery_Vc Varchar(max),@v_IntialQuery_Vc Varchar(1000)

	IF @v_UserTypeId_inVc=1 --IF usertypeId is 1, Admin Search
	BEGIN
		IF @v_Typeofuser_inVc=8
			BEGIN
				SET @v_TempQuery_Vc =  ' and b.userId=a.userId and userTyperId in (1,4,5,8) '
			END
		ELSE
			BEGIN
				IF @v_Typeofuser_inVc=0
					BEGIN
						SET @v_TempQuery_Vc =  ' and b.userId=a.userId '
					END
				ELSE
					BEGIN
						SET @v_TempQuery_Vc =  ' and b.userId=a.userId and userTyperId=''' + cast(@v_Typeofuser_inVc as varchar(50)) +''''
					END
			END

	END
	ELSE IF @v_UserTypeId_inVc=5 --IF usertypeId is 5, Organisation Admin
	BEGIN
		SET @v_TempQuery_Vc =  'and a.userId in ( select userId from tbl_EmployeeOffices eo,tbl_OfficeMaster
								om,tbl_EmployeeMaster em
								where eo.employeeId=em.employeeId and  eo.officeId=om.officeId
								and departmentId in (select departmentId from tbl_DepartmentMaster where
								approvingAuthorityId=''' + cast(@v_UserId_inVc as varchar(50)) +'''))'
	END
	ELSE IF @v_UserTypeId_inVc=4 --IF usertypeId is 4, PE Admin
	BEGIN
		SET @v_TempQuery_Vc = ' and a.userId in (select userId from tbl_EmployeeOffices eo,tbl_OfficeMaster
								om,tbl_EmployeeMaster em
								where eo.employeeId=em.employeeId and  eo.officeId=om.officeId
								and eo.officeId in (select officeId from tbl_OfficeAdmin where userId=''' + cast(@v_UserId_inVc as varchar(50))+'''))'
	END
	ELSE IF @v_UserTypeId_inVc=7 OR @v_UserTypeId_inVc=6 --IF usertypeId is 7 or 6, Schedule Bank OR DP Partner
	BEGIN
		SET @v_TempQuery_Vc = ' and a.userId in ( select userId from tbl_PartnerAdmin where sBankDevelopId in(  select
								sBankDevelopId from tbl_ScBankDevPartnerMaster where sBankDevelopId  in(
								select sBankDevelopId from tbl_PartnerAdmin where userId=''' + cast(@v_UserId_inVc as varchar(50)) +''')))'
	END


	--This condition is use to make search with financial Year parameter
	IF @v_Searchfordate_inVc IS NULL OR @v_Searchfordate_inVc = ''
		BEGIN
			SET @v_TempQuery_Vc = @v_TempQuery_Vc + ''
		END
	ELSE
		BEGIN
			IF @v_TempQuery_Vc =  ''
				BEGIN
					SET @v_TempQuery_Vc = @v_TempQuery_Vc + ' CONVERT(VARCHAR(10),sessionStartDt,103)sessionStartDt = '''+CONVERT(VARCHAR(10),@v_Searchfordate_inVc,103) +''''
				END
			ELSE
				BEGIN
					SET @v_TempQuery_Vc = @v_TempQuery_Vc + ' and CONVERT(VARCHAR(10),sessionStartDt,103) = '''+CONVERT(VARCHAR(10),@v_Searchfordate_inVc,103) +''''
				END
		END

	IF (@v_DateFrom_inVc is NULL OR @v_DateFrom_inVc='') and (@v_DateTo_inVc is NULL OR @v_DateTo_inVc='')
		BEGIN
				SET @v_TempQuery_Vc=@v_TempQuery_Vc
		END
	ELSE
		BEGIN
			if (@v_TempQuery_Vc='')
				BEGIN
					SET @v_TempQuery_Vc=@v_TempQuery_Vc +' convert(datetime,sessionStartDt,103) >=
convert(datetime,'''+@v_DateFrom_inVc+''',103) and convert(datetime,sessionStartDt,103) <= convert(datetime,'''+@v_DateTo_inVc+''',103)+1'
				END
			ELSE
				BEGIN
					SET @v_TempQuery_Vc=@v_TempQuery_Vc + ' AND convert(datetime,sessionStartDt,103) >=
convert(datetime,'''+@v_DateFrom_inVc+''',103) and convert(datetime,sessionStartDt,103) <= convert(datetime,'''+@v_DateTo_inVc+''',103)+1'
				END
		END



	IF @v_EmailId_inVc IS NULL OR @v_EmailId_inVc = ''
		BEGIN
			SET @v_TempQuery_Vc = @v_TempQuery_Vc + ''
		END
	ELSE
		BEGIN
			IF @v_TempQuery_Vc =  ''
				BEGIN
					SET @v_TempQuery_Vc = @v_TempQuery_Vc + ' emailId =  '''+ convert (varchar(100),@v_EmailId_inVc)+''''
				END
			ELSE
				BEGIN
					SET @v_TempQuery_Vc = @v_TempQuery_Vc + ' and emailId = '''+ convert (varchar(100),@v_EmailId_inVc)+''''
				END
		END

	IF @v_ipaddress_inVc IS NOT NULL OR @v_ipaddress_inVc != ''
	BEGIN
		IF @v_TempQuery_Vc =  ''
		BEGIN
			SET @v_TempQuery_Vc = @v_TempQuery_Vc + ' ipaddress =  ''%'+ convert (varchar(100),@v_ipaddress_inVc)+'%'''
		END
		ELSE
		BEGIN
			SET @v_TempQuery_Vc = @v_TempQuery_Vc + ' and ipaddress like ''%'+ convert (varchar(100),@v_ipaddress_inVc)+'%'''
		END
	END

	IF @v_TempQuery_Vc=''
		BEGIN
			SET @v_IntialQuery_Vc ='from tbl_SessionMaster a,tbl_LoginMaster b where b.userId=a.userId '
		END
	ELSE
		BEGIN
			SET @v_IntialQuery_Vc= 'from tbl_SessionMaster a,tbl_LoginMaster b where b.userId=a.userId '
		END

	declare @v_ExecutingQuery_Vc as varchar(max)

	IF @v_PageId_inVc=0  --when 0 is passed to PageID, then all data is displayed on one page.
		BEGIN
			set @v_ExecutingQuery_Vc='DECLARE @v_Reccountf Int
			SELECT @v_Reccountf = Count(*) From (
			SELECT * From (SELECT ROW_NUMBER() OVER (order by sessionId desc) As Rownumber
			'+@v_IntialQuery_Vc+''+@v_TempQuery_Vc+') AS DATA) AS TTT

			SELECT *,0 as TotalPages,@v_Reccountf as TotalRecords From (SELECT ROW_NUMBER() OVER (order by sessionId desc) As Rownumber,a.userId
			,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userId=a.userId) as EmailId
			,cast((select userTyperId from tbl_LoginMaster where tbl_LoginMaster.userId=a.userId )as Int) as userTyperId
			,sessionStartDt,sessionEndDt,ipAddress
			'+@v_IntialQuery_Vc+''+@v_TempQuery_Vc+') AS DATA'
		END
	ELSE
		BEGIN
			set @v_ExecutingQuery_Vc='DECLARE @v_Reccountf Int
			DECLARE @v_TotalPagef Int
			SELECT @v_Reccountf = Count(*) From (
			SELECT * From (SELECT ROW_NUMBER() OVER (order by sessionId desc) As Rownumber
			'+@v_IntialQuery_Vc+''+@v_TempQuery_Vc+') AS DATA) AS TTT
			SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_TotalPage_inVc as varchar(10))+')
			SELECT *,@v_TotalPagef as TotalPages,@v_Reccountf as TotalRecords From (SELECT ROW_NUMBER() OVER (order by sessionId desc) As Rownumber,a.userId
			,(select emailid from tbl_LoginMaster where tbl_LoginMaster.userId=a.userId) as EmailId
			,cast((select userTyperId from tbl_LoginMaster where tbl_LoginMaster.userId=a.userId )as Int) as userTyperId
			,sessionStartDt,sessionEndDt,ipAddress
			'+@v_IntialQuery_Vc+''+@v_TempQuery_Vc+') AS DATA where Rownumber between '+cast(((@v_PageId_inVc - 1) * @v_TotalPage_inVc + 1) as varchar(10))+'
			AND '+cast((@v_PageId_inVc * @v_TotalPage_inVc) as varchar(10))+''
		END

	print @v_ExecutingQuery_Vc
	Exec(@v_ExecutingQuery_Vc)
END

