USE [eGPBhutanLive21122017]
GO
/****** Object:  StoredProcedure [dbo].[p_getappcommondata]    Script Date: 24-Dec-17 3:56:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: View Get App Common Data.
-- Author: Kinjal Shah
-- Date: 29-10-2010

-- Last Modified:
-- Modified By: Kinjal Shah,  Krish

-- SP Name: [p_dump_neg_forms_data]
-- Module: APP, Package, Tender
-- Function: Store procedure use to get common information for Annual Procuremetn Plan.

-----------------------------------------------------------------------------
-- Organization: 	 Get Department details on the basis of given department type.
-- GetPEAdminOffice: Get PE admin Office details.
-- GetOrgAdminOffice:
-- GetPEUser: 		Get PE User Details.
-- Ministry: 		Get Ministry details on the basis of given department type.
-- GetPkgDetail: 	Get Package Details.
-- GetEmployee: 	Get Employee Details.
-- Division: 		Get department details on the basis of given department type.
-- ExcludePEOffice: Get OfficeId other than given PE Office.
-- Organization: 	Get Department detail on the basis of given department type.
-- Office:			Get office details on the basis of given departement id.
-- ProjectPartner: 	Get Scheduled bank Developer details on the basis of given project Id.
-- SourceOfFund: 	Get Source of fund for given project id.
-- FinancialYear: 	Get details of financial year.
-- GetAppAuth: 		Get Approving authority on the basis of given package id.
-- CheckPE: 		Get PE details basis on given user id.
-- checkQuota:
-- isTdsApplicable: Get IttHeader details if tds applicable for given section id.
-- WorkflowAction:
-- Project:			Get all 'Approved' project details.
-- AppProject:		Get App Project detail of given user id.
-- MyProject:		Get project details for given user id.
-- PendingProject:	Get details fo pending projects.
-- ProjectRoles:	Get Project Id from project role for given project id.
-- ApproveProject:	Get all 'Approved' Project details.
-- ProjectCode:		Get Project id, project code and other project details for given project id.
-- checkprojectcode:	Get project details on the basis of given project code.
-- checkprojectname:	Get project details on the basis of given project Name.
-- Programme:		Get Programme details.
-- ProjectOffice:	Get Project Office details on the basis of given project id.
-- Peoffice:		Get PE Office detail for given user id.
-- Hope:			Get Hope information for given user id and employee id.
-- Pe:				Get PE information on the basis of given office id.
-- App:				Get App details on the basis of given app Id.
-- AllApp:			Get all current finanacial year's approved APP details.
-- AppPackage:		Get all approved APP package details.
-- DevelopmentPartner:	Get Schedule bank developer partner detail for given project id.
-- dpOrganization:	Get Schedule bank Developer details.
-- dpOffice:		Get Schedule bank developer details on the basis of given ScheduleBankDeveloperHeadId.
-- procurementmethod:	Get Procurement Method details.
-- cpv:				Get CPV details for given appid.
-- WorkflowCreate:
-- WorkflowEdit:
-- FileOnHandLevel:
-- WorkflowLevel:
-- UserOrganization:	Get departement detail for department type is 'Organization'.
-- AllProcurementRoles:	Get Procurement Role details.
-- WorkflowRuleEngine:
-- DpSearchUser:	Get Schedule bank developer partener detials for given sbankDeveloperId.
-- SearchUserByRole:	Get user details on the basis of given procurement role id.
-- SearchUser:	        Get user details base on given search criteria.
-- GetAppMinistry:	Get APP Ministry details for given AppID.
-- ImplementingAgency:	Get Implementing Agencies details for given AppID.
-- getRandomCode:	Get randome code for emial verification for given user id.
-- getAppEmailId:	Get Email id of employee who have generated given APP.
-- AppPackageTenderNotCreated:	Get APP Package details for which Package not created and App status is Approved.
-- getEmployeeByRoleAndOffice:  Get Employee details on the basis of procurement Role id.
-----------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- [p_getappcommondata] 'GetAppAuth','268'
ALTER PROCEDURE [dbo].[p_getappcommondata]
	@v_fieldName1Vc varchar(200)=NULL,
	@v_fieldName2Vc varchar(8000)=NULL,
	@v_fieldName3Vc varchar(20)=NULL
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

IF @v_fieldName1Vc = 'Organization'  --Get all rows from table tbl_FinancialYear
BEGIN
select convert(varchar(400),departmentId) as FieldValue1, departmentName as FieldValue2,'' AS FieldValue3,
'' AS FieldValue4,'' AS  FieldValue5,'' AS FieldValue6 from tbl_departmentMaster where departmentType=@v_fieldName1Vc
END

IF @v_fieldName1Vc = 'GetPEAdminOffice'  --Get all rows from table tbl_FinancialYear
BEGIN
 select convert(varchar(400),om.officeId) as FieldValue1, om.officename as FieldValue2 from tbl_officeAdmin oa, tbl_OfficeMaster om where oa.userId=@v_fieldName2Vc and om.officeId=oa.officeId

 END

 IF @v_fieldName1Vc = 'checkInitiator'
 BEGIN
	select convert(varchar(400),worfklowId) as FieldValue1, convert(varchar(400),wfLevel) as FieldValue2,convert(varchar(400),procurementRoleId) as FieldValue3, convert(varchar(400),wfRoleId) as FieldValue4,convert(varchar(400),eventId) as FieldValue5, convert(varchar(400),activityId) as FieldValue6  from tbl_WorkFlowLevelConfig where userId = @v_fieldName3Vc and objectId = @v_fieldName2Vc

 END

IF @v_fieldName1Vc = 'AppCodeForDepositWork'
BEGIN
	DECLARE @v_OfficeName_Vc varchar(150),
	@v_DepartmentName_Vc varchar(150),
	@v_AppStatus_Vc varchar(20),
	@v_IsTenderCreated_Vc varchar(10),
	@v_AppID_Int int,
	@v_AppType_Int int,
	@v_IsAlreadyDeposited_Vc varchar(10)

	select @v_OfficeName_Vc = officeName , @v_DepartmentName_Vc = departmentName, @v_AppStatus_Vc = appStatus, @v_AppID_Int = appId, @v_AppType_Int = appType
	from 
		(select om.officeName,am.departmentId,am.appStatus,am.appId,am.appType
		from tbl_OfficeMaster om
		inner join tbl_AppMaster am on am.officeId = om.officeId
		where am.appCode = @v_fieldName2Vc) et
		inner join tbl_DepartmentMaster dp on dp.departmentId=et.departmentId

	IF @v_AppID_Int in (select distinct appId from tbl_TenderMaster)
	Begin
		Set @v_IsTenderCreated_Vc = 'YES'
	End
	Else
	Begin
		Set @v_IsTenderCreated_Vc = 'NO'
	End

	IF @v_AppID_Int in (select distinct refAppId from tbl_AppMaster where refAppId is not null)
	Begin
		Set @v_IsAlreadyDeposited_Vc = 'YES'
	End
	Else
	Begin
		Set @v_IsAlreadyDeposited_Vc = 'NO'
	End

	Select @v_OfficeName_Vc as FieldValue1,
		   @v_DepartmentName_Vc as FieldValue2,
		   @v_AppStatus_Vc as FieldValue3,
		   @v_IsTenderCreated_Vc as FieldValue4,
		   CONVERT(VARCHAR(10), @v_AppID_Int) as FieldValue5,
		   @v_IsAlreadyDeposited_Vc as FieldValue6,
		   CONVERT(VARCHAR(10), @v_AppType_Int) as FieldValue7
END

IF @v_fieldName1Vc = 'AppCode'
BEGIN
	select officeName as FieldValue1 
	from tbl_OfficeMaster om
	inner join tbl_AppMaster am on am.officeId = om.officeId
	where am.appCode = @v_fieldName2Vc

END

IF @v_fieldName1Vc = 'ImplementingAgency'
BEGIN
	select convert(varchar(20),t3.appId) as FieldValue1,t3.appCode as FieldValue2,t3.departmentName as FieldValue3,t3.officeName as FieldValue4,convert(varchar(20),ap.packageId) as FieldValue5,ap.packageNo as FieldValue6,ap.packageDesc as FieldValue7,convert(varchar(20),ap.estimatedCost) as FieldValue8,ap.procurementnature as FieldValue9
	from
		(select t2.appId, t2.departmentId, dm.departmentName, t2.officeId, t2.officeName, t2.appCode
		from
			(select t1.appId,t1.departmentId,t1.officeId,t1.appCode,om.officeName
			from
				(select appId,officeId,departmentId,appCode
				from tbl_AppMaster
				where refAppId = @v_fieldName2Vc and appStatus='Approved') t1
			inner join tbl_OfficeMaster om on t1.officeId=om.officeId) t2
		inner join tbl_DepartmentMaster dm on t2.departmentId=dm.departmentId) t3
	inner join tbl_AppPackages ap on ap.appId=t3.appId
END

IF @v_fieldName1Vc = 'ImplementingAgencyDetails'
BEGIN
	select  convert(varchar(50),t1.appId) as FieldValue1,convert(varchar(50),t1.officeId) as FieldValue2,om.officeName as FieldValue3,om.address as FieldValue4
	from
		(select appId,officeId
		from tbl_AppMaster
		where refAppId = @v_fieldName2Vc and appStatus='Approved') t1
	inner join tbl_OfficeMaster om on t1.officeId=om.officeId
END


IF @v_fieldName1Vc = 'AppCodeFilter'
BEGIN
	select convert(varchar(200),appId)  as FieldValue1 
	from tbl_AppMaster 
	where appCode = @v_fieldName2Vc

END



 IF @v_fieldName1Vc = 'GetOrgAdminOffice'  
BEGIN
 --select distinct convert(varchar(400),om.officeId) as FieldValue1, om.officename as FieldValue2 from  tbl_OfficeMaster om where om.departmentId in (select dm.departmentId from tbl_DepartmentMaster dm where dm.approvingAuthorityId = @v_fieldName2Vc)
select distinct convert(varchar(400),om.officeId) as FieldValue1, om.officename as FieldValue2 from  tbl_OfficeMaster om inner join tbl_officeadmin am on om.officeId = am.officeID and am.userid = @v_fieldName2Vc
 END

IF @v_fieldName1Vc = 'GetPEUser'  --Get all rows from table tbl_FinancialYear
BEGIN
select convert(varchar(400),e.userid) as FieldValue1,convert(varchar(400),e.employeeName) as FieldValue2 from tbl_employeemaster e,tbl_EmployeeRoles er
where e.employeeid=er.employeeid and
procurementroleid=@v_fieldName2Vc and
 e.employeeid in (select employeeId from tbl_EmployeeOffices where officeId in(select officeId from tbl_OfficeAdmin where userId=@v_fieldName3Vc))


  END
IF @v_fieldName1Vc = 'Ministry'  --Get all rows from table tbl_FinancialYear
BEGIN
select convert(varchar(400),departmentId) as FieldValue1, departmentName as FieldValue2,'' AS FieldValue3,
'' AS FieldValue4,'' AS  FieldValue5,'' AS FieldValue6 from tbl_departmentMaster where departmentType=@v_fieldName1Vc
END
IF @v_fieldName1Vc = 'GetPkgDetail'  --Get all rows from table tbl_FinancialYear
BEGIN
select convert(varchar(400),procurementMethodId) as FieldValue1, isPQRequired as FieldValue2,reoiRfaRequired AS FieldValue3,
'' AS FieldValue4,'' AS  FieldValue5,'' AS FieldValue6 from tbl_Apppackages where packageId=@v_fieldName3Vc
END
declare @v_FinalQueryVc varchar(Max)=null, @v_Query1Vc varchar(1000)=null

IF @v_fieldName1Vc = 'GetEmployee'  --Get all rows from table tbl_FinancialYear
BEGIN
	set @v_Query1Vc = ''
	if @v_fieldName2Vc = 4
		begin
		set @v_Query1Vc = ' and  (employeeId in (select employeeId from tbl_EmployeeOffices where officeId in(select officeId from tbl_OfficeAdmin where userId='+ @v_fieldName3Vc+')) or createdBy='+ @v_fieldName3Vc+')'
		End
	else if @v_fieldName2Vc = 5
	begin
		set @v_Query1Vc = ' and  ( employeeId in (select employeeId from tbl_EmployeeOffices where officeId in(select officeId from tbl_OfficeMaster where departmentId in(select departmentId from tbl_DepartmentMaster where approvingAuthorityId='+@v_fieldName3Vc+'))) or  createdBy='+ @v_fieldName3Vc+' )'
	End

	set @v_FinalQueryVc =
'select CONVERT(VARCHAR(10),a.employeeId) FieldValue1, FieldValue2, FieldValue3,REPLACE(CONVERT(VARCHAR(11),registereddate, 106), '' '', ''-'') FieldValue4, FieldValue5 ,convert(varchar(20),isnull(b.employeeId,0)) FieldValue6  from (select  e.employeeId  ,employeeName as FieldValue2,emailId as FieldValue3,registereddate   ,status as FieldValue5  from tbl_EmployeeMaster e,tbl_LoginMaster l where
e.userId=l.userId ' + @v_Query1Vc +')a left outer join (select distinct employeeid from tbl_employeeroles)b on a.employeeid=b.employeeid  order by FieldValue2   '



exec(@v_FinalQueryVc)
END
IF @v_fieldName1Vc = 'Division'  --Get all rows from table tbl_FinancialYear
BEGIN
select convert(varchar(400),departmentId) as FieldValue1, departmentName as FieldValue2,'' AS FieldValue3,
'' AS FieldValue4,'' AS  FieldValue5,'' AS FieldValue6 from tbl_departmentMaster where departmentType=@v_fieldName1Vc
END
IF @v_fieldName1Vc = 'ExcludePEOffice'  --Get all rows from table tbl_FinancialYear
BEGIN

 select  convert(varchar(400),officeId) as FieldValue1 from tbl_EmployeeRoles e,tbl_EmployeeOffices f
 where e.employeeId=f.employeeId and procurementRoleId=1 and officeid in(@v_fieldName2Vc)
 and e.employeeId !=@v_fieldName3Vc
 END
IF @v_fieldName1Vc = 'Organization'  --Get all rows from table tbl_FinancialYear
BEGIN
select convert(varchar(400),departmentId) as FieldValue1, departmentName as FieldValue2,'' AS FieldValue3,
'' AS FieldValue4,'' AS  FieldValue5,'' AS FieldValue6 from tbl_departmentMaster where departmentType=@v_fieldName1Vc
END
IF @v_fieldName1Vc = 'Office'  --Get all rows from table tbl_FinancialYear
BEGIN
select convert(varchar(400),officeId) as FieldValue1, officeName as FieldValue2,'' AS FieldValue3,
'' AS FieldValue4,'' AS  FieldValue5,'' AS FieldValue6 from tbl_OfficeMaster  where departmentId=@v_fieldName2Vc
END
ELSE IF @v_fieldName1Vc = 'ProjectPartner' --Get all rows from table tbl_ScBankDevPartnerMaster, tbl_ProjectMaster
BEGIN
Select convert(varchar(400),s.sBankDevelopId) AS FieldValue1,sbDevelopName AS FieldValue2,'' AS
FieldValue3,'' AS FieldValue4,'' AS FieldValue5,'' AS FieldValue6 from
[dbo].[tbl_ScBankDevPartnerMaster] s,[dbo].[tbl_ProjectPartners] p
 where partnertype='Development' and s.sBankDevelopId=p.sBankDevelopId
 and p.projectId=@v_fieldName2Vc

END
IF @v_fieldName1Vc = 'SourceOfFund' --Get all rows from table tbl_ProjectMaster
BEGIN
Select '' AS FieldValue1,replace(p.sourceOfFund,',',', ') AS FieldValue2,'' AS
FieldValue3,'' AS FieldValue4,'' AS FieldValue5,'' AS FieldValue6 from
[dbo].[tbl_ProjectMaster] p
where  p.projectId=@v_fieldName2Vc
END

IF @v_fieldName1Vc = 'FinancialYear'  --Get all rows from table tbl_FinancialYear
BEGIN
select convert(varchar(400),financialYearId) as FieldValue1, financialYear as FieldValue2,isCurrent AS FieldValue3,
'' AS FieldValue4,'' AS  FieldValue5,'' AS FieldValue6 from tbl_FinancialYear order by financialYear asc
END

IF @v_fieldName1Vc = 'GetAppAuth'  --Get Approving Authority By packageId  --> Krish : For AddPackageDates.jsp
BEGIN
	SELECT CONVERT(VARCHAR(30), procurementRoleId) AS FieldValue1, pr.procurementRole AS FieldValue2 FROM
	dbo.tbl_AppPackages ap INNER JOIN dbo.tbl_ProcurementRole pr ON
	ap.approvingAuthEmpId = pr.procurementRoleId
	WHERE ap.packageId = @v_fieldName2Vc
END

IF @v_fieldName1Vc = 'CheckPE'  --Get all rows from table tbl_FinancialYear
BEGIN
select convert(varchar(400),userId) as FieldValue1 from tbl_employeemaster e,tbl_EmployeeRoles er
where  e.employeeId=er.employeeId and procurementRoleId=1 and userId=@v_fieldName2Vc

END

IF @v_fieldName1Vc = 'CheckCreator'
BEGIN
select convert(varchar(400),createdBy) as FieldValue1 from tbl_TenderMaster
where  tenderId=@v_fieldName2Vc and createdBy=@v_fieldName3Vc

END

IF @v_fieldName1Vc = 'CheckAu'  --Add by Aprojit
BEGIN
select convert(varchar(400),userId) as FieldValue1 from tbl_employeemaster e,tbl_EmployeeRoles er
where  e.employeeId=er.employeeId and procurementRoleId=5 and userId=@v_fieldName2Vc

END

--added by Nishith
IF @v_fieldName1Vc = 'CheckHOPE'  --Get all rows from table tbl_FinancialYear
BEGIN
select convert(varchar(400),userId) as FieldValue1 from tbl_employeemaster e,tbl_EmployeeRoles er
where  e.employeeId=er.employeeId and procurementRoleId=6 and userId=@v_fieldName2Vc

END

-- added by sristy
IF @v_fieldName1Vc = 'CheckHOPE'  --Get all rows from table tbl_FinancialYear
BEGIN
select convert(varchar(400),userId) as FieldValue1 from tbl_employeemaster e,tbl_EmployeeRoles er
where  e.employeeId=er.employeeId and procurementRoleId=6 and userId=@v_fieldName2Vc

END
-- end

IF @v_fieldName1Vc = 'checkQuota'  --Get all rows from table tbl_FinancialYear
BEGIN
IF exists(select tcd.documentSize from dbo.tbl_TempCompanyDocuments tcd ,dbo.tbl_TempTendererMaster ttd where tcd.tendererId=ttd.tendererId and ttd.userId=@v_fieldName2Vc)
BEGIN
	select 'FieldValue1' =CASE WHEN SUM(cast(tcd.documentSize as int)/(1024*1024)) <= (select totalSize from tbl_ConfigurationMaster where userType='tenderer') THEN 'ok' ELSE 'Your file upload quota is over' END from dbo.tbl_TempCompanyDocuments tcd ,dbo.tbl_TempTendererMaster ttd where tcd.tendererId=ttd.tendererId and ttd.userId=@v_fieldName2Vc
END
ELSE
BEGIN
	SELECT 'ok' as FieldValue1
END
END
IF @v_fieldName1Vc = 'isTdsApplicable'  --Get all rows from table tbl_FinancialYear
BEGIN
SELECT convert(varchar(400),h.ittheaderid) as FieldValue1, ittHeaderName as FieldValue2,'' AS FieldValue3,
'' AS FieldValue4,'' AS  FieldValue5,'' AS FieldValue6
  FROM [dbo].[tbl_IttSubClause] i,
  [dbo].[tbl_IttClause] l,[dbo].[tbl_IttHeader] h
  where i.ittClauseId=l.ittClauseId and l.ittHeaderId=h.ittHeaderId
  and isTdsApplicable ='Yes' and sectionId=@v_fieldName2Vc

END
IF @v_fieldName1Vc = 'WorkflowAction'  --Get all rows from table tbl_FinancialYear
BEGIN
select convert(varchar(400),actionId) as FieldValue1, action as FieldValue2,(select distinct eventname from tbl_ModuleMaster m,tbl_EventMaster e where m.moduleId=e.moduleId and e.eventid=@v_fieldName2Vc) AS FieldValue3,
(select distinct moduleName from tbl_ModuleMaster m,tbl_EventMaster e where m.moduleId=e.moduleId and e.eventid=@v_fieldName2Vc) AS FieldValue4,'' AS  FieldValue5,'' AS FieldValue6 from tbl_ActionMaster
END
ELSE IF @v_fieldName1Vc = 'Project'  --Get all rows from table tbl_ProjectMaster
BEGIN
SELECT convert(varchar(400),[projectId]) AS FieldValue1,[projectName] AS FieldValue2,[projectCode] AS FieldValue3,
'Project' AS FieldValue4,'' AS  FieldValue5,'' AS FieldValue6 FROM tbl_ProjectMaster
where  projectStatus='Approved'
END
ELSE IF @v_fieldName1Vc = 'AppProject'  --Get all rows from table tbl_ProjectMaster
BEGIN
SELECT convert(varchar(400),[projectId]) AS FieldValue1,[projectName] AS FieldValue2,[projectCode] AS FieldValue3,
'Project' AS FieldValue4,projectEndDate  FieldValue5,'' AS FieldValue6 FROM tbl_ProjectMaster
where  projectStatus='Approved' and projectid in(SELECT  projectId
  FROM [dbo].[tbl_ProjectRoles] pr  where   pr.userId=@v_fieldName2Vc
   ) and sourceoffund like    '%'+@v_fieldName3Vc+'%'
   -- and projectid not in (select projectid from tbl_appmaster)
END
ELSE IF @v_fieldName1Vc = 'MyProject'  --Get all rows from table tbl_ProjectMaster
BEGIN
SELECT convert(varchar(400),[projectId]) AS FieldValue1,[projectName] AS FieldValue2,[projectCode] AS FieldValue3,
'Project' AS FieldValue4,projectEndDate  FieldValue5,'' AS FieldValue6 FROM tbl_ProjectMaster
where  projectStatus='Approved' and projectid in(SELECT  projectId
  FROM [dbo].[tbl_ProjectRoles] pr  where   pr.userId=@v_fieldName2Vc
   )
   -- and projectid not in (select projectid from tbl_appmaster)
END
ELSE IF @v_fieldName1Vc = 'PendingProject'  --Get all rows from table tbl_ProjectMaster
BEGIN
declare @v_qry varchar(Max) =null

If (select userTyperId from tbl_LoginMaster where userId=@v_fieldName3Vc)=1
Begin -- eGP Admin Case
	Set @v_qry =
	'SELECT convert(varchar(400),[projectId]) AS FieldValue1,[projectName] AS FieldValue2,
	[projectCode] AS FieldValue3,
	convert(varchar(400), Convert(decimal,(round([projectCost], 0)),0) ) AS FieldValue4,REPLACE(CONVERT(VARCHAR(11),projectStartDate, 106), '' '', ''-'') AS  FieldValue5,REPLACE(CONVERT(VARCHAR(11),projectEndDate, 106), '' '', ''-'') AS FieldValue6
	FROM tbl_ProjectMaster where
	projectStatus=''Pending''  ' + @v_fieldName2Vc
End
Else
Begin

	Set @v_qry =
	'SELECT convert(varchar(400),[projectId]) AS FieldValue1,[projectName] AS FieldValue2,
	[projectCode] AS FieldValue3,
	convert(varchar(400), Convert(decimal,(round([projectCost], 0)),0) ) AS FieldValue4,REPLACE(CONVERT(VARCHAR(11),projectStartDate, 106), '' '', ''-'') AS  FieldValue5,REPLACE(CONVERT(VARCHAR(11),projectEndDate, 106), '' '', ''-'') AS FieldValue6
	FROM tbl_ProjectMaster where
	(projectId in
			(	select projectId from tbl_ProjectOffice PO
				inner join tbl_OfficeMaster OA On PO.officeId=OA.officeId
				inner join tbl_officeAdmin oad on OA.officeId = oad.OfficeID and oad.userid = ' + @v_fieldName3Vc + '
			) OR createdBy =' + @v_fieldName3Vc + ')
	and projectStatus=''Pending''  ' + @v_fieldName2Vc

End

print  @v_qry
exec(@v_qry)
END
ELSE IF @v_fieldName1Vc = 'ProjectRoles'  --Get all rows from table tbl_ProjectMaster
BEGIN
SELECT convert(varchar(400),[projectId]) AS FieldValue1,'' AS FieldValue2,
'' AS FieldValue3,
'' AS FieldValue4,'' AS  FieldValue5,'' AS FieldValue6 FROM tbl_ProjectRoles where projectId=@v_fieldName2Vc
END
ELSE IF @v_fieldName1Vc = 'ApproveProject'  --Get all rows from table tbl_ProjectMaster
BEGIN

declare @v_qry1 varchar(max)=null
print '22'

	If (select userTyperId from tbl_LoginMaster where userId=@v_fieldName3Vc)=1
	Begin -- eGP Admin Case
			set @v_qry1 = 'SELECT convert(varchar(400),[projectId]) AS FieldValue1,[projectName] AS FieldValue2,
		[projectCode] AS FieldValue3, convert(varchar(400),projectCost) AS FieldValue4,REPLACE(CONVERT(VARCHAR(11),projectStartDate, 106), '' '', ''-'') AS  FieldValue5,REPLACE(CONVERT(VARCHAR(11),projectEndDate, 106), '' '', ''-'') AS FieldValue6
		FROM tbl_ProjectMaster
		where
		projectStatus=''Approved'' ' +  @v_fieldName2Vc + '  '
	End
	Else
	Begin
			set @v_qry1 = 'SELECT convert(varchar(400),[projectId]) AS FieldValue1,[projectName] AS FieldValue2,
		[projectCode] AS FieldValue3, convert(varchar(400),projectCost) AS FieldValue4,REPLACE(CONVERT(VARCHAR(11),projectStartDate, 106), '' '', ''-'') AS  FieldValue5,REPLACE(CONVERT(VARCHAR(11),projectEndDate, 106), '' '', ''-'') AS FieldValue6
		FROM tbl_ProjectMaster
		where
		(projectId in
			(	select projectId from tbl_ProjectOffice PO
				inner join tbl_OfficeMaster OA On PO.officeId=OA.officeId
				Where departmentid in(select departmentid from tbl_departmentmaster where approvingAuthorityId =' + @v_fieldName3Vc + ')
			) OR createdBy =' + @v_fieldName3Vc + ')
		and projectStatus=''Approved'' ' +  @v_fieldName2Vc + '  '

	End



print  @v_qry1
exec(@v_qry1)
END
ELSE IF @v_fieldName1Vc = 'ProjectCode'  --Get all rows from table tbl_ProjectMaster
BEGIN
SELECT convert(varchar(400),[projectId]) AS FieldValue1,[projectName] AS FieldValue2,[projectCode] AS FieldValue3,
'Project' AS FieldValue4,'' AS  FieldValue5,'' AS FieldValue6 FROM tbl_ProjectMaster where projectid=@v_fieldName2Vc
END
ELSE IF @v_fieldName1Vc = 'checkprojectcode'  --Get all rows from table tbl_ProjectMaster
BEGIN
SELECT convert(varchar(400),[projectId]) AS FieldValue1,[projectName] AS FieldValue2,[projectCode] AS FieldValue3,
'Project' AS FieldValue4,'' AS  FieldValue5,'' AS FieldValue6 FROM tbl_ProjectMaster where projectCode=@v_fieldName2Vc and createdby = @v_fieldName3Vc
END
ELSE IF @v_fieldName1Vc = 'checkprojectname'  --Get all rows from table tbl_ProjectMaster
BEGIN
SELECT convert(varchar(400),[projectId]) AS FieldValue1,[projectName] AS FieldValue2,[projectCode] AS FieldValue3,
'Project' AS FieldValue4,'' AS  FieldValue5,'' AS FieldValue6 FROM tbl_ProjectMaster where projectName=@v_fieldName2Vc and createdby = @v_fieldName3Vc
END
ELSE IF @v_fieldName1Vc = 'Programme' --Get all rows from table tbl_ProgrammeMaster
BEGIN
SELECT convert(varchar(400),ProgId) AS FieldValue1,ProgName AS FieldValue2,ProgCode AS FieldValue3,'' AS FieldValue4,
'' AS FieldValue5,'' AS FieldValue6 FROM tbl_ProgrammeMaster
END
ELSE IF @v_fieldName1Vc = 'ProjectOffice' --Get all rows from table tbl_ProjectMaster,tbl_OfficeMaster and tbl_ProjectOffice
BEGIN
SELECT distinct convert(varchar(400), p.projectId) AS FieldValue1,OfficeName AS FieldValue2,convert(varchar(400),o.OfficeId) AS  FieldValue3,convert(varchar(400),departmentId) AS FieldValue4,
'' AS FieldValue5,'' AS FieldValue6
FROM [dbo].[tbl_ProjectMaster]  p ,[dbo].[tbl_OfficeMaster] O ,tbl_ProjectOffice po
            WHERE p.projectid=po.projectId and po.officeid =o.officeId and p.projectId=@v_fieldName2Vc
            and o.officeId in(SELECT  officeid
  FROM [dbo].[tbl_ProjectRoles] pr,
  tbl_ProjectOffice po where pr.projectid=po.projectId and po.userId=@v_fieldName3Vc  and pr.projectId=@v_fieldName2Vc)
END

ELSE IF @v_fieldName1Vc = 'Peoffice'  --Get all rows from table tbl_OfficeMaster
BEGIN
SELECT convert(varchar(400),eo.officeid) AS FieldValue1,OfficeName AS FieldValue2,convert(varchar(400),eo.OfficeId) AS FieldValue3 ,convert(varchar(400),departmentId) AS FieldValue4,
'' AS FieldValue5,'' AS FieldValue6
FROM [dbo].[tbl_EmployeeMaster] e,[dbo].tbl_EmployeeOffices eo,
[dbo].tbl_OfficeMaster om
WHERE e.employeeId=eo.employeeId and eo.officeid=om.officeid and userid=@v_fieldName2Vc
END
ELSE IF @v_fieldName1Vc = 'Hope'  --Get all rows from table tbl_EmployeeMaster,tbl_EmployeeOffices,tbl_EmployeeRoles and tbl_procurementRole
BEGIN
select convert(varchar(400),em.employeeId) AS FieldValue1,EmployeeName AS FieldValue2,'' AS FieldValue3,'' AS FieldValue4 ,'' AS FieldValue5,'' AS FieldValue6 from [dbo].[tbl_EmployeeMaster] em,[dbo].tbl_EmployeeOffices eo,
[dbo].[tbl_EmployeeRoles]  er, [dbo].[tbl_procurementRole]  pr where em.employeeId=er.employeeId
   and pr.procurementroleid=er.procurementroleid and procurementrole='HOPE' and em.employeeid=eo.employeeid and officeid in
(select officeid from [dbo].[tbl_EmployeeRoles] e,[dbo].[tbl_EmployeeMaster] em where userid=@v_fieldName2Vc and e.employeeId=em.employeeid )
END
ELSE IF @v_fieldName1Vc = 'Pe'  --Get all rows from table tbl_EmployeeMaster,tbl_EmployeeOffices,tbl_EmployeeRoles and tbl_procurementRole
BEGIN
	SELECT	CONVERT(VARCHAR(400),em.employeeId) AS FieldValue1,
			EmployeeName AS FieldValue2,
			'' AS FieldValue3,
			'' AS FieldValue4 ,
			'' AS FieldValue5,
			'' AS FieldValue6
	FROM	[dbo].[tbl_EmployeeMaster] em,[dbo].tbl_EmployeeOffices eo,
			[dbo].[tbl_EmployeeRoles]  er, [dbo].[tbl_procurementRole]  pr,
			[dbo].[tbl_LoginMaster] LM
	WHERE	em.employeeId=er.employeeId
			AND pr.procurementroleid=er.procurementroleid
			AND procurementrole='PE'
			AND em.employeeid=eo.employeeid
			AND LM.userId=em.userId
			AND LM.status='approved'
			AND officeid = @v_fieldName2Vc
END
ELSE IF @v_fieldName1Vc = 'App' --Get all rows from table tbl_EmployeeMaster,tbl_EmployeeOffices,tbl_EmployeeRoles and tbl_procurementRole
BEGIN
--Budget Type changed as Development to Capital and Revenue to Recurrent by Proshanto Kumar Saha
Select convert(varchar(400),appid) AS FieldValue1,convert(varchar(400),projectid) AS FieldValue2,appcode AS FieldValue3,financialyear AS FieldValue4 ,convert(varchar(400),case when budgetType=1 then 'Capital' when budgetType=2 then 'Recurrent' else 'Own Fund' End) AS FieldValue5,projectName AS FieldValue6, convert(varchar(400),createdBy) as FieldValue7, activityName as FieldValue8 from [dbo].[tbl_AppMaster]
where appId=@v_fieldName2Vc
END


--framework contract package list for app where no pe office 
ELSE IF @v_fieldName1Vc = 'AppOfficeFramework' --Done by nafiul for, officer/APPDashboard.jsp
BEGIN
select convert(varchar(400),am.appId) as FieldValue1, convert(varchar(400),ap.packageId) as FieldValue2
from tbl_AppMaster am 
inner join tbl_AppPackages ap on am.appId=ap.appId
where am.appId=@v_fieldName2Vc and ap.procurementMethodId=18 and ap.PackageID  not in 
(select PackageID from tbl_AppFrameworkOffice)
END


ELSE IF @v_fieldName1Vc = 'AllApp' --Get all rows from table tbl_EmployeeMaster,tbl_EmployeeOffices,tbl_EmployeeRoles and tbl_procurementRole
BEGIN

	IF @v_fieldName2Vc=''
		BEGIN
			--Select convert(varchar(400),appid) AS FieldValue1,convert(varchar(400),appcode) AS FieldValue2,'' AS FieldValue3,'' AS FieldValue4 ,'' AS FieldValue5,'' AS FieldValue6 from [dbo].[tbl_AppMaster]
			--where appStatus='Approve'
			SELECT convert(varchar(400),tbl_AppMaster.appid) AS FieldValue1,convert(varchar(400),tbl_AppMaster.appcode) AS FieldValue2,'' AS FieldValue3,'' AS FieldValue4 ,'' AS FieldValue5,'' AS FieldValue6
			FROM tbl_AppMaster INNER JOIN
				 tbl_FinancialYear ON tbl_AppMaster.financialYear = tbl_FinancialYear.financialYear
			WHERE tbl_AppMaster.appStatus='Approved' and tbl_FinancialYear.isCurrent='Yes'
			 and
			 officeId in (SELECT officeid FROM tbl_EmployeeOffices WHERE employeeId in (SELECT employeeId FROM tbl_EmployeeMaster WHERE userId =  @v_fieldName3Vc) union all
 select officeid from tbl_ProjectOffice where  userId =@v_fieldName3Vc )
			and appId not in (select distinct refAppId from tbl_AppMaster where refAppId is not null)
			 Order by tbl_AppMaster.appcode
		END
	ELSE
		BEGIN
			Select distinct convert(varchar(400),tbl_AppMaster.appid) AS FieldValue1,convert(varchar(400),tbl_AppMaster.appcode) AS FieldValue2,'' AS FieldValue3,'' AS FieldValue4 ,'' AS FieldValue5,'' AS FieldValue6 from [dbo].[tbl_AppMaster], [dbo].[tbl_AppPackages]
			where tbl_AppMaster.appStatus='Approved' and financialYear=@v_fieldName2Vc
			and
			tbl_AppMaster.appId = tbl_AppPackages.appId and tbl_AppPackages.appStatus = 'Approved'
			and 
			tbl_appPackages.packageid not in(select packageid from tbl_tendermaster t,tbl_tenderdetails td where t.tenderid=td.tenderid and tenderstatus in ('Pending','Approved'))

			and
			 officeId in (SELECT officeid FROM tbl_EmployeeOffices WHERE employeeId in (SELECT employeeId FROM tbl_EmployeeMaster WHERE userId = @v_fieldName3Vc) union all
 select officeid from tbl_ProjectOffice where  userId =@v_fieldName3Vc ) 
			and tbl_AppMaster.appId not in (select distinct refAppId from tbl_AppMaster where refAppId is not null)
		END

END
ELSE IF @v_fieldName1Vc = 'AppPackage' --Get all rows from table tbl_EmployeeMaster,tbl_EmployeeOffices,tbl_EmployeeRoles and tbl_procurementRole
BEGIN

Select convert(varchar(400),[packageId]) AS FieldValue1,convert(varchar(400),[packageNo]) AS FieldValue2,convert(varchar(2000),[packagedesc]) AS FieldValue3,'' AS FieldValue4 ,'' AS FieldValue5,'' AS FieldValue6 FROM [dbo].[tbl_AppPackages] where appId=@v_fieldName2Vc
 	and appStatus='Approved'
END
ELSE IF @v_fieldName1Vc = 'Partner' --Get all rows from table tbl_ScBankDevPartnerMaster, tbl_ProjectMaster
BEGIN
Select convert(varchar(400),projectid) AS FieldValue1,sBankDevelopId AS FieldValue2,sbDevelopName AS FieldValue3,projectname AS FieldValue4,'' AS FieldValue5,'' AS FieldValue6 from [dbo].[tbl_ScBankDevPartnerMaster] s, [dbo].[tbl_ProjectMaster] p where projectid= @v_fieldName2Vc
END
ELSE IF @v_fieldName1Vc = 'DevelopmentPartner' --Get all rows from table tbl_ScBankDevPartnerMaster, tbl_ProjectMaster
BEGIN
Select convert(varchar(400),sBankDevelopId) AS FieldValue1,sbDevelopName AS FieldValue2,'' AS
FieldValue3,'' AS FieldValue4,'' AS FieldValue5,'' AS FieldValue6 from
[dbo].[tbl_ScBankDevPartnerMaster] s  where partnertype='Development' and isBranchOffice='no'

END
ELSE IF @v_fieldName1Vc = 'dpOrganization' --Get all rows from table tbl_ScBankDevPartnerMaster, tbl_ProjectMaster
BEGIN
Select convert(varchar(400),sBankDevelopId) AS FieldValue1,sbDevelopName AS FieldValue2,'' AS
FieldValue3,'' AS FieldValue4,'' AS FieldValue5,'' AS FieldValue6 from
[dbo].[tbl_ScBankDevPartnerMaster] s  where partnertype='Development' and isBranchOffice='No'

END
ELSE IF @v_fieldName1Vc = 'dpOffice' --Get all rows from table tbl_ScBankDevPartnerMaster, tbl_ProjectMaster
BEGIN
Select convert(varchar(400),sBankDevelopId) AS FieldValue1,sbDevelopName AS FieldValue2,'' AS
FieldValue3,'' AS FieldValue4,'' AS FieldValue5,'' AS FieldValue6 from
[dbo].[tbl_ScBankDevPartnerMaster] s  where partnertype='Development' and isBranchOffice='Yes' and
sBankDevelHeadId=@v_fieldName2Vc

END
ELSE IF @v_fieldName1Vc = 'procurementmethod' --Get all rows from table tbl_ProcurementMethod
BEGIN
Select convert(varchar(400),procurementmethodid) AS FieldValue1,
  procurementmethod
AS FieldValue2,procurementFullName AS FieldValue3,'' AS FieldValue4,'' AS FieldValue5,'' AS FieldValue6 from [dbo].[tbl_ProcurementMethod]
END
ELSE IF @v_fieldName1Vc = 'cpv' --Get all rows from table tbl_cpvclassification
BEGIN
Select cpvcode AS FieldValue1,CpvDescription AS FieldValue2,'' AS FieldValue3,'' AS FieldValue4,'' AS FieldValue5,'' AS FieldValue6 from  [dbo].[tbl_cpvclassification]  where cpvcode in
(select cpvcode from [dbo].[tbl_AppMaster] where appid= @v_fieldName2Vc)
END
ELSE IF @v_fieldName1Vc = 'WorkflowCreate' --Get Data for workflow
BEGIN
select  moduleName as FieldValue1,eventName  as FieldValue2,convert(varchar(400),activityId) as FieldValue3 from tbl_modulemaster m,tbl_eventmaster e,tbl_ActivityMaster a
where m.moduleId=e.moduleId and a.eventId=e.eventId and activityid=@v_fieldName2Vc
END
ELSE IF @v_fieldName1Vc = 'WorkflowEdit' --Get Data for workflow
BEGIN
select  distinct moduleName as FieldValue1,eventName  as FieldValue2,convert(varchar(10),noOfReviewer) as FieldValue3,convert(varchar(10),noOfDays) FieldValue4,isDonorConReq FieldValue5 from tbl_modulemaster m,tbl_eventmaster e,tbl_ActivityMaster a,tbl_WorkFlowEventConfig wc
where m.moduleId=e.moduleId and a.eventId=e.eventId and activityid=@v_fieldName2Vc
and wc.eventId=e.eventId and objectId=@v_fieldName3Vc
END
ELSE IF @v_fieldName1Vc = 'FileOnHandLevel' --Get file on hand information
BEGIN
SELECT  convert(varchar(400),[worfklowId]) AS FieldValue1,convert(varchar(400),activityId) AS FieldValue2,convert(varchar(400), [moduleId]) AS FieldValue3,[wfLevel] AS FieldValue4,'' AS FieldValue5,'' AS FieldValue6
FROM [dbo].[tbl_WorkFlowLevelConfig]
WHERE objectId=@v_fieldName1Vc AND childId=@v_fieldName2Vc AND fileOnHand='Yes' AND
activityId=@v_fieldName3Vc
END
ELSE IF @v_fieldName1Vc = 'WorkflowLevel' --Get Starts By/Ends By
BEGIN
SELECT  convert(varchar(400),[worfklowId]) AS FieldValue1,convert(varchar(400),activityId) AS FieldValue2,convert(varchar(400), [moduleId]) AS FieldValue3,[wfLevel] AS FieldValue4,'' AS FieldValue5,'' AS FieldValue6
FROM [dbo].[tbl_WorkFlowLevelConfig]
WHERE objectId=@v_fieldName1Vc AND childId=@v_fieldName2Vc AND fileOnHand='Yes' AND
activityId=@v_fieldName3Vc
END
ELSE IF @v_fieldName1Vc = 'UserOrganization' --Get  User Organization
BEGIN
select convert(varchar(400),dm.departmentId) FieldValue1,departmentName AS FieldValue2,'' AS FieldValue3,'' AS
 FieldValue4,'' AS FieldValue5,'' AS FieldValue6
 from tbl_DepartmentMaster dm where departmentType='Organization'
 /*from
 tbl_EmployeeMaster em,tbl_EmployeeOffices eo,
tbl_DesignationMaster dsm,tbl_DepartmentMaster dm
where eo.designationId=dsm.designationId and dsm.departmentid=dm.departmentId
and eo.employeeid=em.employeeid */

--and userId=@v_fieldName2Vc
END
ELSE IF @v_fieldName1Vc = 'AllProcurementRoles' --Get All Procurement Roles
BEGIN
select convert(varchar(400),procurementRoleId) FieldValue1,procurementRole AS FieldValue2,'' AS FieldValue3,'' AS
 FieldValue4,'' AS FieldValue5,'' AS FieldValue6 from tbl_ProcurementRole where
 procurementRole not in ('AA','DP','Authorized User','Development Partner','TOC/POC','TEC/PEC','AU','Account Officer','PMC')
END
ELSE IF @v_fieldName1Vc = 'WorkflowRuleEngine' --Get data for workflow rule engile
BEGIN
select  distinct   i.modulename FieldValue1 , i.eventName FieldValue2, convert(varchar(400),i.initiatorRole) FieldValue3,convert(varchar(400),a.approverRole) FieldValue4,isPubDateReq FieldValue5,'' FieldValue6 from (SELECT isPubDateReq, modulename,em.eventId,wfRoleId initiat ,eventName,wfinit.procurementRoleId initiatorRole
  FROM [dbo].[tbl_WorkflowRuleEngine] wfinit,
 [dbo].[tbl_ProcurementRole] pr, [dbo].[tbl_EventMaster] em,
  [dbo].[tbl_ModuleMaster] mm
  where em.moduleid=mm.moduleId and wfinit.procurementRoleId=pr.procurementRoleId and
  wfinit.eventid=em.eventid and wfRoleId=1 and em.eventId=@v_fieldName2Vc)i left outer join
  (SELECT  em.eventId,wfRoleId approver ,eventName,wfinit.procurementRoleId approverRole
  FROM [dbo].[tbl_WorkflowRuleEngine] wfinit,
  [dbo].[tbl_ProcurementRole] pr, [dbo].[tbl_EventMaster] em
  where wfinit.procurementRoleId=pr.procurementRoleId and
  wfinit.eventid=em.eventid and wfRoleId=2  and em.eventId=@v_fieldName2Vc)a on i.eventId=a.eventId
END
ELSE IF @v_fieldName1Vc = 'DpSearchUser' --Get all rows from table tbl_WorkFlowLevelConfig
BEGIN
	--IF (@v_fieldName2Vc IS  NULL or @v_fieldName2Vc ='') and (@v_fieldName3Vc IS NULL or @v_fieldName3Vc='')
	--	BEGIN
	--	select fullName FieldValue1,
	--	convert(varchar(400),lm.userId) FieldValue2,'' AS FieldValue3,'' AS FieldValue4,'' AS FieldValue5,'' AS FieldValue6 from tbl_LoginMaster lm,tbl_PartnerAdmin pm
	--		where lm.userId=pm.userId
	--	END
	--ELSE IF (@v_fieldName2Vc IS NULL or @v_fieldName2Vc='') and (@v_fieldName3Vc IS NOT NULL or @v_fieldName3Vc!='')
	--	BEGIN
		select fullName FieldValue1,
		convert(varchar(400),lm.userId) FieldValue2,'' AS FieldValue3,'' AS FieldValue4,'' AS
		FieldValue5,'' AS FieldValue6 from tbl_LoginMaster lm,tbl_PartnerAdmin pm,
		tbl_ScBankDevPartnerMaster scm
where lm.userId=pm.userId  and scm.sBankDevelopId=pm.sBankDevelopId
			and scm.sBankDevelopId=@v_fieldName3Vc and lm.status='approved'
			and isAdmin='No'
		--END
END
ELSE IF @v_fieldName1Vc = 'SearchUserByRole' --Get all rows from table tbl_WorkFlowLevelConfig
BEGIN
--dbo.tbl_DepartmentMaster.departmentName+'-'+
	If @v_fieldName2Vc = 0
	Begin
	SELECT   distinct (dbo.tbl_EmployeeMaster.employeeName+' , ['+dbo.tbl_DesignationMaster.designationName + ' at ' + dbo.tbl_OfficeMaster.officeName + ' ]' ) AS FieldValue1, 
                      convert(varchar(400),dbo.tbl_LoginMaster.userId) AS FieldValue2,'' AS FieldValue3,'' AS FieldValue4,'' AS FieldValue5,'' AS FieldValue6
			FROM      dbo.tbl_DepartmentMaster INNER JOIN
                      dbo.tbl_DesignationMaster ON dbo.tbl_DepartmentMaster.departmentId = dbo.tbl_DesignationMaster.departmentid AND 
                      dbo.tbl_DepartmentMaster.departmentId = dbo.tbl_DesignationMaster.departmentid 
                      INNER JOIN
                      dbo.tbl_EmployeeOffices ON dbo.tbl_DesignationMaster.designationId = dbo.tbl_EmployeeOffices.designationId AND 
                      dbo.tbl_DesignationMaster.designationId = dbo.tbl_EmployeeOffices.designationId INNER JOIN
                      dbo.tbl_EmployeeMaster ON dbo.tbl_EmployeeMaster.employeeId = dbo.tbl_EmployeeOffices.employeeId AND 
                      dbo.tbl_EmployeeMaster.employeeId = dbo.tbl_EmployeeOffices.employeeId 
                      INNER JOIN dbo.tbl_OfficeMaster ON dbo.tbl_EmployeeOffices.officeId = dbo.tbl_OfficeMaster.officeId
                      INNER JOIN 
                      dbo.tbl_LoginMaster ON dbo.tbl_EmployeeMaster.userId = dbo.tbl_LoginMaster.userId AND 
                      dbo.tbl_EmployeeMaster.userId = dbo.tbl_LoginMaster.userId  and dbo.tbl_LoginMaster.status='approved'
                     -- and dbo.tbl_EmployeeMaster.employeeId in(select employeeId from tbl_EmployeeRoles where procurementRoleId=@v_fieldName3Vc)
                     and dbo.tbl_EmployeeMaster.userid in(
                     select  userid from tbl_EmployeeRoles e,tbl_employeemaster em where e.employeeid=em.employeeid and procurementRoleId=@v_fieldName3Vc
                     union
                     select  userid from tbl_projectroles p  where procurementRoleId=@v_fieldName3Vc
                     
                     )
                      
	End
	Else
	Begin
	SELECT   distinct (dbo.tbl_EmployeeMaster.employeeName+' ,[' +dbo.tbl_DesignationMaster.designationName + ' at ' + dbo.tbl_OfficeMaster.officeName + ' ]') AS FieldValue1, 
                      convert(varchar(400),dbo.tbl_LoginMaster.userId) AS FieldValue2,'' AS FieldValue3,'' AS FieldValue4,'' AS FieldValue5,'' AS FieldValue6
			FROM      dbo.tbl_DepartmentMaster INNER JOIN
                      dbo.tbl_DesignationMaster ON dbo.tbl_DepartmentMaster.departmentId = dbo.tbl_DesignationMaster.departmentid AND 
                      dbo.tbl_DepartmentMaster.departmentId = dbo.tbl_DesignationMaster.departmentid 
                      INNER JOIN
                      dbo.tbl_EmployeeOffices ON dbo.tbl_DesignationMaster.designationId = dbo.tbl_EmployeeOffices.designationId AND 
                      dbo.tbl_DesignationMaster.designationId = dbo.tbl_EmployeeOffices.designationId INNER JOIN
                      dbo.tbl_EmployeeMaster ON dbo.tbl_EmployeeMaster.employeeId = dbo.tbl_EmployeeOffices.employeeId AND 
                      dbo.tbl_EmployeeMaster.employeeId = dbo.tbl_EmployeeOffices.employeeId INNER JOIN
                      dbo.tbl_OfficeMaster ON dbo.tbl_EmployeeOffices.officeId = dbo.tbl_OfficeMaster.officeId
                      INNER JOIN                       
                      dbo.tbl_LoginMaster ON dbo.tbl_EmployeeMaster.userId = dbo.tbl_LoginMaster.userId AND 
                      dbo.tbl_EmployeeMaster.userId = dbo.tbl_LoginMaster.userId  and dbo.tbl_LoginMaster.status='approved'
                      --and dbo.tbl_EmployeeMaster.employeeId in(select employeeId from tbl_EmployeeRoles where procurementRoleId=@v_fieldName3Vc)
                       -- and dbo.tbl_EmployeeMaster.employeeId in(select employeeId from tbl_EmployeeRoles where procurementRoleId=@v_fieldName3Vc)
                     and dbo.tbl_EmployeeMaster.userid in(
                     select  userid from tbl_EmployeeRoles e,tbl_employeemaster em where e.employeeid=em.employeeid and procurementRoleId=@v_fieldName3Vc
                     union
                     select  userid from tbl_projectroles p  where procurementRoleId=@v_fieldName3Vc
                     
                     )
                      and dbo.tbl_EmployeeOffices.officeId =@v_fieldName2Vc
    end            
			
END

IF @v_fieldName1Vc = 'GetAppMinistry'
BEGIN
declare
  @v_CurrDepartmentId_Int int,
@v_CurrDepartmentType_Vc varchar(15),
@v_CurrDepartmentName_Vc varchar(150),
@v_Ministry_Vc varchar(150),
@v_Division_Vc varchar(150),
@v_Agency_Vc varchar(150),
@v_DeptId_Int Int, @v_OffId_Int Int


SELECT @v_DeptId_Int = departmentId, @v_OffId_Int= officeId FROM [tbl_AppMaster] where appId = @v_fieldName2Vc

  Select @v_CurrDepartmentId_Int=departmentId ,@v_CurrDepartmentType_Vc=departmentType, @v_CurrDepartmentName_Vc=departmentName
					from tbl_DepartmentMaster where departmentId=@v_DeptId_Int

				If @v_CurrDepartmentType_Vc='Ministry'
				Begin
					Select @v_Ministry_Vc=@v_CurrDepartmentName_Vc, @v_Division_Vc='',@v_Agency_Vc=''
				End
				Else If @v_CurrDepartmentType_Vc='Division'
				Begin
					Select @v_Ministry_Vc=(select departmentName from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_CurrDepartmentId_Int)),
					@v_Division_Vc=@v_CurrDepartmentName_Vc,
					@v_Agency_Vc=''
				End
				Else -- Agency/Organisation Case
				Begin
					Select @v_Ministry_Vc=(select departmentName from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_CurrDepartmentId_Int))),
					@v_Division_Vc= (select departmentName from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_CurrDepartmentId_Int)),
					@v_Agency_Vc=@v_CurrDepartmentName_Vc
				End


				select @v_Ministry_Vc as FieldValue1, @v_Division_Vc as FieldValue2, @v_Agency_Vc as FieldValue3, officename+' - '+pecode FieldValue4, convert(varchar(20),officeId) AS FieldValue6 from tbl_OfficeMaster where officeId = @v_OffId_Int
END

IF @v_fieldName1Vc = 'getRandomCode'
BEGIN
	select randCode as FieldValue1, REPLACE(CONVERT(VARCHAR(11),generatedDt, 106), ' ', '-')  as FieldValue2
	From tbl_EmailVerificationCode
	Where userId= @v_fieldName2Vc
END

IF @v_fieldName1Vc = 'getAppEmailId'
BEGIN
set @v_FinalQueryVc='SELECT DISTINCT emailid as FieldValue1 , countryCode+mobileNo as MobileNo FROM tbl_loginmaster l ,tbl_TendererMaster, tbl_CountryMaster WHERE companyid in
			     (SELECT  companyId FROM tbl_CompanyMaster WHERE '+
			     @v_fieldName2Vc + ')
			     and l.userid=tbl_TendererMaster.userid and tbl_TendererMaster.country = tbl_CountryMaster.countryName
			UNION ALL

			SELECT DISTINCT emailid as FieldValue1, countryCode+mobileNo as MobileNo FROM tbl_loginmaster l,tbl_TendererMaster,
			 tbl_CountryMaster WHERE '+ @v_fieldName2Vc + '
			 and l.userid=tbl_TendererMaster.userid  and tbl_TendererMaster.country = tbl_CountryMaster.countryName'
			  print @v_FinalQueryVc
			exec(@v_FinalQueryVc)
End
IF @v_fieldName1Vc = 'AppPackageTenderNotCreated' --Get all rows from table tbl_EmployeeMaster,tbl_EmployeeOffices,tbl_EmployeeRoles and tbl_procurementRole
BEGIN

Select convert(varchar(400),[packageId]) AS FieldValue1,convert(varchar(400),[packageNo]) AS FieldValue2,convert(varchar(2000),[packagedesc]) AS FieldValue3,'' AS FieldValue4 ,'' AS FieldValue5,'' AS FieldValue6 FROM [dbo].[tbl_AppPackages] where appId=@v_fieldName2Vc
 	and appStatus='Approved'
 	and packageid not in(select packageid from tbl_tendermaster t,tbl_tenderdetails td where t.tenderid=td.tenderid and tenderstatus in ('Pending','Approved'))
END
END

IF @v_fieldName1Vc = 'getEmployeeByRoleAndOffice'
BEGIN
select convert(varchar(400),em.userId) AS FieldValue1 , em.employeeName AS FieldValue2
from tbl_EmployeeMaster em where em.employeeId in
(select eo.employeeId from tbl_EmployeeOffices eo where eo.officeId = @v_fieldName2Vc and eo.employeeId in
(select er.employeeId from tbl_EmployeeRoles er where er.procurementRoleId in
(select pr.procurementRoleId from tbl_ProcurementRole pr where pr.procurementRoleId <> 25 and pr.procurementRole like @v_fieldName3Vc)))
END



