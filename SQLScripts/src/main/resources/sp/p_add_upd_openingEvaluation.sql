USE [eGPBhutanLive23092018]
GO
/****** Object:  StoredProcedure [dbo].[p_add_upd_openingEvaluation]    Script Date: 04-Oct-18 4:04:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <11,12,2013>
-- Description:	<Added for operations on Opening and Evaluation Stage>
-- =============================================
ALTER PROCEDURE [dbo].[p_add_upd_openingEvaluation] 
	-- Add the parameters for the stored procedure here
	@v_fieldName1Vc varchar(500)=NULL,	
	@v_fieldName2Vc varchar(1000)=NULL,	
	@v_fieldName3Vc varchar(1000)=NULL,	
	@v_fieldName4Vc varchar(1000)=NULL,	
	@v_fieldName5Vc varchar(1000)=NULL,	
	@v_fieldName6Vc varchar(1000)=NULL,	
	@v_fieldName7Vc varchar(1000)=NULL,	
	@v_fieldName8Vc varchar(1000)=NULL,	
	@v_fieldName9Vc varchar(1000)=NULL,	
	@v_fieldName10Vc varchar(1000)=NULL,	
	@v_fieldName11Vc varchar(MAX)=NULL,	
	@v_fieldName12Vc varchar(1000)=NULL,	
	@v_fieldName13Vc varchar(1000)=NULL,	
	@v_fieldName14Vc varchar(1000)=NULL,	
	@v_fieldName15Vc varchar(1000)=NULL,	
	@v_fieldName16Vc varchar(1000)=NULL,	
	@v_fieldName17Vc varchar(1000)=NULL,	
	@v_fieldName18Vc varchar(1000)=NULL,	
	@v_fieldName19Vc varchar(1000)=NULL,	
	@v_fieldName20Vc varchar(1000)=NULL,
	@v_fieldName21Vc varchar(1000)=NULL,
	@v_fieldName22Vc varchar(1000)=NULL,
	@v_fieldName23Vc varchar(1000)=NULL,
	@v_fieldName24Vc varchar(1000)=NULL,
	@v_fieldName25Vc varchar(1000)=NULL,
	@v_fieldName26Vc varchar(1000)=NULL,
	@v_fieldName27Vc varchar(1000)=NULL,
	@v_fieldName28Vc varchar(1000)=NULL,
	@v_fieldName29Vc varchar(1000)=NULL,
	@v_fieldName30Vc varchar(1000)=NULL
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @v_Success_bit AS bit;
	DECLARE @IdentVal AS int = 0;
	Declare @evalCount int
IF @v_fieldName1Vc = 'DeleteBOQ'
BEGIN
	begin try
	delete from [tbl_BoQMaster]
	where boqid = cast(@v_fieldName2Vc as bigint)
    
    Set @v_Success_bit=1
	Select @v_Success_bit as flag, ' Deleted Successfully ' as Message , @IdentVal as Id		
    end try
   begin catch
	Set @v_Success_bit=0
	Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id   

   end catch
END	

--start by nafiul
IF @v_fieldName1Vc = 'SendApproveEvalReport'
BEGIN
	BEGIN TRY	
		if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
					SET @evalCount = 0
				else
					select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

		if (SELECT COUNT(evalRptTCId) FROM  tbl_EvalRptSentTC WHERE tenderId = @v_fieldName2Vc and pkgLotId=@v_fieldName3Vc and roundId=@v_fieldName4Vc and rptStatus = @v_fieldName8Vc and evalCount = @evalCount) =0
			BEGIN
				

				INSERT INTO [tbl_EvalRptSentTC] VALUES
				(@v_fieldName2Vc,@v_fieldName3Vc,@v_fieldName4Vc,@v_fieldName5Vc,@v_fieldName6Vc,GETDATE(),@v_fieldName7Vc,@v_fieldName8Vc, @evalCount)
				
				Set @v_Success_bit=1
				Select @v_Success_bit as flag, ' Inserted Successfully ' as Message , @IdentVal as Id			
			END
		ELSE
			BEGIN
				Set @v_Success_bit=1
				Select @v_Success_bit as flag, ' Already Inserted '  as Message, @IdentVal as Id
			END
	END TRY 
	
	BEGIN CATCH
		BEGIN
			Set @v_Success_bit=0
			Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id
		END
	END CATCH
    
END
--end by nafiul

IF @v_fieldName1Vc = 'EditBOQ'
BEGIN
	begin try
	update [tbl_BoQMaster]
	set category = @v_fieldName2Vc,
	[SubCategory]=@v_fieldName3Vc,
	[Code]=@v_fieldName4Vc,
	[Description]=@v_fieldName5Vc,
	[Unit]=@v_fieldName6Vc
	where boqid = cast(@v_fieldName7Vc as bigint)
    
    Set @v_Success_bit=1
	Select @v_Success_bit as flag, ' Updated Successfully ' as Message , @IdentVal as Id		
    end try
   begin catch
	Set @v_Success_bit=0
	Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id   

   end catch
END	
	
IF @v_fieldName1Vc = 'AddBOQ'
BEGIN
	begin try
	INSERT INTO [tbl_BoQMaster]
           ([Category]
           ,[SubCategory]
           ,[Code]
           ,[Description]
           ,[Unit])
     VALUES
           (@v_fieldName2Vc,@v_fieldName3Vc,@v_fieldName4Vc,@v_fieldName5Vc,@v_fieldName6Vc)
    
    Set @v_Success_bit=1
	Select @v_Success_bit as flag, ' Inserted Successfully ' as Message , @IdentVal as Id		
    end try
   begin catch
	Set @v_Success_bit=0
	Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id   

   end catch
END
	
IF @v_fieldName1Vc = 'InsertForSendingToPE'
BEGIN
	BEGIN TRAN
	BEGIN TRY	
		if (SELECT COUNT(tenderId) FROM  tbl_TOSListing WHERE tenderId = @v_fieldName2Vc) =0
			BEGIN
				
				INSERT INTO tbl_TOSListing 
				(tenderId,sentDate,comments,sentBy,sentTo,sentByGovUserId,sentToGovUserId)
				values (@v_fieldName2Vc,@v_fieldName3Vc,@v_fieldName4Vc,@v_fieldName5Vc,
				@v_fieldName6Vc,@v_fieldName7Vc,@v_fieldName8Vc)
				Select @IdentVal= ISNULL(MAX(ListingId),0) from tbl_TOSListing where tenderId = @v_fieldName2Vc;	
				COMMIT TRAN	
				Set @v_Success_bit=1
				Select @v_Success_bit as flag, ' Inserted Successfully ' as Message , @IdentVal as Id			
			END
		ELSE
			BEGIN
			Select @IdentVal= ISNULL(MAX(ListingId),0) from tbl_TOSListing where tenderId = @v_fieldName2Vc;
			Set @v_Success_bit=1
			Select @v_Success_bit as flag, ' Already Inserted '  as Message, @IdentVal as Id				
			ROLLBACK TRAN
		END
	END TRY 
	
	BEGIN CATCH
		BEGIN
			Set @v_Success_bit=0
			Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
			ROLLBACK TRAN
		END
	END CATCH
    
END

IF @v_fieldName1Vc = 'InsertPaymentAndPaymentStatus'
BEGIN
	BEGIN TRAN
		BEGIN TRY
			BEGIN	
				INSERT INTO tbl_TenderPayment 
					(paymentFor, paymentInstType, instRefNumber, amount, instDate, instValidUpto, bankName, branchName, comments,
					 tenderId, userId, createdBy, createdDate, pkgLotId, eSignature, status, paymentMode, extValidityRef, 
					 currency, issuanceBank, issuanceBranch, isVerified, isLive, dtOfAction, partTransId, OnlineTransId)
					values (@v_fieldName2Vc, @v_fieldName3Vc, @v_fieldName4Vc, @v_fieldName5Vc, @v_fieldName6Vc, @v_fieldName7Vc, 
					@v_fieldName8Vc, @v_fieldName9Vc, @v_fieldName10Vc, @v_fieldName11Vc, @v_fieldName12Vc, @v_fieldName13Vc, 
					@v_fieldName14Vc, @v_fieldName15Vc, '', @v_fieldName16Vc, @v_fieldName17Vc, @v_fieldName18Vc, @v_fieldName19Vc, 
					@v_fieldName20Vc, @v_fieldName21Vc, 'No', 'YES', @v_fieldName22Vc, @v_fieldName23Vc, NULL)

					Select @IdentVal= ISNULL(MAX(tenderPaymentId),0) from tbl_TenderPayment where tenderId = @v_fieldName11Vc and userId = @v_fieldName12Vc;	
				
				INSERT INTO tbl_TenderPaymentStatus 
					(tenderPaymentId, paymentStatus, paymentStatusDt, createdBy, comments, tenderPayRefId, refPartTransId)
					values (@IdentVal, @v_fieldName16Vc, @v_fieldName22Vc, @v_fieldName13Vc, @v_fieldName10Vc, 0, @v_fieldName23Vc)

					COMMIT TRAN	
					Set @v_Success_bit=1
					Select @v_Success_bit as flag, ' Inserted Successfully ' as Message , @IdentVal as Id	
			END		
		END TRY 
		BEGIN CATCH
			BEGIN
				Set @v_Success_bit=0
				Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
				ROLLBACK TRAN
			END
		END CATCH
END

IF @v_fieldName1Vc = 'UpdatePayment'
BEGIN
	BEGIN TRAN
		BEGIN TRY
			BEGIN	
				Update tbl_TenderPayment 
				SET isLive='no'
				WHERE tenderPaymentId = @v_fieldName2Vc

				COMMIT TRAN	
				Set @v_Success_bit=1
				Select @v_Success_bit as flag, ' Updated Successfully ' as Message , 1 as Id	
			END		
		END TRY 
		BEGIN CATCH
			BEGIN
				Set @v_Success_bit=0
				Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
				ROLLBACK TRAN
			END
		END CATCH
END

IF @v_fieldName1Vc = 'InsertPaymentVerificationAndUpdatePayment'
BEGIN
	BEGIN TRAN
		BEGIN TRY
			BEGIN	
				INSERT INTO tbl_TenderPaymentVerification (paymentId,verifiedBy,remarks,verificationDt,verifyPartTransId)
				VALUES (@v_fieldName2Vc,@v_fieldName3Vc,@v_fieldName4Vc,@v_fieldName5Vc,@v_fieldName6Vc)

				Select @IdentVal= ISNULL(MAX(verificationId),0) from tbl_TenderPaymentVerification where paymentId = @v_fieldName2Vc;	
				
				Update tbl_TenderPayment 
				SET isVerified='yes'
				WHERE tenderPaymentId = @v_fieldName2Vc

				COMMIT TRAN	
				Set @v_Success_bit=1
				Select @v_Success_bit as flag, 'Inserted and Updated Successfully' as Message , @IdentVal as Id	
			END		
		END TRY 
		BEGIN CATCH
			BEGIN
				Set @v_Success_bit=0
				Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
				ROLLBACK TRAN
			END
		END CATCH
END

IF @v_fieldName1Vc = 'SeekClarification'
BEGIN
	BEGIN TRAN
	BEGIN TRY	
			BEGIN
					if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
						SET @evalCount = 0
					else
						select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc


					INSERT INTO tbl_EvalFormQues 
					(tenderId,pkgLotId,tenderFormId,userId,quePostedBy,quePostDt,answer,queSentByTec,evalStatus,question,postedByGovUserId,evalCount)
					values (@v_fieldName2Vc,@v_fieldName3Vc,@v_fieldName4Vc,@v_fieldName5Vc,@v_fieldName6Vc,@v_fieldName7Vc,@v_fieldName8Vc,@v_fieldName9Vc,@v_fieldName10Vc,@v_fieldName11Vc,@v_fieldName12Vc,@evalCount)
					--Select @IdentVal= ISNULL(MAX(evalBidderRspId),0) from tbl_EvalBidderResp where tenderId = @v_fieldName2Vc;	
					COMMIT TRAN	
				Set @v_Success_bit=1
				Select @v_Success_bit as flag, ' Inserted Successfully ' as Message , @IdentVal as Id			
			END
	END TRY 
	
	BEGIN CATCH
		BEGIN
			Set @v_Success_bit=0
			Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
			ROLLBACK TRAN
		END
	END CATCH
END



IF @v_fieldName1Vc = 'NotifyChairpersonAfterPostedQuestion'
BEGIN
	BEGIN TRAN
	BEGIN TRY	
		if (SELECT COUNT(tenderId) FROM  tbl_EvalSentQueToCp WHERE tenderId = @v_fieldName2Vc and sentFor=@v_fieldName7Vc and sentBy=@v_fieldName3Vc) =0
			BEGIN
				
				INSERT INTO tbl_EvalSentQueToCp 
				(tenderId,sentBy,sentDt,evalStatus,remarks,sentFor)
				values (@v_fieldName2Vc,@v_fieldName3Vc,@v_fieldName4Vc,@v_fieldName5Vc,@v_fieldName6Vc,@v_fieldName7Vc)
				--Select @IdentVal= ISNULL(MAX(evalBidderRspId),0) from tbl_EvalBidderResp where tenderId = @v_fieldName2Vc;	
				COMMIT TRAN	
				Set @v_Success_bit=1
				Select @v_Success_bit as flag, ' Inserted Successfully ' as Message , @IdentVal as Id			
			END
		ELSE
			BEGIN
			--Select @IdentVal= ISNULL(MAX(evalBidderRspId),0) from tbl_EvalBidderResp where tenderId = @v_fieldName2Vc;
			Set @v_Success_bit=1
			Select @v_Success_bit as flag, ' Already Inserted '  as Message, @IdentVal as Id				
			ROLLBACK TRAN
		END
	END TRY 
	
	BEGIN CATCH
		BEGIN
			Set @v_Success_bit=0
			Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
			ROLLBACK TRAN
		END
	END CATCH
END






IF @v_fieldName1Vc = 'PostQuestionsToTenderer'
BEGIN
	BEGIN TRAN
	BEGIN TRY	
		if (SELECT COUNT(tenderId) FROM  tbl_EvalBidderResp WHERE tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc ) =0
			BEGIN
				
				INSERT INTO tbl_EvalBidderResp 
				(tenderId,userId,sentBy,expectedComplDt,evalstatus,isClarificationComp,sentDate,remarks)
				values (@v_fieldName2Vc,@v_fieldName3Vc,@v_fieldName4Vc,@v_fieldName5Vc,@v_fieldName6Vc,@v_fieldName7Vc,@v_fieldName8Vc,@v_fieldName9Vc)
				--Select @IdentVal= ISNULL(MAX(evalBidderRspId),0) from tbl_EvalBidderResp where tenderId = @v_fieldName2Vc;	
				COMMIT TRAN	
				Set @v_Success_bit=1
				Select @v_Success_bit as flag, ' Inserted Successfully ' as Message , @IdentVal as Id			
			END
		ELSE
			BEGIN
			--Select @IdentVal= ISNULL(MAX(evalBidderRspId),0) from tbl_EvalBidderResp where tenderId = @v_fieldName2Vc;
			Set @v_Success_bit=1
			Select @v_Success_bit as flag, ' Already Inserted '  as Message, @IdentVal as Id				
			ROLLBACK TRAN
		END
	END TRY 
	
	BEGIN CATCH
		BEGIN
			Set @v_Success_bit=0
			Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
			ROLLBACK TRAN
		END
	END CATCH
END


IF @v_fieldName1Vc = 'EvaluateForm'
BEGIN
	BEGIN TRAN
	BEGIN TRY	
		if (SELECT COUNT(tenderId) FROM  tbl_EvalMemStatus WHERE tenderId = @v_fieldName2Vc and userId=@v_fieldName3Vc and formId=@v_fieldName4Vc and evalBy= @v_fieldName8Vc) =0
			BEGIN
				
				INSERT INTO tbl_EvalMemStatus 
				(tenderId,userId,formId,isComplied,evalStatus,evalStatusDt,evalBy,evalNonCompRemarks,actualMarks)
				values (@v_fieldName2Vc,@v_fieldName3Vc,@v_fieldName4Vc,@v_fieldName5Vc,@v_fieldName6Vc,@v_fieldName7Vc,@v_fieldName8Vc,@v_fieldName9Vc,@v_fieldName10Vc)
				--Select @IdentVal= ISNULL(MAX(evalBidderRspId),0) from tbl_EvalBidderResp where tenderId = @v_fieldName2Vc;	
				COMMIT TRAN	
				Set @v_Success_bit=1
				Select @v_Success_bit as flag, ' Inserted Successfully ' as Message , @IdentVal as Id			
			END
		ELSE
			BEGIN
			--Select @IdentVal= ISNULL(MAX(evalBidderRspId),0) from tbl_EvalBidderResp where tenderId = @v_fieldName2Vc;
			Set @v_Success_bit=1
			Select @v_Success_bit as flag, ' Already Inserted '  as Message, @IdentVal as Id				
			ROLLBACK TRAN
		END
	END TRY 
	
	BEGIN CATCH
		BEGIN
			Set @v_Success_bit=0
			Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
			ROLLBACK TRAN
		END
	END CATCH
END

IF @v_fieldName1Vc = 'NotifyChairpersonAfterEvaluation'
BEGIN
	BEGIN TRAN
	BEGIN TRY	
		if (SELECT COUNT(tenderId) FROM  tbl_EvalSentQueToCp WHERE tenderId = @v_fieldName2Vc and sentFor = @v_fieldName7Vc and sentBy=@v_fieldName3Vc) =0
			BEGIN
				
				INSERT INTO tbl_EvalSentQueToCp 
				(tenderId,sentBy,sentDt,evalStatus,remarks,sentFor)
				values (@v_fieldName2Vc,@v_fieldName3Vc,@v_fieldName4Vc,@v_fieldName5Vc,@v_fieldName6Vc,@v_fieldName7Vc)
				--Select @IdentVal= ISNULL(MAX(evalBidderRspId),0) from tbl_EvalBidderResp where tenderId = @v_fieldName2Vc;	
				COMMIT TRAN	
				Set @v_Success_bit=1
				Select @v_Success_bit as flag, ' Inserted Successfully ' as Message , @IdentVal as Id			
			END
		ELSE
			BEGIN
			--Select @IdentVal= ISNULL(MAX(evalBidderRspId),0) from tbl_EvalBidderResp where tenderId = @v_fieldName2Vc;
			Set @v_Success_bit=1
			Select @v_Success_bit as flag, ' Already Inserted '  as Message, @IdentVal as Id				
			ROLLBACK TRAN
		END
	END TRY 
	
	BEGIN CATCH
		BEGIN
			Set @v_Success_bit=0
			Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
			ROLLBACK TRAN
		END
	END CATCH
END

IF @v_fieldName1Vc = 'InsertEvalConfig'
BEGIN
	BEGIN TRAN
	BEGIN TRY	
	if(@v_fieldName3Vc = 'team' or @v_fieldName3Vc = 'ind')
		BEGIN
			if (SELECT COUNT(tenderId) FROM  tbl_EvalConfig WHERE tenderId = @v_fieldName2Vc) =0
				BEGIN
					INSERT INTO tbl_EvalConfig 
					(tenderId,configType,evalCommittee,configBy,configDate,isPostQualReq,tecMemberId,tscMemberId,isTscReq,evalStatus,tecGovUserId)
					values (@v_fieldName2Vc,@v_fieldName3Vc,@v_fieldName4Vc,@v_fieldName5Vc,@v_fieldName6Vc,@v_fieldName7Vc,@v_fieldName8Vc,@v_fieldName9Vc,@v_fieldName10Vc,@v_fieldName11Vc,@v_fieldName12Vc)
					Select @IdentVal= ISNULL(MAX(evalconfigid),0) from tbl_EvalConfig where tenderId = @v_fieldName2Vc;	

					INSERT INTO tbl_EvalNomination 
						(evalConfigId,tenderId,envelopeId,nomineeUserId,nominationDate,nomineeAction,evaltype,
					nomineeStatus,remarks,isCurrent,nomineeActionDt)
					select distinct @IdentVal as evalConfigId ,@v_fieldName2Vc as tenderId, @v_fieldName9Vc as envelopeId, 
					 cm.userId as nomineeUserId , getdate() as  nominationDate , 
					@v_fieldName13Vc as nomineeAction , @v_fieldName3Vc as evaltype,
					@v_fieldName14Vc as nomineeStatus , replace(@v_fieldName16Vc,'''','''''') as  remarks,
					@v_fieldName15Vc as isCurrent , getdate() as nomineeActionDt
					from tbl_CommitteeMembers as cm
					inner join tbl_Committee as c on cm.committeeId=c.committeeId
					where tenderId=@v_fieldName2Vc and cm.memberRole<>'cp' and c.committeeType in ('TEC','PEC')

					COMMIT TRAN	
					Set @v_Success_bit=1
					Select @v_Success_bit as flag, ' Inserted Successfully ' as Message , @IdentVal as Id	

				END
			ELSE
				BEGIN
				--Select @IdentVal= ISNULL(MAX(evalBidderRspId),0) from tbl_EvalBidderResp where tenderId = @v_fieldName2Vc;
				Set @v_Success_bit=1
				Select @v_Success_bit as flag, ' Already Inserted '  as Message, @IdentVal as Id				
				ROLLBACK TRAN
				END
		END
	END TRY 
	
	BEGIN CATCH
		BEGIN
			Set @v_Success_bit=0
			Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
			ROLLBACK TRAN
		END
	END CATCH
END
--Edit by Palash for TER4 and reTenderRecommendetion value in table name tbl_PostQualification
IF @v_fieldName1Vc = 'SignTER4'
BEGIN
	BEGIN TRAN
	BEGIN TRY	
		BEGIN
			
				BEGIN
					INSERT INTO tbl_TORRptSign 
					(reportType,tenderId,pkgLotId,userId,comments,signedDate,govUserId,aggDisAggStatus,roundId,evalCount)
					values (@v_fieldName2Vc,@v_fieldName3Vc,@v_fieldName4Vc,@v_fieldName5Vc,@v_fieldName6Vc,@v_fieldName7Vc,@v_fieldName8Vc,@v_fieldName9Vc,@v_fieldName10Vc,@v_fieldName12Vc)
					
						if(@v_fieldName2Vc = 'ter4' and @v_fieldName5Vc = ( select userId from tbl_CommitteeMembers where committeeId in (select committeeId from tbl_Committee where tenderId = @v_fieldName3Vc and committeeType in ('TEC','PEC') and memberRole = 'cp')))
						BEGIN
							update tbl_PostQualification set reTenderRecommendetion = @v_fieldName11Vc where tenderId = @v_fieldName3Vc and evalCount = @v_fieldName12Vc and postQualStatus = 'Qualify'
						END
						
					COMMIT TRAN	
					Set @v_Success_bit=1
					Select @v_Success_bit as flag, ' Inserted Successfully ' as Message , @IdentVal as Id			
				END
		END
	END TRY 
	
	BEGIN CATCH
		BEGIN
			Set @v_Success_bit=0
			Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
			ROLLBACK TRAN
		END
	END CATCH
END

--Start for INSERT INTO EVALUATION NOMINATION TABLE

IF @v_fieldName1Vc = 'UpdateEvalNomination'
BEGIN
	
	BEGIN TRY	
	BEGIN TRAN
		UPDATE tbl_EvalNomination
		SET nomineeAction = @v_fieldName2Vc, nomineeActionDt = @v_fieldName3Vc, remarks = @v_fieldName4Vc
		WHERE evalConfigId = @v_fieldName5Vc and nomineeUserId = @v_fieldName6Vc and nomineeStatus = @v_fieldName7Vc and isCurrent = @v_fieldName8Vc ;
	COMMIT TRAN
		Set @v_Success_bit=1
		Select @v_Success_bit as flag, ' Updated Successfully ' as Message , @IdentVal as Id	 
	END TRY
	
	
	BEGIN CATCH
		BEGIN
			Set @v_Success_bit=0
			Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
			ROLLBACK TRAN
		END
	END CATCH
END

--end for INSERT INTO EVALUATION NOMINATION TABLE 


--Update of EvalConfig and EvalNomination Table 
IF @v_fieldName1Vc = 'UpdateEvalConfigInsertEvalNomination'
BEGIN
	
	BEGIN TRY	
	BEGIN TRAN
		UPDATE tbl_EvalConfig
		SET configType = @v_fieldName3Vc, tecMemberId = @v_fieldName4Vc
		WHERE tenderId = @v_fieldName2Vc;
		
		Select @IdentVal= ISNULL(MAX(evalconfigid),0) from tbl_EvalConfig where tenderId = @v_fieldName2Vc;	
		
		Update tbl_EvalNomination
		SET isCurrent = @v_fieldName5Vc
		WHERE tenderId = @v_fieldName2Vc;
		
		INSERT INTO tbl_EvalNomination 
		(evalConfigId,tenderId,envelopeId,nomineeUserId,nominationDate,nomineeAction,evaltype,
		  nomineeStatus,remarks,isCurrent,nomineeActionDt)
		select distinct @IdentVal as evalConfigId ,@v_fieldName2Vc as tenderId, @v_fieldName6Vc as envelopeId, 
		cm.userId as nomineeUserId , getdate() as  nominationDate , 
		@v_fieldName8Vc as nomineeAction , @v_fieldName9Vc as evaltype,
		@v_fieldName10Vc as nomineeStatus , replace(@v_fieldName16Vc,'''','''''') as  remarks,
		@v_fieldName12Vc as isCurrent , getdate() as nomineeActionDt
		from tbl_CommitteeMembers as cm
		inner join tbl_Committee as c on cm.committeeId=c.committeeId
		where tenderId=@v_fieldName2Vc and cm.memberRole<>'cp' and c.committeeType in ('TEC','PEC')
	
		
	COMMIT TRAN
		Set @v_Success_bit=1
		Select @v_Success_bit as flag, ' Updated Successfully ' as Message , @IdentVal as Id	 
	END TRY
	
	
	BEGIN CATCH
		BEGIN
			Set @v_Success_bit=0
			Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
			ROLLBACK TRAN
		END
	END CATCH
END

--for re-evaluation
IF @v_fieldName1Vc = 'FinalizeResponsiveness'
BEGIN
	BEGIN TRAN
	BEGIN TRY	
			
			if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
						SET @evalCount = 0
			else
						select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc
			
			if (SELECT COUNT(tenderId) FROM  tbl_EvalBidderStatus WHERE tenderId = @v_fieldName2Vc and userId=@v_fieldName3Vc and pkgLotId = 0 and evalCount = @evalCount) =0
				BEGIN						
					INSERT INTO tbl_EvalBidderStatus 
					(tenderId,userId,bidderStatus,bidderMarks,passingMarks,remarks,result,evalStatus,pkgLotId,evalBy,evalDt,evalCount)
					values (@v_fieldName2Vc,@v_fieldName3Vc,@v_fieldName4Vc,@v_fieldName5Vc,@v_fieldName6Vc,@v_fieldName7Vc,@v_fieldName8Vc,@v_fieldName9Vc,@v_fieldName10Vc,@v_fieldName11Vc,@v_fieldName12Vc,@evalCount)
					COMMIT TRAN	
					Set @v_Success_bit=1
					Select @v_Success_bit as flag, ' Inserted Successfully ' as Message , @IdentVal as Id			
				END
			ELSE
				BEGIN
				UPDATE tbl_EvalBidderStatus SET bidderStatus = @v_fieldName4Vc
				WHERE tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc and evalCount = @evalCount
				COMMIT TRAN
				Set @v_Success_bit=1
				Select @v_Success_bit as flag, ' Updated Successfully '  as Message, @IdentVal as Id
								
				END
		
	END TRY 
	
	BEGIN CATCH
		BEGIN
			Set @v_Success_bit=0
			Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
			ROLLBACK TRAN
		END
	END CATCH
END

--for re-evaluation
IF @v_fieldName1Vc = 'PostQualification'
BEGIN
	BEGIN TRAN
	BEGIN TRY	
					if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName3Vc)=0
						SET @evalCount = 0
					else
						select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName3Vc
	
			if (SELECT COUNT(tenderId) FROM  tbl_PostQualification WHERE tenderId = @v_fieldName3Vc and userId=@v_fieldName2Vc and evalCount = @evalCount and pkgLotId = @v_fieldName6Vc) =0
				BEGIN
					INSERT INTO tbl_PostQualification 
					(userId,tenderId,siteVisit,siteVisitReqDt,pkgLotId,comments,entryDate,createdBy,postQualStatus,siteVisitDate,siteVisitStatus,noaStatus,siteVisitComments,rank,evalCount)
					values (@v_fieldName2Vc,@v_fieldName3Vc,@v_fieldName4Vc,@v_fieldName5Vc,@v_fieldName6Vc,@v_fieldName7Vc,@v_fieldName8Vc,@v_fieldName9Vc,@v_fieldName10Vc,@v_fieldName11Vc,@v_fieldName12Vc,@v_fieldName13Vc,@v_fieldName14Vc,@v_fieldName15Vc,@evalCount)
					COMMIT TRAN	
					Set @v_Success_bit=1
					Select @v_Success_bit as flag, ' Inserted Successfully ' as Message , @IdentVal as Id			
				END
			ELSE
				BEGIN
					Set @v_Success_bit=1
					Select @v_Success_bit as flag, ' Already Inserted '  as Message, @IdentVal as Id				
					ROLLBACK TRAN						
				END
		
	END TRY 
	
	BEGIN CATCH
		BEGIN
			Set @v_Success_bit=0
			Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
			ROLLBACK TRAN
		END
	END CATCH
END

--for re-evaluation
IF @v_fieldName1Vc = 'ReevaluationByHOPE'
BEGIN
	BEGIN TRAN
	BEGIN TRY	
			select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc
			update tbl_EvalRptSentToAA set rptStatus = @v_fieldName3Vc, aaRemarks = @v_fieldName4Vc, rptApproveDt = GETDATE(), evalCount = @evalCount+1
			where evalRptToAAId = @v_fieldName5Vc
			
			delete from tbl_EvalCpMemClarification where evalMemStatusId in (select evalMemStatusId from tbl_EvalMemStatus where tenderId = @v_fieldName2Vc)
			delete from tbl_EvalMemStatus where tenderId = @v_fieldName2Vc
			delete from tbl_EvalSentQueToCp where tenderId = @v_fieldName2Vc	
			delete from tbl_EvalBidderResp where tenderId = @v_fieldName2Vc
	COMMIT TRAN	
			Set @v_Success_bit=1
			Select @v_Success_bit as flag, ' Updated Successfully ' as Message , @IdentVal as Id			
	END TRY 
	
	BEGIN CATCH
		BEGIN
			Set @v_Success_bit=0
			Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
			ROLLBACK TRAN
		END
	END CATCH
END
--added by ahsan
IF @v_fieldName1Vc = 'AutoInsertEvalConfigNomination'
BEGIN
	Declare @memberId int
	Declare @memberName varchar(100)
	Declare @memberGovId int
	Declare @eventType varchar(10)

	BEGIN TRAN
	BEGIN TRY	
		select @memberId = cm.userId,@memberName = et.employeeName,@memberGovId = et.govUserId from tbl_CommitteeMembers cm inner join tbl_EmployeeTrasfer et
		on cm.userId = et.userId
		where cm.memberRole <> 'cp' and et.isCurrent = 'Yes'
		and cm.committeeId = (select committeeId from tbl_Committee where tenderId = @v_fieldName2Vc and committeeType in ('TEC','PEC'))
		
		UPDATE tbl_CommitteeMembers SET appStatus = 'Approved', appDate = GETDATE() WHERE committeeId = (select committeeId from tbl_Committee where tenderId = @v_fieldName2Vc and committeeType in ('TEC','PEC'))

		INSERT INTO tbl_EvalConfig 
		(tenderId,configType,evalCommittee,configBy,configDate,isPostQualReq,tecMemberId,tscMemberId,isTscReq,evalStatus,tecGovUserId)
		values
		(@v_fieldName2Vc,'team',@memberName,@v_fieldName3Vc,GETDATE(),'yes',@memberId,0,'no','Evaluation',@memberGovId)
		
		Select @IdentVal= ISNULL(MAX(evalconfigid),0) from tbl_EvalConfig where tenderId = @v_fieldName2Vc;	
		INSERT INTO tbl_EvalNomination 
		(evalConfigId,tenderId,envelopeId,nomineeUserId,nominationDate,nomineeAction,evaltype,
		  nomineeStatus,remarks,isCurrent,nomineeActionDt)
		 values (@IdentVal,@v_fieldName2Vc,0,@memberId,GETDATE(),'Agreed','team','Live','','Yes',GETDATE())
		
		select @eventType= eventtype from tbl_TenderDetails where tenderId = @v_fieldName2Vc
		if(@eventType <> 'REOI')
		Begin
			INSERT INTO tbl_EvalBidderStatus 
			(tenderId,userId,bidderStatus,evalBy,evalDt,bidderMarks,evalStatus,passingMarks,remarks,result,pkgLotId,evalCount)
			select @v_fieldName2Vc,userId ,'Technically Responsive',@v_fieldName3Vc,GETDATE(),'','evaluation','','','',0,0
			from tbl_FinalSubmission where tenderId = @v_fieldName2Vc and bidSubStatus = 'finalsubmission'
		End
		
		--values (@v_fieldName2Vc,  ,'Technically Responsive',@v_fieldName3Vc,GETDATE(),'','evaluation','','','',0,0)
			
					
					
	COMMIT TRAN	
			Set @v_Success_bit=1
			Select @v_Success_bit as flag, ' Insert Successfull ' as Message , @IdentVal as Id			
	END TRY 
	
	BEGIN CATCH
		BEGIN
			Set @v_Success_bit=0
			Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
			ROLLBACK TRAN
		END
	END CATCH
END
--added by ahsan
IF @v_fieldName1Vc = 'SendEvalReportToTC'
BEGIN
	
	BEGIN TRAN
	BEGIN TRY	
			insert into tbl_EvalRptSentToAA values 
			(0,@v_fieldName2Vc,@v_fieldName3Vc,@v_fieldName4Vc,@v_fieldName5Vc,GETDATE(),@v_fieldName6Vc,@v_fieldName7Vc,@v_fieldName8Vc,@v_fieldName9Vc,
			@v_fieldName10Vc,@v_fieldName11Vc,@v_fieldName12Vc,@v_fieldName13Vc,@v_fieldName14Vc,@v_fieldName15Vc)
			--insert into tbl_EvalRptSentTc(tenderId,pkgLotId,roundId,sentByUserId,sentToUserId,sentDate,remarks,rptStatus) 
			--values (@v_fieldName2Vc,@v_fieldName3Vc,@v_fieldName4Vc,@v_fieldName5Vc,@v_fieldName6Vc,GETDATE(),@v_fieldName7Vc,@v_fieldName8Vc)
	COMMIT TRAN	
			Set @v_Success_bit=1
			Select @v_Success_bit as flag, ' Insert or Update Successfull ' as Message , @IdentVal as Id			
	END TRY 
	
	BEGIN CATCH
		BEGIN
			Set @v_Success_bit=0
			Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
			ROLLBACK TRAN
		END
	END CATCH
END
IF @v_fieldName1Vc = 'SendEvalReportToPA'
BEGIN
	
	BEGIN TRAN
	BEGIN TRY
	
			if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
					SET @evalCount = 0
				else
					select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc
						
			if (SELECT COUNT(evalRptTCId) FROM  tbl_EvalRptSentTC WHERE tenderId = @v_fieldName2Vc and pkgLotId=@v_fieldName3Vc and roundId=@v_fieldName4Vc and rptStatus = @v_fieldName8Vc and evalCount = @evalCount) =0
			BEGIN
				insert into tbl_EvalRptSentTc(tenderId,pkgLotId,roundId,sentByUserId,sentToUserId,sentDate,remarks,rptStatus,evalCount) 
				values (@v_fieldName2Vc,@v_fieldName3Vc,@v_fieldName4Vc,@v_fieldName5Vc,@v_fieldName6Vc,GETDATE(),@v_fieldName7Vc,@v_fieldName8Vc,@evalCount)
			END
	COMMIT TRAN	
			Set @v_Success_bit=1
			Select @v_Success_bit as flag, ' Insert or Update Successfull ' as Message , @IdentVal as Id			
	END TRY 
	
	BEGIN CATCH
		BEGIN
			Set @v_Success_bit=0
			Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
			ROLLBACK TRAN
		END
	END CATCH
END
IF @v_fieldName1Vc = 'ApproveEvalReportByTC'
BEGIN
	
	BEGIN TRAN
	BEGIN TRY	
			
			update tbl_EvalRptSentToAA set rptStatus = @v_fieldName5Vc, aaRemarks = @v_fieldName6Vc,rptApproveDt=GETDATE()
			where tenderId = @v_fieldName2Vc and roundId = @v_fieldName3Vc and pkgLotId = @v_fieldName4Vc

			COMMIT TRAN	
			Set @v_Success_bit=1
			Select @v_Success_bit as flag, ' Update Successfull ' as Message , @IdentVal as Id			
	END TRY 
	
	BEGIN CATCH
		BEGIN
			Set @v_Success_bit=0
			Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
			ROLLBACK TRAN
		END
	END CATCH
END

IF @v_fieldName1Vc = 'NOAAcceptanceAndContractSign'
BEGIN
	
	BEGIN TRAN
	BEGIN TRY	
			
			update tbl_PostQualification set noaStatus = @v_fieldName7Vc
				where tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc and pkgLotId = @v_fieldName4Vc

			update tbl_NoaAcceptance set comments = @v_fieldName6Vc, [acceptRejDt] = GETDATE()
				,[acceptRejStatus] = @v_fieldName8Vc,[accountTitle] = @v_fieldName9Vc
				,[bankName] = @v_fieldName10Vc,[branchName] = @v_fieldName11Vc
				,[accountNo] = @v_fieldName12Vc,[address] = @v_fieldName13Vc
				,[telephone] = @v_fieldName14Vc,[fax] = @v_fieldName15Vc
				,[email] = @v_fieldName16Vc,[swiftCode] = @v_fieldName17Vc
				where noaIssueId = @v_fieldName5Vc

			IF @v_fieldName7Vc = 'accepted'
			BEGIN
			--print(@v_fieldName7Vc)
				/*INSERT INTO [dbo].[tbl_TenderPayment]
					([paymentFor],[paymentInstType],[instRefNumber],[paymentMode]
					,[amount],[instDate],[instValidUpto]
					,[bankName],[branchName],[comments]
					,[tenderId],[userId],[createdBy]
					,[createdDate],[pkgLotId],[eSignature]
					,[status],[isVerified])
					VALUES ('Performance Security', 'Cash Warrant', 'refno1', 'bank'
					,0.00, GETDATE(), DATEADD(day,30,GETDATE())
					,'Bank of Bhutan', 'Main Branch', 'paid'
					,@v_fieldName2Vc, @v_fieldName3Vc, 92
					,GETDATE(), @v_fieldName4Vc, ''
					,'paid', 'yes')*/

				declare @v_TenderPaymentId_Int int, @v_PAUser_Id int--, @v_NOA_Id int
				DECLARE @v_Contract_Id int
				-- Set @v_TenderPaymentId_Int=IDENT_CURRENT('dbo.tbl_TenderPayment')

				/*INSERT INTO [dbo].[tbl_PaymentVerification]
					([paymentId],[verifiedBy]
					,[remarks],[verificationDt])
					VALUES
					(@v_TenderPaymentId_Int, 97, 'verified', GETDATE())*/

				select @v_PAUser_Id=createdBy from tbl_TenderMaster where tenderId=@v_fieldName2Vc

				/*select @v_NOA_Id=nd.noaIssueId from tbl_NoaAcceptance na
					inner join tbl_NoaIssueDetails nd
					on na.noaIssueId=nd.noaIssueId
					and na.userId=nd.userId
					where na.userId=@v_fieldName3Vc and nd.tenderId=@v_fieldName2Vc
					and nd.pkgLotId=@v_fieldName4Vc*/

					if exists (select * from tbl_ContractNOARelation cs
					inner join tbl_NoaIssueDetails ni on ni.noaissueid = cs.noaId
					where ni.userId=@v_fieldName3Vc and ni.tenderId=@v_fieldName2Vc)
						BEGIN
							SELECT @v_Contract_Id=ContractID FROM tbl_ContractNOARelation 
								WHERE noaId = (select top 1 (ni.noaIssueId) from tbl_NoaIssueDetails ni
								inner join tbl_NoaAcceptance na on na.noaIssueId=ni.noaIssueId
								where na.acceptRejStatus='approved' and tenderId=@v_fieldName2Vc and ni.userId=@v_fieldName3Vc)  
							print 'noa id'
							print @v_fieldName5Vc
							print 'contract id'
							print (@v_Contract_Id)

							INSERT INTO [dbo].[tbl_ContractNOARelation]
							([ContractID],[NOAID],[UserID],[TenderID])
							VALUES (@v_Contract_Id,@v_fieldName5Vc, @v_fieldName3Vc,@v_fieldName2Vc) 
						END
					ELSE
						BEGIN
						INSERT INTO [dbo].[tbl_ContractSign]
								([lastDtContractDt],[contractSignDt]
								,[isPubAggOnWeb],[paymentTerms],[isRepeatOrder]
								,[contractSignLocation]
								,[createdDt],[createdBy],[witnessInfo],[noaId])
								VALUES
								(DATEADD(day, 7, GETDATE()), GETDATE(),
								'yes', 'anyitemanyp','no',
								'thimphu'
								,GETDATE(), @v_PAUser_Id, 'Witness from PA@Witness from Bidder',0) 

								Set @v_Contract_Id=IDENT_CURRENT('dbo.tbl_ContractSign')

								INSERT INTO [dbo].[tbl_ContractNOARelation]
								([ContractID],[NOAID],[UserID],[TenderID])
								VALUES (@v_Contract_Id,@v_fieldName5Vc, @v_fieldName3Vc,@v_fieldName2Vc)
						END

				


			END

			COMMIT TRAN	
			Set @v_Success_bit=1
			Select @v_Success_bit as flag, ' Update Successfull ' as Message , @IdentVal as Id			
	END TRY 
	
	BEGIN CATCH
		BEGIN
			Set @v_Success_bit=0
			Select @v_Success_bit as flag, ' ' + Error_Message() as Message, 0 as Id				
			ROLLBACK TRAN
		END
	END CATCH
END
END




