USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_Dynamic_Validation]    Script Date: 4/24/2016 10:45:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Search In AppData
--
--
-- Author: Rajesh Singh
-- Date: 3-11-2010
--
-- Last Modified:
-- Modified By:
-- Date 3-1-2010
-- Modification:
--------------------------------------------------------------------------------

--[p_Dynamic_Validation] 'PQ'
ALTER PROCEDURE [dbo].[p_Dynamic_Validation]
	 @v_MainField_inVc Varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	DECLARE @v_fileID_inInt int,@RulesQuery VARCHAR(max),@Messages VARCHAR(max)
	SET @RulesQuery=''
	SET @Messages=''
	
	IF @v_MainField_inVc='PQ'
		BEGIN
		
		DECLARE fileCursor CURSOR FOR
		SELECT id from tbl_tempfieldname Where PQ='Yes'
		OPEN fileCursor
		FETCH NEXT FROM fileCursor INTO @v_fileID_inInt

		WHILE @@FETCH_STATUS = 0
		BEGIN
		
		IF @RulesQuery =''
		BEGIN
			set @RulesQuery=(select Rules from tbl_tempfieldmessage where fieldid=@v_fileID_inInt)
		END
		ELSE
		BEGIN
			set @RulesQuery=@RulesQuery+','+(select Rules from tbl_tempfieldmessage where fieldid=@v_fileID_inInt)
		END
		
		
		
		IF @Messages =''
		BEGIN
			set @Messages=(select Message from tbl_tempfieldmessage where fieldid=@v_fileID_inInt)
		END
		ELSE
		BEGIN
			set @Messages=@Messages+','+(select Message from tbl_tempfieldmessage where fieldid=@v_fileID_inInt)
		END
		
				
		FETCH NEXT FROM fileCursor INTO @v_fileID_inInt
		END
		CLOSE fileCursor
		DEALLOCATE fileCursor
			SELECT '$(document).ready(function (){$("#rajeshvalidate").validate({'+@RulesQuery+'},messages: {'+@Messages+'}});});' As Jquery
		END
	END

