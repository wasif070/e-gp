USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_get_DataUpToCMSForRHD]    Script Date: 4/24/2016 10:56:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[p_get_DataUpToCMSForRHD] 
as	 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select officeId, peOfficeName PE_Name,peCode PE_Code,dbo.f_getbiddercompany(n.userId) NoA_Name,td.tenderId Tender_Id,
procurementNature Proc_Nature ,procurementMethod Proc_Method,procurementType Proc_Type,
budgetType Budget_Type,sourceOfFund Source_Of_Funds,projectCode Project_Code,
projectName Project_Name,reoiRfpRefNo Tender_RefNo,invitationFor Tender_InvitationFor,
packageNo Tender_PkgNo,packageDescription Tender_PkgDesc,cpvCode Tender_Category, tenderPubDt Tender_PubDt,
docEndDate Tender_DocLstSelDt,preBidStartDt Tender_PreMeetingStrtDt,
preBidEndDt Tender_PreMeetingEndDt,submissionDt Tender_ClosingDt,
openingDt Tender_OpeningDt,securityLastDt Tender_SecLstSubDt,
tenderBrief Tender_BriefDesc,evalType Tender_EvaluationType,
docAvlMethod Tender_DocAvailable,docFeesMethod Tender_DocFees,pkgDocFees Tender_DocPrice,
(select SUM(tenderSecurityAmt) from tbl_TenderLotSecurity ls where ls.tenderId=td.tenderId ) Tender_SecAmt,
'Bank' Tender_SecPayMode,tenderSecurityDt Tender_SecValidUpToDt,
tenderValidityDt Tender_ValidUpToDt,
(select replace(convert(varchar(11),max(instValidUpto),111),'/','-') from tbl_TenderPayment tp where  tp.tenderId=td.tenderid 
and tp.userId=n.userid   and
paymentFor='Performance Security'  and isLive='yes' and isVerified='yes') Per_SecValidUpToDt,
(select top 1 bankName from tbl_TenderPayment tp where  tp.tenderId=td.tenderid 
and tp.userId=n.userid and  n.pkgLotId=tp.pkgLotId and
paymentFor='Performance Security'  and isLive='yes' and isVerified='yes' order by dtOfAction) Per_SecBankName,
(select top 1 branchName from tbl_TenderPayment tp where  tp.tenderId=td.tenderid 
and tp.userId=n.userid and
paymentFor='Performance Security'  and isLive='yes' and isVerified='yes' order by dtOfAction) Per_SecBankBranchName,
(select top 1  amount from tbl_TenderPayment tp where  tp.tenderId=td.tenderid 
and tp.userId=n.userid and
paymentFor='Performance Security'  and isLive='yes' and isVerified='yes' order by dtOfAction) Per_PayAmount,
(select top 1  tp.dtOfAction from tbl_TenderPayment tp where  tp.tenderId=td.tenderid 
and tp.userId=n.userid and
paymentFor='Performance Security'  and isLive='yes' and isVerified='yes' order by dtOfAction) Per_PayPaidDt,
(select top 1 tp.paymentInstType from tbl_TenderPayment tp where  tp.tenderId=td.tenderid 
and tp.userId=n.userid and
paymentFor='Performance Security'  and isLive='yes' and isVerified='yes' order by dtOfAction) Per_SecPayMode,
(select top 1 tp.instRefNumber from tbl_TenderPayment tp where  tp.tenderId=td.tenderid 
and tp.userId=n.userid and
paymentFor='Performance Security'  and isLive='yes' and isVerified='yes' order by dtOfAction) Per_PayInstrumentNo,
(select top 1 tp.issuanceBank from tbl_TenderPayment tp where  tp.tenderId=td.tenderid 
and tp.userId=n.userid and
paymentFor='Performance Security'  and isLive='yes' and isVerified='yes' order by dtOfAction) Per_PayIssuingBank,
(select top 1 tp.issuanceBranch from tbl_TenderPayment tp where  tp.tenderId=td.tenderid 
and tp.userId=n.userid and
paymentFor='Performance Security'  and isLive='yes' and isVerified='yes' order by dtOfAction) Per_PayIssuingBranch,
(select top 1 tp.instDate from tbl_TenderPayment tp where  tp.tenderId=td.tenderid 
and tp.userId=n.userid and
paymentFor='Performance Security'  and isLive='yes' and isVerified='yes' order by dtOfAction) Per_PayIssuanceDt,
contractNo Contract_No,
(select lotNo from tbl_TenderLotSecurity tls where tls.appPkgLotId=n.pkgLotId and tls.tenderId=n.tenderId) Contract_LotNo,
(select lotDesc from tbl_TenderLotSecurity tls where tls.appPkgLotId=n.pkgLotId and tls.tenderId=n.tenderId) Contract_DescOfLot,
case when procurementNatureId =3 then 
(select replace(convert(varchar(11),MIN(tls.indStartDt),111),'/','-') from tbl_TenderPhasing tls where tls.tenderId=n.tenderId)
else 
(select replace(convert(varchar(11),MIN(tls.startTime),111),'/','-') from tbl_TenderLotSecurity tls where tls.tenderId=n.tenderId)
end  as Contract_StartDt,
case when procurementNatureId =3 then 
(select replace(convert(varchar(11),max(tls.indEndDt),111),'/','-') from tbl_TenderPhasing tls where tls.tenderId=n.tenderId)
else 
(select replace(convert(varchar(11),max(tls.completionTime),111),'/','-') from tbl_TenderLotSecurity tls where tls.tenderId=n.tenderId)
end  as Contract_CompletionDt,
contractAmt Contract_Value,
n.contractDt NoA_Dt,
peName NoA_PEName

from tbl_TenderDetails  td,tbl_NoaIssueDetails n,tbl_ContractSign cs
where  td.tenderId=n.tenderId and n.noaIssueId=cs.noaId
-- and td.departmentId in(3,4,5)   -- Commented by Sudhir chavhan for Testing
                                   -- We need to add Road and Highway Department.

END

