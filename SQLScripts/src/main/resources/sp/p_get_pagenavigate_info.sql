USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_get_pagenavigate_info]    Script Date: 4/24/2016 10:58:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Select information for page navigation
--
--
-- Author: Karan
-- Date: 04-11-2010
--
-- Last Modified:
-- Modified By:
-- Date:
-- Modification: 
--				 
--------------------------------------------------------------------------------
-- SP Name	:   p_get_pagenavigate_info
-- Module	:	All Modules
-- Funftion	:	N.A.

ALTER PROCEDURE [dbo].[p_get_pagenavigate_info]

@v_Userid_inInt int 

AS

BEGIN

SET NOCOUNT ON;

DECLARE @v_NextScreen_Vc varchar(30), @v_isJvca_Vc varchar(10), @v_RegistrationType_Vc varchar(50)
	
	SELECT @v_NextScreen_Vc=nextScreen, 
		   @v_isJvca_Vc=isJvca, 
		   @v_RegistrationType_Vc=registrationType 
		   FROM tbl_LoginMaster WHERE userid=@v_Userid_inInt;  
		   
	IF (SUBSTRING(@v_RegistrationType_Vc,1,1)='C' Or SUBSTRING(@v_RegistrationType_Vc,1,1)='M' Or SUBSTRING(@v_RegistrationType_Vc,1,1)='G') And @v_isJvca_Vc='yes'
	BEGIN
		If @v_NextScreen_Vc = 'CompanyDetails'
			SELECT 'CompanyDetails-D, PersonalDetails-D, JvcaDetails-D, SupportingDocuments-D, Authenticate-D' as PageString 
		Else If @v_NextScreen_Vc = 'PersonalDetails'	
			SELECT 'CompanyDetails-E, PersonalDetails-E, JvcaDetails-D, SupportingDocuments-D, Authenticate-D' as PageString 
		Else If @v_NextScreen_Vc = 'JvcaDetails'	
			SELECT 'CompanyDetails-E, PersonalDetails-E, JvcaDetails-E, SupportingDocuments-D, Authenticate-D' as PageString 	
		Else If @v_NextScreen_Vc = 'SupportingDocuments'	
			SELECT 'CompanyDetails-E, PersonalDetails-E, JvcaDetails-E, SupportingDocuments-E, Authenticate-D' as PageString 
		Else If @v_NextScreen_Vc = 'Authenticate'	
			SELECT 'CompanyDetails-E, PersonalDetails-E, JvcaDetails-E, SupportingDocuments-E, Authenticate-E' as PageString 		
	END
	
	ELSE IF (SUBSTRING(@v_RegistrationType_Vc,1,1)='C' Or SUBSTRING(@v_RegistrationType_Vc,1,1)='M' Or SUBSTRING(@v_RegistrationType_Vc,1,1)='G') And @v_isJvca_Vc='no'
	BEGIN	
		If @v_NextScreen_Vc = 'CompanyDetails'
			SELECT 'CompanyDetails-D, PersonalDetails-D, SupportingDocuments-D, Authenticate-D' as PageString 
		Else If @v_NextScreen_Vc = 'PersonalDetails'	
			SELECT 'CompanyDetails-E, PersonalDetails-E, SupportingDocuments-D, Authenticate-D' as PageString 			
		Else If @v_NextScreen_Vc = 'SupportingDocuments'	
			SELECT 'CompanyDetails-E, PersonalDetails-E, SupportingDocuments-E, Authenticate-D' as PageString 
		Else If @v_NextScreen_Vc = 'Authenticate'	
			SELECT 'CompanyDetails-E, PersonalDetails-E, SupportingDocuments-E, Authenticate-E' as PageString
			 		
	END
	
	ELSE
	BEGIN
	print 'abc'
		If @v_NextScreen_Vc = 'IndividualConsultant'
			SELECT 'IndividualConsultant -D, SupportingDocuments-D, Authenticate-D' as PageString 		
		Else If @v_NextScreen_Vc = 'SupportingDocuments'	
			SELECT 'IndividualConsultant-E, SupportingDocuments-E, Authenticate-D' as PageString 
		Else If @v_NextScreen_Vc = 'Authenticate'	
			SELECT 'IndividualConsultant-E, SupportingDocuments-E, Authenticate-E' as PageString 	
	END

SET NOCOUNT OFF;

END

