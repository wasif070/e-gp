USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_gen_companyid]    Script Date: 4/24/2016 10:48:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- alter date: <alter Date,,>
-- Description:	<Description,,>
-- =============================================
-- SP Name:    p_gen_companyid
-- Module: User Registration
-- Function: Sp returns company id when new user register their details

ALTER PROCEDURE [dbo].[p_gen_companyid]
 @v_companyRegNumber_inVc varchar(20),
 @v_userId_inInt int
AS
BEGIN
	 INSERT INTO [dbo].[tbl_TempCompanyMaster]
           ([userId]
           ,[companyName]
           ,[companyNameInBangla]
           ,[companyRegNumber]
           ,[establishmentYear]
           ,[licIssueDate]
           ,[licExpiryDate]
           ,[regOffAddress]
           ,[regOffCountry]
           ,[regOffState]
           ,[regOffCity]
           ,[regOffUpjilla]
           ,[regOffPostcode]
           ,[regOffPhoneNo]
           ,[regOffFaxNo]
           ,[corpOffAddress]
           ,[corpOffCountry]
           ,[corpOffState]
           ,[corpOffCity]
           ,[corpOffUpjilla]
           ,[corpOffPostcode]
           ,[corpOffPhoneno]
           ,[corpOffFaxNo]
           ,[specialization]
           ,[legalStatus]
           ,[tinNo]
           ,[website])
     select @v_userId_inInt
           ,[companyName]
           ,[companyNameInBangla]
           ,[companyRegNumber]
           ,[establishmentYear]
           ,[licIssueDate]
           ,[licExpiryDate]
           ,[regOffAddress]
           ,[regOffCountry]
           ,[regOffState]
           ,[regOffCity]
           ,[regOffUpjilla]
           ,[regOffPostcode]
           ,[regOffPhoneNo]
           ,[regOffFaxNo]
           ,[corpOffAddress]
           ,[corpOffCountry]
           ,[corpOffState]
           ,[corpOffCity]
           ,[corpOffUpjilla]
           ,[corpOffPostcode]
           ,[corpOffPhoneno]
           ,[corpOffFaxNo]
           ,[specialization]
           ,[legalStatus]
           ,[tinNo]
           ,[website] from [dbo].[tbl_CompanyMaster] where [companyRegNumber]=@v_companyRegNumber_inVc
           update [dbo].[tbl_LoginMaster] set nextScreen='PersonalDetails',isJvca='no' where userid= @v_userId_inInt
           select Ident_Current('dbo.[tbl_TempCompanyMaster]')  companyId
END

