USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_search_SearchDebarredTenderers]    Script Date: 4/24/2016 11:21:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--
-- Purpose: DebarredTenderers WebService Reports
--
--
-- Author: Darshan Shah
-- Date: 21-04-2011
--
-- Last Modified:
-- Modified By: Sreenu
-- Date: 05-05-2011
-- Modification:



-- SP Name:  p_search_SearchDebarredTenderers
-- Module:   Debarment
-- Function: Store Procedure is use for Debarred Tenderers WebService Reports.

--------------------------------------------------------------------------------
/*

EXEC [dbo].[[p_search_SearchDebarredTenderers]

*/


ALTER PROCEDURE [dbo].[p_search_SearchDebarredTenderers]
AS
BEGIN
	SELECT	
			DebarmentReq.debarmentId,
			(CASE WHEN TendererMaster.companyId = 1 THEN TendererMaster.firstName + ' '+ TendererMaster.lasatName ELSE  compMaster.companyName END) AS companyName,
			DebarmentTypes.debarType AS debarredAt ,
			empMaster.employeeName,
			CONVERT(VARCHAR,debarDetail.debarmentDt, 106) AS debarDate,
			DebarmentReq.comments,
			CONVERT(VARCHAR,debarDetail.debarStartDt,106) + ' to '+ CONVERT(VARCHAR,debarDetail.debarEdnDt, 106) AS period,
			(selecT officeId from tbl_EmployeeOffices a, tbl_EmployeeMaster b where a.employeeId = b.employeeId and b.userId = DebarmentReq.debarmentBy) as officeId
	FROM	tbl_DebarmentReq  DebarmentReq
			INNER JOIN tbl_DebarmentTypes DebarmentTypes ON DebarmentTypes.debarTypeId = DebarmentReq.debarTypeId
			INNER JOIN tbl_DebarmentDetails debarDetail ON debarDetail.debarmentId = DebarmentReq.debarmentId
						AND ISNULL(debarDetail.debarStartDt, '') <> ''
						AND ISNULL(debarDetail.debarEdnDt, '') <> ''
						AND debarDetail.debarStatus = 'BYHOPE'
			INNER JOIN tbl_EmployeeMaster empMaster ON empMaster.userId = DebarmentReq.debarmentBy
			INNER JOIN tbl_TendererMaster TendererMaster ON TendererMaster.userId = DebarmentReq.userId
			INNER JOIN tbl_CompanyMaster compMaster ON compMaster.companyId = TendererMaster.companyId
			WHERE DebarmentReq.debarmentStatus in ('appdebarhope', 'appdebaregp', 'appdebarcomhope')
	ORDER BY debarmentDt DESC
END

