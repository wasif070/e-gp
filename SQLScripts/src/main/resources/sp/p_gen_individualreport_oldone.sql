USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_gen_individualreport_oldone]    Script Date: 4/24/2016 10:52:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- alter date: <alter Date,,>
-- Description:	<Description,,>
-- =============================================
-- exec [dbo].[p_gen_individualreport_oldone]  27
ALTER PROCEDURE [dbo].[p_gen_individualreport_oldone] 
	-- Add the parameters for the stored procedure here
	@v_formId_inInt int,
	@v_userId_inInt int=91
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	---- PARAMETERS DECLARATION ----------
	declare @v_TableNames_Vc varchar(8000), @v_ReportHtml varchar(Max), @v_FormName varchar(100), @v_TableId_Int int, @v_ColumnId_Int int, @v_ColumnHeader_vc varchar(200), @v_FilledBy_in int, @v_FormTableHeader varchar(950), @v_ColumnHeaderhtml_vc varchar(max), @v_RowId_Int int, @v_RowColId_int int, @v_RowCellValue varchar(8000), @v_CellHtml_Vc varchar(8000), @v_CellFinalHtml_vc varchar(max), @v_RowHtml_Vc varchar(8000), @v_rowfill_Int int, @v_colfill_Int int, @v_BidTableId_Int int, @v_FormfinalTableHeader varchar(max), @v_RowfinalHtml_Vc varchar(max)
	
	------------- SELECT FORMNAME ------------
	
	Select @v_FormName = formName from [tbl_TenderForms] where tenderFormId = @v_formId_inInt
	Set @v_FormTableHeader=''
	
	----- SELECT TABLES IN FORMS ----------
	
	
	
	Select @v_TableNames_Vc=  COALESCE(@v_TableNames_Vc+', ', ' ') + convert(varchar(20),tenderTableId)from [tbl_TenderTables] WHERE tenderFormId = @v_formId_inInt
	
	Select @v_TableNames_Vc 
	
	Set @v_ColumnHeaderhtml_vc = ''
	Set @v_CellHtml_Vc = ''
	set @v_CellFinalHtml_vc = ''
	set @v_RowHtml_Vc =''
	--set @v_FormfinalTableHeader=null
	
	
	
	
	--declare cur_tblNames cursor FAST_FORWARD For Select [tenderTableId] = 323 -- from dbo.f_split(@v_TableNames_Vc,',')

declare cur_tblNames cursor FAST_FORWARD For Select tenderTableId, bidTableId from 
tbl_TenderBidForm tf,tbl_TenderBidTable tb where tf.bidId = tb.bidid 
and tf.userId = 91 and tf.tenderFormId = 89

	
			--Set @v_FormTableHeader= @v_FormTableHeader + '<table border="2" width="100%" bordercolorlight="#A8DA7F" id="table1" style="border-collapse:" bordercolor="#A8DA7F"><tr><td colspan="3">'+'Table Name'+'</td></tr><tr>'		
			open cur_tblNames        
			
			fetch next from cur_tblNames into @v_TableId_Int, @v_BidTableId_Int
						
			print 'STEP 1 : First table Cursor'
			While @@Fetch_status = 0        
			Begin 				
			
			if @v_FormfinalTableHeader <> ''
			begin
			print 'in <>'
					Set @v_FormTableHeader= @v_FormfinalTableHeader + '</table><table border="2" width="100%" bordercolorlight="#A8DA7F" id="table1" style="border-collapse:" bordercolor="#A8DA7F"><tr><td colspan="3">'+'Table Name'+'</td></tr><tr>'
	
	end
					
			else
			begin
			print 'in else <>'
			Set @v_FormTableHeader= @v_FormTableHeader + '<table border="2" width="100%" bordercolorlight="#A8DA7F" id="table1" style="border-collapse:" bordercolor="#A8DA7F"><tr><td colspan="3">'+'Table Name'+'</td></tr><tr>'
		end	
					print 'TableID: ' + convert(varchar(200),@v_TableId_Int)
					print 'BidID: ' + convert(varchar(200),@v_BidTableId_Int)
							
					declare cur_tblfields cursor FAST_FORWARD For Select top 1 [columnId],[filledBy] from tbl_TenderColumns where [tenderTableId] = @v_TableId_Int
					
					open cur_tblfields        
			fetch next from cur_tblfields into @v_ColumnId_Int, @v_FilledBy_in			
			
			print 'STEP 1 : 2n table fields Cursor'
			While @@Fetch_status = 0        
			Begin 		
			
		select @v_TableId_Int as tableid, @v_ColumnId_Int as columnid,  @v_FilledBy_in as filledby
	
			Select @v_ColumnHeader_vc=  COALESCE(@v_ColumnHeader_vc+'</td><td>', ' ') + convert(varchar(20),columnHeader)from tbl_TenderColumns WHERE [tenderTableId] = @v_TableId_Int 
			set @v_ColumnHeaderhtml_vc =''
			--print @v_ColumnHeader_vc
			Set @v_ColumnHeaderhtml_vc = @v_ColumnHeaderhtml_vc + '<td>'+@v_ColumnHeader_vc+'</td>'
			print 'header td:'+ @v_ColumnHeaderhtml_vc
			Set @v_FormfinalTableHeader= @v_FormTableHeader +@v_ColumnHeaderhtml_vc +'</tr>'
			set @v_ColumnHeader_vc=null
			set @v_FormTableHeader = ''
			print 'FormfinalTableHeader: ' + @v_FormfinalTableHeader
					/* Inner Cursor for setting cell values */
	if @v_FilledBy_in = 1
	begin
	print 'taking rows from [tbl_TenderCells]'
	declare cur_rowValues cursor FAST_FORWARD For 			
					--Select distinct rowid from [tbl_TenderCells] where [tenderTableId] =@v_TableId_Int and columnid = @v_ColumnId_Int order by rowid
	
	
Select rowId, filledby
from vw_get_tendercolumndata
where tenderTableId =@v_TableId_Int and columnid = @v_ColumnId_Int order by rowid, columnid				
	end	
	else
	begin
	print 'taking rows from [tbl_TenderBidDetail]'
		declare cur_rowValues cursor FAST_FORWARD For 			
					--Select distinct rowid from tbl_TenderBidDetail where [tenderTableId] =@v_TableId_Int and columnid = @v_ColumnId_Int order by rowid
	
	
Select rowId, filledby
from vw_get_tenderbiddata 
where tenderTableId =@v_TableId_Int and columnid = @v_ColumnId_Int and bidtableid = @v_BidTableId_Int order by rowid, columnid				
	end			
			open cur_rowValues        
			fetch next from cur_rowValues into @v_rowId_Int, @v_rowfill_Int
			print 'STEP 2 : 2nd Row Cursor'
			print 'in rowid: '+ convert(varchar(20),@v_rowId_Int)
			
			While @@Fetch_status = 0        
			Begin 					
				if @v_rowfill_Int = 1
					begin
 			print 'taking cell val from [tbl_TenderCells]'
 				declare cur_cellValues cursor FAST_FORWARD For 			
					--Select columnid from [tbl_TenderCells] where [tenderTableId] =@v_TableId_Int and rowid = @v_rowId_Int and cellvalue <> '' order by rowid, columnid
	
	
Select columnId, filledby
from vw_get_tendercolumndata 
where tenderTableId =@v_TableId_Int and rowid = @v_rowId_Int  order by rowid, columnid

				
	
	end	
	else
	begin
	print 'taking cell val from [tbl_TenderBidDetail]'
				declare cur_cellValues cursor FAST_FORWARD For 			
					--Select columnid from tbl_TenderBidDetail where [tenderTableId] =@v_TableId_Int and rowid = @v_rowId_Int and cellvalue <> ''  order by rowid, columnid	
	
	

Select columnId, filledby
from  vw_get_tenderbiddata
where tenderTableId =@v_TableId_Int and rowid = @v_rowId_Int and bidtableid = @v_BidTableId_Int order by rowid, columnid

				
		end		
			open cur_cellValues        
			fetch next from cur_cellValues into @v_RowColId_int, @v_colfill_Int  
			print 'STEP 3 : 3nd cell Cursor'
			
			print @v_TableId_Int
			print 'Row id: ' +convert (varchar(50),@v_rowId_Int)
			print 'col id: ' +convert (varchar(50),@v_RowColId_int)			
			set @v_RowHtml_Vc = @v_RowHtml_Vc + '<tr>'
					
			While @@Fetch_status = 0        
			Begin 		
				
				if @v_colfill_Int = 1
					begin
						print 'taking cellrowValue val from [tbl_TenderCells]'
 			--Select @v_RowCellValue = cellvalue from [tbl_TenderCells] where [tenderTableId] =@v_TableId_Int and rowid = @v_rowId_Int and columnId = @v_RowColId_int
 	
 	
 		
Select   @v_RowCellValue = cellvalue
from vw_get_tendercolumndata where tenderTableId =@v_TableId_Int and rowid = @v_rowId_Int and columnid = @v_RowColId_int  order by rowid, columnid		
 	
 			
 			end
 			else
 			begin
 				print 'taking cellrowValue val from [tbl_TenderBidDetail]'
 				--Select @v_RowCellValue = cellvalue from tbl_TenderBidDetail where [tenderTableId] =@v_TableId_Int and rowid = @v_rowId_Int and columnId = @v_RowColId_int
 	
 	print 'kkk '+ convert (varchar(50),@v_rowId_Int) + 'column '+convert (varchar(50),@v_ColumnId_Int)

Select  @v_RowCellValue = cellvalue from vw_get_tenderbiddata where tenderTableId =@v_TableId_Int and rowid = @v_rowId_Int and columnid = @v_RowColId_int and bidtableid = @v_BidTableId_Int order by rowid, columnid
			
 			end
 			print 'Cell Value: '+@v_RowCellValue
 			
 			Set @v_CellHtml_vc = @v_CellHtml_vc + '<td>'+@v_RowCellValue+'</td>'
 			print @v_CellHtml_vc
 			--select @v_CellHtml_vc
 			
 			fetch next from cur_cellValues into @v_RowColId_int, @v_colfill_Int
			End   
			print 'cell fetch'   
				
			set @v_RowHtml_Vc = @v_RowHtml_Vc + @v_CellHtml_vc + '</tr>' 
				
			print 'row HTML' + @v_RowHtml_Vc 
			close cur_cellValues        
			deallocate cur_cellValues
			
 			print 'row fetch'  
 			
 			set @v_CellHtml_vc = ''
 			
 --			set @v_CellFinalHtml_vc = @v_CellFinalHtml_vc + '<tr>'+@v_CellHtml_vc+'</tr>'
 --			Select @v_CellFinalHtml_vc
 			fetch next from cur_rowValues into @v_rowId_Int, @v_rowfill_Int
 			    
			End       

			close cur_rowValues        
			deallocate cur_rowValues		
			
			fetch next from cur_tblfields into  @v_ColumnId_Int, @v_FilledBy_in      
		  set @v_FormfinalTableHeader = @v_FormfinalTableHeader + @v_RowHtml_Vc 
		  set @v_RowHtml_Vc = ''
		  print 'final html'+ @v_FormfinalTableHeader
		  
			End       

			close cur_tblfields        
			deallocate cur_tblfields
			--set @v_RowHtml_Vc = @v_RowHtml_Vc + '</table>' 
						
			fetch next from cur_tblNames into @v_TableId_Int, @v_BidTableId_Int
		  
			End       

			close cur_tblNames        
			deallocate cur_tblNames
			
			
	--Set @v_ColumnHeaderhtml_vc = @v_ColumnHeaderhtml_vc + '</tr>'
	--Select @v_FormTableHeader +'<tr>' +@v_ColumnHeaderhtml_vc+'</tr>'+@v_RowHtml_Vc
	Select @v_FormfinalTableHeader + '</table>'
	
END

