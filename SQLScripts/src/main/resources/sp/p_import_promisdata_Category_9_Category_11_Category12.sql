USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_import_promisdata_Category_9_Category_11_Category12]    Script Date: 4/24/2016 11:13:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- Modified By Dohatec on 10th August 2015 To Segregate Promis data import basis of Indicator Category 
-- =============================================
ALTER PROCEDURE [dbo].[p_import_promisdata_Category_9_Category_11_Category12]
AS
BEGIN

 

UPDATE  [Tbl_PromisIndicator]
SET [ComplaintReceived] = T.ComplaintReceived
           ,[ResolAwardModification] = T.ResolAwardModification
           ,[ComplaintResolved] = T.ComplaintResolved
           ,[RevPanelDecUpheld] = T.RevPanelDecUpheld
 FROM         
(select tenderId derivedTenderId,
case
	when
	(select COUNT(complaintId) from tbl_CMS_ComplaintMaster where tenderId = td.tenderId) > 0
	then
	'1'
	else
	'0'
end ComplaintReceived,
case 
when
(select COUNT(complaintId) from tbl_CMS_ComplaintMaster where tenderId = td.tenderId and complaintTypeId = 6 and complaintLevelId = 4 and complaintStatus = 'accept' ) >0
then '1'
else 
	'0'
end ResolAwardModification, --done
case
	when
	(select COUNT(complaintId) from tbl_CMS_ComplaintMaster 
	where tenderId = td.tenderId and 
	(complaintStatus = 'accept' or (complaintStatus = 'reject' and complaintLevelId = 4))) > 0
	then
	'1'
	else
		case
			when
				(select COUNT(complaintId) from tbl_CMS_ComplaintMaster 
				where tenderId = td.tenderId and (complaintStatus = 'pending' or complaintStatus = 'Clarification')) > 0
			then
				'100'
			else
				'0'	
		end
end ComplaintResolved,	--done on 9th Feb 2012 if at level 4 rejected then resolved
case
	when
		(selecT COUNT(complaintId) from tbl_CMS_ComplaintMaster 
		where complaintLevelId = 4 and panelId != 0 and (complaintStatus = 'pending' OR complaintStatus = 'Clarification') and tenderId = td.tenderId) > 0		
	then
		'100'
	else
		case
			when
				(selecT COUNT(complaintId) from tbl_CMS_ComplaintMaster 
				where complaintLevelId = 4 and panelId != 0 and (complaintStatus != 'pending' or complaintStatus != 'Clarification') and tenderId = td.tenderId) > 0
			then
				'1'
			else
				'0'
		end 
end RevPanelDecUpheld,
case
	when
		(select COUNT(complaintId) from tbl_CMS_ComplaintMaster 
		where tenderId = td.tenderId and complaintTypeId = 6) = 0
	then
		'0'
	else
		case
			when
				(select COUNT(complaintId) from tbl_CMS_ComplaintMaster 
				where tenderId = td.tenderId and complaintTypeId = 6) >= 1
			then
				case
					when
						(select COUNT(complaintId) from tbl_CMS_ComplaintMaster 
						where tenderId = td.tenderId and complaintTypeId = 6 and
						(complaintStatus = 'pending' or complaintStatus = 'Clarification'))	>= 1
					then
						'100'
					else
						case
							when
								(select COUNT(complaintId) from tbl_CMS_ComplaintMaster 
								where tenderId = td.tenderId and complaintTypeId = 6 and
								(complaintStatus = 'accept' 
								or (complaintStatus = 'reject' and complaintLevelId = 4))) >= 1
							then
								'0'
							else
								'1'
						end
				end
		end
end ContUnresolvDispute
from tbl_TenderDetails td,tbl_DepartmentMaster dm 
where tenderStatus not in('Pending')
and td.departmentId=dm.departmentId
and submissionDt < GETDATE())T
WHERE tenderId = T.derivedTenderId

-- Indicator 42
UPDATE Tbl_PromisIndicator set DetFraudCorruption = debarmentNumber FROM (
SELECT DebarOfficeID , MintenderId ,COUNT(debarmentId) debarmentNumber from
(SELECT debarmentId,T.OfficeID DebarOfficeID ,ApproveDate , min(tenderId) MintenderId from
(SELECT debarmentId,T.OfficeID ,ApproveDate ,creationDate,tenderId  from	
( SELECT debarmentId,OfficeID , MAX(entryDt) ApproveDate FROM
(SELECT d.debarmentId ,dbo.f_UserWiseOfficeID(debarmentBy) OfficeID,  dc.entryDt from tbl_DebarmentReq d
inner join tbl_DebarmentComments dc on d.debarmentId = dc.debarmentId
where debarmentStatus in ('appdebaregp','appdebarhope','appdebarcomhope')
and debarmentType in ('Corrupt Practice','Fraudulent Practice') )T
GROUP BY debarmentId,OfficeID) T
INNER JOIN (select tpi.tenderid, tpi.officeId , tm.creationDate from 
tbl_promisindicator tpi inner join tbl_TenderMaster tm on tpi.tenderId=tm.tenderId) T1
on T.OfficeID=  t1.OfficeID where T1.creationDate >= T. ApproveDate
) T GROUP BY debarmentId,T.OfficeID ,ApproveDate)T
GROUP BY DebarOfficeID ,MintenderId) T WHERE tenderId= MinTenderId and officeId = DebarOfficeID



END
