USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_get_tendercommondata1]    Script Date: 4/24/2016 11:01:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------
--
-- Purpose: Get Tender Common Data
--
--
-- Author: Rajesh Singh
-- Date: 11-11-2010
--
-- Last Modified:
-- Modified By: Kinjal,Rajesh,Krishnraj,Taher
-- Date 16-11-2010
-- Modification: Input output para changed
--------------------------------------------------------------------------------
-- exec p_get_tendercommondata 'GetTendererHash','1421' ,'0'
-- exec p_get_tendercommondata 'getformnamebytenderidandlotid',1483,'0'
-- [p_get_tendercommondata] 'getTenderNamebyTenderId','1495','1'
ALTER PROCEDURE [dbo].[p_get_tendercommondata1]
	 @v_fieldName1Vc varchar(500)=NULL,		  -- Action - AACombo/
	 @v_fieldName2Vc varchar(500)=NULL, -- tenderID
	 @v_fieldName3Vc varchar(500)=NULL --@v_estimatedcost_inVc
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	
	Declare @v_procurementNatureId_inVc as varchar(100),@v_budgetTypeId_inVc as varchar(100),
	@v_tempvarible_Vc as varchar(200),@v_departmentidCounter_Vc as varchar(500),@v_tempCounter_Vc as varchar(100),
	@v_departmentidlist_Vc VARCHAR(MAX),@v_designationIdlist_Vc VARCHAR(MAX),@v_employeeIdlist_Vc VARCHAR(MAX),
	@v_FinalemployeeIdlist_Vc VARCHAR(MAX),@v_departmentid_Vc as varchar(200), @v_tableName_Vc VARCHAR(500), 
	@v_formId_In INT, @v_tenderTableId_In INT, @v_colId_In INT, @v_flag_Vc Char(2), @v_formName_Vc VARCHAR(500), 
	@v_sectionId_In INT, @v_sectionName_Vc VARCHAR(500), @v_roleNameVc varchar(100), @v_newTenderTableId_In INT, 
	@v_newFormId_In INT, @v_Qry_Vc VARCHAR(MAX), @v_docAvlMethod_Vc VARCHAR(20), @v_appIdInt Int, @v_projectIdInt Int, @v_procureRoleIdInt Int, @v_roleIdInt int,
	@v_DocFeesMethod_Vc varchar(50), @v_curUserId int,
	@v_EnvelopeCnt_Int int
	-- print @v_fieldName3Vc
	
--- FOR PAGING	
Declare @v_RecordPerPage_inInt int,@v_StartAt_Int int, @v_Page_inInt int, @v_StopAt_Int int
Declare @v_PgCntQry_Vc varchar(8000),@v_CntQry_Vc varchar(8000),@v_FinalQueryVc varchar(max), @v_ConditionString_Vc varchar(50)
--------------------------------------------------------------------------------------------

-- For Payment
declare @v_BankDevelHeadId_Int int, @v_BankName varchar(500), @v_BankUserType varchar(500) 

If @v_fieldName1Vc='GetTecCpEmail'      -- Done by Rajesh for BidPreperation.jsp
Begin
	 SELECT  emailId as FieldValue1,reoiRfpRefNo as FieldValue2,
	 Convert(varchar(50), lm.userId ) as FieldValue3
	 from tbl_Committee c,tbl_CommitteeMembers cm,tbl_loginmaster lm,tbl_TenderDetails t
where c.committeeId=cm.committeeId 
and c.tenderId=@v_fieldName2Vc and committeeType in ('TEC','PEC')
and c.committeStatus='approved'
and memberRole='cp' and lm.userId=cm.userId and t.tenderId=c.tenderId 
End

IF @v_fieldName1Vc = 'GetEmpRoles'
BEGIN
select dbo.[getEmpRoles](@v_fieldName2Vc) as FieldValue1
END	
IF @v_fieldName1Vc = 'GetStatusOfUser'
BEGIN
select status as FieldValue1,firstLogin as FieldValue2 from tbl_LoginMaster where userId=@v_fieldName2Vc
END	
IF @v_fieldName1Vc = 'GetEmpRolesList'
BEGIN
	select convert(varchar(20), procurementRole) as FieldValue1
	from tbl_EmployeeRoles e,tbl_EmployeeMaster em,tbl_procurementrole pm 
    where   e.employeeId=em.employeeId and 
    em.userid=@v_fieldName2Vc and e.procurementroleid=pm.procurementroleid

END	
--IF @v_fieldName1Vc = 'GetProcurementNature'
--BEGIN
--	select  procurementnature from tbl_TenderDetails where tenderId=@v_fieldName2Vc

--END	

IF @v_fieldName1Vc = 'AACombo'
BEGIN

	--(1)Based on tenderId take procurementnatureid and budgettypeid from select * from Tbl_TenderDetails
	SELECT @v_departmentid_Vc=departmentId,@v_procurementNatureId_inVc=procurementNatureId,@v_budgetTypeId_inVc=budgetTypeId FROM tbl_TenderDetails WHERE tenderid=@v_fieldName2Vc
    --Departmentid=114
   
    --(2)Based on tenderId take APP Id from Tbl_TenderMaster
    --SELECT appid FROM tbl_TenderMaster WHERE tenderid=@v_fieldName2Vc
   
    --(3)From AppID take deptid from tbl_appMaster
    --SELECT appid FROM tbl_TenderMaster WHERE tenderid=@v_fieldName2Vc
    set @v_departmentidCounter_Vc=@v_departmentid_Vc
    set @v_tempCounter_Vc=''
	
	
	--select departmentType from tbl_DepartmentMaster where departmentId='8'
	
	If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Organization'
		Begin
			SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)
			SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
		End
	
	
	if @v_tempCounter_Vc=''
		BEGIN
			If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Division'
				BEGIN
					SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
					SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
				END
		END
	ELSE
		BEGIN
	
			If (select departmentType from tbl_DepartmentMaster where departmentId=@v_tempCounter_Vc)='Division'
				BEGIN
					SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
					SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)) as varchar(10))
				END
		END
		
	--if @v_tempCounter_Vc=''
	--	BEGIN
	--		If (select departmentType from tbl_DepartmentMaster where departmentId=@v_fieldName2Vc)='Ministry'
	--			BEGIN
	--				Set @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast(@v_tempvarible_Vc as varchar(10))
	--			END
	--	END
	--ELSE
	--	BEGIN
	--		If (select departmentType from tbl_DepartmentMaster where departmentId=@v_tempCounter_Vc)='Ministry'
	--			BEGIN
	--				Set @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast(@v_tempvarible_Vc as varchar(10))
	--			END
	--	END	
		
	
	SELECT @v_departmentidlist_Vc = COALESCE(@v_departmentidlist_Vc+',' , '') + cast(departmentid as varchar(10))
	FROM tbl_AppMaster WHERE appId in (select dbo.f_trim(items) from dbo.f_split(@v_departmentidCounter_Vc,','))
	
	--print @v_departmentidlist_Vc --114
	--(5)Take designationid from tbl_DesignationMaster based on all commaseparated deptid
	SELECT @v_designationIdlist_Vc = COALESCE(@v_designationIdlist_Vc+',' , '') + cast(designationId as varchar(10))
	FROM tbl_DesignationMaster WHERE departmentid in (select dbo.f_trim(items) from dbo.f_split(@v_departmentidlist_Vc,','))
	
	--(6)From commasepareted designationId take comma separted emaployeeid string from tbl_EmployeeMaster
	SELECT @v_employeeIdlist_Vc = COALESCE(@v_employeeIdlist_Vc+',' , '') + cast(employeeId as varchar(10))
	FROM tbl_EmployeeMaster WHERE designationId in (select dbo.f_trim(items) from dbo.f_split(@v_designationIdlist_Vc,','))
	
	--(7)With the use of employeeid string, procurementnatureid, budgettypeid, Estimated cost, 
	--compare on amount field from  [tbl_EmployeeFinancialPower], take all employeeid from 
	--[tbl_EmployeeFinancialPower] whose amount >= estimatedcost
	
	SELECT @v_FinalemployeeIdlist_Vc = COALESCE(@v_FinalemployeeIdlist_Vc+',' , '') + cast(employeeId as varchar(10))
	FROM tbl_EmployeeFinancialPower WHERE employeeid in (select dbo.f_trim(items) from dbo.f_split(@v_employeeIdlist_Vc,','))
	and procurementNatureId=@v_procurementNatureId_inVc and budgettypeid=@v_budgetTypeId_inVc and amount>=@v_fieldName3Vc
	
	--(8)From all employeeId take employeeName and bind combo with it.
	select convert(varchar(50),employeeId) as FieldValue1,employeeName as FieldValue2, '' AS FieldValue3,
	'' AS FieldValue4,'' AS  FieldValue5,'' AS FieldValue6, '' as FieldValue7, '' as FieldValue8 , '' as FieldValue9 , '' as FieldValue10 from tbl_EmployeeMaster where employeeId in (select dbo.f_trim(items) from dbo.f_split(@v_FinalemployeeIdlist_Vc,','))
	
END
IF @v_fieldName1Vc = 'getTenderConfigData'
BEGIN
 
	SELECT  convert(varchar(50),estCost) as FieldValue1 , (Select employeeName from tbl_EmployeeMaster where userid = approvingAuthId) as FieldValue2, convert(varchar(50),docAccess) as FieldValue3,  convert(varchar(50),tenderValDays) as FieldValue4, convert(varchar(50),tenderSecurityDays) as FieldValue5, (select templateName from tbl_TemplateMaster where templateId =stdTemplateId) as FieldValue6, Convert(varchar(50), approvingAuthId) as FieldValue7, eventtype as FieldValue8, procurementNature as FieldValue9 , procurementMethod as FieldValue10 
	
	from tbl_tenderdetails WHERE tenderId = convert(int,@v_fieldName2Vc)
END

IF @v_fieldName1Vc = 'chkDomesticPreference'
BEGIN
	IF((SELECT COUNT(tenderId) from tbl_tenderdetails WHERE tenderId = convert(int,@v_fieldName2Vc) and  procurementNature in('Goods','Works') and procurementMethod='OTM' and procurementType='ICT') != 0)
	BEGIN
		select 'true' as FieldValue1,domesticPref as FieldValue2,convert(varchar(50),cast(domesticPercent as  float)) as FieldValue3 from tbl_tenderdetails WHERE tenderId = convert(int,@v_fieldName2Vc)
	END
	Else
	BEGIN
		select 'false' as FieldValue1,domesticPref as FieldValue2,convert(varchar(50),domesticPercent) as FieldValue3 from tbl_tenderdetails WHERE tenderId = convert(int,@v_fieldName2Vc)
	END
END

IF @v_fieldName1Vc = 'SubContract'
BEGIN

select  distinct 
       convert(varchar(50),t.tenderId)  as FieldValue1
      ,[dbo].[f_getbiddercompany](invToUserId) as FieldValue2
      , REPLACE(CONVERT(VARCHAR(11),lastAcceptDt, 106), ' ', '-')  as FieldValue3
    
      ,convert(varchar(50),[remarks]) as FieldValue4
       ,  REPLACE(CONVERT(VARCHAR(11),[invSentDt], 106), ' ', '-') 
        
      , convert(varchar(250),cmm.companyName)  as FieldValue6
     ,convert(varchar(20),invAcceptStatus)  as FieldValue7 ,
     acceptComments  as FieldValue9
      from tbl_subcontracting t,tbl_TendererMaster tm,tbl_CompanyMaster cm,
tbl_TendererMaster ttm,tbl_CompanyMaster cmm
where t.invToUserId=tm.userId and tm.companyId=cm.companyId
and tenderid=@v_fieldName2Vc and invFromUserId=@v_fieldName3Vc
and ttm.userid=t.invFromuserid and ttm.companyid=cmm.companyId 
 

END
IF @v_fieldName1Vc = 'SubContractReq'
BEGIN
 select 
 distinct 
       convert(varchar(50),s.tenderId)  as FieldValue1
      ,[dbo].[f_getbiddercompany](st.invToUserId) as FieldValue2
      ,REPLACE(CONVERT(VARCHAR(11),st.lastAcceptDt, 106), ' ', '-') as FieldValue3
      ,convert(varchar(50),st.[remarks]) as FieldValue4
     , REPLACE(CONVERT(VARCHAR(11),st.[invSentDt], 106), ' ', '-') 
     , convert(varchar(250),c.companyName)  as FieldValue6
     ,convert(varchar(20),st.invAcceptStatus)  as FieldValue7,
     convert(varchar(50),st.subConId)  as FieldValue8,
     st.acceptComments  as FieldValue9
 
   from tbl_subcontracting s,
 tbl_TendererMaster t,tbl_CompanyMaster c,tbl_subcontracting st,
 tbl_TendererMaster tm,tbl_CompanyMaster cm
 where s.invFromUserId=t.userId and t.companyId=c.companyid
 and st.invToUserId =tm.userId and tm.companyid=cm.companyid
 and st.invToUserId=@v_fieldName3Vc and s.tenderId=@v_fieldName2Vc
 
--select  distinct 
--       convert(varchar(50),t.tenderId)  as FieldValue1
--      ,convert(varchar(250),cm.companyName) as FieldValue2
--      ,convert(varchar(10),lastAcceptDt,111) as FieldValue3
--      ,convert(varchar(50),[remarks]) as FieldValue4
--     , CONVERT(VARCHAR(10),[invSentDt],103) +' ' +Substring(CONVERT(VARCHAR(30),[invSentDt],108),1,5) 
--     , convert(varchar(250),cmm.companyName)  as FieldValue6
--     ,convert(varchar(20),invAcceptStatus)  as FieldValue7,
--     convert(varchar(50),subConId)  as FieldValue8
--      from tbl_subcontracting t,tbl_TendererMaster tm,tbl_CompanyMaster cm,
--tbl_TendererMaster ttm,tbl_CompanyMaster cmm
--where t.invToUserId=tm.userId and tm.companyId=cm.companyId
--and tenderid=@v_fieldName2Vc and invTouserid=@v_fieldName3Vc
--and ttm.userid=t.invFromuserid and ttm.companyid=cmm.companyId 
 

END
IF @v_fieldName1Vc = 'SubContractApprovedReq'
BEGIN

select  distinct 
       convert(varchar(50),t.tenderId)  as FieldValue1
      ,[dbo].[f_getbiddercompany](invFromUserId) as FieldValue2
      ,REPLACE(CONVERT(VARCHAR(11),lastAcceptDt, 106), ' ', '-') as FieldValue3
      ,convert(varchar(50),[remarks]) as FieldValue4
       ,REPLACE(CONVERT(VARCHAR(11),[invSentDt], 106), ' ', '-')  as FieldValue5
      , convert(varchar(250),cmm.companyName)  as FieldValue6
     ,convert(varchar(20),invAcceptStatus)  as FieldValue7,
     convert(varchar(20),subConId )  as FieldValue8
     ,acceptComments  as FieldValue9,lm.emailid  as FieldValue10
      from tbl_subcontracting t,tbl_TendererMaster tm,tbl_CompanyMaster cm,
tbl_TendererMaster ttm,tbl_CompanyMaster cmm,tbl_loginmaster lm
where t.invToUserId=tm.userId and tm.companyId=cm.companyId and lm.userId=ttm.userId
and tenderid=@v_fieldName2Vc and (invTouserid=@v_fieldName3Vc )
and ttm.userid=t.invFromuserid and ttm.companyid=cmm.companyId 
 

END

IF @v_fieldName1Vc = 'SubContractInvAppReq'
BEGIN

select  distinct 
       convert(varchar(50),t.tenderId)  as FieldValue1
      ,convert(varchar(250),cm.companyName) as FieldValue2
      ,REPLACE(CONVERT(VARCHAR(11),lastAcceptDt, 106), ' ', '-') as FieldValue3
      ,convert(varchar(50),[remarks]) as FieldValue4
       ,REPLACE(CONVERT(VARCHAR(11),[invSentDt], 106), ' ', '-')  as FieldValue5
      , convert(varchar(250),cmm.companyName)  as FieldValue6
     ,convert(varchar(20),invAcceptStatus)  as FieldValue7,acceptComments  as FieldValue9,convert(varchar(20),invFromUserId)  as FieldValue10 
      from tbl_subcontracting t,tbl_TendererMaster tm,tbl_CompanyMaster cm,
tbl_TendererMaster ttm,tbl_CompanyMaster cmm
where t.invFromuserid=tm.userId and tm.companyId=cm.companyId
and tenderid=@v_fieldName2Vc and (invFromuserid=@v_fieldName3Vc or  invTouserid=@v_fieldName3Vc)
and ttm.userid=t.invFromuserid and ttm.companyid=cmm.companyId 
and invAcceptStatus='Approved'
 

END


IF @v_fieldName1Vc = 'EvalCom'
BEGIN
 select  convert(varchar(50),[minTenderVal]) as FieldValue1
      , convert(varchar(50),[maxTenderVal]) as FieldValue2
      , convert(varchar(50),[minMemReq]) as FieldValue3
      , convert(varchar(50),[maxMemReq]) as FieldValue4
      , convert(varchar(50),[minMemOutSidePe]) as FieldValue5
      , convert(varchar(50),[minMemFromPe]) as FieldValue6
      , convert(varchar(50),[minMemFromTec]) as FieldValue7,case when t.procurementNatureId =1 then 'TEC' when t.procurementNatureId =2 then 'TEC' when t.procurementNatureId =3 then 'PEC' END FieldValue8
        from tbl_TenderDetails  t,tbl_configtec p
 where tenderId=@v_fieldName2Vc and t.procurementNatureId=p.procurementNatureId  
 and committeeType=(select case when  procurementNatureId =1 then 'TEC' when  procurementNatureId =2 then 'TEC' when  procurementNatureId =3 then 'PEC' END from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
 
END

IF @v_fieldName1Vc = 'TSCCom'
BEGIN
 select  convert(varchar(50),[minTenderVal]) as FieldValue1
      , convert(varchar(50),[maxTenderVal]) as FieldValue2
      , convert(varchar(50),[minMemReq]) as FieldValue3
      , convert(varchar(50),[maxMemReq]) as FieldValue4
      , convert(varchar(50),[minMemOutSidePe]) as FieldValue5
      , convert(varchar(50),[minMemFromPe]) as FieldValue6
      , convert(varchar(50),[minMemFromTec]) as FieldValue7,'TSC'   FieldValue8
        from  tbl_configtec p
 where   committeeType='TSC'
 
END

IF @v_fieldName1Vc = 'OpenComHistory'
BEGIN
   select convert(varchar(50),minMembers) as FieldValue3
      , convert(varchar(50),maxMembers) as FieldValue4
      , convert(varchar(50),minMemOutSide) as FieldValue5
      , convert(varchar(50),minMemFromPe) as FieldValue6
      , convert(varchar(50),minMemFromTec) as FieldValue7,   committeeType FieldValue8 ,
      convert(varchar(50),committeeId) as FieldValue9 
        from tbl_CommitteeHistory
 where tenderId=@v_fieldName2Vc  and committeeType in('TOC','POC')
 
 END

IF @v_fieldName1Vc = 'EvalComHistory'
BEGIN
 select 
       convert(varchar(50),minMembers) as FieldValue3
      , convert(varchar(50),maxMembers) as FieldValue4
      , convert(varchar(50),minMemOutSide) as FieldValue5
      , convert(varchar(50),minMemFromPe) as FieldValue6
      , convert(varchar(50),minMemFromTec) as FieldValue7,   committeeType FieldValue8,
      convert(varchar(50),committeeId) as FieldValue9
        from tbl_CommitteeHistory
 where tenderId=@v_fieldName2Vc  and committeeType in('TEC','PEC')
 
END

IF @v_fieldName1Vc = 'TSCComHistory'
BEGIN
select
   convert(varchar(50),minMembers) as FieldValue3
      , convert(varchar(50),maxMembers) as FieldValue4
      , convert(varchar(50),minMemOutSide) as FieldValue5
      , convert(varchar(50),minMemFromPe) as FieldValue6
      , convert(varchar(50),minMemFromTec) as FieldValue7,   committeeType FieldValue8,
      convert(varchar(50),committeeId) as FieldValue9
        from tbl_CommitteeHistory
 where tenderId=@v_fieldName2Vc  and committeeType in('TSC')
 
 
END

IF @v_fieldName1Vc = 'GetDocuments'
BEGIN
 
declare @v_FormList_Vc1 varchar(2000)
 
 select @v_FormList_Vc1 = COALESCE(@v_FormList_Vc1+',', ' ') + convert(varchar(20), formid) 
 from tbl_biddocuments where userId=@v_fieldName3Vc
 --print    @v_FormList_Vc1 
 select  
  convert(varchar(50),a.companyDocId ) as FieldValue1,
 convert(varchar(150),documentName) as FieldValue2
      , convert(varchar(150),documentSize) as FieldValue3
      , convert(varchar(250),documentBrief) as FieldValue4
      , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), ' ', '-')   as FieldValue5
      , convert(varchar(50),docstatus) as FieldValue6
      , convert(varchar(50),b.companyDocId ) as FieldValue7
       
      ,case when b.companyDocId is not null then @v_FormList_Vc1 else null end as FieldValue10
       
      from (select c.* from tbl_CompanyDocuments c,tbl_TendererMaster t
where c.tendererId=t.tendererId and userId=@v_fieldName3Vc and folderId=@v_fieldName2Vc and docStatus!='archive' ) a
left outer join
(select distinct companyDocId  from tbl_biddocuments where userId=@v_fieldName3Vc) b
on a.companyDocId=b.companydocid 
 --print @v_FormList_Vc1
END
IF @v_fieldName1Vc = 'GetBidderDocuments'
BEGIN
 
 select  
  convert(varchar(50),a.companyDocId ) as FieldValue1,
 convert(varchar(150),documentName) as FieldValue2
      , convert(varchar(150),documentSize) as FieldValue3
      , convert(varchar(250),documentBrief) as FieldValue4
      , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), ' ', '-')   as FieldValue5
      , convert(varchar(50),docstatus) as FieldValue6 
       ,  convert(varchar(50),formId)  as FieldValue10
        
      from (select c.*,formid from tbl_CompanyDocuments c,tbl_TendererMaster t,tbl_biddocuments b
where c.tendererId=t.tendererId and t.userId=@v_fieldName2Vc  and b.companyDocId=c.companyDocId
and formId=@v_fieldName3Vc ) a
 
 
END
IF @v_fieldName1Vc = 'GetFolders'
BEGIN
 select  
  convert(varchar(50),folderId ) as FieldValue1,
 convert(varchar(150),folderName) as FieldValue2
    
       
      from     tbl_TendererFolderMaster c,tbl_TendererMaster t
where c.tendererId=t.tendererId and userId=@v_fieldName3Vc  
 
END
IF @v_fieldName1Vc = 'GetPrebidDate'
BEGIN
 select  
  REPLACE(CONVERT(VARCHAR(11),preBidStartDt, 106), ' ', '-') +' ' +Substring(CONVERT(VARCHAR(30),preBidStartDt,108),1,5)   as FieldValue1,
 REPLACE(CONVERT(VARCHAR(11),preBidEndDt, 106), ' ', '-') +' '+Substring(CONVERT(VARCHAR(30),preBidEndDt,108),1,5) as FieldValue2
    
       
      from     tbl_TenderDetails  
where tenderId=@v_fieldName2Vc  
 
END

IF @v_fieldName1Vc = 'IndConsultant'
BEGIN
 
select convert(varchar(50),l.userid) as FieldValue1,
convert(varchar(50),emailid)+'-'+convert(varchar(400),fullName)
 as FieldValue2,'' as FieldValue3,'' as FieldValue4,'' as FieldValue5,'' as FieldValue6 from tbl_LoginMaster l,
 tbl_ExternalMemInfo t
where l.userId=t.userid  
and status='Approved' and emailid=''+@v_fieldName2Vc+''
 
END


IF @v_fieldName1Vc = 'TocMemEncDec'
BEGIN
 
select convert(varchar(50),tenderId) as FieldValue1  from tbl_TenderHash t
where  tenderId=''+@v_fieldName2Vc+''
 
END


IF @v_fieldName1Vc = 'GetTdsInfo'
BEGIN
select distinct  convert(varchar(50),its.ittHeaderId) as FieldValue1 ,
case when ittReference=0 then '' else ittHeaderName end FieldValue8 ,
case when ittReference=0 then '' else convert(varchar(50),ittClauseId)  end FieldValue2,
case when ittReference=0 then '' else ittClauseName end FieldValue3,
case when ittReference=0 then '' else convert(varchar(50),ittsubclauseid) end FieldValue4,
case when ittReference=0 then '' else ittSubClauseName end FieldValue5,
convert(varchar(50),tdsSubClauseId) FieldValue7 ,
tdsSubClauseName FieldValue6,convert(varchar(50),tdsSubClauseId) FieldValue9,
convert(varchar(50),orderNumber) FieldValue10,ittReference,orderNumber,ittSubClauseId   
from (select h.*,k.[ittClauseId] 
       
      ,[ittClauseName], l.[ittSubClauseId],l.[ittSubClauseName] from (select * from tbl_IttHeader where ittHeaderId=@v_fieldName2Vc )h
left outer join
(select [ittClauseId],
       i.ittHeaderId 
      ,[ittClauseName] from tbl_ittClause c,tbl_IttHeader i where 
i.ittheaderid =@v_fieldName2Vc and i.ittheaderid=c.ittheaderid ) k
on
h.ittHeaderId=k.ittHeaderId 
inner join
(select [ittSubClauseId],
		c.[ittClauseId]
      ,[ittSubClauseName]
      ,[isTdsApplicable] from
 Tbl_IttHeader i,
Tbl_IttClause c,Tbl_IttSubClause s 
 where i.ittHeaderId=c.ittHeaderId 
 and i.ittHeaderId=@v_fieldName2Vc and c.ittClauseId=s.ittClauseId and isTdsApplicable='yes')l
 on k.ittClauseId=l.ittClauseId ) its
 left outer join (select * from tbl_TdsSubClause where ittHeaderId=@v_fieldName2Vc) tds
 on its.ittheaderid=tds.ittHeaderId  and (its.ittSubClauseId=tds.ittReference or tds.ittReference=0)
 order by ittSubClauseId
 

  
 
 
END

IF @v_fieldName1Vc = 'OpenCom'
BEGIN
 select  convert(varchar(50),[minTenderVal]) as FieldValue1
      , convert(varchar(50),[maxTenderVal]) as FieldValue2
      , convert(varchar(50),[minMemReq]) as FieldValue3
      , convert(varchar(50),[maxMemReq]) as FieldValue4
      , convert(varchar(50),[minMemOutSidePe]) as FieldValue5
      , convert(varchar(50),[minMemFromPe]) as FieldValue6
      , convert(varchar(50),[minMemFromTec]) as FieldValue7,case when t.procurementNatureId =1 then 'TOC' when t.procurementNatureId =2 then 'TOC' when t.procurementNatureId =3 then 'POC' END FieldValue8  from tbl_TenderDetails  t,tbl_configtec p
 where tenderId=@v_fieldName2Vc and t.procurementNatureId=p.procurementNatureId  
 and committeeType=(select case when  procurementNatureId =1 then 'TOC' when  procurementNatureId =2 then 'TOC' when  procurementNatureId =3 then 'POC' END from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
END
IF @v_fieldName1Vc = 'SamePE'
BEGIN
select convert(varchar(50),em.userId) as FieldValue1,convert(varchar(150),em.employeeName + '-' + dm.designationName) as FieldValue2,dbo.getEmpRoles(em.userId) as FieldValue3,
convert(varchar(50),tem.govUserId) as FieldValue4,'' as FieldValue5,'' as FieldValue6
 from tbl_officemaster o,tbl_TenderDetails t,tbl_employeeOffices eo,tbl_EmployeeMaster em,tbl_DesignationMaster dm,tbl_LoginMaster lm,tbl_EmployeeTrasfer tem
where o.officeId=t.officeId and t.tenderId=@v_fieldName2Vc  and o.officeId=eo.officeId 
and eo.employeeId=em.employeeId and eo.designationId=dm.designationId
and tem.employeeId=eo.employeeId and tem.isCurrent='yes'
and lm.userId=em.userId and lm.status='Approved'

END
IF @v_fieldName1Vc = 'SamePETSC'
BEGIN
select convert(varchar(50),em.userId) as FieldValue1,convert(varchar(150),em.employeeName + '-' + dm.designationName) as FieldValue2,dbo.getEmpRoles(em.userId) as FieldValue3,
convert(varchar(50),tem.govUserId) as FieldValue4,'' as FieldValue5,'' as FieldValue6
 from tbl_officemaster o,tbl_TenderDetails t,tbl_employeeOffices eo,tbl_EmployeeMaster em,tbl_DesignationMaster dm,tbl_LoginMaster lm,tbl_EmployeeTrasfer tem
where o.officeId=t.officeId and t.tenderId=@v_fieldName2Vc  and o.officeId=eo.officeId 
and eo.employeeId=em.employeeId and eo.designationId=dm.designationId
and tem.employeeId=eo.employeeId and tem.isCurrent='yes'
and lm.userId=em.userId and lm.status='Approved' 
and lm.userId not in (select cm.userId from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId= cm.committeeId and c.tenderId=@v_fieldName2Vc and c.committeeType in ('TEC','PEC') and c.committeStatus='approved') 

END
IF @v_fieldName1Vc = 'OtherAgency'
BEGIN
select convert(varchar(50),em.userId) as FieldValue1,convert(varchar(150),em.employeeName + '-' + dm.designationName) as FieldValue2,
dbo.getEmpRoles(em.userId) as FieldValue3,convert(varchar(50),tem.govUserId) as FieldValue4,'' as FieldValue5,'' as FieldValue6
 from  tbl_employeeOffices eo,tbl_EmployeeMaster em,tbl_DesignationMaster dm,tbl_loginmaster lm,tbl_EmployeeTrasfer tem
where officeId=@v_fieldName2Vc  and eo.employeeId=em.employeeId
and tem.employeeId = eo.employeeId and tem.isCurrent='yes'
and lm.userId=em.userId and lm.status='approved'
 and eo.designationId=dm.designationId 
END
IF @v_fieldName1Vc = 'TECMember'
BEGIN
 
select distinct convert(varchar(50),em.userId) as FieldValue1,em.employeeName as FieldValue2, cm.memberRole as FieldValue3,
cm.memberFrom as FieldValue4,c.committeeName as FieldValue5, convert(varchar(50),c.committeeId) as FieldValue6, convert(varchar(50),cm.govUserId) as FieldValue7,tcrm.comRoleId  from tbl_Committee c,tbl_CommitteeMembers cm,tbl_EmployeeMaster em,tbl_EmployeeOffices eo,tbl_CommitteeRoleMaster tcrm,
tbl_DesignationMaster dm
 where  tcrm.comRole=cm.memberRole and  c.committeeId=cm.committeeId  and cm.userId=em.userid and tenderId=@v_fieldName2Vc
 and em.employeeId=eo.employeeId and eo.designationId=dm.designationId 
 and  committeeType=(select case when  procurementNatureId =1 then 'TEC' when 
  procurementNatureId =2 then 'TEC' when  procurementNatureId =3 then 'PEC' END from tbl_TenderDetails 
  where tenderId=@v_fieldName2Vc) order by tcrm.comRoleId
END

IF @v_fieldName1Vc = 'TECMemberHistory'
BEGIN
 
select distinct convert(varchar(50),em.userId) as FieldValue1,em.employeeName as FieldValue2, cm.memberRole as FieldValue3,
cm.memberFrom as FieldValue4,c.committeeName as FieldValue5, convert(varchar(50),c.committeeId) as FieldValue6,convert(varchar(50),cm.govUserId) as FieldValue7,tcrm.comRoleId  from tbl_CommitteeHistory c,tbl_CommitteeMembersHistory cm,tbl_EmployeeMaster em,tbl_EmployeeOffices eo,tbl_CommitteeRoleMaster tcrm,
tbl_DesignationMaster dm
 where  tcrm.comRole=cm.memberRole and  c.committeeId=cm.committeeId  and cm.userId=em.userid and tenderId=@v_fieldName2Vc
 and em.employeeId=eo.employeeId and eo.designationId=dm.designationId 
 and cm.committeeId=@v_fieldName3Vc  order by tcrm.comRoleId
END

IF @v_fieldName1Vc = 'IsTECMember'
BEGIN
 
select distinct convert(varchar(50),em.userId) as FieldValue1,em.employeeName as FieldValue2, cm.memberRole as FieldValue3,
cm.memberFrom as FieldValue4,c.committeeName as FieldValue5, convert(varchar(50),c.committeeId) as FieldValue6 from tbl_Committee c,tbl_CommitteeMembers cm,tbl_EmployeeMaster em,tbl_EmployeeOffices eo,
tbl_DesignationMaster dm
 where   c.committeeId=cm.committeeId  and cm.userId=em.userid and tenderId=@v_fieldName2Vc and cm.userid=@v_fieldName3Vc
 and em.employeeId=eo.employeeId and eo.designationId=dm.designationId 
 and  committeeType=(select case when  procurementNatureId =1 then 'TEC' when 
  procurementNatureId =2 then 'TEC' when  procurementNatureId =3 then 'PEC' END from tbl_TenderDetails 
  where tenderId=@v_fieldName2Vc)
END
IF @v_fieldName1Vc = 'SameTEC'
BEGIN
 
select distinct convert(varchar(50),em.userId) as FieldValue1,convert(varchar(150),em.employeeName + '-' + dm.designationName) as FieldValue2, dbo.getEmpRoles(em.userId) as FieldValue3,
convert(varchar(50),tem.govUserId) as FieldValue4,'' as FieldValue5, '' as FieldValue6 from tbl_Committee c,tbl_CommitteeMembers cm,tbl_EmployeeMaster em,tbl_EmployeeOffices eo,tbl_EmployeeTrasfer tem,
tbl_DesignationMaster dm
 where   c.committeeId=cm.committeeId  and cm.userId=em.userid and tenderId=@v_fieldName2Vc
 and em.employeeId=eo.employeeId and eo.designationId=dm.designationId 
 and em.employeeId = tem.employeeId and tem.isCurrent='yes'
 and  committeStatus='Approved'
 and  committeeType in('TEC','PEC')
END
IF @v_fieldName1Vc = 'TOCMember'
BEGIN
select  distinct convert(varchar(50),em.userId) as FieldValue1,em.employeeName as FieldValue2, cm.memberRole as FieldValue3,
cm.memberFrom as FieldValue4, c.committeeName  as FieldValue5, convert(varchar(50),c.committeeId) as FieldValue6,convert(varchar(50),cm.govUserId) as FieldValue7,tcrm.comRoleId from tbl_Committee c,tbl_CommitteeMembers cm,tbl_EmployeeMaster em,tbl_EmployeeOffices eo,tbl_CommitteeRoleMaster tcrm,
tbl_DesignationMaster dm
 where  tcrm.comRole=cm.memberRole and  c.committeeId=cm.committeeId  and cm.userId=em.userid
 and em.employeeId=eo.employeeId and eo.designationId=dm.designationId 
  and tenderId=@v_fieldName2Vc
 and  committeeType=(select case when  procurementNatureId =1 then 'TOC' when  procurementNatureId =2 then 'TOC' when  procurementNatureId =3 then 'POC' END from tbl_TenderDetails where tenderId=@v_fieldName2Vc) order by tcrm.comRoleId
END

IF @v_fieldName1Vc = 'TOCMemberHistory'
BEGIN
select  distinct convert(varchar(50),em.userId) as FieldValue1,em.employeeName as FieldValue2, cm.memberRole as FieldValue3,
cm.memberFrom as FieldValue4, c.committeeName  as FieldValue5, convert(varchar(50),c.committeeId) as FieldValue6,convert(varchar(50),cm.govUserId) as FieldValue7,tcrm.comRoleId from tbl_CommitteeHistory c,tbl_CommitteeMembersHistory cm,tbl_EmployeeMaster em,tbl_EmployeeOffices eo,tbl_CommitteeRoleMaster tcrm,
tbl_DesignationMaster dm
 where  tcrm.comRole=cm.memberRole and  c.committeeId=cm.committeeId  and cm.userId=em.userid
 and em.employeeId=eo.employeeId and eo.designationId=dm.designationId 
  and cm.committeeId=@v_fieldName3Vc  order by tcrm.comRoleId
END

IF @v_fieldName1Vc = 'TOCApprovedMember'
BEGIN
select  distinct convert(varchar(50),em.userId) as FieldValue1,em.employeeName as FieldValue2, cm.memberRole as FieldValue3,
cm.memberFrom as FieldValue4, c.committeeName  as FieldValue5, convert(varchar(50),c.committeeId) as FieldValue6,tcrm.comRoleId from tbl_Committee c,tbl_CommitteeMembers cm,tbl_EmployeeMaster em,tbl_EmployeeOffices eo,tbl_CommitteeRoleMaster tcrm,
tbl_DesignationMaster dm
 where tcrm.comRole=cm.memberRole and c.committeeId=cm.committeeId  and cm.userId=em.userid
 and em.employeeId=eo.employeeId and eo.designationId=dm.designationId 
  and tenderId=@v_fieldName2Vc and c.committeStatus='Approved'
 and  committeeType=(select case when  procurementNatureId =1 then 'TOC' when  procurementNatureId =2 then 'TOC' when  procurementNatureId =3 then 'POC' END from tbl_TenderDetails where tenderId=@v_fieldName2Vc) order by tcrm.comRoleId
END
IF @v_fieldName1Vc = 'TSCMember'
BEGIN
select convert(varchar(50),em.userId) as FieldValue1,em.employeeName as FieldValue2, cm.memberRole as FieldValue3,
cm.memberFrom as FieldValue4, c.committeeName  as FieldValue5, convert(varchar(50),c.committeeId) as FieldValue6,convert(varchar(50),cm.govUserId) as FieldValue7 from tbl_Committee c,tbl_CommitteeMembers cm,tbl_EmployeeMaster em,tbl_EmployeeOffices eo,
tbl_DesignationMaster dm

 where   c.committeeId=cm.committeeId  and cm.userId=em.userid
 and em.employeeId=eo.employeeId and eo.designationId=dm.designationId 
 and  committeeType='TSC' and tenderId=@v_fieldName2Vc
 union
 select convert(varchar(50),lm.userId) as FieldValue1,fullName   as FieldValue2, cm.memberRole as FieldValue3,
cm.memberFrom as FieldValue4, c.committeeName  as FieldValue5,
 convert(varchar(50),c.committeeId) as FieldValue6,convert(varchar(50),cm.govUserId) as FieldValue7 from tbl_Committee c,
 tbl_CommitteeMembers cm,tbl_LoginMaster lm,tbl_ExternalMemInfo tm
 
 where   c.committeeId=cm.committeeId  and cm.userId=lm.userid
 and lm.userId=tm.userId
 
 and  committeeType='TSC' and tenderId=@v_fieldName2Vc
END

IF @v_fieldName1Vc = 'TSCMemberHistory'
BEGIN
select convert(varchar(50),em.userId) as FieldValue1,em.employeeName as FieldValue2, cm.memberRole as FieldValue3,
cm.memberFrom as FieldValue4, c.committeeName  as FieldValue5, convert(varchar(50),c.committeeId) as FieldValue6 from tbl_CommitteeHistory c,tbl_CommitteeMembersHistory cm,tbl_EmployeeMaster em,tbl_EmployeeOffices eo,
tbl_DesignationMaster dm

 where   c.committeeId=cm.committeeId  and cm.userId=em.userid
 and em.employeeId=eo.employeeId and eo.designationId=dm.designationId 
 and  committeeType='TSC' and cm.committeeId=@v_fieldName3Vc
 union
 select convert(varchar(50),lm.userId) as FieldValue1,fullName   as FieldValue2, cm.memberRole as FieldValue3,
cm.memberFrom as FieldValue4, c.committeeName  as FieldValue5,
 convert(varchar(50),c.committeeId) as FieldValue6 from tbl_Committee c,
 tbl_CommitteeMembersHistory cm,tbl_LoginMaster lm,tbl_ExternalMemInfo tm
 
 where   c.committeeId=cm.committeeId  and cm.userId=lm.userid
 and lm.userId=tm.userId
 
 and  committeeType='TSC' and cm.committeeId=@v_fieldName3Vc
END


IF @v_fieldName1Vc = 'getStd' -- for ConfigureKeyInfo.jsp 
BEGIN

select * from (SELECT Distinct 1 as ID ,convert(varchar(20),tbl_TemplateMaster.templateId) as FieldValue1, 
tbl_TemplateMaster.templateName as FieldValue2 FROM tbl_ConfigStd INNER JOIN tbl_TemplateMaster 
ON tbl_ConfigStd.templateId = tbl_TemplateMaster.templateId INNER JOIN tbl_TenderDetails 
ON tbl_ConfigStd.procurementMethodId = tbl_TenderDetails.procurementmethodId 
AND tbl_ConfigStd.procurementNatureId = tbl_TenderDetails.procurementNatureId 
inner join tbl_ProcurementTypes on tbl_ProcurementTypes.procurementType = 
case when tbl_ConfigStd.procurementTypeId =2 then 'ICT' else 'NCT' END inner join 
tbl_TenderTypes on tbl_ConfigStd.tenderTypeId = tbl_TenderTypes.tenderTypeId 
where tbl_TenderDetails.tenderId = @v_fieldName2Vc and status='A'
and
case when tbl_ConfigStd.operator = '>' then 
	case when estCost > tendervalue then 1 else 0 end	
	when tbl_ConfigStd.operator = '<' then 
		case when estCost < tendervalue then 1 else 0 end
	when tbl_ConfigStd.operator = '<=' then 
		case when estCost <= tendervalue then 1 else 0 end
	when tbl_ConfigStd.operator = '>=' then 
		case when estCost >= tendervalue then 1 else 0 end
	when tbl_ConfigStd.operator = '=' then
		 case when estCost =tendervalue then 1 else 0 end
	when tbl_ConfigStd.operator = '<>' then 
		case when estCost  <>tendervalue then 1 else 0 end
end=1 
union SELECT Distinct  2 as ID,convert(varchar(20),tbl_TemplateMaster.templateId) as FieldValue1,  
tbl_TemplateMaster.templateName as FieldValue2 FROM tbl_TemplateMaster where status='A') a order by ID

 
END


IF @v_fieldName1Vc = 'CorrigendumInfo'
BEGIN
  
   --  SELECT Convert(varchar(50), corriDetailId) as FieldValue1, Convert(varchar(50), CM.corrigendumId) as FieldValue2, 
   --  corrigendumText as FieldValue3, 
   --  fieldName as FieldValue4, oldValue as FieldValue5, newValue as FieldValue6 
   --FROM tbl_CorrigendumDetail CD INNER JOIN tbl_CorrigendumMaster CM ON CD.corrigendumId=CM.corrigendumId
   --WHERE tenderId=@v_fieldName2Vc
   
   If @v_fieldName3Vc='Officer'
   Begin
    select Convert(varchar(50), corrigendumId) as FieldValue1, corrigendumStatus as FieldValue2, corrigendumText as FieldValue3
	from tbl_CorrigendumMaster Where corrigendumStatus in ('Pending', 'Approved') And tenderId=@v_fieldName2Vc
	 
   End
   Else  If @v_fieldName3Vc='Tenderer'
   Begin
    select Convert(varchar(50), corrigendumId) as FieldValue1, corrigendumStatus as FieldValue2, corrigendumText as FieldValue3
	from tbl_CorrigendumMaster Where corrigendumStatus in ('Approved') And tenderId=@v_fieldName2Vc
   End
   Else  If @v_fieldName3Vc='Visiter'
   Begin
    select Convert(varchar(50), corrigendumId) as FieldValue1, corrigendumStatus as FieldValue2, corrigendumText as FieldValue3
	from tbl_CorrigendumMaster Where corrigendumStatus in ('Approved') And tenderId=@v_fieldName2Vc
   End
   Else If @v_fieldName3Vc='masterInfo'
   Begin
    select Convert(varchar(50), corrigendumId) as FieldValue1, corrigendumStatus as FieldValue2, corrigendumText as FieldValue3
	from tbl_CorrigendumMaster Where tenderId=@v_fieldName2Vc
	Order by corrigendumId desc
   End
   Else If @v_fieldName3Vc='detailInfo'
   Begin    
     Select fieldName as FieldValue1, oldValue as FieldValue2, newValue as FieldValue3
     from tbl_CorrigendumDetail Where corrigendumId=@v_fieldName2Vc
   End
   Else If @v_fieldName3Vc='docInfo'
   Begin
		Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(corrDocId  as varchar(10)) as FieldValue4 
		from tbl_CorrigendumDocs Where corrigendumId=@v_fieldName2Vc
   End
   Else If @v_fieldName3Vc='PubCorriInfo'
   Begin
		select Convert(varchar(50),corrigendumId) as FieldValue1,Convert(varchar(50),corrigendumStatus) as FieldValue2,Convert(varchar(5000),corrigendumText) as FieldValue3
		from tbl_CorrigendumMaster where corrigendumStatus like 'Approved' 
		and tenderId = @v_fieldName2Vc order by corrigendumId desc
   End
  
   	

END
  If @v_fieldName1Vc='EngEstDocInfo'
   Begin
		Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(tenderEstDocId  as varchar(10)) as FieldValue4 
		from tbl_TenderEngEstDocs Where tenderId=@v_fieldName2Vc
   End
    If @v_fieldName1Vc='contractSignDoc'
   Begin
		Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(contractSignDocId  as varchar(10)) as FieldValue4 
		from tbl_ContractSignDocs Where noaId=@v_fieldName2Vc
   End
     If @v_fieldName1Vc='PostQualEstDocInfo'
   Begin
		Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(postQualDocId  as varchar(10)) as FieldValue4 
		from tbl_PostQualificationDocs Where postQualId=@v_fieldName2Vc
   End
   If @v_fieldName1Vc='TenderSectionDocInfo'
   Begin
		Select docName as FieldValue1, [description] as FieldValue2, docSize as FieldValue3,cast(tenderSectionDocId  as varchar(10)) as FieldValue4,
		cast(templateSectionDocId  as varchar(10)) as FieldValue5,status as FieldValue6  
		from tbl_TenderSectionDocs Where tenderSectionId=@v_fieldName2Vc
   End	
  If @v_fieldName1Vc='TosDocInfo'
   Begin
		Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(tosDocId  as varchar(10)) as FieldValue4 
		from tbl_TosDocs Where tenderId=@v_fieldName2Vc
   End
   	   	
IF @v_fieldName1Vc = 'tenderinfobar' -- for ConfigureKeyInfo.jsp
BEGIN
	--Select convert(varchar(20),tenderid) as FieldValue1, reoiRfpRefNo as FieldValue2,isnull(convert(varchar(15),openingDt,103),'N.A.') as FieldValue3, isnull(convert(varchar(15),openingDt,103),'N.A.') as FieldValue4, peOfficeName as FieldValue5,
	-- tenderBrief as FieldValue6,docAvlMethod  as FieldValue7,packageDescription  as FieldValue8 from tbl_TenderDetails
	--where tenderId = @v_fieldName2Vc
	
	--Select convert(varchar(20),tenderid) as FieldValue1, reoiRfpRefNo as FieldValue2,
	--isnull(convert(varchar(15),	openingDt,103) + ' ' +Substring(CONVERT(VARCHAR(5),openingDt,108),1,5) ,'N.A.') as FieldValue3, 
	--isnull(convert(varchar(15), submissionDt,103) + ' ' +Substring(CONVERT(VARCHAR(5),submissionDt,108),1,5),'N.A.') as FieldValue4, 
	--peOfficeName as FieldValue5,
	-- tenderBrief as FieldValue6,docAvlMethod  as FieldValue7,packageDescription  as FieldValue8 from tbl_TenderDetails
	--where tenderId = @v_fieldName2Vc
	
	Select distinct convert(varchar(20),TD.tenderid) as FieldValue1, reoiRfpRefNo as FieldValue2,
	isnull(REPLACE(CONVERT(VARCHAR(11),	TD.openingDt, 106), ' ', '-')  + ' ' +Substring(CONVERT(VARCHAR(5),TD.openingDt,108),1,5) ,'N.A.') as FieldValue3, 
	isnull(REPLACE(CONVERT(VARCHAR(11), submissionDt, 106), ' ', '-')  + ' ' +Substring(CONVERT(VARCHAR(5),submissionDt,108),1,5),'N.A.') as FieldValue4, 
	peOfficeName as FieldValue5,
	 tenderBrief as FieldValue6,docAvlMethod  as FieldValue7,packageDescription  as FieldValue8,
	 isnull(REPLACE(CONVERT(VARCHAR(11),TOD.openingDt, 106), ' ', '-')  + ' ' +Substring(CONVERT(VARCHAR(5),TOD.openingDt,108),1,5) ,'N.A.') as FieldValue9,
	 tenderStatus as FieldValue10
	 from tbl_TenderDetails td Left Join tbl_TenderOpenDates TOD On td.tenderId=TOD.tenderId
	where td.tenderId = @v_fieldName2Vc
	
End

IF @v_fieldName1Vc = 'getCorrigenduminfo' -- for Amendment.jsp
BEGIN
	SELECT convert(varchar(20),corrigendumId) as FieldValue1
      ,convert(varchar(20),tenderId) as FieldValue2
      ,corrigendumText as FieldValue3
      ,corrigendumStatus as FieldValue4
      ,workFlowStatus as FieldValue5
      ,'' as FieldValue6 
  FROM tbl_CorrigendumMaster where tenderId=@v_fieldName2Vc and corrigendumStatus='Pending'
End

IF @v_fieldName1Vc = 'getLocationCombo' -- for Marquee.jsp
BEGIN
	SELECT convert(varchar(20),locationid) as FieldValue1
      ,locationName as FieldValue2     
  FROM tbl_locationMaster
 
End

IF @v_fieldName1Vc = 'getprojectcodeCombo' -- for Marquee.jsp
begin
  SELECT convert(varchar(20),usertypeid) as FieldValue1
      ,userType as FieldValue2    
  FROM tbl_UserTypeMaster
  end
  
  --exec [p_get_tendercommondata] 'getmarqueedisplay','Msg Box','1'
  IF @v_fieldName1Vc = 'getmarqueedisplay' -- for MarqueeDisplay.jsp
begin
Declare @v_marqTxt varchar(max)

  SELECT   @v_marqTxt =  COALESCE(@v_marqTxt+'  |  ', ' ') + convert(VARCHAR(max),marqueeText) 
	FROM   tbl_LocationMaster INNER JOIN
           tbl_MarqueeMaster ON tbl_LocationMaster.locationId = tbl_MarqueeMaster.locationId INNER JOIN tbl_UserTypeMaster ON tbl_MarqueeMaster.userTypeId = tbl_UserTypeMaster.userTypeId 
  WHERE tbl_LocationMaster.locationName = @v_fieldName2Vc and tbl_MarqueeMaster.userTypeId = @v_fieldName3Vc and cast(floor(cast( GETDATE() as float))as datetime) between cast(floor(cast( tbl_MarqueeMaster.marqueeStartDt as float))as datetime) and cast(floor(cast( tbl_MarqueeMaster.marqueeEndDt as float))as datetime) Order by marqueeId desc 
Select case when @v_marqTxt = null then '' else isnull(@v_marqTxt,'') end as FieldValue1

  End
  
   IF @v_fieldName1Vc = 'getmarqueebyID' -- for Marquee.jsp
begin
    SELECT convert(varchar(20),tm.marqueeId) as FieldValue1, tm.marqueeText as FieldValue2,
    cast(convert(varchar, marqueeStartDt, 103) as varchar(10)) +' ' +Substring(CONVERT(VARCHAR(20),marqueeStartDt,108),1,5) as FieldValue3,
    cast(convert(varchar, marqueeEndDt, 103) as varchar(10)) +' ' +Substring(CONVERT(VARCHAR(20),marqueeEndDt,108),1,5) as FieldValue4,
    convert(varchar(20),tm.locationId) as FieldValue5 
    ,convert(varchar(20),userTypeId) as FieldValue6 
  FROM tbl_MarqueeMaster tm Where tm.marqueeId=@v_fieldName2Vc 
  end 
  
   IF @v_fieldName1Vc = 'getnewslisting' -- for admin/NewsManagement.jsp
begin
	IF @v_fieldName3Vc='archive' 
	Begin
		Select CONVERT(Varchar(50),newsId) as FieldValue1, newsBrief as FieldValue2, locationName as FieldValue3
			From dbo.tbl_NewsMaster NM Inner Join tbl_LocationMaster LM On NM.locationId=LM.locationId
			Where isArchive='yes' And isDeleted='no'  
			Order by newsId desc
	End
	Else
	Begin
		Select CONVERT(Varchar(50),newsId) as FieldValue1, newsBrief as FieldValue2, locationName as FieldValue3
			From dbo.tbl_NewsMaster NM Inner Join tbl_LocationMaster LM On NM.locationId=LM.locationId  
			Where isArchive='no' And isDeleted='no'  
			Order by newsId desc
	End

  --Select CONVERT(Varchar(50),newsId) as FieldValue1, newsBrief as FieldValue2, locationName as FieldValue3
  --From dbo.tbl_NewsMaster NM Inner Join tbl_LocationMaster LM On NM.locationId=LM.locationId
  ----Where newsType=@v_fieldName2Vc // Condition commented
  --Order by newsId desc
  --SELECT convert(varchar(20),tm.marqueeId) as FieldValue1, tm.marqueeText as FieldValue2,marqueeStartDt as FieldValue3,marqueeEndDt as FieldValue4,
  -- tm.locationId as FieldValue5 
  --  ,userTypeId as FieldValue6 
  --FROM tbl_MarqueeMaster tm Where tm.marqueeId=@v_fieldName2Vc
  end 

IF @v_fieldName1Vc = 'getnewsbyid'     -- For News.jsp
BEGIN

	IF @v_fieldName2Vc <> '0'
	BEGIN
		
		SELECT CONVERT(VARCHAR(50), tn.newsId) as FieldValue1, CONVERT(VARCHAR(50), tn.locationId) as FieldValue2,
		tn.isImp as FieldValue3, tn.newsBrief as FieldValue4, tn.newsDetails as FieldValue5 
		FROM  dbo.tbl_NewsMaster tn 
		WHERE tn.newsId = @v_fieldName2Vc  -- and tn.newsType = @v_fieldName3Vc
	
	END
	ELSE
	BEGIN
		
		SELECT '0' as FieldValue1, '1' as FieldValue2,
		'No' as FieldValue3, ' ' as FieldValue4, ' ' as FieldValue5 
		
	END

END

IF @v_fieldName1Vc = 'gettenderlotbytenderid'     -- For LotTendPrep.jsp
BEGIN

	

	SELECT @v_docAvlMethod_Vc = docAvlMethod FROM dbo.tbl_TenderDetails WHERE tenderId = @v_fieldName2Vc

	IF @v_docAvlMethod_Vc = 'Lot'
	BEGIN
		
		SELECT lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3 
		FROM  dbo.tbl_TenderLotSecurity 
		WHERE tenderId = @v_fieldName2Vc  
	
	END
	ELSE
	BEGIN
		
		SELECT 'Package' as FieldValue1 
		
	END

END

IF @v_fieldName1Vc = 'getlotorpackagebytenderid'     -- For tenderer/BidPreperation.jsp
BEGIN
		
	IF LEFT(@v_fieldName3Vc, CHARINDEX(',', @v_fieldName3Vc) - 1) = 'Lot'
	BEGIN
		
		SELECT lotNo as FieldValue1, lotDesc as FieldValue2 
		FROM  dbo.tbl_TenderLotSecurity 
		WHERE tenderId = @v_fieldName2Vc AND appPkgLotId = SUBSTRING(@v_fieldName3Vc, 
																									CHARINDEX(',', @v_fieldName3Vc) + 1, 
																									LEN(@v_fieldName3Vc) - CHARINDEX(',', @v_fieldName3Vc))
	
	END
	ELSE
	BEGIN
		
		SELECT packageNo as FieldValue1, packageDescription as FieldValue2,'0' as FieldValue3 FROM 
			dbo.tbl_TenderDetails WHERE tenderId = @v_fieldName2Vc
		
	END
		
END

IF @v_fieldName1Vc = 'GetFormNameByTenderIdForResultShare'     -- For TenResultSharing.jsp
BEGIN
		
	IF @v_fieldName3Vc <> '0'
	BEGIN
	
		SELECT DISTINCT formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
		CASE WHEN EXISTS (SELECT tbpd.tenderFormId FROM tbl_TenderTables tbpd inner join tbl_TenderBidTable tbi ON tbpd.tenderTableId=tbi.tenderTableId Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
		ISNULL(tr.reportType, '') as FieldValue7, 
		ISNULL(trc.reportType, '') as FieldValue8
		FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId Left Outer Join  
		(SELECT tenderId, formId, reportType from dbo.tbl_TenderResultSharing where tenderid=@v_fieldName2Vc and reportType='Individual') tr 
		On td.tenderId = tr.tenderId And tf.tenderFormId = tr.formId 
		 Left Outer Join 
		(SELECT tenderId, formId, reportType from dbo.tbl_TenderResultSharing where tenderid=@v_fieldName2Vc and reportType='Comparative') trc 
		On td.tenderId = trc.tenderId And tf.tenderFormId = trc.formId 
		Where td.tenderId = @v_fieldName2Vc and packageLotId = @v_fieldName3Vc
		Order By isPriceBid
		
	END
	ELSE
	BEGIN
	
	SELECT distinct formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
		Case When Exists (select tbpd.tenderFormId from tbl_TenderTables tbpd inner join tbl_TenderBidTable tbi ON tbpd.tenderTableId=tbi.tenderTableId Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
		ISNULL(tr.reportType, '') as FieldValue7, 
		ISNULL(trc.reportType, '') as FieldValue8 
		FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId Left Outer Join 
		(select tenderId, formId, reportType from dbo.tbl_TenderResultSharing where tenderid=@v_fieldName2Vc and reportType='Individual') tr 
		On td.tenderId = tr.tenderId And tf.tenderFormId = tr.formId 
		 Left Outer Join 
		(select tenderId, formId, reportType from dbo.tbl_TenderResultSharing where tenderid=@v_fieldName2Vc and reportType='Comparative') trc 
		On td.tenderId = trc.tenderId And tf.tenderFormId = trc.formId 
		Where td.tenderId = @v_fieldName2Vc
		Order By isPriceBid
	
	END
		
END

IF @v_fieldName1Vc = 'getformnamebytenderidandlotid'     -- For tenderer/BidPreperation.jsp
BEGIN
		
	IF @v_fieldName3Vc <> '0'
	BEGIN
	
		SELECT distinct formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
		Case When Exists (select tbpd.tenderFormId from tbl_TenderTables tbpd inner join tbl_TenderBidTable tbi ON tbpd.tenderTableId=tbi.tenderTableId Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
		ISNULL(tr.reportType, '') as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9,
		(select Case When (openingDt is null Or openingDt='') then 'N.A.' When openingDt > getdate() Then 'no' Else 'yes' End from tbl_TenderDetails Where tenderId=@v_fieldName2Vc) as  FieldValue10, 
		ISNULL(trc.reportType, '') as FieldValue11
		FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId Left Outer Join  
		(select tenderId, formId, reportType from dbo.tbl_TenderResultSharing where tenderid=@v_fieldName2Vc and reportType='Individual') tr 
		On td.tenderId = tr.tenderId And tf.tenderFormId = tr.formId 
		 Left Outer Join 
		(select tenderId, formId, reportType from dbo.tbl_TenderResultSharing where tenderid=@v_fieldName2Vc and reportType='Comparative') trc 
		On td.tenderId = trc.tenderId And tf.tenderFormId = trc.formId 
		Where td.tenderId = @v_fieldName2Vc and packageLotId = @v_fieldName3Vc
		Order By isPriceBid
	
		--SELECT formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
		--Case When Exists (select tbpd.tenderFormId from tbl_TenderTables tbpd inner join tbl_TenderBidTable tbi ON tbpd.tenderTableId=tbi.tenderTableId Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
		--ISNULL(tr.reportType, '') as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9,
		--(select Case When (openingDt is null Or openingDt='') then 'N.A.' When openingDt > getdate() Then 'no' Else 'yes' End from tbl_TenderDetails Where tenderId=@v_fieldName2Vc) as  FieldValue10
		--FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		--ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		--dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId Left Outer Join 
		--dbo.tbl_TenderResultSharing tr On td.tenderId = tr.tenderId And tf.tenderFormId = tr.formId 
		--Where td.tenderId = @v_fieldName2Vc and packageLotId = @v_fieldName3Vc
		--Order By isPriceBid
	
		--SELECT formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
		--Case When Exists (select tbpd.tenderFormId from tbl_TenderBidPlainData tbpd Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
		--ISNULL(tr.reportType, '') as FieldValue7, isEncryption as FieldValue8
		--FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		--ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		--dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId Left Outer Join 
		--dbo.tbl_TenderResultSharing tr On td.tenderId = tr.tenderId And tf.tenderFormId = tr.formId 
		--Where td.tenderId = @v_fieldName2Vc and packageLotId = @v_fieldName3Vc
		--Order By isPriceBid
		
	END
	ELSE
	BEGIN
	
	SELECT distinct formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
		Case When Exists (select tbpd.tenderFormId from tbl_TenderTables tbpd inner join tbl_TenderBidTable tbi ON tbpd.tenderTableId=tbi.tenderTableId Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
		ISNULL(tr.reportType, '') as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9,
		(select Case When (openingDt is null Or openingDt='') then 'N.A.' When openingDt > getdate() Then 'no' Else 'yes' End from tbl_TenderDetails Where tenderId=@v_fieldName2Vc) as  FieldValue10, 
		ISNULL(trc.reportType, '') as FieldValue11
		FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId Left Outer Join 
		(select tenderId, formId, reportType from dbo.tbl_TenderResultSharing where tenderid=@v_fieldName2Vc and reportType='Individual') tr 
		On td.tenderId = tr.tenderId And tf.tenderFormId = tr.formId 
		 Left Outer Join 
		(select tenderId, formId, reportType from dbo.tbl_TenderResultSharing where tenderid=@v_fieldName2Vc and reportType='Comparative') trc 
		On td.tenderId = trc.tenderId And tf.tenderFormId = trc.formId 
		Where td.tenderId = @v_fieldName2Vc
		Order By isPriceBid
	
	--SELECT distinct formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
	--	Case When Exists (select tbpd.tenderFormId from tbl_TenderTables tbpd inner join tbl_TenderBidTable tbi ON tbpd.tenderTableId=tbi.tenderTableId Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
	--	ISNULL(tr.reportType, '') as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9,
	--	(select Case When (openingDt is null Or openingDt='') then 'N.A.' When openingDt > getdate() Then 'no' Else 'yes' End from tbl_TenderDetails Where tenderId=@v_fieldName2Vc) as  FieldValue10
	--	FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
	--	ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
	--	dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId Left Outer Join 
	--	dbo.tbl_TenderResultSharing tr On td.tenderId = tr.tenderId And tf.tenderFormId = tr.formId 
	--	Where td.tenderId = @v_fieldName2Vc
	--	Order By isPriceBid
	
		--SELECT formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
		--Case When Exists (select tbpd.tenderFormId from tbl_TenderTables tbpd inner join tbl_TenderBidTable tbi ON tbpd.tenderTableId=tbi.tenderTableId Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
		--ISNULL(tr.reportType, '') as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9,
		--(select Case When (openingDt is null Or openingDt='') then 'N.A.' When openingDt > getdate() Then 'no' Else 'yes' End from tbl_TenderDetails Where tenderId=@v_fieldName2Vc) as  FieldValue10
		--FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		--ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		--dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId Left Outer Join 
		--dbo.tbl_TenderResultSharing tr On td.tenderId = tr.tenderId And tf.tenderFormId = tr.formId 
		--Where td.tenderId = @v_fieldName2Vc
		--Order By isPriceBid
	
	
		--SELECT formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
		--Case When Exists (select tbpd.tenderFormId from tbl_TenderBidPlainData tbpd Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
		--ISNULL(tr.reportType, '') as FieldValue7, isEncryption as FieldValue8 
		--FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		--ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		--dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId Left Outer Join 
		--dbo.tbl_TenderResultSharing tr On td.tenderId = tr.tenderId And tf.tenderFormId = tr.formId 
		--Where td.tenderId = @v_fieldName2Vc
		--Order By isPriceBid
	
	END
		
END

IF @v_fieldName1Vc = 'getAnnouncements' -- for MarqueeDisplay.jsp
	begin
		SELECT CONVERT(VARCHAR(50),newsId) as FieldValue1
			  ,CONVERT(VARCHAR(50),[locationId]) as FieldValue2
			  ,[newsType] as FieldValue3
			  ,[isImp] as FieldValue4
			  ,[newsBrief] as FieldValue5
			  ,[newsDetails] as FieldValue6
		  FROM tbl_NewsMaster 
		  Where isArchive='no' And isDeleted='no'
		  order by newsId desc
  
	End
  
IF @v_fieldName1Vc = 'getNews' -- for MarqueeDisplay.jsp
	begin
		SELECT CONVERT(VARCHAR(50),newsId) as FieldValue1
			  ,CONVERT(VARCHAR(50),[locationId]) as FieldValue2
			  ,[newsType] as FieldValue3
			  ,[isImp] as FieldValue4
			  ,[newsBrief] as FieldValue5
			  ,[newsDetails] as FieldValue6
		  FROM tbl_NewsMaster  order by newsId desc
  
	End  
  
END

IF @v_fieldName1Vc = 'getTenderCurrency' -- for tenderer/Declaration.jsp
BEGIN
  Select CONVERT(Varchar(50),tenderCurId) as FieldValue1 ,CONVERT(Varchar(50),tenderId) as FieldValue2, 
  CONVERT(Varchar(50),CM.currencyId) as FieldValue3, currencyName as FieldValue4 
	from tbl_tendercurrency TC 
	inner join tbl_CurrencyMaster CM On TC.currencyId=CM.currencyId
	Where tenderId=@v_fieldName2Vc
  
END

IF @v_fieldName1Vc = 'getCommListing' -- for  /officer/CommListing.jsp
BEGIN
	IF @v_fieldName2Vc='TSC'
	Begin
		select Convert(varchar(50), t.tenderid) as FieldValue1,
		Convert(varchar(50), c.committeeId) as FieldValue2 ,
		tenderBrief as FieldValue3,
		isnull(REPLACE(CONVERT(VARCHAR(11),openingDt, 106), ' ', '-') +' ' +Substring(CONVERT(VARCHAR(5),openingDt,108),1,5),'N.A.') as FieldValue4,
		packageNo as FieldValue5,
		agency as FieldValue6,
		peOfficeName as FieldValue7,
		Case When Exists (Select tenderId from tbl_TOSListing where tenderId=c.tenderId) Then 'yes' Else 'no' End as  FieldValue8 
		from tbl_CommitteeMembers cm Inner Join	tbl_Committee c On cm.committeeId =c.committeeId
		Inner Join tbl_TenderDetails t On c.tenderid=t.tenderid
		where c.committeeId=cm.committeeId 
		and committeeType=@v_fieldName2Vc
		and userId=@v_fieldName3Vc 
		Order by t.tenderId desc
	End
	ELSE IF @v_fieldName2Vc='TEC'
	Begin
			select Convert(varchar(50), t.tenderid) as FieldValue1,
		Convert(varchar(50), c.committeeId) as FieldValue2 ,
		tenderBrief as FieldValue3,
		isnull(REPLACE(CONVERT(VARCHAR(11),openingDt, 106), ' ', '-') +' ' +Substring(CONVERT(VARCHAR(5),openingDt,108),1,5),'N.A.') as FieldValue4,
		packageNo as FieldValue5,
		agency as FieldValue6,
		peOfficeName as FieldValue7,
		Case When Exists (Select tenderId from tbl_TOSListing where tenderId=c.tenderId) Then 'yes' Else 'no' End as  FieldValue8
		from tbl_CommitteeMembers cm Inner Join	tbl_Committee c On cm.committeeId =c.committeeId
		Inner Join tbl_TenderDetails t On c.tenderid=t.tenderid
		where c.committeeId=cm.committeeId 
		and committeeType in ('TEC','PEC')
		and userId=@v_fieldName3Vc 
		Order by t.tenderId desc
	End
	ELSE IF @v_fieldName2Vc='TOC'
	Begin
		select Convert(varchar(50), t.tenderid) as FieldValue1,
		Convert(varchar(50), c.committeeId) as FieldValue2 ,
		tenderBrief as FieldValue3,
		isnull(REPLACE(CONVERT(VARCHAR(11),openingDt, 106), ' ', '-') +' ' +Substring(CONVERT(VARCHAR(5),openingDt,108),1,5),'N.A.') as FieldValue4,
		packageNo as FieldValue5,
		agency as FieldValue6,
		peOfficeName as FieldValue7,
		Case When Exists (Select tenderId from tbl_TOSListing where tenderId=c.tenderId) Then 'yes' Else 'no' End as  FieldValue8 
		from tbl_CommitteeMembers cm Inner Join	tbl_Committee c On cm.committeeId =c.committeeId
		Inner Join tbl_TenderDetails t On c.tenderid=t.tenderid
		where c.committeeId=cm.committeeId 
		and committeeType in ('TOC','POC')
		and userId=@v_fieldName3Vc 
		Order by t.tenderId desc
	End

	
END


IF @v_fieldName1Vc = 'getTenderPackageByTenderId' -- for  /TOS.jsp
BEGIN
	select Convert(varchar(50), tenderDtlId ) as FieldValue1,
	Convert(varchar(50), packageNo) as FieldValue2 ,
	packageDescription as FieldValue3
	from tbl_TenderDetails
	where tenderDtlId=@v_fieldName2Vc
END

IF @v_fieldName1Vc = 'getTenderLotSecurityByTenderId' -- for  /TOS.jsp
BEGIN
	--select Convert(varchar(50), lotNo ) as FieldValue1,
	--lotDesc  as FieldValue2,
	--Convert(varchar(50), tenderSecurityAmt ) as FieldValue3,
	--cast(procurementNatureId as varchar(10)) as FieldValue4
	--from tbl_TenderLotSecurity inner join tbl_TenderDetails 
	--on tbl_TenderLotSecurity.tenderId=tbl_TenderDetails.tenderId
	--where tbl_TenderLotSecurity.tenderId=@v_fieldName2Vc
	select Convert(varchar(50), lotNo ) as FieldValue1,
	lotDesc  as FieldValue2,
	Convert(varchar(50), tenderSecurityAmt ) as FieldValue3,
	cast(procurementNatureId as varchar(10)) as FieldValue4,
	(select Convert(varchar(50), sum(tenderSecurityAmt)) 
	from tbl_TenderLotSecurity inner join tbl_TenderDetails 
	on tbl_TenderLotSecurity.tenderId=tbl_TenderDetails.tenderId
	where tbl_TenderLotSecurity.tenderId=@v_fieldName2Vc) as FieldValue5
	from tbl_TenderLotSecurity inner join tbl_TenderDetails 
	on tbl_TenderLotSecurity.tenderId=tbl_TenderDetails.tenderId
	where tbl_TenderLotSecurity.tenderId=@v_fieldName2Vc
END


IF @v_fieldName1Vc = 'getEmployeeinfo' -- for  /TOS.jsp
BEGIN
	  select tbl_EmployeeMaster.employeeName as FieldValue1, tbl_DesignationMaster.designationName as FieldValue2, case tbl_CommitteeMembers.memberRole when 'm' then 'Member' when 'cp' then 'Chair Person' else 'Member Secretary' end as FieldValue3, 
        REPLACE(CONVERT(VARCHAR(11), tbl_CommitteeMembers.appDate, 106), ' ', '-')  + ' ' +Substring(CONVERT(VARCHAR(5),tbl_CommitteeMembers.appdate,108),1,5) as FieldValue4, tbl_CommitteeMembers.appStatus as FieldValue5 
		from	tbl_Committee,tbl_CommitteeMembers,tbl_EmployeeMaster,tbl_EmployeeOffices,
		tbl_DesignationMaster
		where  tbl_Committee.committeeId=tbl_CommitteeMembers.committeeId  and tbl_CommitteeMembers.userId=tbl_EmployeeMaster.userid
		and tbl_EmployeeMaster.employeeId=tbl_EmployeeOffices.employeeId and tbl_EmployeeOffices.designationId=tbl_DesignationMaster.designationId 
		 and  committeeType in ('TOC','POC')
		 and tenderId=@v_fieldName2Vc
END


IF @v_fieldName1Vc = 'getTenderNamebyTenderId' -- for  /TOS.jsp
BEGIN

	select distinct FieldValue1,FieldValue2,FieldValue3,FieldValue4 from (SELECT   cast(tbl_FinalSubmission.tenderId as varchar(20)) as FieldValue1,firstName as FieldValue2,cast(tbl_LoginMaster.userId as varchar(10)) as FieldValue3
	,(select companyname from tbl_CompanyMaster where tbl_CompanyMaster.companyId=tbl_TendererMaster.companyId) as FieldValue4 
	FROM tbl_FinalSubmission INNER JOIN
         tbl_LoginMaster ON tbl_FinalSubmission.userId = tbl_LoginMaster.userId INNER JOIN
         tbl_TendererMaster ON tbl_LoginMaster.userId = tbl_TendererMaster.userId 
         AND tbl_LoginMaster.userId = tbl_TendererMaster.userId where 
         tbl_FinalSubmission.tenderId=@v_fieldName2Vc and tbl_FinalSubmission.bidSubStatus='finalsubmission') as a left outer join
         (
			select tbl_BidModification.tenderId ,tbl_BidModification.bidStatus from tbl_BidModification where tbl_BidModification.tenderId=@v_fieldName2Vc and tbl_BidModification.bidStatus!='withdrawal'
         ) as b ON a.FieldValue1=b.tenderId
         
	--SELECT   cast(tbl_FinalSubmission.tenderId as varchar(20)) as FieldValue1,firstName as FieldValue2,cast(tbl_LoginMaster.userId as varchar(10)) as FieldValue3
	--,(select companyname from tbl_CompanyMaster where tbl_CompanyMaster.companyId=tbl_TendererMaster.companyId) as FieldValue4 
	--FROM tbl_FinalSubmission INNER JOIN
 --        tbl_LoginMaster ON tbl_FinalSubmission.userId = tbl_LoginMaster.userId INNER JOIN
 --        tbl_TendererMaster ON tbl_LoginMaster.userId = tbl_TendererMaster.userId 
 --        AND tbl_LoginMaster.userId = tbl_TendererMaster.userId where 
 --        tbl_FinalSubmission.tenderId=@v_fieldName2Vc and tbl_FinalSubmission.bidSubStatus='finalsubmission'
END


IF @v_fieldName1Vc = 'getBidModificationbyTenderId' -- for  /TOS.jsp
BEGIN
	SELECT  tbl_TendererMaster.firstname as FieldValue1,case tbl_BidModification.bidStatus when 'modify' then 'Modification' else 'Withdrawal' end as FieldValue2,tbl_BidModification.comments as FieldValue3,REPLACE(CONVERT(VARCHAR(11), tbl_BidModification.bidModDt, 106), ' ', '-')  + ' ' +Substring(CONVERT(VARCHAR(5),tbl_BidModification.bidModDt,108),1,5) as FieldValue4
	FROM tbl_bidmodification INNER JOIN
         tbl_LoginMaster ON tbl_bidmodification.userId = tbl_LoginMaster.userId INNER JOIN
         tbl_TendererMaster ON tbl_LoginMaster.userId = tbl_TendererMaster.userId 
         AND tbl_LoginMaster.userId = tbl_TendererMaster.userId 
         where tbl_bidmodification.tenderId=@v_fieldName2Vc
END

IF @v_fieldName1Vc = 'getTenderAdDetail' -- for  officer/TenderAdv.jsp
BEGIN	
	
	SELECT convert(varchar(50),tenderAdvtId) as FieldValue1, 
	newsPaper as FieldValue2, 
	isnull(REPLACE(CONVERT(VARCHAR(11),newsPaperPubDt, 106), ' ', '-') ,'N.A.') as FieldValue3, 
	webSite as FieldValue4, 
	isnull(REPLACE(CONVERT(VARCHAR(11),webSiteAdvtDt, 106), ' ', '-') ,'N.A.') as FieldValue5,
	convert(varchar(50),tenderId) as FieldValue6,
	Replace(convert(varchar(50),creationDate,102),'.','-')+' '+convert(varchar, creationDate, 8) as  FieldValue7
	from 
	tbl_TenderAdvertisement
	where tenderAdvtId=@v_fieldName2Vc
	
END

IF @v_fieldName1Vc = 'getTenderAdDetail_ForEdit' -- for  officer/TenderAdv.jsp
BEGIN	
	
	SELECT convert(varchar(50),tenderAdvtId) as FieldValue1, 
	newsPaper as FieldValue2, 
	isnull(REPLACE(CONVERT(VARCHAR(11),newsPaperPubDt, 103), ' ', '-') ,'N.A.') as FieldValue3, 
	webSite as FieldValue4, 
	isnull(REPLACE(CONVERT(VARCHAR(11),webSiteAdvtDt, 103), ' ', '-') ,'N.A.') as FieldValue5,
	convert(varchar(50),tenderId) as FieldValue6,
	Replace(convert(varchar(50),creationDate,102),'.','-')+' '+convert(varchar, creationDate, 8) as  FieldValue7
	from 
	tbl_TenderAdvertisement
	where tenderAdvtId=@v_fieldName2Vc
	
END


IF @v_fieldName1Vc = 'getTOSSigningInfo' -- for  /TOS.jsp
BEGIN
		 select a.employeeName AS FieldValue1, a.designationName AS FieldValue2, 
                cast(a.memberRole as varchar(50)) AS FieldValue3, CAST(a.createdDate AS varchar(50)) AS FieldValue4, 
                isnull(b.tosStatus,'Pending') AS FieldValue5,cast(a.committeeId as varchar(10)) as FieldValue6
                , isnull(b.comments,'') as FieldValue7,
                isnull(REPLACE(CONVERT(VARCHAR(11), b.signedDate, 106), ' ', '-') + ' ' + convert(varchar(5), b.signedDate, 108),'') as FieldValue8, 
                cast(a.userId as varchar(10)) as FieldValue9                
                from (SELECT employeeName ,designationName,tbl_CommitteeMembers.userId, 
                memberRole,tbl_CommitteeMembers.createdDate, 
                committeStatus,tbl_CommitteeMembers.committeeId
		 FROM tbl_Committee INNER JOIN
             tbl_CommitteeMembers ON tbl_Committee.committeeId = tbl_CommitteeMembers.committeeId INNER JOIN
             tbl_EmployeeMaster ON tbl_CommitteeMembers.userId = tbl_EmployeeMaster.userId INNER JOIN
             tbl_EmployeeOffices ON tbl_EmployeeMaster.employeeId = tbl_EmployeeOffices.employeeId INNER JOIN
             tbl_DesignationMaster ON tbl_EmployeeOffices.designationId = tbl_DesignationMaster.designationId 
             and tbl_Committee.committeStatus='approved' and  tbl_Committee.committeeType in ('TOC','POC')
			 and tbl_Committee.tenderId=@v_fieldName2Vc) a left outer join 
			 (select * from tbl_TosSheetSign where tenderid=@v_fieldName2Vc) b on a.committeeId=b.committeeId and a.userId=b.userId
END	

IF @v_fieldName1Vc = 'getCommitteeInfo' -- for  /TOS.jsp
BEGIN
			 SELECT tbl_LoginMaster.emailId as FieldValue1,
			 cast(tbl_LoginMaster.userId as varchar(10)) as FieldValue2,
					tbl_LoginMaster.password as FieldValue3,
					cast (tbl_Committee.tenderId as varchar(10)) as FieldValue4,
			 tbl_Committee.committeeType as FieldValue5,
			 Case tbl_Committee.committeeType 
					When 'tsc'
					Then dbo.f_gettscmembername(tbl_LoginMaster.userId, tbl_CommitteeMembers.govUserId)
					Else
						dbo.f_GovUserName(tbl_CommitteeMembers.govUserId, 'tbl_EmployeeTrasfer') 
					End as FieldValue6 	
			 FROM	tbl_Committee INNER JOIN
					tbl_CommitteeMembers ON tbl_Committee.committeeId = tbl_CommitteeMembers.committeeId AND 
					tbl_Committee.committeeId = tbl_CommitteeMembers.committeeId INNER JOIN
					tbl_LoginMaster ON tbl_CommitteeMembers.userId = tbl_LoginMaster.userId
             where	tbl_Committee.committeeId=@v_fieldName2Vc and tbl_LoginMaster.userId=@v_fieldName3Vc
END

IF @v_fieldName1Vc = 'getTenderAdvtList' -- for  officer/TenderAdvList.jsp
BEGIN	
	
	SELECT convert(varchar(50),tenderAdvtId) as FieldValue1, 
	newsPaper as FieldValue2, 
	isnull(REPLACE(CONVERT(VARCHAR(11),newsPaperPubDt, 106), ' ', '-') ,'N.A.') as FieldValue3, 
	webSite as FieldValue4, 
	isnull(REPLACE(CONVERT(VARCHAR(11),webSiteAdvtDt, 106), ' ', '-') ,'N.A.') as FieldValue5,
	convert(varchar(50),tenderId) as FieldValue6,
	REPLACE(CONVERT(VARCHAR(11),creationDate, 106), ' ', '-')  as  FieldValue7
	from 
	tbl_TenderAdvertisement
	where tenderId=@v_fieldName2Vc
	
END

IF @v_fieldName1Vc = 'checkLogin' -- for  TOS.jsp
BEGIN	
	
	select * from tbl_LoginMaster 
	where emailId=@v_fieldName2Vc and 
			password=@v_fieldName3Vc and 
			isEmailVerified='yes' and 
			status='Approved' 
			--and validUpTo >=getdate()
END
IF @v_fieldName1Vc = 'GetArchiveDocuments'
BEGIN
declare @v_FormList_Vc3 varchar(2000)
 
 select @v_FormList_Vc3 = COALESCE(@v_FormList_Vc3+',', ' ') + convert(varchar(20), formid) 
 from tbl_biddocuments where userId=@v_fieldName3Vc
 select  
  convert(varchar(50),a.companyDocId ) as FieldValue1,
 convert(varchar(150),documentName) as FieldValue2
      , convert(varchar(150),documentSize) as FieldValue3
      , convert(varchar(250),documentBrief) as FieldValue4
      , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), ' ', '-')   as FieldValue5
      , convert(varchar(50),docstatus) as FieldValue6
      , convert(varchar(50),b.companyDocId ) as FieldValue7
       , case when b.companyDocId is not null then @v_FormList_Vc3 else null end as FieldValue10
       
      from (select c.* from tbl_CompanyDocuments c,tbl_TendererMaster t
where c.tendererId=t.tendererId and userId=@v_fieldName3Vc and docstatus='archive' ) a
left outer join
(select distinct companyDocId  from tbl_biddocuments where  userId=@v_fieldName3Vc) b
on a.companyDocId=b.companydocid 
 
END

IF @v_fieldName1Vc = 'GetAllDocuments'
BEGIN
declare @v_FormList_Vc varchar(2000)
 
 select @v_FormList_Vc = COALESCE(@v_FormList_Vc+',', ' ') + convert(varchar(20), formid) 
 from tbl_biddocuments where userId=@v_fieldName3Vc
  select  
  convert(varchar(50),a.companyDocId ) as FieldValue1,
 convert(varchar(150),documentName) as FieldValue2
      , convert(varchar(150),documentSize) as FieldValue3
      , convert(varchar(250),documentBrief) as FieldValue4
      , REPLACE(CONVERT(VARCHAR(11),uploadedDate, 106), ' ', '-')   as FieldValue5
      , convert(varchar(50),docstatus) as FieldValue6
      , convert(varchar(50),b.companyDocId ) as FieldValue7,convert(varchar(50),a.folderId ) as FieldValue8,
        convert(varchar(50),isnull(c.foldername,'-') ) as FieldValue9,
          case when b.companyDocId is not null then @v_FormList_Vc else null end  as FieldValue10
       
      from (select c.* from tbl_CompanyDocuments c,tbl_TendererMaster t
where c.tendererId=t.tendererId and userId=@v_fieldName3Vc    and docstatus!='archive' ) a
left outer join
(select distinct companyDocId from tbl_biddocuments where userId=@v_fieldName3Vc) b
on a.companyDocId=b.companydocid 
 left outer join
(select distinct t.tendererId,folderName,folderid from tbl_TendererFolderMaster t,tbl_tenderermaster m
 where t.tendererId=m.tendererId and  m.userId=@v_fieldName3Vc ) c
on a.tendererId=c.tendererId and a.folderId=c.folderId
END
IF @v_fieldName1Vc = 'getTenderEmployeeinfo' -- for/officer/LotPackageDetail.jsp
BEGIN
	  --select distinct tbl_EmployeeMaster.employeeName as FieldValue1, 
	  
	  select distinct 
	  dbo.f_GovUserName(tbl_CommitteeMembers.govUserId, 'tbl_EmployeeTrasfer') as FieldValue1, 
	  tbl_DesignationMaster.designationName as FieldValue2, 
	  tbl_CommitteeMembers.memberRole as FieldValue3, 
       Case When tbl_CommitteeMembers.appDate = '1900-01-01 00:00:00' Then '-' 
       Else REPLACE(CONVERT(VARCHAR(11), tbl_CommitteeMembers.appDate, 106), ' ', '-') + ' ' + convert(varchar(5), tbl_CommitteeMembers.appDate, 108) End as FieldValue4, 
       dbo.f_initcap(tbl_CommitteeMembers.appStatus) as FieldValue5,
        convert(varchar(50), tbl_Committee.committeeId)  as FieldValue6,
        convert(varchar(50), tbl_EmployeeMaster.userid) as FieldValue7,
        (select convert(varchar(50),COUNT(comMemberId)) 
			from 
			tbl_Committee,tbl_CommitteeMembers,tbl_EmployeeMaster,tbl_EmployeeOffices,
			tbl_DesignationMaster
			where  tbl_Committee.committeeId=tbl_CommitteeMembers.committeeId  and tbl_CommitteeMembers.userId=tbl_EmployeeMaster.userid
			and tbl_EmployeeMaster.employeeId=tbl_EmployeeOffices.employeeId and tbl_EmployeeOffices.designationId=tbl_DesignationMaster.designationId 
			and committeStatus='approved' and  committeeType in ('TOC','POC')
			 and tenderId=@v_fieldName2Vc and appStatus='approved') as  FieldValue8,          
		(select Case When (openingDt is null Or openingDt='') then 'N.A.' When openingDt > getdate() Then 'no' Else 'yes' End from tbl_TenderDetails Where tenderId=@v_fieldName2Vc) as  FieldValue9,
		tbl_CommitteeRoleMaster.comRoleId          
		 
		FRom 
		tbl_Committee,tbl_CommitteeMembers,tbl_EmployeeMaster,tbl_EmployeeOffices,
		tbl_DesignationMaster, tbl_CommitteeRoleMaster
		where  tbl_Committee.committeeId=tbl_CommitteeMembers.committeeId  
		and tbl_CommitteeMembers.userId=tbl_EmployeeMaster.userid
		and tbl_EmployeeMaster.employeeId=tbl_EmployeeOffices.employeeId 
		and tbl_EmployeeOffices.designationId=tbl_DesignationMaster.designationId
		And tbl_CommitteeMembers.memberRole=tbl_CommitteeRoleMaster.comRole 
		 and  committeStatus='approved' and committeeType in ('TOC','POC')
		 and tenderId=@v_fieldName2Vc
		 Order by tbl_CommitteeRoleMaster.comRoleId
END

IF @v_fieldName1Vc = 'getMinRequirements' -- for/officer/LotPackageDetail.jsp
BEGIN
	select Convert(varchar(50),C.minMembers) as FieldValue1, 
	IsNull(REPLACE(CONVERT(VARCHAR(11), TD.openingDt, 106), ' ', '-') + ' ' + convert(varchar(5), TD.openingDt, 108),'')
	as FieldValue2  
	from tbl_Committee C 
	Inner Join tbl_TenderDetails TD 
	On C.tenderId=TD.tenderId 
	Where committeStatus='approved' and  committeeType in ('TOC','POC') and TD.tenderId=@v_fieldName2Vc
END


IF @v_fieldName1Vc = 'getfrmNamebytenderidandlotid'     -- For tenderer/FinalSubmission.jsp
BEGIN
	IF @v_fieldName3Vc <> '0'
		BEGIN
		
			select cast(a.tenderid as varchar(10)) as FieldValue1, cast(a.tenderformid as varchar(10)) as FieldValue2,a.formName as FieldValue3,cast(a.packageLotid as varchar(10)) as FieldValue4,case when b.tenderFormid is null then 'No' else 'Yes' end as FieldValue5 from (select tenderId,tenderformid,formName,packageLotId from tbl_TenderForms t,tbl_TenderSection ts,
				tbl_TenderStd sd where t.tenderSectionId=ts.tenderSectionId
				and ts.tenderStdId=sd.tenderStdId and tenderId=@v_fieldName2Vc and sd.packageLotId=@v_fieldName3Vc)a
				left outer join (select  tenderformid from tbl_FinalSubDetail b,tbl_FinalSubmission f
				where b.finalSubmissionId=f.finalSubmissionId
				)b
				on a.tenderFormId=b.tenderFormid 
		END
	ELSE
		BEGIN
		
			select cast(a.tenderid as varchar(10)) as FieldValue1, cast(a.tenderformid as varchar(10)) as FieldValue2,a.formName as FieldValue3,cast(a.packageLotid as varchar(10)) as FieldValue4,case when b.tenderFormid is null then 'No' else 'Yes' end as FieldValue5 from (select tenderId,tenderformid,formName,packageLotId from tbl_TenderForms t,tbl_TenderSection ts,
				tbl_TenderStd sd where t.tenderSectionId=ts.tenderSectionId
				and ts.tenderStdId=sd.tenderStdId and tenderId=@v_fieldName2Vc)a
				left outer join (select  tenderformid from tbl_FinalSubDetail b,tbl_FinalSubmission f
				where b.finalSubmissionId=f.finalSubmissionId
				)b
				on a.tenderFormId=b.tenderFormid  
		END
END


IF @v_fieldName1Vc = 'SearchTenPayment' -- for/partner/SearchTenPayment.jsp
BEGIN
	IF @v_fieldName2Vc != '' and @v_fieldName3Vc != ''
	BEGIN
		select (cast(case when tenderId IS null then 'N.A' else tenderId end as varchar(10))+',<br/>'+cast(case when reoiRfpRefNo ='' then 'N.A' else reoiRfpRefNo end as varchar(100))) as FieldValue1,(case when procurementNature  ='' then 'N.A' else procurementNature  end+',<br/>'+case when tenderBrief ='' then 'N.A' else tenderBrief end) as FieldValue2,
		(case when ministry  ='' then 'N.A' else ministry  end +',<br/>'+case when division  ='' then 'N.A' else division  end+',<br/>'+case when agency  ='' then 'N.A' else agency  end+',<br/>'+case when peOfficeName  ='' then 'N.A' else peOfficeName  end) as FieldValue3,(procurementType +',<br/>'+procurementMethod) as FieldValue4,(isnull(cast(REPLACE(CONVERT(VARCHAR(11),tenderPubDt, 106), ' ', '-') as varchar(100)),'N.A') +','+isnull(cast(REPLACE(CONVERT(VARCHAR(11),submissionDt, 106), ' ', '-') as varchar(100)),'N.A')) as FieldValue5, docAvlMethod as FieldValue6
		from tbl_TenderDetails WHERE tenderId=@v_fieldName2Vc and reoiRfpRefNo=@v_fieldName3Vc and tbl_TenderDetails.tenderStatus='Approved'
	END
	ELSE IF (@v_fieldName2Vc = '' or @v_fieldName2Vc is NULL) and @v_fieldName3Vc != ''
	BEGIN
		select (cast(case when tenderId IS null then 'N.A' else tenderId end as varchar(10))+',<br/>'+cast(case when reoiRfpRefNo ='' then 'N.A' else reoiRfpRefNo end as varchar(100))) as FieldValue1,(case when procurementNature  ='' then 'N.A' else procurementNature  end+',<br/>'+case when tenderBrief ='' then 'N.A' else tenderBrief end) as FieldValue2,
		(case when ministry  ='' then 'N.A' else ministry  end +',<br/>'+case when division  ='' then 'N.A' else division  end+',<br/>'+case when agency  ='' then 'N.A' else agency  end+',<br/>'+case when peOfficeName  ='' then 'N.A' else peOfficeName  end) as FieldValue3,(procurementType +',<br/>'+procurementMethod) as FieldValue4,(isnull(cast(REPLACE(CONVERT(VARCHAR(11),tenderPubDt, 106), ' ', '-') as varchar(100)),'N.A') +','+isnull(cast(REPLACE(CONVERT(VARCHAR(11),submissionDt, 106), ' ', '-') as varchar(100)),'N.A')) as FieldValue5,docAvlMethod as FieldValue6
		from tbl_TenderDetails WHERE reoiRfpRefNo=@v_fieldName3Vc  and tbl_TenderDetails.tenderStatus='Approved'
	END
	ELSE IF @v_fieldName2Vc != '' and (@v_fieldName3Vc = '' OR @v_fieldName3Vc is NULL)
	BEGIN
		select (cast(case when tenderId is null then 'N.A' else tenderId end as varchar(10))+',<br/>'+cast(case when reoiRfpRefNo ='' then 'N.A' else reoiRfpRefNo end as varchar(100))) as FieldValue1,(case when procurementNature  ='' then 'N.A' else procurementNature  end+',<br/>'+case when tenderBrief ='' then 'N.A' else tenderBrief end) as FieldValue2,
		(case when ministry  ='' then 'N.A' else ministry  end +',<br/>'+case when division  ='' then 'N.A' else division  end+',<br/>'+case when agency  ='' then 'N.A' else agency  end+',<br/>'+case when peOfficeName  ='' then 'N.A' else peOfficeName  end) as FieldValue3,(procurementType +',<br/>'+procurementMethod) as FieldValue4,(isnull(cast(REPLACE(CONVERT(VARCHAR(11),tenderPubDt, 106), ' ', '-') as varchar(100)),'N.A') +','+isnull(cast(REPLACE(CONVERT(VARCHAR(11),submissionDt, 106), ' ', '-') as varchar(100)),'N.A')) as FieldValue5,docAvlMethod as FieldValue6
		from tbl_TenderDetails WHERE tenderId=@v_fieldName2Vc  and tbl_TenderDetails.tenderStatus='Approved'
	END
	ELSE IF (@v_fieldName2Vc = '' OR @v_fieldName2Vc is NULL) and (@v_fieldName3Vc = '' or @v_fieldName3Vc is NULL)
	BEGIN
		select (cast(case when tenderId is null then 'N.A' else tenderId end as varchar(10))+',<br/>'+cast(case when reoiRfpRefNo ='' then 'N.A' else reoiRfpRefNo end as varchar(100))) as FieldValue1,(case when procurementNature  ='' then 'N.A' else procurementNature  end+',<br/>'+case when tenderBrief ='' then 'N.A' else tenderBrief end) as FieldValue2,
		(case when ministry  ='' then 'N.A' else ministry  end +',<br/>'+case when division  ='' then 'N.A' else division  end+',<br/>'+case when agency  ='' then 'N.A' else agency  end+',<br/>'+case when peOfficeName  ='' then 'N.A' else peOfficeName  end) as FieldValue3,(procurementType +',<br/>'+procurementMethod) as FieldValue4,(isnull(cast(REPLACE(CONVERT(VARCHAR(11),tenderPubDt, 106), ' ', '-') as varchar(100)),'N.A') +','+isnull(cast(REPLACE(CONVERT(VARCHAR(11),submissionDt, 106), ' ', '-') as varchar(100)),'N.A')) as FieldValue5,docAvlMethod as FieldValue6
		from tbl_TenderDetails where tbl_TenderDetails.tenderStatus='Approved'
	END
END

IF @v_fieldName1Vc = 'SearchTenEmail' -- for/partner/SearchTenderer.jsp
BEGIN
	select CONVERT(varchar(50), LM.userId)  as FieldValue1, emailId as FieldValue2, 
	CONVERT(varchar(50), CM.companyId) as FieldValue3, companyName as FieldValue4 
    from tbl_LoginMaster LM 
    inner join tbl_tenderermaster TM on LM.userId=TM.userId
    inner join tbl_CompanyMaster CM on TM.companyId=CM.companyId
    Where emailId=@v_fieldName2Vc
END


IF @v_fieldName1Vc = 'getDocFeesAmt' -- for/partner/MakePayment.jsp
BEGIN
 IF @v_fieldName3Vc IS NOT NULL AND @v_fieldName3Vc<>''
 BEGIN
	SELECT cast(docFess as varchar(50)) as FieldValue1 from tbl_TenderLotSecurity Where tenderId=@v_fieldName2Vc And appPkgLotId=@v_fieldName3Vc
 END
 ELSE
 BEGIN
	SELECT cast(SUM(docFess) as varchar(50)) as FieldValue1 from tbl_TenderLotSecurity Where tenderId=@v_fieldName2Vc
 END
END



IF @v_fieldName1Vc = 'getTenderSecurityAmt' -- for/partner/MakePayment.jsp
BEGIN
 IF @v_fieldName3Vc IS NOT NULL AND @v_fieldName3Vc<>''
 BEGIN
	SELECT cast(tenderSecurityAmt as varchar(50)) as FieldValue1 from tbl_TenderLotSecurity Where tenderId=@v_fieldName2Vc And appPkgLotId=@v_fieldName3Vc
 END
 ELSE
 BEGIN
	SELECT cast(SUM(tenderSecurityAmt) as varchar(50)) as FieldValue1 from tbl_TenderLotSecurity Where tenderId=@v_fieldName2Vc
 END
END


IF @v_fieldName1Vc = 'PackageWorkFlowStatus' --Done by Rajesh for, officer/APPDashboard.jsp
BEGIN
declare @wQuery4 varchar(5000)
	set @wQuery4 = 'select appStatus as FieldValue1,workflowStatus as FieldValue2,CONVERT(varchar(50), appId) as FieldValue3 
	 from tbl_AppPackages
	 where packageId='+@v_fieldName2Vc+' '
	 --and workflowStatus in(+@v_fieldName3Vc+')'
	 --print @wQuery
	 exec (@wQuery4)
	 
END

IF @v_fieldName1Vc = 'PreTenderMetWorkFlowStatus' --Done by Rajesh for, officer/APPDashboard.jsp
BEGIN
declare @PreTenderMetWorkFlowStatus varchar(5000)
	set @PreTenderMetWorkFlowStatus = 'select prebidStatus as FieldValue1,workflowStatus as FieldValue2,CONVERT(varchar(50), preTenderMetDocId) as FieldValue3 
	 from tbl_PreTenderMetDocs
	 where tenderId='+@v_fieldName2Vc+'  
	 and (workFlowStatus in('+@v_fieldName3Vc+') or prebidStatus=''Approved'')'
	  
	 exec (@PreTenderMetWorkFlowStatus)
	 
END

IF @v_fieldName1Vc = 'AppWorkFlowStatus' --Done by Rajesh for, officer/APPDashboard.jsp
BEGIN
declare @wQuery5 varchar(5000)
	set @wQuery5 = 'select appStatus as FieldValue1,workflowStatus as FieldValue2,CONVERT(varchar(50), appId) as FieldValue3 
	 from tbl_AppPackages
	 where appId='+@v_fieldName2Vc+' '
	 --order by packageid desc 
	 -- and workflowStatus in('+@v_fieldName3Vc+')'
	 --print @wQuery
	 exec (@wQuery5)
	 
END
IF @v_fieldName1Vc = 'TenderWorkFlowStatus'  
BEGIN
	declare @wQuery varchar(5000)
	set @wQuery = 'select tenderstatus as FieldValue1,workflowStatus as FieldValue2,CONVERT(varchar(50), tenderId) as FieldValue3 
	 from tbl_TenderDetails
	 where tenderId='+@v_fieldName2Vc+' and (workflowStatus in('+@v_fieldName3Vc+') or tenderstatus =''Approved'')'
	 --print @wQuery
	 exec (@wQuery)
END
IF @v_fieldName1Vc = 'CancelTenderWorkFlowStatus'  
BEGIN
	declare @cQuery varchar(5000)
	set @cQuery = 'select tenderstatus as FieldValue1,workflowStatus as FieldValue2,CONVERT(varchar(50), tenderId) as FieldValue3 
	 from tbl_CancelTenderRequest
	 where tenderId='+@v_fieldName2Vc+' and (workflowStatus in('+@v_fieldName3Vc+') or tenderstatus =''Cancelled'')'
	 print @cQuery
	 exec (@cQuery)
END

IF @v_fieldName1Vc = 'GetMyPreBidQuery'  
BEGIN
	 select CONVERT(varchar(50),l.queryId) as FieldValue1,queryText as FieldValue2,CONVERT(varchar(50), isNull(replyAction,'Pending')) as FieldValue3 ,CONVERT(varchar(50),l.userId) as FieldValue6
	 from (select * from tbl_PreTenderQuery where tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc and queryType='Prebid') l left outer join tbl_PreTenderReply r
	 on l.queryId=r.queryId and tenderId=@v_fieldName2Vc and l.userId=@v_fieldName3Vc
END
IF @v_fieldName1Vc = 'GetAllPreBidQuery'  
BEGIN

	 select CONVERT(varchar(50),l.queryId) as FieldValue1,queryText as FieldValue2,CONVERT(varchar(50), isNull(replyAction,'Pending')) as FieldValue3 ,CONVERT(varchar(50),l.userId) as FieldValue6
	 from (select * from tbl_PreTenderQuery where tenderId=@v_fieldName2Vc   and queryType='Prebid') l left outer join tbl_PreTenderReply r
	 on l.queryId=r.queryId and tenderId=@v_fieldName2Vc  
	-- select CONVERT(varchar(50),queryId) as FieldValue1,queryText as FieldValue2,CONVERT(varchar(50), replyAction) as FieldValue3  ,CONVERT(varchar(50),l.userId) as FieldValue6
	-- from tbl_PreTenderQuery l
	-- where tenderId=@v_fieldName2Vc  and queryType='Prebid'
END
 If @v_fieldName1Vc='PreTenderQueDocs'
   Begin
		Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(prebidQueryDocId  as varchar(10)) as FieldValue4 
		from tbl_PreTenderQryDocs Where tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc and queryId=0  and docType='Prebid'
   End
   If @v_fieldName1Vc='PreTenderMetDocs'
   Begin
		Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(preTenderMetDocId  as varchar(10)) as FieldValue4 
		from tbl_PreTenderMetDocs Where tenderId=@v_fieldName2Vc 
   End
   If @v_fieldName1Vc='ViewPreTenderMetsDocs'
   Begin
		Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(preTenderMetDocId  as varchar(10)) as FieldValue4 
		from tbl_PreTenderMetDocs Where tenderId=@v_fieldName2Vc 
   End
   
   If @v_fieldName1Vc='PreTenderReplyMetDocs'               --done by swati
   Begin
		Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(replyDocId  as varchar(10)) as FieldValue4 
		from tbl_PreTenderReplyDocs Where tenderId=@v_fieldName2Vc
   End  
   
 If @v_fieldName1Vc='checkTOSEntry'
Begin
	select resultShareId from tbl_TenderResultSharing where tenderId=@v_fieldName2Vc
End
 If @v_fieldName1Vc='TenderPaymentDocs'
   Begin
		Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(paymentRefDocId  as varchar(10)) as FieldValue4 
		from tbl_TenderPaymentDocs Where tenderPaymentId=@v_fieldName2Vc 
   End
 If @v_fieldName1Vc='PreTenderReplyDocs'
   Begin
		Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(replyDocId  as varchar(10)) as FieldValue4 
		from tbl_PreTenderReplyDocs Where queryId=@v_fieldName2Vc   and docType='Prebid'
   End  
   
   
IF @v_fieldName1Vc='TenExtReqHOPE'  ----Done by Rajesh for, officer/TenExtReqHOPE.jsp
BEGIN
	IF not exists(SELECT tenderid FROM tbl_TenderValidityExtDate WHERE tenderId=@v_fieldName2Vc)
		BEGIN
			select employeeName as FieldValue1,cast(userId as varchar(10)) as FieldValue2,
			(select emailid from tbl_LoginMaster where userId=tbl_EmployeeMaster.userId) as FieldValue3,
			(select emailid from tbl_LoginMaster where userId=@v_fieldName3Vc) as FieldValue4
			 from tbl_EmployeeMaster 
			where employeeId=(select employeeId from tbl_EmployeeRoles where departmentId= 
			(select departmentId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
			and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='HOPE'))
		END
END

IF @v_fieldName1Vc='TenExtReqData'  ----Done by Rajesh for, officer/TenExtReqHOPE.jsp
BEGIN
	Select cast(tenderid as varchar(20)) as FieldValue1 FROM tbl_Tenderdetails where tenderId=@v_fieldName2Vc UNION
	SELECT cast(tenderid as varchar(20)) as FieldValue1 FROM tbl_TenderValidityExtDate WHERE tenderId=@v_fieldName2Vc
END

 If @v_fieldName1Vc='checkPERole'
Begin
	Select Convert(varchar(50),PR.procurementRoleId) as FieldValue1, procurementRole as FieldValue2 ,
	(Select IsNull(CONVERT(varchar(50), datediff(day, GETDATE(), tenderValidityDt)),'null') from dbo.tbl_TenderDetails where tenderId=@v_fieldName2Vc) as FieldValue3, 
	(Select REPLACE(CONVERT(VARCHAR(11), tenderValidityDt, 106), ' ', '-') from dbo.tbl_TenderDetails where tenderId=@v_fieldName2Vc) as FieldValue4 
	from dbo.tbl_ProcurementRole PR
	Inner Join tbl_EmployeeRoles ER On ER.procurementRoleId=PR.procurementRoleId
	Inner Join tbl_employeemaster EM On ER.employeeId=EM.employeeId
	Inner Join tbl_LoginMaster LM On EM.userId=LM.userId
	Where procurementRole='PE' And LM.userId=@v_fieldName3Vc
End

IF @v_fieldName1Vc = 'OpenComWorkFlowStatus'  
BEGIN
	declare @wQuery1 varchar(5000)
	set @wQuery1 = 'select committeStatus as FieldValue1,workflowStatus as FieldValue2,
	CONVERT(varchar(50), tenderId) as FieldValue3 ,iscurrent as FieldValue4 
	 from tbl_Committee
	 where tenderId='+@v_fieldName2Vc+' and committeetype in(''TOC'',''POC'')
	  and (workflowStatus in('+@v_fieldName3Vc+') or committeStatus=''Approved'')'
	 print @wQuery1
	 exec (@wQuery1)
END
IF @v_fieldName1Vc = 'EvalComWorkFlowStatus'  
BEGIN
	declare @wQuery2 varchar(5000)
	set @wQuery2 = 'select committeStatus as FieldValue1,workflowStatus as FieldValue2,
	CONVERT(varchar(50), tenderId) as FieldValue3 ,iscurrent as FieldValue4
	 from tbl_Committee
	 where tenderId='+@v_fieldName2Vc+' and committeetype in(''TEC'',''PEC'')
	  and (workflowStatus in('+@v_fieldName3Vc+') or committeStatus=''Approved'')'
	 --print @wQuery
	 exec (@wQuery2)
END
IF @v_fieldName1Vc = 'TSCWorkFlowStatus'  
BEGIN
	declare @wQuery11 varchar(5000)
	set @wQuery11 = 'select committeStatus as FieldValue1,workflowStatus as FieldValue2,CONVERT(varchar(50), tenderId) as FieldValue3 
	 from tbl_Committee
	 where tenderId='+@v_fieldName2Vc+' and committeetype in(''TSC'') and (workflowStatus in('+@v_fieldName3Vc+') or committeStatus=''Approved'')'
	 --print @wQuery
	 exec (@wQuery11)
END

IF @v_fieldName1Vc='TenExtReqSecretary'  ----Done by Rajesh for, officer/TenExtReqHOPE.jsp
BEGIN
	IF exists(SELECT tenderid FROM tbl_TenderValidityExtDate WHERE tenderId=@v_fieldName2Vc)
	BEGIN
	
		SELECT @v_departmentid_Vc=departmentId FROM tbl_TenderDetails WHERE tenderid=@v_fieldName2Vc
		set @v_departmentidCounter_Vc=@v_departmentid_Vc
		set @v_tempCounter_Vc=''
		
		If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Organization'
			Begin
				SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)
				SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
			End
		
		
		if @v_tempCounter_Vc=''
			BEGIN
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
					END
			END
		ELSE
			BEGIN
		
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_tempCounter_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)) as varchar(10))
					END
			END
		
		
		SELECT @v_employeeIdlist_Vc = COALESCE(@v_employeeIdlist_Vc+',' , '') + cast(employeeId as varchar(10))
		FROM tbl_EmployeeRoles WHERE departmentId in (select dbo.f_trim(items) from dbo.f_split(@v_departmentidCounter_Vc,',')) 
		and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='Secretary')
		
		select employeename as FieldValue1,cast(userId as varchar(10)) as FieldValue2,(select emailId from tbl_loginmaster where userid=tbl_EmployeeMaster.userid) as FieldValue3,(select emailid from tbl_LoginMaster where userId=@v_fieldName3Vc) as FieldValue4 from tbl_EmployeeMaster where employeeId in (select dbo.f_trim(items) from dbo.f_split(@v_employeeIdlist_Vc,','))
	
	END

END
IF @v_fieldName1Vc = 'GetPendingQuery'  
BEGIN
 
	 select CONVERT(varchar(50),p.queryId) as FieldValue1,queryText as FieldValue2,CONVERT(varchar(50), 'Pending') as FieldValue3 ,CONVERT(varchar(50), p.userId) as FieldValue6 
	 from tbl_PreTenderQuery p where  queryType ='Prebid' and queryid not in (select queryId from tbl_PreTenderReply where  tenderId=@v_fieldName2Vc )  and tenderId=@v_fieldName2Vc 
	 
END
IF @v_fieldName1Vc = 'GetRepliedQuery'  
BEGIN
	 select CONVERT(varchar(50),p.queryId) as FieldValue1,queryText as FieldValue2,CONVERT(varchar(50), replyAction) as FieldValue3,CONVERT(varchar(50), replyText) as FieldValue4,CONVERT(varchar(50), rephraseQryText) as FieldValue5 ,CONVERT(varchar(50), p.userId) as FieldValue6 
	 from tbl_PreTenderQuery p,tbl_PreTenderReply r
	 where p.queryId=r.queryId and queryType ='Prebid' and   tenderId=@v_fieldName2Vc   and replyAction='Reply'
END
IF @v_fieldName1Vc = 'GetHoldQuery'  
BEGIN
	 select CONVERT(varchar(50),p.queryId) as FieldValue1,queryText as FieldValue2,CONVERT(varchar(50), replyAction) as FieldValue3 ,CONVERT(varchar(50), p.userId) as FieldValue6 
	 from tbl_PreTenderQuery p,tbl_PreTenderReply r
	 where p.queryId=r.queryId and  queryType ='Prebid' and  tenderId=@v_fieldName2Vc   and replyAction='Hold'
END	
IF @v_fieldName1Vc = 'GetIgnoreQuery'  
BEGIN
	 select CONVERT(varchar(50),p.queryId) as FieldValue1,queryText as FieldValue2,CONVERT(varchar(50), replyAction) as FieldValue3 ,CONVERT(varchar(50), p.userId) as FieldValue6 
	 from tbl_PreTenderQuery p,tbl_PreTenderReply r
	 where p.queryId=r.queryId and queryType ='Prebid' and  tenderId=@v_fieldName2Vc   and replyAction='Ignore'
END	
IF @v_fieldName1Vc = 'GetQueryText'  
BEGIN
	 select CONVERT(varchar(50),queryId) as FieldValue1,queryText as FieldValue2,CONVERT(varchar(50), queryStatus) as FieldValue3 ,CONVERT(varchar(50), userId) as FieldValue6 
	 from tbl_PreTenderQuery
	 where queryId=@v_fieldName2Vc and queryType ='Prebid'   
END	
IF @v_fieldName1Vc = 'ValidityExtensionRequestPending'  
BEGIN
	--Select CONVERT(varchar(10),tbl_TenderDetails.tenderid) as FieldValue1, CONVERT(varchar(10),reoiRfpRefNo) as FieldValue2,
	--(select DepartmentName from tbl_DepartmentMaster where DepartmentId=tbl_TenderDetails.departmentId ) as FieldValue3, peOfficeName  as FieldValue4,
	--tenderBrief as FieldValue5,CONVERT(varchar(10),tbl_TenderValidityExtDate.valExtDtId) as FieldValue6 from tbl_TenderDetails inner join tbl_TenderValidityExtDate
	--on tbl_TenderDetails.tenderId=tbl_TenderValidityExtDate.tenderId 
	--where tbl_TenderValidityExtDate.extActionBy=@v_fieldName2Vc and tbl_TenderValidityExtDate.extStatus=@v_fieldName3Vc
	
	    Select CONVERT(varchar(10),tbl_TenderDetails.tenderid) as FieldValue1,
	    CONVERT(varchar(10),reoiRfpRefNo) as FieldValue2,
		(select DepartmentName from tbl_DepartmentMaster where DepartmentId=tbl_TenderDetails.departmentId ) as FieldValue3,
		peOfficeName  as FieldValue4,
		tenderBrief as FieldValue5,
		CONVERT(varchar(10),tbl_TenderValidityExtDate.valExtDtId) as FieldValue6,
		CONVERT(varchar(10),tenderValDays) as FieldValue7,
		REPLACE(CONVERT(VARCHAR(11),tenderValidityDt, 106), ' ', '-')  as FieldValue8,
		REPLACE(CONVERT(VARCHAR(11),tenderSecurityDt, 106), ' ', '-')  as FieldValue9
		from tbl_TenderDetails inner join tbl_TenderValidityExtDate
		on tbl_TenderDetails.tenderId=tbl_TenderValidityExtDate.tenderId 
		where tbl_TenderValidityExtDate.extActionBy=@v_fieldName2Vc and tbl_TenderValidityExtDate.extStatus='Pending' and tbl_TenderValidityExtDate.tenderId=@v_fieldName3Vc order by FieldValue6
END

IF @v_fieldName1Vc = 'ValidityExtensionRequestApproved'  
BEGIN
	--Select CONVERT(varchar(10),tbl_TenderDetails.tenderid) as FieldValue1, CONVERT(varchar(10),reoiRfpRefNo) as FieldValue2,
	--(select DepartmentName from tbl_DepartmentMaster where DepartmentId=tbl_TenderDetails.departmentId ) as FieldValue3, peOfficeName  as FieldValue4,
	--tenderBrief as FieldValue5,CONVERT(varchar(10),tbl_TenderValidityExtDate.valExtDtId) as FieldValue6 from tbl_TenderDetails inner join tbl_TenderValidityExtDate
	--on tbl_TenderDetails.tenderId=tbl_TenderValidityExtDate.tenderId 
	--where tbl_TenderValidityExtDate.extActionBy=@v_fieldName2Vc and tbl_TenderValidityExtDate.extStatus=@v_fieldName3Vc
	
	Select CONVERT(varchar(10),tbl_TenderDetails.tenderid) as FieldValue1, CONVERT(varchar(10),reoiRfpRefNo) as FieldValue2,
	(select DepartmentName from tbl_DepartmentMaster where DepartmentId=tbl_TenderDetails.departmentId ) as FieldValue3, peOfficeName  as FieldValue4,
	tenderBrief as FieldValue5,cast('0' as varchar(10)) as FieldValue6,
	cast(tenderValDays as varchar(10)) as FieldValue7,REPLACE(CONVERT(VARCHAR(11),tenderValidityDt, 106), ' ', '-')  as FieldValue8,REPLACE(CONVERT(VARCHAR(11),tenderSecurityDt, 106), ' ', '-')  as FieldValue9
	from tbl_TenderDetails
	where tbl_TenderDetails.tenderId=@v_fieldName3Vc UNION all
	Select CONVERT(varchar(10),tbl_TenderDetails.tenderid) as FieldValue1, CONVERT(varchar(10),reoiRfpRefNo) as FieldValue2,
	(select DepartmentName from tbl_DepartmentMaster where DepartmentId=tbl_TenderDetails.departmentId ) as FieldValue3, peOfficeName  as FieldValue4,
	tenderBrief as FieldValue5,CONVERT(varchar(10),tbl_TenderValidityExtDate.valExtDtId) as FieldValue6,
	cast(tenderValDays as varchar(10)) as FieldValue7,REPLACE(CONVERT(VARCHAR(11),tenderValidityDt, 106), ' ', '-')  as FieldValue8,REPLACE(CONVERT(VARCHAR(11),tenderSecurityDt, 106), ' ', '-')  as FieldValue9
	from tbl_TenderDetails inner join tbl_TenderValidityExtDate
	on tbl_TenderDetails.tenderId=tbl_TenderValidityExtDate.tenderId 
	where tbl_TenderValidityExtDate.extActionBy=@v_fieldName2Vc and tbl_TenderValidityExtDate.extStatus='Approved' and tbl_TenderValidityExtDate.tenderId=@v_fieldName3Vc order by FieldValue6
END	

IF @v_fieldName1Vc = 'SearchEmail' -- for/partner/SearchTenderer.jsp
BEGIN
	Set @v_Qry_Vc='	
	select CONVERT(varchar(50), LM.userId)  as FieldValue1, emailId as FieldValue2, 
		CONVERT(varchar(50), CM.companyId) as FieldValue3, 
		dbo.f_getbiddercompany(LM.userId) as FieldValue4,
		Case 
			When Exists (select tenderPaymentId from tbl_TenderPayment iTP 
				inner join tbl_LoginMaster iLM On iTP.userId=iLM.userId 
					Where iLM.status=''approved'' And ' + @v_fieldName3Vc +') 			
			Then ''Paid''
			Else ''Pending''
		End as 	FieldValue5,
		(select top 1 CONVERT(varchar(50),tenderPaymentId) from tbl_TenderPayment iTP 
				inner join tbl_LoginMaster iLM On iTP.userId=iLM.userId 
					Where iLM.status=''approved'' And ' + @v_fieldName3Vc +' Order by tenderPaymentId Desc) as FieldValue6,
		(select top 1 dbo.f_initcap(iTP.status) from tbl_TenderPayment iTP 
				inner join tbl_LoginMaster iLM On iTP.userId=iLM.userId 
					Where iLM.status=''approved'' And ' + @v_fieldName3Vc +' Order by tenderPaymentId Desc) as FieldValue7,
		(select top 1 iTP.paymentMode from tbl_TenderPayment iTP 
				inner join tbl_LoginMaster iLM On iTP.userId=iLM.userId 
					Where iLM.status=''approved'' And ' + @v_fieldName3Vc +' Order by tenderPaymentId Desc) as FieldValue8
					 
		from tbl_LoginMaster LM 
		inner join tbl_tenderermaster TM on LM.userId=TM.userId
		inner join tbl_CompanyMaster CM on TM.companyId=CM.companyId
		Where LM.status=''approved'' And emailId=''' + @v_fieldName2Vc+ ''''
		
		--print (@v_Qry_Vc)
		Exec (@v_Qry_Vc)
END

IF @v_fieldName1Vc='getPaidCustomers_forDocumentFees'
BEGIN
	IF @v_fieldName3Vc is not null and @v_fieldName3Vc<>''
	Begin
		select CONVERT(varchar(50), LM.userId)  as FieldValue1, emailId as FieldValue2, 
			CONVERT(varchar(50), CM.companyId) as FieldValue3, 
			dbo.f_getbiddercompany(LM.userId) as FieldValue4,
			CONVERT(varchar(50), TP.tenderPaymentId) as FieldValue5,			
			dbo.f_initcap(TP.status) FieldValue6, TP.paymentMode as FieldValue7,
			CONVERT(varchar(50), TP.createdBy)  as FieldValue8
			from 
			tbl_TenderPayment TP
			inner join tbl_LoginMaster LM on LM.userId=TP.userId
			inner join tbl_tenderermaster TM on LM.userId=TM.userId
			inner join tbl_CompanyMaster CM on TM.companyId=CM.companyId
			Where LM.status='approved' And TP.paymentFor='Document Fees' And TP.tenderId=@v_fieldName2Vc And TP.pkgLotId=@v_fieldName3Vc
			ORder by TP.tenderPaymentId desc
	End
	Else
	Begin
		--select top 1 CONVERT(varchar(50), LM.userId)  as FieldValue1, emailId as FieldValue2, 
		--	CONVERT(varchar(50), CM.companyId) as FieldValue3, companyName as FieldValue4,
		--	CONVERT(varchar(50), TP.tenderPaymentId) as FieldValue5,
		--	TP.status FieldValue6, TP.paymentMode as FieldValue7  
		--	from 
		--	tbl_TenderPayment TP
		--	inner join tbl_LoginMaster LM on LM.userId=TP.userId
		--	inner join tbl_tenderermaster TM on LM.userId=TM.userId
		--	inner join tbl_CompanyMaster CM on TM.companyId=CM.companyId
		--	Where LM.status='approved' And TP.paymentFor='Document Fees' And TP.tenderId=@v_fieldName2Vc
		--	ORder by TP.tenderPaymentId desc
		select CONVERT(varchar(50), LM.userId)  as FieldValue1, emailId as FieldValue2, 
			CONVERT(varchar(50), CM.companyId) as FieldValue3, 
			dbo.f_getbiddercompany(LM.userId) as FieldValue4,
			CONVERT(varchar(50), TP.tenderPaymentId) as FieldValue5,
			dbo.f_initcap(TP.status)  FieldValue6, TP.paymentMode as FieldValue7,
			CONVERT(varchar(50), TP.createdBy)  as FieldValue8  
			from 
			tbl_TenderPayment TP
			inner join tbl_LoginMaster LM on LM.userId=TP.userId
			inner join tbl_tenderermaster TM on LM.userId=TM.userId
			inner join tbl_CompanyMaster CM on TM.companyId=CM.companyId
			Where LM.status='approved' And TP.paymentFor='Document Fees' And TP.tenderId=@v_fieldName2Vc
			ORder by TP.tenderPaymentId desc
	End
END

IF @v_fieldName1Vc='getPaidCustomers_forTenderSecurity'
BEGIN
	IF @v_fieldName3Vc is not null and @v_fieldName3Vc<>''
	Begin
		select CONVERT(varchar(50), LM.userId)  as FieldValue1, emailId as FieldValue2, 
			CONVERT(varchar(50), CM.companyId) as FieldValue3, 
			dbo.f_getbiddercompany(LM.userId) as FieldValue4,
			CONVERT(varchar(50), TP.tenderPaymentId) as FieldValue5,
			dbo.f_initcap(TP.status)  FieldValue6, TP.paymentMode as FieldValue7,
			CONVERT(varchar(50), TP.createdBy)  as FieldValue8
			from 
			tbl_TenderPayment TP
			inner join tbl_LoginMaster LM on LM.userId=TP.userId
			inner join tbl_tenderermaster TM on LM.userId=TM.userId
			inner join tbl_CompanyMaster CM on TM.companyId=CM.companyId
			Where LM.status='approved' And TP.paymentFor='Tender Security' And TP.tenderId=@v_fieldName2Vc And TP.pkgLotId=@v_fieldName3Vc
			ORder by TP.tenderPaymentId desc
	End
	Else
	Begin
		select CONVERT(varchar(50), LM.userId)  as FieldValue1, emailId as FieldValue2, 
			CONVERT(varchar(50), CM.companyId) as FieldValue3, 
			dbo.f_getbiddercompany(LM.userId) as FieldValue4,
			CONVERT(varchar(50), TP.tenderPaymentId) as FieldValue5,
			dbo.f_initcap(TP.status)  FieldValue6, TP.paymentMode as FieldValue7,
			CONVERT(varchar(50), TP.createdBy)  as FieldValue8 
			from 
			tbl_TenderPayment TP
			inner join tbl_LoginMaster LM on LM.userId=TP.userId
			inner join tbl_tenderermaster TM on LM.userId=TM.userId
			inner join tbl_CompanyMaster CM on TM.companyId=CM.companyId
			Where LM.status='approved' And TP.paymentFor='Tender Security' And TP.tenderId=@v_fieldName2Vc
			ORder by TP.tenderPaymentId desc
	End
END

IF @v_fieldName1Vc='getPaidCustomers_forPerformanceSecurity'
BEGIN
	IF @v_fieldName3Vc is not null and @v_fieldName3Vc<>''
	Begin
		select CONVERT(varchar(50), LM.userId)  as FieldValue1, emailId as FieldValue2, 
			CONVERT(varchar(50), CM.companyId) as FieldValue3, 
			dbo.f_getbiddercompany(LM.userId) as FieldValue4,
			CONVERT(varchar(50), TP.tenderPaymentId) as FieldValue5,
			dbo.f_initcap(TP.status)  FieldValue6, TP.paymentMode as FieldValue7,
			CONVERT(varchar(50), TP.createdBy)  as FieldValue8 
			from 
			tbl_TenderPayment TP
			inner join tbl_LoginMaster LM on LM.userId=TP.userId
			inner join tbl_tenderermaster TM on LM.userId=TM.userId
			inner join tbl_CompanyMaster CM on TM.companyId=CM.companyId
			Where LM.status='approved' And TP.paymentFor='Performance Security' And TP.tenderId=@v_fieldName2Vc And TP.pkgLotId=@v_fieldName3Vc
			ORder by TP.tenderPaymentId desc
	End
	Else
	Begin
		select CONVERT(varchar(50), LM.userId)  as FieldValue1, emailId as FieldValue2, 
			CONVERT(varchar(50), CM.companyId) as FieldValue3, 
			dbo.f_getbiddercompany(LM.userId) as FieldValue4,
			CONVERT(varchar(50), TP.tenderPaymentId) as FieldValue5,
			dbo.f_initcap(TP.status)  FieldValue6, TP.paymentMode as FieldValue7,
			CONVERT(varchar(50), TP.createdBy)  as FieldValue8 
			from 
			tbl_TenderPayment TP
			inner join tbl_LoginMaster LM on LM.userId=TP.userId
			inner join tbl_tenderermaster TM on LM.userId=TM.userId
			inner join tbl_CompanyMaster CM on TM.companyId=CM.companyId
			Where LM.status='approved' And TP.paymentFor='Performance Security' And TP.tenderId=@v_fieldName2Vc
			ORder by TP.tenderPaymentId desc
	End
END

 If @v_fieldName1Vc='GetExtDetails'
   Begin
		select extReason as FieldValue1,extStatus as FieldValue2 from tbl_TenderValidityExtDate where valExtDtId=@v_fieldName2Vc
		--select extReason as FieldValue1,extStatus as FieldValue2 from tbl_TenderValidityExt where valExtReqId=@v_fieldName2Vc 
   End  
   
   
IF @v_fieldName1Vc='getTenderPaymentInfo'
BEGIN	
	select @v_docAvlMethod_Vc=docAvlMethod from tbl_TenderDetails where tenderId=@v_fieldName2Vc
	If @v_docAvlMethod_Vc='Package'
	Begin	
		select CONVERT(varchar(50),TM.tenderId) as FieldValue1, 
		docFeesMode as FieldValue2, 
		docAvlMethod as FieldValue3, 
		IsNull(REPLACE(CONVERT(VARCHAR(11),TD.docEndDate, 106), ' ', '-'),'') as FieldValue4, 
		IsNull(CONVERT(varchar(50), datediff(Minute, GETDATE(), TD.docEndDate)),'null') as FieldValue5,
		IsNull(Convert(varchar(50), pkgDocFees),'')  as FieldValue6, 
		IsNull(REPLACE(CONVERT(VARCHAR(11),TD.tenderSecurityDt, 106), ' ', '-'),'') as FieldValue7,
		IsNull(CONVERT(varchar(50), datediff(Minute, GETDATE(), TD.tenderSecurityDt)),'null') as FieldValue8,
		IsNull(Convert(varchar(50),Sum(TLS.tenderSecurityAmt)),'') as FieldValue9,
		IsNull(CONVERT(varchar(50), datediff(Minute, GETDATE(), NOA.perSecSubDt)),'null') as FieldValue10
		from tbl_TenderDetails TD 
		inner join tbl_TenderMaster TM On TD.tenderId=TM.tenderId
		inner join tbl_TenderLotSecurity TLS On TLS.tenderId=TM.tenderId
		left join tbl_noaissuedetails NOA On TM.tenderId=NOA.tenderId
		where TM.tenderId=@v_fieldName2Vc
		Group by TM.tenderId, docFeesMode, docAvlMethod, TD.docEndDate, pkgDocFees, TD.tenderSecurityDt, TLS.tenderSecurityAmt,NOA.perSecSubDt
	End
	ELSE If @v_docAvlMethod_Vc='Lot'
	Begin
		select CONVERT(varchar(50),TM.tenderId) as FieldValue1, 
		docFeesMode as FieldValue2, 
		docAvlMethod as FieldValue3, 
		IsNull(REPLACE(CONVERT(VARCHAR(11),TD.docEndDate, 106), ' ', '-') ,'') as FieldValue4, 
		IsNull(CONVERT(varchar(50), datediff(Minute, GETDATE(), TD.docEndDate)),'null') as FieldValue5,		
		IsNull(Convert(varchar(50), TLS.docFess ),'')  as FieldValue6, 
		IsNull(REPLACE(CONVERT(VARCHAR(11),TD.tenderSecurityDt, 106), ' ', '-') ,'') as FieldValue7,
		IsNull(CONVERT(varchar(50), datediff(Minute, GETDATE(), TD.tenderSecurityDt)),'null') as FieldValue8,
		IsNull(Convert(varchar(50),Sum(TLS.tenderSecurityAmt)),'')  as FieldValue9,
		IsNull(CONVERT(varchar(50), datediff(Minute, GETDATE(), NOA.perSecSubDt)),'null') as FieldValue10
		from tbl_TenderDetails TD 
		inner join tbl_TenderMaster TM	On TD.tenderId=TM.tenderId
		inner join tbl_TenderLotSecurity TLS On TLS.tenderId=TM.tenderId
		left join tbl_noaissuedetails NOA On TM.tenderId=NOA.tenderId		
		where TM.tenderId=@v_fieldName2Vc and TLS.appPkgLotId=@v_fieldName3Vc
		Group by TM.tenderId, docFeesMode, docAvlMethod, TD.docEndDate, TLS.docFess, TD.tenderSecurityDt, TLS.tenderSecurityAmt,NOA.perSecSubDt	
	End
END
   
   
IF @v_fieldName1Vc = 'getTendererPaymentInfo' -- for/partner/SearchTenderer.jsp
BEGIN
	Set @v_Qry_Vc='	
	select CONVERT(varchar(50), LM.userId)  as FieldValue1, emailId as FieldValue2, 
		CONVERT(varchar(50), CM.companyId) as FieldValue3, 
		dbo.f_getbiddercompany(LM.userId) as FieldValue4,
		Case 
			When Exists (select tenderPaymentId from tbl_TenderPayment iTP 
				inner join tbl_LoginMaster iLM On iTP.userId=iLM.userId 
					Where iLM.status=''approved'' And ' + @v_fieldName3Vc +') 			
			Then ''Paid''
			Else ''Pending''
		End as 	FieldValue5,
		(select top 1 CONVERT(varchar(50),tenderPaymentId) from tbl_TenderPayment iTP 
				inner join tbl_LoginMaster iLM On iTP.userId=iLM.userId 
					Where iLM.status=''approved'' And ' + @v_fieldName3Vc +'  Order by tenderPaymentId desc) as FieldValue6,
		(select top 1 dbo.f_initcap(iTP.status) from tbl_TenderPayment iTP 
				inner join tbl_LoginMaster iLM On iTP.userId=iLM.userId 
					Where iLM.status=''approved'' And ' + @v_fieldName3Vc +'  Order by tenderPaymentId desc) as FieldValue7,
		(select top 1 iTP.paymentMode from tbl_TenderPayment iTP 
				inner join tbl_LoginMaster iLM On iTP.userId=iLM.userId 
					Where iLM.status=''approved'' And ' + @v_fieldName3Vc +'  Order by tenderPaymentId desc) as FieldValue8
		from tbl_LoginMaster LM 
		inner join tbl_tenderermaster TM on LM.userId=TM.userId
		inner join tbl_CompanyMaster CM on TM.companyId=CM.companyId
		Where LM.status=''approved'' And LM.userId=''' + @v_fieldName2Vc+ ''''
		
		print (@v_Qry_Vc)
		Exec (@v_Qry_Vc)
END

IF @v_fieldName1Vc = 'getUserIdForReport' -- for indreport.jsp
BEGIN

SELECT distinct convert(varchar(20), tbl_FinalSubmission.userId) as FieldValue1, (select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue2 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c
	where l.userId=t.userId and t.companyId=c.companyId and l.userid = tbl_FinalSubmission.userId) as FieldValue2
FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId where tbl_FinalSubmission.bidSubStatus = 'finalsubmission' and tenderid = @v_fieldName2Vc and tenderFormid = @v_fieldName3Vc order by FieldValue2
End
IF @v_fieldName1Vc = 'getFinalSubCompany' -- for indreport.jsp
BEGIN

SELECT distinct convert(varchar(20), tbl_FinalSubmission.userId) as FieldValue1, (select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue2 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c
	where l.userId=t.userId and t.companyId=c.companyId and l.userid = tbl_FinalSubmission.userId) as FieldValue2
FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission
 ON tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId AND
  tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId 
  where tbl_FinalSubmission.bidSubStatus = 'finalsubmission' and tenderid = @v_fieldName2Vc  order by FieldValue2
End
IF @v_fieldName1Vc = 'getFinalUserIdForReport' -- for indreport.jsp
BEGIN
if((select procurementnature from tbl_TenderDetails where tenderid=@v_fieldName2Vc)!='Services' )
begin
SELECT distinct convert(varchar(20), tbl_FinalSubmission.userId) as FieldValue1, (select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue2 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c
	where l.userId=t.userId and t.companyId=c.companyId and l.userid = tbl_FinalSubmission.userId) as FieldValue2
FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId 
 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId 
 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
  and tenderid = @v_fieldName2Vc 
  --and userid not in(select userid from tbl_PostQualification where postQualStatus='Disqualify' and  tenderid=@v_fieldName2Vc and pkgLotId in(select pkglotid  from tbl_ReportLots where reportId= @v_fieldName3Vc ) )
  and  tbl_FinalSubmission.userId in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and (bidderStatus='Technically Responsive'or result='pass'))
  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc) order by FieldValue2
 end
 else
 begin
 if((Select tcm.evalMethod
	from tbl_TenderDetails ttd,tbl_TenderTypes ttt,tbl_ConfigEvalMethod tcm
	 where ttd.eventType =ttt.tenderType
	 and ttd.procurementMethodId =tcm.procurementMethodId 
	 and ttt.tenderTypeId=tcm.tenderTypeId  
	 and ttd.procurementNatureId =tcm.procurementNatureId 
	 and ttd.tenderId=@v_fieldName2Vc)!='3')
	 begin
 SELECT distinct convert(varchar(20), tbl_FinalSubmission.userId) as FieldValue1, (select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue2 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c
	where l.userId=t.userId and t.companyId=c.companyId and l.userid = tbl_FinalSubmission.userId) as FieldValue2
FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId 
 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId 
 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
  and tenderid = @v_fieldName2Vc 
  --and userid not in(select userid from tbl_PostQualification where postQualStatus='Disqualify' and tenderid=@v_fieldName2Vc and pkgLotId in(select pkglotid  from tbl_ReportLots where reportId= @v_fieldName3Vc ) )
  and  tbl_FinalSubmission.userId in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and (bidderStatus='Technically Responsive'or result='pass'))
  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc) order by FieldValue2
	end
	else
	begin
	SELECT distinct convert(varchar(20), tbl_FinalSubmission.userId) as FieldValue1, (select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue2 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c
	where l.userId=t.userId and t.companyId=c.companyId and l.userid = tbl_FinalSubmission.userId) as FieldValue2
FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId 
 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId 
 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
  and tenderid = @v_fieldName2Vc 
  --and userid not in(select userid from tbl_PostQualification where  postQualStatus='Disqualify' and  tenderid=@v_fieldName2Vc and pkgLotId in(select pkglotid  from tbl_ReportLots where reportId= @v_fieldName3Vc ) )
  and  tbl_FinalSubmission.userId in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and 
  (result='pass'))
  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc) order by FieldValue2
-- and bidderMarks=(select max(biddermarks) from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and result='Pass')
	end
 end 
End 
IF @v_fieldName1Vc = 'getAllBidders' -- for indreport.jsp
BEGIN

SELECT distinct convert(varchar(20), tbl_FinalSubmission.userId) as FieldValue1, (select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue2 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c
	where l.userId=t.userId and t.companyId=c.companyId and l.userid = tbl_FinalSubmission.userId) as FieldValue2
FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId 
 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId 
 --and userid not in(select userid from tbl_PostQualification where postQualStatus='Disqualify' and tenderid=@v_fieldName2Vc and pkgLotId in(select pkglotid  from tbl_ReportLots where reportId= @v_fieldName3Vc ) )
 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
  and tenderid = @v_fieldName2Vc
   and userId not in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and 
  ((isnull(result,'pass')='fail') or (isnull(bidderStatus,'Technically Responsive')='Technically Unresponsive')))
  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc) order by FieldValue2
End 
IF @v_fieldName1Vc = 'ValidityExtReq' -- Done by Rajesh for officer/TenderExtReq.jsp
BEGIN
Declare @vDate as datetime,@sDate as Datetime
set @vDate=(select dbo.f_gettendervaliditydate(@v_fieldName2Vc))
set @sDate=(select dbo.f_gettendersecuritydate(@v_fieldName2Vc)) 

--select cast(tenderValDays as varchar(10)) as FieldValue1,
--	   IsNull(REPLACE(CONVERT(VARCHAR(11),@vDate, 106), ' ', '-') ,'') as FieldValue2,
--	   IsNull(REPLACE(CONVERT(VARCHAR(11),@sDate, 106), ' ', '-') ,'') as FieldValue3 
--from tbl_tenderdetails 
--where tenderId= @v_fieldName2Vc

select cast(tenderValDays as varchar(10)) as FieldValue1,
	   IsNull(REPLACE(CONVERT(VARCHAR(11),@vDate, 106), ' ', '-') ,'') as FieldValue2,
	   IsNull(REPLACE(CONVERT(VARCHAR(11),@sDate, 106), ' ', '-') ,'') as FieldValue3,
	   IsNull(REPLACE(CONVERT(VARCHAR(11),@vDate, 103), ' ', '-') ,'') as FieldValue4,
	   IsNull(REPLACE(CONVERT(VARCHAR(11),@sDate, 103), ' ', '-') ,'') as FieldValue5  
from tbl_tenderdetails 
where tenderId= @v_fieldName2Vc

End


IF @v_fieldName1Vc='ValidityTenExtReqHOPE'  ----Done by Rajesh for, officer/TenderExtReq.jsp
BEGIN
	IF not exists(SELECT tenderid FROM tbl_TenderValidityExtDate WHERE tenderId=@v_fieldName2Vc)
		BEGIN
			select employeeName as FieldValue1,cast(employeeId as varchar(10)) as FieldValue2 from tbl_EmployeeMaster 
			where employeeId=(select employeeId from tbl_EmployeeRoles where departmentId= 
			(select departmentId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
			and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='HOPE'))
		END
END


IF @v_fieldName1Vc='ValidityTenExtReqSecretary'  ----Done by Rajesh for, officer/TenderExtReq.jsp
BEGIN
	IF exists(SELECT tenderid FROM tbl_TenderValidityExtDate WHERE tenderId=@v_fieldName2Vc)
	BEGIN
	
		SELECT @v_departmentid_Vc=departmentId FROM tbl_TenderDetails WHERE tenderid=@v_fieldName2Vc
		set @v_departmentidCounter_Vc=@v_departmentid_Vc
		set @v_tempCounter_Vc=''
		
		If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Organization'
			Begin
				SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)
				SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
			End
		
		
		if @v_tempCounter_Vc=''
			BEGIN
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
					END
			END
		ELSE
			BEGIN
		
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_tempCounter_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)) as varchar(10))
					END
			END
		
		
		SELECT @v_employeeIdlist_Vc = COALESCE(@v_employeeIdlist_Vc+',' , '') + cast(employeeId as varchar(10))
		FROM tbl_EmployeeRoles WHERE departmentId in (select dbo.f_trim(items) from dbo.f_split(@v_departmentidCounter_Vc,',')) 
		and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='Secretary')
		
		print @v_employeeIdlist_Vc
		select employeename as FieldValue1,cast(employeeId as varchar(10)) as FieldValue2,
		(select emailid from tbl_LoginMaster where userId=tbl_EmployeeMaster.userId) as FieldValue3,
		(select emailid from tbl_LoginMaster where userId=@v_fieldName3Vc) as FieldValue4 from tbl_EmployeeMaster where employeeId in (select dbo.f_trim(items) from dbo.f_split(@v_employeeIdlist_Vc,','))
	
	END
END
IF @v_fieldName1Vc = 'GetReplyText'  
BEGIN
	 select CONVERT(varchar(50),preTenderReplyId) as FieldValue1,replyText as FieldValue2,CONVERT(varchar(50), rephraseQryText) as FieldValue3 
	 from tbl_PreTenderReply
	 where queryId=@v_fieldName2Vc
END	
IF @v_fieldName1Vc = 'GetProcMethodDays'  
BEGIN
select  convert(varchar(10),minSubDays) as FieldValue1,convert(varchar(10),maxSubDays) as FieldValue2 from tbl_tenderdetails t,tbl_configprocurement p,tbl_ProcurementTypes tm,tbl_TenderTypes tt
where t.procurementMethodId=p.procurementMethodId
and t.budgetTypeId=p.budgetTypeId and t.procurementNatureId=p.procurementnatureid
and t.procurementType=tm.procurementType 
and tt.tenderType=t.eventtype
and tenderid=@v_fieldName2Vc and isNationalDisaster=pkgUrgency
END

--IF @v_fieldName1Vc = 'ValidityExtensionRequestListingPending'   ----Done by Rajesh for, officer/TenderExtReqListing.jsp
--BEGIN
--	Select CONVERT(varchar(10),tbl_TenderDetails.tenderid) as FieldValue1, CONVERT(varchar(10),reoiRfpRefNo) as FieldValue2,
--	(select DepartmentName from tbl_DepartmentMaster where DepartmentId=tbl_TenderDetails.departmentId ) as FieldValue3, peOfficeName  as FieldValue4,
--	tenderBrief as FieldValue5,CONVERT(varchar(10),tbl_TenderValidityExtDate.valExtDtId) as FieldValue6 from tbl_TenderDetails inner join tbl_TenderValidityExtDate
--	on tbl_TenderDetails.tenderId=tbl_TenderValidityExtDate.tenderId 
--	where tbl_TenderValidityExtDate.extSentToRole=@v_fieldName2Vc and tbl_TenderValidityExtDate.extStatus='Pending'
--END	

--IF @v_fieldName1Vc = 'ValidityExtensionRequestListingApproved'   ----Done by Rajesh for, officer/TenderExtReqListing.jsp
--BEGIN
--	Select CONVERT(varchar(10),tbl_TenderDetails.tenderid) as FieldValue1, CONVERT(varchar(10),reoiRfpRefNo) as FieldValue2,
--	(select DepartmentName from tbl_DepartmentMaster where DepartmentId=tbl_TenderDetails.departmentId ) as FieldValue3, peOfficeName  as FieldValue4,
--	tenderBrief as FieldValue5,CONVERT(varchar(10),tbl_TenderValidityExtDate.valExtDtId) as FieldValue6 from tbl_TenderDetails inner join tbl_TenderValidityExtDate
--	on tbl_TenderDetails.tenderId=tbl_TenderValidityExtDate.tenderId 
--	where tbl_TenderValidityExtDate.extSentToRole=@v_fieldName2Vc and (tbl_TenderValidityExtDate.extStatus='Approved' Or tbl_TenderValidityExtDate.extStatus='Reject')
--END


IF @v_fieldName1Vc = 'GetValidityExtDetail'   ----Done by Rajesh for, officer/TenderExtReqListing.jsp
BEGIN
	if (@v_fieldName2Vc=0)
	BEGIN
		SELECT cast('0' as varchar(10)) as FieldValue1,
		cast(tenderValDays as varchar(10)) as FieldValue2,REPLACE(CONVERT(VARCHAR(11),tenderValidityDt, 106), ' ', '-')  as FieldValue3,REPLACE(CONVERT(VARCHAR(11),tenderSecurityDt, 106), ' ', '-')  as FieldValue4
		from tbl_TenderDetails
		where tbl_TenderDetails.tenderId=@v_fieldName3Vc
	END
	ELSE
	BEGIN
		select CONVERT(varchar(10),tenderValDays) as FieldValue1,
			REPLACE(CONVERT(VARCHAR(11),lastPropValDt, 106), ' ', '-')  as FieldValue2,
			REPLACE(CONVERT(VARCHAR(11),newPropValDt, 106), ' ', '-')  as FieldValue3,
			REPLACE(CONVERT(VARCHAR(11),tenderSecLastDt, 106), ' ', '-')  as FieldValue4,
			REPLACE(CONVERT(VARCHAR(11),tenderSecNewDt, 106), ' ', '-')  as FieldValue5,
			extStatus as FieldValue6,
			(select employeeName from tbl_EmployeeMaster where employeeId=(select employeeId from tbl_EmployeeMaster where userId=extActionBy)) as FieldValue7,
			extReason as FieldValue8,REPLACE(CONVERT(VARCHAR(11),lastValAcceptDt, 106), ' ', '-')  as FieldValue9,notifyComments as FieldValue10
		from dbo.tbl_TenderValidityExtDate inner join tbl_TenderDetails
			on tbl_TenderValidityExtDate.tenderId=tbl_TenderDetails.tenderId
			where tbl_TenderValidityExtDate.valExtDtId=@v_fieldName2Vc
	END
END


IF @v_fieldName1Vc = 'getBankInfo' 
BEGIN
	--declare @v_BankDevelHeadId_Int int, @v_BankName varchar(500) 

	--Select top 1 @v_BankDevelHeadId_Int = SBP.sBankDevelHeadId 
	--from tbl_PartnerAdmin PA Inner join tbl_ScBankDevPartnerMaster SBP
	--On PA.sBankDevelopId=SBP.sBankDevelopId
	--Where userId=@v_fieldName2Vc

	--	If @v_BankDevelHeadId_Int=0
	--	Begin
	--		Select top 1 @v_BankName = sbDevelopName from tbl_PartnerAdmin PA Inner join tbl_ScBankDevPartnerMaster SBP
	--		On PA.sBankDevelopId=SBP.sBankDevelopId
	--		Where PA.userId=@v_fieldName2Vc
	--	End
	--	Else
	--	Begin
	--		Select top 1 @v_BankName = sbDevelopName from tbl_ScBankDevPartnerMaster 
	--		Where sBankDevelopId=(
	--			Select top 1 SBP.sBankDevelHeadId from tbl_PartnerAdmin PA Inner join tbl_ScBankDevPartnerMaster SBP
	--				On PA.sBankDevelopId=SBP.sBankDevelopId
	--			Where PA.userId=@v_fieldName2Vc)
	--	End

	--Select top 1 @v_BankName as FieldValue1, sbDevelopName as FieldValue2, PA.fullName as FieldValue3 
	--From tbl_PartnerAdmin PA Inner join tbl_ScBankDevPartnerMaster SBP
	--	On PA.sBankDevelopId=SBP.sBankDevelopId
	--	Where PA.userId=@v_fieldName2Vc
	
	--	declare @v_BankDevelHeadId_Int int, @v_BankName varchar(500) 

	Select top 1 @v_BankDevelHeadId_Int = SBP.sBankDevelHeadId 
	from tbl_PartnerAdminTransfer PA Inner join tbl_ScBankDevPartnerMaster SBP
	On PA.sBankDevelopId=SBP.sBankDevelopId
	Where PA.partTransId=@v_fieldName2Vc

		If @v_BankDevelHeadId_Int=0
		Begin
			Select top 1 @v_BankName = sbDevelopName from tbl_PartnerAdminTransfer PA Inner join tbl_ScBankDevPartnerMaster SBP
			On PA.sBankDevelopId=SBP.sBankDevelopId
			Where PA.partTransId=@v_fieldName2Vc
		End
		Else
		Begin
			Select top 1 @v_BankName = sbDevelopName from tbl_ScBankDevPartnerMaster 
			Where sBankDevelopId=(
				Select top 1 SBP.sBankDevelHeadId from tbl_PartnerAdminTransfer PA Inner join tbl_ScBankDevPartnerMaster SBP
					On PA.sBankDevelopId=SBP.sBankDevelopId
				Where PA.partTransId=@v_fieldName2Vc)
		End

	Select top 1 @v_BankName as FieldValue1, sbDevelopName as FieldValue2, PA.fullName as FieldValue3 
	From tbl_PartnerAdminTransfer PA Inner join tbl_ScBankDevPartnerMaster SBP
		On PA.sBankDevelopId=SBP.sBankDevelopId
		Where PA.partTransId=@v_fieldName2Vc
	
	

	--Select @v_BankUserType = 
	--Case isMakerChecker When 'BranchMaker' Then 'maker' When 'BranchChecker' Then 'checker' End 
	--from tbl_PartnerAdminTransfer PA Inner join tbl_ScBankDevPartnerMaster SBP
	--On PA.sBankDevelopId=SBP.sBankDevelopId
	--Where PA.partTransId=@v_fieldName2Vc

	--If @v_fieldName3Vc is Not Null And @v_fieldName3Vc<>'' And @v_fieldName3Vc<>'0'
	--Begin
	--	-- Edit Payment Case
	--	If (Select isMakerChecker from tbl_PartnerAdminTransfer Where partTransId=@v_fieldName2Vc)='BranchChecker'		
	--	Begin
	--		-- // If Current User is Branch Checker i.e.: Edit Payment by Checker Case
	--		-- Select the partTransId of the Bank Maker who has done the Payment
	--		Select @v_fieldName2Vc= partTransId 
	--		from tbl_TenderPayment where tenderPaymentId=@v_fieldName3Vc
	--	End
	--End

	--Select top 1 @v_BankDevelHeadId_Int = SBP.sBankDevelHeadId 
	--from tbl_PartnerAdminTransfer PA Inner join tbl_ScBankDevPartnerMaster SBP
	--On PA.sBankDevelopId=SBP.sBankDevelopId
	--Where PA.partTransId=@v_fieldName2Vc

	--	If @v_BankDevelHeadId_Int=0
	--	Begin
	--		Select top 1 @v_BankName = sbDevelopName from tbl_PartnerAdminTransfer PA Inner join tbl_ScBankDevPartnerMaster SBP
	--		On PA.sBankDevelopId=SBP.sBankDevelopId
	--		Where PA.partTransId=@v_fieldName2Vc
	--	End
	--	Else
	--	Begin
	--		Select top 1 @v_BankName = sbDevelopName from tbl_ScBankDevPartnerMaster 
	--		Where sBankDevelopId=(
	--			Select top 1 SBP.sBankDevelHeadId from tbl_PartnerAdminTransfer PA Inner join tbl_ScBankDevPartnerMaster SBP
	--				On PA.sBankDevelopId=SBP.sBankDevelopId
	--			Where PA.partTransId=@v_fieldName2Vc)
	--	End

	--Select top 1 @v_BankName as FieldValue1, 
	--sbDevelopName as FieldValue2, 
	--PA.fullName as FieldValue3,
	--@v_BankUserType as FieldValue4,
	--Convert(varchar(50),PA.userId) as FieldValue5,
	--Convert(varchar(50),PA.partTransId) as FieldValue6
	--From tbl_PartnerAdminTransfer PA Inner join tbl_ScBankDevPartnerMaster SBP
	--	On PA.sBankDevelopId=SBP.sBankDevelopId
	--	Where PA.partTransId=@v_fieldName2Vc
	
END




IF @v_fieldName1Vc = 'getPaymentDetail' 
BEGIN
	--select paymentFor as FieldValue1, paymentInstType as FieldValue2, instRefNumber as FieldValue3, 
	--Convert(varchar(50),amount) as FieldValue4, 
	--IsNull(REPLACE(CONVERT(VARCHAR(11),instDate, 106), ' ', '-') ,'') as FieldValue5, 
	--IsNull(REPLACE(CONVERT(VARCHAR(11),instValidUpto, 106), ' ', '-') ,'') as FieldValue6,
	--IsNull((bankName) , '') as FieldValue7, comments as FieldValue8, 
	--REPLACE(CONVERT(VARCHAR(11),createdDate, 106), ' ', '-')  as FieldValue9,IsNull((branchName) , '') as FieldValue10
	--from tbl_TenderPayment Where tenderPaymentId=@v_fieldName2Vc
	
	select paymentFor as FieldValue1, paymentInstType as FieldValue2, instRefNumber as FieldValue3, 
	Convert(varchar(50),amount) as FieldValue4, 
	IsNull(CONVERT(VARCHAR(10),instDate,103),'') as FieldValue5, 
	IsNull(CONVERT(VARCHAR(10),instValidUpto,103),'') as FieldValue6,
	IsNull((bankName) , '') as FieldValue7, comments as FieldValue8, 
	CONVERT(VARCHAR(10),createdDate,103) as FieldValue9,IsNull((branchName) , '') as FieldValue10
	from tbl_TenderPayment Where tenderPaymentId=@v_fieldName2Vc
END


IF @v_fieldName1Vc = 'BidderSideTendervalidity' 
BEGIN
	--IF Not exists(SELECT * FROM tbl_TenderValAcceptance where tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc)
	--BEGIN
		SELECT cast(tbl_TenderValidityExtDate.valExtDtId as varchar(10)) as FieldValue1,IsNull(REPLACE(CONVERT(VARCHAR(11),lastPropValDt, 106), ' ', '-') ,'') as FieldValue2,IsNull(REPLACE(CONVERT(VARCHAR(11),newPropValDt, 106), ' ', '-') ,'') as FieldValue3,IsNull(REPLACE(CONVERT(VARCHAR(11),lastValAcceptDt, 106), ' ', '-') ,'') as FieldValue4,cast(tbl_TenderValidityExtDate.tenderId as varchar(10)) as FieldValue5 
			FROM tbl_TenderValidityExtDate inner join tbl_TenderValAcceptance on tbl_TenderValidityExtDate.valExtDtId=tbl_TenderValAcceptance.valExtDtId
			where  tbl_TenderValidityExtDate.tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc and tbl_TenderValAcceptance.status='Pending'

	  --SELECT cast(tbl_TenderValidityExtDate.valExtDtId as varchar(10)) as FieldValue1,IsNull(CONVERT(VARCHAR(10),lastPropValDt,103),'') as FieldValue2,IsNull(CONVERT(VARCHAR(10),newPropValDt,103),'') as FieldValue3,IsNull(CONVERT(VARCHAR(10),lastValAcceptDt,103),'') as FieldValue4,cast(tbl_TenderValidityExtDate.tenderId as varchar(10)) as FieldValue5 FROM tbl_FinalSubmission INNER JOIN tbl_TenderValidityExtDate 
	  --on tbl_FinalSubmission.tenderId=tbl_TenderValidityExtDate.tenderId 
	  --where tbl_FinalSubmission.tenderId=@v_fieldName2Vc and tbl_FinalSubmission.userId=@v_fieldName3Vc
    --END
END

IF @v_fieldName1Vc = 'BidderSideTendervalidityProcessed' 
BEGIN
	--IF exists(SELECT * FROM tbl_TenderValAcceptance where tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc)
	--BEGIN
		SELECT cast(tbl_TenderValidityExtDate.valExtDtId as varchar(10)) as FieldValue1,IsNull(REPLACE(CONVERT(VARCHAR(11),lastPropValDt, 106), ' ', '-') ,'') as FieldValue2,IsNull(REPLACE(CONVERT(VARCHAR(11),newPropValDt, 106), ' ', '-') ,'') as FieldValue3,IsNull(REPLACE(CONVERT(VARCHAR(11),lastValAcceptDt, 106), ' ', '-') ,'') as FieldValue4,cast(tbl_TenderValidityExtDate.tenderId as varchar(10)) as FieldValue5 
			FROM tbl_TenderValidityExtDate inner join tbl_TenderValAcceptance on tbl_TenderValidityExtDate.valExtDtId=tbl_TenderValAcceptance.valExtDtId
			where  tbl_TenderValidityExtDate.tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc and tbl_TenderValAcceptance.status='Approved' OR tbl_TenderValAcceptance.status='Reject'

	  --SELECT cast(tbl_TenderValidityExtDate.valExtDtId as varchar(10)) as FieldValue1,IsNull(CONVERT(VARCHAR(10),lastPropValDt,103),'') as FieldValue2,IsNull(CONVERT(VARCHAR(10),newPropValDt,103),'') as FieldValue3,IsNull(CONVERT(VARCHAR(10),lastValAcceptDt,103),'') as FieldValue4,cast(tbl_TenderValidityExtDate.tenderId as varchar(10)) as FieldValue5 FROM tbl_FinalSubmission INNER JOIN tbl_TenderValidityExtDate 
	  --on tbl_FinalSubmission.tenderId=tbl_TenderValidityExtDate.tenderId 
	  --where tbl_FinalSubmission.tenderId=@v_fieldName2Vc and tbl_FinalSubmission.userId=@v_fieldName3Vc
    --END
END
IF @v_fieldName1Vc = 'AmendmentWorkFlowStatus'  
BEGIN
	declare @wQuery8 varchar(5000)
	set @wQuery8 = 'select corrigendumStatus as FieldValue1,workFlowStatus as FieldValue2,CONVERT(varchar(50), tenderId) as FieldValue3 
	 from tbl_CorrigendumMaster
	 where corrigendumid='+@v_fieldName2Vc+'
	 
	  and (workFlowStatus in('+@v_fieldName3Vc+') or corrigendumStatus=''Approved'')'
	 --print @wQuery
	 exec (@wQuery8)
END
IF @v_fieldName1Vc = 'CheckPE'  
BEGIN
select convert(varchar(400),userId) as FieldValue1 from tbl_employeemaster e,tbl_EmployeeRoles er,tbl_employeeOffices o 
where  e.employeeId=er.employeeId and procurementRoleId=1  
 and er.employeeId=o.employeeId and officeId in(select officeId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
 
END


IF @v_fieldName1Vc = 'getTenderEvaluationinfo' -- for/officer/LotPackageDetail.jsp
BEGIN
	  --select distinct tbl_EmployeeMaster.employeeName as FieldValue1, 
	  select distinct 
	  dbo.f_GovUserName(tbl_CommitteeMembers.govUserId, 'tbl_EmployeeTrasfer') as FieldValue1, 
	  tbl_DesignationMaster.designationName as FieldValue2, 
	  case tbl_CommitteeMembers.memberRole when 'm' then 'Member' when 'cp' then 'Chairperson' else 'Member Secretary' end as FieldValue3, 
       REPLACE(CONVERT(VARCHAR(11), tbl_CommitteeMembers.appdate, 106), ' ', '-') + ' ' + convert(varchar(5), tbl_CommitteeMembers.appdate, 108) as FieldValue4, 
       dbo.f_initcap(case when tbl_CommitteeMembers.appStatus = 'approved' then 'Declaration given' else tbl_CommitteeMembers.appStatus end) as FieldValue5,
        convert(varchar(50), tbl_Committee.committeeId)  as FieldValue6,
        convert(varchar(50), tbl_EmployeeMaster.userid) as FieldValue7,
        (select convert(varchar(50),COUNT(comMemberId)) from 
		tbl_Committee,tbl_CommitteeMembers,tbl_EmployeeMaster,tbl_EmployeeOffices,
		tbl_DesignationMaster
		where  tbl_Committee.committeeId=tbl_CommitteeMembers.committeeId  and tbl_CommitteeMembers.userId=tbl_EmployeeMaster.userid
		and tbl_EmployeeMaster.employeeId=tbl_EmployeeOffices.employeeId and tbl_EmployeeOffices.designationId=tbl_DesignationMaster.designationId 
		 and  committeeType in ('TEC','PEC')
		 and tenderId=@v_fieldName2Vc and appStatus='approved') as  FieldValue8 ,tbl_CommitteeMembers.memberRole as FieldValue9,
		 tbl_CommitteeRoleMaster.comRoleId          
		from 
		tbl_Committee,tbl_CommitteeMembers,tbl_EmployeeMaster,tbl_EmployeeOffices,
		tbl_DesignationMaster,tbl_CommitteeRoleMaster
		where  tbl_Committee.committeeId=tbl_CommitteeMembers.committeeId  and tbl_CommitteeMembers.userId=tbl_EmployeeMaster.userid
		and tbl_EmployeeMaster.employeeId=tbl_EmployeeOffices.employeeId and tbl_EmployeeOffices.designationId=tbl_DesignationMaster.designationId 
		 and  committeeType in ('TEC','PEC')
		 and tenderId=@v_fieldName2Vc and tbl_CommitteeRoleMaster.comRole=tbl_CommitteeMembers.memberRole order by tbl_CommitteeRoleMaster.comRoleId
END

IF @v_fieldName1Vc = 'CheckCommitteeStatus'  
BEGIN
	select CAST(tenderId as varchar(20)) as FieldValue1 from tbl_Committee where committeStatus=@v_fieldName3Vc and tenderId=@v_fieldName2Vc
END

IF @v_fieldName1Vc = 'GetInitiatePQ' -- For InitiatePQ.jsp, PostQualification.jsp 
BEGIN
		/*
		SELECT dbo.tbl_CompanyMaster.companyName as FieldValue1, 
		ISNULL(dbo.tbl_PostQualification.postQualStatus, 'Pending') as FieldValue2, 
		CAST(dbo.tbl_FinalSubmission.userId as Varchar) as FieldValue3, 
		CAST(dbo.tbl_PostQualification.postQaulId as Varchar) as FieldValue4 
		FROM dbo.tbl_FinalSubmission INNER JOIN dbo.tbl_CompanyMaster  
		ON tbl_FinalSubmission.userId = dbo.tbl_CompanyMaster.userId LEFT JOIN dbo.tbl_PostQualification 
		ON tbl_FinalSubmission.userId = dbo.tbl_PostQualification.userId AND 
		tbl_FinalSubmission.tenderId = dbo.tbl_PostQualification.tenderId 
		Where tbl_FinalSubmission.bidSubStatus = 'finalsubmission' and dbo.tbl_FinalSubmission.tenderid = @v_fieldName2Vc
		*/
		
		select  case when  t.companyId=1 then firstname+' '+lastname else   c.companyName end as FieldValue1 ,convert(varchar(20),rank) as FieldValue2, convert(varchar(20),b.userId) as FieldValue3  
		from tbl_BidderRank b,tbl_TendererMaster t,tbl_CompanyMaster c,tbl_reportmaster rm,tbl_EvalRoundMaster erm
		 where b.userId=t.userId and t.companyId=c.companyId and b.tenderId=@v_fieldName2Vc and
		 erm.roundId = b.roundId and erm.userId = b.userId
		 and b.reportId=rm.reportId and isTORTER in('TER','PER')		  
		and b.pkgLotId=@v_fieldName3Vc 
		and b.roundId in (select MAX(erm.roundId) from tbl_EvalRoundMaster erm where erm.reportId = rm.reportId and erm.tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc)		
		and erm.userId not in(select userId from tbl_PostQualification where tenderId = @v_fieldName2Vc and pkgLotId = @v_fieldName3Vc)
		

		-- SELECT case when tbl_CompanyMaster.companyid=1 then firstname+' '+lastname else  dbo.tbl_CompanyMaster.companyName end as FieldValue1, 
		--'1' as FieldValue2, 
		--CAST(dbo.tbl_FinalSubmission.userId as Varchar) as FieldValue3, 
		--CAST(dbo.tbl_PostQualification.postQaulId as Varchar) as FieldValue4 
		--FROM dbo.tbl_FinalSubmission INNER JOIN dbo.tbl_TendererMaster  
		--ON tbl_FinalSubmission.userId = dbo.tbl_TendererMaster.userId and tbl_FinalSubmission.bidSubStatus = 'finalsubmission' 
		--INNER JOIN dbo.tbl_CompanyMaster 
		--ON tbl_CompanyMaster.companyId = dbo.tbl_TendererMaster.companyid and  dbo.tbl_TendererMaster.userid not in(select userId from tbl_PostQualification where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc)
		--LEFT JOIN dbo.tbl_PostQualification 
		--ON tbl_FinalSubmission.userId = dbo.tbl_PostQualification.userId AND 
		--tbl_FinalSubmission.tenderId = dbo.tbl_PostQualification.tenderId 
		--Where dbo.tbl_FinalSubmission.tenderid = @v_fieldName2Vc 
		--select dbo.f_getbiddercompany(br.userId) as FieldValue1,convert(varchar(20),br.rank) as FieldValue2,
		--convert(varchar(50),br.userId) as FieldValue3
		--from tbl_BidderRank br inner join tbl_ReportMaster rm on br.reportId = rm.reportId
		--where br.pkgLotId=@v_fieldName3Vc and br.tenderId=@v_fieldName2Vc and rm.isTORTER='TER' 
END

IF @v_fieldName1Vc = 'ViewPostQualification' -- For ViewPostQualificationDtl.jsp 
BEGIN
	--	SELECT siteVisit as FieldValue1, REPLACE(CONVERT(VARCHAR(11), siteVisitReqDt, 106), ' ', '-')  as FieldValue2, ISNULL(comments, '') as FieldValue3, 
	--Case siteVisitDate When '1900-01-01 00:00:00' Then '-' Else 	
	--	REPLACE(CONVERT(VARCHAR(11), siteVisitDate, 106), ' ', '-')  end as FieldValue4, siteVisitStatus as FieldValue5,  
	--	ISNULL(siteVisitComments, '') as FieldValue6, postQualStatus as FieldValue7, dbo.tbl_CompanyMaster.companyName as FieldValue8
	--	FROM dbo.tbl_PostQualification INNER JOIN dbo.tbl_CompanyMaster ON dbo.tbl_PostQualification.userId = dbo.tbl_CompanyMaster.userId 
	--	WHERE dbo.tbl_PostQualification.userId = @v_fieldName2Vc And tenderId = @v_fieldName3Vc 
	
	SELECT siteVisit as FieldValue1, 
	REPLACE(CONVERT(VARCHAR(11),siteVisitReqDt, 106), ' ', '-') + ' ' + CONVERT(VARCHAR(11), siteVisitReqDt, 108) as FieldValue2, 

ISNULL(tbl_PostQualification.comments, '') as FieldValue3, 
	Case siteVisitDate When '1900-01-01 00:00:00' Then '-' 
	Else 	
		REPLACE(CONVERT(VARCHAR(11),siteVisitDate, 106), ' ', '-') + ' ' + CONVERT(VARCHAR(11), siteVisitDate, 108)  
		end as FieldValue4, 
		siteVisitStatus as FieldValue5,  
		ISNULL(siteVisitComments, '') as FieldValue6, 
		postQualStatus as FieldValue7, 
		dbo.f_getbiddercompany(TM.userId) as FieldValue8
		FROM dbo.tbl_PostQualification Inner Join tbl_tendererMaster TM On tbl_PostQualification.userId=TM.userId		
		WHERE  dbo.tbl_PostQualification.userId = @v_fieldName2Vc And dbo.tbl_PostQualification.pkgLotId=@v_fieldName3Vc
END

IF @v_fieldName1Vc = 'GetUseridForPE' -- For TenderClosing.jsp 
BEGIN

	--SELECT CAST(userId as varchar(10)) as FieldValue1
	--	 FROM dbo.tbl_EmployeeMaster 
	--		WHERE employeeId in(select employeeId from tbl_EmployeeRoles where departmentId= 
	--		(select departmentId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
	--		and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='PE'))
	
	--select Convert(varchar(50),e.userId) as FieldValue1, 
	--e.employeeName as FieldValue2
	--from tbl_tendermaster  t,
	--	tbl_AppMaster a,
	--	tbl_employeemaster e 
	--where t.appId=a.appId 
	--		and tenderid=@v_fieldName2Vc 
	--		and a.employeeid=e.employeeId;
					
	select Convert(varchar(50),e.userId) as FieldValue1, 
	e.employeeName as FieldValue2,
	Convert(varchar(50),e.govUserId)  as FieldValue3
	from tbl_tendermaster  t,
		tbl_AppMaster a,
		tbl_EmployeeTrasfer e 
	where t.appId=a.appId 
			and tenderid=@v_fieldName2Vc 
			and a.employeeid=e.employeeId;
			
END

IF @v_fieldName1Vc = 'GetAppAuth'  --Get Approving Authority By packageId  --> Krish : For AddPackageDates.jsp
BEGIN
	SELECT CONVERT(VARCHAR(30), procurementRoleId) AS FieldValue1, pr.procurementRole AS FieldValue2 FROM 
	dbo.tbl_AppPackages ap INNER JOIN dbo.tbl_ProcurementRole pr ON 
	ap.approvingAuthEmpId = pr.procurementRoleId 
	WHERE ap.packageId = @v_fieldName2Vc
END

IF @v_fieldName1Vc = 'GetAppDetailById'  --Get Approving Authority By packageId  --> Krish : For AddPackageDates.jsp
BEGIN
	SELECT Case departmentType 
			When 'Ministry' Then
				[departmentName]
			When 'Division' Then
				(select [departmentName] from dbo.tbl_DepartmentMaster where departmentId = dp.parentDepartmentId)
			When 'Organization' Then
				Case When ((Select departmentType from tbl_DepartmentMaster where departmentId = dp.parentDepartmentId) = 'Ministry') Then 
					(select departmentName from tbl_DepartmentMaster where departmentId = dp.parentDepartmentId)
				else
					(select departmentName from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=dp.parentDepartmentId))
				end
			End AS FieldValue1,
	Case departmentType 
			When 'Ministry' Then
				'-'
			When 'Division' Then
				[departmentName]			
			When 'Organization' Then
				Case When ((Select departmentType from tbl_DepartmentMaster where departmentId = dp.parentDepartmentId) = 'Ministry') Then 
					' - '
				else
					(select [departmentName] from dbo.tbl_DepartmentMaster where departmentId = dp.parentDepartmentId)
				end
			End AS FieldValue2,
	Case departmentType 
			When 'Ministry' Then
				'-'
			When 'Division' Then
				'-'
			When 'Organization' Then
				[departmentName]			
			End AS FieldValue3, 
		officeName AS FieldValue4, st.stateName AS FieldValue6 FROM  
		dbo.tbl_DepartmentMaster dp INNER JOIN dbo.tbl_OfficeMaster om ON om.departmentId = dp.departmentId INNER JOIN 
        dbo.tbl_StateMaster st ON om.stateId = st.stateId  
       WHERE om.officeId = @v_fieldName2Vc 
	
END

IF @v_fieldName1Vc = 'GetAppPlannedDays'  --Get Planned Days By procurementRoleId  --> Krish : For AddPackageDates.jsp
BEGIN
	SELECT CONVERT(VARCHAR(30), expRptSubDays) AS FieldValue1, CONVERT(VARCHAR(30), expRptAppDays) AS FieldValue2, 
	CONVERT(VARCHAR(30), expNoaDays) AS FieldValue3 FROM 
	dbo.tbl_AppPlannedDays 
	WHERE procurementRoleId = @v_fieldName2Vc
END

IF @v_fieldName1Vc = 'GetFlagForTender'  --Get flag for tender if procurementNature = "services" and found in tbl_EvalServiceForms --> Krish : For AfterLoginTSC.jsp
BEGIN
	DECLARE @prNature VARCHAR(10), @flg CHAR(1)
	
	SET @flg = '0'
	
	SELECT @prNature = procurementNature FROM dbo.tbl_TenderDetails WHERE tenderId = @v_fieldName2Vc
	
	IF @prNature = 'Services'
	BEGIN
	
		IF EXISTS (SELECT 1 FROM dbo.tbl_EvalServiceForms WHERE tenderId = @v_fieldName2Vc)
		BEGIN
			SET @flg = '1'
		END
		ELSE
			SET @flg = '0'
		
	END
	ELSE
		SET @flg = '1'
	
	SELECT @flg AS FieldValue1
END
			
IF @v_fieldName1Vc = 'getTOSReportStatus'
BEGIN	
	Select Case When Exists (select top 1 tenderId from tbl_TosRptShare Where tenderId=@v_fieldName2Vc)
			Then 'yes'
			Else 'no'
			End as FieldValue1
END

IF @v_fieldName1Vc = 'GetTenderValidityExt' -- For TECProcess.jsp 
BEGIN
		Select CONVERT(VARCHAR(30), Row_Number() Over(Order by FieldValue3)) as FieldValue1, * From (SELECT dbo.tbl_TenderValidityExt.extSentToRole as FieldValue2, 
		CONVERT(VARCHAR(30), valExtDtId) as FieldValue3, tbl_TenderValidityExt.extStatus as  FieldValue4 
		FROM dbo.tbl_TenderValidityExt INNER JOIN
		dbo.tbl_TenderValidityExtDate 
		ON dbo.tbl_TenderValidityExt.valExtReqId = dbo.tbl_TenderValidityExtDate.valExtReqId 
		Where dbo.tbl_TenderValidityExt.extStatus = 'Approved' and dbo.tbl_TenderValidityExt.tenderId = @v_fieldName2Vc
		Union All 
		SELECT dbo.tbl_TenderValidityExtDate.extSentToRole as FieldValue2, CONVERT(VARCHAR(30), valExtDtId) as FieldValue3, extStatus as  FieldValue4 
		FROM dbo.tbl_TenderValidityExtDate 
		Where dbo.tbl_TenderValidityExtDate.extStatus = 'Approved' and dbo.tbl_TenderValidityExtDate.tenderId = @v_fieldName2Vc and 
		dbo.tbl_TenderValidityExtDate.valExtReqId = 0) a
		Order by FieldValue3 
		
END

IF @v_fieldName1Vc = 'GetCompanynameByUserid' -- For Evaluation module     -> Krish
BEGIN
		
		SELECT CASE CompanyName WHEN '-' THEN firstName+' '+lastName ELSE companyName END AS FieldValue1, 
		ISNULL((SELECT CONVERT(VARCHAR(30), DATEDIFF(DAY, GETDATE(), expectedComplDt)) from tbl_EvalBidderResp 
		Where tenderid = @v_fieldName3Vc and userid = l.userid), '0') as FieldValue2  
		FROM dbo.tbl_loginmaster l,dbo.tbl_TendererMaster t, dbo.tbl_CompanyMaster c
		WHERE l.userId=t.userId AND t.companyId=c.companyId AND l.userid = @v_fieldName2Vc
	
END

IF @v_fieldName1Vc = 'GetEvalBidderRespTenderId' -- For EvalClari.jsp --> Evaluation module     -> Krish
BEGIN
		
		SELECT CONVERT(VARCHAR(20), tenderId) as FieldValue1 FROM dbo.tbl_EvalBidderResp 
		WHERE tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc
		
END

IF @v_fieldName1Vc = 'GetEvalCPClarificationExist' -- For EvalClari.jsp --> Evaluation module     -> Krish
BEGIN
		
		SELECT '1' as FieldValue1 FROM dbo.tbl_EvalCpMemClarification  
		WHERE userId = @v_fieldName2Vc and memAnswerBy = @v_fieldName3Vc
		
END

IF @v_fieldName1Vc = 'GetEvalCPClarificationStatus' -- For EvalClari.jsp --> Evaluation module     -> Krish
BEGIN
		
		SELECT '1' as FieldValue1 FROM dbo.tbl_EvalCpMemClarification  
		WHERE userId = @v_fieldName2Vc and cpQuestionBy = @v_fieldName3Vc
		
END

IF @v_fieldName1Vc = 'GetTendervaliditydate' -- For EvalClariPostBidder.jsp --> Evaluation module     -> Krish
BEGIN
		
		SELECT ISNULL(REPLACE(CONVERT(VARCHAR(11), [dbo].[f_gettendervaliditydate](@v_fieldName2Vc), 103), ' ', '-') , '') as FieldValue1 
		
END

IF @v_fieldName1Vc = 'GetEvalFormQuesCount' -- For EvalClari.jsp --> Evaluation module     -> Krish
BEGIN
		
		SELECT ISNULL(CONVERT(VARCHAR(15), COUNT(evalQueId)), '0') as FieldValue1 from dbo.tbl_EvalFormQues 
		WHERE tenderId = @v_fieldName2Vc and userId = @v_fieldName3Vc
		
END

IF @v_fieldName1Vc = 'GetTenderExtensionReq' -- For TECProcess.jsp 
BEGIN
		SELECT cm.companyName as FieldValue1, tv.comments as FieldValue2, tv.status as FieldValue3, 
		REPLACE(CONVERT(VARCHAR(11), tv.tenderValAcceptDt, 106), ' ', '-')  as FieldValue4 from dbo.tbl_TenderValAcceptance tv
		Inner Join dbo.tbl_CompanyMaster cm ON tv.userId = cm.userId 
		Where tv.valExtDtId = @v_fieldName2Vc
		
END

IF @v_fieldName1Vc = 'IsFormOK' -- By: Krish - For Tender FormCheck
BEGIN
		
	-- 0 FOR FORM NOT EXISTS
	-- 1 FOR TABLE DOESN'T EXISTS 
	-- 2 FOR COLUMN 
	-- 3 FOR CELL 
	-- 4 FOR FORMULA 
	-- 5 OK

	SELECT @v_flag_Vc = '5', @v_formName_Vc = '', @v_tableName_Vc = ''
		
	IF EXISTS (SELECT 1 FROM tbl_TenderForms Where tenderSectionId IN 
		(SELECT tenderSectionId FROM tbl_TenderSection WHERE tenderStdId IN 
		(SELECT tenderStdId FROM tbl_Tenderstd Where tenderId = @v_fieldName2Vc) and contentType in ('Form','TOR')))
	BEGIN

		-- DECLARE CUSRSOR FOR FORMID
		DECLARE formIdNameCursor CURSOR FAST_FORWARD READ_ONLY FOR SELECT tenderFormId, formName FROM tbl_TenderForms Where tenderSectionId IN 
			(SELECT tenderSectionId FROM tbl_TenderSection WHERE tenderStdId IN 
			(SELECT tenderStdId FROM tbl_Tenderstd Where tenderId = @v_fieldName2Vc) and contentType in ('Form','TOR'))
							
		OPEN formIdNameCursor

		FETCH NEXT FROM formIdNameCursor INTO @v_formId_In, @v_formName_Vc

			WHILE @@FETCH_STATUS = 0 
			BEGIN
				
				IF EXISTS(SELECT 1 from dbo.tbl_TenderTables Where tenderFormId = @v_formId_In)
				BEGIN
				
					-- DECLARE CUSRSOR FOR TABLEID
					DECLARE tenderTblIdCursor CURSOR FAST_FORWARD READ_ONLY FOR SELECT tenderTableId, tableName FROM 
						dbo.tbl_TenderTables Where tenderFormId = @v_formId_In
				
					Open tenderTblIdCursor

					FETCH NEXT FROM tenderTblIdCursor INTO @v_tenderTableId_In, @v_tableName_Vc

						WHILE @@FETCH_STATUS = 0 
						BEGIN
						
							IF NOT EXISTS (SELECT 1 from dbo.tbl_TenderColumns Where tenderTableId = @v_tenderTableId_In)
							BEGIN
								
								SET @v_flag_Vc = '2'  -- 3 FOR TABLE-COLUMN DOESN'T EXISTS
								BREAK
											
							END
							IF NOT EXISTS (SELECT 1 FROM dbo.tbl_TenderCells WHERE tenderTableId = @v_tenderTableId_In)
							BEGIN
								
								SET @v_flag_Vc = '3'  -- 3 FOR TABLE-CELL DOESN'T EXISTS
								BREAK
								
							END
							ELSE
							BEGIN
								-- Declare cusrsor for columnid
								DECLARE tenderColIdCursor CURSOR FAST_FORWARD READ_ONLY FOR SELECT columnid from 
									dbo.tbl_TenderColumns Where tenderTableId = @v_tenderTableId_In AND filledBy = 3
									
								OPEN tenderColIdCursor
								
								FETCH NEXT FROM tenderColIdCursor INTO @v_colId_In
								
									WHILE @@FETCH_STATUS = 0 
									BEGIN
						
											IF NOT EXISTS (SELECT 1 FROM dbo.tbl_Tenderformula 
												WHERE tenderTableId = @v_tenderTableId_In AND columnid = @v_colId_In)
											BEGIN
													SET @v_flag_Vc = '4'  -- 4 FOR FORMULA DOESN'T EXISTS
													BREAK					
													--SELECT * FROM dbo.tbl_Tenderformula WHERE tenderTableId = 1 AND columnid = 4 
											END
											
										FETCH NEXT FROM tenderColIdCursor INTO @v_colId_In
				
									END

								CLOSE tenderColIdCursor
								DEALLOCATE tenderColIdCursor
							
							END
							
							FETCH NEXT FROM tenderTblIdCursor INTO @v_tenderTableId_In, @v_tableName_Vc
				
						END
			
					CLOSE tenderTblIdCursor
					DEALLOCATE tenderTblIdCursor
			
				END
				ELSE
				BEGIN
					SET @v_flag_Vc = '1'  -- 1 FOR TABLE DOESN'T EXISTS
					BREAK
				END
			
				IF @v_flag_Vc IN ('2', '3', '4')
					BREAK;
					
				FETCH NEXT FROM formIdNameCursor INTO @v_formId_In, @v_formName_Vc
				
			END
			
			CLOSE formIdNameCursor
			DEALLOCATE formIdNameCursor
			
	END
	ELSE
	BEGIN
			
			SET @v_flag_Vc = '0'  -- 0 FOR FORM DOESN'T EXISTS
			
	END
		
	SELECT @v_flag_Vc AS FieldValue1, @v_formName_Vc AS FieldValue2, @v_tableName_Vc AS FieldValue3
		
END

IF @v_fieldName1Vc = 'IsTemplateFormOK' -- By: Krish - For Template FormCheck
BEGIN
		
	-- -1 SECTION NOT THERE
	-- 0 FOR FORM NOT EXISTS
	-- 1 FOR TABLE DOESN'T EXISTS 
	-- 2 FOR COLUMN 
	-- 3 FOR CELL 
	-- 4 FOR FORMULA 
	-- 5 OK

	SELECT @v_flag_Vc = '5', @v_formName_Vc = '', @v_tableName_Vc = '', @v_sectionName_Vc = ''
	
	IF EXISTS (SELECT 1 FROM dbo.tbl_TemplateSections WHERE templateId = @v_fieldName2Vc AND contentType = 'Form')
	BEGIN

		-- DECLARE CUSRSOR FOR SECTIONID
		DECLARE tmpSectionIdCursor CURSOR FAST_FORWARD READ_ONLY FOR SELECT sectionId, sectionName FROM dbo.tbl_TemplateSections 
				WHERE templateId = @v_fieldName2Vc AND contentType = 'Form'
							
		OPEN tmpSectionIdCursor

		FETCH NEXT FROM tmpSectionIdCursor INTO @v_sectionId_In, @v_sectionName_Vc

			WHILE @@FETCH_STATUS = 0 
			BEGIN
				
				-- CHECKED FOR isPriceBid = "NO"
				IF EXISTS(SELECT 1 from dbo.tbl_TemplateSectionForm WHERE sectionId = @v_sectionId_In AND 
								templateId = @v_fieldName2Vc AND isPriceBid = 'no')
				BEGIN
				
					-- Declare cusrsor for formId
					DECLARE formIdNameCursor CURSOR FAST_FORWARD READ_ONLY FOR SELECT formId, formName FROM 
						dbo.tbl_TemplateSectionForm WHERE sectionId = @v_sectionId_In AND templateId = @v_fieldName2Vc AND isPriceBid = 'no'
				
					Open formIdNameCursor

					FETCH NEXT FROM formIdNameCursor INTO @v_formId_In, @v_formName_Vc

						WHILE @@FETCH_STATUS = 0 
						BEGIN
							
							IF EXISTS(SELECT 1 from dbo.tbl_Templatetables Where formId = @v_formId_In AND sectionId = @v_sectionId_In)
							BEGIN
							
								-- DECLARE CUSRSOR FOR TABLEID
								DECLARE tenderTblIdCursor CURSOR FAST_FORWARD READ_ONLY FOR SELECT tableId, tableName FROM 
									dbo.tbl_Templatetables Where formId = @v_formId_In AND sectionId = @v_sectionId_In
							
								Open tenderTblIdCursor

								FETCH NEXT FROM tenderTblIdCursor INTO @v_tenderTableId_In, @v_tableName_Vc

									WHILE @@FETCH_STATUS = 0 
									BEGIN
									
										IF NOT EXISTS (SELECT 1 from dbo.tbl_Templatecolumns 
											Where tableId = @v_tenderTableId_In AND formId = @v_formId_In AND sectionId = @v_sectionId_In)
										BEGIN
											
											SET @v_flag_Vc = '2'  -- 3 FOR TABLE-COLUMN DOESN'T EXISTS
											BREAK
														
										END
										IF NOT EXISTS (SELECT 1 FROM dbo.tbl_Templatecells WHERE tableId = @v_tenderTableId_In)
										BEGIN
											
											SET @v_flag_Vc = '3'  -- 3 FOR TABLE-CELL DOESN'T EXISTS
											BREAK
											
										END
										ELSE
										BEGIN
											-- Declare cusrsor for columnid
											DECLARE tenderColIdCursor CURSOR FAST_FORWARD READ_ONLY FOR SELECT columnId from 
												dbo.tbl_Templatecolumns Where tableId = @v_tenderTableId_In AND formId = @v_formId_In 
												AND sectionId = @v_sectionId_In AND filledBy = 3
												
											OPEN tenderColIdCursor
											
											FETCH NEXT FROM tenderColIdCursor INTO @v_colId_In
											
												WHILE @@FETCH_STATUS = 0 
												BEGIN
									
														IF NOT EXISTS (SELECT 1 FROM dbo.tbl_Templateformulas 
															WHERE tableId = @v_tenderTableId_In AND columnid = @v_colId_In
															AND formId = @v_formId_In)
														BEGIN
																SET @v_flag_Vc = '4'  -- 4 FOR FORMULA DOESN'T EXISTS
																BREAK					
														END
														
													FETCH NEXT FROM tenderColIdCursor INTO @v_colId_In
							
												END

											CLOSE tenderColIdCursor
											DEALLOCATE tenderColIdCursor
										
										END
										
										FETCH NEXT FROM tenderTblIdCursor INTO @v_tenderTableId_In, @v_tableName_Vc
							
									END
						
								CLOSE tenderTblIdCursor
								DEALLOCATE tenderTblIdCursor
						
							END
							ELSE
							BEGIN
								SET @v_flag_Vc = '1'  -- 1 FOR TABLE DOESN'T EXISTS
								BREAK
							END
						
							IF @v_flag_Vc IN ('2', '3', '4')
								BREAK;
							
							FETCH NEXT FROM formIdNameCursor INTO @v_formId_In, @v_formName_Vc
				
						END
			
					CLOSE formIdNameCursor
					DEALLOCATE formIdNameCursor
			
				END
				ELSE IF EXISTS(SELECT 1 from dbo.tbl_TemplateSectionForm WHERE sectionId = @v_sectionId_In AND 
								templateId = @v_fieldName2Vc AND isPriceBid = 'yes')
				BEGIN
				
					-- Declare cusrsor for formId
					DECLARE formIdNameCursor CURSOR FAST_FORWARD READ_ONLY FOR SELECT formId, formName FROM 
						dbo.tbl_TemplateSectionForm WHERE sectionId = @v_sectionId_In AND templateId = @v_fieldName2Vc AND isPriceBid = 'no'
				
					Open formIdNameCursor

					FETCH NEXT FROM formIdNameCursor INTO @v_formId_In, @v_formName_Vc

						WHILE @@FETCH_STATUS = 0 
						BEGIN
							
							IF EXISTS(SELECT 1 from dbo.tbl_Templatetables Where formId = @v_formId_In AND sectionId = @v_sectionId_In)
							BEGIN
							
								-- DECLARE CUSRSOR FOR TABLEID
								DECLARE tenderTblIdCursor CURSOR FAST_FORWARD READ_ONLY FOR SELECT tableId, tableName FROM 
									dbo.tbl_Templatetables Where formId = @v_formId_In AND sectionId = @v_sectionId_In
							
								Open tenderTblIdCursor

								FETCH NEXT FROM tenderTblIdCursor INTO @v_tenderTableId_In, @v_tableName_Vc

									WHILE @@FETCH_STATUS = 0 
									BEGIN
									
										IF NOT EXISTS (SELECT 1 from dbo.tbl_Templatecolumns 
											Where tableId = @v_tenderTableId_In AND formId = @v_formId_In AND sectionId = @v_sectionId_In)
										BEGIN
											
											SET @v_flag_Vc = '2'  -- 3 FOR TABLE-COLUMN DOESN'T EXISTS
											BREAK
														
										END
										ELSE
										BEGIN
											-- Declare cusrsor for columnid
											DECLARE tenderColIdCursor CURSOR FAST_FORWARD READ_ONLY FOR SELECT columnId from 
												dbo.tbl_Templatecolumns Where tableId = @v_tenderTableId_In AND formId = @v_formId_In 
												AND sectionId = @v_sectionId_In AND filledBy = 3
												
											OPEN tenderColIdCursor
											
											FETCH NEXT FROM tenderColIdCursor INTO @v_colId_In
											
												WHILE @@FETCH_STATUS = 0 
												BEGIN
									
														IF NOT EXISTS (SELECT 1 FROM dbo.tbl_Templateformulas 
															WHERE tableId = @v_tenderTableId_In AND columnid = @v_colId_In
															AND formId = @v_formId_In)
														BEGIN
																SET @v_flag_Vc = '4'  -- 4 FOR FORMULA DOESN'T EXISTS
																BREAK					
														END
														
													FETCH NEXT FROM tenderColIdCursor INTO @v_colId_In
							
												END

											CLOSE tenderColIdCursor
											DEALLOCATE tenderColIdCursor
										
										END
										
										FETCH NEXT FROM tenderTblIdCursor INTO @v_tenderTableId_In, @v_tableName_Vc
							
									END
						
								CLOSE tenderTblIdCursor
								DEALLOCATE tenderTblIdCursor
						
							END
							ELSE
							BEGIN
								SET @v_flag_Vc = '1'  -- 1 FOR TABLE DOESN'T EXISTS
								BREAK
							END
						
							IF @v_flag_Vc IN ('2', '4')
								BREAK;
							
							FETCH NEXT FROM formIdNameCursor INTO @v_formId_In, @v_formName_Vc
				
						END
			
					CLOSE formIdNameCursor
					DEALLOCATE formIdNameCursor
			
				END
				ELSE
				BEGIN
					
					SET @v_flag_Vc = '0'  -- 0 FOR FORM DOESN'T EXISTS
					BREAK;
					
				END
			
				IF @v_flag_Vc IN ('0', '1', '2', '3', '4')
					BREAK;
					
				FETCH NEXT FROM tmpSectionIdCursor INTO @v_sectionId_In, @v_sectionName_Vc
				
			END
			
			CLOSE tmpSectionIdCursor
			DEALLOCATE tmpSectionIdCursor
			
	END
	ELSE
	BEGIN
			
			SET @v_flag_Vc = '-1'  -- 0 FOR TEMPLATE SECTION DOESN'T EXISTS
			
	END
		
	SELECT @v_flag_Vc AS FieldValue1, @v_formName_Vc AS FieldValue2, @v_tableName_Vc AS FieldValue3, @v_sectionName_Vc as FieldValue4

END

IF @v_fieldName1Vc = 'DumpForm' -- By: Krish - For Dump Form
BEGIN

-- 0 ERROR 
-- 1 OK

	SELECT @v_flag_Vc = '1'
		
		BEGIN TRY
		
			BEGIN TRANSACTION frm
			
				SET NOCOUNT ON;
				
				-- 1ST DUMP FORM DATA
				INSERT INTO dbo.tbl_TenderForms (tenderSectionId, templateFormId, filledBy, formName, formHeader, formFooter, noOfTables, 
																isMultipleFilling, isEncryption, isPriceBid, isMandatory, formStatus,pkgLotId)
				SELECT tenderSectionId, 0, filledBy, formName, formHeader, formFooter, noOfTables, isMultipleFilling, isEncryption, 
					isPriceBid, isMandatory, formStatus,pkgLotId FROM dbo.tbl_TenderForms WHERE tenderFormId = @v_fieldName2Vc
				
				SELECT @v_newFormId_In = SCOPE_IDENTITY() 
				
				-- DECLARE CUSRSOR FOR TABLEID
				DECLARE tenderTblIdCursor CURSOR FAST_FORWARD READ_ONLY FOR SELECT tenderTableId FROM 
					dbo.tbl_TenderTables Where tenderFormId = @v_fieldName2Vc
			
				Open tenderTblIdCursor

				FETCH NEXT FROM tenderTblIdCursor INTO @v_tenderTableId_In

					WHILE @@FETCH_STATUS = 0 
					BEGIN
					
							-- NOW DUMP TABLE DATA
							INSERT INTO dbo.tbl_TenderTables (tenderFormId, tableName, tableHeader, tableFooter, noOfRows, noOfCols, isMultipleFilling, 
																			templatetableId)
								SELECT @v_newFormId_In, tableName, tableHeader, tableFooter, noOfRows, noOfCols, isMultipleFilling, 
									templatetableId FROM dbo.tbl_TenderTables WHERE tenderTableId = @v_tenderTableId_In
							
							SELECT @v_newTenderTableId_In = SCOPE_IDENTITY() 
							
							-- DUMP COLUMN DATA
							INSERT INTO dbo.tbl_TenderColumns (tenderTableId, columnId, columnHeader, dataType, filledBy, columnType, sortOrder, 
																				showorhide, templateTableId)
								SELECT @v_newTenderTableId_In, columnId, columnHeader, dataType, filledBy, columnType, sortOrder, showorhide, 
									templateTableId FROM dbo.tbl_TenderColumns WHERE tenderTableId = @v_tenderTableId_In
											
							-- DUMP CELLS DATA
							INSERT INTO dbo.tbl_TenderCells (tenderTableId,rowId,cellDatatype,cellvalue,columnId,templateTableId,templateColumnId,
																		cellId,colId,tenderColId)
								SELECT @v_newTenderTableId_In, rowId, cellDatatype,cellvalue,columnId,templateTableId,templateColumnId,cellId,colId,tenderColId FROM 
								dbo.tbl_TenderCells WHERE tenderTableId = @v_tenderTableId_In
							
							-- DUMP FORMULA DATA
							INSERT INTO dbo.tbl_Tenderformula (tenderFormId, tenderTableId, columnId, formula, isGrandTotal)
								SELECT @v_newFormId_In, @v_newTenderTableId_In, columnId, formula, isGrandTotal 
								FROM dbo.tbl_Tenderformula WHERE tenderTableId = @v_tenderTableId_In
						
						FETCH NEXT FROM tenderTblIdCursor INTO @v_tenderTableId_In
			
					END

				CLOSE tenderTblIdCursor
				DEALLOCATE tenderTblIdCursor
				
				-- SET SUCCESSFUL FLAG
				SET @v_flag_Vc = '1'
			
			COMMIT TRANSACTION frm
			
		END TRY
		BEGIN CATCH
		
			BEGIN
					SET @v_flag_Vc = '0'
					ROLLBACK TRANSACTION frm
			END
			
		END CATCH

	SELECT @v_flag_Vc AS FieldValue1
	
END

IF @v_fieldName1Vc = 'GetCompanyByUserId' 
BEGIN
		SELECT dbo.tbl_CompanyMaster.companyName as FieldValue1 FROM dbo.tbl_CompanyMaster  
		Where dbo.tbl_CompanyMaster.userId = @v_fieldName2Vc
END

IF @v_fieldName1Vc = 'GetEvalMemStatusCount'   -- FOR EvalClari.JSP :- krish
BEGIN
		SELECT CONVERT(VARCHAR(30), COUNT(evalMemStatusId)) as FieldValue1 FROM dbo.tbl_EvalMemStatus WHERE tenderId = @v_fieldName2Vc
		
END

IF @v_fieldName1Vc = 'GetEvalMemfinalStatusCount'   -- FOR EvalClari.JSP :- krish
BEGIN
		SELECT CONVERT(VARCHAR(30), COUNT(evaBidderStatusId)) as FieldValue1 FROM dbo.tbl_EvalBidderStatus WHERE tenderId = @v_fieldName2Vc
END


IF @v_fieldName1Vc = 'IsTEC' 
BEGIN
		
		SELECT CONVERT(VARCHAR(30),tenderId) AS FieldValue1 FROM dbo.tbl_EvalSentQueToCp Where tenderId = @v_fieldName2Vc 
		AND sentBy = @v_fieldName3Vc
		
END

IF @v_fieldName1Vc = 'GetTenderTdsInfo'
BEGIN
 
select distinct  convert(varchar(50),its.tenderIttHeaderId) as FieldValue1 ,
case when tenderIttRefrence=0 then '' else ittHeaderName end FieldValue8 ,
case when tenderIttRefrence=0 then '' else convert(varchar(50),tenderIttClauseId)  end FieldValue2,
case when tenderIttRefrence=0 then '' else ittClauseName end FieldValue3,
case when tenderIttRefrence=0 then '' else convert(varchar(50),tenderIttSubClauseId) end FieldValue4,
case when tenderIttRefrence=0 then '' else ittSubClauseName end FieldValue5,
convert(varchar(50),tenderTdsSubClauseId) FieldValue7 ,
tenderTdsClauseName FieldValue6,convert(varchar(50),tenderTdsSubClauseId) FieldValue9,
 convert(varchar(10), orderNumber)  FieldValue10       
from (select h.*,k.tenderIttClauseId 
       
      ,[ittClauseName], l.tenderIttSubClauseId,l.[ittSubClauseName] from (select * from tbl_TenderIttHeader where tenderIttHeaderId=@v_fieldName2Vc )h
left outer join
(select [tenderIttClauseId],
       i.tenderIttHeaderId 
      ,[ittClauseName] from tbl_TenderittClause c,tbl_TenderIttHeader i where 
i.tenderIttHeaderId =@v_fieldName2Vc and i.tenderIttHeaderId=c.tendederIttHeaderId ) k
on
h.tenderIttHeaderId=k.tenderIttHeaderId 
inner  join
(select tenderIttSubClauseId,
		c.tenderIttClauseId
      ,[ittSubClauseName]
      ,[isTdsApplicable] from
 tbl_TenderIttHeader i,
tbl_TenderIttClause c,Tbl_TenderIttSubClause s 
 where i.tenderIttHeaderId=c.tendederIttHeaderId
 and i.tenderIttHeaderId=@v_fieldName2Vc and c.tenderIttClauseId=s.tenderIttClauseId and isTdsApplicable='yes')l
 on k.tenderIttClauseId=l.tenderIttClauseId ) its
 left outer join (select * from tbl_TenderTdsSubClause where tenderIttHeaderId=@v_fieldName2Vc) tds
 on its.tenderIttHeaderId=tds.tenderIttHeaderId  and (its.tenderIttSubClauseId=tds.tenderIttRefrence
 or tds.tenderIttRefrence=0)
order by  FieldValue10 
 
END

 If @v_fieldName1Vc='EvalRptDocInfo'
   Begin
		Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(evalRptDocId  as varchar(10)) as FieldValue4 
		from tbl_EvalRptDocs Where evalRptId=@v_fieldName2Vc
   End
   
IF @v_fieldName1Vc = 'getfrmnamebytidandlid'     -- For tenderer/BidPreperation.jsp
BEGIN
	declare @temp as varchar(100),@lefttemp as varchar(50),@righttemp as varchar(50)
	
	select @lefttemp=LEFT(@v_fieldName3Vc, CHARINDEX(',', @v_fieldName3Vc) - 1)
	select @righttemp=SUBSTRING(@v_fieldName3Vc, CHARINDEX(',', @v_fieldName3Vc) + 1, LEN(@v_fieldName3Vc) - CHARINDEX(',', @v_fieldName3Vc))	
		
	IF @lefttemp <> '0'
	BEGIN
		select FieldValue1,FieldValue2,FieldValue3,FieldValue4,FieldValue5,FieldValue6,FieldValue7,
		 FieldValue8,FieldValue9,cast(tenderformid as varchar(20)) as FieldValue10
		 from (SELECT tenderformid,formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
		'' as FieldValue6, 
		 ISNULL(tr.reportType, '') as FieldValue7, isEncryption as FieldValue8  ,isMultipleFilling as FieldValue9
		 FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		 ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		 dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId  and  tenderId = @v_fieldName2Vc Left Outer Join 
		 dbo.tbl_TenderResultSharing tr On td.tenderId = tr.tenderId And tf.tenderFormId = tr.formId 
		 Where td.tenderId = @v_fieldName2Vc and packageLotId = @lefttemp)a 
		 Order By FieldValue3
		 
	
		 --select FieldValue1,FieldValue2,FieldValue3,FieldValue4,FieldValue5,FieldValue6,FieldValue7,
		 --FieldValue8,FieldValue9,convert(varchar(10),isnull(bidId,0)) FieldValue10
		 --from (SELECT tenderformid,formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
		 --Case When Exists (select tbpd.tenderFormId from tbl_TenderBidPlainData tbpd Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
		 --ISNULL(tr.reportType, '') as FieldValue7, isEncryption as FieldValue8  ,isMultipleFilling as FieldValue9
		 --FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		 --ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		 --dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId  and  tenderId = @v_fieldName2Vc Left Outer Join 
		 --dbo.tbl_TenderResultSharing tr On td.tenderId = tr.tenderId And tf.tenderFormId = tr.formId 
		 --Where td.tenderId = @v_fieldName2Vc and packageLotId = @lefttemp)a left outer join(select bidid,tenderid,tenderFormId from
		 -- tbl_TenderBidForm where tenderId=@v_fieldName2Vc and userid=@righttemp) b
		 --on a.tenderFormId=b.tenderformid 
		 --Order By FieldValue3
		
	END
	ELSE
	BEGIN
	
		select FieldValue1,FieldValue2,FieldValue3,FieldValue4,FieldValue5,FieldValue6,FieldValue7,
		 FieldValue8,FieldValue9,cast(tenderformid as varchar(20)) as FieldValue10
		 from (SELECT tenderformid,formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
		 '' as FieldValue6, 
		 ISNULL(tr.reportType, '') as FieldValue7, isEncryption as FieldValue8  ,isMultipleFilling as FieldValue9
		 FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		 ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		 dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId  and  tenderId = @v_fieldName2Vc Left Outer Join 
		 dbo.tbl_TenderResultSharing tr On td.tenderId = tr.tenderId And tf.tenderFormId = tr.formId 
		 Where td.tenderId = @v_fieldName2Vc)a 
		 Order By FieldValue3
		 
		--select FieldValue1,FieldValue2,FieldValue3,FieldValue4,FieldValue5,FieldValue6,FieldValue7,
		-- FieldValue8,FieldValue9,convert(varchar(10),isnull(bidId,0)) FieldValue10
		-- from (SELECT tenderformid,formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
		-- Case When Exists (select tbpd.tenderFormId from tbl_TenderBidPlainData tbpd Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
		-- ISNULL(tr.reportType, '') as FieldValue7, isEncryption as FieldValue8  ,isMultipleFilling as FieldValue9
		-- FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		-- ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		-- dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId  and  tenderId = @v_fieldName2Vc Left Outer Join 
		-- dbo.tbl_TenderResultSharing tr On td.tenderId = tr.tenderId And tf.tenderFormId = tr.formId 
		-- Where td.tenderId = @v_fieldName2Vc)a left outer join(select bidid,tenderid,tenderFormId from
		--  tbl_TenderBidForm where tenderId=@v_fieldName2Vc and userid=@righttemp) b
		-- on a.tenderFormId=b.tenderformid 
		-- Order By FieldValue3
	
	END
		
END

If @v_fieldName1Vc='GetCommiteeDetails'
Begin
	select committeeName as FieldValue1, cast(minMembers as varchar(10)) as FieldValue2 from tbl_Committee where tenderId=@v_fieldName2Vc and committeeType in ('TEC','PEC')
End

If @v_fieldName1Vc='GetCommiteeDetailsTSC'
Begin
	select committeeName as FieldValue1, cast(minMembers as varchar(10)) as FieldValue2 from tbl_Committee where tenderId=@v_fieldName2Vc and committeeType in ('TSC')
End

If @v_fieldName1Vc='CheckDocFeesPaid'
Begin
	  
  select convert(varchar(20),userid) as FieldValue1 from tbl_TenderPayment
   where paymentFor='Document Fees' and tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc
End

If @v_fieldName1Vc='GetDocFeesMode'
Begin
	select case when docFeesMode='' then 'Free' else 'Paid' End as FieldValue1
  from tbl_TenderDetails where tenderid=@v_fieldName2Vc
End

If @v_fieldName1Vc='Get'
Begin
	select emailid as FieldValue1, cast(l.userId as varchar(10)) as FieldValue2,employeename as FieldValue3
	 from tbl_loginmaster l,tbl_EmployeeMaster e where l.userId=e.userId and l.userId=@v_fieldName2Vc
End
If @v_fieldName1Vc='GetEmployeeDetail'
Begin
	select emailid as FieldValue1, cast(l.userId as varchar(10)) as FieldValue2,employeename as FieldValue3
	 from tbl_loginmaster l,tbl_EmployeeMaster e where l.userId=e.userId and l.userId=@v_fieldName2Vc
End

If @v_fieldName1Vc='GetEvaluation'
Begin
	Declare @minTemp as varchar(10),@maxTemp as varchar(10)

	set @minTemp=(select  count(tbl_CommitteeMembers.appStatus)
			from 
			tbl_Committee,tbl_CommitteeMembers,tbl_EmployeeMaster,tbl_EmployeeOffices,
			tbl_DesignationMaster
			where  tbl_Committee.committeeId=tbl_CommitteeMembers.committeeId  and tbl_CommitteeMembers.userId=tbl_EmployeeMaster.userid
			and tbl_EmployeeMaster.employeeId=tbl_EmployeeOffices.employeeId and tbl_EmployeeOffices.designationId=tbl_DesignationMaster.designationId 
			 and  committeeType in ('TEC','PEC')
			 and tenderId=@v_fieldName2Vc and tbl_CommitteeMembers.appStatus='approved')
			 
	set @maxTemp=(select minMembers from tbl_Committee where tenderId=@v_fieldName2Vc and committeeType in ('TSC','PSC'))
			 
	if (@minTemp>=@maxTemp)
		BEGIN
			select 'yes' as FieldValue1,evalRptName as FieldValue2,evalRptStatus as FieldValue3,cast(evalRptId as varchar(10)) as FieldValue4 from dbo.tbl_EvalReportMaster where tenderid=20
		END
	ELSE
		BEGIN
			select 'no' as FieldValue1
		END
END
If @v_fieldName1Vc='GetTscMemberStatus'
Begin
 select a.*,isnull(convert(varchar(10),b.userid),'Pending') FieldValue7,
 REPLACE(CONVERT(VARCHAR(11),Signdt, 106), ' ', '-') + ' ' + CONVERT(VARCHAR(11), Signdt, 108) FieldValue8 from  (select convert(varchar(50),em.userId) as FieldValue1,em.employeeName as FieldValue2, cm.memberRole as FieldValue3,
cm.memberFrom as FieldValue4, c.committeeName  as FieldValue5, convert(varchar(50),c.committeeId) as FieldValue6 from tbl_Committee c,tbl_CommitteeMembers cm,tbl_EmployeeMaster em,tbl_EmployeeOffices eo,
tbl_DesignationMaster dm
 where   c.committeeId=cm.committeeId  and cm.userId=em.userid
 and em.employeeId=eo.employeeId and eo.designationId=dm.designationId 
 and  committeeType='TSC' and tenderId=@v_fieldName2Vc )a
 left outer join 
 (select userid,Signdt from tbl_EvalReportMaster e,tbl_evalReportSign s
where e.evalRptId=s.evalRptId and tenderid=@v_fieldName2Vc and s.evalRptId=@v_fieldName3Vc
) b
on a.FieldValue1=b.userid
End

IF @v_fieldName1Vc = 'getTenderEvaluationTSCinfo' -- for/officer/LotPackageDetail.jsp
BEGIN
	  select distinct tbl_EmployeeMaster.employeeName as FieldValue1, 
	  tbl_DesignationMaster.designationName as FieldValue2, 
	  case tbl_CommitteeMembers.memberRole when 'm' then 'Member' when 'cp' then 'Chair Person' else 'Member Secretary' end as FieldValue3, 
       REPLACE(CONVERT(VARCHAR(11), tbl_CommitteeMembers.createdDate, 106), ' ', '-') + ' ' + convert(varchar(5), tbl_CommitteeMembers.createdDate, 108) as FieldValue4, 
        tbl_CommitteeMembers.appStatus as FieldValue5,
        convert(varchar(50), tbl_Committee.committeeId)  as FieldValue6,
        convert(varchar(50), tbl_EmployeeMaster.userid) as FieldValue7,
        (select convert(varchar(50),COUNT(comMemberId)) from 
		tbl_Committee,tbl_CommitteeMembers,tbl_EmployeeMaster,tbl_EmployeeOffices,
		tbl_DesignationMaster 
		where  tbl_Committee.committeeId=tbl_CommitteeMembers.committeeId  and tbl_CommitteeMembers.userId=tbl_EmployeeMaster.userid
		and tbl_EmployeeMaster.employeeId=tbl_EmployeeOffices.employeeId and tbl_EmployeeOffices.designationId=tbl_DesignationMaster.designationId 
		 and  committeeType in ('TSC')
		 and tenderId=@v_fieldName2Vc and appStatus='approved') as  FieldValue8 ,
		 tbl_CommitteeMembers.memberRole as FieldValue9 ,
		 tbl_CommitteeRoleMaster.comRoleId            
		from 
		tbl_Committee,tbl_CommitteeMembers,tbl_EmployeeMaster,tbl_EmployeeOffices,
		tbl_DesignationMaster , tbl_CommitteeRoleMaster
		where  tbl_Committee.committeeId=tbl_CommitteeMembers.committeeId  and tbl_CommitteeMembers.userId=tbl_EmployeeMaster.userid
		and tbl_EmployeeMaster.employeeId=tbl_EmployeeOffices.employeeId and tbl_EmployeeOffices.designationId=tbl_DesignationMaster.designationId 
		 and  committeeType in ('TSC')
		 and tenderId=@v_fieldName2Vc 
		 and tbl_CommitteeRoleMaster.comRole=tbl_CommitteeMembers.memberRole
		 order by tbl_CommitteeRoleMaster.comRoleId
END

IF @v_fieldName1Vc = 'getDocumentList' -- for/officer/EvalCommTSC.jsp
BEGIN
select evalRptName as FieldValue1,cast((select evalRptClrId from tbl_EvalReportQry where tbl_EvalReportQry.evalRptId=tbl_EvalReportMaster.evalRptId) as varchar(100)) as FieldValue2 from tbl_EvalReportMaster where tenderid=@v_fieldName2Vc and evalRptId in (select evalRptId from dbo.tbl_EvalReportQry)
END

IF @v_fieldName1Vc = 'getDocumentDetails' -- officer/SendReportTEC.jsp
BEGIN
		Select cast(evalRptDocId as varchar(10)) as FieldValue1,cast(tbl_EvalRptDocs.evalRptId as varchar(10)) as FieldValue2,evalRptName as FieldValue3,documentName as FieldValue4, 
		cast((SELECT  UserId 
		FROM  tbl_Committee, tbl_CommitteeMembers where 
		tbl_Committee.committeeId = tbl_CommitteeMembers.committeeId
		and tenderId = @v_fieldName2Vc and committeeType in ('TEC','PEC') and memberRole = 'cp') as varchar(10)) as FieldValue5
		from tbl_EvalRptDocs inner join tbl_EvalReportMaster on 
		tbl_EvalRptDocs.evalRptId=tbl_EvalReportMaster.evalRptId where tbl_EvalReportMaster.tenderid=@v_fieldName2Vc and tbl_EvalReportMaster.evalRptId=@v_fieldName3Vc
END


IF @v_fieldName1Vc = 'SubmitReportList'
BEGIN

Declare @userid as varchar(10)
set @userid=(SELECT  UserId 
		FROM  tbl_Committee, tbl_CommitteeMembers where 
		tbl_Committee.committeeId = tbl_CommitteeMembers.committeeId
		and tenderId = @v_fieldName2Vc and committeeType in ('TEC','PEC') and memberRole = 'cp')


select evalRptName as FieldValue1,cast(evalRptId as varchar(10)) as FieldValue2,cast((select evalRptReqId from tbl_EvalReportReq where evalRptId in (select evalrptId from tbl_EvalReportReq)) as varchar(10)) as FieldValue3
,cast(@userid as varchar(10)) as FieldValue4 from tbl_EvalReportMaster where evalRptId in 
(select evalrptId from tbl_EvalReportReq) and tenderid=@v_fieldName2Vc
END


IF @v_fieldName1Vc = 'GetClarification'
BEGIN
	select cast(evalRptClrId as varchar(10)) as FieldValue1,REPLACE(CONVERT(VARCHAR(11), expRspDt, 106), ' ', '-')  as FieldValue2,
	case when reportResponse='' then 'yes' else 'no' end as FieldValue3,
	(SELECT employeeName FROM tbl_EmployeeMaster where employeeId=sentTo) as FieldValue4,
	REPLACE(CONVERT(VARCHAR(11), reportQryDt, 106), ' ', '-')  as FieldValue5
	 from tbl_EvalReportQry where evalRptId
	in (select evalRptId from tbl_EvalReportMaster where tenderid=@v_fieldName2Vc)

END

IF @v_fieldName1Vc = 'GetConfigPrebid'
BEGIN

SELECT top 1 isPreTenderMeetingfReq FieldValue1,convert(varchar(20),postQueDays) FieldValue2,
convert(varchar(20),pubBidDays) FieldValue3,convert(varchar(20),uploadedTenderDays) as  FieldValue4
  FROM  [tbl_ConfigPreTender] c,tbl_tenderdetails tc,tbl_TenderTypes tt,tbl_ProcurementTypes pt
  where  tc.eventType=tt.tenderType and c.tenderTypeId=tt.tenderTypeId 
  and tc.procurementType=pt.procurementType and pt.procurementTypeId=c.procurementTypeId
  and c.procurementMethodId=tc.procurementMethodId 
  and tenderId=@v_fieldName2Vc
  		 

END

IF @v_fieldName1Vc = 'GetConfigStatus'
BEGIN
Select convert(varchar(20),tenderId) as FieldValue1 from [tbl_TenderDetails] where tenderId = @v_fieldName2Vc and  [stdTemplateId] <> 0 --and [tenderSecurityDays] <> 0 
--Select convert(varchar(20),tenderId) as FieldValue1, IsNull(docAvlMethod,'') as FieldValue2 from [tbl_TenderDetails] where tenderId = @v_fieldName2Vc and  [tenderValDays] <> 0 --and [tenderSecurityDays] <> 0 
END

IF @v_fieldName1Vc = 'GetPendingQuestion'  
BEGIN
 
	 select CONVERT(varchar(50),p.queryId) as FieldValue1,queryText as FieldValue2,CONVERT(varchar(50), 'Pending') as FieldValue3,CONVERT(varchar(50), p.userId) as FieldValue6 
	 from tbl_PreTenderQuery p where queryType !='Prebid'
 and 	  queryid not in (select queryId from tbl_PreTenderReply where  tenderId=@v_fieldName2Vc )  and tenderId=@v_fieldName2Vc 
	 
END
IF @v_fieldName1Vc = 'GetRepliedQuestion'  
BEGIN
	-- select CONVERT(varchar(50),p.queryId) as FieldValue1,queryText as FieldValue2,CONVERT(varchar(50), replyAction) as FieldValue3,CONVERT(varchar(50), replyText) as FieldValue4,CONVERT(varchar(50), rephraseQryText) as FieldValue5 ,CONVERT(varchar(50), p.userId) as FieldValue6 
	-- from tbl_PreTenderQuery p,tbl_PreTenderReply r
	-- where  queryType !='Prebid'
 --and  p.queryId=r.queryId and  tenderId=@v_fieldName2Vc   and replyAction='Replied'
 	 select CONVERT(varchar(50),p.queryId) as FieldValue1,
	 queryText as FieldValue2,
	 replyAction as FieldValue3,
	 replyText as FieldValue4,
	 rephraseQryText as FieldValue5 ,CONVERT(varchar(50), p.userId) as FieldValue6 
	 from tbl_PreTenderQuery p,tbl_PreTenderReply r
	 where  queryType !='Prebid'
 and  p.queryId=r.queryId and  tenderId=@v_fieldName2Vc   and replyAction='Replied'
 
END
IF @v_fieldName1Vc = 'GetMyPreBidQuestion'  
BEGIN
	 select CONVERT(varchar(50),l.queryId) as FieldValue1,queryText as FieldValue2,CONVERT(varchar(50), isNull(replyAction,'Pending')) as FieldValue3 
	 from (select * from tbl_PreTenderQuery where  queryType !='Prebid'
 and  tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc) l left outer join tbl_PreTenderReply r
	 on l.queryId=r.queryId and tenderId=@v_fieldName2Vc and l.userId=@v_fieldName3Vc
END
IF @v_fieldName1Vc = 'GetAllPreBidQuestion'  
BEGIN
	 select CONVERT(varchar(50),queryId) as FieldValue1,queryText as FieldValue2,CONVERT(varchar(50), queryStatus) as FieldValue3 
	 from tbl_PreTenderQuery
	 where tenderId=@v_fieldName2Vc  and queryType !='Prebid'
   
END
 If @v_fieldName1Vc='QuestionDocs'
   Begin
		Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(prebidQueryDocId  as varchar(10)) as FieldValue4 
		from tbl_PreTenderQryDocs c,tbl_PreTenderQuery p
		 Where p.tenderId=@v_fieldName2Vc and p.queryId=c.queryId and p.userId=@v_fieldName3Vc  and docType!='Prebid'
   
   End
   If @v_fieldName1Vc='QuestionReplyDocs'
   Begin
		Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(replyDocId  as varchar(10)) as FieldValue4 
		from tbl_PreTenderReplyDocs Where queryId=@v_fieldName2Vc   and docType!='Prebid'
   End  
 IF @v_fieldName1Vc = 'GetQuestionText'  
BEGIN
	 select CONVERT(varchar(50),queryId) as FieldValue1,queryText as FieldValue2,CONVERT(varchar(50), queryStatus) as FieldValue3 ,CONVERT(varchar(50), userId) as FieldValue6 
	 from tbl_PreTenderQuery
	 where queryId=@v_fieldName2Vc and queryType !='Prebid'   
END	   
    IF @v_fieldName1Vc = 'AAFillCombo'
BEGIN

Select @v_roleNameVc= procurementRole, @v_roleIdInt= procurementRoleId from tbl_ProcurementRole where procurementRoleId in (
Select approvingAuthEmpId from tbl_AppPackages where packageId in (
Select packageId from tbl_TenderMaster where tenderId = @v_fieldName2Vc ))
	
	IF @v_roleNameVc = 'PD'
	BEGIN
		
		SELECT @v_appIdInt = appid FROM tbl_TenderMaster WHERE tenderId = @v_fieldName2Vc		
		SELECT @v_projectIdInt = projectId FROM tbl_AppMaster WHERE appId = @v_appIdInt
	
	Select cast(userid as varchar(10)) as FieldValue1, employeeName as FieldValue2 from tbl_EmployeeMaster where userId in
	(SELECT userid FROM tbl_ProjectRoles where ProjectId = @v_projectIdInt and procurementRoleId = @v_roleIdInt)

	END
	
	IF @v_roleNameVc = 'PE'
	BEGIN
		--select cast(employeeId as varchar(10)) as FieldValue1,employeeName as FieldValue2 from tbl_EmployeeMaster 
		--	where employeeId in(select employeeId from tbl_EmployeeRoles where departmentId= 
		--	(select departmentId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
		--	and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='PE'))
	select cast(e.userid as varchar(10)) as FieldValue1,e.employeeName as FieldValue2 from	tbl_tendermaster  t,tbl_AppMaster a,tbl_employeemaster e 
where t.appId=a.appId and  tenderid=@v_fieldName2Vc and a.employeeid=e.employeeId;		
	END
	
	IF @v_roleNameVc = 'HOPE'
	BEGIN
		select cast(userid as varchar(10)) as FieldValue1, employeeName as FieldValue2 from tbl_EmployeeMaster 
			where employeeId in(select employeeId from tbl_EmployeeRoles where departmentId= 
			(select departmentId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
			and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='HOPE'))
	END
	
	IF @v_roleNameVc = 'Authorized Officer' or @v_roleNameVc='AO'
	BEGIN
		select cast(userid as varchar(10)) as FieldValue1, employeeName as FieldValue2 from tbl_EmployeeMaster 
			where employeeId in(select employeeId from tbl_EmployeeRoles where departmentId= 
			(select departmentId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
			and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole in('Authorized Officer','AO')))
	END
	
	IF @v_roleNameVc = 'BOD'
	BEGIN
	print 'sf'
		select cast(userid as varchar(10))   as FieldValue1, employeeName as FieldValue2 from tbl_EmployeeMaster 
			where employeeId in(select employeeId from tbl_EmployeeRoles where departmentId= 
			(select departmentId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
			and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='BOD'))
	END
	
	IF @v_roleNameVc = 'Secretary'
	BEGIN
		SELECT @v_departmentid_Vc=departmentId FROM tbl_TenderDetails WHERE tenderid=@v_fieldName2Vc
		set @v_departmentidCounter_Vc=@v_departmentid_Vc
		set @v_tempCounter_Vc=''
		
		If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Organization'
			Begin
				SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)
				SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
			End		
		
		if @v_tempCounter_Vc=''
			BEGIN
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
					END
			END
		ELSE
			BEGIN
		
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_tempCounter_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)) as varchar(10))
					END
			END
		
		
		SELECT @v_employeeIdlist_Vc = COALESCE(@v_employeeIdlist_Vc+',' , '') + cast(employeeId as varchar(10))
		FROM tbl_EmployeeRoles WHERE departmentId in (select dbo.f_trim(items) from dbo.f_split(@v_departmentidCounter_Vc,',')) 
		and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='Secretary')
		
		select cast(userid as varchar(10))  as FieldValue1, employeename as FieldValue2 from tbl_EmployeeMaster where employeeId in (select dbo.f_trim(items) from dbo.f_split(@v_employeeIdlist_Vc,','))
	END


IF @v_roleNameVc = 'Minister'
	BEGIN
		SELECT @v_departmentid_Vc=departmentId FROM tbl_TenderDetails WHERE tenderid=@v_fieldName2Vc
		set @v_departmentidCounter_Vc=@v_departmentid_Vc
		set @v_tempCounter_Vc=''
		
		If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Organization'
			Begin
				SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)
				SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
			End		
		
		if @v_tempCounter_Vc=''
			BEGIN
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
					END
			END
		ELSE
			BEGIN
		
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_tempCounter_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)) as varchar(10))
					END
			END
		
		
		SELECT @v_employeeIdlist_Vc = COALESCE(@v_employeeIdlist_Vc+',' , '') + cast(employeeId as varchar(10))
		FROM tbl_EmployeeRoles WHERE departmentId in (select dbo.f_trim(items) from dbo.f_split(@v_departmentidCounter_Vc,',')) 
		and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='Minister')
		
		select cast(userid as varchar(10))  as FieldValue1, employeename as FieldValue2 from tbl_EmployeeMaster where employeeId in (select dbo.f_trim(items) from dbo.f_split(@v_employeeIdlist_Vc,','))
	END


IF @v_roleNameVc = 'CCGP'
	BEGIN	
		select cast(userid as varchar(10)) as FieldValue1,employeename as FieldValue2 from tbl_EmployeeMaster where employeeId in (select employeeId from tbl_EmployeeRoles where procurementRoleId in (select procurementRoleId from tbl_ProcurementRole where procurementRole = 'CCGP'))
	END
	
	
IF @v_roleNameVc = 'Development Partner' or @v_roleNameVc = 'DP'
	BEGIN
	--print 'in DP'
		
	SELECT @v_appIdInt = appid FROM tbl_TenderMaster WHERE tenderId = @v_fieldName2Vc		
	SELECT @v_projectIdInt = projectId FROM tbl_AppMaster WHERE appId = @v_fieldName2Vc

	Select fullName as FieldValue1, designation as FieldValue2 from tbl_PartnerAdmin 
	where  sBankDevelopId in 
	( select sBankDevelopId from tbl_ScBankDevPartnerMaster where sBankDevelHeadId in
		(Select sBankDevelopId from tbl_projectpartners where projectId =@v_projectIdInt))
	
	END

END


If @v_fieldName1Vc='TenderEngEstDocs'
Begin
 SELECT  
   documentName as FieldValue1,documentDesc as FieldValue2,docsize   as FieldValue3,CONVERT(varchar(20),d.appEngEstId) as FieldValue4,CONVERT(varchar(20),d.appId) as FieldValue5  from [tbl_TenderMaster] t,tbl_AppEngEstDoc d
   where t.packageId=d.packageid and tenderId=@v_fieldName2Vc
End

If @v_fieldName1Vc='QuestionDocsBy'
Begin
  Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(prebidQueryDocId  as varchar(10)) as FieldValue4 
  from tbl_PreTenderQryDocs Where tenderId=@v_fieldName2Vc and queryid=0 and userId=@v_fieldName3Vc  and docType !='Prebid'
End

IF @v_fieldName1Vc = 'getPostQualificationData' 
BEGIN
		SELECT CM.companyName as FieldValue1, 
		PQ.postQualStatus as FieldValue2, 
		Cast(FS.userId as Varchar) as FieldValue3,
		PQ.siteVisit as FieldValue4,
		IsNull(REPLACE(CONVERT(VARCHAR(11),PQ.siteVisitReqDt , 106), ' ', '-') ,'') as FieldValue5,
		PQ.comments as FieldValue6,
		IsNull(REPLACE(CONVERT(VARCHAR(11),PQ.siteVisitDate  , 106), ' ', '-') ,'') as FieldValue7,
		PQ.siteVisitStatus as FieldValue8,
		IsNull(REPLACE(CONVERT(VARCHAR(11),PQ.entryDate , 106), ' ', '-') ,'') as FieldValue9 
		FROM dbo.tbl_FinalSubmission FS
		INNER JOIN dbo.tbl_CompanyMaster CM ON FS.userId = CM.userId 
		Inner JOIN dbo.tbl_PostQualification PQ ON FS.userId = PQ.userId AND 
		FS.tenderId = PQ.tenderId 
		Where FS.bidSubStatus = 'finalsubmission' and FS.tenderid = @v_fieldName2Vc and PQ.userId=@v_fieldName3Vc
		Order by  PQ.entryDate 
END

IF @v_fieldName1Vc = 'updateTenderdetail' 
BEGIN

	BEGIN TRY
		BEGIN TRAN	
		
		Declare 
		@v_Columns_Vc varchar(max),
		@v_TblStructure_Vc varchar(max) 		
		

select @v_Columns_Vc = Coalesce(@v_Columns_Vc, '') + +ic.COLUMN_NAME +' = convert('+ic.DATA_TYPE + isnull( +'('+convert(varchar(20),ic.CHARACTER_MAXIMUM_LENGTH)+')','') + ','''+tc.newValue +''''+ case when ic.DATA_TYPE ='smalldatetime' then ',103' else '' end +'), ' 		
	  from information_schema.columns ic, tbl_CorrigendumDetail tc
		where ic.table_name = 'tbl_TenderDetails' and tc.fieldName = ic.COLUMN_NAME and tc.corrigendumId = @v_fieldName3Vc
		
--print 'Columns: ' + @v_Columns_Vc
Set @v_Columns_Vc=SUBSTRING(@v_Columns_Vc,0,LEN(@v_Columns_Vc))		
		
set @v_TblStructure_Vc = ' Update tbl_TenderDetails SET ' + @v_Columns_Vc + '  Where tenderid = '+ @v_fieldName2Vc 

--Select @v_TblStructure_Vc
exec(@v_TblStructure_Vc)
--print 'tbl_TenderPhasing: ' 

update tbl_TenderForms set formStatus = 'a' where formStatus = 'createp' and tenderSectionId in 
(select tenderSectionId from tbl_TenderSection where contentType='Form' and tenderStdId in 
(select tenderStdId from tbl_TenderStd where tenderId = @v_fieldName2Vc))

update tbl_TenderForms set formStatus = 'c' where formStatus = 'cp' and tenderSectionId in 
(select tenderSectionId from tbl_TenderSection where contentType='Form' and tenderStdId in 
(select tenderStdId from tbl_TenderStd where tenderId = @v_fieldName2Vc))

update tbl_TenderSectionDocs set status = 'c' where status = 'cp' and tenderSectionId in 
(select tenderSectionId from tbl_TenderSection where contentType='Document By PE' and tenderStdId in 
(select tenderStdId from tbl_TenderStd where tenderId = @v_fieldName2Vc))

update tbl_TenderSectionDocs set status = 'a' where status = 'createp' and tenderSectionId in 
(select tenderSectionId from tbl_TenderSection where contentType='Document By PE' and tenderStdId in 
(select tenderStdId from tbl_TenderStd where tenderId = @v_fieldName2Vc))

update tbl_TenderPhasing
set indEndDt = a.newValue, indStartDt = a.newValue
from (select SUBSTRING(fieldName, 1, charindex('@',fieldName)-1)as fieldname,
 SUBSTRING(fieldName, charindex('@',fieldName)+1, LEN(fieldName)) as Id, newValue
from [tbl_CorrigendumDetail] where corrigendumid = @v_fieldName3Vc and charindex('@',fieldName)>0 and SUBSTRING(fieldName, 1, charindex('@',fieldName)-1) in ('indStartDt','indEndDt')) as a
where tbl_TenderPhasing.tenderPhasingId = a.Id and tenderid = @v_fieldName2Vc

--print 'tbl_TenderLotSecurity: ' 
update tbl_TenderLotSecurity
set docFess = convert(money, a.newValue)
from (select SUBSTRING(fieldName, 1, charindex('@',fieldName)-1)as fieldname,
 SUBSTRING(fieldName, charindex('@',fieldName)+1, LEN(fieldName)) as Id, newValue
from [tbl_CorrigendumDetail] where corrigendumid = @v_fieldName3Vc and charindex('@',fieldName)>0 and SUBSTRING(fieldName, 1, charindex('@',fieldName)-1)='docFess' ) as a
where tenderLotSecId = a.Id and tenderid = @v_fieldName2Vc
update tbl_TenderLotSecurity
set location =  a.newValue
from (select SUBSTRING(fieldName, 1, charindex('@',fieldName)-1)as fieldname,
 SUBSTRING(fieldName, charindex('@',fieldName)+1, LEN(fieldName)) as Id, newValue
from [tbl_CorrigendumDetail] where corrigendumid = @v_fieldName3Vc and charindex('@',fieldName)>0 and SUBSTRING(fieldName, 1, charindex('@',fieldName)-1)='location' ) as a
where tenderLotSecId = a.Id and tenderid = @v_fieldName2Vc
Select 'true' as FieldValue1, 'tbl_TenderDetails updates Successfully.' as FieldValue2

COMMIT TRAN	
	END TRY 
	BEGIN CATCH
			BEGIN				
				Select 'false' as FieldValue1, 'Error while updating tbl_TenderDetails.' as FieldValue2
				ROLLBACK TRAN
			END
	END CATCH
	
------------Code by taher------
	Declare @newdate as smalldatetime,@count as int
	select @count=COUNT(*) from tbl_corrigendumdetail where corrigendumId=2 and fieldName='submissionDt'
	if (@count >0)
	BEGIN
		select @newdate= REPLACE(CONVERT(VARCHAR(11),newValue, 106), ' ', '-')  from tbl_corrigendumdetail where corrigendumId=@v_fieldName3Vc and fieldName='submissionDt'
		update tbl_TenderDetails set tenderValidityDt=(cast(@newdate as datetime) + tenderValdays),tenderSecurityDt=(cast(@newdate as  datetime) + tenderSecurityDays) where tenderId=@v_fieldName2Vc 
		--select (cast(@newdate as datetime) + tenderValdays) as FieldValue1,(cast(@newdate as  datetime) + tenderSecurityDays) as FieldValue2 from tbl_TenderDetails where tenderId=307 
	END
------------Code End-----------	
END



IF @v_fieldName1Vc='getTenderInfoForDocView'
BEGIN			
	select CONVERT(varchar(50),TM.tenderId) as FieldValue1, 
	docFeesMode as FieldValue2, 
	docAvlMethod as FieldValue3, 
	IsNull(REPLACE(CONVERT(VARCHAR(11),TD.docEndDate, 106), ' ', '-') ,'') as FieldValue4, 
	IsNull(CONVERT(varchar(50), datediff(day, GETDATE(), TD.docEndDate)),'null') as FieldValue5,
	IsNull(Convert(varchar(50), pkgDocFees),'')  as FieldValue6,
	Case docFeesMethod 
		When 'Package wise'	
		Then 
			Case 
				When (pkgDocFees > 0) 
				Then 'Paid'
				Else 'Free'
			End
		When 'Lot wise'		
		Then 
			Case 
				When ((select Sum(docFess) from tbl_TenderLotSecurity Where tenderId=@v_fieldName2Vc) > 0)
				Then 'Paid'
				Else 'Free'
			End
		Else 'Free'		
		End as FieldValue7,
	Case 
		When Exists (select top 1 tenderPaymentId from tbl_TenderPayment where tenderId=@v_fieldName2Vc and
		 (
						 userId in 								
						(
							select distinct userId from tbl_TendererMaster 
								where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
						)  								
						Or 
						 userId = @v_fieldName3Vc
					) and paymentFor='Document Fees' and isVerified='yes' and isLive='yes')	
		Then 'yes'
		Else 'no'
	End as 	FieldValue8
	from tbl_TenderDetails TD 
	inner join tbl_TenderMaster TM On TD.tenderId=TM.tenderId	
	where TM.tenderId=@v_fieldName2Vc
	Group by TM.tenderId, docFeesMode, docAvlMethod, TD.docEndDate, pkgDocFees, TD.tenderSecurityDt, TD.docFeesMethod
END


If @v_fieldName1Vc='PreTenderQueDocuments'
   Begin
  Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(prebidQueryDocId  as varchar(10)) as FieldValue4 
  from tbl_PreTenderQryDocs Where tenderId=@v_fieldName2Vc and queryid=@v_fieldName3Vc   and docType='Prebid'
   End

If @v_fieldName1Vc='QuestionDocumentsBy'
Begin
  Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(prebidQueryDocId  as varchar(10)) as FieldValue4 
  from tbl_PreTenderQryDocs Where tenderId=@v_fieldName2Vc  and queryid=@v_fieldName3Vc  and docType !='Prebid'
End

IF @v_fieldName1Vc = 'getLotDescriptonForDocView'     -- For
BEGIN
		
	IF @v_fieldName3Vc is not null  And @v_fieldName3Vc<>''
	BEGIN		
		
		If (select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1
		Begin
			SELECT TLS.lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
		IsNull(Convert(varchar(50),(select top 1 tenderPaymentId from tbl_TenderPayment TP 
		Where paymentFor='Document Fees' 		
		And TP.tenderId=TLS.tenderId		 
		And pkgLotId=TLS.appPkgLotId 
		And isVerified='Yes' And isLive='Yes' 
		And (
						  tp.userId in 								
						(
							select distinct userId from tbl_TendererMaster 
								where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
						)  								
						Or 
						  tp.userId = @v_fieldName3Vc
					)
		Order by tenderPaymentId desc)),'0') as FieldValue4	
		FROM  dbo.tbl_TenderLotSecurity TLS 
		WHERE tenderId = @v_fieldName2Vc	
		End
		Else
		Begin
			SELECT TLS.lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
		IsNull(Convert(varchar(50),(select top 1 tenderPaymentId from tbl_TenderPayment TP Where paymentFor='Document Fees' And TP.userId=@v_fieldName3Vc And TP.tenderId=TLS.tenderId And pkgLotId=TLS.appPkgLotId And isVerified='Yes' And isLive='Yes' Order by tenderPaymentId desc)),'0') as FieldValue4	
		FROM  dbo.tbl_TenderLotSecurity TLS 
		WHERE tenderId = @v_fieldName2Vc	
		End
		
	END
	ELSE
	BEGIN		
		SELECT TLS.lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
		 'N.A.' as FieldValue4			
		FROM  dbo.tbl_TenderLotSecurity TLS 
		WHERE tenderId = @v_fieldName2Vc	
	END
		
END

IF @v_fieldName1Vc = 'getPackageDescriptonForDocView'     -- For 
BEGIN
	IF @v_fieldName3Vc is not null  And @v_fieldName3Vc<>''
	Begin
		If (select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1
		Begin
			SELECT packageNo as FieldValue1, packageDescription as FieldValue2,
			IsNull(Convert(varchar(50),(select top 1 tenderPaymentId from tbl_TenderPayment TP 
			Where paymentFor='Document Fees' And isVerified='Yes' And isLive='Yes' 			
			And TP.tenderId=TD.tenderId 
			And (
						 tp.userId in 								
						(
							select distinct userId from tbl_TendererMaster 
								where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
						)  								
						Or 
						  tp.userId = @v_fieldName3Vc
					)
			Order by tenderPaymentId desc)),'0') as FieldValue3			 
			FROM dbo.tbl_TenderDetails TD 
			WHERE TD.tenderId = @v_fieldName2Vc	
		End
		Else 
		Begin
			SELECT packageNo as FieldValue1, packageDescription as FieldValue2,
			IsNull(Convert(varchar(50),(select top 1 tenderPaymentId from tbl_TenderPayment TP Where paymentFor='Document Fees' And isVerified='Yes' And isLive='Yes' And TP.userId=@v_fieldName3Vc And TP.tenderId=TD.tenderId Order by tenderPaymentId desc)),'0') as FieldValue3			 
			FROM dbo.tbl_TenderDetails TD 
			WHERE TD.tenderId = @v_fieldName2Vc		
		End
		
	End
	Else
	Begin
		SELECT packageNo as FieldValue1, packageDescription as FieldValue2, 'N.A.' as FieldValue3			  
		FROM dbo.tbl_TenderDetails TD 
	    WHERE TD.tenderId = @v_fieldName2Vc		
	End
END
IF @v_fieldName1Vc = 'GetClosingDt'  
BEGIN
	 select CONVERT(varchar(50),tenderid) as FieldValue1, REPLACE(CONVERT(VARCHAR(11),submissionDt, 106), ' ', '-')  +' '+Substring(CONVERT(VARCHAR(30),submissionDt,108),1,5) as FieldValue2
	 
	 from tbl_TenderDetails
	 where tenderId=@v_fieldName2Vc
END	
IF @v_fieldName1Vc = 'getEmailIdCPV'     -- alter commaseprated Emailid string based on CPVCode 
BEGIN
	DECLARE @v_CpvList_Vc varchar(max), @v_Cpvcode_Vc varchar(1500), @v_emailIdList_Vc varchar(max), @refNo varchar(500), @v_MobileList_Vc varchar(max)
	,@peName varchar(500),@tendCloseDate varchar(500),@tenderBrief varchar(2000)
DECLARE @CPVUserid TABLE (userId int, mobileNo varchar(50) )

IF exists (Select tenderId from tbl_LimitedTenders where tenderId = @v_fieldName2Vc)
BEGIN
		insert into @CPVUserid(userid, mobileNo)
	--SELECT DISTINCT userID, null FROM tbl_LimitedTenders WHERE tenderId =@v_fieldName2Vc	
	SELECT DISTINCT tbl_LimitedTenders.userId, countryCode+mobileNo as mobileNo FROM tbl_LimitedTenders, tbl_TendererMaster , tbl_CountryMaster 
where tenderId =@v_fieldName2Vc	and  tbl_TendererMaster.country = tbl_CountryMaster.countryName and  tbl_TendererMaster.userId = tbl_LimitedTenders.userId
SELECT @v_emailIdList_Vc =	COALESCE(@v_emailIdList_Vc+', ', ' ') + emailid			
			 FROM tbl_LoginMaster where userid in (SELECT userId FROM @CPVUserid) and status='approved' and userTyperId=2
			 
			SELECT @v_MobileList_Vc = COALESCE(@v_MobileList_Vc+', ', ' ') + mobileNo FROM @CPVUserid
	SELECT @v_CpvList_Vc = td.cpvcode,@refNo=td.reoiRfpRefNo,@tendCloseDate=CONVERT(varchar(50), td.submissionDt,106)+ ' ' +Substring(CONVERT(VARCHAR(5),submissionDt,108),1,5) ,@peName=om.officeName,@tenderBrief=td.tenderBrief from tbl_TenderDetails td,tbl_OfficeMaster om where td.tenderId = @v_fieldName2Vc and td.officeId=om.officeId
	SELECT @v_emailIdList_Vc AS FieldValue1, @v_MobileList_Vc AS FieldValue2, @refNo AS FieldValue3,
			@peName as FieldValue4,@tendCloseDate as FieldValue5,@tenderBrief as FieldValue6
END
ELSE
BEGIN
SELECT @v_CpvList_Vc = td.cpvcode,@refNo=td.reoiRfpRefNo,@tendCloseDate=CONVERT(varchar(50), td.submissionDt,106)+ ' ' +Substring(CONVERT(VARCHAR(5),submissionDt,108),1,5) ,@peName=om.officeName,@tenderBrief=td.tenderBrief from tbl_TenderDetails td,tbl_OfficeMaster om where td.tenderId = @v_fieldName2Vc and td.officeId=om.officeId

DECLARE cur_CpvDetail CURSOR FAST_FORWARD FOR SELECT DISTINCT items FROM dbo.f_split(@v_CpvList_Vc, ';')    
			open cur_CpvDetail        
			FETCH NEXT FROM cur_CpvDetail INTO @v_Cpvcode_Vc				

			WHILE @@Fetch_status = 0        
			BEGIN 
			INSERT INTO @CPVUserid(userid, mobileNo)
			SELECT DISTINCT userid, MobileNo
			FROM
			(
			SELECT DISTINCT Userid , countryCode+mobileNo as MobileNo FROM tbl_TendererMaster, tbl_CountryMaster WHERE companyid in 
			     (SELECT  companyId FROM tbl_CompanyMaster WHERE  specialization like '%'+@v_Cpvcode_Vc+'%')
			     and tbl_TendererMaster.country = tbl_CountryMaster.countryName
			UNION ALL
			
			SELECT DISTINCT userId, countryCode+mobileNo as MobileNo FROM tbl_TendererMaster, tbl_CountryMaster WHERE specialization like '%'+@v_Cpvcode_Vc+'%' and tbl_TendererMaster.country = tbl_CountryMaster.countryName
			UNION ALL
			
			SELECT DISTINCT b.userId, countryCode+mobileNo as MobileNo FROM tbl_TendererMaster, tbl_CountryMaster,tbl_BidConfirmation b WHERE
			b.userId = tbl_TendererMaster.userId and tenderId = @v_fieldName2Vc and
			tbl_TendererMaster.country = tbl_CountryMaster.countryName
			
			
			) as a
			
			
			FETCH NEXT FROM cur_CpvDetail INTO @v_Cpvcode_Vc        
			END       

			CLOSE cur_CpvDetail        
			DEALLOCATE cur_CpvDetail
			
			SELECT @v_emailIdList_Vc =	COALESCE(@v_emailIdList_Vc+', ', ' ') + emailid			
			 FROM tbl_LoginMaster where userid in (SELECT userId FROM @CPVUserid) and status='approved' and userTyperId=2
			 
			SELECT @v_MobileList_Vc = COALESCE(@v_MobileList_Vc+', ', ' ') + mobileNo FROM @CPVUserid
	
	SELECT @v_emailIdList_Vc AS FieldValue1, @v_MobileList_Vc AS FieldValue2, @refNo AS FieldValue3,
			@peName as FieldValue4,@tendCloseDate as FieldValue5,@tenderBrief as FieldValue6
END	
	
END

IF @v_fieldName1Vc = 'searchSubContractBidder'  
BEGIN

declare @bidQuery1 varchar(Max)

Set @bidQuery1 = 
'Select ct.CompanyName as FieldValue1, ct.companyRegNumber FieldValue2, 
ct.EmailID FieldValue3,  ct.Country FieldValue4,  ct.State as FieldValue5,
  ct.RegistrationType FieldValue6 ,  ct.LegalStatus FieldValue7, 
  convert(varchar(30),ct.userId )as FieldValue8  from 
(
Select CompanyName, companyRegNumber,  EmailID,  Country,  State, 
 RegistrationType,  LegalStatus , lm.userId
from tbl_LoginMaster lm, tbl_TendererMaster tm , tbl_CompanyMaster tc
where lm.userId = tm.userId and lm.userid !=@v_fieldName4Vc and lm.userid not in(select invTouserid from tbl_subcontracting where   tenderid='+@v_fieldName2Vc+') and
 status=''Approved'' and tc.companyId !=1 and 
tc.companyid = tm.companyId '+case when @v_fieldName3Vc is not null then +' 
and ' + @v_fieldName3Vc else '' end +'
) as ct
	  '
print @bidQuery1	
	exec(@bidQuery1)
END	
IF @v_fieldName1Vc = 'searchBidder'  
BEGIN

declare @bidQuery varchar(Max), @v_CpvList1_Vc varchar(max)
SELECT @v_CpvList1_Vc = cpvcode from tbl_TenderDetails where tenderId = @v_fieldName2Vc


Set @bidQuery = 
'Select ct.CompanyName as FieldValue1, ct.companyRegNumber FieldValue2, ct.EmailID FieldValue3,  ct.Country FieldValue4,  ct.State as FieldValue5,  ct.RegistrationType FieldValue6 ,  ct.LegalStatus FieldValue7, convert(varchar(30),ct.userId )as FieldValue8, case when lt.userId IS NULL  then ''UnMapped'' else ''Mapped'' end as FieldValue9  from 
(
Select case CompanyName when ''-'' then tm.firstName+'' ''+ tm.lastName else companyName end as companyName, companyRegNumber,  EmailID,  Country,  State,  RegistrationType,  LegalStatus , lm.userId
from tbl_LoginMaster lm, tbl_TendererMaster tm , tbl_CompanyMaster tc
where lm.userId = tm.userId and lm.registrationType!=''media'' and tm.country=''Bangladesh'' and tc.regOffcountry=''Bangladesh''   and 
(tm.specialization like '+char(39)+'%'+replace(@v_CpvList1_Vc,';','%'+ char(39) +' or tm.specialization like '+char(39)+'%')+'%'+char(39)+ ' or ' + ' tc.specialization like '+char(39)+'%'+replace(@v_CpvList1_Vc,';','%'+ char(39) +' or tc.specialization like '+char(39)+'%')+'%'+char(39) +' ) and status=''Approved'' and tc.companyid = tm.companyId '+case when @v_fieldName3Vc is not null then +' and ' + @v_fieldName3Vc else '' end +'
) as ct
	left outer join (Select userId from tbl_LimitedTenders where tenderId = '+@v_fieldName2Vc+' ) lt
	on ct.userid = lt.userId '
	
--	print(@bidQuery)
	exec(@bidQuery)
END	




If @v_fieldName1Vc='GetSubFormList'      -- Done by Rajesh for BidPreperation.jsp
Begin
	select @lefttemp=LEFT(@v_fieldName3Vc, CHARINDEX(',', @v_fieldName3Vc) - 1)
	select @righttemp=SUBSTRING(@v_fieldName3Vc, CHARINDEX(',', @v_fieldName3Vc) + 1, LEN(@v_fieldName3Vc) - CHARINDEX(',', @v_fieldName3Vc))	

	select cast(tenderId as varchar(20)) as FieldValue1  from tbl_TenderBidForm where tenderId=@v_fieldName2Vc and userId=@lefttemp and tenderFormId=@righttemp
End


If @v_fieldName1Vc='GetReportDetails'      -- Done by Rajesh for BidPreperation.jsp
Begin
	select reportQuery as FieldValue1,REPLACE(CONVERT(VARCHAR(11), expRspDt, 106), ' ', '-')  as FieldValue2,cast(sentBy as varchar(10)) as FieldValue3, cast(sentTo as varchar(10)) as FieldValue4 from tbl_EvalReportQry where evalRptClrId=@v_fieldName2Vc
End


 If @v_fieldName1Vc='docTenderInfo'
   Begin
		If @v_fieldName3Vc='0'
		Begin
			Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(corrDocId  as varchar(10)) as FieldValue4 
		from tbl_CorrigendumDocs Where corrigendumId=@v_fieldName2Vc
		End
		Else
		Begin
			Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(corrDocId  as varchar(10)) as FieldValue4 
		from tbl_CorrigendumDocs Where corrigendumId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
		End
		
   End
   
   
   If @v_fieldName1Vc='getCorriDocLotIds'
   Begin
		select Convert(varchar(50),pkgLotId) as FieldValue1				
		from tbl_CorrigendumDocs CD inner join tbl_CorrigendumMaster CM
		On CM.corrigendumId=CD.corrigendumId
		where CM.tenderId = @v_fieldName2Vc
		And CM.corrigendumId=@v_fieldName3Vc
		Group by pkgLotId
		Order by pkgLotId
	End
	
If @v_fieldName1Vc='lotDocInfo'
   Begin
		Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(corrDocId  as varchar(10)) as FieldValue4 
		from tbl_CorrigendumDocs Where corrigendumId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
   End
   
If @v_fieldName1Vc='TenderSecurityList'
   Begin
	select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue1,cast(l.userId as varchar(10)) as FieldValue2 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c,tbl_TenderPayment tp
	where l.userId=t.userId and t.companyId=c.companyId and tp.userId=l.userId
	and paymentFor='Tender Security' and tenderId=@v_fieldName2Vc
   End	


If @v_fieldName1Vc='getInfoForOpeningProcess'
BEGIN
	--select Convert(varchar(50),tenderId) as FieldValue1,	
	--Case When (openingDt is null Or openingDt='') then 'N.A.' When openingDt > getdate() Then 'no' Else 'yes' End as FieldValue2,
	--Case When (getdate() >= openingDt And getdate() <= DATEADD(HOUR,1,openingDt)) Then 'yes' Else 'no' End as FieldValue3,
	--Case When (getdate() > DATEADD(HOUR,1,openingDt)) Then 'yes' Else 'no' End as FieldValue4,
	--Case When Exists  (select top 1 comMemberId from tbl_CommitteeMembers Where appStatus='approved' And committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And tenderId=@v_fieldName2Vc)) Then 'yes' Else 'no' End as FieldValue5,
	--Case When Exists  (select top 1 comMemberId from tbl_CommitteeMembers Where userId=@v_fieldName3Vc And committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And tenderId=@v_fieldName2Vc)) Then 'yes' Else 'no' End as FieldValue6,
	--Case When Exists  (select top 1 comMemberId from tbl_CommitteeMembers Where userId=@v_fieldName3Vc And appStatus='approved' And committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And tenderId=@v_fieldName2Vc)) Then 'yes' Else 'no' End as FieldValue7,
	--(select Convert(varchar(50),count(comMemberId)) from tbl_CommitteeMembers Where committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And tenderId=@v_fieldName2Vc)) as FieldValue8,
	--(select Convert(varchar(50),count(comMemberId))from tbl_CommitteeMembers Where appStatus='approved' And committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And tenderId=@v_fieldName2Vc)) as FieldValue9
	--from tbl_TenderDetails where tenderId=@v_fieldName2Vc
	
	--select Convert(varchar(50),tenderId) as FieldValue1,	
	--Case When (openingDt is null Or openingDt='') then 'N.A.' When openingDt > getdate() Then 'no' Else 'yes' End as FieldValue2,
	--Case When (getdate() >= openingDt And getdate() <= DATEADD(HOUR,1,openingDt)) Then 'yes' Else 'no' End as FieldValue3,
	--Case When (getdate() > DATEADD(HOUR,1,openingDt)) Then 'yes' Else 'no' End as FieldValue4,
	--Case When Exists  (select top 1 comMemberId from tbl_CommitteeMembers Where appStatus='approved' And committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc)) Then 'yes' Else 'no' End as FieldValue5,
	--Case When Exists  (select top 1 comMemberId from tbl_CommitteeMembers Where userId=@v_fieldName3Vc And committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc)) Then 'yes' Else 'no' End as FieldValue6,
	--Case When Exists  (select top 1 comMemberId from tbl_CommitteeMembers Where userId=@v_fieldName3Vc And appStatus='approved' And committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc)) Then 'yes' Else 'no' End as FieldValue7,
	--(select Convert(varchar(50),count(comMemberId)) from tbl_CommitteeMembers Where committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc)) as FieldValue8,
	--(select Convert(varchar(50),count(comMemberId))from tbl_CommitteeMembers Where appStatus='approved' And committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc)) as FieldValue9,
	--Case When Exists (select top 1 committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc) Then 'yes' Else 'no' End as FieldValue10	
	--from tbl_TenderDetails TD Inner Join tbl_TenderOpenDates TOD On TD.tenderId=TOD.tenderId
	--where tenderId=@v_fieldName2Vc
	
	select Convert(varchar(50),TD.tenderId) as FieldValue1,	
	Case When (TOD.openingDt is null Or TOD.openingDt='') then 'N.A.' When TOD.openingDt > getdate() Then 'no' Else 'yes' End as FieldValue2,
	Case When (getdate() >= TOD.openingDt And getdate() <= DATEADD(HOUR,1,TOD.openingDt)) Then 'yes' Else 'no' End as FieldValue3,
	Case When (getdate() > DATEADD(HOUR,1,TOD.openingDt)) Then 'yes' Else 'no' End as FieldValue4,
	Case When Exists  (select top 1 comMemberId from tbl_CommitteeMembers Where appStatus='approved' And committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc)) Then 'yes' Else 'no' End as FieldValue5,
	Case When Exists  (select top 1 comMemberId from tbl_CommitteeMembers Where userId=@v_fieldName3Vc And committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc)) Then 'yes' Else 'no' End as FieldValue6,
	Case When Exists  (select top 1 comMemberId from tbl_CommitteeMembers Where userId=@v_fieldName3Vc And appStatus='approved' And committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc)) Then 'yes' Else 'no' End as FieldValue7,
	(select Convert(varchar(50),count(comMemberId)) from tbl_CommitteeMembers Where committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc)) as FieldValue8,
	(select Convert(varchar(50),count(comMemberId))from tbl_CommitteeMembers Where appStatus='approved' And committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc)) as FieldValue9,
	Case When Exists (select top 1 committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc) Then 'yes' Else 'no' End as FieldValue10	
	from tbl_TenderDetails TD Inner Join tbl_TenderOpenDates TOD On TD.tenderId=TOD.tenderId
	where TD.tenderId=@v_fieldName2Vc
	
END

If @v_fieldName1Vc='getUserInfoForOpeningProcess'
BEGIN
	select Case memberRole When 'm' Then 'Member' When 'ms' Then 'Member Secretary' When 'cp' Then 'Chair Person' End as FieldValue1, 
	Case When Exists (select items from dbo.f_splitX((select top 1 tenderHashUserId from tbl_TenderHash where tenderId=@v_fieldName2Vc order by tenderHashUserId desc),',') where Items = @v_fieldName3Vc) Then 'yes' Else 'no' End as FieldValue2,
	Case When Exists (select tenderId from tbl_TenderOpenExt Where tenderId=@v_fieldName2Vc and extStatus='pending') Then 'yes' Else 'no' End as FieldValue3,
	Case When Exists (select tenderId from tbl_TenderMegaHash Where tenderId=@v_fieldName2Vc and isVerified='yes') Then 'yes' Else 'no' End as FieldValue4
	from tbl_CommitteeMembers 	
	Where userId=@v_fieldName3Vc And committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc)
END


If @v_fieldName1Vc='getBidTemperedDetail'
   Begin
	select cast(tenderId as varchar(10)) as FieldValue1 from tbl_BidTemper where tenderId=@v_fieldName2Vc
   End
   
If @v_fieldName1Vc='getUserCommitteeInfoForOpeningProcess'
BEGIN
	select Case memberRole When 'm' Then 'Member' When 'ms' Then 'Member Secretary' When 'cp' Then 'Chair Person' End as FieldValue1, 
	Case When Exists (select items from dbo.f_splitX((select tenderHashUserId from tbl_TenderHash where tenderId=@v_fieldName2Vc),',') where Items = @v_fieldName3Vc) Then 'Yes' Else 'No' End as FieldValue2,
	IsNull(dbo.getEmpRoles(@v_fieldName3Vc),'N.A.') as FieldValue3
	from tbl_CommitteeMembers 
	Where userId=@v_fieldName3Vc And committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc)
END

If @v_fieldName1Vc='getFormInfoForOpeningProcess'
Begin
	--select Case When (Count(bidPlainId)>0) Then 'yes' Else 'no' End as FieldValue1
	--from tbl_TenderBidPlainData Where tenderFormId=@v_fieldName2Vc
	
	select Case 
		When (Count(bidPlainId)>0) Then 'yes' Else 'no' End as FieldValue1
	from tbl_TenderBidPlainData Where tenderFormId=@v_fieldName2Vc
End


If @v_fieldName1Vc='getTenderMegaHashStatus'
Begin
	select Case When (Count(tenderMegaHashId)>0) Then 'yes' Else 'no' End as FieldValue1
	from tbl_TenderMegahash Where isVerified='yes' and tenderId=@v_fieldName2Vc
	
End

If @v_fieldName1Vc='getTenderMegaHashVerifiedStatus'
Begin
	DECLARE @v_tenderHash_Vc VARCHAR(MAX), @v_megaHash_Vc VARCHAR(MAX), @v_flag_inVc varchar(10),
	@v_IsVerified_Vc varchar(10)
	
	-- FETCH GENERATED TENDER-HASH
	SELECT @v_megaHash_Vc = megaHash, @v_IsVerified_Vc=isVerified 
		FROM dbo.tbl_TenderMegaHash WHERE tenderId = @v_fieldName2Vc
	
	-- FETCH TENDER-HASH
	SELECT @v_tenderHash_Vc = ISNULL(@v_tenderHash_Vc + '_' + tenderHash, tenderHash) FROM dbo.tbl_FinalSubmission 
	WHERE tenderId = @v_fieldName2Vc and bidSubStatus='finalsubmission'
			
	-- NOW GENERATE SHA1 OF GENERATED HASH
	SELECT @v_tenderHash_Vc = SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('SHA1', @v_tenderHash_Vc)), 3, 41)
			
	-- IF BOTH THE HASH ARE MATCH THEN RETURN TRUE ELSE RETURN FALSE INSERT RECORD INTO tbl_BidTemper TABLE
	IF @v_tenderHash_Vc = @v_megaHash_Vc
	Begin
		SET @v_flag_inVc = 'true'
		If @v_IsVerified_Vc='no'
		Begin
			Update dbo.tbl_TenderMegaHash set isVerified='yes' WHERE tenderId = @v_fieldName2Vc
			
			/* Start : Code to insert into table - tbl_TenderBidPlainData
			insert into tbl_TenderBidPlainData([bidTableId]
				  ,[tenderCelId]
				  ,[tenderColId]
				  ,[tenderTableId]
				  ,[cellValue]
				  ,[rowId]
				  ,[tenderFormId]
				  ,[cellId]
				  ,[submissionDt])
			select tbd.bidTableId,cellId,columnId,tb.tenderTableId,cellValue,rowId,
			t.tenderFormId,null,null from tbl_tenderbidform t,tbl_TenderForms tf,tbl_tenderbidtable tb,
			tbl_TenderBidDetail tbd,tbl_finalsubmission f
			 where t.tenderId=@v_fieldName2Vc and t.tenderFormId=tf.tenderFormId and t.tenderid=f.tenderid and t.userId=f.userId
			  and bidSubStatus='finalsubmission'
			 and isPriceBid='No' and tb.bidid=t.bidid and tbd.bidTableId=tb.bidTableId*/
			 /* end : Code to insert into table - tbl_TenderBidPlainData*/
			
		End	
	End	
	ELSE
	BEGIN	
		SET @v_flag_inVc = 'false'		
		INSERT INTO dbo.tbl_BidTemper (tenderId) SELECT @v_fieldName2Vc			
	END
	
	Select @v_flag_inVc as FieldValue1
	
End


IF @v_fieldName1Vc='getTenderInfoForExtension'
BEGIN
	
	--select Convert(varchar(50),tenderId) as FieldValue1,	
	--Case When (openingDt is null Or openingDt='') then 'N.A.' When openingDt > getdate() Then 'no' Else 'yes' End as FieldValue2,
	--Case When (getdate() >= openingDt And getdate() <= DATEADD(HOUR,1,openingDt)) Then 'yes' Else 'no' End as FieldValue3,
	--Case When (getdate() > DATEADD(HOUR,1,openingDt)) Then 'yes' Else 'no' End as FieldValue4,
	--Case When Exists  (select top 1 comMemberId from tbl_CommitteeMembers Where appStatus='approved' And committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc)) Then 'yes' Else 'no' End as FieldValue5,
	--Case When Exists  (select top 1 comMemberId from tbl_CommitteeMembers Where userId=@v_fieldName3Vc And committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc)) Then 'yes' Else 'no' End as FieldValue6,	
	--Case When Exists  (select top 1 comMemberId from tbl_CommitteeMembers Where userId=@v_fieldName3Vc And appStatus='approved' And committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc)) Then 'yes' Else 'no' End as FieldValue7,	
	--Case When Exists (select top 1 committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc) Then 'yes' Else 'no' End as FieldValue8,	
	--(select isnull(convert(varchar(15),	openingDt,103) + ' ' +Substring(CONVERT(VARCHAR(5),openingDt,108),1,5) ,'N.A.') from tbl_TenderDetails Where tenderId=@v_fieldName2Vc) as FieldValue9,
	--(select CONVERT(varchar(50), userId) from  tbl_EmployeeMaster 
	--	where employeeId in(select employeeId from tbl_EmployeeRoles 
	--		where departmentId= (select departmentId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
	--and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='HOPE'))) as FieldValue10
	--from tbl_TenderDetails 
	--where tenderId=@v_fieldName2Vc
	
	
		select Convert(varchar(50),TD.tenderId) as FieldValue1,	
	Case When (TOD.openingDt is null Or TOD.openingDt='') then 'N.A.' When TOD.openingDt > getdate() Then 'no' Else 'yes' End as FieldValue2,
	Case When (getdate() >= TOD.openingDt And getdate() <= DATEADD(HOUR,1,TOD.openingDt)) Then 'yes' Else 'no' End as FieldValue3,
	Case When (getdate() > DATEADD(HOUR,1,TOD.openingDt)) Then 'yes' Else 'no' End as FieldValue4,
	Case When Exists  (select top 1 comMemberId from tbl_CommitteeMembers Where appStatus='approved' And committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc)) Then 'yes' Else 'no' End as FieldValue5,
	Case When Exists  (select top 1 comMemberId from tbl_CommitteeMembers Where userId=@v_fieldName3Vc And committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc)) Then 'yes' Else 'no' End as FieldValue6,	
	Case When Exists  (select top 1 comMemberId from tbl_CommitteeMembers Where userId=@v_fieldName3Vc And appStatus='approved' And committeeId in (select committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc)) Then 'yes' Else 'no' End as FieldValue7,	
	Case When Exists (select top 1 committeeId from tbl_Committee where committeeType in ('TOC', 'POC') And committeStatus='approved' And tenderId=@v_fieldName2Vc) Then 'yes' Else 'no' End as FieldValue8,	
	(select isnull(REPLACE(CONVERT(VARCHAR(11),	openingDt, 106), ' ', '-')  + ' ' +Substring(CONVERT(VARCHAR(5),openingDt,108),1,5) ,'N.A.') from tbl_TenderDetails Where tenderId=@v_fieldName2Vc) as FieldValue9,
	(select CONVERT(varchar(50), userId) from  tbl_EmployeeMaster 
		where employeeId in(select employeeId from tbl_EmployeeRoles 
			where departmentId= (select departmentId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
	and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='HOPE'))) as FieldValue10
	from tbl_TenderDetails TD Inner Join tbl_TenderOpenDates TOD On TD.tenderId=TOD.tenderId
	where TD.tenderId=@v_fieldName2Vc
	
END


IF @v_fieldName1Vc='getDeclarationDoneStatus'
BEGIN
Select Case When (Count(confirmationId)>0) Then 'yes' Else 'no' End as FieldValue1 
From tbl_BidConfirmation Where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc
END	


IF @v_fieldName1Vc='getOpeningDtExtList'
BEGIN
	Select Convert(varchar(50), ExtDtId) as FieldValue1, 
	REPLACE(CONVERT(VARCHAR(11),extReqDt, 106), ' ', '-')  +' ' +Substring(CONVERT(VARCHAR(30),extReqDt,108),1,5) as FieldValue2,   
	REPLACE(CONVERT(VARCHAR(11),newOpenDt, 106), ' ', '-')  +' ' +Substring(CONVERT(VARCHAR(30),newOpenDt,108),1,5) as FieldValue3,
    EM.employeeName as FieldValue4, dbo.f_initcap(extStatus) as FieldValue5,
    Case finalOpenDt When '1900-01-01 00:00:00' Then '-' Else  
    REPLACE(CONVERT(VARCHAR(11),finalOpenDt, 106), ' ', '-')  +' ' +Substring(CONVERT(VARCHAR(30),finalOpenDt,108),1,5)
    End as FieldValue6
	from tbl_TenderOpenExt TOE inner join tbl_LoginMaster LM On TOE.extReqBy=LM.userId
	inner join tbl_EmployeeMaster EM On TOE.extReqBy=EM.userId
	Where tenderId=@v_fieldName2Vc
	Order by extReqDt desc
END


IF @v_fieldName1Vc='getOpeningDtExtDetails'
BEGIN
	Select Convert(varchar(50), ExtDtId) as FieldValue1, 
	REPLACE(CONVERT(VARCHAR(11),extReqDt, 106), ' ', '-')  +' ' +Substring(CONVERT(VARCHAR(30),extReqDt,108),1,5) as FieldValue2,   
	REPLACE(CONVERT(VARCHAR(11),newOpenDt, 106), ' ', '-')  +' ' +Substring(CONVERT(VARCHAR(30),newOpenDt,108),1,5) as FieldValue3,
    EM.employeeName as FieldValue4, dbo.f_initcap(extStatus) as FieldValue5,
    (select employeeName From tbl_EmployeeMaster Where userId=TOE.userId) as FieldValue6,
    TOE.remarks as FieldValue7,
    Case When (finalOpenDt='1900-01-01 00:00:00') Then '-' Else 
		REPLACE(CONVERT(VARCHAR(11),finalOpenDt, 106), ' ', '-')  +' ' +Substring(CONVERT(VARCHAR(30),finalOpenDt,108),1,5) 
	End as FieldValue8,   
	Case When (dbo.f_trim(hopeComments)='') Then '-' Else  ISNULL(hopeComments,'-') End FieldValue9,
	 Case When (approvalDt='1900-01-01 00:00:00') Then '-' Else 
		REPLACE(CONVERT(VARCHAR(11),approvalDt, 106), ' ', '-')  +' ' +Substring(CONVERT(VARCHAR(30),approvalDt,108),1,5) 
	End as FieldValue10   
	from tbl_TenderOpenExt TOE 
	inner join tbl_LoginMaster LM On TOE.extReqBy=LM.userId
	inner join tbl_EmployeeMaster EM On TOE.extReqBy=EM.userId
	Where tenderId=@v_fieldName2Vc And ExtDtId=@v_fieldName3Vc
	Order by extReqDt desc
END
IF @v_fieldName1Vc='getEmailIdfromUserId'
BEGIN
select emailid as FieldValue1,
REPLACE(CONVERT(VARCHAR(11),registeredDate, 106), ' ', '-')  +' ' +Substring(CONVERT(VARCHAR(30),registeredDate,108),1,5) as FieldValue2
from tbl_LoginMaster where userId=@v_fieldName2Vc
END

IF @v_fieldName1Vc='getInfoForOpeningDateApproval'
BEGIN
	--select top 1  
	--REPLACE(CONVERT(VARCHAR(11),TOD.openingDt, 106), ' ', '-')  +' ' +Substring(CONVERT(VARCHAR(30),TOD.openingDt,108),1,5) as FieldValue1,
	--REPLACE(CONVERT(VARCHAR(11),TOE.newOpenDt, 106), ' ', '-')  +' ' +Substring(CONVERT(VARCHAR(30),TOE.newOpenDt,108),1,5) as FieldValue2,	 
	--TOE.remarks as FieldValue3,CONVERT(VARCHAR(50),extReqBy) FieldValue4
	--from tbl_TenderOpenDates TOD Inner Join tbl_TenderOpenExt TOE On TOE.tenderId=TOD.tenderId
	--Where TOE.extStatus='pending' And
	--TOD.tenderId=@v_fieldName2Vc And TOE.ExtDtId=@v_fieldName3Vc
	
	select top 1  
	CONVERT(VARCHAR(10),TOD.openingDt,103) +' ' +Substring(CONVERT(VARCHAR(30),TOD.openingDt,108),1,5) as FieldValue1,
	CONVERT(VARCHAR(10),TOE.newOpenDt,103) +' ' +Substring(CONVERT(VARCHAR(30),TOE.newOpenDt,108),1,5) as FieldValue2,
	TOE.remarks as FieldValue3,CONVERT(VARCHAR(50),extReqBy) FieldValue4
	from tbl_TenderOpenDates TOD Inner Join tbl_TenderOpenExt TOE On TOE.tenderId=TOD.tenderId
	Where TOE.extStatus='pending' And
	TOD.tenderId=@v_fieldName2Vc And TOE.ExtDtId=@v_fieldName3Vc
	
	
	
END
IF @v_fieldName1Vc='getNegDocDetails'
BEGIN
SELECT count(negDocId) as FieldValue1
  FROM  [tbl_NegotiationDocs] n where 
  n.negid=@v_fieldName2Vc
END

IF @v_fieldName1Vc='getFinalSubComp'
BEGIN
	--SELECT CONVERT(VARCHAR(10),tenderid) as FieldValue1,
	--CONVERT(VARCHAR(10),l.userid) as FieldValue2, 
	--CASE WHEN t.companyId=1 THEN firstName+' '+lastname ELSE companyName end  as FieldValue3, 
	--'-1' as FieldValue4
	--FROM dbo.tbl_loginmaster l,tbl_companymaster c,tbl_TendererMaster t,tbl_FinalSubmission f
	--WHERE l.userId=f.userId and c.companyId=t.companyId
	--and t.userid=l.userid and f.tenderId=@v_fieldName2Vc
	--and bidSubStatus='finalsubmission'
	
	SELECT CONVERT(VARCHAR(10),tenderid) as FieldValue1,
	CONVERT(VARCHAR(10),l.userid) as FieldValue2, 
	CASE WHEN t.companyId=1 THEN firstName+' '+lastname ELSE companyName end  as FieldValue3, 
	'-1' as FieldValue4,
	Case 
		When (select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='Services'
		Then 
			Case When (select COUNT(esFormDetailId) from tbl_EvalSerFormDetail where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) > 0
			Then 'yes'
			Else 'No'
			End 
		Else 	
			Case When (select COUNT(evalMemStatusId) from tbl_EvalMemStatus where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) > 0
			Then 'yes'
			Else 'No'
			End 
		End
	as 	FieldValue5,
	Case 
		When (select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='Services'
		Then 
			Case When 
				(select COUNT(esFormDetailId) from tbl_EvalSerFormDetail where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) 
				= 
				(select COUNT(distinct tbf.tenderFormId) 
					from tbl_TenderBidForm tbf 
					inner join tbl_TenderForms tf On tbf.tenderFormId=tf.tenderFormId
					where tf.isPriceBid='No' and  userId=l.userId And tenderId=@v_fieldName2Vc)
			Then 'yes'
			Else 'No'
			End 
		Else 	
			Case When 
				(select COUNT(evalMemStatusId) from tbl_EvalMemStatus where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) 
				= 
				(select COUNT(distinct tbf.tenderFormId) 
					from tbl_TenderBidForm tbf 
					inner join tbl_TenderForms tf On tbf.tenderFormId=tf.tenderFormId
					where tf.isPriceBid='No' and  userId=l.userId And tenderId=@v_fieldName2Vc)
			Then 'yes'
			Else 'No'
			End 
		End
	as 	FieldValue6,CONVERT(varchar(50),t.tendererId) as FieldValue7,CONVERT(varchar(50),t.companyId) as FieldValue8
	FROM dbo.tbl_loginmaster l,tbl_companymaster c,tbl_TendererMaster t,tbl_FinalSubmission f
	WHERE l.userId=f.userId and c.companyId=t.companyId
	and t.userid=l.userid and f.tenderId=@v_fieldName2Vc
	and bidSubStatus='finalsubmission'
	
END

IF @v_fieldName1Vc='getNegDetails'
BEGIN
SELECT   CONVERT(VARCHAR(10),n.[negId])  as FieldValue1
    ,REPLACE(CONVERT(VARCHAR(11),[negStartDt], 106), ' ', '-')  +' ' +Substring(CONVERT(VARCHAR(30),[negStartDt],108),1,5)   as FieldValue2 
      ,REPLACE(CONVERT(VARCHAR(11),[negEndDt], 106), ' ', '-')  +' ' +Substring(CONVERT(VARCHAR(30),[negEndDt],108),1,5)  as FieldValue3
     
      ,[negStatus] as FieldValue9
       ,case when t.companyId=1 then firstName+' '+lastname else companyName end  
       as FieldValue4
      ,[negLocDetails] as FieldValue5
      ,convert(varchar(20),l.userId) as FieldValue6
      ,[negMode] as FieldValue7,nt.acceptStatus  as FieldValue8,reportAppStatus as FieldValue10
  FROM   tbl_Negotiation n,tbl_LoginMaster l,tbl_TendererMaster t,
  tbl_CompanyMaster c,tbl_NegNotifyTenderer nt
  where n.negid=nt.negid
  and nt.userid=l.userId and l.userId=t.userId
  and t.companyId=c.companyId
  and tenderid=@v_fieldName2Vc
END



IF @v_fieldName1Vc='getBidderNegDetails'
BEGIN
SELECT   CONVERT(VARCHAR(10),n.[negId])  as FieldValue1
    
      ,REPLACE(CONVERT(VARCHAR(11),[negStartDt], 106), ' ', '-')  +' ' +Substring(CONVERT(VARCHAR(30),[negStartDt],108),1,5)   as FieldValue2 
      ,REPLACE(CONVERT(VARCHAR(11),[negEndDt], 106), ' ', '-')  +' ' +Substring(CONVERT(VARCHAR(30),[negEndDt],108),1,5)  as FieldValue3
     
      ,[negStatus] as FieldValue9
       ,case when t.companyId=1 then firstName+' '+lastname else companyName end  
       as FieldValue4
      ,[negLocDetails] as FieldValue5
      ,[negRemarks] as FieldValue6
      ,[negMode] as FieldValue7, nt.acceptStatus as FieldValue8,reportAppStatus as FieldValue10
  FROM   tbl_Negotiation n,tbl_LoginMaster l,tbl_TendererMaster t,
  tbl_CompanyMaster c,tbl_NegNotifyTenderer nt
  where n.negid=nt.negid
  and nt.userid=l.userId and l.userId=t.userId
  and t.companyId=c.companyId
  and tenderid=@v_fieldName2Vc and l.userId=@v_fieldName3Vc
END

IF @v_fieldName1Vc='getBidderNegDetailsById'
BEGIN
SELECT   CONVERT(VARCHAR(10),n.[negId])  as FieldValue1
    
      , REPLACE(CONVERT(VARCHAR(11),[negStartDt], 106), ' ', '-')  as FieldValue2
      ,REPLACE(CONVERT(VARCHAR(11),[negEndDt], 106), ' ', '-')  as FieldValue3
      ,[negStatus] as FieldValue4
       ,case when t.companyId=1 then firstName+' '+lastname else companyName end  
       as FieldValue4
      ,[negLocDetails] as FieldValue5
      ,[negRemarks] as FieldValue6
      ,[negMode] as FieldValue7
  FROM   tbl_Negotiation n,tbl_LoginMaster l,tbl_TendererMaster t,
  tbl_CompanyMaster c,tbl_NegNotifyTenderer nt
  where n.negid=nt.negid
  and nt.userid=l.userId and l.userId=t.userId
  and t.companyId=c.companyId
  and tenderid=@v_fieldName2Vc   and n.negid=@v_fieldName3Vc
END

 If @v_fieldName1Vc='NegotiationDocInfo'
   Begin
		Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(negDocId  as varchar(10)) as FieldValue4 
		from tbl_NegotiationDocs Where negid=@v_fieldName2Vc
   End
   
   
  
   
      If @v_fieldName1Vc='NegQryDocs'
   Begin
		Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(negQueryDocId  as varchar(10)) as FieldValue4 
		from tbl_NegQryDocs Where negId=@v_fieldName2Vc and negQueryId=@v_fieldName3Vc
   End 
   
     If @v_fieldName1Vc='NegReplyDocs'
   Begin
		Select documentName as FieldValue1, docDescription as FieldValue2, docSize as FieldValue3,cast(negReplyDocId  as varchar(10)) as FieldValue4 
		from tbl_NegReplyDocs Where negQueryId=@v_fieldName2Vc
   End   
   
   
   
If @v_fieldName1Vc='GetPackageDetails'
   Begin
		SELECT packageNo as FieldValue1,packageDescription as FieldValue2,cast(pkgLotId as varchar(50)) as FieldValue3 
FROM dbo.tbl_TenderDetails inner join tbl_NoaIssueDetails ON tbl_TenderDetails.tenderId=tbl_NoaIssueDetails.tenderId where tbl_TenderDetails.tenderId=@v_fieldName2Vc and tbl_NoaIssueDetails.userId=@v_fieldName3Vc
   End
   
   
If @v_fieldName1Vc='GetLotDetails'
   Begin
		SELECT cast(tenderId as varchar(20)) as FieldValue1,cast(pkgLotId as varchar(50)) as FieldValue2 
		FROM tbl_NoaIssueDetails where tbl_NoaIssueDetails.tenderId=@v_fieldName2Vc and tbl_NoaIssueDetails.userId=@v_fieldName3Vc
   End
   
   
If @v_fieldName1Vc='SingleLotDetails'
   Begin
		select cast(lotNo as varchar(20)) as FieldValue1,cast(lotDesc as varchar(2000)) as FieldValue2 from tbl_TenderLotSecurity where tbl_TenderLotSecurity.appPkgLotId=@v_fieldName2Vc
   End
   
   
If @v_fieldName1Vc='ContractListing'
   Begin
		select cast(contractNo as varchar(20)) as FieldValue1,cast(contractAmt as varchar(50)) as FieldValue2,
			REPLACE(CONVERT(VARCHAR(11),contractDt, 106), ' ', '-') as FieldValue3,
			ISNULL(REPLACE(CONVERT(VARCHAR(11), acceptRejDt, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), acceptRejDt,108), 1, 5), '-')  as FieldValue4,
			cast(tbl_NoaIssueDetails.noaIssueId as varchar(50)) as FieldValue5,
			acceptRejStatus as FieldValue6,cast(tbl_NoaIssueDetails.tenderId as varchar(50)) as FieldValue7,cast(tbl_NoaIssueDetails.pkgLotId as varchar(50)) as FieldValue8,
			ISNULL(REPLACE(CONVERT(VARCHAR(11),tbl_NoaIssueDetails.noaAcceptDt, 106), ' ', '-'),'-') as FieldValue9
			from tbl_NoaIssueDetails inner join tbl_NoaAcceptance 
			on tbl_NoaIssueDetails.noaIssueId=tbl_NoaAcceptance.noaIssueId 
			where tbl_NoaIssueDetails.userId=@v_fieldName2Vc and tbl_NoaIssueDetails.tenderId=@v_fieldName3Vc
		
		--select cast(contractNo as varchar(20)) as FieldValue1,cast(contractAmt as varchar(50)) as FieldValue2,REPLACE(CONVERT(VARCHAR(11),createdDt, 106), ' ', '-') as FieldValue3,REPLACE(CONVERT(VARCHAR(11),contractDt, 106), ' ', '-') as FieldValue4,cast(tbl_NoaIssueDetails.noaIssueId as varchar(50)) as FieldValue5,acceptRejStatus as FieldValue6,cast(tbl_NoaIssueDetails.tenderId as varchar(50)) as FieldValue7
		--from tbl_NoaIssueDetails inner join tbl_NoaAcceptance on tbl_NoaIssueDetails.noaIssueId=tbl_NoaAcceptance.noaIssueId where tbl_NoaIssueDetails.userId=@v_fieldName2Vc
   End
   
If @v_fieldName1Vc='GetPackagerow'
   Begin
	--select cast(tbl_tenderdetails.tenderId as varchar(50)) as FieldValue1,tbl_tenderdetails.reoiRfpRefNo as FieldValue2,contractNo as FieldValue3,cast(noaIssueId as varchar(20)) as FieldValue4,(select acceptRejStatus from tbl_NoaAcceptance where noaIssueId=tbl_NoaIssueDetails.noaIssueId) as FieldValue5
	--from tbl_NoaIssueDetails  inner join tbl_tenderdetails on tbl_NoaIssueDetails.tenderId=tbl_tenderdetails.tenderId where tbl_NoaIssueDetails.noaIssueId=@v_fieldName2Vc
	select 
	cast(tbl_tenderdetails.tenderId as varchar(50)) as FieldValue1,
	tbl_tenderdetails.reoiRfpRefNo as FieldValue2,
	contractNo as FieldValue3,
	cast(noaIssueId as varchar(20)) as FieldValue4,
	(select acceptRejStatus from tbl_NoaAcceptance where noaIssueId=tbl_NoaIssueDetails.noaIssueId )as FieldValue5,
	dbo.f_getbiddercompany(tbl_NoaIssueDetails.userId) as FieldValue6,
	lotNo as FieldValue7,
	lotDesc as FieldValue8 
	from tbl_NoaIssueDetails inner join tbl_tenderdetails 
	on tbl_NoaIssueDetails.tenderId=tbl_tenderdetails.tenderId 
	inner join tbl_TenderLotSecurity tls On tbl_NoaIssueDetails.pkgLotId=tls.appPkgLotId
	where tbl_NoaIssueDetails.noaIssueId=@v_fieldName2Vc
   End
   
   
If @v_fieldName1Vc='GetAwardDetails'
   Begin
   select cast(contractNo as varchar(20)) as FieldValue1,
	contractName as FieldValue2,
	REPLACE(CONVERT(VARCHAR(11),contractDt, 106), ' ', '-')  as FieldValue3,
	cast(contractAmt as varchar(100))+ ' ('+ RTRIM(contractAmtWords)+ ')' as FieldValue4,
	ttd.peOfficeName as FieldValue5,
	ttd.peName as FieldValue7,
	ttd.peDesignation as FieldValue8,
	(select departmentname from tbl_departmentmaster where departmentid=(select departmentId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)) as FieldValue6
	from tbl_NoaIssueDetails tnd,tbl_TenderDetails ttd where tnd.noaIssueId =@v_fieldName3Vc and ttd.tenderId=@v_fieldName2Vc and ttd.tenderId=tnd.tenderId  	
   End
   
   
If @v_fieldName1Vc='GetAwardDetails2'
   Begin
	select cast(noaIssueDays as varchar(20)) as FieldValue1,
	cast(perfSecAmt as varchar(100))+' ('+ RTRIM(perfSecAmtWords) + ')' as FieldValue2,
	cast(perSecSubDays as varchar(20)) as FieldValue3,
	REPLACE(CONVERT(VARCHAR(11),perSecSubDt, 106), ' ', '-')  as FieldValue4,
	REPLACE(CONVERT(VARCHAR(11),contractSignDt, 106), ' ', '-')  as FieldValue5,
	acceptRejStatus as FieldValue6,
	tbl_NoaAcceptance.comments as FieldValue7,
	tbl_NoaAcceptance.acceptRejStatus as FieldValue8,
	cast(noaIssueDays as varchar(50)) as FieldValue9
	from tbl_NoaIssueDetails inner join tbl_NoaAcceptance on tbl_NoaIssueDetails.noaIssueId=tbl_NoaAcceptance.noaIssueId where tbl_NoaIssueDetails.noaIssueId =@v_fieldName2Vc
   End
   
   
If @v_fieldName1Vc='GetprocurementNature'
   Begin
	select procurementnature as FieldValue1,REPLACE(CONVERT(VARCHAR(11),finalSubmissionDt, 106), ' ', '-')  as FieldValue2 from tbl_tenderDetails inner join tbl_finalsubmission on tbl_tenderDetails.tenderId=tbl_finalsubmission.tenderId
	where tbl_finalsubmission.tenderId=@v_fieldName2Vc and tbl_finalsubmission.userid=@v_fieldName3Vc and tbl_finalsubmission.bidSubStatus='finalsubmission'
   End
   
-- If @v_fieldName1Vc='getTenderClosedInfo'
--   Begin
--		/*
--			FieldValue1= shows Count of Members that need to sign. [No. of TOC/POC Member * No. of Tender Lots * 2] (Two TOR Reports - TOR1, TOR2) 
--			FieldValue2= shows Count of Signed Member
--			FieldValue3= shows whether tender is closed or not,
--			FieldValue4= shows whether tender has been sent to PE Officer or not
--			FieldValue5= shows whether tender has been sent to TEC ChairPerson or Not
--			FieldValue6= show the listingId from table tbl_TOSListing if it exists
--		*/
--	Select 		
--			(Select Convert(varchar(50),COUNT(distinct tbl_CommitteeMembers.comMemberId)) 
--						From 
--							tbl_Committee,tbl_CommitteeMembers,tbl_EmployeeMaster,tbl_EmployeeOffices,
--							tbl_DesignationMaster 
--							where tbl_Committee.committeeId=tbl_CommitteeMembers.committeeId  
--								  and tbl_CommitteeMembers.userId=tbl_EmployeeMaster.userid
--								  and tbl_EmployeeMaster.employeeId=tbl_EmployeeOffices.employeeId and tbl_EmployeeOffices.designationId=tbl_DesignationMaster.designationId 
--								  and committeStatus='approved' and committeeType in ('TOC','POC') 
--								  and tenderId=@v_fieldName2Vc
--		    ) as FieldValue1, 
--			(Select Convert(varchar(50),COUNT(distinct userId))   from tbl_TosSheetSign TSS 
--						Inner Join  tbl_Committee C On TSS.committeeId=C.committeeId
--				    Where C.committeStatus='approved' and  committeeType in ('TOC','POC') And C.tenderId=@v_fieldName2Vc
--		   ) as FieldValue2,	
			
--		Case When Exists (Select tenderClosingId From tbl_TenderClose Where tenderId=@v_fieldName2Vc) Then 'yes'
--			Else 'no' End as FieldValue3,
--		Case When Exists (Select tenderId from tbl_TOSListing where tenderId=@v_fieldName2Vc) Then 'yes' Else 'no' End as FieldValue4,
--		Case When Exists (Select tenderId from tbl_TOSRptShare where tenderId=@v_fieldName2Vc And listingId=(Select listingId from tbl_TOSListing where tenderId=@v_fieldName2Vc)
--		) Then 'yes' Else 'no' End as FieldValue5,
--		IsNull((Select Convert(varchar(50),listingId) from tbl_TOSListing where tenderId=@v_fieldName2Vc),'0') as FieldValue6,
		
--		--IsNull((Select Convert(varchar(50), Count(tenderFormId)) from tbl_TenderForms Where tenderSectionId in (Select distinct tenderSectionId from tbl_TenderSection Where tenderStdId in (Select distinct tenderStdId from tbl_TenderStd Where tenderId=@v_fieldName2Vc))),'0') as FieldValue7,
--		Case When @v_fieldName3Vc='1'
--Then 
--	IsNull((select Convert(varchar(50), COUNT( distinct tbf.tenderFormId))  
--	from tbl_TenderBidForm tbf, tbl_FinalSubmission fs, tbl_TenderForms tf 
--	where tbf.tenderId=fs.tenderId and tbf.tenderId=@v_fieldName2Vc and tbf.tenderFormId=tf.tenderFormId
--	and bidSubStatus='finalsubmission' ),'0')
--	When @v_fieldName3Vc='2'
--Then 
--	IsNull((select Convert(varchar(50), COUNT( distinct tbf.tenderFormId))  
--	from tbl_TenderBidForm tbf, tbl_FinalSubmission fs, tbl_TenderForms tf 
--	where tbf.tenderId=fs.tenderId and tbf.tenderId=@v_fieldName2Vc and tbf.tenderFormId=tf.tenderFormId
--	and bidSubStatus='finalsubmission' And isPriceBid='No' ),'0')
--End
-- as FieldValue7,
--		IsNull
--		(
--			(select Convert(varchar(50), Count(distinct tenderFormId)) from tbl_TenderBidPlainData Where tenderFormId in	
--		( Select distinct tenderFormId from tbl_TenderForms Where tenderSectionId in 
--			(Select distinct tenderSectionId from tbl_TenderSection Where tenderStdId in 
--				(Select distinct tenderStdId from tbl_TenderStd Where tenderId=@v_fieldName2Vc)
--			)
--		)
--			)
--		,'0') as FieldValue8
		
			 
		
--   End  



 If @v_fieldName1Vc='getTenderClosedInfo'
   Begin
		/*
			FieldValue1= shows Count of Members that need to sign. [No. of TOC/POC Member * No. of Tender Lots * 2] (Two TOR Reports - TOR1, TOR2) 
			FieldValue2= shows Count of Signed Member
			FieldValue3= shows whether tender is closed or not,
			FieldValue4= shows whether tender has been sent to PE Officer or not
			FieldValue5= shows whether tender has been sent to TEC ChairPerson or Not
			FieldValue6= show the listingId from table tbl_TOSListing if it exists
		*/
	Select 		
		(Select Convert(varchar(50),(COUNT(distinct tbl_CommitteeMembers.comMemberId) * COUNT(distinct(tbl_TenderLotSecurity.appPkgLotId)) * 2))
						From 
							tbl_Committee,
							tbl_CommitteeMembers,
							tbl_EmployeeMaster,
							tbl_EmployeeOffices,
							tbl_DesignationMaster,
							tbl_TenderLotSecurity 
							where tbl_Committee.committeeId=tbl_CommitteeMembers.committeeId  
								  and tbl_CommitteeMembers.userId=tbl_EmployeeMaster.userid
								  and tbl_EmployeeMaster.employeeId=tbl_EmployeeOffices.employeeId 
								  and tbl_EmployeeOffices.designationId=tbl_DesignationMaster.designationId 
								  and tbl_Committee.tenderId=tbl_TenderLotSecurity.tenderId
								  and committeStatus='approved' and committeeType in ('TOC','POC') 
								  and tbl_Committee.tenderId=@v_fieldName2Vc
		    ) as FieldValue1, 
		    
		    
		(Select Convert(varchar(50),COUNT(distinct torSignId)) from tbl_TORRptSign TRS 
						Inner Join  tbl_Committee C On TRS.tenderId=C.tenderId
				    Where C.committeStatus='approved' and  committeeType in ('TOC','POC') 
				    And C.tenderId=@v_fieldName2Vc
		   ) as FieldValue2,	
		   
		   
			
		Case When Exists (Select tenderClosingId From tbl_TenderClose Where tenderId=@v_fieldName2Vc) Then 'yes'
			Else 'no' End as FieldValue3,
		Case When Exists (Select tenderId from tbl_TOSListing where tenderId=@v_fieldName2Vc) Then 'yes' Else 'no' End as FieldValue4,
		Case When Exists (Select tenderId from tbl_TOSRptShare where tenderId=@v_fieldName2Vc And listingId=(Select listingId from tbl_TOSListing where tenderId=@v_fieldName2Vc)
		) Then 'yes' Else 'no' End as FieldValue5,
		IsNull((Select Convert(varchar(50),listingId) from tbl_TOSListing where tenderId=@v_fieldName2Vc),'0') as FieldValue6,
		
		--IsNull((Select Convert(varchar(50), Count(tenderFormId)) from tbl_TenderForms Where tenderSectionId in (Select distinct tenderSectionId from tbl_TenderSection Where tenderStdId in (Select distinct tenderStdId from tbl_TenderStd Where tenderId=@v_fieldName2Vc))),'0') as FieldValue7,
		Case When @v_fieldName3Vc='1'
Then 
	IsNull((select Convert(varchar(50), COUNT( distinct tbf.tenderFormId))  
	from tbl_TenderBidForm tbf, tbl_FinalSubmission fs, tbl_TenderForms tf 
	where tbf.tenderId=fs.tenderId and tbf.tenderId=@v_fieldName2Vc and tbf.tenderFormId=tf.tenderFormId
	and bidSubStatus='finalsubmission' ),'0')
	When @v_fieldName3Vc='2'
Then 
	IsNull((select Convert(varchar(50), COUNT( distinct tbf.tenderFormId))  
	from tbl_TenderBidForm tbf, tbl_FinalSubmission fs, tbl_TenderForms tf 
	where tbf.tenderId=fs.tenderId and tbf.tenderId=@v_fieldName2Vc and tbf.tenderFormId=tf.tenderFormId
	and bidSubStatus='finalsubmission' And isPriceBid='No' ),'0')
	else '0' 
	
End
 as FieldValue7,
		IsNull
		(
			(select Convert(varchar(50), Count(distinct tenderFormId)) from tbl_TenderBidPlainData Where tenderFormId in	
		( Select distinct tenderFormId from tbl_TenderForms Where tenderSectionId in 
			(Select distinct tenderSectionId from tbl_TenderSection Where tenderStdId in 
				(Select distinct tenderStdId from tbl_TenderStd Where tenderId=@v_fieldName2Vc)
			)
		)
			)
		,'0') as FieldValue8
		
			 
		
   End  
   
IF @v_fieldName1Vc='getPaidCustomers_forBankUser'
BEGIN
	Set @v_Qry_Vc='
		select CONVERT(varchar(50), LM.userId)  as FieldValue1, emailId as FieldValue2, 
			CONVERT(varchar(50), CM.companyId) as FieldValue3, 
			dbo.f_getbiddercompany(LM.userId) as FieldValue4,
			CONVERT(varchar(50), TP.tenderPaymentId) as FieldValue5,			
			dbo.f_initcap(TP.status) FieldValue6, TP.paymentMode as FieldValue7,
			CONVERT(varchar(50), TP.createdBy)  as FieldValue8
			from 
			tbl_TenderPayment TP
			inner join tbl_LoginMaster LM on LM.userId=TP.userId
			inner join tbl_tenderermaster TM on LM.userId=TM.userId
			inner join tbl_CompanyMaster CM on TM.companyId=CM.companyId
			Where LM.status=''approved'' And '
			+@v_fieldName2Vc+
			' ORder by TP.tenderPaymentId desc'
			
			
			--TP.paymentFor='Document Fees' And TP.tenderId=@v_fieldName2Vc And TP.pkgLotId=@v_fieldName3Vc
			--ORder by TP.tenderPaymentId desc
			
	Print (@v_Qry_Vc)		
	Exec (@v_Qry_Vc)
END  

IF @v_fieldName1Vc='getPEOfficerUserIdfromTenderId'
BEGIN	
	--select Convert(varchar(50),e.userId) as FieldValue1, e.employeeName as FieldValue2
	--from tbl_tendermaster  t,tbl_AppMaster a,tbl_employeemaster e 
	--where t.appId=a.appId and tenderid=@v_fieldName2Vc and a.employeeid=e.employeeId;
	
	--select Convert(varchar(50),e.userId) as FieldValue1, 
	--		e.employeeName as FieldValue2, 
	--		o.officeName as FieldValue3, 
	--		departmentName as FieldValue4
	--from tbl_tendermaster  t,
	--	tbl_AppMaster a,
	--	tbl_employeemaster e, 
	--	tbl_EmployeeOffices eo, 
	--	tbl_OfficeMaster o, 
	--	tbl_DepartmentMaster dm
	--where t.appId=a.appId 
	--	and tenderid=@v_fieldName2Vc 
	--	and a.employeeid=e.employeeId 
	--	and e.employeeId=eo.employeeId 
	--	and eo.officeId=o.officeId 
	--	and o.departmentId=dm.departmentId; 
	
	select Convert(varchar(50),e.userId) as FieldValue1, 
			e.employeeName as FieldValue2, 
			o.officeName as FieldValue3, 
			departmentName as FieldValue4,
			emailId as FieldValue5
	from tbl_tendermaster  t,
		tbl_AppMaster a,
		tbl_employeemaster e, 
		tbl_EmployeeOffices eo, 
		tbl_OfficeMaster o, 
		tbl_DepartmentMaster dm,
		tbl_LoginMaster lm
	where t.appId=a.appId 
		and tenderid=@v_fieldName2Vc 
		and a.employeeid=e.employeeId 
		and e.employeeId=eo.employeeId 
		and eo.officeId=o.officeId 
		and o.departmentId=dm.departmentId
		and e.userId=lm.userId; 
	
END


IF @v_fieldName1Vc='getTendererCompanyName'
BEGIN	
	
Declare @v_CompanyId int
Select @v_CompanyId =  TM.companyId from tbl_LoginMaster LM 
Inner Join tbl_TendererMaster TM On LM.userId = TM.userId Where LM.userId=@v_fieldName2Vc 

	If @v_CompanyId=1
	Begin
		-- // Take the First name and last name from table - tbl_CompanyMaster
		Select (TM.firstName + IsNull(' '+ TM.lastName,'')) as FieldValue1,
		IsNull((C.countryCode + TM.mobileNo),'') as FieldValue2  
		from tbl_LoginMaster LM 
			Inner Join tbl_TendererMaster TM On LM.userId = TM.userId
			Inner Join tbl_CountryMaster C ON TM.country=C.countryName
			Where LM.userId=@v_fieldName2Vc 
	End
	Else
	Begin
		-- // Take the company name from table - tbl_CompanyMaster
		Select companyName as FieldValue1,
		IsNull((C.countryCode + TM.mobileNo),'') as FieldValue2 
		from tbl_LoginMaster LM 
			Inner Join tbl_TendererMaster TM On LM.userId = TM.userId	
			Inner join tbl_CompanyMaster CM On TM.companyId=CM.companyId
			Inner Join tbl_CountryMaster C ON TM.country=C.countryName
			Where LM.userId=@v_fieldName2Vc 
	End
END



IF @v_fieldName1Vc = 'gettenderlotbytenderidForPayment'     -- For LotTendPrep.jsp
BEGIN
	If @v_fieldName3Vc='ts' Or @v_fieldName3Vc='ps'
	Begin
		SELECT lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3 
			FROM  dbo.tbl_TenderLotSecurity 
			WHERE tenderId = @v_fieldName2Vc  	
	End
	Else
	Begin
		SELECT @v_docAvlMethod_Vc = docAvlMethod FROM dbo.tbl_TenderDetails WHERE tenderId = @v_fieldName2Vc

		IF @v_docAvlMethod_Vc = 'Lot'
		BEGIN		
			SELECT lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3 
			FROM  dbo.tbl_TenderLotSecurity 
			WHERE tenderId = @v_fieldName2Vc  	
		END
		ELSE
		BEGIN		
			SELECT 'Package' as FieldValue1 
			
		END

	End

	
END

IF @v_fieldName1Vc = 'getTenderPerformanceSecurityInfoforPayment'     -- For LotTendPrep.jsp
BEGIN
	select IsNull(Convert(varchar(50), perfSecAmt),'N.A.') as FieldValue1  
	from tbl_noaissuedetails Where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
END


IF @v_fieldName1Vc='getSecurityPaymentInfo'
BEGIN	
		select CONVERT(varchar(50),TM.tenderId) as FieldValue1, 
		docFeesMode as FieldValue2, 
		docAvlMethod as FieldValue3, 
		IsNull(REPLACE(CONVERT(VARCHAR(11),TD.docEndDate, 106), ' ', '-') ,'') as FieldValue4, 
		IsNull(CONVERT(varchar(50), datediff(Minute, GETDATE(), TD.docEndDate)),'null') as FieldValue5,		
		IsNull(Convert(varchar(50), TLS.docFess ),'')  as FieldValue6, 
		IsNull(REPLACE(CONVERT(VARCHAR(11),TD.tenderSecurityDt, 106), ' ', '-') ,'') as FieldValue7,
		IsNull(CONVERT(varchar(50), datediff(Minute, GETDATE(), TD.tenderSecurityDt)),'null') as FieldValue8,
		IsNull(Convert(varchar(50),Sum(TLS.tenderSecurityAmt)),'')  as FieldValue9,
		IsNull(CONVERT(varchar(50), datediff(Minute, GETDATE(), NOA.perSecSubDt)),'null') as FieldValue10
		from tbl_TenderDetails TD 
		inner join tbl_TenderMaster TM	On TD.tenderId=TM.tenderId
		inner join tbl_TenderLotSecurity TLS On TLS.tenderId=TM.tenderId
		left join tbl_noaissuedetails NOA On TM.tenderId=NOA.tenderId		
		where TM.tenderId=@v_fieldName2Vc and TLS.appPkgLotId=@v_fieldName3Vc
		Group by TM.tenderId, docFeesMode, docAvlMethod, TD.docEndDate, TLS.docFess, TD.tenderSecurityDt, TLS.tenderSecurityAmt,NOA.perSecSubDt	
	
END

IF @v_fieldName1Vc = 'getAdditionalPaymentDetail' 
BEGIN
	/*
		FieldValue1 = Payment done for
		FieldValue2 = Mode of Payment
		FieldValue3 = Amount of Payment
		FieldValue4 = UserId of Bidder/Tenderer for which Payment is done 
		FieldValue5 = UserId of one who has done the payment
		FieldValue6 = Current Status of Payment		
	*/
	select top 1 paymentFor as FieldValue1, 
	dbo.f_initcap(paymentMode) as FieldValue2,
	Convert(varchar(50),amount) as FieldValue3,
	Convert(varchar(50),userId) as FieldValue4,  				
	Convert(varchar(50),createdBy) as FieldValue5,
  	dbo.f_initcap(status) as FieldValue6,
	REPLACE(CONVERT(VARCHAR(11),createdDate, 106), ' ', '-')  +' ' +Substring(CONVERT(VARCHAR(30),createdDate,108),1,5) as FieldValue7	
	from tbl_TenderPayment Where tenderPaymentId=@v_fieldName2Vc
	
END


IF @v_fieldName1Vc = 'getPerSecPayConditionForUser' -- for/partner/SearchTenderer.jsp
BEGIN
	--Set @v_Qry_Vc='	
	--select top 1 Convert(varchar(50),IsNull(noaIssueId,0)) FielsVaue1,
	--		IsNull(CONVERT(varchar(50), datediff(Minute, GETDATE(), perSecSubDt)),''0'') as FieldValue2,					
	--		IsNull(CONVERT(varchar(50), perfSecAmt),''0'') as FieldValue3 
	--		from tbl_noaissuedetails 
	--		Where ' + @v_fieldName2Vc
	
	Set @v_Qry_Vc='	
	select top 1 Convert(varchar(50),IsNull(ND.noaIssueId,0)) FielsVaue1,
			IsNull(CONVERT(varchar(50), datediff(Minute, GETDATE(), perSecSubDt)),''0'') as FieldValue2,					
			IsNull(CONVERT(varchar(50), perfSecAmt),''0'') as FieldValue3,
			IsNull(CONVERT(varchar(50), datediff(Minute, GETDATE(), ND.noaAcceptDt)),''0'') as FieldValue4,
			NA.acceptRejStatus as FieldValue5
			from tbl_noaissuedetails ND Inner Join tbl_NoaAcceptance NA On ND.noaIssueId=NA.noaIssueId
			Where '+ @v_fieldName2Vc + ' Order by NA.noaAcceptId desc, ND.noaIssueId desc'
		
		--print (@v_Qry_Vc)
		Exec (@v_Qry_Vc)
END


IF @v_fieldName1Vc = 'getFormsForEvlauationMapping'     -- For officer/MapEvalForms.jsp
BEGIN
		
	IF @v_fieldName3Vc <> '0'
	BEGIN	
		--SELECT formName as FieldValue1, 
		--CONVERT(varchar(50), tf.tenderFormId) as FieldValue2,
		--Case 
		--	When Exists (Select evalFormId from tbl_EvalMapForms Where formId=tf.tenderFormId And tenderId=@v_fieldName2Vc And pkgLotId=@v_fieldName3Vc And tecCom='yes')  
		--	Then 'tecCom'		
		--	When Exists (Select evalFormId from tbl_EvalMapForms Where formId=tf.tenderFormId And tenderId=@v_fieldName2Vc And pkgLotId=@v_fieldName3Vc And tscCom='yes')  
		--	Then 'tscCom'		
		--	Else 'null'
		--End as 	FieldValue3 		
		--FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		--ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		--dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId Left Outer Join 
		--dbo.tbl_TenderResultSharing tr On td.tenderId = tr.tenderId And tf.tenderFormId = tr.formId 
		--Where tf.isPriceBid='no' And td.tenderId = @v_fieldName2Vc and packageLotId = @v_fieldName3Vc
		--Order By isPriceBid	
		
		SELECT formName as FieldValue1, 
		CONVERT(varchar(50), tf.tenderFormId) as FieldValue2,
		Case 
			When Exists (Select evalFormId from tbl_EvalMapForms Where formId=tf.tenderFormId And tenderId=@v_fieldName2Vc And pkgLotId=@v_fieldName3Vc And tecCom='yes')  
			Then 'tecCom'		
			When Exists (Select evalFormId from tbl_EvalMapForms Where formId=tf.tenderFormId And tenderId=@v_fieldName2Vc And pkgLotId=@v_fieldName3Vc And tscCom='yes')  
			Then 'tscCom'		
			Else 'null'
		End as 	FieldValue3 		
		FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 		
		Where tf.isPriceBid='no' And td.tenderId = @v_fieldName2Vc and packageLotId = @v_fieldName3Vc
		And (tf.formStatus is null or tf.formStatus in ('p', 'cp'))
		Order By isPriceBid		
	END
	ELSE
	BEGIN
		--SELECT formName as FieldValue1,
		--CONVERT(varchar(50), tf.tenderFormId) as FieldValue2,
		--Case 
		--	When Exists (Select evalFormId from tbl_EvalMapForms Where formId=tf.tenderFormId And tenderId=@v_fieldName2Vc And tecCom='yes')  
		--	Then 'tecCom'		
		--	When Exists (Select evalFormId from tbl_EvalMapForms Where formId=tf.tenderFormId And tenderId=@v_fieldName2Vc And tscCom='yes')  
		--	Then 'tscCom'		
		--	Else 'null'
		--End as 	FieldValue3
		--FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		--ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		--dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId Left Outer Join 
		--dbo.tbl_TenderResultSharing tr On td.tenderId = tr.tenderId And tf.tenderFormId = tr.formId 
		--Where tf.isPriceBid='no' And td.tenderId = @v_fieldName2Vc
		--Order By isPriceBid		
		
		SELECT formName as FieldValue1,
		CONVERT(varchar(50), tf.tenderFormId) as FieldValue2,
		Case 
			When Exists (Select evalFormId from tbl_EvalMapForms Where formId=tf.tenderFormId And tenderId=@v_fieldName2Vc And tecCom='yes')  
			Then 'tecCom'		
			When Exists (Select evalFormId from tbl_EvalMapForms Where formId=tf.tenderFormId And tenderId=@v_fieldName2Vc And tscCom='yes')  
			Then 'tscCom'		
			Else 'null'
		End as 	FieldValue3
		FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 		
		Where tf.isPriceBid='no' And td.tenderId = @v_fieldName2Vc
		And (tf.formStatus is null or tf.formStatus in ('p', 'cp'))
		Order By isPriceBid		
		
	END
		
END

IF @v_fieldName1Vc = 'getTenderDocFeesInfo'     
BEGIN
	select 
	CONVERT(varchar(50), tenderId) as FieldValue1, 
	docAvlMethod as FieldValue2, 
	docFeesMethod as FieldValue3, 
	docFeesMode as FieldValue4 
	from tbl_TenderDetails Where tenderId=@v_fieldName2Vc
END



IF @v_fieldName1Vc='getTenderPaymentInfoForDocFees'
BEGIN	
	
	
	select @v_DocFeesMethod_Vc=docFeesMethod from tbl_TenderDetails where tenderId=@v_fieldName2Vc
	If @v_DocFeesMethod_Vc='Package wise'
	Begin	
		select CONVERT(varchar(50),TM.tenderId) as FieldValue1, 
		docFeesMode as FieldValue2, 
		docAvlMethod as FieldValue3, 
		IsNull(REPLACE(CONVERT(VARCHAR(11),TD.docEndDate, 106), ' ', '-') ,'') as FieldValue4, 
		IsNull(CONVERT(varchar(50), datediff(Minute, GETDATE(), TD.docEndDate)),'null') as FieldValue5,
		IsNull(Convert(varchar(50), pkgDocFees),'')  as FieldValue6, 
		IsNull(REPLACE(CONVERT(VARCHAR(11),TD.tenderSecurityDt, 106), ' ', '-') ,'') as FieldValue7,
		IsNull(CONVERT(varchar(50), datediff(Minute, GETDATE(), TD.tenderSecurityDt)),'null') as FieldValue8,
		IsNull(Convert(varchar(50),TD.pkgDocFees),'') as FieldValue9,
		IsNull(CONVERT(varchar(50), datediff(Minute, GETDATE(), NOA.perSecSubDt)),'null') as FieldValue10
		from tbl_TenderDetails TD 
		inner join tbl_TenderMaster TM On TD.tenderId=TM.tenderId
		inner join tbl_TenderLotSecurity TLS On TLS.tenderId=TM.tenderId
		left join tbl_noaissuedetails NOA On TM.tenderId=NOA.tenderId
		where TM.tenderId=@v_fieldName2Vc
		Group by TM.tenderId, docFeesMode, docAvlMethod, TD.docEndDate, pkgDocFees, TD.tenderSecurityDt, TLS.tenderSecurityAmt,NOA.perSecSubDt
	End
	ELSE If @v_DocFeesMethod_Vc='Lot wise'
	Begin
		select CONVERT(varchar(50),TM.tenderId) as FieldValue1, 
		docFeesMode as FieldValue2, 
		docAvlMethod as FieldValue3, 
		IsNull(REPLACE(CONVERT(VARCHAR(11),TD.docEndDate, 106), ' ', '-') ,'') as FieldValue4, 
		IsNull(CONVERT(varchar(50), datediff(Minute, GETDATE(), TD.docEndDate)),'null') as FieldValue5,		
		IsNull(Convert(varchar(50), TLS.docFess ),'')  as FieldValue6, 
		IsNull(REPLACE(CONVERT(VARCHAR(11),TD.tenderSecurityDt, 106), ' ', '-') ,'') as FieldValue7,
		IsNull(CONVERT(varchar(50), datediff(Minute, GETDATE(), TD.tenderSecurityDt)),'null') as FieldValue8,
		IsNull(Convert(varchar(50),TLS.tenderSecurityAmt),'')  as FieldValue9,
		IsNull(CONVERT(varchar(50), datediff(Minute, GETDATE(), NOA.perSecSubDt)),'null') as FieldValue10
		from tbl_TenderDetails TD 
		inner join tbl_TenderMaster TM	On TD.tenderId=TM.tenderId
		inner join tbl_TenderLotSecurity TLS On TLS.tenderId=TM.tenderId
		left join tbl_noaissuedetails NOA On TM.tenderId=NOA.tenderId		
		where TM.tenderId=@v_fieldName2Vc and TLS.appPkgLotId=@v_fieldName3Vc
		Group by TM.tenderId, docFeesMode, docAvlMethod, TD.docEndDate, TLS.docFess, TD.tenderSecurityDt, TLS.tenderSecurityAmt,NOA.perSecSubDt	
	End
END
   
   
IF @v_fieldName1Vc='getBasicInfoForPayment' -- partner/ForPayment.jsp
BEGIN
	/*
		FieldValue1=shows type of docFeesMehod and whether Document Fees is payable or free
		FieldValue1=shows type of Tender Security and whether it is payable or free
		FieldValue3=shows whether the tender(status) has been published or no
	*/
	
	Select 
		Case docFeesMethod 
			When 'Package wise' Then 'Package Wise' 
			When 'Lot wise' Then 'Lot wise'
			Else 'null'
		End as FieldValue1,
		Case When Exists (select top 1 tenderLotSecId from tbl_TenderLotSecurity Where tenderId=@v_fieldName2Vc And tenderSecurityAmt>0 )
			 Then 'paid'
			 Else 'free'
		End as FieldValue2,
		tenderStatus as FieldValue3, 
		docFeesMode as FieldValue4,
		Case docFeesMethod 
		When 'Package wise'	
		Then 
			Case 
				When (pkgDocFees > 0) 
				Then 'Paid'
				Else 'Free'
			End
		When 'Lot wise'
		Then 
			Case 
				When ((select Sum(docFess) from tbl_TenderLotSecurity Where tenderId=@v_fieldName2Vc) > 0)
				Then 'Paid'
				Else 'Free'
			End
		Else 'Free'		
		End as FieldValue5,
		Case 
			When GETDATE() > submissionDt
			Then 'Yes' 
			Else 'No'
			End as FieldValue6,
		Case 
			When Exists (select top 1 tenderId from tbl_Committee c
							inner join tbl_CommitteeMembers cm on c.committeeId=cm.committeeId
							where c.tenderId=@v_fieldName2Vc
							and c.committeeType in ('toc','poc')
							and c.committeStatus='approved'
							and cm.appStatus='approved'
						)
			Then 'Yes'
			Else 'No'
		End as FieldValue7		
			
		From tbl_TenderDetails 
		Where tenderId=@v_fieldName2Vc
END   


IF @v_fieldName1Vc='getEvaluationFormQuestions' 
BEGIN
	Set @v_Qry_Vc='Select CONVERT(varchar(50), evalQueId) as FieldValue1, 
	   CONVERT(varchar(50), tenderId) as FieldValue2,  
	   CONVERT(varchar(50), pkgLotId) as FieldValue3,
	   CONVERT(varchar(50), tenderFormId) as FieldValue4, 
	   CONVERT(varchar(50), userId) as FieldValue5, 
	   question as FieldValue6, 
	   answer FieldValue7, Isnull((select Isnull(employeeName, '''') 
	   from tbl_EmployeeMaster where userId=tbl_EvalFormQues.quePostedBy), '''') as FieldValue8, 
	   CONVERT(varchar(50), quePostedBy) as FieldValue9 
	from dbo.tbl_EvalFormQues Where ' + @v_fieldName2Vc   
	
	--Print (@v_Qry_Vc)
	Exec (@v_Qry_Vc)
END


IF @v_fieldName1Vc = 'EvalTECChairPerson'  
BEGIN
select distinct convert(varchar(20),cm.userid) as FieldValue1, 
memberRole as FieldValue2,
IsNull((Select Convert(varchar(50),listingId) from tbl_TOSListing where tenderId=@v_fieldName2Vc),'0') as FieldValue3,
dbo.f_GovUserName(cm.govUserId, 'tbl_EmployeeTrasfer') as FieldValue4,
 convert(varchar(20),cm.govUserId) as FieldValue5,emailId as FieldValue6

from tbl_committee c,tbl_CommitteeMembers cm,tbl_loginmaster lm
 where tenderId=@v_fieldName2Vc and c.committeeId=cm.committeeId  and lm.userId=cm.userId
 and committeeType in('TEC','PEC')  and memberRole='cp'
END

IF @v_fieldName1Vc='getBidderClarificationCondition' 
BEGIN
	Select IsNull(CONVERT(varchar(50), datediff(Minute, GETDATE(), expectedComplDt)),'null') as FieldValue1,
	ISNULL(isClarificationComp,'null') as FieldValue2,
	Case 
		When Exists (Select top 1 evalQueId from tbl_EvalFormQues Where (answer <>'' And answer is not null) And tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc)	
		Then 'yes'
		Else 'no'
		End	FieldValue3,
	remarks as FieldValue4,
	REPLACE(CONVERT(VARCHAR(11),expectedComplDt, 106), ' ', '-')  +' ' +Substring(CONVERT(VARCHAR(30),expectedComplDt,108),1,5) as FieldValue5		
	From tbl_EvalBidderResp EBR Where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc
END




IF @v_fieldName1Vc = 'getFormsForEvlauationQuestions'     -- For officer/MapEvalForms.jsp
BEGIN	
		--SELECT formName as FieldValue1, 
		--CONVERT(varchar(50), tf.tenderFormId) as FieldValue2			
		--FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		--ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		--dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId Left Outer Join 
		--dbo.tbl_TenderResultSharing tr On td.tenderId = tr.tenderId And tf.tenderFormId = tr.formId 
		--Where tf.isPriceBid='no' And td.tenderId = @v_fieldName2Vc and packageLotId = @v_fieldName3Vc
		--And tf.tenderFormId in (select distinct tenderForm Idfrom tbl_EvalFormQues Where tender)
		--Order By isPriceBid		
		
		--Set @v_Qry_Vc='SELECT formName as FieldValue1, 
		--CONVERT(varchar(50), tf.tenderFormId) as FieldValue2			
		--FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		--ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		--dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 
		--Where tf.isPriceBid=''no'' And ' + @v_fieldName2Vc +
		--' And tf.tenderFormId in (select distinct tenderFormId from tbl_EvalFormQues Where ' + @v_fieldName3Vc + ' )
		--Order By isPriceBid'		
		
		Set @v_Qry_Vc='SELECT formName as FieldValue1, 
		CONVERT(varchar(50), tf.tenderFormId) as FieldValue2,
		Case When ((select COUNT(evalQueId) from tbl_EvalFormQues where '+@v_fieldName3Vc+' And answer is not null and answer<> '''' And tbl_EvalFormQues.tenderFormId=tf.tenderFormId)>0)
		Then ''Yes'' Else ''No'' End as FieldValue3
		FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 
		Where tf.isPriceBid=''no'' And ' + @v_fieldName2Vc +
		' And tf.tenderFormId in (select distinct tenderFormId from tbl_EvalFormQues Where ' + @v_fieldName3Vc + ' )
		Order By isPriceBid'
		
	--Print (@v_Qry_Vc)
	Exec (@v_Qry_Vc)
END


IF @v_fieldName1Vc = 'getCurrentUserPaymentInfo'     -- For officer/MapEvalForms.jsp
BEGIN	
		
	Set @v_Qry_Vc='Select 
		Case 
			When Exists (Select top 1 tenderPaymentId from tbl_TenderPayment Where paymentFor=''Document Fees'' And ' + @v_fieldName2Vc + ')
			Then ''paid''
			Else ''pending''
		End as FieldValue1,
		Case When Exists (Select top 1 tenderPaymentId from tbl_TenderPayment Where paymentFor=''Tender Security'' And ' + @v_fieldName2Vc + ' And pkgLotId=' + @v_fieldName3Vc +')
			Then ''paid''
			Else ''pending''
		End as FieldValue2
		'
	--Print (@v_Qry_Vc)
	Exec (@v_Qry_Vc)
END

IF @v_fieldName1Vc = 'getValidityExtensionBidder'
BEGIN
	if not exists(select * from tbl_TenderValAcceptance where tenderId=@v_fieldName2Vc)
	BEGIN
		select a.*,case when isnull(b.tenderPaymentId,0) =0 then 'Not Paid' else 'Paid' End FieldValue3,
		CAST(b.tenderPaymentId as varchar(10)) as FieldValue4
		,cast(b.userId as varchar(10)) as FieldValue5,cast(b.pkgLotId as varchar(10)) as FieldValue6,cast(b.paymentMode as varchar(10)) as FieldValue7 
		from (SELECT distinct convert(varchar(20), tbl_FinalSubmission.userId) as FieldValue1, (select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue2
		from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c
			where l.userId=t.userId and t.companyId=c.companyId and l.userid = tbl_FinalSubmission.userId) as FieldValue2
		FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission
		 ON tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId AND
		  tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId 
		  where tbl_FinalSubmission.bidSubStatus = 'finalsubmission' and tenderid = @v_fieldName2Vc)a
		  left outer join(select * from tbl_tenderpayment where tenderId=@v_fieldName2Vc   and (extValidityRef=@v_fieldName3Vc or extValidityRef is null))b
		  on a.FieldValue1=b.userId
	END
	ELSE
	BEGIN  
		select a.*,case when isnull(b.tenderPaymentId,0) =0 then 'Not Paid' else 'Paid' End FieldValue3,
		CAST(b.tenderPaymentId as varchar(10)) as FieldValue4
		,cast(b.userId as varchar(10)) as FieldValue5,cast(b.pkgLotId as varchar(10)) as FieldValue6,cast(b.paymentMode as varchar(10)) as FieldValue7,
		(select status from tbl_TenderValAcceptance where tenderId=@v_fieldName2Vc and userId=b.userId) FieldValue8 
		 from (SELECT distinct convert(varchar(20), tbl_FinalSubmission.userId) as FieldValue1, (select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue2 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c
			where l.userId=t.userId and t.companyId=c.companyId and l.userid = tbl_FinalSubmission.userId) as FieldValue2
		FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission 
		 ON tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId AND
		  tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId 
		  INNER JOIN (select userid from  tbl_TenderValAcceptance 
		  where valExtDtId =@v_fieldName3Vc and tenderid=@v_fieldName2Vc) ta on ta.userId=tbl_FinalSubmission.userId
		  where tbl_FinalSubmission.bidSubStatus = 'finalsubmission' and tbl_FinalSubmission.tenderId = @v_fieldName2Vc)a
		  left outer join(select * from tbl_tenderpayment where tenderId=@v_fieldName2Vc   and extValidityRef=@v_fieldName3Vc)b
		  on a.FieldValue1=b.userId 
	END   
END

IF @v_fieldName1Vc = 'showExtendLink'
BEGIN
	select extStatus as FieldValue1 from tbl_TenderValidityExtDate where tenderId=@v_fieldName2Vc and extStatus='Pending'
End	


IF @v_fieldName1Vc = 'GetAllBidderForExt'
BEGIN
	select cast(f.userId as varchar(20)) as FieldValue1,l.emailId as FieldValue2,(select emailid from tbl_loginmaster where userid=@v_fieldName3Vc) as FieldValue3 from tbl_loginmaster l,tbl_companymaster c,tbl_TendererMaster t,tbl_FinalSubmission f
	where l.userId=f.userId and c.companyId=t.companyId
	and t.userid=l.userid and f.tenderId=@v_fieldName2Vc
	and bidSubStatus='finalsubmission'
End	


IF @v_fieldName1Vc = 'getFormNameFromFormId'
BEGIN
	select formName as FieldValue1 from tbl_TenderForms Where tenderFormId=@v_fieldName2Vc
END


IF @v_fieldName1Vc = 'getTenderLotsForBidderClarification'     -- For LotTendPrep.jsp
BEGIN
SELECT lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3 
	FROM  dbo.tbl_TenderLotSecurity 
	WHERE tenderId = @v_fieldName2Vc  	
	And appPkgLotId In (
		Select pkgLotId From tbl_EvalFormQues Where userId=@v_fieldName3Vc And tenderId=@v_fieldName2Vc And queSentByTec=1
	)

END

IF @v_fieldName1Vc = 'getTenderInfoForPayment'     
BEGIN
select case When submissionDt > GETDATE() Then 'yes' Else 'no'
 End as FieldValue1
 from tbl_TenderDetails Where tenderId=@v_fieldName2Vc
END

IF @v_fieldName1Vc = 'getValidityRefId'     
BEGIN
 Select top 1 Convert(varchar(50),Max(valExtDtId)) as FieldValue1  from tbl_TenderValidityExtDate 
 Where tenderId=@v_fieldName2Vc And extStatus='Approved' 
 And tenderSecNewDt is not null and tenderSecNewDt <>'1900-01-01'
 Group by valExtDtId, tenderSecNewDt
 Order by valExtDtId desc
END

IF @v_fieldName1Vc = 'chkTECTSCComMember'
BEGIN

	Select userId, committeeName from tbl_Committee tc, tbl_CommitteeMembers tcm where tc.committeeId = tcm.committeeId and tc.tenderId = @v_fieldName2Vc and tc. committeetype  in ('TEC','PEC') and tc.committeStatus = 'approved' and userid = @v_fieldName3Vc
	union 
	Select userId, committeeName from tbl_Committee tc, tbl_CommitteeMembers tcm where tc.committeeId = tcm.committeeId and tc.tenderId = @v_fieldName2Vc and tc. committeetype in ('TSC','PSC') and tc.committeStatus = 'approved' and userid = @v_fieldName3Vc
	
	
END

IF @v_fieldName1Vc = 'GetEvaluatedBidders'
BEGIN
if(select COUNT(tenderId) from tbl_TenderDetails where procurementNatureId = 3 and tenderId = @v_fieldName2Vc)!=0
begin 
	SELECT  case cm.CompanyName when '-' then firstName+' '+lastName else cm.companyName end as FieldValue1,
	convert(varchar(20),tm.userid) as FieldValue2,convert(varchar(20),t.rank) as FieldValue3, convert(varchar(20),t.amount) as FieldValue4,
	convert(varchar(20),t.rank)  as FieldValue5
	  FROM  [tbl_BidderRank] t,tbl_TendererMaster tm,tbl_companymaster cm,tbl_ReportMaster rm
	 where t.userId=tm.userId and t.tenderId=@v_fieldName2Vc  
	and tm.companyId=cm.companyId 
	--and t.userId in 
	--(select userId from tbl_PostQualification where tenderId=@v_fieldName2Vc 
	--and postQualStatus='Qualify' and noaStatus!='rejected')	
	and t.reportId = rm.reportId and rm.isTORTER='TER' 
	and t.userId not in (select userid from tbl_NoaIssueDetails where tenderId=@v_fieldName2Vc)
end
else
begin
	SELECT  case cm.CompanyName when '-' then firstName+' '+lastName else cm.companyName end as FieldValue1,
	convert(varchar(20),tm.userid) as FieldValue2,convert(varchar(20),t.rank) as FieldValue3, convert(varchar(20),t.amount) as FieldValue4,
	convert(varchar(20),t.rank)  as FieldValue5
	  FROM  [tbl_BidderRank] t,tbl_TendererMaster tm,tbl_companymaster cm,tbl_ReportMaster rm
	 where t.userId=tm.userId and t.tenderId=@v_fieldName2Vc  and t.pkgLotId = @v_fieldName3Vc
	and tm.companyId=cm.companyId and t.reportId = rm.reportId and rm.isTORTER='TER'  and t.userId not in (select userid from tbl_NoaIssueDetails where tenderId=@v_fieldName2Vc and pkgLotId = @v_fieldName3Vc)
	and t.userId in 
	(select userId from tbl_PostQualification where tenderId=@v_fieldName2Vc 
	and postQualStatus='Qualify' and noaStatus!='rejected' and pkgLotId = @v_fieldName3Vc)
	
	
	
end	
	
END
IF @v_fieldName1Vc = 'GetEvaluatedBiddersNOA'
BEGIN
if(select COUNT(tenderId) from tbl_TenderDetails where procurementNatureId = 3 and tenderId = @v_fieldName2Vc)!=0
begin 
	SELECT  dbo.f_getbiddercompany(t.userId) as FieldValue1,
	convert(varchar(20),t.userid) as FieldValue2,convert(varchar(20),t.rank) as FieldValue3, convert(varchar(20),t.amount) as FieldValue4
	  FROM  [tbl_BidderRank] t,tbl_ReportMaster rm
	 where t.tenderId=@v_fieldName2Vc  
	and t.userId in 
	(select userId from tbl_PostQualification where tenderId=@v_fieldName2Vc 
	and postQualStatus='Qualify' and noaStatus!='rejected')	
	and t.reportId = rm.reportId and rm.isTORTER='TER' and t.userId not in (select userid from tbl_NoaIssueDetails where tenderId=@v_fieldName2Vc)
end
else
begin
	SELECT  dbo.f_getbiddercompany(t.userId)  as FieldValue1,
	convert(varchar(20),t.userid) as FieldValue2,convert(varchar(20),t.rank) as FieldValue3, convert(varchar(20),t.amount) as FieldValue4
	  FROM  [tbl_BidderRank] t,tbl_ReportMaster rm
	 where t.tenderId=@v_fieldName2Vc  and t.pkgLotId = @v_fieldName3Vc
	 and t.reportId = rm.reportId and rm.isTORTER='TER'  and t.userId not in (select userid from tbl_NoaIssueDetails where tenderId=@v_fieldName2Vc and pkgLotId = @v_fieldName3Vc)
	 and t.userId in 
	(select userId from tbl_PostQualification where tenderId=@v_fieldName2Vc 
	and postQualStatus='Qualify' and noaStatus!='rejected' and pkgLotId = @v_fieldName3Vc)
	

end	
	
END

IF @v_fieldName1Vc = 'chkTOSComMember'
BEGIN

	Select userId, committeeName from tbl_Committee tc, tbl_CommitteeMembers tcm where tc.committeeId = tcm.committeeId and tc.tenderId = @v_fieldName2Vc and tc. committeetype in ('TOC','POC') and tc.committeStatus = 'approved' and userid = @v_fieldName3Vc
	
	
END


IF @v_fieldName1Vc = 'getTenderDatesDiff'
BEGIN
Select 
Convert(varchar(50), datediff(Day, GETDATE(), dbo.f_gettendervaliditydate(@v_fieldName2Vc))) as FieldValue1,
Convert(varchar(50),datediff(Day, GETDATE(), dbo.f_gettendersecuritydate(@v_fieldName2Vc))) as FieldValue2

END


IF @v_fieldName1Vc = 'getCurrentPaymentValidityRef'
BEGIN
	Select IsNull(Convert(varchar(50),extValidityRef),'0') as FieldValue1, 
			paymentMode as FieldValue2,
			Case When Exists (
		Select tenderPaymentId from tbl_TenderPayment 
		Where 
			tenderId=(select tenderId from tbl_TenderPayment Where tenderPaymentId=@v_fieldName2Vc)
			And pkgLotId=(select pkgLotId from tbl_TenderPayment Where tenderPaymentId=@v_fieldName2Vc)
			And userId=(select userId from tbl_TenderPayment Where tenderPaymentId=@v_fieldName2Vc)
			And paymentFor=(select paymentFor from tbl_TenderPayment Where tenderPaymentId=@v_fieldName2Vc)
	) Then 'yes' Else 'no' End as FieldValue3			 
	from tbl_TenderPayment Where tenderPaymentId=@v_fieldName2Vc
END


IF @v_fieldName1Vc = 'getPendingActionForPayment'
BEGIN
	select Case 
	When Exists
		(Select requestAction from tbl_PaymentActionRequest Where paymentId=@v_fieldName2Vc and requestAction='release' and actionStatus='pending')
	Then 'yes' else 'no'
	End as FieldValue1,
	Case 
	When Exists
		(Select requestAction from tbl_PaymentActionRequest Where paymentId=@v_fieldName2Vc and requestAction='forfeit' and actionStatus='pending')
	Then 'yes' else 'no'
	End as FieldValue2
END

IF @v_fieldName1Vc = 'GetPerformanceSecurityList'
BEGIN
	select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue1,cast(l.userId as varchar(10)) as FieldValue2,
	cast(tenderPaymentId as varchar(50)) as FieldValue3,
		REPLACE(CONVERT(VARCHAR(11),instDate, 106), ' ', '-')  as FieldValue4,
		cast(tp.userId as varchar(50)) as FieldValue5,
		cast(pkgLotId as varchar(50)) as FieldValue6,
		paymentMode as FieldValue7,
		cast(extValidityRef as varchar(50)) as FieldValue8
	 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c,tbl_TenderPayment tp
		where l.userId=t.userId and t.companyId=c.companyId and tp.userId=l.userId
		and paymentFor='Performance Security' and tenderId=@v_fieldName2Vc
END	

IF @v_fieldName1Vc = 'GetBidderDefaultList'
BEGIN
	--select  case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue1,cast(l.userId as varchar(10)) as FieldValue2,
	--cast(tenderPaymentId as varchar(50)) as FieldValue3,
	--	convert(varchar(15),instDate,103) as FieldValue4,
	--	cast(tp.userId as varchar(50)) as FieldValue5,
	--	cast(pkgLotId as varchar(50)) as FieldValue6,
	--	paymentMode as FieldValue7,
	--	cast(extValidityRef as varchar(50)) as FieldValue8
	-- from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c,tbl_TenderPayment tp
	--	where l.userId=t.userId and t.companyId=c.companyId and tp.userId=l.userId and tenderId=@v_fieldName2Vc
	
	select  distinct case CompanyName when '-' then firstName+' '+lastName else companyName end as FieldValue1,cast(l.userId as varchar(10)) as FieldValue2,
	cast(tenderPaymentId as varchar(50)) as FieldValue3,
		REPLACE(CONVERT(VARCHAR(11),instDate, 106), ' ', '-')  as FieldValue4,
		cast(tp.userId as varchar(50)) as FieldValue5,
		cast(pkgLotId as varchar(50)) as FieldValue6,
		paymentMode as FieldValue7,
		cast(extValidityRef as varchar(50)) as FieldValue8
	 from tbl_loginmaster l,tbl_TendererMaster t,tbl_CompanyMaster c,
	 (select * from tbl_tenderpayment where tenderPaymentId in(select MAX(tenderPaymentId) from tbl_tenderpayment where tenderid=@v_fieldName2Vc
	  and  paymentFor='Tender Security' and status in('paid','extended') group by tenderId,userId)) tp
		where l.userId=t.userId and t.companyId=c.companyId and tp.userId=l.userId and tenderId=@v_fieldName2Vc
END	


IF @v_fieldName1Vc = 'CheckTECMember'
BEGIN
	SELECT  emailId as FieldValue1,cast(reoiRfpRefNo as varchar(20)) as FieldValue2  from tbl_Committee c,tbl_CommitteeMembers cm,tbl_loginmaster lm,tbl_TenderDetails t
		where c.committeeId=cm.committeeId 
		and c.tenderId=@v_fieldName2Vc and committeeType in('TEC','PEC')
		and memberRole='cp' and lm.userId=cm.userId and t.tenderId=c.tenderId 
		and lm.userId=@v_fieldName3Vc
END



IF @v_fieldName1Vc = 'getPaymentPaidByUserId'     
BEGIN
 select convert(varchar(50), createdBy) as FieldValue1, 
 IsNull((select convert(varchar(50), verifiedBy) from tbl_TenderPaymentVerification where paymentId=tp.tenderPaymentId),'') as FieldValue2
 from tbl_TenderPayment tp Where tenderPaymentId=@v_fieldName2Vc
END

IF @v_fieldName1Vc = 'getCmpfordebar'  
BEGIN

declare @dibarQuery varchar(Max)
Set @dibarQuery = 
'Select ct.CompanyName as FieldValue1, convert(varchar(30),ct.userId )as FieldValue2 from 
(
Select case CompanyName when ''-'' then tm.firstName+'' ''+ tm.lastName else companyName end as companyName, lm.userId
from tbl_LoginMaster lm, tbl_TendererMaster tm , tbl_CompanyMaster tc
where lm.userId = tm.userId   and 
 status=''Approved'' and tc.companyid = tm.companyId '+case when @v_fieldName2Vc is not null then +' and ' + @v_fieldName2Vc else '' end +'
) as ct'
	print @dibarQuery
	exec(@dibarQuery)
END	



IF @v_fieldName1Vc = 'CheckSubmissionDateAndAcceptStatus'     
BEGIN
 	Declare @submissiondate as datetime
	set @submissiondate =(select tenderValidityDt from tbl_TenderDetails where tenderId=2059)

	if(CAST(FLOOR(CAST( @submissiondate AS float)) AS datetime) <= CAST(FLOOR(CAST( GETDATE() AS float)) AS datetime))
		BEGIN
		Declare @count1 as int
		if exists(select * from tbl_TenderValAcceptance where tenderId=@v_fieldName2Vc)
			BEGIN
				set @count1=(select cast(count(*) as varchar(10)) as FieldValue1 from tbl_TenderValAcceptance where tenderId=@v_fieldName2Vc and status='Approved'
				and valExtDtId=(select max(valExtDtId) from tbl_TenderValAcceptance where tenderId=@v_fieldName2Vc))
				if(@count1 >=0)
				BEGIN
					select cast('0' as varchar(10)) as FieldValue1
				END
			END
			ElSE
			BEGIN
				select cast('0' as varchar(10)) as FieldValue1
			END
		END
	ELSE
		BEGIN
			select cast('1' as varchar(10)) as FieldValue1
		END	
END


IF @v_fieldName1Vc = 'CheckValueIntblTosRptShare'     
BEGIN
 select cast(tenderId as varchar(10)) as FieldValue1 from tbl_TosRptShare where tenderId=@v_fieldName2Vc
END

IF @v_fieldName1Vc = 'getSingleTender'     --- For Debar
BEGIN
select cast(tenderId as varchar(10))as FieldValue1 ,reoiRfpRefNo as FieldValue2 ,tenderBrief as FieldValue3 from tbl_tenderdetails where officeId in(select officeId from tbl_EmployeeOffices e,tbl_EmployeeMaster em
where e.employeeId=em.employeeId and userId=@v_fieldName2Vc
)
END
IF @v_fieldName1Vc = 'getPackage'      --- --- For Debar
BEGIN
select cast(packageid as varchar(10)) as FieldValue1 ,appcode as FieldValue2,packageNo as FieldValue3 from tbl_AppMaster a,tbl_apppackages p where officeId in(select officeId from tbl_EmployeeOffices e,tbl_EmployeeMaster em
where e.employeeId=em.employeeId and userId=@v_fieldName2Vc) and a.appid=p.appid
END
IF @v_fieldName1Vc = 'getProject'      --- --- For Debar
BEGIN
select cast(projectId as varchar(10))as FieldValue1 ,projectName as FieldValue2 ,projectCode as FieldValue3 from tbl_ProjectMaster p where projectId in(select projectId from tbl_ProjectOffice 
where officeid in(select officeId from tbl_EmployeeOffices e,tbl_EmployeeMaster em
where e.employeeId=em.employeeId and userId=@v_fieldName2Vc)
)
END
IF @v_fieldName1Vc = 'getProcuringEntity'     --- --- For Debar 
BEGIN
select cast(e.officeId as varchar(10)) as FieldValue1 ,officeName as FieldValue2 ,pecode as FieldValue3 from tbl_EmployeeOffices e,tbl_EmployeeMaster em,tbl_officemaster o
where e.employeeId=em.employeeId and userId=@v_fieldName2Vc and e.officeid=o.officeId
END

IF @v_fieldName1Vc = 'getorganisation'     --- --- For Debar 
BEGIN
	select cast(d.departmentId as varchar(10)) as FieldValue1,departmentname as FieldValue2,d.departmentType as FieldValue3 from tbl_EmployeeOffices e,tbl_EmployeeMaster em,tbl_officemaster o,tbl_departmentmaster d
	where e.employeeId=em.employeeId and userId=@v_fieldName2Vc and e.officeid=o.officeId and o.departmentId=d.departmentId
END

IF @v_fieldName1Vc = 'GetTECEmail'
BEGIN
	select (select emailid from tbl_loginmaster where userid=extActionBy) as FieldValue1,
	(select emailid from tbl_loginmaster where userid=@v_fieldName3Vc) as FieldValue2 from tbl_TenderValidityExtDate where valExtDtId=@v_fieldName2Vc
END

IF @v_fieldName1Vc = 'FindServices'
BEGIN
	select ProcurementNature as FieldValue1 from tbl_TenderDetails where tenderId=@v_fieldName2Vc
END

IF @v_fieldName1Vc = 'tendererDebarDetail' ---by Taher for Debarment
BEGIN
		select tdr.clarification as FieldValue1,REPLACE(CONVERT(VARCHAR(11),tdr.lastResponseDt, 106), ' ', '-')     as FieldValue2,tdrp.responseTxt as FieldValue3,tcm.companyName as FieldValue4,tm.firstName as FieldValue5,tm.lastName as FieldValue6 ,tdr.comments as FieldValue9,convert(varchar(20),tdr.debarTypeId) as FieldValue10,
	(select employeename from tbl_EmployeeRoles er,tbl_EmployeeOffices eo,tbl_EmployeeMaster em
	where er.employeeid=eo.employeeId and procurementRoleId=6 and er.employeeId=em.employeeid and officeId in(select officeid from tbl_officemaster where departmentid in (select departmentId from tbl_EmployeeOffices eo,tbl_OfficeMaster om,tbl_EmployeeMaster em
	where eo.officeid=om.officeid and em.employeeId=eo.employeeid
	and userid=tdr.debarmentBy) 
	)) as FieldValue7,(select convert(varchar(20),em.userId)  from tbl_EmployeeRoles er,tbl_EmployeeOffices eo,tbl_EmployeeMaster em
	where er.employeeid=eo.employeeId and procurementRoleId=6 and er.employeeId=em.employeeid and officeId in(select officeid from tbl_officemaster where departmentid in (select departmentId from tbl_EmployeeOffices eo,tbl_OfficeMaster om,tbl_EmployeeMaster em
	where eo.officeid=om.officeid and em.employeeId=eo.employeeid
	and userid=tdr.debarmentBy)
	))as FieldValue8
	from tbl_DebarmentReq tdr,tbl_tenderermaster tm, tbl_CompanyMaster tcm,tbl_DebarmentResp tdrp
	where tdr.userid=tm.userid and tm.companyid=tcm.companyid 
	and tdr.debarmentId = tdrp.debarmentId
	and tdr.debarmentStatus=''+@v_fieldName3Vc+'' and tdr.debarmentId=@v_fieldName2Vc
	;	
END


If @v_fieldName1Vc='getTenderInfoForOpeningProcess'
BEGIN
	select 	
	Case When Exists (select tenderId from tbl_TenderOpenExt Where tenderId=@v_fieldName2Vc and extStatus='pending') Then 'yes' Else 'no' End as FieldValue1,
	Case When Exists (select tenderId from tbl_TenderMegaHash Where tenderId=@v_fieldName2Vc and isVerified='yes') Then 'yes' Else 'no' End as FieldValue2,
	Case When Exists (select tenderId from tbl_Committee where tenderId=@v_fieldName2Vc and committeeType in ('TOC','POC')) Then 'yes' Else 'no' End as FieldValue3,
	Case When (select tenderStatus from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='pending' Then 'no' Else 'yes' End as FieldValue4,
	Case When ((select Count(f.finalSubmissionId) from tbl_FinalSubmission f where bidSubStatus='finalsubmission' And tenderId=@v_fieldName2Vc)>0) Then 'Yes' Else 'No' End as FieldValue5

END

IF @v_fieldName1Vc = 'FindFormInTenderBidPlainData'
BEGIN
	select cast(bidPlainId as varchar(10)) as FieldValue1 from tbl_TenderBidPlainData where tenderFormId = @v_fieldName2Vc
END	
IF @v_fieldName1Vc = 'TOSSentToPE'
BEGIN
	select cast(tenderId as varchar(10)) as FieldValue1 from [tbl_TOSListing] where tenderid = @v_fieldName2Vc
END	
IF @v_fieldName1Vc = 'getAllNegdetails'
BEGIN
SELECT convert(varchar(30),negMode) as FieldValue1,convert(varchar(30),negFinalSub) as FieldValue2,
convert(varchar(30),negOfficeAggree) as FieldValue3, REPLACE(CONVERT(VARCHAR(11),negOfficerAggreeDt, 106), ' ', '-')  +' ' +Substring(CONVERT(VARCHAR(30),negOfficerAggreeDt,108),1,5) as FieldValue4, REPLACE(CONVERT(VARCHAR(11),reportAppDt, 106), ' ', '-')  +' ' +Substring(CONVERT(VARCHAR(30),reportAppDt,108),1,5)as FieldValue5, convert(varchar(30),bidAggree) as FieldValue6, REPLACE(CONVERT(VARCHAR(11),bidAggreeDt, 106), ' ', '-')  +' ' +Substring(CONVERT(VARCHAR(30),bidAggreeDt,108),1,5)as FieldValue7,case when convert(varchar(30),negdocId) IS null then 'no'  else 'yes' end as  FieldValue8
FROM  tbl_NegNotifyTenderer INNER JOIN tbl_Negotiation ON tbl_NegNotifyTenderer.negId = tbl_Negotiation.negId and tbl_Negotiation.negId =  @v_fieldName2Vc  LEFT OUTER JOIN tbl_NegotiationDocs ON tbl_Negotiation.negId = tbl_NegotiationDocs.negId where tbl_Negotiation.negId =  @v_fieldName2Vc
END

IF @v_fieldName1Vc = 'FindInEvalServiceForms'
BEGIN
	select cast(tenderId as varchar(10)) as FieldValue1 from tbl_EvalServiceForms where tenderid = @v_fieldName2Vc and configBy=@v_fieldName3Vc
END


IF @v_fieldName1Vc = 'FindapprovedCommiteeMember'
BEGIN
	select cast(count(*) as varchar(10)) as FieldValue1 from (select distinct tbl_EmployeeMaster.employeeName as FieldValue1, 
	  tbl_DesignationMaster.designationName as FieldValue2, 
	  tbl_CommitteeMembers.memberRole as FieldValue3, 
       REPLACE(CONVERT(VARCHAR(11), tbl_CommitteeMembers.createdDate, 106), ' ', '-') + ' ' + convert(varchar(5), tbl_CommitteeMembers.createdDate, 108) as FieldValue4, 
        tbl_CommitteeMembers.appStatus as FieldValue5,
        convert(varchar(50), tbl_Committee.committeeId)  as FieldValue6,
        convert(varchar(50), tbl_EmployeeMaster.userid) as FieldValue7,
        (select convert(varchar(50),COUNT(comMemberId)) from 
		tbl_Committee,tbl_CommitteeMembers,tbl_EmployeeMaster,tbl_EmployeeOffices,
		tbl_DesignationMaster
		where  tbl_Committee.committeeId=tbl_CommitteeMembers.committeeId  and tbl_CommitteeMembers.userId=tbl_EmployeeMaster.userid
		and tbl_EmployeeMaster.employeeId=tbl_EmployeeOffices.employeeId and tbl_EmployeeOffices.designationId=tbl_DesignationMaster.designationId 
		 and  committeeType in ('TEC','PEC')
		 and tenderId=@v_fieldName2Vc and appStatus='approved') as  FieldValue8 ,tbl_CommitteeMembers.memberRole as FieldValue9          
		from 
		tbl_Committee,tbl_CommitteeMembers,tbl_EmployeeMaster,tbl_EmployeeOffices,
		tbl_DesignationMaster
		where  tbl_Committee.committeeId=tbl_CommitteeMembers.committeeId  and tbl_CommitteeMembers.userId=tbl_EmployeeMaster.userid
		and tbl_EmployeeMaster.employeeId=tbl_EmployeeOffices.employeeId and tbl_EmployeeOffices.designationId=tbl_DesignationMaster.designationId 
		 and  committeeType in ('TEC','PEC')
		 and tenderId=@v_fieldName2Vc) a where a.FieldValue5='approved'
END


IF @v_fieldName1Vc = 'EvalTECChairPerson_Info'  
BEGIN
	select distinct convert(varchar(20),userid) as FieldValue1, 
	memberRole as FieldValue2,
	IsNull((Select Convert(varchar(50),listingId) from tbl_TOSListing where tenderId=@v_fieldName2Vc),'0') as FieldValue3,
	Case 
		When Exists (Select committeeId from tbl_Committee Where committeStatus='approved' and committeeType in ('TEC','PEC') and tenderId=@v_fieldName2Vc) 
		Then 'yes'
		Else 'no'
	End as FieldValue4,
	Case 
		When Exists (Select tenderId from tbl_EvalFormQues Where tenderId=@v_fieldName2Vc) 
		Then 'yes'
		Else 'no'
	End as FieldValue5,
	Case 
		When Exists (Select tenderId from tbl_EvalMemStatus Where tenderId=@v_fieldName2Vc) 
		Then 'yes'
		Else 'no'
	End as FieldValue6,
	Case 
		When Exists (Select tenderId from tbl_EvalConfig Where tenderId=@v_fieldName2Vc and isTscReq='yes') 
		Then 'yes'
		Else 'no'
	End as FieldValue7,
	Case 
		When Exists (Select tenderId from tbl_TosRptShare Where tenderId=@v_fieldName2Vc)
		Then 'yes'
		Else 'no'
	End as FieldValue8 			
	from tbl_committee c,tbl_CommitteeMembers cm
	 where tenderId=@v_fieldName2Vc and c.committeeId=cm.committeeId 
	 and committeeType in('TEC','PEC')  and memberRole='cp'
END

IF @v_fieldName1Vc = 'GetDebarmentComUser'
BEGIN
SELECT   distinct convert(varchar(400),dbo.tbl_LoginMaster.userId) AS FieldValue1,(dbo.tbl_EmployeeMaster.employeeName+'-'+dbo.tbl_DesignationMaster.designationName) AS FieldValue2 
                      ,(select dbo.getEmpRoles(dbo.tbl_LoginMaster.userid))  AS FieldValue3,'' AS FieldValue4,'' AS FieldValue5,'' AS FieldValue6
			FROM      dbo.tbl_DepartmentMaster INNER JOIN
                      dbo.tbl_DesignationMaster ON dbo.tbl_DepartmentMaster.departmentId = dbo.tbl_DesignationMaster.departmentid AND 
                      dbo.tbl_DepartmentMaster.departmentId = dbo.tbl_DesignationMaster.departmentid 
                      INNER JOIN
                      dbo.tbl_EmployeeOffices ON dbo.tbl_DesignationMaster.designationId = dbo.tbl_EmployeeOffices.designationId AND 
                      dbo.tbl_DesignationMaster.designationId = dbo.tbl_EmployeeOffices.designationId INNER JOIN
                      dbo.tbl_EmployeeMaster ON dbo.tbl_EmployeeMaster.employeeId = dbo.tbl_EmployeeOffices.employeeId AND 
                      dbo.tbl_EmployeeMaster.employeeId = dbo.tbl_EmployeeOffices.employeeId INNER JOIN
                      dbo.tbl_LoginMaster ON dbo.tbl_EmployeeMaster.userId = dbo.tbl_LoginMaster.userId AND 
                      dbo.tbl_EmployeeMaster.userId = dbo.tbl_LoginMaster.userId and  dbo.tbl_LoginMaster.status='Approved' WHERE
                         tbl_EmployeeOffices.officeid = ''+@v_fieldName2Vc+''
END	



IF @v_fieldName1Vc = 'getmarqueelisting'
BEGIN

	Declare @v_TeampQueryVc as varchar(max)
	Declare @v_PagePerRecordN int
	set @v_PagePerRecordN =10
	
	Declare @v_PageN int
	set @v_PageN=@v_fieldName2Vc
	
	
	Declare @v_TotalPages int
	set @v_TeampQueryVc=
	'DECLARE @v_Reccountf int
	DECLARE @v_TotalPagef int
	SELECT @v_Reccountf = Count(*) From (
	SELECT * From (SELECT ROW_NUMBER() OVER (order by marqueeId desc) As Rownumber
	FROM tbl_MarqueeMaster
	) AS DATA) AS TTT
	
	SET @v_TotalPagef =CEILING(@v_Reccountf/'+cast(@v_PagePerRecordN as varchar(10))+')
	SELECT *,cast(@v_TotalPagef as varchar(20)) as FieldValue5,cast(cast(@v_Reccountf as int) as varchar(20)) as FieldValue6 From (SELECT cast(ROW_NUMBER() OVER (order by marqueeId desc) as varchar(20)) As FieldValue1,cast(marqueeId as varchar(20)) as FieldValue2, marqueeText as FieldValue3, 
	REPLACE(CONVERT(VARCHAR(20), createdTime, 106), '' '', ''-'') +'' '' +Substring(CONVERT(VARCHAR(20),createdTime,108),1,5) as FieldValue4,
	REPLACE(CONVERT(VARCHAR(20), marqueeStartDt, 106), '' '', ''-'') +'' '' +Substring(CONVERT(VARCHAR(20),marqueeStartDt,108),1,5) as FieldValue7,
	REPLACE(CONVERT(VARCHAR(20), marqueeEndDt, 106), '' '', ''-'') +'' '' +Substring(CONVERT(VARCHAR(20),marqueeEndDt,108),1,5) as FieldValue8,
	CONVERT(varchar(50),userTypeId) as FieldValue9,
	CONVERT(varchar(50),locationId) as FieldValue10
	FROM tbl_MarqueeMaster
	) AS DATA where FieldValue1 between  ('+cast(@v_PageN as varchar(10))+' - 1) * '+cast(@v_PagePerRecordN as varchar(10))+' + 1 
	AND '+cast(@v_PageN as varchar(10))+' * '+cast(@v_PagePerRecordN as varchar(10))+''
	
	print @v_TeampQueryVc								
	exec(@v_TeampQueryVc)





	--select cast(marqueeId as varchar(20)) as FieldValue1, marqueeText as FieldValue2, 
	--REPLACE(CONVERT(VARCHAR(20), createdTime, 106), ' ', '-') +' ' +Substring(CONVERT(VARCHAR(20),createdTime,108),1,5) as FieldValue3,
	--cast((select COUNT(*) from tbl_MarqueeMaster) as varchar(20)) as FieldValue4 from 
	--tbl_MarqueeMaster order by createdTime desc
END


IF @v_fieldName1Vc = 'geTenderLotsForEvaluationMapping'
BEGIN		
	SELECT convert(varchar(20),lotNo) as FieldValue1, 
	lotDesc as FieldValue2, 
	CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
	Case When Exists (Select pkgLotId From tbl_EvalMapForms Where tenderId=@v_fieldName2Vc And pkgLotId=TLS.appPkgLotId) 
		 Then 'Mapped'
		 Else 'Pending'
		End as FieldValue4
	FROM  dbo.tbl_TenderLotSecurity TLS 
	WHERE tenderId = @v_fieldName2Vc  	
END


IF @v_fieldName1Vc = 'SavedDebarmentComUser' --by TaherT
BEGIN
SELECT   distinct convert(varchar(400),dbo.tbl_LoginMaster.userId) AS FieldValue1,(dbo.tbl_EmployeeMaster.employeeName) AS FieldValue2 
                      ,(select dbo.getEmpRoles(dbo.tbl_LoginMaster.userid))  AS FieldValue3,(select memberType from tbl_DebarmentCommittee tdc ,tbl_DebarmentComMembers tdcm where tdc.debarCommitteeId=tdcm.debarCommitteeId and tdcm.userId=dbo.tbl_LoginMaster.userId and  tdc.debarCommitteeId=''+@v_fieldName2Vc+'') AS FieldValue4,'' AS FieldValue5,'' AS FieldValue6
			FROM      dbo.tbl_DepartmentMaster INNER JOIN
                      dbo.tbl_DesignationMaster ON dbo.tbl_DepartmentMaster.departmentId = dbo.tbl_DesignationMaster.departmentid AND 
                      dbo.tbl_DepartmentMaster.departmentId = dbo.tbl_DesignationMaster.departmentid 
                      INNER JOIN
                      dbo.tbl_EmployeeOffices ON dbo.tbl_DesignationMaster.designationId = dbo.tbl_EmployeeOffices.designationId AND 
                      dbo.tbl_DesignationMaster.designationId = dbo.tbl_EmployeeOffices.designationId INNER JOIN
                      dbo.tbl_EmployeeMaster ON dbo.tbl_EmployeeMaster.employeeId = dbo.tbl_EmployeeOffices.employeeId AND 
                      dbo.tbl_EmployeeMaster.employeeId = dbo.tbl_EmployeeOffices.employeeId INNER JOIN
                      dbo.tbl_LoginMaster ON dbo.tbl_EmployeeMaster.userId = dbo.tbl_LoginMaster.userId AND 
                      dbo.tbl_EmployeeMaster.userId = dbo.tbl_LoginMaster.userId and  dbo.tbl_LoginMaster.status='Approved' WHERE
                         dbo.tbl_LoginMaster.userId in (select userId from tbl_DebarmentCommittee tdc ,tbl_DebarmentComMembers tdcm where tdc.debarCommitteeId=tdcm.debarCommitteeId and  tdc.debarCommitteeId=''+@v_fieldName2Vc+'')
END                        
                         
 IF @v_fieldName1Vc = 'GetConfigMessage'
BEGIN		
	SELECT convert(varchar(500),msgKey) as FieldValue1,convert(varchar(500),msgValue) as FieldValue2
	 
	FROM  dbo.tbl_MsgConfiguration   
	 	
END
                        
                      
IF @v_fieldName1Vc = 'SearchEmailForRegFee' -- for/partner/SearchTenderer.jsp
BEGIN
	/*
		FieldValue1: Bidder UserId
		FieldValue2: Bidder EmailId
		FieldValue3: Bidder CompanyId
		FieldValue4: Bidder Company Name
		FieldValue5: shows whether Email is verified or not
	*/
	
	Set @v_curUserId = (Select LM.userId from tbl_LoginMaster LM 				
		Where emailId=@v_fieldName2Vc)
		
	Set @v_Qry_Vc='	
	select CONVERT(varchar(50), LM.userId)  as FieldValue1, 
			emailId as FieldValue2, 
			''0'' as FieldValue3, 
			IsNull(dbo.f_getbiddercompany(LM.userId),'''') as FieldValue4,
			isEmailVerified as FieldValue5,
			IsNull((Select top 1 CONVERT(varchar(50), regPaymentId) from tbl_RegFeePayment RFP Where isLive=''yes'' And RFP.userId='+CONVERT(varchar(50),@v_curUserId)+' Order by regPaymentId desc),''0'') as FieldValue6,
			IsNull((Select top 1 dbo.f_initcap(status) from tbl_RegFeePayment RFP Where isLive=''yes'' And  RFP.userId='+CONVERT(varchar(50),@v_curUserId)+' Order by regPaymentId desc),''null'') as FieldValue7,
			REPLACE(CONVERT(VARCHAR(11),registeredDate, 106), '' '', ''-'')  +'' '' +Substring(CONVERT(VARCHAR(30),registeredDate,108),1,5) as FieldValue8,
			IsNull((Select top 1 dbo.f_initcap(isVerified) from tbl_RegFeePayment RFP Where isLive=''yes'' And  RFP.userId='+CONVERT(varchar(50),@v_curUserId)+' Order by regPaymentId desc),''null'') as FieldValue9,
			IsNull((Select top 1 CONVERT(varchar(50), sBankDevelopId) from tbl_RegFeePayment RFP Inner Join tbl_PartnerAdmin PA On RFP.createdBy=PA.userId Where isLive=''yes'' And  RFP.userId='+CONVERT(varchar(50),@v_curUserId)+' Order by regPaymentId desc),''0'') as FieldValue10
		from tbl_LoginMaster LM 		
		Where LM.userTyperId=2 And emailId=''' + @v_fieldName2Vc+ ''''
		
		print (@v_Qry_Vc)
		Exec (@v_Qry_Vc)
END   

IF @v_fieldName1Vc = 'getRegistrationFeePaymentDetail' 
BEGIN

--	Select CONVERT(varchar(50), amount) FieldValue1, paymentInstType as FieldValue2,
--	instRefNumber as FieldValue3, 
--	REPLACE(CONVERT(VARCHAR(11),dtOfPayment, 106), ' ', '-') as FieldValue4, 
	
--	bankName as FieldValue5, branchName as FieldValue6, 
--	PA.fullName as FieldValue7,
--	Comments as FieldValue8,
--	REPLACE(CONVERT(VARCHAR(11),instValidityDt, 106), ' ', '-') as FieldValue9
--from tbl_RegFeePayment RP Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
--Where regPaymentId=@v_fieldName2Vc

	/*
	FieldValue1: Branch Name
	FieldValue2: Branch Manager (Name of Person who has done payment)
	FieldValue3: Curreny
	FieldValue4: Amount
	FieldValue5: Payment Instrument Type
	FieldValue6: Date Of Payment
	FieldValue7: Comments
	
	*/
	
	Select branchName as FieldValue1,
		dbo.f_GovUserName(RP.partTransId, 'tbl_PartnerAdminTransfer') as FieldValue2,
		currency as FieldValue3,
		Convert(varchar(50), amount) as FieldValue4,
		paymentInstType as FieldValue5,
		REPLACE(CONVERT(VARCHAR(11),dtOfPayment, 106), ' ', '-') + ' ' + Substring(CONVERT(VARCHAR(5),dtOfPayment,108),1,8)  as FieldValue6,		 
		comments as FieldValue7,
		isVerified as FieldValue8,
		Convert(varchar(50), validityPeriodRef) as FieldValue9,
		RP.status as FieldValue10		
	from tbl_RegFeePayment RP Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
Where regPaymentId=@v_fieldName2Vc And isLive='yes'


END

IF @v_fieldName1Vc = 'getRegistrationFeePaymentDetailMore' 
BEGIN
	
	/*
	FieldValue1: Instrument/Account No.
	FieldValue2: Issance Bank Name
	FieldValue3: Issance Bank Branch
	FieldValue4: Issuance Date
	FieldValue5: Valid Date
	*/
	
	Select 
	instRefNumber as FieldValue1,
	issuanceBank as FieldValue2,
	issuanceBranch as FieldValue3,	 
	REPLACE(CONVERT(VARCHAR(11),issuanceDate, 106), ' ', '-') as FieldValue4,	 
	REPLACE(CONVERT(VARCHAR(11),instValidityDt, 106), ' ', '-')	as FieldValue5
	from tbl_RegFeePayment RP Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
	Where regPaymentId=@v_fieldName2Vc And isLive='yes'

END



IF @v_fieldName1Vc = 'getmarqueelist' -- for MarqueeListing.jsp
begin
  SELECT convert(varchar(20),tm.marqueeId) as FieldValue1, tm.marqueeText as FieldValue2, (select tl.locationName from tbl_locationmaster tl where tl.locationId = tm.locationId)
  as FieldValue3   
  FROM tbl_MarqueeMaster tm order by tm.marqueeId desc
end

IF @v_fieldName1Vc = 'GetCompNameByUserid' 
BEGIN
		
		SELECT l.emailId as FieldValue1,CASE CompanyName WHEN '-' THEN firstName+' '+lastName ELSE companyName END AS FieldValue2 
		FROM dbo.tbl_loginmaster l,dbo.tbl_TendererMaster t, dbo.tbl_CompanyMaster c
		WHERE l.userId=t.userId AND t.companyId=c.companyId AND l.userid = @v_fieldName2Vc
	
END



IF @v_fieldName1Vc = 'getformnamebytenderidandlotidForOpening'     
BEGIN
	
	
	Select @v_EnvelopeCnt_Int= tcm.noOfEnvelops
	from tbl_TenderDetails ttd,tbl_TenderTypes ttt,tbl_ConfigEvalMethod tcm
	 where ttd.eventType =ttt.tenderType
	 and ttd.procurementMethodId =tcm.procurementMethodId 
	 and ttt.tenderTypeId=tcm.tenderTypeId  
	 and ttd.procurementNatureId =tcm.procurementNatureId 
	 and ttd.tenderId=@v_fieldName2Vc
 

		
	IF @v_fieldName3Vc <> '0'
	BEGIN
		If @v_EnvelopeCnt_Int>1
		Begin
			-- IF TENDER HAS MORE THAN 1 ENVELOPES THEN DISPLAY ONLY TECHNICAL FORMS
			SELECT distinct formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
			Case When Exists (select tbpd.tenderFormId from tbl_TenderTables tbpd inner join tbl_TenderBidTable tbi ON tbpd.tenderTableId=tbi.tenderTableId Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
			ISNULL('', '') as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9,
			(select Case When (openingDt is null Or openingDt='') then 'N.A.' When openingDt > getdate() Then 'no' Else 'yes' End from tbl_TenderDetails Where tenderId=@v_fieldName2Vc) as  FieldValue10
			FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
			ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
			dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 		
			Where td.tenderId = @v_fieldName2Vc and packageLotId = @v_fieldName3Vc
			And isPriceBid='No'
			Order By isPriceBid
		End
		Else
		Begin
			SELECT distinct formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
			Case When Exists (select tbpd.tenderFormId from tbl_TenderTables tbpd inner join tbl_TenderBidTable tbi ON tbpd.tenderTableId=tbi.tenderTableId Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
			ISNULL('', '') as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9,
			(select Case When (openingDt is null Or openingDt='') then 'N.A.' When openingDt > getdate() Then 'no' Else 'yes' End from tbl_TenderDetails Where tenderId=@v_fieldName2Vc) as  FieldValue10
			FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
			ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
			dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 		
			Where td.tenderId = @v_fieldName2Vc and packageLotId = @v_fieldName3Vc
			Order By isPriceBid		
		End
		
	
		--SELECT distinct formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
		--Case When Exists (select tbpd.tenderFormId from tbl_TenderTables tbpd inner join tbl_TenderBidTable tbi ON tbpd.tenderTableId=tbi.tenderTableId Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
		--ISNULL('', '') as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9,
		--(select Case When (openingDt is null Or openingDt='') then 'N.A.' When openingDt > getdate() Then 'no' Else 'yes' End from tbl_TenderDetails Where tenderId=@v_fieldName2Vc) as  FieldValue10
		--FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		--ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		--dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 		
		--Where td.tenderId = @v_fieldName2Vc and packageLotId = @v_fieldName3Vc
		--Order By isPriceBid
	END
	ELSE
	BEGIN
	
		If @v_EnvelopeCnt_Int>1
		Begin
			-- IF TENDER HAS MORE THAN 1 ENVELOPES THEN DISPLAY ONLY TECHNICAL FORMS
			SELECT distinct formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
			Case When Exists (select tbpd.tenderFormId from tbl_TenderTables tbpd inner join tbl_TenderBidTable tbi ON tbpd.tenderTableId=tbi.tenderTableId Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
			ISNULL('', '') as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9,
			(select Case When (openingDt is null Or openingDt='') then 'N.A.' When openingDt > getdate() Then 'no' Else 'yes' End from tbl_TenderDetails Where tenderId=@v_fieldName2Vc) as  FieldValue10
			FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
			ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
			dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 		
			Where td.tenderId = @v_fieldName2Vc
			And isPriceBid='No'
			Order By isPriceBid
		End
		Else
		Begin
			SELECT distinct formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
			Case When Exists (select tbpd.tenderFormId from tbl_TenderTables tbpd inner join tbl_TenderBidTable tbi ON tbpd.tenderTableId=tbi.tenderTableId Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
			ISNULL('', '') as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9,
			(select Case When (openingDt is null Or openingDt='') then 'N.A.' When openingDt > getdate() Then 'no' Else 'yes' End from tbl_TenderDetails Where tenderId=@v_fieldName2Vc) as  FieldValue10
			FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
			ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
			dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 		
			Where td.tenderId = @v_fieldName2Vc
			Order By isPriceBid
		End
		 
	
		--SELECT distinct formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
		--Case When Exists (select tbpd.tenderFormId from tbl_TenderTables tbpd inner join tbl_TenderBidTable tbi ON tbpd.tenderTableId=tbi.tenderTableId Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
		--ISNULL('', '') as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9,
		--(select Case When (openingDt is null Or openingDt='') then 'N.A.' When openingDt > getdate() Then 'no' Else 'yes' End from tbl_TenderDetails Where tenderId=@v_fieldName2Vc) as  FieldValue10
		--FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
		--ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
		--dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 		
		--Where td.tenderId = @v_fieldName2Vc
		--Order By isPriceBid
	
	END
		
END

IF @v_fieldName1Vc = 'getBankUserRole' 
BEGIN
	Select isMakerChecker as FieldValue1, 
	Convert(varchar(50), sBankDevelopId) as FieldValue2 
	From tbl_PartnerAdmin where userId=@v_fieldName2Vc
END


IF @v_fieldName1Vc = 'getRegistrationFeePaymentDetailForEdit' 
BEGIN
	/*
	FieldValue1: Currency
	FieldValue2: Amount
	FieldValue3: Payment Instrument Type
	FieldValue4: Date Of Payment : Format to be displayed
	FieldValue5: Comments	
	FieldValue6: Date Of Payment : Format Changed
	FieldValue7: Verification Status	
	*/
	
	Select	currency as FieldValue1,
		Convert(varchar(50), amount) as FieldValue2,
		paymentInstType as FieldValue3,		
		REPLACE(CONVERT(VARCHAR(11),dtOfPayment, 106), ' ', '-') + ' ' + Substring(CONVERT(VARCHAR(5),dtOfPayment,108),1,8) as FieldValue4,		 
		comments as FieldValue5,
		RePlace(CONVERT(VARCHAR(50),dtOfPayment, 102),'.','-') + ' ' + Substring(CONVERT(VARCHAR(30),dtOfPayment,108),1,8) as FieldValue6,
		isVerified as FieldValue7,
		status as FieldValue8,
		Convert(varchar(50), validityPeriodRef) as FieldValue9		
	from tbl_RegFeePayment RP 
	Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId	
	Where regPaymentId=@v_fieldName2Vc And isLive='yes'
	
	--Select	currency as FieldValue1,
	--	Convert(varchar(50), amount) as FieldValue2,
	--	paymentInstType as FieldValue3,		
	--	REPLACE(CONVERT(VARCHAR(11),dtOfPayment, 106), ' ', '-') as FieldValue4,		 
	--	comments as FieldValue5,
	--	CONVERT(VARCHAR(11),dtOfPayment, 103) as FieldValue6,
	--	isVerified as FieldValue7,
	--	status as FieldValue8		
	--from tbl_RegFeePayment RP 
	--Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId	
	--Where regPaymentId=@v_fieldName2Vc And isLive='yes'
END


IF @v_fieldName1Vc = 'getRegistrationFeePaymentDetailMoreForEdit' 
BEGIN
	
	/*
	FieldValue1: Instrument/Account No.
	FieldValue2: Issance Bank Name
	FieldValue3: Issance Bank Branch
	FieldValue4: Issuance Date
	FieldValue5: Valid Date
	*/
	
	Select 
	instRefNumber as FieldValue1,
	issuanceBank as FieldValue2,
	issuanceBranch as FieldValue3,	 
	CONVERT(VARCHAR(11),issuanceDate, 103) as FieldValue4,	 
	CONVERT(VARCHAR(11),instValidityDt, 103)	as FieldValue5
	
	from tbl_RegFeePayment RP Inner Join tbl_PartnerAdmin PA On RP.createdBy=PA.userId
	Where regPaymentId=@v_fieldName2Vc And isLive='yes'

END


IF @v_fieldName1Vc = 'getBankBranchOrMemberList' 
BEGIN
	

	If @v_fieldName3Vc='getBranchList'
	Begin
		Declare @v_BankUserRole_Vc varchar(50), @v_BankBranchId int, @v_BankId int
	
		Select @v_BankUserRole_Vc=isMakerChecker,
			   @v_BankBranchId = sBankDevelopId		
		From tbl_PartnerAdmin Where userId=@v_fieldName2Vc
		
		Select @v_BankId = Case sBankDevelHeadId 
		When 0 Then @v_BankBranchId Else sBankDevelHeadId End 
		From tbl_ScBankDevPartnerMaster Where sBankDevelopId=@v_BankBranchId
	
		--Select Convert(varchar(50), sBankDevelopId) as FieldValue1, sbDevelopName as FieldValue2 
		--From tbl_ScBankDevPartnerMaster
		--Where (sBankDevelHeadId = @v_BankId) Or (sBankDevelopId = @v_BankId)
		--Order by sbDevelopName
		Select Convert(varchar(50), sBankDevelopId) as FieldValue1, sbDevelopName as FieldValue2 
		From tbl_ScBankDevPartnerMaster
		Where (sBankDevelHeadId = @v_BankId) 
		--Or (sBankDevelopId = @v_BankId)
		Order by sbDevelopName
	End
	If @v_fieldName3Vc='getBranchMemberList'
	Begin
		--Select Convert(varchar(50), userId) as FieldValue1, fullName as FieldValue2 
		--From tbl_PartnerAdmin Where sBankDevelopId=@v_fieldName2Vc And isMakerChecker='BranchMaker'
		--Order by fullName
		
		Select Convert(varchar(50), partTransId) as FieldValue1, fullName as FieldValue2 
		From tbl_PartnerAdminTransfer Where sBankDevelopId=@v_fieldName2Vc And isMakerChecker='BranchMaker'
		Order by fullName
		
	End
	
	
END


IF @v_fieldName1Vc = 'SearchEmailForContentAdmin' 
BEGIN
	/*
		FieldValue1: Payment Id		
	*/
		
	Set @v_curUserId = (Select LM.userId from tbl_LoginMaster LM 				
		Where emailId=@v_fieldName2Vc)
		
	Set @v_Qry_Vc='	
	Select top 1 CONVERT(varchar(50), RFP.regPaymentId) as FieldValue1
	from tbl_LoginMaster LM 
	inner Join tbl_RegFeePayment RFP On LM.userId=RFP.userId
	inner join tbl_PaymentVerification PV On RFP.regPaymentId=PV.PaymentId
	Where RFP.isLive=''yes'' 
	And RFP.isVerified=''yes'' 	
	And emailId=''' + @v_fieldName2Vc+ ''' Order by RFP.regPaymentId desc' 	
		
	print (@v_Qry_Vc)
		Exec (@v_Qry_Vc)
END  


IF @v_fieldName1Vc = 'CheckValueInevalconfig' 
BEGIN
	select cast(tenderId as varchar(20)) as FieldValue1 from tbl_evalconfig where tenderId=@v_fieldName2Vc
END

IF @v_fieldName1Vc = 'GetTendererHash' 
BEGIN
	select Cast(tm.userId as Varchar(20)) as FieldValue1, case when 
tm.companyId!=1 then companyname else  title+' '+tm.firstName+' '+tm.lastName end as FieldValue2, tenderHash as FieldValue3 
	from dbo.tbl_FinalSubmission Inner join dbo.tbl_CompanyMaster 
	Inner join dbo.tbl_tenderermaster tm
	On tm.companyId = dbo.tbl_CompanyMaster.companyId  
	on tm.userId=dbo.tbl_FinalSubmission.userId 
	where tenderId=@v_fieldName2Vc and bidSubStatus='finalsubmission'
	order by FieldValue2
END


If @v_fieldName1Vc='getTenderInfoForOpeningTab'
Begin
Select 
	Case When Exists (Select tenderId from tbl_TOSListing where tenderId=@v_fieldName2Vc) 
	Then 'yes' Else 'no' End as FieldValue1,
	IsNull((Select Convert(varchar(50),sentTo) from tbl_TOSListing where tenderId=@v_fieldName2Vc),'0') as FieldValue2,
	Case When Exists (Select tenderId from tbl_TOSRptShare where tenderId=@v_fieldName2Vc And listingId=(Select listingId from tbl_TOSListing where tenderId=@v_fieldName2Vc)
		) Then 'yes' Else 'no' End as FieldValue3,
	IsNull(( Select Convert(varchar(50),userId)  from tbl_CommitteeMembers CM 
			 Inner Join tbl_Committee C On CM.committeeId=C.committeeId Where C.tenderId=@v_fieldName2Vc And committeeType in ('TEC', 'PEC') And committeStatus='approved' and memberRole='cp'
	),'0') as FieldValue4		
	
End

IF @v_fieldName1Vc='getSentToUserName'
Begin
	If @v_fieldName3Vc='getPEName'
	Begin
		  select IsNull(Convert(varchar(50),e.employeeName),'null') as FieldValue1
			from tbl_tendermaster  t,tbl_AppMaster a,tbl_employeemaster e 
			where t.appId=a.appId and tenderid=@v_fieldName2Vc and a.employeeid=e.employeeId;
	End
	Else If @v_fieldName3Vc='getTECChairPersonName'
	Begin
		--Select IsNull(Convert(varchar(50),EM.employeeName),'null') as FieldValue1
		--from tbl_CommitteeMembers CM 
		--	Inner Join tbl_Committee C On CM.committeeId=C.committeeId 
		--	Inner Join tbl_employeemaster EM On EM.userId=CM.userId
		--	Where C.tenderId=@v_fieldName2Vc And committeeType in ('TEC', 'PEC') 
		--	And committeStatus='approved' and memberRole='cp'
		
		Select dbo.f_GovUserName(CM.govUserId, 'tbl_EmployeeTrasfer') as FieldValue1
		from tbl_CommitteeMembers CM 
			Inner Join tbl_Committee C On CM.committeeId=C.committeeId 			
			Where C.tenderId=@v_fieldName2Vc 
				And committeeType in ('TEC', 'PEC') 
				And committeStatus='approved' 
				and memberRole='cp'
		
	End
End


IF @v_fieldName1Vc='deleteReports'
Begin
	BEGIN TRY
			BEGIN TRAN
			
			Delete From Tbl_BidRankDetail Where bidderRankId in (select bidderRankId from Tbl_BidderRank where reportId=@v_fieldName2Vc)
			Delete From Tbl_BidderRank Where reportId=@v_fieldName2Vc
			
			Delete From Tbl_ReportForms Where reportId=@v_fieldName2Vc
			
			Delete From Tbl_ReportColumnMaster Where reportTableId in (select reportTableId from Tbl_ReportTableMaster where reportId=@v_fieldName2Vc)
			Delete From Tbl_ReportFormulaMaster Where reportTableId in (select reportTableId from Tbl_ReportTableMaster where reportId=@v_fieldName2Vc)
			Delete From Tbl_ReportTableMaster Where reportId=@v_fieldName2Vc
			Delete From Tbl_ReportMaster Where reportId=@v_fieldName2Vc
			
			Select  'true' as FieldValue1, 'Records deleted successfully' as FieldValue2
			COMMIT TRAN	
		END TRY 
	BEGIN CATCH
		BEGIN
			
			Select  'false' as FieldValue1, ERROR_MESSAGE() as FieldValue2				
			ROLLBACK TRAN
		END
	END CATCH	

End

IF @v_fieldName1Vc = 'Checktenderpublishstatus' 
BEGIN
	select tenderstatus as FieldValue1 from tbl_TenderDetails where tenderId=@v_fieldName2Vc
END
IF @v_fieldName1Vc = 'EvalTendereractivate' 
BEGIN
	Select evalBidderRspId from tbl_EvalBidderResp where tenderId = @v_fieldName2Vc
END

IF @v_fieldName1Vc = 'evalNotifycnt' 
BEGIN
	Select convert(varchar(20),COUNT(evalClrToCpId)) as FieldValue1 from tbl_EvalSentQueToCp where sentFor = 'evaluation' and tenderId = @v_fieldName2Vc and sentBy = @v_fieldName3Vc
END

IF @v_fieldName1Vc = 'getSentToAAEntry' 
BEGIN
	Select Case When Exists (Select top 1 evalRptToAAId from tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc)
	Then 'yes' Else 'no' End as FieldValue1,
	Case When Exists (Select top 1 evalRptAppId from tbl_EvalRptApprove where tenderId=@v_fieldName2Vc)
	Then 'yes' Else 'no' End as FieldValue2,
	ISNULL((Select top 1 Convert(varchar(50), evalRptToAAId) from tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc),'0') as FieldValue3		
END


IF @v_fieldName1Vc='getTECReportsList'
BEGIN
	select distinct Convert(varchar(50), TD.tenderId) as FieldValue1,
	tenderBrief as FieldValue2,
	agency as FieldValue3,
	peOfficeName as FieldValue4,
	Convert(varchar(50), ER.evalRptToAAId) as FieldValue5,
	ER.evalRptToAAId	
	from tbl_tenderdetails TD 
	inner join tbl_EvalRptSentToAA ER ON TD.tenderId=ER.tenderId
	where ER.userId=@v_fieldName2Vc
	Order by ER.evalRptToAAId Desc
END

IF @v_fieldName1Vc = 'getTenderAA'
BEGIN
 
	SELECT  convert(varchar(50),estCost) as FieldValue1 , (Select employeeName from 
	tbl_EmployeeMaster where userid = approvingAuthId) as FieldValue2, 
	employeeName +','+designationName  as FieldValue3,  convert(varchar(50),e.userId) as FieldValue4,
	 convert(varchar(50),tenderSecurityDays) as FieldValue5, 
	 (select templateName from tbl_TemplateMaster where templateId =stdTemplateId) as FieldValue6,
	  Convert(varchar(50), approvingAuthId) as FieldValue7, '' as FieldValue8, '' as FieldValue9 , 
	  '' as FieldValue10 from tbl_tenderdetails t,tbl_employeemaster e ,tbl_DesignationMaster dm
	   WHERE t.approvingAuthId=e.userId and e.designationId=dm.designationId and  tenderId = convert(int,@v_fieldName2Vc)
END


IF @v_fieldName1Vc = 'getTenderDocAvlMethod'
BEGIN
Select convert(varchar(20),tenderId) as FieldValue1, IsNull(docAvlMethod,'') as FieldValue2, procurementType as FieldValue3 from [tbl_TenderDetails] where tenderId = @v_fieldName2Vc

END

IF @v_fieldName1Vc = 'getRegistrationValidityInfo'
BEGIN
Select convert(varchar(20),validityId) as FieldValue1 , displayText as FieldValue2 from tbl_RegistrationValidity
Where Status=1 
Order by validityMonths 
END


IF @v_fieldName1Vc = 'getValidityMonthsFromId'
BEGIN
Select convert(varchar(20),validityMonths) as FieldValue1, 
convert(varchar(20),validityYear) as FieldValue2, 
validityYrWords as FieldValue3
from tbl_RegistrationValidity
Where Status=1 And validityId=@v_fieldName2Vc
END


IF @v_fieldName1Vc = 'getTenderTECInfo'
BEGIN
	--Select
	--IsNull((Select CONVERT(varchar(50), Count(comMemberID)) from tbl_CommitteeMembers
	--where committeeId in
	--(select committeeId from tbl_Committee where tenderId=@v_fieldName2Vc and committeeType in ('TEC', 'PEC') and committeStatus='approved')
	--),'0') as FieldValue1,
	
	--IsNull((Select CONVERT(varchar(50), Count(comMemberID)) from tbl_CommitteeMembers
	--where committeeId in
	--(select committeeId from tbl_Committee where tenderId=@v_fieldName2Vc and committeeType in ('TEC', 'PEC') and committeStatus='approved')
	--and appStatus='approved'
	--),'0') as FieldValue2
	
	Select
	IsNull((Select CONVERT(varchar(50), Count(comMemberID)) from tbl_CommitteeMembers
	where memberRole<>'cp' And committeeId in
	(select committeeId from tbl_Committee where tenderId=@v_fieldName2Vc 
	and committeeType in ('TEC', 'PEC') and committeStatus='approved') 
	),'0') as FieldValue1,
	
	IsNull((select 
CONVERT(varchar(50), Count(nomineeId))
from tbl_EvalNomination EN 
Inner Join tbl_EvalConfig EC On EN.evalConfigId=EC.evalConfigId
Inner Join tbl_CommitteeMembers CM On EN.nomineeUserId=CM.userId
Inner Join tbl_Committee C On CM.committeeId=C.committeeId
where C.committeeType in ('TEC','PEC') And C.committeStatus='Approved'
And C.tenderId=@v_fieldName2Vc And EN.isCurrent='Yes'
And EN.nomineeStatus='Live' and EN.nomineeAction='Agreed'
And EN.tenderId=C.tenderId),'0') as FieldValue2

END


IF @v_fieldName1Vc='getEmployeeNamefromUserId'
BEGIN
select employeeName as FieldValue1 , designationName as FieldValue2 from tbl_EmployeeMaster  em, tbl_DesignationMaster dm where em.designationId=dm.designationId and em.userId in  
(
Select tecMemberId from tbl_evalconfig where tenderId=@v_fieldName2Vc and configType='team'
 )
END

IF @v_fieldName1Vc='getEvalConfigInfo'
BEGIN
select convert(varchar(30),evalConfigId) as FieldValue1 from tbl_EvalConfig where tenderId=@v_fieldName2Vc
END

IF @v_fieldName1Vc='getNomineeStatusforEval'
BEGIN
select top 1 nomineeAction as FieldValue1 ,nomineeStatus as FieldValue2 
from tbl_EvalNomination where tenderId=@v_fieldName2Vc and nomineeUserId = @v_fieldName3Vc
Order by nomineeId desc
END

IF @v_fieldName1Vc='getRenewed_ExpiryDt'
BEGIN
	Select 
	REPLACE(CONVERT(VARCHAR(11),dtOfPayment, 106), ' ', '-') as FieldValue1, 
	REPLACE(CONVERT(VARCHAR(11),LM.validUpTo, 106), ' ', '-') as FieldValue2
	from tbl_RegFeePayment RP 
	Inner join tbl_LoginMaster LM On RP.userId=LM.userId
	where regPaymentId=@v_fieldName2Vc
END


IF @v_fieldName1Vc='getTSCRequestStatus'
BEGIN
	/*	
	FieldValue1: Sent To AA for approval or Not
	FieldValue2: Sent To AA has approved or Not
	*/

	IF Exists (Select tenderId from tbl_TSCStatus where tenderId=@v_fieldName2Vc)
	Begin
		Select 'yes' as FieldValue1,		
		TS.status as FieldValue2		 
		from tbl_TSCStatus TS Where tenderId=@v_fieldName2Vc
	End	
END

IF @v_fieldName1Vc='getTenderEvalConfigStatus'
BEGIN
	/*	
	FieldValue1: 
	FieldValue2: 
	*/
	If Exists (select tenderId from tbl_EvalConfig where tenderId=@v_fieldName2Vc) 
	Begin
		select 'yes' as FieldValue1, CONVERT(varchar(50), evalConfigId) as FieldValue2, 
		isTscReq as FieldValue3,
		evalStatus as FieldValue4,
		configType as FieldValue5,
		CONVERT(varchar(50), tecMemberId) as FieldValue6,
		Case configType When 'ind' Then 'Individual' When 'team' Then 'Team' End as FieldValue7,
		Case tecMemberId When 0 Then 'null'
						 Else dbo.f_GovUserName(tecGovUserId, 'tbl_EmployeeTrasfer')
						 End as FieldValue8
		
		from tbl_EvalConfig EC where tenderId=@v_fieldName2Vc	
	End	

END

IF @v_fieldName1Vc='getEvalConfigType'
BEGIN	
	select configType as FieldValue1, isTscReq as  FieldValue2 from tbl_EvalConfig where tenderId=@v_fieldName2Vc	
END


IF @v_fieldName1Vc='GetEvalRptApproval'
BEGIN
select rptStatus as FieldValue1  from tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and rptStatus='Approved'
End


IF @v_fieldName1Vc='getTenderTSCStatus'
BEGIN
Select Convert(varchar(50),tscStatusId) as FieldValue1 from tbl_TSCStatus Where tenderId=@v_fieldName2Vc And status='Approve' and isTECCP='yes'
End

IF @v_fieldName1Vc='getDebarmentListWithPE'
BEGIN
select tdr.clarification as FieldValue1,REPLACE(CONVERT(VARCHAR(11),tdr.lastResponseDt, 106), ' ', '-')     as FieldValue2 ,tcm.companyName as FieldValue4,tm.firstName as FieldValue5,tm.lastName as FieldValue6 ,tdr.comments as FieldValue9,convert(varchar(20),tdr.debarTypeId) as FieldValue10,
	(select employeename from tbl_EmployeeRoles er,tbl_EmployeeOffices eo,tbl_EmployeeMaster em
	where er.employeeid=eo.employeeId and procurementRoleId=6 and er.employeeId=em.employeeid and officeId in(select officeid from tbl_officemaster where departmentid in (select departmentId from tbl_EmployeeOffices eo,tbl_OfficeMaster om,tbl_EmployeeMaster em
	where eo.officeid=om.officeid and em.employeeId=eo.employeeid
	and userid=tdr.debarmentBy) 
	)) as FieldValue7,(select convert(varchar(20),em.userId)  from tbl_EmployeeRoles er,tbl_EmployeeOffices eo,tbl_EmployeeMaster em
	where er.employeeid=eo.employeeId and procurementRoleId=6 and er.employeeId=em.employeeid and officeId in(select officeid from tbl_officemaster where departmentid in (select departmentId from tbl_EmployeeOffices eo,tbl_OfficeMaster om,tbl_EmployeeMaster em
	where eo.officeid=om.officeid and em.employeeId=eo.employeeid
	and userid=tdr.debarmentBy)
	))as FieldValue8
	from tbl_DebarmentReq tdr,tbl_tenderermaster tm, tbl_CompanyMaster tcm 
	where tdr.userid=tm.userid and tm.companyid=tcm.companyid 
	 
 and tdr.debarmentStatus=''+@v_fieldName3Vc+'' and tdr.debarmentId=@v_fieldName2Vc
 END
 
 
IF @v_fieldName1Vc='getCommitteeMemInfo'
BEGIN
select CONVERT(varchar(50), committeeId) as FieldValue1,
CONVERT(varchar(50), userId) as FieldValue2, 
RePlace(CONVERT(VARCHAR(50),createdDate, 102),'.','-') + ' ' + Substring(CONVERT(VARCHAR(30),createdDate,108),1,8) as FieldValue3, 
appStatus as FieldValue4, 
RePlace(CONVERT(VARCHAR(50),appDate, 102),'.','-') + ' ' + Substring(CONVERT(VARCHAR(30),appDate,108),1,8) as FieldValue5, 
remarks as FieldValue6, 
eSignature as FieldValue7, 
digitalSignature as FieldValue8, 
memberRole as FieldValue9, 
memberFrom as FieldValue10
from tbl_CommitteeMembers Where committeeId=@v_fieldName2Vc and userId=@v_fieldName3Vc
 END
 
IF @v_fieldName1Vc='getTSCComments'
BEGIN
--select CONVERT(varchar(50), commentsId) as FieldValue1 , 
--CONVERT(varchar(50), tscUserId) as FieldValue2 , 
--EM.employeeName as FieldValue3 ,
--comments as FieldValue4 ,
--REPLACE(CONVERT(VARCHAR(11),commentsDt, 106), ' ', '-') as FieldValue5  
--from tbl_EvalTsccomments EC
--Inner join tbl_EmployeeMaster EM ON EC.tscUserId=EM.userId
--where formId=@v_fieldName2Vc
--Order by commentsId desc

select CONVERT(varchar(50), commentsId) as FieldValue1 , 
CONVERT(varchar(50), tscUserId) as FieldValue2 , 
dbo.f_gettscmembername(tscUserId,tscGovUserId) as FieldValue3 ,
comments as FieldValue4 ,
REPLACE(CONVERT(VARCHAR(11),commentsDt, 106), ' ', '-') as FieldValue5  
from tbl_EvalTsccomments EC
where 
tenderId in (select tenderId from tbl_EvalTSCNotification where tenderId= EC.tenderId)
and formId=@v_fieldName2Vc

Order by commentsId desc

 END
 
 
 
 IF @v_fieldName1Vc='getTECNomineeStatus'
BEGIN
	select top 1 nomineeAction as FieldValue1
	from tbl_EvalNomination 
	where tenderId=@v_fieldName2Vc And nomineeUserId=@v_fieldName3Vc
	And nomineeStatus='Live'
	Order by nomineeId desc
 END
 
 
 IF @v_fieldName1Vc = 'SearchEmailForTenPayment' -- for/partner/SearchTenderer.jsp
BEGIN
	/*
		FieldValue1: Bidder UserId
		FieldValue2: Bidder EmailId
		FieldValue3: Bidder CompanyId
		FieldValue4: Bidder Company Name
		FieldValue5: shows whether Email is verified or not
	*/
	
	Set @v_curUserId = (Select LM.userId from tbl_LoginMaster LM 				
		Where emailId=@v_fieldName2Vc)
		
	Set @v_Qry_Vc='	
	select CONVERT(varchar(50), LM.userId)  as FieldValue1, 
			emailId as FieldValue2, 
			''0'' as FieldValue3, 
			IsNull(dbo.f_getbiddercompany(LM.userId),'''') as FieldValue4,
			isEmailVerified as FieldValue5,
			IsNull((Select top 1 CONVERT(varchar(50), TP.tenderPaymentId) from tbl_TenderPayment TP Where TP.isLive=''yes'' And TP.userId='+CONVERT(varchar(50),@v_curUserId)+' And  '+@v_fieldName3Vc+' Order by TP.tenderPaymentId desc),''0'') as FieldValue6,
			IsNull((Select top 1 dbo.f_initcap(status) from tbl_TenderPayment TP Where TP.isLive=''yes'' And  TP.userId='+CONVERT(varchar(50),@v_curUserId)+' And ' +@v_fieldName3Vc+ ' Order by TP.tenderPaymentId desc),''null'') as FieldValue7,
			REPLACE(CONVERT(VARCHAR(11),registeredDate, 106), '' '', ''-'')  +'' '' +Substring(CONVERT(VARCHAR(30),registeredDate,108),1,5) as FieldValue8,
			IsNull((Select top 1 dbo.f_initcap(TP.isVerified) from tbl_TenderPayment TP Where isLive=''yes'' And  TP.userId='+CONVERT(varchar(50),@v_curUserId)+' And ' +@v_fieldName3Vc+ ' Order by TP.tenderPaymentId desc),''null'') as FieldValue9,
			IsNull((Select top 1 CONVERT(varchar(50), sBankDevelopId) from tbl_TenderPayment TP Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId Where isLive=''yes'' And  TP.userId='+CONVERT(varchar(50),@v_curUserId)+' And '+@v_fieldName3Vc+' Order by TP.tenderPaymentId desc),''0'') as FieldValue10
		from tbl_LoginMaster LM 		
		Where LM.userTyperId=2 And LM.registrationType<>''media'' And emailId=''' + @v_fieldName2Vc+ ''''
		
		print (@v_Qry_Vc)
		Exec (@v_Qry_Vc)
END   


IF @v_fieldName1Vc = 'getTenderPaymentAmount' 
BEGIN	
	
	If @v_fieldName2Vc='Document Fees'
	Begin
		print ('df')
		Select @v_DocFeesMethod_Vc=docFeesMethod from tbl_TenderDetails where tenderId=(select top 1  dbo.f_trim(Replace(items, 'A.tenderId=','')) from dbo.f_splitX(@v_fieldName3Vc, ' '))
		
		If @v_DocFeesMethod_Vc='Package wise'
		Begin
			Set @v_Qry_Vc='Select IsNull(Convert(varchar(50), A.pkgDocFees),''0.00'') as FieldValue1 
			From tbl_TenderDetails A Where ' + @v_fieldName3Vc
		End
		Else If @v_DocFeesMethod_Vc='Lot wise'
		Begin
			Set @v_Qry_Vc='Select IsNull(Convert(varchar(50), A.docFess ),''0.00'') as FieldValue1 
			From tbl_TenderLotSecurity A Where ' + @v_fieldName3Vc
		End
	End
	Else If @v_fieldName2Vc='Tender Security'
	Begin
		print ('ts')
		Set @v_Qry_Vc='Select IsNull(Convert(varchar(50), A.tenderSecurityAmt ),''0.00'') as FieldValue1 
			From tbl_TenderLotSecurity A Where ' + @v_fieldName3Vc
	End
	Else If @v_fieldName2Vc='Performance Security'
	Begin
		print ('ps')
		Set @v_Qry_Vc='select IsNull(Convert(varchar(50), A.perfSecAmt),''0.00'') as FieldValue1  
		from tbl_noaissuedetails A Where ' + @v_fieldName3Vc
	End
	Else If @v_fieldName2Vc='Bank Guarantee'
	Begin
		print ('bg')
		Set @v_Qry_Vc='select IsNull(Convert(varchar(50), A.bgAmt),''0.00'') as FieldValue1  
		from tbl_Cms_NewBankGuarnatee A Where ' + @v_fieldName3Vc
	End
	--print (@v_Qry_Vc)
	Exec (@v_Qry_Vc)
END   


IF @v_fieldName1Vc = 'getTenderPaymentDetail' 
BEGIN

	/*
	FieldValue1: Branch Name
	FieldValue2: Branch Manager (Name of Person who has done payment)
	FieldValue3: Curreny
	FieldValue4: Amount
	FieldValue5: Payment Instrument Type
	FieldValue6: Date Of Payment
	FieldValue7: Comments
	
	*/
	
--Select branchName as FieldValue1,
--		PA.fullName as FieldValue2,
--		currency as FieldValue3,
--		Convert(varchar(50), amount) as FieldValue4,
--		paymentInstType as FieldValue5,
--		REPLACE(CONVERT(VARCHAR(11),createdDate, 106), ' ', '-') as FieldValue6,		 
--		comments as FieldValue7,
--		isVerified as FieldValue8,
--		Convert(varchar(50), extValidityRef) as FieldValue9,
--		dbo.f_initcap(TP.status) as FieldValue10		
--	from tbl_TenderPayment TP 
--	Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
--Where tenderPaymentId=@v_fieldName2Vc And isLive='yes'

Select branchName as FieldValue1,
		dbo.f_GovUserName(TP.partTransId, 'tbl_PartnerAdminTransfer') as FieldValue2,
		currency as FieldValue3,
		Convert(varchar(50), amount) as FieldValue4,
		paymentInstType as FieldValue5,
		REPLACE(CONVERT(VARCHAR(11),createdDate, 106), ' ', '-')+ ' ' + Substring(CONVERT(VARCHAR(5),createdDate,108),1,8)  as FieldValue6,		 
		comments as FieldValue7,
		isVerified as FieldValue8,
		Convert(varchar(50), extValidityRef) as FieldValue9,
		dbo.f_initcap(TP.status) as FieldValue10		
	from tbl_TenderPayment TP 
	Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
Where tenderPaymentId=@v_fieldName2Vc And isLive='yes'


END



IF @v_fieldName1Vc = 'getTenderPaymentDetailMore' 
BEGIN
	
	/*
	FieldValue1: Instrument/Account No.
	FieldValue2: Issance Bank Name
	FieldValue3: Issance Bank Branch
	FieldValue4: Issuance Date
	FieldValue5: Valid Date
	*/
	
	Select 
	instRefNumber as FieldValue1,
	issuanceBank as FieldValue2,
	issuanceBranch as FieldValue3,	 
	REPLACE(CONVERT(VARCHAR(11),instDate, 106), ' ', '-') as FieldValue4,	 
	REPLACE(CONVERT(VARCHAR(11),instValidUpto, 106), ' ', '-')	as FieldValue5,
	IsNull(CONVERT(VARCHAR(10),instValidUpto, 103), '') as FieldValue6
	from tbl_TenderPayment TP Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
	Where tenderPaymentId=@v_fieldName2Vc And isLive='yes'

END

IF @v_fieldName1Vc = 'getTenderPaymentDetailForEdit' 
BEGIN
	/*
	FieldValue1: Currency
	FieldValue2: Amount
	FieldValue3: Payment Instrument Type
	FieldValue4: Date Of Payment : Format to be displayed
	FieldValue5: Comments	
	FieldValue6: Date Of Payment : Format Changed
	FieldValue7: Verification Status	
	*/
	
	Select	currency as FieldValue1,
		Convert(varchar(50), amount) as FieldValue2,
		paymentInstType as FieldValue3,		
		REPLACE(CONVERT(VARCHAR(11),createdDate, 106), ' ', '-') + ' ' + Substring(CONVERT(VARCHAR(5),createdDate,108),1,8)  as FieldValue4,		 
		comments as FieldValue5,
		RePlace(CONVERT(VARCHAR(50),createdDate, 102),'.','-') + ' ' + Substring(CONVERT(VARCHAR(30),createdDate,108),1,8) as FieldValue6,
		isVerified as FieldValue7,
		status as FieldValue8,
		Convert(varchar(50), extValidityRef) as FieldValue9		
	from tbl_TenderPayment TP 
	Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId	
	Where tenderPaymentId=@v_fieldName2Vc And isLive='yes'

END


IF @v_fieldName1Vc = 'getTenderPaymentDetailMoreForEdit' 
BEGIN
	
	/*
	FieldValue1: Instrument/Account No.
	FieldValue2: Issance Bank Name
	FieldValue3: Issance Bank Branch
	FieldValue4: Issuance Date
	FieldValue5: Valid Date
	*/
	
	Select 
	instRefNumber as FieldValue1,
	issuanceBank as FieldValue2,
	issuanceBranch as FieldValue3,	 
	CONVERT(VARCHAR(11),instDate, 103) as FieldValue4,	 
	CONVERT(VARCHAR(11),instValidUpto, 103)	as FieldValue5
	
	from tbl_TenderPayment TP Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
	Where tenderPaymentId=@v_fieldName2Vc And isLive='yes'

END


IF @v_fieldName1Vc = 'getTenderPaymentDetailExtraForEdit' 
BEGIN
	
	/*
	FieldValue1: Tender Id.
	FieldValue2: Package Lot Id.
	FieldValue3: Payment For info. 
	*/
	
	Select 
	 Convert (varchar(50), tenderId) as FieldValue1,
	Convert (varchar(50), pkgLotId) as FieldValue2,
	paymentFor as FieldValue3,
	Case paymentFor 
		When 'Document Fees' Then 'df'
		When 'Tender Security' Then 'ts'
		When 'Performance Security' Then 'ps'
	End as FieldValue4	 
	from tbl_TenderPayment TP Inner Join tbl_PartnerAdmin PA On TP.createdBy=PA.userId
	Where tenderPaymentId=@v_fieldName2Vc And isLive='yes'

END

IF @v_fieldName1Vc = 'getBranchMakerIdFromPaymentId' 
BEGIN
	Select CONVERT(varchar(50), createdBy) as FieldValue1 from tbl_TenderPayment
	Where tenderPaymentId=@v_fieldName2Vc
END


IF @v_fieldName1Vc = 'getRequestActionFromRequestId' 
BEGIN
	Select top 1 CONVERT(varchar(50), requestId) as FieldValue1,
	dbo.f_initcap(requestAction) as FieldValue2,	
	requestComments as FieldValue3,
	Convert(varchar(50),PA.sBankDevelopId) as FieldValue4
	From tbl_PaymentActionRequest PAR
	inner join tbl_PartnerAdmin PA On PAR.requestTo=PA.userId
	Where requestId=@v_fieldName2Vc 
	And paymentId=@v_fieldName3Vc
	And actionStatus='pending'
END


IF @v_fieldName1Vc = 'getRequestActionFromPaymentId' 
BEGIN
	Select top 1 CONVERT(varchar(50), requestId) as FieldValue1,
	dbo.f_initcap(requestAction) as FieldValue2   
	From tbl_PaymentActionRequest
	Where paymentId=@v_fieldName2Vc And actionStatus='pending'
END

IF @v_fieldName1Vc = 'getTenderScurityExtInfo'     
BEGIN
 Select top 1 Convert(varchar(50),Max(valExtDtId)) as FieldValue1,  
 REPLACE(CONVERT(VARCHAR(11),tenderSecNewDt, 106), ' ', '-') as FieldValue2,
 IsNull(CONVERT(VARCHAR(10),tenderSecNewDt, 103), '') as FieldValue3		 
 from tbl_TenderValidityExtDate 
 Where extStatus='Approved' 
 And tenderSecNewDt is not null and tenderSecNewDt <>'1900-01-01'
 And tenderId=@v_fieldName2Vc And  valExtDtId=@v_fieldName3Vc
 Group by valExtDtId, tenderSecNewDt
 Order by valExtDtId desc
END

IF @v_fieldName1Vc = 'getTenderSecurityPayCurrExtId' 
BEGIN
	Select Convert(varchar(50),extValidityRef) as FieldValue1 
	from tbl_TenderPayment
	Where tenderPaymentId= @v_fieldName2Vc
END


IF @v_fieldName1Vc = 'getPaymentBankInfofromPaymentId' 
BEGIN
	Select  bankName as FieldValue1,
	branchName as FieldValue2
	from tbl_TenderPayment
	Where tenderPaymentId= @v_fieldName2Vc
END

if @v_fieldName1Vc = 'isJVCAPartnerPaid' 
begin
select CONVERT(varchar(20),userid) as FieldValue1 from tbl_TenderPayment where userId in (select userId from tbl_JVCAPartners where JVId in( select jv.JVId from tbl_JointVenture j,tbl_JVCAPartners jv
where j.JVId=jv.JVId and JVStatus='Approved' and userId=@v_fieldName2Vc)) and tenderId=@v_fieldName3Vc
end

IF @v_fieldName1Vc='getMemDeclarationStatus'
BEGIN
	Select CM.appStatus as FieldValue1,
	Case EC.configType 
		When 'ind' Then '1'	
		When 'team' Then 
			Case 
				When (select tecMemberId from tbl_EvalConfig where tenderId=@v_fieldName2Vc)=CM.userId 	 
				Then '1'
				Else '0'
			End
		End as 	FieldValue2	
	from tbl_CommitteeMembers CM 
	Inner Join tbl_Committee C On CM.committeeId=C.committeeId
	Inner Join tbl_EvalConfig EC ON c.tenderId=EC.tenderId
	Where committeeType in ('TEC','PEC') 
		and committeStatus='Approved'
		and c.tenderId=@v_fieldName2Vc 
		And CM.userId=@v_fieldName3Vc
END

IF @v_fieldName1Vc='getMappingInfo'
BEGIN
	select Case When COUNT (evalFormId)>0 Then 'Yes' Else 'No' End as FieldValue1
from tbl_EvalMapForms Where tenderId=@v_fieldName2Vc
END


IF @v_fieldName1Vc='CheckIsTSCMember'
BEGIN
	Select 
	Case When Exists
	(select userId from tbl_CommitteeMembers CM
	Inner Join tbl_Committee C On CM.committeeId=C.committeeId
	where committeeType in ('TSC')And committeStatus='Approved'
	And tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc)
	Then 'Yes' Else 'No' End  as FieldValue1
END


IF @v_fieldName1Vc='getQueSenttoCPStatus'
BEGIN
	/*
		FieldValue1 = Questions Sent to Evaluation Chairperson
		FieldValue1 = Evaluation Notification Given to Chairperson after completing Evaluation
	*/

	Select Case 
	When Exists(select evalClrToCpId from tbl_EvalSentQueToCp Where tenderId=@v_fieldName2Vc And sentBy=@v_fieldName3Vc and sentFor='question')
	Then 'Yes'
	Else 'No'
	End as FieldValue1,
	Case 
	When Exists(select evalClrToCpId from tbl_EvalSentQueToCp Where tenderId=@v_fieldName2Vc And sentBy=@v_fieldName3Vc and sentFor='evaluation')
	Then 'Yes'
	Else 'No'
	End as FieldValue2
	
	
END

IF @v_fieldName1Vc = 'getLotDescripton_ForLotPrep'     -- For
BEGIN
		
	If (select COUNT(tenderLotId) from tbl_TenderLots where tenderId=@v_fieldName2Vc) > 1 -- Multiple Lot Case
	Begin					
		IF @v_fieldName3Vc is not null  And @v_fieldName3Vc<>''
		BEGIN		
			If (select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1
			Begin
				SELECT TLS.lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
			IsNull(Convert(varchar(50),(select top 1 tenderPaymentId from tbl_TenderPayment TP 
			Where paymentFor='Document Fees' 		
			And TP.tenderId=TLS.tenderId		 
			And pkgLotId=TLS.appPkgLotId 
			And isVerified='Yes' And isLive='Yes' 
			And TP.userId in ((select userId from tbl_TendererMaster where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1) ) ) 
			Order by tenderPaymentId desc)),'0') as FieldValue4	
			FROM  dbo.tbl_TenderLotSecurity TLS,tbl_bidderLots tbl 
			WHERE TLS.tenderId = @v_fieldName2Vc and TLS.appPkgLotId=tbl.pkgLotId	and tbl.userId=@v_fieldName3Vc
			End
			Else
			Begin
				SELECT TLS.lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
				IsNull(Convert(varchar(50),(select top 1 tenderPaymentId from tbl_TenderPayment TP Where paymentFor='Document Fees' And TP.userId=@v_fieldName3Vc And TP.tenderId=TLS.tenderId And pkgLotId=TLS.appPkgLotId And isVerified='Yes' And isLive='Yes' Order by tenderPaymentId desc)),'0') as FieldValue4	
				FROM  dbo.tbl_TenderLotSecurity TLS,tbl_bidderLots tbl 
				WHERE TLS.tenderId = @v_fieldName2Vc  and TLS.appPkgLotId=tbl.pkgLotId	and tbl.userId=@v_fieldName3Vc
			End
			
		END
		ELSE
		BEGIN		
			SELECT TLS.lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
			 'N.A.' as FieldValue4			
			FROM  dbo.tbl_TenderLotSecurity TLS,tbl_bidderLots tbl  
			WHERE TLS.tenderId = @v_fieldName2Vc and TLS.appPkgLotId=tbl.pkgLotId and tbl.userId=@v_fieldName3Vc
		END
	End
	Else If (select COUNT(tenderLotId) from tbl_TenderLots where tenderId=@v_fieldName2Vc) = 1 -- Single Lot Case
	Begin
						
		IF @v_fieldName3Vc is not null  And @v_fieldName3Vc<>''
		BEGIN		
			If (select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc)<>1
			Begin
					SELECT TLS.lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
				IsNull(Convert(varchar(50),(select top 1 tenderPaymentId from tbl_TenderPayment TP 
				Where paymentFor='Document Fees' 		
				And TP.tenderId=TLS.tenderId		 
				And pkgLotId=TLS.appPkgLotId 
				And isVerified='Yes' And isLive='Yes' 
				And TP.userId in ((select userId from tbl_TendererMaster where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1) ) ) 
				Order by tenderPaymentId desc)),'0') as FieldValue4	
				FROM  dbo.tbl_TenderLotSecurity TLS
				WHERE TLS.tenderId = @v_fieldName2Vc
			End
			Else
				Begin
					SELECT TLS.lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
				IsNull(Convert(varchar(50),(select top 1 tenderPaymentId from tbl_TenderPayment TP Where paymentFor='Document Fees' And TP.userId=@v_fieldName3Vc And TP.tenderId=TLS.tenderId And pkgLotId=TLS.appPkgLotId And isVerified='Yes' And isLive='Yes' Order by tenderPaymentId desc)),'0') as FieldValue4	
				FROM  dbo.tbl_TenderLotSecurity TLS
				WHERE TLS.tenderId = @v_fieldName2Vc
			End
			
		END
		ELSE
		BEGIN		
			SELECT TLS.lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
			 'N.A.' as FieldValue4			
			FROM  dbo.tbl_TenderLotSecurity TLS 
			WHERE tenderId = @v_fieldName2Vc	
		END
	End

		
END


IF @v_fieldName1Vc = 'getTenderEnvelopeCount'     -- For
BEGIN
	select CONVERT(varchar(50), tcm.noOfEnvelops) as FieldValue1,
	(select Case When Count(appPkgLotId) > 1 Then 'Yes' Else 'No' End from tbl_TenderLotSecurity Where tenderId=@v_fieldName2Vc) as FieldValue2,
	ttd.procurementNature as FieldValue3	
	from tbl_TenderDetails ttd,
	tbl_TenderTypes ttt,
	tbl_ConfigEvalMethod tcm
	
	 where ttd.eventType =ttt.tenderType 
	 and ttd.procurementMethodId =tcm.procurementMethodId 
	 and ttt.tenderTypeId=tcm.tenderTypeId  
	 and ttd.procurementNatureId =tcm.procurementNatureId 	 
	 and ttd.tenderId=@v_fieldName2Vc
end

IF @v_fieldName1Vc = 'getPriceBidForms_ForOpening'     
BEGIN

	IF @v_fieldName3Vc <> '0'
	BEGIN
		SELECT distinct formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
			Case When Exists (select tbpd.tenderFormId from tbl_TenderTables tbpd inner join tbl_TenderBidTable tbi ON tbpd.tenderTableId=tbi.tenderTableId Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
			ISNULL('', '') as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9,
			(select Case When (openingDt is null Or openingDt='') then 'N.A.' When openingDt > getdate() Then 'no' Else 'yes' End from tbl_TenderDetails Where tenderId=@v_fieldName2Vc) as  FieldValue10
			FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
			ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
			dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 		
			Where td.tenderId = @v_fieldName2Vc and tf.pkgLotId = @v_fieldName3Vc
			And isPriceBid='Yes'
			Order By isPriceBid
	END
	Else
	Begin
		SELECT distinct formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
			Case When Exists (select tbpd.tenderFormId from tbl_TenderTables tbpd inner join tbl_TenderBidTable tbi ON tbpd.tenderTableId=tbi.tenderTableId Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
			ISNULL('', '') as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9,
			(select Case When (openingDt is null Or openingDt='') then 'N.A.' When openingDt > getdate() Then 'no' Else 'yes' End from tbl_TenderDetails Where tenderId=@v_fieldName2Vc) as  FieldValue10
			FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
			ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
			dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 		
			Where td.tenderId = @v_fieldName2Vc
			And isPriceBid='Yes'
			Order By isPriceBid		
	End

END

IF @v_fieldName1Vc = 'getTechnicalForms_ForOpening'     
BEGIN		
	IF @v_fieldName3Vc <> '0'
	BEGIN
		SELECT distinct formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
			Case When Exists (select tbpd.tenderFormId from tbl_TenderTables tbpd inner join tbl_TenderBidTable tbi ON tbpd.tenderTableId=tbi.tenderTableId Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
			ISNULL('', '') as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9,
			(select Case When (openingDt is null Or openingDt='') then 'N.A.' When openingDt > getdate() Then 'no' Else 'yes' End from tbl_TenderDetails Where tenderId=@v_fieldName2Vc) as  FieldValue10
			FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
			ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
			dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 		
			Where td.tenderId = @v_fieldName2Vc and packageLotId = @v_fieldName3Vc
			And isPriceBid='No'
			Order By isPriceBid	
	END
	ELSE
	BEGIN	
		SELECT distinct formName as FieldValue1, isEncryption as FieldValue2, isPriceBid as FieldValue3, isMandatory as FieldValue4, convert(varchar, tf.tenderFormId) as FieldValue5,
			Case When Exists (select tbpd.tenderFormId from tbl_TenderTables tbpd inner join tbl_TenderBidTable tbi ON tbpd.tenderTableId=tbi.tenderTableId Where tbpd.tenderFormId=tf.tenderFormId) Then 'yes' Else 'no' End as FieldValue6, 
			ISNULL('', '') as FieldValue7, isEncryption as FieldValue8 ,isMultipleFilling as FieldValue9,
			(select Case When (openingDt is null Or openingDt='') then 'N.A.' When openingDt > getdate() Then 'no' Else 'yes' End from tbl_TenderDetails Where tenderId=@v_fieldName2Vc) as  FieldValue10
			FROM dbo.tbl_TenderForms tf Inner Join dbo.tbl_TenderSection ts
			ON tf.tenderSectionId = ts.tenderSectionId Inner Join 
			dbo.tbl_TenderStd td on ts.tenderStdId = td.tenderStdId 		
			Where td.tenderId = @v_fieldName2Vc
			And isPriceBid='No'
			Order By isPriceBid
	
	END
		
END


IF @v_fieldName1Vc = 'getLoIdLotDesc_ForOpening' 
BEGIN
	select convert(varchar(50),appPkgLotId ) as FieldValue1,
	lotDesc as FieldValue2 , lotNo as FieldValue3
	from tbl_TenderLotSecurity where tenderId=@v_fieldName2Vc
END


IF @v_fieldName1Vc = 'getFormStatus_ForOpening' 
BEGIN
	Select 
	Case formStatus When 'c' Then 'Canceled' Else 'OK' End as FieldValue1,
	Case When Exists (select top 1 f.finalSubmissionId 
			from tbl_FinalSubmission f,tbl_FinalSubDetail fd,tbl_TenderBidForm tf
				where f.finalSubmissionId=fd.finalSubmissionId
					and bidSubStatus ='finalsubmission' and fd.tenderFormid=@v_fieldName2Vc
					and fd.tenderFormid=tf.tenderFormId)
		Then 'Yes' Else 'No' End as FieldValue2			
	from tbl_TenderForms Where tenderFormId=@v_fieldName2Vc
END

IF @v_fieldName1Vc = 'getTenderProcurementNature' 
BEGIN
	Select procurementNature as FieldValue1 
	from tbl_TenderDetails Where tenderId=@v_fieldName2Vc
END



IF @v_fieldName1Vc = 'getEvalBidderStatus' 
BEGIN
	--Set @v_Qry_Vc='Select Case 
	--	When Exists (select evaBidderStatusId 
	--	from tbl_EvalBidderStatus
	--	where '+ @v_fieldName2Vc +') 
	--	Then ''Yes''
	--	Else ''No'' End as FieldValue1'
	
	
	Set @v_Qry_Vc='Select Case 
		When Exists (select esFormDetailId 
		from tbl_EvalSerFormDetail
		where '+ @v_fieldName2Vc +') 
		Then ''Yes''
		Else ''No'' End as FieldValue1'
	
	print (@v_Qry_Vc)	
	exec (@v_Qry_Vc)	
END

IF @v_fieldName1Vc = 'getEvaluateTenderer_linkStatus' 
BEGIN
	Set @v_Qry_Vc='Select Case 
		When Exists (select evaBidderStatusId 
		from tbl_EvalBidderStatus
		where '+ @v_fieldName2Vc +') 
		Then ''Yes''
		Else ''No'' End as FieldValue1'
		
	print (@v_Qry_Vc)	
	exec (@v_Qry_Vc)	
END

IF @v_fieldName1Vc = 'getAllConsultantEvaluationLinkStatus' 
BEGIN
	--Select Case When
	--(select count(evaBidderStatusId) from tbl_EvalBidderStatus 
	--where tenderId=@v_fieldName2Vc And evalBy=@v_fieldName3Vc) > 0
	--Then 'Yes'
	--Else 'No'
	--End as FieldValue1 		
	
	Select Case When
	(select count(esFormDetailId) from tbl_EvalSerFormDetail 
	where tenderId=@v_fieldName2Vc And evalBy=@v_fieldName3Vc) > 0
	Then 'Yes'
	Else 'No'
	End as FieldValue1 	
	
END


IF @v_fieldName1Vc = 'getAllConsultantEvaluationLinkStatusForTeamWise' 
BEGIN
	--Select Case When
	--(select count(evaBidderStatusId) from tbl_EvalBidderStatus 
	--where tenderId=@v_fieldName2Vc And evalBy=@v_fieldName3Vc) > 0
	--Then 'Yes'
	--Else 'No'
	--End as FieldValue1 		
	
	Select Case When
	(select count(esFormDetailId) from tbl_EvalSerFormDetail 
	where tenderId=@v_fieldName2Vc) > 0
	Then 'Yes'
	Else 'No'
	End as FieldValue1 	
	
END

IF @v_fieldName1Vc = 'getOverallTechScore_ForEvaluation' 
BEGIN
	--Select IsNull(Convert(varchar(50), COUNT(cm.comMemberId)),'0') as FieldValue1, 
	--IsNull(Convert(varchar(50), COUNT(es.evalClrToCpId)),'0') as FieldValue2
	--from tbl_CommitteeMembers cm 
	--Inner Join tbl_Committee c On cm.committeeId=c.committeeId
	--Left Join tbl_EvalSentQueToCp es On c.tenderId=es.tenderId And cm.userId=es.sentBy
	--Where c.tenderId=@v_fieldName2Vc And committeeType in ('TEC','PEC') 
	--And committeStatus='Approved' And cm.memberRole<>'cp'
	
	--select Convert(varchar(50), COUNT(tenderId)) as FieldValue1 
	--from tbl_EvalSerFormDetail 
	--where tenderId=@v_fieldName2Vc 
	----And isMemberDropped='No'
	
		select Convert(varchar(50), COUNT(tenderId)) as FieldValue1 
	from tbl_EvalBidderStatus 
	where tenderId=@v_fieldName2Vc 
	
	
	
END


IF @v_fieldName1Vc = 'getBidderEvalReportStatus' 
BEGIN
--	Select IsNull(Convert(varchar(50), COUNT(cm.comMemberId)),'0') as FieldValue1,
--(Select IsNull(Convert(varchar(50), COUNT(distinct evalBy)),'0')  from tbl_EvalSerFormDetail Where tenderId=@v_fieldName2Vc And userId=@v_fieldName3Vc And isMemberDropped='No')as FieldValue2	
--	from tbl_CommitteeMembers cm 
--	Inner Join tbl_Committee c On cm.committeeId=c.committeeId	
--	Where c.tenderId=@v_fieldName2Vc And committeeType in ('TEC','PEC') 
--	And committeStatus='Approved' 

Select  Convert(varchar(50), COUNT(evalClrToCpId)) as FieldValue1 
from tbl_EvalSentQueToCp Where tenderId=@v_fieldName2Vc

END




IF @v_fieldName1Vc = 'getTenderRefNo' -- for ConfigureKeyInfo.jsp
BEGIN
	
	Select convert(varchar(20),TD.tenderid) as FieldValue1, reoiRfpRefNo as FieldValue2	
	 from tbl_TenderDetails td Left Join 
	 tbl_TenderOpenDates TOD On td.tenderId=TOD.tenderId
	where td.tenderId = @v_fieldName2Vc
	
End

IF @v_fieldName1Vc = 'getBidderLotSelectionLinkStatus' -- for ConfigureKeyInfo.jsp
BEGIN
	Select 
	Case 
		When Exists (select finalSubmissionId from tbl_FinalSubmission where tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc and bidSubStatus='finalsubmission' )
		Then '0'
		Else '1'
	End as FieldValue1,
	Case 
		When Exists (select bidModId from tbl_BidModification where tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc And bidStatus='withdrawal' )
		Then '0'
		Else '1'
	End as FieldValue2,
	Case 
		When (select submissionDt from tbl_TenderDetails Where tenderId=@v_fieldName2Vc) > GETDATE()
		Then '1'
		Else '0'
	End as FieldValue3	
	
End


IF @v_fieldName1Vc = 'getTECMemberQuecnt' -- for Evalclarify.jsp
BEGIN

select Convert(varchar(50), COUNT(evalQueId)) as FieldValue1 from tbl_EvalFormQues Where tenderId=@v_fieldName2Vc And quePostedBy=@v_fieldName3Vc

END


IF @v_fieldName1Vc = 'GetEvalTECMemberFinalNotification'   -- FOR Evalclarify.JSP :- 
BEGIN
	If (Select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='Services'
	Begin
		SELECT CONVERT(VARCHAR(30), COUNT(esFormDetailId)) as FieldValue1,
		(Select convert(varchar(20),COUNT(evalClrToCpId)) as FieldValue1 from tbl_EvalSentQueToCp where sentFor = 'evaluation' and tenderId = @v_fieldName2Vc and sentBy = @v_fieldName3Vc) as FieldValue2
		FROM dbo.tbl_EvalSerFormDetail WHERE tenderId = @v_fieldName2Vc And evalBy=@v_fieldName3Vc
	End
	Else
	Begin
		SELECT CONVERT(VARCHAR(30), COUNT(evalMemStatusId)) as FieldValue1,
		(Select convert(varchar(20),COUNT(evalClrToCpId)) as FieldValue1 from tbl_EvalSentQueToCp where sentFor = 'evaluation' and tenderId = @v_fieldName2Vc and sentBy = @v_fieldName3Vc) as FieldValue2 
		FROM dbo.tbl_EvalMemStatus WHERE tenderId = @v_fieldName2Vc And evalBy=@v_fieldName3Vc
	End
		
END



IF @v_fieldName1Vc = 'getEvaluateTenderer_linkStatusForTECMember' 
BEGIN
	select Convert(varchar(50),COUNT(evalClrToCpId)) as FieldValue1
	from tbl_EvalSentQueToCp where sentFor='evaluation' And tenderId=@v_fieldName2Vc and sentBy=@v_fieldName3Vc
END


IF @v_fieldName1Vc = 'GetEvalReportLinkStatus'   -- FOR AfterLoginTSC.JSP :- krish
BEGIN	
		
	If (Select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='Services'
	Begin
		SELECT CONVERT(VARCHAR(30), COUNT(esFormDetailId)) as FieldValue1		
		FROM dbo.tbl_EvalSerFormDetail WHERE tenderId = @v_fieldName2Vc
	End
	Else
	Begin
		SELECT CONVERT(VARCHAR(30), COUNT(evalMemStatusId)) as FieldValue1 
		FROM dbo.tbl_EvalMemStatus WHERE tenderId = @v_fieldName2Vc
	End
		
END


IF @v_fieldName1Vc='getRegValidityStatus'
BEGIN
select Case When validUpTo >= GETDATE() Then 'Yes' Else 'No' End as FieldValue1
from tbl_LoginMaster where userId=@v_fieldName2Vc
END


IF @v_fieldName1Vc='getGovUserIdFromUserId'
BEGIN
	Select Convert(varchar(50),govUserId) as FieldValue1 
	from tbl_EmployeeTrasfer et Where  et.userId=@v_fieldName2Vc
END



IF @v_fieldName1Vc='getEvalTeamSelectedMember'
BEGIN
	select Convert(varchar(50),tecMemberId) as FieldValue1 
	from tbl_EvalConfig Where configType='team' 
	and tenderId=@v_fieldName2Vc
END


IF @v_fieldName1Vc = 'getmarqueelistWithPaging' -- for MarqueeListing.jsp
begin
  --SELECT convert(varchar(20),tm.marqueeId) as FieldValue1, 
  --tm.marqueeText as FieldValue2, 
  --(select tl.locationName from tbl_locationmaster tl where tl.locationId = tm.locationId)as FieldValue3   
  --FROM tbl_MarqueeMaster tm order by tm.marqueeId desc
   



/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName2Vc, @v_RecordPerPage_inInt=@v_fieldName3Vc
	
	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)	
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */

-- // DYNAMIC QUERY TO GET COUNT OF TOTAL RECORDS AND TOTAL PAGES COUNT
	Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT( Distinct tm.marqueeId ) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')
	 FROM tbl_MarqueeMaster tm'
	
		
	Select @v_CntQry_Vc='SELECT Count(Distinct tm.marqueeId) 
	FROM tbl_MarqueeMaster tm'
		
		
	Print ('Count Query')	
	Print(@v_CntQry_Vc)
		
			/* START CODE: DYNAMIC QUERY TO GET RECORDS */
			
			/*
				FieldValue1: Payment Id
				FieldValue2: User Id of Bidder for which Payment is done
				FieldValue3: Email Id of Bidder for which Payment is done
				FieldValue4: Branch Maker - Name of Bank User who has done the Payment
				FieldValue5: Payment Status New Payment/Renewal
				FieldValue6: Date of Payment
			*/
			
		Select @v_FinalQueryVc=
	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int; 
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
	
	Select FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6, FieldValue7, FieldValue8, RowNumber
	From
	(Select Distinct
		convert(varchar(20),Tmp.marqueeId) as FieldValue1, 
		marqueeText as FieldValue2, 
		(select tl.locationName from tbl_locationmaster tl where tl.locationId = tm.locationId)as FieldValue3,
			CONVERT(varchar(50),@v_TotalPageCnt_Int) as FieldValue4, 
			CONVERT(varchar(50),@v_TotalRecordCnt_Int) as FieldValue5,
			CONVERT(varchar(50),Tmp.RowNumber) as FieldValue6,
			CONVERT(varchar(50),userTypeId) as FieldValue7,
			CONVERT(varchar(50),locationId) as FieldValue8,
			RowNumber
		From
		(Select RowNumber, marqueeId From
			(SELECT ROW_NUMBER() Over (Order by marqueeId desc) as RowNumber, marqueeId
				FROM tbl_MarqueeMaster tm
		) B 				
		
		WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int) 		
		+ ' ) Tmp
		Inner Join 
		tbl_MarqueeMaster tm On Tmp.marqueeId=tm.marqueeId		
		) as A Order by RowNumber
		'		
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */
		
	
		PRINT(@v_FinalQueryVc)
		Exec (@v_FinalQueryVc)
  
end


IF @v_fieldName1Vc = 'getArchiveNewslisting' Or @v_fieldName1Vc='getLiveNewslisting'
BEGIN

/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
	Select @v_Page_inInt=@v_fieldName2Vc, @v_RecordPerPage_inInt=@v_fieldName3Vc
	
	Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
	Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)	
	/* END CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */


SET @v_ConditionString_Vc=''

If @v_fieldName1Vc='getArchiveNewslisting' 
Begin
	set @v_ConditionString_Vc = @v_ConditionString_Vc + ' And isArchive=''yes'''
End
else If @v_fieldName1Vc='getLiveNewslisting' 
Begin
	set @v_ConditionString_Vc = @v_ConditionString_Vc + ' And isArchive=''no'''
End


Select @v_PgCntQry_Vc= 'SELECT Ceiling(Cast(COUNT( Distinct newsId ) as float)/'+Convert(varchar(50),@v_RecordPerPage_inInt)+')
	 From dbo.tbl_NewsMaster NM 
			Inner Join tbl_LocationMaster LM On NM.locationId=LM.locationId
		Where isDeleted=''no'' ' + @v_ConditionString_Vc
	
		
	Select @v_CntQry_Vc='SELECT Count(Distinct newsId) 
	From dbo.tbl_NewsMaster NM 
			Inner Join tbl_LocationMaster LM On NM.locationId=LM.locationId
		Where isDeleted=''no''' + @v_ConditionString_Vc 
		
		
	Print ('Count Query')	
	Print(@v_CntQry_Vc)
	
	/* START CODE: DYNAMIC QUERY TO GET RECORDS */
		Select @v_FinalQueryVc=
	'DECLARE @v_TotalPageCnt_Int Int, @v_TotalRecordCnt_Int Int; 
		Set @v_TotalPageCnt_Int = ('+@v_PgCntQry_Vc+');
		Set @v_TotalRecordCnt_Int = ('+@v_CntQry_Vc+');
	
	Select FieldValue1, FieldValue2, FieldValue3, FieldValue4, FieldValue5, FieldValue6, RowNumber
	From
	(Select Distinct
		convert(varchar(20),Tmp.newsId) as FieldValue1, 
		newsBrief as FieldValue2, 
		locationName as FieldValue3,
		CONVERT(varchar(50),@v_TotalPageCnt_Int) as FieldValue4, 
		CONVERT(varchar(50),@v_TotalRecordCnt_Int) as FieldValue5,
		CONVERT(varchar(50),Tmp.RowNumber) as FieldValue6,
		RowNumber
	From
		(Select RowNumber, newsId From
			(SELECT ROW_NUMBER() Over (Order by newsId desc) as RowNumber, newsId
				From dbo.tbl_NewsMaster NM 
				Inner Join tbl_LocationMaster LM On NM.locationId=LM.locationId
			Where isDeleted=''no''' + @v_ConditionString_Vc +
		' ) B 				
		
		WHERE RowNumber Between ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int) 		
		+ ' ) Tmp
		Inner Join 
		dbo.tbl_NewsMaster NM On Tmp.newsId=NM.newsId	
		Inner Join tbl_LocationMaster LM On NM.locationId=LM.locationId		
		) as A Order by RowNumber
		'		
		/* END CODE: DYNAMIC QUERY TO GET RECORDS */

		PRINT(@v_FinalQueryVc)
		Exec (@v_FinalQueryVc)

		--Select CONVERT(Varchar(50),newsId) as FieldValue1, 
		--	newsBrief as FieldValue2, 
		--	locationName as FieldValue3
		--From dbo.tbl_NewsMaster NM 
		--	Inner Join tbl_LocationMaster LM On NM.locationId=LM.locationId
		--Where isArchive='yes' And isDeleted='no'  
		--Order by newsId desc
		
		
END


IF @v_fieldName1Vc = 'GetEvalCPClarificationLinkStatus'
BEGIN
SELECT top 1 a.tenderFormId FROM tbl_EvalCpMemClarification a
inner join tbl_TenderForms b on a.tenderFormId=b.tenderFormId
inner join tbl_TenderSection ts On b.tenderSectionId=ts.tenderSectionId
inner join tbl_TenderStd td On ts.tenderStdId=td.tenderStdId 
Where userId=@v_fieldName2Vc and td.tenderId=@v_fieldName3Vc
END

IF @v_fieldName1Vc = 'GetWorkflowFileHistoryCount'
BEGIN
SELECT Convert(varchar(50),COUNT(wfFileOnHandId)) as FieldValue1 from tbl_WorkFlowFileHistory wfh
Where wfh.objectId=@v_fieldName2Vc and wfh.activityId=@v_fieldName3Vc
END

IF @v_fieldName1Vc = 'GetAppPackageCount'
BEGIN
select Convert(varchar(50),COUNT(packageId)) as FieldValue1
from tbl_AppPackages a, tbl_ConfigDocFees c, tbl_ProcurementTypes pt,
 tbl_TenderTypes tt,tbl_ProcurementMethod pm
where a.procurementMethodId = c.procurementMethodId
 and a.procurementType = pt.procurementType 
 and c.tenderTypeId=tt.tenderTypeId 
 and c.procurementTypeId=pt.procurementTypeId
And c.tenderTypeId=tt.tenderTypeId
and c.procurementMethodId=pm.procurementMethodId
and a.packageId = @v_fieldName2Vc
and tenderType =
(
Select Case 
	When (isPQRequired='yes')
	Then 'PQ'
	When (isPQRequired='no' And procurementMethodId!=5)
	Then 'Tender'
	When (procurementMethodId=1)
	Then 'RFQ'
	When (isPQRequired='' And reoiRfaRequired<>'' And reoiRfaRequired is not null)
	Then reoiRfaRequired
	When (procurementMethodId=5)
	Then '1 stage-TSTM'
	When (procurementMethodId!=5)
	Then 'Tender'
	End
from tbl_AppPackages a
where a.packageId = @v_fieldName2Vc
)
END

IF @v_fieldName1Vc = 'GetAppProcurementCount'
BEGIN
select Convert(varchar(50),COUNT(packageId)) as FieldValue1 from
tbl_ConfigProcurement c, tbl_AppMaster a,tbl_AppPackages p, tbl_ProcurementTypes pt,
tbl_ProcurementNature pn,tbl_TenderTypes tt
where c.procurementMethodId=p.procurementMethodId and a.appId=p.appId 
and c.budgetTypeId=a.budgetType
and pt.procurementTypeId=c.procurementTypeId and p.procurementType=pt.procurementType
and p.procurementnature=pn.procurementNature and c.procurementNatureId=pn.procurementNatureId
and c.isNationalDisaster=p.pkgUrgency and estimatedCost between minValue and maxValue
and tt.tenderTypeId=c.tenderTypeId 
and p.packageId = @v_fieldName2Vc
and tenderType =
(
Select Case 
	When (isPQRequired='yes')
	Then 'PQ'
	When (isPQRequired='no' And procurementMethodId!=5)
	Then 'Tender'
	When (procurementMethodId=1)
	Then 'RFQ'
	When (isPQRequired='' And reoiRfaRequired<>'' And reoiRfaRequired is not null)
	Then reoiRfaRequired
	When (procurementMethodId=5)
	Then '1 stage-TSTM'
	When (procurementMethodId!=5)
	Then 'Tender'
	End
from tbl_AppPackages a
where a.packageId = @v_fieldName2Vc
)
END

IF @v_fieldName1Vc = 'chkEvalMember'
BEGIN
	Select Case 
			When Exists 
				(Select top 1 comMemberId from tbl_CommitteeMembers cm inner join tbl_Committee c on cm.committeeId=c.committeeId where c.tenderId=@v_fieldName2Vc and committeeType in ('TEC','PEC') and cm.userId=@v_fieldName3Vc)
			Then 'Yes'
			Else 'No'
			End as FieldValue1,
			Case 
			When Exists 
				(Select top 1 comMemberId from tbl_CommitteeMembers cm inner join tbl_Committee c on cm.committeeId=c.committeeId where c.tenderId=@v_fieldName2Vc and committeeType in ('TSC') and cm.userId=@v_fieldName3Vc)
			Then 'Yes'
			Else 'No'
			End as FieldValue2,
			Case 
			When Exists 
				(Select top 1 comMemberId from tbl_CommitteeMembers cm inner join tbl_Committee c on cm.committeeId=c.committeeId where c.tenderId=@v_fieldName2Vc and committeeType in ('TEC','PEC') and cm.userId=@v_fieldName3Vc and  appStatus='approved')
			Then 'Yes'
			Else 'No'
			End as FieldValue3,
			IsNull((Select configType from tbl_EvalConfig where tenderId=@v_fieldName2Vc),'N.A.') as FieldValue4
		
END

IF @v_fieldName1Vc = 'tenderinfobarMore' -- for ConfigureKeyInfo.jsp
BEGIN
	Select eventType as FieldValue1
	 from tbl_TenderDetails td
	where td.tenderId = @v_fieldName2Vc
	
End

 If @v_fieldName1Vc='TenderPaymentDocsForView'
   Begin
		Select documentName as FieldValue1, 
		docDescription as FieldValue2, 
		docSize as FieldValue3,
		cast(paymentRefDocId  as varchar(10)) as FieldValue4,
		Case When uploadedBy=Cast (@v_fieldName3Vc as int) Then 'Yes' Else 'No' End as FieldValue5
		from tbl_TenderPaymentDocs 
		Where tenderPaymentId=@v_fieldName2Vc 
   End
   
   
   
IF @v_fieldName1Vc = 'getTenSecExtLinkStatus'     
BEGIN
	/*
		@v_fieldName2Vc = Current Extension Id
		@v_fieldName3Vc = New Extension Id
	*/
	
	 --Select top 1 Convert(varchar(50),Max(valExtDtId)) as FieldValue1  from tbl_TenderValidityExtDate 
	 --Where tenderId=@v_fieldName2Vc And extStatus='Approved' 
	 --And tenderSecNewDt is not null and tenderSecNewDt <>'1900-01-01'
	 --Group by valExtDtId, tenderSecNewDt
	 --Order by valExtDtId desc
	 
	 If @v_fieldName2Vc = 0
	 Begin
		If	(Select tenderSecNewDt 
				from tbl_TenderValidityExtDate 
					Where valExtDtId=@v_fieldName3Vc 
					And extStatus='Approved' 
					And tenderSecNewDt is not null 
					and tenderSecNewDt <>'1900-01-01'
					)
			
			>
			
			(select tenderSecurityDt from tbl_TenderDetails td 
				inner join tbl_TenderValidityExtDate tv On td.tenderId=tv.tenderId
				where tv.valExtDtId = @v_fieldName3Vc )
		Begin
			select 'Yes' as FieldValue1
		End		
		Else 
		Begin
			select 'No' as FieldValue1
		End
	 End
	 Else If @v_fieldName2Vc > 0
	 Begin
	 
		If	(Select tenderSecNewDt 
				from tbl_TenderValidityExtDate 
					Where valExtDtId=@v_fieldName3Vc 
					And extStatus='Approved' 
					And tenderSecNewDt is not null 
					and tenderSecNewDt <>'1900-01-01'
					)
			
			>
			
			(Select tenderSecNewDt 
				from tbl_TenderValidityExtDate 
					Where valExtDtId=@v_fieldName2Vc 
					And extStatus='Approved' 
					And tenderSecNewDt is not null 
					and tenderSecNewDt <>'1900-01-01'
					)
		Begin
			select 'Yes' as FieldValue1
		End		
		Else 
		Begin
			select 'No' as FieldValue1
		End
	 
	 End
	 
	 
END 


IF @v_fieldName1Vc = 'getIsTenderLimitedStatus' -- Get the Value to Indicate whether Tender is Limited or Open     
BEGIN		
	Select Case 
			When (eventType='2 Stage-PQ' And pm.procurementMethod='OTM')
			Then 'Limited'
			When (eventType='TENDER' And pm.procurementMethod='LTM')
			Then 'Limited'
			When ((eventType='RFP' And pqTenderId!=0)  OR (eventType='RFP' And pm.procurementMethod='DC') OR (eventType='RFP' And pm.procurementMethod='SSS') OR (eventType='RFP' And pm.procurementMethod='IC') )			
			Then 'Limited'
			When (eventType='RFA')
			Then 'Limited'
			When (eventType='RFQ' And pm.procurementMethod='RFQ')
			Then 'Limited'
			When (eventType='2 stage-TSTM' And pm.procurementMethod='TSTM')
			Then 'Limited'
			Else 'Open'
			End as FieldValue1
	from tbl_TenderDetails td
	inner join tbl_ProcurementMethod pm On td.procurementMethodId=pm.procurementMethodId
	Where tenderId=	@v_fieldName2Vc	
END


IF @v_fieldName1Vc='getTenderInfoForTenderLotPrep' -- for tender lot preparation case
BEGIN 

	/*	
		FieldValue2 : shows whether document available method is Package or Lot wise
		FieldValue7 : shows whether document is free or paid
		FieldValue8 : shows whether document fees (if applicable)is paid or not
	*/
	select CONVERT(varchar(50),TM.tenderId) as FieldValue1, 
	docFeesMode as FieldValue2, 
	docAvlMethod as FieldValue3, 
	IsNull(REPLACE(CONVERT(VARCHAR(11),TD.docEndDate, 106), ' ', '-') ,'') as FieldValue4, 
	IsNull(CONVERT(varchar(50), datediff(day, GETDATE(), TD.docEndDate)),'null') as FieldValue5,
	IsNull(Convert(varchar(50), pkgDocFees),'')  as FieldValue6,
	Case docFeesMethod 
		When 'Package wise'	
		Then 
			Case 
				When (pkgDocFees > 0) 
				Then 'Paid'
				Else 'Free'
			End
		When 'Lot wise'		
		Then 
			Case 
				When ((select Sum(docFess) from tbl_TenderLotSecurity Where tenderId=@v_fieldName2Vc) > 0)
				Then 'Paid'
				Else 'Free'
			End
		Else 'Free'		
		End as FieldValue7,
	Case 
		When Exists (select top 1 tenderPaymentId from tbl_TenderPayment where tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc and paymentFor='Document Fees' and isVerified='yes' and isLive='yes')	
		Then 'yes'
		Else 'no'
	End as 	FieldValue8
	from tbl_TenderDetails TD 
	inner join tbl_TenderMaster TM On TD.tenderId=TM.tenderId	
	where TM.tenderId=@v_fieldName2Vc
	Group by TM.tenderId, docFeesMode, docAvlMethod, TD.docEndDate, pkgDocFees, TD.tenderSecurityDt, TD.docFeesMethod
END

IF @v_fieldName1Vc='getTenderInfoForTenderLotPrepMore' -- for tender lot preparation case
BEGIN 
	
	If (select docAvlMethod from tbl_TenderDetails where tenderId=@v_fieldName2Vc) = 'Package'
	Begin
		If (select docAccess from tbl_TenderDetails where tenderId=@v_fieldName2Vc) = 'Limited'
		Begin
			-- // Limited Tender Case
			SELECT TLS.lotNo as FieldValue1, 
				lotDesc as FieldValue2,
				CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
				'N.A.' as FieldValue4			
				FROM  dbo.tbl_TenderLotSecurity TLS 
				WHERE tenderId = @v_fieldName2Vc
				And tenderId in
				(select tenderId from tbl_LimitedTenders where tenderId=@v_fieldName2Vc
					 and
					(
						userId in 								
						(
							select distinct userId from tbl_TendererMaster 
							where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
						)  	
						Or
						userId=@v_fieldName3Vc		
					)
				)
		End
		Else
		Begin
			-- // Open Tender Case
			SELECT TLS.lotNo as FieldValue1, 
				lotDesc as FieldValue2,
				CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
				'N.A.' as FieldValue4			
				FROM  dbo.tbl_TenderLotSecurity TLS 
				WHERE tenderId = @v_fieldName2Vc
		End
	End
	
	Else If (select docAvlMethod from tbl_TenderDetails where tenderId=@v_fieldName2Vc) = 'Lot'
	Begin
		-- // Lot case
	
		If (select docAccess from tbl_TenderDetails where tenderId=@v_fieldName2Vc) = 'Limited'
		Begin
			-- // Limited Tender Case
			SELECT TLS.lotNo as FieldValue1, 
				lotDesc as FieldValue2,
				CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
				'N.A.' as FieldValue4			
				FROM  dbo.tbl_TenderLotSecurity TLS 
				WHERE tenderId = @v_fieldName2Vc
				And appPkgLotId in
				(select pkgLotId from tbl_LimitedTenders where tenderId=@v_fieldName2Vc
					 and
					(
						userId in 								
						(
							select distinct userId from tbl_TendererMaster 
							where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
						)  	
						Or
						userId=@v_fieldName3Vc		
					)
				)
		End
		Else
		Begin
			-- // Open Tender Case
			SELECT TLS.lotNo as FieldValue1, 
				lotDesc as FieldValue2,
				CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
				'N.A.' as FieldValue4			
				FROM  dbo.tbl_TenderLotSecurity TLS 
				WHERE tenderId = @v_fieldName2Vc
		End
		
	End
	
END


IF @v_fieldName1Vc = 'getLotDescriptonForDocViewMore'     -- For
BEGIN

	Declare @v_isDocPaid int, @v_DocAccess varchar(50), @v_DocFeesMethod varchar(50)
	
	Select @v_isDocPaid = 
		Case docFeesMethod 
		When 'Package wise'	
		Then 
			Case 
				When (pkgDocFees > 0) 
				Then 1
				Else 0
			End
		When 'Lot wise'		
		Then 
			Case 
				When ((select Sum(docFess) from tbl_TenderLotSecurity Where tenderId=@v_fieldName2Vc) > 0)
				Then 1
				Else 0
			End
		Else 0		
		End,
		@v_DocAccess=docAccess,
		@v_DocFeesMethod=docFeesMethod
		From tbl_TenderDetails where tenderId=@v_fieldName2Vc
		
	--	select @v_isDocPaid as isDiocPaid, @v_DocAccess as DocAccess
		
	If @v_isDocPaid =1
	Begin
		-- // Documents are payable
		If @v_DocAccess='Limited'
		Begin
			-- // Limited Tender Case
			If @v_DocFeesMethod='Package wise'
			Begin
					SELECT TLS.lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
				IsNull(Convert(varchar(50),(select top 1 tenderPaymentId 
				from tbl_TenderPayment TP
				Where paymentFor='Document Fees' 		
				And TP.tenderId=TLS.tenderId		 			
				And isVerified='Yes' And isLive='Yes' 
				And 
					(
						TP.userId in 								
						(
							select distinct userId from tbl_TendererMaster 
								where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
						)  								
						Or 
						TP.userId = @v_fieldName3Vc
					)
				Order by tenderPaymentId desc)),'0') as FieldValue4	
				FROM  dbo.tbl_TenderLotSecurity TLS 
				WHERE tenderId = @v_fieldName2Vc
				And appPkgLotId in
				(
					select distinct pkgLotId from tbl_LimitedTenders
					where tenderId = @v_fieldName2Vc
					and userId in
						(
							select distinct userId from tbl_TendererMaster 
								where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
						)  								
						Or 
						userId = @v_fieldName3Vc
				)
			End
			Else If @v_DocFeesMethod='Lot wise'
			Begin
					SELECT TLS.lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
					IsNull(Convert(varchar(50),(select top 1 tenderPaymentId 
					from tbl_TenderPayment TP
					Where paymentFor='Document Fees' 		
					And TP.tenderId=TLS.tenderId		 
					And pkgLotId=TLS.appPkgLotId 
					And isVerified='Yes' And isLive='Yes' 
					And 
						(
							TP.userId in 								
							(
								select distinct userId from tbl_TendererMaster 
									where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
							)  								
							Or 
							TP.userId = @v_fieldName3Vc
						)
					Order by tenderPaymentId desc)),'0') as FieldValue4	
					FROM  dbo.tbl_TenderLotSecurity TLS 
					WHERE tenderId = @v_fieldName2Vc
					And appPkgLotId in
					(
						select distinct pkgLotId from tbl_LimitedTenders
						where tenderId = @v_fieldName2Vc
						and userId in
							(
								select distinct userId from tbl_TendererMaster 
									where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
							)  								
							Or 
							userId = @v_fieldName3Vc	
					)
			End
		End
		Else If @v_DocAccess='Open'
		Begin
			-- // Open Tender Case
			If @v_DocFeesMethod='Package wise'
			Begin
					SELECT TLS.lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
					IsNull(Convert(varchar(50),(select top 1 tenderPaymentId 
					from tbl_TenderPayment TP
					Where paymentFor='Document Fees' 		
					And TP.tenderId=TLS.tenderId		 			
					And isVerified='Yes' And isLive='Yes' 
					And 
						(
							TP.userId in 								
							(
								select distinct userId from tbl_TendererMaster 
									where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
							)  								
							Or 
							TP.userId = @v_fieldName3Vc
						)
					Order by tenderPaymentId desc)),'0') as FieldValue4	
					FROM  dbo.tbl_TenderLotSecurity TLS 
					WHERE tenderId = @v_fieldName2Vc
					End
					Else If @v_DocFeesMethod='Lot wise'
			Begin
						SELECT TLS.lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
					IsNull(Convert(varchar(50),(select top 1 tenderPaymentId 
					from tbl_TenderPayment TP
					Where paymentFor='Document Fees' 		
					And TP.tenderId=TLS.tenderId		 
					And pkgLotId=TLS.appPkgLotId 
					And isVerified='Yes' And isLive='Yes' 
					And 
						(
							TP.userId in 								
							(
								select distinct userId from tbl_TendererMaster 
									where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
							)  								
							Or 
							TP.userId = @v_fieldName3Vc
						)
					Order by tenderPaymentId desc)),'0') as FieldValue4	
					FROM  dbo.tbl_TenderLotSecurity TLS 
					WHERE tenderId = @v_fieldName2Vc
			End
			
		End
	End	
	Else If @v_isDocPaid =0
	Begin		
		-- // Documents are Free
		If @v_DocAccess='Limited'
		Begin
			If (select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc) = 'Services'
			Begin
				SELECT TLS.lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
					'N.A.' as FieldValue4			
					FROM  dbo.tbl_TenderLotSecurity TLS 
					WHERE tenderId = @v_fieldName2Vc
					And tenderId in
					(
						select distinct tenderId from tbl_LimitedTenders
						where tenderId = @v_fieldName2Vc
						and userId in
							(
								select distinct userId from tbl_TendererMaster 
									where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
							)  								
							Or 
							userId = @v_fieldName3Vc
					)
			End
			Else
			Begin
				SELECT TLS.lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
					'N.A.' as FieldValue4			
					FROM  dbo.tbl_TenderLotSecurity TLS 
					WHERE  tenderId = @v_fieldName2Vc and
					  appPkgLotId in
					(
						select distinct appPkgLotId from tbl_LimitedTenders
						where tenderId = @v_fieldName2Vc
						and userId in
							(
								select distinct userId from tbl_TendererMaster 
									where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
							)  								
							Or 
							userId = @v_fieldName3Vc
					)
			End
			
					
		End
		Else If @v_DocAccess='Open'
		Begin
			-- // Open Tender Case
			SELECT TLS.lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3,
			'N.A.' as FieldValue4			
			FROM  dbo.tbl_TenderLotSecurity TLS 
			WHERE tenderId = @v_fieldName2Vc	
		End
	End
		
END

IF @v_fieldName1Vc='getBasicInfoForViewPayment' -- partner/ForPayment.jsp
BEGIN
	/*
		FieldValue1=shows type of docFeesMehod and whether Document Fees is payable or free
		FieldValue1=shows type of Tender Security and whether it is payable or free
		FieldValue3=shows whether the tender(status) has been published or no
	*/
	
	Select 
		Case docFeesMethod 
			When 'Package wise' Then 'Package Wise' 
			When 'Lot wise' Then 'Lot wise'
			Else 'null'
		End as FieldValue1,
		Case When Exists (select top 1 tenderLotSecId from tbl_TenderLotSecurity Where tenderId=@v_fieldName2Vc And tenderSecurityAmt>0 )
			 Then 'paid'
			 Else 'free'
		End as FieldValue2,
		tenderStatus as FieldValue3, 
		docFeesMode as FieldValue4,
		Case docFeesMethod 
		When 'Package wise'	
		Then 
			Case 
				When (pkgDocFees > 0) 
				Then 'Paid'
				Else 'Free'
			End
		When 'Lot wise'
		Then 
			Case 
				When ((select Sum(docFess) from tbl_TenderLotSecurity Where tenderId=@v_fieldName2Vc) > 0)
				Then 'Paid'
				Else 'Free'
			End
		Else 'Free'		
		End as FieldValue5,
		Case When 
			Exists(
				select top 1 tenderPaymentId from tbl_TenderPayment where tenderId=@v_fieldName2Vc
				and paymentFor='Document Fees' and isVerified='yes' and isLive='yes'
				and
					(
						userId in 								
						(
							select distinct userId from tbl_TendererMaster 
							where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
						)  	
						Or
						userId=@v_fieldName3Vc		
					)
			)
			Then 'Yes'
			Else 'No'
			End as FieldValue6,
			
			Case When 
			Exists(
				select top 1 tenderPaymentId from tbl_TenderPayment where tenderId=@v_fieldName2Vc
				and paymentFor='Tender Security' and isVerified='yes' and isLive='yes'
				and
					(
						userId in 								
						(
							select distinct userId from tbl_TendererMaster 
							where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
						)  	
						Or
						userId=@v_fieldName3Vc		
					)
			)
			Then 'Yes'
			Else 'No'
			End as FieldValue7,
			
			Case When 
			Exists(
				select top 1 tenderPaymentId from tbl_TenderPayment where tenderId=@v_fieldName2Vc
				and paymentFor='Performance Security' and isVerified='yes' and isLive='yes'
				and
					(
						userId in 								
						(
							select distinct userId from tbl_TendererMaster 
							where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
						)  	
						Or
						userId=@v_fieldName3Vc		
					)
			)
			Then 'Yes'
			Else 'No'
			End as FieldValue8,
			IsNull((
				select top 1 Convert(varchar(50), tenderPaymentId) 
				from tbl_TenderPayment where tenderId=@v_fieldName2Vc And pkgLotId=0
				and paymentFor='Document Fees' and isVerified='yes' and isLive='yes'
				and
					(
						userId in 								
						(
							select distinct userId from tbl_TendererMaster 
							where companyId =(select companyId from tbl_TendererMaster where userId=@v_fieldName3Vc and companyId!=1)
						)  	
						Or
						userId=@v_fieldName3Vc		
					)
			),'0') as FieldValue9
			
		From tbl_TenderDetails 
		
		Where tenderId=@v_fieldName2Vc
END   


IF @v_fieldName1Vc='QuestionDocsMore'
BEGIN
Select documentName as FieldValue1, docDescription as FieldValue2, 
		docSize as FieldValue3,
		cast(prebidQueryDocId  as varchar(10)) as FieldValue4 
		from tbl_PreTenderQryDocs c inner join 
		tbl_PreTenderQuery p On c.queryId=p.queryId
		 Where p.queryId=@v_fieldName2Vc 
		 and docType!='Prebid'
END



IF @v_fieldName1Vc = 'getGrandSummaryLink'   
BEGIN
select Convert(varchar(50),COUNT(*)) as FieldValue1
from tbl_TenderGrandSumDetail tsg, tbl_TenderForms tf 
where tsg.tenderFormId = tf.tenderFormId
and tf.pkgLotId = @v_fieldName2Vc 
END

IF @v_fieldName1Vc = 'TOR_tenderer_condition'   
BEGIN
	--select convert(varchar(50),COUNT(c.committeeId)) as FieldValue1 from tbl_Committee c,tbl_CommitteeMembers cm 
	--where c.committeeId=cm.committeeId and c.tenderId=@v_fieldName2Vc and c.committeeType in ('TOC','POC')
	--and cm.userId not in(select userId from tbl_TORRptSign 
	--where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc and reportType='tor2')	  
	select  case when ((select COUNT(userId) from tbl_TORRptSign where  
	tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc  and reportType in ('tor1','tor2')
	)= (select count(userId)*2 from tbl_Committee c,tbl_CommitteeMembers cm
	where tenderId=@v_fieldName2Vc and committeeType in('TOC','POC') 
	and c.committeeId=cm.committeeId and committeStatus='approved')) then '1' else '0' end FieldValue1
END


IF @v_fieldName1Vc = 'getTenderStatus'
BEGIN
 Select tenderStatus as FieldValue1 from tbl_TenderDetails where tenderId=@v_fieldName2Vc
END


IF @v_fieldName1Vc = 'getBankInfoForTenPayment' 
BEGIN
	
	Select @v_BankUserType = 
	Case isMakerChecker When 'BranchMaker' Then 'maker' When 'BranchChecker' Then 'checker' End 
	from tbl_PartnerAdminTransfer PA Inner join tbl_ScBankDevPartnerMaster SBP
	On PA.sBankDevelopId=SBP.sBankDevelopId
	Where PA.partTransId=@v_fieldName2Vc

	If @v_fieldName3Vc is Not Null And @v_fieldName3Vc<>'' And @v_fieldName3Vc<>'0'
	Begin
		-- Edit Payment Case
		If (Select isMakerChecker from tbl_PartnerAdminTransfer Where partTransId=@v_fieldName2Vc)='BranchChecker'		
		Begin
			-- // If Current User is Branch Checker i.e.: Edit Payment by Checker Case
			-- Select the partTransId of the Bank Maker who has done the Payment
			Select @v_fieldName2Vc= partTransId 
			from tbl_TenderPayment where tenderPaymentId=@v_fieldName3Vc
		End
	End

	Select top 1 @v_BankDevelHeadId_Int = SBP.sBankDevelHeadId 
	from tbl_PartnerAdminTransfer PA Inner join tbl_ScBankDevPartnerMaster SBP
	On PA.sBankDevelopId=SBP.sBankDevelopId
	Where PA.partTransId=@v_fieldName2Vc

		If @v_BankDevelHeadId_Int=0
		Begin
			Select top 1 @v_BankName = sbDevelopName from tbl_PartnerAdminTransfer PA Inner join tbl_ScBankDevPartnerMaster SBP
			On PA.sBankDevelopId=SBP.sBankDevelopId
			Where PA.partTransId=@v_fieldName2Vc
		End
		Else
		Begin
			Select top 1 @v_BankName = sbDevelopName from tbl_ScBankDevPartnerMaster 
			Where sBankDevelopId=(
				Select top 1 SBP.sBankDevelHeadId from tbl_PartnerAdminTransfer PA Inner join tbl_ScBankDevPartnerMaster SBP
					On PA.sBankDevelopId=SBP.sBankDevelopId
				Where PA.partTransId=@v_fieldName2Vc)
		End

	Select top 1 @v_BankName as FieldValue1, 
	sbDevelopName as FieldValue2, 
	PA.fullName as FieldValue3,
	@v_BankUserType as FieldValue4,
	Convert(varchar(50),PA.userId) as FieldValue5,
	Convert(varchar(50),PA.partTransId) as FieldValue6
	From tbl_PartnerAdminTransfer PA Inner join tbl_ScBankDevPartnerMaster SBP
		On PA.sBankDevelopId=SBP.sBankDevelopId
		Where PA.partTransId=@v_fieldName2Vc
	
END


IF @v_fieldName1Vc = 'getBankInfoForRegPayment' 
BEGIN

	Select @v_BankUserType = 
	Case isMakerChecker When 'BranchMaker' Then 'maker' When 'BranchChecker' Then 'checker' End 
	from tbl_PartnerAdminTransfer PA Inner join tbl_ScBankDevPartnerMaster SBP
	On PA.sBankDevelopId=SBP.sBankDevelopId
	Where PA.partTransId=@v_fieldName2Vc

	If @v_fieldName3Vc is Not Null And @v_fieldName3Vc<>'' And @v_fieldName3Vc<>'0'
	Begin
		-- Edit Payment Case
		If (Select isMakerChecker from tbl_PartnerAdminTransfer Where partTransId=@v_fieldName2Vc)='BranchChecker'		
		Begin
			-- // If Current User is Branch Checker i.e.: Edit Payment by Checker Case
			-- Select the partTransId of the Bank Maker who has done the Payment
			Select @v_fieldName2Vc= partTransId 
			from tbl_RegFeePayment  where regPaymentId=@v_fieldName3Vc
		End
	End

	Select top 1 @v_BankDevelHeadId_Int = SBP.sBankDevelHeadId 
	from tbl_PartnerAdminTransfer PA Inner join tbl_ScBankDevPartnerMaster SBP
	On PA.sBankDevelopId=SBP.sBankDevelopId
	Where PA.partTransId=@v_fieldName2Vc

		If @v_BankDevelHeadId_Int=0
		Begin
			Select top 1 @v_BankName = sbDevelopName from tbl_PartnerAdminTransfer PA Inner join tbl_ScBankDevPartnerMaster SBP
			On PA.sBankDevelopId=SBP.sBankDevelopId
			Where PA.partTransId=@v_fieldName2Vc
		End
		Else
		Begin
			Select top 1 @v_BankName = sbDevelopName from tbl_ScBankDevPartnerMaster 
			Where sBankDevelopId=(
				Select top 1 SBP.sBankDevelHeadId from tbl_PartnerAdminTransfer PA Inner join tbl_ScBankDevPartnerMaster SBP
					On PA.sBankDevelopId=SBP.sBankDevelopId
				Where PA.partTransId=@v_fieldName2Vc)
		End

	Select top 1 @v_BankName as FieldValue1, 
	sbDevelopName as FieldValue2, 
	PA.fullName as FieldValue3,
	@v_BankUserType as FieldValue4,
	Convert(varchar(50),PA.userId) as FieldValue5,
	Convert(varchar(50),PA.partTransId) as FieldValue6
	From tbl_PartnerAdminTransfer PA Inner join tbl_ScBankDevPartnerMaster SBP
		On PA.sBankDevelopId=SBP.sBankDevelopId
		Where PA.partTransId=@v_fieldName2Vc
	
END

IF @v_fieldName1Vc = 'getTenderExtComments' 
BEGIN
	Select remarks as FieldValue1,hopeComments as FieldValue2,	
	cast(convert(varchar, finalOpenDt, 103) as varchar(10)) +' ' +Substring(CONVERT(VARCHAR(20),finalOpenDt,108),1,5) as FieldValue3
	from tbl_TenderOpenExt where tenderId=@v_fieldName2Vc And ExtDtId=@v_fieldName3Vc
END

IF @v_fieldName1Vc = 'getTORReceivedData' 
BEGIN

Select 

Case When ((select cm.userId from tbl_CommitteeMembers cm 
				inner join tbl_Committee c On cm.committeeId=c.committeeId		
				where c.tenderId=@v_fieldName2Vc and c.committeeType IN ('TEC','PEC') 
				and c.committeStatus='approved' and memberRole='cp'
			 ) = @v_fieldName3Vc)
			 Then 'Yes'
			 Else 'No'
			 
			 End as FieldValue1,

Case When Exists (Select tenderId from tbl_TOSRptShare where tenderId=@v_fieldName2Vc And listingId=(Select listingId from tbl_TOSListing where tenderId=@v_fieldName2Vc)
		) Then 'Yes' Else 'No' End as FieldValue2,

Case When Exists (select tenderId from tbl_TORRptSign where reportType = 'TER3' and tenderId = @v_fieldName2Vc)
		Then 'Yes' Else 'No' End as FieldValue3,

Case When Exists (select tenderId from tbl_TenderDetails where tenderId = @v_fieldName2Vc and domesticPref = 'Yes')
		Then 'Yes' Else 'No' End as FieldValue4
		
END		

IF @v_fieldName1Vc = 'getDomesticTenderersList' 
BEGIN
select CONVERT(varchar(50), f.userId) as FieldValue1,companyName as FieldValue2 
from tbl_TendererMaster tm,tbl_CompanyMaster cm,tbl_FinalSubmission f
where tm.companyId=cm.companyId and tm.companyId !=1 
and regOffCountry='Bangladesh' and bidSubStatus='finalsubmission'
and f.userId=tm.userId and tenderId=@v_fieldName2Vc
Order by companyName

END

IF @v_fieldName1Vc = 'getBidWithdrawalModificationList'  
BEGIN

select distinct Convert(varchar(50), bidModId) as FieldValue1, 
	Convert(varchar(50), finalSubmissionId) as FieldValue2 ,
	Convert(varchar(50),  fs.userId) as FieldValue3, 
	dbo.f_getbiddercompany(fs.userId) as FieldValue4,
	REPLACE(CONVERT(VARCHAR(11), bm.bidModDt, 106), ' ', '-') + ' ' + convert(varchar(5), bm.bidModDt, 108) as FieldValue5,
	--cast(convert(varchar, bm.bidModDt, 106) as varchar(10)) +' ' +Substring(CONVERT(VARCHAR(20),bm.bidModDt,108),1,5) as FieldValue5,
	bm.comments as FieldValue6,
	bm.bidModDt
from tbl_FinalSubmission fs
inner join tbl_BidModification bm On fs.tenderId=bm.tenderId
where  
fs.tenderId=@v_fieldName2Vc
and fs.userId = bm.userId
and bm.bidStatus=@v_fieldName3Vc
--and fs.bidSubStatus=@v_fieldName3Vc
Order by dbo.f_getbiddercompany(fs.userId) 

END
 

IF @v_fieldName1Vc = 'getTenderOpenStatus' 
BEGIN

Select Case When GETDATE()>openingDt Then 'Yes' Else 'No' End as FieldValue1
from tbl_TenderOpenDates Where tenderId=  @v_fieldName2Vc

END

IF @v_fieldName1Vc = 'getTenderBidExistsStatus' 
BEGIN

select Case When Count(f.finalSubmissionId) > 0 Then 'Yes' Else 'No' End as FieldValue1
from tbl_FinalSubmission f
where bidSubStatus='finalsubmission'
And tenderId=@v_fieldName2Vc


END
if @v_fieldName1Vc = 'getTenderHope' 
BEGIN
		select emailid as FieldValue1, employeeName as FieldValue2 from tbl_EmployeeMaster e,tbl_loginmaster l 
			where l.userid=e.userid and employeeId in(select employeeId from tbl_EmployeeRoles where departmentId= 
			(select departmentId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
			and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='HOPE'))
	END
	
IF @v_fieldName1Vc = 'getTenderTSCCommInfo' -- for/officer/LotPackageDetail.jsp
BEGIN
	Select 
	CONVERT(varchar(50), cm.userId) as FieldValue1, 
	dbo.f_gettscmembername(cm.userId, cm.govUserId) as FieldValue2,
	case cm.memberRole when 'm' then 'Member' when 'cp' then 'Chairperson' else 'Member Secretary' end as FieldValue3  ,
	dbo.f_initcap(case when cm.appStatus = 'approved' then 'Declaration given' else cm.appStatus end) as FieldValue4,
	Case cm.appdate When '1900-01-01 00:00:00' Then '-'
	 Else REPLACE(CONVERT(VARCHAR(11), cm.appdate, 106), ' ', '-') + ' ' + convert(varchar(5), cm.appdate, 108) End as FieldValue5, 	
	convert(varchar(50), c.committeeId)  as FieldValue6		
	from 
	tbl_CommitteeMembers cm
	inner join tbl_Committee c on cm.committeeId=c.committeeId
	inner join tbl_CommitteeRoleMaster crm On cm.memberRole=crm.comRole

	where c.tenderId=@v_fieldName2Vc
	and c.committeeType='TSC'
	and c.committeStatus='approved'
	order by crm.comRoleId

END	
		
	
	
IF @v_fieldName1Vc='getTenderTSCMemStatus'
BEGIN
	Select 
	dbo.f_initcap(case when cm.appStatus = 'approved' then 'yes' else 'no' end) as FieldValue1			
	from 
	tbl_CommitteeMembers cm
	inner join tbl_Committee c on cm.committeeId=c.committeeId
	inner join tbl_CommitteeRoleMaster crm On cm.memberRole=crm.comRole

	where c.tenderId=@v_fieldName2Vc And cm.userId=@v_fieldName3Vc 
	and c.committeeType='TSC'
	and c.committeStatus='approved'
	order by crm.comRoleId

END


IF @v_fieldName1Vc = 'TenderExtRoleName'
BEGIN

Select @v_roleNameVc= @v_fieldName3Vc

If @v_roleNameVc = 'PD'
	BEGIN
		
		SELECT @v_appIdInt = appid FROM tbl_TenderMaster WHERE tenderId = @v_fieldName2Vc		
		SELECT @v_projectIdInt = projectId FROM tbl_AppMaster WHERE appId = @v_appIdInt
	
	Select cast(userid as varchar(10)) as FieldValue1, employeeName as FieldValue2 from tbl_EmployeeMaster where userId in
	(SELECT userid FROM tbl_ProjectRoles where ProjectId = @v_projectIdInt and procurementRoleId = @v_roleIdInt)

	END
	
	IF @v_roleNameVc = 'PE'
	BEGIN
		--select cast(employeeId as varchar(10)) as FieldValue1,employeeName as FieldValue2 from tbl_EmployeeMaster 
		--	where employeeId in(select employeeId from tbl_EmployeeRoles where departmentId= 
		--	(select departmentId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
		--	and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='PE'))
	select cast(e.govUserId as varchar(10)) as FieldValue3,cast(e.userid as varchar(10)) as FieldValue1,e.employeeName as FieldValue2 from	tbl_tendermaster  t,tbl_AppMaster a,tbl_EmployeeTrasfer e 
where t.appId=a.appId and  tenderid=@v_fieldName2Vc 
and a.employeeid=e.employeeId and isCurrent='Yes' ;		
	END
	
	IF @v_roleNameVc = 'HOPE'
	BEGIN
		select cast(govUserId as varchar(10)) as FieldValue3,cast(userid as varchar(10)) as FieldValue1, employeeName as FieldValue2 from tbl_EmployeeTrasfer
			where isCurrent='yes' and employeeId in(select employeeId from tbl_EmployeeRoles where departmentId= 
			(select departmentId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
			and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='HOPE'))
	END
	
	IF @v_roleNameVc = 'Authorized Officer'
	BEGIN
		select cast(userid as varchar(10)) as FieldValue1, employeeName as FieldValue2 from tbl_EmployeeMaster 
			where employeeId in(select employeeId from tbl_EmployeeRoles where departmentId= 
			(select departmentId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
			and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='Authorized Officer'))
	END
	
	IF @v_roleNameVc = 'BOD'
	BEGIN
	print 'sf'
		select cast(govUserId as varchar(10)) as FieldValue3,cast(userid as varchar(10))   as FieldValue1, employeeName as FieldValue2 from tbl_EmployeeTrasfer 
			where isCurrent='yes' and employeeId in(select employeeId from tbl_EmployeeRoles where departmentId= 
			(select departmentId from tbl_TenderDetails where tenderId=@v_fieldName2Vc)
			and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='BOD'))
	END
	
	IF @v_roleNameVc = 'Secretary'
	BEGIN
		SELECT @v_departmentid_Vc=departmentId FROM tbl_TenderDetails WHERE tenderid=@v_fieldName2Vc
		set @v_departmentidCounter_Vc=@v_departmentid_Vc
		set @v_tempCounter_Vc=''
		
		If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Organization'
			Begin
				SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)
				SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
			End		
		
		if @v_tempCounter_Vc=''
			BEGIN
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
					END
			END
		ELSE
			BEGIN
		
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_tempCounter_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)) as varchar(10))
					END
			END
		
		
		SELECT @v_employeeIdlist_Vc = COALESCE(@v_employeeIdlist_Vc+',' , '') + cast(employeeId as varchar(10))
		FROM tbl_EmployeeRoles WHERE departmentId in (select dbo.f_trim(items) from dbo.f_split(@v_departmentidCounter_Vc,',')) 
		and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='Secretary')
		
		select cast(govUserId as varchar(10)) as FieldValue3,cast(userid as varchar(10))  as FieldValue1, employeename as FieldValue2 from tbl_EmployeeTrasfer where isCurrent='yes' and employeeId in (select dbo.f_trim(items) from dbo.f_split(@v_employeeIdlist_Vc,','))
	END


IF @v_roleNameVc = 'Minister'
	BEGIN
		SELECT @v_departmentid_Vc=departmentId FROM tbl_TenderDetails WHERE tenderid=@v_fieldName2Vc
		set @v_departmentidCounter_Vc=@v_departmentid_Vc
		set @v_tempCounter_Vc=''
		
		If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Organization'
			Begin
				SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)
				SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
			End		
		
		if @v_tempCounter_Vc=''
			BEGIN
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =@v_departmentid_Vc) as varchar(10))
					END
			END
		ELSE
			BEGIN
		
				If (select departmentType from tbl_DepartmentMaster where departmentId=@v_tempCounter_Vc)='Division'
					BEGIN
						SET @v_tempCounter_Vc=(select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc))
						SET @v_departmentidCounter_Vc=@v_departmentidCounter_Vc+','+cast((select parentDepartmentId from tbl_DepartmentMaster where departmentId =(select parentDepartmentId from tbl_DepartmentMaster where departmentId=@v_departmentid_Vc)) as varchar(10))
					END
			END
		
		
		SELECT @v_employeeIdlist_Vc = COALESCE(@v_employeeIdlist_Vc+',' , '') + cast(employeeId as varchar(10))
		FROM tbl_EmployeeRoles WHERE departmentId in (select dbo.f_trim(items) from dbo.f_split(@v_departmentidCounter_Vc,',')) 
		and procurementRoleId=(select procurementRoleId from tbl_ProcurementRole where procurementRole='Minister')
		
		select cast(govUserId as varchar(10))  as FieldValue3,cast(userid as varchar(10))  as FieldValue1, employeename as FieldValue2 from tbl_EmployeeTrasfer where isCurrent='yes' and employeeId in (select dbo.f_trim(items) from dbo.f_split(@v_employeeIdlist_Vc,','))
	END


IF @v_roleNameVc = 'CCGP'
	BEGIN	
		select cast(userid as varchar(10)) as FieldValue1,employeename as FieldValue2 from tbl_EmployeeMaster where employeeId in (select employeeId from tbl_EmployeeRoles where procurementRoleId in (select procurementRoleId from tbl_ProcurementRole where procurementRole = 'CCGP'))
	END
	
	
IF @v_roleNameVc = 'Development Partner' or @v_roleNameVc = 'DP'
	BEGIN
	--print 'in DP'
		
	SELECT @v_appIdInt = appid FROM tbl_TenderMaster WHERE tenderId = @v_fieldName2Vc		
	SELECT @v_projectIdInt = projectId FROM tbl_AppMaster WHERE appId = @v_fieldName2Vc

	Select fullName as FieldValue1, designation as FieldValue2 from tbl_PartnerAdmin 
	where  sBankDevelopId in 
	( select sBankDevelopId from tbl_ScBankDevPartnerMaster where sBankDevelHeadId in
		(Select sBankDevelopId from tbl_projectpartners where projectId =@v_projectIdInt))
	
	END

END

IF @v_fieldName1Vc = 'GetLotPkgDetail'
BEGIN
	IF @v_fieldName3Vc='0'
		BEGIN
			select packageNo as FieldValue1 ,packageDescription as FieldValue2,reoiRfpRefNo as FieldValue3 from tbl_TenderDetails where tenderId=@v_fieldName2Vc
		END
	ELSE
		BEGIN
			select lotNo as FieldValue1,lotDesc as FieldValue2,td.reoiRfpRefNo as FieldValue3 from tbl_TenderDetails td inner join tbl_TenderLotSecurity ls 
			on td.tenderId = ls.tenderId
			where td.tenderId=@v_fieldName2Vc and ls.appPkgLotId=@v_fieldName3Vc
		END
END

IF @v_fieldName1Vc = 'GetEmailGiveClari'
BEGIN
	select emailId as FieldValue1 from tbl_LoginMaster 
	where userId=(select userId from tbl_EvalRptSentToAA where evalRptToAAId=@v_fieldName2Vc)
END


IF @v_fieldName1Vc = 'getFinalResponseEditstatus'
BEGIN
	select Convert(varchar(50),td.tenderId) as FieldValue1, 
	td.docAvlMethod as FieldValue2,
Case td.docAvlMethod 
	When 'Lot'
	Then 
		Case 
			When (Select Count(SignID) as SignCnt from 
					(select distinct tenderId, appPkgLotId,
						(select top 1 torSignId from tbl_TORRptSign where tenderId=a.tenderId and pkgLotId=a.appPkgLotId and reportType='ter2' ) as  SignID
							from tbl_TenderLotSecurity a
							where tenderId=td.tenderId and appPkgLotId=@v_fieldName3Vc and tenderId=@v_fieldName2Vc) as F 
				)
				
				=
				
				(				
					Select Count(appPkgLotId) from tbl_TenderLotSecurity where tenderId=td.tenderId  and appPkgLotId=@v_fieldName3Vc and tenderId=@v_fieldName2Vc
				)	
			Then 'Block'
			Else 'Allow'
		End
	When 'Package'
	Then 
		Case 
			When 	
			Exists (Select top 1 torSignId from tbl_TORRptSign Where tenderId=td.tenderId And reportType='ter2')
			Then 'Block'
			Else 'Allow'
		End	
End
as FieldValue3,
Case td.docAvlMethod 
	When 'Lot'
	Then 
		Case 
			When (@v_fieldName3Vc IS not null And @v_fieldName3Vc<>'0')	
			Then 
				Case 
					When Exists (Select top 1 torSignId from tbl_TORRptSign Where tenderId=td.tenderId and pkgLotId=@v_fieldName3Vc and tenderId = @v_fieldName2Vc And reportType='ter2')
					Then 'Block'
					Else 'Allow'
				End
			Else 'N.A.'	
		End
	Else 'N.A.'					
End	as FieldValue4

from tbl_TenderDetails td 
inner join tbl_TORRptSign rpt on td.tenderId=rpt.tenderId
where td.tenderId=@v_fieldName2Vc 
and rpt.reportType='ter2'

END
IF @v_fieldName1Vc='getBidderEvaluatedCount'
BEGIN
	--SELECT CONVERT(VARCHAR(10),tenderid) as FieldValue1,
	--CONVERT(VARCHAR(10),l.userid) as FieldValue2, 
	--CASE WHEN t.companyId=1 THEN firstName+' '+lastname ELSE companyName end  as FieldValue3, 
	--'-1' as FieldValue4
	--FROM dbo.tbl_loginmaster l,tbl_companymaster c,tbl_TendererMaster t,tbl_FinalSubmission f
	--WHERE l.userId=f.userId and c.companyId=t.companyId
	--and t.userid=l.userid and f.tenderId=@v_fieldName2Vc
	--and bidSubStatus='finalsubmission'
	
	
	select distinct Convert(varchar(50), l.userId) as FieldValue1,Convert(varchar(50), pkgLotId) as FieldValue2 
	FROM dbo.tbl_loginmaster l,tbl_companymaster c,tbl_TendererMaster t,tbl_FinalSubmission f,tbl_BidderLots bl
	WHERE l.userId=f.userId and c.companyId=t.companyId
	and t.userid=l.userid and f.tenderId=@v_fieldName2Vc and 
	bl.userId=f.userId and bl.tenderId = f.tenderId
	and bidSubStatus='finalsubmission'
	
	--SELECT CONVERT(VARCHAR(10),f.tenderid) as FieldValue1,
	--CONVERT(VARCHAR(10),l.userid) as FieldValue2, 
	--CASE WHEN t.companyId=1 THEN firstName+' '+lastname ELSE companyName end  as FieldValue3, 
	--'-1' as FieldValue4,
	--Case 
	--	When (select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='Services'
	--	Then 
	--		Case When (select COUNT(esFormDetailId) from tbl_EvalSerFormDetail where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) > 0
	--		Then 'yes'
	--		Else 'No'
	--		End 
	--	Else 	
	--		Case When (select COUNT(evalMemStatusId) from tbl_EvalMemStatus where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) > 0
	--		Then 'yes'
	--		Else 'No'
	--		End 
	--	End
	--as 	FieldValue5,
	--Case 
	--	When (select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='Services'
	--	Then 
	--		Case When 
	--			(select COUNT(esFormDetailId) from tbl_EvalSerFormDetail where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) 
	--			= 
	--			(select COUNT(distinct tbf.tenderFormId) 
	--				from tbl_TenderBidForm tbf 
	--				inner join tbl_TenderForms tf On tbf.tenderFormId=tf.tenderFormId
	--				where tf.isPriceBid='No' and  userId=l.userId And tenderId=@v_fieldName2Vc)
	--		Then 'yes'
	--		Else 'No'
	--		End 
	--	Else 	
	--		Case When 
	--			(select COUNT(evalMemStatusId) from tbl_EvalMemStatus where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) 
	--			= 
	--			(select COUNT(distinct tbf.tenderFormId) 
	--				from tbl_TenderBidForm tbf 
	--				inner join tbl_TenderForms tf On tbf.tenderFormId=tf.tenderFormId
	--				where tf.isPriceBid='No' and  userId=l.userId And tenderId=@v_fieldName2Vc)
	--		Then 'yes'
	--		Else 'No'
	--		End 
	--	End
	--as 	FieldValue6,CONVERT(varchar(50),t.tendererId) as FieldValue7,CONVERT(varchar(50),t.companyId) as FieldValue8
	--FROM dbo.tbl_loginmaster l,tbl_companymaster c,tbl_TendererMaster t,tbl_FinalSubmission f,tbl_BidderLots bl
	--WHERE l.userId=f.userId and c.companyId=t.companyId
	--and t.userid=l.userid and f.tenderId=@v_fieldName2Vc and 
	--bl.userId=f.userId and bl.tenderId = f.tenderId
	--and bidSubStatus='finalsubmission'
	
END
IF @v_fieldName1Vc = 'getProcurementNatureForEval'     -- For LotTendPrep.jsp
BEGIN

	

	SELECT @v_docAvlMethod_Vc = procurementNature FROM dbo.tbl_TenderDetails WHERE tenderId = @v_fieldName2Vc

	IF @v_docAvlMethod_Vc = 'Services'
	BEGIN
	SELECT 'Package' as FieldValue1 	
	END
	ELSE
	BEGIN
		SELECT lotNo as FieldValue1, lotDesc as FieldValue2, CONVERT(VARCHAR(50), appPkgLotId) as FieldValue3 
		FROM  dbo.tbl_TenderLotSecurity 
		WHERE tenderId = @v_fieldName2Vc  
		
		
	END

END
IF @v_fieldName1Vc = 'getFinalSubCompanyLotWise' 
BEGIN
if ((select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc) = 'Services') 
	begin
		SELECT CONVERT(VARCHAR(10),tenderid) as FieldValue1,
	CONVERT(VARCHAR(10),l.userid) as FieldValue2, 
	CASE WHEN t.companyId=1 THEN firstName+' '+lastname ELSE companyName end  as FieldValue3, 
	'-1' as FieldValue4,
	Case 
		When (select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='Services'
		Then 
			Case When (select COUNT(esFormDetailId) from tbl_EvalSerFormDetail where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) > 0
			Then 'yes'
			Else 'No'
			End 
		Else 	
			Case When (select COUNT(evalMemStatusId) from tbl_EvalMemStatus where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) > 0
			Then 'yes'
			Else 'No'
			End 
		End
	as 	FieldValue5,
	Case 
		When (select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='Services'
		Then 
			Case When 
				(select COUNT(esFormDetailId) from tbl_EvalSerFormDetail where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) 
				= 
				(select COUNT(distinct tbf.tenderFormId) 
					from tbl_TenderBidForm tbf 
					inner join tbl_TenderForms tf On tbf.tenderFormId=tf.tenderFormId
					where tf.isPriceBid='No' and  userId=l.userId And tenderId=@v_fieldName2Vc)
			Then 'yes'
			Else 'No'
			End 
		Else 	
			Case When 
				(select COUNT(evalMemStatusId) from tbl_EvalMemStatus where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) 
				= 
				(select COUNT(distinct tbf.tenderFormId) 
					from tbl_TenderBidForm tbf 
					inner join tbl_TenderForms tf On tbf.tenderFormId=tf.tenderFormId
					where tf.isPriceBid='No' and  userId=l.userId And tenderId=@v_fieldName2Vc)
			Then 'yes'
			Else 'No'
			End 
		End
	as 	FieldValue6,CONVERT(varchar(50),t.tendererId) as FieldValue7,CONVERT(varchar(50),t.companyId) as FieldValue8
	FROM dbo.tbl_loginmaster l,tbl_companymaster c,tbl_TendererMaster t,tbl_FinalSubmission f
	WHERE l.userId=f.userId and c.companyId=t.companyId
	and t.userid=l.userid and f.tenderId=@v_fieldName2Vc
	and bidSubStatus='finalsubmission' order by FieldValue3
	end
	else
	begin

if @v_fieldName3Vc !=0
begin
SELECT  distinct CONVERT(VARCHAR(10),f.tenderid) as FieldValue1,
	CONVERT(VARCHAR(10),l.userid) as FieldValue2, 
	CASE WHEN t.companyId=1 THEN firstName+' '+lastname ELSE companyName end  as FieldValue3, 
	'-1' as FieldValue4,
	Case 
		When (select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='Services'
		Then 
			Case When (select COUNT(esFormDetailId) from tbl_EvalSerFormDetail where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) > 0
			Then 'yes'
			Else 'No'
			End 
		Else 	
			Case When (select COUNT(evalMemStatusId) from tbl_EvalMemStatus where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) > 0
			Then 'yes'
			Else 'No'
			End 
		End
	as 	FieldValue5,
	Case 
		When (select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='Services'
		Then 
			Case When 
				(select COUNT(esFormDetailId) from tbl_EvalSerFormDetail where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) 
				= 
				(select COUNT(distinct tbf.tenderFormId) 
					from tbl_TenderBidForm tbf 
					inner join tbl_TenderForms tf On tbf.tenderFormId=tf.tenderFormId
					where tf.isPriceBid='No' and  userId=l.userId And tenderId=@v_fieldName2Vc)
			Then 'yes'
			Else 'No'
			End 
		Else 	
			Case When 
				(select COUNT(evalMemStatusId) from tbl_EvalMemStatus where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) 
				= 
				(select COUNT(distinct tbf.tenderFormId) 
					from tbl_TenderBidForm tbf 
					inner join tbl_TenderForms tf On tbf.tenderFormId=tf.tenderFormId
					where tf.isPriceBid='No' and  userId=l.userId And tenderId=@v_fieldName2Vc)
			Then 'yes'
			Else 'No'
			End 
		End
	as 	FieldValue6,CONVERT(varchar(50),t.tendererId) as FieldValue7,CONVERT(varchar(50),t.companyId) as FieldValue8
	FROM dbo.tbl_loginmaster l,tbl_companymaster c,tbl_TendererMaster t,tbl_FinalSubmission f,tbl_BidderLots bl
	WHERE l.userId=f.userId and c.companyId=t.companyId
	and t.userid=l.userid and f.tenderId=@v_fieldName2Vc
	and bl.userId = f.userId and bl.tenderId = f.tenderId and bl.pkgLotId = @v_fieldName3Vc
	and bidSubStatus='finalsubmission' order by FieldValue3
end
else
begin 
SELECT  distinct CONVERT(VARCHAR(10),f.tenderid) as FieldValue1,
	CONVERT(VARCHAR(10),l.userid) as FieldValue2, 
	CASE WHEN t.companyId=1 THEN firstName+' '+lastname ELSE companyName end  as FieldValue3, 
	'-1' as FieldValue4,
	Case 
		When (select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='Services'
		Then 
			Case When (select COUNT(esFormDetailId) from tbl_EvalSerFormDetail where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) > 0
			Then 'yes'
			Else 'No'
			End 
		Else 	
			Case When (select COUNT(evalMemStatusId) from tbl_EvalMemStatus where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) > 0
			Then 'yes'
			Else 'No'
			End 
		End
	as 	FieldValue5,
	Case 
		When (select procurementNature from tbl_TenderDetails where tenderId=@v_fieldName2Vc)='Services'
		Then 
			Case When 
				(select COUNT(esFormDetailId) from tbl_EvalSerFormDetail where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) 
				= 
				(select COUNT(distinct tbf.tenderFormId) 
					from tbl_TenderBidForm tbf 
					inner join tbl_TenderForms tf On tbf.tenderFormId=tf.tenderFormId
					where tf.isPriceBid='No' and  userId=l.userId And tenderId=@v_fieldName2Vc)
			Then 'yes'
			Else 'No'
			End 
		Else 	
			Case When 
				(select COUNT(evalMemStatusId) from tbl_EvalMemStatus where tenderId=@v_fieldName2Vc and userId=l.userId and evalBy=@v_fieldName3Vc) 
				= 
				(select COUNT(distinct tbf.tenderFormId) 
					from tbl_TenderBidForm tbf 
					inner join tbl_TenderForms tf On tbf.tenderFormId=tf.tenderFormId
					where tf.isPriceBid='No' and  userId=l.userId And tenderId=@v_fieldName2Vc)
			Then 'yes'
			Else 'No'
			End 
		End
	as 	FieldValue6,CONVERT(varchar(50),t.tendererId) as FieldValue7,CONVERT(varchar(50),t.companyId) as FieldValue8
	FROM dbo.tbl_loginmaster l,tbl_companymaster c,tbl_TendererMaster t,tbl_FinalSubmission f,tbl_BidderLots bl
	WHERE l.userId=f.userId and c.companyId=t.companyId
	and t.userid=l.userid and f.tenderId=@v_fieldName2Vc
	and bl.userId = f.userId and bl.tenderId = f.tenderId  
	and bidSubStatus='finalsubmission' order by FieldValue3

end
	end
End

IF @v_fieldName1Vc = 'getEvalCommPubDate'
Begin
	select ISNULL(REPLACE(CONVERT(VARCHAR(11), publishDate, 106), ' ', '-') + ' ' +SUBSTRING(CONVERT(VARCHAR(5), publishDate,108), 1, 5), 'N.A.') as FieldValue1 from tbl_Committee where tenderId = @v_fieldName2Vc  and committeeType in ('TEC','PEC') and committeStatus='approved'
END

if @v_fieldName1Vc='CheckTenderUserRights'
begin
select CONVERT(varchar(20),em.userid) as FieldValue1 from tbl_TenderDetails t,tbl_EmployeeOffices e,tbl_EmployeeRoles er,tbl_EmployeeMaster em
where t.officeId=e.officeId and e.employeeId=er.employeeId
and er.procurementRoleId in(1,5) and tenderId=@v_fieldName3Vc
and em.employeeId=er.employeeId
and userId=@v_fieldName2Vc
end

if @v_fieldName1Vc='getCellForGrandTotalCaption'
begin
select CONVERT(varchar(50),t.tenderTableId) as FieldValue1,CONVERT(varchar(50),MAX(cellId)) as FieldValue2 from tbl_TenderFormula t,tbl_TenderCells c 
where formula like '%TOTAL%' and tenderFormId=@v_fieldName2Vc and t.tenderTableId = @v_fieldName3Vc
and t.tenderTableId=c.tenderTableId
and t.columnId-1=c.columnId group by t.tenderTableId
end
IF @v_fieldName1Vc = 'GetEvaluatedBiddersRoundWise'
BEGIN
if(select COUNT(tenderId) from tbl_TenderDetails where procurementNatureId = 3 and tenderId = @v_fieldName2Vc)!=0
begin 
print '3'
	SELECT  dbo.f_getbiddercompany(t.userId) as FieldValue1,
	convert(varchar(20),t.userid) as FieldValue2,
	convert(varchar(20),t.roundId) as FieldValue3, convert(varchar(20),t.amount) as FieldValue4,
	convert(varchar(20),t.rank)  as FieldValue5
	  FROM  tbl_BidderRank t,tbl_ReportMaster rm ,tbl_evalroundmaster erm
	 where t.tenderId=@v_fieldName2Vc  
	 and t.roundId=erm.roundid and t.userid=erm.userId
	and t.reportId = rm.reportId and rm.isTORTER='TER' 
	and t.userId not in (select userid from tbl_NoaIssueDetails where tenderId=@v_fieldName2Vc)
	and t.roundId in 
	(select max(erm.roundId) from tbl_EvalRoundMaster erm
	where erm.tenderId=@v_fieldName2Vc and erm.reportType='L1')
	and t.roundId in(select roundId from tbl_EvalRptSentToAA where rptStatus = 'Approved' and roundId in(
	select max(erm.roundId) from tbl_EvalRoundMaster erm
	where erm.tenderId=@v_fieldName2Vc and erm.reportType='L1'))
end
else
begin
	if(select COUNT(tenderId) from tbl_TenderDetails where procurementMethodId in(1,14) and tenderId = @v_fieldName2Vc)!=0
begin 
print '1'
	SELECT  case cm.CompanyName when '-' then firstName+' '+lastName else cm.companyName end as FieldValue1,
	convert(varchar(20),tm.userid) as FieldValue2,convert(varchar(20),t.roundId) as FieldValue3, convert(varchar(20),t.amount) as FieldValue4,
	convert(varchar(20),t.rank)  as FieldValue5
	  FROM  [tbl_BidderRank] t,tbl_TendererMaster tm,tbl_companymaster cm,tbl_ReportMaster rm,tbl_evalroundmaster erm
	 where t.userId=tm.userId and t.tenderId=@v_fieldName2Vc  and t.pkgLotId = @v_fieldName3Vc
	and tm.companyId=cm.companyId and t.reportId = rm.reportId and rm.isTORTER='TER'  and t.userId not in (select userid from tbl_NoaIssueDetails where tenderId=@v_fieldName2Vc and pkgLotId = @v_fieldName3Vc)
	 and t.roundId=erm.roundid and t.userid=erm.userId and t.roundId in 
	(select max(erm.roundId) from tbl_EvalRoundMaster erm
	where erm.tenderId=@v_fieldName2Vc and erm.reportType='L1' and erm.pkgLotId = @v_fieldName3Vc)
	and t.roundId in(select roundId from tbl_EvalRptSentToAA where rptStatus = 'Approved' and roundId in(
	select max(erm.roundId) from tbl_EvalRoundMaster erm
	where erm.tenderId=@v_fieldName2Vc and erm.reportType='L1' and erm.pkgLotId = @v_fieldName3Vc))
	end
else
begin
print '2'
SELECT  case cm.CompanyName when '-' then firstName+' '+lastName else cm.companyName end as FieldValue1,
	convert(varchar(20),tm.userid) as FieldValue2,convert(varchar(20),t.roundId) as FieldValue3, convert(varchar(20),t.amount) as FieldValue4,
	convert(varchar(20),t.rank)  as FieldValue5
	  FROM  [tbl_BidderRank] t,tbl_TendererMaster tm,tbl_companymaster cm,tbl_ReportMaster rm,tbl_evalroundmaster erm
	 where t.userId=tm.userId and t.tenderId=@v_fieldName2Vc  and t.pkgLotId = @v_fieldName3Vc
	and tm.companyId=cm.companyId and t.reportId = rm.reportId and rm.isTORTER='TER'  and t.userId not in (select userid from tbl_NoaIssueDetails where tenderId=@v_fieldName2Vc and pkgLotId = @v_fieldName3Vc)
	and t.roundId=erm.roundid and t.userid=erm.userId
	and t.userId in 
	(select userId from tbl_PostQualification where tenderId=@v_fieldName2Vc 
	and postQualStatus='Qualify' and noaStatus!='rejected' and pkgLotId = @v_fieldName3Vc) and 
	t.roundId in 
	(select max(erm.roundId) from tbl_EvalRoundMaster erm
	where erm.tenderId=@v_fieldName2Vc and erm.reportType='L1'  and erm.pkgLotId = @v_fieldName3Vc)
	and t.roundId in(select roundId from tbl_EvalRptSentToAA where rptStatus = 'Approved' and roundId in(
	select max(erm.roundId) from tbl_EvalRoundMaster erm
	where erm.tenderId=@v_fieldName2Vc and erm.reportType='L1'  and erm.pkgLotId = @v_fieldName3Vc))
end	
end		
END

IF @v_fieldName1Vc = 'tenderStatusForTenderer'
BEGIN
	declare @v_tendStatus varchar(20), @v_corriTen int
	IF(select COUNT(tenderId) from tbl_TenderDetails where tenderId = @v_fieldName2Vc and submissionDt >= GETDATE()) != 0
	BEGIN
		select @v_corriTen = COUNT(tenderId) from tbl_CorrigendumMaster where tenderId=@v_fieldName2Vc and corrigendumStatus = 'approved'
		IF(@v_corriTen)!=0 
		BEGIN
			SELECT @v_tendStatus =  'Amendment/Corrigendum issued' + convert(varchar(20),@v_corriTen)
		END
		--ELSE
		--BEGIN
		--	SELECT @v_tendStatus =  'Live'
		--END
	END
	IF ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
	where rptstatus like '%reject%' and  tenderId = @v_fieldName2Vc order by evalRptToAAId desc)='Rejected/Re-Tendering')
	BEGIN
		SELECT @v_tendStatus = 'Re-Tendered'
	END
	--IF(select COUNT(tnid.tenderId) from tbl_NoaIssueDetails tnid, tbl_NoaAcceptance tna
--		where tnid.noaIssueId = tna.noaIssueId and tenderId = @v_fieldName2Vc and tna.acceptRejStatus = 'approved') = 0
	IF(SELECT COUNT(*) FROM	tbl_TenderDetails WHERE	submissionDt <= GETDATE() AND tenderId = @v_fieldName2Vc) = 1
	BEGIN
		SELECT @v_tendStatus = 'Being processed'
	END
	IF(select COUNT(tnid.tenderId) from tbl_NoaIssueDetails tnid, tbl_NoaAcceptance tna,tbl_ContractSign tcs
		where tnid.noaIssueId = tna.noaIssueId and tcs.noaId = tnid.noaIssueId
		and tenderId = @v_fieldName2Vc and tna.acceptRejStatus = 'approved') != 0
	BEGIN
		SELECT @v_tendStatus = 'Contract Awarded'
	END
	IF(select COUNT(tenderId) from tbl_TenderDetails where tenderId = @v_fieldName2Vc AND tenderStatus = 'Cancelled') != 0
	BEGIN
		SELECT @v_tendStatus = 'Cancelled'
	END
	select @v_tendStatus as FieldValue1
End
IF @v_fieldName1Vc = 'tenderStatusForPEHOPE'
BEGIN
	declare @v_tendStatusPEHOPE varchar(20), @v_corriTenPEHOPE int
	SET @v_tendStatusPEHOPE = ''
	--IF(select COUNT(tenderId) from tbl_TenderDetails where tenderId = @v_fieldName2Vc and submissionDt >= GETDATE()) != 0
	--BEGIN
	--	SELECT @v_tendStatusPEHOPE = 'Live'
	--END
	IF(SELECT COUNT(*) FROM	tbl_TenderDetails WHERE	(tenderStatus = 'Approved' And tenderPubDt > getdate()) Or tenderStatus = 'Pending'
		AND	tenderId = @v_fieldName2Vc) = 0
	BEGIN		
		SELECT @v_tendStatusPEHOPE = NULL
			PRINT @v_tendStatusPEHOPE
	END
--	IF(select COUNT(tnid.tenderId) from tbl_NoaIssueDetails tnid, tbl_NoaAcceptance tna
--		where tnid.noaIssueId = tna.noaIssueId and tenderId = @v_fieldName2Vc and tna.acceptRejStatus = 'approved') = 0 AND @v_tendStatusPEHOPE = ''
	IF(SELECT COUNT(*) FROM	tbl_TenderDetails WHERE	submissionDt <= GETDATE() AND tenderId = @v_fieldName2Vc) = 1
	BEGIN
		SELECT @v_tendStatusPEHOPE = 'Being processed'
			PRINT @v_tendStatusPEHOPE
	END		
	IF(select COUNT(tenderId) from tbl_TenderDetails where tenderId = @v_fieldName2Vc and submissionDt >= GETDATE()) != 0
	BEGIN
		select @v_corriTenPEHOPE = COUNT(tenderId) from tbl_CorrigendumMaster where tenderId=@v_fieldName2Vc and corrigendumStatus = 'approved'
		IF(@v_corriTenPEHOPE)!=0 
		BEGIN
			SELECT @v_tendStatusPEHOPE =  'Amendment/Corrigendum issued' + convert(varchar(20),@v_corriTenPEHOPE)
			PRINT @v_tendStatusPEHOPE
		END
		PRINT @v_tendStatusPEHOPE + 'aaa'
	END
	IF ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
	where rptstatus like '%reject%' and  tenderId = @v_fieldName2Vc order by evalRptToAAId desc)='Rejected/Re-Tendering') 
	BEGIN
		SELECT @v_tendStatusPEHOPE = 'Re-Tendered'
		PRINT @v_tendStatusPEHOPE
	END
	IF(select COUNT(tenderId) from tbl_TenderDetails where tenderId = @v_fieldName2Vc AND tenderStatus = 'Cancelled') != 0 AND @v_tendStatusPEHOPE = ''
	BEGIN
		SELECT @v_tendStatusPEHOPE = 'Cancelled'
			PRINT @v_tendStatusPEHOPE
	END	
	IF(select COUNT(tenderId) from tbl_TenderClose where tenderId=@v_fieldName2Vc) != 0 AND @v_tendStatusPEHOPE = ''
	BEGIN
		SELECT @v_tendStatusPEHOPE = 'Opening Completed'
	END
	IF (select count(tenderId) from tbl_TosRptShare where tenderId = @v_fieldName2Vc) >= 1 AND @v_tendStatusPEHOPE = ''
	BEGIN
		--SELECT @v_tendStatusPEHOPE = 'Going on Evaluation by TEC'
		SELECT @v_tendStatusPEHOPE = 'Being evaluated'
		PRINT @v_tendStatusPEHOPE
	END
	IF(select COUNT(distinct ea.tenderId) from tbl_EvalRptSentToAA ea where rptstatus = 'Seek-Clarification' and tenderId = @v_fieldName2Vc) != 0 AND @v_tendStatusPEHOPE = ''
	BEGIN
		SELECT @v_tendStatusPEHOPE = 'Clarification Requested'
			PRINT @v_tendStatusPEHOPE
	END
	IF(select COUNT(distinct ea.tenderId) from tbl_EvalRptSentToAA ea where rptstatus = 'Approved' and tenderId = @v_fieldName2Vc) != 0 AND @v_tendStatusPEHOPE = ''
	BEGIN
		SELECT @v_tendStatusPEHOPE = 'Approved'
			PRINT @v_tendStatusPEHOPE
	END
	IF(select COUNT(tnid.tenderId) from tbl_NoaIssueDetails tnid, tbl_NoaAcceptance tna
		where tnid.noaIssueId = tna.noaIssueId and tenderId = @v_fieldName2Vc and tna.acceptRejStatus = 'decline') != 0 AND @v_tendStatusPEHOPE = ''
	BEGIN
		SELECT @v_tendStatusPEHOPE = 'NOA declined'
			PRINT @v_tendStatusPEHOPE
	END
	IF(select COUNT(tnid.tenderId) from tbl_NoaIssueDetails tnid, tbl_NoaAcceptance tna,tbl_ContractSign tcs
		where tnid.noaIssueId = tna.noaIssueId and tcs.noaId = tnid.noaIssueId
		and tenderId = @v_fieldName2Vc and tna.acceptRejStatus = 'approved') != 0 AND @v_tendStatusPEHOPE = '' 
	BEGIN
		SELECT @v_tendStatusPEHOPE = 'Contract Awarded'
			PRINT @v_tendStatusPEHOPE
	END

	select @v_tendStatusPEHOPE as FieldValue1
End


IF @v_fieldName1Vc = 'tenderStatusForCM'
BEGIN
	declare @v_tendStatusCM varchar(20)
	
	IF ((select top 1 rptstatus from tbl_EvalRptSentToAA ea
	where rptstatus like '%reject%' and  tenderId = @v_fieldName2Vc order by evalRptToAAId desc)='Rejected/Re-Tendering')
	BEGIN
		SELECT @v_tendStatusCM = 'Re-Tendered'
	END
	IF(select COUNT(tenderId) from tbl_NoaIssueDetails where tenderId = @v_fieldName2Vc) !=0
	BEGIN
		SELECT @v_tendStatusCM = 'NOA issued'
	END
	IF(select COUNT(tnid.tenderId) from tbl_NoaIssueDetails tnid,tbl_NoaAcceptance tna 
		where tnid.noaIssueId = tna.noaIssueId and tna.acceptRejStatus='approved' and tenderId = @v_fieldName2Vc) !=0
	BEGIN
		SELECT @v_tendStatusCM = 'NOA Accepted'
	END
	IF(select COUNT(tnid.tenderId) from tbl_NoaIssueDetails tnid, tbl_NoaAcceptance tna,tbl_ContractSign tcs
		where tnid.noaIssueId = tna.noaIssueId and tcs.noaId = tnid.noaIssueId
		and tenderId = @v_fieldName2Vc and tna.acceptRejStatus = 'approved') != 0
	BEGIN
		SELECT @v_tendStatusCM = 'Contract Awarded'
	END
	IF((select count(tenderId) from tbl_TosRptShare where tenderId = @v_fieldName2Vc)>=1)
	BEGIN
		--SELECT @v_tendStatusPEHOPE = 'Going on Evaluation by TEC'
		SELECT @v_tendStatusCM = 'Being evaluated'
	END
	IF(select COUNT(distinct ea.tenderId) from tbl_EvalRptSentToAA ea where rptstatus = 'Approved' and tenderId = @v_fieldName2Vc) != 0
	BEGIN
		SELECT @v_tendStatusCM = 'Approved'
	END
	IF(select COUNT( distinct ea.tenderId) from tbl_EvalRptSentToAA ea where rptstatus = 'Seek-Clarification' and tenderId = @v_fieldName2Vc) != 0
	BEGIN
		SELECT @v_tendStatusCM = 'Clarification Requested'
	END
	IF(select COUNT(tenderId) from tbl_TenderDetails where tenderId = @v_fieldName2Vc AND tenderStatus = 'Cancelled') != 0
	BEGIN
		SELECT @v_tendStatusCM = 'Cancelled'
	END
	select @v_tendStatusCM as FieldValue1
END

IF @v_fieldName1Vc = 'checkHopeFromTenderId'
BEGIN
	select CONVERT(varchar(10),tdm.approvingAuthorityId) as FieldValue1 
	from tbl_TenderDetails ttd,tbl_DepartmentMaster tdm
	where tdm.departmentId = ttd.departmentId and ttd.tenderId = @v_fieldName2Vc
END

IF @v_fieldName1Vc = 'checkEvaluationCommMem'
BEGIN
	IF(select COUNT(tc.committeeId) from tbl_CommitteeMembers tcm inner join tbl_Committee tc on tc.committeeId = tcm.committeeId
	where tc.committeeType in('TEC','PEC')) !=0
	BEGIN
		select 'Yes' as FieldValue1 
	END
	ELSE
	BEGIN
		select 'No' as FieldValue1 
	END
END

