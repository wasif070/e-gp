USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_ins_boqFormFixedRates]    Script Date: 4/24/2016 11:14:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--------------------------------------------------------------------------------
--
-- Purpose: To create New BOQ form with fixed rates or salvage materials in works
--
-- exec p_ins_boqFormFixedRates 'createFixedOrSalvageBOQ','1805', '9840','BOQfixed'
-- Author: Kiran
-- Date: 22/04/2015

ALTER  PROCEDURE [dbo].[p_ins_boqFormFixedRates]
	 @v_fieldName1Vc varchar(500)=NULL,		  -- action
	 @v_fieldName2Vc varchar(500)=NULL, -- tenderID
	 @v_fieldName3Vc varchar(500)=NULL, --  sectionID
	 @v_fieldName4Vc varchar(500)=NULL--fixed or salvage BOQ
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;

/* GSS - Fixed rate BOQ / salavage items BOQ starts */
IF @v_fieldName1Vc = 'createFixedOrSalvageBOQ'
BEGIN

-- 0 ERROR
-- 1 OK
declare @v_packageOrLotId int
declare @v_flag_Vc varchar(10)
declare @v_formname_Vc varchar(500)
declare @v_tablename_Vc varchar(500)
declare @v_newFormId_In int
declare @v_formheader_Vc varchar(2000)
declare @v_formfooter_Vc varchar(2000)
declare @v_formStatus_Vc varchar(50)
declare @v_formType_Vc varchar(50)
declare @v_newTenderTableId_In int
declare @v_newTenderListId_In int
		BEGIN TRY

			BEGIN TRANSACTION creteForm
   IF @v_fieldName4Vc='BOQfixed' OR @v_fieldName4Vc='BOQfixedcorriform'
   BEGIN
     SET @v_formname_Vc='Bill of Quantities (Fixed rates items )'
     SET @v_formheader_Vc='<p>   This form contains items</p>  <p>   1. Items with fixed rates . Tenderer should give consent by selecting &quot;I AGREE &quot; Combo box</p>'
     SET @v_formfooter_Vc='<p>   All items should be accepted by Tenderer</p> '
     SET @v_tablename_Vc='Fixed Rate items '
     SET @v_formType_Vc='BOQfixed'
     IF  @v_fieldName4Vc='BOQfixedcorriform'
       BEGIN
		 SET @v_formStatus_Vc='createp'
       END
     ELSE
      BEGIN
		 SET @v_formStatus_Vc=@v_fieldName4Vc
      END

   END
   ELSE IF @v_fieldName4Vc='BOQSalvage' OR @v_fieldName4Vc='BOQSalvagecorriform'
   BEGIN
     SET @v_formname_Vc='Bill of Quantities (Salvage items )'
     SET @v_formheader_Vc='<p>   This form contains items</p>  <p>   1. Salvage items. Tenderer should give consent by selecting &quot;I AGREE &quot; Combo box</p>'
     SET @v_formfooter_Vc='<p>   All items should be accepted by Tenderer</p> '
     SET @v_tablename_Vc='Salvage materials'
     SET @v_formType_Vc='BOQSalvage'
     IF  @v_fieldName4Vc='BOQSalvagecorriform'
       BEGIN
		 SET @v_formStatus_Vc='createp'
       END
     ELSE
      BEGIN
		 SET @v_formStatus_Vc=@v_fieldName4Vc
      END
   END

				SET NOCOUNT ON;
         SELECT @v_packageOrLotId=appPkgLotId  FROM tbl_TenderLotSecurity where tenderId=@v_fieldName2Vc


           --insert into tbl_TenderForms
           INSERT INTO dbo.tbl_TenderForms (tenderSectionId, templateFormId, filledBy, formName, formHeader, formFooter, noOfTables,
																isMultipleFilling, isEncryption, isPriceBid, isMandatory, formStatus,pkgLotId,FormType)

             VALUES(@v_fieldName3Vc,0,1,@v_formname_Vc,@v_formheader_Vc,@v_formfooter_Vc,1,'no','yes','yes','yes',@v_formStatus_Vc,@v_packageOrLotId,@v_formType_Vc)

          SELECT @v_newFormId_In = SCOPE_IDENTITY()
          --insert into tbl_TenderTables
        INSERT INTO dbo.tbl_TenderTables (tenderFormId, tableName, tableHeader, tableFooter, noOfRows, noOfCols, isMultipleFilling,
																			templatetableId)

          VALUES(@v_newFormId_In,@v_tablename_Vc,'','',0,11,'no',1)

           SELECT @v_newTenderTableId_In = SCOPE_IDENTITY()
          --insert into tbl_TenderColumns

          INSERT INTO dbo.tbl_TenderColumns (tenderTableId, columnId, columnHeader, dataType, filledBy, columnType, sortOrder,
																				showorhide, templateTableId)
		  VALUES(@v_newTenderTableId_In,1,	'Item No.' ,1,1,1,1,1,0),
		        (@v_newTenderTableId_In,2,	'Group',1,1,1,2,1,0),
				(@v_newTenderTableId_In,3,	'Item Code<br/>(if any)',1,1,1,3,1,0),
				(@v_newTenderTableId_In,4,	'Description of Items of Works',2,1,1,4,1,0),
				(@v_newTenderTableId_In,5,	'Measurement Unit',1,1,1,5,1,0),
				(@v_newTenderTableId_In,6,	'Quantity',4,1,1,6,1,0),
				(@v_newTenderTableId_In,7,	'Unit Price In figures (BDT)',13,1,1,7,1,0),
				(@v_newTenderTableId_In,8,	'Unit Price In words (BDT)',2,3,1,8,1,0),
				(@v_newTenderTableId_In,9,	'Total Price in figures (BDT)',4,3,7,9,1,0),
				(@v_newTenderTableId_In,10,	'Total Price In words (BDT)',2,3,1,10,1,0),
				(@v_newTenderTableId_In,11,	'Tenderer  Response',10,2,1,11,1,0)
          --insert into tbl_TenderListBox
          INSERT INTO dbo.tbl_TenderListBox (listBoxName,tenderFormId,listBoxId,isCalcReq)
                                      VALUES('Tenderer Response',@v_newFormId_In,0,'No')
          SELECT @v_newTenderListId_In = SCOPE_IDENTITY()
          --inser into tbl_TenderListDetail
          INSERT INTO dbo.tbl_TenderListDetail (tenderListId,itemId,itemValue,itemText,isDefault)

                                VALUES(@v_newTenderListId_In,1,1,'SELECT','yes'),
                                      (@v_newTenderListId_In,2,2,'I AGREE','no')
          --insert into tbl_TenderFormula
          INSERT INTO dbo.tbl_TenderFormula(tenderFormId,tenderTableId,columnId,formula,isGrandTotal)
                          VALUES(@v_newFormId_In,@v_newTenderTableId_In,8,'WORD(7)','no'),
								(@v_newFormId_In,@v_newTenderTableId_In,9,'6*7','no'),
								(@v_newFormId_In,@v_newTenderTableId_In,9,'TOTAL(9)','yes'),
								(@v_newFormId_In,@v_newTenderTableId_In,10,'WORD(9)','no')

           SET @v_flag_Vc = '1'

			COMMIT TRANSACTION creteForm

		END TRY
		BEGIN CATCH

			BEGIN
					SET @v_flag_Vc = '0'
					ROLLBACK TRANSACTION creteForm
			END

		END CATCH

	SELECT @v_flag_Vc AS FieldValue1


END




/* GSS - Fixed rate BOQ / salavage items BOQ ends */

END


