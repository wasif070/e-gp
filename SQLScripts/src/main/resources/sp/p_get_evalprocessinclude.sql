USE [eGPLive22Feb2017]
GO
/****** Object:  StoredProcedure [dbo].[p_get_evalprocessinclude]    Script Date: 3/9/2017 6:54:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ahsan
-- Create date: 29.7.2015
-- Description:	<for optimizing EvalProcessInclude.jsp>
-- =============================================
ALTER PROCEDURE [dbo].[p_get_evalprocessinclude] 
	-- Add the parameters for the stored procedure here
	@v_fieldName1Vc varchar(500)=NULL,	
	@v_fieldName2Vc varchar(1000)=NULL,	
	@v_fieldName3Vc varchar(1000)=NULL,	
	@v_fieldName4Vc varchar(1000)=NULL,	
	@v_fieldName5Vc varchar(1000)=NULL,	
	@v_fieldName6Vc varchar(1000)=NULL,	
	@v_fieldName7Vc varchar(1000)=NULL,	
	@v_fieldName8Vc varchar(1000)=NULL,	
	@v_fieldName9Vc varchar(1000)=NULL,	
	@v_fieldName10Vc varchar(1000)=NULL,	
	@v_fieldName11Vc varchar(1000)=NULL,	
	@v_fieldName12Vc varchar(1000)=NULL,	
	@v_fieldName13Vc varchar(1000)=NULL,	
	@v_fieldName14Vc varchar(1000)=NULL,	
	@v_fieldName15Vc varchar(1000)=NULL,	
	@v_fieldName16Vc varchar(1000)=NULL,	
	@v_fieldName17Vc varchar(1000)=NULL,	
	@v_fieldName18Vc varchar(1000)=NULL,	
	@v_fieldName19Vc varchar(1000)=NULL,	
	@v_fieldName20Vc varchar(1000)=NULL
AS
	Declare @evalCount int


--start nafiul
IF @v_fieldName1Vc = 'isTERSentToTc'
Begin
	 SELECT CONVERT(varchar(50),COUNT(evalRptTCId)) as FieldValue1 FROM  tbl_EvalRptSentTC WHERE tenderId = @v_fieldName2Vc and pkgLotId=@v_fieldName3Vc and roundId=@v_fieldName4Vc and rptStatus = @v_fieldName5Vc
end
--end nafiul


IF @v_fieldName1Vc = 'getCRReportId'
Begin
	 if @v_fieldName3Vc='0'
	 begin
		select CONVERT(varchar(50),rm.reportId) as FieldValue1,
		case when((select COUNT(bidderRankId) from tbl_BidderRank br where
		br.tenderId=@v_fieldName2Vc and br.reportId=rm.reportId and roundId=@v_fieldName4Vc)=0)
		then '0' else '1' end as FieldValue2
		 from tbl_ReportMaster rm where rm.tenderId=@v_fieldName2Vc and rm.isTORTER='TER'
	 end
	 else
	 begin
	 select CONVERT(varchar(50),rm.reportId) as FieldValue1,
	 case when((select COUNT(bidderRankId) from tbl_BidderRank br
	 where br.tenderId=@v_fieldName2Vc and br.pkgLotId=@v_fieldName3Vc and br.reportId=rm.reportId  and roundId=@v_fieldName4Vc)=0)
		then '0' else '1' end as FieldValue2
	 from tbl_ReportMaster rm,tbl_ReportLots rl
		where rm.tenderId=@v_fieldName2Vc  and rm.reportId = rl.reportId and rl.pkgLotId=@v_fieldName3Vc
		and rm.isTORTER='TER'
	 end
end

IF @v_fieldName1Vc = 'isCRFormulaMadeTORTER'
BEGIN
	IF @v_fieldName3Vc='0'
		BEGIN
			select case when COUNT(rm.reportId)!=0 then '1' else '0' end as FieldValue1
			from tbl_ReportMaster rm inner join tbl_ReportTableMaster rtm
			on rm.reportId = rtm.reportId inner join tbl_ReportFormulaMaster rfm on rfm.reportTableId = rtm.reportTableId
			where tenderId=@v_fieldName2Vc and isTORTER=''+@v_fieldName4Vc+''
		END
	ELSE
		BEGIN
			select case when COUNT(rm.reportId)!=0 then '1' else '0' end as FieldValue1
			from tbl_ReportMaster rm inner join tbl_ReportTableMaster rtm
			on rm.reportId = rtm.reportId inner join tbl_ReportFormulaMaster rfm on rfm.reportTableId = rtm.reportTableId
			inner join tbl_ReportLots rl on rl.reportId = rm.reportId
			where tenderId=@v_fieldName2Vc and isTORTER=''+@v_fieldName4Vc+'' and rl.pkgLotId=@v_fieldName3Vc
		END
END

IF @v_fieldName1Vc='isFinalizeDoneForLot'
BEGIN
	Declare @field1 int
	Declare @field2 int
	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
			SET @evalCount = 0
		else
			select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc


	if ((select docAvlMethod from tbl_TenderDetails where tenderId=@v_fieldName2Vc) <> 'Package')
	begin
		select case when
		(
		(select COUNT(f.userId) FROM tbl_FinalSubmission f,tbl_BidderLots bl WHERE f.tenderId=@v_fieldName2Vc and
		  bl.userId=f.userId and bl.tenderId = f.tenderId and bl.pkgLotId = @v_fieldName3Vc	and bidSubStatus='finalsubmission')=
		(SELECT COUNT(evaBidderStatusId) FROM dbo.tbl_EvalBidderStatus WHERE tenderId = @v_fieldName2Vc and evalCount = @v_fieldName4Vc
		  and pkgLotId = @v_fieldName3Vc)
		)
		then '1' else '0' end as FieldValue1
	end
	else
	begin
		IF((select  COUNT(tbl_noaissuedetails.userId)  from tbl_noaacceptance inner join tbl_noaissuedetails 
		on tbl_noaissuedetails.noaissueId = tbl_noaacceptance.noaissueId where tenderId = @v_fieldName2Vc and acceptRejStatus in ('decline','Performance Security not paid')
		) > 0 and @evalCount > 0 )
		BEGIN
		 select @field1 = COUNT(evaBidderStatusId) from tbl_EvalBidderStatus ebs,tbl_FinalSubmission f,tbl_NoaIssueDetails tnid 
			where ebs.tenderId = f.tenderId and ebs.evalCount = @v_fieldName4Vc and f.tenderId = @v_fieldName2Vc and f.userId = ebs.userId
			and tnid.tenderId  = @v_fieldName2Vc and ebs.userId not in (select userId from tbl_NoaIssueDetails where tenderId = @v_fieldName2Vc)
		select @field2 = count(userId) from tbl_NoaIssueDetails where tenderId = @v_fieldName2Vc
			select case when
			(
						
			(select COUNT(f.userId) FROM tbl_FinalSubmission f WHERE f.tenderId=@v_fieldName2Vc and
			 bidSubStatus='finalsubmission')= @field1 + @field2
			
			  )			 
			
		then '1' else '0' end as FieldValue1
		END
		ELSE 
		BEGIN
			select case when
			(
			(select COUNT(f.userId) FROM tbl_FinalSubmission f WHERE f.tenderId=@v_fieldName2Vc and
			 bidSubStatus='finalsubmission')=
			(SELECT COUNT(evaBidderStatusId) FROM dbo.tbl_EvalBidderStatus WHERE tenderId = @v_fieldName2Vc and evalCount = @evalCount
			  )
			)
		then '1' else '0' end as FieldValue1
		END
	end
END

IF @v_fieldName1Vc = 'getRoundsforEvaluation'
BEGIN
	if(@v_fieldName5Vc is not null)
		SET @evalCount = @v_fieldName5Vc
	else
	BEGIN
	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
			SET @evalCount = 0
		else
			select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc
	END	
		
	--IF ((select COUNT(tbl_noaissuedetails.userId)  from tbl_noaacceptance inner join tbl_noaissuedetails 
	--on tbl_noaissuedetails.noaissueId = tbl_noaacceptance.noaissueId where tenderId = @v_fieldName2Vc and acceptRejStatus in ('decline','Performance Security not paid')
	--	) > 0 and @evalCount > 0 )
	--BEGIN
	--	select convert(varchar(50),er1.roundId) as FieldValue1,convert(varchar(50),er1.userId) as FieldValue2,
	-- case when
	--    (select COUNT(er2.roundId) from tbl_EvalRoundMaster er2 where er2.tenderId=@v_fieldName2Vc and er2.reportType='T1L1' and er2.roundId > er1.roundId)!=0
	-- Then
	--	dbo.f_getbiddercompany((select top 1 er2.userId from tbl_EvalRoundMaster er2 where er2.tenderId=@v_fieldName2Vc and er2.reportType='T1L1' and er2.roundId > er1.roundId order by er2.roundId asc))
	-- Else
	--  case when
	--	 (select COUNT(er2.roundId) from tbl_EvalRoundMaster er2 where er2.tenderId=@v_fieldName2Vc and er2.reportType='T1' and er2.roundId < er1.roundId)!=0
	--  Then
	--	 dbo.f_getbiddercompany((select top 1 er2.userId from tbl_EvalRoundMaster er2 where er2.tenderId=@v_fieldName2Vc and er2.reportType='T1' and er2.roundId < er1.roundId order by er2.roundId asc))
	--  Else
	--	dbo.f_getbiddercompany(er1.userId)
	--  End
	-- End
	--as FieldValue3
	--from tbl_EvalRoundMaster er1 where er1.tenderId=@v_fieldName2Vc and er1.reportId=@v_fieldName3Vc and er1.reportType=@v_fieldName4Vc and er1.evalCount = @evalCount and 
	--er1.userId not in (select tbl_noaissuedetails.userId  from tbl_noaacceptance inner join tbl_noaissuedetails 
	--on tbl_noaissuedetails.noaissueId = tbl_noaacceptance.noaissueId where tenderId = @v_fieldName2Vc and acceptRejStatus in ('decline','Performance Security not paid'))
	--END
	--ELSE
	BEGIN
	select convert(varchar(50),er1.roundId) as FieldValue1,convert(varchar(50),er1.userId) as FieldValue2,
	 case when
	    (select COUNT(er2.roundId) from tbl_EvalRoundMaster er2 where er2.tenderId=@v_fieldName2Vc and er2.reportType='T1L1' and er2.pkglotid = @v_fieldName5Vc and er2.roundId > er1.roundId)!=0
	 Then
		dbo.f_getbiddercompany((select top 1 er2.userId from tbl_EvalRoundMaster er2 where er2.tenderId=@v_fieldName2Vc and er2.reportType='T1L1' and er2.pkglotid = @v_fieldName5Vc and er2.roundId > er1.roundId order by er2.roundId asc))
	 Else
		  case when
			 (select COUNT(er2.roundId) from tbl_EvalRoundMaster er2 where er2.tenderId=@v_fieldName2Vc and er2.reportType='T1' and er2.pkglotid = @v_fieldName5Vc and er2.roundId < er1.roundId)!=0
		  Then
			 dbo.f_getbiddercompany((select top 1 er2.userId from tbl_EvalRoundMaster er2 where er2.tenderId=@v_fieldName2Vc and er2.reportType='T1' and er2.pkglotid = @v_fieldName5Vc and er2.roundId < er1.roundId order by er2.roundId asc))
		  Else
			dbo.f_getbiddercompany(er1.userId)
		  End
	 End
	as FieldValue3
	from tbl_EvalRoundMaster er1 where er1.tenderId=@v_fieldName2Vc and er1.reportId=@v_fieldName3Vc and er1.reportType=@v_fieldName4Vc and er1.evalCount = @evalCount and er1.pkglotid = @v_fieldName6Vc
    END
END

IF @v_fieldName1Vc = 'FirstRoundAndNoaBiddersChk'
BEGIN
	DECLARE @var_cnt int
	if ((select count(tenderid) from tbl_TenderDetails where tenderId=@v_fieldName2Vc and procurementMethod in('DPM')) >=1  )
	BEGIN
		SET @v_fieldName3Vc='0'
	END

	IF @v_fieldName3Vc!='0'
	BEGIN
		IF(SELECT COUNT(na.userId) FROM tbl_NoaIssueDetails nd inner join tbl_NoaAcceptance na on
			na.noaIssueId = nd.noaIssueId and acceptRejStatus in ('decline','Performance Security not paid') and tenderId =@v_fieldName2Vc
			and (pkgLotId = @v_fieldName3Vc OR pkgLotId=0))=
			(SELECT CASE WHEN COUNT(userId)=0 then 1 else COUNT(userId) end  from tbl_BidderRank where
			roundId in (select MIN(roundId) from tbl_EvalRoundMaster where tenderId=@v_fieldName2Vc
			 and reportId=@v_fieldName4Vc and evalCount = @v_fieldName5Vc))
		BEGIN
			SELECT @var_cnt=0
		END
		ELSE
		BEGIN
			SELECT @var_cnt=1
		END

		 IF @var_cnt=0
		 BEGIN
			 IF(select  case when COUNT(userId)= 0 then 5 else COUNT(userId) end from tbl_PostQualification pq
				where postQualStatus='disqualify' and tenderId = @v_fieldName2Vc and evalCount = @v_fieldName5Vc
				and (pkgLotId = @v_fieldName3Vc or pkgLotId=0 )) =
				(select case when COUNT(userId)=0 then 1 else COUNT(userId) end  from tbl_BidderRank where
				roundId in (select MIN(roundId) from tbl_EvalRoundMaster where tenderId=@v_fieldName2Vc
				 and reportId=@v_fieldName4Vc and evalCount = @v_fieldName5Vc))
			 BEGIN
				SELECT @var_cnt=0
			 END
			 ELSE
			 BEGIN
				SELECT @var_cnt=1
			 END
		 END
		 IF @var_cnt=1
		 Begin
			IF (select count(roundId) from tbl_EvalRoundMaster where tenderId=@v_fieldName2Vc and evalCount = @v_fieldName5Vc)!=0
			BEGIN
				IF((select top 1 userId from tbl_PostQualification where  (postQualStatus='disqualify' or noaStatus='rejected' or noaStatus='Performance Security not paid') and tenderId=@v_fieldName2Vc and evalCount = @v_fieldName5Vc order by postQaulId desc)
				=
			   (select top 1 userId from tbl_EvalRoundMaster where tenderId=@v_fieldName2Vc and evalCount = @v_fieldName5Vc order by roundId desc))
				BEGIN
					SELECT '1' as FieldValue1
				END
				ELSE
				BEGIN
					SELECT '0' as FieldValue1
				END
			END
			ELSE
			BEGIN
				SELECT '1' as FieldValue1
			END
	     END
	END
	ELSE--start for others @v_fieldName3Vc='0'
	BEGIN
		IF(select COUNT(na.userId) from tbl_NoaIssueDetails nd inner join tbl_NoaAcceptance na on
				na.noaIssueId = nd.noaIssueId and acceptRejStatus in ('decline','Performance Security not paid') and tenderId =@v_fieldName2Vc
				and (pkgLotId = @v_fieldName3Vc OR pkgLotId=0))=
				(select case when COUNT(userId)=0 then 1 else COUNT(userId) end  from tbl_BidderRank where
				roundId in (select MIN(roundId) from tbl_EvalRoundMaster where tenderId=@v_fieldName2Vc
				 and reportId=@v_fieldName4Vc and evalCount = @v_fieldName5Vc))
		Begin
			select @var_cnt=0
		END
		ELSE
		BEGIN
			select @var_cnt=1
		End
		IF @var_cnt=1
		Begin
			IF(select count(nn.userId) from tbl_Negotiation tn inner join tbl_NegNotifyTenderer nn on tn.negId = nn.negId
				where negStatus='Failed' and tenderId = @v_fieldName2Vc and evalCount = @v_fieldName5Vc ) =
				(select count(userId)  from tbl_EvalRoundMaster where
				roundId in (select MAX(roundId) from tbl_EvalRoundMaster where tenderId=@v_fieldName2Vc
				 and reportId=@v_fieldName4Vc and evalCount = @v_fieldName5Vc and reportType not in ('N1','T1')))
			BEGIN
				SELECT '1' as FieldValue1
			END
			ELSE
			BEGIN
				SELECT '0' as FieldValue1
			END
		END
		ELSE
		BEGIN
			select '0' as FieldValue1
		END
	END
END	

IF @v_fieldName1Vc = 'BidderRoundsforEvaluationChk'
BEGIN

	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName3Vc)=0
			SET @evalCount = 0
		else
			select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName3Vc
	if ((select count(tenderid) from tbl_TenderDetails where tenderId=@v_fieldName3Vc and procurementMethod in('DPM')) >=1  )
	begin
	set @v_fieldName4Vc='0'
	end
	If @v_fieldName4Vc!='0'
		Begin
			select case when (select COUNT(na.userId) from tbl_NoaIssueDetails nd inner join tbl_NoaAcceptance na on
			na.noaIssueId = nd.noaIssueId and acceptRejStatus in ('decline','Performance Security not paid')
			and roundId = @v_fieldName2Vc)=
			(select COUNT(userId) from tbl_EvalRoundMaster where
			roundId = @v_fieldName2Vc and evalCount = @evalCount)
			then '1'
			when (select COUNT(pq.userId) from tbl_PostQualification pq
			 where postQualStatus='disqualify' and tenderId=@v_fieldName3Vc and (pkgLotId=@v_fieldName4Vc or pkgLotId=0)
			 and userId In (select userId from tbl_EvalRoundMaster where
			roundId = @v_fieldName2Vc) and evalCount = @evalCount
			 )=
			(select COUNT(userId) from tbl_EvalRoundMaster where
			roundId = @v_fieldName2Vc and evalCount = @evalCount)
			then '1'
			 else '0' end as FieldValue1
		End
	Else
		Begin
			select case when (select COUNT(na.userId) from tbl_NoaIssueDetails nd inner join tbl_NoaAcceptance na on
			na.noaIssueId = nd.noaIssueId and acceptRejStatus in ('decline','Performance Security not paid')
			and roundId = @v_fieldName2Vc)=
			(select COUNT(userId) from tbl_EvalRoundMaster where
			roundId = @v_fieldName2Vc)
			then '1'
			when (select count(nn.userId) from tbl_Negotiation tn inner join tbl_NegNotifyTenderer nn on tn.negId = nn.negId
				where negStatus='Failed' and tenderId = @v_fieldName3Vc and userId IN (select userId from tbl_EvalRoundMaster where
			roundId = @v_fieldName2Vc))=
			(select COUNT(userId) from tbl_EvalRoundMaster where
			roundId = @v_fieldName2Vc)
			then '1'
			 else '0' end as FieldValue1
		End
END

If @v_fieldName1Vc='isTERNotified'
BEGIN
	select case when count(evalNotId)!=0 then '1' else '0' end as FieldValue1
	from tbl_EvalTERNotifyMember  where  tenderId=@v_fieldName2Vc
	and pkgLotId=@v_fieldName3Vc and reportType=@v_fieldName4Vc
	and roundId = @v_fieldName5Vc
	--select reportType as FieldValue1
	--from tbl_EvalTERNotifyMember  where  tenderId=@v_fieldName2Vc
	--and pkgLotId=@v_fieldName3Vc 
	--and roundId = @v_fieldName4Vc
END

--IF @v_fieldName1Vc = 'isTERSignedLabel'
--BEGIN
--	DECLARE @v_CountCommitteeMembers int

--	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
--			SET @evalCount = 0
--		else
--			select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc
	
--	SET @v_CountCommitteeMembers=(select count(distinct userId) from tbl_Committee tc 
--			  inner join tbl_CommitteeMembers tcm on tc.committeeId = tcm.committeeId
--			  where tc.tenderId = @v_fieldName2Vc and tc.committeeType  in ('TEC','PEC'))

			
--	if(select COUNT(torSignId) from tbl_TORRptSign where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc --and reportType=''+@v_fieldName4Vc+''
--	and userId=@v_fieldName4Vc and roundId = @v_fieldName5Vc and evalCount = @evalCount)=0
--	BEGIN
--		select 'View and Sign' as FieldValue1,reportType as FieldValue2, '' as FieldValue3
--		from tbl_TORRptSign where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc --and reportType=''+@v_fieldName4Vc+''
--		and userId=@v_fieldName4Vc and roundId = @v_fieldName5Vc and evalCount = @evalCount
--	END
--	else
--	BEGIN
--		select 'View' as FieldValue1, 
--		reportType as FieldValue2,
--		case when (select reportType) in (select reportType from tbl_TORRptSign where tenderId = @v_fieldName2Vc and evalCount = @evalCount and roundId = @v_fieldName5Vc and pkgLotId=@v_fieldName3Vc and
--		userId in (select userId from tbl_CommitteeMembers where committeeId = (select committeeId from tbl_Committee where tenderId = @v_fieldName2Vc and committeeType in ('TEC','PEC'))) 
--		group by reportType having COUNT(userid)=@v_CountCommitteeMembers)
--		then '(Signed)' else '' end as FieldValue3 
--		from tbl_TORRptSign where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc 
--		and userId=@v_fieldName4Vc and roundId = @v_fieldName5Vc and evalCount = @evalCount 
		
--	END
--end

IF @v_fieldName1Vc = 'AllBidderDisqualified'
BEGIN
if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
	SET @evalCount = 0
 else
	select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc
	
SELECT CASE WHEN(SELECT COUNT(DISTINCT CONVERT(varchar(20),userid)) as FieldValue1
from tbl_PostQualification where postQualStatus='Disqualify' and
tenderid=@v_fieldName2Vc and evalCount = @evalCount) =
(SELECT COUNT (DISTINCT convert(varchar(20), tbl_FinalSubmission.userId)) as FieldValue1
FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
  and tenderid = @v_fieldName2Vc 
   and userId not in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and evalCount = @evalCount and
  ((isnull(result,'pass')='fail') or (isnull(bidderStatus,'Technically Responsive')='Technically Unresponsive')))
  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc))
 THEN '1' ELSE '0' END AS FieldValue1
 END

IF @v_fieldName1Vc = 'AllBidderDisqualifiedAfterNOA'
BEGIN

 if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
	SET @evalCount = 0
 else
	select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

Declare @totalCount varchar(50)
set @totalCount = (SELECT COUNT(DISTINCT CONVERT(varchar(20),userid)) as FieldValue1 
from tbl_PostQualification where postQualStatus='Disqualify' and 
tenderid=@v_fieldName2Vc and evalCount = @evalCount) + (SELECT COUNT(DISTINCT CONVERT(varchar(20),userid)) as FieldValue1 
from tbl_PostQualification where tenderid=@v_fieldName2Vc and evalCount = @evalCount and
noaStatus in ('rejected','Performance Security not paid') )

IF @totalCount  =
(SELECT COUNT (DISTINCT convert(varchar(20), tbl_FinalSubmission.userId)) as FieldValue1
FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
  and tenderid = @v_fieldName2Vc
   and userId not in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and evalCount = @evalCount and
  ((isnull(result,'pass')='fail') or (isnull(bidderStatus,'Technically Responsive')='Technically Unresponsive')))
  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc))
	Begin
		select '1' AS FieldValue1
	END
 ELSE
	Begin
		select '0' AS FieldValue1
	END

END

IF @v_fieldName1Vc = 'isAllBOQDecrypted'
BEGIN
	if @v_fieldName3Vc='0'
	Begin
		select @v_fieldName3Vc = appPkgLotId from tbl_TenderLotSecurity where tenderId=@v_fieldName2Vc
	End
	select case when
	 (select Convert(varchar(50), COUNT( distinct tbf.tenderFormId))
		from tbl_TenderBidForm tbf, tbl_FinalSubmission fs, tbl_TenderForms tf
		where tbf.tenderId=fs.tenderId and tbf.tenderId=@v_fieldName2Vc and tbf.tenderFormId=tf.tenderFormId
		and tf.pkgLotId=@v_fieldName3Vc
		and bidSubStatus='finalsubmission' And isPriceBid='Yes' and tf.formStatus != 'c')=
		(
		select Convert(varchar(50), COUNT( distinct tbf.tenderFormId))
		from tbl_tenderbidplaindata bp,tbl_TenderBidForm tbf, tbl_FinalSubmission fs, tbl_TenderForms tf
		where tbf.tenderId=fs.tenderId and bp.tenderFormId=tbf.tenderFormId
		and tf.pkgLotId=@v_fieldName3Vc
		 and tbf.tenderId=@v_fieldName2Vc and tbf.tenderFormId=tf.tenderFormId
		and bidSubStatus='finalsubmission' And isPriceBid='Yes' and tf.formStatus != 'c') then '1' else '0' end as FieldValue1
END

IF @v_fieldName1Vc = 'GetTenderEnvCount'
BEGIN
	select	convert(varchar(5),noOfEnvelops) as FieldValue1,
			convert(varchar(5),evalMethod) as FieldValue2
	from	tbl_ConfigEvalMethod cm ,
			tbl_TenderDetails td,
			tbl_TenderTypes tt
	where	cm.procurementMethodId=td.procurementMethodId
			and cm.tenderTypeId=tt.tenderTypeId
			and tt.tenderType=td.eventType
			and cm.procurementNatureId=td.procurementNatureId
			and tenderId=@v_fieldName2Vc
End

IF @v_fieldName1Vc = 'getEvalOpenCommonMember'
BEGIN
	select convert(varchar(50),cm.userId) as FieldValue1 from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
	and c.tenderId = @v_fieldName2Vc and memberFrom in ('TEC','PEC') and c.committeStatus='approved' and cm.appStatus='approved'
END

IF @v_fieldName1Vc = 'getT1L1RoundWise'
BEGIN
	select convert(varchar(50),roundId) as FieldValue1,
	 convert(varchar(50),userId) as FieldValue2,
	 dbo.f_getbiddercompany(userId) as FieldValue3
	from tbl_EvalRoundMaster  where tenderId=@v_fieldName2Vc and reportId=0 and reportType=@v_fieldName3Vc
END

IF @v_fieldName1Vc = 'PerfSecurityAvailChk'
BEGIN
	select case when (count(perCostLotId)!=0) then '1' else '0' end as FieldValue1
	 from tbl_TenderPerfSecurity where roundId=@v_fieldName2Vc
ENd

IF @v_fieldName1Vc = 'PerfSecurityNaoChk'
BEGIN
	select case when (count(noaIssueId)!=0) then '1' else '0' end as FieldValue1
	 from tbl_NoaIssueDetails where roundId=@v_fieldName2Vc
ENd

if @v_fieldName1Vc='isTERSignedByAll'
begin
	--if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
	--	SET @evalCount = 0
	--else
	--	select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc
		
	if @v_fieldName4Vc ='4' -- TER's count
	begin
		IF @v_fieldName3Vc='0'
		BEGIN
			  select (case
					when

						(select COUNT(cm.userId)*convert(int,@v_fieldName4Vc) from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
						where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved'  and c.committeeType in ('TEC','PEC'))
						=
						(select COUNT(trs.torSignId) from tbl_TORRptSign trs
						Where tenderId=@v_fieldName2Vc and trs.evalCount = @v_fieldName7Vc and
						(
						(trs.reportType in ('ter1') and (roundId=0))
							or
						(trs.reportType in ('ter2','ter3','ter4') and (roundId=@v_fieldName6Vc))
						)
						and pkgLotId=@v_fieldName3Vc)

				then '1' else '0' end) as FieldValue1
		END
		ELSE
		BEGIN
					select case
					when
					(
						(select COUNT(cm.userId)*convert(int,@v_fieldName4Vc) from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
						where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved'  and c.committeeType in ('TEC','PEC'))
						=
						(select COUNT(trs.torSignId) from tbl_TORRptSign trs
						Where tenderId=@v_fieldName2Vc and trs.evalCount = @v_fieldName7Vc and
						(
						(trs.reportType in ('ter1','ter2') and (roundId=0))
							or
						(trs.reportType in ('ter3','ter4') and (roundId=@v_fieldName6Vc))
						)
						and pkgLotId=@v_fieldName3Vc)
					)
				then '1' else '0' end as FieldValue1
		END
END
else if @v_fieldName4Vc ='3' -- TER's count
	begin
		IF @v_fieldName3Vc='0'
		BEGIN
			select case
				when
				(
				(select COUNT(cm.userId)*convert(int,@v_fieldName4Vc) from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
				where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved'  and c.committeeType in ('TEC','PEC'))
				 =
				(select COUNT(trs.torSignId) from tbl_TORRptSign trs Where tenderId=@v_fieldName2Vc and trs.evalCount = @v_fieldName7Vc and 
				(
					(trs.reportType in ('ter1') and (roundId=0))
						or
					(trs.reportType in ('ter2','ter3') and (roundId=@v_fieldName6Vc))
				)
				and pkgLotId=@v_fieldName3Vc)
				 )
				then '1' else '0' end as FieldValue1
		END
		ELSE
		BEGIN
				select case
				when
				(
				(select COUNT(cm.userId)*convert(int,@v_fieldName4Vc) from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
				where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved'  and c.committeeType in ('TEC','PEC'))
				 =
				(select COUNT(trs.torSignId) from tbl_TORRptSign trs Where tenderId=@v_fieldName2Vc and trs.evalCount = @v_fieldName7Vc and
				(
					(trs.reportType in ('ter1','ter2') and (roundId=0))
						or
					(trs.reportType in ('ter3') and (roundId=@v_fieldName6Vc))
				)
				and pkgLotId=@v_fieldName3Vc)
				 )
			then '1' else '0' end as FieldValue1
		END
end
else if @v_fieldName4Vc ='2' -- TER's count
	begin
		IF @v_fieldName3Vc='0'
		BEGIN
			select case
				when
				(
				(select COUNT(cm.userId)*convert(int,@v_fieldName4Vc) from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
				where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved' and c.committeeType in ('TEC','PEC'))
				=
				(select COUNT(trs.torSignId) from tbl_TORRptSign trs Where tenderId=@v_fieldName2Vc and trs.evalCount = @v_fieldName7Vc and
				(
					(trs.reportType in ('ter1') and (roundId=0))
						or
					(trs.reportType in ('ter2') and (roundId=@v_fieldName6Vc))
				)
				and pkgLotId=@v_fieldName3Vc)
				)
			then '1' else '0' end as FieldValue1
		END
		ELSE
		begin
				select case
				when
				(
				(select COUNT(cm.userId)*convert(int,@v_fieldName4Vc) from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
				where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved' and c.committeeType in ('TEC','PEC'))
				=
				(select COUNT(trs.torSignId) from tbl_TORRptSign trs Where tenderId=@v_fieldName2Vc and trs.reportType in ('ter1','ter2') and trs.evalCount = @v_fieldName7Vc 
				 and pkgLotId=@v_fieldName3Vc and roundId=@v_fieldName6Vc)
				)
			then '1' else '0' end as FieldValue1
		END

end
else if @v_fieldName4Vc ='1' -- TER's count
	begin
		select case
	when
		(
		(select COUNT(cm.userId)*convert(int,@v_fieldName4Vc) from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
		where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved' and c.committeeType in ('TEC','PEC'))
		=
		(
		  select COUNT(trs.torSignId) from tbl_TORRptSign trs Where tenderId=@v_fieldName2Vc and trs.reportType in ('ter1') and pkgLotId=@v_fieldName3Vc and trs.evalCount = @v_fieldName7Vc)
		)
	then '1' else '0' end as FieldValue1
end
end

if @v_fieldName1Vc='isTERSentToAA'
begin
	if (@v_fieldName6Vc = null or @v_fieldName6Vc = '')
	BEGIN
		select convert(varchar(50),COUNT(evalRptToAAId)) as FieldValue1 from
		tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
		and envelopeId=@v_fieldName4Vc and roundId = @v_fieldName5Vc
	END
	else
	BEGIN
		--select convert(varchar(50),COUNT(evalRptToAAId)) as FieldValue1 from
		--tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
		--and envelopeId=@v_fieldName4Vc and roundId = @v_fieldName5Vc and evalCount = @v_fieldName6Vc
		if
		(select convert(varchar(50),COUNT(evalRptToAAId)) as FieldValue1 from
		tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
		and envelopeId=@v_fieldName4Vc and roundId = @v_fieldName5Vc and evalCount = @v_fieldName6Vc)=0
		begin
			if
			(select convert(varchar(50),COUNT(evalRptToAAId)) as FieldValue1 from
			tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
			and envelopeId=@v_fieldName4Vc and roundId = @v_fieldName5Vc and evalCount = 0 and rptStatus = 'Pending')=0	
			select  '0' as FieldValue1
			else select '1'  as FieldValue1
			end
		else
		select convert(varchar(50),COUNT(evalRptToAAId)) as FieldValue1 from
		tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
		and envelopeId=@v_fieldName4Vc and roundId = @v_fieldName5Vc and evalCount = @v_fieldName6Vc
		
	END
end
--added by dohatec for re-evauation
if @v_fieldName1Vc='isTERSentToAA1s2eafter'
begin
if exists (select convert(varchar(50),COUNT(evalRptToAAId)) as FieldValue1 from
			tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
			and envelopeId=@v_fieldName4Vc and roundId = @v_fieldName5Vc and rptStatus in ('Approved','Pending') and evalCount = @v_fieldName6Vc
)
	BEGIN
	if ((select evalRptToAAId from
			tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
			and envelopeId <> 0 and roundId <> 0 ) > (select max(evalRptToAAId) from
			tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
			and envelopeId=@v_fieldName4Vc and roundId = @v_fieldName5Vc) 
			)
			
			select '0' as FieldValue1
	else
			select '1' as FieldValue1
	END
else
	select '0' as FieldValue1

end

if @v_fieldName1Vc='isTERSentToAA1s2e'
begin
	if (@v_fieldName6Vc = null or @v_fieldName6Vc = '')
	BEGIN
		select convert(varchar(50),COUNT(evalRptToAAId)) as FieldValue1 from
		tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
		and envelopeId=@v_fieldName4Vc and roundId = @v_fieldName5Vc
	END
	else
	BEGIN

	select convert(varchar(50),COUNT(evalRptToAAId)) as FieldValue1 from
			tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
			and envelopeId=@v_fieldName4Vc and roundId = @v_fieldName5Vc and rptStatus in ('Approved','Pending') --and evalCount = @v_fieldName6Vc
	END
end
--added by dohatec for re-evauation
if @v_fieldName1Vc='isTERSentToAAService'
begin
	select convert(varchar(50),COUNT(evalRptToAAId)) as FieldValue1 from
			tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
			and envelopeId=@v_fieldName4Vc and roundId = @v_fieldName5Vc and rptStatus in ('Approved','Pending')

end

if @v_fieldName1Vc='isTERSentToAAApp'
begin
	select convert(varchar(50),COUNT(evalRptToAAId)) as FieldValue1 from
	tbl_EvalRptSentToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc
	and envelopeId=@v_fieldName4Vc and roundId = @v_fieldName5Vc and rptStatus='Approved'
end

if @v_fieldName1Vc='isTERSentForReview'
begin
	select convert(varchar(50),COUNT(evalFFRptId)) as FieldValue1 from 
	tbl_EvalRptForwardToAA where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc 
	and roundId = @v_fieldName4Vc
end

if @v_fieldName1Vc='isClarificationAsked'
begin
	select  top 1 convert(varchar(50),er.evalRptToAAId) as FieldValue1,
                er.answer AS FieldValue2
	from    tbl_EvalReportClarification er inner join tbl_EvalRptSentToAA es on  er.evalRptToAAId = es.evalRptToAAId
	where   es.tenderId=@v_fieldName2Vc and es.pkgLotId=@v_fieldName3Vc and es.roundId = @v_fieldName4Vc
	order by evalRptClariId desc
end

if @v_fieldName1Vc='isEvaluationWorkFlowCreated'
begin
	SELECT     worfklowId FROM         tbl_WorkFlowLevelConfig
	WHERE     (eventId = 12) AND (moduleId = 2) AND (activityId = 13) AND 
	(objectId = @v_fieldName2Vc) 
end

if @v_fieldName1Vc='evaluationMemberCheck'
begin
	if @v_fieldName4Vc = '1'
	begin
		select convert(varchar(50),COUNT(cm.userId))  as FieldValue1 from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
		where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved'  and c.committeeType in ('TEC','PEC')
		and cm.userId=@v_fieldName3Vc and cm.memberRole in ('cp')
	end
	if @v_fieldName4Vc = '0'
	begin
		select convert(varchar(50),COUNT(cm.userId))  as FieldValue1 from tbl_Committee c inner join tbl_CommitteeMembers cm on c.committeeId = cm.committeeId
		where c.tenderId=@v_fieldName2Vc and c.committeStatus='approved'  and c.committeeType in ('TEC','PEC')
		and cm.userId=@v_fieldName3Vc and cm.memberRole in ('m','ms')
	end
end

If @v_fieldName1Vc='getResponsiveBidderCount'
Begin
	select CONVERT(varchar(20),COUNT(userId)) as FieldValue1
	from tbl_EvalBidderStatus
	where tenderId=@v_fieldName2Vc and bidderStatus='Technically Responsive'
End

If @v_fieldName1Vc='getResponsiveBidderCountAfterNOA'
Begin
 
--Declare @field1 int
--Declare @field2 int


if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
	SET @evalCount = 0
 else
	select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

select @field2 = COUNT(tbl_noaissuedetails.userId)  from tbl_noaacceptance inner join tbl_noaissuedetails 
	on tbl_noaissuedetails.noaissueId = tbl_noaacceptance.noaissueId where tenderId = @v_fieldName2Vc and acceptRejStatus in ('decline','Performance Security not paid')

--if(@field2 = 0)
--	select @field1 = COUNT(userId) from tbl_EvalBidderStatus where tenderId=@v_fieldName2Vc and bidderStatus='Technically Responsive' and evalCount = @evalCount
if ((select TOP 1 DATEDIFF(Minute, ersa.sentDate, tnid.createdDt) from tbl_NoaIssueDetails tnid ,tbl_EvalRptSentToAA  ersa
			where tnid.tenderId = @v_fieldName2Vc and tnid.tenderId = ersa.tenderId and ersa.evalCount != 0 and ersa.evalCount = (select MAX(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc))<0)		
	if(select COUNT(userId) from tbl_EvalBidderStatus where tenderId=@v_fieldName2Vc and bidderStatus in ('Technically Responsive') and evalCount = @evalCount) = 0
		set @field1 = 0
	else
		select @field1 = COUNT(userId) from tbl_EvalBidderStatus where tenderId=@v_fieldName2Vc and bidderStatus in ('Technically Responsive','Technically Unresponsive') and evalCount = @evalCount-1
else 
	select @field1 = COUNT(userId) from tbl_EvalBidderStatus where tenderId=@v_fieldName2Vc and bidderStatus='Technically Responsive' and evalCount = @evalCount	
if (convert(varchar(50),@field1-@field2) < 0)
	set @field2 = 0
else
	set @field2 = convert(varchar(50),@field1-@field2)
select convert(varchar(50),@field1) as FieldValue1,convert(varchar(50),@field2) as FieldValue2
End

if @v_fieldName1Vc='isAllBidderEvaluated'

begin
declare @v_docAvlMethod_Vc varchar(30)
	
SELECT @v_docAvlMethod_Vc = docAvlMethod FROM dbo.tbl_TenderDetails WHERE tenderId = @v_fieldName2Vc
if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
	SET @evalCount = 0
else
	select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

	IF @v_docAvlMethod_Vc = 'Lot'
	BEGIN

		select case when
	(select COUNT(userId) from tbl_EvalBidderStatus  where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc and evalCount =@evalCount)=
	(select COUNT(distinct f.userId) from tbl_FinalSubmission f,tbl_BidderLots b where f.tenderId=@v_fieldName2Vc
	and f.tenderId=b.tenderId and f.userId=b.userid and pkgLotId =@v_fieldName3Vc
	and bidSubStatus='finalsubmission') then '1' else '0' end as FieldValue1

	END
	ELSE
	BEGIN
	IF((select  COUNT(tbl_noaissuedetails.userId)  from tbl_noaacceptance inner join tbl_noaissuedetails 
		on tbl_noaissuedetails.noaissueId = tbl_noaacceptance.noaissueId where tenderId = @v_fieldName2Vc and acceptRejStatus in ('decline','Performance Security not paid')
		) > 0 and @evalCount >0 )
		BEGIN
		 select @field1 = COUNT(evaBidderStatusId) from tbl_EvalBidderStatus ebs,tbl_FinalSubmission f,tbl_NoaIssueDetails tnid 
			where ebs.tenderId = f.tenderId and ebs.evalCount = @evalCount and f.tenderId = @v_fieldName2Vc and f.userId = ebs.userId
			and tnid.tenderId  = @v_fieldName2Vc and ebs.userId not in (select userId from tbl_NoaIssueDetails where tenderId = @v_fieldName2Vc)
		select @field2 = count(userId) from tbl_NoaIssueDetails where tenderId = @v_fieldName2Vc

		select case when
		(Convert(int,@field1)+Convert(int,@field2))=
		(select COUNT(userId) from tbl_FinalSubmission where tenderId=@v_fieldName2Vc
		and bidSubStatus='finalsubmission') then '1' else '0' end as FieldValue1
		END
	else
		BEGIN
		select case when
		(select COUNT(userId) from tbl_EvalBidderStatus where tenderId=@v_fieldName2Vc and evalCount =@evalCount)=
		(select COUNT(userId) from tbl_FinalSubmission where tenderId=@v_fieldName2Vc
		and bidSubStatus='finalsubmission') then '1' else '0' end as FieldValue1
		END
	END

end

If @v_fieldName1Vc='getResponsiveBidderCountService'
Begin
	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
	SET @evalCount = 0
 else
	select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

	select CONVERT(varchar(20),COUNT(userId)) as FieldValue1
	from tbl_EvalBidderStatus
	where tenderId=@v_fieldName2Vc and bidderStatus='evaluated' 
		and result = 'Pass' and evalCount = @evalCount
End

IF @v_fieldName1Vc = 'AllBidderDisqualifiedService'
BEGIN
--Declare @totalCount varchar(50)
if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
	SET @evalCount = 0
 else
	select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

set @totalCount = (SELECT COUNT(DISTINCT CONVERT(varchar(20),negId)) as FieldValue1 
from tbl_Negotiation where negStatus='Failed' and evalCount = @evalCount and
tenderid=@v_fieldName2Vc) 

IF @totalCount  = 
(SELECT COUNT (DISTINCT convert(varchar(20), tbl_FinalSubmission.userId)) as FieldValue1
FROM tbl_FinalSubDetail INNER JOIN tbl_FinalSubmission ON
 tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
 AND tbl_FinalSubDetail.finalSubmissionId = tbl_FinalSubmission.finalSubmissionId
 where tbl_FinalSubmission.bidSubStatus = 'finalsubmission'
  and tenderid = @v_fieldName2Vc
   and userId not in(select userId from tbl_EvalBidderStatus where tenderid=@v_fieldName2Vc and evalCount = @evalCount and
  (isnull(result,'pass')='fail'))
  and tenderFormid in (select tenderFormid from tbl_ReportForms where reportId= @v_fieldName3Vc))
	Begin
		select '1' AS FieldValue1
	END
 ELSE
	Begin
		select '0' AS FieldValue1
	END
 
END

If @v_fieldName1Vc='getRoundsforEvalByMethod'
Begin
	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
	SET @evalCount = 0
	else
	select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc

	select dbo.f_getbiddercompany(br.userId) as FieldValue1 from tbl_EvalRoundMaster em inner join tbl_BidderRank br on em.roundId = br.roundId
	and em.tenderId=@v_fieldName2Vc and reportType=@v_fieldName3Vc and em.evalCount  = @evalCount order by rank asc
End
IF @v_fieldName1Vc = 'isEvalReportSigned'
BEGIN
	select convert(varchar(50),COUNT(torSignId)) as FieldValue1 from
	tbl_TORRptSign where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc and reportType=''+@v_fieldName4Vc+''
	and roundId = @v_fieldName5Vc and evalCount = @v_fieldName6Vc
end

If @v_fieldName1Vc='getTenderReportSignStatus'
		Begin	 		
			 
		 DECLARE @v_CountCommitteeMembers int
		 DECLARE @v_CountSignMembers int    
		 
		 if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
			SET @evalCount = 0
		else
			select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc
    
		 If @v_fieldName5Vc='LTM'
			Begin 
			SET @v_CountCommitteeMembers=(select count(distinct userId) from tbl_Committee tc 
			inner join tbl_CommitteeMembers tcm on tc.committeeId = tcm.committeeId
			where tc.tenderId = @v_fieldName2Vc and tc.committeeType  = @v_fieldName3Vc)

			SET @v_CountSignMembers= (select count(distinct userId) from tbl_TORRptSign 
			where tenderId=@v_fieldName2Vc and reportType = @v_fieldName4Vc and roundId=@v_fieldName6Vc and evalCount = @evalCount )

				if(@v_CountCommitteeMembers=@v_CountSignMembers)
       				select 'Signed' AS FieldValue1
					else  	select '0' AS FieldValue1
		   END 
		 else 
			Begin
			  SET @v_CountCommitteeMembers=(select count(distinct userId) from tbl_Committee tc 
			  inner join tbl_CommitteeMembers tcm on tc.committeeId = tcm.committeeId
			  where tc.tenderId = @v_fieldName2Vc and tc.committeeType  = @v_fieldName3Vc)

			  SET @v_CountSignMembers= (select count(distinct userId) from tbl_TORRptSign 
			  where tenderId=@v_fieldName2Vc and reportType = @v_fieldName4Vc and roundId=@v_fieldName6Vc and evalCount = @evalCount)

			  if(@v_CountCommitteeMembers=@v_CountSignMembers)
       			select 'Signed' AS FieldValue1
			  else  select '0' AS FieldValue1
			END
          	
END
IF @v_fieldName1Vc = 'isTERSignedLabel'
BEGIN

	if(select count(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc)=0
			SET @evalCount = 0
		else
			select @evalCount = max(evalCount) from tbl_EvalRptSentToAA where tenderId = @v_fieldName2Vc
			
	select case when COUNT(torSignId)=0 then 'View and Sign' else 'View' end as FieldValue1
	from tbl_TORRptSign where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc and reportType=''+@v_fieldName4Vc+''
	and userId=@v_fieldName5Vc and roundId = @v_fieldName6Vc and evalCount = @evalCount
END



IF @v_fieldName1Vc = 'getLTMwinningBidder'
BEGIN
	if(SELECT COUNT(distinct evalCount) FROM tbl_EvalRoundMaster WHERE tenderId = @v_fieldName2Vc )=1
	BEGIN
		 select dbo.f_getbiddercompany(erm.userId) AS FieldValue1 , 
		 case	when pq.postQualStatus IS null or pq.postQualStatus='Initiated' THEN 'Pending'
				when pq.postQualStatus IS not  null and (pq.noaStatus = 'pending'  or pq.noaStatus = 'accepted') and pq.postQualStatus='Disqualify' THEN 'Disqualified'
				when pq.postQualStatus IS not  null and (pq.noaStatus = 'pending'  or pq.noaStatus = 'accepted') and pq.postQualStatus!='Disqualify' THEN 'Qualified'
				when pq.postQualStatus IS not  null  and pq.noaStatus != 'rejected'  THEN 'Qualified but tenderer Rejected NOA'
				when pq.postQualStatus IS not  null  and pq.noaStatus != 'Performance Security not paid'  THEN 'Qualified but tenderer did not paid Performance Security'
				end FieldValue2
		 from 
		tbl_EvalRoundMaster erm Left join tbl_PostQualification pq on erm.tenderId = pq.tenderId
		and erm.evalCount = pq.evalCount and erm.userId = pq.userId
		where reportType=@v_fieldName3Vc and erm.tenderId = @v_fieldName2Vc 
	END
	ELSE if(SELECT COUNT(distinct evalCount) FROM tbl_EvalRoundMaster WHERE tenderId = @v_fieldName2Vc )>1
	BEGIN
	select dbo.f_getbiddercompany(erm.userId) AS FieldValue1 , 
		 case	when pq.postQualStatus IS null or pq.postQualStatus='Initiated' THEN 'Pending'
				when pq.postQualStatus IS not  null and (pq.noaStatus = 'pending'  or pq.noaStatus = 'accepted') and pq.postQualStatus='Disqualify' THEN 'Disqualified (Re-Evaluated)'
				when pq.postQualStatus IS not  null and (pq.noaStatus = 'pending'  or pq.noaStatus = 'accepted') and pq.postQualStatus!='Disqualify' THEN 'Qualified (Re-Evaluated)'
				when pq.postQualStatus IS not  null  and pq.noaStatus != 'rejected'  THEN 'Qualified but tenderer Rejected NOA (Re-Evaluated)'
				when pq.postQualStatus IS not  null  and pq.noaStatus != 'Performance Security not paid'  THEN 'Qualified but tenderer did not paid Performance Security (Re-Evaluated)'
				end FieldValue2
		 from 
		tbl_EvalRoundMaster erm Left join tbl_PostQualification pq on erm.tenderId = pq.tenderId
		and erm.evalCount = pq.evalCount and erm.userId = pq.userId
		where reportType=@v_fieldName3Vc and erm.tenderId = @v_fieldName2Vc 
		and erm.evalCount != (SELECT MAX(evalCount) FROM tbl_EvalRoundMaster WHERE tenderId = @v_fieldName2Vc) 
		UNION
		 select dbo.f_getbiddercompany(erm.userId) AS FieldValue1 , 
		 case	when pq.postQualStatus IS null or pq.postQualStatus='Initiated' THEN 'Pending'
				when pq.postQualStatus IS not  null and (pq.noaStatus = 'pending'  or pq.noaStatus = 'accepted') and pq.postQualStatus='Disqualify' THEN 'Disqualified'
				when pq.postQualStatus IS not  null and (pq.noaStatus = 'pending'  or pq.noaStatus = 'accepted') and pq.postQualStatus!='Disqualify' THEN 'Qualified'
				when pq.postQualStatus IS not  null  and pq.noaStatus != 'rejected'  THEN 'Qualified but tenderer Rejected NOA'
				when pq.postQualStatus IS not  null  and pq.noaStatus != 'Performance Security not paid'  THEN 'Qualified but tenderer did not paid Performance Security'
				end FieldValue2
		 from 
		tbl_EvalRoundMaster erm Left join tbl_PostQualification pq on erm.tenderId = pq.tenderId
		and erm.evalCount = pq.evalCount and erm.userId = pq.userId
		where reportType=@v_fieldName3Vc and erm.tenderId = @v_fieldName2Vc 
		and erm.evalCount = (SELECT MAX(evalCount) FROM tbl_EvalRoundMaster WHERE tenderId = @v_fieldName2Vc) 
	END

END
--ahsan
IF @v_fieldName1Vc = 'getEvalRptSent'
BEGIN 
		select rptStatus as FieldValue1 from
		tbl_EvalRptSentTc where tenderId=@v_fieldName2Vc and pkgLotId=@v_fieldName3Vc and roundId = @v_fieldName4Vc	
end

IF @v_fieldName1Vc = 'checkBidderLot'
BEGIN
	if(select count(bidderLotId) from tbl_bidderLots where tenderId=@v_fieldName2Vc and userId=@v_fieldName3Vc)>0
	BEGIN
		SELECT '1' AS FieldValue1
	END
	ELSE
	BEGIN
		SELECT '0' AS FieldValue1
	END
END 
