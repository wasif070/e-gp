USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[spGetTenderDashboardOfflineData]    Script Date: 4/24/2016 11:24:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Istiak
-- Create date: 04.Sep.12
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spGetTenderDashboardOfflineData] 
	(
		@moduleFlag VARCHAR(50),
		@searchFlag VARCHAR(50),		
		@procNature VARCHAR(50), 
		@procType VARCHAR(50),	
		@procMethod VARCHAR(50), 
		@id int,	
		@status VARCHAR(50),	
		@refNo VARCHAR(50),	
		@pubDateFrom VARCHAR(50),	 
		@pubDateTo VARCHAR(50),
		@comments VARCHAR(1000)
	)
AS
BEGIN
	
	Declare 
	@condition VARCHAR(1000),
	@query VARCHAR(max)
	DECLARE @flag_bit bit	
	DECLARE @count int
	SET @condition = ''
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF @moduleFlag = 'dashboard'
		BEGIN
			IF @searchFlag = 'false'
				BEGIN
					SELECT --ROW_NUMBER() OVER (ORDER BY tenderOfflineId) AS RowNum ,
										* FROM (SELECT DISTINCT td.tenderId AS tenderOfflineId, reoiRfpRefNo, procurementNature,
										tenderBrief AS BriefDescription, ministry AS ministryOrDivision, agency, peOfficeName,procurementType, 
										procurementMethod, tenderPubDt AS tenderPubDate, submissionDt AS tenderClosingDate ,
										TenderStatus,eventType,packageDescription as PackageName FROM tbl_TenderDetails td inner join tbl_TenderClose tc on 
										td.tenderId = tc.tenderId ) T ORDER BY tenderOfflineId DESC 	  
				END
			ELSE IF @searchFlag = 'true'
				BEGIN
					IF @procNature is not null and @procNature<>''
						SELECT	@condition = @condition + ' AND procurementNature = '''+@procNature+''''
					IF @procMethod is not null and @procMethod<>''		
						SELECT	@condition = @condition + ' AND procurementMethod = '''+@procMethod+''''
					IF @procType is not null and @procType<>''		
						SELECT	@condition = @condition + ' AND procurementType = '''+@procType+''''
					IF @refNo is not null and @refNo<>''		
						SELECT	@condition = @condition + ' AND reoiRfpRefNo = '''+@refNo+''''				
					IF @id is not null and @id<>''		
						SELECT	@condition = @condition + ' AND tenderOfflineId = '+CONVERT(VARCHAR(50),@id)
											
					IF @pubDateFrom is not null And @pubDateTo is not null and @pubDateFrom <>'' and @pubDateTo <>''
						Begin
							SELECT @condition =@condition + ' And Cast (Floor(Cast (tenderPubdate as Float)) as Datetime)  between ''' + @pubDateFrom + ''' And ''' + @pubDateTo + ''''
						End
					ELSE IF @pubDateFrom is not null and @pubDateFrom <>''
						Begin
							SELECT @condition =@condition + ' And Cast (Floor(Cast (tenderPubdate as Float)) as Datetime) >= ''' + @pubDateFrom + ''''
						End
					ELSE IF @pubDateTo is not null and @pubDateTo <>''
						Begin
							SELECT @condition = @condition + ' And Cast (Floor(Cast (tenderPubdate as Float)) as Datetime) <= ''' + @pubDateTo + ''''
						End		
						
					SELECT @query = 'SELECT --ROW_NUMBER() OVER (ORDER BY tenderOfflineId) AS RowNum ,
					* FROM (SELECT DISTINCT td.tenderId AS tenderOfflineId, reoiRfpRefNo, procurementNature,
										tenderBrief AS BriefDescription, ministry AS ministryOrDivision, agency, peOfficeName,procurementType, 
										procurementMethod, tenderPubDt AS tenderPubDate, submissionDt AS tenderClosingDate ,
										TenderStatus,eventType,packageDescription as PackageName FROM tbl_TenderDetails td inner join tbl_TenderClose tc on 
										td.tenderId = tc.tenderId ) T where TenderStatus = '''+@status +'''' +@condition
					Print (@query)
					Exec (@query)	
				END
		END
	ELSE IF @moduleFlag = 'view'
		BEGIN
			SELECT * FROM tbl_TenderDetailsOffline WHERE tenderOfflineId = @id
		END
	ELSE IF @moduleFlag = 'tenderdelete'
		BEGIN
			BEGIN TRAN
				DELETE FROM tbl_TenderLotPhasingOffline WHERE tenderOfflineId = @id
				DELETE FROM tbl_TenderDetailsOffline WHERE tenderOfflineId = @id
			COMMIT TRAN
			
			SET @flag_bit=1
			SELECT @flag_bit AS flag, 'Tender Offline Data has been deleted.' AS Message
		END
	ELSE IF @moduleFlag = 'tenderapprove'
		BEGIN
			UPDATE tbl_TenderDetailsOffline
			SET TenderStatus = 'Approved', ApproveComments = @comments
			WHERE tenderOfflineId = @id
			
			SET @flag_bit=1
			SELECT @flag_bit AS flag, 'Tender Offline Data has been Approved.' AS Message
		END
	ELSE IF @moduleFlag = 'corrigendum'
		BEGIN
			SELECT @count = COUNT(*) FROM tbl_CorrigendumDetailOffline
			WHERE tenderOfflineId = @id AND CorrigendumStatus = 'Pending'
			
			if(@count > 0)
				BEGIN
					SET @flag_bit=1
				END
			ELSE
				BEGIN
					SET @flag_bit=0
				END
				
			SELECT @flag_bit AS flag, 
				   'Corrigendum.' AS Message, 
				   (SELECT MAX(CorNo) FROM tbl_CorrigendumDetailOffline WHERE tenderOfflineId = @id) AS CorNo
		END
	ELSE IF @moduleFlag = 'viewlot'
		BEGIN
			SELECT lotOrRefNo, lotIdentOrPhasingServ, location, tenderSecurityAmt, startDateTime, completionDateTime
			FROM tbl_TenderLotPhasingOffline
			WHERE tenderOfflineId = @id 
			AND (lotOrRefNo != '' or lotIdentOrPhasingServ!= '' or location!= '' or completionDateTime!= '') 
		END	
	ELSE IF @moduleFlag = 'viewcorrigendum'
		BEGIN
			SELECT * FROM tbl_CorrigendumDetailOffline
			WHERE tenderOfflineId = @id
			ORDER BY CorrigendumStatus ASC, CorNo
		END
	ELSE IF @moduleFlag = 'deletecorrigendum'
		BEGIN
			DELETE FROM tbl_CorrigendumDetailOffline
			WHERE tenderOfflineId = @id AND CorrigendumStatus = 'Pending'
			
			SET @flag_bit=1
			SELECT @flag_bit AS flag, 'Corrigendum Offline Data has been deleted.' AS Message
		END
	ELSE IF @moduleFlag = 'corapprove'
		BEGIN
			UPDATE tbl_CorrigendumDetailOffline
			SET CorrigendumStatus = 'Approved', Comments = @comments, 
				CorNo = 1 + (
					SELECT CorNo
					FROM tbl_CorrigendumDetailOffline
					WHERE tenderOfflineId = @id AND CorrigendumStatus = 'Pending'
					GROUP BY CorNo
				)
			WHERE tenderOfflineId = @id AND CorrigendumStatus = 'Pending'
			
			SET @flag_bit=1
			SELECT @flag_bit AS flag, 'COR (Tender Offline Data) has been Approved.' AS Message
		END
END

