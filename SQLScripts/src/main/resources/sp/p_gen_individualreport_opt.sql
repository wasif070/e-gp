USE [egp_bhutan]
GO
/****** Object:  StoredProcedure [dbo].[p_gen_individualreport_opt]    Script Date: 4/24/2016 10:52:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- alter date: <alter Date,,>
-- Description:	<Description,,>
-- =============================================
-- exec [dbo].[p_gen_individualreport_opt]  27
ALTER PROCEDURE [dbo].[p_gen_individualreport_opt] 
	@v_formId_inInt INT =89,
	@v_userId_inInt INT=164
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	/******* PARAMETERS DECLARATION ***********/
	DECLARE @v_TableNames_Vc VARCHAR(8000),  @v_FormName VARCHAR(100), @v_TableId_Int INT,  @v_ColumnHeader_vc VARCHAR(200),  @v_FormTableHeader VARCHAR(950), @v_ColumnHeaderhtml_vc VARCHAR(max), @v_RowId_Int INT, @v_RowColId_int INT, @v_RowCellValue VARCHAR(8000), @v_CellHtml_Vc VARCHAR(8000),  @v_RowHtml_Vc VARCHAR(8000),  @v_colfill_Int INT, @v_BidTableId_Int INT, @v_FormfinalTableHeader VARCHAR(max) 
	
	SET @v_ColumnHeaderhtml_vc = ''
	SET @v_CellHtml_Vc = ''
	SET @v_RowHtml_Vc =''
	
	/******* SELECT FORMNAME ***********/
	
	SELECT @v_FormName = formName FROM [tbl_TenderForms] WHERE tenderFormId = @v_formId_inInt
	SELECT @v_FormTableHeader=''
	
	/******* SELECT TABLES IN FORMS ***********/
	
	
	
	SELECT @v_TableNames_Vc=  COALESCE(@v_TableNames_Vc+', ', ' ') + convert(VARCHAR(20),tenderTableId) FROM [tbl_TenderTables] WHERE tenderFormId = @v_formId_inInt
	
	---Select @v_TableNames_Vc 	
		
	
DECLARE cur_tblNames CURSOR FAST_FORWARD FOR SELECT tenderTableId, bidTableId FROM 
tbl_TenderBidForm tf,tbl_TenderBidTable tb WHERE tf.bidId = tb.bidid  
and tf.userId = @v_userId_inInt and tf.tenderFormId = @v_formId_inInt
	
			OPEN cur_tblNames        
			
			FETCH NEXT FROM cur_tblNames INTO @v_TableId_Int, @v_BidTableId_Int
						
			print 'STEP 1 : First table Cursor'
			WHILE @@Fetch_status = 0        
			BEGIN 				
			
			IF @v_FormfinalTableHeader <> ''
			BEGIN
			print 'in <>'
					SET @v_FormTableHeader= @v_FormfinalTableHeader + '</table><table border="2" width="100%" bordercolorlight="#A8DA7F" id="table1" style="border-collapse:" bordercolor="#A8DA7F"><tr><td colspan="'+ (SELECT CONVERT(VARCHAR(15), noOfCols) FROM tbl_TenderTables WHERE tenderFormId = @v_formId_inInt and tendertableId = @v_TableId_Int )+'">'+'Table Name'+'</td></tr><tr>'
	
			END
					
			ELSE
			BEGIN
				print 'in else <>'
			SET @v_FormTableHeader= @v_FormTableHeader + '<table border="2" width="100%" bordercolorlight="#A8DA7F" id="table1" style="border-collapse:" bordercolor="#A8DA7F"><tr><td colspan="'+ (SELECT CONVERT(VARCHAR(15), noOfCols) FROM tbl_TenderTables WHERE tenderFormId = @v_formId_inInt and tendertableId = @v_TableId_Int )+'">'+'Table Name'+'</td></tr><tr>'
			END	
					print 'TableID: ' + convert(VARCHAR(200),@v_TableId_Int)
					print 'BidID: ' + convert(VARCHAR(200),@v_BidTableId_Int)
					
					
					SELECT @v_ColumnHeader_vc=  COALESCE(@v_ColumnHeader_vc+'</td><td>', ' ') + CONVERT(VARCHAR(20),columnHeader) FROM tbl_TenderColumns WHERE [tenderTableId] = @v_TableId_Int 
			SET @v_ColumnHeaderhtml_vc =''
			--print @v_ColumnHeader_vc
			SET @v_ColumnHeaderhtml_vc = @v_ColumnHeaderhtml_vc + '<td>'+@v_ColumnHeader_vc+'</td>'
			print 'header td:'+ @v_ColumnHeaderhtml_vc
			SET @v_FormfinalTableHeader= @v_FormTableHeader +@v_ColumnHeaderhtml_vc +'</tr>'
			SET @v_ColumnHeader_vc=null
			SET @v_FormTableHeader = ''
			print 'FormfinalTableHeader: ' + @v_FormfinalTableHeader
							
			
					/* FETCH DATA ROW WISE */
	
	DECLARE cur_rowValues CURSOR FAST_FORWARD FOR 			
SELECT DISTINCT rowid FROM vw_get_tenderbiddata WHERE tenderTableId = @v_TableId_Int ORDER BY rowid			
			OPEN cur_rowValues        
			FETCH NEXT FROM cur_rowValues INTO @v_rowId_Int
			print 'STEP 2 : 2nd Row Cursor'
			print 'in rowid: '+ convert(VARCHAR(20),@v_rowId_Int)
			
			WHILE @@FETCH_STATUS = 0        
			BEGIN 					
				print 'Row id: ' +CONVERT (VARCHAR(50),@v_rowId_Int)						
				SET @v_RowHtml_Vc = @v_RowHtml_Vc + '<tr>'
 			
 				print 'taking COLUMN ID from [tbl_Tender_Columns]'
 					
 					/* FETCH DATA ROW-COLUMN WISE */
 			
 				DECLARE cur_columnId CURSOR FAST_FORWARD FOR 			
				SELECT DISTINCT columnid, filledby FROM tbl_TenderColumns 
				WHERE [tenderTableId] =@v_TableId_Int order by columnid 

				OPEN cur_columnId        
				FETCH NEXT FROM cur_columnId into @v_RowColId_int, @v_colfill_Int  
					
					print 'STEP 3 : 3nd cell Cursor'								
					
					WHILE @@Fetch_status = 0        
					BEGIN 	
					
						print 'Rowid: ' + CONVERT(VARCHAR(50),@v_rowId_Int)
						print 'Colid: ' + CONVERT(VARCHAR(50),@v_RowColId_int)	
				
					IF @v_colfill_Int = 1
					BEGIN
						print 'taking cellrowValue val from [tbl_TenderCells]' 	
 		
						SELECT   @v_RowCellValue = cellvalue
						FROM vw_get_tendercolumndata WHERE tenderTableId =@v_TableId_Int 
						and rowid = @v_rowId_Int and columnid = @v_RowColId_int  
						ORDER BY rowid, columnid		 	
 			
 					END
 					ELSE
 					BEGIN
 						print 'taking cellrowValue val from [tbl_TenderBidDetail]' 		

						SELECT  @v_RowCellValue = cellvalue FROM vw_get_tenderbiddata 
						WHERE tenderTableId =@v_TableId_Int and rowid = @v_rowId_Int 
						and columnid = @v_RowColId_int and bidtableid = @v_BidTableId_Int 
						ORDER BY rowid, columnid
			
 					END
 			
 						print 'Cell Value: '+@v_RowCellValue
			 			
 						SET @v_CellHtml_vc = @v_CellHtml_vc + '<td>'+@v_RowCellValue+'</td>'
 						print @v_CellHtml_vc
 						
 			
 				FETCH NEXT FROM cur_columnId INTO @v_RowColId_int, @v_colfill_Int
				END   
					print 'cell fetch'   
				
						SET @v_RowHtml_Vc = @v_RowHtml_Vc + @v_CellHtml_vc + '</tr>' 				
						PRINT 'row HTML' + @v_RowHtml_Vc 
				CLOSE cur_columnId        
				DEALLOCATE cur_columnId
			
 			print 'row HTML' + @v_RowHtml_Vc 		
 			print 'row fetch'
 			
 			SET @v_CellHtml_vc = ''
 			

 			FETCH NEXT FROM cur_rowValues into @v_rowId_Int
 			     SET @v_FormfinalTableHeader = @v_FormfinalTableHeader + @v_RowHtml_Vc 
				 SET @v_RowHtml_Vc = ''
			END       

			CLOSE cur_rowValues        
			DEALLOCATE cur_rowValues		
			
		  
		 
		  print 'final html'+ @v_FormfinalTableHeader
		  
			
						
			FETCH NEXT FROM cur_tblNames INTO @v_TableId_Int, @v_BidTableId_Int
		  
			END       

			CLOSE cur_tblNames        
			DEALLOCATE cur_tblNames			
			
	
	SELECT @v_FormfinalTableHeader + '</table>' as reportHtml
	SET NOCOUNT OFF
END

