-- FROM ABC
ALTER TABLE tbl_UserPrefrence ALTER COLUMN emailAlert varchar(150)
ALTER TABLE tbl_UserPrefrence ALTER COLUMN smsAlert varchar(150)
-- sms prefmaster update -- FROM ABC
UPDATE tbl_smsPrefMaster SET  usrtTypeID='2,3,12' where prefranceType='Attendance sheet'

-- sms and email prefmaster update queries 10-01-2012 -- FROM ABC

ALTER TABLE tbl_smsPrefMaster ALTER COLUMN govtRoles varchar(150)
ALTER TABLE tbl_emailPrefMaster ALTER COLUMN govtRoles varchar(150) 

-- FROM ABC
update tbl_smsPrefMaster set usrtTypeID='2,3,7,12,15' where prefranceType='Compensation of performance security or New Performance Security'
update tbl_smsPrefMaster set state='M' where prefranceType='e-GP manage users(Registration alert)'
update tbl_emailPrefMaster set state='M' where prefranceType='e-GP manage users(Registration alert)'
update tbl_emailPrefMaster set state='M' where prefranceType='Tender / Proposal Opening Process'
update tbl_emailPrefMaster set usrtTypeID='2,3,7,12' where prefranceType='Compensation of performance security or New Performance Security'
update tbl_emailPrefMaster set usrtTypeID='2,3,7,12' where prefranceType='Performance security release'
update tbl_smsPrefMaster set usrtTypeID='2,3,7,12' where prefranceType='Compensation of performance security or New Performance Security'
update tbl_smsPrefMaster set usrtTypeID='2,3,7,12' where prefranceType='Performance security release'


update tbl_emailPrefMaster set usrtTypeID='2,3,17' where prefranceType = 'Complaint management'
update tbl_emailPrefMaster set usrtTypeID='2,3,7' where prefranceType = 'Compensation of performance security or New Performance Security'
update tbl_emailPrefMaster set usrtTypeID='2,3,7' where prefranceType = 'Performance security release'
update tbl_emailPrefMaster set usrtTypeID='1,3,4,5,8' where prefranceType = 'e-GP manage users(Registration alert)'
update tbl_emailPrefMaster set usrtTypeID='1,3,4,5,8' where prefranceType = 'Forgot and reset password'
update tbl_emailPrefMaster set usrtTypeID='3' where prefranceType = 'Tender / Proposal Opening Process'
update tbl_emailPrefMaster set usrtTypeID='3' where prefranceType in ('Workflow','Opening committee','Evaluation committee')
update tbl_emailPrefMaster set usrtTypeID='2,3' where prefranceType in ('Repeat order','Contract termination','Work completion certificate','Account officer','Invoice generation','work plan','Variation order','Attendance sheet','Pre tender meeting','NOA','Negotiation','Tender/Proposal and Security Validity Extension','Evaluation process(Debriefing on Tender)')
update tbl_smsPrefMaster set usrtTypeID='2,3,17' where prefranceType ='Complaint management'
update tbl_smsPrefMaster set usrtTypeID='2' where prefranceType ='Repeat order'
update tbl_smsPrefMaster set usrtTypeID='2,3,7' where prefranceType ='Compensation of performance security or New Performance Security'
update tbl_smsPrefMaster set usrtTypeID='2,3,7' where prefranceType ='Performance security release'
update tbl_smsPrefMaster set usrtTypeID='2,3' where prefranceType in ('Contract termination','Work completion certificate','Account officer','Invoice generation','work plan','Variation order','Attendance sheet','Pre tender meeting')
update tbl_smsPrefMaster set usrtTypeID='3' where prefranceType in ('Opening committee','Evaluation committee','Workflow')
update tbl_emailPrefMaster set govtRoles='PE,HOPE,Secretary,CCGP' where prefranceType ='Complaint management'
update tbl_smsPrefMaster set govtRoles='PE,HOPE,Secretary,CCGP' where prefranceType ='Complaint management'
update tbl_emailPrefMaster set usrtTypeID='2,3,16,17' where prefranceType ='Complaint management'
update tbl_smsPrefMaster set usrtTypeID='2,3,16,17' where prefranceType ='Complaint management'

-- email and sms queries 2-1-2012 FROM ABC
update tbl_emailPrefMaster set prefranceType='Evaluation process(Debriefing on Tender)' where prefranceType='2,3,17'
update tbl_emailPrefMaster set prefranceType='Evaluation process(Debriefing on Tender)' where prefranceType='Evaluation process{Debriefing on Tender)'
update tbl_emailPrefMaster set usrtTypeID='2,3' where prefranceType='Evaluation process(Debriefing on Tender)'

-- FROM DOHA-TEC ON 22-Jan-2013

ALTER TABLE tbl_TenderDetails ADD pkgDocFeesUSD money
	 
ALTER TABLE tbl_TenderLotSecurity ADD tenderSecurityAmtUSD money

ALTER TABLE tbl_Cms_NewBankGuarnatee ADD currencyName varchar(50)

ALTER TABLE tbl_Cms_WpDetailHistory ADD currencyName varchar(50)

ALTER TABLE tbl_Cms_WpDetailHistory ADD supplierVat decimal(15, 3)

ALTER TABLE tbl_Cms_WpDetailHistory ADD InlandOthers decimal(15, 3)

ALTER TABLE tbl_Cms_WpDetailHistory ADD CONSTRAINT DF_tbl_Cms_WpDetailHistory_supplierVat DEFAULT 0 FOR supplierVat

ALTER TABLE tbl_Cms_WpDetailHistory ADD CONSTRAINT DF_tbl_Cms_WpDetailHistory_InlandOthers DEFAULT 0 FOR InlandOthers

ALTER TABLE tbl_Cms_WpDetail ADD currencyName varchar(50)

ALTER TABLE tbl_Cms_WpDetail ADD supplierVat decimal(15, 3)

ALTER TABLE tbl_Cms_WpDetail ADD InlandOthers decimal(15, 3)

ALTER TABLE tbl_Cms_WpDetail ADD CONSTRAINT DF_tbl_Cms_WpDetail_supplierVat DEFAULT 0 FOR supplierVat

ALTER TABLE tbl_Cms_WpDetail ADD CONSTRAINT DF_tbl_Cms_WpDetail_InlandOthers DEFAULT 0 FOR InlandOthers

ALTER TABLE [tbl_TemplateSectionForm] ADD [FormType] varchar(50)

ALTER TABLE [tbl_TenderForms] ADD [FormType] varchar(50)

ALTER TABLE tbl_CurrencyMaster ADD currencyShortName varchar(50) NULL

DELETE FROM tbl_CurrencyMaster WHERE currencyName <> 'Taka'

ALTER TABLE tbl_NoaIssueDetails
ADD currencyId int

ALTER TABLE tbl_TenderPerfSecurity
ADD currencyId int

UPDATE tbl_CurrencyMaster SET currencyShortName='BDT', [currencySymbol]='TK.'   WHERE currencyName='Taka'

UPDATE tbl_TemplateMaster SET templateName='e-PW2(a) (For GOB Fund only)' WHERE templateName='e-PW2(a)'

-- FORM ABC ON 18-April-2013

Alter table tbl_MarqueeMaster alter column marqueeStartDt datetime NOT NULL;
Alter table tbl_MarqueeMaster alter column marqueeEndDt datetime NOT NULL;
--Alter table tbl_MarqueeMaster alter column createdTime datetime NOT NULL;

-- FORM ABC ON 30-MAY-2013
ALTER Table tbl_deleteUserJob Alter column jobstatus varchar(MAX)

-- FROM DOHATEC ON 01-July-2013
ALTER TABLE tbl_TenderDetailsOffline
alter column packageNo varchar (2000)

ALTER TABLE tbl_TenderDetailsOffline
alter column PackageName varchar (2000)

ALTER TABLE tbl_ContractAwardedOffline
alter column packageNo varchar (2000)

ALTER TABLE tbl_ContractAwardedOffline
alter column PackageName varchar (2000)

ALTER TABLE tbl_ContractAwardedOffline
alter column projectName varchar (2000)

ALTER TABLE tbl_ContractAwardedOffline
alter column NameofTenderer varchar (200)

-- FROM DOHATEC ON 01-SEPTEMBER-2013
ALTER TABLE tbl_TenderDetailsOffline
alter column reoiRfpRefNo varchar (200)

ALTER TABLE tbl_CorrigendumDetailOffline
alter column oldValue varchar (5000)

ALTER TABLE tbl_CorrigendumDetailOffline
alter column newValue varchar (5000)

-- FROM DOHATEC ON 22-january-2014 TO RESOLVE ISSUE 2659
ALTER TABLE tbl_TenderDetails
alter column projectName varchar(500) NULL

ALTER TABLE tbl_ProjectMaster
alter column projectName varchar(500) NOT NULL

ALTER TABLE tbl_AppMaster
alter column projectName varchar(500) NOT NULL

-- FROM DOHATEC ON 01-JANUARY-2014 TO RESOLVE ISSUE 2477
ALTER TABLE tbl_NoaIssueDetails
alter column contractName varchar (1000)
	
 -- FROM DOHATEC ON 12-FEBRUARY-2014 TO RESOLVE ISSUE 2962
ALTER TABLE tbl_CMS_WpDetail alter column groupId varchar (1000)
ALTER TABLE tbl_CMS_WpDetail alter column wpUom varchar (max) not null

-- FROM DOHATEC ON 16-MARCH-2014 TO RESOLVE ISSUE 3146
ALTER TABLE tbl_EvalMemStatus alter column evalNonCompRemarks varchar (2000)

--From GSS edit option for PE in corrigendum
ALTER TABLE tbl_CorrigendumDetail alter column fieldName varchar (100)
ALTER TABLE tbl_CorrigendumDetail alter column oldValue varchar (max)
ALTER TABLE tbl_CorrigendumDetail alter column newValue varchar (max)

--From GSS 14th May-2015
ALTER VIEW [dbo].[vw_get_AppData]
AS
SELECT     am.appId, am.appCode, dm.departmentName, em.employeeName, sm.stateName, ap.procurementnature, ap.packageNo, ap.packageDesc,
                      am.projectName, am.financialYear, CONVERT(int, dm.departmentId) AS departmentId, em.employeeId, ap.cpvCode, ap.procurementType,
                      ap.estimatedCost, ap.pkgEstCost, am.officeId, am.budgetType, pm.procurementMethod, om.officeName, ap.appStatus, ap.packageId,
                      ap.workflowStatus, am.projectId, dm.departmentType, dm.parentDepartmentId
FROM         dbo.tbl_AppMaster AS am INNER JOIN
                      dbo.tbl_AppPackages AS ap ON am.appId = ap.appId INNER JOIN
                      dbo.tbl_ProcurementMethod AS pm ON ap.procurementMethodId = pm.procurementMethodId INNER JOIN
                      dbo.tbl_OfficeMaster AS om ON am.officeId = om.officeId INNER JOIN
                      dbo.tbl_EmployeeMaster AS em ON am.employeeId = em.employeeId INNER JOIN
                      dbo.tbl_DesignationMaster AS ds ON em.designationId = ds.designationId INNER JOIN
                      dbo.tbl_DepartmentMaster AS dm ON ds.departmentid = dm.departmentId INNER JOIN
                      dbo.tbl_StateMaster AS sm ON om.stateId = sm.stateId

GO
-- From Dohatec on 11-May-2015 on TER2 Report
delete from tbl_EvalTERReportCriteria where  criteriaId=25

update  tbl_EvalTERReportCriteria set criteria='Tenderer''s Experience' where criteriaId=22
update  tbl_EvalTERReportCriteria set criteria='Turnover' where criteriaId=23
update  tbl_EvalTERReportCriteria set criteria='Liquid Asset' where criteriaId=24

update  tbl_EvalTERReportCriteria set criteria='Conformity of the Scheduled of Requirements' where criteriaId=26
update  tbl_EvalTERReportCriteria set criteria='Eligibility and Conformity of Materials, Equipments and Services' where criteriaId=27
update  tbl_EvalTERReportCriteria set criteria='Adequacy of Technical Proposal' where criteriaId=28

update [tbl_EvalTERReport] set criteriaId=26 where criteriaId=22
update [tbl_EvalTERReport] set criteriaId=27 where criteriaId=23
update [tbl_EvalTERReport] set criteriaId=28 where criteriaId=24

--To resolve the online tracking id 17607  By Dohatec on 04-August-2015
ALTER TABLE tbl_ReportFormulaMaster  ALTER COLUMN formula varchar(700) 

--Edit by Palash, Dohatec

ALTER TABLE tbl_PostQualification ADD reTenderRecommendetion varchar (5) NULL
GO
--End

--GSS for salvage negative handling in CMS
ALTER TABLE tbl_NoaIssueDetails alter column salvageAmt numeric (20,3) NULL
ALTER TABLE tbl_CMS_InvoiceAccDetails  alter column salvageAdjAmt decimal (15,3) NULL

--End
-- From Dohatec on 10-September-2015 for Re-evaluation
ALTER TABLE tbl_EvalRptSentToAA ADD evalCount INT NOT NULL DEFAULT '0'
ALTER TABLE Tbl_EvalFormQues ADD evalCount INT NOT NULL DEFAULT '0'
ALTER TABLE tbl_EvalTERReport ADD evalCount INT NOT NULL DEFAULT '0'
ALTER TABLE tbl_EvalBidderStatus ADD evalCount INT NOT NULL DEFAULT '0'
ALTER TABLE tbl_EvalRoundMaster ADD evalCount INT NOT NULL DEFAULT '0'
ALTER TABLE tbl_Postqualification ADD evalCount INT NOT NULL DEFAULT '0'
ALTER TABLE tbl_EvalClariDocs ADD evalCount INT NOT NULL DEFAULT '0'
ALTER TABLE tbl_TorRptSign ADD evalCount INT NOT NULL DEFAULT '0'

-- From Dohatec on 10-September-2015 for Re-evaluation Service
ALTER TABLE tbl_EvalSerFormDetail ADD evalCount INT NOT NULL DEFAULT '0'
ALTER TABLE tbl_EvalBidderMarks ADD evalCount INT NOT NULL DEFAULT '0'
ALTER TABLE tbl_Negotiation ADD evalCount INT NOT NULL DEFAULT '0'

--To resolve the online tracking id 20228  By Dohatec on 5-October-2015
ALTER TABLE tbl_EvalConfig ALTER COLUMN evalCommittee varchar(500)

--To resolve the online tracking id 20819  By Dohatec on 15-October-2015
ALTER TABLE tbl_TenderSectionDocs ALTER COLUMN tenderId INT NOT NULL

--To resolve the online tracking id 20819  By Dohatec on 21-October-2015
ALTER TABLE tbl_OfficeMaster Add officeType varchar(50) DEFAULT 'Others' NOT NULL

--To change the password policy By Dohatec on 8-November-2015
update [tbl_ConfigurationMaster] set failedLoginAttempt = 5 where configId in (1,2,6,11)
ALTER TABLE tbl_LoginMaster ADD countResetSMS TINYINT NOT NULL DEFAULT '0'

--To Change Activity Name by Nafiul on 26-Feb-2016
Update [tbl_ActivityMaster] set activityName='Tender Notice And Document Approval Workflow' where activityId=2

-- Insert value into tbl_cascade_link_details and tbl_cascade_link_master table for Tender Committee Formation
    INSERT [tbl_cascade_link_details] ([pageName], [parentLinkList], [userTypeId], [status]) VALUES 
    (N'TenderComm.jsp',	N'4,67,74,963',	N'3',	N'yes'),
    (N'TenderCommFormation.jsp',	N'4,67,74,963,965',	N'3',	N'yes'),
    (N'TenderCommFormation.jsp',	N'4,67,74,963,966',	N'3',	N'yes'),
    (N'TenderCommFormation.jsp',	N'4,67,74,963,967',	N'3',	N'yes'),
    (N'TenderCommFormation.jsp',	N'4,67,74,963,968',	N'3',	N'yes'),
    (N'MapCommittee.jsp',	N'4,67,74,963,969',	N'3',	N'yes')

    INSERT [tbl_cascade_link_master] ([linkName], [parentLinkId], [userTypeId], [hyperLinkPageName]) VALUES
    (N'Tender Committee'	N'74'	N'3'	N'TenderComm.jsp'),
    (N'Edit Committee Details',	N'963',	N'3',	N'TenderCommFormation.jsp'),
    (N'View Committee Details',	N'963',	N'3',	N'TenderCommFormation.jsp'),
    (N'Notify Committee Details',	N'963',	N'3',	N'TenderCommFormation.jsp'),
    (N'Create' ,	N'963',	N'3',	N'TenderCommFormation.jsp'),
    (N'Use Existing Committee',	N'963',	N'3',	N'MapCommittee.jsp')

-- Alter tbl_EmployeeMaster table
    ALTER TABLE tbl_EmployeeMaster ADD userEmployeeId varchar(100) 
    ALTER TABLE tbl_EmployeeMaster ADD ContactAddress varchar(1000)

-- Alter tbl_EmployeeTrasfer table
    ALTER TABLE tbl_EmployeeTrasfer ADD userEmployeeId varchar(100) 
    ALTER TABLE tbl_EmployeeTrasfer ADD ContactAddress varchar(1000)

-- Alter tbl_tempcompanymaster table
    ALTER TABLE tbl_tempcompanymaster workCategory varchar(50)
    ALTER TABLE tbl_tempcompanymaster originCountry varchar(100)
    ALTER TABLE tbl_tempcompanymaster statutoryCertificateNo varchar(100)
    ALTER TABLE tbl_tempcompanymaster tradeLicenseNumber varchar(30)

-- Alter tbl_companymaster table
    ALTER TABLE tbl_companymaster workCategory varchar(50)
    ALTER TABLE tbl_companymaster originCountry varchar(100)
    ALTER TABLE tbl_companymaster statutoryCertificateNo varchar(100)
    ALTER TABLE tbl_companymaster tradeLicenseNumber varchar(30)
    
ALTER TABLE tbl_CompanyMaster ALTER COLUMN companyRegNumber varchar(50) NULL
--

ALTER TABLE tbl_CompanyDocuments ADD ReferenceID varchar(20) NULL 
 
ALTER TABLE tbl_DepartmentMaster ALTER COLUMN city varchar(100) NULL

ALTER TABLE tbl_AppMaster ADD
[appType] [varchar] (50) NULL,
[entrustingAgency] [varchar] (150) NULL,
[refAppId] [int] NULL,
[reviseCount] [int] NULL,
[lastRevisionDate] [datetime] NULL


ALTER TABLE tbl_StateMaster ADD areaCode varchar(30) NULL

UPDATE [egp_stg].[dbo].[tbl_StateMaster]
   SET [areaCode] = 11
 WHERE stateId = 1848
GO

UPDATE [egp_stg].[dbo].[tbl_StateMaster]
   SET [areaCode] = 02
 WHERE stateId = 1849
GO

UPDATE [egp_stg].[dbo].[tbl_StateMaster]
   SET [areaCode] = 13
 WHERE stateId = 1850
GO

UPDATE [egp_stg].[dbo].[tbl_StateMaster]
   SET [areaCode] = 20
 WHERE stateId = 1853
GO

UPDATE [egp_stg].[dbo].[tbl_StateMaster]
   SET [areaCode] = 03
 WHERE stateId = 1858
GO

UPDATE [egp_stg].[dbo].[tbl_StateMaster]
   SET [areaCode] = 09
 WHERE stateId = 1859
GO

UPDATE [egp_stg].[dbo].[tbl_StateMaster]
   SET [areaCode] = 18
 WHERE stateId = 1863
GO

UPDATE [egp_stg].[dbo].[tbl_StateMaster]
   SET [areaCode] = 15
 WHERE stateId = 1864
GO

UPDATE [egp_stg].[dbo].[tbl_StateMaster]
   SET [areaCode] = 07
 WHERE stateId = 1868
GO

UPDATE [egp_stg].[dbo].[tbl_StateMaster]
   SET [areaCode] = 12
 WHERE stateId = 1869
GO

UPDATE [egp_stg].[dbo].[tbl_StateMaster]
   SET [areaCode] = 05
 WHERE stateId = 1871
GO

UPDATE [egp_stg].[dbo].[tbl_StateMaster]
   SET [areaCode] = 08
 WHERE stateId = 1873
GO

UPDATE [egp_stg].[dbo].[tbl_StateMaster]
   SET [areaCode] = 14
 WHERE stateId = 1875
GO

UPDATE [egp_stg].[dbo].[tbl_StateMaster]
   SET [areaCode] = 06
 WHERE stateId = 1876
GO

UPDATE [egp_stg].[dbo].[tbl_StateMaster]
   SET [areaCode] = 17
 WHERE stateId = 1878
GO

UPDATE [egp_stg].[dbo].[tbl_StateMaster]
   SET [areaCode] = 01
 WHERE stateId = 1879
GO

UPDATE [egp_stg].[dbo].[tbl_StateMaster]
   SET [areaCode] = 19
 WHERE stateId = 1881
GO

UPDATE [egp_stg].[dbo].[tbl_StateMaster]
   SET [areaCode] = 16
 WHERE stateId = 1882
GO

UPDATE [egp_stg].[dbo].[tbl_StateMaster]
   SET [areaCode] = 10
 WHERE stateId = 1892
GO

UPDATE [egp_stg].[dbo].[tbl_StateMaster]
   SET [areaCode] = 04
 WHERE stateId = 1899
GO


ALTER TABLE tbl_MandatoryDoc ADD procurementNature varchar(30) NULL

UPDATE [egp_stg].[dbo].[tbl_MandatoryDoc]
   SET [docType] = 'Company Registration Number'
      ,[isMandatory] = 'no'
 WHERE mandatoryDocId = 2
GO


UPDATE [egp_stg].[dbo].[tbl_MandatoryDoc]
   SET [isMandatory] = 'yes'
      ,[procurementNature] = 'All'
 WHERE mandatoryDocId = 5
GO

UPDATE [egp_stg].[dbo].[tbl_MandatoryDoc]
   SET [docType] = 'Valid Value Added Tax (VAT) or Goods and Service Tax(GST) Registration Certificate'
      ,[isMandatory] = 'no'
 WHERE mandatoryDocId = 7
GO

UPDATE [egp_stg].[dbo].[tbl_MandatoryDoc]
   SET [docType] = 'CID No.'
      ,[isMandatory] = 'yes'
       ,[procurementNature] = 'All'
 WHERE mandatoryDocId = 8
GO


UPDATE [egp_stg].[dbo].[tbl_MandatoryDoc]
   SET [docType] = 'e-GP Registration Fee Payment Slip (if Applicable)'
      ,[isMandatory] = 'no'
       ,[procurementNature] = 'All'
 WHERE mandatoryDocId = 15
GO

UPDATE [egp_stg].[dbo].[tbl_MandatoryDoc]
   SET [docType] = 'CDB License'
      ,[isMandatory] = 'yes'
      ,[docTypeValue] = 'CDBLic'
       ,[procurementNature] = 'Works'
 WHERE mandatoryDocId = 34
GO

UPDATE [egp_stg].[dbo].[tbl_MandatoryDoc]
   SET [docType] = 'Company Authorization Letter'
      ,[isMandatory] = 'yes'
       ,[procurementNature] = 'Works'
 WHERE mandatoryDocId = 37
GO


INSERT INTO [egp_stg].[dbo].[tbl_MandatoryDoc]
           ([regType]
           ,[isBangladesh]
           ,[docType]
           ,[docTypeValue]
           ,[isMandatory]
           ,[procurementNature])
     VALUES
           ('company'
           ,'no'
           ,'Statutory Certificate'
           ,'statutorycert'
           ,'no'
           ,'All')
GO

INSERT INTO [egp_stg].[dbo].[tbl_MandatoryDoc]
           ([regType]
           ,[isBangladesh]
           ,[docType]
           ,[docTypeValue]
           ,[isMandatory]
           ,[procurementNature])
     VALUES
           ('company'
           ,'no'
           ,'Company Registration'
           ,'companyreg'
           ,'no'
           ,'All')
GO

INSERT INTO [egp_stg].[dbo].[tbl_MandatoryDoc]
           ([regType]
           ,[isBangladesh]
           ,[docType]
           ,[docTypeValue]
           ,[isMandatory]
           ,[procurementNature])
     VALUES
           ('company'
           ,'no'
           ,'Other'
           ,'other'
           ,'no'
           ,'All')
GO
   

ALTER TABLE tbl_TempCompanyDocuments ADD DocRefNo varchar(250) NULL
ALTER TABLE tbl_TempCompanyDocuments ADD DocDescription varchar(100) NULL

ALTER TABLE tbl_CompanyDocuments ADD DocRefNo varchar(250) NULL
ALTER TABLE tbl_CompanyDocuments ADD DocDescription varchar(100) NULL

ALTER TABLE tbl_TempCompanyMaster ADD regOffSubDistrict varchar(100) NULL
ALTER TABLE tbl_TempCompanyMaster ADD regOffMobileNo varchar(20) NULL
ALTER TABLE tbl_TempCompanyMaster ADD corpOffSubDistrict varchar(100) NULL
ALTER TABLE tbl_TempCompanyMaster  ADD corpOffMobMobileNo varchar(20) NULL

ALTER TABLE tbl_CompanyMaster ADD regOffSubDistrict varchar(100) NULL
ALTER TABLE tbl_CompanyMaster ADD regOffMobileNo varchar(20) NULL
ALTER TABLE tbl_CompanyMaster ADD corpOffSubDistrict varchar(100) NULL
ALTER TABLE tbl_CompanyMaster ADD corpOffMobileNo varchar(20) NULL

ALTER TABLE tbl_TendererMaster ADD subDistrict varchar(100) NULL
ALTER TABLE tbl_TempTendererMaster ADD subDistrict varchar(100) NULL

ALTER TABLE tbl_BiddingPermission ADD IsReapplied int NULL


ALTER TABLE tbl_OfficeMaster ALTER COLUMN city varchar(100) NULL
ALTER TABLE tbl_OfficeMaster ALTER COLUMN upjilla varchar(100) NULL
ALTER TABLE tbl_OfficeMaster ALTER COLUMN postCode varchar(20) NULL


-- alter tbl_TempCompanyMaster by sristy
ALTER TABLE tbl_TempCompanyMaster ALTER COLUMN regOffCity varchar(50)  NULL
ALTER TABLE tbl_TempCompanyMaster ALTER COLUMN regOffUpjilla	varchar(50) NULL
ALTER TABLE tbl_TempCompanyMaster ALTER COLUMN regOffPostcode varchar(15) NULL
ALTER TABLE tbl_TempCompanyMaster ALTER COLUMN regOffPhoneNo	varchar(40) NULL
ALTER TABLE tbl_TempCompanyMaster ALTER COLUMN regOffFaxNo	varchar(40) NULL

ALTER TABLE tbl_TempCompanyMaster ALTER COLUMN corpOffCity varchar(50) NULL
ALTER TABLE tbl_TempCompanyMaster ALTER COLUMN corpOffUpjilla varchar(50) NULL
ALTER TABLE tbl_TempCompanyMaster ALTER COLUMN corpOffPostcode varchar(15) NULL
ALTER TABLE tbl_TempCompanyMaster ALTER COLUMN corpOffPhoneno varchar(40) NULL
ALTER TABLE tbl_TempCompanyMaster ALTER COLUMN corpOffFaxNo varchar(40) NULL
-- end

-- alter tbl_CompanyMaster by sristy
ALTER TABLE tbl_CompanyMaster ALTER COLUMN regOffCity varchar(50)  NULL
ALTER TABLE tbl_CompanyMaster ALTER COLUMN regOffUpjilla	varchar(50) NULL
ALTER TABLE tbl_CompanyMaster ALTER COLUMN regOffPostcode varchar(15) NULL
ALTER TABLE tbl_CompanyMaster ALTER COLUMN regOffPhoneNo	varchar(40) NULL
ALTER TABLE tbl_CompanyMaster ALTER COLUMN regOffFaxNo	varchar(40) NULL

ALTER TABLE tbl_CompanyMaster ALTER COLUMN corpOffCity varchar(50) NULL
ALTER TABLE tbl_CompanyMaster ALTER COLUMN corpOffUpjilla varchar(50) NULL
ALTER TABLE tbl_CompanyMaster ALTER COLUMN corpOffPostcode varchar(15) NULL
ALTER TABLE tbl_CompanyMaster ALTER COLUMN corpOffPhoneno varchar(40) NULL
ALTER TABLE tbl_CompanyMaster ALTER COLUMN corpOffFaxNo varchar(40) NULL

ALTER TABLE tbl_TendererMaster ADD emailAddress varchar(100) NULL

ALTER TABLE tbl_TendererMaster ADD OtherTitle varchar(100) NULL
ALTER TABLE tbl_TempTendererMaster ADD OtherTitle varchar(100) NULL
-- end

-- 29 May 16, by nafiul, Update area code

UPDATE [tbl_StateMaster]
   SET [areaCode] = 07
 WHERE stateId = 1848
GO

UPDATE [tbl_StateMaster]
   SET [areaCode] = 08
 WHERE stateId = 1849
GO

UPDATE [tbl_StateMaster]
   SET [areaCode] = 06
 WHERE stateId = 1850
GO

UPDATE [tbl_StateMaster]
   SET [areaCode] = 03
 WHERE stateId = 1853
GO

UPDATE [tbl_StateMaster]
   SET [areaCode] = 06
 WHERE stateId = 1858
GO

UPDATE [tbl_StateMaster]
   SET [areaCode] = 07
 WHERE stateId = 1859
GO

UPDATE [tbl_StateMaster]
   SET [areaCode] = 06
 WHERE stateId = 1863
GO

UPDATE [tbl_StateMaster]
   SET [areaCode] = 04
 WHERE stateId = 1864
GO

UPDATE [tbl_StateMaster]
   SET [areaCode] = 04
 WHERE stateId = 1868
GO

UPDATE [tbl_StateMaster]
   SET [areaCode] = 05
 WHERE stateId = 1869
GO

UPDATE [tbl_StateMaster]
   SET [areaCode] = 08
 WHERE stateId = 1871
GO

UPDATE [tbl_StateMaster]
   SET [areaCode] = 08
 WHERE stateId = 1873
GO

UPDATE [tbl_StateMaster]
   SET [areaCode] = 02
 WHERE stateId = 1875
GO

UPDATE [tbl_StateMaster]
   SET [areaCode] = 04
 WHERE stateId = 1876
GO

UPDATE [tbl_StateMaster]
   SET [areaCode] = 03
 WHERE stateId = 1878
GO

UPDATE [tbl_StateMaster]
   SET [areaCode] = 03
 WHERE stateId = 1879
GO

UPDATE [tbl_StateMaster]
   SET [areaCode] = 02
 WHERE stateId = 1881
GO

UPDATE [tbl_StateMaster]
   SET [areaCode] = 04
 WHERE stateId = 1882
GO

UPDATE [tbl_StateMaster]
   SET [areaCode] = 02
 WHERE stateId = 1892
GO

UPDATE [tbl_StateMaster]
   SET [areaCode] = 02
 WHERE stateId = 1899
GO

/*31 May 2016 */
UPDATE [egp_stg].[dbo].[tbl_MandatoryDoc]
   SET [docType] = 'Power of Attorney'
      ,[procurementNature] = 'All'
 WHERE mandatoryDocId = 37
GO


ALTER TABLE  tbl_ReportTableMaster ADD pkglotId int NOT NULL

-- update debartment type by sristy
update tbl_DebarmentTypes set debarType='Procuring Agency' where debarTypeId=4 --Procuring Entity
update tbl_DebarmentTypes set debarType='Procuring Organization' where debarTypeId=5 --Procuring Agency/Organization
update tbl_DebarmentTypes set debarType='e-GP System' where debarTypeId=6 --e-GP Portal

-- end

-- Mohsina 9 June 2016--
ALTER TABLE tbl_HolidayMaster ADD Description varchar(150) NULL

INSERT INTO [tbl_ProcurementMethod]
     VALUES
           (19
           ,'LEQ'
           ,'Limited Enquiry')
GO
--

-- Mohsina 11 June 2016--
ALTER TABLE tbl_OfficeMaster ALTER COLUMN phoneNo varchar(250)
ALTER TABLE tbl_OfficeMaster ALTER COLUMN faxNo varchar(250)


UPDATE [tbl_ProcurementMethod]
   SET [procurementFullName] = 'Selection of Independent Individual Consultant'
 WHERE [procurementMethod] = 'IC'
GO

--

-- update tbl_emailPrefMaster where usrtTypeID=2 by sristy 13/06/2016
update tbl_emailPrefMaster set usrtTypeID='3,16,17' where prefranceType='Complaint management'
update tbl_emailPrefMaster set usrtTypeID='3' where prefranceType='Repeat order'
update tbl_emailPrefMaster set usrtTypeID='3,7' where prefranceType='Compensation of performance security or New Performance Security'
update tbl_emailPrefMaster set usrtTypeID='3,7' where prefranceType='Performance security release'
update tbl_emailPrefMaster set usrtTypeID='3' where prefranceType='Contract termination'
update tbl_emailPrefMaster set usrtTypeID='3' where prefranceType='Work completion certificate'
update tbl_emailPrefMaster set usrtTypeID='3' where prefranceType='Account officer'
update tbl_emailPrefMaster set usrtTypeID='3' where prefranceType='Invoice generation'
update tbl_emailPrefMaster set usrtTypeID='3' where prefranceType='work plan'
update tbl_emailPrefMaster set usrtTypeID='3' where prefranceType='Variation order'
update tbl_emailPrefMaster set usrtTypeID='3' where prefranceType='Attendance sheet'
update tbl_emailPrefMaster set usrtTypeID='3' where prefranceType='NOA'
update tbl_emailPrefMaster set usrtTypeID='3' where prefranceType='Negotiation'
update tbl_emailPrefMaster set usrtTypeID='3' where prefranceType='Evaluation process(Debriefing on Tender)'

-- delete tbl_emailPrefMaster where usrtTypeID=2 by sristy
delete from tbl_emailPrefMaster where prefranceType='Sub Contracting'
delete from tbl_emailPrefMaster where prefranceType='Joint Venture'
delete from tbl_emailPrefMaster where prefranceType='Bank payment'
delete from tbl_emailPrefMaster where prefranceType='Contract signing'
delete from tbl_emailPrefMaster where prefranceType='Progress report'
delete from tbl_emailPrefMaster where prefranceType='Commencement Date'
delete from tbl_emailPrefMaster where prefranceType='Delivery Schedule'
delete from tbl_emailPrefMaster where prefranceType='work schedule'

-- update tbl_emailPrefMaster where usrtTypeID=2 by sristy
update tbl_smsPrefMaster set usrtTypeID='3,16,17' where prefranceType='Complaint management'
update tbl_smsPrefMaster set usrtTypeID='3,7' where prefranceType='Compensation of performance security or New Performance Security'
update tbl_smsPrefMaster set usrtTypeID='3,7' where prefranceType='Performance security release'
update tbl_smsPrefMaster set usrtTypeID='3' where prefranceType='Contract termination'
update tbl_smsPrefMaster set usrtTypeID='3' where prefranceType='Work completion certificate'
update tbl_smsPrefMaster set usrtTypeID='3' where prefranceType='Account officer'
update tbl_smsPrefMaster set usrtTypeID='3' where prefranceType='Invoice generation'
update tbl_smsPrefMaster set usrtTypeID='3' where prefranceType='work plan'
update tbl_smsPrefMaster set usrtTypeID='3' where prefranceType='Variation order'
update tbl_smsPrefMaster set usrtTypeID='3' where prefranceType='Attendance sheet'

-- delete tbl_smsPrefMaster where usrtTypeID=2 by sristy
delete from tbl_smsPrefMaster where prefranceType='Repeat order'
delete from tbl_smsPrefMaster where prefranceType='Commencement Date'
delete from tbl_smsPrefMaster where prefranceType='Delivery Schedule'
delete from tbl_smsPrefMaster where prefranceType='Contract signing'
delete from tbl_smsPrefMaster where prefranceType='work schedule'
delete from tbl_smsPrefMaster where prefranceType='Progress report'

-- insert tbl_smsPrefMaster where usrtTypeID=2 by sristy
insert into tbl_smsPrefMaster values ('Corrigendum / Amendment', '2', 'O', null)
insert into tbl_smsPrefMaster values ('Tender/Proposal and Security Validity Extension', '2', 'O', null)
-- end 13/06/2016

-- BOQ Master
CREATE TABLE [tbl_BoQMaster](
	[BoQId] [bigint] IDENTITY(1,1) NOT NULL,
	[Category] [varchar](50) NOT NULL,
	[SubCategory] [varchar](50) NULL,
	[Code] [varchar](50) NULL,
	[Description] [varchar](250) NOT NULL,
	[Unit] [varchar](10) NOT NULL,
 CONSTRAINT [PK_tbl_BoQMaster] PRIMARY KEY CLUSTERED 
(
	[BoQId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

--end BOQ

--for Bid Security Declaration 14 June 2016
ALTER TABLE  tbl_TenderLotSecurity ADD BidSecurityType varchar(2) NULL
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1=Finacial Institution Payment, 2=Bid Security Decalaration, 3=Anyone' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_TenderLotSecurity', @level2type=N'COLUMN',@level2name=N'BidSecurityType'


update tbl_TenderLotSecurity
set BidSecurityType='1'
--end

--ahsan
ALTER TABLE tbl_EvaluationReportDocs
ADD lotId int

--16 June 2016
UPDATE [tbl_ProcurementMethod]
   SET [procurementFullName] = 'Limited Enquiry'
 WHERE [procurementMethod] = 'RFQ'
GO

Delete from tbl_ProcurementMethod
where [procurementMethod] = 'LEQ'
--end

ALTER TABLE tbl_TemplateMandatoryDoc alter column documentName varchar (500);
ALTER TABLE tbl_TenderMandatoryDoc alter column documentName varchar (500);

--Nishith dated; 20th June, 2016

ALTER TABLE tbl_DepartmentMaster alter column phoneNo varchar(250);
ALTER TABLE tbl_DepartmentMaster alter column faxNo varchar(250);

ALTER TABLE tbl_OfficeMaster alter column faxNo varchar(250);
ALTER TABLE tbl_OfficeMaster alter column phoneNo varchar(250);

--Mohsina 21 June 2016
ALTER TABLE tbl_BIdDeclaration alter column CreatedDate smalldatetime;

--nafiul 21 june 2016
alter table tbl_DebarredBiddingPermission ADD DebarredFrom [date] NULL
alter table tbl_DebarredBiddingPermission ADD Comments [varchar](MAX) NULL 

--nafiul 23 june 2016
alter table tbl_DebarredBiddingPermission ADD DebarredDate [date] NULL
alter table tbl_DebarredBiddingPermission ADD DebarredBy [int] NULL 
alter table tbl_DebarredBiddingPermission ADD DebarredReason [varchar(200)] NULL

--nishith 28th June 2016 
UPDATE tbl_ProcurementMethod SET procurementFullName = 'Limited Enquiry' WHERE procurementMethod = 'RFQ'

--nishith 29th June 2016
update tbl_TenderLotSecurity set BidSecurityType='1' where BidSecurityType is null

-- Mohsina 12 July 2016
alter table tbl_NewsMaster ADD fileName [varchar] (250) NULL
alter table tbl_NewsMaster ADD letterNumber [varchar] (250) NULL 

UPDATE tbl_ProcurementMethod SET procurementFullName = 'Limited Enquiry Method' WHERE procurementMethod = 'RFQ'
UPDATE tbl_ProcurementMethod SET procurementFullName = 'Direct Contracting Method' WHERE procurementMethod = 'DPM'

--nafiul 14 jul 2016
ALTER TABLE tbl_TempAppPackages ALTER COLUMN bidderCategory varchar(25)

alter table tbl_ProgrammeMaster ADD createdBy [int] NULL 
ALTER TABLE [dbo].[tbl_ProgrammeMaster]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ProgrammeMaster_tbl_LoginMaster] FOREIGN KEY([createdBy])
REFERENCES [dbo].[tbl_LoginMaster] ([userId])
GO

ALTER TABLE [dbo].[tbl_ProgrammeMaster] CHECK CONSTRAINT [FK_tbl_ProgrammeMaster_tbl_LoginMaster]
GO

--aprojit 8th August 2016
update tbl_ReportColumnMaster set colHeader = 'Quoted Amount (in Nu.)' where colHeader = 'Quoted Amount (in BDT)'



--sristy 10 Aug 2016
alter table tbl_TempTendererMaster  ADD UserType [int] NULL
alter table tbl_TendererMaster ADD UserType [int] NULL
alter table tbl_MandatoryDoc ADD userType [int] NULL

UPDATE [tbl_MandatoryDoc] SET [userType] = 0
UPDATE [tbl_MandatoryDoc] SET [userType] = 2, [isMandatory]='yes', [procurementNature]=null WHERE docType='Power of Attorney'
INSERT [tbl_MandatoryDoc]  VALUES('company', 'no','Self Declaration', 'SelfDeclaration', 'yes', NULL,1)
UPDATE [tbl_TendererMasrer] SET [UserType] = 0
UPDATE [tbl_TempTendererMasrer] SET [UserType] = 0

--rased 28 aug 2016
alter table tbl_BoQMaster ALTER COLUMN Category varchar(250) NULL
alter table tbl_BoQMaster ALTER COLUMN SubCategory varchar(250) NULL
alter table tbl_BoQMaster ALTER COLUMN Unit varchar(50) NULL

--nafiul 6 sep 2016
alter table tbl_DebarredBiddingPermission ALTER COLUMN DebarredDate [smalldatetime] NULL
alter table tbl_DebarredBiddingPermission ALTER COLUMN DebarredFrom [smalldatetime] NULL
alter table tbl_DebarredBiddingPermission ALTER COLUMN DebarredUpto [smalldatetime] NULL

alter table tbl_DebarredBiddingPermission  ADD ReinstateComments varchar(MAX) NULL
alter table tbl_DebarredBiddingPermission  ADD IsActive [int] NULL


--FEROZ 02 OCT 2016
alter table tbl_DesignationMaster ALTER COLUMN designationName varchar(250)

--SRISTY 04 OCT 2016
UPDATE tbl_DepartmentMaster SET departmentName='PemaGatshel' WHERE departmentId=203

--SRiSTY 23 OCT 2016
update tbl_BiddingPermission set ProcurementCategory='Services' where ProcurementCategory='Service'
update tbl_TempBiddingPermission set ProcurementCategory='Services' where ProcurementCategory='Service'

--Mohsina 7 Nov 2016
alter table tbl_AppMaster  ADD activityName varchar(MAX) NULL

--aprojit 15 Nov 2016
ALTER TABLE tbl_FinalSubmission ADD otp varchar(10) NULL


--FEROZ, DOHATEC NEW MEDIA, 1st February 2017
--Tender Document was not being shown
alter table tbl_TenderForms alter column formHeader nvarchar(MAX)
--for deleting duplicate value
DELETE FROM [dbo].[tbl_IttClause]  WHERE ittClauseId = 1371

--mohsina 2 Feb 2017
INSERT [dbo].[tbl_ConfigDocFees] ([docAccess], [isDocFeesReq], [procurementMethodId], [procurementTypeId], [tenderTypeId], [isPerSecFeesReq], [isTenderSecReq], [isTenderValReq]) VALUES (N'open', N'No', 12, 1, 5, N'No', N'No', N'No')

--aprojit, 05-02-2017
ALTER TABLE tbl_FinalSubmission ADD otpDateTime smalldatetime NULL


--Nitish 23-02-2017
ALTER TABLE tbl_ConfigNoa ADD loiNoa smallint

--mohsina 23 Feb 2017
update tbl_ConfigDocFees set isDocFeesReq='Yes',  isPerSecFeesReq='Yes', isTenderSecReq='Yes', isTenderValReq='Yes'
where procurementMethodId =14 --DCM configuration

delete from [dbo].[tbl_ConfigEvalMethod]
where tenderTypeId in(6,7,8,9) -- remove RFA, 1 stage-TSTM,2 stage-TSTM, 2 Stage-PQ  configuration

delete from tbl_ConfigProcurement
where tenderTypeId in(6,7,8,9) -- remove RFA, 1 stage-TSTM,2 stage-TSTM, 2 Stage-PQ  configuration

delete from tbl_ConfigPreTender
where tenderTypeId in(6,7,8,9) -- remove RFA, 1 stage-TSTM,2 stage-TSTM, 2 Stage-PQ  configuration

delete from tbl_ConfigDocFees
where tenderTypeId in(6,7,8,9) -- remove RFA, 1 stage-TSTM,2 stage-TSTM, 2 Stage-PQ  configuration



--nafiul 26 Feb 2017
ALTER TABLE tbl_EvalRptSentTC ADD evalCount TINYINT NOT NULL DEFAULT '0'.

--ahsan 05 March 2017
ALTER TABLE tbl_TemplateMaster ADD foreignCurrency varchar(5) NOT NULL DEFAULT 'No'

--ahsan 07 march 2017
update tbl_CurrencyMaster set currencyName = 'Bhutanese Ngultrum', currencySymbol = 'Nu.' , currencyShortName = 'BTN' where currencyId = 2

-- Masud 12 March 2017
UPDATE tbl_TemplateMaster SET stdFor='Royal Government of Bhutan'
UPDATE tbl_TemplateMaster SET templateName='Selection Based on Consultant&#8217;s Qualification (CQS) - (For value up to Nu. 1.0 Million)'
Where templateId=90

--Mohsina 12 March 2017 (updated procurement type from 'srvcmp' to 'srvindi')
update [dbo].[tbl_TemplateMaster]
set procType='srvindi'
where templateName='Standard Request for Expression of Interest (SREoI)- Individual Consultant-(Time-based and Lumpsum)'
--end

-- Mohsina 27 March 2017 (deleted multilang content containing Bangla fonts excepts the top 7 Dzongkha menu)
delete from [dbo].[tbl_MultiLangContent]
where [language] !='en_US' 
and [contentId] not in (38, 41, 42, 62, 192, 194, 225)
-- end

-- Mohsina 10 April 2017  (deleted configuration related to 'PQ' )
delete from tbl_ConfigStd
where tenderTypeId = 1
-- end

-- Mohsina: delete Area Column as it is not needed in this table 27 April 2017
ALTER TABLE tbl_ConfigProcurement DROP Column Area;

--aprojit create table tbl_ConfigPaThreshold 05 june 2017

CREATE TABLE [dbo].[tbl_ConfigPaThreshold](
	[configPAThresholdId] [int] IDENTITY(1,1) NOT NULL,
	[area] [varchar](15) NOT NULL,
	[procurementNatureId] [tinyint] NOT NULL,
	[maxValue] [numeric](19, 2) NOT NULL,
	[minValue] [numeric](19, 2) NOT NULL,
 CONSTRAINT [PK_tbl_ConfigPAThreshold] PRIMARY KEY CLUSTERED 
(
	[configPAThresholdId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

--end


--FEROZ 02 NOV 2017
--Content Admin not able to input more than 100 characters while returning for modification to bidders
alter table tbl_TendererAuditTrail ALTER COLUMN activity varchar(1050)

--Date: 28.03.2018
ALTER TABLE tbl_ReportFormulaMaster
ALTER COLUMN formula varchar(MAX);

ALTER TABLE tbl_ReportFormulaMaster
ALTER COLUMN displayFormula varchar(MAX);

--10th July, 2018
--Extend the validity of bidder registration by 50 years
UPDATE tbl_LoginMaster SET validUpTo = DATEADD(yyyy,50,validUpTo)
where userId in (select userId from tbl_TendererMaster)


--aprojit 05-August-2018 (Reference: TOR 1.3.3)
delete from tbl_HintQuestionMaster where hintQuestionId > 5


