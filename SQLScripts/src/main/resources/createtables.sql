CREATE TABLE [tbl_cascade_link_details](
	linkDetailsId int IDENTITY(1,1) NOT NULL,
	pageName varchar(150) NULL,
	parentLinkList varchar(150) NULL,
	userTypeId varchar(60) NULL,
	status varchar(150) NULL,
 CONSTRAINT PK_tbl_cascade_link_details PRIMARY KEY CLUSTERED
(
	linkDetailsId ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [tbl_cascade_link_master](
	linkId int IDENTITY(1,1) NOT NULL,
	linkName varchar(150) NULL,
	parentLinkId varchar(150) NULL,
	userTypeId varchar(60) NULL,
	hyperLinkPageName varchar(60) NULL,
 CONSTRAINT PK_tbl_cascade_link_master PRIMARY KEY CLUSTERED
(
	linkId ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

--  FOR TBL_EMAILPREFMASTER
CREATE TABLE [tbl_emailPrefMaster](
	[epid] [int] IDENTITY(1,1) NOT NULL,
	[prefranceType] [varchar](150) NULL,
	[usrtTypeID] [varchar](60) NULL,
	[state] [varchar](10) NULL,
	[govtRoles] [varchar](20) NULL,
 CONSTRAINT [PK_tbl_emailPrefMaster] PRIMARY KEY CLUSTERED 
(
	[epid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

-- FOR TBL_SMSPREFMASTER
CREATE TABLE [tbl_smsPrefMaster](
	[spid] [int] IDENTITY(1,1) NOT NULL,
	[prefranceType] [varchar](150) NULL,
	[usrtTypeID] [varchar](60) NULL,
	[state] [varchar](10) NULL,
	[govtRoles] [varchar](20) NULL,
 CONSTRAINT [PK_tbl_smsPrefMaster] PRIMARY KEY CLUSTERED 
(
	[spid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

--for offline data entry
CREATE TABLE [tbl_TenderDetailsOffline](
	[tenderOfflineId] [int] IDENTITY(1,1) NOT NULL,
	[ministryOrDivision] [varchar](150) NOT NULL,
	[agency] [varchar](150) NOT NULL,
	[peOfficeName] [varchar](150) NOT NULL,
	[peCode] [varchar](15) NULL,
	[peDistrict] [varchar](100) NOT NULL,
	[eventType] [varchar](30) NOT NULL,
	[invitationFor] [varchar](50) NULL,
	[reoiRfpFor] [varchar](30) NULL,
	[reoiRfpRefNo] [varchar](50) NOT NULL,
	[IssueDate] [smalldatetime] NULL,
	[procurementMethod] [varchar](40) NOT NULL,
	[procurementNature] [varchar](10) NOT NULL,
	[procurementType] [varchar](5) NOT NULL,
	[budgetType] [varchar](30) NOT NULL,
	[sourceOfFund] [varchar](100) NOT NULL,
	[devPartners] [varchar](300) NULL,
	[projectCode] [varchar](150) NULL,
	[projectName] [varchar](150) NULL,
	[packageNo] [varchar](50) NOT NULL,
	[PackageName] [varchar](150) NULL,
	[tenderPubDate] [smalldatetime] NULL,
	[preTenderREOIDate] [smalldatetime] NULL,
	[PreTenderREOIPlace] [varchar](2000) NULL,
	[LastSellingDate] [smalldatetime] NULL,
	[ClosingDate] [smalldatetime] NULL,
	[OpeningDate] [smalldatetime] NULL,
	[SellingAddPrinciple] [varchar](2000) NULL,
	[SellingAddOthers] [varchar](2000) NULL,
	[ReceivingAdd] [varchar](2000) NULL,
	[OpeningAdd] [varchar](2000) NULL,
	[EligibilityCriteria] [varchar](2000) NULL,
	[BriefDescription] [varchar](2000) NULL,
	[RelServicesOrDeliverables] [varchar](2000) NULL,
	[OtherDetails] [varchar](2000) NULL,
	[ForeignFirm] [varchar](20) NULL,
	[DocumentPrice] [money] NULL,
	[TenderStatus] [varchar](15) NULL,
	[peName] [varchar](200) NOT NULL,
	[peDesignation] [varchar](200) NOT NULL,
	[peAddress] [varchar](5000) NOT NULL,
	[peContactDetails] [varchar](1000) NOT NULL,
	[UserID] [int] NULL,
	[InsertUpdateDate] [smalldatetime] NULL,
	[ApproveComments] [varchar](1000) NULL,
 CONSTRAINT [tenderOfflineId_PK] PRIMARY KEY CLUSTERED 
(
	[tenderOfflineId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [tbl_TenderDetailsOffline] ADD  CONSTRAINT [DF_tbl_TenderDetailsOffline_TenderStatus]  DEFAULT ('Pending') FOR [TenderStatus]
GO


CREATE TABLE [tbl_TenderLotPhasingOffline](
	[tenderLotPhaId] [int] IDENTITY(1,1) NOT NULL,
	[tenderOfflineId] [int] NOT NULL,
	[lotOrRefNo] [varchar](150) NOT NULL,
	[lotIdentOrPhasingServ] [varchar](2000) NOT NULL,
	[location] [varchar](100) NOT NULL,
	[tenderSecurityAmt] [money] NOT NULL,
	[startDateTime] [varchar](100) NULL,
	[completionDateTime] [varchar](100) NOT NULL,
 CONSTRAINT [tenderLotSecIdOfflineNew_PK] PRIMARY KEY CLUSTERED 
(
	[tenderLotPhaId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [tbl_CorrigendumDetailOffline](
	[corrigendumOLId] [int] IDENTITY(1,1) NOT NULL,
	[tenderOfflineId] [int] NOT NULL,
	[LotorPhaseIdentity] [varchar](200) NULL,
	[fieldName] [varchar](50) NOT NULL,
	[oldValue] [varchar](500) NOT NULL,
	[newValue] [varchar](500) NOT NULL,
	[CorrigendumStatus] [varchar](15) NULL,
	[CorNo] [int] NULL,
	[Comments] [varchar](500) NULL,
	[userID] [int] NULL,
	[insertUpdateDate] [smalldatetime] NULL,
 CONSTRAINT [corrigendumOLId_PK] PRIMARY KEY CLUSTERED 
(
	[corrigendumOLId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [tbl_ContractAwardedOffline](
	[conAwardOfflineId] [int] IDENTITY(1,1) NOT NULL,
	[ministry] [varchar](150) NOT NULL,
	[division] [varchar](150) NOT NULL,
	[agency] [varchar](150) NOT NULL,
	[peOfficeName] [varchar](150) NOT NULL,
	[peCode] [varchar](15) NOT NULL,
	[peDistrict] [varchar](100) NOT NULL,
	[AwardForPNature] [varchar](30) NOT NULL,
	[RefNo] [varchar](50) NOT NULL,
	[procurementMethod] [varchar](150) NOT NULL,
	[budgetType] [varchar](30) NOT NULL,
	[sourceOfFund] [varchar](100) NOT NULL,
	[devPartners] [varchar](300) NULL,
	[projectCode] [varchar](150) NULL,
	[projectName] [varchar](150) NULL,
	[packageNo] [varchar](100) NOT NULL,
	[packageName] [varchar](150) NOT NULL,
	[DateofAdvertisement] [smalldatetime] NULL,
	[DateofNOA] [smalldatetime] NULL,
	[DateofContractSign] [smalldatetime] NULL,
	[DateofPCCompletion] [smalldatetime] NULL,
	[Sold] [varchar](2000) NOT NULL,
	[Received] [varchar](2000) NOT NULL,
	[Response] [varchar](2000) NOT NULL,
	[DescriptionofContract] [varchar](2000) NOT NULL,
	[ContractValue] [money] NOT NULL,
	[NameofTenderer] [varchar](15) NOT NULL,
	[AddressofTenderer] [varchar](1000) NOT NULL,
	[DeliveryPlace] [varchar](1000) NOT NULL,
	[IsSamePersonNOA] [smallint] NULL,
	[NOAReason] [varchar](300) NULL,
	[IsPSecurityDueTime] [smallint] NULL,
	[PSecurityReason] [varchar](300) NULL,
	[IsSignedDueTime] [smallint] NULL,
	[SignedReason] [varchar](300) NULL,
	[OfficerName] [varchar](300) NOT NULL,
	[OfficerDesignation] [varchar](200) NOT NULL,
	[UserID] [int] NULL,
	[Date] [smalldatetime] NULL,
	[Status] [varchar](50) NULL,
	[Comment] [varchar](500) NULL,
 CONSTRAINT [conAwardOfflineId_PK] PRIMARY KEY CLUSTERED 
(
	[conAwardOfflineId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[tbl_EvalRptForwardToAA]    ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_EvalRptForwardToAA](
	[evalFFRptId] [int] IDENTITY(1,1) NOT NULL,
	[tenderId] [int] NOT NULL,
	[pkgLotId] [int] NOT NULL,
	[roundId] [int] NOT NULL,
	[rptStatus] [varchar](50) NOT NULL,
	[sentByUserId] [int] NOT NULL,
	[sentByGovUserId] [int] NOT NULL,
	[sentDate] [smalldatetime] NOT NULL,
	[sentUserRemarks] [varchar](2000) NOT NULL,	
	[actionUserRole] [varchar](15) NOT NULL,
	[actionUserId] [int] NOT NULL,
	[actionGovUserId] [int] NOT NULL,	
	[action]  [varchar](25) NOT NULL,	
	[actionDt] [smalldatetime] NOT NULL,
	[actionUserRemarks] [varchar](2000) NOT NULL,
 CONSTRAINT [PK_tbl_EvalRptForwardToAA] PRIMARY KEY CLUSTERED 
(
	[evalFFRptId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbl_EvalRptForwardToAA]  WITH CHECK ADD  CONSTRAINT [evalRptForwardToAATenderId_FK1] FOREIGN KEY([tenderId])
REFERENCES [dbo].[tbl_TenderMaster] ([tenderId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  Table [dbo].[tbl_EvalRptForwardToAA]    ******/

-- FROM DOHA-TEC  ON 22-JAN-2013

CREATE TABLE [dbo].[tbl_ConfigDocumentFees](
	[DocFeeID] [int] NOT NULL,
	[ProcurementType] [varchar](10) NOT NULL,
	[MaximumDocAmtBDT] [int] NOT NULL,
	[MaximumDocAmtUSD] [int] NULL,
 CONSTRAINT [PK_tbl_ConfigDocumentFees] PRIMARY KEY CLUSTERED 
(
	[DocFeeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- Constrains Pasrt Start Form here
ALTER TABLE [dbo].[tbl_EvalRptForwardToAA] CHECK CONSTRAINT [evalRptForwardToAATenderId_FK1]
GO

ALTER TABLE [dbo].[tbl_EvalRptForwardToAA] ADD  CONSTRAINT [DF_tbl_EvalRptForwardToAA_actionUserRole]  DEFAULT ('Reviewer') FOR [actionUserRole]
GO

ALTER TABLE [dbo].[tbl_EvalRptForwardToAA] ADD  CONSTRAINT [DF_tbl_EvalRptForwardToAA_pkgLotId]  DEFAULT ((0)) FOR [pkgLotId]
GO

ALTER TABLE [dbo].[tbl_EvalRptForwardToAA] ADD  CONSTRAINT [DF_tbl_EvalRptForwardToAA_rptStatus]  DEFAULT ('Pending') FOR [rptStatus]
GO

ALTER TABLE [dbo].[tbl_EvalRptForwardToAA] ADD  CONSTRAINT [DF_tbl_EvalRptForwardToAA_roundId]  DEFAULT ((1)) FOR [roundId]
GO

-- FROM GSS 18-FEB-2013

/****** Object:  Table [dbo].[tbl_AdvertisementConfig]    Script Date: 02/18/2013 18:25:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_AdvertisementConfig](
	[adConfigId] [int] IDENTITY(1,1) NOT NULL,
	[userId] [varchar](200) NULL,
	[location] [varchar](200) NOT NULL,
	[bannerSize] [varchar](200) NOT NULL,
	[duration] [varchar](200) NOT NULL,
	[instructions] [varchar](2000) NOT NULL,
	[fileFormat] [varchar](200) NOT NULL,
	[dimensions] [varchar](200) NOT NULL,
	[createDate] [smalldatetime] NOT NULL,
	[fee] [money] NULL,
 CONSTRAINT [PK_tbl_AdvertisementConfig] PRIMARY KEY CLUSTERED 
(
	[adConfigId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[tbl_Advertisement]    Script Date: 02/18/2013 18:25:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_Advertisement](
	[adId] [int] IDENTITY(1,1) NOT NULL,
	[userName] [varchar](200) NOT NULL,
	[bannerName] [varchar](200) NOT NULL,
	[description] [varchar](200) NOT NULL,
	[comments] [varchar](2000) NULL,
	[duration] [varchar](200) NOT NULL,
	[address] [varchar](200) NOT NULL,
	[createDate] [smalldatetime] NOT NULL,
	[paymentDate] [smalldatetime] NULL,
	[approvalDate] [smalldatetime] NULL,
	[publishDate] [smalldatetime] NULL,
	[expiryDate] [smalldatetime] NULL,
	[location] [varchar](100) NOT NULL,
	[dimension] [varchar](100) NOT NULL,
	[bannerSize] [varchar](100) NOT NULL,
	[price] [money] NULL,
	[organization] [varchar](100) NOT NULL,
	[eMailId] [varchar](200) NOT NULL,
	[phoneNumber] [varchar](100) NOT NULL,
	[url] [varchar](500) NOT NULL,
	[status] [varchar](500) NOT NULL,
	[transId] [varchar](200) NULL,
	[userId] [int] NULL,
 CONSTRAINT [PK_tbl_Advertisement] PRIMARY KEY CLUSTERED 
(
	[adId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[tbl_PaymentExtensionActionRequest]    Script Date: 10/20/2014 17:04:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_PaymentExtensionActionRequest](
	[requestId] [int] IDENTITY(1,1) NOT NULL,
	[paymentId] [int] NOT NULL,
	[requestBy] [int] NOT NULL,
	[requestTo] [int] NOT NULL,
	[requestAction] [varchar](100) NOT NULL,
	[requestComments] [varchar](1000) NOT NULL,
	[actionStatus] [varchar](50) NOT NULL,
	[actionCompDt] [datetime] NOT NULL,
	[actionComments] [varchar](1000) NOT NULL,
	[requestDate] [datetime] NOT NULL,
	[OldValidityDate] [datetime] NOT NULL,
	[NewValidityDate] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_PaymentExtensionActionRequest] PRIMARY KEY CLUSTERED 
(
	[requestId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

-- For eGP Admin From Dohatec (Istiak) - 07 June 2015
/****** Object:  Table [dbo].[tbl_ProcurementApprovalTimeline]    Script Date: 06/07/2015 16:05:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ProcurementApprovalTimeline](
	[RuleId] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalAuthority] [varchar](128) NOT NULL,
	[ProcurementNature] [varchar](50) NOT NULL,
	[NoOfDaysForTSC] [int] NOT NULL,
	[NoOfDaysForTECorPEC] [int] NOT NULL,
	[TotalNoOfDaysForEvaluation]  AS ([NoOfDaysForTSC]+[NoOfDaysForTECorPEC]),
	[NoOfDaysForApproval] [int] NOT NULL,
 CONSTRAINT [PK_tbl_ProcurementApprovalTimeline] PRIMARY KEY CLUSTERED
(
	[RuleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO

-- Added by Palash--
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_AdminTransfer](
	[admintransId] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NOT NULL,
	[fullName] [varchar](150) NULL,
	[nationalId] [varchar](50) NULL,
	[adminId] [smallint] NOT NULL,
	[emailId] [varchar](150) NULL,
	[transferedBy] [int] NULL,
	[transferDt] [datetime] NULL,
	[replacedBy] [varchar](200) NULL,
	[replacedByEmailId] [varchar](200) NULL,
	[comments] [varchar](2000) NULL,
	[isCurrent] [varchar](50) NULL,
	[mobileNo] [varchar](20) NULL,
 CONSTRAINT [PK_tbl_AdminTransfer] PRIMARY KEY CLUSTERED 
(
	[admintransId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbl_AdminTransfer]  WITH CHECK ADD  CONSTRAINT [FK_tbl_AdminTransfer_tbl_LoginMaster] FOREIGN KEY([userId])
REFERENCES [dbo].[tbl_LoginMaster] ([userId])
GO

ALTER TABLE [dbo].[tbl_AdminTransfer] CHECK CONSTRAINT [FK_tbl_AdminTransfer_tbl_LoginMaster]
GO
-- End, Palash--

-- Financial Delegation For eGP Admin From Dohatec (Istiak) - 15 June 2015
/****** Object:  Table [dbo].[tbl_FinancialDelegation]    Script Date: 06/14/2015 10:46:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_FinancialDelegation](
	[FinancialDeligationId] [int] IDENTITY(1,1) NOT NULL,
	[BudgetType] [varchar](50) NOT NULL,
	[TenderType] [varchar](50) NOT NULL,
	[ProcurementMethod] [varchar](50) NOT NULL,
	[ProcurementType] [varchar](50) NULL,
	[ProcurementNature] [varchar](50) NOT NULL,
	[TenderEmergency] [varchar](50) NOT NULL,
	[MinValueBDT] [numeric](19, 2) NOT NULL,
	[MaxValueBDT] [numeric](19, 2) NOT NULL,
	[ApprovingAuthority] [varchar](50) NOT NULL,
	[MinProjectValueBDT] [numeric](19, 2) NOT NULL,
	[MaxProjectValueBDT] [numeric](19, 2) NOT NULL,
	[IsBoD] [nvarchar](3) NOT NULL,
	[IsCorporation] [nvarchar](3) NOT NULL,
	[PEOfficeLevel] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_tbl_FinancialDelegation] PRIMARY KEY CLUSTERED
(
	[FinancialDeligationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO

-- For eGP Admin From Dohatec (Istiak) - 08 July 2015
/****** Object:  Table [dbo].[tbl_DelegationDesignationRank]    Script Date: 07/08/2015 12:09:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_DelegationDesignationRank](
	[RankId] [int] IDENTITY(1,1) NOT NULL,
	[ApprovingAuthority] [varchar](128) NOT NULL,
 CONSTRAINT [PK_tbl_DelegationDesignationRank] PRIMARY KEY CLUSTERED
(
	[RankId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_DelegationDesignationRank]    End ******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_ReportDocs](
	[reportDocId] [int] IDENTITY(1,1) NOT NULL,
	[documentName] [varchar](250) NOT NULL,
	[docDescription] [varchar](500) NOT NULL,
	[docSize] [varchar](50) NOT NULL,
	[uploadedDate] [smalldatetime] NOT NULL,
	[userId] [int] NOT NULL,
	[status] [varchar](5) NULL,
 CONSTRAINT [PK_tbl_ReportDocs] PRIMARY KEY CLUSTERED 
(
	[reportDocId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
-- End, Palash--

/* GSS APP revision temporary tables and view June-2015*/

/****** Object:  Table [dbo].[tbl_TempAppPackages]    Script Date: 06/15/2015 16:02:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_TempAppPackages](
	[packageId] [int] NOT NULL,
	[appId] [int] NOT NULL,
	[procurementnature] [varchar](10) NOT NULL,
	[servicesType] [varchar](100) NOT NULL,
	[packageNo] [varchar](50) NOT NULL,
	[packageDesc] [varchar](2000) NOT NULL,
	[allocateBudget] [money] NOT NULL,
	[estimatedCost] [money] NOT NULL,
	[cpvCode] [varchar](8000) NOT NULL,
	[pkgEstCost] [money] NOT NULL,
	[approvingAuthEmpId] [int] NOT NULL,
	[isPQRequired] [varchar](10) NOT NULL,
	[reoiRfaRequired] [varchar](10) NOT NULL,
	[procurementMethodId] [tinyint] NOT NULL,
	[procurementType] [varchar](10) NOT NULL,
	[sourceOfFund] [varchar](200) NOT NULL,
	[appStatus] [varchar](20) NOT NULL,
	[workflowStatus] [varchar](20) NOT NULL,
	[noOfStages] [tinyint] NOT NULL,
	[pkgUrgency] [varchar](20) NULL,
	[pkgEstCode] [numeric](19, 2) NULL,
 CONSTRAINT [packageId_PKTemp] PRIMARY KEY CLUSTERED
(
	[packageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[tbl_TempAppPackages]  ends ****/

/****** Object:  Table [dbo].[tbl_TempAppPkgLots]    Script Date: 06/15/2015 16:04:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_TempAppPkgLots](
	[appPkgLotId] [int] NOT NULL,
	[packageId] [int] NOT NULL,
	[appId] [int] NOT NULL,
	[lotNo] [varchar](150) NOT NULL,
	[lotDesc] [varchar](2000) NOT NULL,
	[quantity] [numeric](18, 2) NULL,
	[unit] [varchar](50) NULL,
	[lotEstCost] [money] NOT NULL,
 CONSTRAINT [PK_tbl_AppPkgLotsTemp] PRIMARY KEY CLUSTERED
(
	[appPkgLotId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[tbl_TempAppPkgLots] ENDS *****/


/****** Object:  Table [dbo].[tbl_TempAppPqTenderDates]    Script Date: 06/15/2015 16:05:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbl_TempAppPqTenderDates](
	[pqDtId] [int] NOT NULL,
	[appid] [int] NOT NULL,
	[packageId] [int] NOT NULL,
	[advtDt] [date] NULL,
	[advtDays] [smallint] NOT NULL,
	[subDt] [date] NULL,
	[subDays] [smallint] NOT NULL,
	[openDt] [date] NULL,
	[openDays] [smallint] NOT NULL,
	[evalRptDt] [date] NULL,
	[evalRptDays] [smallint] NOT NULL,
	[appLstDt] [date] NULL,
	[tenderAdvertDt] [date] NULL,
	[tenderAdvertDays] [smallint] NOT NULL,
	[tenderSubDt] [date] NULL,
	[tenderSubDays] [smallint] NOT NULL,
	[tenderOpenDt] [date] NULL,
	[tenderOpenDays] [smallint] NOT NULL,
	[techSubCmtRptDt] [date] NULL,
	[techSubCmtRptDays] [smallint] NOT NULL,
	[tenderEvalRptDt] [date] NULL,
	[tenderEvalRptdays] [smallint] NOT NULL,
	[tenderEvalRptAppDt] [date] NULL,
	[tenderEvalRptAppDays] [smallint] NOT NULL,
	[tenderContractAppDt] [date] NULL,
	[tenderContractAppDays] [smallint] NOT NULL,
	[tenderNoaIssueDt] [date] NULL,
	[tenderNoaIssueDays] [smallint] NOT NULL,
	[tenderContractSignDt] [date] NULL,
	[tenderContractSignDays] [smallint] NOT NULL,
	[tenderContractCompDt] [date] NULL,
	[reoiReceiptDt] [date] NULL,
	[reoiReceiptDays] [smallint] NOT NULL,
	[rfpTechEvalDt] [date] NULL,
	[rfpTechEvalDays] [smallint] NOT NULL,
	[rfpFinancialOpenDt] [date] NULL,
	[rfpFinancialOpenDays] [smallint] NOT NULL,
	[rfpNegCompDt] [date] NULL,
	[rfpNegCompDays] [smallint] NOT NULL,
	[rfpContractAppDt] [date] NULL,
	[rfpContractAppDays] [smallint] NOT NULL,
	[rfaAdvertDt] [date] NULL,
	[rfaAdvertDays] [smallint] NOT NULL,
	[rfaReceiptDt] [date] NULL,
	[rfaReceiptDays] [smallint] NOT NULL,
	[rfaEvalDt] [date] NULL,
	[rfaEvalDays] [smallint] NOT NULL,
	[rfaInterviewDt] [date] NULL,
	[rfaInterviewDays] [smallint] NOT NULL,
	[rfaFinalSelDt] [date] NULL,
	[rfaFinalSelDays] [smallint] NOT NULL,
	[rfaEvalRptSubDt] [date] NULL,
	[rfaEvalRptSubDays] [smallint] NOT NULL,
	[rfaAppConsultantDt] [date] NULL,
	[actAdvtDt] [date] NULL,
	[actSubDt] [date] NULL,
	[actEvalRptDt] [date] NULL,
	[actAppLstDt] [date] NULL,
	[actTenderAdvertDt] [date] NULL,
	[actTenderSubDt] [date] NULL,
	[actTenderOpenDt] [date] NULL,
	[actTechSubCmtRptDt] [date] NULL,
	[actTenderEvalRptDt] [date] NULL,
	[acttenderEvalRptAppDt] [date] NULL,
	[actTenderContractAppDt] [date] NULL,
	[actTenderNoaIssueDt] [date] NULL,
	[actTenderContractSignDt] [date] NULL,
	[actTenderContractCompDt] [date] NULL,
	[actReoiReceiptDt] [date] NULL,
	[actRfpTechEvalDt] [date] NULL,
	[actRfpFinancialOpenDt] [date] NULL,
	[actRfpNegComDt] [date] NULL,
	[actRfpContractAppDt] [date] NULL,
	[actRfaAdvertDt] [date] NULL,
	[actRfaReceiptDt] [date] NULL,
	[actRfaEvalDt] [date] NULL,
	[actRfaInterviewDt] [date] NULL,
	[actRfaFinalSelDt] [date] NULL,
	[actRfaEvalRptSubDt] [date] NULL,
	[actRfaAppConsultantDt] [date] NULL,
	[pkgPubDate] [smalldatetime] NULL,
 CONSTRAINT [pqDtId_PKTemp] PRIMARY KEY CLUSTERED
(
	[pqDtId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[tbl_TempAppPqTenderDates]  ENDS ****/

/****** Object:  View [dbo].[vw_app_temppackagedetails]    Script Date: 06/15/2015 16:08:58 ******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vw_app_temppackagedetails]
AS
SELECT     dbo.tbl_AppMaster.appCode, dbo.tbl_AppMaster.financialYear, dbo.tbl_AppMaster.budgetType, dbo.tbl_AppMaster.projectName,
                      dbo.tbl_TempAppPackages.procurementnature, dbo.tbl_TempAppPackages.servicesType, dbo.tbl_TempAppPackages.packageNo, dbo.tbl_TempAppPackages.packageDesc,
                      dbo.tbl_TempAppPackages.allocateBudget, dbo.tbl_TempAppPackages.pkgEstCost, dbo.tbl_TempAppPackages.isPQRequired, dbo.tbl_TempAppPackages.reoiRfaRequired,
                      dbo.tbl_TempAppPackages.procurementMethodId, dbo.tbl_TempAppPackages.procurementType, dbo.tbl_TempAppPackages.sourceOfFund,
                      dbo.tbl_TempAppPqTenderDates.advtDt, dbo.tbl_TempAppPqTenderDates.subDt, dbo.tbl_TempAppPqTenderDates.tenderAdvertDt,
                      dbo.tbl_TempAppPqTenderDates.tenderSubDt, dbo.tbl_TempAppPqTenderDates.tenderOpenDt, dbo.tbl_TempAppPqTenderDates.tenderNoaIssueDt,
                      dbo.tbl_TempAppPqTenderDates.tenderContractSignDt, dbo.tbl_TempAppPqTenderDates.tenderContractCompDt, dbo.tbl_EmployeeMaster.employeeName AS PE,
                      dbo.tbl_StateMaster.stateName AS district, dbo.tbl_TempAppPqTenderDates.techSubCmtRptDt, dbo.tbl_TempAppPqTenderDates.tenderEvalRptDt,
                      dbo.tbl_TempAppPqTenderDates.tenderEvalRptAppDt, dbo.tbl_TempAppPqTenderDates.tenderContractAppDt, dbo.tbl_TempAppPqTenderDates.reoiReceiptDt,
                      dbo.tbl_TempAppPqTenderDates.rfpTechEvalDt, dbo.tbl_TempAppPqTenderDates.rfpFinancialOpenDt, dbo.tbl_TempAppPqTenderDates.rfpNegCompDt,
                      dbo.tbl_TempAppPqTenderDates.rfpContractAppDt, dbo.tbl_TempAppPqTenderDates.rfaAdvertDt, dbo.tbl_TempAppPqTenderDates.rfaReceiptDt,
                      dbo.tbl_TempAppPqTenderDates.rfaEvalDt, dbo.tbl_TempAppPqTenderDates.rfaInterviewDt, dbo.tbl_TempAppPqTenderDates.rfaFinalSelDt,
                      dbo.tbl_TempAppPqTenderDates.rfaEvalRptSubDt, dbo.tbl_TempAppPqTenderDates.rfaAppConsultantDt, dbo.tbl_TempAppPqTenderDates.actSubDt,
                      dbo.tbl_TempAppPqTenderDates.actAdvtDt, dbo.tbl_TempAppPqTenderDates.actEvalRptDt, dbo.tbl_TempAppPqTenderDates.actAppLstDt,
                      dbo.tbl_TempAppPqTenderDates.actTenderAdvertDt, dbo.tbl_TempAppPqTenderDates.actTenderSubDt, dbo.tbl_TempAppPqTenderDates.actTenderOpenDt,
                      dbo.tbl_TempAppPqTenderDates.actTechSubCmtRptDt, dbo.tbl_TempAppPqTenderDates.actTenderEvalRptDt, dbo.tbl_TempAppPqTenderDates.acttenderEvalRptAppDt,
                      dbo.tbl_TempAppPqTenderDates.actTenderContractAppDt, dbo.tbl_TempAppPqTenderDates.actTenderNoaIssueDt,
                      dbo.tbl_TempAppPqTenderDates.actTenderContractSignDt, dbo.tbl_TempAppPqTenderDates.actTenderContractCompDt,
                      dbo.tbl_TempAppPqTenderDates.actReoiReceiptDt, dbo.tbl_TempAppPqTenderDates.actRfpTechEvalDt, dbo.tbl_TempAppPqTenderDates.actRfpFinancialOpenDt,
                      dbo.tbl_TempAppPqTenderDates.actRfpNegComDt, dbo.tbl_TempAppPqTenderDates.actRfpContractAppDt, dbo.tbl_TempAppPqTenderDates.actRfaAdvertDt,
                      dbo.tbl_TempAppPqTenderDates.actRfaReceiptDt, dbo.tbl_TempAppPqTenderDates.actRfaEvalDt, dbo.tbl_TempAppPqTenderDates.actRfaInterviewDt,
                      dbo.tbl_TempAppPqTenderDates.actRfaFinalSelDt, dbo.tbl_TempAppPqTenderDates.actRfaEvalRptSubDt, dbo.tbl_TempAppPqTenderDates.actRfaAppConsultantDt,
                      dbo.tbl_TempAppPqTenderDates.evalRptDt, dbo.tbl_TempAppPackages.packageId, dbo.tbl_TempAppPqTenderDates.appLstDt, dbo.tbl_TempAppPqTenderDates.subDays,
                      dbo.tbl_TempAppPqTenderDates.advtDays, dbo.tbl_TempAppPqTenderDates.openDays, dbo.tbl_TempAppPqTenderDates.openDt,
                      dbo.tbl_TempAppPqTenderDates.evalRptDays, dbo.tbl_TempAppPqTenderDates.tenderAdvertDays, dbo.tbl_TempAppPqTenderDates.tenderSubDays,
                      dbo.tbl_TempAppPqTenderDates.tenderOpenDays, dbo.tbl_TempAppPqTenderDates.techSubCmtRptDays, dbo.tbl_TempAppPqTenderDates.tenderEvalRptdays,
                      dbo.tbl_TempAppPqTenderDates.tenderEvalRptAppDays, dbo.tbl_TempAppPqTenderDates.tenderContractAppDays,
                      dbo.tbl_TempAppPqTenderDates.tenderNoaIssueDays, dbo.tbl_TempAppPqTenderDates.tenderContractSignDays, dbo.tbl_TempAppPqTenderDates.rfaAdvertDays,
                      dbo.tbl_TempAppPqTenderDates.rfaReceiptDays, dbo.tbl_TempAppPqTenderDates.rfaEvalDays, dbo.tbl_TempAppPqTenderDates.rfaInterviewDays,
                      dbo.tbl_TempAppPqTenderDates.rfaFinalSelDays, dbo.tbl_TempAppPqTenderDates.rfaEvalRptSubDays, dbo.tbl_TempAppPqTenderDates.rfpContractAppDays,
                      dbo.tbl_TempAppPqTenderDates.rfpNegCompDays, dbo.tbl_TempAppPqTenderDates.rfpFinancialOpenDays, dbo.tbl_TempAppPqTenderDates.rfpTechEvalDays,
                      dbo.tbl_TempAppPqTenderDates.reoiReceiptDays, dbo.tbl_TempAppPqTenderDates.pqDtId, dbo.tbl_TempAppPackages.appId, dbo.tbl_AppMaster.projectId,
                      dbo.tbl_TempAppPackages.approvingAuthEmpId, dbo.tbl_TempAppPackages.pkgUrgency, dbo.tbl_TempAppPackages.cpvCode
FROM         dbo.tbl_AppMaster INNER JOIN
                      dbo.tbl_TempAppPackages ON dbo.tbl_AppMaster.appId = dbo.tbl_TempAppPackages.appId INNER JOIN
                      dbo.tbl_TempAppPqTenderDates ON dbo.tbl_AppMaster.appId = dbo.tbl_TempAppPqTenderDates.appid AND
                      dbo.tbl_TempAppPackages.packageId = dbo.tbl_TempAppPqTenderDates.packageId INNER JOIN
                      dbo.tbl_OfficeMaster ON dbo.tbl_AppMaster.officeId = dbo.tbl_OfficeMaster.officeId INNER JOIN
                      dbo.tbl_StateMaster ON dbo.tbl_OfficeMaster.stateId = dbo.tbl_StateMaster.stateId AND
                      dbo.tbl_OfficeMaster.stateId = dbo.tbl_StateMaster.stateId INNER JOIN
                      dbo.tbl_EmployeeMaster ON dbo.tbl_AppMaster.employeeId = dbo.tbl_EmployeeMaster.employeeId


GO

/****** Object:  View [dbo].[vw_app_temppackagedetails]  ENDS *****/


/*** GSS APP revision temporary tables and view June-2015   ENDS ****/

GO

/** Added For Framework Contract **/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_AppFrameworkOffice](
	[FrameWorkOfficeId] [bigint] IDENTITY(1,1) NOT NULL,
	[APPId] [int] NOT NULL,
	[PackageID] [int] NOT NULL,
	[PEOfficeID] [int] NOT NULL,
	[OfficeName] [varchar](150) NOT NULL,
	[State] [varchar](100) NULL,
	[Address] [varchar](150) NULL,
 CONSTRAINT [PK_tbl_AppFrameworkOffice] PRIMARY KEY CLUSTERED 
(
	[FrameWorkOfficeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbl_AppFrameworkOffice]  WITH CHECK ADD  CONSTRAINT [FK_Pkg_Framework] FOREIGN KEY([PackageID])
REFERENCES [dbo].[tbl_AppPackages] ([packageId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tbl_AppFrameworkOffice] CHECK CONSTRAINT [FK_Pkg_Framework]
GO

/****** Object:  Table [dbo].[tbl_TenderFrameWork]    Script Date: 04/26/2016 18:02:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_TenderFrameWork](
	[tenderFrameWorkOfficeId] [bigint] IDENTITY(1,1) NOT NULL,
	[APPId] [int] NOT NULL,
	[PackageId] [int] NOT NULL,
	[TenderId] [int] NOT NULL,
	[PEOfficeId] [int] NOT NULL,
	[PEOfficeName] [varchar](150) NOT NULL,
	[OfficeDistrict] [varchar](100) NOT NULL,
        [OfficeAddress] [varchar](150) NOT NULL, 
	[ItemName] [varchar](max) NOT NULL,
	[Quantity] [float] NOT NULL,
        [Unit] [varchar](20) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [int] NOT NULL,
 CONSTRAINT [PK_tbl_TenderFrameWork] PRIMARY KEY CLUSTERED 
(
	[tenderFrameWorkOfficeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

--added by ahsan
/****** Object:  Table [dbo].[tbl_EvaluationReportDocs]    Script Date: 04/26/2016 10:12:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_EvaluationReportDocs](
	[evalDocId] [int] IDENTITY(1,1) NOT NULL,
	[documentName] [varchar](250) NOT NULL,
	[docDescription] [varchar](500) NOT NULL,
	[docSize] [varchar](10) NOT NULL,
	[uploadedDate] [smalldatetime] NOT NULL,
	[uploadedBy] [int] NOT NULL,
	[tenderId] [int] NOT NULL,
	[roundId] [int] NOT NULL,
 CONSTRAINT [PK_tbl_EvaluationReportDocs] PRIMARY KEY CLUSTERED 
(
	[evalDocId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


--added by ahsan
/****** Object:  Table [dbo].[tbl_EvalRptSentTC]    Script Date: 04/26/2016 10:11:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_EvalRptSentTC](
	[evalRptTCId] [int] IDENTITY(1,1) NOT NULL,
	[tenderId] [int] NOT NULL,
	[pkgLotId] [int] NOT NULL,
	[roundId] [int] NOT NULL,
	[sentByUserId] [int] NOT NULL,
	[sentToUserId] [int] NOT NULL,
	[sentDate] [smalldatetime] NOT NULL,
	[remarks] [varchar](500) NOT NULL,
	[rptStatus] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tbl_evalRptSentTC] PRIMARY KEY CLUSTERED 
(
	[evalRptTCId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[tbl_BiddingPermission]    Script Date: 05/07/2016 16:36:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_BiddingPermission](
	[BiddingPermissionId] [bigint] IDENTITY(1,1) NOT NULL,
	[ProcurementCategory] [varchar](10) NOT NULL,
	[WorkType] [varchar](50) NULL,
	[WorkCategroy] [varchar](50) NULL,
	[CompanyID] [int] NOT NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_tbl_BiddingPermission] PRIMARY KEY CLUSTERED 
(
	[BiddingPermissionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'if Individual Consultant selected Then company ID 1 , Otherwise bidder Company ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_BiddingPermission', @level2type=N'COLUMN',@level2name=N'CompanyID'
GO


/****** Object:  Table [dbo].[tbl_AppPermission]    Script Date: 05/09/2016 00:05:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_AppPermission]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_AppPermission]
GO

USE [egp_stg]
GO

/****** Object:  Table [dbo].[tbl_AppPermission]    Script Date: 05/09/2016 00:05:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_AppPermission](
	[AppPermissionid] [bigint] IDENTITY(1,1) NOT NULL,
	[packageId] [int] NOT NULL,
	[WorkType] [varchar](50) NULL,
	[WorkCategroy] [varchar](50) NULL,
 CONSTRAINT [PK_tbl_AppPermission] PRIMARY KEY CLUSTERED 
(
	[AppPermissionid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



/****** Object:  Table [dbo].[tbl_TempBiddingPermission]    Script Date: 05/08/2016 12:09:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_TempBiddingPermission](
	[BiddingPermissionId] [bigint] IDENTITY(1,1) NOT NULL,
	[ProcurementCategory] [varchar](10) NOT NULL,
	[WorkType] [varchar](50) NULL,
	[WorkCategroy] [varchar](50) NULL,
	[CompanyID] [int] NOT NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_tbl_TempBiddingPermission] PRIMARY KEY CLUSTERED 
(
	[BiddingPermissionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_TempDescription', @value=N'if Individual Consultant selected Then company ID 1 , Otherwise bidder Company ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tbl_BiddingPermission', @level2type=N'COLUMN',@level2name=N'CompanyID'
GO

USE [egp_stg]
GO

/****** Object:  Table [dbo].[tbl_APPReviseConfiguration]    Script Date: 05/09/2016 17:45:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_APPReviseConfiguration](
	[appConfgId] [int] IDENTITY(1,1) NOT NULL,
	[financialYear] [varchar](30) NOT NULL,
	[startDate] [smalldatetime] NOT NULL,
	[endDate] [smalldatetime] NOT NULL,
	[maxReviseCount] [int] NOT NULL,
	[userId] [int] NOT NULL,
	[lastUpdatedDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_tbl_APPReviseConfiguration] PRIMARY KEY CLUSTERED 
(
	[appConfgId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_TCPermission](
	[TCPermissionId] [bigint] NOT NULL,
	[FromUserId] [int] NOT NULL,
	[ToUserId] [int] NOT NULL,
	[TenderId] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[Status] [varchar](10) NOT NULL,
	[Comments] [varchar](250) NOT NULL,
 CONSTRAINT [PK_tbl_TCPermission] PRIMARY KEY CLUSTERED 
(
	[TCPermissionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[tbl_BIdDeclaration]    Script Date: 06/18/2016 12:45:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_BIdDeclaration](
	[DeclarationId] [bigint] IDENTITY(1,1) NOT NULL,
	[TenderId] [int] NOT NULL,
	[LotId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[CreatedDate] [date] NOT NULL,
	[Remarks] [varchar](250) NULL,
 CONSTRAINT [PK_tbl_BIdDeclaration] PRIMARY KEY CLUSTERED 
(
	[DeclarationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[tbl_DebarredBiddingPermission]    Script Date: 19-Jun-16 2:45:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_DebarredBiddingPermission](
	[BiddingPermissionId] [bigint] IDENTITY(1,1) NOT NULL,
	[ProcurementCategory] [varchar](10) NOT NULL,
	[WorkType] [varchar](50) NULL,
	[WorkCategroy] [varchar](50) NULL,
	[CompanyID] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[IsReapplied] [int] NULL,
	[DebarredUpto] [date] NULL,
 CONSTRAINT [PK_tbl_DebarredBiddingPermission] PRIMARY KEY CLUSTERED 
(
	[BiddingPermissionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

-- mohsina 27 Feb 2017
-- Clarification date configuration based on SBD
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ConfigClarification](
	[configClarificationId] [int] IDENTITY(1,1) NOT NULL,
	[templateId] [smallint] NOT NULL,
	[clarificationDays] [smallint] NOT NULL,
 CONSTRAINT [PK_tbl_ConfigClarification] PRIMARY KEY CLUSTERED 
(
	[configClarificationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[tbl_ConfigClarification]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ConfigClarification_tbl_TemplateMaster] FOREIGN KEY([templateId])
REFERENCES [dbo].[tbl_TemplateMaster] ([templateId])
GO
ALTER TABLE [dbo].[tbl_ConfigClarification] CHECK CONSTRAINT [FK_tbl_ConfigClarification_tbl_TemplateMaster]
GO

/****** Object:  Table [dbo].[tbl_ContractNOARelation]    Script Date: 3/12/2017 4:53:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbl_ContractNOARelation](
	[ContractNOAId] [bigint] IDENTITY(1,1) NOT NULL,
	[ContractID] [int] NOT NULL,
	[NOAID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[TenderID] [int] NOT NULL,
 CONSTRAINT [PK_ContractNOARelation] PRIMARY KEY CLUSTERED 
(
	[ContractNOAId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


-- Mohsina /****** Object:  Table [dbo].[tbl_ConfigPaThreshold]    Script Date: 27-Apr-17 3:43:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ConfigPaThreshold](
	[configPAThresholdId] [int] IDENTITY(1,1) NOT NULL,
	[area] [varchar](15) NOT NULL,
	[procurementNatureId] [tinyint] NOT NULL,
	[maxValue] [numeric](19, 2) NOT NULL,
	[minValue] [numeric](19, 2) NOT NULL,
 CONSTRAINT [PK_tbl_ConfigPAThreshold] PRIMARY KEY CLUSTERED 
(
	[configPAThresholdId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbl_ConfigPaThreshold]  WITH CHECK ADD  CONSTRAINT [FK_tbl_ConfigPAThreshold_tbl_ProcurementNature] FOREIGN KEY([procurementNatureId])
REFERENCES [dbo].[tbl_ProcurementNature] ([procurementNatureId])
GO
ALTER TABLE [dbo].[tbl_ConfigPaThreshold] CHECK CONSTRAINT [FK_tbl_ConfigPAThreshold_tbl_ProcurementNature]
GO
--end


-- Mohsina Object:  Table [dbo].[tbl_BhutanDebarmentCommittee]    Script Date: 27-Apr-17 3:43:31 PM 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_BhutanDebarmentCommittee](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[memberName] [varchar](200) NOT NULL,
	[designation] [varchar](100) NOT NULL,
	[role] [varchar](100) NOT NULL,
	[emailId] [varchar](100) NOT NULL,
	[mobileNo] [varchar](20) NOT NULL,
	[nationalId] [varchar](30) NOT NULL,
	[createdBy] [int] NOT NULL,
	[contactAddress] [varchar](1000) NOT NULL,
	[fromDate] [smalldatetime] NOT NULL,
	[toDate] [smalldatetime] NOT NULL,
	[createDate] [smalldatetime] NOT NULL,
	[isActive] [bit] NOT NULL,
	[remarks] [varchar](max) NULL,
 CONSTRAINT [PK_tbl_DebarmentCommittee1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO


/*Object:  Table [dbo].[tbl_faqBhutan]    Script Date: 07-Jun-17 10:55:05 AM */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_faqBhutan](
	[faqID] [int] IDENTITY(1,1) NOT NULL,
	[question] [varchar](max) NOT NULL,
	[answer] [varchar](max) NOT NULL,
 CONSTRAINT [PK_tbl_faqBhutan] PRIMARY KEY CLUSTERED 
(
	[faqID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
--end

/*Object:  Table [dbo].[tbl_QuestionModule]    Script Date: 07-Jun-17 10:55:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_QuestionModule](
	[moduleId] [int] IDENTITY(1,1) NOT NULL,
	[moduleName] [varchar](max) NOT NULL,
 CONSTRAINT [PK_tbl_QuestionModule] PRIMARY KEY CLUSTERED 
(
	[moduleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
--end


/*Object:  Table [dbo].[tbl_QuizAnswer]    Script Date: 07-Jun-17 10:55:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_QuizAnswer](
	[answerId] [int] IDENTITY(1,1) NOT NULL,
	[answer] [varchar](250) NOT NULL,
	[questionId] [int] NOT NULL,
	[isCorrect] [bit] NOT NULL,
 CONSTRAINT [PK_tbl_QuizAnswer] PRIMARY KEY CLUSTERED 
(
	[answerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbl_QuizAnswer]  WITH CHECK ADD  CONSTRAINT [FK_tbl_QuizAnswer_tbl_QuizQuestion] FOREIGN KEY([questionId])
REFERENCES [dbo].[tbl_QuizQuestion] ([questionId])
GO
ALTER TABLE [dbo].[tbl_QuizAnswer] CHECK CONSTRAINT [FK_tbl_QuizAnswer_tbl_QuizQuestion]
GO
--end


/* Object:  Table [dbo].[tbl_QuizQuestion]    Script Date: 07-Jun-17 10:55:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_QuizQuestion](
	[questionId] [int] IDENTITY(1,1) NOT NULL,
	[moduleId] [int] NOT NULL,
	[question] [varchar](512) NOT NULL,
 CONSTRAINT [PK_tbl_QuizQuestion] PRIMARY KEY CLUSTERED 
(
	[questionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tbl_QuizQuestion]  WITH CHECK ADD  CONSTRAINT [FK_tbl_QuizQuestion_tbl_QuestionModule] FOREIGN KEY([moduleId])
REFERENCES [dbo].[tbl_QuestionModule] ([moduleId])
GO
ALTER TABLE [dbo].[tbl_QuizQuestion] CHECK CONSTRAINT [FK_tbl_QuizQuestion_tbl_QuestionModule]
GO

--end











