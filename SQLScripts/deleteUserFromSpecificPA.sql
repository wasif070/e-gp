Declare @office int
Set @office = 4

IF OBJECT_ID('tempdb..#tbl_tempDeletePA') IS NOT NULL DROP TABLE tbl_tempDeletePA
create table tbl_tempDeletePA (userid int);

insert into tbl_tempDeletePA (userid) (
SELECT userid
FROM tbl_EmployeeMaster where employeeId in (select employeeId from tbl_EmployeeOffices where officeId = @office));

delete tbl_EmployeeRoles where employeeId in (select employeeId from tbl_EmployeeOffices where officeId = @office)

delete tbl_EmployeeMaster where employeeId in (select employeeId from tbl_EmployeeOffices where officeId = @office)

delete tbl_LoginMaster where userId in (select * from tbl_tempDeletePA)

delete tbl_OfficeAdmin where officeId=@office

delete tbl_AdminMaster where userId in (select * from tbl_tempDeletePA)

delete tbl_AdminTransfer where userId in (select * from tbl_tempDeletePA)

IF OBJECT_ID('tempdb..#tbl_tempDeletePA') IS NOT NULL DROP TABLE tbl_tempDeletePA