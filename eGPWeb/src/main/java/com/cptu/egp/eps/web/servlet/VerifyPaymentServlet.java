/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
//import com.cptu.egp.eps.dao.storedprocedure.UserApprovalBean;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.web.utility.AppContext;
//import com.cptu.egp.eps.web.utility.DateUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import java.math.BigDecimal;
import java.math.RoundingMode;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
//import java.text.SimpleDateFormat;
//import java.util.Date;

/**
 * this servlet handles all the request for the RegFeePaymenDailyTrans.jsp,veryfypayment.jsp,registrationfeepayment.jsp
 * tenpayment dailytrans.jsp,tenpaymentlisting.jsp ,todaysveryfydpaymentregpayments.jsp ,veryfytenderpayment.jsp
 * @author Administrator
 */
public class VerifyPaymentServlet extends HttpServlet {

    private String logUserId = "0";
    private static final Logger LOGGER = Logger.getLogger(VerifyPaymentServlet.class);
    private CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
    private CommonSearchDataMoreService objCommonSearchDataMoreImpl = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
    private TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

    public void setLogUserId(String logUserId) {
        commonSearchService.setLogUserId(logUserId);
        objCommonSearchDataMoreImpl.setLogUserId(logUserId);
        tenderCommonService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession session = request.getSession();
        LOGGER.debug("processRequest : " + logUserId + "starts");
        String strUserTypeId = "";
        String strPartTransId = "";
        Object objUserId = session.getAttribute("userId");
        if (objUserId != null) {
            strUserTypeId = session.getAttribute("userId").toString();
        }

        if (session.getAttribute("govUserId") != null) {
            strPartTransId = session.getAttribute("govUserId").toString();
        }

        setLogUserId(strUserTypeId);
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String strAction = "";

        if (request.getParameter("action") != null) {
            strAction = request.getParameter("action");
        }
        /*getting data from database and displaying in veryfyPayment.jsp jqgrid*/
        try {
            if ("fetchData".equals(strAction)) {
                LOGGER.debug("fetchData : " + logUserId + "starts");
                response.setContentType("text/xml;charset=UTF-8");
                //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String userId = request.getParameter("userId");
                String emailId = request.getParameter("emailId");
                String isVerified = request.getParameter("isVerified");
                String branchId = request.getParameter("branchId");
                String branchMakerId = request.getParameter("branchMakerId");
                String fromDt = request.getParameter("fromDt");
                String toDt = request.getParameter("toDt");
                String sord = "desc";
                String sidx = "regPaymentId";

                if (!"".equalsIgnoreCase(request.getParameter("sidx"))) {
                    sord = request.getParameter("sord");
                    sidx = request.getParameter("sidx");
                }
                int totalPages = 0;
                //int totalCount = 0;

                if (fromDt != null) {
                    if (!"".equalsIgnoreCase(fromDt)) {
                         String[] dtFrmArr = fromDt.split("/");
                        fromDt = dtFrmArr[2] + "-" + dtFrmArr[1] + "-" + dtFrmArr[0];
                    }
                }

                if (toDt != null) {
                    if (!"".equalsIgnoreCase(toDt)) {
                        String[] dtToArr = toDt.split("/");
                        toDt = dtToArr[2] + "-" + dtToArr[1] + "-" + dtToArr[0];
                    }
                }
                List<SPCommonSearchDataMore> getList =
                        objCommonSearchDataMoreImpl.getCommonSearchData("getSearchRegUserListing", page, rows, userId, emailId, isVerified, branchId, branchMakerId, fromDt, toDt, sidx, sord, null, null, null, null, null, null, null, null);

                if (getList.isEmpty()) {
                    //totalCount = Integer.parseInt(getList.get(0).getFieldName8());
                    totalPages = 0;
                } else {
                    //totalCount=0;
                    totalPages = Integer.parseInt(getList.get(0).getFieldName7());
                }
                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");
                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + getList.size() + "</records>");
                //int srNo = 1;
                int rowId = 0;
                if (!getList.isEmpty()) {
                    for (SPCommonSearchDataMore details : getList) {
                    out.print("<row id='" + rowId + "'>");
                    out.print("<cell class=\"t-align-center\"><![CDATA[" + details.getFieldName9() + "]]></cell>");
                    out.print("<cell><![CDATA[" + details.getFieldName3() + "]]></cell>");
                    out.print("<cell><![CDATA[" + details.getFieldName4() + "]]></cell>");
                    out.print("<cell><![CDATA[" + details.getFieldName6() + "]]></cell>");
                    out.print("<cell><![CDATA[" + details.getFieldName5() + "]]></cell>");
                        if ("no".equalsIgnoreCase(details.getFieldName10())) {
                            out.print("<cell><![CDATA[<a href='VerifyPayDetails.jsp?payId=" + details.getFieldName1() + "&uId=" + details.getFieldName2() + "'>Verify</a> &nbsp;|&nbsp; <a href='RegistrationFeePayment.jsp?payId=" + details.getFieldName1() + "&uId=" + details.getFieldName2() + "&action=edit'>Edit</a> &nbsp;|&nbsp; <a href='RegistrationFeePaymentDetails.jsp?payId=" + details.getFieldName1() + "&uId=" + details.getFieldName2() + "'>View</a>]]></cell>");
                    } else {
                            out.print("<cell><![CDATA[<a href='RegistrationFeePaymentDetails.jsp?payId=" + details.getFieldName1() + "&uId=" + details.getFieldName2() + "'>View</a>]]></cell>");
                    }

                    out.print("</row>");
                    rowId++;
                }
               } else {
                    String noDataMsg = "No record found";
                    out.print("<row id='" + rowId + "'>");
                    out.print("<cell class=\"t-align-center\"><![CDATA[1]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("</row>");

               }

                out.print("</rows>");
                // Nullify List objects
                getList = null;
                // Nullify String objects
                rows = null;
                page = null;
                userId = null;
                emailId = null;
                isVerified = null;
                branchId = null;
                branchMakerId = null;
                fromDt = null;
                toDt = null;
                sord = null;
                sidx = null;
                LOGGER.debug("fetchData : " + logUserId + "ends");

                /*getting data from database and displaying in RegFeePaymentDailyTrans.jsp jqgrid*/

            }
            else if("fetchDataForReport".equals(strAction)) {
                LOGGER.debug("fetchData : " + logUserId + "starts");
                response.setContentType("text/xml;charset=UTF-8");
                //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String userId = request.getParameter("userId");
                String emailId = request.getParameter("emailId");
                String isVerified = request.getParameter("isVerified");
                String branchId = request.getParameter("branchId");
                String branchMakerId = request.getParameter("branchMakerId");
                String fromDt = request.getParameter("fromDt");
                String toDt = request.getParameter("toDt");
                String sord = "desc";
                String sidx = "regPaymentId";
                double bdtSum=0.00;
                double otherSum=0.00;

                if (!"".equalsIgnoreCase(request.getParameter("sidx"))) {
                    sord = request.getParameter("sord");
                    sidx = request.getParameter("sidx");
                }
                int totalPages = 0;
                //int totalCount = 0;

                if (fromDt != null) {
                    if (!"".equalsIgnoreCase(fromDt)) {
                         String[] dtFrmArr = fromDt.split("/");
                        fromDt = dtFrmArr[2] + "-" + dtFrmArr[1] + "-" + dtFrmArr[0];
                    }
                }

                if (toDt != null) {
                    if (!"".equalsIgnoreCase(toDt)) {
                        String[] dtToArr = toDt.split("/");
                        toDt = dtToArr[2] + "-" + dtToArr[1] + "-" + dtToArr[0];
                    }
                }
                List<SPCommonSearchDataMore> getList =
                        objCommonSearchDataMoreImpl.getCommonSearchData("getSearchRegUserListingForReport", page, rows, userId, emailId, isVerified, branchId, branchMakerId, fromDt, toDt, sidx, sord, null, null, null, null, null, null, null, null);

                if (getList.isEmpty()) {
                    //totalCount = Integer.parseInt(getList.get(0).getFieldName8());
                    totalPages = 0;
                } else {
                    //totalCount=0;
                    totalPages = Integer.parseInt(getList.get(0).getFieldName7());
                }
                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");
                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + getList.size() + "</records>");
                //int srNo = 1;
                int rowId = 0;
                if (!getList.isEmpty()) {
                    for (SPCommonSearchDataMore details : getList) {
                    out.print("<row id='" + rowId + "'>");
                    out.print("<cell class=\"t-align-center\"><![CDATA[" + details.getFieldName9() + "]]></cell>");
                    out.print("<cell><![CDATA[" + details.getFieldName3() + "]]></cell>");
                    out.print("<cell><![CDATA[" + details.getFieldName4() + "]]></cell>");
                    out.print("<cell><![CDATA[" + details.getFieldName14() + "]]></cell>");
                    out.print("<cell><![CDATA[" + details.getFieldName6() + "]]></cell>");
                    out.print("<cell><![CDATA[" + details.getFieldName5() + "]]></cell>");
                    if(details.getFieldName10().toString().equalsIgnoreCase("yes"))
                             out.print("<cell><![CDATA[" + "Verified" + "]]></cell>");
                        else
                              out.print("<cell><![CDATA[" + "Pending" + "]]></cell>");
                    if(!details.getFieldName11().toString().equalsIgnoreCase("Online"))
                        out.print("<cell><![CDATA[" + "Offline" + "]]></cell>");
                    else
                        out.print("<cell><![CDATA[" + "Online" + "]]></cell>");
                   // out.print("<cell><![CDATA[" + details.getFieldName12() + "]]></cell>");
                    if(details.getFieldName13().toString().equals("BTN")){
                            out.print("<cell><![CDATA[" + details.getFieldName12() + "]]></cell>");
                             bdtSum = bdtSum + Double.parseDouble(details.getFieldName12().toString());
                        }
                        else{
                            out.print("<cell><![CDATA[" + "0" + "]]></cell>");
                             bdtSum = bdtSum + 0.00;
                        }


                        if(!details.getFieldName13().toString().equals("BTN")){
                            out.print("<cell><![CDATA[" + details.getFieldName12() + "]]></cell>");
                            otherSum = otherSum + Double.parseDouble(details.getFieldName12().toString());
                        }
                        else{
                            out.print("<cell><![CDATA[" + "0" + "]]></cell>");
                            otherSum = otherSum + 0.00;
                        }
                    out.print("</row>");
                    rowId++;
                }
                    out.print("<row id='" + rowId + "'>");
                    out.print("<cell class=\"t-align-center\"><![CDATA[]]></cell>");
                    out.print("<cell><![CDATA[" + "" + "]]></cell>");
                    out.print("<cell><![CDATA[" + "" + "]]></cell>");
                    out.print("<cell><![CDATA[" + "" + "]]></cell>");
                    out.print("<cell><![CDATA[" + "" + "]]></cell>");
                    out.print("<cell><![CDATA[" + "" + "]]></cell>");
                    out.print("<cell><![CDATA[" + "" + "]]></cell>");
                    out.print("<cell><![CDATA[" + "Total" + "]]></cell>");
                    out.print("<cell><![CDATA[" + BigDecimal.valueOf(bdtSum).setScale(2,RoundingMode.CEILING) + "]]></cell>");
                    out.print("<cell><![CDATA[" + BigDecimal.valueOf(otherSum).setScale(2,RoundingMode.CEILING) + "]]></cell>");

                    out.print("</row>");
               } else {
                    String noDataMsg = "No record found";
                    out.print("<row id='" + rowId + "'>");
                    out.print("<cell class=\"t-align-center\"><![CDATA[1]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("</row>");

               }

                out.print("</rows>");
                // Nullify List objects
                getList = null;
                // Nullify String objects
                rows = null;
                page = null;
                userId = null;
                emailId = null;
                isVerified = null;
                branchId = null;
                branchMakerId = null;
                fromDt = null;
                toDt = null;
                sord = null;
                sidx = null;
                LOGGER.debug("fetchData : " + logUserId + "ends");

                /*getting data from database and displaying in RegFeePaymentDailyTrans.jsp jqgrid*/

            }
            else if ("fetchDailyData".equals(strAction)) {
                response.setContentType("text/xml;charset=UTF-8");
                LOGGER.debug("fetchDailyData : " + logUserId + "starts");
                //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String userId = request.getParameter("userId");
                String emailId = request.getParameter("emailId");
                String isVerified = request.getParameter("isVerified");
                String branchId = request.getParameter("branchId");
                String branchMakerId = request.getParameter("branchMakerId");
                String fromDt = request.getParameter("fromDt");
                String toDt = request.getParameter("toDt");
                String sord = "desc";
                String sidx = "regPaymentId";
                if (!"".equalsIgnoreCase(request.getParameter("sidx"))) {
                    sord = request.getParameter("sord");
                    sidx = request.getParameter("sidx");
            }
                int totalPages = 0;
                //int totalCount = 0;
                if (fromDt != null) {
                    if (!"0".equalsIgnoreCase(fromDt) && !"".equalsIgnoreCase(fromDt)) {
                        String[] dtFrmArr = fromDt.split("/");
                        fromDt = dtFrmArr[2] + "-" + dtFrmArr[1] + "-" + dtFrmArr[0];
                    }
                }

                if (toDt != null) {
                    if (!"0".equalsIgnoreCase(toDt) && !"".equalsIgnoreCase(toDt)) {
                        String[] dtToArr = toDt.split("/");
                        toDt = dtToArr[2] + "-" + dtToArr[1] + "-" + dtToArr[0];
                    }
                }
                List<SPCommonSearchDataMore> getList =
                        objCommonSearchDataMoreImpl.getCommonSearchData("getTodaysRegUserListing", page, rows, userId, emailId, isVerified, branchId, branchMakerId, fromDt, toDt, sidx, sord, null, null, null, null, null, null, null, null);
                if (getList.isEmpty()) {
                    //totalCount = Integer.parseInt(getList.get(0).getFieldName8());
                    totalPages = 0;

                } else {
                    //totalCount=0;
                    totalPages = Integer.parseInt(getList.get(0).getFieldName7());
                }
                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");
                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + getList.size() + "</records>");
                //int srNo = 1;
                int rowId = 0;
                if (!getList.isEmpty()) {
                    for (SPCommonSearchDataMore details : getList) {
                        out.print("<row id='" + rowId + "'>");
                        out.print("<cell class=\"t-align-center\"><![CDATA[" + details.getFieldName9() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName3() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName4() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName6() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName5() + "]]></cell>");
                        if ("no".equalsIgnoreCase(details.getFieldName10())) {
                            out.print("<cell><![CDATA[<a href='RegistrationFeePaymentDetails.jsp?payId=" + details.getFieldName1() + "&uId=" + details.getFieldName2() + "'>View</a>]]></cell>");
                        } else {
                            out.print("<cell><![CDATA[<a href='RegistrationFeePaymentDetails.jsp?payId=" + details.getFieldName1() + "&uId=" + details.getFieldName2() + "'>View</a>]]></cell>");
                        }
                        out.print("</row>");
                        rowId++;
                    }
                } else {
                    String noDataMsg = "No record found";
                    out.print("<row id='" + rowId + "'>");
                    out.print("<cell class=\"t-align-center\"><![CDATA[1]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("</row>");
                }
                out.print("</rows>");
                getList = null;
                rows = null;
                page = null;
                userId = null;
                emailId = null;
                isVerified = null;
                branchId = null;
                branchMakerId = null;
                fromDt = null;
                toDt = null;
                sord = null;
                sidx = null;
                LOGGER.debug("fetchDailyData : " + logUserId + "starts");

                 /*getting data from database and displaying in TodaysVerifydRegPaymentsdata.jsp jqgrid*/

            } else if ("fetchDailyVerifiedRegPaymentData".equals(strAction)) {
                response.setContentType("text/xml;charset=UTF-8");
                LOGGER.debug("fetchDailyVerifiedRegPaymentData : " + logUserId + "starts");
                //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String userId = request.getParameter("userId");
                String sord = "desc";
                String sidx = "regPaymentId";

                if (!"".equalsIgnoreCase(request.getParameter("sidx"))) {
                    sord = request.getParameter("sord");
                    sidx = request.getParameter("sidx");
                }
                int totalPages = 0;
                //int totalCount = 0;



                List<SPCommonSearchDataMore> getList =
                        objCommonSearchDataMoreImpl.getCommonSearchData("getTodaysVerifiedRegUserListing", page, rows, userId, sidx, sord, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

                if (getList.isEmpty()) {
                    //totalCount = Integer.parseInt(getList.get(0).getFieldName8());
                    totalPages = 0;
                } else {
                    //totalCount=0;
                    totalPages = Integer.parseInt(getList.get(0).getFieldName7());
                }

                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + getList.size() + "</records>");
                //int srNo = 1;
                int rowId = 0;
                if (!getList.isEmpty()) {

                    for (SPCommonSearchDataMore details : getList) {
                        out.print("<row id='" + rowId + "'>");
                        out.print("<cell class=\"t-align-center\"><![CDATA[" + details.getFieldName9() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName3() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName4() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName6() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName5() + "]]></cell>");
                        if ("no".equalsIgnoreCase(details.getFieldName10())) {
                            out.print("<cell><![CDATA[<a href='RegistrationFeePaymentDetails.jsp?payId=" + details.getFieldName1() + "&uId=" + details.getFieldName2() + "'>View</a>]]></cell>");
                        } else {
                            out.print("<cell><![CDATA[<a href='RegistrationFeePaymentDetails.jsp?payId=" + details.getFieldName1() + "&uId=" + details.getFieldName2() + "'>View</a>]]></cell>");
                        }

                        out.print("</row>");
                        //srNo++;
                        rowId++;
                    }
                } else {
                    String noDataMsg = "No record found";
                    out.print("<row id='" + rowId + "'>");
                    out.print("<cell class=\"t-align-center\"><![CDATA[1]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("</row>");

                }

                out.print("</rows>");
                // Nullify List objects
                getList = null;
                // Nullify String objects
                rows = null;
                page = null;
                userId = null;
                sord = null;
                sidx = null;
                LOGGER.debug("fetchDailyVerifiedRegPaymentData : " + logUserId + "Ends");
            } else if (strAction != null) {
                if ("getBranchMembers".equalsIgnoreCase(strAction)) {
                    String branchId = request.getParameter("branchId");
                    String str = "";
                    try {
                        str = getBranchMembers(branchId);
                    } catch (Exception e) {
                       LOGGER.error("processrequest : "+e);
                    } finally {
                        branchId = null;
                    }
                    out.write(str);

                } else if ("getRegFeePaymentAmt".equalsIgnoreCase(strAction)) {
                    LOGGER.debug("getRegFeePaymentAmt : " + logUserId + "Starts");
                    String payCase = request.getParameter("payCase");
                    String payCurrency = request.getParameter("payCurrency");
                    String validityId = request.getParameter("validityId");
                    String userId = request.getParameter("userId");
                    String str = "";

                    try {
                        str = getRegFeePaymentAmt(payCase, payCurrency, validityId, userId);
                    } catch (Exception e) {
                        LOGGER.error("Error in getRegFeePaymentAmt : " + e.toString());
                    } finally {
                        payCase = null;
                        payCurrency = null;
                        validityId = null;
                        userId = null;
                    }
                    out.write(str);
                    LOGGER.debug("getRegFeePaymentAmt : " + logUserId + "Ends");
                }

        }
        } finally {
            out.close();
        }
        LOGGER.debug("processRequest : " + logUserId + "ends");
    }

    /**
     * this method is for getting the branchmembers data 
     * @param branchId - this is the branch id
     * @return string
     * @throws Exception
     */
   public String getBranchMembers(String branchId) throws Exception {
        LOGGER.debug("getBranchMembers : " + logUserId + "starts");
        StringBuilder str = new StringBuilder();

        try {
                str.append("<option value=''>- Select Branch Maker -</option>");

            for (SPTenderCommonData sptcd : tenderCommonService.returndata("getBankBranchOrMemberList", branchId, "getBranchMemberList")) {
               str.append("<option value='").append(sptcd.getFieldName1()).append("'>").append(sptcd.getFieldName2()).append("</option>");
            }
            //return str.toString();
        } catch (Exception e) {
            LOGGER.error("getBranchMembers : " + e);
            str.append("");
        } finally {
            tenderCommonService = null;
        }
        LOGGER.debug("getBranchMembers : " + logUserId + "ends");
        return str.toString();
    }

    /**
     * this method gives registration payments fees amount
     * @param payCase
     * @param payCurrency
     * @param validityId
     * @param userId
     * @return string, if execute successfully return amount otherwise return null
     * @throws Exception
     */
    public String getRegFeePaymentAmt(String payCase, String payCurrency, String validityId, String userId) throws Exception {
        LOGGER.debug("getRegFeePaymentAmt : " + logUserId + "starts");
        StringBuilder str = new StringBuilder();

        try {


            List<SPCommonSearchData> lstPaymentAmt = commonSearchService.searchData("getRegFeePaymentAmount", payCase, payCurrency, validityId, userId, null, null, null, null, null);

            if (!lstPaymentAmt.isEmpty()) {
                str.append(lstPaymentAmt.get(0).getFieldName1());
            }

            lstPaymentAmt = null;
            //commonSearchService = null;

        } catch (Exception e) {
            LOGGER.error("getRegFeePaymentAmt : " + e);
            str.append("");
        }
        LOGGER.debug("getRegFeePaymentAmt : " + logUserId + "ends");
        return str.toString();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
