/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblWsMaster;
import com.cptu.egp.eps.service.serviceinterface.WsMasterService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 * This Class is prepare JQuery grid for list of web-services
 * @author Sreenu
 */
public class WebServiceDetailsServlet extends HttpServlet {

    static final Logger LOGGER = Logger.getLogger(WebServiceDetailsServlet.class);
    String logUserId = "0";
    static final String STARTS = " Starts";
    static final String ENDS = " Ends";
    static final String EMPTY_STRING = "";
    private WsMasterService wsMasterService = null;

    /**
     * his method process the requests from WSList.jsp and prepare the JQuery grid.
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.debug("processRequest " + logUserId + STARTS);
        wsMasterService = (WsMasterService) AppContext.getSpringBean("WsMasterService");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
            logUserId = session.getAttribute("userId").toString();
        }
        wsMasterService.setLogUserId(logUserId);
        //preparing the grid
        response.setContentType("text/xml;charset=UTF-8");
        String rows = request.getParameter("rows");
        String page = request.getParameter("page");
        boolean searchFromGrid = Boolean.parseBoolean(request.getParameter("_search"));
        List<TblWsMaster> webServicesList = new ArrayList<TblWsMaster>();
        if (searchFromGrid) {
            webServicesList = getDataSearchFromGrid(request, response);
        } else {
            webServicesList = getDataOrderFromGrid(request, response);
        }
        //set pagination
        int totalPages = 0;
        long totalCount = webServicesList.size();
        LOGGER.debug("totalCount : " + totalCount);
        if (totalCount > 0) {
            if (totalCount % Integer.parseInt(rows) == 0) {
                totalPages = (int) (totalCount / Integer.parseInt(rows));
            } else {
                totalPages = (int) ((totalCount / Integer.parseInt(rows)) + 1);
            }
        } else {
            totalPages = 0;
        }
        //preparing XML for jquery grid
        out.print("<?xml version='1.0' encoding='utf-8'?>\n");
        out.print("<rows>");
        out.print("<page>" + request.getParameter("page") + "</page>");
        out.print("<total>" + totalPages + "</total>");
        out.print("<records>" + webServicesList.size() + "</records>");
        int recordSize = 0;
        int no = Integer.parseInt(request.getParameter("page"));
        if (no == 1) {
            recordSize = 0;
        } else {
            if (Integer.parseInt(rows) == 30) {
                recordSize = ((no - 1) * 30);
            } else if (Integer.parseInt(rows) == 20) {
                recordSize = ((no - 1) * 20);
            } else {
                recordSize = ((no - 1) * 10);
            }
        }
        //'Web-service Name','Description','Status','Authentication','Action'
        for (int i = recordSize; i < webServicesList.size(); i++) {
            out.print("<row id='" + webServicesList.get(i).getWsId() + "'>");
            out.print("<cell><![CDATA[" + webServicesList.get(i).getWsName() + "]]> </cell>");
            out.print("<cell><![CDATA[" + webServicesList.get(i).getWsDesc() + "]]> </cell>");
            String status = "";
            String statusChange = "";
            if ("Y".equalsIgnoreCase(webServicesList.get(i).getIsActive())) {
                status = "Active";
                statusChange = "Deactivate";
            } else {
                status = "Deactivate";
                statusChange = "Active";
            }
            String authentication = "";
            if ("Y".equalsIgnoreCase(webServicesList.get(i).getIsAuthenticate())) {
                authentication = "Required";
            } else {
                authentication = "Not Required";
            }
            out.print("<cell><![CDATA[" + status + "]]> </cell>");
            out.print("<cell><![CDATA[" + authentication + "]]> </cell>");
            String editLink = "<a href=\"WSDetailEdit.jsp?webServiceId="
                    + webServicesList.get(i).getWsId() + "&action=edit\">Edit</a>";
            String viewLink = "<a href=\"WSDetailView.jsp?webServiceId="
                    + webServicesList.get(i).getWsId() + "&action=view\">View</a>";
            String statusLink = "<a href=\"../WebServiceControlServlet?webServiceId="
                    + webServicesList.get(i).getWsId() + "&action=statusChange&status="
                    + statusChange + "\">" + statusChange + "</a>";
            out.print("<cell>");
            out.print("<![CDATA[" + editLink + "]]>");
            out.print(" | ");
            out.print("<![CDATA[" + viewLink + "]]>");
            out.print(" | ");
            out.print("<![CDATA[" + statusLink + "]]>");
            out.print("</cell>");
            out.print("</row>");
            recordSize++;
        }//end for loop
        out.print("</rows>");
        LOGGER.debug("processRequest " + logUserId + ENDS);
    }

    /***
     * This method process the Search request from JQuery Grid.
     * @param request
     * @param response
     * @return List<TblWsMaster>
     */
    private List<TblWsMaster> getDataSearchFromGrid(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.debug("getDataSearchFromGrid " + logUserId + STARTS);
        List<TblWsMaster> webServicesList = new ArrayList<TblWsMaster>();
        String searchField = EMPTY_STRING, searchString = EMPTY_STRING, searchOperation = EMPTY_STRING;
        searchField = request.getParameter("searchField");
        searchString = request.getParameter("searchString");
        searchOperation = request.getParameter("searchOper");
        StringBuffer queryBuffer = new StringBuffer();
        queryBuffer.append(" WHERE ");
        String searchQuery = EMPTY_STRING;
        if (searchField.equalsIgnoreCase("webSerivceName")) {
            if (searchOperation.equalsIgnoreCase("cn")) {
                searchQuery = "wsMaster.wsName LIKE '%" + searchString + "%'";
            } else if (searchOperation.equalsIgnoreCase("eq")) {
                searchQuery = "wsMaster.wsName = '" + searchString + "'";
            }
            queryBuffer.append(searchQuery);
        }
        if (searchField.equalsIgnoreCase("wsDescription")) {
            if (searchOperation.equalsIgnoreCase("cn")) {
                searchQuery = "wsMaster.wsDesc LIKE '%" + searchString + "%'";
            } else if (searchOperation.equalsIgnoreCase("eq")) {
                searchQuery = "wsMaster.wsDesc = '" + searchString + "'";
            }
            queryBuffer.append(searchQuery);
        }
        webServicesList = wsMasterService.getRequiredTblWsMasterList(queryBuffer.toString());
        LOGGER.debug("getDataSearchFromGrid " + logUserId + ENDS);
        return webServicesList;
    }

    /***
     * This method process the Sort request from JQuery Grid.
     * @param request
     * @param response
     * @return List<TblWsMaster>
     */
    private List<TblWsMaster> getDataOrderFromGrid(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.debug("getDataOrderFromGrid " + logUserId + STARTS);
        List<TblWsMaster> webServicesList = new ArrayList<TblWsMaster>();
        String orderColumn = EMPTY_STRING;
        String order = EMPTY_STRING;
        String sortOrder = request.getParameter("sord");
        String sortIndex = request.getParameter("sidx");
        String ascending = "ASC";
        String descending = "DESC";
        if (sortIndex.equals("")) {
            orderColumn = "wsMaster.createdDate";
            order = descending;
        } else if (sortIndex.equalsIgnoreCase("webSerivceName")) {
            orderColumn = "wsMaster.wsName";
            if (sortOrder.equalsIgnoreCase(ascending)) {
                order = ascending;
            } else if (sortOrder.equalsIgnoreCase(descending)) {
                order = descending;
            }
        } else if (sortIndex.equalsIgnoreCase("wsDescription")) {
            orderColumn = "wsMaster.wsDesc";
            if (sortOrder.equalsIgnoreCase(ascending)) {
                order = ascending;
            } else if (sortOrder.equalsIgnoreCase(descending)) {
                order = descending;
            }
        } else if (sortIndex.equalsIgnoreCase("wsStatus")) {
            orderColumn = "wsMaster.isActive";
            if (sortOrder.equalsIgnoreCase(ascending)) {
                order = ascending;
            } else if (sortOrder.equalsIgnoreCase(descending)) {
                order = descending;
            }
        } else if (sortIndex.equalsIgnoreCase("wsAuthentication")) {
            orderColumn = "wsMaster.isAuthenticate";
            if (sortOrder.equalsIgnoreCase(ascending)) {
                order = ascending;
            } else if (sortOrder.equalsIgnoreCase(descending)) {
                order = descending;
            }
        }
        String searchQuery = " ORDER BY " + orderColumn + " " + order;
        webServicesList = wsMasterService.getRequiredTblWsMasterList(searchQuery);
        LOGGER.debug("getDataOrderFromGrid " + logUserId + ENDS);
        return webServicesList;
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
