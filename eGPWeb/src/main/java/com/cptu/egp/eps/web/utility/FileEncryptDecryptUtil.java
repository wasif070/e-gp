/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * This class is used when downloading and uploading the file...File will be upploaded and then encrypted
 * When downloaded then decrypted..
 */

package com.cptu.egp.eps.web.utility;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Random;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import org.apache.log4j.Logger;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 *For Encryption and Decryption File
 * @author Administrator,TaherT
 */
public class FileEncryptDecryptUtil {
    private static final Logger LOGGER = Logger.getLogger(FileEncryptDecryptUtil.class);
    /**
     * Encryption of Uploaded File
     * @param file UploadedFile
     * @param size Size of UploadedFile
     * @return Size of Encrypted File
     */
    public long fileEncryptUtil(File file,int size){
         String password="egp@fileencrypt";
         String data = null;
         long fileSize = 0;
          try {
                      InputStream fis = new FileInputStream(file);
                     byte[] buf = new byte[size];
                        int offset = 0;
                        int numRead = 0;
                while ((offset < buf.length) && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {
                    offset += numRead;
                        }
                    char[] bpassword = password.toCharArray();
                    byte[] salt = new byte[8];
                    Random r = new Random();
                    r.nextBytes(salt);
                    PBEKeySpec kspec = new PBEKeySpec(bpassword);
                    SecretKeyFactory kfact = SecretKeyFactory.getInstance("pbewithmd5anddes");
                    SecretKey key = kfact.generateSecret(kspec);
                    PBEParameterSpec pspec = new PBEParameterSpec(salt, 1000);
                    Cipher cipher = Cipher.getInstance("pbewithmd5anddes");
                    cipher.init(Cipher.ENCRYPT_MODE, key, pspec);
                    byte[] enc = cipher.doFinal(buf);
                    BASE64Encoder encoder = new BASE64Encoder();
                    String saltString = encoder.encode(salt);
                    String ciphertextString = encoder.encode(enc);
                    data =  saltString + ciphertextString;
                     buf = new byte[data.getBytes().length];
                        while ((offset < buf.length)
                                && ((numRead = fis.read(data.getBytes(), offset, buf.length - offset)) >= 0)) {

                            offset += numRead;

                        }
                      fis.close();// Taher                      
                      BufferedWriter bw = new BufferedWriter(new FileWriter(file));                      
                        bw.write(data);

                        fileSize =data.getBytes().length;
                        bw.flush();
                        bw.close();
                    } catch (IllegalBlockSizeException ex) {
                        LOGGER.error("fileEncryptUtil method : "+ex);
                    } catch (BadPaddingException ex) {
                        LOGGER.error("fileEncryptUtil method : "+ex);
                    } catch (InvalidKeyException ex) {
                        LOGGER.error("fileEncryptUtil method : "+ex);
                    } catch (InvalidAlgorithmParameterException ex) {
                        LOGGER.error("fileEncryptUtil method : "+ex);
                    } catch (NoSuchPaddingException ex) {
                        LOGGER.error("fileEncryptUtil method : "+ex);
                    } catch (InvalidKeySpecException ex) {
                        LOGGER.error("fileEncryptUtil method : "+ex);
                    } catch (NoSuchAlgorithmException ex) {
                        LOGGER.error("fileEncryptUtil method : "+ex);
                    } catch (IOException ex) {
                        LOGGER.error("fileEncryptUtil method : "+ex);
                    }
        return fileSize;
    }

    /**
     *Decrypt when Downloading the File
     * @param buf ByeteArray EncryptedFile
     * @return ByeteArray for Downloading file
     */
    public byte[] fileDecryptUtil(byte[] buf){
            String encrypt =new String(buf);
            String password="egp@fileencrypt";
             byte[] plaintextArray = null;
              try {
            String salt = encrypt.substring(0, 12);
            String ciphertext = encrypt.substring(12, encrypt.length());
            BASE64Decoder decoder = new BASE64Decoder();
            byte[] saltArray = decoder.decodeBuffer(salt);
            byte[] ciphertextArray = decoder.decodeBuffer(ciphertext);
            PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray());
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("pbewithmd5anddes");
            SecretKey key = keyFactory.generateSecret(keySpec);
            PBEParameterSpec paramSpec = new PBEParameterSpec(saltArray, 1000);
            Cipher cipher = Cipher.getInstance("pbewithmd5anddes");
            cipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
            plaintextArray = cipher.doFinal(ciphertextArray);
        } catch (IllegalBlockSizeException ex) {
            LOGGER.error("Error API : "+ex);
        } catch (BadPaddingException ex) {
            LOGGER.error("Error API : "+ex);
        } catch (InvalidKeyException ex) {
            LOGGER.error("Error API : "+ex);
        } catch (InvalidAlgorithmParameterException ex) {
            LOGGER.error("Error API : "+ex);
        } catch (NoSuchPaddingException ex) {
            LOGGER.error("Error API : "+ex);
        } catch (InvalidKeySpecException ex) {
            LOGGER.error("Error API : "+ex);
        } catch (NoSuchAlgorithmException ex) {
            LOGGER.error("Error API : "+ex);
        } catch (IOException ex) {
            LOGGER.error("Error API : "+ex);
        }
             return plaintextArray;
    }

}
