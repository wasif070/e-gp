/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblCmsContractTermination;
import com.cptu.egp.eps.model.table.TblCmsCtcertDoc;
import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.service.serviceimpl.ConsolodateService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.servicebean.CmsContractTerminationBean;
import com.cptu.egp.eps.web.servicebean.CmsCtCertDocServiceBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CheckExtension;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.log4j.Logger;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author Sreenu.Durga
 */
public class CTUploadServlet extends HttpServlet {

    private String TMP_DIR_PATH;
    private File tmpDir;
    private static final String DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("CTUploadServlet");
    private File destinationDir;
    private static final String UPLOAD = "upload";
    private static final String DOWNLOAD = "download";
    private static final String REMOVE = "remove";
    private int contractTerminationId = 0;
    private TblCmsContractTermination cmsContractTermination;
    private String documentName;
    private String docSize;
    private String docDescription;
    private int uploadedBy;
    private Date uploadedDate;
    private int ctCertDocId;
    private int tenderId;
    static final Logger LOGGER = Logger.getLogger(CTUploadServlet.class);
    private String logUserId = "0";
    static final String STARTS = " Starts";
    static final String ENDS = " Ends";
    private static CmsContractTerminationBean cmsContractTerminationBean = new CmsContractTerminationBean();
    private static CmsCtCertDocServiceBean cmsCtCertDocServiceBean = new CmsCtCertDocServiceBean();
    private int contractSignId;
    private static final String SEPERATOR = "\\";
    private String action = "";
    private static final String APPROVED = "approved";
    private static final String PENDING = "pending";
    private String contextPath;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        TMP_DIR_PATH = System.getProperty("java.io.tmpdir");
        tmpDir = new File(TMP_DIR_PATH);
        if (!tmpDir.isDirectory()) {
            tmpDir.mkdir();
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");        
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        contextPath = request.getContextPath();
        HttpSession session = request.getSession();
        response.setContentType("text/html;charset=UTF-8");
        String functionName = "";
        boolean check = true;
        String userTypeId = "";
        if (request.getSession().getAttribute("userTypeId") != null) {
            userTypeId = request.getSession().getAttribute("userTypeId").toString();
        }
        if (session.getAttribute("userId") == null) {
            response.sendRedirect("SessionTimedOut.jsp");
        } else {
            logUserId = session.getAttribute("userId").toString();
            cmsContractTerminationBean.setLogUserId(logUserId);
            cmsCtCertDocServiceBean.setLogUserId(logUserId);
            try {
                if (request.getParameter("functionName") != null) {
                    functionName = request.getParameter("functionName");
                    LOGGER.error("functionName : " + functionName);
                }
                if (functionName.length() != 0) {
                    if (UPLOAD.equalsIgnoreCase(functionName)) {
                        check = uploadFile(request, response);
                        if (!check) {
                            TblCmsCtcertDoc tblCmsCtCertDoc = new TblCmsCtcertDoc();
                            tblCmsCtCertDoc.setDocDescription(docDescription);
                            tblCmsCtCertDoc.setDocSize(docSize);
                            tblCmsCtCertDoc.setDocumentName(documentName);
                            cmsContractTermination = cmsContractTerminationBean.getCmsContractTermination(contractTerminationId);
                            tblCmsCtCertDoc.setContractTerminationId(cmsContractTermination.getContractTerminationId());
                            tblCmsCtCertDoc.setUploadedBy(uploadedBy);
                            tblCmsCtCertDoc.setUserTypeId(Integer.parseInt(userTypeId));
                            uploadedDate = Calendar.getInstance().getTime();
                            tblCmsCtCertDoc.setUploadedDate(uploadedDate);
                            ctCertDocId = cmsCtCertDocServiceBean.insertCmsCtcertDoc(tblCmsCtCertDoc);
                            if (PENDING.equalsIgnoreCase(cmsContractTermination.getWorkflowStatus())) {
                                action = UPLOAD;
                            } else if (APPROVED.equalsIgnoreCase(cmsContractTermination.getWorkflowStatus())) {
                                action = DOWNLOAD;
                            }
                        }else{
                            action = UPLOAD;
                        }
                    } else if (DOWNLOAD.equalsIgnoreCase(functionName)) {
                        int conId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Contract_Termination.getName(), "Download Document", "");
                        check = downloadFile(request, response);
                    } else if (REMOVE.equalsIgnoreCase(functionName)) {
                        int conId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Contract_Termination.getName(), "Remove Document", "");
                        check = removeFile(request, response);
                    }
                    if("2".equalsIgnoreCase(userTypeId))
                    {
                        response.sendRedirect(contextPath + "/tenderer/ContractTeminationDownload.jsp?contractTerminationId=" + contractTerminationId + "&tenderId="
                            + tenderId + "&contractSignId=" + contractSignId + "&check=" + check + "&functionName=" + functionName + "&action=" + action);
                    }else{    
                    response.sendRedirect(contextPath + "/resources/common/ContractTeminationUpload.jsp?contractTerminationId=" + contractTerminationId + "&tenderId="
                            + tenderId + "&contractSignId=" + contractSignId + "&check=" + check + "&functionName=" + functionName + "&action=" + action);
                    }
                }// if check function length
            } catch (FileUploadException ex) {
                LOGGER.error(functionName + " File" + ex);
            } catch (Exception ex) {
                LOGGER.error(functionName + " File" + ex);
            }
        }//else
    }

    /****
     * This function downloads the selected file. it returns
     * <b>FALSE : if downloading fails</b>
     * <b>TRUE : if downloading success</b>
     * @param request
     * @param response
     * @return boolean
     * @throws FileUploadException
     * @throws Exception
     */
    private boolean downloadFile(HttpServletRequest request, HttpServletResponse response) throws FileUploadException, Exception {
        LOGGER.debug("downloadFile : " + STARTS);
        boolean checkFlag = false;
        contractTerminationId = 0;
        tenderId = 0;
        contractSignId = 0;
        ctCertDocId = 0;
        if (request.getParameter("ctCertDocId") != null) {
            ctCertDocId = Integer.parseInt(request.getParameter("ctCertDocId"));
        }
        if (request.getParameter("contractTerminationId") != null) {
            contractTerminationId = Integer.parseInt(request.getParameter("contractTerminationId"));
        }
        if (request.getParameter("tenderId") != null) {
            tenderId = Integer.parseInt(request.getParameter("tenderId"));
        }
        if (request.getParameter("contractSignId") != null) {
            contractSignId = Integer.parseInt(request.getParameter("contractSignId"));
        }
        String realPath = DESTINATION_DIR_PATH + tenderId + SEPERATOR + contractSignId + SEPERATOR + contractTerminationId + SEPERATOR;
        String fileName = "";
        TblCmsCtcertDoc tblCmsCtCertDoc = new TblCmsCtcertDoc();
        tblCmsCtCertDoc = cmsCtCertDocServiceBean.getCmsCtcertDoc(ctCertDocId);
        if (tblCmsCtCertDoc != null) {
            fileName = tblCmsCtCertDoc.getDocumentName();
            File downloadFile = new File(realPath + fileName);
            InputStream fis = new FileInputStream(downloadFile);
            byte[] buf = new byte[(int) downloadFile.length()];
            int offset = 0;
            int numRead = 0;
            while ((offset < buf.length) && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {
                offset += numRead;
            }
            fis.close();
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=" + downloadFile.getName().replace(" ", "") + "");
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.write(buf);
            outputStream.flush();
            outputStream.close();
        } else {
            checkFlag = true;
        }
        LOGGER.debug("downloadFile : " + ENDS);
        return checkFlag;
    }

    /***
     * This function removes the selected file. it returns
     * <b>FALSE : if removing a file fails</b>
     * <b>TRUE : if removing a file success</b>
     * @param request
     * @param response
     * @return boolean
     * @throws FileUploadException
     * @throws Exception
     */
    private boolean removeFile(HttpServletRequest request, HttpServletResponse response) throws FileUploadException, Exception {
        LOGGER.debug("removeFile : " + STARTS);
        boolean checkFlag = false;
        contractTerminationId = 0;
        tenderId = 0;
        contractSignId = 0;
        ctCertDocId = 0;
        if (request.getParameter("ctCertDocId") != null) {
            ctCertDocId = Integer.parseInt(request.getParameter("ctCertDocId"));
        }
        if (request.getParameter("contractTerminationId") != null) {
            contractTerminationId = Integer.parseInt(request.getParameter("contractTerminationId"));
        }
        if (request.getParameter("tenderId") != null) {
            tenderId = Integer.parseInt(request.getParameter("tenderId"));
        }
        if (request.getParameter("contractSignId") != null) {
            contractSignId = Integer.parseInt(request.getParameter("contractSignId"));
        }
        String realPath = DESTINATION_DIR_PATH + tenderId + SEPERATOR + contractSignId + SEPERATOR + contractTerminationId + SEPERATOR;
        String fileName = "";
        TblCmsCtcertDoc tblCmsCtCertDoc = new TblCmsCtcertDoc();
        tblCmsCtCertDoc = cmsCtCertDocServiceBean.getCmsCtcertDoc(ctCertDocId);
        if (tblCmsCtCertDoc != null) {
            fileName = tblCmsCtCertDoc.getDocumentName();
            boolean isRecordDelete = cmsCtCertDocServiceBean.deleteCmsCtcertDoc(tblCmsCtCertDoc);
            if (isRecordDelete) {
                boolean isFileDelete = deleteFile(realPath + fileName);
                if (isFileDelete) {
                    checkFlag = false;
                } else {
                    checkFlag = true;
                }
            }
        } else {
            checkFlag = true;
        }
        LOGGER.debug("removeFile : " + ENDS);
        return checkFlag;
    }

    /***
     *  This function uploads the selected file. it returns
     * <b>FALSE : if uploading a file fails</b>
     * <b>TRUE : if uploading a file success</b>
     * @param request
     * @param response
     * @return boolean
     * @throws FileUploadException
     * @throws Exception
     */
    private boolean uploadFile(HttpServletRequest request, HttpServletResponse response) throws FileUploadException, Exception {
        LOGGER.debug("uploadFile : " + STARTS);
        boolean checkFlag = false;
        File file = null;
        long fileSize = 0;
        boolean checkret = false;
        String fileName = "";
        DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
        //Set the size threshold, above which content will be stored on disk.
        fileItemFactory.setSizeThreshold(4 * 1024 * 1024); //1 MB
        //Set the temporary directory to store the uploaded files of size above threshold.
        fileItemFactory.setRepository(tmpDir);
        ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
        //Parse the request
        List items = uploadHandler.parseRequest(request);
        Iterator itr = items.iterator();
        //For Supporting Document
        while (itr.hasNext()) {
            FileItem item = (FileItem) itr.next();
            //For Supporting Document
            //Handle Form Fields.
            if (item.isFormField()) {
                if (item.getFieldName().equals("documentBrief")) {
                    docDescription = item.getString();
                } else if (item.getFieldName().equals("contractTerminationId")) {
                    contractTerminationId = Integer.parseInt(item.getString());
                } else if (item.getFieldName().equals("tenderId")) {
                    tenderId = Integer.parseInt(item.getString());
                } else if (item.getFieldName().equals("contractUserId")) {
                    uploadedBy = Integer.parseInt(item.getString());
                } else if (item.getFieldName().equals("contractSignId")) {
                    contractSignId = Integer.parseInt(item.getString());
                }
            } else {
                //Handle Uploaded files.
                if (item.getName().lastIndexOf(SEPERATOR) != -1) {
                    fileName = item.getName().substring(item.getName().lastIndexOf(SEPERATOR) + 1, item.getName().length());
                } else {
                    fileName = item.getName();
                }
                fileName = fileName.replaceAll(" ", "");
                documentName = fileName;
                String realPath = DESTINATION_DIR_PATH + tenderId + SEPERATOR + contractSignId + SEPERATOR + contractTerminationId + SEPERATOR;
                destinationDir = new File(realPath);
                if (!destinationDir.isDirectory()) {
                    destinationDir.mkdirs();
                }
                String docSizeMsg = docSizeMsg(Integer.parseInt(logUserId));
                if (!docSizeMsg.equals("ok")) {
                } else {
                    fileSize = item.getSize();
                    docSize = fileSize + "";
                    checkret = checkExnAndSize(fileName, fileSize, "officer");
                    if (!checkret) {
                        checkFlag = true;
                        break;
                    } else {
                        file = new File(destinationDir, fileName);
                        if (file.isFile()) {
                            checkFlag = true;
                            break;
                        }
                        item.write(file);
                    }
                }
            }
        }//while loop
        LOGGER.debug("uploadFile : " + ENDS);
        int conId = service.getContractId(tenderId);
        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Contract_Termination.getName(), "Upload Document", "");
        /* for Document Upload added by Dohatec Start */
       file = new File(destinationDir, fileName);
       if (!checkFlag && !file.isFile()) {
                 checkFlag = true;
        }
        /* for Document Upload added by Dohatec End */
        return checkFlag;
    }

    /***
     * This function deletes a file in given path.
     * <b>TRUE: if deleting a file fails</b>
     * <b>FALSE : if deleting a file success</b>
     * @param filePath
     * @return boolean
     */
    public boolean deleteFile(String filePath) {
        LOGGER.debug("deleteFile : " + STARTS);
        boolean isDelete = false;
        File f = new File(filePath);
        if (f.delete()) {
            isDelete = true;
        } else {
            isDelete = false;
        }
        LOGGER.debug("deleteFile : " + ENDS);
        return isDelete;
    }

    /***
     * This method returns the max document size limit for the given user
     * @param int userId
     * @return String size
     */
    public String docSizeMsg(int userId) {
        LOGGER.debug("docSizeMsg : " + STARTS);
        String documentSize = "";
        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
        documentSize = userRegisterService.docSizeCheck(userId);
        LOGGER.debug("docSizeMsg : " + ENDS);
        return documentSize;
    }

    /***
     * This method checks the extension and size. it returns
     * TRUE: if the size and extension of the file valid
     * FALSE : if either the size or extension of the file invalid.
     * @param extn
     * @param size
     * @param userType
     * @return boolean
     */
    public boolean checkExnAndSize(String extn, long size, String userType) {
        LOGGER.debug("checkExnAndSize : " + STARTS);
        boolean chextn = false;
        float fsize = 0.0f;
        float dsize = 0.0f;
        int j = extn.lastIndexOf('.');
        String lst = extn.substring(j + 1);
        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        String str = configurationMaster.getAllowedExtension();
        String[] str1 = str.split(",");
        for (int i = 0; i < str1.length; i++) {
            if (str1[i].trim().equalsIgnoreCase(lst)) {
                chextn = true;
            }
        }
        if (chextn) {
            fsize = size / (1024 * 1024);
            dsize = configurationMaster.getFileSize();
            if (dsize > fsize) {
                chextn = true;
            } else {
                chextn = false;
            }
        }
        LOGGER.debug("checkExnAndSize : " + ENDS);
        return chextn;
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
