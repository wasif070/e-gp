/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

import com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService;
import com.cptu.egp.eps.web.utility.AppContext;

/**
 *
 * @author Administrator
 */
public class PartnerAdminMasterDtBean
{

    /**
     * 
     */
    public PartnerAdminMasterDtBean()
    {
    }
    private int partnerId;
    private int userId;
    private String fullName;
    private String nationalId;
    private String mobileNo;
    private int sbankDevelopId;
    private String isAdmin;
    private byte userTypeId;
    private String officeName;
    private String emailId;
    private String sbDevelopName;
    private String designation="Admin";
    private String isMakerChecker;

    /**
     * getter of ismakerChecker
     * @return
     */
    public String getIsMakerChecker() {
        return isMakerChecker;
    }

    /**
     * setter of ismakerChecker
     * @param isMakerChecker
     */
    public void setIsMakerChecker(String isMakerChecker) {
        this.isMakerChecker = isMakerChecker;
    }

    /**
     * getter of Designation
     * @return
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * setter of Designation
     * @param designation
     */
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    /**
     * getter of SbDevelopName
     * @return
     */
    public String getSbDevelopName()
    {
        return sbDevelopName;
    }

    /**
     * setter of SbDevelopName
     * @param sbDevelopName
     */
    public void setSbDevelopName(String sbDevelopName)
    {
        this.sbDevelopName = sbDevelopName;
    }

    /**
     * getter of UserTypeId
     * @return
     */
    public byte getUserTypeId()
    {
        return userTypeId;
    }

    /**
     * setter of UserTypeId
     * @param userTypeId
     */
    public void setUserTypeId(byte userTypeId)
    {
        this.userTypeId = userTypeId;
    }

    /**
     * getter of FullName
     * @return
     */
    public String getFullName()
    {
        return fullName;
    }

    /**
     * setter of FullName
     * @param fullName
     */
    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    /**
     * getter of isAdmin
     * @return
     */
    public String getIsAdmin()
    {
        return isAdmin;
    }

    /**
     * setter of isAdmin
     * @param isAdmin
     */
    public void setIsAdmin(String isAdmin)
    {
        this.isAdmin = isAdmin;
    }

    /**
     * getter of mobileNo
     * @return
     */
    public String getMobileNo()
    {
        return mobileNo;
    }

    /**
     * setter of mobileNo
     * @param mobileNo
     */
    public void setMobileNo(String mobileNo)
    {
        this.mobileNo = mobileNo;
    }

    /**
     * getter of nationalId
     * @return
     */
    public String getNationalId()
    {
        return nationalId;
    }

    /**
     * setter of nationalId
     * @param nationalId
     */
    public void setNationalId(String nationalId)
    {
        this.nationalId = nationalId;
    }

    /**
     * getter of partnerId
     * @return
     */
    public int getPartnerId()
    {
        return partnerId;
    }

    /**
     * setter of partnerId
     * @param partnerId
     */
    public void setPartnerId(int partnerId)
    {
        this.partnerId = partnerId;
    }

    /**
     * getter of sbankDevelopId
     * @return
     */
    public int getSbankDevelopId()
    {
        return sbankDevelopId;
    }

    /**
     * setter of sbankDevelopId
     * @param sbankDevelopId
     */
    public void setSbankDevelopId(int sbankDevelopId)
    {
        this.sbankDevelopId = sbankDevelopId;
    }

    /**
     * getter of userId
     * @return
     */
    public int getUserId()
    {
        return userId;
    }

    /**
     * setter of userId
     * @param userId
     */
    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    /**
     * getter of emailId
     * @return
     */
    public String getEmailId()
    {
        return emailId;
    }

    /**
     * setter of emailId
     * @param emailId
     */
    public void setEmailId(String emailId)
    {
        this.emailId = emailId;
    }

    /**
     * getter of officeName
     * @return
     */
    public String getOfficeName()
    {
        return officeName;
    }

    /**
     * setter of officeName
     * @param officeName
     */
    public void setOfficeName(String officeName)
    {
        this.officeName = officeName;
    }

    /**
     * Used for displaying records as per scBankdevPartnerId in update and view page
     */
    public void populateInfo(String...values)
    {
        ScBankDevpartnerService scBankDevpartnerService = (ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");
        String userType = null;
        if(values!=null && values.length!=0){
            userType = values[0];
        }
        Object[] admin = scBankDevpartnerService.retriveAdminInfo(userId, userTypeId,userType);
        if (admin != null) {
            setUserId((Integer) admin[0]);
            setSbankDevelopId((Integer) admin[1]);
            setSbDevelopName(String.valueOf(admin[2]));
            setEmailId(String.valueOf(admin[3]));
            setFullName(String.valueOf(admin[4]));
            setMobileNo(String.valueOf(admin[5]));
            setNationalId(String.valueOf(admin[6]));
            setPartnerId((Integer) admin[7]);
            setIsAdmin(String.valueOf(admin[8]));
            setDesignation(String.valueOf(admin[9]));
            setIsMakerChecker(String.valueOf(admin[10]));
        }
//        setEmailId(String.valueOf(admin[1]));
//        setPartnerId((Integer)admin[2]);
//        setFullName(String.valueOf(admin[3]));
//        setNationalId(String.valueOf(admin[4]));
//        setMobileNo(String.valueOf(admin[5]));
//        setSbankDevelopId((Integer)admin[6]);
//        setIsAdmin(String.valueOf(admin[7]));
    }
}
