/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.daointerface.TblAppPackagesDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonAppData;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.CommonSPReturn;
import com.cptu.egp.eps.dao.storedprocedure.CommonWeekendsHolidays;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblAppPackages;
import com.cptu.egp.eps.model.table.TblAppreviseConfiguration;
import com.cptu.egp.eps.model.table.TblBidDeclaration;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.service.serviceinterface.APPService;
import com.cptu.egp.eps.service.serviceinterface.AppReviseConfigurationService;
import com.cptu.egp.eps.service.serviceinterface.BidDeclarationService;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.DateUtils;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import static org.hibernate.util.CollectionHelper.arrayList;

/**
 *
 * @author Sanjay Prajapati
 */
public class APPServlet extends HttpServlet {

    private final AppReviseConfigurationService appReviseConService=(AppReviseConfigurationService)AppContext.getSpringBean("AppReviseConfigurationService");
    private final BidDeclarationService bidDeclarationService = (BidDeclarationService)AppContext.getSpringBean("BidDeclarationService");
    private final APPService appService = (APPService) AppContext.getSpringBean("APPService");
    private final CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
    private final CommonSearchService cmnSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
    private static final Logger LOGGER = Logger.getLogger(APPServlet.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND  = "Ends";
    private String logUserId ="0";
    private AuditTrail auditTrail;
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws Exception
     */
     public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
     }

     public AuditTrail getAuditTrail() {
        return auditTrail;
     }

     public void setAuditTrail(AuditTrail auditTrail) {
         this.auditTrail = auditTrail;
         appService.setAuditTrail(auditTrail);
         commonXMLSPService.setAuditTrail(auditTrail);
     }

     /**
      * Main method of APP Servlet uses for getting information for
      * getPrjOffice , getPE, getPM, getDP, getCalcDate, getPEOffice, delPkg, verifyPkgNo, getAppProject, getProjectEndDate
      * base on passing above value into variable funName.
      * @param request
      * @param response
      * @throws ServletException
      * @throws IOException
      * @throws Exception
      */
     protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
        LOGGER.debug("processRequest : "+logUserId+ LOGGERSTART);
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String param1 = "";
        String param2 = "";
        String param3 = "";
        String param4 = "";
        String startDate = "";
        String endDate = "";
        String str = "";
        String funName = "";

        String procNature=null;

        HttpSession session = request.getSession();
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
            appService.setLogUserId(logUserId);
        }
         if (request.getParameter("procNature") != null && !"".equals(request.getParameter("procNature"))){
            procNature = request.getParameter("procNature");
        }
        if(request.getParameter("projectId")!=null && !"".equals(request.getParameter("projectId"))){
            param2 = request.getParameter("projectId");
        }
        if (request.getParameter("param2") != null && !"".equals(request.getParameter("param2"))){
            param2 = request.getParameter("param2");
        }
        
        if(request.getParameter("param1") != null && !"".equals(request.getParameter("param1"))){
            param1 = request.getParameter("param1");
        }

        if(request.getParameter("cmbId")!=null && !"".equals(request.getParameter("cmbId"))){
            param3 = request.getParameter("cmbId");
        }
        
        if(request.getParameter("param4")!=null && !"".equals(request.getParameter("param4"))){
            param4 = request.getParameter("param4");
        }

        if(request.getParameter("funName")!=null && !"".equals(request.getParameter("funName"))){
            funName = request.getParameter("funName");
        }
        
        try {
            if("getPrjOffice".equalsIgnoreCase(funName)) {
                LOGGER.debug("processRequest : action : getPrjOffice : "+logUserId+ LOGGERSTART);
                if(Integer.parseInt(param2) == 0) {
                    if(session.getAttribute("userId")!=null){
                        param2 = session.getAttribute("userId").toString();
                    }
                    param1 = "PeOffice";
                    str = getAPPDetails(param1,param2);
                    
                } else {
                    param1 = "ProjectOffice";
                    if(session.getAttribute("userId")!=null){
                        param3 = session.getAttribute("userId").toString();
                    }
                    str = getAPPPrjOfficeDetails(param1,param2,param3);
                }
                LOGGER.debug("processRequest : action : getPrjOffice : "+logUserId+ LOGGEREND);
                //str = getAPPDetails(param1,param2);
            }
            else if("getBankBranchListDD".equalsIgnoreCase(funName)) 
            {
                    str = getBankBranchListDD(param1, param2, param4);
                
            } else if("getPE".equalsIgnoreCase(funName)) {
                    LOGGER.debug("processRequest : action : getPE : "+logUserId+ LOGGERSTART);
                    param1 = "Pe";
                    char c = '_';
                    param2 = param2.substring(0, param2.indexOf(c));
                    str = getAPPDetails(param1,param2);
                    LOGGER.debug("processRequest : action : getPE : "+logUserId+LOGGEREND);
            } else if("getPM".equalsIgnoreCase(funName)) {
                    LOGGER.debug("processRequest : action : getPM : "+logUserId+ LOGGERSTART);
                    param1 = "procurementmethod";
                    String procType = "";
                    if(request.getParameter("procType")!=null && !"".equalsIgnoreCase(request.getParameter("procType"))){
                        procType = request.getParameter("procType");
                    }
                    str = getProcMethod(param1,param2,param3, param4,procType,procNature);
                    LOGGER.debug("processRequest : action : getPM : "+logUserId+ LOGGEREND);
            } else if("getDP".equalsIgnoreCase(funName)) {
                    LOGGER.debug("processRequest : action : getDP : "+logUserId+ LOGGERSTART);
                    param1 = "ProjectPartner";
                    str = getDPDetails(param1,param2);
                    LOGGER.debug("processRequest : action : getDP : "+logUserId+ LOGGEREND);
            } else if("getCalcDate".equalsIgnoreCase(funName)) {
                    LOGGER.debug("processRequest : action : getCalcDate : "+logUserId+ LOGGERSTART);
                    str = getAppPkgDatesHoliday(param1,param2);
                    LOGGER.debug("processRequest : action : getCalcDate : "+logUserId+ LOGGEREND);
            }else if("getCalcDays".equalsIgnoreCase(funName)) {
                    LOGGER.debug("processRequest : action : getCalcDays : "+logUserId+ LOGGERSTART);
                    str = getRfqexpdtSignNo(param1,param2);
                    LOGGER.debug("processRequest : action : getCalcDays : "+logUserId+ LOGGEREND);
            }else if("getPEOffice".equalsIgnoreCase(funName)) {
                    LOGGER.debug("processRequest : action : getPEOffice : "+logUserId+ LOGGERSTART);
                    param1 = "PeOffice";
                    str = getAPPDetails(param1,param2);
                    LOGGER.debug("processRequest : action : getPEOffice : "+logUserId+ LOGGEREND);
            }else if("delPkg".equalsIgnoreCase(funName)) {
                   LOGGER.debug("processRequest : action : delPkg : "+logUserId+ LOGGERSTART);
                   int appId=0;
                   if(request.getParameter("appId")!=null)
                       appId=Integer.parseInt(request.getParameter("appId"));
                   if(delPkgDetail(Integer.parseInt(param1),appId)){
                        str = "true";
                   }else{
                        str = "false";
                   }
                   LOGGER.debug("processRequest : action : delPkg : "+logUserId+ LOGGEREND);
            }else if("verifyPkgNo".equalsIgnoreCase(funName)) {
                    LOGGER.debug("processRequest : action : verifyPkgNo : "+logUserId+ LOGGERSTART);
                   /* if(session.getAttribute("userId")!=null){
                        param2 = session.getAttribute("userId").toString();
                    } */
                    //param1 =packageno param2=pkg id BY GSS  for APP package info unique check when edit/revise
                    str = chkPkgNo(param1,param2);
                    LOGGER.debug("processRequest : action : verifyPkgNo : "+logUserId+ LOGGEREND);
            }else if("getAppProject".equalsIgnoreCase(funName)) {
                    LOGGER.debug("processRequest : action : getAppProject : "+logUserId+ LOGGERSTART);
                    if(session.getAttribute("userId")!=null){
                        param2 = session.getAttribute("userId").toString();
            }
                    if(request.getParameter("startDate")!=null && !"".equals(request.getParameter("startDate"))){
                        startDate = request.getParameter("startDate");
                    }
                    if(request.getParameter("endDate")!=null && !"".equals(request.getParameter("endDate"))){
                        endDate = request.getParameter("endDate");
                    }
                    str = getAppPrj(param4,param2,param1,startDate,endDate);
                    LOGGER.debug("processRequest : action : getAppProject : "+logUserId+ LOGGERSTART);
            }else if("getProjectSourceOfFund".equalsIgnoreCase(funName)) {
                    //LOGGER.debug("processRequest : action : getAppProject : "+logUserId+ LOGGERSTART);
                    if(session.getAttribute("userId")!=null){
                        param2 = session.getAttribute("userId").toString();
                    }
                    str = getProjectSourceOfFund(funName,param1,param2);
                    LOGGER.debug("processRequest : action : getAppProject : "+logUserId+ LOGGERSTART);
            }else if("getProjectEndDate".equalsIgnoreCase(funName)) {
                    LOGGER.debug("processRequest : action : getProjectEndDate : "+logUserId+ LOGGERSTART);
                    if(session.getAttribute("userId")!=null){
                        param2 = session.getAttribute("userId").toString();
                    }
                    str = getAppPrjEndDate(param1);
                    LOGGER.debug("processRequest : action : getProjectEndDate : "+logUserId+ LOGGERSTART);
            }
            else if("getOfficeInfoFromAPP".equalsIgnoreCase(funName)) {
                    LOGGER.debug("processRequest : action : getOfficeInfoFromAPP : "+logUserId+ LOGGERSTART);
                    if(session.getAttribute("userId")!=null){
                        param2 = session.getAttribute("userId").toString();
                    }
                    str = getOfficeInfoFromAPP(param1);
                    LOGGER.debug("processRequest : action : getProjectEndDate : "+logUserId+ LOGGERSTART);
            }
            else if("getOfficeInfoFromAPPDepositWork".equalsIgnoreCase(funName)) {
                    LOGGER.debug("processRequest : action : getOfficeInfoFromAPPDepositWork : "+logUserId+ LOGGERSTART);
                    if(session.getAttribute("userId")!=null){
                        param2 = session.getAttribute("userId").toString();
                    }
                    str = getOfficeInfoFromAPPDepositWork(param1);
                    LOGGER.debug("processRequest : action : getProjectEndDate : "+logUserId+ LOGGERSTART);
            }
            else if("CountAPPRevise".equalsIgnoreCase(funName)) {
                    LOGGER.debug("processRequest : action : CountAPPRevise : "+logUserId+ LOGGERSTART);
                    int appID = 0;
                    if(session.getAttribute("userId")!=null){
                        param2 = session.getAttribute("userId").toString();
                    }
                    if(request.getParameter("appID") != null && !"".equals(request.getParameter("appID"))){
                        appID = Integer.parseInt(request.getParameter("appID"));
                    }
                    str = CountAPPRevise(appID);
                    LOGGER.debug("processRequest : action : CountAPPRevise : "+logUserId+ LOGGERSTART);
            }
             else if("addBidDeclaration".equalsIgnoreCase(funName)) {
                    LOGGER.debug("processRequest : action : addBidDeclaration : "+logUserId+ LOGGERSTART);
                    String userID = null;
                    if(request.getParameter("userID") != null && !"".equals(request.getParameter("userID"))){
                        userID = (request.getParameter("userID"));
                    }
                        //str = addBidDeclaration(Integer.parseInt(param1), Integer.parseInt(param2), Integer.parseInt(logUserId));
                        str = addBidDeclaration(param1,param2,logUserId);
                    LOGGER.debug("processRequest : action : CountAPPRevise : "+logUserId+ LOGGERSTART);
            }
            LOGGER.debug("processRequest : "+logUserId+ LOGGEREND);
            out.write(str);
        } finally { 
            out.close();
        }
    } 
    
    /**
     * 
     * @param param1
     * @param param2
     * @return
     * @throws Exception
     */
    public String getAPPDetails(String param1,String param2) throws Exception {
        LOGGER.debug("getAPPDetails : "+logUserId+ LOGGERSTART);
        StringBuilder str = new StringBuilder();
        try{
            if("ProjectOffice".equalsIgnoreCase(param1)){
                str.append("<option value=''>- Select Project Office -</option>");
            }else if("PeOffice".equalsIgnoreCase(param1)){
                str.append("<option value=''>- Select PE Office -</option>");
            }
            for(CommonAppData commonAppData  : appService.getAPPDetailsBySP(param1, param2, "")){
                if("ProjectOffice".equalsIgnoreCase(param1) || "PeOffice".equalsIgnoreCase(param1)){
                    if("ProjectOffice".equalsIgnoreCase(param1)){
                        str.append("<option value='").append(commonAppData.getFieldName3()).append("_").append(commonAppData.getFieldName4()).append("'>").append(commonAppData.getFieldName2()).append("</option>");
                    } else{
                        str.append("<option value='").append(commonAppData.getFieldName1()).append("_").append(commonAppData.getFieldName4()).append("'>").append(commonAppData.getFieldName2()).append("</option>");
                    }
                }else {
                    str.append("<option value='").append(commonAppData.getFieldName1()).append("'>").append(commonAppData.getFieldName2()).append("</option>");
                }
            }
        } catch(Exception ex) {
             LOGGER.error("getAPPDetails "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("getAPPDetails : "+logUserId+ LOGGEREND);
            return str.toString();
        }

    /**
     *
     * @param param1
     * @param param2
     * @return
     * @throws Exception
     */
    public String getAPPPrjOfficeDetails(String param1,String param2,String param3) throws Exception {
        LOGGER.debug("getAPPPrjOfficeDetails : "+logUserId+ LOGGERSTART);
        StringBuilder str = new StringBuilder();
        
        try{
            str.append("<option value=''>- Select Project Office -</option>");
            for(CommonAppData commonAppData  : appService.getAPPDetailsBySP(param1, param2, param3)){
                    str.append("<option value='").append(commonAppData.getFieldName3()).append("_").append(commonAppData.getFieldName4()).append("'>").append(commonAppData.getFieldName2()).append("</option>");
            }
           
        } catch(Exception ex) {
            LOGGER.error("getAPPPrjOfficeDetails "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("getAPPPrjOfficeDetails : "+logUserId+ LOGGEREND);
            return str.toString();
        }


    /**
     *
     * @param param1
     * @param param2
     * @param param3
     * @return
     * @throws Exception
     */
    public List<CommonAppData> getAPPDetails(String param1,String param2,String param3) throws Exception {
        LOGGER.debug("getAPPDetails : "+logUserId+ LOGGERSTART);
        List<CommonAppData> appList = new ArrayList<CommonAppData>();
        try{
            for(CommonAppData commonAppData  : appService.getAPPDetailsBySP(param1, param2, param3)){
                appList.add(commonAppData);
            }
        } catch(Exception ex) {
            LOGGER.error("getAPPDetails "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("getAPPDetails : "+logUserId+ LOGGEREND);
            return appList;
        }
    
    private String getOfficeInfoFromAPP(String appCode) throws Exception
    {
        LOGGER.debug("getOfficeInfoFromAPP : "+logUserId+ LOGGERSTART);
        CommonAppData commonAppData = appService.getOfficeInfoFromAPP("AppCode", appCode);
        return commonAppData.getFieldName1();     
    }
    private String getOfficeInfoFromAPPDepositWork(String appCode) throws Exception
    {
        LOGGER.debug("getOfficeInfoFromAPP : "+logUserId+ LOGGERSTART);
        CommonAppData commonAppData = appService.getOfficeInfoFromAPP("AppCodeForDepositWork", appCode);
        if(commonAppData.getFieldName7()!=null && commonAppData.getFieldName7().equalsIgnoreCase("2"))
        {
            return "Reference APP is Deposit Work type.";
        }
        else if(commonAppData.getFieldName3()!=null && commonAppData.getFieldName3().equalsIgnoreCase("Pending"))
        {
            return "The reference APP is not Approved.";
        }
        else if(commonAppData.getFieldName3()!=null && commonAppData.getFieldName3().equalsIgnoreCase("Approved"))
        {
            if(commonAppData.getFieldName4()!=null && commonAppData.getFieldName4().equalsIgnoreCase("YES"))
            {
                return "Tender is created from the reference APP.";
            }
            else if(commonAppData.getFieldName6()!=null && commonAppData.getFieldName6().equalsIgnoreCase("YES"))
            {
                return "The reference APP is already Deposited.";
            }
            else if(commonAppData.getFieldName4()!=null && commonAppData.getFieldName4().equalsIgnoreCase("NO") && commonAppData.getFieldName6()!=null && commonAppData.getFieldName6().equalsIgnoreCase("NO"))
            {
                return commonAppData.getFieldName2() + "->" + commonAppData.getFieldName1();
            }
            else
            {
                return "";
            }
        }
        else
        {
            return "";
        }
    }
    //aprojit-Start
    public boolean CheckAPPRevise(String financialYear,int appID){
        LOGGER.debug("CheckAPPRevise : "+logUserId+ LOGGERSTART);
        TblAppreviseConfiguration tblAppreviseConfiguration = new TblAppreviseConfiguration();
        List forCheck = null;
        boolean check = false;
        forCheck = appReviseConService.getAppConfigurationDetails(financialYear);
        System.out.println(financialYear+"---------------------------->>>>>Check Date "+forCheck.size());
         if(forCheck.size()>0){
                tblAppreviseConfiguration = (TblAppreviseConfiguration)forCheck.get(0);
                long diff1 = currentDate().getTime() - tblAppreviseConfiguration.getStartDate().getTime() ;
                long diff2 = tblAppreviseConfiguration.getEndDate().getTime() - currentDate().getTime() ;
                String Revision = updateAPPRevisioncount(financialYear,appID);
                if(currentDate().before(tblAppreviseConfiguration.getStartDate())){
                    check = false;
                }
               else if((diff1 / (1000 * 60 * 60 * 24))>=0 && (diff2 / (1000 * 60 * 60 * 24))>=0){
                   if(Revision.equalsIgnoreCase("Not Filled"))
                   {
                    System.out.println("---------------------------->>>>>Check Okay ");
                      check= true;
                   }
                   if(Revision.equalsIgnoreCase("Filled")){
                    check= false;
                    }
                }
                else if(currentDate().after(tblAppreviseConfiguration.getEndDate())){
                    check = false;
                }
                else{
                    if(Revision.equalsIgnoreCase("Not Filled")){
                    check= true;
                    }
                    if(Revision.equalsIgnoreCase("Filled")){
                    check= false;
                    }
                }
         }
         else{
             check = false;
         }
      return check;
    }
    private Date currentDate(){
	   Date date = new Date();
           return date;     
    }
    
   private String updateAPPRevisioncount(String financialYear, int appId){
        LOGGER.debug("updateAPPcount : "+logUserId+ LOGGERSTART);
        String str = "";
        str = appService.updateAPPcount(financialYear,"RevisionCount", appId);
        LOGGER.debug("updateAPPcount : "+logUserId+ LOGGEREND);
        return str;
    }
   
   public String CountAPPRevise(int appID){    
       LOGGER.debug("CountAPPRevise : "+logUserId+ LOGGERSTART);
       String str = "";
       str = String.valueOf(appService.CountAPPRevise("CountAPPRevise", appID, currentDate()));
       System.out.println("Count Check-------------->>>"+str);
       LOGGER.debug("CountAPPRevise : "+logUserId+ LOGGEREND);
    return str;
    }
   
   private String addBidDeclaration(String tenderID, String lotID, String userID){
     LOGGER.debug("addBidDeclaration : "+logUserId+ LOGGERSTART);
       String str = "";
//       TblBidDeclaration tblBidDeclaration = new TblBidDeclaration();
//       tblBidDeclaration.setTenderId(tenderID);
//       tblBidDeclaration.setLotId(lotID);
//       tblBidDeclaration.setUserId(userID);
//       tblBidDeclaration.setCreatedDate(currentDate());
//       tblBidDeclaration.setRemarks("Remarks");
       
       //str = bidDeclarationService.addBidDeclaration(tblBidDeclaration);
       
      str = cmnSearchService.addBidDeclaration(tenderID, lotID, userID, currentDate());
       
       LOGGER.debug("addBidDeclaration : "+logUserId+ LOGGEREND);
    return str;
   }
    
    
    //aprojit-End
    /**
     * This Method will insert data for APP Details.
     * @param financialYearId
     * @param budgetType
     * @param prjId
     * @param prjName
     * @param officeId
     * @param empId
     * @param aaEmpId
     * @param appCode
     * @param createdBy
     * @param deptId
     * @param aaProcRoleId
     * @param action
     * @param appId
     * @param appType
     * @param entrustingAgency
     * @param refAppId
     * @param revisionCount
     * @param lastRevisionDate
     * @return
     * @throws Exception
     */
    public String addAPPDetail(String financialYearId,int budgetType,int prjId,String prjName,int officeId,int empId,int aaEmpId,String appCode,int createdBy,int deptId, int aaProcRoleId,String action,int appId,
            String appType, String entrustingAgency, Integer refAppId, int revisionCount, Date lastRevisionDate, String ActivityName) throws Exception {
        LOGGER.debug("addAPPDetail : "+logUserId+ LOGGERSTART);
        String str = "";
        CommonSPReturn commonSPReturn = appService.insertAPDetailsBySP(financialYearId, budgetType, prjId, prjName, officeId, empId, aaEmpId, appCode, createdBy, deptId, aaProcRoleId,action, appId,
                appType, entrustingAgency, refAppId, revisionCount, lastRevisionDate,ActivityName).get(0);
        if (commonSPReturn.getFlag()) {
            str ="Success_"+commonSPReturn.getId().toString();
        }else{
            str = "Fail";
        }
        LOGGER.debug("addAPPDetail : "+logUserId+ LOGGEREND);
        return str;
    }

    /**
     *
     * @param appId
     * @param procNature
     * @param serviceType
     * @param pkgNo
     * @param pkgDesc
     * @param aloBud
     * @param estCost
     * @param cpvCode
     * @param pkgEstCost
     * @param aaEmpId
     * @param isPQReq
     * @param reoiRfa
     * @param procMtdId
     * @param procType
     * @param sourceFund
     * @param appStatus
     * @param wfStatus
     * @param noOfStages
     * @param action
     * @param pkgId
     * @param lotNo
     * @param lotDesc
     * @param qty
     * @param unit
     * @param lotEstCost
     * @param pkgUrgency
     * @return
     * @throws Exception
     */
    public String addPkgDetail(Integer appId, String procNature, String serviceType, String pkgNo, String pkgDesc,String depoplanWork, String entrustingAgency, String timeFrame, String bidderCategory, String workCategory, Float aloBud, Float estCost, String cpvCode, Float pkgEstCost, Integer aaEmpId, String isPQReq, String reoiRfa, Integer procMtdId, String procType, String sourceFund, String appStatus, String wfStatus, Integer noOfStages, String action, Integer pkgId, String lotNo, String lotDesc, String qty, String unit, String lotEstCost,String pkgUrgency,String w1, String w2, String w3, String w4, String userid) throws Exception {
        LOGGER.debug("addPkgDetail : "+logUserId+ LOGGERSTART);
        String str = "Fail";
        CommonSPReturn commonSPReturn = null;
        try{
            String pkgid=null;
            if(action.equalsIgnoreCase("Create"))
            {
                pkgid=appService.chkPkgNoExist(pkgNo,Integer.parseInt(userid));
            }
            if(pkgid == null)
            {
              commonSPReturn = appService.insertPkgBySP(appId, procNature, serviceType, pkgNo, pkgDesc, depoplanWork, entrustingAgency, timeFrame, bidderCategory, workCategory, aloBud, estCost, cpvCode, pkgEstCost, aaEmpId, isPQReq, reoiRfa, procMtdId, procType, sourceFund, appStatus, wfStatus, noOfStages, action, pkgId, lotNo, lotDesc, qty, unit, lotEstCost,pkgUrgency,w1,w2,w3,w4).get(0);
                if (commonSPReturn.getFlag()) {
                    str = "Success_"+commonSPReturn.getId().toString();
                }else{
                    str = "Fail";
                }
            }
            else
            {
                str="alreadyExist_"+pkgid;
            }
        }catch(Exception ex){
            LOGGER.error("addPkgDetail "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("addPkgDetail : "+logUserId+ LOGGERSTART);
        return str;
    }

    /**
     *
     * @param param1
     * @param param2
     * @param param3
     * @return
     * @throws Exception
     */
    public String getProcMethod(String param1,String param2,String param3,String param4,String procType,String procNature) throws Exception {
        LOGGER.debug("getProcMethod : "+logUserId+ LOGGERSTART);
        StringBuilder str = new StringBuilder();
        String retstr = "";
        try{
            str.append("<option value=''>-- Please Select Procurement Method--</option>");
            for(CommonAppData commonAppData  : appService.getAPPDetailsBySP(param1, param2, ""))
            {

                if(procNature!=null && procNature.equalsIgnoreCase("Goods") && commonAppData.getFieldName2()!=null && (commonAppData.getFieldName2().equalsIgnoreCase("RFQU") || commonAppData.getFieldName2().equalsIgnoreCase("RFQL") ))
                {
                    continue;
                }
//                if(procNature!=null && procNature.equalsIgnoreCase("Works") && commonAppData.getFieldName2()!=null && commonAppData.getFieldName2().equalsIgnoreCase("RFQ"))
//                {
//                    continue;
//                }
                
                /*if("OSTETM".equalsIgnoreCase(commonAppData.getFieldName2())){
                    if("NCT".equalsIgnoreCase(procType) && !"Yes".equalsIgnoreCase(param2)){
                        if(!("REOI".equalsIgnoreCase(param2) || "RFA".equalsIgnoreCase(param2) || "RFP".equalsIgnoreCase(param2))){
                            str.append("<option");
                            if("OSTETM".equalsIgnoreCase(param4)){str.append(" selected ");}
                            str.append(" value='").append(commonAppData.getFieldName1()).append("'>").append(commonAppData.getFieldName3()).append("</option>");
                        }                        
                    }
                }*/
                if("Yes".equalsIgnoreCase(param2)){
                    if("OTM".equalsIgnoreCase(commonAppData.getFieldName2())){
                        str.append("<option");
                        if("OTM".equalsIgnoreCase(param4)){str.append(" selected ");}
                        str.append(" value='").append(commonAppData.getFieldName1()).append("'>").append(commonAppData.getFieldName3()).append("</option>");
                    }
                    
                } else if("No".equalsIgnoreCase(param2)){
                    if("OTM".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                       "LTM".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                       "DP".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                      // "LEQ".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                     //  "TSTM".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                       "RFQ".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                      // "RFQU".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                     //  "RFQL".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                       "FC".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                      // ("OSTETM".equalsIgnoreCase(commonAppData.getFieldName2()) && !"NCT".equalsIgnoreCase(procType)) ||
                       "DPM".equalsIgnoreCase(commonAppData.getFieldName2())){
                            if("".equalsIgnoreCase(param4)){
                                str.append("<option value='").append(commonAppData.getFieldName1()).append("'>").append(commonAppData.getFieldName3()).append("</option>");
                            }else{
                                str.append("<option");
                                if(param4.equalsIgnoreCase(commonAppData.getFieldName2())){str.append(" selected ");}
                                str.append(" value='").append(commonAppData.getFieldName1()).append("'>").append(commonAppData.getFieldName3()).append("</option>");
                            }
                    }
                } else if("REOI".equalsIgnoreCase(param2)){
                    if("QCBS".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                       "LCS".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                       //"CSO".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                      // "DC".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                       "SBCQ".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                       "SFB".equalsIgnoreCase(commonAppData.getFieldName2()) ){
                            if("".equalsIgnoreCase(param4)){
                                str.append("<option value='").append(commonAppData.getFieldName1()).append("'>").append(commonAppData.getFieldName3()).append("</option>");
                            }else{
                                str.append("<option ");
                                if(param4.equalsIgnoreCase(commonAppData.getFieldName2())){str.append(" selected ");}
                                str.append(" value='").append(commonAppData.getFieldName1()).append("'>").append(commonAppData.getFieldName3()).append("</option>");
                            }
                    }
                } else if("RFA".equalsIgnoreCase(param2) || "RFP".equalsIgnoreCase(param2)){
                    if("QCBS".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                        "SFB".equalsIgnoreCase(commonAppData.getFieldName2())||
                        "LCS".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                       //"SBCQ".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                       "SSS".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                      // "CSO".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                     //  "DC".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                       "IC".equalsIgnoreCase(commonAppData.getFieldName2())){
                            if("".equalsIgnoreCase(param4)){
                                str.append("<option value='").append(commonAppData.getFieldName1()).append("'>").append(commonAppData.getFieldName3()).append("</option>");
                            }else{
                                str.append("<option ");
                                if(param4.equalsIgnoreCase(commonAppData.getFieldName2())){str.append(" selected ");}
                                str.append(" value='").append(commonAppData.getFieldName1()).append("'>").append(commonAppData.getFieldName3()).append("</option>");
                            }
                    }
                }
            }
            //System.out.println("str : "+str.toString());
            retstr = str.toString();
        } catch(Exception ex) {
            LOGGER.error("getProcMethod "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("getProcMethod : "+logUserId+ LOGGEREND);
        return  retstr;
    }

    /**
     *
     * @param param1
     * @param param2
     * @return
     * @throws Exception
     */
    public String getDPDetails(String param1,String param2) throws Exception {
        LOGGER.debug("getDPDetails : "+logUserId+ LOGGEREND);
        StringBuilder str = new StringBuilder();
        String retStr = "";
        try{
            int maxLength = appService.getAPPDetailsBySP(param1, param2, "").size();
            int i=0;
            for(CommonAppData commonAppData  : appService.getAPPDetailsBySP(param1, param2, "")){
                if(maxLength == (i+1)){
                    str.append(commonAppData.getFieldName2());
                } else{
                str.append(commonAppData.getFieldName2()).append(" , ");
                }

                i++;
            }
            retStr = str.toString();
        } catch(Exception ex) {
            LOGGER.error("getDPDetails "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("getDPDetails : "+logUserId+ LOGGEREND);
        return retStr;
    }

    /**
     *
     * @param xmlData
     * @param action
     * @return
     * @throws Exception
     */
    public boolean addAPPPkgDates(String xmlData,String action,String...values) throws Exception {
        LOGGER.debug("addAPPPkgDates : "+logUserId+ LOGGERSTART);
        boolean flag = false;
        CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP(action, "tbl_AppPqTenderDates", xmlData,"",values).get(0);
        if(commonMsgChk.getFlag()){
            flag = true;
        }
        LOGGER.debug("addAPPPkgDates : "+logUserId+ LOGGEREND);
        return flag;
    }

    /**
     *
     * @param param1
     * @param param2
     * @return
     * @throws Exception
     */
    public String getRfqexpdtSignNo(String param1,String param2){ 
      LOGGER.debug("getRfqexpdtSignNo : "+logUserId+ LOGGERSTART);
      String Days = "";
      SimpleDateFormat myFormat1 = new SimpleDateFormat("dd/MM/yyyy");
      SimpleDateFormat myFormat2 = new SimpleDateFormat("dd-MMM-yyyy");
      try {
            Date date1 = myFormat2.parse(param1);
            Date date2 = myFormat1.parse(param2);
            long diff = date2.getTime() - date1.getTime();
            System.out.println ("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
            long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            Days = String.valueOf(days);           
        } catch (ParseException e) {
           // e.printStackTrace();
            LOGGER.error("getRfqexpdtSignNo "+logUserId+" : "+e.toString());
        }
      LOGGER.debug("getRfqexpdtSignNo : "+logUserId+ LOGGEREND);
      return Days;
    }
    
    public String getAppPkgDatesHoliday(String param1,String param2) throws Exception {
        LOGGER.debug("getAppPkgDatesHoliday : "+logUserId+ LOGGERSTART);
        Date dateParam = null;
        String str = "";
        char c = '/';
        if(param1.indexOf(c) >= 0){
            dateParam = new SimpleDateFormat("dd/MM/yyyy").parse(param1);
        }else{
            dateParam = new SimpleDateFormat("dd-MMM-yyyy").parse(param1);
        }
        
        Integer daysN = Integer.parseInt(param2);
        List<CommonWeekendsHolidays> appList = new ArrayList<CommonWeekendsHolidays>();
        try{
            for(CommonWeekendsHolidays CommonWeekendsHolidays  : appService.getDateWithDiffBySP(dateParam, daysN)){
                appList.add(CommonWeekendsHolidays);
            }
            str = appList.get(0).isFlag()+"_"+appList.get(0).getFlagMessage()+"_"+(new SimpleDateFormat("dd-MMM-yyyy").format(new java.util.Date(appList.get(0).getHolidayDate().getTime())));
        } catch(Exception ex) {
            LOGGER.error("getAppPkgDatesHoliday "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("getAppPkgDatesHoliday : "+logUserId+ LOGGEREND);
        return str;
    }

    /**
     * This method will delete Package details.
     * @param pkgId
     * @param appId
     * @return
     * @throws Exception
     */
    public Boolean delPkgDetail(int pkgId,int appId) throws Exception {
        LOGGER.debug("delPkgDetail : "+logUserId+ LOGGERSTART);
        boolean flag = false;
        try{
            appService.delPkgDetail(pkgId,appId);
            flag = true;
        }catch(Exception ex){
            LOGGER.error("delPkgDetail "+logUserId+" : "+ex.toString());
    }
        LOGGER.debug("delPkgDetail : "+logUserId+ LOGGEREND);
        return flag;

    }

    /**
     * It is use to check Package no is already exist or not.
     * @param param1
     * @param param2
     * @return
     * @throws Exception
     */

    public String chkPkgNo(String param1,String param2) throws Exception {
        LOGGER.debug("chkPkgNo : "+logUserId+ LOGGEREND);
        String str = "";
        Boolean flag = appService.chkPkgNo(param1, Integer.parseInt(param2));
        if(flag){
            str  ="Package No. already exist";
        }else{
            str= "OK";
        }
        LOGGER.debug("chkPkgNo : "+logUserId+ LOGGEREND);
        return str;
    }

    /**
     * It is used to generate Project Combo box.
     * @param param1
     * @param param2
     * @param param3
     * @param param4
     * @param param5
     * @return
     * @throws Exception
     */
    public String getAppPrj(String param1,String param2,String param3,String param4, String param5) throws Exception {
        LOGGER.debug("getAppPrj : "+logUserId+ LOGGEREND);
        StringBuilder str = new StringBuilder();

//        SimpleDateFormat sd1 = new SimpleDateFormat("yyyy-MM-dd");
//        SimpleDateFormat sd = new SimpleDateFormat("dd-MMM-yyyy");
//        
//        param4 = sd1.format(sd.parse(param4));
//        param5 = sd1.format(sd.parse(param5));
        
        String retstr = "";
        try{
            str.append("<option value=''>- Select Project -</option>");
            for(SPCommonSearchData commonAppData  : cmnSearchService.searchData(param1, param2, param3, param4 , param5, null, null, null, null, null)){
                    str.append("<option value='").append(commonAppData.getFieldName1()).append("'>").append(commonAppData.getFieldName2()).append("</option>");
                    //str.append("<input type=\"hidden\" name=\"ProjectSourceOfFund\" id=\"ProjectSourceOfFund\" value=\"").append(commonAppData.getFieldName7()).append("\" />");
            }
            retstr = str.toString();
        } catch(Exception ex) {
            LOGGER.error("getAppPrj "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("getAppPrj : "+logUserId+ LOGGEREND);
        return retstr;
    }
    
    public String getBankBranchListDD(String PayType,String IssuanceBankNm, String SelectedBranch) throws Exception 
    {
        StringBuilder str = new StringBuilder();
        String retstr = "";
        try
        {
            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            str.append("<option value=''>-- Select Branch --</option>");
            List<SPTenderCommonData> BranchListDD = tenderCommonService.returndata("getBankBranchListDD", IssuanceBankNm, "");
            if(BranchListDD!=null)
            {
                for(SPTenderCommonData BranchInfoDD : BranchListDD)
                {
                    if(BranchInfoDD.getFieldName1().equals(SelectedBranch))
                    {
                        str.append("<option value='").append(BranchInfoDD.getFieldName1()).append("' selected='selected' >").append(BranchInfoDD.getFieldName1()).append("</option>");
                    }
                    else
                    {
                        str.append("<option value='").append(BranchInfoDD.getFieldName1()).append("'>").append(BranchInfoDD.getFieldName1()).append("</option>");
                    }
                }
            }
            retstr = str.toString();
        } catch(Exception ex) {
            LOGGER.error("getAppPrj "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("getAppPrj : "+logUserId+ LOGGEREND);
        return retstr;
    }
    
    public String getProjectSourceOfFund(String FunName,String ProjectId,String UserId) throws Exception {   
        String SourceOfFund = "";
        try{
            List<SPCommonSearchData> commonAppData  = cmnSearchService.searchData(FunName, ProjectId, UserId, null , null, null, null, null, null, null);
                    //str.append("<option value='").append(commonAppData.getFieldName1()).append("'>").append(commonAppData.getFieldName2()).append("</option>");
                    //str.append("<input type=\"hidden\" name=\"ProjectSourceOfFund\" id=\"ProjectSourceOfFund\" value=\"").append(commonAppData.getFieldName7()).append("\" />");
            
            SourceOfFund = commonAppData.get(0).getFieldName1();
        } catch(Exception ex) {
            LOGGER.error("getProjectSourceOfFund "+logUserId+" : "+ex.toString());
        }
        //LOGGER.debug("getAppPrj : "+logUserId+ LOGGEREND);
        return SourceOfFund;
    }

    /**
     * It will return App Project End date.
     * @param param1
     * @return
     */
    public String getAppPrjEndDate(String param1){
        LOGGER.debug("getAppPrjEndDate : "+logUserId+ LOGGEREND);
        StringBuilder str = new StringBuilder();
        Date date = null;
        String strdate = "";
        try{
            date = appService.getProjectEndDate(Integer.parseInt(param1));
            if(date!=null){
                strdate = DateUtils.customDateFormate(date);
            }
        } catch(Exception ex) {
            LOGGER.error("getAppPrjEndDate "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("getAppPrjEndDate: "+logUserId+ LOGGEREND);
        return strdate;
    }

    /**
     * It will check Package Date Exist or not.
     * @param pkgNo
     * @return
     * @throws Exception
     */
    public  boolean chkPkgDatesExist(String pkgNo) throws Exception{
        LOGGER.debug("chkPkgDatesExist : "+logUserId+ LOGGEREND);
        Boolean flag =false;
        flag = appService.chkPkgDatesExist(pkgNo);
        
        LOGGER.debug("chkPkgDatesExist : "+logUserId+ LOGGEREND);
        return flag;
    }
    public List<TblAppPackages> getAPPDetailsByPackageNo(String pkgNo){
        try {
            return appService.getAPPDetailsByPackageNo(pkgNo);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(APPServlet.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<TblAppPackages>();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        LOGGER.debug("doGet : "+logUserId+ LOGGERSTART);
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            LOGGER.error("doGet "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("doGet : "+logUserId+ LOGGEREND);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        LOGGER.debug("doPost : "+logUserId+ LOGGERSTART);
        try {
            processRequest(request, response);
        } catch (Exception ex) {
           LOGGER.error("doPost "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("doPost : "+logUserId+ LOGGEREND);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
