/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.CommonAppData;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.CommonSPWfFileHistory;
import com.cptu.egp.eps.model.table.TblAppMaster;
import com.cptu.egp.eps.model.table.TblModuleMaster;
import com.cptu.egp.eps.model.table.TblProcurementRole;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import com.cptu.egp.eps.model.table.TblWorkFlowDocuments;
import com.cptu.egp.eps.model.table.TblWorkFlowFileHistory;
import com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig;
import com.cptu.egp.eps.model.table.TblWorkflowRuleEngine;
import com.cptu.egp.eps.service.serviceimpl.WorkFlowServiceImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class WorkFlowSrBean {

    private String logUserId = "0";
    final Logger logger = Logger.getLogger(WorkFlowSrBean.class);
    private AuditTrail auditTrail;
    WorkFlowServiceImpl workFlowServiceImpl =
            (WorkFlowServiceImpl) AppContext.getSpringBean("WorkFlowService");

    /**
     * Set UserId at bean level.
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        workFlowServiceImpl.setUserId(logUserId);
        this.logUserId = logUserId;
    }
    
    public void setAuditTrail(AuditTrail auditTrail) {
         this.auditTrail = auditTrail;
         workFlowServiceImpl.setAuditTrail(auditTrail);
     }
     
    /**
     * Get Work flow Data
     * @return List of Module Master
     */
    public List<TblModuleMaster> getWorkflowData() {
        logger.debug("getWorkflowData : " + logUserId + "starts");
        List<TblModuleMaster> moduleMaster = workFlowServiceImpl.getWorkFlowData();
        logger.debug("getWorkflowData : " + logUserId + "ends");
        return moduleMaster;
    }

    /**
     * Get User Roles.
     * @return List of Procurement role.
     */
    public List<TblProcurementRole> getRoles() {
        logger.debug("getRoles : " + logUserId + "starts");
        List<TblProcurementRole> procurementRole = workFlowServiceImpl.getProcurementRoles();
        logger.debug("getRoles : " + logUserId + "ends");
        return procurementRole;
    }

    /**
     * Create Work flow.
     * @param initiatorprocurementroleid
     * @param approverprocurementroleid
     * @param initiatorwfroleid
     * @param approverwfroleid
     * @param ispublishdaterequired
     * @param eventid
     * @param action
     * @param actionby
     * @param actiondate
     * @param wfinitruleid
     * @return list of CommonMsgChk
     */
    public List<CommonMsgChk> createWorkFlow(Integer initiatorprocurementroleid,
            Integer approverprocurementroleid, String initiatorwfroleid,
            String approverwfroleid, String ispublishdaterequired, Integer eventid,
            String action, Integer actionby, Date actiondate, Integer wfinitruleid) {
        logger.debug("createWorkFlow : " + logUserId + "starts");
        List<CommonMsgChk> createWorkFlow = null;
        try {
            createWorkFlow = workFlowServiceImpl.createWorkFlow(initiatorprocurementroleid, approverprocurementroleid,
                    initiatorwfroleid, approverwfroleid, ispublishdaterequired,
                    eventid, action, actionby, actiondate, wfinitruleid);
        } catch (Exception e) {
            logger.error("createWorkFlow : " + e);
        }


        logger.debug("createWorkFlow : " + logUserId + "ends");
        return createWorkFlow;

    }

    /**
     * Edit Work flow Data.
     * @param fieldName1
     * @param fieldName2
     * @param fieldName3
     * @return List of Common App Data
     */
    public List<CommonAppData> editWorkFlowData(String fieldName1, String fieldName2, String fieldName3) {
        logger.debug("editWorkFlowData : " + logUserId + "starts");
        List<CommonAppData> workflowEditData = null;
        try {
            workflowEditData = workFlowServiceImpl.editWorkFlowData(fieldName1, fieldName2, fieldName3);
        } catch (Exception e) {
            logger.error("editWorkFlowData : " + e);
        }


        logger.debug("editWorkFlowData : " + logUserId + "ends");
        return workflowEditData;
    }

    /**
     * Create work flow Event.
     * @param noOfReviewer
     * @param noOfDays
     * @param isDonorConReq
     * @param eventid
     * @param objectId
     * @param WfRoleId
     * @param Action
     * @param wfEvtConfId
     * @param actiondate
     * @param actionby
     * @return List of common message check.
     */
    public List<CommonMsgChk> createWorkFlowEvent(Integer noOfReviewer, Integer noOfDays,
            String isDonorConReq, Integer eventid, Integer objectId, Integer WfRoleId,
            String Action, Integer wfEvtConfId, Date actiondate, Integer actionby) {
        List<CommonMsgChk> listdata = null;
        logger.debug("createWorkFlowEvent : " + logUserId + "starts");
        try {
            listdata = workFlowServiceImpl.createWorkFlowEvent(noOfReviewer, noOfDays,
                    isDonorConReq, eventid, objectId, WfRoleId, Action, wfEvtConfId,
                    actiondate, actionby);
        } catch (Exception e) {
            logger.error("createWorkFlowEvent : " + e);
        }



        logger.debug("createWorkFlowEvent : " + logUserId + "ends");
        return listdata;
    }

    /**
     * check if Donor condition required or not.
     * @param eventid from tbl_EventMaster
     * @param objectid from tbl_WorkFlowEventConfig
     * @return if Donor condition required or not.
     */
    public String isDonorReq(String eventid, Integer objectid) {
        logger.debug("isDonorReq : " + logUserId + "starts");
        String donorReq = workFlowServiceImpl.isDonorConRequired(eventid, objectid);
        logger.debug("isDonorReq : " + logUserId + "ends");
        return donorReq;

    }

    /**
     * Check is Donor condition required or not.
     * @param activityId
     * @param objectId - appId from Tbl_AppMaster
     * @return is Donor condition required or not.
     */
    public boolean isDonorCond(int activityId, int objectId) {
        logger.debug("isDonorCond : " + logUserId + "starts");
        boolean donorReq = workFlowServiceImpl.isDonorCond(activityId, objectId);
        logger.debug("isDonorCond : " + logUserId + "ends");
        return donorReq;

    }

    /**
     * Add Workflow level
     * @param EventId
     * @param ObjectId
     * @param ActionDate
     * @param ActivityId
     * @param ChildId
     * @param FileOnHand
     * @param Action
     * @param worfklowId
     * @param actionBy
     * @param WfLevel
     * @param UserId
     * @param WfRoleId
     * @param Procurementroleid
     * @return List of CommonMsgChk
     */
    public List<CommonMsgChk> addWorkFlowLevel(Integer EventId, Integer ObjectId, Date ActionDate,
            Integer ActivityId, Integer ChildId, String FileOnHand, String Action,
            Integer worfklowId, String actionBy, String WfLevel, String UserId,
            String WfRoleId, String Procurementroleid) {
        List<CommonMsgChk> listdata = null;
        logger.debug("addWorkFlowLevel : " + logUserId + "starts");
        try {
            listdata = workFlowServiceImpl.addWorkFlowLevel(EventId, ObjectId, ActionDate,
                    ActivityId, ChildId, FileOnHand, Action, worfklowId, actionBy,
                    WfLevel, UserId, WfRoleId, Procurementroleid);
        } catch (Exception e) {
            logger.error("addWorkFlowLevel : " + e);
        }



        logger.debug("addWorkFlowLevel : " + logUserId + "ends");
        return listdata;
    }

    /**
     *  this is for to get workFlowlevel records based on
     *  objectid,childid,activityid.
     * @param objectid
     * @param childid
     * @param activityid
     * @return list from Tbl_WorkFlowLevelConfig
     */
    public List<TblWorkFlowLevelConfig> editWorkFlowLevel(int objectid, int childid, short activityid) {
        logger.debug("editWorkFlowLevel : " + logUserId + "starts");
        List<TblWorkFlowLevelConfig> tblWorkFlowLevelConf = workFlowServiceImpl.editWorkFlowLevel(objectid, childid, activityid);
        logger.debug("editWorkFlowLevel : " + logUserId + "ends");
        return tblWorkFlowLevelConf;
    }

    /**
     * Get Official and Designation name from userId.
     * @param userid from tbl_LoginMaster
     * @return emaloy name and designation name
     */
    public String getOfficialAndDesignationName(int userid) {
        logger.debug("getOfficialAndDesignationName : " + logUserId + "starts");
        String des = workFlowServiceImpl.getOfficialAndDesignationName(userid);
        logger.debug("getOfficialAndDesignationName : " + logUserId + "ends");
        return des;
    }

    /**
     * this is for getting department and designation names
     * @param userid from tbl_LoginMaster
     * @return employ name and designation name
     */
    public String getOfficeNameAndDesignationByDP(int userid) {
        logger.debug("getOfficeNameAndDesignationByDP : " + logUserId + "starts");

        String ofcdes = workFlowServiceImpl.getOfficeNameAndDesignationByDP(userid);
        logger.debug("getOfficeNameAndDesignationByDP : " + logUserId + "ends");
        return ofcdes;
    }

    /**
     * check if work flow level event exist or not.
     * @param eventid
     * @param objectid
     * @return true if work flow event exist else false
     */
    public boolean isWorkFlowEventExist(short eventid, int objectid) {
        logger.debug("isWorkFlowEventExist : " + logUserId + "starts");
        boolean exist = workFlowServiceImpl.isWorkFlowEventExist(eventid,
                objectid);
        logger.debug("isWorkFlowEventExist : " + logUserId + "ends");
        return exist;
    }

    /**
    /**
     * this is for saving workflow documents into database.
     * @param tblWorkFlowDocuments list to be added
     * @return true if uploaded work flow file else false
     */
    public boolean upLoadWorkFlowFile(TblWorkFlowDocuments tblWorkFlowDocuemtns) {
        logger.debug("upLoadWorkFlowFile : " + logUserId + "starts");
        boolean flag = false;
        try {
            workFlowServiceImpl.upLoadWorkFlowFile(tblWorkFlowDocuemtns);
            flag = true;
        } catch (Exception ex) {
            logger.error("upLoadWorkFlowFile : " + logUserId + ex);
        }
        logger.debug("upLoadWorkFlowFile : " + logUserId + "ends");
        return flag;
    }

    /**
     * this is for fetching all wfdocuments based on userid,acivityid,
     * childid,objectid.
     * @param activityid from tbl_WorkFlowDocuments
     * @param childid from tbl_WorkFlowDocuments
     * @param objectid from tbl_WorkFlowDocuments
     * @param userid from tbl_WorkFlowDocuments
     * @return list of tbl_WorkFlowDocuments
     */
    public List<TblWorkFlowDocuments> getWorkFlowDocuments(int objectid, int activityid, int childid,
            int userid) {
        logger.debug("getWorkFlowDocuments : " + logUserId + "starts");
        List<TblWorkFlowDocuments> tblWorkFlowDocuments = null;
        try {
            tblWorkFlowDocuments = workFlowServiceImpl.getWorkFlowDocuments(activityid, childid, objectid, userid);
        } catch (Exception e) {
            logger.error("getWorkFlowDocuments : " + e);
        }


        logger.debug("getWorkFlowDocuments : " + logUserId + "ends");
        return tblWorkFlowDocuments;
    }

    /**
     * Fetching details from tbl_WorkFlowDocuments
     * @param docIds - it is comma separated wfDocumentId
     * @return list of tbl_WorkFlowDocuments
     */
    public List<TblWorkFlowDocuments> getDocumetnsByIds(String docIds) {
        logger.debug("getDocumetnsByIds : " + logUserId + "starts");
        List<TblWorkFlowDocuments> tblWorkFlowDocuments = null;
        try {
            tblWorkFlowDocuments = workFlowServiceImpl.getDocumetnsByIds(docIds);
        } catch (Exception e) {
            logger.error("getDocumetnsByIds : " + e);
        }


        logger.debug("getDocumetnsByIds : " + logUserId + "ends");
        return tblWorkFlowDocuments;

    }

    /**
     * is Work flow publish date required or not.
     * @param eventid from tbl_WorkflowRuleEngine
     * @return list of tbl_WorkflowRuleEngine
     */
    public List<TblWorkflowRuleEngine> isPublishDateRequire(short eventid) {
        List<TblWorkflowRuleEngine> tblWorkflowRuleEngine = null;
        logger.debug("isPublishDateRequire : " + logUserId + "starts");
        try {
            tblWorkflowRuleEngine = workFlowServiceImpl.isPublishDateReq(eventid);
        } catch (Exception e) {
            logger.error("isPublishDateRequire : " + e);
        }


        logger.debug("isPublishDateRequire : " + logUserId + "ends");
        return tblWorkflowRuleEngine;
    }

    /**
     * Edit Work flow Data.
     * @param fieldName1
     * @param fieldName2
     * @param fieldName3
     * @return List of Common App Data
     */
    public List<CommonAppData> getWorkFlowActions(String action, String field2, String field3) {
        List<CommonAppData> actions = null;
        logger.debug("getWorkFlowActions : " + logUserId + "starts");
        try {
            actions = workFlowServiceImpl.editWorkFlowData(action, field2, field3);
        } catch (Exception e) {
            logger.error("getWorkFlowActions : " + e);
        }


        logger.debug("getWorkFlowActions : " + logUserId + "ends");
        return actions;
    }

    /**
     * this is for fetching workflowlevelconfig objects based on
     * userid,eventid,objectid,childid.
     * @param userid from tbl_LoginMaster
     * @param eventid from tbl_WorkFlowLevelConfig
     * @param objectid from tbl_WorkFlowLevelConfig
     * @param childid from tbl_WorkFlowLevelConfig
     * @return list of tbl_WorkFlowLevelConfig
     */
    public List<TblWorkFlowLevelConfig> getWorkFlowLevelConfig(int userid,
            short eventid, int objectid, int childid) {
        logger.debug("getWorkFlowLevelConfig : " + logUserId + "starts");
        List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = null;
        try {
            tblWorkFlowLevelConfig = workFlowServiceImpl.getWorkFlowLevelConfig(userid, eventid, objectid, childid);
        } catch (Exception e) {
            logger.error("getWorkFlowLevelConfig : " + e);
        }



        logger.debug("getWorkFlowLevelConfig : " + logUserId + "ends");
        return tblWorkFlowLevelConfig;

    }

    /**
     * Get Work flow level config data by object id activity id child id and userid
     * @param objectid
     * @param activityid
     * @param childid
     * @param wflevel
     * @param userid
     * @return list of TblWorkFlowLevelConfig
     */
    public List<TblWorkFlowLevelConfig> getWorkFlowConfig(int objectid,
            short activityid, int childid, short wflevel, int userid) {
        logger.debug("getWorkFlowConfig : " + logUserId + "starts");

        List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = null;
        try {
            tblWorkFlowLevelConfig = workFlowServiceImpl.getWorkFlowConfig(objectid,
                    activityid, childid, wflevel, userid);
        } catch (Exception e) {
            logger.error("getWorkFlowConfig : " + e);
        }


        logger.debug("getWorkFlowConfig : " + logUserId + "ends");
        return tblWorkFlowLevelConfig;
    }

    /**
     * this method is for delete document from path and regarding record in database.
     * @param tblWorkFlowDocument
     * @param userId
     */
    public void deleteDoc(TblWorkFlowDocuments tblWorkFlowDocument, int userId) {
        logger.debug("deleteDoc : " + logUserId + "starts");
        workFlowServiceImpl.deleteDoc(tblWorkFlowDocument);
    }

    /**
     * this method is for fetching all wfHistory
     * @param ViewType
     * @param ActivityId
     * @param ObjectId
     * @param ChildId
     * @param UserId
     * @param Page
     * @param RecordPerPage
     * @param sidx
     * @param sord
     * @param moduleName
     * @param eventName
     * @param processBy
     * @param objectId
     * @param FromProcessDate
     * @param FromProcessTo
     * @return list of CommonSPWfFileHistory
     */
    public List<CommonSPWfFileHistory> getWfHistory(String ViewType, Integer ActivityId,
            Integer ObjectId, Integer ChildId, Integer UserId, Integer Page,
            Integer RecordPerPage, String sidx, String sord, String moduleName, String eventName, String processBy, Integer objectId, String FromProcessDate, String FromProcessTo) {
        logger.debug("getWfHistory : " + logUserId + "starts");

        List<CommonSPWfFileHistory> commonSpWfFileHistory = null;
        try {
            commonSpWfFileHistory = workFlowServiceImpl.getWfHistory(ViewType, ActivityId, ObjectId, ChildId,
                    UserId, Page, RecordPerPage, sidx, sord, moduleName, eventName, processBy, objectId, FromProcessDate, FromProcessTo);
        } catch (Exception e) {
            logger.error("getWfHistory : " + e);
        }


        logger.debug("getWfHistory : " + logUserId + "ends");
        return commonSpWfFileHistory;
    }

    /**
     * Add update Work flow file processing.
     * @param activityId
     * @param objectId
     * @param childId
     * @param fromUserId
     * @param action
     * @param remarks
     * @param levelid
     * @param wftrigger
     * @param pubDt
     * @param documentId
     * @return list of CommonMsgChk
     */
    public List<CommonMsgChk> addUpdWffileprocessing(Integer activityId, Integer objectId,
            Integer childId, Integer fromUserId, String action, String remarks,
            Integer levelid, String wftrigger, Date pubDt, String documentId) {
        logger.debug("addUpdWffileprocessing : " + logUserId + "starts");
        List<CommonMsgChk> commonMsg = null;
        try {

            commonMsg = workFlowServiceImpl.addUpdWffileprocessing(activityId, objectId, childId,
                    fromUserId, action, remarks, levelid, wftrigger, pubDt, documentId);
        } catch (Exception e) {
            logger.error("addUpdWffileprocessing : " + e);
        }

        logger.debug("addUpdWffileprocessing : " + logUserId + "ends");
        return commonMsg;

    }

    /**
     * Convert array to List
     * @param spfilehistory
     * @param viewType
     * @return List
     */
    public List convertToList(List<CommonSPWfFileHistory> spfilehistory, String viewType) {
        List list = new ArrayList();
        logger.debug("convertToList : " + logUserId + "starts");
        try {
            DateFormat formatter;
            for (CommonSPWfFileHistory history : spfilehistory) {
                list.add(history.getFileSentFrom());
                formatter = new SimpleDateFormat("dd/MM/yy");
                String str = formatter.format(history.getProcessDate());
                list.add(str);
                list.add(history.getAction());
                list.add(history.getRemarks());
                list.add(history.getFileSentTo());
                if (viewType.equalsIgnoreCase("pending")) {
                    list.add("<a  href='#'>process</a>");
                }

            }
        } catch (Exception e) {
            logger.error("convertToList : " + e);
        }


        logger.debug("convertToList : " + logUserId + "ends");
        return list;
    }

    /**
     * Get Json Array from list.
     * @param list
     * @param para
     * @return
     * @throws JSONException
     */
    public JSONArray getJSONArray(List list, String para[]) throws JSONException {
        logger.debug("getJSONArray : " + logUserId + "starts");
        JSONArray jSONArray = new JSONArray();

        for (int j = 0; j < (list.size() / para.length); j++) {
            JSONObject jSONObject = new JSONObject();
            for (int i = 0; i < para.length; i++) {
                int c = (para.length * j) + i;
                jSONObject.put(para[i], list.get(c));
            }
            jSONArray.put(jSONObject);
        }

        logger.debug("getJSONArray : " + logUserId + "ends");

        return jSONArray;
    }

    /**
     * Get Work flow History by work flow Id.
     * @param hisid - wfHistoryId from tbl_WorkFlowFileHistory
     * @return list of TblWorkFlowFileHistory
     */
    public List<TblWorkFlowFileHistory> getWfFileHistoryById(int hisid) {
        List<TblWorkFlowFileHistory> tblWorkFlowFileHistorys = null;
        logger.debug("getWfFileHistoryById : " + logUserId + "starts");
        try {
            tblWorkFlowFileHistorys = workFlowServiceImpl.getWfFileHistoryById(hisid);
        } catch (Exception e) {
            logger.error("getWfFileHistoryById : " + e);
        }


        logger.debug("getWfFileHistoryById : " + logUserId + "ends");
        return tblWorkFlowFileHistorys;
    }

    /**
     * Check Revenue by Approve tender Id.
     * @param appOrTenId appId from Tbl_AppMaster
     * @return list of TblAppMaster
     */
    public List<TblAppMaster> checkRevinueByApporTenId(int appOrTenId) {
        List<TblAppMaster> appMaster = new ArrayList<TblAppMaster>();
        logger.debug("checkRevinueByApporTenId : " + logUserId + "starts");
        try {
            appMaster = workFlowServiceImpl.checkRevinueByApporTenId(appOrTenId);
        } catch (Exception e) {
            logger.error("checkRevinueByApporTenId : " + e);
        }
        logger.debug("checkRevinueByApporTenId : " + logUserId + "ends");
        return appMaster;
    }

    /**
     * Check Submission date of tenderId.
     * @param tenderId from tbl_TenderMaster
     * @return list of TblTenderDetails
     */
    public List<TblTenderDetails> checkSubmissionDate(int tenderId) {
        List<TblTenderDetails> tenderDetails = null;
        logger.debug("checkSubmissionDate : " + logUserId + "starts");
        try {
            tenderDetails = workFlowServiceImpl.checkSubmissionDate(tenderId);
        } catch (Exception e) {
            logger.error("checkSubmissionDate : " + e);
        }


        logger.debug("checkSubmissionDate : " + logUserId + "ends");
        return tenderDetails;
    }

    /**
     * Get Procurement method by procurement method id.
     * @param proId - procurementRoleId from tbl_ProcurementRole
     * @return procurement role
     */
    public String getProcurementById(byte proId) {
        List<TblProcurementRole> tblProcurementRoles = null;
        logger.debug("getProcurementById : " + logUserId + "starts");
        String retProcName = null;
        try {
            TblProcurementRole tblProcurementRole;

            tblProcurementRoles = workFlowServiceImpl.getProcurementById(proId);
            if (tblProcurementRoles.size() > 0) {
                tblProcurementRole = tblProcurementRoles.get(0);
                retProcName = tblProcurementRole.getProcurementRole();
            }
        } catch (Exception e) {
            logger.error("getProcurementById : " + e);
        }


        logger.debug("getProcurementById : " + logUserId + "ends");
        return retProcName;
    }

    /**
     * Delete all work flow levels.
     * @param objectId from tbl_WorkFlowLevelConfig
     * @param childId from tbl_WorkFlowLevelConfig
     * @param activityId from tbl_WorkFlowLevelConfig
     * @return 0 if not deleted successfully
     */
    public int flushWorkflowLevels(int objectId, int childId, short activityId) {
        int ret = 0;
        logger.debug("flushWorkflowLevels : " + logUserId + "starts");
        try {
            ret = workFlowServiceImpl.flushWorkflowLevels(objectId, childId, activityId);
        } catch (Exception e) {
            logger.error("flushWorkflowLevels : " + e);
        }


        logger.debug("flushWorkflowLevels : " + logUserId + "ends");
        return ret;
    }

    /**
     * count number of records from tbl_WorkFlowLevelConfig
     * @param objectId from tbl_WorkFlowLevelConfig
     * @param childId from tbl_WorkFlowLevelConfig
     * @param activityId from tbl_WorkFlowLevelConfig
     * @return number of records
     */
    public long getCountOfWorkflowlevelConfig(int objectId, int childId, short activityId) {
        long cont = 0;

        logger.debug("getCountOfWorkflowlevelConfig : " + logUserId + "starts");
        try {
            cont = workFlowServiceImpl.getCountOfWorkflowlevelConfig(objectId, childId, activityId);
        } catch (Exception e) {
            logger.error("getCountOfWorkflowlevelConfig : " + e);
        }


        logger.debug("getCountOfWorkflowlevelConfig : " + logUserId + "ends");
        return cont;
    }

    /**
     * Delete all Work flow levels.
     * @param objectId from tbl_WorkFlowLevelConfig
     * @param childId from tbl_WorkFlowLevelConfig
     * @param activityId from tbl_WorkFlowLevelConfig
     * @param wflevel from tbl_WorkFlowLevelConfig
     * @return 0 if not deleted successfully
     */
    public int flushWorkflowLevels(int objectId, int childId, short activityId, short wflevel) {
        int ret = 0;
        logger.debug("flushWorkflowLevels : " + logUserId + "starts");
        try {
            ret = workFlowServiceImpl.flushWorkflowLevels(objectId, childId, activityId, wflevel);
        } catch (Exception e) {
            logger.error("flushWorkflowLevels : " + e);
        }


        logger.debug("flushWorkflowLevels : " + logUserId + "ends");
        return ret;
    }

    /**
     * get details from tbl_WorkFlowLevelConfig
     * @param objectId from tbl_WorkFlowLevelConfig
     * @param childId from tbl_WorkFlowLevelConfig
     * @param activityId from tbl_ActivityMaster
     * @param wflevels from tbl_WorkFlowLevelConfig
     * @return list of TblWorkFlowLevelConfig
     */
    public List<TblWorkFlowLevelConfig> getwflevels(int objectId, int childId, short activityId, String wflevels) {
        List<TblWorkFlowLevelConfig> tblwflconfig = null;
        logger.debug("getwflevels : " + logUserId + "starts");
        try {
            tblwflconfig = workFlowServiceImpl.getwflevels(objectId, childId, activityId, wflevels);
        } catch (Exception e) {
            logger.error("getwflevels : " + e);
        }
        logger.debug("getwflevels : " + logUserId + "ends");
        return tblwflconfig;

    }
}
