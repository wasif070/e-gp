/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

import java.io.UnsupportedEncodingException;
import org.apache.log4j.Logger;

/**
 *
 * @author TaherT
 */
public class BanglaNameUtils {
    static final Logger LOGGER = Logger.getLogger(SendMessageUtil.class);

    /**
     * Method will convert string to byte array using UTF-8 Encoding , used for multi language purpose.
     * @param str
     * @return byte array.
     */
    public static byte[] getBytes(String str) {
        LOGGER.debug("getBytes Starts:");         
        if(str.length()%2!=0){
            str+=" ";
        }
        byte[] val= null;
        try {
            val =  str.getBytes("UTF-8");
        } catch (UnsupportedEncodingException ex) {
            LOGGER.error("sendEmail :"+ex);
        }
        LOGGER.debug("getBytes Ends:");
        return val;
    }

    /**
     * Method for convert byte array to string using UTF-8 Encoding , used for multi language purpose.
     * @param byteStr
     * @return
     */
    public static String getUTFString(byte[] byteStr) {
        LOGGER.debug("getUTFString Starts:");
        String val = "";
        try {
            val = new String(byteStr, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            LOGGER.error("getUTFString :"+ex);
        }
        LOGGER.debug("getUTFString Ends:");
        return val;
    }
}
