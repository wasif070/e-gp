/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblCmsCTReasonType;
import com.cptu.egp.eps.service.serviceinterface.CmsCTReasonTypeService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Sreenu.Durga
 */
public class CmsCTReasonTypeBean {

    final Logger logger = Logger.getLogger(CmsAitConfigBean.class);
    private final static CmsCTReasonTypeService cmsCTReasonTypeService =
            (CmsCTReasonTypeService) AppContext.getSpringBean("CmsCTReasonTypeService");
    private String logUserId = "0";
    private static final String STARTS = " Starts";
    private static final String ENDS = " Ends";
    private static final String SPACE = " : ";

    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        cmsCTReasonTypeService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /***
     * This method inserts a TblCmsCTReasonType object in database
     * @param tblCmsCTReasonType
     * @return int
     */
    public int insertCmsCTReasonType(TblCmsCTReasonType tblCmsCTReasonType) {
        logger.debug("insertCmsCTReasonType : " + logUserId + STARTS);
        int id = 0;
        try {
            id = cmsCTReasonTypeService.insertCmsCTReasonType(tblCmsCTReasonType);
        } catch (Exception ex) {
            logger.error("insertCmsCTReasonType : " + logUserId + SPACE + ex);
        }
        logger.debug("insertCmsCTReasonType : " + logUserId + ENDS);
        return id;
    }

    /****
     * This method updates an TblCmsCTReasonType object in DB
     * @param tblCmsCTReasonType
     * @return boolean
     */
    public boolean updateCmsCTReasonType(TblCmsCTReasonType tblCmsCTReasonType) {
        logger.debug("updateCmsCTReasonType : " + logUserId + STARTS);
        boolean flag = false;
        try {
            flag = cmsCTReasonTypeService.updateCmsCTReasonType(tblCmsCTReasonType);
        } catch (Exception ex) {
            logger.error("updateCmsCTReasonType : " + logUserId + SPACE + ex);
        }
        logger.debug("updateCmsCTReasonType : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method deletes an TblCmsCTReasonType object in DB
     * @param tblCmsCTReasonType
     * @return boolean
     */
    public boolean deleteCmsCTReasonType(TblCmsCTReasonType tblCmsCTReasonType) {
        logger.debug("deleteCmsCTReasonType : " + logUserId + STARTS);
        boolean flag = false;
        try {
            flag = cmsCTReasonTypeService.deleteCmsCTReasonType(tblCmsCTReasonType);
        } catch (Exception ex) {
            logger.error("deleteCmsCTReasonType : " + logUserId + SPACE + ex);
        }
        logger.debug("deleteCmsCTReasonType : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method returns all TblCmsCTReasonType objects from database
     * @return List<TblCmsCTReasonType>
     */
    public List<TblCmsCTReasonType> getAllCmsCTReasonType() {
        logger.debug("getAllCmsCTReasonType : " + logUserId + STARTS);
        List<TblCmsCTReasonType> cmsCTReasonTypeList = new ArrayList<TblCmsCTReasonType>();
        try {
            cmsCTReasonTypeList = cmsCTReasonTypeService.getAllCmsCTReasonType();
        } catch (Exception ex) {
            logger.error("getAllCmsCTReasonType : " + logUserId + SPACE + ex);
        }
        logger.debug("getAllCmsCTReasonType : " + logUserId + ENDS);
        return cmsCTReasonTypeList;
    }

    /***
     *  This method returns no. of TblCmsCTReasonType objects from database
     * @return long
     */
    public long getCmsCTReasonTypeCount() {
        logger.debug("getCmsCTReasonTypeCount : " + logUserId + STARTS);
        long cmsCTReasonTypeCount = 0;
        try {
            cmsCTReasonTypeCount = cmsCTReasonTypeService.getCmsCTReasonTypeCount();
        } catch (Exception ex) {
            logger.error("getCmsCTReasonTypeCount : " + logUserId + SPACE + ex);
        }
        logger.debug("getCmsCTReasonTypeCount : " + logUserId + ENDS);
        return cmsCTReasonTypeCount;
    }

    /***
     * This method returns TblCmsCTReasonType for the given Id
     * @param int id
     * @return TblCmsCTReasonType
     */
    public TblCmsCTReasonType getCmsCTReasonType(int ctReasonTypeId) {
        logger.debug("getCmsCTReasonType : " + logUserId + STARTS);
        TblCmsCTReasonType tblCmsCTReasonType = null;
        try {
            tblCmsCTReasonType = cmsCTReasonTypeService.getCmsCTReasonType(ctReasonTypeId);
        } catch (Exception ex) {
            logger.error("getCmsCTReasonType : " + logUserId + SPACE + ex);
        }
        logger.debug("getCmsCTReasonType : " + logUserId + ENDS);
        return tblCmsCTReasonType;
    }

    /***
     * This method returns TblCmsCTReasonType for the given reason
     * @param String reason
     * @return TblCmsCTReasonType
     */
    public TblCmsCTReasonType getCmsCTReasonTypeForReason(String reason) {
        logger.debug("getCmsCTReasonTypeForReason : " + logUserId + STARTS);
        TblCmsCTReasonType tblCmsCTReasonType = null;
        try {
            tblCmsCTReasonType = cmsCTReasonTypeService.getCmsCTReasonTypeForReason(reason);
        } catch (Exception ex) {
            logger.error("getCmsCTReasonTypeForReason : " + logUserId + " : " + ex);
        }       
        logger.debug("getCmsCTReasonTypeForReason : " + logUserId + ENDS);
        return tblCmsCTReasonType;
    }
}
