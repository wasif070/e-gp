/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author rishita
 */
public class TenderExtReqListingServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String status = request.getParameter("status");
            String strPageNo = request.getParameter("pageNo");
            int pageNo = Integer.parseInt(strPageNo);
            String strOffset = request.getParameter("size");
            int recordOffset = Integer.parseInt(strOffset);
            String styleClass = "";
            String userId = "";
            userId = request.getSession().getAttribute("userId").toString();

            CommonSearchService commonSearchService =(CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            List<SPCommonSearchData> listas;
            if (status.equalsIgnoreCase("Pending")) {

                //listas = tenderCommonService.returndata("ValidityExtensionRequestListingPending", userId, null);
                listas = commonSearchService.searchData("ValidityExtensionRequestListingPending", strOffset, strPageNo,userId , "", "", "", "", "", "");
                if (!listas.isEmpty()) {
                    for (int i = 0; i < listas.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }
                        SPCommonSearchData listing = listas.get(i);
                        out.print("<tr class='" + styleClass + "'>");
                        out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * 10 + (i + 1)) + "</td>");
                        out.print("<td class=\"t-align-center\">" + listing.getFieldName1() + "</td>");
                        out.print("<td class=\"t-align-center\">" + listing.getFieldName2() + "</td>");
                        out.print("<td class=\"t-align-center\">" + listing.getFieldName3() + "</td>");
                        out.print("<td class=\"t-align-center\">" + listing.getFieldName4() + "</td>");
                        out.print("<td class=\"t-align-center\">" + listing.getFieldName10() + "</td>");
                        out.print("<td class=\"t-align-center\"><a href=\"TenExtReqApproval.jsp?tenderId=" + listing.getFieldName1() + "&ExtId=" + listing.getFieldName6() + "\">Process</a></td>");
                        out.print("</tr>");
                    }
                } else {
                    out.print("<tr>");
                    out.print("<td id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\" colspan=\"7\">No Record Found!</td>");
                    out.print("</tr>");
                }
                int totalPages = 1;
                if (listas.size() > 0) {
                    int rowCount = Integer.parseInt(listas.get(0).getFieldName8());
                    totalPages = (int) Math.ceil(Math.ceil(rowCount) / recordOffset);
                    if (totalPages == 0) {
                        totalPages = 1;
                    }
                }
                out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
            } else if (status.equalsIgnoreCase("Approved")) {
                listas = commonSearchService.searchData("ValidityExtensionRequestListingApproved", strOffset, strPageNo,userId , "", "", "", "", "", "");
                if (!listas.isEmpty()) {
                    for (int i = 0; i < listas.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }
                        SPCommonSearchData listing = listas.get(i);
                        out.print("<tr class='" + styleClass + "'>");
                        out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * 10 + (i + 1)) + "</td>");
                        out.print("<td class=\"t-align-center\">" + listing.getFieldName1() + "</td>");
                        out.print("<td class=\"t-align-center\">" + listing.getFieldName2() + "</td>");
                        out.print("<td class=\"t-align-center\">" + listing.getFieldName3() + "</td>");
                        out.print("<td class=\"t-align-center\">" + listing.getFieldName4() + "</td>");
                        out.print("<td class=\"t-align-center\">" + listing.getFieldName10() + "</td>");
                        out.print("<td class=\"t-align-center\"><a href=\"TenExtReqView.jsp?tenderId=" + listing.getFieldName1() + "&ExtId=" + listing.getFieldName6() + "\">View</a></td>");
                        out.print("</tr>");
                    }
                } else {
                    out.print("<tr>");
                    out.print("<td id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\" colspan=\"7\">No Record Found!</td>");
                    out.print("</tr>");
                }
                int totalPages = 1;
                if (listas.size() > 0) {
                    int rowCount = Integer.parseInt(listas.get(0).getFieldName8());
                    totalPages = (int) Math.ceil(Math.ceil(rowCount) / recordOffset);
                    if (totalPages == 0) {
                        totalPages = 1;
                    }
                }
                out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
