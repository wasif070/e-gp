/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.service.serviceinterface.ConfigMasterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Naresh.Akurathi
 */
public class ConfigMasterSrBean {

    private static final Logger LOGGER = Logger.getLogger(ConfigMasterSrBean.class);
    private final ConfigMasterService preTenderRuleService = (ConfigMasterService) AppContext.getSpringBean("configMasterService");
    private String logUserId = "0";
    private static final String STARTS = " Starts";
    private static final String ENDS = " Ends";
    private AuditTrail auditTrail;
    
    public void setAuditTrail(AuditTrail auditTrail) {
        preTenderRuleService.setAuditTrail(auditTrail);
        this.auditTrail = auditTrail;
    }

    public void setLogUserId(String logUserId) {
        preTenderRuleService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /**
     * Get all TblConfigurationMaster
     * @return List of TblConfigurationMaster
     */
    public List<TblConfigurationMaster> getConfigMaster() {
        LOGGER.debug("getConfigMaster : " + logUserId + STARTS);
        LOGGER.debug("getConfigMaster : " + logUserId + ENDS);
        return preTenderRuleService.getConfigMaster();
    }

    /**
     *Updates TblConfigurationMaster on floginatmpt & pkireq
     * @param floginatmpt
     * @param pkireq
     * @return Values Updated or Not Updated for success or fail
     */
    public String updateConfigMaster(int floginatmpt, String pkireq) {
        LOGGER.debug("updateConfigMaster : " + logUserId + STARTS);
        LOGGER.debug("updateConfigMaster : " + logUserId + ENDS);
        return preTenderRuleService.updateConfigMaster(floginatmpt, pkireq);
    }

    /**
     *List of TblConfigurationMaster based on usertype
     * @param usertype
     * @return List of TblConfigurationMaster
     */
    public List<TblConfigurationMaster> getConfigMasterDetails(String usertype) {
        LOGGER.debug("getConfigMasterDetails : " + logUserId + STARTS);
        LOGGER.debug("getConfigMasterDetails : " + logUserId + ENDS);
        return preTenderRuleService.getConfigMasterDetails(usertype);
    }

    /**
     *List of TblConfigurationMaster based on configId
     * @param configId
     * @return List of TblConfigurationMaster
     */
    public List<TblConfigurationMaster> getConfigMasterDetails(int configId) {
        LOGGER.debug("getConfigMasterDetails : " + logUserId + STARTS);
        LOGGER.debug("getConfigMasterDetails : " + logUserId + ENDS);
        return preTenderRuleService.getConfigMasterDetails(configId);
    }

    /**
     *Updates configuration master
     * @param fileType
     * @param userType
     * @param singleFile
     * @param totalFile
     * @return Updated or null for success or fail
     */
    public String updateConfigMaster(String fileType, String userType, byte singleFile, byte totalFile) {
        LOGGER.debug("updateConfigMaster : " + logUserId + STARTS);
        LOGGER.debug("updateConfigMaster : " + logUserId + ENDS);
        return preTenderRuleService.updateConfigMaster(fileType, userType, singleFile, totalFile);
    }

    /**
     *Inserts configuration
     * @param fileType
     * @param userType
     * @param singleFile
     * @param totalFile
     * @return ok or Error for success or fail
     */
    public String insertConfigMaster(String fileType, String userType, byte singleFile, byte totalFile) {
        LOGGER.debug("insertConfigMaster : " + logUserId + STARTS);
        LOGGER.debug("insertConfigMaster : " + logUserId + ENDS);
        return preTenderRuleService.insertConfigMaster(fileType, userType, singleFile, totalFile);
    }

    /**
     *List of all Configuration Master
     * @return List of TblConfigurationMaster
     */
    public List<TblConfigurationMaster> getAllConfigMaster() {
        LOGGER.debug("getAllConfigMaster : " + logUserId + STARTS);
        LOGGER.debug("getAllConfigMaster : " + logUserId + ENDS);
        return preTenderRuleService.getAllConfigMaster();
    }

    /**
     *Get all UserTypes
     * @return List of TblUserTypeMaster
     */
    public List<Object> getAllUserType() {
        LOGGER.debug("getAllUserType : " + logUserId + STARTS);
        LOGGER.debug("getAllUserType : " + logUserId + ENDS);
         return  preTenderRuleService.getAllUserType();
     }

    /**
     *Get all Configuration User Type
     * @return List of ConfigUserType
     */
    public List<Object> getConfigUserType() {
        LOGGER.debug("getConfigUserType : " + logUserId + STARTS);
        LOGGER.debug("getConfigUserType : " + logUserId + ENDS);
         return  preTenderRuleService.getConfigUserType();
     }

}
