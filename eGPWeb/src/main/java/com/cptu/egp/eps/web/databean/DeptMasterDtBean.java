/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.databean;

import com.cptu.egp.eps.model.table.TblCountryMaster;
import com.cptu.egp.eps.model.table.TblStateMaster;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class DeptMasterDtBean {

    public DeptMasterDtBean() {
    }

    private short departmentId;
     private TblStateMaster tblStateMaster;
     private TblCountryMaster tblCountryMaster;
     private String departmentName;
     private String address;
     private String upJilla;
     private String phoneNo;
     private String faxNo;
     private String city;
     private int createdBy;
     private Date createdDate;
     private String status;
     private short parentDepartmentId;
     private String departmentType;
     private Integer approvingAuthorityId;
     private byte[] deptNameInBangla;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getApprovingAuthorityId() {
        return approvingAuthorityId;
    }

    public void setApprovingAuthorityId(Integer approvingAuthorityId) {
        this.approvingAuthorityId = approvingAuthorityId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public short getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(short departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentType() {
        return departmentType;
    }

    public void setDepartmentType(String departmentType) {
        this.departmentType = departmentType;
    }

    public byte[] getDeptNameInBangla() {
        return deptNameInBangla;
    }

    public void setDeptNameInBangla(byte[] deptNameInBangla) {
        this.deptNameInBangla = deptNameInBangla;
    }

    public String getFaxNo() {
        return faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    public short getParentDepartmentId() {
        return parentDepartmentId;
    }

    public void setParentDepartmentId(short parentDepartmentId) {
        this.parentDepartmentId = parentDepartmentId;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public TblCountryMaster getTblCountryMaster() {
        return tblCountryMaster;
    }

    public void setTblCountryMaster(TblCountryMaster tblCountryMaster) {
        this.tblCountryMaster = tblCountryMaster;
    }

    public TblStateMaster getTblStateMaster() {
        return tblStateMaster;
    }

    public void setTblStateMaster(TblStateMaster tblStateMaster) {
        this.tblStateMaster = tblStateMaster;
    }

    public String getUpJilla() {
        return upJilla;
    }

    public void setUpJilla(String upJilla) {
        this.upJilla = upJilla;
    }

     
}
