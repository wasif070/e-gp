/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//Search Formation changed by Emtaz on 15/May/2016

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.web.databean.ManageAdminDtBean;
import com.cptu.egp.eps.web.servicebean.UpdateAdminSrBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class OrganizationAdminGrid extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        final Logger logger = Logger.getLogger(OrganizationAdminGrid.class);
        String logUserId = "0";
        response.setContentType("text/html;charset=UTF-8");
        try {
            //<editor-fold>
            /*if (request.getParameter("action").equals("fetchData")) {
                UpdateAdminSrBean updateAdminSrBean = new UpdateAdminSrBean();
                if (request.getSession().getAttribute("userId") != null) {
                    logUserId = request.getSession().getAttribute("userId").toString();
                    updateAdminSrBean.setLogUserId(logUserId);
                }
                logger.debug(request.getParameter("action") + " : " + logUserId + " Starts");
                response.setContentType("text/xml;charset=UTF-8");
                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String sord = request.getParameter("sord");
                String sidx = request.getParameter("sidx");
                byte userTypeId = Byte.parseByte(request.getParameter("userTypeId"));
                int userId = Integer.parseInt(request.getSession().getAttribute("userId").toString());
                int userTID = Integer.parseInt(request.getSession().getAttribute("userTypeId").toString());
                //System.out.println(userTID+" userTID");
                //System.out.println(userTypeId+" userTypeId");
                updateAdminSrBean.setSearch(_search);
                updateAdminSrBean.setLimit(Integer.parseInt(rows));
                int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));

                String RoleType = "";

                if (request.getParameter("RoleType") != null && request.getParameter("RoleType").toString() != "") {
                    RoleType = request.getParameter("RoleType").toString();
                }

                updateAdminSrBean.setOffset(offset);
                updateAdminSrBean.setSortOrder(sord);
                updateAdminSrBean.setSortCol(sidx);


                PrintWriter out = response.getWriter();
                String searchField = "", searchString = "", searchOper = "";
                if (userTID == 1 || userTID == 19 || userTID == 21) {
                    List<ManageAdminDtBean> organizationGrid = null;
                    if (_search) {
                        searchField = request.getParameter("searchField");
                        searchString = request.getParameter("searchString");
                        searchOper = request.getParameter("searchOper");
                        organizationGrid = updateAdminSrBean.getOrganizationGrid(userTypeId, searchField, searchString, searchOper);
                    } else {
                        String orderClause = null;
                        if (sidx.equalsIgnoreCase("")) {
                            orderClause = "tlm.registeredDate desc";
                        } else if (sidx.equalsIgnoreCase("tlm.emailId")) {
                            orderClause = sidx + " " + sord;
                        } else if (sidx.equalsIgnoreCase("departmentName")) {
                            orderClause = sidx + " " + sord;
                        } else if (sidx.equalsIgnoreCase("tam.fullName")) {
                            orderClause = sidx + " " + sord;
                        } else if (sidx.equalsIgnoreCase("tam.rollId")) {
                            orderClause = sidx + " " + sord;
                        } else if (sidx.equalsIgnoreCase("om.officeName")) {
                            orderClause = sidx + " " + sord;
                        }
                        organizationGrid = updateAdminSrBean.getOrganizationGrid(userTypeId, orderClause);
                    }
                    int totalPages = 0;
                    int totalCount = 0;
                    if (_search) {
                        totalCount = (int) updateAdminSrBean.getSearchCountOfOrganization(userTypeId, searchField, searchString, searchOper);
                    } else {
                        totalCount = (int) updateAdminSrBean.getAllCountOfOrganization(userTypeId);
                    }

                    if (totalCount > 0) {
                        if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                            totalPages = totalCount / Integer.parseInt(request.getParameter("rows"));
                        } else {
                            totalPages = (totalCount / Integer.parseInt(request.getParameter("rows"))) + 1;
                        }

                    } else {
                        totalPages = 0;
                    }

                    if (userTypeId == 5) {
                        out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                        out.print("<rows>");
                        out.print("<page>" + request.getParameter("page") + "</page>");

                        out.print("<total>" + totalPages + "</total>");
                        out.print("<records>" + organizationGrid.size() + "</records>");
                        int j = 0;
                        int no = Integer.parseInt(request.getParameter("page"));
                        if (no == 1) {
                            j = 1;
                        } else {
                            if (Integer.parseInt(rows) == 30) {
                                j = ((no - 1) * 30) + 1;
                            } else if (Integer.parseInt(rows) == 20) {
                                j = ((no - 1) * 20) + 1;
                            } else {
                                j = ((no - 1) * 10) + 1;
                            }
                        }
                        // be sure to put text data in CDATA
                        for (int i = 0; i < organizationGrid.size(); i++) {
                            out.print("<row id='" + organizationGrid.get(i).getUserId() + "'>");
                            out.print("<cell>" + j + "</cell>");
                            out.print("<cell><![CDATA[" + organizationGrid.get(i).getDepartmentName() + "]]></cell>");
                            out.print("<cell><![CDATA[" + organizationGrid.get(i).getEmailId() + "]]></cell>");
                            out.print("<cell><![CDATA[" + organizationGrid.get(i).getFullName() + "]]></cell>");
                            String link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a>";
                            String strStatus = organizationGrid.get(i).getStatus();
                            Short adminId = organizationGrid.get(i).getAdminId();
                            if (userTID == 1) { // only e-GP Admin can change status
                                if ("approved".equalsIgnoreCase(strStatus)) {
                                    link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Deactivate</a>";
                                } else {
                                    link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Activate</a>";
                                }
                            }
                            out.print("<cell><![CDATA[" + link + "]]></cell>");
                            out.print("</row>");
                            j++;
                        }
                        out.print("</rows>");
                    } else if (userTypeId == 8) {
                        out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                        out.print("<rows>");
                        out.print("<page>" + request.getParameter("page") + "</page>");

                        out.print("<total>" + totalPages + "</total>");
                        out.print("<records>" + organizationGrid.size() + "</records>");
                        int j = 0;
                        int no = Integer.parseInt(request.getParameter("page"));
                        if (no == 1) {
                            j = 1;
                        } else {
                            if (Integer.parseInt(rows) == 30) {
                                j = ((no - 1) * 30) + 1;
                            } else if (Integer.parseInt(rows) == 20) {
                                j = ((no - 1) * 20) + 1;
                            } else {
                                j = ((no - 1) * 10) + 1;
                            }
                        }
                        // be sure to put text data in CDATA
                        for (int i = 0; i < organizationGrid.size(); i++) {
                            out.print("<row id='" + organizationGrid.get(i).getUserId() + "'>");
                            out.print("<cell>" + j + "</cell>");
                            out.print("<cell><![CDATA[" + organizationGrid.get(i).getEmailId() + "]]></cell>");
                            out.print("<cell><![CDATA[" + organizationGrid.get(i).getFullName() + "]]></cell>");
                            String link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a>";
                            String strStatus = organizationGrid.get(i).getStatus();
                            Short adminId = organizationGrid.get(i).getAdminId();
                            if (userTID == 1) { // only e-GP Admin can change status
                                if ("approved".equalsIgnoreCase(strStatus)) {
                                    link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Deactivate</a>";
                                } else {
                                    link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Activate</a>";
                                }
                            }
                            out.print("<cell><![CDATA[" + link + "]]></cell>");
                            out.print("</row>");
                            j++;
                        }
                        out.print("</rows>");
                    } else if (userTypeId == 4) {
                        out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                        out.print("<rows>");
                        out.print("<page>" + request.getParameter("page") + "</page>");

                        out.print("<total>" + totalPages + "</total>");
                        out.print("<records>" + organizationGrid.size() + "</records>");
                        int j = 0;
                        int no = Integer.parseInt(request.getParameter("page"));
                        if (no == 1) {
                            j = 1;
                        } else {
                            if (Integer.parseInt(rows) == 30) {
                                j = ((no - 1) * 30) + 1;
                            } else if (Integer.parseInt(rows) == 20) {
                                j = ((no - 1) * 20) + 1;
                            } else {
                                j = ((no - 1) * 10) + 1;
                            }
                        }
                        String actDeactLink = null; // be sure to put text data in CDATA
                        for (int i = 0; i < organizationGrid.size(); i++) {
                            out.print("<row id='" + organizationGrid.get(i).getUserId() + "'>");
                            out.print("<cell>" + j + "</cell>");
                            out.print("<cell><![CDATA[" + organizationGrid.get(i).getEmailId() + "]]></cell>");
                            out.print("<cell><![CDATA[" + organizationGrid.get(i).getFullName() + "]]></cell>");
                            out.print("<cell><![CDATA[" + organizationGrid.get(i).getOfficeName() + "]]></cell>");
                            String link = "<a href=\"ManagePEAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewPEAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view\">View</a>";
                            if (userTID == 1) {
                                if ("approved".equalsIgnoreCase(organizationGrid.get(i).getStatus())) {
                                    link = "<a href=\"ManagePEAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | "
                                            + "<a href=\"ViewPEAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view\">View</a> | "
                                            + "<a href=\"ViewPEAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&status=deactivate&userTypeid=" + userTypeId + "\">Deactivate</a>";
                                } else {
                                    link = "<a href=\"ManagePEAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | "
                                            + "<a href=\"ViewPEAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view\">View</a> | "
                                            + "<a href=\"ViewPEAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&status=activate&userTypeid=" + userTypeId + "\">Activate</a>";

                                }
                            }
                            out.print("<cell><![CDATA[" + link + "]]></cell>");
                            out.print("</row>");
                            j++;
                        }
                        out.print("</rows>");
                    } else if (userTypeId == 12) {
                        out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                        out.print("<rows>");
                        out.print("<page>" + request.getParameter("page") + "</page>");

                        out.print("<total>" + totalPages + "</total>");
                        out.print("<records>" + organizationGrid.size() + "</records>");
                        int j = 0;
                        int no = Integer.parseInt(request.getParameter("page"));
                        if (no == 1) {
                            j = 1;
                        } else {
                            if (Integer.parseInt(rows) == 30) {
                                j = ((no - 1) * 30) + 1;
                            } else if (Integer.parseInt(rows) == 20) {
                                j = ((no - 1) * 20) + 1;
                            } else {
                                j = ((no - 1) * 10) + 1;
                            }
                        }
                        // be sure to put text data in CDATA
                        for (int i = 0; i < organizationGrid.size(); i++) {
                            out.print("<row id='" + organizationGrid.get(i).getUserId() + "'>");
                            out.print("<cell>" + j + "</cell>");
                            out.print("<cell><![CDATA[" + organizationGrid.get(i).getEmailId() + "]]></cell>");
                            out.print("<cell><![CDATA[" + organizationGrid.get(i).getFullName() + "]]></cell>");
                            //out.print("<cell>" + departmentMasterList.get(i).getDepartmentType() + "</cell>");
                            String link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a>";
                          String strStatus = organizationGrid.get(i).getStatus();
                            Short adminId = organizationGrid.get(i).getAdminId();
                            if (userTID == 1) { // only e-GP Admin can change status
                                if ("approved".equalsIgnoreCase(strStatus)) {
                                    link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Deactivate</a>";
                                } else {
                                    link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Activate</a>";
                                }
                            }
                            out.print("<cell><![CDATA[" + link + "]]></cell>");
                            out.print("</row>");
                            j++;
                        }
                        out.print("</rows>");
                    } else if (userTypeId == 19) {
                        out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                        out.print("<rows>");
                        out.print("<page>" + request.getParameter("page") + "</page>");

                        out.print("<total>" + totalPages + "</total>");
                        out.print("<records>" + organizationGrid.size() + "</records>");
                        int j = 0;
                        int no = Integer.parseInt(request.getParameter("page"));
                        if (no == 1) {
                            j = 1;
                        } else {
                            if (Integer.parseInt(rows) == 30) {
                                j = ((no - 1) * 30) + 1;
                            } else if (Integer.parseInt(rows) == 20) {
                                j = ((no - 1) * 20) + 1;
                            } else {
                                j = ((no - 1) * 10) + 1;
                            }
                        }
                        // be sure to put text data in CDATA
                        for (int i = 0; i < organizationGrid.size(); i++) {
                            out.print("<row id='" + organizationGrid.get(i).getUserId() + "'>");
                            out.print("<cell>" + j + "</cell>");
                            out.print("<cell><![CDATA[" + organizationGrid.get(i).getEmailId() + "]]></cell>");
                            out.print("<cell><![CDATA[" + organizationGrid.get(i).getFullName() + "]]></cell>");
                            //out.print("<cell>" + departmentMasterList.get(i).getDepartmentType() + "</cell>");
                            String link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a>";
                            String strStatus = organizationGrid.get(i).getStatus();
                            Short adminId = organizationGrid.get(i).getAdminId();
                            if (userTID == 1) { // only e-GP Admin can change status
                                if ("approved".equalsIgnoreCase(strStatus)) {
                                    link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Deactivate</a> | <a href=\"ManageUserRights.jsp?uId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&msg=\">Rights</a>";
                                } else {
                                    link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Activate</a> | <a href=\"ManageUserRights.jsp?uId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&msg=\">Rights</a>";
                                }
                            }
                            out.print("<cell><![CDATA[" + link + "]]></cell>");
                            out.print("</row>");
                            j++;
                        }
                        out.print("</rows>");
                        } else if (userTypeId == 21) {
                        out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                        out.print("<rows>");
                        out.print("<page>" + request.getParameter("page") + "</page>");

                        out.print("<total>" + totalPages + "</total>");
                        out.print("<records>" + organizationGrid.size() + "</records>");
                        int j = 0;
                        int no = Integer.parseInt(request.getParameter("page"));
                        if (no == 1) {
                            j = 1;
                        } else {
                            if (Integer.parseInt(rows) == 30) {
                                j = ((no - 1) * 30) + 1;
                            } else if (Integer.parseInt(rows) == 20) {
                                j = ((no - 1) * 20) + 1;
                            } else {
                                j = ((no - 1) * 10) + 1;
                            }
                        }
                        // be sure to put text data in CDATA
                        for (int i = 0; i < organizationGrid.size(); i++) {
                            out.print("<row id='" + organizationGrid.get(i).getUserId() + "'>");
                            out.print("<cell>" + j + "</cell>");
                            out.print("<cell><![CDATA[" + organizationGrid.get(i).getEmailId() + "]]></cell>");
                            out.print("<cell><![CDATA[" + organizationGrid.get(i).getFullName() + "]]></cell>");
                            //out.print("<cell>" + departmentMasterList.get(i).getDepartmentType() + "</cell>");
                            String link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a>";
                            String strStatus = organizationGrid.get(i).getStatus();
                            Short adminId = organizationGrid.get(i).getAdminId();
                            if (userTID == 1) { // only e-GP Admin can change status
                                if ("approved".equalsIgnoreCase(strStatus)) {
                                    link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Deactivate</a>";
                                } else {
                                    link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Activate</a>";
                                }
                            }
                            out.print("<cell><![CDATA[" + link + "]]></cell>");
                            out.print("</row>");
                            j++;
                        }
                        out.print("</rows>");
                    } else if (userTypeId == 20) {
                        out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                        out.print("<rows>");
                        out.print("<page>" + request.getParameter("page") + "</page>");

                        out.print("<total>" + totalPages + "</total>");
                        out.print("<records>" + totalCount + "</records>");
                        int j = 0;
                        int no = Integer.parseInt(request.getParameter("page"));
                        if (no == 1) {
                            j = 1;
                        } else {
                            if (Integer.parseInt(rows) == 30) {
                                j = ((no - 1) * 30) + 1;
                            } else if (Integer.parseInt(rows) == 20) {
                                j = ((no - 1) * 20) + 1;
                            } else {
                                j = ((no - 1) * 10) + 1;
                            }
                        }
                        // be sure to put text data in CDATA
                        for (int i = 0; i < organizationGrid.size(); i++) {
                            out.print("<row id='" + organizationGrid.get(i).getUserId() + "'>");
                            out.print("<cell>" + j + "</cell>");
                            out.print("<cell><![CDATA[" + organizationGrid.get(i).getEmailId() + "]]></cell>");
                            out.print("<cell><![CDATA[" + organizationGrid.get(i).getFullName() + "]]></cell>");
                            //out.print("<cell>" + departmentMasterList.get(i).getDepartmentType() + "</cell>");
                            String link = "<a href=\"OANDMRegister.jsp?uId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&isUser=Yes&action=Edit\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view&isUser=Yes&msg=\">View</a>";
                            String roleLink = "";
                            String rType="Role Wise";
                            String strStatus = organizationGrid.get(i).getStatus();
                            Short adminId = organizationGrid.get(i).getAdminId();
                            if (organizationGrid.get(i).getRollId() == 0) {
                                roleLink = "| <a href=\"ManageUserRights.jsp?uId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&msg=\">Rights</a>";
                                rType="Individual";
                            }
                            if (userTID == 1 || userTID == 19) { // only e-GP Admin can change status
                                     if ("approved".equalsIgnoreCase(strStatus)) {
                                        link = "<a href=\"OANDMRegister.jsp?uId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&isUser=Yes&action=Edit\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view&isUser=Yes&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Deactivate</a> "+roleLink;
                                    } else {
                                        link = "<a href=\"OANDMRegister.jsp?uId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&isUser=Yes&action=Edit\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view&isUser=Yes&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Activate</a> "+roleLink;
                                    }
                            }

                            out.print("<cell><![CDATA[" + rType + "]]></cell>");
                            out.print("<cell><![CDATA[" + link + "]]></cell>");
                            out.print("</row>");
                            j++;
                        }
                        out.print("</rows>");
                    }
                } else {
                    int totalPages = 0;
                    int totalCount = 0;
                    List<ManageAdminDtBean> organizationGrid = null;
                    if (_search) {
                        searchField = request.getParameter("searchField");
                        searchString = request.getParameter("searchString");
                        searchOper = request.getParameter("searchOper");
                        organizationGrid = updateAdminSrBean.getOrganizationGrid(userTypeId, userId, searchField, searchString, searchOper);
                        totalCount = (int) updateAdminSrBean.getOrganizationAdminGridCount(String.valueOf(userId), String.valueOf(userTypeId), searchField, searchString, searchOper);
                    } else {
                        String orderClause = "";
                        if (sidx.equalsIgnoreCase("")) {
                            orderClause = "tlm.registeredDate desc";
                        } else if (sidx.equalsIgnoreCase("tlm.emailId")) {
                            orderClause = sidx + " " + sord;
                        } else if (sidx.equalsIgnoreCase("departmentName")) {
                            orderClause = sidx + " " + sord;
                        } else if (sidx.equalsIgnoreCase("tam.fullName")) {
                            orderClause = sidx + " " + sord;
                        } else if (sidx.equalsIgnoreCase("om.officeName")) {
                            orderClause = sidx + " " + sord;
                        }
                        organizationGrid = updateAdminSrBean.getOrganizationGrid(userTypeId, userId, orderClause);
                        totalPages = 0;
                        totalCount = (int) updateAdminSrBean.getOrganizationAdminGridCount(String.valueOf(userId), String.valueOf(userTypeId));
                    }
                    if (totalCount > 0) {
                        if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                            totalPages = totalCount / Integer.parseInt(request.getParameter("rows"));
                        } else {
                            totalPages = (totalCount / Integer.parseInt(request.getParameter("rows"))) + 1;
                        }

                    } else {
                        totalPages = 0;
                    }


                    out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                    out.print("<rows>");
                    out.print("<page>" + request.getParameter("page") + "</page>");

                    out.print("<total>" + totalPages + "</total>");
                    out.print("<records>" + organizationGrid.size() + "</records>");
                    int j = 0;
                    int no = Integer.parseInt(request.getParameter("page"));
                    if (no == 1) {
                        j = 1;
                    } else {
                        if (Integer.parseInt(rows) == 30) {
                            j = ((no - 1) * 30) + 1;
                        } else if (Integer.parseInt(rows) == 20) {
                            j = ((no - 1) * 20) + 1;
                        } else {
                            j = ((no - 1) * 10) + 1;
                        }
                    }
                    // be sure to put text data in CDATA
                    for (int i = 0; i < organizationGrid.size(); i++) {
                        out.print("<row id='" + organizationGrid.get(i).getUserId() + "'>");
                        out.print("<cell>" + j + "</cell>");
                        out.print("<cell><![CDATA[" + organizationGrid.get(i).getEmailId() + "]]></cell>");
                        out.print("<cell><![CDATA[" + organizationGrid.get(i).getFullName() + "]]></cell>");
                        out.print("<cell><![CDATA[" + organizationGrid.get(i).getOfficeName() + "]]></cell>");
                        // Code added by Palash, Dohatec
                        String link = "<a href=\"ManagePEAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewPEAdmin.jsp?userId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "&action=view\">View</a>| <a href=\"GovReplaceUser.jsp?adminId=" + organizationGrid.get(i).getAdminId() + "&adminUserId=" + organizationGrid.get(i).getUserId() + "&userTypeid=" + userTypeId + "\">Transfer</a>";
                       // End, palash
                        out.print("<cell><![CDATA[" + link + "]]></cell>");
                        out.print("</row>");
                        j++;
                    }
                    out.print("</rows>");

                }
                logger.debug(request.getParameter("action") + " : " + logUserId + " Ends");
            }*/
            //</editor-fold>
            if (request.getParameter("action").equals("fetchData")) {
                UpdateAdminSrBean updateAdminSrBean = new UpdateAdminSrBean();
                if (request.getSession().getAttribute("userId") != null) {
                    logUserId = request.getSession().getAttribute("userId").toString();
                    updateAdminSrBean.setLogUserId(logUserId);
                }
                logger.debug(request.getParameter("action") + " : " + logUserId + " Starts");
                byte userTypeId = Byte.parseByte(request.getParameter("userTypeId"));
                int userId = Integer.parseInt(request.getSession().getAttribute("userId").toString());
                int userTID = Integer.parseInt(request.getSession().getAttribute("userTypeId").toString());
                updateAdminSrBean.setLimit(1000000);
                int offset = 0;
                String RoleType = "";
                if (request.getParameter("RoleType") != null && request.getParameter("RoleType").toString() != "") {
                    RoleType = request.getParameter("RoleType").toString();
                }

                updateAdminSrBean.setOffset(offset);
            
                PrintWriter out = response.getWriter();
                int pageNo = Integer.parseInt(request.getParameter("pageNo"));
                int Size = Integer.parseInt(request.getParameter("size"));
                String Organization = request.getParameter("Organization");
                String EmailID = request.getParameter("EmailID");
                String FullName = request.getParameter("FullName");
                String OfficeName = request.getParameter("OfficeName"); 
                
                if (userTID == 1 || userTID == 19 || userTID == 21) {
                    List<ManageAdminDtBean> organizationGrid = null;
                    List<ManageAdminDtBean> organizationGridSearched = new ArrayList<ManageAdminDtBean>();
                    
                    organizationGrid = updateAdminSrBean.getOrganizationGrid(userTypeId, "tlm.registeredDate desc");
                  
                    for(int j=0;j<organizationGrid.size();j++)
                    {
                        boolean ToAdd = true;
                        if(Organization!=null && !Organization.equalsIgnoreCase(""))
                        {
                            if(!Organization.equalsIgnoreCase(organizationGrid.get(j).getDepartmentName()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(EmailID!=null && !EmailID.equalsIgnoreCase(""))
                        {
                            if(!organizationGrid.get(j).getEmailId().toLowerCase().contains(EmailID.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(FullName!=null && !FullName.equalsIgnoreCase(""))
                        {
                            if(!organizationGrid.get(j).getFullName().toLowerCase().contains(FullName.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(OfficeName!=null && !OfficeName.equalsIgnoreCase(""))
                        {
                            if(!organizationGrid.get(j).getOfficeName().toLowerCase().contains(OfficeName.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                       
                        if(ToAdd)
                        {
                            //SPCommonSearchData commonAppData = getGovData.get(j);
                            organizationGridSearched.add(organizationGrid.get(j));
                        }
                    }
                    
                    int RecordFrom = (pageNo-1)*Size;
                    
                    if (userTypeId == 5) {
                        
                        int k= 0;
                        String styleClass = "";
                        if (organizationGridSearched != null && !organizationGridSearched.isEmpty()) {
                            for(k=RecordFrom;k<RecordFrom+Size && k<organizationGridSearched.size();k++)
                            {
                                if(k%2==0){
                                    styleClass = "bgColor-white";
                                }else{
                                    styleClass = "bgColor-Green";
                                }
                                out.print("<tr class='"+styleClass+"'>");
                                out.print("<td width=\"5%\" class=\"t-align-center\">" + (k+1) + "</td>");
                                out.print("<td width=\"30%\" class=\"t-align-center\">" + organizationGridSearched.get(k).getDepartmentName() + "</td>");
                                out.print("<td width=\"25%\" class=\"t-align-center\">" + organizationGridSearched.get(k).getEmailId() + "</td>");
                                out.print("<td width=\"20%\" class=\"t-align-center\">" + organizationGridSearched.get(k).getFullName() + "</td>");
                                String link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a>";
                                String strStatus = organizationGridSearched.get(k).getStatus();
                                Short adminId = organizationGridSearched.get(k).getAdminId();
                                if (userTID == 1) { // only e-GP Admin can change status
                                    if ("approved".equalsIgnoreCase(strStatus)) {
                                        link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Deactivate</a>";
                                    } else {
                                        link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Activate</a>";
                                    }
                                }
                                out.print("<td width=\"20%\" class=\"t-align-center\">" + link + "</td>");
                                out.print("</tr>");
                            }

                        }
                        else
                        {
                            out.print("<tr>");
                            out.print("<td colspan=\"5\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                            out.print("</tr>");
                        }  
                        int totalPages = 1;
                        if (organizationGridSearched.size() > 0) {
                            totalPages = (int) (Math.ceil(Math.ceil(organizationGridSearched.size()) / Size));
                            System.out.print("totalPages--"+totalPages+"records "+ organizationGridSearched.size());
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                    } else if (userTypeId == 8) {
                        int k= 0;
                        String styleClass = "";
                        if (organizationGridSearched != null && !organizationGridSearched.isEmpty()) {
                            for(k=RecordFrom;k<RecordFrom+Size && k<organizationGridSearched.size();k++)
                            {
                                if(k%2==0){
                                    styleClass = "bgColor-white";
                                }else{
                                    styleClass = "bgColor-Green";
                                }
                                out.print("<tr class='"+styleClass+"'>");
                                out.print("<td width=\"5%\" class=\"t-align-center\">" + (k+1) + "</td>");
                                out.print("<td width=\"35%\" class=\"t-align-center\">" + organizationGridSearched.get(k).getEmailId() + "</td>");
                                out.print("<td width=\"30%\" class=\"t-align-center\">" + organizationGridSearched.get(k).getFullName() + "</td>");
                                String link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a>";
                                String strStatus = organizationGridSearched.get(k).getStatus();
                                Short adminId = organizationGridSearched.get(k).getAdminId();
                                if (userTID == 1) { // only e-GP Admin can change status
                                    if ("approved".equalsIgnoreCase(strStatus)) {
                                        link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Deactivate</a>";
                                    } else {
                                        link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Activate</a>";
                                    }
                                }
                                out.print("<td width=\"30%\" class=\"t-align-center\">" + link + "</td>");
                                out.print("</tr>");
                            }

                        }
                        else
                        {
                            out.print("<tr>");
                            out.print("<td colspan=\"4\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                            out.print("</tr>");
                        }  
                        int totalPages = 1;
                        if (organizationGridSearched.size() > 0) {
                            totalPages = (int) (Math.ceil(Math.ceil(organizationGridSearched.size()) / Size));
                            System.out.print("totalPages--"+totalPages+"records "+ organizationGridSearched.size());
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                        
                    } else if (userTypeId == 4) {
                        int k= 0;
                        String styleClass = "";
                        if (organizationGridSearched != null && !organizationGridSearched.isEmpty()) {
                            for(k=RecordFrom;k<RecordFrom+Size && k<organizationGridSearched.size();k++)
                            {
                                if(k%2==0){
                                    styleClass = "bgColor-white";
                                }else{
                                    styleClass = "bgColor-Green";
                                }
                                out.print("<tr class='"+styleClass+"'>");
                                out.print("<td width=\"5%\" class=\"t-align-center\">" + (k+1) + "</td>");
                                out.print("<td width=\"30%\" class=\"t-align-center\">" + organizationGridSearched.get(k).getEmailId() + "</td>");
                                out.print("<td width=\"25%\" class=\"t-align-center\">" + organizationGridSearched.get(k).getFullName() + "</td>");
                                out.print("<td width=\"20%\" class=\"t-align-center\">" + organizationGridSearched.get(k).getOfficeName() + "</td>");
                                String link = "<a href=\"ManagePEAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewPEAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view\">View</a>";
                                if (userTID == 1) {
                                    if ("approved".equalsIgnoreCase(organizationGridSearched.get(k).getStatus())) {
                                        link = "<a href=\"ManagePEAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | "
                                                + "<a href=\"ViewPEAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view\">View</a> | "
                                                + "<a href=\"ViewPEAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&status=deactivate&userTypeid=" + userTypeId + "\">Deactivate</a>";
                                    } else {
                                        link = "<a href=\"ManagePEAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | "
                                                + "<a href=\"ViewPEAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view\">View</a> | "
                                                + "<a href=\"ViewPEAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&status=activate&userTypeid=" + userTypeId + "\">Activate</a>";

                                    }
                                }
                                out.print("<td width=\"20%\" class=\"t-align-center\">" + link + "</td>");
                                out.print("</tr>");
                            }

                        }
                        else
                        {
                            out.print("<tr>");
                            out.print("<td colspan=\"5\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                            out.print("</tr>");
                        }  
                        int totalPages = 1;
                        if (organizationGridSearched.size() > 0) {
                            totalPages = (int) (Math.ceil(Math.ceil(organizationGridSearched.size()) / Size));
                            System.out.print("totalPages--"+totalPages+"records "+ organizationGridSearched.size());
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                       
                    } else if (userTypeId == 12) {
                        int k= 0;
                        String styleClass = "";
                        if (organizationGridSearched != null && !organizationGridSearched.isEmpty()) {
                            for(k=RecordFrom;k<RecordFrom+Size && k<organizationGridSearched.size();k++)
                            {
                                if(k%2==0){
                                    styleClass = "bgColor-white";
                                }else{
                                    styleClass = "bgColor-Green";
                                }
                                out.print("<tr class='"+styleClass+"'>");
                                out.print("<td width=\"5%\" class=\"t-align-center\">" + (k+1) + "</td>");
                                out.print("<td width=\"35%\" class=\"t-align-center\">" + organizationGridSearched.get(k).getEmailId() + "</td>");
                                out.print("<td width=\"30%\" class=\"t-align-center\">" + organizationGridSearched.get(k).getFullName() + "</td>");
                                String link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a>";
                                String strStatus = organizationGridSearched.get(k).getStatus();
                                Short adminId = organizationGridSearched.get(k).getAdminId();
                                if (userTID == 1) {
                                    if ("approved".equalsIgnoreCase(strStatus)) {
                                        link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Deactivate</a>";
                                    } else {
                                        link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Activate</a>";
                                    }
                                }
                                out.print("<td width=\"30%\" class=\"t-align-center\">" + link + "</td>");
                                out.print("</tr>");
                            }

                        }
                        else
                        {
                            out.print("<tr>");
                            out.print("<td colspan=\"4\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                            out.print("</tr>");
                        }  
                        int totalPages = 1;
                        if (organizationGridSearched.size() > 0) {
                            totalPages = (int) (Math.ceil(Math.ceil(organizationGridSearched.size()) / Size));
                            System.out.print("totalPages--"+totalPages+"records "+ organizationGridSearched.size());
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                       
                    } else if (userTypeId == 19) {
                        int k= 0;
                        String styleClass = "";
                        if (organizationGridSearched != null && !organizationGridSearched.isEmpty()) {
                            for(k=RecordFrom;k<RecordFrom+Size && k<organizationGridSearched.size();k++)
                            {
                                if(k%2==0){
                                    styleClass = "bgColor-white";
                                }else{
                                    styleClass = "bgColor-Green";
                                }
                                out.print("<tr class='"+styleClass+"'>");
                                out.print("<td width=\"5%\" class=\"t-align-center\">" + (k+1) + "</td>");
                                out.print("<td width=\"35%\" class=\"t-align-center\">" + organizationGridSearched.get(k).getEmailId() + "</td>");
                                out.print("<td width=\"30%\" class=\"t-align-center\">" + organizationGridSearched.get(k).getFullName() + "</td>");
                                String link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a>";
                                String strStatus = organizationGridSearched.get(k).getStatus();
                                Short adminId = organizationGridSearched.get(k).getAdminId();
                                if (userTID == 1) { // only e-GP Admin can change status
                                    if ("approved".equalsIgnoreCase(strStatus)) {
                                        link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Deactivate</a> | <a href=\"ManageUserRights.jsp?uId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&msg=\">Rights</a>";
                                    } else {
                                        link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Activate</a> | <a href=\"ManageUserRights.jsp?uId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&msg=\">Rights</a>";
                                    }
                                }
                                out.print("<td width=\"30%\" class=\"t-align-center\">" + link + "</td>");
                                out.print("</tr>");
                            }

                        }
                        else
                        {
                            out.print("<tr>");
                            out.print("<td colspan=\"4\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                            out.print("</tr>");
                        }  
                        int totalPages = 1;
                        if (organizationGridSearched.size() > 0) {
                            totalPages = (int) (Math.ceil(Math.ceil(organizationGridSearched.size()) / Size));
                            System.out.print("totalPages--"+totalPages+"records "+ organizationGridSearched.size());
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                      
                        } else if (userTypeId == 21) {
                            int k= 0;
                            String styleClass = "";
                            if (organizationGridSearched != null && !organizationGridSearched.isEmpty()) {
                                for(k=RecordFrom;k<RecordFrom+Size && k<organizationGridSearched.size();k++)
                                {
                                    if(k%2==0){
                                        styleClass = "bgColor-white";
                                    }else{
                                        styleClass = "bgColor-Green";
                                    }
                                    out.print("<tr class='"+styleClass+"'>");
                                    out.print("<td width=\"5%\" class=\"t-align-center\">" + (k+1) + "</td>");
                                    out.print("<td width=\"35%\" class=\"t-align-center\">" + organizationGridSearched.get(k).getEmailId() + "</td>");
                                    out.print("<td width=\"30%\" class=\"t-align-center\">" + organizationGridSearched.get(k).getFullName() + "</td>");
                                    String link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a>";
                                    String strStatus = organizationGridSearched.get(k).getStatus();
                                    Short adminId = organizationGridSearched.get(k).getAdminId();
                                    if (userTID == 1) { 
                                        if ("approved".equalsIgnoreCase(strStatus)) {
                                            link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Deactivate</a>";
                                        } else {
                                            link = "<a href=\"ManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Activate</a>";
                                        }
                                    }
                                    out.print("<td width=\"30%\" class=\"t-align-center\">" + link + "</td>");
                                    out.print("</tr>");
                                }

                            }
                            else
                            {
                                out.print("<tr>");
                                out.print("<td colspan=\"4\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                                out.print("</tr>");
                            }  
                            int totalPages = 1;
                            if (organizationGridSearched.size() > 0) {
                                totalPages = (int) (Math.ceil(Math.ceil(organizationGridSearched.size()) / Size));
                                System.out.print("totalPages--"+totalPages+"records "+ organizationGridSearched.size());
                                if (totalPages == 0) {
                                    totalPages = 1;
                                }
                            }
                            out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                      
                        } else if (userTypeId == 20) {
                            int k= 0;
                            String styleClass = "";
                            if (organizationGridSearched != null && !organizationGridSearched.isEmpty()) {
                                for(k=RecordFrom;k<RecordFrom+Size && k<organizationGridSearched.size();k++)
                                {
                                    if(k%2==0){
                                        styleClass = "bgColor-white";
                                    }else{
                                        styleClass = "bgColor-Green";
                                    }
                                    out.print("<tr class='"+styleClass+"'>");
                                    out.print("<td width=\"5%\" class=\"t-align-center\">" + (k+1) + "</td>");
                                    out.print("<td width=\"35%\" class=\"t-align-center\">" + organizationGridSearched.get(k).getEmailId() + "</td>");
                                    out.print("<td width=\"30%\" class=\"t-align-center\">" + organizationGridSearched.get(k).getFullName() + "</td>");
                                    String link = "<a href=\"OANDMRegister.jsp?uId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&isUser=Yes&action=Edit\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view&isUser=Yes&msg=\">View</a>";
                                    String roleLink = "";
                                    String rType="Role Wise";
                                    String strStatus = organizationGridSearched.get(k).getStatus();
                                    Short adminId = organizationGridSearched.get(k).getAdminId();
                                    if (organizationGridSearched.get(k).getRollId() == 0) {
                                        roleLink = "| <a href=\"ManageUserRights.jsp?uId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&msg=\">Rights</a>";
                                        rType="Individual";
                                    }
                                    if (userTID == 1 || userTID == 19) { 
                                        if ("approved".equalsIgnoreCase(strStatus)) {
                                           link = "<a href=\"OANDMRegister.jsp?uId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&isUser=Yes&action=Edit\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view&isUser=Yes&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Deactivate</a> "+roleLink;
                                       } else {
                                           link = "<a href=\"OANDMRegister.jsp?uId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&isUser=Yes&action=Edit\">Edit</a> | <a href=\"ViewManageAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view&isUser=Yes&msg=\">View</a> | <a href=\"ManageAdminStatus.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&userOldStatus=" + strStatus + "&adminId=" + adminId + "&msg=\">Activate</a> "+roleLink;
                                       }
                                    }
                                    out.print("<td width=\"30%\" class=\"t-align-center\">" + link + "</td>");
                                    out.print("</tr>");
                                }

                            }
                            else
                            {
                                out.print("<tr>");
                                out.print("<td colspan=\"4\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                                out.print("</tr>");
                            }  
                            int totalPages = 1;
                            if (organizationGridSearched.size() > 0) {
                                totalPages = (int) (Math.ceil(Math.ceil(organizationGridSearched.size()) / Size));
                                System.out.print("totalPages--"+totalPages+"records "+ organizationGridSearched.size());
                                if (totalPages == 0) {
                                    totalPages = 1;
                                }
                            }
                            out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                        }
                } else {
                   
                    List<ManageAdminDtBean> organizationGrid = null;
                    List<ManageAdminDtBean> organizationGridSearched = new ArrayList<ManageAdminDtBean>();
                    
                    organizationGrid = updateAdminSrBean.getOrganizationGrid(userTypeId, userId, "tlm.registeredDate desc");
                    
                    for(int j=0;j<organizationGrid.size();j++)
                    {
                        boolean ToAdd = true;
                        if(Organization!=null && !Organization.equalsIgnoreCase(""))
                        {
                            if(!Organization.equalsIgnoreCase(organizationGrid.get(j).getDepartmentName()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(EmailID!=null && !EmailID.equalsIgnoreCase(""))
                        {
                            if(!organizationGrid.get(j).getEmailId().toLowerCase().contains(EmailID.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(FullName!=null && !FullName.equalsIgnoreCase(""))
                        {
                            if(!organizationGrid.get(j).getFullName().toLowerCase().contains(FullName.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                       
                        if(ToAdd)
                        {
                            //SPCommonSearchData commonAppData = getGovData.get(j);
                            organizationGridSearched.add(organizationGrid.get(j));
                        }
                    }
                    
                    int RecordFrom = (pageNo-1)*Size;

                    int k= 0;
                    String styleClass = "";
                    if (organizationGridSearched != null && !organizationGridSearched.isEmpty()) {
                        for(k=RecordFrom;k<RecordFrom+Size && k<organizationGridSearched.size();k++)
                        {
                            if(k%2==0){
                                styleClass = "bgColor-white";
                            }else{
                                styleClass = "bgColor-Green";
                            }
                            out.print("<tr class='"+styleClass+"'>");
                            out.print("<td width=\"5%\" class=\"t-align-center\">" + (k+1) + "</td>");
                            out.print("<td width=\"30%\" class=\"t-align-center\">" + organizationGridSearched.get(k).getEmailId() + "</td>");
                            out.print("<td width=\"25%\" class=\"t-align-center\">" + organizationGridSearched.get(k).getFullName() + "</td>");
                            out.print("<td width=\"20%\" class=\"t-align-center\">" + organizationGridSearched.get(k).getOfficeName() + "</td>");
                            String link = "<a href=\"ManagePEAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "\">Edit</a> | <a href=\"ViewPEAdmin.jsp?userId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "&action=view\">View</a>| <a href=\"GovReplaceUser.jsp?adminId=" + organizationGridSearched.get(k).getAdminId() + "&adminUserId=" + organizationGridSearched.get(k).getUserId() + "&userTypeid=" + userTypeId + "\">Transfer</a>";
                            out.print("<td width=\"20%\" class=\"t-align-center\">" + link + "</td>");
                            out.print("</tr>");
                        }

                    }
                    else
                    {
                        out.print("<tr>");
                        out.print("<td colspan=\"5\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        out.print("</tr>");
                    }  
                    int totalPages = 1;
                    if (organizationGridSearched.size() > 0) {
                        totalPages = (int) (Math.ceil(Math.ceil(organizationGridSearched.size()) / Size));
                        System.out.print("totalPages--"+totalPages+"records "+ organizationGridSearched.size());
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                }
                logger.debug(request.getParameter("action") + " : " + logUserId + " Ends");
            }

        } catch (Exception e) {
            logger.error(request.getParameter("action") + " : " + logUserId + e);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
