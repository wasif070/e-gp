/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.CommonAppData;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.CommonSPReturn;
import com.cptu.egp.eps.dao.storedprocedure.SPProjectDetailReturn;
import com.cptu.egp.eps.dao.storedprocedure.SPProjectFPReturn;
import com.cptu.egp.eps.dao.storedprocedure.SPProjectRolesReturn;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblProcurementMethod;
import com.cptu.egp.eps.model.table.TblProjectRoles;
import com.cptu.egp.eps.service.serviceimpl.CreateProjectServiceImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.SelectItem;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author parag
 */
public class ProjectSrBean extends HttpServlet {

    /**
     * Class handles all the request regarding project created by OrgAdmin
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     *
     */
    private CreateProjectServiceImpl projectService = (CreateProjectServiceImpl) AppContext.getSpringBean("CreateProjectService");
    /**
     * CreateProjectServiceImpl Spring bean
     */
    public String emailId = "";
    /**
     * String emailId
     */
    public int officeId = 0;
    private AuditTrail auditTrail;
    private String logUserId = "0";
    /**
     * Logging purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     * Setting AUdit Trail Object for Audit Trail entry
     * @param auditTrail
     */
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        projectService.setAuditTrail(auditTrail);
    }

   
    
    
    /**
     * Getter of EmailId
     * @return
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * Setter of EmailId
     * @param emailId
     */
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    /**
     * Getter of OfficeId
     * @return
     */
    public int getOfficeId() {
        return officeId;
    }

    /**
     * Setter of OfficeId
     * @param officeId
     */
    public void setOfficeId(int officeId) {
        this.officeId = officeId;
    }
    private static final Logger LOGGER = Logger.getLogger(ProjectSrBean.class);
    
    /**
     * 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
                projectService.setUserId(logUserId);
        }
        LOGGER.debug("processRequest : "+logUserId+" Starts");

        String action = request.getParameter("action");
        String funName = request.getParameter("funName");
        String department = request.getParameter("deptId");
        String prevOffId = request.getParameter("prevOffId");
        String role = request.getParameter("role");
        //String email = request.getParameter("emailId");
        String office = request.getParameter("officeId");
        String funnum = request.getParameter("funnum");
        String snUserId = request.getSession().getAttribute("userId").toString();
        //String search = request.getParameter("search");

        String projectCode = request.getParameter("projectCode");
        String pId = request.getParameter("projectId");
        String uId = request.getParameter("userId");
        String projectName = request.getParameter("projectName");
        // String funNameforPD = request.getParameter("funNameforPD");

        String str = "";
        try {
            if (funName != null) {
                if ("Office".equals(funName)) {
                    LOGGER.debug("ProjectSrbean : funname : office" + logUserId + " Starts");
                    int deptId = Integer.parseInt(department);
                    LOGGER.debug(" dept id :: " + deptId);
                    str = getOffice(deptId);
                    LOGGER.debug(" in funname str :" + str);
                    out.print(str);
                    out.flush();
                    LOGGER.debug("ProjectSrbean : funname : office" + logUserId + " Ends");
                } else if ("DpOffice".equalsIgnoreCase(funName)) {
                    LOGGER.debug("ProjectSrbean : funname : DpOffice" + logUserId + " Starts");
                    int deptId = Integer.parseInt(department);
                    LOGGER.debug(" dept id :: " + deptId);
                    str = getDpOffice(deptId, prevOffId, funnum);
                    LOGGER.debug(" in funname str :" + str);
                    out.print(str);
                    out.flush();
                    LOGGER.debug("ProjectSrbean : funname : DpOffice" + logUserId + " Ends");
                }
                if ("SOffice".equals(funName)) {
                    LOGGER.debug("ProjectSrbean : funname : SOffice" + logUserId + " Starts");
                    int deptId = Integer.parseInt(department);
                    int officeId = Integer.parseInt(office);
                    LOGGER.debug(" dept id :: " + deptId);
                    str = getSelectedOffice(deptId, officeId);
                    LOGGER.debug(" in funname str :" + str);
                    out.print(str);
                    out.flush();
                    LOGGER.debug("ProjectSrbean : funname : SOffice" + logUserId + " Ends");
                }
                if ("verifyProjCode".equals(funName)) {
                    LOGGER.debug("ProjectSrbean : funname : verifyProjCode" + logUserId + " Starts");
                    int projId = Integer.parseInt(pId);
                    str = verifyProjectCode(projectCode, projId);
                    LOGGER.debug(" in funname str :" + str);
                    out.print(str);
                    out.flush();
                    LOGGER.debug("ProjectSrbean : funname : verifyProjCode" + logUserId + " Ends");
                }
                if ("verifyProjName".equals(funName)) {
                    LOGGER.debug("ProjectSrbean : funname : verifyProjName" + logUserId + " Starts");
                    int projId = Integer.parseInt(pId);
                    str = verifyProjectName(projectName, projId);
                    LOGGER.debug(" in funname str :" + str);
                    out.print(str);
                    out.flush();
                    LOGGER.debug("ProjectSrbean : funname : verifyProjName" + logUserId + " Ends");
                }
                if ("checkPD".equals(funName)) {
                    LOGGER.debug("ProjectSrbean : funname : checkPD" + logUserId + " Starts");
                    int projId = Integer.parseInt(pId);
                    if (isDataAvailableForPDForProjectId(projId, "PD")) {
                        out.print("true");
                    } else {
                        out.print("false");
                    }
                    LOGGER.debug(" in funname str :" + str);
                    out.flush();
                    LOGGER.debug("ProjectSrbean : funname : checkPD" + logUserId + " Ends");
                }
                if ("checkProjectPE".equals(funName)) {
                    LOGGER.debug("ProjectSrbean : funname : checkProjectPE" + logUserId + " Starts");
                    int projId = Integer.parseInt(pId);
                    if (isDataAvailableForPDForProjectId(projId, "PE")) {
                        out.print("true");
                    } else {
                        out.print("false");
                    }
                    LOGGER.debug(" in funname str :" + str);
                    out.flush();
                    LOGGER.debug("ProjectSrbean : funname : checkProjectPE" + logUserId + " Ends");
                }
                if ("checkPE".equals(funName)) {
                    LOGGER.debug("ProjectSrbean : funname : checkPE" + logUserId + " Starts");
                    LOGGER.debug(" in check PE ===============================================");
                    int projId = Integer.parseInt(pId);
                    int user = Integer.parseInt(uId);
                    if (isDataAvailableForProjForUser(projId, user, 1)) {
                        out.print("true");
                    } else {
                        out.print("false");
                    }
                    LOGGER.debug(" in funname str :" + str);
                    out.flush();
                    LOGGER.debug("ProjectSrbean : funname : checkPE" + logUserId + " Ends");
                }
                if ("checkAU".equals(funName)) {
                    LOGGER.debug("ProjectSrbean : funname : checkAU" + logUserId + " Starts");
                    int projId = Integer.parseInt(pId);
                    int user = Integer.parseInt(uId);
                    if (isDataAvailableForProjForUser(projId, user, 5)) {
                        out.print("true");
                    } else {
                        out.print("false");
                    }
                    LOGGER.debug(" in funname str :" + str);
                    out.flush();
                    LOGGER.debug("ProjectSrbean : funname : checkAU" + logUserId + " Ends");
                }
//                if (funName.equals("search")) {
//
//                    int emailId = Integer.parseInt(email);
//                    int officeId = Integer.parseInt(office);
//
//                    
//
//                    str = getSearchData(emailId, officeId);
//                    
//                    out.print(str);
//                    out.flush();
//                }
                if("getEmprole".equalsIgnoreCase(funName)){
                    LOGGER.debug("ProjectSrbean : funname : getEmprole" + logUserId + " Starts");
                    str = getEmployeeRole(office,role);
                    out.print(str);
                    LOGGER.debug(" in funname str :" + funName);
                    out.flush();
                    LOGGER.debug("ProjectSrbean : funname : getEmprole" + logUserId + " Ends");
                }
            }



            if ("create".equalsIgnoreCase(action) || "update".equalsIgnoreCase(action)) {
                LOGGER.debug("ProjectSrbean : action : create || update" + logUserId + " Starts");
                String startDate = "";
                String endDate = "";

                
                
                int projectId = 0;

                if ("update".equalsIgnoreCase(action)) {
                    projectId = Integer.parseInt(request.getParameter("hidProjectId").toString());
                    LOGGER.debug("update  ==============>>> " + projectId);

                }

                BigDecimal projectCost = null;
                if (request.getParameter("txtProjCost") != null) {
                    MathContext mc = new MathContext(10);
                    projectCost = new BigDecimal(request.getParameter("txtProjCost").toString()).multiply(new BigDecimal("1000000"),mc);
                }

                LOGGER.debug(" projectCost  " + projectCost);

                Date sdate = new Date();
                Date edate = new Date();

                if (request.getParameter("dtStartDate") != null) {
                    startDate = request.getParameter("dtStartDate").toString();
                }
                if (request.getParameter("dtEndDate") != null) {
                    endDate = request.getParameter("dtEndDate").toString();
                }
                    //SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
	           // SimpleDateFormat sd1 = new SimpleDateFormat("dd-MM-yyyy");

                   // sdate = sd1.format(sd.parse("startDate"));

                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                sdate = format.parse(startDate);
                edate = format.parse(endDate);

                String dPartner[] = null;
                StringBuilder dPartValue = new StringBuilder();
                if (request.getParameter("cmbDPartner") != null && !"".equalsIgnoreCase(request.getParameter("cmbDPartner"))) {
                    dPartner = request.getParameterValues("cmbDPartner");
                    for (String string : dPartner) {
                        dPartValue.append(string + "@$");
                    }
                    
                }
                
                String sourceOfFund[] = null;
                String sourceFund = "";
                if (request.getParameter("cmbSource") != null) {
                    sourceOfFund = request.getParameterValues("cmbSource");
                    for (String sfundStr : sourceOfFund) {
                        sourceFund += sfundStr + ",";
                    }
                    sourceFund = sourceFund.substring(0, sourceFund.length() - 1);
                    
                }

                List<CommonSPReturn> commonSPReturns = null;
                BigDecimal projId = null;
                if ("create".equalsIgnoreCase(action)) {
                    projectService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
                    commonSPReturns = projectService.insertProjectDetailsBySP(request.getParameter("txtProjName").trim(),
                            request.getParameter("txtProjCode").trim(),
                            Integer.parseInt(request.getParameter("cmbProg")),
                            projectCost, sdate, edate, sourceFund, dPartValue.toString(), "create", 0,Integer.parseInt(snUserId));
                    projId = commonSPReturns.get(0).getId();
                    LOGGER.debug(commonSPReturns.get(0).getMsg());
                    
                }
                if ("update".equalsIgnoreCase(action)) {
                    boolean delProjPar = false;
                    projectService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
                    delProjPar = projectService.deleteProjectPartners(projectId);

                    commonSPReturns = projectService.insertProjectDetailsBySP(request.getParameter("txtProjName").trim(),
                            request.getParameter("txtProjCode").trim(),
                            Integer.parseInt(request.getParameter("cmbProg")),
                            projectCost, sdate, edate, sourceFund, dPartValue.toString(), "update", projectId,Integer.parseInt(snUserId));
                    try {
                        LOGGER.debug(" in delete projectId" + projectId);

                        if ("Government".contains(sourceFund) || "Own fund".contains(sourceFund)) {
                            delProjPar = projectService.deleteProjectPartners(projectId);
                        }
                        
                    } catch (Exception e) {
                        LOGGER.error(" Error in delProjPar ::: " + e);
                    }

                    projId = new BigDecimal(projectId);
                }

                out.println("true");
                LOGGER.debug(" out put size :: " + commonSPReturns.size());

                LOGGER.debug("ProjectSrbean : action : create || update" + logUserId + " Ends");
                response.sendRedirect("admin/ProjectRoles.jsp?hidProjectId=" + projId);

            } else if ("createProjectRoles".equalsIgnoreCase(action) || "updateProjRoles".equalsIgnoreCase(action)) {
                LOGGER.debug("ProjectSrbean : action : createProjectRoles || updateProjRoles" + logUserId + " Starts");
                int projectId = 0;
                int userId = 0;

                if (request.getParameter("hidProjectId") != null) {
                    projectId = Integer.parseInt(request.getParameter("hidProjectId").toString());
                    
                }

                if (request.getParameter("hidUser") != null) {
                    userId = Integer.parseInt(request.getParameter("hidUser"));
                }


                StringBuilder forXmlStr = new StringBuilder();
                forXmlStr.append("<root>");

                String procurementRole[];
                //StringBuilder pRole = new StringBuilder();
                request.getParameter("txtProjCode");
                if (request.getParameter("chkProcurementRole") != null) {
                    procurementRole = request.getParameterValues("chkProcurementRole");
                    LOGGER.debug(" procurementRole " + procurementRole);
                    for (String string : procurementRole) {
                        
                        forXmlStr.append("<tbl_ProjectRoles projectId=\"" + projectId + "\"");
                        forXmlStr.append(" procurementRoleId=\"" + string + "\" ");
                        forXmlStr.append(" userId=\"" + userId + "\" /> ");
                    }
                    //pRole.append(procurementRole + "@");
                }
                forXmlStr.append("</root>");

                try {
                    if ("updateProjRoles".equalsIgnoreCase(action)) {
                        List<CommonMsgChk> deleteRoles = projectService.projectOperationDetailsBySP("insdel",
                                "tbl_ProjectRoles", "", " projectId = " + projectId + " and  userId = " + userId);

                        LOGGER.debug("  deleteRoles -------------------  ::: " + deleteRoles);
                    }

                    LOGGER.debug("  pRoel :: " + forXmlStr.toString());
                    List<CommonMsgChk> commonMsgChks = projectService.projectOperationDetailsBySP("insert",
                            "tbl_ProjectRoles", forXmlStr.toString(), "");

                } catch (Exception e) {
                    LOGGER.error("  Error in " + action + "  tbl_ProjectRoles ::" + e);
                }


                int officeId = Integer.parseInt(request.getParameter("hidProjOffice"));
                StringBuilder forXmlforOffice = new StringBuilder();
                forXmlforOffice.append("<root>");



                forXmlforOffice.append("<tbl_ProjectOffice projectId=\"" + projectId + "\"");
                forXmlforOffice.append(" officeId=\"" + officeId + "\" ");
                forXmlforOffice.append(" userId=\"" + userId + "\" /> ");
                forXmlforOffice.append("</root>");

                boolean flag = false;
                try {

                    if ("updateProjRoles".equalsIgnoreCase(action)) {
                        List<CommonMsgChk> deleteOffice = projectService.projectOperationDetailsBySP("insdel",
                                "tbl_ProjectOffice", "", " officeId = " + officeId);

                        LOGGER.debug("  deleteOffice -------------------------- ::: " + deleteOffice);
                    }

                    List<CommonMsgChk> commonMsgForOffice = projectService.projectOperationDetailsBySP("insert",
                            "tbl_ProjectOffice", forXmlforOffice.toString(), "");
                    flag = true;

                } catch (Exception e) {
                    LOGGER.debug("  Error in " + action + "  tbl_ProjectOffice :: " + e);
                }

                LOGGER.debug("ProjectSrbean : action : createProjectRoles || updateProjRoles" + logUserId + " Ends");
                response.sendRedirect("admin/ProjectRoles.jsp?hidProjectId=" + projectId);

            } else if ("Remove".equalsIgnoreCase(action)) {
                LOGGER.debug("ProjectSrbean : action : Remove : " + logUserId + " Starts");
                int projectId = 0;
                int userId = 0;
                int officeId = 0;

                if (request.getParameter("hidProjectId") != null) {
                    projectId = Integer.parseInt(request.getParameter("hidProjectId").toString());
                    
                }

                if (request.getParameter("userId") != null) {
                    userId = Integer.parseInt(request.getParameter("userId"));
                }
                if (request.getParameter("projectId") != null) {
                    officeId = Integer.parseInt(request.getParameter("projectId"));
                }

                LOGGER.debug(" in Remove  userId ::::" + userId + " :: officeID ::" + officeId);

                List<CommonMsgChk> deleteRoles = projectService.projectOperationDetailsBySP("insdel",
                        "tbl_ProjectRoles", "", " projectId = " + projectId + " and  userId = " + userId);

               LOGGER.debug("  in Remove deleteRoles :: " + deleteRoles);

                List<CommonMsgChk> deleteOffice = projectService.projectOperationDetailsBySP("insdel",
                        "tbl_ProjectOffice", "", " officeId = " + officeId);

                LOGGER.debug("  in Remove deleteOffice :: " + deleteOffice);

                List<CommonMsgChk> deleteFP = projectService.projectOperationDetailsBySP("insdel",
                        "tbl_ProjectFinPower", "", " projectId = " + projectId + " and  userId = " + userId);

                LOGGER.debug("  Remove  deleteFP  ::: " + deleteFP);
                
                LOGGER.debug("ProjectSrbean : action : Remove : " + logUserId + " Ends");
                response.sendRedirect("admin/ProjectRoles.jsp?hidProjectId=" + projectId);
            } else if ("assignFP".equalsIgnoreCase(action) || "deleteFP".equalsIgnoreCase(action)) {

                LOGGER.debug("ProjectSrbean : action : assignFP || deleteFP" + logUserId + " Starts");
                LOGGER.debug(" count is :: " + request.getParameter("txtCount"));
                int count = Integer.parseInt(request.getParameter("txtCount").toString());
                LOGGER.debug(" count is  :: " + count);
                int projectId = 1;
                //byte procurementMethod=0;
                String procurementMethod = "";
                String operator = "";

                int userId = 0;
                if (request.getParameter("userId") != null) {
                    userId = Integer.parseInt(request.getParameter("userId").toString());
                }
                if (request.getParameter("hidProjectId") != null) {
                    projectId = Integer.parseInt(request.getParameter("hidProjectId").toString());
                    
                }
                StringBuilder forXmlStr = new StringBuilder();
                forXmlStr.append("<root>");

                //int j=0;
                for (int i = 1; i <= count; i++) {

                    if (request.getParameter("cmbMethod" + i + "_" + 1) != null) {

                        procurementMethod = request.getParameter("cmbMethod" + i + "_" + 1);
                        operator = request.getParameter("cmbOperator" + i + "_" + 2);
                        BigDecimal amount = new BigDecimal(request.getParameter("txtAmount" + i + "_" + 3));

                        forXmlStr.append("<tbl_ProjectFinPower projectId=\"" + projectId + "\"");
                        forXmlStr.append(" userId=\"" + userId + "\" procurementMethodId=\"" + procurementMethod + "\" ");
                        forXmlStr.append(" amount=\"" + amount + "\" ");
                        forXmlStr.append(" operation=\"" + operator + "\" ");
                        forXmlStr.append("  /> ");

                    }
                }
                forXmlStr.append("</root>");

                if ("deleteFP".equalsIgnoreCase(action)) {
                    List<CommonMsgChk> deleteFP = projectService.projectOperationDetailsBySP("insdel",
                            "tbl_ProjectFinPower", "", " projectId = " + projectId + " and  userId = " + userId);
                    LOGGER.debug("  deleteFP  ::: " + deleteFP);
                }

                LOGGER.debug("  assignFP =============>>>>>>>>>> :: " + forXmlStr.toString());

                List<CommonMsgChk> commonMsgChks = projectService.projectOperationDetailsBySP("insert",
                        "tbl_ProjectFinPower", forXmlStr.toString(), "");

                LOGGER.debug("  commonMsgChks  :: " + commonMsgChks);
                //response.sendRedirect("admin/ProjectRoles.jsp?hidProjectId="+projectId);
                LOGGER.debug("ProjectSrbean : action : assignFP || deleteFP" + logUserId + " Ends");
                response.sendRedirect("admin/ProjectDetail.jsp");
            } else if ("approve".equalsIgnoreCase(action)) {
                LOGGER.debug(" in finalSubmission");
                int projectId = 0;

                if (request.getParameter("hidProjectId") != null) {
                    projectId = Integer.parseInt(request.getParameter("hidProjectId"));
                }

                boolean flag = projectService.projectMasterUpdate(projectId);
                response.sendRedirect("admin/ProjectDetail.jsp?hidProjectId=" + projectId + "&flag=" + flag + "&action=" + action);
            }

        } catch (Exception e) {
            LOGGER.error(" Error in ProjectSrBean :: " + e);
        } finally {
            out.close();
        }
        LOGGER.debug("processRequest : "+logUserId+" Ends");
    }
    private List<SelectItem> departmentList = new ArrayList<SelectItem>();

    /**
     * For handiling ajax call for department
     * @return String
     */
    public List<SelectItem> getDepartmentList() {
        projectService.setUserId(logUserId);
        LOGGER.debug("getDepartmentList : "+logUserId+" Starts");
        try {
        if (departmentList.isEmpty()) {
            List<TblDepartmentMaster> listDepartment = projectService.getOrganizationDetail();
            for (TblDepartmentMaster tblDepartmentMaster : listDepartment) {
                departmentList.add(new SelectItem(tblDepartmentMaster.getDepartmentId(), tblDepartmentMaster.getDepartmentName()));
            }
        }
        } catch (Exception e) {
            LOGGER.error("getDepartmentList : "+logUserId+" : "+e);
        }
        LOGGER.debug("processRequest : "+logUserId+" Ends");
        return departmentList;
    }

    //chalapthi-- getting Dp Office
    /**
     * Handle ajax request for loading Development Partner List
     * @param deptId
     * @param prevOffId
     * @param funnum
     * @return String
     */
    public String getDpOffice(int deptId, String prevOffId, String funnum) {
        projectService.setUserId(logUserId);
        LOGGER.debug("getDpOffice : "+logUserId+" Starts");
        StringBuilder str = new StringBuilder();
        List<CommonAppData> list = null;
        int flag = Integer.parseInt(funnum);
        int prevofcid = 0;
        if (!"null".equals(prevOffId)) {
            prevofcid = Integer.parseInt(prevOffId);
        }

        try {
            if (flag == 1) {
                list = projectService.getDetailsBySP("Office", "" + deptId, "0");
            } else {
                list = projectService.getDetailsBySP("DpOffice", "" + deptId, "0");
            }
            for (CommonAppData object : list) {
                if (prevofcid == Integer.parseInt(object.getFieldName1())) {
                    str.append("<option selected='true' value='" + object.getFieldName1() + "'>" + object.getFieldName2() + "</option>");
                } else {
                    str.append("<option value='" + object.getFieldName1() + "'>" + object.getFieldName2() + "</option>");
                }
            }
            LOGGER.debug(" Str is ::: " + str);
        } catch (Exception e) {
            LOGGER.error("getDpOffice : "+logUserId+" :"+e);
        }
        LOGGER.debug("getDpOffice : "+logUserId+" Ends");
        return str.toString();
    }

    /**
     * Handle ajax request for loading Office List
     * @param deptId
     * @return
     */
    private String getOffice(int deptId) {
        projectService.setUserId(logUserId);
        LOGGER.debug("getOffice : "+logUserId+" Starts");
        StringBuilder str = new StringBuilder();
        try {
            List<CommonAppData> list = projectService.getDetailsBySP("Office", "" + deptId, "0");
            for (CommonAppData object : list) {
                str.append("<option value='" + object.getFieldName1() + "'>" + object.getFieldName2() + "</option>");
            }
            LOGGER.debug(" Str is ::: " + str);
        } catch (Exception e) {
            LOGGER.error("getOffice : "+logUserId+" :"+e);
        }
        LOGGER.debug("getOffice : "+logUserId+" Ends");
        return str.toString();
    }

    /**
     * Handle ajax request for loading Office List with default selected office option
     * @param deptId
     * @param officeId
     * @return
     */
    private String getSelectedOffice(int deptId, int officeId) {
        projectService.setUserId(logUserId);
        LOGGER.debug("getSelectedOffice : "+logUserId+" Starts");
        StringBuilder str = new StringBuilder();
        try {
            List<CommonAppData> list = projectService.getDetailsBySP("Office", "" + deptId, "0");
            for (CommonAppData object : list) {
                if (Integer.parseInt(object.getFieldName1()) == officeId) {
                    str.append("<option value='" + object.getFieldName1() + "' selected >" + object.getFieldName2() + "</option>");
                } else {
                    str.append("<option value='" + object.getFieldName1() + "'>" + object.getFieldName2() + "</option>");
                }
            }
        } catch (Exception e) {
            LOGGER.debug("getSelectedOffice : "+logUserId+" :"+e);
        }
        LOGGER.debug("getSelectedOffice : "+logUserId+" Ends");
        return str.toString();
    }

    /**
     * Give list of user
     * @param pRoleType
     * @return List<CommonAppData> 
     */
    public List<CommonAppData> getSearchList(String pRoleType) {
        projectService.setUserId(logUserId);
        LOGGER.debug("getSearchList : "+logUserId+" Starts");
        List<CommonAppData> searchList = new ArrayList<CommonAppData>();
        try {
            searchList = projectService.getDetailsBySP("Searchuser", "" + pRoleType, "" + getOfficeId());
        } catch (Exception e) {
            LOGGER.error("getSearchList : "+logUserId+" Ends"+e);
        }
        LOGGER.debug("getSearchList : "+logUserId+" Ends");
        return searchList;
    }

    /**
     * Get work flow SearchList
     * @param field1
     * @param field2
     * @param field3
     * @return
     */
    public List<CommonAppData> getWfSearchList(String field1, String field2, String field3) {
        projectService.setUserId(logUserId);
        LOGGER.debug("getWfSearchList : "+logUserId+" Starts");
        List<CommonAppData> cad = null;
        try {
            cad = projectService.getDetailsBySP(field1, field2, field3);
        } catch (Exception e) {
            LOGGER.error("getWfSearchList : "+logUserId+" :"+ e);
        }
        LOGGER.debug("getWfSearchList : "+logUserId+" Ends");
        return cad;
    }
    private List<SelectItem> programList = new ArrayList<SelectItem>();

    /**
     * Handle Ajax Request of Loading ProgramList
     * @return List<SelectItem>
     */
    public List<SelectItem> getProgramList() {
        projectService.setUserId(logUserId);
        LOGGER.debug("getProgramList : "+logUserId+" Starts");
        try {
            List<CommonAppData> programList1 = projectService.getDetailsBySP("programme", " ", " ");
            programList.add(new SelectItem("0", "---Select Programme---"));
            for (CommonAppData commonAppData : programList1) {
                programList.add(new SelectItem(commonAppData.getFieldName1(), commonAppData.getFieldName2()));
            }
            LOGGER.debug(" programList is ::: " + programList);
        } catch (Exception e) {
            LOGGER.error("getProgramList : "+logUserId+" Ends"+ e);
        }
        LOGGER.debug("getProgramList : "+logUserId+" Ends");
        return programList;
    }

    /**
     * Setter of programList
     * @param programList
     */
    public void setProgramList(List<SelectItem> programList) {
        this.programList = programList;
    }
    private List<SelectItem> developmentPartner = new ArrayList<SelectItem>();

    /**
     * Handle Ajax request for loading Development Partner
     * @return
     */
    public List<SelectItem> getDevelopmentPartner() {
        projectService.setUserId(logUserId);
        LOGGER.debug("getDevelopmentPartner : "+logUserId+" Starts");
        try {
            List<CommonAppData> developmentPartner1 = projectService.getDetailsBySP("developmentpartner", " ", " ");
            for (CommonAppData commonAppData : developmentPartner1) {
                developmentPartner.add(new SelectItem(commonAppData.getFieldName1(), commonAppData.getFieldName2()));
            }
        } catch (Exception e) {
            LOGGER.error("getDevelopmentPartner : "+logUserId+" Ends"+ e);
        }
        LOGGER.debug("getDevelopmentPartner : "+logUserId+" Ends");
        return developmentPartner;
    }

    /**
     * Setter of DevelopmentPartner
     * @param developmentPartner
     */
    public void setDevelopmentPartner(List<SelectItem> developmentPartner) {
        this.developmentPartner = developmentPartner;
    }
    /**
     * String projectCode
     */
    public String projectCode = "";

    /**
     * Give projectCode using projectId
     * @param projectId
     * @return
     */
    public String getProjectCode(int projectId) {
        projectService.setUserId(logUserId);
        LOGGER.debug("getProjectCode : "+logUserId+" Starts");
        try {
            CommonAppData projectList = projectService.getDetailsBySP("projectcode", "" + projectId, " ").get(0);
            projectCode = projectList.getFieldName3();
        } catch (Exception e) {
            LOGGER.error("getProjectCode : "+logUserId+" :"+ e);
        }
        LOGGER.debug("getProjectCode : "+logUserId+" Ends");
        return projectCode;
    }

    /**
     * Get Data of project Office(PE)
     * @param userId
     * @return
     */
    public CommonAppData getProjectOffice(int userId) {
        LOGGER.debug("getProjectOffice : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        CommonAppData officeList = null;
        try {
            officeList = projectService.getDetailsBySP("GetPEAdminOffice", userId+"", "").get(0);

        } catch (Exception ex) {
            LOGGER.debug("getProjectOffice : "+logUserId+" :"+ ex);
        }
        LOGGER.debug("getProjectOffice : "+logUserId+" Ends:");
        return officeList;
    }
    /**
     * Get AU,PE,PD for office
     * @param role
     * @param userId
     * @return List<CommonAppData>
     */
    public List<CommonAppData> getAuUserForOffice(int role, int userId) {
        LOGGER.debug("getAuUserForOffice : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        List<CommonAppData> auUserList = null;
        try {
            auUserList = projectService.getDetailsBySP("GetPEUser", role+"",userId+"");

        } catch (Exception ex) {
            LOGGER.error("getAuUserForOffice : "+logUserId+" Ends"+ ex);
        }
        LOGGER.debug("getAuUserForOffice : "+logUserId+" Ends");
        return auUserList;
    }

    /**
     * getPdOrPeForOffice
     * @param role
     * @param userId
     * @return CommonAppData
     */
    public CommonAppData getPdOrPeForOffice(int role, int userId) {
        LOGGER.debug("getPdOrPeForOffice : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        CommonAppData peOrPdUser = null;

        try {
                peOrPdUser = projectService.getDetailsBySP("GetPEUser", role+"", userId+"").get(0);
        } catch (Exception ex) {
            LOGGER.error("getPdOrPeForOffice : "+logUserId+" :"+ ex);
        }
        LOGGER.debug("getPdOrPeForOffice : "+logUserId+" Ends");
        return peOrPdUser;
    }

    /**
     * setter of ProjectCode
     * @param projectCode
     */
    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }
    /**
     * List<SPProjectDetailReturn>;
     */
    public List<SPProjectDetailReturn> pProjectDetailReturns = new ArrayList<SPProjectDetailReturn>();

    /**
     * get Project Detail
     * @param projectId
     * @param userId
     * @return List Of SPProjectDetailReturn
     */
    public List<SPProjectDetailReturn> getpProjectDetailReturns(int projectId,int userId) {
        LOGGER.debug("getpProjectDetailReturns : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        try {
            pProjectDetailReturns = projectService.getProjectMasterBySP("", "", 0, null, null, null, "", "", "getProject", projectId,userId);
        } catch (Exception e) {
            LOGGER.error("getpProjectDetailReturns : "+logUserId+" :" + e);
        }
        LOGGER.debug("getpProjectDetailReturns : "+logUserId+" Ends");
        return pProjectDetailReturns;
    }

    /**
     * Setter of List<SPProjectDetailReturn>
     * @param pProjectDetailReturns
     */
    public void setpProjectDetailReturns(List<SPProjectDetailReturn> pProjectDetailReturns) {
        this.pProjectDetailReturns = pProjectDetailReturns;
    }
    /**
     * 
     */
    public List<SPProjectDetailReturn> developmentPar = new ArrayList<SPProjectDetailReturn>();

    /**
     * Get Details of getDevelopmentPartner
     * @param projectId
     * @param userId
     * @return List of SPProjectDetailReturn
     */
    public List<SPProjectDetailReturn> getDevelopmentPar(int projectId,int userId) {
        LOGGER.debug("getDevelopmentPar : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        try {
            developmentPar = projectService.getProjectMasterBySP("", "", 0, null, null, null, "", "", "getPartner", projectId,userId);
        } catch (Exception e) {
            LOGGER.error("getDevelopmentPar : "+logUserId+" :" + e);
        }
        LOGGER.debug("getDevelopmentPar : "+logUserId+" Ends");
        return developmentPar;
    }

    /**
     * Setter of Development Partner
     * @param developmentPar
     */
    public void setDevelopmentPar(List<SPProjectDetailReturn> developmentPar) {
        this.developmentPar = developmentPar;
    }
    /**
     * List of SPProjectRolesReturn
     */
    public List<SPProjectRolesReturn> rolesReturns = new ArrayList<SPProjectRolesReturn>();

    /**
     * Get Roles using projectId
     * @param projectId
     * @return
     */
    public List<SPProjectRolesReturn> getRolesReturns(int projectId) {
        LOGGER.debug("getRolesReturns : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        try {
            rolesReturns = projectService.getProjectRolesBySP("", "", null, null, null, null, "", "", "editPartner", projectId,0);
        } catch (Exception e) {
             LOGGER.error("getRolesReturns : "+logUserId+" Ends"+ e);
        }
        LOGGER.debug("getRolesReturns : "+logUserId+" Ends");
        return rolesReturns;
    }

    /**
     * Setter of SPProjectRolesReturn
     * @param rolesReturns
     */
    public void setRolesReturns(List<SPProjectRolesReturn> rolesReturns) {
        this.rolesReturns = rolesReturns;
    }
    /**
     * List SPProjectRolesReturn
     */
    public List<SPProjectRolesReturn> userRolesReturns = new ArrayList<SPProjectRolesReturn>();

    /**
     * get user roles sung projectId and UserId
     * @param projectId
     * @param userId
     * @return
     * @throws Exception
     */
    public List<SPProjectRolesReturn> getUserRolesReturns(int projectId, int userId) throws Exception {
        LOGGER.debug("getUserRolesReturns : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        try {
            userRolesReturns = projectService.getProjectRolesBySP("", "", userId, null, null, null, "", "", "editPartner", projectId,0);
        } catch (Exception e) {
            LOGGER.error("getUserRolesReturns : "+logUserId+" : "+ e);
        }
        LOGGER.debug("getUserRolesReturns : "+logUserId+" Ends");
        return userRolesReturns;
    }

    /**
     * Setter of SPProjectRolesReturn
     * @param userRolesReturns
     */
    public void setUserRolesReturns(List<SPProjectRolesReturn> userRolesReturns) {
        this.userRolesReturns = userRolesReturns;
    }
    /**
     * 
     */
    public List<TblProcurementMethod> procurementMethods = new ArrayList<TblProcurementMethod>();

    /**
     * Get All the data of TblProcurementMethod
     * @return List<TblProcurementMethod> 
     */
    public List<TblProcurementMethod> getProcurementMethods() {
        LOGGER.debug("getProcurementMethods : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        try {
        procurementMethods = projectService.getListProcurementMethod();
        } catch (Exception e) {
            LOGGER.error("getProcurementMethods : "+logUserId+" : "+ e);
        }
        LOGGER.debug("getProcurementMethods : "+logUserId+" Ends");
        return procurementMethods;
    }

    /**
     * setter of procurementMethods
     * @param procurementMethods
     */
    public void setProcurementMethods(List<TblProcurementMethod> procurementMethods) {
        this.procurementMethods = procurementMethods;
    }
    /**
     * List pProjectFPReturns
     */
    public List<SPProjectFPReturn> pProjectFPReturns = new ArrayList<SPProjectFPReturn>();

    /**
     * get Project Financial Power using userId and ProjectId
     * @param userId
     * @param projectId
     * @return List<SPProjectFPReturn> 
     */
    public List<SPProjectFPReturn> getpProjectFPReturns(int userId, int projectId){
        LOGGER.debug("getpProjectFPReturns : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        try {
        pProjectFPReturns = projectService.getProjectFPBySP("", "", userId, null, null, null, "", "", "getfinpower", projectId);
        } catch (Exception e) {
            LOGGER.error("getpProjectFPReturns : "+logUserId+" : "+ e);
        }
        LOGGER.debug("getpProjectFPReturns : "+logUserId+" Ends");
        return pProjectFPReturns;
    }

    /**
     * setter of pProjectFPReturns
     * @param pProjectFPReturns
     */
    public void setpProjectFPReturns(List<SPProjectFPReturn> pProjectFPReturns) {
        this.pProjectFPReturns = pProjectFPReturns;
    }

    /**
     * Verify Project Code exist or not.
     * @param projectCode
     * @param projectId
     * @return
     */
    private String verifyProjectCode(String projectCode, int projectId) {
        LOGGER.debug("verifyProjectCode : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        String str = "";
        try {
            List<CommonAppData> list = projectService.getDetailsBySP("checkprojectcode", "" + projectCode, "" + projectId);
            if (list.isEmpty()) {
                str = "<br />"+"OK";
            } else {
                str = "<br />"+"Project Code already exists";
            }
            
        } catch (Exception e) {
            LOGGER.error("verifyProjectCode : "+logUserId+" : "+ e);
        }
        LOGGER.debug("verifyProjectCode : "+logUserId+" Ends");
        return str;
    }

    /**
     * Verify Project Name exist or not.
     * @param projectName
     * @param projectId
     * @return
     */
    private String verifyProjectName(String projectName, int projectId) {
        LOGGER.debug("verifyProjectName : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        String str = "";
        try {
            List<CommonAppData> list = projectService.getDetailsBySP("checkprojectname", "" + projectName, "" + projectId);
            if (list.isEmpty()) {
                str = "<br />"+"OK";
            } else {
                str = "<br />"+"Project Name already exists";
            }
            
        } catch (Exception e) {
            LOGGER.error("verifyProjectName : "+logUserId+" : "+ e);
        }
        LOGGER.debug("verifyProjectName : "+logUserId+" Ends");
        return str;
    }

    /**
     * Get Department by userId
     * @param field1
     * @param userid
     * @param field3
     * @return List of commonData
     */
    public List<CommonAppData> getDepartmentsByUserid(String field1, String userid, String field3) {
        LOGGER.debug("getDepartmentsByUserid : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        List<CommonAppData> departmentList = null;
        try {
        departmentList = projectService.getDepartmentsByUserid(field1, userid, field3);
        } catch (Exception e) {
            LOGGER.error("getDepartmentsByUserid : "+logUserId+" : "+ e);
        }
        LOGGER.debug("getDepartmentsByUserid : "+logUserId+" Ends");
        return departmentList;
    }

    /**
     * Boolean if data is available of PD using projectId
     * @param projectId
     * @param prole
     * @return boolean
     */
    public boolean isDataAvailableForPDForProjectId(int projectId, String prole) {
        LOGGER.debug("isDataAvailableForPDForProjectId : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        boolean flag = false;
        try {
            flag = projectService.isDataAvailableForPDForProjectId(projectId, prole);
        } catch (Exception e) {
             LOGGER.error("isDataAvailableForPDForProjectId : "+logUserId+" : "+ e);
    }
        LOGGER.debug("isDataAvailableForPDForProjectId : "+logUserId+" Ends");
        return flag;
    }

    /**
     * Is Data available For Project for User using projectId, userId, procureId
     * @param projectId
     * @param userId
     * @param procureId
     * @return boolean
     */
    public boolean isDataAvailableForProjForUser(int projectId, int userId, int procureId) {
        LOGGER.debug("isDataAvailableForProjForUser : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        boolean flag = false;
        try {
            LOGGER.debug("  in isDataAvailableForProjForUser ======== ====");
            flag = projectService.isDataAvailableForProjForUser(projectId, userId, procureId);

        } catch (Exception e) {
            LOGGER.error("isDataAvailableForProjForUser : "+logUserId+" : "+ e);
    }
        LOGGER.debug("isDataAvailableForProjForUser : "+logUserId+" Ends");
        return flag;
    }

    /**
     * Handle Ajax when office is select then give data
     * @param deptId
     * @return
     */
    public List<CommonAppData> getSelectOfficeData(int deptId) {
        LOGGER.debug("getSelectOfficeData : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        List<CommonAppData> list = null;
        List<CommonAppData> soffice = null;
        try {
            list = projectService.getDetailsBySP("Office", "" + deptId, "0");
        } catch (Exception e) {
            LOGGER.error("getSelectOfficeData : "+logUserId+" : "+ e);
        }
        LOGGER.debug("getSelectOfficeData : "+logUserId+" Ends");
        return list;
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /**
     * Check if Project Role is exist or not
     * @param porjectId
     * @return tblProjectRoles
     */
    public List<TblProjectRoles> checkIsProjectRoleExist(int porjectId) {
        LOGGER.debug("checkIsProjectRoleExist : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        List<TblProjectRoles> projectRoles = null;
        try {
        projectRoles = projectService.checkIsProjectRoleExist(porjectId);
        } catch (Exception e) {
            LOGGER.error("checkIsProjectRoleExist : "+logUserId+" : "+ e);
        }
        LOGGER.debug("checkIsProjectRoleExist : "+logUserId+" Ends");
        return projectRoles;
    }

    /**
     * give count of prject if exist
     * @param projectId
     * @return
     */
    public long checkIsProjectExist(int projectId) {
        LOGGER.debug("checkIsProjectExist : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        long val = 0;
        try {
        val = projectService.checkIsProjectExist(projectId);
        } catch (Exception e) {
            LOGGER.error("checkIsProjectExist : "+logUserId+" : "+ e);
        }
        LOGGER.debug("checkIsProjectExist : "+logUserId+" Ends");
        return val;
    }
    
    /**
     * Project Office of Organization Admin
     * @param userId
     * @return List of common App Data
     */
    public List<CommonAppData> getProjectOfficeOrg(int userId) {
        LOGGER.debug("getProjectOffice : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        List<CommonAppData> officeList = new ArrayList<CommonAppData>();
        try {
            officeList = projectService.getDetailsBySP("GetOrgAdminOffice", userId+"", "");

        } catch (Exception ex) {
            LOGGER.debug("getProjectOffice : "+logUserId+" :"+ ex);
        }
        LOGGER.debug("getProjectOffice : "+logUserId+" Ends:");
        return officeList;
    }
    
    /**
     * Get Employee Role
     * @param officeId
     * @param role
     * @return String
     */
    public String getEmployeeRole(String officeId,String role) {
        LOGGER.debug("getProjectOffice : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        if("PD".equalsIgnoreCase(role)){
            role = "%";
        }
        List<CommonAppData> officeList = new ArrayList<CommonAppData>();
        StringBuffer sb = new StringBuffer();
        try {
            officeList = projectService.getDetailsBySP("getEmployeeByRoleAndOffice",officeId, role);
            if(officeList!=null && !officeList.isEmpty()){
                sb.append("<select class='formTxtBox_1' name='hidUser' id='hidUser' name='txtUser' style='width: 200px' onchange='cheksameUser();'>");
                sb.append("<option value='' >-- Select Employee --</option>");
                for (CommonAppData commonAppData : officeList) {
                    sb.append("<option value='"+commonAppData.getFieldName1()+"' >"+commonAppData.getFieldName2()+"</option>");
                }
                sb.append("</select>");
            }else{
                sb.append("<label>No user(s) found in this Office</label><input type='hidden' id='hidUser' name='txtUser' value=''/>");
            }

        } catch (Exception ex) {
            LOGGER.debug("getProjectOffice : "+logUserId+" :"+ ex);
        }
        LOGGER.debug("getProjectOffice : "+logUserId+" Ends:");
        return sb.toString();
    }
}
