/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

import java.util.Date;

/**
 *
 * @author Chalapathi.Bavisetti
 */
public class ViewMessageDtBean {
    private Date viewMsgDate;
    private String from;
    private String to;
    private String cc;
    private String subject;
    private String priority;
    private String textArea;
    private String toCcReply;

    /**
     * @return the viewMsgDate
     */
    public Date getViewMsgDate() {
        return viewMsgDate;
    }

    /**
     * @param viewMsgDate the viewMsgDate to set
     */
    public void setViewMsgDate(Date viewMsgDate) {
        this.viewMsgDate = viewMsgDate;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the priority
     */
    public String getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(String priority) {
        this.priority = priority;
    }

    /**
     * @return the textArea
     */
    public String getTextArea() {
        return textArea;
    }

    /**
     * @param textArea the textArea to set
     */
    public void setTextArea(String textArea) {
        this.textArea = textArea;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public String getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * @return the cc
     */
    public String getCc() {
        return cc;
    }

    /**
     * @param cc the cc to set
     */
    public void setCc(String cc) {
        this.cc = cc;
    }

    /**
     * @return the toCcReply
     */
    public String getToCcReply() {
        return toCcReply;
    }

    /**
     * @param toCcReply the toCcReply to set
     */
    public void setToCcReply(String toCcReply) {
        this.toCcReply = toCcReply;
    }
}
