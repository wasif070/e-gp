/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommitteDtBean;
import com.cptu.egp.eps.dao.storedprocedure.CommitteMemDtBean;
import com.cptu.egp.eps.dao.storedprocedure.OfficeMemberDtBean;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblTenderHash;
import com.cptu.egp.eps.service.serviceimpl.CommitteMemberService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.databean.TenderCommitteDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SHA1HashEncryption;
import com.cptu.egp.eps.web.utility.SelectItem;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import com.cptu.eps.service.audit.AuditTrail;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Create,Update and Publish of Opening, Evaluation and Technical Sub Committee
 * @author taher
 */
public class TenderCommitteSrBean {

    private String logUserId = "0";
    private AuditTrail auditTrail;
    final Logger logger = Logger.getLogger(TenderCommitteSrBean.class);
    CommitteMemberService committeMemberService = (CommitteMemberService) AppContext.getSpringBean("CommitteMemberService");

    public void setLogUserId(String logUserId) {
        committeMemberService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        committeMemberService.setAuditTrail(auditTrail);
        this.auditTrail = auditTrail;
    }

    /**
     * Committe Creation Details
     * @param tenderid tenderid from request
     * @param funName evalcom,opencom
     * @return CommitteDtBean in DAO Bean
     */
    public CommitteDtBean findCommitteDetails(int tenderid, String funNmae) {
        CommitteDtBean findCommitteDetails = null;
        logger.debug("findCommitteDetails : "+logUserId+"starts");
        try{
            findCommitteDetails = committeMemberService.findCommitteDetail(tenderid, funNmae);
    }
        catch(Exception e)
        {
            logger.error("findCommitteDetails : " + e);
        }
        logger.debug("findCommitteDetails : "+logUserId+"ends");
        return findCommitteDetails;
        
    }

     /**
     *Search on tenderid and funName returns Member details
     * @param object search parameter
     * @param funName samepe,otheragency
     * @return List of OfficeMemberDtBean
     */
    public List<OfficeMemberDtBean> findOfficeMember(int tenderid, String funNmae) {
        List<OfficeMemberDtBean> findOfficeMember = null;
        logger.debug("findOfficeMember : "+logUserId+"starts");
        try{
            findOfficeMember = committeMemberService.findOfficeMember(String.valueOf(tenderid), funNmae);
    }
        catch(Exception e)
        {
            logger.error("findOfficeMember : " + e);
        }
        logger.debug("findOfficeMember : "+logUserId+"ends");
        return findOfficeMember;
        
    }

    /**
     *Returns List of deptId and DeptName for combo box
     * @param values variable arguments
     * @return List of SelectItem
     * @throws Exception
     */
    public List<SelectItem> getDeptList(Object... values) throws Exception {
        List<SelectItem> selectItems = new ArrayList<SelectItem>();

        logger.debug("getDeptList : " + logUserId + "starts");
        try{
        List<TblDepartmentMaster> list = committeMemberService.organizationList(values);

        for (TblDepartmentMaster tblDepartmentMaster : list) {
            selectItems.add(new SelectItem(tblDepartmentMaster.getDepartmentId(), tblDepartmentMaster.getDepartmentName()));
        }
        }
        catch(Exception e)
        {
            logger.error("getDeptList : " + e);
        }
        
        logger.debug("getDeptList : " + logUserId + "ends");
        return selectItems;
    }

    /**
     *This method creates or update evaluation,opening or tsc committee
     * @param tcdb Object of TenderCommitteDtBean containing information for committee
     * @param isEdit based on this insert or update is executed
     */
    public void createUpdateCommitte(TenderCommitteDtBean tcdb, boolean isEdit) {
        logger.debug("TenderCommitteDtBean : " + logUserId + "starts");
        try{
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String comString = "";
        if(!("TOC".equalsIgnoreCase(tcdb.getCommitteeType()) || "POC".equalsIgnoreCase(tcdb.getCommitteeType()) || "TEC".equalsIgnoreCase(tcdb.getCommitteeType()) || "TC".equalsIgnoreCase(tcdb.getCommitteeType())  )){
            comString = "<root><tbl_Committee tenderId=\"" + tcdb.getTenderid() + "\" committeeName=\"" + tcdb.getCommitteeName() + "\" createdBy=\"" + tcdb.getUserid() + "\" workflowStatus=\"pending\" committeStatus=\"pending\" minMembers=\"" + tcdb.getMinMembers() + "\" maxMembers=\"" + tcdb.getMaxMembers() + "\" minMemOutSide=\"" + tcdb.getMinMemOutSide() + "\" committeeType=\"" + tcdb.getCommitteeType() + "\" isCurrent=\"yes\" remarks=\"\" createdDate=\"" + format.format(new Date()) + "\" publishDate=\"\" minMemFromPe=\"" + tcdb.getMinMemFromPe() + "\" minMemFromTec=\"" + tcdb.getMinMemFrmTecPec() + "\"/></root>";
        }else{
            comString = "<root><tbl_Committee tenderId=\"" + tcdb.getTenderid() + "\" committeeName=\"" + tcdb.getCommitteeName() + "\" createdBy=\"" + tcdb.getUserid() + "\" workflowStatus=\"pending\" committeStatus=\"pending\" minMembers=\"" + tcdb.getMinMembers() + "\" maxMembers=\"0\" minMemOutSide=\"" + tcdb.getMinMemOutSide() + "\" committeeType=\"" + tcdb.getCommitteeType() + "\" isCurrent=\"yes\" remarks=\"\" createdDate=\"" + format.format(new Date()) + "\" publishDate=\"\" minMemFromPe=\"" + tcdb.getMinMemFromPe() + "\" minMemFromTec=\"" + tcdb.getMinMemFrmTecPec() + "\"/></root>";
        }
        StringBuilder comMemString = new StringBuilder();
        comMemString.append("<root>");
        for (int i = 0; i < tcdb.getMemUserIds().length; i++) {
            comMemString.append("<tbl_CommitteeMembers  committeeId=\"IdentKey\" userId=\"" + tcdb.getMemUserIds()[i] + "\" createdDate=\"" + format.format(new Date()) + "\" appStatus=\"pending\" appDate=\"\" remarks=\"\" eSignature=\"\" digitalSignature=\"\" memberRole=\"" + tcdb.getMemberRoles()[i] + "\" memberFrom=\"" + tcdb.getMemberFroms()[i] + "\" govUserId=\""+tcdb.getMemGovIds()[i]+"\"/>");
        }
        comMemString.append("</root>");
        if (isEdit) {
            committeMemberService.updateCommitteMem(tcdb.getCommitteeId(), comString, comMemString.toString(),tcdb.getTenderid(),tcdb.getCommitteeType());
        } else {
            committeMemberService.createCommitteMem(comString, comMemString.toString(),tcdb.getTenderid(),tcdb.getCommitteeType());
        }
    }
        catch(Exception e)
        {
            System.out.println(e);
            logger.error("createUpdateCommitte : " + e);
        }
        
        logger.debug("createUpdateCommitte : " + logUserId + "ends");
    }

    /**
     *Search on tenderid and funName returns Member details
     * @param object search parameter
     * * @param funName based on which respective function is called from tendercommondata
     * @return List of CommitteMemDtBean
     */
    public List<CommitteMemDtBean> committeUser(int tenderid, String funNmae) {
        List<CommitteMemDtBean> committeUser = null;
        logger.debug("committeUser : "+logUserId+"starts");
        try{
            committeUser = committeMemberService.findCommitteMember(String.valueOf(tenderid), funNmae);
            committeUser.size();
    }
        catch(Exception e)
        {
            logger.error("committeUser : " + e);
        }
        logger.debug("committeUser : "+logUserId+"ends");
        return committeUser;
    }

    /**
     *Publishes the committee and send mail to committee members
     * @param commId from tbl_Committee
     * @param remarks added by Committee Chairperson
     * @param tenderId for which committee is created
     * @param comType type of committee(TEC(PEC),TOC(PEC))
     * @return true or false for success or fail
     */
    public boolean publishComm(String commId, String remarks,String tenderId,String comType) {
        boolean publishComm = false;
        logger.debug("publishComm : "+logUserId+"starts");
        try {
            List<Object> list = committeMemberService.publishComm(commId, remarks,tenderId,comType);
            if(list!=null){
                UserRegisterService  registerService = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                MailContentUtility mailContentUtility = new MailContentUtility();
                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                String mailText = null;
                String msgText = null;
                if(!list.isEmpty()){
                    mailText = mailContentUtility.committeePublishContent(tenderId, list.get(0).toString(), list.get(1).toString(), comType);
                    msgText = msgBoxContentUtility.committeePublishContent(tenderId, list.get(0).toString(), list.get(1).toString(), comType);
                }
                mailContentUtility=null;
                msgBoxContentUtility=null;
                boolean smsflag=false;
                for (int i = 2; i < list.size(); i++) {
                    if(list.get(i).toString().equals("junk_data")){
                        smsflag=true;
                    }
                    if(smsflag){
                        sendMessageUtil.setSmsNo(list.get(i).toString());
                        if(comType.equals("eval")){
                             //sendMessageUtil.setSmsBody("Dear User,%0AYou have been included in the Evaluation committee for the following tender.%0ATender ID: "+tenderId+"%0APE: "+list.get(0).toString()+"%0ALogon to the e-GP system or contact PE for more details.%0Ae-GP System");
                             sendMessageUtil.setSmsBody("Dear User,%0AYou have been included in the Evaluation committee for the following tender.%0ATender ID: "+tenderId+"%0APE: "+list.get(0).toString()+"%0ALogon to the e-GP system or contact PA for more details.%0Ae-GP System");
                         }else if(comType.equals("open")){
                             //sendMessageUtil.setSmsBody("Dear User,%0AYou have been included in the Opening committee for the following tender.%0ATender ID: "+tenderId+"%0APE: "+list.get(0).toString()+"%0ALogon to the e-GP system or contact PE for more details.%0Ae-GP System");
                             sendMessageUtil.setSmsBody("Dear User,%0AYou have been included in the Opening committee of%0ATender ID: "+tenderId+"%0APE: "+list.get(0).toString()+"%0Ae-GP System");
                         }else if(comType.equals("tsc")){
                            //sendMessageUtil.setSmsBody("Dear User,%0AYou have been included in the TSC committee for the following tender.%0ATender ID: "+tenderId+"%0APE: "+list.get(0).toString()+"%0ALogon to the e-GP system or contact PE for more details.%0Ae-GP System");
                            sendMessageUtil.setSmsBody("Dear User,%0AYou have been included in the TSC committee for the following tender.%0ATender ID: "+tenderId+"%0APE: "+list.get(0).toString()+"%0ALogon to the e-GP system or contact PA for more details.%0Ae-GP System");
                         }else if(comType.equals("TenderCommittee")){
                            sendMessageUtil.setSmsBody("Dear User,%0AYou have been included in the Tender committee of%0ATender ID: "+tenderId+"%0APE: "+list.get(0).toString()+"%0Ae-GP System");
                         } 
                        sendMessageUtil.sendSMS();
                    }else{
                        String[] mail = {list.get(i).toString()};
                        sendMessageUtil.setEmailTo(mail);
                         if(comType.equals("eval")){
                             sendMessageUtil.setEmailSub("e-GP: Evaluation Committee Member");
                         }else if(comType.equals("open")){
                             sendMessageUtil.setEmailSub("e-GP: Opening Committee Member");
                         }else if(comType.equals("tsc")){
                            sendMessageUtil.setEmailSub("e-GP: Technical Sub Committee Member");
                         }
                         else if(comType.equals("TenderCommittee")){
                            sendMessageUtil.setEmailSub("e-GP: Tender Committee Member");
                         }
                        sendMessageUtil.setEmailMessage(mailText);
                        sendMessageUtil.sendEmail();
                        registerService.contentAdmMsgBox(mail[0], XMLReader.getMessage("emailIdNoReply"),sendMessageUtil.getEmailSub(), msgText);
                        mail=null;
                    }
                    
                }          
                sendMessageUtil=null;
                list=null;
                publishComm = true;
            }
        } catch (Exception e) {
            logger.error("publishComm : " + e);
        }
        logger.debug("publishComm : "+logUserId+"ends");
        return publishComm;
    }

    /**
     *Maps Opening Committee members for decryption during Tender Opening
     * @param userIds committe members userIds
     * @param tenderid from tbl_TenderMaster
     * @param userid pe userId
     */
    public void openComMemEncry(String userIds[], String tenderid, String userid) {
        logger.debug("openComMemEncry : " + logUserId + "starts");
        try {
        String encryString = SHA1HashEncryption.encodeStringSHA1(committeMemberService.openComMemEncry(userIds));
        String userArr = "";
        for (int i = 0; i < userIds.length; i++) {
            userArr += userIds[i] + ",";
        }
        userArr = userArr.substring(0, userArr.length() - 1);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String xml = "<root><tbl_TenderHash tenderId=\"" + tenderid + "\" tenderHashUserId=\"" + userArr + "\" tenderHash=\"" + encryString + "\" creationDate=\"" + format.format(new Date()) + "\" createdBy=\"" + userid + "\"/></root>";
        committeMemberService.addComEncryDetails(xml,tenderid);
        } catch (Exception e) {
            logger.error("openComMemEncry : " + e);
    }

        logger.debug("openComMemEncry : " + logUserId + "ends");
    }

    /**
     *Gives List of tbl_TenderHash based on criteria
     * @param tenderid from tbl_tenderMaster
     * @return List of tbl_TenderHash
     * @throws Exception
     */
    public String[] findTenderHash(String tenderid) throws Exception {
        logger.debug("findTenderHash : " + logUserId + "starts");
        TblTenderHash tblTenderHash = null;
        try {
            tblTenderHash = committeMemberService.findTenderHash("tenderId", Operation_enum.EQ, Integer.parseInt(tenderid)).get(0);
        } catch (Exception e) {
            logger.error("findTenderHash : " + e);
        }


        logger.debug("findTenderHash : " + logUserId + "ends");
        return tblTenderHash.getTenderHashUserId().split(",");
    }

    /**
     *Updates Opening Committee members for decryption during Tender Opening
     * @param userIds committe members userIds
     * @param tenderid from tbl_TenderMaster
     * @param userid pe userId
     */
    public void updateComMemEncry(String userIds[], String tenderid, String userid) {
        logger.debug("updateComMemEncry : " + logUserId + "starts");
        try {
        String encryString = SHA1HashEncryption.encodeStringSHA1(committeMemberService.openComMemEncry(userIds));
        String userArr = "";
        for (int i = 0; i < userIds.length; i++) {
            userArr += userIds[i] + ",";
        }
            userArr = userArr.substring(0, userArr.length() - 1);
            committeMemberService.updateComMemEncry(userArr, userid, encryString, tenderid);

        } catch (Exception e) {
            logger.error("updateComMemEncry : " + e);
    }



        logger.debug("updateComMemEncry : " + logUserId + "ends");
    }

    /**
     * Based on function name method returns CommitteDtBean
     * @param tenderid from tbl_tenderMaster
     * @param funName on base of funName different functions from tendercommondata is called
     * @return List of CommitteDtBean
     */
    public List<CommitteDtBean> findCommitteDetailsHistory(int tenderid, String funNmae) {
        List<CommitteDtBean> committeUserHistory = null;
        logger.debug("findCommitteDetailsHistory : "+logUserId+"starts");
        try {
            committeUserHistory = committeMemberService.findCommitteDetailHistory(tenderid, funNmae);
        } catch (Exception e) {
            logger.error("findCommitteDetailsHistory : " + e);
    }
        logger.debug("findCommitteDetailsHistory : "+logUserId+"ends");
        return committeUserHistory;



    }

     /**
     *Finds History of the previously configured committee for the tenderId
     * @param object ex : tenderId
     * @param funName on base of funName different functions from tendercommondata is called
     * @param commId committeeId from tbl_Committee
     * @return List of CommitteDtBean
     */
     public List<CommitteMemDtBean> committeUserHistory(int tenderid, String funNmae, String commId) {
        List<CommitteMemDtBean> committeUserHistory = null;
        logger.debug("committeUserHistory : "+logUserId+"starts");
        try {
            committeUserHistory = committeMemberService.findCommitteMemberHistory(String.valueOf(tenderid), funNmae, commId);
        } catch (Exception e) {
            logger.error("committeUserHistory : " + e);
        }
        logger.debug("committeUserHistory : "+logUserId+"ends");
        return committeUserHistory;

    }

    /**
      *Gives the officeId from which the tender is floated
      * @param tenderId from tbl_tenderMaster
      * @return officeId
      */
    public String getTenderOfficeId(String tenderId){
         return committeMemberService.getTenderOfficeId(tenderId);
     }
}
