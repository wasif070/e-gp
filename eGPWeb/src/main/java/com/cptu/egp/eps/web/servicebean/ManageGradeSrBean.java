/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblGradeMaster;
import com.cptu.egp.eps.service.serviceinterface.CreateGradeService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * <b>ManageGradeSrBean</b> <Description Goes Here> Nov 24, 2010 4:40:55 PM
 * @author Administrator
 */
public class ManageGradeSrBean {

    final Logger logger = Logger.getLogger(ManageGradeSrBean.class);

    CreateGradeService gradeService=(CreateGradeService) AppContext.getSpringBean("CreateGradeService");
    
    private int offset = 0;
    private int limit = 0;
    private String colName = "";
    private String op_ENUM = "";
    private String condition = "";
    private boolean _search = false;
    private String getSortOrder = "";
    private String sortCol = "";
    private String logUserId="0";

     public void setLogUserId(String logUserId) {
        gradeService.setUserId(logUserId);
        this.logUserId = logUserId;
     }
    public String getSortOrder() {
        return getSortOrder;
    }

    public void setSortOrder(String getSortOrder) {
        this.getSortOrder = getSortOrder;
    }

    public String getSortCol() {
        return sortCol;
    }

    public void setSortCol(String sortCol) {
        this.sortCol = sortCol;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public boolean isSearch() {
        return _search;
    }

    public void setSearch(boolean _search) {
        this._search = _search;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getOp_ENUM() {
        return op_ENUM;
    }

    public void setOp_ENUM(String op_ENUM) {
        this.op_ENUM = op_ENUM;
    }

      public long getAllCountOfGrades() {
        logger.debug("getAllCountOfGrades : "+logUserId+" Starts ");
        long lng =0;
          try {
              lng = gradeService.getCntGradeMaster();
          } catch (Exception e) {
            logger.error("getAllCountOfGrades : "+logUserId+" : "+e);
    }
          logger.debug("getAllCountOfGrades : "+logUserId+" Ends ");
        return lng;
    }

    List<TblGradeMaster> gradeList = new ArrayList<TblGradeMaster>();

    public List<TblGradeMaster> getGradeList()
    {
        logger.debug("getGradeList : "+logUserId+" Starts ");
        List<TblGradeMaster> list = null;
        try{
        list = new ArrayList<TblGradeMaster>();
     if (gradeList.isEmpty()) {
         if(getSortCol().equalsIgnoreCase("")){
             list = gradeService.getGradeMasterList(getOffset(), getLimit(), "gradeId",Operation_enum.ORDERBY,Operation_enum.DESC);
         }else{
            String orderClause = null;
            if(getSortCol().equalsIgnoreCase("class")){
                orderClause = "grade";
            }else if(getSortCol().equalsIgnoreCase("level")){
                orderClause = "gradeLevel";
            }
            if(getSortOrder.equalsIgnoreCase("asc")){
                list = gradeService.getGradeMasterList(getOffset(), getLimit(), orderClause,Operation_enum.ORDERBY,Operation_enum.ASC);
            }else if(getSortOrder.equalsIgnoreCase("desc")){
                list = gradeService.getGradeMasterList(getOffset(), getLimit(), orderClause,Operation_enum.ORDERBY,Operation_enum.DESC);
            }
         }
            //Object[] values={"gradeId",Operation_enum.ORDERBY,Operation_enum.DESC};
            //Object[] values={"grade",Operation_enum.ORDERBY,Operation_enum.ASC};
//            for (TblGradeMaster gradeMaster : gradeService.getGradeMasterList(getOffset(), getLimit(), values)) {
//                gradeList.add(gradeMaster);
//            }
        }
        }catch(Exception e)
        {
            logger.error("getGradeList : "+logUserId+" : "+e);
        }
         logger.debug("getGradeList : "+logUserId+" Ends ");
        return list;
    }

    public void setGradeList(List<TblGradeMaster> gradeList)
    {
        this.gradeList = gradeList;
    }

    public String getGetSortOrder()
    {
        return getSortOrder;
    }

    public void setGetSortOrder(String getSortOrder)
    {
        this.getSortOrder = getSortOrder;
    }

    public CreateGradeService getGradeService()
    {
        return gradeService;
    }

    public void setGradeService(CreateGradeService gradeService)
    {
        this.gradeService = gradeService;
    }

}
