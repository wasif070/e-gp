/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * This servlet is used in viewQueAns.jsp and redirect the page to SeekEvalClari page
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.service.serviceinterface.AddUpdateOpeningEvaluation;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class ViewQueAnsServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        boolean flag = true;
        int counter = 0;
       // String isComplied = "";
        String formId = "";
        String userId = "";
        String tenderId = "";
        String errMsg = "";
        String marks = "0";
        HttpSession session = request.getSession();
        boolean isError = true;
        String st = "";
        String techQua = "", methodType = "";
        String action = null;
        String evalNonCompRemarks = "";
        AddUpdateOpeningEvaluation addUpdate = (AddUpdateOpeningEvaluation) AppContext.getSpringBean("AddUpdateOpeningEvaluation");
        AuditTrail auditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer"));
        try {
            if (session.getAttribute("userId") == null) {
                response.sendRedirect("SessionTimedOut.jsp");
            } else {
              //  counter = Integer.parseInt(request.getParameter("counter"));
               // for (int i = 1; i <= counter; i++) {
                    st = request.getParameter("st");
                    evalNonCompRemarks = "";

                    if (request.getParameter("tenderId") == null) {
                        flag = false;
                        errMsg = "1,";
                    }
                    if (request.getParameter("evalMark") != null) {
                        marks = request.getParameter("evalMark");
                    }
                    if(flag){
                    tenderId = request.getParameter("tenderId");
                    userId = request.getParameter("userId");
                    formId = request.getParameter("formId");
                  //  isComplied = request.getParameter("Complied");
                    //marks = request.getParameter("compMark");
                   
                     
                                    methodType = request.getParameter("methodType");

                                    if (methodType.equalsIgnoreCase("OTM")) {
                                        if (request.getParameter("techQualify").equalsIgnoreCase("techQualify")) {
                                            techQua = "Accepted";
                                        }
                                        if (request.getParameter("techQualify").equalsIgnoreCase("techFail")) {
                                            techQua = "Rejected";
                                        }
                                    } else if (methodType.equalsIgnoreCase("LTM")) {
                                        if (request.getParameter("techQualify").equalsIgnoreCase("techQualify")) {
                                            techQua = "Accepted";
                                        }
                                        if (request.getParameter("techQualify").equalsIgnoreCase("techFail")) {
                                            techQua = "Rejected";
                                        }
                                    } else if (methodType.equalsIgnoreCase("2STM")) {
                                        if (request.getParameter("techQualify").equalsIgnoreCase("techQualify")) {
                                            techQua = "Responsive";
                                        }
                                        if (request.getParameter("techQualify").equalsIgnoreCase("techFail")) {
                                            techQua = "Unresponsive";
                                        }
                                    } else if (methodType.equalsIgnoreCase("REOI")) {
                                        if (request.getParameter("techQualify").equalsIgnoreCase("techQualify")) {
                                            techQua = "Excellent";
                                        }
                                        if (request.getParameter("techQualify").equalsIgnoreCase("techFail")) {
                                            techQua = "Very Good";
                                        }
                                        if (request.getParameter("techQualify").equalsIgnoreCase("techFail2")) {
                                            techQua = "Good & Poor";
                                        }
                                    } else if (methodType.equalsIgnoreCase("RFP")) {
                                        techQua = request.getParameter("marks");
                                    } else if (methodType.equalsIgnoreCase("PQ")) {
                                        if (request.getParameter("techQualify").equalsIgnoreCase("techQualify")) {
                                            techQua = "Qualified";
                                        }
                                        if (request.getParameter("techQualify").equalsIgnoreCase("techFail")) {
                                            techQua = "Conditionally Qualified";
                                        }
                                        if (request.getParameter("techQualify").equalsIgnoreCase("techFail2")) {
                                            techQua = "Disqualified";
                                        }
                                    } else {
                                        if (request.getParameter("techQualify").equalsIgnoreCase("techQualify")) {
                                            techQua = "Technically Qualified";
                                        }
                                        if (request.getParameter("techQualify").equalsIgnoreCase("techFail")) {
                                            techQua = "Technically Not Qualified";
                                        }
                                    }
                        action = "TEC Evaluate the form "+techQua;
                        if (request.getParameter("evalNonCompRemarks") == null || request.getParameter("evalNonCompRemarks").trim().length() == 0) {
                            flag = false;
                            errMsg = "5";
                        } else {
                            evalNonCompRemarks = request.getParameter("evalNonCompRemarks");
                        }

                   
                    CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                    CommonMsgChk commonMsgChk = null;
                    if(request.getParameter("type")==null){
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    String xmldata = "";
                    String evalStatus = "evaluation";
                   // xmldata = "<tbl_EvalMemStatus tenderId=\"" + tenderId + "\" userId=\"" + userId + "\" formId=\"" + formId + "\" isComplied=\"" + techQua + "\" evalStatus=\"" + evalStatus + "\" evalStatusDt=\"" + format.format(new Date()) + "\" evalBy=\"" + session.getAttribute("userId") + "\" evalNonCompRemarks=\"" + URLEncoder.encode(evalNonCompRemarks,"UTF-8") + "\" acceptComments=\"\" actualMarks=\"" +marks+ "\" />";
                  //  xmldata = "<root>" + xmldata + "</root>";
                    //System.out.println(xmldata);
                //    commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_EvalMemStatus", xmldata, "").get(0);
                    commonMsgChk = addUpdate.addUpdOpeningEvaluation("EvaluateForm",tenderId,userId,formId,techQua,evalStatus, format.format(new Date()), session.getAttribute("userId").toString(),URLEncoder.encode(evalNonCompRemarks,"UTF-8"),marks,"","","","","","","","","","").get(0);
                    //System.out.println("commonMsgChk::" + commonMsgChk.getMsg());
                    if (!commonMsgChk.getFlag()) {
                        flag = false;
                        errMsg = "0";
                    }
                    }else{
                        action = "TEC Update Evaluate the form "+techQua;
                        String evalStatus = request.getParameter("evalStatus");
                        String updateData = "isComplied='"+techQua+"', evalStatusDt=getDate(), evalNonCompRemarks='"+URLEncoder.encode(evalNonCompRemarks,"UTF-8")+"', actualMarks='"+marks+"'";
                        String whereCondition = "evalMemStatusId="+evalStatus;
                        commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_EvalMemStatus", updateData, whereCondition).get(0);
                        //System.out.println(commonMsgChk.getMsg());
                         if (!commonMsgChk.getFlag()) {
                        flag = false;
                        errMsg = "0";
                    }
                    }
                    if (commonXMLSPService != null || commonMsgChk != null) {
                        commonMsgChk = null;
                        commonXMLSPService = null;
                    }

                }
              //  }
            }
            if (!flag) {
                response.sendRedirect("officer/ViewQueAns.jsp?uid=" + userId +"&formId="+formId+ "&tenderId=" + tenderId + "&err=" + errMsg+"&st="+st);
            } else {
                response.sendRedirect("officer/SeekEvalClari.jsp?tenderId=" + tenderId+"&uId="+userId+"&st="+st+"&lnk=et");
            }
        } catch (Exception ex) {
            Logger.getLogger(ViewQueAnsServlet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(tenderId), "TenderId", EgpModule.Evaluation.getName(), action, evalNonCompRemarks);
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);




    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);




    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";


    }// </editor-fold>
}
