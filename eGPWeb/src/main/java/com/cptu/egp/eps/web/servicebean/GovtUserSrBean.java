/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.CommonAppData;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SPGovtEmpRolesReturn;
import com.cptu.egp.eps.model.table.TblBudgetType;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblDesignationMaster;
import com.cptu.egp.eps.model.table.TblEmployeeFinancialPower;
import com.cptu.egp.eps.model.table.TblEmployeeMaster;
import com.cptu.egp.eps.model.table.TblEmployeeOffices;
import com.cptu.egp.eps.model.table.TblEmployeeRoles;
import com.cptu.egp.eps.model.table.TblEmployeeTrasfer;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import com.cptu.egp.eps.model.table.TblProcurementRole;
import com.cptu.egp.eps.model.table.TblUserTypeMaster;
import com.cptu.egp.eps.model.view.VwEmpfinancePower;
import com.cptu.egp.eps.service.serviceimpl.CreateProjectServiceImpl;
import com.cptu.egp.eps.service.serviceimpl.TransferEmployeeServiceImpl;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.GovtUserCreationService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.BanglaNameUtils;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.SHA1HashEncryption;
import com.cptu.egp.eps.web.utility.SelectItem;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author parag
 */
public class GovtUserSrBean extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private String logUserId = "0";

    private AuditTrail auditTrail;

    GovtUserCreationService govtUserCreationService = (GovtUserCreationService) AppContext.getSpringBean("GovtUserCreationService");

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        govtUserCreationService.setAuditTrail(auditTrail);
    }

    /**
     * For logging purpose
     *
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     * Emp Id
     */
    public int empId = 0;
    /**
     * userId
     */
    public int userId = 0;
    /**
     * departmentId
     */
    public short departmentId = 0;

    /**
     * getter empId
     *
     * @return
     */
    public int getEmpId() {
        return empId;
    }

    /**
     * setter empId
     *
     * @param empId
     */
    public void setEmpId(int empId) {
        this.empId = empId;
    }

    /**
     * getter UserId
     *
     * @return
     */
    public int getUserId() {
        return userId;
    }

    /**
     * setter UserId
     *
     * @param userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    static final Logger LOGGER = Logger.getLogger(GovtUserSrBean.class);

    /**
     * getter DepartmentId
     *
     * @return
     */
    public short getDepartmentId() {
        return departmentId;
    }

    /**
     * setter DepartmentId
     *
     * @param departmentId
     */
    public void setDepartmentId(short departmentId) {
        this.departmentId = departmentId;
    }

    /**
     * Government user creation,edit,assign roles and assign finance power.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.debug("processRequest " + logUserId + " ");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String action = request.getParameter("action");
        LOGGER.debug("Action:: " + action);
        String funName = request.getParameter("funName");
        String objectId = request.getParameter("objectId");
        String employee = request.getParameter("employeeId");
        String designation = request.getParameter("designationId");
        String funNameforSecond = request.getParameter("funNameforSecond");
        String funNameforHope = request.getParameter("funNameforHope");
        String funNameforPE = request.getParameter("funNameforPE");

        String nationalId = request.getParameter("nationalId");
        String mobileNo = request.getParameter("mobileNo");

        String str = "";

        HttpSession session = request.getSession();
        int sessionUserId = 0;
        if (session.getAttribute("userId") != null) {
            sessionUserId = Integer.parseInt(session.getAttribute("userId").toString());
            logUserId = session.getAttribute("userId").toString();
        }
        govtUserCreationService.setLogUserId(logUserId);
        LOGGER.debug("processRequest " + logUserId + " Starts:");
        try {
            if (funNameforPE != null) {
                if ("checkPE".equalsIgnoreCase(funNameforPE)) {
                    LOGGER.debug("checkPe " + logUserId + " Starts:");
                    String officeId[] = null;
                    String strOffice = "";
                    if (request.getParameter("office") != null) {
                        officeId = request.getParameterValues("office");
                    }

                    for (String tempOffice : officeId) {
                        strOffice += tempOffice + ",";
                    }
                    strOffice = strOffice.substring(0, strOffice.length() - 1);

                    if (isDataAvailableForPEForOfficeId(strOffice)) {
                        out.print("true");
                    } else {
                        out.print("false");
                    }
                    //  LOGGER.debug(" in funname str funNameforPE ========== :" + str);
                    LOGGER.debug("checkPe " + logUserId + " Ends:");
                    out.flush();
                }
            }
        } catch (Exception e) {
            LOGGER.error("GovtUserSrBean " + logUserId + " :" + e);
        }
        try {
            try {
                // LOGGER.debug(" in  funName  ::: ");
                if (funName != null) {
                    if (("Office").equals(funName)) {
                        LOGGER.debug("Office " + logUserId + " Starts:");
                        int deptId = Integer.parseInt(objectId);
                        if (request.getParameter("userTypeId") != null) {
                            str = getOfficeForPe(sessionUserId);
                        } else {
                            str = getOffice(deptId);
                        }
                        LOGGER.debug("Office " + logUserId + " Ends:");
                        out.print(str);
                        out.flush();
                    }
                    if (("SOffice").equals(funName)) {
                        LOGGER.debug("SOffice " + logUserId + " Starts:");
                        int deptId = Integer.parseInt(objectId);
                        int employeeId = Integer.parseInt(employee);
                        if (request.getParameter("userTypeId") != null) {
                            str = getOfficeForPe(sessionUserId);
                        } else {
                            str = getSelectedOffice(deptId, employeeId);
                        }
                        LOGGER.debug("SOffice " + logUserId + " Ends:");
                        out.print(str);
                        out.flush();
                    }
                    if (funName.equals("verifyNationId")) {
                        LOGGER.debug("verifyNationId " + logUserId + " Starts:");
                        str = verifyNationalId(nationalId);
                        out.print(str);
                        LOGGER.debug("verifyNationId " + logUserId + " Ends:");
                        out.flush();
                    }
                    if (funName.equals("verifyMobile")) {
                        LOGGER.debug("verifyMobile " + logUserId + " Starts:");
                        //LOGGER.debug(" mobileNo  ========>>" + mobileNo);
                        str = verifyMobileNo(mobileNo);
                        //LOGGER.debug(" verifyMobile :: " + str);
                        out.print(str);
                        LOGGER.debug("verifyMobile " + logUserId + " Ends:");
                        out.flush();
                    }
                    if (funName.equals("getDistrictSubDistrict")) {
                        LOGGER.debug("verifyMobile " + logUserId + " Starts:");
                        //LOGGER.debug(" mobileNo  ========>>" + mobileNo);
                        str = getDistrictSubDistrict(objectId);
                        System.out.println(str);
                        //LOGGER.debug(" verifyMobile :: " + str);
                        out.print(str);
                        LOGGER.debug("verifyMobile " + logUserId + " Ends:");
                        out.flush();
                    }

                }
            } catch (Exception e) {
                LOGGER.error("" + funName + " " + logUserId + " :" + e);
            }
            try {
                if (funNameforSecond != null) {
                    if (("Designation").equals(funNameforSecond)) {
                        LOGGER.debug("Designation " + logUserId + " Starts:");
                        short deptId = Short.parseShort(objectId);
                        str = getDesignation(deptId);
                        out.print(str);
                        LOGGER.debug("Designation " + logUserId + " Ends:");
                        out.flush();
                    }

                    if (funNameforSecond.equals("SDesignation")) {
                        LOGGER.debug("SDesignation " + logUserId + " Starts:");
                        short deptId = Short.parseShort(objectId);
                        int desigId = Integer.parseInt(designation);
                        str = getSelectedDesignation(deptId, desigId);
                        out.print(str);
                        LOGGER.debug("SDesignation " + logUserId + " Ends:");
                        out.flush();
                    }
                }
            } catch (Exception e) {
                LOGGER.error("" + funNameforSecond + " " + logUserId + " :" + e);
            }
            try {
                //  LOGGER.debug(" in  funNameforHope  ::: ");
                if (funNameforHope != null) {
                    if ("checkHope".equals(funNameforHope)) {
                        LOGGER.debug("checkHope " + logUserId + " Starts:");
                        short deptId = Short.parseShort(objectId);
                        if (isDataAvailableForHopeForDeptId(deptId)) {
                            out.print("true");
                        } else {
                            out.print("false");
                        }
                        LOGGER.debug("checkHope " + logUserId + " Ends:");
                        out.flush();
                    }
                }
            } catch (Exception e) {
                LOGGER.error("checkHope " + logUserId + " :" + e);
            }

            if ("createGovtUser".equalsIgnoreCase(action)) {
                try {
                    LOGGER.debug("createGovtUser " + logUserId + " Starts:");
                    String emailId = "";
                    String citizenId = "";
                    String mobId = "";
                    String userEmployeeId = "";
                    String contactAddress = "";
                    int dId = 0;

                    if (request.getParameter("designation") != null) {
                        dId = Integer.parseInt(request.getParameter("designation").toString());
                    }

                    if (request.getParameter("mailId") != null) {
                        emailId = request.getParameter("mailId").trim();
                    }
                    if (request.getParameter("citizenId") != null) {
                        citizenId = request.getParameter("citizenId").trim();
                    } else {
                        citizenId = "";
                    }
                    if (request.getParameter("mobileNo") != null) {
                        mobId = request.getParameter("mobileNo").trim();
                    } else {
                        mobId = "";
                    }

                    if (request.getParameter("employeeId") != null) {
                        userEmployeeId = request.getParameter("employeeId").trim();
                    } else {
                        userEmployeeId = "";
                    }

                    if (request.getParameter("contactAddress") != null) {
                        contactAddress = request.getParameter("contactAddress").trim();
                    } else {
                        contactAddress = "";
                    }

                    TblLoginMaster loginMaster = new TblLoginMaster();
                    loginMaster.setEmailId(emailId);
                    loginMaster.setPassword(request.getParameter("password"));
                    //loginMaster.setPassword(HashUtil.getHash(request.getParameter("password"), "SHA-1"));
                    loginMaster.setRegistrationType("officer");
                    loginMaster.setIsJvca("no");
                    //loginMaster.setBusinessCountryName("bangladesh");
                    loginMaster.setBusinessCountryName("bhutan");
                    loginMaster.setNextScreen("GovtUserCreateRoleOff");
                    loginMaster.setIsEmailVerified("yes");
                    loginMaster.setFailedAttempt((byte) 0);
                    loginMaster.setValidUpTo(null);
                    loginMaster.setFirstLogin("yes");
                    loginMaster.setIsPasswordReset("no");
                    loginMaster.setResetPasswordCode("0");
                    loginMaster.setStatus("pending");
                    loginMaster.setRegisteredDate(new Date());
                    //loginMaster.setNationality("bangladeshi");
                    loginMaster.setNationality("Bhutanese");
                    loginMaster.setHintQuestion("");
                    loginMaster.setHintAnswer("");
                    loginMaster.setTblUserTypeMaster(new TblUserTypeMaster((byte) 3));

                    TblEmployeeMaster employeeMaster = new TblEmployeeMaster();
                    //employeeMaster.setTblLoginMaster(loginMaster);
                    employeeMaster.setEmployeeName(request.getParameter("fullName").trim());
                    //employeeMaster.setEmployeeNameBangla(BanglaNameUtils.getBytes(request.getParameter("banglaName")));
                    employeeMaster.setEmployeeNameBangla(BanglaNameUtils.getBytes(new String(request.getParameter("banglaName").getBytes("ISO-8859-1"), "UTF-8")));
                    employeeMaster.setNationalId(citizenId);
                    employeeMaster.setMobileNo(mobId);
                    employeeMaster.setUserEmployeeId(userEmployeeId);
                    employeeMaster.setContactAddress(contactAddress);
                    employeeMaster.setCreatedBy(sessionUserId);
                    employeeMaster.setTblDesignationMaster(new TblDesignationMaster((int) dId));

                    boolean flag = false;
                    String validEmailStr = "Please Enter Valid e-mail ID";
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    if (commonService.verifyMail(emailId).contains(validEmailStr)) {
                        response.sendRedirect("admin/GovtUserCreation.jsp?flag=" + flag + "&action=" + action + "&msg=emailPattern");
                    } else if (!"OK".equals(commonService.verifyMail(emailId))) {
                        response.sendRedirect("admin/GovtUserCreation.jsp?flag=" + flag + "&action=" + action + "&msg=email");
                    } /*else if(!"OK".equals(commonService.verifyNationalId(naId))){
                         response.sendRedirect("admin/GovtUserCreation.jsp?flag="+flag+"&action="+action+"&msg=nation");
                    }*/ /*else if(!"OK".equals(commonService.verifyMobileNo(mobId))) {
                         response.sendRedirect("admin/GovtUserCreation.jsp?flag="+flag+"&action="+action+"&msg=mobileId");
                    }*/ else if (!"OK".equals(commonService.verifyUserEmployeeId(userEmployeeId))) {
                        response.sendRedirect("admin/GovtUserCreation.jsp?flag=" + flag + "&action=" + action + "&msg=useremployeeid");
                    } else {

                        int empId = 0;
                        //if(sendMailAndSMS(emailId, request.getParameter("password"), "+975"+mobId)){//msg n mail
                        //empId = govtUserCreationService.createGovtUser(loginMaster, employeeMaster);

                        String officeId[] = {"0"};
                        officeId[0] = "208";
                        int oOffice = 2;
                        List<Object[]> list = govtUserCreationService.officeMasterForPe(sessionUserId);
                        if (list != null) {
                            if (!list.isEmpty()) {
                                oOffice = Integer.parseInt(list.get(0)[0].toString());
                            }
                        }
                        empId = govtUserCreationService.createGovtUser(loginMaster, employeeMaster, oOffice);
//                    TblEmployeeOffices tblEmployeeOffices = new TblEmployeeOffices();
//                    tblEmployeeOffices.setTblEmployeeMaster(new TblEmployeeMaster(empId));
//                    tblEmployeeOffices.setTblOfficeMaster(new TblOfficeMaster(oOffice));
//                    tblEmployeeOffices.setDesignationId(dId);
//                    govtUserCreationService.createEmployeeOffices(tblEmployeeOffices);
//                    tblEmployeeOffices = null;
//                    
//                    govtUserCreationService.updateEmployeeMaster(empId,"user",dId);

//                    if (request.getParameter("bigOffice") != null) {
//                    officeId = request.getParameterValues("bigOffice");
//                    //LOGGER.debug(" officeId ::  "+officeId);
//
//                    for (String officeStr : officeId) {
//                        TblEmployeeOffices tblEmployeeOffices = new TblEmployeeOffices();
//                        tblEmployeeOffices.setTblEmployeeMaster(new TblEmployeeMaster(empId));
//                        tblEmployeeOffices.setTblOfficeMaster(new TblOfficeMaster(Integer.parseInt(officeStr)));
//                        tblEmployeeOffices.setDesignationId(30);
//                        govtUserCreationService.createEmployeeOffices(tblEmployeeOffices);
//                        tblEmployeeOffices = null;
//                    }
//
//                }
                        //}
                        if (empId > 0) {
                            flag = true;
                        }
                        LOGGER.debug("createGovtUser " + logUserId + " Ends:");
                        response.sendRedirect("admin/GovtUserCreateRoleOff.jsp?empId=" + empId + "&flag=" + flag + "&action=" + action);
                    }
                } catch (Exception e) {
                    LOGGER.error("createGovtUser " + logUserId + ":" + e);
                }
            } else if ("createGovtUserRole".equalsIgnoreCase(action) || "editGovtUserRole".equalsIgnoreCase(action)) {
//               HttpSession session = request.getSession();
                LOGGER.debug("createGovtUserRole  editGovtUserRole " + logUserId + " Starts:");
                String query = "";
                short deptId = 0;
                String officeId[] = null;
                int empId = 0;
                byte procurementId = 0;
                //int designationId=0;
                boolean flag = false;
                String ccgp = "",
                        procRoleGrOne = "",
                        procRoleGrTwo = "",
                        procRoleGrThree = "";

                if (request.getParameter("empId") != null) {
                    empId = Integer.parseInt(request.getParameter("empId").toString());
                }

                if (request.getParameter("txtdepartmentid") != null) {
                    deptId = Short.parseShort(request.getParameter("txtdepartmentid").toString());
                }
//                if (request.getParameter("designation") != null) {
//                    designationId = Integer.parseInt(request.getParameter("designation").toString());
//                }

                //if ("editGovtUserRole".equalsIgnoreCase(action)) {
                // edit page is not in use ---------------------
                // govtUserCreationService.deleteEmployeeRoles(empId);
                // LOGGER.debug(" editGovtUserRole =================>>>>>>");
                //}
                if (request.getParameter("ccgp") != null) {
                    try {
                        LOGGER.debug("ccgp " + logUserId + " Starts:");
                        ccgp = request.getParameter("ccgp");
                        procurementId = Byte.parseByte(ccgp);
//                        TblEmployeeRoles tblEmployeeRoles = new TblEmployeeRoles();
//                        tblEmployeeRoles.setTblDepartmentMaster(new TblDepartmentMaster((short)deptId));
//                        tblEmployeeRoles.setTblProcurementRole(new TblProcurementRole((byte)procurementId));
//                        tblEmployeeRoles.setTblEmployeeMaster(new TblEmployeeMaster(empId));
//                        govtUserCreationService.createGovtUserRole(tblEmployeeRoles);
                        query = query + "INSERT INTO [tbl_EmployeeRoles]([procurementRoleId],[employeeId],[departmentId])"
                                + "VALUES (" + (byte) procurementId + "," + empId + "," + (short) deptId + ")";
//                         tblEmployeeRoles = null;
                    } catch (Exception e) {
                        LOGGER.error("createGovtUserRole ccgp:" + e);
                    }
                    LOGGER.debug("ccgp " + logUserId + " Ends:");
                }
                if (request.getParameter("procRoleGrOne") != null) {
                    try {
                        LOGGER.debug("procRoleGrOne " + logUserId + " Starts:");
                        procRoleGrOne = request.getParameter("procRoleGrOne");
                        procurementId = Byte.parseByte(procRoleGrOne);
//                        TblEmployeeRoles tblEmployeeRoles = new TblEmployeeRoles();
//                        tblEmployeeRoles.setTblDepartmentMaster(new TblDepartmentMaster((short) deptId));
//                        tblEmployeeRoles.setTblProcurementRole(new TblProcurementRole(procurementId));
//                        tblEmployeeRoles.setTblEmployeeMaster(new TblEmployeeMaster(empId));
//                        govtUserCreationService.createGovtUserRole(tblEmployeeRoles);
                        query = query + " INSERT INTO [tbl_EmployeeRoles]([procurementRoleId],[employeeId],[departmentId])"
                                + "VALUES (" + (byte) procurementId + "," + empId + "," + (short) deptId + ")";
//                         tblEmployeeRoles = null;
                    } catch (Exception e) {
                        LOGGER.error("procRoleGrOne " + logUserId + "" + e);
                    }
                    LOGGER.debug("procRoleGrOne " + logUserId + " Ends:");
                }
                if (request.getParameter("procRoleGrTwo") != null) {
                    try {
                        LOGGER.debug("procRoleGrTwo " + logUserId + " Starts");
                        procRoleGrTwo = request.getParameter("procRoleGrTwo");
                        procurementId = Byte.parseByte(procRoleGrTwo);
//                        TblEmployeeRoles tblEmployeeRoles = new TblEmployeeRoles();
//                        tblEmployeeRoles.setTblDepartmentMaster(new TblDepartmentMaster((short) deptId));
//                        tblEmployeeRoles.setTblProcurementRole(new TblProcurementRole(procurementId));
//                        tblEmployeeRoles.setTblEmployeeMaster(new TblEmployeeMaster(empId));
//                        govtUserCreationService.createGovtUserRole(tblEmployeeRoles);
//                        tblEmployeeRoles = null;
                        query = query + " INSERT INTO [tbl_EmployeeRoles]([procurementRoleId],[employeeId],[departmentId])"
                                + "VALUES (" + (byte) procurementId + "," + empId + "," + (short) deptId + ")";
                    } catch (Exception e) {
                        LOGGER.error("procRoleGrTwo " + logUserId + " " + e);
                    }
                    LOGGER.debug("procRoleGrTwo " + logUserId + " Ends");
                }
                if (request.getParameter("procRoleGrThree") != null && !"null".equalsIgnoreCase(request.getParameter("procRoleGrThree"))) {
                    try {
                        LOGGER.debug("procRoleGrThree " + logUserId + " Starts:");
                        procRoleGrThree = request.getParameter("procRoleGrThree");
                        procurementId = Byte.parseByte(procRoleGrThree);
//                        TblEmployeeRoles tblEmployeeRoles = new TblEmployeeRoles();
//                        tblEmployeeRoles.setTblDepartmentMaster(new TblDepartmentMaster((short)deptId));
//                        tblEmployeeRoles.setTblProcurementRole(new TblProcurementRole(procurementId));
//                        tblEmployeeRoles.setTblEmployeeMaster(new TblEmployeeMaster(empId));
//                        govtUserCreationService.createGovtUserRole(tblEmployeeRoles);
//                        tblEmployeeRoles = null;
                        query = query + " INSERT INTO [tbl_EmployeeRoles]([procurementRoleId],[employeeId],[departmentId])"
                                + "VALUES (" + (byte) procurementId + "," + empId + "," + (short) deptId + ")";
                    } catch (Exception e) {
                        LOGGER.error("procRoleGrThree " + logUserId + " :" + e);
                    }
                    LOGGER.debug("procRoleGrThree " + logUserId + " Ends:");
                }
                if (request.getParameter("procRoleGrFour") != null && !"null".equalsIgnoreCase(request.getParameter("procRoleGrFour"))) {
                    try {
                        LOGGER.debug("procRoleGrFour " + logUserId + " Starts");
                        procRoleGrThree = request.getParameter("procRoleGrFour");
                        procurementId = Byte.parseByte(procRoleGrThree);
//                        TblEmployeeRoles tblEmployeeRoles = new TblEmployeeRoles();
//                        tblEmployeeRoles.setTblDepartmentMaster(new TblDepartmentMaster((short)deptId));
//                        tblEmployeeRoles.setTblProcurementRole(new TblProcurementRole(procurementId));
//                        tblEmployeeRoles.setTblEmployeeMaster(new TblEmployeeMaster(empId));
//                        govtUserCreationService.createGovtUserRole(tblEmployeeRoles);
//                        tblEmployeeRoles = null;
                        query = query + " INSERT INTO [tbl_EmployeeRoles]([procurementRoleId],[employeeId],[departmentId])"
                                + "VALUES (" + (byte) procurementId + "," + empId + "," + (short) deptId + ")";
                    } catch (Exception e) {
                        LOGGER.error("procRoleGrFour " + logUserId + " :" + e);
                    }
                    LOGGER.debug("procRoleGrFour " + logUserId + " Ends");
                }
                if (request.getParameter("procRoleGrFive") != null && !"null".equalsIgnoreCase(request.getParameter("procRoleGrFive"))) {
                    try {
                        LOGGER.debug("procRoleGrFive " + logUserId + " Starts:");
                        procRoleGrThree = request.getParameter("procRoleGrFive");
                        procurementId = Byte.parseByte(procRoleGrThree);
//                        TblEmployeeRoles tblEmployeeRoles = new TblEmployeeRoles();
//                        tblEmployeeRoles.setTblDepartmentMaster(new TblDepartmentMaster((short)deptId));
//                        tblEmployeeRoles.setTblProcurementRole(new TblProcurementRole(procurementId));
//                        tblEmployeeRoles.setTblEmployeeMaster(new TblEmployeeMaster(empId));
//                        govtUserCreationService.createGovtUserRole(tblEmployeeRoles);
                        query = query + " INSERT INTO [tbl_EmployeeRoles]([procurementRoleId],[employeeId],[departmentId])"
                                + "VALUES (" + (byte) procurementId + "," + empId + "," + (short) deptId + ")";
//                        tblEmployeeRoles = null;
                    } catch (Exception e) {
                        LOGGER.error("procRoleGrFive " + logUserId + " " + e);
                    }
                    LOGGER.debug("procRoleGrFive " + logUserId + " Ends:");
                }
                if (request.getParameter("procRoleGrSix") != null && !"null".equalsIgnoreCase(request.getParameter("procRoleGrSix"))) {
                    try {
                        LOGGER.debug("procRoleGrSix " + logUserId + " Starts:");
                        procRoleGrThree = request.getParameter("procRoleGrSix");
                        procurementId = Byte.parseByte(procRoleGrThree);
//                        TblEmployeeRoles tblEmployeeRoles = new TblEmployeeRoles();
//                        tblEmployeeRoles.setTblDepartmentMaster(new TblDepartmentMaster((short)deptId));
//                        tblEmployeeRoles.setTblProcurementRole(new TblProcurementRole(procurementId));
//                        tblEmployeeRoles.setTblEmployeeMaster(new TblEmployeeMaster(empId));
//                        govtUserCreationService.createGovtUserRole(tblEmployeeRoles);
                        query = query + " INSERT INTO [tbl_EmployeeRoles]([procurementRoleId],[employeeId],[departmentId])"
                                + "VALUES (" + (byte) procurementId + "," + empId + "," + (short) deptId + ")";
//                        tblEmployeeRoles = null;
                    } catch (Exception e) {
                        LOGGER.error("procRoleGrSix " + logUserId + " " + e);
                    }
                    LOGGER.debug("procRoleGrSix " + logUserId + " Ends:");
                }
                if (request.getParameter("procRoleGrSeven") != null && !"null".equalsIgnoreCase(request.getParameter("procRoleGrSeven"))) {
                    try {
                        LOGGER.debug("procRoleGrSeven " + logUserId + " Starts:");
                        procRoleGrThree = request.getParameter("procRoleGrSeven");
                        procurementId = Byte.parseByte(procRoleGrThree);
//                        TblEmployeeRoles tblEmployeeRoles = new TblEmployeeRoles();
//                        tblEmployeeRoles.setTblDepartmentMaster(new TblDepartmentMaster((short)deptId));
//                        tblEmployeeRoles.setTblProcurementRole(new TblProcurementRole(procurementId));
//                        tblEmployeeRoles.setTblEmployeeMaster(new TblEmployeeMaster(empId));
//                        govtUserCreationService.createGovtUserRole(tblEmployeeRoles);
                        query = query + " INSERT INTO [tbl_EmployeeRoles]([procurementRoleId],[employeeId],[departmentId])"
                                + "VALUES (" + (byte) procurementId + "," + empId + "," + (short) deptId + ")";
//                        tblEmployeeRoles = null;
                    } catch (Exception e) {
                        LOGGER.error("procRoleGrSeven " + logUserId + " " + e);
                    }
                    LOGGER.debug("procRoleGrSeven " + logUserId + " Ends:");
                }
                if (request.getParameter("procRoleGrEight") != null && !"null".equalsIgnoreCase(request.getParameter("procRoleGrEight"))) {
                    try {
                        LOGGER.debug("procRoleGrEight " + logUserId + " Starts:");
                        procRoleGrThree = request.getParameter("procRoleGrEight");
                        procurementId = Byte.parseByte(procRoleGrThree);
//                        TblEmployeeRoles tblEmployeeRoles = new TblEmployeeRoles();
//                        tblEmployeeRoles.setTblDepartmentMaster(new TblDepartmentMaster((short)deptId));
//                        tblEmployeeRoles.setTblProcurementRole(new TblProcurementRole(procurementId));
//                        tblEmployeeRoles.setTblEmployeeMaster(new TblEmployeeMaster(empId));
//                        govtUserCreationService.createGovtUserRole(tblEmployeeRoles);
                        query = query + " INSERT INTO [tbl_EmployeeRoles]([procurementRoleId],[employeeId],[departmentId])"
                                + "VALUES (" + (byte) procurementId + "," + empId + "," + (short) deptId + ")";
//                        tblEmployeeRoles = null;
                    } catch (Exception e) {
                        LOGGER.error("procRoleGrEight " + logUserId + " " + e);
                    }
                    LOGGER.debug("procRoleGrEight " + logUserId + " Ends:");
                }
                try {
                    LOGGER.debug("Update ccgp " + logUserId + " Starts:");
                    String finPowerBy = "user";
//                    if(request.getParameter("assFinPower") != null){
//                        finPowerBy = request.getParameter("assFinPower");
//                    }

                    //govtUserCreationService.updateLoginMaster(empId);
//                    master.setNextScreen("GovtUserFinanceRole");
                    query = query + " UPDATE tbl_LoginMaster SET nextScreen='GovtUserFinanceRole' WHERE userId="
                            + " (select userid from tbl_EmployeeMaster where employeeId = " + empId + ")";
                    govtUserCreationService.createGovtUserRole(query);

//                    if ("editGovtUserRole".equalsIgnoreCase(action)) {
//                        govtUserCreationService.deleteEmployeeFinanceRole(empId);
//                        LOGGER.debug(" editGovtUserRole deleteEmployeeFinanceRole =================>>>>>>");
//                    }
                    flag = true;
                } catch (Exception e) {
                    LOGGER.error("Update ccgp " + logUserId + " :" + e);
                }
                LOGGER.debug("Update ccgp " + logUserId + " Ends:");
                response.sendRedirect("admin/GovtUserCreateRoleOff.jsp?empId=" + empId + "&flag=" + flag + "&action=" + action);
            } else if ("createGovtFinanceRole".equalsIgnoreCase(action) || "editGovtFinanceRole".equalsIgnoreCase(action)) {
                LOGGER.debug("createGovtFinanceRole  editGovtFinanceRole " + logUserId + " Starts:");
                int count = Integer.parseInt(request.getParameter("txtCount").toString());

                boolean flag = false;
                String eRoleId = "";
                String finPowerBy = "";
                // LOGGER.debug(" count is  :: "+count);
                byte procurementNature = 0;
                byte procurementMethod = 0;
                byte budgetType = 0;
                int empRoleId = 0;
                int empId = 0;

                if (request.getParameter("empId") != null) {
                    empId = Integer.parseInt(request.getParameter("empId").toString());
                }

                if (request.getParameter("hidFinPowerBy") != null) {
                    finPowerBy = request.getParameter("hidFinPowerBy");
                }

                //int j=0;
                if ("role".equalsIgnoreCase(request.getParameter("hidFinPowerBy"))) {
                    LOGGER.debug("role" + logUserId + " Starts:");
                    if (request.getParameter("hidEmpRoleId") != null) {
                        eRoleId = request.getParameter("hidEmpRoleId");
                        try {
                            govtUserCreationService.deleteEmployeeFinanceRole(eRoleId);
                        } catch (Exception e) {
                            LOGGER.error(" Error in hidEmpRoleId emp role delete -------------------------------->>>>>>>>>");
                        }
                    }
                    if ("editGovtFinanceRole".equalsIgnoreCase(action)) {
                        govtUserCreationService.deleteEmployeeUserWiseFinancePower(empId);
                    }

                    String rId[] = eRoleId.split(",");
                    for (int i = 1; i <= count; i++) {
                        for (int j = 0; j < (rId.length); j++) {

                            LOGGER.debug(" i value is :: " + i);
                            if (request.getParameter("cmbnature" + i + "_" + 1) != null) {
                                TblEmployeeFinancialPower tblEmployeeFinancialPower = new TblEmployeeFinancialPower();
                                tblEmployeeFinancialPower.setTblEmployeeMaster(new TblEmployeeMaster(empId));
                                empRoleId = Integer.parseInt(rId[j].trim());
                                tblEmployeeFinancialPower.setEmployeeRoleId(empRoleId);

                                procurementNature = Byte.parseByte(request.getParameter("cmbnature" + i + "_" + 1));
                                tblEmployeeFinancialPower.setProcurementNatureId(procurementNature);

                                procurementMethod = Byte.parseByte(request.getParameter("cmbMethod" + i + "_" + 2));
                                tblEmployeeFinancialPower.setProcurementMethodId(procurementMethod);

                                budgetType = Byte.parseByte(request.getParameter("cmbBudgetType" + i + "_" + 3));
                                tblEmployeeFinancialPower.setTblBudgetType(new TblBudgetType(budgetType));

                                tblEmployeeFinancialPower.setOperator(request.getParameter("cmbOperator" + i + "_" + 4));
                                BigDecimal amount = new BigDecimal(request.getParameter("txtAmount" + i + "_" + 5));

                                tblEmployeeFinancialPower.setAmount(amount);
                                govtUserCreationService.addGovtUserFinancePower(tblEmployeeFinancialPower);

                                tblEmployeeFinancialPower = null;

                            }
                        }
                    }
                    LOGGER.debug("role" + logUserId + " Ends:");
                } else {//"user".equalsIgnoreCase(request.getParameter("hidFinPowerBy"))
                    LOGGER.debug(request.getParameter("hidFinPowerBy") + " " + logUserId + " Starts:");
                    if ("editGovtFinanceRole".equalsIgnoreCase(action)) {
                        govtUserCreationService.deleteEmployeeUserWiseFinancePower(empId);
                    }

                    for (int i = 1; i <= count; i++) {
                        if (request.getParameter("cmbnature" + i + "_" + 1) != null) {
                            TblEmployeeFinancialPower tblEmployeeFinancialPower = new TblEmployeeFinancialPower();
                            tblEmployeeFinancialPower.setTblEmployeeMaster(new TblEmployeeMaster(empId));

                            empRoleId = Integer.parseInt("9999");
                            tblEmployeeFinancialPower.setEmployeeRoleId(empRoleId);

                            procurementNature = Byte.parseByte(request.getParameter("cmbnature" + i + "_" + 1));
                            tblEmployeeFinancialPower.setProcurementNatureId(procurementNature);

                            procurementMethod = Byte.parseByte(request.getParameter("cmbMethod" + i + "_" + 2));
                            tblEmployeeFinancialPower.setProcurementMethodId(procurementMethod);

                            budgetType = Byte.parseByte(request.getParameter("cmbBudgetType" + i + "_" + 3));
                            tblEmployeeFinancialPower.setTblBudgetType(new TblBudgetType(budgetType));

                            tblEmployeeFinancialPower.setOperator(request.getParameter("cmbOperator" + i + "_" + 4));
                            BigDecimal amount = new BigDecimal(request.getParameter("txtAmount" + i + "_" + 5));

                            tblEmployeeFinancialPower.setAmount(amount);
                            govtUserCreationService.addGovtUserFinancePower(tblEmployeeFinancialPower);

                            tblEmployeeFinancialPower = null;

                        }
                    }
                }
                LOGGER.debug("createGovtFinanceRole  editGovtFinanceRole " + logUserId + " Ends:");
                response.sendRedirect("admin/ManageGovtUser.jsp?flag=" + flag + "&action=" + action);
            } ///////  for EDIT --------------------------------------------------------------------->>>>>>>>
            else if ("editGovtUser".equalsIgnoreCase(action)) {
                LOGGER.debug("editGovtUser " + logUserId + " Starts:");
                int empId = 0;
                //int userId=0;
                boolean flag = false;

                String naId = "";
                String mobId = "";
                String prvNalId = "";
                String prvmobId = "";
                String fromWhere = "";
                String mailId = null;
                String oldMail = "";
                String contactAddress = "";
                String userEmployeeId = "";
                int dId = 0;
                int officeId = 0;
                if (request.getParameter("mailId") != null) {
                    mailId = request.getParameter("mailId").trim();
                }
                if (request.getParameter("hdnMailId") != null) {
                    oldMail = request.getParameter("hdnMailId").trim();
                }
                if (request.getParameter("citizenId") != null) {
                    naId = request.getParameter("citizenId").trim();
                }
                if (request.getParameter("mobileNo") != null) {
                    mobId = request.getParameter("mobileNo").trim();
                }
                if (request.getParameter("nId") != null) {
                    prvNalId = request.getParameter("nId").trim();
                }
                if (request.getParameter("mobId") != null) {
                    prvmobId = request.getParameter("mobId").trim();
                }
                if (request.getParameter("fromWhere") != null) {
                    fromWhere = request.getParameter("fromWhere");
                }

                if (request.getParameter("userId") != null) {
                    userId = Integer.parseInt(request.getParameter("userId").toString());
                }

                if (request.getParameter("empId") != null) {
                    empId = Integer.parseInt(request.getParameter("empId").toString());
                }

                if (request.getParameter("employeeId") != null) {
                    userEmployeeId = request.getParameter("employeeId").trim();
                }

                if (request.getParameter("contactAddress") != null) {
                    contactAddress = request.getParameter("contactAddress").trim();
                }

                if (request.getParameter("designation") != null) {
                    dId = Integer.parseInt(request.getParameter("designation").trim());
                }

                if (request.getParameter("office") != null) {
                    officeId = Integer.parseInt(request.getParameter("office").trim());
                }

                LOGGER.debug(" empId is :: " + empId);
                //TblLoginMaster loginMaster = new TblLoginMaster();
                //loginMaster.setEmailId(request.getParameter("mailId"));
                //loginMaster.setPassword(request.getParameter("password"));
                int uId = 0;
                for (TblEmployeeMaster master : govtUserCreationService.getUserIdForUpdate(empId)) {
                    uId = master.getTblLoginMaster().getUserId();
                }

                TblEmployeeMaster employeeMaster = new TblEmployeeMaster();
                /*This if conditon is used when satus is not completed and user change employees mail id*/
                if (mailId != null) {
                    if (!oldMail.equals(mailId)) {
                        flag = govtUserCreationService.updateMailId(mailId, empId);
                    }
                }
                employeeMaster.setEmployeeName(request.getParameter("fullName"));
                //String bangName = 
                //employeeMaster.setEmployeeNameBangla(BanglaNameUtils.getBytes(request.getParameter("banglaName")));
                employeeMaster.setEmployeeNameBangla(BanglaNameUtils.getBytes(new String(request.getParameter("banglaName").getBytes("ISO-8859-1"), "UTF-8")));
                employeeMaster.setNationalId(naId);
                employeeMaster.setMobileNo(mobId);
                employeeMaster.setUserEmployeeId(userEmployeeId);
                employeeMaster.setContactAddress(contactAddress);

                try {
                    if (naId.equals(prvNalId) && mobId.equals(prvmobId)) {
                        empId = govtUserCreationService.editGovtUser(employeeMaster, empId, uId, dId, officeId);
                        if (empId > 0) {
                            flag = true;
                        }
                        if ("EditProfile".equalsIgnoreCase(fromWhere)) {
                            response.sendRedirect("resources/common/PaDashboard.jsp?viewtype=pending&proup=true");
                        } else {
                            response.sendRedirect("admin/ManageGovtUser.jsp?&empId=" + empId + "&action=" + action + "&flag=" + flag + "&msg=" + flag);
                        }
                    } else {
                        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                        /*if (!"OK".equals(commonService.verifyNationalId(naId)) && !naId.equals(prvNalId)) {
                            response.sendRedirect("admin/GovtUserCreation.jsp?empId=" + empId + "&flag=" + flag + "&action=" + action + "&msg=nation");
                        } else*/ /*if (!"OK".equals(commonService.verifyMobileNo(mobId)) && !mobId.equals(prvmobId)) {
                            response.sendRedirect("admin/GovtUserCreation.jsp?empId=" + empId + "&flag=" + flag + "&action=" + action + "&msg=mobileId");
                        } else { */
                        empId = govtUserCreationService.editGovtUser(employeeMaster, empId, uId, dId, officeId);
                        if (empId > 0) {
                            flag = true;
                        }

                        if ("EditProfile".equalsIgnoreCase(fromWhere)) {
                            response.sendRedirect("resources/common/PaDashboard.jsp?viewtype=pending&proup=true");
                        } else {
                            response.sendRedirect("admin/ManageGovtUser.jsp?&empId=" + empId + "&action=" + action + "&flag=" + flag + "&msg=" + flag);
                        }
                        //}
                    }
                } catch (Exception e) {
                    LOGGER.error("editGovtUser " + logUserId + " :" + e);
                }
                LOGGER.debug("editGovtUser " + logUserId + " Ends:");
            } else if ("Remove".equalsIgnoreCase(action)) {
                LOGGER.debug("Remove " + logUserId + " Starts:");
                // LOGGER.debug("  in remove ----------------");
                String eRoleId = "";
                String officeId = "";
                int empId = 0;
                boolean flag = false;

                if (request.getParameter("empId") != null) {
                    empId = Integer.parseInt(request.getParameter("empId").toString());
                }
                if (request.getParameter("empRoleId") != null) {
                    eRoleId = request.getParameter("empRoleId");
                }
                if (request.getParameter("officeId") != null) {
                    officeId = request.getParameter("officeId");
                }

                try {
                    govtUserCreationService.deleteEmployeeRoles(eRoleId);
                    govtUserCreationService.deleteEmployeeOffices("" + empId);
                    govtUserCreationService.deleteEmployeeUserWiseFinancePower(empId);
                    //govtUserCreationService.deleteEmployeeFinanceRole(eRoleId); not in use for employee role wise
                } catch (Exception e) {
                    LOGGER.error(" Error in hidEmpRoleId emp role delete -------------------------------->>>>>>>>>");
                }
                LOGGER.debug("Remove " + logUserId + " Ends:");
                response.sendRedirect("admin/GovtUserCreateRoleOff.jsp?empId=" + empId + "&action=" + action);
            } else if ("officeEdit".equalsIgnoreCase(action)) {
                LOGGER.debug("officeEdit " + logUserId + " Starts:");
                String officeId[] = null;
                String officeId1 = "";
                int empId = 0;
                int designationId = 0;
                boolean flag = false;
                String pRoleId = "";
                boolean isPEExist = false;

                if (request.getParameter("designation") != null) {
                    designationId = Integer.parseInt(request.getParameter("designation").toString());
                }
                if (request.getParameter("empId") != null) {
                    empId = Integer.parseInt(request.getParameter("empId").toString());
                }
                if (request.getParameter("officeId") != null) {
                    officeId1 = request.getParameter("officeId");
                }

                if (request.getParameter("pRoleId") != null) {
                    pRoleId = request.getParameter("pRoleId");
                }

                // LOGGER.debug("  officeEdit --------------------------->>>>>>"+officeId1);
                officeId = officeId1.split(",");
                try {
                    String sofficeId[] = null;
                    String sOffId = "";
                    if (request.getParameterValues("soffice") != null) {
                        sofficeId = request.getParameterValues("soffice");
                        for (String sId : sofficeId) {
                            sOffId += sId + ",";
                        }
                        sOffId = sOffId.substring(0, sOffId.length() - 1);
                    }
                    CreateProjectServiceImpl getOfficeData = (CreateProjectServiceImpl) AppContext.getSpringBean("CreateProjectService");
                    getOfficeData.setUserId(logUserId);
                    List<CommonAppData> list = null;
                    if (pRoleId.contains("PE")) {
                        list = getOfficeData.getDepartmentsByUserid("ExcludePEOffice", "" + sOffId, "" + empId);
                        if (list != null && !list.isEmpty()) {
                            isPEExist = true;
                        }
                    }
                    if (!isPEExist) {
                        LOGGER.debug("isPEExist " + logUserId + " Strats:");
                        govtUserCreationService.deleteEmployeeOffices("" + empId);

                        for (String officeStr : sofficeId) {
                            TblEmployeeOffices tblEmployeeOffices = new TblEmployeeOffices();
                            tblEmployeeOffices.setTblEmployeeMaster(new TblEmployeeMaster(empId));
                            tblEmployeeOffices.setTblOfficeMaster(new TblOfficeMaster(Integer.parseInt(officeStr)));
                            tblEmployeeOffices.setDesignationId(designationId);
                            govtUserCreationService.createEmployeeOffices(tblEmployeeOffices);
                            tblEmployeeOffices = null;
                        }
                        //govtUserCreationService.deleteEmployeeFinanceRole(eRoleId); not in use for employee role wise
                        flag = true;
                    }
                } catch (Exception e) {
                    LOGGER.error("isPEExist " + logUserId + " :" + e);
                }
                LOGGER.debug("officeEdit " + logUserId + " Ends:");
                response.sendRedirect("admin/GovtUserCreateRoleOff.jsp?empId=" + empId + "&action=" + action + "&flag=" + flag + "&Edit=Edit");

            } else if ("finalSubmission".equalsIgnoreCase(action)) {
                LOGGER.debug("finalSubmission " + logUserId + " Starts:");
                int userId = 0;
                String emailId = "";
                String password = "";
                String mobNo = "";
                String procRoleName = "";
                int procRole = 0;
                String msg = "";

                if (request.getParameter("userId") != null) {
                    userId = Integer.parseInt(request.getParameter("userId"));
                }
                if (request.getParameter("edit") != null) {
                    msg = request.getParameter("edit");
                }
                if (request.getParameter("hidEmailId") != null) {
                    emailId = request.getParameter("hidEmailId");
                }
                if (request.getParameter("hidPassword") != null) {
                    password = request.getParameter("hidPassword");
                }
                if (request.getParameter("hidMobNo") != null) {
                    mobNo = request.getParameter("hidMobNo");
                }
                if (request.getParameter("role") != null) {
                    procRole = Integer.parseInt(request.getParameter("role").trim());
                }
                if (request.getParameter("roleName") != null) {
                    procRoleName = request.getParameter("roleName");
                }

                List<Object[]> otherDetails = null;
                otherDetails = govtUserCreationService.getOfficeDetailsforMail(userId);
                boolean flag = govtUserCreationService.loginMasterUpdate(userId, SHA1HashEncryption.encodeStringSHA1(password));
                try {
                    List<TblEmployeeMaster> list = govtUserCreationService.getDetailsForTransfer(userId);
                    if (list != null && !list.isEmpty()) {
                        TransferEmployeeServiceImpl transferEmployeeServiceImpl = (TransferEmployeeServiceImpl) AppContext.getSpringBean("TransferEmployeeServiceImpl");
                        TblEmployeeTrasfer tblEmployeeTrasfer = new TblEmployeeTrasfer();
                        tblEmployeeTrasfer.setTblLoginMaster(new TblLoginMaster(userId));
                        tblEmployeeTrasfer.setTransferDt(new Date());
                        tblEmployeeTrasfer.setCreatedBy(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
                        tblEmployeeTrasfer.setRemarks("");
                        tblEmployeeTrasfer.setEmployeeName(list.get(0).getEmployeeName());
                        tblEmployeeTrasfer.setEmployeeNameBangla(list.get(0).getEmployeeNameBangla());
                        tblEmployeeTrasfer.setMobileNo(list.get(0).getMobileNo());
                        tblEmployeeTrasfer.setNationalId(list.get(0).getNationalId());
                        tblEmployeeTrasfer.setFinPowerBy(list.get(0).getFinPowerBy());
                        tblEmployeeTrasfer.setEmployeeId(list.get(0).getEmployeeId());
                        tblEmployeeTrasfer.setAction("create");
                        tblEmployeeTrasfer.setEmailId("");
                        tblEmployeeTrasfer.setReplacedBy("");
                        tblEmployeeTrasfer.setReplacedByEmailId("");
                        tblEmployeeTrasfer.setIsCurrent("Yes");
                        tblEmployeeTrasfer.setComments("");
                        tblEmployeeTrasfer.setUserEmployeeId(list.get(0).getUserEmployeeId());
                        tblEmployeeTrasfer.setContactAddress(list.get(0).getContactAddress());
                        List<TblEmployeeTrasfer> tblEmployeeTrasferForCheck = transferEmployeeServiceImpl.findTblEmployeeMaster(userId,"create","Yes");
                        if(tblEmployeeTrasferForCheck.size() == 0)
                        {
                            transferEmployeeServiceImpl.insertInToTblEmployeeMaster(tblEmployeeTrasfer);
                        }
                    }

                } catch (Exception e) {
                    LOGGER.error("finalSubmission " + logUserId + ":" + e);
                }
                //boolean flag=govtUserCreationService.loginMasterUpdate(userId,password);
                try {
                    boolean isEmail = sendMailAndSMS(emailId, password, "+975" + mobNo, procRole, procRoleName, otherDetails);
                } catch (Exception e) {
                    LOGGER.error("finalSubmission " + logUserId + ":" + e);
                }
                LOGGER.debug("finalSubmission " + logUserId + " Ends:");
                //out.flush();
                //out.print(flag);
                //out.flush();
                response.sendRedirect("admin/ManageGovtUser.jsp?flag=" + flag + "&msg=" + msg);
            } else if ("getProjectFrmPrograme".equalsIgnoreCase(action)) {
                StringBuffer options = new StringBuffer();
                CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                String progId = request.getParameter("progId");
                List<SPCommonSearchDataMore> appListDtBean = dataMoreService.geteGPDataMore("getProjectName", progId.equals("0") ? "" : progId);
                options.append("<option  value='0'>Please Select Project</option>");
                for (SPCommonSearchDataMore prog : appListDtBean) {
                    options.append("<option value='" + prog.getFieldName1() + "'>" + prog.getFieldName2() + "</option>");
                }
                out.print(options.toString());
                out.flush();
            }
        } catch (Exception e) {
            LOGGER.error("GovtUserSrBean  " + logUserId + " :" + e);
        } finally {
            out.close();
        }
        LOGGER.debug("GovtUserSrBean  " + logUserId + " Ends:");
    }

    public int getDptId(int uId) {
        int dptId = 2;
        List<Object[]> list = govtUserCreationService.dptIdForPe(uId);
        if (list != null) {
            if (!list.isEmpty()) {
                dptId = Integer.parseInt(list.get(0)[1].toString());
            }
        }
        return dptId;

    }

    public int getOnlyOffice(int uId) {
        int oOffice = 2;
        List<Object[]> list = govtUserCreationService.officeMasterForPe(uId);
        if (list != null) {
            if (!list.isEmpty()) {
                oOffice = Integer.parseInt(list.get(0)[0].toString());
            }
        }
        return oOffice;

    }

    private String getOffice(int deptId) {
        LOGGER.debug(" getOffice " + logUserId + " Starts:");
        String str = "";
        List<TblOfficeMaster> list = govtUserCreationService.officeMasterList(deptId);
        int count = 0;
        try {
            govtUserCreationService.setLogUserId(logUserId);
            // str = "<option value='0'>-- Select Office --</option>";
            if (list.size() > 0) {
                str = "<option value='0'>--Select PA Office--</option>";
                for (TblOfficeMaster object : list) {
                    //count++;
                    str += "<option value='" + object.getOfficeId() + "'>" + object.getOfficeName() + "</option>";
                    //str += object.getOfficeName() + "&nbsp;&nbsp;<input type='checkbox' name='office' id='office' value='" + object.getOfficeId() + "' /> ";
                    // str += "<option value='" + object.getOfficeId() + "' />" + object.getOfficeName() + "</option>";
//            if (count == 5) {
//                str += "</br>";
//                count = 0;
//            }
                }
            } else {
                str = "<option value='0'> No Office Found.</option>";
            }
            LOGGER.debug(" Str is ::: " + str);
        } catch (Exception e) {
            LOGGER.error(" getOffice " + logUserId + "::  " + e);
        }
        LOGGER.debug(" getOffice " + logUserId + " Ends:");
        return str;
    }

    private String getOfficeForPe(int userId) {
        LOGGER.debug("getOfficeForPe " + logUserId + " Starts:");
        String str = null;
        try {
            govtUserCreationService.setLogUserId(logUserId);
            List<Object[]> list = govtUserCreationService.officeMasterForPe(userId);
            if (list != null) {
                if (!list.isEmpty()) {
                    str = list.get(0)[1] + "<input type='hidden' name='office'   id='cmbOffice' value='" + list.get(0)[0] + "' />";
                } else {
                    str = "No Office Found";
                }
            } else {
                str = "No Office Found.";
            }
        } catch (Exception e) {
            LOGGER.debug("getOfficeForPe " + logUserId + " :" + e);
        }
        LOGGER.debug("getOfficeForPe " + logUserId + " Ends:");
        return str;
    }

    private String getSelectedOffice(int deptId, int employeeId) {
        String str = "";
        LOGGER.debug("getSelectedOffice " + logUserId + " Starts:");
        try {
            govtUserCreationService.setLogUserId(logUserId);
            List<TblOfficeMaster> list = govtUserCreationService.officeMasterList(deptId);
            List<TblEmployeeOffices> selectlist = govtUserCreationService.getGovtEmpOfficeData(employeeId);
            // LOGGER.debug("  selectlist  is :: ------------------------>>>>  "+selectlist);
            int count = 0;
            String isChecked = "";
            boolean flag = false;
            for (TblOfficeMaster object : list) {
                count++;
                // LOGGER.debug(" tblEmployeeOffices ::::===================>>"+selectlist.size());
                for (TblEmployeeOffices tblEmployeeOffices : selectlist) {
                    // LOGGER.debug(object.getOfficeId() +  " == " +tblEmployeeOffices.getTblOfficeMaster().getOfficeId());
                    if (object.getOfficeId() == tblEmployeeOffices.getTblOfficeMaster().getOfficeId()) {
                        flag = true;
                        break;
                    }
                }
                if (flag) {
                    isChecked = "selected";
                    flag = false;
                } else {
                    isChecked = "";
                }
                // str += object.getOfficeName() + "&nbsp;&nbsp;<input type='checkbox' name='office' id='office' value='" + object.getOfficeId() + "'  "+isChecked+" /> ";
                str += "<option value='" + object.getOfficeId() + "'  " + isChecked + "  >" + object.getOfficeName() + "</option>";
                //            if (count == 5) {
                //                str += "</br>";
                //                count = 0;
                //            }
            }

        } catch (Exception e) {
            LOGGER.debug("getSelectedOfficeForEdit " + logUserId + " :" + e);
        }
        LOGGER.debug("getSelectedOffice " + logUserId + " Ends:");
        //LOGGER.debug(" Str is ::: " + str);
        return str;
    }

    private String getSelectedOfficeForEdit(int deptId, int employeeId) {
        LOGGER.debug("getSelectedOfficeForEdit " + logUserId + " Starts:");
        StringBuffer str = new StringBuffer();
        try {
            CreateProjectServiceImpl getOfficeData = (CreateProjectServiceImpl) AppContext.getSpringBean("CreateProjectService");
            getOfficeData.setUserId(logUserId);
            List<CommonAppData> list = getOfficeData.getDepartmentsByUserid("ExcludePEOffice", "" + deptId, null);
            List<TblEmployeeOffices> selectlist = govtUserCreationService.getGovtEmpOfficeData(employeeId);
            //LOGGER.debug("  selectlist  is :: ------------------------>>>>  "+selectlist);
            int count = 0;
            String isChecked = "";
            boolean flag = false;
            for (CommonAppData object : list) {
                count++;
                // LOGGER.debug(" tblEmployeeOffices ::::===================>>"+selectlist.size());
                for (TblEmployeeOffices tblEmployeeOffices : selectlist) {
                    // LOGGER.debug(object.getOfficeId() +  " == " +tblEmployeeOffices.getTblOfficeMaster().getOfficeId());
                    if (Integer.parseInt(object.getFieldName1()) == tblEmployeeOffices.getTblOfficeMaster().getOfficeId()) {
                        flag = true;
                        break;
                    }
                }
                if (flag) {
                    isChecked = "selected";
                    flag = false;
                } else {
                    isChecked = "";
                }
                str.append("<option value='" + object.getFieldName1() + "'  " + isChecked + "  >" + object.getFieldName2() + "</option>");

            }
            getOfficeData = null;
            list = null;
            selectlist = null;
        } catch (Exception e) {
            LOGGER.debug("getSelectedOfficeForEdit " + logUserId + " :" + e);
        }

        LOGGER.debug("getSelectedOfficeForEdit " + logUserId + " Ends:");
        return str.toString();
    }

    /**
     * Getting gov user designation.
     *
     * @return String
     */
    private String getDesignation(short deptId) {
        StringBuffer str = new StringBuffer();
        LOGGER.debug("getDesignation " + logUserId + " Starts:");
        try {
            govtUserCreationService.setLogUserId(logUserId);
            List<TblDesignationMaster> list = govtUserCreationService.designationMasterList(deptId);
            str.append("<option value='0'> ---- Select Designation ----- </option>");
            for (TblDesignationMaster object : list) {
                str.append("<option value='" + object.getDesignationId() + "'>" + object.getDesignationName() + "</option>");
            }
        } catch (Exception e) {
            LOGGER.error("getDesignation " + logUserId + " :" + e);
        }
        /*List<TblDesignationMaster> list = govtUserCreationService.designationMasterList(deptId);
        str.append("<option value='0'> ---- Select Designation ----- </option>");
        for (TblDesignationMaster object : list) {
            str.append("<option value='" + object.getDesignationId() + "'>" + object.getDesignationName() + "</option>");
        }*/
        //LOGGER.debug(" Str is ::: " + str);
        LOGGER.debug("getDesignation " + logUserId + " Ends:");
        return str.toString();
    }

    /**
     * Getting gov user designation.
     *
     * @return String
     */
    private String getSelectedDesignation(short deptId, int designationId) {
        LOGGER.debug("getSelectedDesignation " + logUserId + " Starts:");
        StringBuffer str = new StringBuffer();

        try {
            govtUserCreationService.setLogUserId(logUserId);
            List<TblDesignationMaster> list = govtUserCreationService.designationMasterList(deptId);
            for (TblDesignationMaster object : list) {
                if (object.getDesignationId() == designationId) {
                    str.append("<option value='" + object.getDesignationId() + "' selected >" + object.getDesignationName() + "</option>");
                } else {
                    str.append("<option value='" + object.getDesignationId() + "' >" + object.getDesignationName() + "</option>");
                }
            }
        } catch (Exception e) {
            LOGGER.error("getSelectedDesignation " + logUserId + " :" + e);

        }
        LOGGER.debug("getSelectedDesignation " + logUserId + " Ends:");
        return str.toString();
    }

    /**
     * To get government user detail.
     *
     * @return array of object
     */
    public Object[] getGovtUserData(String... values) {
        Object[] obj = null;
        String fromWhere = "";
        if (values != null && values.length != 0) {
            fromWhere = values[0];
        }

        LOGGER.debug(" getGovtUserData " + logUserId + " Starts:");
        try {
            govtUserCreationService.setLogUserId(logUserId);
            govtUserCreationService.setUserId(userId + "");
            obj = govtUserCreationService.getGovtUserData(getEmpId(), fromWhere).get(0);
        } catch (Exception e) {
            LOGGER.error(" getGovtUserData " + logUserId + " :" + e);
        }
        LOGGER.debug(" getGovtUserData " + logUserId + " Ends:");
        return obj;
    }

    /**
     * Get the history of user as per userId
     *
     * @param userId
     * @return TblEmployeeTrasfer
     */
    public List<TblEmployeeTrasfer> getUserHistory(int userId) {
        List<TblEmployeeTrasfer> list = null;
        LOGGER.debug(" getUserHistory " + logUserId + " Starts:");
        try {
            govtUserCreationService.setLogUserId(logUserId);
            list = govtUserCreationService.getUserHistory(userId);
        } catch (Exception e) {
            LOGGER.error(" getUserHistory " + logUserId + " :" + e);
        }
        LOGGER.debug(" getUserHistory " + logUserId + " Ends:");
        return list;
    }

    /**
     * Get employee roles detail.
     *
     * @return List of employee roles
     */
    public List<TblEmployeeRoles> getEmployeeRoles() {
        LOGGER.debug(" getEmployeeRoles " + logUserId + " Starts:");
        List<TblEmployeeRoles> list = null;
        try {
            govtUserCreationService.setLogUserId(logUserId);
            list = govtUserCreationService.getGovtEmpData(getEmpId());
        } catch (Exception e) {
            LOGGER.error(" getEmployeeRoles " + logUserId + " :" + e);
        }
        LOGGER.debug(" getEmployeeRoles " + logUserId + " Ends:");
        return list;
    }

    /**
     * Get employee offices
     *
     * @return List of employee offices
     */
    public List<TblEmployeeOffices> getEmployeeOfficeses() {
        LOGGER.debug("getEmployeeOfficeses " + logUserId + " Starts:");
        List<TblEmployeeOffices> list = null;
        try {
            govtUserCreationService.setLogUserId(logUserId);
            list = govtUserCreationService.getGovtEmpOfficeData(getEmpId());
        } catch (Exception e) {
            LOGGER.error("getEmployeeOfficeses " + logUserId + " :" + e);
        }
        LOGGER.debug("getEmployeeOfficeses " + logUserId + " Ends:");
        return list;
    }

    /**
     * Get department master detail
     *
     * @return object of TblDepartmentMaster
     */
    public TblDepartmentMaster getDepartmentMasterData() {
        LOGGER.debug("getDepartmentMasterData " + logUserId + " Starts:");
        TblDepartmentMaster tblDepartmentMaster = null;
        try {
            govtUserCreationService.setLogUserId(logUserId);
            tblDepartmentMaster = govtUserCreationService.getDepartmentMasterData(getDepartmentId());
        } catch (Exception e) {
            LOGGER.debug("getDepartmentMasterData " + logUserId + " :" + e);
        }
        LOGGER.debug("getDepartmentMasterData " + logUserId + " Ends:");
        return tblDepartmentMaster;
    }

    /**
     * Get employee finance power
     *
     * @return List of employee finance power
     */
    public List<VwEmpfinancePower> getGovtFinanceRole() {
        LOGGER.debug("getGovtFinanceRole " + logUserId + " Starts:");
        List<VwEmpfinancePower> list = null;
        try {
            govtUserCreationService.setLogUserId(logUserId);
            list = govtUserCreationService.getGovtUserFinanceRole(getEmpId(), getDepartmentId());
        } catch (Exception e) {
            LOGGER.error("getGovtFinanceRole " + logUserId + " :" + e);
        }
        LOGGER.debug("getGovtFinanceRole " + logUserId + " Ends:");
        return list;

    }

    List<SelectItem> departmentList = new ArrayList<SelectItem>();

    /**
     * Get department list.
     *
     * @return List of SelectItem
     */
    public List<SelectItem> getDepartmentList() {
        LOGGER.debug("getDepartmentList " + logUserId + " Starts:");
        try {
            govtUserCreationService.setLogUserId(logUserId);
            if (departmentList.isEmpty()) {
                List<TblDepartmentMaster> listDepartment = govtUserCreationService.departmentMasterList();
                for (TblDepartmentMaster tblDepartmentMaster : listDepartment) {
                    departmentList.add(new SelectItem(tblDepartmentMaster.getDepartmentId(), tblDepartmentMaster.getDepartmentName()));
                }
            }
        } catch (Exception e) {
            LOGGER.error("getDepartmentList " + logUserId + " :" + e);
        }
        LOGGER.debug("getDepartmentList " + logUserId + " Ends:");
        return departmentList;
    }

    List<SelectItem> roleList = new ArrayList<SelectItem>();

    List<String> procurementList = new ArrayList<String>();

    /**
     * Get employee roles.
     *
     * @return List of SelectItem
     */
    public List<SelectItem> getRole() {
        LOGGER.debug("getRole " + logUserId + " Starts:");
        try {
            govtUserCreationService.setLogUserId(logUserId);
            if (roleList.isEmpty()) {
                String str = govtUserCreationService.getProcurementRole(getEmpId());
                String str1[] = str.split(",");
                for (int i = 0; i < (str1.length) - 1; i = i + 2) {
                    roleList.add(new SelectItem(str1[i], str1[i + 1]));
                }
            }
        } catch (Exception e) {
            LOGGER.error("getRole " + logUserId + " :" + e);
        }
        LOGGER.debug("getRole " + logUserId + " Ends:");
        return roleList;
    }

    /**
     * Get Procurement List.
     *
     * @return String
     */
    public List<String> getProcurementList() {
        LOGGER.debug("getProcurementList " + logUserId + " Starts:");
        procurementList.add(1, "PE");
        procurementList.add(2, "Secretary");
        procurementList.add(3, "Minister");
        procurementList.add(4, "BOD");
        procurementList.add(5, "Authorized User");
        procurementList.add(6, "HOPA");
        procurementList.add(7, "CCGP");
        procurementList.add(19, "Authorized Officer");
        LOGGER.debug("getProcurementList " + logUserId + " Ends:");
        return procurementList;
    }

    /**
     * setter procurement list
     *
     * @param procurementList
     */
    public void setProcurementList(List<String> procurementList) {
        this.procurementList = procurementList;
    }

    List<SelectItem> budgetTypes = new ArrayList<SelectItem>();

    /**
     * Get budget types.
     *
     * @return List of selectItem
     */
    public List<SelectItem> getBudgetTypes() {
        LOGGER.debug("getBudgetTypes " + logUserId + " Starts:");
        try {
            govtUserCreationService.setLogUserId(logUserId);
            if (budgetTypes.isEmpty()) {
                List<TblBudgetType> listBudget = govtUserCreationService.getBudgetTypes();
                boolean flag = false;
                for (TblBudgetType tblBudgetType : listBudget) {
                    budgetTypes.add(new SelectItem(tblBudgetType.getBudgetTypeId(), tblBudgetType.getBudgetType()));
                }
            }

        } catch (Exception e) {
            LOGGER.error("getBudgetTypes " + logUserId + " :" + e);
        }
        LOGGER.debug("getBudgetTypes " + logUserId + " Ends:");
        return budgetTypes;
    }

    /**
     * setter of budgetTypes
     *
     * @param budgetTypes
     */
    public void setBudgetTypes(List<SelectItem> budgetTypes) {
        this.budgetTypes = budgetTypes;
    }

    private String verifyNationalId(String nationalId) {
//        StringBuilder msg= new StringBuilder();
//        msg.append(govtUserCreationService.verifyNationalId(nationalId));
//        return msg.toString();
        return "OK";
    }

    private String verifyMobileNo(String mobileNo) {
//        StringBuilder msg= new StringBuilder();
//        msg.append(govtUserCreationService.verifyMobileNo(mobileNo));
//        return msg.toString();
        return "OK";
    }

    /**
     * Get designation name.
     *
     * @param desgId
     * @return String
     */
    public String takeDesignationName(int desgId) {
        LOGGER.debug("takeDesignationName " + logUserId + " Starts:");
        String val = "";
        try {
            govtUserCreationService.setLogUserId(logUserId);
            val = govtUserCreationService.getDesignationName(desgId);
        } catch (Exception e) {
            LOGGER.debug("takeDesignationName " + logUserId + " :" + e);
        }
        LOGGER.debug("takeDesignationName " + logUserId + " Ends:");
        return val;
    }

    /**
     * Get user id from employee id.
     *
     * @param empId
     * @return int
     */
    public int takeUserIdFromEmpId(int empId) {
        LOGGER.debug("takeEmpIdFromUserId " + logUserId + " Starts:");
        int count = 0;
        try {
            govtUserCreationService.setLogUserId(logUserId);
            count = govtUserCreationService.getUserIdFromEmpId(empId);
        } catch (Exception e) {
            LOGGER.error("takeEmpIdFromUserId " + logUserId + " :" + e);
        }
        LOGGER.debug("takeEmpIdFromUserId " + logUserId + " Ends:");
        return count;
    }

    /**
     * Get employee id from user id.
     *
     * @param userId
     * @return int
     */
    public int takeEmpIdFromUserId(int userId) {
        LOGGER.debug("takeEmpIdFromUserId " + logUserId + " Starts:");
        int count = 0;
        try {
            govtUserCreationService.setLogUserId(logUserId);
            count = govtUserCreationService.getEmpIdFromUserId(userId);
        } catch (Exception e) {
            LOGGER.error("takeEmpIdFromUserId " + logUserId + " :" + e);
        }
        LOGGER.debug("takeEmpIdFromUserId " + logUserId + " Ends:");
        return count;
    }

    List<SPGovtEmpRolesReturn> empRoleses = new ArrayList<SPGovtEmpRolesReturn>();

    /**
     * Get employee roles
     *
     * @param empId
     * @return List of SPGovtEmpRolesReturn
     */
    public List<SPGovtEmpRolesReturn> getEmpRoleses(int empId) {
        LOGGER.debug("getEmpRoleses " + logUserId + " Starts:");
        try {
            govtUserCreationService.setLogUserId(logUserId);
            empRoleses = govtUserCreationService.getGovtEmpBySP(empId);
        } catch (Exception e) {
            LOGGER.error("getEmpRoleses " + logUserId + " :" + e);
        }
        LOGGER.debug("getEmpRoleses " + logUserId + " Ends:");
        return empRoleses;
    }

    /**
     * setter of empRoleses
     *
     * @param empRoleses
     */
    public void setEmpRoleses(List<SPGovtEmpRolesReturn> empRoleses) {
        this.empRoleses = empRoleses;
    }

    /**
     * Get user type wise department id.
     *
     * @param govUserId
     * @param userTypeId
     * @return int
     */
    public int getUserTypeWiseDepartmentId(int govUserId, int userTypeId) {
        LOGGER.debug("getUserTypeWiseDepartmentId " + logUserId + " Starts:");
        int deptId = 0;
        try {
            govtUserCreationService.setLogUserId(logUserId);
            deptId = govtUserCreationService.getUserTypeWiseDepartmentId(govUserId, userTypeId);
        } catch (Exception e) {
            LOGGER.error("getUserTypeWiseDepartmentId  " + logUserId + " :" + e);
        }
        LOGGER.debug("getUserTypeWiseDepartmentId " + logUserId + " Ends:");
        return deptId;
    }

    /**
     * Sent mail and sms when gov user final submission is completed.
     *
     * @param govUserId
     * @param userTypeId
     * @return boolean
     */
    private boolean sendMailAndSMS(String mailId, String password, String mobNo, int procRole, String procRoleName, List<Object[]> otherDetails) {
        LOGGER.debug("sendMailAndSMS " + logUserId + " Starts:");
        boolean mailSent = false;
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            String[] mailTo = {mailId};
            MailContentUtility mailContentUtility = new MailContentUtility();

            List list = null;
            if (procRole == 1) {
                list = mailContentUtility.getGovtPEContent(mailId, password, otherDetails);
            } else if (procRole == 22 || procRole == 23) {
                list = mailContentUtility.getGovtTECTOCMailContent(mailId, password, procRoleName, otherDetails);
            } else {
                list = mailContentUtility.getGovtOtherMailContent(mailId, password, otherDetails);
            }
            String mailSub = list.get(0).toString();
            String mailText = list.get(1).toString();
            sendMessageUtil.setEmailTo(mailTo);
            sendMessageUtil.setEmailSub(mailSub);
            sendMessageUtil.setEmailMessage(mailText);
            try {
                sendMessageUtil.sendEmail();
                sendMessageUtil.setSmsNo("" + mobNo);
                sendMessageUtil.setSmsBody("Dear User,%0Aprofile has been created Login information is sent to your registered Email ID.%0Ae-GP User GPPMD Office");
                sendMessageUtil.sendSMS();
            } catch (Exception e) {
                LOGGER.error("sendMailAndSMS " + logUserId + " :" + e);
            }
            mailSent = true;
        } catch (Exception ex) {
            LOGGER.error("sendMailAndSMS " + logUserId + " :" + ex);
        }
        LOGGER.debug("sendMailAndSMS " + logUserId + " Ends:");
        return mailSent;
    }

    /**
     * get if data is available for hope
     *
     * @param deptId
     * @return boolean
     */
    public boolean isDataAvailableForHopeForDeptId(short deptId) {
        LOGGER.debug("isDataAvailableForHopeForDeptId " + logUserId + " Starts:");
        boolean flag = false;
        try {
            govtUserCreationService.setLogUserId(logUserId);
            flag = govtUserCreationService.isDataAvailableForHopeForDeptId(deptId);
        } catch (Exception e) {
            LOGGER.error("isDataAvailableForHopeForDeptId " + logUserId + " :" + e);
        }
        LOGGER.debug("isDataAvailableForHopeForDeptId " + logUserId + " Ends:");
        return flag;
    }

    /**
     * get if data is available for hope
     *
     * @param officeId
     * @return boolean
     */
    public boolean isDataAvailableForPEForOfficeId(String officeId) {
        LOGGER.debug("isDataAvailableForPEForOfficeId " + logUserId + " Starts:");
        boolean flag = false;
        try {
            govtUserCreationService.setLogUserId(logUserId);
            flag = govtUserCreationService.isDataAvailableForPEForOfficeId(officeId);
        } catch (Exception e) {
            LOGGER.debug("isDataAvailableForPEForOfficeId " + logUserId + " :" + e);
        }
        LOGGER.debug("isDataAvailableForPEForOfficeId " + logUserId + " Ends:");
        return flag;
    }

    /**
     *
     * @param from
     * @param where
     * @return
     */
    public long countForQuery(String from, String where) {
        LOGGER.debug("countForQuery " + logUserId + " Starts:");

        long val = 0;
        try {
            govtUserCreationService.setLogUserId(logUserId);
            val = govtUserCreationService.countForQuery(from, where);
        } catch (Exception e) {
            LOGGER.error("countForQuery " + logUserId + " :" + e);
        }
        LOGGER.debug("countForQuery " + logUserId + " Ends:");
        return val;
    }

    /**
     * Get employee status
     *
     * @param employeeId
     * @return String
     */
    public String employeeStatus(int employeeId) {
        LOGGER.debug("employeeStatus " + logUserId + " Starts:");
        String val = "";

        try {
            govtUserCreationService.setLogUserId(logUserId);
            val = govtUserCreationService.employeeStatus(employeeId);
        } catch (Exception e) {
            LOGGER.error("employeeStatus " + logUserId + " :" + e);
        }
        LOGGER.debug("employeeStatus " + logUserId + " Ends:");
        return val;
    }

    public String getDistrictSubDistrict(String departmentId) {
        LOGGER.debug("getDistrictSubDistrict : " + logUserId + " Starts:");
        List<Object> list = null;
        String district = "";
        CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
        try {
            district = dataMoreService.geteGPData("getDistrictSubDistrict", departmentId).get(0).getFieldName1();
        } catch (Exception ex) {
            LOGGER.error("getDistrictSubDistrict : " + logUserId + ex);
        }
        LOGGER.debug("getDistrictSubDistrict : " + logUserId + " Ends:");
        return district;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
