/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearch;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.model.table.TblEvalReportClarification;
import com.cptu.egp.eps.model.table.TblEvalRptForwardToAa;
import com.cptu.egp.eps.model.table.TblEvalRptSentToAa;
import com.cptu.egp.eps.model.table.TblEvalTernotifyMember;
import com.cptu.egp.eps.service.serviceimpl.EvaluationService;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.DateUtils;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import com.cptu.egp.eps.service.serviceinterface.AccPaymentService;//added by dohatec
import com.cptu.egp.eps.service.serviceinterface.AddUpdateOpeningEvaluation;
/**
 *
 * @author nishit,rishita
 */
public class EvaluationServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(EvaluationServlet.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND = "Ends";
    private String logUserId = "0";
    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
            HttpSession session= request.getSession(true);
            // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer"));
                    
        try {
            EvaluationService evaluationService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
            evaluationService.setAuditTrail(objAuditTrail);
            String tenderid = request.getParameter("tenderid");
            String lotId = request.getParameter("lotId");
            String action = request.getParameter("action");
            String strEvalType = request.getParameter("eval_Type");
            String strRoundId = (request.getParameter("rId")!= null ? request.getParameter("rId"):"");
            String pageName = "";
            String s_desig ="";
            String strRole = "";
            if ("empCombo".equalsIgnoreCase(action)) {
                PrintWriter out = response.getWriter();
                s_desig = request.getParameter("designation");
                s_desig = getOfficerName(s_desig, tenderid);
                out.print(s_desig);
                out.flush();
                out.close();
            }

            if ("addAARpt".equalsIgnoreCase(action)) {
                strRole = request.getParameter("sentRole");
                if(strRole.equalsIgnoreCase("Secretary")){
                    String idType="tenderId";
                    int auditId=Integer.parseInt(request.getParameter("tenderid"));
                    String auditAction=request.getParameter("cmbStatus") +" Evaluation Report";
                    String moduleName=EgpModule.Evaluation.getName();
                    String remarks= request.getParameter("txtRemark");
                    boolean flag = insertForTblEvalRptForward(request);
                    if (flag) {
                        pageName = "officer/Evalclarify.jsp?tenderId=" + tenderid +"&st=rp&comType=TEC&msgAA=s";
                    } else {
                        pageName = "officer/SendReportAA.jsp?tenderid=" + tenderid + "&lotId=" + lotId + "&stat="+strEvalType+"&rId="+strRoundId+"&msg=error";
                    }
                } else{
                    boolean flag = insertForTblEvalRpt(request);
                    if (flag) {
                        pageName = "officer/Evalclarify.jsp?tenderId=" + tenderid +"&st=rp&comType=TEC&msgAA=s";
                    } else {
                        pageName = "officer/SendReportAA.jsp?tenderid=" + tenderid + "&lotId=" + lotId + "&msg=error";
                    }
                }
            }
            if ("updateReviewRpt".equalsIgnoreCase(action)) {
                // Setting Audit data.
                String idType="tenderId";
                int auditId=Integer.parseInt(request.getParameter("tenderid"));
                String auditAction="Review Evaluation Report";
                String moduleName=EgpModule.Evaluation.getName();
                String remarks= request.getParameter("txtRemark");

                boolean flag = updateReviewRpt(request, evaluationService,true);
                if (flag) {
                    flag = insertForTblEvalRpt(request);
                    if (flag) {
                        pageName = "officer/ReviewReportListing.jsp?msg=sucess&tenderid=" + tenderid + "&lotId=" + lotId;
                    } else {
                        flag = updateReviewRpt(request, evaluationService,false);
                        auditAction="Error in Forward Evaluation report to AA";
                        pageName = "officer/ReviewReportProcessing.jsp?tenderid=" + tenderid + "&lotId=" + lotId + "&msg=error&evalRptFFid="+request.getParameter("rptId");
                    }
                } else {
                    auditAction="Error in Forward Evaluation report to AA";
                    pageName = "officer/ReviewReportProcessing.jsp?tenderid=" + tenderid + "&lotId=" + lotId + "&msg=error&evalRptFFid="+request.getParameter("rptId");
                }
                session= request.getSession(true);
            }
            if ("updateAppRpt".equalsIgnoreCase(action)) {
                // Setting Audit data.
                String idType="tenderId";
                int auditId=Integer.parseInt(request.getParameter("tenderid"));
                String auditAction=request.getParameter("cmbStatus") +" Evaluation Report";
                String moduleName=EgpModule.Evaluation.getName();
                String remarks= request.getParameter("txtRemark");
               
                boolean flag = updateAppRpt(request, evaluationService);
                
                if (flag) 
                {
                    pageName = "officer/EvalReportListing.jsp?msg=sucess&tenderid=" + tenderid + "&lotId=" + lotId;
                }
                else
                {
                    auditAction="Error in Approve Oppening Date Extension Request";
                    pageName = "officer/ReportProcessing.jsp?tenderid=" + tenderid + "&lotId=" + lotId + "&msg=error&evalRptToAaid="+request.getParameter("rptId");
                }
                session= request.getSession(true);
               }
            if("notifyMail".equalsIgnoreCase(action))
            {
                boolean flag = false;
                
                // Setting Audit data.
                String idType="tenderId";
                int auditId=Integer.parseInt(request.getParameter("tenderid"));
                String auditAction="Notify TEC Members to sign "+request.getParameter("rp").toUpperCase();
                String moduleName=EgpModule.Evaluation.getName();
                String remarks= "";
                flag  = addEvalNotify(request,evaluationService);
                
                if(flag)
                {
                    pageName = "officer/Evalclarify.jsp?tenderId=" + tenderid +"&st=rp&comType=TEC&msgN=ns";
                }
                else
                {
                     auditAction="Error in "+auditAction;
                   pageName = "officer/Evalclarify.jsp?tenderId=" + tenderid +"&st=rp&comType=TEC&msgN=nu";
                }
                    makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                    
            }
            /*Dohatec Start*/
            if("notifyPE".equalsIgnoreCase(action))
            {
                boolean flag = false;
                // Setting Audit data.
                String idType="tenderId";
                int auditId=Integer.parseInt(request.getParameter("tenderid"));
                String auditAction="Notify PE to Create Workflow "+request.getParameter("rp").toUpperCase();
                String moduleName=EgpModule.Evaluation.getName();
                String remarks= request.getParameter("txtRemark");
                flag  = addEvalWorkFlowNotify(request,evaluationService,remarks);
                if(flag)
                {
                    pageName = "officer/Evalclarify.jsp?tenderId=" + tenderid +"&st=rp&comType=TEC&msgN=nps";
                }
                else
                {
                     auditAction="Error in "+auditAction;
                   pageName = "officer/Evalclarify.jsp?tenderId=" + tenderid +"&st=rp&comType=TEC&msgN=npu";
                }
                    makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
            }
            /*Dohatec end*/
            if (action.equals("seekClari")) {
                    if (seekClari(request, evaluationService)) {
                        pageName = "officer/EvalReportListing.jsp?tenderid=" + request.getParameter("tenderId") + "&msgSC=success";
                    }
            }
            if (action.equals("giveClari")) {
                if (giveClari(request, evaluationService)) {
                    pageName = "officer/Evalclarify.jsp?tenderId=" + request.getParameter("tenderId") + "&st=rp&comType=TEC&msgGC=success";
                }
            }
            if(!"".equalsIgnoreCase(pageName)){
                response.sendRedirect(pageName);
            }
           
        } finally {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /**
     * 
     * @param request
     * @return
     */
    private boolean insertForTblEvalRptForward(HttpServletRequest request) {
        boolean flag = false;
        int tenderId = Integer.parseInt(request.getParameter("tenderid"));
        String s_officer = request.getParameter("cmbName");
        String[] s_officerName = s_officer.split("_");
        TblEvalRptForwardToAa objTblEvalRptForwardToAa = new TblEvalRptForwardToAa();
        objTblEvalRptForwardToAa.setTblTenderMaster(new TblTenderMaster(tenderId));
        objTblEvalRptForwardToAa.setPkgLotId(Integer.parseInt(request.getParameter("lotId")));
        objTblEvalRptForwardToAa.setRptStatus("Pending");
        objTblEvalRptForwardToAa.setRoundId(Integer.parseInt(request.getParameter("rId")));

        objTblEvalRptForwardToAa.setSentByUserId(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
        objTblEvalRptForwardToAa.setSentByGovUserId(Integer.parseInt(request.getSession().getAttribute("govUserId").toString()));
        objTblEvalRptForwardToAa.setSentUserRemarks(request.getParameter("textComments"));
        objTblEvalRptForwardToAa.setSentDate(new Date());

        objTblEvalRptForwardToAa.setActionUserId(Integer.parseInt(s_officerName[0]));
        objTblEvalRptForwardToAa.setActionGovUserId(Integer.parseInt(s_officerName[1]));
        objTblEvalRptForwardToAa.setActionUserRole(request.getParameter("sentRole"));
        objTblEvalRptForwardToAa.setAction("");
        objTblEvalRptForwardToAa.setActionDt(new Date());
        objTblEvalRptForwardToAa.setActionUserRemarks("");

        EvaluationService evaluationService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
        flag = evaluationService.inserToTblEvalRptForward(objTblEvalRptForwardToAa);
        if(flag){
            flag = sendMsgForSendRport(tenderId+"",request.getParameter("lotId"),"SendReport"+objTblEvalRptForwardToAa.getActionUserRole(),s_officerName[0]);
        }
        return flag;
    }

    /**
     * 
     * @param request
     * @param evalService
     * @return
     */
    private boolean updateReviewRpt(HttpServletRequest request, EvaluationService evalService,boolean isSuccess) {
        boolean flag = false;
        TblEvalRptForwardToAa tblEvalRptForwardToAa = new TblEvalRptForwardToAa();
        int rptId = Integer.parseInt(request.getParameter("rptId"));
        int rId = Integer.parseInt(request.getParameter("rId"));
        String remark = request.getParameter("textComments");
        String status = (request.getParameter("txtRptStatus")!=null && !request.getParameter("txtRptStatus").trim().equalsIgnoreCase("f") && !"".equals(request.getParameter("txtRptStatus").trim())?"Pending":"Forward");
        if(!isSuccess){
            status = "Pending";
        }
        String role = request.getParameter("sentRole");
        tblEvalRptForwardToAa.setActionUserRole(role);
        tblEvalRptForwardToAa.setEvalFfrptId(rptId);
        tblEvalRptForwardToAa.setActionUserRemarks(remark);
        tblEvalRptForwardToAa.setRptStatus(status);
        tblEvalRptForwardToAa.setPkgLotId(Integer.parseInt(request.getParameter("lotId")));
        tblEvalRptForwardToAa.setTblTenderMaster(new TblTenderMaster(Integer.parseInt(request.getParameter("tenderid"))));
        tblEvalRptForwardToAa.setRoundId(rId);
        tblEvalRptForwardToAa.setActionDt(new Date());
        tblEvalRptForwardToAa.setAction("Forward by "+role);
        flag = evalService.updateEvalRptForwardToAa(tblEvalRptForwardToAa);
        if(flag){
            sendMsgBoxForEval(request.getParameter("tenderid"), request.getParameter("lotId"), request.getParameter("sUserId"), role, status);
        }
        return flag;
    }

    /**
     *
     * @param designation
     * @param tenderId
     * @return
     */

    private String getOfficerName(String designation, String tenderId) {
        StringBuilder sb = new StringBuilder();
        SPCommonSearch commonSearchData = (SPCommonSearch) AppContext.getSpringBean("SPCommonSearch");
        List<SPCommonSearchData> list_office = commonSearchData.executeProcedure("GetTenderAAHopeAo", designation.trim(), tenderId, null, null, null, null, null, null, null,null);
        if (list_office != null && !list_office.isEmpty()) {
            if ("AO".equalsIgnoreCase(designation) || "Secretary".equalsIgnoreCase(designation)) {
                sb.append("<select name='cmbName' id='cmbName' class='formTxtBox_1'>");
                sb.append("<option selected='selected' value=''>- Select Officer's Name -</option>");
                for (int i = 0; i < list_office.size(); i++) {
                    sb.append("<option value='"+list_office.get(i).getFieldName2() + "_" + list_office.get(i).getFieldName3() + "'>" + list_office.get(i).getFieldName1() + "</option>");
                }
                sb.append("</select>");
            } else {
                sb.append("<lable>" + list_office.get(0).getFieldName1() + "</lable>");
                sb.append("<input type='hidden' name='cmbName' id='cmbName'  value='" + list_office.get(0).getFieldName2() + "_" + list_office.get(0).getFieldName3() + "' />");
            }
        } else {
            sb.append("<lable>No Officer Found</lable>");
            sb.append("<input type='hidden' name='cmbName' id='cmbName' value='' />");
        }

        return sb.toString();
    }

    /**
     *
     * @param request
     * @return
     */
    private boolean insertForTblEvalRpt(HttpServletRequest request) {
        boolean flag = false;
        int tenderId = Integer.parseInt(request.getParameter("tenderid"));
        String s_officer = request.getParameter("cmbName");
        String[] s_officerName = s_officer.split("_");
        int rId = Integer.parseInt(request.getParameter("rId"));
        TblEvalRptSentToAa add_obj = new TblEvalRptSentToAa();
        add_obj.setTblTenderMaster(new TblTenderMaster(tenderId));
        add_obj.setEvalRptId(0);
        add_obj.setSentBy(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
        add_obj.setSentByGovUserId(Integer.parseInt(request.getSession().getAttribute("govUserId").toString()));
        add_obj.setPkgLotId(Integer.parseInt(request.getParameter("lotId")));
        add_obj.setRemarks(request.getParameter("textComments"));
        add_obj.setUserId(Integer.parseInt(s_officerName[0]));
        add_obj.setAaGovUserId(Integer.parseInt(s_officerName[1]));
        add_obj.setSentRole(request.getParameter("sentRole"));
        add_obj.setSentDate(new Date());
        add_obj.setRptStatus("Pending");
        add_obj.setAaRemarks("");
        add_obj.setRoundId(rId);
        add_obj.setEnvelopeId(Integer.parseInt(request.getParameter("rptMethod")));
        EvaluationService evaluationService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
        flag = evaluationService.inserToTblEvalRptSent(add_obj);
        if(flag){
            flag = sendMsgForSendRport(tenderId+"",request.getParameter("lotId"),"SendReportAA",s_officerName[0]);
        }
        return flag;
    }

    /**
     * for Giving clarification
     * @param request - all requests from previous page
     * @param evaluationService
     * @return true if gave clarification successfully else false
     */public boolean giveClari(HttpServletRequest request, EvaluationService evaluationService) {
        LOGGER.debug("giveClari : " + logUserId + LOGGERSTART);
        boolean flag = false;
        try {
            int answeredByGovUserId = 0;
            int answeredByUserId = 0;
            if (request.getSession().getAttribute("govUserId") != null) {
                answeredByGovUserId = Integer.parseInt(request.getSession().getAttribute("govUserId").toString());
            }
            if (request.getSession().getAttribute("userId") != null) {
                answeredByUserId = Integer.parseInt(request.getSession().getAttribute("userId").toString());
            }

            String tenderId = "";
            if (request.getParameter("tenderId") != null && !request.getParameter("tenderId").equals("")) {
                tenderId = request.getParameter("tenderId");
            }
            String refNo = "";
            if (request.getParameter("rfpNo") != null && !request.getParameter("rfpNo").equals("")) {
                refNo = request.getParameter("rfpNo");
            }
            String lotPkgNo = "";
            if (request.getParameter("lotPkgNo") != null && !request.getParameter("lotPkgNo").equals("")) {
                lotPkgNo = request.getParameter("lotPkgNo");
            }
            String lotPckNoTxt = "";
            if (request.getParameter("lotPckNoTxt") != null && !request.getParameter("lotPckNoTxt").equals("")) {
                lotPckNoTxt = request.getParameter("lotPckNoTxt");
            }
            String lotPkgDesc = "";
            if (request.getParameter("lotPkgDesc") != null && !request.getParameter("lotPkgDesc").equals("")) {
                lotPkgDesc = request.getParameter("lotPkgDesc");
            }
            String lotPckDescTxt = "";
            if (request.getParameter("lotPckDescTxt") != null && !request.getParameter("lotPckDescTxt").equals("")) {
                lotPckDescTxt = request.getParameter("lotPckDescTxt");
            }
            String lastDateTime = "";
            if (request.getParameter("lastDateTime") != null && !request.getParameter("lastDateTime").equals("")) {
                lastDateTime = request.getParameter("lastDateTime");
            }
            String answer = "";
            if (request.getParameter("response") != null && !request.getParameter("response").equals("")) {
                answer = request.getParameter("response");
            }
            int evalRptClariId = 0;
            if (request.getParameter("evalRptClariId") != null && !request.getParameter("evalRptClariId").equals("")) {
                evalRptClariId = Integer.parseInt(request.getParameter("evalRptClariId"));
            }
            int evalRptToAaid = 0;
            if (request.getParameter("evalRptToAaid") != null && !request.getParameter("evalRptToAaid").equals("")) {
                evalRptToAaid = Integer.parseInt(request.getParameter("evalRptToAaid"));
            }
            String rptStatus = "Pending";
            if(evaluationService.updateTblEvalRptSentToAA(evalRptToAaid,rptStatus)){
                if (evaluationService.updateEvalReportClarification(answer, answeredByUserId, answeredByGovUserId, evalRptClariId,tenderId)) {
                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    List<SPTenderCommonData> commonData = tenderCommonService.returndata("GetEmailGiveClari", request.getParameter("evalRptToAaid"), null);
                    if (!commonData.isEmpty()) {
                        String mails[] = {commonData.get(0).getFieldName1()};
                        if (sendMsgGiveClari(mails, tenderId, refNo, lotPkgNo, lotPckNoTxt, lotPkgDesc, lotPckDescTxt, lastDateTime)) {
                            flag = true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("giveClari : " + logUserId + e);
        }
        LOGGER.debug("giveClari : " + logUserId + LOGGEREND);
        return flag;
    }

     /**
      * seek clarification
      * @param request - all requests from previous page
      * @param evaluationService
      * @return true or false
      */
    public boolean seekClari(HttpServletRequest request, EvaluationService evaluationService) {
        LOGGER.debug("seekClari : " + logUserId + LOGGERSTART);
        boolean flag = false;
        String tenderId = "";
        String query = "";
        try {
            int queryPostedByGovUserId = 0;
            int queryPostedByUserId = 0;
            if (request.getSession().getAttribute("govUserId") != null) {
                queryPostedByGovUserId = Integer.parseInt(request.getSession().getAttribute("govUserId").toString());
            }
            if (request.getSession().getAttribute("userId") != null) {
                queryPostedByUserId = Integer.parseInt(request.getSession().getAttribute("userId").toString());
            }
            int evalRptToAaid = 0;
            if (request.getParameter("evalRptToAaid") != null && !request.getParameter("evalRptToAaid").equals("")) {
                evalRptToAaid = Integer.parseInt(request.getParameter("evalRptToAaid"));
            }
            String role = "";
            for (TblEvalRptSentToAa tblEvalRptSentToAa : evaluationService.fetchRoleSC(evalRptToAaid)) {
                role = tblEvalRptSentToAa.getSentRole();
            }
            int rId = 0;
            if (request.getParameter("rId") != null && !request.getParameter("rId").equals("")) {
                rId = Integer.parseInt(request.getParameter("rId"));
            }
            
            if (request.getParameter("query") != null && !request.getParameter("query").equals("")) {
                query = request.getParameter("query");
            }
            
            if (request.getParameter("tenderId") != null && !request.getParameter("tenderId").equals("")) {
                tenderId = request.getParameter("tenderId");
            }
            String refNo = "";
            if (request.getParameter("rfpNo") != null && !request.getParameter("rfpNo").equals("")) {
                refNo = request.getParameter("rfpNo");
            }
            String lotPkgNo = "";
            if (request.getParameter("lotPkgNo") != null && !request.getParameter("lotPkgNo").equals("")) {
                lotPkgNo = request.getParameter("lotPkgNo");
            }
            String lotPckNoTxt = "";
            if (request.getParameter("lotPckNoTxt") != null && !request.getParameter("lotPckNoTxt").equals("")) {
                lotPckNoTxt = request.getParameter("lotPckNoTxt");
            }
            String lotPkgDesc = "";
            if (request.getParameter("lotPkgDesc") != null && !request.getParameter("lotPkgDesc").equals("")) {
                lotPkgDesc = request.getParameter("lotPkgDesc");
            }
            String lotPckDescTxt = "";
            if (request.getParameter("lotPckDescTxt") != null && !request.getParameter("lotPckDescTxt").equals("")) {
                lotPckDescTxt = request.getParameter("lotPckDescTxt");
            }

            Date lastDateTime = null;
            if (request.getParameter("lastDateTime") != null && !request.getParameter("lastDateTime").equals("")) {
                lastDateTime = DateUtils.convertDateToStr(request.getParameter("lastDateTime"));
            }

            TblEvalReportClarification tblEvalReportClarification = new TblEvalReportClarification();
            TblEvalRptSentToAa tblEvalrtpSentToAA = new TblEvalRptSentToAa(evalRptToAaid);
            tblEvalrtpSentToAA.setTblTenderMaster(new TblTenderMaster(Integer.parseInt(tenderId)));
            tblEvalReportClarification.setTblEvalRptSentToAa(tblEvalrtpSentToAA);
            tblEvalReportClarification.setQuery(query);
            tblEvalReportClarification.setQueryDt(new Date());
            tblEvalReportClarification.setAnswer("");
            tblEvalReportClarification.setAnswerDt(null);
            tblEvalReportClarification.setQueryPostedByGovUserId(queryPostedByGovUserId);
            tblEvalReportClarification.setQueryPostedByUserId(queryPostedByUserId);
            tblEvalReportClarification.setAnsweredByGovUserId(0);
            tblEvalReportClarification.setAnsweredByUserId(0);
            tblEvalReportClarification.setLastResponseDate(lastDateTime);
            tblEvalReportClarification.setRoundId(rId);
            String rptStatus = "Seek Clarification";
            if(evaluationService.updateTblEvalRptSentToAA(evalRptToAaid,rptStatus)){
                if (evaluationService.insertEvalReportClarification(tblEvalReportClarification)) {
                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    List<SPTenderCommonData> commonData = tenderCommonService.returndata("EvalTECChairPerson", tenderId, null);
                    if (!commonData.isEmpty()) {
                        String mails[] = {commonData.get(0).getFieldName6()};
                        if (sendMsgSeekClari(mails, tenderId, refNo, lotPkgNo, lotPckNoTxt, lotPkgDesc, lotPckDescTxt, request.getParameter("lastDateTime"), role)) {
                            flag = true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("seekClari : " + logUserId + e);
        }
        LOGGER.debug("seekClari : " + logUserId + LOGGEREND);
        return flag;
    }

    /**
     * for sending mail to particular email id
     * @param mails - email id
     * @param tenderId from tbl_tenderMaster
     * @param refNo - reference number
     * @param lotPkgNo - lot or package number
     * @param lotPckNoTxt - lot or package content
     * @param lotPkgDesc - lot or package desc
     * @param lotPckDescTxt - lot or package desc content
     * @param lastDateNTime - last date n time
     * @param role
     * @return true if mail has been sent successfully
     */public boolean sendMsgSeekClari(String mails[], String tenderId, String refNo, String lotPkgNo, String lotPckNoTxt, String lotPkgDesc, String lotPckDescTxt, String lastDateNTime, String role) {
        LOGGER.debug("sendMsgSeekClari : " + logUserId + LOGGERSTART);
        boolean flag = false;
        try {
            UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            registerService.contentAdmMsgBox(mails[0], XMLReader.getMessage("emailIdNoReply"), "Clarification has been sought in Evaluation Report", msgBoxContentUtility.contentSeekClari(tenderId, refNo, lotPkgNo, lotPckNoTxt, lotPkgDesc, lotPckDescTxt, lastDateNTime, role));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            sendMessageUtil.setEmailTo(mails);
            MailContentUtility mailContentUtility = new MailContentUtility();
            String mailText = mailContentUtility.contentSeekClari(tenderId, refNo, lotPkgNo, lotPckNoTxt, lotPkgDesc, lotPckDescTxt, lastDateNTime, role);
            sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
            sendMessageUtil.setEmailSub("Clarification has been sought in Evaluation Report");
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            flag = true;
        } catch (Exception e) {
            LOGGER.error("sendMsgSeekClari : " + logUserId + e);
        }
        LOGGER.debug("sendMsgSeekClari : " + logUserId + LOGGEREND);
        return flag;
    }
    /**
     * sending mail
     * @param mails - mail id
     * @param tenderId tenderId from tbl_TenderMaster
     * @param refNo - reference number
     * @param lotPkgNo - lot or package number
     * @param lotPckNoTxt - lot or package text
     * @param lotPkgDesc - lot or package description
     * @param lotPckDescTxt - lot or package description text
     * @param lastDateNTime - last date and time
     * @return true if mail sent successfully else false
     */public boolean sendMsgGiveClari(String mails[], String tenderId, String refNo, String lotPkgNo, String lotPckNoTxt, String lotPkgDesc, String lotPckDescTxt, String lastDateNTime) {
        LOGGER.debug("sendMsgSeekClari : " + logUserId + LOGGERSTART);
        boolean flag = false;
        try {
            UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            registerService.contentAdmMsgBox(mails[0], XMLReader.getMessage("emailIdNoReply"), "Clarification for Evaluation Report has been given by Evaluation Committee Chairperson", msgBoxContentUtility.contentGiveClari(tenderId, refNo, lotPkgNo, lotPckNoTxt, lotPkgDesc, lotPckDescTxt, lastDateNTime));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            sendMessageUtil.setEmailTo(mails);
            MailContentUtility mailContentUtility = new MailContentUtility();
            String mailText = mailContentUtility.contentGiveClari(tenderId, refNo, lotPkgNo, lotPckNoTxt, lotPkgDesc, lotPckDescTxt, lastDateNTime);
            sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
            sendMessageUtil.setEmailSub("Clarification for Evaluation Report has been given by Evaluation Committee Chairperson");
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            flag = true;
        } catch (Exception e) {
            LOGGER.error("sendMsgSeekClari : " + logUserId + e);
        }
        LOGGER.debug("sendMsgSeekClari : " + logUserId + LOGGEREND);
        return flag;
    }

    private boolean updateAppRpt(HttpServletRequest request, EvaluationService evalService) {
        boolean flag = false;
        TblEvalRptSentToAa tblEvalRptSentToAa = new TblEvalRptSentToAa();
        int rptId = Integer.parseInt(request.getParameter("rptId"));
        int rId = Integer.parseInt(request.getParameter("rId"));
        String remark = request.getParameter("txtRemark");
        String status = request.getParameter("cmbStatus");
        if("Rejected".equalsIgnoreCase(status)){
            String reTender = request.getParameter("reTendering");
            if("Yes".equalsIgnoreCase(reTender)){
                status = "Rejected / Re-Tendering";
            }
        }
        String role = request.getParameter("role");
        tblEvalRptSentToAa.setEvalRptToAaid(rptId);
        tblEvalRptSentToAa.setAaRemarks(remark);
        tblEvalRptSentToAa.setRptStatus(status);
        tblEvalRptSentToAa.setPkgLotId(Integer.parseInt(request.getParameter("lotId")));
        tblEvalRptSentToAa.setTblTenderMaster(new TblTenderMaster(Integer.parseInt(request.getParameter("tenderid"))));
        tblEvalRptSentToAa.setRoundId(rId);
        tblEvalRptSentToAa.setRptApproveDt(new Date());
        if(!status.equals("Re-evaluation")){
            flag = evalService.updateEvalSentRptToAa(tblEvalRptSentToAa);
        }
        else{
            CommonMsgChk commonMsgChk;
            AddUpdateOpeningEvaluation addUpdate = (AddUpdateOpeningEvaluation) AppContext.getSpringBean("AddUpdateOpeningEvaluation");
            commonMsgChk = addUpdate.addUpdOpeningEvaluation("ReevaluationByHOPE",request.getParameter("tenderid").toString(),status,remark,String.valueOf(rptId),"","","","","","","","","","","","","","","").get(0);
            flag = commonMsgChk.getFlag();
        }
        if(flag){
            sendMsgBoxForEval(request.getParameter("tenderid"), request.getParameter("lotId"), request.getParameter("sUserId"), role, status);
        }
        return flag;
    }
    
    public boolean sendMsgForSendRport(String tenderId,String lotId, String method,String userId){
        boolean flag = false;
        String s_type = "Lot";
        if("0".equalsIgnoreCase(lotId)){
            s_type = "Package";
        }
        EvaluationService evalService = (EvaluationService)AppContext.getSpringBean("EvaluationService");
        Object[] obj = evalService.getEmailMobileOfAA(Integer.parseInt(userId));
        String emailTo[] = {obj[0].toString()};
        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        List<SPTenderCommonData> commonData = tenderCommonService.returndata("GetLotPkgDetail", tenderId, lotId);
        if(!commonData.isEmpty()){
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            String mailConte = "";
            if("SendReportAA".equalsIgnoreCase(method)){
            mailConte = mailContentUtility.SendReportAA(tenderId, commonData.get(0).getFieldName3(), commonData.get(0).getFieldName1(), commonData.get(0).getFieldName2(), s_type);
            }
            sendMessageUtil.setEmailTo(emailTo);
            sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
            sendMessageUtil.setEmailSub("e-GP System:  Approval of Evaluation Report");
            sendMessageUtil.setEmailMessage(mailConte);
            sendMessageUtil.sendEmail();
            sendMessageUtil.setSmsNo("+975"+obj[1].toString());
            sendMessageUtil.setSmsBody("Dear User,%0A Approval of Evaluation Report for below mentioned tender: Tender ID: "+tenderId+".%0Ae-GP User Registration Desk");
            sendMessageUtil.sendSMS();
            flag = true;
            if(flag){
                UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                registerService.contentAdmMsgBox(emailTo[0], XMLReader.getMessage("emailIdNoReply"), "Approval of Evaluation Report", msgBoxContentUtility.SendReportAA(tenderId, commonData.get(0).getFieldName3(), commonData.get(0).getFieldName1(), commonData.get(0).getFieldName2(), s_type));
                flag = true;
            }
        }
        return flag;
    }
    private boolean sendMsgBoxForEval(String tenderId, String lotId, String userId, String role, String method){
        boolean flag = false;
        String s_type = "Lot";
        if("0".equalsIgnoreCase(lotId)){
            s_type = "Package";
        }
        
        SendMessageUtil sendMessageUtil = new SendMessageUtil();
        MailContentUtility mailContentUtility = new MailContentUtility();
        CommonSearchDataMoreService commonService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
        List<SPCommonSearchDataMore> mails = new ArrayList<SPCommonSearchDataMore>();
        
        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        List<SPTenderCommonData> commonData = tenderCommonService.returndata("GetLotPkgDetail", tenderId, lotId);
        UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
        MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
        boolean isAAMinister = false;
        CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
        List<SPCommonSearchDataMore> lstChkUserType = dataMore.geteGPData("getTenderAAMinister", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        if(lstChkUserType !=null && !lstChkUserType.isEmpty() && Integer.parseInt(lstChkUserType.get(0).getFieldName1()) > 0){
            isAAMinister = true;
        }
                if("Rejected / Re-Tendering".equalsIgnoreCase(method) || "Rejected".equalsIgnoreCase(method)){
                    if(isAAMinister){
                        mails = commonService.geteGPData("getSendMAILForMinisterReject", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    }else{
                        mails = commonService.geteGPData("getCpAndPe", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    }
                    String mailConte = "";
                    int i=0;
                        for (SPCommonSearchDataMore mailId : mails) {
                           String s_mailId[] = {mailId.getFieldName1()};
                           if("Rejected".equalsIgnoreCase(method)){
                               registerService.contentAdmMsgBox(mailId.getFieldName1(), XMLReader.getMessage("emailIdNoReply"), role+" has rejected the Evaluation Report", msgBoxContentUtility.aARejectRpt(tenderId, commonData.get(0).getFieldName3(), commonData.get(0).getFieldName1(), commonData.get(0).getFieldName2(), s_type,role));
                               sendMessageUtil.setEmailSub("e-GP System:  "+role+" has rejected the Evaluation Report");
                               mailConte = mailContentUtility.aARejectRpt(tenderId, commonData.get(0).getFieldName3(), commonData.get(0).getFieldName1(), commonData.get(0).getFieldName2(), s_type,role);
                           }else{
                             registerService.contentAdmMsgBox(mailId.getFieldName1(), XMLReader.getMessage("emailIdNoReply"), "Recommended for Re-tendering", msgBoxContentUtility.aAReTenderRpt(tenderId, commonData.get(0).getFieldName3(), commonData.get(0).getFieldName1(), commonData.get(0).getFieldName2(), s_type,role));
                             sendMessageUtil.setEmailSub("e-GP System:  Recommended for Re-tendering");
                             mailConte = mailContentUtility.aAReTenderRpt(tenderId, commonData.get(0).getFieldName3(), commonData.get(0).getFieldName1(), commonData.get(0).getFieldName2(), s_type,role);
                           }
                            if(mailId!=null){
                                mailId = null;
                            }
                        sendMessageUtil.setEmailTo(s_mailId);
                        sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                        sendMessageUtil.setEmailMessage(mailConte);
                        sendMessageUtil.sendEmail();
                        }
                        
                flag = true;
                }if("Approved".equalsIgnoreCase(method)){
                    String mailConte  = null;
                    if(isAAMinister){
                        mails = commonService.geteGPData("getSendMAILForMinisterApprove", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    }else{
                        mails = commonService.geteGPData("getTecAndPe", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    }
                        for (SPCommonSearchDataMore mailId : mails) {
                             String s_mailId[] = {mailId.getFieldName1()};
                             registerService.contentAdmMsgBox(mailId.getFieldName1(), XMLReader.getMessage("emailIdNoReply"), "Approval of Evaluation Report", msgBoxContentUtility.aAApproveRpt(tenderId, commonData.get(0).getFieldName3(), commonData.get(0).getFieldName1(), commonData.get(0).getFieldName2(), s_type,role));
                            if(mailId!=null){
                                mailId = null;
                            }
                            sendMessageUtil.setEmailTo(s_mailId);
                            sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                            sendMessageUtil.setEmailSub("e-GP System:  Approval of Evaluation Report");
                            mailConte = mailContentUtility.aAApproveRpt(tenderId, commonData.get(0).getFieldName3(), commonData.get(0).getFieldName1(), commonData.get(0).getFieldName2(), s_type,role);
                            sendMessageUtil.setEmailMessage(mailConte);
                            sendMessageUtil.sendEmail();
                        }
                        
                        mails.clear();
                        
                        mails = commonService.geteGPData("getRejectedTendererEmail", tenderId);
                        for (SPCommonSearchDataMore mailId : mails) {
                            String s_mailId[] = {mailId.getFieldName1()};
                             registerService.contentAdmMsgBox(mailId.getFieldName1(), XMLReader.getMessage("emailIdNoReply"), "Unsuccessful of your Tender", msgBoxContentUtility.aAApproveRptTend(tenderId, commonData.get(0).getFieldName3(), commonData.get(0).getFieldName1(), commonData.get(0).getFieldName2(), s_type,role));
                            if(mailId!=null){
                                mailId = null;
                            }
                            mailConte  = null;
                            sendMessageUtil.setEmailTo(s_mailId);
                            sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                            sendMessageUtil.setEmailSub("e-GP System:  Unsuccessful of your Tender");
                            mailConte = mailContentUtility.aAApproveRptTend(tenderId, commonData.get(0).getFieldName3(), commonData.get(0).getFieldName1(), commonData.get(0).getFieldName2(), s_type,role);
                            sendMessageUtil.setEmailMessage(mailConte);
                            sendMessageUtil.sendEmail();
                        }
                        
                        mails.clear();
                        mails = commonService.geteGPData("getSucTenderEvalRpt", tenderId);
                        if(mails!=null && !mails.isEmpty()){
                            if("REOI".equalsIgnoreCase(mails.get(0).getFieldName4())){
                                for (SPCommonSearchDataMore mailId : mails) {
                                    String mailIdarr[] = {mailId.getFieldName1()};
                                    registerService.contentAdmMsgBox(mailId.getFieldName1(), XMLReader.getMessage("emailIdNoReply"), "e-GP: You have been shortlisted in REOI", msgBoxContentUtility.aAApproveRptSucessTen(tenderId, commonData.get(0).getFieldName3(),mailId.getFieldName2(),mailId.getFieldName3()));
                                    mailConte  = null;
                                    sendMessageUtil.setEmailTo(mailIdarr);
                                    sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                                    sendMessageUtil.setEmailSub("e-GP System:  You have been shortlisted in REOI");
                                    mailConte = mailContentUtility.aAApproveRptSucessTen(tenderId, commonData.get(0).getFieldName3(),mailId.getFieldName2(),mailId.getFieldName3());
                                    sendMessageUtil.setEmailMessage(mailConte);
                                    sendMessageUtil.sendEmail();
                                    if(mailId!=null){
                                        mailId = null;
                                    }
                                }   
                            }
                        }
                        flag = true;
                }
                if("notiyFyMail".equalsIgnoreCase(method)){
                    mails = commonService.geteGPData("getTecMember", tenderId);
                        String rptLab = role;
                        rptLab  = (lotId.equals("0") ? rptLab.replaceFirst("t", "p") : rptLab);
                        for (SPCommonSearchDataMore mailId : mails) {
                            String s_mailId[] = {mailId.getFieldName1()};
                             registerService.contentAdmMsgBox(mailId.getFieldName1(), XMLReader.getMessage("emailIdNoReply"), "Sign Evaluation Report", msgBoxContentUtility.notifyTECMember(tenderId, commonData.get(0).getFieldName3(), commonData.get(0).getFieldName1(), commonData.get(0).getFieldName2(),s_type,rptLab));
                            if(mailId!=null){
                                mailId = null;
                            }
                            sendMessageUtil.setEmailTo(s_mailId);
                            sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                            sendMessageUtil.setEmailSub("e-GP System:  Sign Evaluation Report");
                            String mailConte = mailContentUtility.notifyTECMember(tenderId, commonData.get(0).getFieldName3(), commonData.get(0).getFieldName1(), commonData.get(0).getFieldName2(),s_type,rptLab);
                            sendMessageUtil.setEmailMessage(mailConte);
                            sendMessageUtil.sendEmail();
                        }
                        
                       
                        flag = true;
                }

        return flag;
    }
    /* Dohatec Start */
      private boolean sendMsgBoxForEvalPE(String tenderId, String lotId, String userId, String role, String method, String remarks){
        boolean flag = false;
        String s_type = "Lot";
        if("0".equalsIgnoreCase(lotId)){
            s_type = "Package";
        }

        SendMessageUtil sendMessageUtil = new SendMessageUtil();
        MailContentUtility mailContentUtility = new MailContentUtility();
        CommonSearchDataMoreService commonService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
        List<SPCommonSearchDataMore> mails = new ArrayList<SPCommonSearchDataMore>();

        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        List<SPTenderCommonData> commonData = tenderCommonService.returndata("GetLotPkgDetail", tenderId, lotId);
        UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
        MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();

                /*Dohatec Start*/
                if("notiyFyPE".equalsIgnoreCase(method)){
                    mails = commonService.geteGPData("getPe", tenderId);
                        String rptLab = role;
                        rptLab  = (lotId.equals("0") ? rptLab.replaceFirst("t", "p") : rptLab);
                        for (SPCommonSearchDataMore mailId : mails) {
                            String s_mailId[] = {mailId.getFieldName1()};
                            sendSMS(mailId.getFieldName1(),tenderId);
                             registerService.contentAdmMsgBox(mailId.getFieldName1(), XMLReader.getMessage("emailIdNoReply"), "Create Contract Approval Workflow", msgBoxContentUtility.notifyPE(tenderId, commonData.get(0).getFieldName3(), commonData.get(0).getFieldName1(), commonData.get(0).getFieldName2(),s_type,remarks));
                            if(mailId!=null){
                                mailId = null;
                            }
                            sendMessageUtil.setEmailTo(s_mailId);
                            sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                            sendMessageUtil.setEmailSub("e-GP System:  Create Contract Approval Workflow");
                            String mailConte = mailContentUtility.notifyPE(tenderId, commonData.get(0).getFieldName3(), commonData.get(0).getFieldName1(), commonData.get(0).getFieldName2(),s_type,remarks);
                            sendMessageUtil.setEmailMessage(mailConte);
                            sendMessageUtil.sendEmail();
                        }
                        flag = true;
                }
        return flag;
    }
    /* Dohatec End */

    public boolean addEvalNotify(HttpServletRequest request, EvaluationService evaluationService){
        boolean flag = false;
        int tenderId = 0;
        int lotId = 0;
        int userId = 0;
        int rId= 0;
        String rpType = "";
        if(request.getParameter("tenderid")!=null)
         tenderId = Integer.parseInt(request.getParameter("tenderid"));
        if(request.getParameter("lotId")!=null){
            lotId = Integer.parseInt(request.getParameter("lotId"));
        }
        if(request.getParameter("rId")!=null){
            rId = Integer.parseInt(request.getParameter("rId"));
        }
        if(request.getParameter("rp")!=null){
            rpType = request.getParameter("rp");
        }
        if(request.getSession().getAttribute("userId")!=null){
            userId = Integer.parseInt(request.getSession().getAttribute("userId").toString());
        }
        
        TblEvalTernotifyMember tblEvalTernotifyMember = new TblEvalTernotifyMember();
        tblEvalTernotifyMember.setNotifyType("notify");
        tblEvalTernotifyMember.setPkgLotId(lotId);
        tblEvalTernotifyMember.setTenderId(tenderId);
        tblEvalTernotifyMember.setSentBy(userId);
        tblEvalTernotifyMember.setSentDt(new Date());
        tblEvalTernotifyMember.setReportType(rpType);
        tblEvalTernotifyMember.setRoundId(rId);
        
        flag = evaluationService.addEvalNotify(tblEvalTernotifyMember);
        if(flag){
            sendMsgBoxForEval(tenderId+"", lotId+"",userId+"", rpType, "notiyFyMail");
        }
        return flag;
    }
/*Dohatec Start*/
     public boolean addEvalWorkFlowNotify(HttpServletRequest request, EvaluationService evaluationService,String remarks){
        boolean flag = false;
        int tenderId = 0;
        int lotId = 0;
        int userId = 0;
        int rId= 0;
        String rpType = "wf";
        if(request.getParameter("tenderid")!=null)
         tenderId = Integer.parseInt(request.getParameter("tenderid"));
        if(request.getParameter("lotId")!=null){
            lotId = Integer.parseInt(request.getParameter("lotId"));
        }
        if(request.getParameter("rId")!=null){
            rId = Integer.parseInt(request.getParameter("rId"));
        }
        if(request.getSession().getAttribute("userId")!=null){
            userId = Integer.parseInt(request.getSession().getAttribute("userId").toString());
        }
        TblEvalTernotifyMember tblEvalTernotifyMember = new TblEvalTernotifyMember();
        tblEvalTernotifyMember.setNotifyType("notify");
        tblEvalTernotifyMember.setPkgLotId(lotId);
        tblEvalTernotifyMember.setTenderId(tenderId);
        tblEvalTernotifyMember.setSentBy(userId);
        tblEvalTernotifyMember.setSentDt(new Date());
        tblEvalTernotifyMember.setReportType(rpType);
        tblEvalTernotifyMember.setRoundId(rId);
        flag = evaluationService.addEvalNotify(tblEvalTernotifyMember);
        if(flag){
            sendMsgBoxForEvalPE(tenderId+"", lotId+"",userId+"", rpType, "notiyFyPE",remarks);
        }
        return flag;
    }

        /**
     * for sending sms
     * @param mailId
     * @param tenderId of Tbl_TenderMaster
     * @return true or false
     */
    private boolean sendSMS(String mailId, String tenderId) {
        UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService"); //Dohatec Added
        LOGGER.debug("sendSMS : " + logUserId + " Starts");
        boolean smsSent = false;
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
            sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(mailId)));
            StringBuilder sb = new StringBuilder();
            sb.append("Dear User,%0AYou have been notified for creating contract approval workflow ");
            sb.append("for tender id " + tenderId );
            sb.append("%0AThanks,%0AeGP System");
            sendMessageUtil.setSmsBody(sb.toString());
            try {
                sendMessageUtil.sendSMS();
            } catch (Exception e) {
                LOGGER.error(" sendSMS : error in send sms : " + logUserId + " : " + e);
            }
            smsSent = true;
        } catch (Exception ex) {
            LOGGER.error("sendSMS : " + logUserId + " : " + ex);
        }
        LOGGER.debug("sendSMS : " + logUserId + " Ends");
        return smsSent;
    }


     /*Dohatec End*/
}