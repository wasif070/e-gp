/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.databean;

import com.cptu.egp.eps.model.table.TblGradeMaster;
import com.cptu.egp.eps.service.serviceinterface.CreateGradeService;
import com.cptu.egp.eps.web.utility.AppContext;

/**
 * <b>GradeDtBean</b> Nov 23, 2010 11:29:34 AM
 * DataBean for Grades
 * @author Malhar
 */
public class GradeDtBean {

    private short gradeId;
    private String gradeName;
    private int gradeLevel;
    private boolean dataExists=false;

    public boolean isDataExists()
    {
        return dataExists;
    }

    public void setDataExists(boolean dataExists)
    {
        this.dataExists = dataExists;
    }

    public short getGradeId()
    {
        return gradeId;
    }

    public void setGradeId(short gradeId)
    {
        this.gradeId = gradeId;
    }

    public int getGradeLevel()
    {
        return gradeLevel;
    }

    public void setGradeLevel(int gradeLevel)
    {
        this.gradeLevel = gradeLevel;
    }

    public String getGradeName()
    {
        return gradeName;
    }

    public void setGradeName(String gradeName)
    {
        this.gradeName = gradeName;
    }

    public void populateInfo(short gradeId)
    {
        this.setGradeId(gradeId);
        CreateGradeService gradeService=(CreateGradeService) AppContext.getSpringBean("CreateGradeService");
        TblGradeMaster gradeMaster=gradeService.getGradeDetails(gradeId);
        if(gradeMaster!=null)
        {
            this.setGradeName(gradeMaster.getGrade());
            this.setGradeLevel(gradeMaster.getGradeLevel());
            this.setDataExists(true);
        }
    }
}
