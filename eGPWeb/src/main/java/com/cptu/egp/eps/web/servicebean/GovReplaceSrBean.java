/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.service.serviceimpl.TransferEmployeeServiceImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class GovReplaceSrBean {

    private TransferEmployeeServiceImpl transferEmployeeServiceImpl = (TransferEmployeeServiceImpl) AppContext.getSpringBean("TransferEmployeeServiceImpl");

    final static Logger LOGGER = Logger.getLogger(GovReplaceSrBean.class);

    private String logUserId = "0";
    private String start = " Starts";
    private String end = " Ends";

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
        transferEmployeeServiceImpl.setLogUserId(logUserId);
    }
// Code Added by Palash, Dohatec
    public List<Object[]> getDetailsExistingUser(int employeeId, int userId){
        List<Object[]> list = null;
        LOGGER.debug("getDetailsExistingUser : " + logUserId + start);
        try {
            // Code Added by Palash, Dohatec
            list = transferEmployeeServiceImpl.getDetailsExistingUser(employeeId, userId);
        //End, Palash
        } catch (Exception e) {
            LOGGER.error("getDetailsExistingUser : " + logUserId + e);
        }
        LOGGER.debug("getDetailsExistingUser : " + logUserId + end);
        return list;
    }
    public List<Object> getDetailsExistingUserOffice(int employeeId){
        List<Object> list = null;
        LOGGER.debug("getDetailsExistingUser : " + logUserId + start);
        try {
            list = transferEmployeeServiceImpl.getDetailsExistingUserOffice(employeeId);
        } catch (Exception e) {
            LOGGER.error("getDetailsExistingUser : " + logUserId + e);
        }
        LOGGER.debug("getDetailsExistingUser : " + logUserId + end);
        return list;
    }
    public List<Object> getDetailsExistingUserPR(int employeeId){
        List<Object> list = null;
        LOGGER.debug("getDetailsExistingUser : " + logUserId + start);
        try {
            list = transferEmployeeServiceImpl.getDetailsExistingUserPR(employeeId);
        } catch (Exception e) {
            LOGGER.error("getDetailsExistingUser : " + logUserId + e);
        }
        LOGGER.debug("getDetailsExistingUser : " + logUserId + end);
        return list;
    }
}
