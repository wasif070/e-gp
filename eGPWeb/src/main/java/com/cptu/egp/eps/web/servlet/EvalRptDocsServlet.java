/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.model.table.TblEvalReportDocs;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.service.serviceimpl.EvaluationService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CheckExtension;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author nishit
 */
public class EvalRptDocsServlet extends HttpServlet {private static final String TMP_DIR_PATH = "c:\\tmp";
    private File tmpDir;
    private static final String DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("ClarificationRefDocServlet");
    private File destinationDir;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        tmpDir = new File(TMP_DIR_PATH);
        if (!tmpDir.isDirectory()) {
            tmpDir.mkdir();
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();

        int tenderId = 0;
        File file = null;
        String docSizeMsg = "";
        boolean checkret = false;
        boolean flag = false;
        String documentBrief = "";
        String fileName = "";
        long fileSize = 0;
        String queryString = "";
        String pageName = "officer/EvalRptDocUpload.jsp";
        response.setContentType("text/html");
        int lotId = 0;
        int rId = 0;
        boolean dis = false;
        EvaluationService evaluationService  = (EvaluationService)AppContext.getSpringBean("EvaluationService");
        if (session.getAttribute("userId") == null) {
            response.sendRedirect("SessionTimedOut.jsp");
        } else {
            try {
                try {
                    if ((request.getParameter("funName") != null) && request.getParameter("funName").equalsIgnoreCase("docTable")) {
                        PrintWriter out = response.getWriter();
                        String s_rId = request.getParameter("rId");
                        s_rId = getDocTable(request.getParameter("tenderid"),request.getParameter("lotId"),s_rId,request.getContextPath(),evaluationService);
                        out.print(s_rId);
                        out.flush();
                        out.close();
                    }
                    if ((request.getParameter("funName") != null) && request.getParameter("funName").equalsIgnoreCase("docReviewTable")) {
                        PrintWriter out = response.getWriter();
                        String s_rId = request.getParameter("rId");
                        int userId = Integer.parseInt(session.getAttribute("userId").toString());
                        s_rId = getReviewDocTable(request.getParameter("tenderid"),request.getParameter("lotId"),s_rId,request.getContextPath(),evaluationService,userId);
                        out.print(s_rId);
                        out.flush();
                        out.close();
                    }
                    if ((request.getParameter("funName") != null) && request.getParameter("funName").equalsIgnoreCase("docDelete")) {
                        PrintWriter out = response.getWriter();
                        int docId = 0;
                        String id = "";
                        String docName = "";
                        if (request.getParameter("docId") != null) {
                            docId = Integer.parseInt(request.getParameter("docId"));

                        }
                        if (request.getParameter("lotId") != null) {
                            lotId = Integer.parseInt(request.getParameter("lotId"));

                        }
                        if (request.getParameter("docName") != null) {
                            docName = request.getParameter("docName");
                        }
                         if (request.getParameter("tenderid") != null) {
                            tenderId = Integer.parseInt(request.getParameter("tenderid"));
                        }
                         if (request.getParameter("rId") != null) {
                            rId = Integer.parseInt(request.getParameter("rId"));
                        }
                         rId = delDocs(tenderId, lotId, docName, docId, evaluationService);
                        out.print(rId);
                        out.flush();
                        out.close();
                    }
                        if ((request.getParameter("funName") != null) && request.getParameter("funName").equalsIgnoreCase("remove")) {
                                
                                int docId = 0;
                                String id = "";
                                String docName = "";
                                if (request.getParameter("docId") != null) {
                                    docId = Integer.parseInt(request.getParameter("docId"));

                                }
                                if (request.getParameter("lotId") != null) {
                                    lotId = Integer.parseInt(request.getParameter("lotId"));

                                }
                                if (request.getParameter("docName") != null) {
                                    docName = request.getParameter("docName");
                                }
                                 if (request.getParameter("tenderid") != null) {
                                    tenderId = Integer.parseInt(request.getParameter("tenderid"));
                                }
                                 if (request.getParameter("rId") != null) {
                                    rId = Integer.parseInt(request.getParameter("rId"));
                                }
                                fileName = DESTINATION_DIR_PATH + tenderId+"_"+lotId + "\\" + docName;
                                checkret = deleteFile(fileName);
                                if (checkret) {
                                    int i_cnt = evaluationService.removeUploadDoc(docId);
                                    if(i_cnt>0){
                                    queryString = "?lotId=" + lotId+"&tenderid="+tenderId+"&msg=re&rId="+rId;
                                    id="&msg=re";
                                    }else{
                                        queryString = "?lotId=" + lotId+"&tenderid="+tenderId+"&msg=reerror&rId="+rId;
                                        id="&msg=reerror";
                                    }
                                }else{
                                    queryString = "?lotId=" + lotId+"&tenderid="+tenderId+"&msg=reerror&rId="+rId;
                                    id="&msg=reerror";
                                }
                                if(request.getHeader("Referer")!=null){
                                    response.sendRedirect(request.getHeader("Referer")+id);
                                }else{
                                    response.sendRedirect(pageName + queryString);
                                }
                            } else if ((request.getParameter("funName") != null) && request.getParameter("funName").equalsIgnoreCase("download")) {
                                file = new File(DESTINATION_DIR_PATH + request.getParameter("tenderid")+"_"+ request.getParameter("lotId") +"\\" + request.getParameter("docName"));//request.getSession().getAttribute("userId")
                                InputStream fis = new FileInputStream(file);
                                byte[] buf = new byte[(int)file.length()];
                                int offset = 0;
                                int numRead = 0;
                                while ((offset < buf.length)
                                        && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {

                                    offset += numRead;

                                }
                                fis.close();
                                response.setContentType("application/octet-stream");
                                response.setHeader("Content-Disposition", "attachment;filename=\"" + file.getName() + "\"");
                                ServletOutputStream outputStream = response.getOutputStream();
                                outputStream.write(buf);
                                outputStream.flush();
                                outputStream.close();
                            } else {
                                DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
                                /*
                                 *Set the size threshold, above which content will be stored on disk.
                                 */
                                fileItemFactory.setSizeThreshold(4 * 1024 * 1024); //1 MB
		/*
                                 * Set the temporary directory to store the uploaded files of size above threshold.
                                 */
                                fileItemFactory.setRepository(tmpDir);

                                ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
                                /*
                                 * Parse the request
                                 */
                                List items = uploadHandler.parseRequest(request);
                                Iterator itr = items.iterator();
                                //For Supporting Document
                                while (itr.hasNext()) {
                                    FileItem item = (FileItem) itr.next();
                                    //For Supporting Document
                    /*
                                     * Handle Form Fields.
                                     */
                                    if (item.isFormField()) {

                                        if (item.getFieldName().equals("documentBrief")) {
                                            if (item.getString() == null || item.getString().trim().length() == 0) {
                                                dis = true;
                                                break;
                                            }

                                            documentBrief = item.getString();

                                        } else if (item.getFieldName().equals("lotId")) {
                                            lotId = Integer.parseInt(item.getString());

                                        }else if (item.getFieldName().equals("tenderid")) {
                                            tenderId = Integer.parseInt(item.getString());
                                        }
                                        else if (item.getFieldName().equals("roundId")) {
                                            rId = Integer.parseInt(item.getString());
                                        }
                                    } else {
                                        if (item.getName().lastIndexOf("\\") != -1) {
                                            fileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1, item.getName().length());
                                        } else {
                                            fileName = item.getName();
                                        }
                                        //fileName  = fileName.replaceAll(" ", "");
                                        String realPath = DESTINATION_DIR_PATH + tenderId+"_"+lotId;//request.getSession().getAttribute("userId")
                                        destinationDir = new File(realPath);
                                        if (!destinationDir.isDirectory()) {
                                            destinationDir.mkdir();
                                        }
                                        docSizeMsg = docSizeMsg(Integer.parseInt(request.getSession().getAttribute("userTypeId").toString()));
                                        // docSizeMsg = docSizeMsg(1);//userID.
                                        if (!docSizeMsg.equals("ok")) {
                                            //response.sendRedirect("SupportingDocuments.jsp?fq="+docSizeMsg);
                                        } else {
                                            fileSize = item.getSize();
                                            checkret = checkExnAndSize(fileName, item.getSize(), "officer");

                                            if (!checkret) {

                                                break;
                                            } else {
                                                file = new File(destinationDir, fileName);
                                                if (file.isFile()) {
                                                    flag = true;
                                                    break;
                                                }
                                                item.write(file);
                                            }
                                        }
                                    }

                                }
                            }
                } catch (FileUploadException ex) {
                    log("Error encountered while parsing the request", ex);
                } catch (Exception ex) {
                    log("Error encountered while uploading file", ex);
                }


                if (!docSizeMsg.equals("ok")) {
                    queryString = "?fq=" + docSizeMsg;

                } else {
                    if (dis) {
                        docSizeMsg = "File already Exists";
                        queryString = "?fq=" + docSizeMsg + "&lotId=" + lotId+"&tenderid="+tenderId+"&rId="+rId;
                        
                    } else {
                        if (flag) {
                            docSizeMsg = "File already Exists";
                            queryString = "?fq=" + docSizeMsg + "&lotId=" + lotId+"&tenderid="+tenderId+"&rId="+rId;
                        } else {

                            if (!checkret) {
                                CheckExtension ext = new CheckExtension();
                                TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("officer");
                                queryString = "?fs=" + configurationMaster.getFileSize() + "&ft=" + configurationMaster.getAllowedExtension()+"&lotId="+lotId+"&tenderid="+tenderId+"&rId="+rId;

                            } else {
                                if (documentBrief != null && documentBrief.trim().length() != 0) {
                                    TblEvalReportDocs add_obj = new TblEvalReportDocs();
                                    add_obj.setDocDescription(documentBrief);
                                    add_obj.setDocSize(fileSize+"");
                                    add_obj.setDocumentName(fileName);
                                    add_obj.setUploadedBy(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
                                    add_obj.setPkgLotId(lotId);
                                    add_obj.setTblTenderMaster(new TblTenderMaster(tenderId));
                                    add_obj.setUploadedDt(new Date());
                                    add_obj.setRoundId(rId);
                                    flag = evaluationService.addUploadDoc(add_obj);
                                    if(flag){
                                        queryString = "?lotId=" + lotId+"&tenderid="+tenderId+"&msg=up&rId="+rId;
                                    }else{
                                        queryString = "?lotId=" + lotId+"&tenderid="+tenderId+"&msg=uperror&rId="+rId;
                                    }

                                }
                            }

                        }
                    }
                    response.sendRedirect(pageName + queryString);
                }
            } finally {
                //   out.close();
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public boolean checkExnAndSize(String extn, long size, String userType) {
        boolean chextn = false;
        float fsize = 0.0f;
        float dsize = 0.0f;
        int j = extn.lastIndexOf('.');
        String lst = extn.substring(j + 1);
        System.out.println("Size" + size);
        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        String str = configurationMaster.getAllowedExtension();
        String[] str1 = str.split(",");
        for (int i = 0; i < str1.length; i++) {
            if (str1[i].trim().equalsIgnoreCase(lst)) {
                chextn = true;
            }
        }
        if (chextn) {
            fsize = size / (1024 * 1024);
            System.out.println("fsize" + fsize);
            dsize = configurationMaster.getFileSize();
            System.out.println("DataSize" + dsize);
            if (dsize > fsize) {
                chextn = true;
            } else {
                chextn = false;
            }
        }
        return chextn;
    }

    public String docSizeMsg(int userId) {
        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
        return userRegisterService.docSizeCheck(userId);
    }

    public boolean deleteFile(String filePath) {

        File f = new File(filePath);
        if (f.delete()) {
            return true;
        } else {
            return false;
        }

    }
    public String getDocTable(String tenderId,String lotId,String rId,String url,EvaluationService evaluationService){
        List<TblEvalReportDocs> list_disp = new ArrayList<TblEvalReportDocs>();
        list_disp=evaluationService.getListOfDocs(Integer.parseInt(tenderId),Integer.parseInt(lotId),Integer.parseInt(rId));
        StringBuffer strDocData = new StringBuffer();
        int docCnt=0;
        strDocData.append("<table width='100%' cellspacing='0' class='tableList_1 t_space'>");
        strDocData.append("<tr>");
        strDocData.append("<th width='4%' class='t-align-left'>Sl.  No.</th>");
        strDocData.append("<th class='t-align-left' width='25%'>File Name</th>");
        strDocData.append("<th class='t-align-left' width='56%'>File Description</th>");
        strDocData.append("<th class='t-align-left' width='8%'>File Size <br />(in KB)</th>");
        strDocData.append("<th class='t-align-left' width='7%'>Action</th>");
        strDocData.append("</tr>");
        if(list_disp!=null && !list_disp.isEmpty()){
            for (TblEvalReportDocs tblEvalReportDocs : list_disp) {
                docCnt++;
                            strDocData.append("<tr>");
                            strDocData.append("<td class='t-align-center'>" + docCnt + "</td>");

                            strDocData.append("<td class='t-align-left'>" + tblEvalReportDocs.getDocumentName()+ "</td>");
                            strDocData.append("<td class='t-align-left'>" + tblEvalReportDocs.getDocDescription() + "</td>");

                            strDocData.append("<td class='t-align-center'>" + Long.parseLong(tblEvalReportDocs.getDocSize())/1024  + "</td>");
                            String linkDownloadDoc = "";

                            String linkDeleteDoc = "";
                            linkDeleteDoc = "<a href='javascript:void(0);' onclick=\"deleteFile('" + tblEvalReportDocs.getEvalRptDocId() + "','" + tblEvalReportDocs.getDocumentName() + "','" + tenderId + "','"+lotId+"','"+rId+"')\" ><img src='"+url+"/resources/images/Dashboard/Delete.png' alt='Remove' /></a>";
                            linkDownloadDoc = "<a href='"+url+"/EvalRptDocsServlet?tenderid="+tenderId+"&lotId="+lotId+"&docId="+tblEvalReportDocs.getEvalRptDocId()+"&docName="+tblEvalReportDocs.getDocumentName()+"&funName=download' title='Download'><img src='"+url+"/resources/images/Dashboard/Download.png' alt='Download' /></a>";
                            strDocData.append("<td class='t-align-center' width='18%'>" + linkDownloadDoc + "&nbsp;" + linkDeleteDoc + "</td>");

                            strDocData.append("</tr>");
            }
        }else{
           strDocData.append("<tr>");
            strDocData.append("<td colspan='5' class='t-align-center'>No records found.</td>");
            strDocData.append("</tr>"); 
        }
        return strDocData.toString();
    }
    public String getReviewDocTable(String tenderId,String lotId,String rId,String url,EvaluationService evaluationService,int userId){
        List<TblEvalReportDocs> list_disp = new ArrayList<TblEvalReportDocs>();
        list_disp=evaluationService.getListOfDocs(Integer.parseInt(tenderId),Integer.parseInt(lotId),Integer.parseInt(rId));
        StringBuffer strDocData = new StringBuffer();
        int docCnt=0;
        strDocData.append("<table width='100%' cellspacing='0' class='tableList_1 t_space'>");
        strDocData.append("<tr>");
        strDocData.append("<th width='4%' class='t-align-left'>Sl.  No.</th>");
        strDocData.append("<th class='t-align-left' width='25%'>File Name</th>");
        strDocData.append("<th class='t-align-left' width='56%'>File Description</th>");
        strDocData.append("<th class='t-align-left' width='8%'>File Size <br />(in KB)</th>");
        strDocData.append("<th class='t-align-left' width='7%'>Action</th>");
        strDocData.append("</tr>");
        if(list_disp!=null && !list_disp.isEmpty()){
            for (TblEvalReportDocs tblEvalReportDocs : list_disp) {
                docCnt++;
                            strDocData.append("<tr>");
                            strDocData.append("<td class='t-align-center'>" + docCnt + "</td>");

                            strDocData.append("<td class='t-align-left'>" + tblEvalReportDocs.getDocumentName()+ "</td>");
                            strDocData.append("<td class='t-align-left'>" + tblEvalReportDocs.getDocDescription() + "</td>");

                            strDocData.append("<td class='t-align-center'>" + Long.parseLong(tblEvalReportDocs.getDocSize())/1024  + "</td>");
                            String linkDownloadDoc = "";

                            String linkDeleteDoc = "";
                            if(userId == tblEvalReportDocs.getUploadedBy())
                            {
                            linkDeleteDoc = "<a href='javascript:void(0);' onclick=\"deleteFile('" + tblEvalReportDocs.getEvalRptDocId() + "','" + tblEvalReportDocs.getDocumentName() + "','" + tenderId + "','"+lotId+"','"+rId+"')\" ><img src='"+url+"/resources/images/Dashboard/Delete.png' alt='Remove' /></a>";
                }
                            linkDownloadDoc = "<a href='"+url+"/EvalRptDocsServlet?tenderid="+tenderId+"&lotId="+lotId+"&docId="+tblEvalReportDocs.getEvalRptDocId()+"&docName="+tblEvalReportDocs.getDocumentName()+"&funName=download' title='Download'><img src='"+url+"/resources/images/Dashboard/Download.png' alt='Download' /></a>";
                            strDocData.append("<td class='t-align-center' width='18%'>" + linkDownloadDoc + "&nbsp;" + linkDeleteDoc + "</td>");

                            strDocData.append("</tr>");
            }
        }else{
           strDocData.append("<tr>");
            strDocData.append("<td colspan='5' class='t-align-center'>No records found.</td>");
            strDocData.append("</tr>");
        }
        return strDocData.toString();
    }
    public int delDocs(int tenderId,int lotId,String docName,int docId,EvaluationService evaluationService){
       String fileName = DESTINATION_DIR_PATH + tenderId+"_"+lotId + "\\" + docName;
       int i_cnt = 0;
       boolean  checkret = deleteFile(fileName);
        if (checkret) {
             i_cnt = evaluationService.removeUploadDoc(docId);
        }
        return i_cnt;
    }
}
