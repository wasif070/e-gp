/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.service.serviceinterface.SubContractingService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class SubContractingSrbean {

    static final Logger logger = Logger.getLogger(SubContractingSrbean.class);
    private String logUserId = "0";
    SubContractingService subContractingService = (SubContractingService) AppContext.getSpringBean("SubContractingService");

    /**
     * for Login userId
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
        subContractingService.setUserId(logUserId);
    }

    /**
     * Not used now
     * @param subContractingDtBean
     * @return Created by Prashant
     */
   /* public SubContractingDtBean setCompanyDetails(SubContractingDtBean subContractingDtBean) {
        logger.debug("setCompanyDetails : " + logUserId + " Starts");
       TblCompanyMaster details = subContractingService.getCompanyDetails(subContractingDtBean.getEmailId());
        if (details != null && details.getCompanyId() != 0) {
       subContractingDtBean.setDataExist(true);
       subContractingDtBean.setCompanyName(details.getCompanyName());
       subContractingDtBean.setCompanyRegNumber(details.getCompanyRegNumber());
       //subContractingDtBean.setDetails(details.get());
       subContractingDtBean.setRegOffCity(details.getRegOffCity());
       subContractingDtBean.setRegOffCountry(details.getRegOffCountry());
       subContractingDtBean.setRegOffState(details.getRegOffState());
       subContractingDtBean.setUserId(details.getTblLoginMaster().getUserId());
        } else {
        subContractingDtBean.setDataExist(false);
        }
        logger.debug("setCompanyDetails : " + logUserId + " Ends");
       return subContractingDtBean;

    }*/
    
    /**
     * This method is called when subcontracting request is sent to other tenderer
     * @param emaiId : emailId of another tenderer  
     * @param name 
     * @param tenderId 
     * @param tenderRefNo : TenderRefNo
     * @param listMailDtl 
     * @return flag if sucess else fail
     */
    public boolean sendMail(String emaiId, String name, String tenderId, String tenderRefNo,List<Object[]> listMailDtl) {
        logger.debug("sendMail : " + logUserId + " Starts");
          boolean mailSent = false;
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            String[] mailTo = {emaiId};
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
            registerService.contentAdmMsgBox(emaiId, XMLReader.getMessage("emailIdNoReply"), "e-GP: Invitation for Sub-Contracting / Sub-Consultant", msgBoxContentUtility.getSubContractInvtDetails(emaiId, name, tenderId, tenderRefNo , listMailDtl));
            List list = mailContentUtility.getSubContractInvtDetails(emaiId, name, tenderId, tenderRefNo , listMailDtl);
             String mailSub = list.get(0).toString();
             String mailText = list.get(1).toString();
             sendMessageUtil.setEmailTo(mailTo);
             sendMessageUtil.setEmailSub(mailSub);
             sendMessageUtil.setEmailMessage(mailText);
             sendMessageUtil.sendEmail();
             mailSent = true;
        } catch (Exception ex) {
            logger.error("sendMail : " + logUserId + " : " + ex);
          }
        listMailDtl.clear();
        listMailDtl=null;
        logger.debug("sendMail : " + logUserId + " Ends");
        return mailSent;
          }

   /**
     * This method is called when ProcessSubContractInv request is sent to other tenderer
     * @param emaiId : emailId of another tenderer  
     * @param tenderId 
     * @param tenderRefNo : TenderRefNo
     * @param invAcceptStatus 
     * @param name 
     * @param listMailDtl 
     * @return flag if sucess else fail
     */
    public boolean sendMail(String emaiId, String tenderId, String tenderRefNo, String invAcceptStatus, String name, List<Object[]> listMailDtl) {
        logger.debug("sendMail : " + logUserId + " Starts");
          boolean mailSent = false;
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            String[] mailTo = {emaiId};
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            MailContentUtility mailContentUtility = new MailContentUtility();
            UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
            registerService.contentAdmMsgBox(emaiId, XMLReader.getMessage("emailIdNoReply"), "e-GP:  Processing of Sub Contracting Request by Sub Contractor", msgBoxContentUtility.getProcessSubContractInv(emaiId, tenderId, tenderRefNo, invAcceptStatus, name, listMailDtl));
            List list = mailContentUtility.getProcessSubContractInv(emaiId, tenderId, tenderRefNo, invAcceptStatus, name, listMailDtl);
             String mailSub = list.get(0).toString();
             String mailText = list.get(1).toString();
             sendMessageUtil.setEmailTo(mailTo);
             sendMessageUtil.setEmailSub(mailSub);
             sendMessageUtil.setEmailMessage(mailText);
             sendMessageUtil.sendEmail();
             mailSent = true;
        } catch (Exception ex) {
            logger.error("sendMail : " + logUserId + " : " + ex);
          }
        listMailDtl.clear();
        listMailDtl=null;
        logger.debug("sendMail : " + logUserId + " Ends");
          return mailSent;
     }
 //   public List<TblCompanyMaster> getAllTblCompanyMaster()
    /**
     * This method give details of bid widraw status
     * @param tenderId
     * @param userId
     * @return true if bid is not widraw else bid is widraw
     */
    public String getBidWidrawStatus(int tenderId, int userId){
        logger.debug("getBidWidrawStatus : " + logUserId + " Starts");
        String flag = "error";
        try {
           flag =  subContractingService.getBidWidrawStatus(tenderId, userId);
        } catch (Exception e) {
           logger.error("getBidWidrawStatus : " + logUserId + " : " + e);
        }
        logger.debug("getBidWidrawStatus : " + logUserId + " Ends");
        return flag;
    }
}
