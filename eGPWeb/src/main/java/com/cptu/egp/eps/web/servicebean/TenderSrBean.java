/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonAppPkgDetails;
import com.cptu.egp.eps.dao.storedprocedure.CommonSPReturn;
import com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblAppFrameworkOffice;
import com.cptu.egp.eps.model.table.TblBidderLots;
import com.cptu.egp.eps.model.table.TblCancelTenderRequest;
import com.cptu.egp.eps.model.table.TblCorrigendumDetail;
import com.cptu.egp.eps.model.table.TblCorrigendumDocs;
import com.cptu.egp.eps.model.table.TblCorrigendumMaster;
import com.cptu.egp.eps.model.table.TblEvalServiceWeightage;
import com.cptu.egp.eps.model.table.TblLimitedTenders;
import com.cptu.egp.eps.model.table.TblNoaAcceptance;
import com.cptu.egp.eps.model.table.TblTenderAuditTrail;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import com.cptu.egp.eps.model.table.TblTenderFrameWork;
import com.cptu.egp.eps.model.table.TblTenderLotSecurity;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.model.table.TblTenderOpenDates;
import com.cptu.egp.eps.model.table.TblTenderPhasing;
import com.cptu.egp.eps.model.table.TblAppPermission;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceimpl.TenderCorriService;
import com.cptu.egp.eps.service.serviceinterface.TenderAuditTrailService;
import com.cptu.egp.eps.service.serviceinterface.TenderService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.DateUtils;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SHA1HashEncryption;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.cptu.egp.eps.model.table.TblTenderSection;
import com.cptu.egp.eps.service.serviceimpl.TenderCoriGrandSumDetailServiceImpl;
import com.cptu.egp.eps.service.serviceimpl.TenderDocumentService;
import com.cptu.egp.eps.service.serviceinterface.AppViewPkgService;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.TenderFrameWorkService;
import com.cptu.egp.eps.web.databean.MapTendererDtBean;
import com.cptu.egp.eps.web.utility.AppExceptionHandler;
import com.cptu.egp.eps.web.utility.GeneratePdfConstant;
import com.cptu.eps.service.audit.AuditTrail;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita,taher
 */
public class TenderSrBean {

    private String logUserId = "0";
    private static final Logger LOGGER = Logger.getLogger(TenderSrBean.class);
    private final TenderService tenderService = (TenderService) AppContext.getSpringBean("TenderService");
    private final TenderAuditTrailService tenderAuditTrailService = (TenderAuditTrailService) AppContext.getSpringBean("TenderAuditTrailService");
    private final TenderCorriService tenderCorriService = (TenderCorriService) AppContext.getSpringBean("TenderCorriService");
    private final UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    private final TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    private final TenderDocumentService tDocService = (TenderDocumentService) AppContext.getSpringBean("TenderDocumentService");
    private final CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
    private final TenderFrameWorkService tenderFrameWorkService = (TenderFrameWorkService) AppContext.getSpringBean("TenderFrameWorkService");
    private AuditTrail auditTrail;

    /**
     * Setting userId for each used Service
     *
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        tenderService.setUserId(logUserId);
        tenderAuditTrailService.setUserId(logUserId);
        tenderCorriService.setLogUserId(logUserId);
        userRegisterService.setUserId(logUserId);
        tenderCommonService.setLogUserId(logUserId);
        tDocService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /**
     * this method is for checking the tenderer fees. yet fees has been paid or
     * not
     *
     * @param paidFor - this is the tenderer name
     * @param tenderId - this is the tenderer id. for whom the fees going to be
     * checked
     * @param lotId - this is the lot id. fees has been paid through lot wise
     * @param userId - this is the user id
     * @return boolean, true - if paid, false - if not paid
     */
    public boolean chkTenderFeePaid(String paidFor, int tenderId, int lotId, int userId) {
        LOGGER.debug("chkTenderFeePaid : " + logUserId + "starts");
        boolean flag = false;
        try {
            flag = tenderService.chkTenderLotPaid(paidFor, tenderId, lotId, userId);
        } catch (Exception e) {
            LOGGER.error("chkTenderFeePaid : " + e);
        }
        LOGGER.debug("chkTenderFeePaid : " + logUserId + "ends");
        return flag;
    }

    /**
     * this methos is for mapped documents. yet document has been mapped or not
     *
     * @param tenderId - this is the tenderer id. for whom the document going to
     * be checked
     * @return boolean true- if mapped ,false-if not mapped
     */
    public boolean chkDocMaped(int tenderId) {
        LOGGER.debug("chkDocMaped : " + logUserId + "starts");
        boolean flag = false;
        try {
            flag = tenderService.chkDocMaped(tenderId);
        } catch (Exception e) {

            LOGGER.error("chkDocMaped : " + e);

        }
        LOGGER.debug("chkDocMaped : " + logUserId + "ends");
        return flag;
    }

    /**
     * this method is for getting the list of data
     *
     * @param appId - this is the appid
     * @param pckId - this is the package id
     * @param createdBy - this is the user name
     * @return list of data
     */
    public List<CommonSPReturn> insertSPAddYpTEndernotice(int appId, int pckId, String createdBy) { // Two Blank Params are added for ICT for PkgDocFeesVcUSD and TenderSecurityAmtUSD
        LOGGER.debug("insertSPAddYpTEndernotice : " + logUserId + " starts");
        AppViewPkgService appViewPkgService = (AppViewPkgService) AppContext.getSpringBean("AppViewPkgService");
        List<CommonAppPkgDetails> appPkgDeatils = appViewPkgService.getAppPkgDetails(appId, pckId, "Package");
        DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        Date subDt, opnDt, pubDt;
        subDt=opnDt=pubDt=null;
        
        List<CommonSPReturn> insertSPAddYpTEndernotice = null;
        try {
            if(appPkgDeatils.get(0).getTenderSubDt()!=null)
                subDt =df.parse(appPkgDeatils.get(0).getTenderSubDt());
            if(appPkgDeatils.get(0).getTenderOpenDt()!=null)
                opnDt =df.parse(appPkgDeatils.get(0).getTenderOpenDt()); 
            if(appPkgDeatils.get(0).getTenderAdvertDt()!=null)
                pubDt =df.parse(appPkgDeatils.get(0).getTenderAdvertDt()); 
            insertSPAddYpTEndernotice = tenderService.insertSPAddYpTEndernoticeBySP("","","","","",appId, pckId, "Package", createdBy, " ", null, null, null, subDt, opnDt, " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", null, " ", " ", " ", " ", " ", " ", " ", "insert", 0, 0, " ", " ", " ", " ", " ", " ", " ", " ", " ", pubDt, " ", " ", new BigDecimal(0.0), 0,"","","");

        } catch (Exception e) {
            LOGGER.error("insertSPAddYpTEndernotice : " + e);
        }
        LOGGER.debug("insertSPAddYpTEndernotice : " + logUserId + "ends");
        return insertSPAddYpTEndernotice;
    }

    /**
     * Fetching information about tender notice information
     *
     * @param tenderid
     * @param viewType
     * @return List of Notice Details
     * @throws Exception
     */
    public List<CommonTenderDetails> getAPPDetails(int tenderid, String viewType) throws Exception {
        LOGGER.debug("getAPPDetails : " + logUserId + "starts");
        List<CommonTenderDetails> appList = new ArrayList<CommonTenderDetails>();
        try {
            //added null for district and thana by proshanto kumar saha
            for (CommonTenderDetails commonTenderDetails : tenderService.gettenderinformationSP("get details", viewType, tenderid, null, null, null, null, null, null, null, null, "Pending", null, null, null, null, null, null, 1, 10, null, null)) {
                appList.add(commonTenderDetails);
            }

        } catch (Exception e) {
            LOGGER.error("getAPPDetails : " + e);
        }
        LOGGER.debug("getAPPDetails : " + logUserId + "ends");
        return appList;
    }

    /**
     * this method is for save the data to database insert into
     * TblTenderAuditTrail , TblTenderOpenDates
     *
     * @param tenderId
     * @param action
     * @param userId
     * @param eSignature
     * @param remarks
     * @param openingDate
     * @throws ParseException
     */
    public void insertInToTenderAuditTrail(int tenderId, String action, int userId, String eSignature, String remarks, String openingDate) throws ParseException {
        LOGGER.debug("insertInToTenderAuditTrail : " + logUserId + "starts");
        try {
            TblTenderAuditTrail tblTenderAuditTrail = new TblTenderAuditTrail();
            tblTenderAuditTrail.setTblTenderMaster(new TblTenderMaster(tenderId));
            tblTenderAuditTrail.setAction(action);
            tblTenderAuditTrail.setActionBy(userId);
            tblTenderAuditTrail.setActionDate(new Date());
            tblTenderAuditTrail.setEsignature(eSignature);
            tblTenderAuditTrail.setDigitalSignature("");
            tblTenderAuditTrail.setActivity(action);
            tblTenderAuditTrail.setRemarks(remarks);
            Date oDate = DateUtils.convertDateToStr(openingDate);
            for (Object obj : tenderAuditTrailService.getenvId(tenderId)) {
                TblTenderOpenDates tblTenderOpenDates = new TblTenderOpenDates();
                byte eId = (Byte) obj;
                tblTenderOpenDates.setTenderId(tenderId);
                tblTenderOpenDates.setEnvelopeId(eId);
                tblTenderOpenDates.setEntryDt(new Date());
                tblTenderOpenDates.setOpeningDt(oDate);
                tenderAuditTrailService.addTblTenderOpenDates(tblTenderOpenDates);
                tblTenderOpenDates = null;
            }
            tenderAuditTrailService.addTenderAuditTrail(tblTenderAuditTrail);
            sendTenderDetailMail("" + tenderId);
        } catch (Exception e) {
            LOGGER.error("insertInToTenderAuditTrail : " + e);
        }
        LOGGER.debug("insertInToTenderAuditTrail : " + logUserId + "ends");
    }

    /**
     * this method is for dump STD.
     *
     * @param tenderId from tbl_TenderMaster
     * @param templateId STD templateId
     * @param userId PE userId
     * @return boolean true- if dump ,false-if not dump
     */
    public boolean isSTDDump(String tenderId, String templateId, String userId) {
        LOGGER.debug("isSTDDump : " + logUserId + "starts");
        boolean flag = false;
        try {
            tenderService.deleteTenderGrandSum(Integer.parseInt(tenderId));
            flag = tenderService.isSTDDump(Integer.parseInt(tenderId));
            if (flag) {
                tDocService.dumpSTD(tenderId, templateId, userId);
            }
        } catch (Exception e) {
            LOGGER.error("isSTDDump : " + e);
        }
        LOGGER.debug("isSTDDump : " + logUserId + "ends");
        return flag;
    }

    /**
     * insert / update tender information
     *
     * @param appId
     * @param pckId
     * @param createdBy
     * @param reoiRfpRefNo
     * @param docEndDate_ins
     * @param preBidStartDt
     * @param preBidEndDt
     * @param submissionDt
     * @param openingDt
     * @param eligibilityCriteria
     * @param tenderBrief
     * @param deliverables
     * @param otherDetails
     * @param foreignFirm
     * @param docAvlMethod
     * @param evalType
     * @param docFeesMethod
     * @param docFeesMode
     * @param docOfficeAdd
     * @param securityLastDt
     * @param securitySubOff
     * @param location
     * @param docFees
     * @param tenderSecurityAmt
     * @param completionTime
     * @param startTime
     * @param action
     * @param tenderId
     * @param corrId
     * @param phasingRefNo
     * @param phasingOfService
     * @param phasingLocation
     * @param indStartDt
     * @param indEndDt
     * @param eSignature
     * @param digitalSignature
     * @param tenderLotSecId
     * @param contractTypeNew
     * @param tenderPubDt
     * @param pkgDocFees
     * @param estCost
     * @param passingMarks
     * @throws Exception
     */
    // Two Param 1. pkgDocFeesVcUSD and 2. tenderSecurityAmtMUSD are added for ICT - Dohatec
    public void updateSPAddYpTEndernotice(String bidCategory, String W1, String W2, String W3, String W4, int appId, int pckId, String createdBy, String reoiRfpRefNo, Date docEndDate_ins, Date preBidStartDt, Date preBidEndDt, Date submissionDt, Date openingDt, String eligibilityCriteria, String tenderBrief, String deliverables, String otherDetails, String foreignFirm, String docAvlMethod, String evalType, String docFeesMethod, String docFeesMode, String docOfficeAdd, Date securityLastDt, String securitySubOff, String location, String docFees, String tenderSecurityAmt, String tenderSecurityAmtUSD, String completionTime, String startTime, String action, int tenderId, int corrId, String phasingRefNo, String phasingOfService, String phasingLocation, String indStartDt, String indEndDt, String eSignature, String digitalSignature, String tenderLotSecId, String contractTypeNew, Date tenderPubDt, String pkgDocFees, String pkgDocFeesVcUSD, BigDecimal estCost, Integer passingMarks,String ProcurementMethod, String bidSecurityType, String eventType , String... auditInfo) throws Exception {
        LOGGER.debug("updateSPAddYpTEndernotice : " + logUserId + "starts");
        try {
            tenderService.insertSPAddYpTEndernoticeBySP(bidCategory, W1, W2, W3, W4, appId, pckId, "Package", createdBy, reoiRfpRefNo, docEndDate_ins, preBidStartDt, preBidEndDt, submissionDt, openingDt, eligibilityCriteria, tenderBrief, deliverables, otherDetails, foreignFirm, docAvlMethod, evalType, docFeesMethod, docFeesMode, docOfficeAdd, securityLastDt, securitySubOff, location, docFees, tenderSecurityAmt, tenderSecurityAmtUSD, completionTime, startTime, action, tenderId, corrId, phasingRefNo, phasingOfService, phasingLocation, indStartDt, indEndDt, eSignature, digitalSignature, tenderLotSecId, contractTypeNew, tenderPubDt, pkgDocFees, pkgDocFeesVcUSD, estCost, passingMarks, ProcurementMethod,bidSecurityType, eventType, auditInfo);
        } catch (Exception e) {
            LOGGER.error("updateSPAddYpTEndernotice : " + logUserId + e);
        }
        LOGGER.debug("updateSPAddYpTEndernotice : " + logUserId + "ends");
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        tenderAuditTrailService.setAuditTrail(auditTrail);
        tenderCorriService.setAuditTrail(auditTrail);
        tenderService.setAuditTrail(auditTrail);
    }
    private boolean inWatchList = false;
    private int watchId = 0;

    public int getWatchId() {
        return watchId;
    }

    public void setWatchId(int watchId) {
        this.watchId = watchId;
    }

    public boolean isInWatchList() {
        return inWatchList;
    }

    public void setInWatchList(boolean inWatchList) {
        this.inWatchList = inWatchList;
    }

    /**
     * check watch list
     *
     * @param userId
     * @param packageId
     * @return int (id of watch from TblTenderWatchList)
     */
    public int checkWatchList(int userId, int packageId) {
        try {
            LOGGER.debug("checkWatchList : " + logUserId + "starts");
            watchId = 0;
            watchId = tenderService.checkTenderWatchList(userId, packageId);
            if (watchId != 0) {
                setInWatchList(true);
            }
        } catch (Exception ex) {
            LOGGER.error("checkWatchList : " + ex);
        }
        LOGGER.debug("checkWatchList : " + logUserId + "ends");
        return watchId;
    }

    /**
     * Creates Corrigendum/Amendment for the Tender
     *
     * @param tenderid from tbl_tenderMaster
     * @param corriId from tbl_CorrigendumMaster
     * @param securityLastDt map with lastDateTenderSub
     * @param preBidStartDt map with preTenderMeetStartDate
     * @param openingDt map with preQualOpenDate
     * @param preBidEndDt map with preTenderMeetEndDate
     * @param submissionDt map with preQualCloseDate
     * @param securitySubOff map with nameAddressTenderSub
     * @param docEndDate map with tenderLastSellDate
     * @param indStartDt map with indicativeStartDate
     * @param indEndDt map with indicativeComplDate
     * @param docfess map with docFeeslot
     * @param pkgDocFees map with preQualDocPrice
     * @param eligibilityCriteria map with eligibilityofTenderer
     * @param tenderBrief map with briefDescGoods
     * @param location map with locationRefNo & locationlot
     * @param tenderSecurityAmt map with tenderSecurityAmount
     * @param completionTime map with complTimeLotNo
     * @param phasingRefNov map with txtrefNo
     * @param phasingOfService map with phasingService
     * @param startTime map with startDateLotNo
     * @param deliverables map with expRequired
     * @param otherDetails map with otherDetails
     * @param isedit map with
     * @throws Exception
     */
    public void createCorri(String tenderid, int corriId, String securityLastDt, String preBidStartDt, String openingDt, String preBidEndDt, String submissionDt, String securitySubOff, String docEndDate, String[] indStartDt, String[] indEndDt, String[] docfess, String pkgDocFees, String pkgDocFeesUSD, String eligibilityCriteria, String tenderBrief, String[] location, String[] tenderSecurityAmt, String[] tenderSecurityAmtUSD, String[] completionTime, String[] phasingRefNo, String[] phasingOfService, String[] startTime, String deliverables, String otherDetails, boolean isedit) throws Exception {

        // For ICT 2 parameters are added :: 1. preQualDocPriceUSD, 2. tenderSecurityAmountUSD
        LOGGER.debug("createCorri : " + logUserId + "starts");
        TblTenderDetails tblTenderDetails = tenderCorriService.getTenderDetails("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(Integer.parseInt(tenderid)));

        List<TblCorrigendumDetail> list = new ArrayList<TblCorrigendumDetail>();

        if (docEndDate != null && !DateUtils.convertStrToDate(tblTenderDetails.getDocEndDate()).equals(docEndDate)) {
            list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "docEndDate", DateUtils.convertStrToDate(tblTenderDetails.getDocEndDate()), docEndDate));
        }

        if (!"".equals(preBidStartDt) && (tblTenderDetails.getPreBidStartDt() == null || !DateUtils.convertStrToDate(tblTenderDetails.getPreBidStartDt()).equals(preBidStartDt))) {
            list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "preBidStartDt", tblTenderDetails.getPreBidStartDt() != null ? DateUtils.convertStrToDate(tblTenderDetails.getPreBidStartDt()) : "", preBidStartDt));
        }
        if (!"".equals(preBidEndDt) && (tblTenderDetails.getPreBidEndDt() == null || !DateUtils.convertStrToDate(tblTenderDetails.getPreBidEndDt()).equals(preBidEndDt))) {
            list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "preBidEndDt", tblTenderDetails.getPreBidEndDt() != null ? DateUtils.convertStrToDate(tblTenderDetails.getPreBidEndDt()) : "", preBidEndDt));
        }

        if (!DateUtils.convertStrToDate(tblTenderDetails.getSubmissionDt()).equals(submissionDt)) {
            list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "submissionDt", DateUtils.convertStrToDate(tblTenderDetails.getSubmissionDt()), submissionDt));
        }

        if (!DateUtils.convertStrToDate(tblTenderDetails.getOpeningDt()).equals(openingDt)) {
            list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "openingDt", DateUtils.convertStrToDate(tblTenderDetails.getOpeningDt()), openingDt));
        }
        if (securityLastDt != null && !DateUtils.convertStrToDate(tblTenderDetails.getSecurityLastDt()).equals(securityLastDt)) {
            list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "securityLastDt", DateUtils.convertStrToDate(tblTenderDetails.getSecurityLastDt()), securityLastDt));
        }

        if (eligibilityCriteria != null && !tblTenderDetails.getEligibilityCriteria().equals(eligibilityCriteria)) {
            list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "eligibilityCriteria", tblTenderDetails.getEligibilityCriteria(), eligibilityCriteria));
        }

        if (tenderBrief != null && !tblTenderDetails.getTenderBrief().equals(tenderBrief)) {
            list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "tenderBrief", tblTenderDetails.getTenderBrief(), tenderBrief));
        }

        if (securitySubOff != null && !tblTenderDetails.getSecuritySubOff().equals(securitySubOff)) {
            list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "securitySubOff", tblTenderDetails.getSecuritySubOff(), securitySubOff));
        }
        if (deliverables != null && !tblTenderDetails.getDeliverables().equals(deliverables)) {
            list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "deliverables", tblTenderDetails.getDeliverables(), deliverables));
        }
        if (otherDetails != null && !tblTenderDetails.getOtherDetails().equals(otherDetails)) {
            list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "otherDetails", tblTenderDetails.getOtherDetails(), otherDetails));
        }

        if (tblTenderDetails.getProcurementNature().equals("Services")) {
            List<TblTenderPhasing> phasings = tenderCorriService.findTenderPhasing("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(Integer.parseInt(tenderid)));
            for (int i = 0; i < phasings.size(); i++) {
                if (!DateUtils.formatStdDate(phasings.get(i).getIndStartDt()).equals(indStartDt[i])) {
                    list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "indStartDt@" + phasings.get(i).getTenderPhasingId(), DateUtils.formatStdDate(phasings.get(i).getIndStartDt()), indStartDt[i]));
                }
                if (!DateUtils.formatStdDate(phasings.get(i).getIndEndDt()).equals(indEndDt[i])) {
                    list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "indEndDt@" + phasings.get(i).getTenderPhasingId(), DateUtils.formatStdDate(phasings.get(i).getIndEndDt()), indEndDt[i]));
                }
                if (phasingRefNo != null && !phasings.get(i).getPhasingRefNo().equals(phasingRefNo[i])) {
                    list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "phasingRefNo@" + phasings.get(i).getTenderPhasingId(), phasings.get(i).getPhasingRefNo(), phasingRefNo[i]));
                }
                if (phasingOfService != null && !phasings.get(i).getPhasingOfService().equals(phasingOfService[i])) {
                    list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "phasingOfService@" + phasings.get(i).getTenderPhasingId(), phasings.get(i).getPhasingOfService(), phasingOfService[i]));
                }
                if (location != null && !phasings.get(i).getLocation().equals(location[i])) {
                    list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "location@" + phasings.get(i).getTenderPhasingId(), phasings.get(i).getLocation(), location[i]));
                }
            }

        } else {
            //if(tblTenderDetails.getDocFeesMethod().equalsIgnoreCase("Lot wise")){///Logic problem related to DocFeesMethod fixed
            List<TblTenderLotSecurity> lotSecuritys = tenderCorriService.findTenderLotSecurity("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(Integer.parseInt(tenderid)));
            for (int i = 0; i < lotSecuritys.size(); i++) {
                if (tblTenderDetails.getDocFeesMethod().equalsIgnoreCase("Lot wise")) {
                    if (docfess != null && lotSecuritys.get(i).getDocFess().setScale(2, 0).compareTo(new BigDecimal(docfess[i])) != 0) {
                        list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "docfess@" + lotSecuritys.get(i).getTenderLotSecId(), String.valueOf(lotSecuritys.get(i).getDocFess().floatValue()), docfess[i]));
                    }
                }
                if (tenderSecurityAmt != null && lotSecuritys.get(i).getTenderSecurityAmt().setScale(2, 0).compareTo(new BigDecimal(tenderSecurityAmt[i])) != 0) {
                    list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "tenderSecurityAmt@" + lotSecuritys.get(i).getTenderLotSecId(), String.valueOf(lotSecuritys.get(i).getTenderSecurityAmt().floatValue()), tenderSecurityAmt[i]));
                }

                if (tenderSecurityAmtUSD != null && lotSecuritys.get(i).getTenderSecurityAmtUSD().setScale(2, 0).compareTo(new BigDecimal(tenderSecurityAmtUSD[i])) != 0) {
                    // For ICT Dohatec
                    list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "tenderSecurityAmtUSD@" + lotSecuritys.get(i).getTenderLotSecId(), String.valueOf(lotSecuritys.get(i).getTenderSecurityAmtUSD().floatValue()), tenderSecurityAmtUSD[i]));
                }
                if (location != null && !lotSecuritys.get(i).getLocation().equals(location[i])) {
                    list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "location@" + lotSecuritys.get(i).getTenderLotSecId(), lotSecuritys.get(i).getLocation(), location[i]));
                }
                if (startTime != null && !lotSecuritys.get(i).getStartTime().equals(startTime[i])) {
                    list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "startTime@" + lotSecuritys.get(i).getTenderLotSecId(), lotSecuritys.get(i).getStartTime(), startTime[i]));
                }
                if (completionTime != null && !lotSecuritys.get(i).getCompletionTime().equals(completionTime[i])) {
                    list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "completionTime@" + lotSecuritys.get(i).getTenderLotSecId(), lotSecuritys.get(i).getCompletionTime(), completionTime[i]));
                }
            }
            /*}else*///Logic problem related to DocFeesMethod fixed
            if (tblTenderDetails.getDocFeesMethod().equalsIgnoreCase("Package wise") && pkgDocFees != null && tblTenderDetails.getPkgDocFees().setScale(2, 0).compareTo(new BigDecimal(pkgDocFees)) != 0) {
                list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "pkgDocFees", String.valueOf(tblTenderDetails.getPkgDocFees().floatValue()), pkgDocFees));
            }

            if (tblTenderDetails.getDocFeesMethod().equalsIgnoreCase("Package wise") && pkgDocFeesUSD != null && tblTenderDetails.getPkgDocFeesUSD().setScale(2, 0).compareTo(new BigDecimal(pkgDocFeesUSD)) != 0) {
                // For ICT Dohatec
                list.add(new TblCorrigendumDetail(0, new TblCorrigendumMaster(corriId), "pkgDocFeesUSD", String.valueOf(tblTenderDetails.getPkgDocFeesUSD().floatValue()), pkgDocFeesUSD));
            }
        }
        if (!list.isEmpty()) {
            tenderCorriService.insertAllCorriDetails(list, isedit, corriId);
        }
        LOGGER.debug("createCorri : " + logUserId + "ends");
    }

    /**
     * Method use for get Corrigendum Details.
     *
     * @param corrid
     * @return Details of Corrigendum
     * @throws Exception
     */
    public List<TblCorrigendumDetail> corrigendumDetails(int corrid) throws Exception {
        LOGGER.debug("corrigendumDetails : " + logUserId + "starts");

        List<TblCorrigendumDetail> corrigendumDetails = null;
        try {
            corrigendumDetails = tenderCorriService.findCorrigendumDetail("tblCorrigendumMaster", Operation_enum.EQ, new TblCorrigendumMaster(corrid));
        } catch (Exception e) {
            LOGGER.error("corrigendumDetails : " + e);
        }

        LOGGER.debug("corrigendumDetails : " + logUserId + "ends");
        return corrigendumDetails;

    }

    /**
     * Get PhaseRefferenceno or lotno.
     *
     * @param isphase true then it returns phashrefferenceno else return lotno
     * @param objId phaseid or lotid
     * @return phashrefferenceno if isphase is set as true else return lotno.
     */
    public String phaseSecureDetail(boolean isphase, String objId) {
        LOGGER.debug("phaseSecureDetail : " + logUserId + "starts");
        String phaseSecureDetail = "";
        try {
            phaseSecureDetail = tenderCorriService.phaseOrLotSecureDetail(isphase, objId);
        } catch (Exception e) {
            LOGGER.error("phaseSecureDetail : " + e);
        }

        LOGGER.debug("phaseSecureDetail : " + logUserId + "ends");
        return phaseSecureDetail;
    }

    /**
     * Publish Corrigendum for the tender
     *
     * @param tenderid
     * @param corrid
     * @param remarks
     * @param action
     * @param actionBy
     * @param reqURL
     * @param reqQuery
     * @param folderName
     */
    public void publishCorri(String tenderid, String corrid, String remarks, String action, String actionBy, String reqURL, String reqQuery, String folderName) {
        LOGGER.debug("publishCorri : " + logUserId + "starts");

        // Following two lines added to transfer temporary data of corrigendum grand summary into original tables.
        TenderCoriGrandSumDetailServiceImpl tenderCoriGrandSumDetailService = (TenderCoriGrandSumDetailServiceImpl) AppContext.getSpringBean("TenderCoriGrandSumDetailServiceImpl");;
        tenderCoriGrandSumDetailService.performGrandSumCorriPublish(tenderid, corrid);

        tenderCorriService.publishCorri(tenderid, corrid, remarks, action, actionBy, SHA1HashEncryption.encodeStringSHA1(remarks));
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        if ("Services".equalsIgnoreCase(commonService.getProcNature(tenderid).toString())) {
            chngStatusMark(tenderid);
        }
        try {
            reqURL = reqURL.replace("ViewAmendment", "ViewTender");
            reqURL = reqURL.replace("officer", "resources/common");
            GenreatePdfCmd obj = new GenreatePdfCmd();
            obj.genrateCmd(reqURL, reqQuery, folderName, tenderid);
        } catch (Exception e) {
            LOGGER.error("publishCorri : " + e);
        }
        LOGGER.debug("publishCorri : " + logUserId + "ends");
    }

    /**
     * Get TenderRule Engine for creating corrigendum
     *
     * @param tenderid
     * @return
     */
    public Object[] findTenderRuleEngine(String tenderid) {
        LOGGER.debug("findTenderRuleEngine : " + logUserId + "starts");
        Object[] findTenderRuleEngine = null;
        try {
            findTenderRuleEngine = tenderCorriService.findTenderRuleEngine(tenderid);
        } catch (Exception e) {
            LOGGER.error("findTenderRuleEngine : " + e);
        }
        LOGGER.debug("findTenderRuleEngine : " + logUserId + "ends");
        return findTenderRuleEngine;
    }
    //update status in TendedDetails

    /**
     * update information into TblTenderDetails
     *
     * @param tenderId
     * @param reqURL
     * @param reqQuery
     * @param folderName
     * @param tenderValidityDt
     * @param tenderSecurityDt
     * @return true true or false
     */
    public boolean updateStatus(int tenderId, String reqURL, String reqQuery, String folderName, Date tenderValidityDt, Date tenderSecurityDt, String updatePeName) {
        boolean flag = false;
        LOGGER.debug("updateStatus : " + logUserId + "starts");
        List<SPTenderCommonData> details = tenderCommonService.getdocAccess(tenderId);
        if (!details.isEmpty()) {
            System.out.println("in tenderSrBean tenderValidityDt------->" + tenderValidityDt);
            if (tenderService.updateStatus(tenderId, tenderValidityDt, tenderSecurityDt, details.get(0).getFieldName1(), updatePeName)) {

                try {
                    String tempURL = reqURL;
                    String tempURLForms = reqURL;
                    GenreatePdfCmd obj = new GenreatePdfCmd();
                    //http://localhost:8080/resources/common/ViewTender.jsp?id=1700
                    reqURL = reqURL.replace("officer/PublishViewTender", "resources/common/ViewTender");
                    reqQuery = reqQuery + "&isPDF=true";
                    obj.genrateCmd(reqURL, reqQuery, folderName, Integer.toString(tenderId));
//
//                //http://localhost:8080/eGPWeb/officer/PublishViewTender.jsp?id=50
//                int tenderStdId = 0;
//                int tenderittSectionId = 0;
//                String pdfId = "";
//                File folderFile = new File(FilePathUtility.getFilePath().get("TenderSecUploadServlet")+tenderId);
//                    if(!folderFile.exists()){
//                        folderFile.mkdir();
//                    }
//
//                File file = null;
//                File dtFile = null;
//                GeneratePdfConstant pdfConst = new GeneratePdfConstant();
//                TenderDocumentSrBean dDocSrBean = new TenderDocumentSrBean();
//                tenderStdId = dDocSrBean.getTenderSTDId(tenderId);
//                List<TblTenderSection> tSections = dDocSrBean.getTenderSections(tenderStdId);
//                String fileName = null;
//                String copyfolderName = null;
//                int int_cnt=0;
//                if (tSections != null) {
//                    if (!tSections.isEmpty()) {
//                        int ittId = -1, gccId = -1;
//                        for (TblTenderSection tts : tSections) {
//                            int_cnt++;
//                            //tempURL = reqURL;
//                            if (tts.getContentType().equalsIgnoreCase("ITT")) {
//                                ittId = tts.getTenderSectionId();
//                            } else if (tts.getContentType().equalsIgnoreCase("GCC")) {
//                                gccId = tts.getTenderSectionId();
//                            }
//                            if ((tts.getContentType().equalsIgnoreCase("TDS")) || (tts.getContentType().equalsIgnoreCase("PCC")) || (tts.getContentType().equalsIgnoreCase("PDC"))) {
//                                tempURL = tempURL.replace("PublishViewTender", "TenderTDSView");
//                                if (tts.getContentType().equalsIgnoreCase("TDS")) {
//                                    tenderittSectionId = ittId;
//                                    copyfolderName = "Section"+int_cnt+"_"+tts.getSectionName();
//                                } else if (tts.getContentType().equalsIgnoreCase("PCC")|| (tts.getContentType().equalsIgnoreCase("PDC"))) {
//                                    copyfolderName = "Section"+int_cnt+"_"+tts.getSectionName();
//                                    tenderittSectionId = gccId;
//                                }
//                                dtFile = new File(FilePathUtility.getFilePath().get("TenderSecUploadServlet")+tenderId+"\\"+copyfolderName+"\\");
//                                if(!dtFile.exists()){
//                                    dtFile.mkdir();
//                                }
//                                reqQuery = "tenderStdId=" + tenderStdId + "&tenderId=" + tenderId + "&sectionId=" + tenderittSectionId+"&isPDF=true";
//                                pdfId = tenderId + "_" + tenderittSectionId + "_" + tenderStdId;
//                                folderName = pdfConst.TENDERDOC;
//                                file = new File(obj.path+"/"+folderName+"/"+pdfId+"/TenderDoc.pdf");
//                                pdfId = tenderId + "_" + tenderittSectionId + "_" + tenderStdId;
//                                obj.genrateCmd(tempURL, reqQuery, folderName, pdfId);
//                                dtFile = new File(FilePathUtility.getFilePath().get("TenderSecUploadServlet")+tenderId+"\\"+copyfolderName+"\\"+copyfolderName+".pdf");
//                                copyfile(file, dtFile);
//                                //<a href="TenderTDSDashBoard.jsp?tenderStdId=<%=tenderStdId%>&tenderId=<%=tenderId%>&sectionId=<%=tenderittSectionId%>">View</a>
//                            }
//                            if (tts.getContentType().equals("Form")) {
//                                copyfolderName = "Section"+int_cnt+"_"+tts.getSectionName();
//                                StringBuilder strForAllFormPDF = new StringBuilder();
//                                java.util.List<com.cptu.egp.eps.model.table.TblTenderForms> forms = dDocSrBean.getTenderForm(tts.getTenderSectionId());
//                                dtFile = new File(FilePathUtility.getFilePath().get("TenderSecUploadServlet")+tenderId+"\\"+copyfolderName+"\\");
//                                if(!dtFile.exists()){
//                                    dtFile.mkdir();
//                                }
//                                if (forms != null) {
//                                    if (!forms.isEmpty()) {
//                                        tempURL = tempURLForms.replace("PublishViewTender", "TestTenderForm");
//                                        pdfId = tenderId + "_" + tts.getTenderSectionId() + "_" + tenderStdId;
//                                        folderName = pdfConst.TENDERDOC;
//                                        for (int jj = 0; jj < forms.size(); jj++) {
//                                          //http://localhost:8080/eGPWeb/officer/ViewTenderCompleteForm.jsp?tenderId=1356&sectionId=538&formId=313
//                                            reqQuery = "tenderId=" + tenderId + "&sectionId=" + tts.getTenderSectionId() + "&formId=" + forms.get(jj).getTenderFormId();
//                                          /*  if(jj==0){
//                                               strForAllFormPDF.append(tempURL + "?" + reqQuery + "&isPDF=true" + "\" ");
//                                            }else if(jj==(forms.size()-1)){
//                                                strForAllFormPDF.append("\"" + tempURL + "?" + reqQuery + "&isPDF=true");
//                                            }else{
//                                                strForAllFormPDF.append("\"" + tempURL + "?" + reqQuery + "&isPDF=true" + "\" ");
//                                            } */
//
//                                            strForAllFormPDF.append("\"" + tempURL + "?" + reqQuery + "&isPDF=true" + "\" ");
//                                        } //end for loop
//                                        obj.genrateCmdForm(strForAllFormPDF.toString(), "", folderName, pdfId);
//
//                                        file = new File(obj.path+"/"+folderName+"/"+pdfId+"/TenderDoc.pdf");
//                                        //dtFile = new File(FilePathUtility.getFilePath().get("SevletEngEstUpload")+tenderId+"\\"+tts.getContentType()+"_TenderDoc.pdf");
//                                        dtFile = new File(FilePathUtility.getFilePath().get("TenderSecUploadServlet")+tenderId+"\\"+copyfolderName+"\\"+copyfolderName+".pdf");
//
//                                        copyfile(file, dtFile);
//                                    }
//                                }//end if(forms != null)
//                            }
//                        } //end for
//                    }//end if(tSections.size() > 0)
//                }//end if(tSections != null)
//
//                folderFile = null;
                } catch (Exception e) {
                    e.printStackTrace();
                    LOGGER.error("updateStatus : " + e);
                    AppExceptionHandler appExceptionHandler = new AppExceptionHandler();
                    appExceptionHandler.stack2string(e);
                }
                flag = true;
            } else {
                flag = false;
            }
        }
        LOGGER.debug("updateStatus : " + logUserId + "ends");
        return flag;
    }

    /**
     * Generate Pdf when load the page
     *
     * @param tenderId
     * @param reqURL
     * @param reqQuery
     * @param folderName
     * @return true if success else false
     */
    public boolean generatePdfOnLoad(int tenderId, String reqURL, String reqQuery, String folderName) {
        boolean flag = false;
        LOGGER.debug("generatePdfOnLoad : " + logUserId + "starts");

        try {
            String tempURL = reqURL;
            GenreatePdfCmd obj = new GenreatePdfCmd();
            //obj.genrateCmd(reqURL, reqQuery, folderName, Integer.toString(tenderId));
            int tenderStdId = 0;
            int tenderittSectionId = 0;
            String pdfId = "";
            GeneratePdfConstant pdfConst = new GeneratePdfConstant();
            TenderDocumentSrBean dDocSrBean = new TenderDocumentSrBean();
            tenderStdId = dDocSrBean.getTenderSTDId(tenderId);
            List<TblTenderSection> tSections = dDocSrBean.getTenderSections(tenderStdId);
            if (tSections != null) {
                if (!tSections.isEmpty()) {
                    int ittId = -1, gccId = -1;
                    for (TblTenderSection tts : tSections) {
                        tempURL = reqURL;
                        if (tts.getContentType().equalsIgnoreCase("ITT") || tts.getContentType().equalsIgnoreCase("ITC")) {
                            ittId = tts.getTenderSectionId();
                        } else if (tts.getContentType().equalsIgnoreCase("GCC")) {
                            gccId = tts.getTenderSectionId();
                        }
                        if ((tts.getContentType().equalsIgnoreCase("TDS")) || (tts.getContentType().equalsIgnoreCase("PCC"))) {
                            tempURL = tempURL.replace("GenerateTendDocFreshPDF", "TenderTDSView");
                            if (tts.getContentType().equalsIgnoreCase("TDS")) {
                                tenderittSectionId = ittId;
                            } else if (tts.getContentType().equalsIgnoreCase("PCC")) {
                                tenderittSectionId = gccId;
                            }
                            reqQuery = "tenderStdId=" + tenderStdId + "&tenderId=" + tenderId + "&sectionId=" + tenderittSectionId;
                            folderName = pdfConst.TENDERDOC;
                            pdfId = tenderId + "_" + tenderittSectionId + "_" + tenderStdId;
                            //if(file.exists())
                            obj.genrateCmd(tempURL, reqQuery, folderName, pdfId);
                        }
                        if (tts.getContentType().equals("Form")) {
                            StringBuilder strForAllFormPDF = new StringBuilder();
                            java.util.List<com.cptu.egp.eps.model.table.TblTenderForms> forms = dDocSrBean.getTenderForm(tts.getTenderSectionId());
                            if (forms != null) {
                                if (!forms.isEmpty()) {
                                    tempURL = tempURL.replace("GenerateTendDocFreshPDF", "TestTenderForm");
                                    pdfId = tenderId + "_" + tts.getTenderSectionId() + "_" + tenderStdId;
                                    folderName = pdfConst.TENDERDOC;
                                    for (int jj = 0; jj < forms.size(); jj++) {
                                        //http://localhost:8080/eGPWeb/officer/ViewTenderCompleteForm.jsp?tenderId=1356&sectionId=538&formId=313
                                        reqQuery = "tenderId=" + tenderId + "&sectionId=" + tts.getTenderSectionId() + "&formId=" + forms.get(jj).getTenderFormId();

                                        strForAllFormPDF.append("\"" + tempURL + "?" + reqQuery + "&isPDF=true" + "\" ");
                                    }
                                    //if(!file.exists())
                                    obj.genrateCmdForm(strForAllFormPDF.toString(), "", folderName, pdfId);
                                }
                            }//end if(forms != null)
                        }
                    }
                }
            }
            flag = true;
        } catch (Exception e) {
            LOGGER.error("generatePdfOnLoad : " + e);
        }

        LOGGER.debug("generatePdfOnLoad : " + logUserId + "ends");
        return flag;

    }

    /**
     * fetching details for publish tender
     *
     * @param tenderId
     * @return List of TblTenderDetails
     * @throws Exception
     */
    public List<TblTenderDetails> getTenderDetails(int tenderId) throws Exception {
        LOGGER.debug("getTenderDetails : " + logUserId + "starts");
        List<TblTenderDetails> tDetails = new ArrayList<TblTenderDetails>();
        try {
            if (tDetails.isEmpty()) {
                for (TblTenderDetails details : tenderService.getTenderDetails(tenderId)) {
                    tDetails.add(details);
                }
            }
        } catch (Exception e) {
            LOGGER.error("getTenderDetails : " + e);
        }
        LOGGER.debug("getTenderDetails : " + logUserId + "ends");

        return tDetails;
    }

    /**
     * Method for send mail for corrigendum.
     *
     * @param tenderid
     */
    public void sendCorriDetailMail(String tenderid) {

        LOGGER.debug("sendCorriDetailMail : " + logUserId + "starts");
        String[] tmp = tenderCorriService.getEmailSMSforCorri(tenderid);
        String refNo = tmp[2];
        SendMessageUtil sendMessageUtil = new SendMessageUtil();
        if (tmp[0] != null) {
            String mails[] = tmp[0].split(",");
            MailContentUtility mailContentUtility = new MailContentUtility();
            String mailText = mailContentUtility.tenderCorriPub(tenderid, refNo);
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String msgBox = msgBoxContentUtility.tenderCorriPubMsgBox(tenderid, refNo);
            sendMessageUtil.setEmailSub("e-GP: Amendment Published in Tender Id:" + tenderid);
            sendMessageUtil.setEmailMessage(mailText);
            for (int i = 0; i < mails.length; i++) {
                String mail[] = {mails[i]};
                userRegisterService.contentAdmMsgBox(mails[i], XMLReader.getMessage("msgboxfrom"), "Amendment / Corrigendum published", msgBox);
                sendMessageUtil.setEmailTo(mail);
                sendMessageUtil.sendEmail();
                mail = null;
            }
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        /*if (tmp[1] != null) {
            String mobs[] = tmp[1].split(",");
            sendMessageUtil.setSmsBody("Dear User,%0AAmendment / Corrigendum has been published for the Id: " + tenderid + " Tender Ref No: " + refNo + ". Please visit e-GP portal for more details.");
            for (int i = 0; i < mobs.length; i++) {
                sendMessageUtil.setSmsNo(mobs[i]);
                sendMessageUtil.sendSMS();
            }
        }*/
        sendMessageUtil = null;
        LOGGER.debug("sendCorriDetailMail : " + logUserId + "ends");
    }

    /**
     * Get published corrigendum details.
     *
     * @param tenderId
     * @return corrigendum details
     * @throws Exception
     */
    public List<SPTenderCommonData> getPublishCorriDetails(int tenderId) throws Exception {
        LOGGER.debug("getPublishCorriDetails : " + logUserId + "starts");

        List<SPTenderCommonData> getPublishCorriDetails = null;
        try {
            getPublishCorriDetails = tenderCommonService.returndata("CorrigendumInfo", String.valueOf(tenderId), "PubCorriInfo");
        } catch (Exception e) {
            LOGGER.error("getPublishCorriDetails : " + e);
        }

        LOGGER.debug("getPublishCorriDetails : " + logUserId + "ends");
        return getPublishCorriDetails;

    }

    /**
     * Get published corrigendum document details.
     *
     * @param corriId
     * @return corrigendum document details
     * @throws Exception
     */
    public List<SPTenderCommonData> getPublishCorriDocDetails(String corriId) throws Exception {
        LOGGER.debug("getPublishCorriDocDetails : " + logUserId + "ends");
        List<SPTenderCommonData> getPublishCorriDocDetails = null;
        try {
            getPublishCorriDocDetails = tenderCommonService.returndata("CorrigendumInfo", corriId, "docInfo");
        } catch (Exception e) {
            LOGGER.error("getPublishCorriDocDetails : " + e);
        }

        LOGGER.debug("getPublishCorriDocDetails : " + logUserId + "ends");
        return getPublishCorriDocDetails;

    }

    /**
     * Get corrigendum detail information
     *
     * @param corriId
     * @return Corrigendum details
     * @throws Exception
     */
    public List<SPTenderCommonData> getPublishCorriValueDetails(String corriId) throws Exception {
        LOGGER.debug("getPublishCorriValueDetails : " + logUserId + "starts");
        List<SPTenderCommonData> getPublishCorriValueDetails = null;
        try {
            getPublishCorriValueDetails = tenderCommonService.returndata("CorrigendumInfo", corriId, "detailInfo");
        } catch (Exception e) {
            LOGGER.error("getPublishCorriValueDetails : " + e);
        }

        LOGGER.debug("getPublishCorriValueDetails : " + logUserId + "ends");
        return getPublishCorriValueDetails;

    }

    /**
     * Get corrigendum Documents details
     *
     * @param tenderId
     * @param corriId
     * @param lotId
     * @return document details
     * @throws Exception
     */
    public List<TblCorrigendumDocs> getCorrigendumDocs(int tenderId, int corriId, int lotId) throws Exception {
        //TenderCommonService  tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        LOGGER.debug("getCorrigendumDocs : " + logUserId + "starts");
        List<TblCorrigendumDocs> getCorrigendumDoc = null;
        try {
            getCorrigendumDoc = tenderService.getTenderLotCorriDoc(tenderId, corriId, lotId);
        } catch (Exception e) {
            LOGGER.error("getCorrigendumDocs : " + e);
        }

        LOGGER.debug("getCorrigendumDocs : " + logUserId + "ends");
        return getCorrigendumDoc;

    }

    /**
     * Bidder Search for mapping in case of LTM
     *
     * @param tenderid from tbl_tendermaster
     * @param dtBean
     * @param lotId from tbl_tenderLotSecurity
     * @param isLTM check for LTM
     * @param isSearch based on this listing is fetched
     * @return List of SPCommonSearchDataMore
     */
    public List<SPCommonSearchDataMore> searchBidderForMapping(String tenderid, MapTendererDtBean dtBean, boolean isSearch, String lotId, String isLTM, String procN) {
        LOGGER.debug("searchBidderForMapping : " + logUserId + "starts");
        StringBuilder searchString = new StringBuilder();
        /*if ("state".equals(field)) {
            searchString = "(tc.regOffState like '%" + value + "%' or state like '%" + value + "%')";
        } else if ("thana".equals(field)) {
            searchString = "(tc.regOffUpjilla like '%" + value + "%' or upJilla like '%" + value + "%')";
        } else if ("city".equals(field)) {
            searchString = "(tc.regOffCity like '%" + value + "%' or city like '%" + value + "%')";
        } else {
            searchString = field + " like '%" + value + "%'";
        }*/
        
        //Categorywise Bidder load
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        
        
        if (isSearch) {
            if (dtBean.getEmailId() != null && !dtBean.getEmailId().equals("")) {
                searchString.append("lm.emailId like '%" + dtBean.getEmailId() + "%' and ");
            }
            if (dtBean.getCompanyName() != null && !dtBean.getCompanyName().equals("")) {
                searchString.append("tc.companyName like '%" + dtBean.getCompanyName() + "%' and ");
            }
            //if (dtBean.getRegsNo() != null && !dtBean.getRegsNo().equals("")) {
            //    searchString.append("tc.companyRegNumber like '%" + dtBean.getRegsNo() + "%' and ");
            //}
            if (dtBean.getContryOfBusiness() != null && !dtBean.getContryOfBusiness().equals("")) {
                searchString.append("lm.businessCountryName like '%" + dtBean.getContryOfBusiness() + "%' and ");
            }
            if (dtBean.getComRegAddContry() != null && !dtBean.getComRegAddContry().equals("")) {
                searchString.append("tc.regOffCountry like '%" + dtBean.getComRegAddContry() + "%' and ");
            }
            if (dtBean.getComRegAddState() != null && !dtBean.getComRegAddState().equals("")) {
                searchString.append("tc.regOffState like '%" + dtBean.getComRegAddState() + "%' and ");
            }
            if (dtBean.getComRegCityTown() != null && !dtBean.getComRegCityTown().equals("")) {
                searchString.append("tc.regOffCity like '%" + dtBean.getComRegCityTown() + "%' and ");
            }
            if (dtBean.getComRegThanaUpzilla() != null && !dtBean.getComRegThanaUpzilla().equals("")) {
                searchString.append("tc.regOffUpjilla like '%" + dtBean.getComRegThanaUpzilla() + "%' and ");
            }
            if (dtBean.getCorHeadContry() != null && !dtBean.getCorHeadContry().equals("")) {
                searchString.append("tc.corpOffCountry like '%" + dtBean.getCorHeadContry() + "%' and ");
            }
            if (dtBean.getCorHeadState() != null && !dtBean.getCorHeadState().equals("")) {
                searchString.append("tc.corpOffState like '%" + dtBean.getCorHeadState() + "%' and ");
            }
            if (dtBean.getCorHeadCityTown() != null && !dtBean.getCorHeadCityTown().equals("")) {
                searchString.append("tc.corpOffCity like '%" + dtBean.getCorHeadCityTown() + "%' and ");
            }
            if (dtBean.getCorHeadThanaUpzilla() != null && !dtBean.getCorHeadThanaUpzilla().equals("")) {
                searchString.append("tc.corpOffUpjilla like '%" + dtBean.getCorHeadThanaUpzilla() + "%' and ");
            }
            if (dtBean.getPerAddContry() != null && !dtBean.getPerAddContry().equals("")) {
                searchString.append("tm.country like '%" + dtBean.getPerAddContry() + "%' and ");
            }
            if (dtBean.getPerAddState() != null && !dtBean.getPerAddState().equals("")) {
                searchString.append("tm.state like '%" + dtBean.getPerAddState() + "%' and ");
            }
            if (dtBean.getPerAddCityTown() != null && !dtBean.getPerAddCityTown().equals("")) {
                searchString.append("tm.city like '%" + dtBean.getPerAddCityTown() + "%' and ");
            }
            if (dtBean.getPerAddThanaUpzilla() != null && !dtBean.getPerAddThanaUpzilla().equals("")) {
                searchString.append("tm.upJilla like '%" + dtBean.getPerAddThanaUpzilla() + "%' and ");
            }
            searchString = new StringBuilder(searchString.substring(0, searchString.length() - 5));
        }
        List<SPCommonSearchDataMore> Result = null;
        String appLotId = "0";
        if (lotId != null) {
            appLotId = lotId;
        }

        if (isSearch) {
            Result = tenderCorriService.searchBidderForMapping(tenderid, searchString.toString(), appLotId, isLTM, procN);
        } else {
            Result = tenderCorriService.searchBidderForMapping(tenderid, null, appLotId, isLTM, procN);
        }        
    
        LOGGER.debug("searchBidderForMapping : " + logUserId + "ends");
        return Result;
    }

    /**
     * Map the Bidder for the Tender
     *
     * @param userid contains userId's of Bidder
     * @param tenderid from tbl_tendermaster
     * @param isMap check whether it maps or unmaps
     * @param lotId from tbl_TenderLotSecurity
     */
    public void mapBidderWithTender(String[] userid, int tenderid, boolean isMap, String lotId) {
        LOGGER.debug("mapBidderWithTender : " + logUserId + "starts");
        try {
            int appLotId = 0;
            if (lotId != null) {
                appLotId = Integer.parseInt(lotId);
            }
            tenderCorriService.mapUnmapBidderWithTender(userid, tenderid, isMap, appLotId);
        } catch (Exception e) {
            LOGGER.error("mapBidderWithTender : " + e);
        }

        LOGGER.debug("mapBidderWithTender : " + logUserId + "ends");
    }

    /**
     * Check whether Tender Forms are Ok or not
     *
     * @param tenderId from tbl_TenderMaster
     * @return whether Tender Forms are Ok or not
     */
    public SPTenderCommonData isTenderFormOk(int tenderId) {
        LOGGER.debug("isTenderFormOk : " + logUserId + "starts");
        SPTenderCommonData spTenCommData = null;
        try {
            List<SPTenderCommonData> lstCD = tenderCommonService.returndata("IsFormOK", tenderId + "", "");
            if (lstCD != null && !lstCD.isEmpty()) {
                spTenCommData = lstCD.get(0);
            }
        } catch (Exception ex) {
            LOGGER.error("isTenderFormOk : " + ex);

        }
        LOGGER.debug("isTenderFormOk : " + logUserId + "ends");
        return spTenCommData;

    }

    /**
     * Search for CPV
     *
     * @param tenderId from tbl_TenderMaster
     * @return List of procurementMethod,cpvCode & procurementType
     */
    public List<Object[]> getPMethodCPV(String tenderId) {
        LOGGER.debug("getPMethodCPV : " + logUserId + "starts");
        List<Object[]> getPMethodCPV = null;
        try {
            getPMethodCPV = tenderCorriService.getPMethodCPV(tenderId);
        } catch (Exception e) {
            LOGGER.error("getPMethodCPV : " + e);
        }
        LOGGER.debug("getPMethodCPV : " + logUserId + "ends");
        return getPMethodCPV;
    }
    
    public List<Object[]> getCompanyPermissions(String userId) {
        LOGGER.debug("getCompanyPermissions : " + logUserId + "starts");
        List<Object[]> permissions = null;
        try {
            permissions = tenderCommonService.getCompanyPermissions(userId);
        } catch (Exception e) {
            LOGGER.error("getCompanyPermissions : " + e);
        }
        LOGGER.debug("getCompanyPermissions : " + logUserId + "ends");
        return permissions;
    }

    /**
     * This method is for viewing Lot selection screen
     *
     * @param tenderId
     * @return List of TblTenderLotSecurity
     */
    public List<TblTenderLotSecurity> isMultilpleLot(int tenderId) {
        LOGGER.debug("isMultilpleLot : " + logUserId + "starts");
        List<TblTenderLotSecurity> isMultilpleLot = null;
        try {
            isMultilpleLot = tenderService.isMultipleLot(tenderId);
        } catch (Exception e) {
            LOGGER.error("isMultilpleLot : " + e);
        }
        LOGGER.debug("isMultilpleLot : " + logUserId + "ends");
        return isMultilpleLot;
    }

    /**
     * Records from TblBidderLots
     *
     * @param tenderId
     * @param userId
     * @return List of TblBidderLots
     */
    public List<TblBidderLots> viewBidderLots(int tenderId, int userId) {
        List<TblBidderLots> viewBidderLots = null;
        LOGGER.debug("viewBidderLots : " + logUserId + "starts");
        try {
            viewBidderLots = tenderService.viewBidderLots(tenderId, userId);
        } catch (Exception e) {
            LOGGER.error("viewBidderLots : " + e);
        }
        LOGGER.debug("viewBidderLots : " + logUserId + "ends");
        return viewBidderLots;
    }

    /**
     * Records from TblConfigDocFees (Business Rule Configure)
     *
     * @param tenderId from tbl_TenderMaster
     * @return List of TblConfigDocFees
     */
    public List<Object[]> getConfiForTender(int tenderId) {
        List<Object[]> details = null;
        LOGGER.debug("getConfiForTender : " + logUserId + "starts");
        try {
            details = tenderService.getConfiForTender(tenderId);
        } catch (Exception e) {
            LOGGER.error("viewBidderLots : " + e);
        }
        LOGGER.debug("getConfiForTender : " + logUserId + "ends");
        return details;
    }

    /**
     * Records from TblTenderDetails
     *
     * @param tenderId
     * @return List of TblTenderDetails
     */
    public List<TblTenderDetails> getCancelTenDet(int tenderId) {
        List<TblTenderDetails> details = null;
        LOGGER.debug("getCancelTenDet : " + logUserId + " Starts");
        try {
            details = tenderService.getCancelTenDet(tenderId);
        } catch (Exception e) {
            LOGGER.error("getCancelTenDet : " + e);
        }
        LOGGER.debug("getCancelTenDet : " + logUserId + " Ends");
        return details;
    }

    /**
     * Records from tbl_CancelTenderRequest
     *
     * @param tenderId
     * @return List of TblCancelTenderRequest
     */
    public List<TblCancelTenderRequest> getDetailsCancelTender(int tenderId) {
        List<TblCancelTenderRequest> details = null;
        LOGGER.debug("getDetailsCancelTender : " + logUserId + " Starts");
        try {
            details = tenderService.getDetailsCancelTender(tenderId);
        } catch (Exception e) {
            LOGGER.error("getDetailsCancelTender : " + e);
        }
        LOGGER.debug("getDetailsCancelTender : " + logUserId + " Ends");
        return details;
    }

    /**
     * Send Mail
     *
     * @param tenderid
     */
    public void sendTenderDetailMail(String tenderid) {

        LOGGER.debug("sendTenderDetailMail : " + logUserId + "starts");
        TblTenderDetails tblTenderDetails = new TblTenderDetails();
        try {
            tblTenderDetails = getTenderDetails(Integer.parseInt(tenderid)).get(0);

            String[] tmp = tenderCorriService.getEmailSMSforCorri(tenderid);
            String refNo = tmp[2];
            String peName = tmp[3];
            String closingDate = tmp[4];
            String tendBrief = tmp[5];
            String tenderValDays = tmp[7];
            String peAddress = tmp[8];
            String peDesignation = tmp[9];
            /*mobMail[6] = commonData.getFieldName7();//Tenderer Name
            mobMail[7] = commonData.getFieldName8();//Validity Days
            mobMail[8] = commonData.getFieldName9();//PE Adress
            mobMail[9] = commonData.getFieldName10();//PE Designation*/
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            if (tmp[0] != null) {
                String mails[] = tmp[0].split(",");
                String name[] = tmp[6].split(",");
                // MailContentUtility mailContentUtility = new MailContentUtility();
                //String mailText = mailContentUtility.tenderPub(tenderid, refNo,peName,closingDate,tendBrief);
                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();

                String tenderCap = "Tender/Proposal";
                if ("rfp".equalsIgnoreCase(tblTenderDetails.getEventType()) && "ltm".equalsIgnoreCase(tblTenderDetails.getProcurementMethod())) {
                    tenderCap = "e - RFP";
                } else if ("reoi".equalsIgnoreCase(tblTenderDetails.getEventType())) {
                    tenderCap = "e - RFOI";
                }
                String msgBox = msgBoxContentUtility.tenderPubMsgBox(tenderid, refNo, peName, closingDate, tendBrief, tenderCap);
                sendMessageUtil.setEmailSub("e-GP: " + tenderCap + " Published of Your Choice");
                /* Now mail will send using cron job so below code is not required*/
                //sendMessageUtil.setEmailMessage(mailText);
                for (int i = 0; i < mails.length; i++) {
                    String mail[] = {mails[i]};
                    if ("RFQU".equalsIgnoreCase(tblTenderDetails.getProcurementMethod()) || "RFQL".equalsIgnoreCase(tblTenderDetails.getProcurementMethod()) || "rfq".equalsIgnoreCase(tblTenderDetails.getProcurementMethod())) {
//                            if("Goods".equalsIgnoreCase(tblTenderDetails.getProcurementNature())){
//                                //String tenderid,String refNo,String peName,String closingDate,String tendBrief, String tenderValDays,String nameOfBidder,String peAddress,String peOfficeName,String peDesignation)
//                                msgBox = msgBoxContentUtility.tenderPubMsgBoxRFQUORRFQLGoods(tenderid,refNo,tblTenderDetails.getPeOfficeName(),closingDate,tendBrief,tenderValDays,name[i],tblTenderDetails.getPeAddress().replace("border=\"0\"", "border=\"1\"").replace("width=\"100%\"", "width=\"50%\" class=\"t-align-center\"").replace("width=\"30%\"","width=\"20%\"").replace("width=\"70%\"","width=\"30%\"").replaceAll(":", ""),tblTenderDetails.getPeName(),peDesignation);
//                            }else if("Works".equalsIgnoreCase(tblTenderDetails.getProcurementNature())){
//                                msgBox = msgBoxContentUtility.tenderPubMsgBoxRFQUORRFQLWorks(tenderid,refNo,tblTenderDetails.getPeOfficeName(),closingDate,tendBrief,tenderValDays,name[i],tblTenderDetails.getPeAddress().replace("border=\"0\"", "border=\"1\"").replace("width=\"100%\"", "width=\"50%\" class=\"t-align-center\"").replace("width=\"30%\"","width=\"20%\"").replace("width=\"70%\"","width=\"30%\"").replaceAll(":", ""),tblTenderDetails.getPeName(),peDesignation);
//                            }
                        if (tmp[1] != null) {
                            String mobs[] = tmp[1].split(",");
                            //sendMessageUtil.setSmsBody("Dear User, You are invited to participate a RFQ being published. Please logon to "+XMLReader.getMessage("url")+" for more detail.");
                            //For Testing purpose below one after testing remove below and above one is for live .
                            sendMessageUtil.setSmsBody("Dear User, You are invited to participate a LEM being published. Please logon to " + XMLReader.getMessage("url"));
                            for (int j = 0; j < mobs.length; j++) {
                                sendMessageUtil.setSmsNo(mobs[j]);
                                sendMessageUtil.sendSMS();
                            }
//                            }
//                        }else if("rfq".equalsIgnoreCase(tblTenderDetails.getProcurementMethod())){
                            sendMessageUtil.setEmailSub("e-GP: e-LEM  notice of your interest");
                            msgBox = msgBoxContentUtility.tenderPubMsgBoxRFQTender(tenderid, refNo, closingDate, tendBrief, tblTenderDetails.getPeOfficeName());
//                                String mobs[] = tmp[1].split(",");
//                                sendMessageUtil.setSmsBody("Dear User,%0AYou are invited to participate an RFQ being publsihed. Please logon to "+XMLReader.getMessage("url")+" for more detail.");
//                                for (int j = 0; j < mobs.length; j++) {
//                                    sendMessageUtil.setSmsNo(mobs[j]);
//                                    sendMessageUtil.sendSMS();
                        }

                    } else if ("rfp".equalsIgnoreCase(tblTenderDetails.getEventType()) && !"Open".equalsIgnoreCase(tblTenderDetails.getDocAccess())) {
                        sendMessageUtil.setEmailSub("e-GP:  e-Letter of Invitation for e-Request for Proposal (e-RFP)");
                        msgBox = msgBoxContentUtility.tenderPubMsgBoxRFPTender(tenderid, refNo, closingDate, tendBrief, tblTenderDetails.getPeOfficeName(), name);
                    }
                    userRegisterService.contentAdmMsgBox(mails[i], XMLReader.getMessage("msgboxfrom"), sendMessageUtil.getEmailSub(), msgBox);
                    /* Now mail will send using cron job so below code is not required*/
                    //sendMessageUtil.setEmailTo(mail);
                    // sendMessageUtil.sendEmail();
                    mail = null;
                }
                // mailContentUtility = null;
                msgBoxContentUtility = null;
            }
            /*if (tmp[1] != null) {
                    String mobs[] = tmp[1].split(",");
                    sendMessageUtil.setSmsBody("Dear User,%0ATender has been published for the Id: " + tenderid + " Tender Ref No: " + refNo + ". Please visit e-GP portal for more details.");
                    for (int i = 0; i < mobs.length; i++) {
                        sendMessageUtil.setSmsNo(mobs[i]);
                        sendMessageUtil.sendSMS();
                    }
                }*/
            // sendMessageUtil=null;
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("sendTenderDetailMail : " + e.getMessage());
        }
        LOGGER.debug("sendTenderDetailMail : " + logUserId + "ends");
    }

    /**
     * This method is used for cheking tender is live or not
     *
     * @param tenderId
     * @return if true then live else false
     */
    public boolean chkTenderStatus(String tenderId) {
        LOGGER.debug("chkTenderStatus : " + logUserId + " Starts");
        boolean flag = false;
        try {
            flag = tenderService.chkTenderStatus(Integer.parseInt(tenderId));
        } catch (Exception e) {
            LOGGER.error("chkTenderStatus : " + logUserId + " : " + e);
        }
        LOGGER.debug("chkTenderStatus : " + logUserId + " Ends");
        return flag;
    }

    /**
     * This method is used for fetching tender status
     *
     * @param tenderId
     * @return list for tender details
     */
    public List<TblTenderDetails> getTenderStatus(String tenderId) {
        LOGGER.debug("getTenderStatus : " + logUserId + " Starts");
        List<TblTenderDetails> list = null;
        try {
            list = tenderService.getTenderStatus(Integer.parseInt(tenderId));
        } catch (Exception e) {
            LOGGER.error("getTenderStatus : " + logUserId + " : " + e);
        }
        LOGGER.debug("getTenderStatus : " + logUserId + " Ends");
        return list;
    }

    /**
     * this method is for geting allocated budget cost for notice
     *
     * @param tenderId
     * @return allocated budget cost
     */
    public List<Object> getAllocatedCost(int tenderId) {
        LOGGER.debug("getAllocatedCost : " + logUserId + "starts");
        List<Object> list = null;
        try {
            list = tenderService.getAllocatedCost(tenderId);
        } catch (Exception e) {
            LOGGER.error("getAllocatedCost : " + e);
        }
        LOGGER.debug("getAllocatedCost : " + logUserId + "ends");
        return list;
    }

    /**
     * this method is for updating Domestic information
     *
     * @param tenderId from tbl_TenderMaster
     * @param domesticPref preference
     * @param domesticPercent percentage
     * @return true or false
     */
    public boolean insertDomestic(int tenderId, String domesticPref, String domesticPercent) {
        LOGGER.debug("insertDomestic : " + logUserId + " Starts");
        boolean flag = false;
        try {
            flag = tenderService.insertDomestic(tenderId, domesticPref, domesticPercent);
        } catch (Exception e) {
            LOGGER.error("insertDomestic : " + e);
        }
        LOGGER.debug("insertDomestic : " + logUserId + " Ends");
        return flag;
    }

    /**
     * this method is for checking data available or not
     *
     * @param tenderId
     * @return list of data
     */
    public List<SPCommonSearchDataMore> isAvalTenSecAmt(String tenderId) {
        LOGGER.debug("isAvalTenSecAmt : " + logUserId + " Starts");
        List<SPCommonSearchDataMore> sPCommonSearchDataMores = null;
        try {
            sPCommonSearchDataMores = commonSearchDataMoreService.geteGPData("isAvailTenSecAmt", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        } catch (Exception e) {
            LOGGER.error("isAvalTenSecAmt : " + e);
        }
        LOGGER.debug("isAvalTenSecAmt : " + logUserId + " Ends");
        return sPCommonSearchDataMores;
    }

    /**
     * this method is for checking data available or not
     *
     * @param tenderId from tbl_TenderMaster
     * @param pcklotid from tbl_TenderLotSecurity
     * @param chkPckLot for checking package or lot
     * @return list of data
     */
    public int chkTblLimitedTenders(String tenderId, int pcklotid, boolean chkPckLot) {
        LOGGER.debug("chkTblLimitedTenders : " + logUserId + " Starts");
        int size = 0;
        List<TblLimitedTenders> tblLimitedTenderses = null;
        try {
            tblLimitedTenderses = tenderCorriService.chkTblLimitedTenders(Integer.parseInt(tenderId), pcklotid, chkPckLot);
            if (!tblLimitedTenderses.isEmpty()) {
                size = tblLimitedTenderses.size();
            }
        } catch (Exception e) {
            LOGGER.error("chkTblLimitedTenders : " + e);
        }
        LOGGER.debug("chkTblLimitedTenders : " + logUserId + " Ends");
        return size;
    }

    /**
     * For defining Financial and Technical weight for Tender
     *
     * @param weightTec Technical weight %
     * @param weightFin Financial weight %
     * @param tenderId from tbl_tenderMaster
     * @return true or false for success or fail
     */
    public boolean addEvalServiceWeightage(int weightTec, int weightFin, int tenderId) {
        LOGGER.debug("addEvalServiceWeightage : " + logUserId + " Starts");
        boolean flag = false;
        TblEvalServiceWeightage tblEvalServiceWeightage = new TblEvalServiceWeightage();
        tblEvalServiceWeightage.setFinancialWeight(weightFin);
        tblEvalServiceWeightage.setTechWeight(weightTec);
        tblEvalServiceWeightage.setTblTenderMaster(new TblTenderMaster(tenderId));
        flag = tenderService.addEvalServiceWeightage(tblEvalServiceWeightage);
        LOGGER.debug("addEvalServiceWeightage : " + logUserId + " End");
        return flag;
    }

    /**
     * For updating Financial and Technical weight for Tender
     *
     * @param weightTec Technical weight %
     * @param weightFin Financial weight %
     * @param tenderId from tbl_tenderMaster
     * @param weightId for which updation is to be done
     * @return true or false for success or fail
     */
    public boolean updateEvalServiceWeightage(int weightTec, int weightFin, int tenderId, int weightId) {
        LOGGER.debug("updateEvalServiceWeightage : " + logUserId + " Starts");
        boolean flag = false;
        TblEvalServiceWeightage tblEvalServiceWeightage = new TblEvalServiceWeightage();
        tblEvalServiceWeightage.setServWeightId(weightId);
        tblEvalServiceWeightage.setFinancialWeight(weightFin);
        tblEvalServiceWeightage.setTechWeight(weightTec);
        tblEvalServiceWeightage.setTblTenderMaster(new TblTenderMaster(tenderId));
        flag = tenderService.updateEvalServiceWeightage(tblEvalServiceWeightage);
        LOGGER.debug("updateEvalServiceWeightage : " + logUserId + " End");
        return flag;
    }

    /**
     * Getting NOA Issue Details for given NOA ISSUE ID.
     *
     * @param noaIssueId = TblNoaIssueDetails.noaIssueId
     * @return object of TblNoaAcceptance
     */
    public TblNoaAcceptance getTblNoaAcceptance(int noaIssueId) {
        TblNoaAcceptance tblNoaAcceptance = new TblNoaAcceptance();
        tblNoaAcceptance = tenderService.getNoaAcceptance(noaIssueId);
        return tblNoaAcceptance;
    }

    /**
     * Changing Configure Mark Status.
     *
     * @param tenderId = TblEvalServiceForms.tenderId &
     * TblEvalServiceForms.tenderId
     * @return true/false weather successfully status change or not.
     */
    public boolean chngStatusMark(String tenderId) {
        return tenderCorriService.chngConfigMarkStatus(tenderId);
    }

    public boolean serLotSercurity(String tenderId, String amt) {
        boolean flag = false;
        try {
            flag = tenderService.serTenderSecurity(tenderId, amt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public String TenderReportStatus(String tenderId) {
        String TenderStatus = "";
        try {
            TenderStatus = tenderService.getTenderReportStatus(tenderId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return TenderStatus;
    }

    public boolean isTenderSectionsUpdated(String tenderId, String corriId) {
        return tenderCorriService.isTenderSectionsUpdated(tenderId, corriId);
    }

    public List<SPTenderCommonData> TenderPEuserIdandName(String tenderId) {
        List<SPTenderCommonData> details = null;
        try {
            details = tenderCommonService.returndata("getTenderPEUserIDName", String.valueOf(tenderId), "");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("TenderPublishPEUseIDName " + e.getMessage());
        }
        return details;
    }

    /**
     * Map the PE for the Package
     *
     * @param appid contains APP id
     * @param packageId contains package Id
     * @param peofficeId contains PE office Id
     * @param officeName contains office Name
     * @param state check contains state
     * @param address from contains address
     * @param isMap from check
     */
    public void mapPEWithPackage(int appid, int packageId, String[] peofficeId, String[] officeName, String[] state, String[] address, boolean isMap) {
        LOGGER.debug("mapPEWithPackage : " + logUserId + "starts");
        try {
            tenderCorriService.mapUnmapPEWithPackage(appid, packageId, peofficeId, officeName, state, address, isMap);
        } catch (Exception e) {
            LOGGER.error("mapPEWithPackage : " + e);
        }
        LOGGER.debug("mapPEWithPackage : " + logUserId + "ends");
    }
    
    public List<TblAppFrameworkOffice> getAppFrameworkOfficeByPkgId (int appId, int packageId) throws Exception{
        
        return tenderCorriService.findAppFrameworkOffice("appid", Operation_enum.EQ, appId, "packageId", Operation_enum.EQ, packageId);
    }
    
     public List<TblAppFrameworkOffice> getAppFrameworkOfficeByPkgId (int packageId) throws Exception{
        
        return tenderCorriService.findAppFrameworkOffice("packageId", Operation_enum.EQ, packageId);
    }
        
    public TblTenderMaster getTenderMasterData(int tendID){
        return tenderService.getTenderMasterDetails(tendID);
    }
    public void addTblTenderFrameWork(int appid, int packageId,int tenderId,String[] peofficeId, String[] officeName,String[] OfficeDistrict, String[] OfficeAddress, String[] ItemName,String[] Quantity, String[] Unit, int UserID, boolean isMap){
        tenderFrameWorkService.addTblTenderFrameWork(appid, packageId, tenderId, peofficeId, officeName, OfficeDistrict, OfficeAddress, ItemName, Quantity, Unit, UserID, isMap);
    }
    public List<TblTenderFrameWork> findTblTenderFrameWork(int tenderId) throws Exception {
        return tenderFrameWorkService.findTblTenderFrameWork("tenderId", Operation_enum.EQ, tenderId);
    }
}

