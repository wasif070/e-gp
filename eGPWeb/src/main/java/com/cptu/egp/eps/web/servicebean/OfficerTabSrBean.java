/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblEvalReportMaster;
import java.util.ArrayList;
import java.util.List;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import com.cptu.egp.eps.service.serviceimpl.OfficerTabServiceImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class OfficerTabSrBean {

    final Logger logger = Logger.getLogger(NegotiationProcessSrBean.class);
    private String logUserId ="0";
    OfficerTabServiceImpl tabServiceImpl = (OfficerTabServiceImpl) AppContext.getSpringBean("OfficerTabServiceImpl");
    
    /**
     * For logging purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        tabServiceImpl.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /**
     * Get Procurement Nature using tenderId
     * @param tenderId tbl_tenderMaster
     * @return List of TblTenderDetails
     */
    public List<TblTenderDetails> getProcurementNature(int tenderId) {
        logger.debug("getProcurementNature : "+logUserId+" Starts");
        
        List<TblTenderDetails> tblTenderDetails = new ArrayList<TblTenderDetails>();
        try {
            if (tblTenderDetails.isEmpty()) {
                for (TblTenderDetails tDetails : tabServiceImpl.getProcurementNature(tenderId)) {
                    tblTenderDetails.add(tDetails);
                }
            }
            //return tabServiceImpl.getProcurementNature(tenderId);
        } catch (Exception ex) {
            logger.error("getProcurementNature : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getProcurementNature : "+logUserId+" Ends");
        return tblTenderDetails;
    }

    /**
     * Get status of approving authority
     * @param tenderId tbl_tenderMaster
     * @return List of TblEvalReportMaster
     */
    public List<TblEvalReportMaster> getStatusAA(int tenderId) {
        logger.debug("getStatusAA : "+logUserId+" Starts");
        List<TblEvalReportMaster> tblEvalReportMaster = null;
        try {
            if (tblEvalReportMaster.isEmpty()) {
                for (TblEvalReportMaster tStatus : tabServiceImpl.getStatusAA(tenderId)) {
                    tblEvalReportMaster.add(tStatus);
                }
            }
        } catch (Exception ex) {
            logger.error("getStatusAA : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getStatusAA : "+logUserId+" Ends");
        return tblEvalReportMaster;
    }
}
