/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblIttHeader;
import com.cptu.egp.eps.model.table.TblTdsSubClause;
import com.cptu.egp.eps.service.serviceinterface.PrepareTds;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author yanki
 */
public class PrepareTDSSrBean extends HttpServlet {

    final static Logger LOGGER = Logger.getLogger(PrepareTDSSrBean.class);
    private String logUserId ="0";

    PrepareTds prepareTds = (PrepareTds) AppContext.getSpringBean("prepareTdsService");

    public void setLogUserId(String logUserId) {
        prepareTds.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        LOGGER.debug("processRequest : "+logUserId+" Starts");
        try {
            HttpSession session = request.getSession();
            if(session.getAttribute("userId")!=null){
                logUserId = session.getAttribute("userId").toString();
                prepareTds.setLogUserId(logUserId);
            }
            if(request.getParameter("action") != null){
                if(request.getParameter("action").equalsIgnoreCase("save")){
                    LOGGER.debug("processRequest action : save : "+logUserId+" Starts");
                    
                    int total = Integer.parseInt(request.getParameter("total"));
                    for(int i=1; i<=total; i++){
                        TblTdsSubClause tblTdsSubClause = new TblTdsSubClause();
                        tblTdsSubClause.setOrderNumber((short)i);
                        tblTdsSubClause.setTdsSubClauseName(handleSpecialCharHere(request.getParameter("tds_" + i)));
                        String hdTdsInfo = request.getParameter("hd_tds_" + i);
                        int ittHeaderId = 0;
                        try{
                            if(hdTdsInfo.split("_").length == 3){
                                ittHeaderId = Integer.parseInt(hdTdsInfo.split("_")[0]);
                                //String ittClauseId = hdTdsInfo.split(hdTdsInfo)[1];
                                int ittSubClauseId = Integer.parseInt(hdTdsInfo.split("_")[2]);
                                tblTdsSubClause.setIttReference(ittSubClauseId);
                            }else{
                                ittHeaderId = Integer.parseInt(hdTdsInfo);
                            }
                        }catch(Exception ex){
                            LOGGER.error(request.getParameter("action") + logUserId + ex);
                        }
                        tblTdsSubClause.setTblIttHeader(new TblIttHeader(ittHeaderId));
                        int flag = prepareTds.insertTdsSubClause(tblTdsSubClause);
                        tblTdsSubClause = null;
                    }
                    LOGGER.debug("processRequest action : save : "+logUserId+" ends");
                    response.sendRedirect("admin/TDSDashBoard.jsp?sectionId=" + request.getParameter("sectionId") + "&templateId=" + request.getParameter("templateId"));
                    //response.sendRedirectFilter("TDSDashBoard.jsp?sectionId=" + request.getParameter("sectionId") + "&templateId=" + request.getParameter("templateId"));
                } else if(request.getParameter("action").equalsIgnoreCase("update")){
                    LOGGER.debug("processRequest action : update : "+logUserId+" Starts");
                    String tdsInfo = request.getParameter("tdsInfo");
                    if(tdsInfo != null){
                        tdsInfo = handleSpecialCharHere(tdsInfo);
                    }
                    int tdsSubClauseId = Integer.parseInt(request.getParameter("tdsSubClauseId"));
                    int headerId = Integer.parseInt(request.getParameter("headerId"));
                    TblTdsSubClause tblTdsSubClause = new TblTdsSubClause();

                    tblTdsSubClause.setTdsSubClauseId(tdsSubClauseId);
                    tblTdsSubClause.setTblIttHeader(new TblIttHeader(headerId));
                    tblTdsSubClause.setTdsSubClauseName(tdsInfo);
                    boolean flag = prepareTds.updateTDSSubClause(tblTdsSubClause);
                    if(flag){
                        out.print("OK");
                    } else {
                        out.print("error");
                    }
                    LOGGER.debug("processRequest action : update : "+logUserId+" ends");
                } else if(request.getParameter("action").equalsIgnoreCase("delete")){
                    LOGGER.debug("processRequest action : delete : "+logUserId+" Starts");
                    
                    int tdsSubClauseId = Integer.parseInt(request.getParameter("tdsSubClauseId"));
                    int headerId = Integer.parseInt(request.getParameter("headerId"));
                    TblTdsSubClause tblTdsSubClause = new TblTdsSubClause();

                    tblTdsSubClause.setTdsSubClauseId(tdsSubClauseId);
                    tblTdsSubClause.setTblIttHeader(new TblIttHeader(headerId));
                    boolean flag = prepareTds.deleteTDSSubClause(tblTdsSubClause);
                    if(flag){
                        out.print("OK");
                    } else {
                        out.print("error");
                    }
                    LOGGER.debug("processRequest action : delete : "+logUserId+" ends");
                } else if(request.getParameter("action").equalsIgnoreCase("add")){
                    LOGGER.debug("processRequest action : add : "+logUserId+" Starts");
                    int ittSubClauseId = 0;
                    int headerId = Integer.parseInt(request.getParameter("headerId"));
                    String tdsInfo = request.getParameter("tdsInfo");
                    if(tdsInfo != null){
                        tdsInfo = handleSpecialCharHere(tdsInfo);
                    }
                    short orderNo = Short.parseShort(request.getParameter("orderNo"));
                    TblTdsSubClause tblTdsSubClause = new TblTdsSubClause();

                    tblTdsSubClause.setIttReference(ittSubClauseId);
                    tblTdsSubClause.setTdsSubClauseName(tdsInfo);
                    tblTdsSubClause.setTblIttHeader(new TblIttHeader(headerId));
                    tblTdsSubClause.setOrderNumber(++orderNo);

                    int flag = prepareTds.insertTdsSubClause(tblTdsSubClause);
                    if(flag > 0){
                        out.print("OK,"+flag);
                    } else {
                        out.print("error");
                    }
                     LOGGER.debug("processRequest action : add : "+logUserId+" ends");
                }  else if(request.getParameter("action").equalsIgnoreCase("addFirst")){
                    LOGGER.debug("processRequest action : addFirst : "+logUserId+" Starts");
                    int ittSubClauseId = 0;
                    int headerId = Integer.parseInt(request.getParameter("headerId"));
                    String tdsInfo = request.getParameter("tdsInfo");
                    if(tdsInfo != null){
                        tdsInfo = handleSpecialCharHere(tdsInfo);
                    }
                    short orderNo = Short.parseShort(request.getParameter("orderNo"));
                    ittSubClauseId = Integer.parseInt(request.getParameter("ittsubclauseid"));

                   
                    TblTdsSubClause tblTdsSubClause = new TblTdsSubClause();

                    tblTdsSubClause.setIttReference(ittSubClauseId);
                    tblTdsSubClause.setTdsSubClauseName(tdsInfo);
                    tblTdsSubClause.setTblIttHeader(new TblIttHeader(headerId));
                    tblTdsSubClause.setOrderNumber(++orderNo);

                    int flag = prepareTds.insertTdsSubClause(tblTdsSubClause);
                    if(flag > 0){
                        out.print("OK,"+flag);
                    } else {
                        out.print("error");
                    }
                    LOGGER.debug("processRequest action : addFirst : "+logUserId+" ends");
                }
            } else {
                response.sendRedirect(request.getQueryString() + "/index.jsp?errorAtPage=PrepareTDSSrBean");
            }
        } catch(Exception ex){
            LOGGER.error("processRequest : "+logUserId+" : "+ex.toString());
        } finally {
            out.close();
        }
        LOGGER.debug("processRequest : "+logUserId+" ends");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public List<SPTenderCommonData> getTDSSubClause(int ittHeaderId){
        LOGGER.debug("getTDSSubClause : "+logUserId+" Starts");
        List<SPTenderCommonData> tblTdsSubClause = null;
        try{
            tblTdsSubClause = prepareTds.getTDSSubClause(ittHeaderId);
        }catch(Exception ex){
           LOGGER.error("getTDSSubClause : "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("getTDSSubClause : "+logUserId+" Ends");
        return tblTdsSubClause ;
    }

    public boolean isTdsSubClauseGenerated(int ittHeaderId){
        LOGGER.debug("isTdsSubClauseGenerated : "+logUserId+" Starts");
        boolean isTdsSubClauseGenerated = false;
        try{
            isTdsSubClauseGenerated = prepareTds.isTdsSubClauseGenerated(ittHeaderId);
        }catch(Exception ex){
            LOGGER.error("isTdsSubClauseGenerated : "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("isTdsSubClauseGenerated : "+logUserId+" Ends");
        return isTdsSubClauseGenerated ;
    }

    public static String handleSpecialCharHere(String str){
        LOGGER.debug("handleSpecialCharHere : " + " Starts");
        String msg = "";
        try {
            msg = str.replaceAll("'", "&rsquo;");
        } catch (Exception e) {
            LOGGER.error("handleSpecialCharHere : " + e);
    }
        LOGGER.debug("handleSpecialCharHere : " + " Ends");
        return msg;
    }
}
