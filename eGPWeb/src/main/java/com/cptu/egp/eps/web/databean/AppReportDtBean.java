/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

import java.math.BigDecimal;

/**
 *
 * @author Sanjay Prajapati
 */
public class AppReportDtBean {

    public AppReportDtBean() {

        this.appId = 0;
        this.packageId = 0;
        this.packageNo = "";
        this.procMethod = "";
        this.ministryDetails = "";
        this.projectNameCode = "";
        this.procType = "";
        this.appAut = "";
        this.srcFund = "";
        this.estCost = new BigDecimal(0);
        this.totEstCost = new BigDecimal(0);
        this.formattedEstCost = "";
        this.formattedtotEstCost = "";
        this.isPQRequired = "";

        this.gApplstdt = "";
        this.gPlanDtInAdvtDt = "";
        this.gPlanDyInAdvtDy = 0;
        this.gActDtInAdvtDt = "";
        this.gPlanDtTenderOpenDt = "";
        this.gPlanDyTenderOpenDy = 0;
        this.gActDtTenderOpenDt = "";
        this.gPlanDtTenderEvalDt = "";
        this.gPlanDyTenderEvalDy = 0;
        this.gActDtTenderEvalDt = "";
        this.gPlanDtAppAwardDt = "";
        this.gPlanDyAppAwardDy = 0;
        this.gActDtAppAwardDt = "";
        //Code by Proshanto Kumar Saha
        this.gPlanDtLintAwardDt = "";
        this.gPlanDyLintAwardDy = 0;
        //Code End
        this.gActDtLintAwardDt = "";
        this.gPlanDtNtiAwardDt = "";
        this.gPlanDyNtiAwardDy = 0;
        this.gActDtNtiAwardDt = "";
        this.gPlanDtSignCtcDt = "";
        this.gPlanDySignCtcDy = 0;
        this.gActDtSignCtcDt = "";
        this.gPlanDyTotTimeSignContractDy = 0;
        this.gPlanDtTotTimeComContractDt = "";
        this.gPlanDyTotTimeComContractDy = 0;
        this.wPlanDtAdvtPreDt = "";
        this.wPlanDyAdvtPreDy = 0;
        this.wActDtAdvtPreDt = "";
        this.wPlanDtInitAdvtDt = "";
        this.wPlanDyInitAdvtDy = 0;
        this.wActDtAdvtDt = "";
        this.wPlanDtTenderOpenDt = "";
        this.wPlanDyTenderOpenDy = 0;
        this.wActDtTenderOpenDt = "";
        this.wPlanDtTenderEvalDt = "";
        this.wPlanDyTenderEvalDy = 0;
        this.wActDtTenderEvalDt = "";
        this.wPlanDtAppAwardDt = "";
        this.wPlanDyAppAwardDy = 0;
        this.wActDtAppAwardDt = "";
        //Code by Proshanto
        this.wPlanDtLintAwardDt = "";
        this.wPlanDyLintAwardDy = 0;
        //End
        this.wActDtLintAwardDt = "";
        this.wPlanDtNtiAwardDt = "";
        this.wPlanDyNtiAwardDy = 0;
        this.wActDtNtiAwardDt = "";
        this.wPlanDtSignCtcDt = "";
        this.wPlanDySignCtcDy = 0;
        this.wActDtSignCtcDt = "";
        this.wPlanDyTotTimeSignContractDy = 0;
        this.wPlanDtTotTimeComContractDt = "";
        this.wPlanDyTotTimeComContractDy = 0;
        this.sPlanDtAdvtEOIDt = "";
        this.sPlanDyAdvtEOIDy = 0;
        this.sActDtAdvtEOIDt = "";
        this.sPlanDtIsuRFPDt = "";
        this.sPlanDyIsuRFPDy = 0;
        this.sActDtIsuRFPDt = "";
        this.sPlanDtTechProOpenDt = "";
        this.sPlanDyTechProOpenDy = 0;
        this.sActDtTechProOpenDt = "";
        this.sPlanDtTechProEvalDt = "";
        this.sPlanDyTechProEvalDy = 0;
        this.sActDtTechProEvalDt = "";
        this.sPlanDtFinProEvalDt = "";
        this.sPlanDyFinProEvalDy = 0;
        this.sActDtFinProEvalDt = "";
        this.sPlanDtNegoDt = "";
        this.sPlanDyNegoDy = 0;
        this.sActDtNegoDt = "";
        this.sPlanDtAppDt = "";
        this.sPlanDyAppDy = 0;
        this.sActDtAppDt = "";
        this.sPlanDtSignCtcDt = "";
        this.sPlanDySignCtcDy = 0;
        this.sActDtSignCtcDt = "";
        this.sPlanDyTotTimeSignContractDy = 0;
        this.sPlanDtTotTimeComContractDt = "";
        this.sPlanDyTotTimeComContractDy = 0;
    }

    public String getFormattedEstCost() {
        return formattedEstCost;
    }

    public void setFormattedEstCost(String formattedEstCost) {
        this.formattedEstCost = formattedEstCost;
    }

    public String getFormattedtotEstCost() {
        return formattedtotEstCost;
    }

    public void setFormattedtotEstCost(String formattedtotEstCost) {
        this.formattedtotEstCost = formattedtotEstCost;
    }

    private int appId;
    private int packageId;
    private String ministryDetails;
    private String projectNameCode;
    private String packageNo;
    private String procMethod;
    private String procType;
    private String appAut;
    private String srcFund;
    private BigDecimal estCost;
    private BigDecimal totEstCost;
    private String formattedEstCost;
    private String formattedtotEstCost;
    private String isPQRequired;

    //Goods
    private String gAdvtDt;
    private Short gAdvtDy;
    private String gApplstdt;
    private String gPlanDtInAdvtDt;
    private Short gPlanDyInAdvtDy;
    private String gActDtInAdvtDt;

    private String gPlanDtTenderOpenDt;
    private Short gPlanDyTenderOpenDy;
    private String gActDtTenderOpenDt;

    private String gPlanDtTenderEvalDt;
    private Short gPlanDyTenderEvalDy;
    private String gActDtTenderEvalDt;

    private String gPlanDtAppAwardDt;
    private Short gPlanDyAppAwardDy;
    private String gActDtAppAwardDt;
    //Code by Proshanto Kumar Saha
    private String gPlanDtLintAwardDt;
    private String gActDtLintAwardDt;
    private Short gPlanDyLintAwardDy;
    //Code End 
    private String gPlanDtNtiAwardDt;
    private Short gPlanDyNtiAwardDy;
    private String gActDtNtiAwardDt;

    private String gPlanDtSignCtcDt;
    private Short gPlanDySignCtcDy;
    private String gActDtSignCtcDt;

    // Total Time for Goods
    private Short gPlanDyTotTimeSignContractDy;
    private String gPlanDtTotTimeComContractDt;
    private Short gPlanDyTotTimeComContractDy;

    // Works
    private String wAdvtDt;
    private Short wAdvtDy;
    private String wPlanDtAdvtPreDt;
    private Short wPlanDyAdvtPreDy;
    private String wActDtAdvtPreDt;

    private String wPlanDtInitAdvtDt;
    private Short wPlanDyInitAdvtDy;
    private String wActDtAdvtDt;

    private String wPlanDtTenderOpenDt;
    private Short wPlanDyTenderOpenDy;
    private String wActDtTenderOpenDt;

    private String wPlanDtTenderEvalDt;
    private Short wPlanDyTenderEvalDy;
    private String wActDtTenderEvalDt;

    private String wPlanDtAppAwardDt;
    private Short wPlanDyAppAwardDy;
    private String wActDtAppAwardDt;

    //Code by Proshanto
    private String wPlanDtLintAwardDt;
    private String wActDtLintAwardDt;
    private Short wPlanDyLintAwardDy;
    //End
    private String wPlanDtNtiAwardDt;
    private Short wPlanDyNtiAwardDy;
    private String wActDtNtiAwardDt;

    private String wPlanDtSignCtcDt;
    private Short wPlanDySignCtcDy;
    private String wActDtSignCtcDt;

    // Total Time for Works
    private Short wPlanDyTotTimeSignContractDy;
    private String wPlanDtTotTimeComContractDt;
    private Short wPlanDyTotTimeComContractDy;

    // Services
    private String sAdvtDt;
    private Short sAdvtDy;
    private String sPlanDtAdvtEOIDt;
    private Short sPlanDyAdvtEOIDy;
    private String sActDtAdvtEOIDt;

    private String sPlanDtIsuRFPDt;
    private Short sPlanDyIsuRFPDy;
    private String sActDtIsuRFPDt;

    private String sPlanDtTechProOpenDt;
    private Short sPlanDyTechProOpenDy;
    private String sActDtTechProOpenDt;

    private String sPlanDtTechProEvalDt;
    private Short sPlanDyTechProEvalDy;
    private String sActDtTechProEvalDt;

    private String sPlanDtFinProEvalDt;
    private Short sPlanDyFinProEvalDy;
    private String sActDtFinProEvalDt;

    private String sPlanDtNegoDt;
    private Short sPlanDyNegoDy;
    private String sActDtNegoDt;

    private String sPlanDtAppDt;
    private Short sPlanDyAppDy;
    private String sActDtAppDt;

    private String sPlanDtSignCtcDt;
    private Short sPlanDySignCtcDy;
    private String sActDtSignCtcDt;

    // Total Time for Services
    private Short sPlanDyTotTimeSignContractDy;
    private String sPlanDtTotTimeComContractDt;
    private Short sPlanDyTotTimeComContractDy;

    public int getAppId() {
        return appId;
    }

    public void setAppId(int appId) {
        this.appId = appId;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public String getAppAut() {
        return appAut;
    }

    public void setAppAut(String appAut) {
        this.appAut = appAut;
    }

    public void setgApplstdt(String gApplstdt) {
        this.gApplstdt = gApplstdt;
    }

    public String getgApplstdt() {
        return gApplstdt;
    }

    public String getIsPQRequired() {
        return isPQRequired;
    }

    public void setIsPQRequired(String isPQRequired) {
        this.isPQRequired = isPQRequired;
    }

    public BigDecimal getEstCost() {
        return estCost;
    }

    public void setEstCost(BigDecimal estCost) {
        this.estCost = estCost;
    }

    public String getgActDtAppAwardDt() {
        return gActDtAppAwardDt;
    }

    public void setgActDtAppAwardDt(String gActDtAppAwardDt) {
        this.gActDtAppAwardDt = gActDtAppAwardDt;
    }

    public String getgActDtInAdvtDt() {
        return gActDtInAdvtDt;
    }

    public void setgActDtInAdvtDt(String gActDtInAdvtDt) {
        this.gActDtInAdvtDt = gActDtInAdvtDt;
    }

    public String getgActDtNtiAwardDt() {
        return gActDtNtiAwardDt;
    }

    public void setgActDtNtiAwardDt(String gActDtNtiAwardDt) {
        this.gActDtNtiAwardDt = gActDtNtiAwardDt;
    }

    public String getgActDtSignCtcDt() {
        return gActDtSignCtcDt;
    }

    public void setgActDtSignCtcDt(String gActDtSignCtcDt) {
        this.gActDtSignCtcDt = gActDtSignCtcDt;
    }

    public String getgActDtTenderEvalDt() {
        return gActDtTenderEvalDt;
    }

    public void setgActDtTenderEvalDt(String gActDtTenderEvalDt) {
        this.gActDtTenderEvalDt = gActDtTenderEvalDt;
    }

    public String getgActDtTenderOpenDt() {
        return gActDtTenderOpenDt;
    }

    public void setgActDtTenderOpenDt(String gActDtTenderOpenDt) {
        this.gActDtTenderOpenDt = gActDtTenderOpenDt;
    }

    public String getgPlanDtAppAwardDt() {
        return gPlanDtAppAwardDt;
    }

    public void setgPlanDtAppAwardDt(String gPlanDtAppAwardDt) {
        this.gPlanDtAppAwardDt = gPlanDtAppAwardDt;
    }

    public String getgPlanDtInAdvtDt() {
        return gPlanDtInAdvtDt;
    }

    public void setgPlanDtInAdvtDt(String gPlanDtInAdvtDt) {
        this.gPlanDtInAdvtDt = gPlanDtInAdvtDt;
    }

    public String getgPlanDtNtiAwardDt() {
        return gPlanDtNtiAwardDt;
    }

    public void setgPlanDtNtiAwardDt(String gPlanDtNtiAwardDt) {
        this.gPlanDtNtiAwardDt = gPlanDtNtiAwardDt;
    }

    public String getgPlanDtSignCtcDt() {
        return gPlanDtSignCtcDt;
    }

    public void setgPlanDtSignCtcDt(String gPlanDtSignCtcDt) {
        this.gPlanDtSignCtcDt = gPlanDtSignCtcDt;
    }

    public String getgPlanDtTenderEvalDt() {
        return gPlanDtTenderEvalDt;
    }

    public void setgPlanDtTenderEvalDt(String gPlanDtTenderEvalDt) {
        this.gPlanDtTenderEvalDt = gPlanDtTenderEvalDt;
    }

    public String getgPlanDtTenderOpenDt() {
        return gPlanDtTenderOpenDt;
    }

    public void setgPlanDtTenderOpenDt(String gPlanDtTenderOpenDt) {
        this.gPlanDtTenderOpenDt = gPlanDtTenderOpenDt;
    }

    public String getgPlanDtTotTimeComContractDt() {
        return gPlanDtTotTimeComContractDt;
    }

    public void setgPlanDtTotTimeComContractDt(String gPlanDtTotTimeComContractDt) {
        this.gPlanDtTotTimeComContractDt = gPlanDtTotTimeComContractDt;
    }

    public Short getgPlanDyAppAwardDy() {
        return gPlanDyAppAwardDy;
    }

    public void setgPlanDyAppAwardDy(Short gPlanDyAppAwardDy) {
        this.gPlanDyAppAwardDy = gPlanDyAppAwardDy;
    }

    public Short getgPlanDyInAdvtDy() {
        return gPlanDyInAdvtDy;
    }

    public void setgPlanDyInAdvtDy(Short gPlanDyInAdvtDy) {
        this.gPlanDyInAdvtDy = gPlanDyInAdvtDy;
    }

    public Short getgPlanDyNtiAwardDy() {
        return gPlanDyNtiAwardDy;
    }

    public void setgPlanDyNtiAwardDy(Short gPlanDyNtiAwardDy) {
        this.gPlanDyNtiAwardDy = gPlanDyNtiAwardDy;
    }

    public Short getgPlanDySignCtcDy() {
        return gPlanDySignCtcDy;
    }

    public void setgPlanDySignCtcDy(Short gPlanDySignCtcDy) {
        this.gPlanDySignCtcDy = gPlanDySignCtcDy;
    }

    public Short getgPlanDyTenderEvalDy() {
        return gPlanDyTenderEvalDy;
    }

    public void setgPlanDyTenderEvalDy(Short gPlanDyTenderEvalDy) {
        this.gPlanDyTenderEvalDy = gPlanDyTenderEvalDy;
    }

    public Short getgPlanDyTenderOpenDy() {
        return gPlanDyTenderOpenDy;
    }

    public void setgPlanDyTenderOpenDy(Short gPlanDyTenderOpenDy) {
        this.gPlanDyTenderOpenDy = gPlanDyTenderOpenDy;
    }

    public Short getgPlanDyTotTimeComContractDy() {
        return gPlanDyTotTimeComContractDy;
    }

    public void setgPlanDyTotTimeComContractDy(Short gPlanDyTotTimeComContractDy) {
        this.gPlanDyTotTimeComContractDy = gPlanDyTotTimeComContractDy;
    }

    public Short getgPlanDyTotTimeSignContractDy() {
        return gPlanDyTotTimeSignContractDy;
    }

    public void setgPlanDyTotTimeSignContractDy(Short gPlanDyTotTimeSignContractDy) {
        this.gPlanDyTotTimeSignContractDy = gPlanDyTotTimeSignContractDy;
    }

    public String getPackageNo() {
        return packageNo;
    }

    public void setPackageNo(String packageNo) {
        this.packageNo = packageNo;
    }

    public String getProcMethod() {
        return procMethod;
    }

    public void setProcMethod(String procMethod) {
        this.procMethod = procMethod;
    }

    public String getProcType() {
        return procType;
    }

    public void setProcType(String procType) {
        this.procType = procType;
    }

    public String getsActDtAdvtEOIDt() {
        return sActDtAdvtEOIDt;
    }

    public void setsActDtAdvtEOIDt(String sActDtAdvtEOIDt) {
        this.sActDtAdvtEOIDt = sActDtAdvtEOIDt;
    }

    public String getsActDtAppDt() {
        return sActDtAppDt;
    }

    public void setsActDtAppDt(String sActDtAppDt) {
        this.sActDtAppDt = sActDtAppDt;
    }

    public String getsActDtFinProEvalDt() {
        return sActDtFinProEvalDt;
    }

    public void setsActDtFinProEvalDt(String sActDtFinProEvalDt) {
        this.sActDtFinProEvalDt = sActDtFinProEvalDt;
    }

    public String getsActDtIsuRFPDt() {
        return sActDtIsuRFPDt;
    }

    public void setsActDtIsuRFPDt(String sActDtIsuRFPDt) {
        this.sActDtIsuRFPDt = sActDtIsuRFPDt;
    }

    public String getsActDtNegoDt() {
        return sActDtNegoDt;
    }

    public void setsActDtNegoDt(String sActDtNegoDt) {
        this.sActDtNegoDt = sActDtNegoDt;
    }

    public String getsActDtSignCtcDt() {
        return sActDtSignCtcDt;
    }

    public void setsActDtSignCtcDt(String sActDtSignCtcDt) {
        this.sActDtSignCtcDt = sActDtSignCtcDt;
    }

    public String getsActDtTechProEvalDt() {
        return sActDtTechProEvalDt;
    }

    public void setsActDtTechProEvalDt(String sActDtTechProEvalDt) {
        this.sActDtTechProEvalDt = sActDtTechProEvalDt;
    }

    public String getsPlanDtAdvtEOIDt() {
        return sPlanDtAdvtEOIDt;
    }

    public void setsPlanDtAdvtEOIDt(String sPlanDtAdvtEOIDt) {
        this.sPlanDtAdvtEOIDt = sPlanDtAdvtEOIDt;
    }

    public String getsPlanDtAppDt() {
        return sPlanDtAppDt;
    }

    public void setsPlanDtAppDt(String sPlanDtAppDt) {
        this.sPlanDtAppDt = sPlanDtAppDt;
    }

    public String getsPlanDtFinProEvalDt() {
        return sPlanDtFinProEvalDt;
    }

    public void setsPlanDtFinProEvalDt(String sPlanDtFinProEvalDt) {
        this.sPlanDtFinProEvalDt = sPlanDtFinProEvalDt;
    }

    public String getsPlanDtIsuRFPDt() {
        return sPlanDtIsuRFPDt;
    }

    public void setsPlanDtIsuRFPDt(String sPlanDtIsuRFPDt) {
        this.sPlanDtIsuRFPDt = sPlanDtIsuRFPDt;
    }

    public String getsPlanDtNegoDt() {
        return sPlanDtNegoDt;
    }

    public void setsPlanDtNegoDt(String sPlanDtNegoDt) {
        this.sPlanDtNegoDt = sPlanDtNegoDt;
    }

    public String getsPlanDtSignCtcDt() {
        return sPlanDtSignCtcDt;
    }

    public void setsPlanDtSignCtcDt(String sPlanDtSignCtcDt) {
        this.sPlanDtSignCtcDt = sPlanDtSignCtcDt;
    }

    public String getsPlanDtTechProEvalDt() {
        return sPlanDtTechProEvalDt;
    }

    public void setsPlanDtTechProEvalDt(String sPlanDtTechProEvalDt) {
        this.sPlanDtTechProEvalDt = sPlanDtTechProEvalDt;
    }

    public String getsPlanDtTotTimeComContractDt() {
        return sPlanDtTotTimeComContractDt;
    }

    public void setsPlanDtTotTimeComContractDt(String sPlanDtTotTimeComContractDt) {
        this.sPlanDtTotTimeComContractDt = sPlanDtTotTimeComContractDt;
    }

    public Short getsPlanDyAdvtEOIDy() {
        return sPlanDyAdvtEOIDy;
    }

    public void setsPlanDyAdvtEOIDy(Short sPlanDyAdvtEOIDy) {
        this.sPlanDyAdvtEOIDy = sPlanDyAdvtEOIDy;
    }

    public Short getsPlanDyAppDy() {
        return sPlanDyAppDy;
    }

    public void setsPlanDyAppDy(Short sPlanDyAppDy) {
        this.sPlanDyAppDy = sPlanDyAppDy;
    }

    public Short getsPlanDyFinProEvalDy() {
        return sPlanDyFinProEvalDy;
    }

    public void setsPlanDyFinProEvalDy(Short sPlanDyFinProEvalDy) {
        this.sPlanDyFinProEvalDy = sPlanDyFinProEvalDy;
    }

    public Short getsPlanDyIsuRFPDy() {
        return sPlanDyIsuRFPDy;
    }

    public void setsPlanDyIsuRFPDy(Short sPlanDyIsuRFPDy) {
        this.sPlanDyIsuRFPDy = sPlanDyIsuRFPDy;
    }

    public Short getsPlanDyNegoDy() {
        return sPlanDyNegoDy;
    }

    public void setsPlanDyNegoDy(Short sPlanDyNegoDy) {
        this.sPlanDyNegoDy = sPlanDyNegoDy;
    }

    public Short getsPlanDySignCtcDy() {
        return sPlanDySignCtcDy;
    }

    public void setsPlanDySignCtcDy(Short sPlanDySignCtcDy) {
        this.sPlanDySignCtcDy = sPlanDySignCtcDy;
    }

    public Short getsPlanDyTechProEvalDy() {
        return sPlanDyTechProEvalDy;
    }

    public void setsPlanDyTechProEvalDy(Short sPlanDyTechProEvalDy) {
        this.sPlanDyTechProEvalDy = sPlanDyTechProEvalDy;
    }

    public Short getsPlanDyTotTimeComContractDy() {
        return sPlanDyTotTimeComContractDy;
    }

    public void setsPlanDyTotTimeComContractDy(Short sPlanDyTotTimeComContractDy) {
        this.sPlanDyTotTimeComContractDy = sPlanDyTotTimeComContractDy;
    }

    public Short getsPlanDyTotTimeSignContractDy() {
        return sPlanDyTotTimeSignContractDy;
    }

    public void setsPlanDyTotTimeSignContractDy(Short sPlanDyTotTimeSignContractDy) {
        this.sPlanDyTotTimeSignContractDy = sPlanDyTotTimeSignContractDy;
    }

    public String getSrcFund() {
        return srcFund;
    }

    public void setSrcFund(String srcFund) {
        this.srcFund = srcFund;
    }

    public BigDecimal getTotEstCost() {
        return totEstCost;
    }

    public void setTotEstCost(BigDecimal totEstCost) {
        this.totEstCost = totEstCost;
    }

    public String getwActDtAdvtDt() {
        return wActDtAdvtDt;
    }

    public void setwActDtAdvtDt(String wActDtAdvtDt) {
        this.wActDtAdvtDt = wActDtAdvtDt;
    }

    public String getwActDtAdvtPreDt() {
        return wActDtAdvtPreDt;
    }

    public void setwActDtAdvtPreDt(String wActDtAdvtPreDt) {
        this.wActDtAdvtPreDt = wActDtAdvtPreDt;
    }

    public String getwActDtAppAwardDt() {
        return wActDtAppAwardDt;
    }

    public void setwActDtAppAwardDt(String wActDtAppAwardDt) {
        this.wActDtAppAwardDt = wActDtAppAwardDt;
    }

    public String getwActDtNtiAwardDt() {
        return wActDtNtiAwardDt;
    }

    public void setwActDtNtiAwardDt(String wActDtNtiAwardDt) {
        this.wActDtNtiAwardDt = wActDtNtiAwardDt;
    }

    public String getwActDtSignCtcDt() {
        return wActDtSignCtcDt;
    }

    public void setwActDtSignCtcDt(String wActDtSignCtcDt) {
        this.wActDtSignCtcDt = wActDtSignCtcDt;
    }

    public String getwActDtTenderEvalDt() {
        return wActDtTenderEvalDt;
    }

    public void setwActDtTenderEvalDt(String wActDtTenderEvalDt) {
        this.wActDtTenderEvalDt = wActDtTenderEvalDt;
    }

    public String getwActDtTenderOpenDt() {
        return wActDtTenderOpenDt;
    }

    public void setwActDtTenderOpenDt(String wActDtTenderOpenDt) {
        this.wActDtTenderOpenDt = wActDtTenderOpenDt;
    }

    public String getwPlanDtAdvtPreDt() {
        return wPlanDtAdvtPreDt;
    }

    public void setwPlanDtAdvtPreDt(String wPlanDtAdvtPreDt) {
        this.wPlanDtAdvtPreDt = wPlanDtAdvtPreDt;
    }

    public String getwPlanDtAppAwardDt() {
        return wPlanDtAppAwardDt;
    }

    public void setwPlanDtAppAwardDt(String wPlanDtAppAwardDt) {
        this.wPlanDtAppAwardDt = wPlanDtAppAwardDt;
    }

    public String getwPlanDtInitAdvtDt() {
        return wPlanDtInitAdvtDt;
    }

    public void setwPlanDtInitAdvtDt(String wPlanDtInitAdvtDt) {
        this.wPlanDtInitAdvtDt = wPlanDtInitAdvtDt;
    }

    public String getwPlanDtNtiAwardDt() {
        return wPlanDtNtiAwardDt;
    }

    public void setwPlanDtNtiAwardDt(String wPlanDtNtiAwardDt) {
        this.wPlanDtNtiAwardDt = wPlanDtNtiAwardDt;
    }

    public String getwPlanDtSignCtcDt() {
        return wPlanDtSignCtcDt;
    }

    public void setwPlanDtSignCtcDt(String wPlanDtSignCtcDt) {
        this.wPlanDtSignCtcDt = wPlanDtSignCtcDt;
    }

    public String getwPlanDtTenderEvalDt() {
        return wPlanDtTenderEvalDt;
    }

    public void setwPlanDtTenderEvalDt(String wPlanDtTenderEvalDt) {
        this.wPlanDtTenderEvalDt = wPlanDtTenderEvalDt;
    }

    public String getwPlanDtTenderOpenDt() {
        return wPlanDtTenderOpenDt;
    }

    public void setwPlanDtTenderOpenDt(String wPlanDtTenderOpenDt) {
        this.wPlanDtTenderOpenDt = wPlanDtTenderOpenDt;
    }

    public String getwPlanDtTotTimeComContractDt() {
        return wPlanDtTotTimeComContractDt;
    }

    public void setwPlanDtTotTimeComContractDt(String wPlanDtTotTimeComContractDt) {
        this.wPlanDtTotTimeComContractDt = wPlanDtTotTimeComContractDt;
    }

    public Short getwPlanDyAdvtPreDy() {
        return wPlanDyAdvtPreDy;
    }

    public void setwPlanDyAdvtPreDy(Short wPlanDyAdvtPreDy) {
        this.wPlanDyAdvtPreDy = wPlanDyAdvtPreDy;
    }

    public Short getwPlanDyAppAwardDy() {
        return wPlanDyAppAwardDy;
    }

    public void setwPlanDyAppAwardDy(Short wPlanDyAppAwardDy) {
        this.wPlanDyAppAwardDy = wPlanDyAppAwardDy;
    }

    public Short getwPlanDyInitAdvtDy() {
        return wPlanDyInitAdvtDy;
    }

    public void setwPlanDyInitAdvtDy(Short wPlanDyInitAdvtDy) {
        this.wPlanDyInitAdvtDy = wPlanDyInitAdvtDy;
    }

    public Short getwPlanDyNtiAwardDy() {
        return wPlanDyNtiAwardDy;
    }

    public void setwPlanDyNtiAwardDy(Short wPlanDyNtiAwardDy) {
        this.wPlanDyNtiAwardDy = wPlanDyNtiAwardDy;
    }

    public Short getwPlanDySignCtcDy() {
        return wPlanDySignCtcDy;
    }

    public void setwPlanDySignCtcDy(Short wPlanDySignCtcDy) {
        this.wPlanDySignCtcDy = wPlanDySignCtcDy;
    }

    public Short getwPlanDyTenderEvalDy() {
        return wPlanDyTenderEvalDy;
    }

    public void setwPlanDyTenderEvalDy(Short wPlanDyTenderEvalDy) {
        this.wPlanDyTenderEvalDy = wPlanDyTenderEvalDy;
    }

    public Short getwPlanDyTenderOpenDy() {
        return wPlanDyTenderOpenDy;
    }

    public void setwPlanDyTenderOpenDy(Short wPlanDyTenderOpenDy) {
        this.wPlanDyTenderOpenDy = wPlanDyTenderOpenDy;
    }

    public Short getwPlanDyTotTimeComContractDy() {
        return wPlanDyTotTimeComContractDy;
    }

    public void setwPlanDyTotTimeComContractDy(Short wPlanDyTotTimeComContractDy) {
        this.wPlanDyTotTimeComContractDy = wPlanDyTotTimeComContractDy;
    }

    public Short getwPlanDyTotTimeSignContractDy() {
        return wPlanDyTotTimeSignContractDy;
    }

    public void setwPlanDyTotTimeSignContractDy(Short wPlanDyTotTimeSignContractDy) {
        this.wPlanDyTotTimeSignContractDy = wPlanDyTotTimeSignContractDy;
    }

    public String getsActDtTechProOpenDt() {
        return sActDtTechProOpenDt;
    }

    public void setsActDtTechProOpenDt(String sActDtTechProOpenDt) {
        this.sActDtTechProOpenDt = sActDtTechProOpenDt;
    }

    public String getsPlanDtTechProOpenDt() {
        return sPlanDtTechProOpenDt;
    }

    public void setsPlanDtTechProOpenDt(String sPlanDtTechProOpenDt) {
        this.sPlanDtTechProOpenDt = sPlanDtTechProOpenDt;
    }

    public Short getsPlanDyTechProOpenDy() {
        return sPlanDyTechProOpenDy;
    }

    public void setsPlanDyTechProOpenDy(Short sPlanDyTechProOpenDy) {
        this.sPlanDyTechProOpenDy = sPlanDyTechProOpenDy;
    }

    public String getgAdvtDt() {
        return gAdvtDt;
    }

    public void setgAdvtDt(String gAdvtDt) {
        this.gAdvtDt = gAdvtDt;
    }

    public Short getgAdvtDy() {
        return gAdvtDy;
    }

    public void setgAdvtDy(Short gAdvtDy) {
        this.gAdvtDy = gAdvtDy;
    }

    public String getsAdvtDt() {
        return sAdvtDt;
    }

    public void setsAdvtDt(String sAdvtDt) {
        this.sAdvtDt = sAdvtDt;
    }

    public Short getsAdvtDy() {
        return sAdvtDy;
    }

    public void setsAdvtDy(Short sAdvtDy) {
        this.sAdvtDy = sAdvtDy;
    }

    public String getwAdvtDt() {
        return wAdvtDt;
    }

    public void setwAdvtDt(String wAdvtDt) {
        this.wAdvtDt = wAdvtDt;
    }

    public Short getwAdvtDy() {
        return wAdvtDy;
    }

    public void setwAdvtDy(Short wAdvtDy) {
        this.wAdvtDy = wAdvtDy;
    }

    public String getMinistryDetails() {
        return ministryDetails;
    }

    public void setMinistryDetails(String ministryDetails) {
        this.ministryDetails = ministryDetails;
    }

    public String getProjectNameCode() {
        return projectNameCode;
    }

    public void setProjectNameCode(String projectNameCode) {
        this.projectNameCode = projectNameCode;
    }

    //Code by Proshanto Kumar Saha
    /**
     * @return the gPlanDtLintAwardDt
     */
    public String getgPlanDtLintAwardDt() {
        return gPlanDtLintAwardDt;
    }

    /**
     * @param gPlanDtLintAwardDt the gPlanDtLintAwardDt to set
     */
    public void setgPlanDtLintAwardDt(String gPlanDtLintAwardDt) {
        this.gPlanDtLintAwardDt = gPlanDtLintAwardDt;
    }

    //Code by Proshanto Kumar Saha
    /**
     * @return the gPlanDtLintAwardDt
     */
    public String getgActDtLintAwardDt() {
        return gActDtLintAwardDt;
    }

    /**
     * @param gPlanDtLintAwardDt the gPlanDtLintAwardDt to set
     */
    public void setgActDtLintAwardDt(String gActDtLintAwardDt) {
        this.gActDtLintAwardDt = gActDtLintAwardDt;
    }
    /**
     * @return the gPlanDyLintAwardDy
     */
    public Short getgPlanDyLintAwardDy() {
        return gPlanDyLintAwardDy;
    }

    /**
     * @param gPlanDyLintAwardDy the gPlanDyLintAwardDy to set
     */
    public void setgPlanDyLintAwardDy(Short gPlanDyLintAwardDy) {
        this.gPlanDyLintAwardDy = gPlanDyLintAwardDy;
    }

   
    /**
     * @return the wPlanDtLintAwardDt
     */
    public String getwPlanDtLintAwardDt() {
        return wPlanDtLintAwardDt;
    }

    /**
     * @param wPlanDtLintAwardDt the wPlanDtLintAwardDt to set
     */
    public void setwPlanDtLintAwardDt(String wPlanDtLintAwardDt) {
        this.wPlanDtLintAwardDt = wPlanDtLintAwardDt;
    }

    /**
     * @return the wPlanDyLintAwardDy
     */
    public Short getwPlanDyLintAwardDy() {
        return wPlanDyLintAwardDy;
    }

    /**
     * @param wPlanDyLintAwardDy the wPlanDyLintAwardDy to set
     */
    public void setwPlanDyLintAwardDy(Short wPlanDyLintAwardDy) {
        this.wPlanDyLintAwardDy = wPlanDyLintAwardDy;
    }
    
    //Code End by Proshanto Kumar Saha

    /**
     * @return the wActDtLintAwardDt
     */
    public String getwActDtLintAwardDt() {
        return wActDtLintAwardDt;
    }

    /**
     * @param wActDtLintAwardDt the wActDtLintAwardDt to set
     */
    public void setwActDtLintAwardDt(String wActDtLintAwardDt) {
        this.wActDtLintAwardDt = wActDtLintAwardDt;
    }

}
