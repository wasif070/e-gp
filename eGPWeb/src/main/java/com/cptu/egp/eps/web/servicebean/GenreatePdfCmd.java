/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.web.utility.AppExceptionHandler;
import com.cptu.egp.eps.web.utility.XMLReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import org.apache.log4j.Logger;
import java.nio.file.*;

/**
 *
 * @author Swati Patel
 * Created On 24th dec. 2010
 */
public class GenreatePdfCmd {

    final Logger logger = Logger.getLogger(GenreatePdfCmd.class);
    String logUserId = "0";
    /**
     * 
     */
    
    String line;
    private String strDriveLetter = XMLReader.getMessage("driveLatter");
    private String PDFSoftwarePath = XMLReader.getMessage("PDFSoftwarePath");
    private String Port = XMLReader.getMessage("Port");
    public String path = strDriveLetter+"/eGP/PDF";

    private boolean isDevelopment = false;
    private boolean isStaging = false;
    private boolean isProduction = false;
    private boolean isTraining = false;
    private boolean isInHouse = false;
    //String Header = "Electronic Government Procurement (e-GP) System \n\n\n\n\n\n\n Government Procurement and Property Management Division, Department of National Properties \n  Ministry of Finance, Royal Government of Bhutan\n\n ";
    
    
    //String wkhtmltopdfPath = XMLReader.getMessage("wkhtmltopdfPath");

    /**
     * Method for generate PDF file (like Tender Section,etc).
     * @param reqURL Url for which pdf generated
     * @param reqQuery Url query string value
     * @param folderName Folder name where to store PDF
     * @param id Object Id
     */
    public void genrateCmd(String reqURL, String reqQuery, String folderName, String id) {
        try {
            //BufferedReader Filebr = new BufferedReader(new FileReader("C:\\eGP\\SoftwarePath\\PDFSoftwarePath.txt"));
            //PDFSoftwarePath = Filebr.readLine();
            logger.debug("genrateCmd " + logUserId + " Starts");
            logger.debug(reqQuery + "req URL--" + reqURL);
//            System.out.print("req URL--" + reqURL);
            // Check the server information
            
            if(reqURL.contains("development.eprocure.gov.bd") || reqURL.contains("10.1.3.21")){
                isDevelopment = true;
            }
            else if(reqURL.contains("staging.eprocure.gov.bd") || reqURL.contains("10.1.4.21"))
            {
                isStaging = true;
            }
            else if(reqURL.contains("training.eprocure.gov.bd") || reqURL.contains("10.1.40.25"))
            {
                isTraining = true;
            }
            else if(reqURL.contains("www.eprocure.gov.bd") || reqURL.contains("10.1.2.26") || reqURL.contains("10.1.2.27"))
            {
                isProduction = true;
            }
/*
              if(isDevelopment || isTraining)
            {
                strDriveLetter = "D:";
            }
            else if(isStaging)
            {
                strDriveLetter = "E:";
            }
            else if(isProduction)
            {
                strDriveLetter = "\\\\egpprdblog\\AppSharedDrive";
            }*/

            File destFoldereGP = new File(strDriveLetter+"/eGP");
            if (destFoldereGP.exists() == false) {
                destFoldereGP.mkdirs();
            }

            File destFolderPDF = new File(strDriveLetter+"/eGP/PDF/");
            if (destFolderPDF.exists() == false) {
                destFolderPDF.mkdirs();
            }

            File destFolder = new File(strDriveLetter+"/eGP/PDF/" + folderName);
            if (destFolder.exists() == false) {
                destFolder.mkdirs();
            }

            File destFolder2 = new File(strDriveLetter+"/eGP/PDF/" + folderName + "/" + id);
            if (destFolder2.exists() == false) {
                destFolder2.mkdirs();
            }

            // 115.127.40.5

            if (isDevelopment) {
                reqURL = reqURL.replace("development.egp.gov.bt", "10.1.3.21");
            }
            if (isStaging) {
                reqURL = reqURL.replace("staging.egp.gov.bt", "10.1.4.21");
                reqURL = reqURL.replace("www.","");
            }
            if (isTraining) {
                reqURL = reqURL.replace("training.egp.gov.bt", "10.1.40.25");
                reqURL = reqURL.replace("www.","");
            }

           /*
            //For Fixing the issue of wkhtmltopdf crash on all environment
            // crash reason wkhtmltopdf not support https port
            if (isProduction || isInHouse || isStaging || isTraining || reqURL.contains("localhost/")) {
                //reqURL = reqURL.replace("www.eprocure.gov.bd", "10.1.2.26");
                reqURL = reqURL.replace("http", "https");
                reqURL = reqURL.replace("httpss", "https");
            }
            */
            String Protocol = "";
            if(reqURL.startsWith("https://"))
            {
                //reqURL = reqURL.replace("https://", "http://");
                Protocol = "https://";
            }
            else if(reqURL.startsWith("http://"))
            {
                Protocol = "http://";
            }
            else
            {
                //reqURL = "http://" + reqURL;
                Protocol = "http://";
            }
            
            String HostandPort = "";
            int SlashCounter = 0,CharPointer=0;
            while(SlashCounter<3)
            {
                if(reqURL.charAt(CharPointer)=='/')
                {
                    SlashCounter++;
                }
                HostandPort = HostandPort + reqURL.charAt(CharPointer);
                CharPointer++;
            }
            String HostandPort2 = Protocol + "localhost";
            if(Port != null && !Port.equalsIgnoreCase(""))
            {
                HostandPort2 = HostandPort2 + ":" + Port + "/";
            }
            else
            {
                HostandPort2 = HostandPort2 + "/";
            }
            reqURL = reqURL.replace(HostandPort, HostandPort2);
            reqURL = reqURL.replace("\"", "");
            reqURL = reqURL.replace(" ", "");
            System.out.println(reqURL);
            
            //PrintWriter writer = new PrintWriter("C:\\eGP\\SoftwarePath\\HeaderAddress.txt", "UTF-8");
            //writer.println(HostandPort +"PDFHeader.html");
            //writer.close();

            String pdfName=null;
            if(folderName.equals("TenderDetail")){
                pdfName = "DocumentFeesReceipt";
            }else if(folderName.equalsIgnoreCase("RegistrationPayment")){
                pdfName = "RegistrationFees";
            }else{
                pdfName = folderName;
            }
           
            logger.debug(PDFSoftwarePath + " --footer-right \"Page [page] of [toPage]\" --margin-bottom 20mm --margin-top 35mm --header-html "+ HostandPort2 +"PDFHeader.html --header-spacing 5 --footer-spacing 5 --orientation Landscape --page-size A4  " + reqURL + "?" + reqQuery.replaceAll(" ","%20") + "&isPDF=true&xulfz=d4f8r5s1 "+strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + pdfName + ".pdf");
            //Process p = Runtime.getRuntime().exec("wkhtmltopdf.exe  --disable-smart-shrinking --minimum-font-size 20 -H --header-left \"Date : [date] [time]\" --header-right \"Page [page] of [toPage]\" --header-spacing 2 " + reqURL + "?" + reqQuery.replaceAll(" ","%20") + "&isPDF=true&xulfz=d4f8r5s1 "+strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + pdfName + ".pdf");
            Process p = Runtime.getRuntime().exec(PDFSoftwarePath + " --footer-right \"Page [page] of [toPage]\" --margin-bottom 20mm --margin-top 35mm --header-html " + HostandPort2 + "PDFHeader.html --header-spacing 5 --footer-spacing 5 --orientation Landscape --page-size A4 \"" + reqURL + "?" + reqQuery.replaceAll(" ","%20") + "&isPDF=true&xulfz=d4f8r5s1\" \""+strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + pdfName + ".pdf\"");
            //Process p = Runtime.getRuntime().exec(PDFSoftwarePath + " --footer-right \"Page [page] of [toPage]\" --margin-bottom 20mm --footer-spacing 5 --orientation Landscape --page-size A4  " + reqURL + "?" + reqQuery.replaceAll(" ","%20") + "&isPDF=true&xulfz=d4f8r5s1 "+strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + pdfName + ".pdf");
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            System.out.println(PDFSoftwarePath + " --footer-right \"Page [page] of [toPage]\" --margin-bottom 20mm --margin-top 35mm --header-html "+ HostandPort2 +"PDFHeader.html --header-spacing 5 --footer-spacing 5 --orientation Landscape --page-size A4 \"" + reqURL + "?" + reqQuery.replaceAll(" ","%20") + "&isPDF=true&xulfz=d4f8r5s1\" \""+strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + pdfName + ".pdf\"");
            while ((line = br.readLine()) != null) {
               System.out.println("Read from error stream: \"" + line + "\"");
            }
            br.close();
            p.destroy();
        } catch (IOException e1) {
                e1.printStackTrace();
//                System.out.println("Generate PDF CMD e1"+new java.util.Date());
                logger.error("genrateCmd " + logUserId + " :" + e1.toString());
                AppExceptionHandler expHandle = new AppExceptionHandler();
                expHandle.stack2string(e1);
        } catch (Exception e) {
            e.printStackTrace();
//            System.out.println("Generate PDF CMD e2"+new java.util.Date());
            logger.error("genrateCmd " + logUserId + " :" + e.toString());
            AppExceptionHandler expHandle = new AppExceptionHandler();
            expHandle.stack2string(e);
        }
        logger.debug("genrateCmd " + logUserId + " Ends");
    }

    /**
     * Method for generate PDF for STD Sections
     * @param reqURL Url for which pdf generated
     * @param reqQuery Url query string value
     * @param folderName Folder name where to store PDF
     * @param id Object Id
     * @param pdfName Name of Pdf File
     */
    public void genrateSTDpdf(String reqURL, String reqQuery, String folderName, String id, String pdfName) {
        try {
            //BufferedReader Filebr = new BufferedReader(new FileReader("C:\\eGP\\SoftwarePath\\PDFSoftwarePath.txt"));
            //PDFSoftwarePath = Filebr.readLine();
            String HostandPort = "";
            int SlashCounter = 0,CharPointer=0;
            
            // Check the server information
            if(reqURL.contains("development.eprocure.gov.bd") || reqURL.contains("10.1.3.21")){
                isDevelopment = true;
            }
            if(reqURL.contains("staging.eprocure.gov.bd") || reqURL.contains("10.1.4.21")){
                isStaging = true;
            }
            if(reqURL.contains("training.eprocure.gov.bd") || reqURL.contains("10.1.40.25")){
                isTraining = true;
            }
            if(reqURL.contains("www.eprocure.gov.bd") || reqURL.contains("10.1.2.26") || reqURL.contains("10.1.2.27")){
                isProduction = true;
            }
            if(reqURL.contains("192.168.100.131/") || reqURL.contains("192.168.100.152/") || reqURL.contains("122.170.119.66/") || reqURL.contains("inhouse.eprocure.gov.bd/")){
                isInHouse = true;
            }
            
              /*
            if(isDevelopment || isTraining)
            {
                strDriveLetter = "D:";
            }
            else if(isStaging)
            {
                strDriveLetter = "E:";
            }
            else if(isProduction)
            {
                strDriveLetter = "\\\\egpprdblog\\AppSharedDrive";
            }*/

            path = strDriveLetter+"/eGP/PDF/";

            logger.debug("genrateCmd " + logUserId + " Starts");
            logger.debug(reqQuery + "req URL--" + reqURL);
            File destFoldereGP = new File(strDriveLetter+"/eGP");
            if (destFoldereGP.exists() == false) {
                destFoldereGP.mkdirs();
            }

            File destFolderPDF = new File(strDriveLetter+"/eGP/PDF/");
            if (destFolderPDF.exists() == false) {
                destFolderPDF.mkdirs();
            }

            File destFolder = new File(strDriveLetter+"/eGP/PDF/" + folderName);
            if (destFolder.exists() == false) {
                destFolder.mkdirs();
            }

            File destFolder2 = new File(strDriveLetter+"/eGP/PDF/" + folderName + "/" + id);
            if (destFolder2.exists() == false) {
                destFolder2.mkdirs();
            }

            // 115.127.40.5

            if (isDevelopment) {
                reqURL = reqURL.replace("development.eprocure.gov.bd", "10.1.3.21");
            }
            if (isStaging) {
                reqURL = reqURL.replace("staging.eprocure.gov.bd", "10.1.4.21");
                reqURL = reqURL.replace("www.","");
            }
            if (isTraining) {
                reqURL = reqURL.replace("training.eprocure.gov.bd", "10.1.40.25");
                reqURL = reqURL.replace("www.","");
            }
            if(isInHouse)
            {
                reqURL = reqURL.replace("inhouse.eprocure.gov.bd", "192.168.100.152:8090");
            }
            /*
             if (isProduction || isInHouse || isStaging || isTraining || reqURL.contains("localhost/")) {
                //reqURL = reqURL.replace("www.eprocure.gov.bd", "10.1.2.26");
                 reqURL = reqURL.replace("http", "https");
                 reqURL = reqURL.replace("httpss", "https");
            }
             */
            String Protocol = "";
            if(reqURL.startsWith("https://"))
            {
                //reqURL = reqURL.replace("https://", "http://");
                Protocol = "https://";
            }
            else if(reqURL.startsWith("http://"))
            {
                Protocol = "http://";
            }
            else
            {
                //reqURL = "http://" + reqURL;
                Protocol = "http://";
            }
            
            while(SlashCounter<3)
            {
                if(reqURL.charAt(CharPointer)=='/')
                {
                    SlashCounter++;
                }
                HostandPort = HostandPort + reqURL.charAt(CharPointer);
                CharPointer++;
            }
            String HostandPort2 = Protocol + "localhost";
            if(Port != null && !Port.equalsIgnoreCase(""))
            {
                HostandPort2 = HostandPort2 + ":" + Port + "/";
            }
            else
            {
                HostandPort2 = HostandPort2 + "/";
            }
            reqURL = reqURL.replace(HostandPort, HostandPort2);
            reqURL = reqURL.replace("\"", "");
            reqURL = reqURL.replace(" ", "");
            System.out.println(reqURL);
            
            //PrintWriter writer = new PrintWriter("C:\\eGP\\SoftwarePath\\HeaderAddress.txt", "UTF-8");
            //writer.println(HostandPort +"PDFHeader.html");
            //writer.close();
            
            logger.debug(PDFSoftwarePath + " --footer-right \"Page [page] of [toPage]\" --margin-bottom 20mm --margin-top 35mm --header-html "+ HostandPort2 +"PDFHeader.html --header-spacing 5 --footer-spacing 5 --orientation Landscape --page-size A4 " + reqURL + "?" + reqQuery + "&isPDF=true&xulfz=d4f8r5s1 "+"\" "+strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + "/" + pdfName + ".pdf"+"\"");
//            Process p = Runtime.getRuntime().exec("wkhtmltopdf.exe --disable-smart-shrinking --minimum-font-size 20 -H --header-left \"Date : [date] [time]\" --header-right \"Page [page] of [toPage]\" --header-spacing 2 " + reqURL + "?" + reqQuery + "&isPDF=true&xulfz=d4f8r5s1 "+"\""+strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + "/" + pdfName + ".pdf"+"\"");
            Process p = Runtime.getRuntime().exec(PDFSoftwarePath + " --footer-right \"Page [page] of [toPage]\" --margin-bottom 20mm --margin-top 35mm --header-html "+ HostandPort2 +"PDFHeader.html --header-spacing 5 --footer-spacing 5 --orientation Landscape --page-size A4 \"" + reqURL + "?" + reqQuery + "&isPDF=true&xulfz=d4f8r5s1\" \"" +strDriveLetter+ "/eGP/PDF/" + folderName + "/" + id + "/" + "/" + pdfName + ".pdf"+"\"");
            //Process p = Runtime.getRuntime().exec(PDFSoftwarePath + " --footer-right \"Page [page] of [toPage]\" --footer-spacing 5 --orientation Landscape --page-size A4 " + reqURL + "?" + reqQuery + "&isPDF=true&xulfz=d4f8r5s1 "+"\""+strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + "/" + pdfName + ".pdf"+"\"");
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            System.out.println(PDFSoftwarePath + " --footer-right \"Page [page] of [toPage]\" --margin-bottom 20mm --margin-top 35mm --header-html "+ HostandPort2 +"PDFHeader.html --header-spacing 5 --footer-spacing 5 --orientation Landscape --page-size A4 \"" + reqURL + "?" + reqQuery + "&isPDF=true&xulfz=d4f8r5s1\" \"" +strDriveLetter+ "/eGP/PDF/" + folderName + "/" + id + "/" + "/" + pdfName + ".pdf"+"\"");
            while ((line = br.readLine()) != null) {
                System.out.println("Read from error stream: \"" + line + "\"");
            }
            br.close();
            p.destroy();
        } catch (IOException e1) {
            e1.printStackTrace();
//            System.out.println("Generate PDF CMD ex1"+new java.util.Date());
            logger.error("genrateCmd " + logUserId + " :" + e1.toString());
            AppExceptionHandler expHandle = new AppExceptionHandler();
            expHandle.stack2string(e1);
        } catch (Exception e) {
            e.printStackTrace();
//            System.out.println("Generate PDF CMD ex2"+new java.util.Date());
            logger.error("genrateCmd " + logUserId + " :" + e.toString());
            AppExceptionHandler expHandle = new AppExceptionHandler();
            expHandle.stack2string(e);
        }
        logger.debug("genrateCmd " + logUserId + " Ends");
    }

   /**
     * Method for generate PDF for Tender Forms
     * @param reqURL Url for which pdf generated
     * @param reqQuery Url query string value
     * @param folderName Folder name where to store PDF
     * @param id Object Id
    */
    public void genrateCmdForm(String reqURL, String reqQuery, String folderName, String id) {
        logger.debug("genrateCmdForm " + logUserId + " Starts");
        try {
            //BufferedReader Filebr = new BufferedReader(new FileReader("C:\\eGP\\SoftwarePath\\PDFSoftwarePath.txt"));
            //PDFSoftwarePath = Filebr.readLine();
            String HostandPort = "";
            int SlashCounter = 0,CharPointer=0;
            
            String line;
            // Check the server information
             if(reqURL.contains("development.eprocure.gov.bd") || reqURL.contains("10.1.3.21")){
                isDevelopment = true;
            }
            if(reqURL.contains("staging.eprocure.gov.bd") || reqURL.contains("10.1.4.21")){
                isStaging = true;
            }
            if(reqURL.contains("training.eprocure.gov.bd") || reqURL.contains("10.1.40.25")){
                isTraining = true;
            }
            if(reqURL.contains("www.eprocure.gov.bd") || reqURL.contains("10.1.2.26") || reqURL.contains("10.1.2.27")){
                isProduction = true;
            }
            if(reqURL.contains("192.168.100.131/") || reqURL.contains("192.168.100.152/") || reqURL.contains("122.170.119.66/") || reqURL.contains("inhouse.eprocure.gov.bd/")){
                isInHouse = true;
            }

            /*
            if(isDevelopment || isTraining)
            {
                strDriveLetter = "D:";
            }
            else if(isStaging)
            {
                strDriveLetter = "E:";
            }
            else if(isProduction)
            {
                strDriveLetter = "\\\\egpprdblog\\AppSharedDrive";
            }*/

            path = strDriveLetter+"/eGP/PDF/";

            logger.debug(reqQuery + "req URL--" + reqURL);
            File destFoldereGP = new File(strDriveLetter+"/eGP");
            if (destFoldereGP.exists() == false) {
                destFoldereGP.mkdirs();
            }

            File destFolderPDF = new File(strDriveLetter+"/eGP/PDF/");
            if (destFolderPDF.exists() == false) {
                destFolderPDF.mkdirs();
            }

            File destFolder = new File(strDriveLetter+"/eGP/PDF/" + folderName);
            if (destFolder.exists() == false) {
                destFolder.mkdirs();
            }

            File destFolder2 = new File(strDriveLetter+"/eGP/PDF/" + folderName + "/" + id);
            if (destFolder2.exists() == false) {
                destFolder2.mkdirs();
            }


            if (isDevelopment) {
                reqURL = reqURL.replace("development.eprocure.gov.bd", "10.1.3.21");
            }
            if (isStaging) {
                reqURL = reqURL.replace("staging.eprocure.gov.bd", "10.1.4.21");
                reqURL = reqURL.replace("www.","");
            }
            if (isTraining) {
                reqURL = reqURL.replace("training.eprocure.gov.bd", "10.1.40.25");
                reqURL = reqURL.replace("www.","");
            }
            if(isInHouse)
            {
                reqURL = reqURL.replace("inhouse.eprocure.gov.bd", "192.168.100.152:8090");
            }
/*
             if (isProduction || isInHouse || isStaging || isTraining || reqURL.contains("localhost/")) {
                //reqURL = reqURL.replaceAll("eprocure.gov.bd", "10.1.2.26");
                reqURL = reqURL.replace("http", "https");
                reqURL = reqURL.replace("httpss", "https");
            }
            */
            String Protocol = "";
            if(reqURL.startsWith("https://"))
            {
                //reqURL = reqURL.replace("https://", "http://");
                Protocol = "https://";
            }
            else if(reqURL.startsWith("http://"))
            {
                Protocol = "http://";
            }
            else
            {
                //reqURL = "http://" + reqURL;
                Protocol = "http://";
            }
            
            while(SlashCounter<3)
            {
                if(reqURL.charAt(CharPointer)=='/')
                {
                    SlashCounter++;
                }
                HostandPort = HostandPort + reqURL.charAt(CharPointer);
                CharPointer++;
            }
            String HostandPort2 = Protocol + "localhost";
            if(Port != null && !Port.equalsIgnoreCase(""))
            {
                HostandPort2 = HostandPort2 + ":" + Port + "/";
            }
            else
            {
                HostandPort2 = HostandPort2 + "/";
            }
            reqURL = reqURL.replace(HostandPort, HostandPort2);
            reqURL = reqURL.replace("\"", "");
            reqURL = reqURL.replace(" ", "");
            System.out.println(reqURL);
            
            //PrintWriter writer = new PrintWriter("C:\\eGP\\SoftwarePath\\HeaderAddress.txt", "UTF-8");
            //writer.println(HostandPort +"PDFHeader.html");
            //writer.close();

            if(reqURL.trim().contains("&isPDF=true"))
            {
                reqURL = reqURL.trim().replace("&isPDF=true", "&isPDF=true&xulfz=d4f8r5s1");
            }
            logger.debug(PDFSoftwarePath + " --footer-right \"Page [page] of [toPage]\" --margin-bottom 20mm --margin-top 35mm --header-html "+ HostandPort2 +"PDFHeader.html --header-spacing 5 --footer-spacing 5 --orientation Landscape --page-size A4 " + reqURL +" "+ strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + folderName + ".pdf");
            System.out.println(PDFSoftwarePath + " --footer-right \"Page [page] of [toPage]\" --margin-bottom 20mm --margin-top 35mm --header-html "+ HostandPort2 +"PDFHeader.html --header-spacing 5 --footer-spacing 5 --orientation Landscape --page-size A4 \"" + reqURL +"\" \""+ strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + folderName + ".pdf\"");
//            Process p = Runtime.getRuntime().exec("wkhtmltopdf.exe  --disable-smart-shrinking --minimum-font-size 20 -H --header-left \"Date : [date] [time]\" --header-right \"Page [page] of [toPage]\" --header-spacing 2 " + reqURL +" "+ strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + folderName + ".pdf");
            Process p = Runtime.getRuntime().exec(PDFSoftwarePath + " --footer-right \"Page [page] of [toPage]\" --margin-bottom 20mm --margin-top 35mm --header-html "+ HostandPort2 +"PDFHeader.html --header-spacing 5 --footer-spacing 5 --orientation Landscape --page-size A4 \"" + reqURL +"\" \""+ strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + folderName + ".pdf\"");
            //Process p = Runtime.getRuntime().exec(PDFSoftwarePath + " --footer-right \"Page [page] of [toPage]\" --margin-bottom 20mm --footer-spacing 5 --orientation Landscape --page-size A4 " + reqURL +" "+ strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + folderName + ".pdf");
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            while ((line = br.readLine()) != null) {
                System.out.println("Read from error stream: \"" + line + "\"");
            }
            br.close();
            try {
                logger.debug("genrateCmdForm" + logUserId + ":" + p.getOutputStream().toString());
                p.destroy();
            } catch (Exception ex) {
                ex.printStackTrace();
//                System.out.println("Generate PDF CMD ee"+new java.util.Date());
                logger.error("genrateCmdForm in output stream " + logUserId + " :" + ex.toString());
                AppExceptionHandler expHandle = new AppExceptionHandler();
                expHandle.stack2string(ex);
            }
            logger.debug("genrateCmdForm " + logUserId + " Ends");
        } catch (IOException e1) {
            e1.printStackTrace();
//            System.out.println("Generate PDF CMD ee1"+new java.util.Date());
            logger.error("genrateCmdForm Exception iO " + e1.toString());
            AppExceptionHandler expHandle = new AppExceptionHandler();
            expHandle.stack2string(e1);
        } catch (Exception e) {
            e.printStackTrace();
//            System.out.println("Generate PDF CMD ee2"+new java.util.Date());
            logger.error("genrateCmdForm Esception normal " + e.toString());
            AppExceptionHandler expHandle = new AppExceptionHandler();
            expHandle.stack2string(e);
        }
        logger.debug("genrateCmdForm " + logUserId + " Ends");
    }

    // Added to solve issue id  : 2226
    /**
     * Method for generate PDF for STD Forms
     * @param reqURL Url for which pdf generated
     * @param reqQuery Url query string value
     * @param folderName Folder name where to store PDF
     * @param id Object Id
     * @param pdfName Name of Pdf File
     */
    public void genrateSTDForm(String reqURL, String reqQuery, String folderName, String id, String pdfName) {
        logger.debug("genrateCmdForm " + logUserId + " Starts");
        try {
            //BufferedReader Filebr = new BufferedReader(new FileReader("C:\\eGP\\SoftwarePath\\PDFSoftwarePath.txt"));
            //PDFSoftwarePath = Filebr.readLine();
            String HostandPort = "";
            int SlashCounter = 0,CharPointer=0;
            
            String line;
            // Check the server information
            if(reqURL.contains("development.eprocure.gov.bd") || reqURL.contains("10.1.3.21")){
                isDevelopment = true;
            }
            if(reqURL.contains("staging.eprocure.gov.bd") || reqURL.contains("10.1.4.21")){
                isStaging = true;
            }
            if(reqURL.contains("training.eprocure.gov.bd") || reqURL.contains("10.1.40.25")){
                isTraining = true;
            }
            if(reqURL.contains("www.eprocure.gov.bd") || reqURL.contains("10.1.2.26") || reqURL.contains("10.1.2.27")){
                isProduction = true;
            }
            if(reqURL.contains("192.168.100.131/") || reqURL.contains("192.168.100.152/") || reqURL.contains("122.170.119.66/") || reqURL.contains("inhouse.eprocure.gov.bd/")){
                isInHouse = true;
            }

            /*
            if(isDevelopment || isTraining)
            {
                strDriveLetter = "D:";
            }
            else if(isStaging)
            {
                strDriveLetter = "E:";
            }
            else if(isProduction)
            {
                strDriveLetter = "\\\\egpprdblog\\AppSharedDrive";
            }
             */
            path = strDriveLetter+"/eGP/PDF/";

            logger.debug(reqQuery + "req URL--" + reqURL);
            File destFoldereGP = new File(strDriveLetter+"/eGP");
            if (destFoldereGP.exists() == false) {
                destFoldereGP.mkdirs();
            }

            File destFolderPDF = new File(strDriveLetter+"/eGP/PDF/");
            if (destFolderPDF.exists() == false) {
                destFolderPDF.mkdirs();
            }

            File destFolder = new File(strDriveLetter+"/eGP/PDF/" + folderName);
            if (destFolder.exists() == false) {
                destFolder.mkdirs();
            }

            File destFolder2 = new File(strDriveLetter+"/eGP/PDF/" + folderName + "/" + id);
            if (destFolder2.exists() == false) {
                destFolder2.mkdirs();
            }


            if (isDevelopment) {
                reqURL = reqURL.replace("development.eprocure.gov.bd", "10.1.3.21");
            }
            if (isStaging) {
                reqURL = reqURL.replace("staging.eprocure.gov.bd", "10.1.4.21");
                reqURL = reqURL.replace("www.","");
            }
            if (isTraining) {
                reqURL = reqURL.replace("training.eprocure.gov.bd", "10.1.40.25");
                reqURL = reqURL.replace("www.","");
            }
            if(isInHouse)
            {
                reqURL = reqURL.replace("inhouse.eprocure.gov.bd", "192.168.100.152:8090");
            }
            /*
            if (isProduction || isInHouse || isStaging || isTraining || reqURL.contains("localhost/")) {
                //reqURL = reqURL.replaceAll("eprocure.gov.bd", "10.1.2.26");
                reqURL = reqURL.replace("http", "https");
                reqURL = reqURL.replace("httpss", "https");
            }
            */
            String Protocol = "";
            if(reqURL.startsWith("https://"))
            {
                //reqURL = reqURL.replace("https://", "http://");
                Protocol = "https://";
            }
            else if(reqURL.startsWith("http://"))
            {
                Protocol = "http://";
            }
            else
            {
                //reqURL = "http://" + reqURL;
                Protocol = "http://";
            }
            
            while(SlashCounter<3)
            {
                if(reqURL.charAt(CharPointer)=='/')
                {
                    SlashCounter++;
                }
                HostandPort = HostandPort + reqURL.charAt(CharPointer);
                CharPointer++;
            }
            String HostandPort2 = Protocol + "localhost";
            if(Port != null && !Port.equalsIgnoreCase(""))
            {
                HostandPort2 = HostandPort2 + ":" + Port + "/";
            }
            else
            {
                HostandPort2 = HostandPort2 + "/";
            }
            reqURL = reqURL.replace(HostandPort, HostandPort2);
            reqURL = reqURL.replace("\"", "");
            reqURL = reqURL.replace(" ", "");
            System.out.println(reqURL);
            
            //PrintWriter writer = new PrintWriter("C:\\eGP\\SoftwarePath\\HeaderAddress.txt", "UTF-8");
            //writer.println(HostandPort +"PDFHeader.html");
            //writer.close();


            if(reqURL.trim().contains("&isPDF=true"))
            {
                reqURL = reqURL.trim().replace("&isPDF=true", "&isPDF=true&xulfz=d4f8r5s1");
            }
            logger.debug(PDFSoftwarePath + " --footer-right \"Page [page] of [toPage]\" --margin-bottom 20mm --margin-top 35mm --header-html "+ HostandPort2 +"PDFHeader.html --header-spacing 5 --footer-spacing 5 --orientation Landscape --page-size A4 " + reqURL + " "+"\""+strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + pdfName + ".pdf"+"\"");
            System.out.println(PDFSoftwarePath + " --footer-right \"Page [page] of [toPage]\" --margin-bottom 20mm --margin-top 35mm --header-html "+ HostandPort2 +"PDFHeader.html --header-spacing 5 --footer-spacing 5 --orientation Landscape --page-size A4 \"" + reqURL + "\" \""+strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + pdfName + ".pdf"+"\"");
//            Process p = Runtime.getRuntime().exec("wkhtmltopdf.exe --disable-smart-shrinking --minimum-font-size 20 -H --header-left \"Date : [date] [time]\" --header-right \"Page [page] of [toPage]\" --header-spacing 2 " + reqURL + " "+"\""+strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + pdfName + ".pdf"+"\"");
            Process p = Runtime.getRuntime().exec(PDFSoftwarePath + " --footer-right \"Page [page] of [toPage]\" --margin-bottom 20mm --margin-top 35mm --header-html "+ HostandPort2 +"PDFHeader.html --header-spacing 5 --footer-spacing 5 --orientation Landscape --page-size A4 \"" + reqURL + "\" \""+strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + pdfName + ".pdf"+"\"");
            //Process p = Runtime.getRuntime().exec(PDFSoftwarePath + " --footer-right \"Page [page] of [toPage]\" --margin-bottom 20mm --footer-spacing 5 --orientation Landscape --page-size A4 " + reqURL + " "+"\""+strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + pdfName + ".pdf"+"\"");
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            while ((line = br.readLine()) != null) {
                System.out.println("Read from error stream: \"" + line + "\"");
            }
            br.close();
            try {
                p.destroy();
                logger.debug("genrateCmdForm" + logUserId + ":" + p.getOutputStream().toString());
            } catch (Exception ex) {
                ex.printStackTrace();
//                System.out.println("Generate PDF CMD ey"+new java.util.Date());
                logger.error("genrateCmdForm in output stream " + logUserId + " :" + ex.toString());
                AppExceptionHandler expHandle = new AppExceptionHandler();
                expHandle.stack2string(ex);
            }
            logger.debug("genrateCmdForm " + logUserId + " Ends");
        } catch (IOException e1) {
            e1.printStackTrace();
//             System.out.println("Generate PDF CMD ey2"+new java.util.Date());
            logger.error("genrateCmdForm Exception iO " + e1.toString());
            AppExceptionHandler expHandle = new AppExceptionHandler();
            expHandle.stack2string(e1);
        } catch (Exception e) {
            e.printStackTrace();
//            System.out.println("Generate PDF CMD ey2"+new java.util.Date());
            logger.error("genrateCmdForm Esception normal " + e.toString());
            AppExceptionHandler expHandle = new AppExceptionHandler();
            expHandle.stack2string(e);
        }
        logger.debug("genrateCmdForm " + logUserId + " Ends");
    }
    /**
     *
     *This method is used for making report of tor1 and tor2
     * @param reqURL URL of tor1.jsp and tor2.jsp
     * @param reqQuery  Query sTring for tor1.jsp
     * @param id Name of folder tenderId_lotId
     * @param folderName Foldername is report
     * @param fileName  FileName is tor1_tenderId_lotId
     */
    public boolean genrateCmd(String reqURL, String reqQuery, String folderName, String id,String fileName) {
        boolean flag = true;
        try {
            //BufferedReader Filebr = new BufferedReader(new FileReader("C:\\eGP\\SoftwarePath\\PDFSoftwarePath.txt"));
            //PDFSoftwarePath = Filebr.readLine();
            String HostandPort = "";
            int SlashCounter = 0,CharPointer=0;
            
            // Check the server information

            if(reqURL.contains("development.eprocure.gov.bd") || reqURL.contains("10.1.3.21")){
                isDevelopment = true;
            }
            if(reqURL.contains("staging.eprocure.gov.bd") || reqURL.contains("10.1.4.21")){
                isStaging = true;
            }
            if(reqURL.contains("training.eprocure.gov.bd") || reqURL.contains("10.1.40.25")){
                isTraining = true;
            }
            if(reqURL.contains("www.eprocure.gov.bd") || reqURL.contains("10.1.2.26") || reqURL.contains("10.1.2.27")){
                isProduction = true;
            }
            if(reqURL.contains("192.168.100.131/") || reqURL.contains("192.168.100.152/") || reqURL.contains("122.170.119.66/") || reqURL.contains("inhouse.eprocure.gov.bd/")){
                isInHouse = true;
            }

            /*
            if(isDevelopment || isTraining)
            {
                strDriveLetter = "D:";
            }
            else if(isStaging)
            {
                strDriveLetter = "E:";
            }
            else if(isProduction)
            {
                strDriveLetter = "\\\\egpprdblog\\AppSharedDrive";
            }
             */
            path = strDriveLetter+"/eGP/PDF/";

            logger.debug("genrateCmd " + logUserId + " Starts");
            logger.debug(reqQuery + "req URL--" + reqURL);
            File destFoldereGP = new File(strDriveLetter+"/eGP");
            if (destFoldereGP.exists() == false) {
                destFoldereGP.mkdirs();
            }

            File destFolderPDF = new File(strDriveLetter+"/eGP/PDF/");
            if (destFolderPDF.exists() == false) {
                destFolderPDF.mkdirs();
            }

            File destFolder = new File(strDriveLetter+"/eGP/PDF/" + folderName);
            if (destFolder.exists() == false) {
                destFolder.mkdirs();
            }

            File destFolder2 = new File(strDriveLetter+"/eGP/PDF/" + folderName + "/" + id);
            if (destFolder2.exists() == false) {
                destFolder2.mkdirs();
            }

            // 115.127.40.5

            if (isDevelopment) {
                reqURL = reqURL.replace("development.eprocure.gov.bd", "10.1.3.21");
            }
            if (isStaging) {
                reqURL = reqURL.replace("staging.eprocure.gov.bd", "10.1.4.21");
                reqURL = reqURL.replace("www.","");
            }
            if (isTraining) {
                reqURL = reqURL.replace("training.eprocure.gov.bd", "10.1.40.25");
                reqURL = reqURL.replace("www.","");
            }
            if(isInHouse)
            {
                reqURL = reqURL.replace("inhouse.eprocure.gov.bd", "192.168.100.152:8090");
                //reqURL = reqURL.replace("122.170.119.66", "192.168.100.152");
            }
            /*
             if (isProduction || isInHouse || isStaging || isTraining || reqURL.contains("localhost/")) {
                //reqURL = reqURL.replace("www.eprocure.gov.bd", "10.1.2.26");
                 reqURL = reqURL.replace("http", "https");
                 reqURL = reqURL.replace("httpss", "https");
            }
            */
            String Protocol = "";
            if(reqURL.startsWith("https://"))
            {
                //reqURL = reqURL.replace("https://", "http://");
                Protocol = "https://";
            }
            else if(reqURL.startsWith("http://"))
            {
                Protocol = "http://";
            }
            else
            {
                //reqURL = "http://" + reqURL;
                Protocol = "http://";
            }
            
            while(SlashCounter<3)
            {
                if(reqURL.charAt(CharPointer)=='/')
                {
                    SlashCounter++;
                }
                HostandPort = HostandPort + reqURL.charAt(CharPointer);
                CharPointer++;
            }
            String HostandPort2 = Protocol + "localhost";
            if(Port != null && !Port.equalsIgnoreCase(""))
            {
                HostandPort2 = HostandPort2 + ":" + Port + "/";
            }
            else
            {
                HostandPort2 = HostandPort2 + "/";
            }
            reqURL = reqURL.replace(HostandPort, HostandPort2);
            reqURL = reqURL.replace("\"", "");
            reqURL = reqURL.replace(" ", "");
            System.out.println(reqURL);
            
            //PrintWriter writer = new PrintWriter("C:\\eGP\\SoftwarePath\\HeaderAddress.txt", "UTF-8");
            //writer.println(HostandPort +"PDFHeader.html");
            //writer.close();
            
            logger.debug(PDFSoftwarePath + " --footer-right \"Page [page] of [toPage]\" --margin-bottom 20mm --margin-top 35mm --header-html "+ HostandPort2 +"PDFHeader.html --header-spacing 5 --footer-spacing 5 --orientation Landscape --page-size A4 " + reqURL + "?" + reqQuery + "&isPDF=true&xulfz=d4f8r5s1 "+strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + fileName + ".pdf");
//            Process p = Runtime.getRuntime().exec("wkhtmltopdf.exe  --disable-smart-shrinking --minimum-font-size 20 -H --header-left \"Date : [date] [time]\" --header-right \"Page [page] of [toPage]\" --header-spacing 2 " + reqURL + "?" + reqQuery + "&isPDF=true&xulfz=d4f8r5s1 "+strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + fileName + ".pdf");
            Process p = Runtime.getRuntime().exec(PDFSoftwarePath + " --footer-right \"Page [page] of [toPage]\" --margin-bottom 20mm --margin-top 35mm --header-html "+ HostandPort2 +"PDFHeader.html --header-spacing 5 --footer-spacing 5 --orientation Landscape --page-size A4 \"" + reqURL + "?" + reqQuery + "&isPDF=true&xulfz=d4f8r5s1\" \""+strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + fileName + ".pdf\"");
            //Process p = Runtime.getRuntime().exec(PDFSoftwarePath + " --footer-right \"Page [page] of [toPage]\" --margin-bottom 20mm --footer-spacing 5 --orientation Landscape --page-size A4 " + reqURL + "?" + reqQuery + "&isPDF=true&xulfz=d4f8r5s1 "+strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + fileName + ".pdf");
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            System.out.println(PDFSoftwarePath + " --footer-right \"Page [page] of [toPage]\" --margin-bottom 20mm --margin-top 35mm --header-html "+ HostandPort2 +"PDFHeader.html --header-spacing 5 --footer-spacing 5 --orientation Landscape --page-size A4 \"" + reqURL + "?" + reqQuery + "&isPDF=true&xulfz=d4f8r5s1\" \""+strDriveLetter+"/eGP/PDF/" + folderName + "/" + id + "/" + fileName + ".pdf\"");
            while ((line = br.readLine()) != null) {
                System.out.println("Read from error stream: \"" + line + "\"");
            }
            br.close();
            p.destroy();
        } catch (IOException e1) {
            flag = false;
            e1.printStackTrace();
//             System.out.println("Generate PDF CMD ez2"+new java.util.Date());
            logger.error("genrateCmd " + logUserId + " :" + e1.toString());
            AppExceptionHandler expHandle = new AppExceptionHandler();
            expHandle.stack2string(e1);
        } catch (Exception e) {
            flag = false;
            e.printStackTrace();
//             System.out.println("Generate PDF CMD ez2"+new java.util.Date());
            logger.error("genrateCmd " + logUserId + " :" + e.toString());
            AppExceptionHandler expHandle = new AppExceptionHandler();
            expHandle.stack2string(e);
        }
        logger.debug("genrateCmd " + logUserId + " Ends");
        return flag;
    }

    /**
     * Get Path (Not using)
     * @param reqURL
     * @return
     */
    public String getPathForPdf(String reqURL){
        String path = "";
        /*String strDriveLetter = "C:";

        boolean isDevelopment = false;
        boolean isStaging = false;
        boolean isProduction = false;
        boolean isTraining = false;
        boolean isInHouse = false;
        if(reqURL.contains("development.eprocure.gov.bd") || reqURL.contains("10.1.3.21")){
                isDevelopment = true;
            }
            if(reqURL.contains("staging.eprocure.gov.bd") || reqURL.contains("10.1.4.21")){
                isStaging = true;
            }
            if(reqURL.contains("training.eprocure.gov.bd") || reqURL.contains("10.1.40.25")){
                isTraining = true;
            }
            if(reqURL.contains("www.eprocure.gov.bd") || reqURL.contains("10.1.2.26") || reqURL.contains("10.1.2.27")){
                isProduction = true;
            }
            if(reqURL.contains("192.168.100.131/") || reqURL.contains("192.168.100.152/") || reqURL.contains("61.17.38.85/")){
                isInHouse = true;
            }

            if(isDevelopment || isTraining)
            {
                strDriveLetter = "D:";
            }
            else if(isStaging)
            {
                strDriveLetter = "E:";
            }
            else if(isProduction)
            {
                strDriveLetter = "\\\\egpprdblog\\AppSharedDrive";
            }
         */
            path = strDriveLetter+"/eGP/PDF/";
//            System.out.print("Path--> "+path+ " ReqURL "+ reqURL);
            return path;
    }
}
