/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblWsMaster;
import com.cptu.egp.eps.service.serviceinterface.WsMasterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import java.util.Calendar;
import java.util.Date;

/**
 * This class is used to add a web-service and update a web-service.
 * @author Sreenu
 */
public class WebServiceControlServlet extends HttpServlet {

    static final Logger LOGGER = Logger.getLogger(WebServiceControlServlet.class);
    String logUserId = "0";
    static final String STARTS = " Starts";
    static final String ENDS = " Ends";
    static final String EMPTY_STRING = "";
    private WsMasterService wsMasterService = null;
    static final String ADD = "add";
    static final String EDIT = "edit";
    static final String STATUS_CHANGE = "statusChange";
    static final String ACTIVE = "active";
    static final String DEACTIVATE = "deactivate";
    private String message = EMPTY_STRING;

    /***
     * This web method process the request from adding a web-service or
     * editing the details of a web-service or change status of a webService
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.debug("processRequest " + logUserId + STARTS);
        wsMasterService = (WsMasterService) AppContext.getSpringBean("WsMasterService");
        String action = request.getParameter("action");
        if (STATUS_CHANGE.equalsIgnoreCase(action)) {
            changeStatus(request, response);
        } else {
            editOrCreationWs(request, response);
        }
        LOGGER.debug("processRequest " + logUserId + ENDS);
    }

    /***
     * This method changes the status of web service
     * @param request
     * @param response
     * @throws IOException
     */
    private void changeStatus(HttpServletRequest request, HttpServletResponse response) throws IOException {
        LOGGER.debug("changeStatus " + logUserId + STARTS);
        String status = request.getParameter("status");
        int webServiceId = Integer.parseInt(request.getParameter("webServiceId"));
        TblWsMaster tblWsMaster = null;
        message = STATUS_CHANGE;
        if (webServiceId != 0) {
            tblWsMaster = wsMasterService.getTblWsMaster(webServiceId);
        }
        if (tblWsMaster != null) {
            if (ACTIVE.equalsIgnoreCase(status)) {
                tblWsMaster.setIsActive("Y");
            } else if (DEACTIVATE.equalsIgnoreCase(status)) {
                tblWsMaster.setIsActive("N");
            }
            wsMasterService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
            wsMasterService.updateWsMaster(tblWsMaster,ACTIVE.equalsIgnoreCase(status)?"Activate":"Deactivate");
            response.sendRedirect("/admin/WSList.jsp?flag=true&message=" + message);
        }//null checking
        LOGGER.debug("changeStatus " + logUserId + ENDS);
    }//

    /***
     * This method calls when a request changes the web-service details or
     * adding a web-service
     * @param request
     * @param response
     * @throws IOException
     */
    private void editOrCreationWs(HttpServletRequest request, HttpServletResponse response) throws IOException {
        LOGGER.debug("editOrCreationWs " + logUserId + STARTS);
        String action = request.getParameter("action");
        TblWsMaster tblWsMaster = null;
        HttpSession session = request.getSession();
        String webServiceName = request.getParameter("txtWebServiceName");
        String webServiceDesc = request.getParameter("txtWebServiceDesc");
        String webServiceStatus = request.getParameter("selectWsStatus");
        String webServiceAuthentication = request.getParameter("selectWsAuthentication");
        Date currentDate = getCurrentDate();
        int userId = 0;
        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
            userId = Integer.parseInt(session.getAttribute("userId").toString());
        }
        if (ADD.equalsIgnoreCase(action)) {
            tblWsMaster = new TblWsMaster();
        } else {
            int webServiceId = Integer.parseInt(request.getParameter("webServiceId"));
            tblWsMaster = wsMasterService.getTblWsMaster(webServiceId);
        }
        tblWsMaster.setWsName(webServiceName);
        tblWsMaster.setWsDesc(webServiceDesc);
        tblWsMaster.setCreatedBy(userId);
        tblWsMaster.setCreatedDate(currentDate);
        tblWsMaster.setIsActive(webServiceStatus);
        tblWsMaster.setIsAuthenticate(webServiceAuthentication);
        if (ADD.equalsIgnoreCase(action)) {
            message = action;
            wsMasterService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
            wsMasterService.insertWsMaster(tblWsMaster);
            response.sendRedirect("/admin/WSList.jsp?flag=true&message=" + message);
        } else if (EDIT.equalsIgnoreCase(action)) {
            message = action;
            wsMasterService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
            wsMasterService.updateWsMaster(tblWsMaster,"Edit");
            response.sendRedirect("/admin/WSList.jsp?flag=true&message=" + message);
        }
        LOGGER.debug("editOrCreationWs " + logUserId + ENDS);
    }

    /***
     * This method returns the current date
     * @return Date
     */
    public Date getCurrentDate() {
        LOGGER.debug("getCurrentDate : " + STARTS);
        Calendar currentDate = Calendar.getInstance();
        Date dateNow = (currentDate.getTime());
        LOGGER.debug("getCurrentDate : " + ENDS);
        return dateNow;
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
