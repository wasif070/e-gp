/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.xmlpojos;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Chalapathi.Bavisetti
 */
public class AnnualProcurementPlan {

    private Integer appId;
    private String appCode;
    private String departmentName;
    private String officeName;
    private String stateName;
    private String procurementNature;
    private String projectName;
    private String packageDesc;
    private BigDecimal estimatedCost;
    private String procurementMethod;
    private String packageNo;
    private Integer officeId;

    /**
     * @return the appId
     */
    @XmlElement(nillable = true)
    public Integer getAppId() {
        return appId;
    }

    /**
     * @param appId the appId to set
     */
    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    /**
     * @return the appCode
     */
    @XmlElement(nillable = true)
    public String getAppCode() {
        return appCode;
    }

    /**
     * @param appCode the appCode to set
     */
    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    /**
     * @return the departmentName
     */
    @XmlElement(nillable = true)
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * @param departmentName the departmentName to set
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * @return the officeName
     */
    @XmlElement(nillable = true)
    public String getOfficeName() {
        return officeName;
    }

    /**
     * @param officeName the officeName to set
     */
    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    /**
     * @return the stateName
     */
    @XmlElement(nillable = true)
    public String getStateName() {
        return stateName;
    }

    /**
     * @param stateName the stateName to set
     */
    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    /**
     * @return the procurementNature
     */
    @XmlElement(nillable = true)
    public String getProcurementNature() {
        return procurementNature;
    }

    /**
     * @param procurementNature the procurementNature to set
     */
    public void setProcurementNature(String procurementNature) {
        this.procurementNature = procurementNature;
    }

    /**
     * @return the projectName
     */
    @XmlElement(nillable = true)
    public String getProjectName() {
        return projectName;
    }

    /**
     * @param projectName the projectName to set
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    /**
     * @return the packageDesc
     */
    @XmlElement(nillable = true)
    public String getPackageDesc() {
        return packageDesc;
    }

    /**
     * @param packageDesc the packageDesc to set
     */
    public void setPackageDesc(String packageDesc) {
        this.packageDesc = packageDesc;
    }

    /**
     * @return the estimatedCost
     */
    @XmlElement(nillable = true)
    public BigDecimal getEstimatedCost() {
        return estimatedCost;
    }

    /**
     * @param estimatedCost the estimatedCost to set
     */
    public void setEstimatedCost(BigDecimal estimatedCost) {
        this.estimatedCost = estimatedCost;
    }

    /**
     * @return the procurementMethod
     */
    @XmlElement(nillable = true)
    public String getProcurementMethod() {
        return procurementMethod;
    }

    /**
     * @param procurementMethod the procurementMethod to set
     */
    public void setProcurementMethod(String procurementMethod) {
        this.procurementMethod = procurementMethod;
    }

    /**
     *
     * @return returns PackageNo
     */
    @XmlElement(nillable = true)
    public String getPackageNo() {
        return packageNo;
    }

    /**
     * 
     * @param sets packageNo
     */
    public void setPackageNo(String packageNo) {
        this.packageNo = packageNo;
    }

    /**
     *
     * @return returns PackageNo
     */
    @XmlElement(nillable = true)
    public Integer getOfficeId() {
        return officeId;
    }

    /**
     *
     * @param sets OfficeId
     */
    public void setOfficeId(Integer officeId) {
        this.officeId = officeId;
    }
}
