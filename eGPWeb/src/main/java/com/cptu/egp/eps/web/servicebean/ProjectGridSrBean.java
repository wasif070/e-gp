/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.CommonAppData;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.service.serviceimpl.CreateProjectServiceImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author parag
 */
//@WebServlet(name="ProjectGridSrBean", urlPatterns={"/ProjectGridSrBean"})
public class ProjectGridSrBean extends HttpServlet {

    private CreateProjectServiceImpl projectService = (CreateProjectServiceImpl) AppContext.getSpringBean("CreateProjectService");
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final Logger LOGGER = Logger.getLogger(ProjectGridSrBean.class);
    private String logUserId = "0";
    /**
     * For Logging purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     * 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String action = request.getParameter("action");

        HttpSession session=request.getSession();
        if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
                projectService.setUserId(logUserId);
        }

        LOGGER.debug("processRequest : "+logUserId+" Starts");
       
        PrintWriter out = response.getWriter();
        int userTypeId=0;
        if(session.getAttribute("userTypeId") != null){
            userTypeId = Integer.parseInt(session.getAttribute("userTypeId").toString());
        }
        try {
            if ("viewProjectData".equalsIgnoreCase(action)) {
                LOGGER.debug("processRequest : action : viewProjectData : "+logUserId+" Starts");
                
                response.setContentType("text/xml;charset=UTF-8");
                String rows = request.getParameter("rows");
                //String page = request.getParameter("page");
                
                String sord = "desc";
                String sidx = "projectId";
                if(request.getParameter("sidx")!=null && !request.getParameter("sidx").toString().trim().equals("")){
                sidx = request.getParameter("sidx");
                sord = request.getParameter("sord");
                }
                //String rowList = request.getParameter("rowList");
                //String type = request.getParameter("govUserType");
                String status = request.getParameter("status");
                
                String str_queryForOreder = "order by "+sidx+" "+sord;
                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));

                List<CommonAppData> projData = null;
                String searchField = "", searchString = "", searchOper = "";
                if(_search){
                    searchField = request.getParameter("searchField");
                    searchString = request.getParameter("searchString");
                    searchOper = request.getParameter("searchOper");
                    
                    projData = getProjectData1(status, searchField, searchString, searchOper ,logUserId);
                    
                } else {
                    
                    projData = getProjectData(status,str_queryForOreder,logUserId);
                }

                int totalPages = 0;
                long totalCount = projData.size();
                
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(rows) == 0) {
                        totalPages = (int) (totalCount / Integer.parseInt(rows));
                    } else {
                        totalPages = (int) ((totalCount / Integer.parseInt(rows)) + 1);
                    }
                } else {
                    totalPages = 0;
                }

                //int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));
                 out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + projData.size() + "</records>");
                int srNo = 0;
                int j =0;
                int no = Integer.parseInt(request.getParameter("page"));
                if(no == 1){
                    j = 0;
                    srNo = 1;
                }else{
                    if(Integer.parseInt(rows) == 30){
                        j = ((no-1)*30);
                        srNo = ((no-1)*30)+1;
                    }else if(Integer.parseInt(rows) == 20){
                        j = ((no-1)*20);
                        srNo = ((no-1)*20)+1;
                    }
                    else{
                        j = ((no-1)*10);
                        srNo = ((no-1)*10)+1;
                    }
                }
                // be sure to put text data in CDATA
                //for (int i = (Integer.parseInt(page)-1)*10 ; i < projData.size(); i++) {
                for (int i = j ; i < projData.size(); i++) {
                  
                    out.print("<row id='" + projData.get(i).getFieldName1() + "'>");
                    out.print("<cell>" + srNo + "</cell>");
                    out.print("<cell> <![CDATA[" + projData.get(i).getFieldName3() + "]]> </cell>");
                    out.print("<cell> <![CDATA[" + projData.get(i).getFieldName2() + "]]> </cell>");
                    out.print("<cell> <![CDATA[" + projData.get(i).getFieldName4() + "]]> </cell>");
                  //  SimpleDateFormat sd = new SimpleDateFormat("yyyy-MMM-dd");
                 //   SimpleDateFormat sd1 = new SimpleDateFormat("dd-MMM-yyyy");
                    try{                        
                        //out.print("<cell>" + sd1.format(sd.parse(projData.get(i).getFieldName5().trim())) + "</cell>");
                        //out.print("<cell>" + sd1.format(sd.parse(projData.get(i).getFieldName6().trim())) + "</cell>");
                          out.print("<cell>" + (projData.get(i).getFieldName5().trim()) + "</cell>");
                        out.print("<cell>" + (projData.get(i).getFieldName6().trim()) + "</cell>");
                    }catch(Exception e){
                        out.print("<cell> - </cell>");
                        out.print("<cell> - </cell>");
                        
                    }
                    
                        
                    String editlink="";
                    String viewlink="";
                    String approve="";
                     if(userTypeId==4){
                    viewlink="<a href=\"ViewProjectDetail.jsp?projectId="+ projData.get(i).getFieldName1() +"&mode=View\"> View </a>";

                    if("pending".equalsIgnoreCase(status)){
                        editlink = "<a href=\"CreateEditProject.jsp?projectId=" + projData.get(i).getFieldName1() + "&Edit=Edit\"> Edit </a>";                                            
                        approve="<a href=\"ViewProjectDetail.jsp?projectId="+ projData.get(i).getFieldName1() +"&mode=approve\"> Approve </a>";
                    
                        out.print("<cell><![CDATA[" + editlink+ "]]> | <![CDATA[" + viewlink+ "]]> ");
                        boolean isDataAvailableFP = isProjectFPDataAvailable(Integer.parseInt(projData.get(i).getFieldName1()));
                        if(isDataAvailableFP){
                            out.print("| <![CDATA[" + approve+ "]]></cell>");
                        }else{
                            out.print("</cell>");
                        }
                    }else{
                        editlink = "<a href=\"CreateEditProject.jsp?projectId=" + projData.get(i).getFieldName1() + "&Edit=Edit\"> Edit </a>";
                        out.print("<cell><![CDATA[" + editlink+ "]]> | <![CDATA[" + viewlink+ "]]> </cell> ");
                    }
                    }else{
                        viewlink="<a href=\"ViewProjectDetail.jsp?projectId="+ projData.get(i).getFieldName1() +"&mode=View\"> View </a>";
                         out.print("<cell><![CDATA[" + editlink+ "]]>  <![CDATA[" + viewlink+ "]]> </cell> ");
                    }
                    
//                    if ("pending".equalsIgnoreCase(projData.get(i).getFieldName5())) {
//                       isDataAvailableFP =govtUserCreationService.isEmployeeDataAvailableInFP(Integer.parseInt(projData.get(i).getFieldName1()));
//                    }
                    out.print("</row>");
                    j++;
                    srNo++;
                }
                out.print("</rows>");
            }
            LOGGER.debug("processRequest : action : viewProjectData : "+logUserId+" Ends");
        } catch(Exception ex){
            LOGGER.error("getProjectData : "+logUserId+" : "+ex);
        } finally {
            out.close();
        }
        LOGGER.debug("processRequest : "+logUserId+" Ends");
    }

    /**
     * Get ProjectData
     * @param status
     * @param str_queryForOreder
     * @param userid
     * @return comonData
     */
    public List<CommonAppData> getProjectData(String status,String str_queryForOreder,String userid){
        LOGGER.debug("getProjectData : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        List<CommonAppData> commonAppDatas=null;
        try{
            if("pending".equalsIgnoreCase(status)){
                commonAppDatas = projectService.getDetailsBySP("PendingProject",str_queryForOreder,userid);
            }else{
                commonAppDatas = projectService.getDetailsBySP("ApproveProject",str_queryForOreder,userid);
            }
        }
        catch(Exception e){
            LOGGER.error("getProjectData : "+logUserId+" : "+e);
           
        }
        LOGGER.debug("getProjectData : "+logUserId+" Ends");
            return commonAppDatas;
        }

    /**
     * Get ProjectData while searching
     * @param status
     * @param searchField
     * @param searchString
     * @param searchOper
     * @param userid
     * @return
     */
    public List<CommonAppData> getProjectData1(String status, String searchField,String searchString,String searchOper,String userid){
        List<SPCommonSearchData> commonAppDatas=null;
        List<CommonAppData> commonAppData = null;
        LOGGER.debug("getProjectData1 : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
        commonSearchService.setLogUserId(logUserId);
        try{
           
            if("pending".equalsIgnoreCase(status)){
                commonAppDatas = commonSearchService.searchData("searchPendingProject", "", "", searchField, searchString, searchOper, userid, "", "", "");
            }else{
                commonAppDatas = commonSearchService.searchData("searchApproveProject", "", "", searchField, searchString, searchOper, userid, "", "", "");
            }
            commonAppData = new ArrayList<CommonAppData>();
            for(int i=0;i<commonAppDatas.size(); i++){
                CommonAppData cad = new CommonAppData();
                cad.setFieldName1(commonAppDatas.get(i).getFieldName1());
                cad.setFieldName2(commonAppDatas.get(i).getFieldName2());
                cad.setFieldName3(commonAppDatas.get(i).getFieldName3());
                cad.setFieldName4(commonAppDatas.get(i).getFieldName4());
                cad.setFieldName5(commonAppDatas.get(i).getFieldName5());
                commonAppData.add(cad);
                //cad = null;
            }
        }
        catch(Exception e){
            LOGGER.error("getProjectData1 : "+logUserId+" : "+e);
        }
        LOGGER.debug("getProjectData1 : "+logUserId+" Ends");
        return commonAppData;
    }

    /**
     * Financial Power Data is available or not
     * @param projectId
     * @return
     */
    public boolean isProjectFPDataAvailable(int projectId) {
        LOGGER.debug("isProjectFPDataAvailable : "+logUserId+" Starts");
        projectService.setUserId(logUserId);
        boolean flag = false;
         try {
             List<CommonAppData> isDataAvailable = projectService.getDetailsBySP("ProjectRoles", "" + projectId, "");
             if (!isDataAvailable.isEmpty()) {
                 flag = true;
             }
         } catch (Exception e) {
            LOGGER.error("isProjectFPDataAvailable : "+logUserId+" : "+e);
         }
         LOGGER.debug("isProjectFPDataAvailable : "+logUserId+" Ends");
        return flag;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
