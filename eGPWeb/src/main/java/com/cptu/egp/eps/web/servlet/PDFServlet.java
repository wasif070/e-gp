/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean;
import com.cptu.egp.eps.web.utility.GeneratePdfConstant;
import com.lowagie.text.DocumentException;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.xhtmlrenderer.pdf.ITextRenderer;

/**
 *
 * @author Administrator
 */
public class PDFServlet extends HttpServlet
{
    static final Logger LOGGER = Logger.getLogger(PDFServlet.class);
    String logUserId = "0";

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * Pass @param reportName 
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
            
    {
         
        
        try {
            if(request.getParameter("funName")!=null){
                response.setContentType("text/html;charset=UTF-8");
                PrintWriter out = response.getWriter();
            boolean flag = false;
                    String reqURL = request.getParameter("reqUrl");
                    if(reqURL.indexOf("192.168.100.152") > 0 || reqURL.indexOf("www.eprocure.gov.bd") > 0){
                        reqURL = reqURL.replace("http", "https");
                    }
                    String reqQuery = request.getParameter("queryString");
                    //String reqQuery = request.getQueryString() ;
                    GeneratePdfConstant pdfConstant = new GeneratePdfConstant();
                    String folderName = pdfConstant.TENDERNOTICE;
                    String id = request.getParameter("tenderId");
                    TenderDocumentSrBean tenderDocumentSrBean = new TenderDocumentSrBean();
                    flag = tenderDocumentSrBean.tenderPDFGenreation(Integer.parseInt(id),reqURL,reqQuery,folderName);
                    if(flag){
                        out.print("ok");
                    }
                    else{
                        out.print("error");
                    }
                    out.flush();
                    out.close();
        
        }else{
                    if (request.getSession().getAttribute("userId") != null) {
                        logUserId = request.getSession().getAttribute("userId").toString();
                    }
                    LOGGER.debug("PDFServlet : " + logUserId + " Starts");
                    String reportPage=request.getParameter("reportPage");
                    StringBuilder calledFrom=new StringBuilder();
                    if(reportPage==null || reportPage.isEmpty())
                        calledFrom.append(request.getHeader("referer"));
                    else
                    {
                        calledFrom.append("http://localhost:8080/eGPWeb/report/");
                        calledFrom.append(reportPage);
                    }
                    calledFrom.append("&print=yes");

                    String reportName=request.getParameter("reportName");

                    response.setContentType("application/pdf");
                    response.setHeader("Content-Disposition", "attachment;filename=\""+reportName+".pdf\"");
                    ITextRenderer renderer = new ITextRenderer();
                    renderer.setDocument(calledFrom.toString());
                    renderer.layout();
                    renderer.createPDF(response.getOutputStream());
                    response.getOutputStream().flush();
                    response.getOutputStream().close();
            }
            LOGGER.debug("PDFServlet : " + logUserId + " Ends");
        }
        catch (DocumentException ex) {
            LOGGER.error("PDFServlet : " + logUserId + ex);
        }
        }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
}
