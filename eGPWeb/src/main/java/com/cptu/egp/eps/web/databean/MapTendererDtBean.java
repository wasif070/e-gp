/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

/**
 *
 * @author TaherT
 */
public class MapTendererDtBean {

    private String emailId;
    private String contryOfBusiness;
    private String companyName;
    private String regsNo;
    private String comRegAddContry;
    private String comRegAddState;
    private String comRegCityTown;
    private String comRegThanaUpzilla;
    private String corHeadContry;
    private String corHeadState;    
    private String corHeadCityTown;
    private String corHeadThanaUpzilla;
    private String perAddContry;
    private String perAddState;
    private String perAddCityTown;
    private String perAddThanaUpzilla;

    public String getPerAddContry() {
        return perAddContry;
    }

    public void setPerAddContry(String perAddContry) {
        perAddContry = perAddContry.substring(perAddContry.indexOf("_")+1, perAddContry.length());
        this.perAddContry = perAddContry;
    }

    public String getComRegAddContry() {
        return comRegAddContry;
    }

    public void setComRegAddContry(String comRegAddContry) {
        comRegAddContry = comRegAddContry.substring(comRegAddContry.indexOf("_")+1, comRegAddContry.length());
        this.comRegAddContry = comRegAddContry;
    }

    public String getComRegAddState() {
        return comRegAddState;
    }

    public void setComRegAddState(String comRegAddState) {
        this.comRegAddState = comRegAddState;
    }

    public String getComRegCityTown() {
        return comRegCityTown;
    }

    public void setComRegCityTown(String comRegCityTown) {
        this.comRegCityTown = comRegCityTown;
    }

    public String getComRegThanaUpzilla() {
        return comRegThanaUpzilla;
    }

    public void setComRegThanaUpzilla(String comRegThanaUpzilla) {
        this.comRegThanaUpzilla = comRegThanaUpzilla;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getContryOfBusiness() {
        return contryOfBusiness;
    }

    public void setContryOfBusiness(String contryOfBusiness) {
        this.contryOfBusiness = contryOfBusiness;
    }

    public String getCorHeadCityTown() {
        return corHeadCityTown;
    }

    public void setCorHeadCityTown(String corHeadCityTown) {
        this.corHeadCityTown = corHeadCityTown;
    }

    public String getCorHeadContry() {
        return corHeadContry;
    }

    public void setCorHeadContry(String corHeadContry) {
        corHeadContry = corHeadContry.substring(corHeadContry.indexOf("_")+1, corHeadContry.length());
        this.corHeadContry = corHeadContry;
    }

    public String getCorHeadState() {
        return corHeadState;
    }

    public void setCorHeadState(String corHeadState) {
        this.corHeadState = corHeadState;
    }

    public String getCorHeadThanaUpzilla() {
        return corHeadThanaUpzilla;
    }

    public void setCorHeadThanaUpzilla(String corHeadThanaUpzilla) {
        this.corHeadThanaUpzilla = corHeadThanaUpzilla;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPerAddCityTown() {
        return perAddCityTown;
    }

    public void setPerAddCityTown(String perAddCityTown) {
        this.perAddCityTown = perAddCityTown;
    }

    public String getPerAddState() {
        return perAddState;
    }

    public void setPerAddState(String perAddState) {
        this.perAddState = perAddState;
    }

    public String getPerAddThanaUpzilla() {
        return perAddThanaUpzilla;
    }

    public void setPerAddThanaUpzilla(String perAddThanaUpzilla) {
        this.perAddThanaUpzilla = perAddThanaUpzilla;
    }

    public String getRegsNo() {
        return regsNo;
    }

    public void setRegsNo(String regsNo) {
        this.regsNo = regsNo;
    }
}
