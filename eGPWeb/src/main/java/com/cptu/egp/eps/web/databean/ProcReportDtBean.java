/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

import java.math.BigDecimal;
/**
 *
 * @author taher
 */
public class ProcReportDtBean {

    private final BigDecimal crore = new BigDecimal(10000000);
    private int counter;
    private String pNature;
    private String pMethod;
    private String nAPPNos;
    private String iAPPNos;
    private String nAPPEst;
    private String iAPPEst;
    private String nContractNos;
    private String iContractNos;
    private String nContractAmount;
    private String iContractAmount;

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getiAPPEst() {
        return iAPPEst;
    }

    public void setiAPPEst(String iAPPEst) {
        this.iAPPEst = iAPPEst!=null ? new BigDecimal(iAPPEst).divide(crore).setScale(3,0).toString() : iAPPEst;
    }

    public String getiAPPNos() {
        return iAPPNos;
    }

    public void setiAPPNos(String iAPPNos) {
        this.iAPPNos = iAPPNos;
    }

    public String getiContractAmount() {
        return iContractAmount;
    }

    public void setiContractAmount(String iContractAmount) {
        this.iContractAmount = iContractAmount!=null ? new BigDecimal(iContractAmount).divide(crore).setScale(3,0).toString() : iContractAmount;
    }

    public String getiContractNos() {
        return iContractNos;
    }

    public void setiContractNos(String iContractNos) {
        this.iContractNos = iContractNos;
    }

    public String getnAPPEst() {
        return nAPPEst;
    }

    public void setnAPPEst(String nAPPEst) {
        this.nAPPEst = nAPPEst!=null ? new BigDecimal(nAPPEst).divide(crore).setScale(3,0).toString() : nAPPEst;

    }

    public String getnAPPNos() {
        return nAPPNos;
    }

    public void setnAPPNos(String nAPPNos) {
        this.nAPPNos = nAPPNos;
    }

    public String getnContractAmount() {
        return nContractAmount;
    }

    public void setnContractAmount(String nContractAmount) {
        this.nContractAmount = nContractAmount!=null ? new BigDecimal(nContractAmount).divide(crore).setScale(3,0).toString() : nContractAmount;
    }

    public String getnContractNos() {
        return nContractNos;
    }

    public void setnContractNos(String nContractNos) {
        this.nContractNos = nContractNos;
    }

    public String getpMethod() {
        return pMethod;
    }

    public void setpMethod(String pMethod) {
        this.pMethod = pMethod;
    }

    public String getpNature() {
        return pNature;
    }

    public void setpNature(String pNature) {
        this.pNature = pNature;
    }

    public ProcReportDtBean(int counter, String pNature, String pMethod, String nAPPNos, String iAPPNos, String nAPPEst, String iAPPEst, String nContractNos, String iContractNos, String nContractAmount, String iContractAmount) {
        this.counter = counter;
        this.pNature = pNature;
        this.pMethod = pMethod;
        this.nAPPNos = nAPPNos;
        this.iAPPNos = iAPPNos;
        this.nAPPEst = nAPPEst!=null ? new BigDecimal(nAPPEst).divide(crore).setScale(3,0).toString()  : nAPPEst;
        this.iAPPEst = iAPPEst!=null ? new BigDecimal(iAPPEst).divide(crore).setScale(3,0).toString() : iAPPEst;
        this.nContractNos = nContractNos;
        this.iContractNos = iContractNos;
        this.nContractAmount = nContractAmount!=null ? new BigDecimal(nContractAmount).divide(crore).setScale(3,0).toString() : nContractAmount;
        this.iContractAmount = iContractAmount!=null ? new BigDecimal(iContractAmount).divide(crore).setScale(3,0).toString() : iContractAmount;
    }

    public ProcReportDtBean() {
    }
}
