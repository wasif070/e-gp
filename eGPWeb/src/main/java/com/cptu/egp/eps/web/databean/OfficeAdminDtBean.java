/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.databean;

import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblOfficeMaster;

/**
 *
 * @author Administrator
 */
public class OfficeAdminDtBean {

    public OfficeAdminDtBean() {
    }

     private int officeAdminId;
     private TblOfficeMaster tblOfficeMaster;
     private TblLoginMaster tblLoginMaster;
     private String adminType;

     private int officeId;
     private String officeName;

    public int getOfficeId() {
        return officeId;
    }

    public void setOfficeId(int officeId) {
        this.officeId = officeId;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    public OfficeAdminDtBean(int officeId, String officeName) {
        this.officeId = officeId;
        this.officeName = officeName;
    }

    public String getAdminType() {
        return adminType;
    }

    public void setAdminType(String adminType) {
        this.adminType = adminType;
    }

    public int getOfficeAdminId() {
        return officeAdminId;
    }

    public void setOfficeAdminId(int officeAdminId) {
        this.officeAdminId = officeAdminId;
    }

    public TblLoginMaster getTblLoginMaster() {
        return tblLoginMaster;
    }

    public void setTblLoginMaster(TblLoginMaster tblLoginMaster) {
        this.tblLoginMaster = tblLoginMaster;
    }

    public TblOfficeMaster getTblOfficeMaster() {
        return tblOfficeMaster;
    }

    public void setTblOfficeMaster(TblOfficeMaster tblOfficeMaster) {
        this.tblOfficeMaster = tblOfficeMaster;
    }



}
