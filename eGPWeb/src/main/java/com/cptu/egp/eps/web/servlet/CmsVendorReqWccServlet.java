/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblCmsVendorReqWcc;
import com.cptu.egp.eps.service.serviceimpl.ConsolodateService;
import com.cptu.egp.eps.service.serviceinterface.AccPaymentService;
import com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService;
import com.cptu.egp.eps.service.serviceinterface.CmsVendorReqWccService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rikin.p
 */
public class CmsVendorReqWccServlet extends HttpServlet {

    private int userId;
    private int lotId;
    private int cntId;
    private int tenderId;
    private CmsVendorReqWccService cmsVendorReqWccService = (CmsVendorReqWccService) AppContext.getSpringBean("CmsVendorReqWccService");
    private UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    private ConsolodateService c_ConsSrv = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
    private CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
    private CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
    private AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String logUserId = "0";
        CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
        int conId = c_ConsSrv.getContractId(Integer.parseInt(request.getParameter("tenderId")));
        if (request.getSession().getAttribute("userId") != null) {
            logUserId = request.getSession().getAttribute("userId").toString();
            accPaymentService.setLogUserId(logUserId);
            cmsConfigDateService.setLogUserId(logUserId);
            commonservice.setUserId(logUserId);
            registerService.setUserId(logUserId);
            cmsVendorReqWccService.setLogUserId(logUserId);
        }
        try {
            String procnature = commonservice.getProcNature(request.getParameter("tenderId")).toString();
            HttpSession session = request.getSession();
            String action = "";

            if (request.getParameter("action") != null && !"".equals(request.getParameter("action"))) {
                action = request.getParameter("action");
            }

            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                userId = Integer.parseInt(session.getAttribute("userId").toString());
            }
            if (request.getParameter("lotId") != null && !"".equals(request.getParameter("lotId"))) {
                lotId = Integer.parseInt(request.getParameter("lotId"));
            }
            if (request.getParameter("tenderId") != null && !"".equals(request.getParameter("tenderId"))) {
                tenderId = Integer.parseInt(request.getParameter("tenderId"));
            }

            if ("Request".equalsIgnoreCase(action)) {
                issueRequest(request);
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), conId, "contractId", EgpModule.Work_Completion_Certificate.getName(), "Tenderer Request for Work Completion Certificate", "");
                String str = "";
                if ("Services".equalsIgnoreCase(procnature)) {
                    str = "Consultant";
                } else if ("goods".equalsIgnoreCase(procnature)) {
                    str = "Supplier";
                } else {
                    str = "Contractor";
                }
                sendMailForSupplierRequestIssueWC(Integer.toString(tenderId), Integer.toString(lotId), logUserId, str);
                if ("services".equalsIgnoreCase(procnature)) {
                    if (request.getParameter("type") != null) {
                        response.sendRedirect("tenderer/ProgressReportMain.jsp?tenderId=" + tenderId + "&msg=reqested");
                    } else {
                        response.sendRedirect("tenderer/SrvLumpSumPr.jsp?tenderId=" + tenderId + "&msg=reqested");
                    }
                } else {
                    response.sendRedirect("tenderer/progressReport.jsp?tenderId=" + tenderId + "&msg=reqested");
                }
            }

        } finally {
            out.close();
        }
    }

    public void issueRequest(HttpServletRequest request) {

        if (request.getParameter("lotId") != null && !"".equals(request.getParameter("lotId"))) {
            lotId = Integer.parseInt(request.getParameter("lotId"));
        }
        if (request.getParameter("cntId") != null && !"".equals(request.getParameter("cntId"))) {
            cntId = Integer.parseInt(request.getParameter("cntId"));
        }
        TblCmsVendorReqWcc tblCmsVendorReqWcc = new TblCmsVendorReqWcc();
        tblCmsVendorReqWcc.setTendererId(userId);
        tblCmsVendorReqWcc.setReqDate(new java.util.Date());
        tblCmsVendorReqWcc.setLotId(lotId);
        tblCmsVendorReqWcc.setStatus("Pending");
        tblCmsVendorReqWcc.setWcCertiHistId(0);
        tblCmsVendorReqWcc.setCntId(cntId);
        cmsVendorReqWccService.addCmsVendorReqWcc(tblCmsVendorReqWcc);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /** send mail notification to PE on requesting issue work completion certificate by tenderer */
    private boolean sendMailForSupplierRequestIssueWC(String tenderId, String lotId, String logUserId, String str) {
        boolean flag = false;
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peEmailId = "";
            String strFrom = commonservice.getEmailId(logUserId);
            List<Object[]> peEmailObj = accPaymentService.getPEemailID(Integer.parseInt(tenderId));
            if (!peEmailObj.isEmpty()) {
                Object[] objj = peEmailObj.get(0);
                peEmailId = objj[0].toString();
            }
            String strTo[] = {peEmailId};
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String mailSubject = "e-GP : " + str + " has Requested for Issuing Work Completion Certificate";
            registerService.contentAdmMsgBox(strTo[0], strFrom, mailSubject, msgBoxContentUtility.RequestforissueneOfWorkCompletionCertificate(c_isPhysicalPrComplete, obj));
            String mailText = mailContentUtility.RequestforissueneOfWorkCompletionCertificate(c_isPhysicalPrComplete, obj);
            sendMessageUtil.setEmailTo(strTo);
            sendMessageUtil.setEmailFrom(strFrom);
            sendMessageUtil.setEmailSub(mailSubject);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
            StringBuilder sb = new StringBuilder();
            sb.append("Dear User,%0AThis is to inform you that " + str + " has requested for Work Completion Certificate ");
            sb.append("for Contract No." + obj[1].toString() + " (" + obj[15].toString() + ")" + " ");
            sb.append("%0AThanks,%0AeGP System");
            sendMessageUtil.setSmsBody(sb.toString());
            sendMessageUtil.sendSMS();
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }
}
