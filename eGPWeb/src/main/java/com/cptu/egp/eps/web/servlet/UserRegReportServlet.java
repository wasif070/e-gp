/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.web.servicebean.GenreatePdfCmd;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.GeneratePdfConstant;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sun.misc.BASE64Encoder;

/**
 *
 * @author Administrator
 */
public class UserRegReportServlet extends HttpServlet {
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    MakeAuditTrailService auditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String styleClass = "";
        String userName = request.getSession().getAttribute("userName").toString();
        CommonSearchDataMoreService searchService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
        auditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), Integer.parseInt(request.getSession().getAttribute("userId").toString()), "userId", EgpModule.Report.getName(), "Generate Registration Fee Payment Report", "");
        List<SPCommonSearchDataMore> spSearchData = null;
        List<SPCommonSearchDataMore> spCriteriaTotal = null;
         List<SPCommonSearchDataMore> spCriteriaVerifiedTotal = null;
          List<SPCommonSearchDataMore> spCriteriaPaidTotal = null;


        try {
            String action = request.getParameter("action");
            if (action.contains("Search")) {

                String status = "";
                String paymentFrom = "";
                String paymentTo = "";
                String emailId = "";
                String companyName = "";
                String[] dtArr = null;
                String sortEmail = "";
                String sortCmp = "";
                String branchName = null;
                String bankName = null;
                String paymentMode = "All";
                double totalAmount = 0;
                String strPaymentFrom = "";
                String strPaymentTo = "";
                int pageNo=1;
                String strPageNo="";
                int searchTotalTaka=0;
                int searchVerifiedTaka=0;
                int searchPaidTaka=0;
                int searchTotalUsd=0;
                int searchVerifiedUsd=0;
                int searchPaidUsd=0;


                if(request.getParameter("paymentFrom")!=null && !"".equalsIgnoreCase(request.getParameter("paymentFrom"))){
                    strPaymentFrom = request.getParameter("paymentFrom");
                    paymentFrom = request.getParameter("paymentFrom");
                    dtArr = paymentFrom.split("/");
                    paymentFrom = dtArr[2] + "-" + dtArr[1] + "-" + dtArr[0];
                }
                if(request.getParameter("paymetMode")!=null && !"".equalsIgnoreCase(request.getParameter("paymetMode"))){
                    paymentMode = request.getParameter("paymetMode");
                }
                if(request.getParameter("paymentTo")!=null && !"".equalsIgnoreCase(request.getParameter("paymentTo"))){
                   strPaymentTo = request.getParameter("paymentTo");
                    paymentTo = request.getParameter("paymentTo");
                   dtArr = paymentTo.split("/");
                   paymentTo = dtArr[2] + "-" + dtArr[1] + "-" + dtArr[0];
                }

                if(request.getParameter("emailId")!=null && !"".equalsIgnoreCase(request.getParameter("emailId"))){
                   emailId = request.getParameter("emailId");
                   sortEmail = request.getParameter("sortEmail");
                }
                if(request.getParameter("companyName")!=null && !"".equalsIgnoreCase(request.getParameter("companyName"))){
                   companyName = request.getParameter("companyName");
                   sortCmp = request.getParameter("sortCmp");
                }
                if(request.getParameter("status")!=null){
                   status = request.getParameter("status");
                }
                if(request.getParameter("branchName")!=null){
                   branchName = request.getParameter("branchName");
                }
                if(request.getParameter("bankName")!=null){
                   bankName = request.getParameter("bankName");
                }
                 if(request.getParameter("pageNo")!=null){
                 strPageNo = request.getParameter("pageNo");
                 pageNo = Integer.parseInt(strPageNo);
                }

                String strOffset = request.getParameter("size");
                int recordOffset = Integer.parseInt(strOffset);
                String fromLink = paymentMode+ ":"+status+":"+strPaymentFrom+":"+strPaymentTo+":"+emailId+":"+sortEmail+":"+companyName+":"+sortCmp+":"+bankName +":"+branchName+":"+strPageNo+":"+strOffset;
                if(fromLink.equalsIgnoreCase("All::::::::::1:10")){
                    fromLink = "";
                }
                int srNo = 0;
                srNo = (pageNo-1)*(recordOffset);
                spSearchData = searchService.geteGPData("getRegFeePayment_MISReport", strPageNo, strOffset, status, paymentFrom, paymentTo, emailId, companyName,sortEmail,sortCmp,bankName,branchName,paymentMode);
                spCriteriaTotal = searchService.geteGPData("getPaymentReportTotal",null,null,status, paymentFrom, paymentTo, emailId, companyName,sortEmail,sortCmp,bankName,branchName,paymentMode);
                spCriteriaVerifiedTotal = searchService.geteGPData("getPaymentReportTotal","verified",null,status, paymentFrom, paymentTo, emailId, companyName,sortEmail,sortCmp,bankName,branchName,paymentMode);
                spCriteriaPaidTotal = searchService.geteGPData("getPaymentReportTotal","paid",null,status, paymentFrom, paymentTo, emailId, companyName,sortEmail,sortCmp,bankName,branchName,paymentMode);
   if (action.equalsIgnoreCase("Search")) {
                try {
                if (spSearchData != null && !spSearchData.isEmpty()) {
                    String link = "";
                   for (int i = 0; i < spSearchData.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }
                        //By Emtaz Taka->Nu. on 28/April/2016
                        String TakatoNU = spSearchData.get(i).getFieldName10().replaceAll("Taka", "Nu.");
                        String regType= "";
                        if(spSearchData.get(i).getFieldName3().contains("Tenderer")){
                            regType=spSearchData.get(i).getFieldName3().replace("Tenderer", "Bidder");
                        }
                        else{
                         regType=spSearchData.get(i).getFieldName3();
                        }
                        out.print("<tr class='" + styleClass + "'>");
                        out.print("<td class=\"t-align-center\">" + (srNo+i+1) + "</td>");
                        out.print("<td class=\"t-align-center\">" + regType + "</td>");
                        out.print("<td class=\"t-align-center\">" + spSearchData.get(i).getFieldName4() + "</td>");
                        out.print("<td class=\"t-align-left\">"   + spSearchData.get(i).getFieldName5() + "</td>");
                        out.print("<td class=\"t-align-center\">" + spSearchData.get(i).getFieldName6() + "</td>");
                        out.print("<td class=\"t-align-center\">" + spSearchData.get(i).getFieldName7() + "</td>");
                        out.print("<td class=\"t-align-center\">" + spSearchData.get(i).getFieldName8() + "</td>");
                        out.print("<td class=\"t-align-center\">" + spSearchData.get(i).getFieldName14() + "</td>");
                        out.print("<td class=\"t-align-center\">" + spSearchData.get(i).getFieldName15() + "</td>");
                        out.print("<td class=\"t-align-center\">" + spSearchData.get(i).getFieldName9() + "</td>");
                        out.print("<td class=\"t-align-right\">" + TakatoNU + "</td>");
                        out.print("<td class=\"t-align-center\"><a href='ViewUserRegReport.jsp?payId="+spSearchData.get(i).getFieldName1()+"&uId="+spSearchData.get(i).getFieldName2()+"&searchCriteria="+new BASE64Encoder().encode(fromLink.getBytes())+"'>View Details</a></td>");
                        out.print("</tr>");

                    }
                    int totalPages = 1;

                    if (spSearchData.size() > 0) {
                        int count = Integer.parseInt(spSearchData.get(0).getFieldName12());
                        totalPages = (int) Math.ceil(Math.ceil(count) / recordOffset);
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                       
                     
                        
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                     //code for printing totals based on search criteria
    
                      

                } else {
                    out.print("<tr>");
                    out.print("<input type=\"hidden\" id=\"1\" value=\"1\">");
                    out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"12\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }

                } catch (Exception e) {
                    System.out.println("Error During payment report :"+e);
                }
                }
                else if (action.equalsIgnoreCase("SearchCriteria"))  
                {
                    out.print("<tr><td width=\"70%\" align='right' ><b>Grand Total Of Verified:</b></td><td align=\"right\"width=\"30%\" ><b>");
                    // Changed By Emtaz on 13/April/2016. BTN->Nu.
                    if(spCriteriaVerifiedTotal!=null&&!spCriteriaVerifiedTotal.isEmpty())
                    {
                       if(spCriteriaVerifiedTotal.get(0)!=null)
                       {
                           if(spCriteriaVerifiedTotal.get(0).getFieldName1().equalsIgnoreCase("BTN"))
                           {
                               out.print("Nu.:"+spCriteriaVerifiedTotal.get(0).getFieldName2());
                           }
                           else
                           {
                                out.print(spCriteriaVerifiedTotal.get(0).getFieldName1()+":"+spCriteriaVerifiedTotal.get(0).getFieldName2());
                           }
                       } 
                       else
                       {
                            // out.print("BTN:0");
                            out.print("Nu.:0");
                       }
                       if(spCriteriaVerifiedTotal.size()>1)
                       { 
                           if(spCriteriaVerifiedTotal.get(1)!=null)
                            {
                                 out.print("  " + spCriteriaVerifiedTotal.get(1).getFieldName1()+":"+spCriteriaVerifiedTotal.get(1).getFieldName2());
                            }
                            else
                            {
                                 out.print("USD:0");
                            }
                       }
                       else
                       {
                            out.print("USD:0");
                       }
                    }
                    else
                    {        
                      //out.print("BTN:0 USD:0"); 
                      out.print("Nu.:0 USD:0"); 
                    }
                    
                    out.print(" </b> </td></tr>");
                    out.print("<tr><td  width=\"70%\" align='right' ><b>Grand Total Of Paid:</b></td><td align=\"right\"width=\"30%\" ><b>");

                    if(spCriteriaPaidTotal!=null&&!spCriteriaPaidTotal.isEmpty())
                    {
                       if(spCriteriaPaidTotal.get(0)!=null)
                       {
                           if(spCriteriaPaidTotal.get(0).getFieldName1().equalsIgnoreCase("BTN"))
                           {
                               out.print("Nu.:"+spCriteriaPaidTotal.get(0).getFieldName2());
                           }
                           else
                           {
                                out.print(spCriteriaPaidTotal.get(0).getFieldName1()+":"+spCriteriaPaidTotal.get(0).getFieldName2());
                           }
                       } 
                       else
                       {
                        //out.print("BTN:0");
                        out.print("Nu.:0");
                       }
                       if(spCriteriaPaidTotal.size()>1)
                       { 
                           if(spCriteriaPaidTotal.get(1)!=null)
                           {
                               out.print("  " + spCriteriaPaidTotal.get(1).getFieldName1()+":"+spCriteriaPaidTotal.get(1).getFieldName2());
                           }
                           else
                           {
                               out.print("USD:0");
                           }
                       }
                       else
                       {
                           out.print("USD:0");
                       }
                    }
                    else
                    {
                      //out.print("BTN:0 USD:0");
                      out.print("Nu.:0 USD:0");
                    }
                    out.print(" </b> </td></tr>");
                    out.print("<tr><td  width=\"70%\" align='right' ><b>Grand Total: </b></td><td align=\"right\"width=\"30%\" ><b>");

                    if(spCriteriaTotal!=null&&!spCriteriaTotal.isEmpty())
                    {
                        if(spCriteriaTotal.get(0)!=null)
                        {
                            if(spCriteriaTotal.get(0).getFieldName1().equalsIgnoreCase("BTN"))
                            {
                                out.print("Nu.:"+spCriteriaTotal.get(0).getFieldName2());
                            }
                            else
                            {
                                out.print(spCriteriaTotal.get(0).getFieldName1()+":"+spCriteriaTotal.get(0).getFieldName2());
                            }
                        } 
                        else
                        {
                            // out.print("BTN:0");
                            out.print("Nu.:0");
                        }
                        if(spCriteriaTotal.size()>1)
                        { 
                            if(spCriteriaTotal.get(1)!=null)
                            {
                                 out.print("  " + spCriteriaTotal.get(1).getFieldName1()+":"+spCriteriaTotal.get(1).getFieldName2());
                            }
                            else
                            {
                                 out.print("USD:0");
                            }
                        }
                        else
                        {
                            out.print("USD:0");
                        }
                    }
                    else
                    {
                        // out.print("BTN:0 USD:0");
                        out.print("Nu.:0 USD:0");
                    }
                    out.print(" </b> </td></tr>");

                }

                try {
                    String userId = String.valueOf(request.getSession().getAttribute("userId"));
                    GenreatePdfCmd obj = new GenreatePdfCmd();
                    GeneratePdfConstant constant = new GeneratePdfConstant();
                    String cdate = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date(new Date().getTime()));
                    String folderName = constant.PAYMENT;
                    String genId = cdate + "_" + userId;
                    if("=".equalsIgnoreCase(sortCmp)){
                        sortCmp = "eq";
                    }
                    if("=".equalsIgnoreCase(sortEmail)){
                        sortEmail = "eq";
                    }
                    int userTypeID = Integer.parseInt(request.getSession().getAttribute("userTypeId").toString());
                    if(userTypeID== 1 || userTypeID== 8) {
                        strOffset = "0";
                    }
                    String reqURL = request.getRequestURL().toString();
                    reqURL = reqURL.replace("UserRegReportServlet", "admin/UserRegReportPdf.jsp");
                    String reqQuery = "param1=getRegFeePayment_MISReport&strPageNo="+strPageNo+"&strOffset="+strOffset+"&status="+status+"&paymentFrom="+paymentFrom+"&paymentTo="+paymentTo+"&emailId="+emailId+"&companyName="+companyName+"&bankName="+bankName.replace(" ", "_") +"&branchName="+branchName.replace(" ", "_")+"&sortEmail="+sortEmail+"&sortCmp="+sortCmp;
                    obj.genrateCmd(reqURL, reqQuery, folderName, genId);
                } catch (Exception e) {
                    System.out.println("exception-" + e);
                }

            }

        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
