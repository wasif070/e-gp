/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonSPWfFileHistory;
import com.cptu.egp.eps.web.servicebean.WorkFlowSrBean;
import com.cptu.egp.eps.web.utility.DateUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author test
 */
public class WorkFlowHistoryServlet extends HttpServlet {

     private static final Logger LOGGER = Logger.getLogger(WorkFlowHistoryServlet.class);
     private static final String LOGGERSTART = "Starts";
     private static final String LOGGEREND  = "Ends";
     private String logUserId ="0";
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {   
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        LOGGER.debug("processRequest : "+logUserId+ LOGGERSTART);
        try {
            response.setContentType("text/xml;charset=UTF-8");
                Integer recPerPage = Integer.parseInt(request.getParameter("rows"));
                Integer pag =Integer.parseInt(request.getParameter("page"));
                String sord = request.getParameter("sord");
                String sidx = request.getParameter("sidx");
                  if("processDate".equals(sidx)){
                  sidx = "processDate";
                 }else{
                    sord = "desc";
                    sidx = "processDate";
                 }
                 int totalpages = 0;
                 int totalRecords = 0;
                 String objectid = request.getParameter("objectid");
                String childid = request.getParameter("childid");
                String funName = request.getParameter("funName");
                String activityid = request.getParameter("activityid");
                String userid = request.getParameter("userid");
                WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                 List<CommonSPWfFileHistory> commonSpWfFileHistory = workFlowSrBean.getWfHistory(funName, Integer.parseInt(activityid),
                                      Integer.parseInt(objectid),Integer.parseInt(childid) ,Integer.parseInt(userid), pag, recPerPage,sidx,sord,"","","",0,"","");
                                      request.setAttribute("commonSpWfFileHistory", commonSpWfFileHistory);

                 CommonSPWfFileHistory cspwf = null;
                 if(!commonSpWfFileHistory.isEmpty()){
                   cspwf =  commonSpWfFileHistory.get(0);
                  totalRecords =  cspwf.getTotalRecords().intValue();
                  if(totalRecords%recPerPage == 0){
                      totalpages = totalRecords/recPerPage;
                  }else{
                       totalpages = totalRecords/recPerPage+1;
                      }

                   }

           StringBuilder sb = new StringBuilder();
           sb.append("<?xml version='1.0' encoding='utf-8'?><rows><page>" + pag + "</page><total>" + totalpages + "</total><records>" +totalRecords+ "</records>");
                                 int j = 1;
           if(!commonSpWfFileHistory.isEmpty()){
           for (int i = 0; i < commonSpWfFileHistory.size(); i++) {                
                                    String str3 = DateUtils.gridDateToStr(commonSpWfFileHistory.get(i).getProcessDate());
                                    String str4 = "<a target='_blank' href='"+request.getContextPath()+"/officer/CommentsView.jsp?wfhistoryid="+commonSpWfFileHistory.get(i).getWfHistoryId()+"' style='text-decoration:underline;' title='comments' )'>View</a>";
                sb.append("<row><cell>"+j+"</cell><cell><![CDATA[" + commonSpWfFileHistory.get(i).getFileSentFrom() + "]]></cell><cell>" +str3 + "</cell><cell><![CDATA["+str4+"]]></cell>");

                if("Approve".equalsIgnoreCase(commonSpWfFileHistory.get(i).getAction())){
                                        sb.append("<cell><![CDATA[Approved]]></cell>");
               } else if ("Forward".equalsIgnoreCase(commonSpWfFileHistory.get(i).getAction())){
                                        sb.append("<cell><![CDATA[Forwarded]]></cell>");
               } else if ("Pull".equalsIgnoreCase(commonSpWfFileHistory.get(i).getAction())){
                                        sb.append("<cell><![CDATA[Pulled]]></cell>");
               } else if ("Return".equalsIgnoreCase(commonSpWfFileHistory.get(i).getAction())){
                                        sb.append("<cell><![CDATA[Returned]]></cell>");
               } else if ("Reject".equalsIgnoreCase(commonSpWfFileHistory.get(i).getAction())){
                                        sb.append("<cell><![CDATA[Rejected]]></cell>");
               } else if ("Conditional Approval".equalsIgnoreCase(commonSpWfFileHistory.get(i).getAction())){
                                        sb.append("<cell><![CDATA[Conditional Approved]]></cell>");
               } else{
                                        sb.append("<cell><![CDATA[" + commonSpWfFileHistory.get(i).getAction() + "]]></cell>");
               }
               sb.append("<cell><![CDATA[" + commonSpWfFileHistory.get(i).getFileSentTo()+ "]]></cell></row>");
                                   j++;
                                }
                                 }else {
                sb.append("<row><cell>"+j+"</cell><cell>No Data Found</cell><cell>No Data Found</cell><cell>No Data Found</cell><cell>No Data Found</cell><cell>No Data Found</cell></row>");
                                }
                                       sb.append("</rows>");
                                       out.print(sb.toString());
        }catch(Exception ex){
             LOGGER.error("processRequest "+logUserId+" : "+ex.toString());
        } finally {
            out.close();
        }
        LOGGER.debug("processRequest : "+logUserId+ LOGGEREND);
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
