/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblConfigClarification;
import com.cptu.egp.eps.model.table.TblTemplateMaster;
import com.cptu.egp.eps.service.serviceimpl.ConfigClarificationService;
import com.cptu.egp.eps.service.serviceimpl.TemplateMasterImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.cptu.egp.eps.web.servicebean.GenreatePdfCmd;
import com.cptu.egp.eps.web.servicebean.DefineSTDInDtlSrBean;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.ArrayList;
import org.apache.log4j.Logger;
/**
 *
 * @author yanki
 */
//@WebServlet(name="StdGrid", urlPatterns={"/StdGrid"})
public class StdGrid extends HttpServlet {
    static final Logger LOGGER = Logger.getLogger(StdGrid.class);
    private AuditTrail auditTrail;
    TemplateMasterImpl tempMstrList = (TemplateMasterImpl) AppContext.getSpringBean("TemplateMasterService");
    private final ConfigClarificationService configService = (ConfigClarificationService) AppContext.getSpringBean("ConfigClarificationService");
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        tempMstrList.setAuditTrail(auditTrail);
    }
    
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String strStarts = " Starts", strEnds = " Ends";
        HttpSession httpSession = request.getSession();
        String logUserId = httpSession.getAttribute("userId") == null ? "0":httpSession.getAttribute("userId").toString();
        int logUserTypeId = httpSession.getAttribute("userTypeId") == null ? 0: Integer.parseInt(httpSession.getAttribute("userTypeId").toString());
        LOGGER.debug("processRequest : " + logUserId + strStarts);
        PrintWriter out = response.getWriter();
        try {
            if(httpSession.getAttribute("userId") == null){
                response.sendRedirect("SessionTimedOut.jsp");
            }else{
                String action = request.getParameter("action");
                LOGGER.debug("processRequest : Action = " + action + " : User ID " + logUserId+ " logUserTypeId "+logUserTypeId);
                
                //<editor-fold>
                /*if("getData".equals(action)){
                    
                    response.setContentType("text/xml;charset=UTF-8");
                    boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                    String rows = request.getParameter("rows");
                    String page = request.getParameter("page");
                    String sord = request.getParameter("sord");
                    String sidx = request.getParameter("sidx");
                    if("".equals(sidx)){
                        sord = "desc";
                        sidx = "templateId";
                    }
                    String tStatus = "P";
                    if("A".equals(request.getParameter("stat"))){
                        tStatus = "A";
                    } else if("C".equals(request.getParameter("stat"))){
                        tStatus = "C";
                    }
                    int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));
                    int totalPages = 0;
                    int totalCount = 0;
                    String searchField = "", searchString = "", searchOper = "";
                    if(_search){
                        searchField = request.getParameter("searchField");
                        searchString = request.getParameter("searchString");
                        searchOper = request.getParameter("searchOper");
                        totalCount = (int) tempMstrList.getSearchNoOfSTDs(tStatus, searchField, searchString, searchOper);
                    } else {
                        totalCount = (int) tempMstrList.getTotalNoOfSTDs(tStatus);
                    }
                    
                    if (totalCount > 0) {
                        if (totalCount % Integer.parseInt(rows) == 0) {
                            totalPages = totalCount / Integer.parseInt(rows);
                        } else {
                            totalPages = (totalCount / Integer.parseInt(rows)) + 1;
                        }
                    } else {
                        totalPages = 0;
                    }

                    List<TblTemplateMaster> tMaster = null;

                    if(_search){
                        tMaster = tempMstrList.getTemplateList(offset, Integer.parseInt(rows), sord, sidx, tStatus, searchField, searchString, searchOper);
                    } else {
                        tMaster = tempMstrList.getTemplateList(offset, Integer.parseInt(rows), sord, sidx, tStatus);
                    }
                            
                    //XML Creation Start
                    out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                    out.print("<rows>");
                    out.print("<page>" + page + "</page>");
                    out.print("<total>" + totalPages + "</total>");
                    out.print("<records>" + totalCount + "</records>");
                    for (int i = 0; i < tMaster.size(); i++) {
                        out.print("<row id='" + tMaster.get(i).getTemplateId() + "'>");
                        out.print("<cell>" + (offset + i + 1) + "</cell>");
                        out.print("<cell><![CDATA[" + tMaster.get(i).getTemplateName() + "]]></cell>");

                        if(tMaster.get(i).getProcType() == null){
                            out.print("<cell><![CDATA[-]]></cell>");
                        } else {
                            if("goods".equalsIgnoreCase(tMaster.get(i).getProcType())){
                                out.print("<cell><![CDATA[Goods]]></cell>");
                            }else if("works".equalsIgnoreCase(tMaster.get(i).getProcType())){
                                out.print("<cell><![CDATA[Works]]></cell>");
                            }else if("srvcmp".equalsIgnoreCase(tMaster.get(i).getProcType())){
                                out.print("<cell><![CDATA[Services - Consulting Firms]]></cell>");
                            }else if("srvindi".equalsIgnoreCase(tMaster.get(i).getProcType())){
                                out.print("<cell><![CDATA[Services - Individual Consultant]]></cell>");
                            }
                            else if("srvnoncon".equalsIgnoreCase(tMaster.get(i).getProcType())){
                                out.print("<cell><![CDATA[Services - Non consulting services]]></cell>");
                        }
                        }
                        out.print("<cell><![CDATA[" + tMaster.get(i).getNoOfSections() + "]]></cell>");
                        //<a href=\"#\"> Edit </a> |
                        String acceptLink ="";
                        if("P".equals(tStatus)){
                            if(logUserTypeId == 1){ // Accept is only available for egpAdmin
                                acceptLink = " | <a href=\"ViewTemplate.jsp?templateId=" + tMaster.get(i).getTemplateId() + "&action=acc\">Accept</a> | <a onclick='return confirm(\"Do you really want to delete the STD?\");' href=\"../StdGrid?action=delStd&templateId=" + tMaster.get(i).getTemplateId() + "\" > Delete </a>" ;
                            }else if(logUserTypeId == 8){
                                acceptLink = " | <a href=\"ViewTemplate.jsp?templateId=" + tMaster.get(i).getTemplateId() + "&action=acc\">Accept</a>";
                            }
                            out.print("<cell><![CDATA[" + "<a href=\"GuidenceDocUpload.jsp?templateId=" + tMaster.get(i).getTemplateId() + "\">Guidance Notes</a> | <a href=\"DefineSTDInDtl.jsp?templateId=" + tMaster.get(i).getTemplateId() + "\">Dashboard</a> "+ acceptLink + "]]></cell>");
                        }
                        if("A".equals(tStatus)){
                            if(logUserTypeId == 1){ // Accept is only available for egpAdmin
                                acceptLink = " | <a href=\"ViewTemplate.jsp?templateId=" + tMaster.get(i).getTemplateId() + "&action=can\">Cancel</a>";
                            }
                            out.print("<cell><![CDATA[" +"<a href=\"GuidenceDocUpload.jsp?templateId=" + tMaster.get(i).getTemplateId() + "\">Guidance Notes</a> | <a href=\"DefineSTDInDtl.jsp?templateId=" + tMaster.get(i).getTemplateId() + "\">Dashboard</a>" + acceptLink + "]]></cell>");
                        }
                        //if("P".equals(tStatus)){
                        //    out.print("<cell><![CDATA[" + "<a href=\"DefineSTDInDtl.jsp?templateId=" + tMaster.get(i).getTemplateId() + "\"> Dashboard </a> | " + "<a href=\"../StdGrid?action=delStd&templateId=" + tMaster.get(i).getTemplateId() + "\" onclick='return confirmDelete();'> Delete </a>" + "]]></cell>");
                        //}
                        if("C".equals(tStatus)){
                            out.print("<cell><![CDATA[" + "<a href=\"DefineSTDInDtl.jsp?templateId=" + tMaster.get(i).getTemplateId() + "\"> Dashboard </a>" + "]]></cell>");
                        }
                        out.print("</row>");
                    }
                    out.print("</rows>");
                    //XML Creation End
                }*/
                //</editor-fold>
                
                if("getData".equals(action)){
                    
                    response.setContentType("text/html;charset=UTF-8");
                    //response.setContentType("text/xml;charset=UTF-8");
                    //boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
//                    String rows = request.getParameter("rows");
//                    String page = request.getParameter("page");
//                    String sord = request.getParameter("sord");
//                    String sidx = request.getParameter("sidx");
                    
                    int pageNo = Integer.parseInt(request.getParameter("pageNo"));
                    int Size = Integer.parseInt(request.getParameter("size"));
                    String SBDName = request.getParameter("SBDName");
                   
                    String tStatus = "P";
                    if("A".equals(request.getParameter("stat"))){
                        tStatus = "A";
                    } else if("C".equals(request.getParameter("stat"))){
                        tStatus = "C";
                    }
                    int offset = (pageNo - 1) * Size;
                    int totalPages = 0;
                    int totalCount = 0;
                    //String searchField = "", searchString = "", searchOper = "";
                    
                    totalCount = (int) tempMstrList.getTotalNoOfSTDs(tStatus);
                    
                    if (totalCount > 0) {
                        if (totalCount % Size == 0) {
                            totalPages = totalCount / Size;
                        } else {
                            totalPages = (totalCount / Size) + 1;
                        }
                    } else {
                        totalPages = 0;
                    }

                    List<TblTemplateMaster> tMaster = null;
                    List<TblTemplateMaster> tMasterSearched = new ArrayList<>();


                    tMaster = tempMstrList.getTemplateList(offset, Size, "asc", "", tStatus);
                    
                    for(int j=0;j<tMaster.size();j++)
                    {
                        boolean ToAdd = true;
                        if(SBDName!=null && !SBDName.equalsIgnoreCase(""))
                        {
                            if(!tMaster.get(j).getTemplateName().toLowerCase().contains(SBDName.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                       
                        if(ToAdd)
                        {
                            //SPCommonSearchData commonAppData = getGovData.get(j);
                            tMasterSearched.add(tMaster.get(j));
                        }
                    }
                    
                    int RecordFrom = (pageNo-1)*Size;
                    int k= 0;
                    String styleClass = "";
                    if (tMasterSearched != null && !tMasterSearched.isEmpty()) {
                        for(k=RecordFrom;k<RecordFrom+Size && k<tMasterSearched.size();k++)
                        {
//                            if(k%2==0){
//                                styleClass = "bgColor-white";
//                            }else{
//                                styleClass = "bgColor-Green";
//                            }
                            out.print("<tr class='"+styleClass+"'>");
                            out.print("<td width=\"5%\" class=\"t-align-center\">" + (k+1) + "</td>");
                            out.print("<td width=\"40%\" class=\"t-align-center\">" + tMasterSearched.get(k).getTemplateName() + "</td>");
                            if(tMasterSearched.get(k).getProcType() == null)
                            {
                                out.print("<td width=\"20%\" class=\"t-align-center\"> - </td>");
                            } 
                            else 
                            {
                                if("goods".equalsIgnoreCase(tMasterSearched.get(k).getProcType()))
                                {
                                    out.print("<td width=\"20%\" class=\"t-align-center\"> Goods </td>");
                                }
                                else if("works".equalsIgnoreCase(tMasterSearched.get(k).getProcType()))
                                {
                                    out.print("<td width=\"20%\" class=\"t-align-center\"> Works </td>");
                                }
                                else if("srvcmp".equalsIgnoreCase(tMasterSearched.get(k).getProcType()))
                                {
                                    out.print("<td width=\"20%\" class=\"t-align-center\"> Services - Consulting Firms </td>");
                                }
                                else if("srvindi".equalsIgnoreCase(tMasterSearched.get(k).getProcType()))
                                {
                                    out.print("<td width=\"20%\" class=\"t-align-center\"> Services - Individual Consultant </td>");
                                }
                                else if("srvnoncon".equalsIgnoreCase(tMasterSearched.get(k).getProcType()))
                                {
                                    out.print("<td width=\"20%\" class=\"t-align-center\"> Services - Non consulting services </td>");
                                }
                            }
                            
                            String acceptLink ="";
                            if("P".equals(tStatus)){
                                if(logUserTypeId == 1){ // Accept is only available for egpAdmin
                                    acceptLink = " | <a href=\"ViewTemplate.jsp?templateId=" + tMasterSearched.get(k).getTemplateId() + "&action=acc\">Accept</a> | <a onclick='return confirm(\"Do you really want to delete the SBD?\");' href=\"../StdGrid?action=delStd&templateId=" + tMasterSearched.get(k).getTemplateId() + "\" > Delete </a>" ;
                                }else if(logUserTypeId == 8){
                                    acceptLink = " | <a href=\"ViewTemplate.jsp?templateId=" + tMasterSearched.get(k).getTemplateId() + "&action=acc\">Accept</a>";
                                }
                                out.print("<td width=\"35%\" class=\"t-align-center\">" + "<a href=\"GuidenceDocUpload.jsp?templateId=" + tMasterSearched.get(k).getTemplateId() + "\">Guidance Notes</a> | <a href=\"DefineSTDInDtl.jsp?templateId=" + tMasterSearched.get(k).getTemplateId() + "\">Dashboard</a> "+ acceptLink + "</td>");
                            }
                            if("A".equals(tStatus)){
                                if(logUserTypeId == 1){ // Accept is only available for egpAdmin
                                    acceptLink = " | <a href=\"ViewTemplate.jsp?templateId=" + tMasterSearched.get(k).getTemplateId() + "&action=can\">Cancel</a>";
                                }
                                out.print("<td width=\"35%\" class=\"t-align-center\">" + "<a href=\"GuidenceDocUpload.jsp?templateId=" + tMasterSearched.get(k).getTemplateId() + "\">Guidance Notes</a> | <a href=\"DefineSTDInDtl.jsp?templateId=" + tMasterSearched.get(k).getTemplateId() + "\">Dashboard</a>" + acceptLink + "</td>");
                            }
                            
                            if("C".equals(tStatus)){
                                out.print("<td width=\"35%\" class=\"t-align-center\">" + "<a href=\"DefineSTDInDtl.jsp?templateId=" + tMasterSearched.get(k).getTemplateId() + "\"> Dashboard </a>" + "</td>");
                            }
                            out.print("</tr>");
                        }

                    }
                    else
                    {
                        out.print("<tr>");
                        out.print("<td colspan=\"4\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        out.print("</tr>");
                    }  
                    
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                            
                    //XML Creation Start
                    /*out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                    out.print("<rows>");
                    //out.print("<page>" + page + "</page>");
                    out.print("<total>" + totalPages + "</total>");
                    out.print("<records>" + totalCount + "</records>");
                    for (int i = 0; i < tMaster.size(); i++) {
                        out.print("<row id='" + tMaster.get(i).getTemplateId() + "'>");
                        out.print("<cell>" + (offset + i + 1) + "</cell>");
                        out.print("<cell><![CDATA[" + tMaster.get(i).getTemplateName() + "]]></cell>");

                        if(tMaster.get(i).getProcType() == null){
                            out.print("<cell><![CDATA[-]]></cell>");
                        } else {
                            if("goods".equalsIgnoreCase(tMaster.get(i).getProcType())){
                                out.print("<cell><![CDATA[Goods]]></cell>");
                            }else if("works".equalsIgnoreCase(tMaster.get(i).getProcType())){
                                out.print("<cell><![CDATA[Works]]></cell>");
                            }else if("srvcmp".equalsIgnoreCase(tMaster.get(i).getProcType())){
                                out.print("<cell><![CDATA[Services - Consulting Firms]]></cell>");
                            }else if("srvindi".equalsIgnoreCase(tMaster.get(i).getProcType())){
                                out.print("<cell><![CDATA[Services - Individual Consultant]]></cell>");
                            }
                            else if("srvnoncon".equalsIgnoreCase(tMaster.get(i).getProcType())){
                                out.print("<cell><![CDATA[Services - Non consulting services]]></cell>");
                        }
                        }
                        out.print("<cell><![CDATA[" + tMaster.get(i).getNoOfSections() + "]]></cell>");
                        
                        String acceptLink ="";
                        if("P".equals(tStatus)){
                            if(logUserTypeId == 1){ // Accept is only available for egpAdmin
                                acceptLink = " | <a href=\"ViewTemplate.jsp?templateId=" + tMaster.get(i).getTemplateId() + "&action=acc\">Accept</a> | <a onclick='return confirm(\"Do you really want to delete the STD?\");' href=\"../StdGrid?action=delStd&templateId=" + tMaster.get(i).getTemplateId() + "\" > Delete </a>" ;
                            }else if(logUserTypeId == 8){
                                acceptLink = " | <a href=\"ViewTemplate.jsp?templateId=" + tMaster.get(i).getTemplateId() + "&action=acc\">Accept</a>";
                            }
                            out.print("<cell><![CDATA[" + "<a href=\"GuidenceDocUpload.jsp?templateId=" + tMaster.get(i).getTemplateId() + "\">Guidance Notes</a> | <a href=\"DefineSTDInDtl.jsp?templateId=" + tMaster.get(i).getTemplateId() + "\">Dashboard</a> "+ acceptLink + "]]></cell>");
                        }
                        if("A".equals(tStatus)){
                            if(logUserTypeId == 1){ // Accept is only available for egpAdmin
                                acceptLink = " | <a href=\"ViewTemplate.jsp?templateId=" + tMaster.get(i).getTemplateId() + "&action=can\">Cancel</a>";
                            }
                            out.print("<cell><![CDATA[" +"<a href=\"GuidenceDocUpload.jsp?templateId=" + tMaster.get(i).getTemplateId() + "\">Guidance Notes</a> | <a href=\"DefineSTDInDtl.jsp?templateId=" + tMaster.get(i).getTemplateId() + "\">Dashboard</a>" + acceptLink + "]]></cell>");
                        }
                        //if("P".equals(tStatus)){
                        //    out.print("<cell><![CDATA[" + "<a href=\"DefineSTDInDtl.jsp?templateId=" + tMaster.get(i).getTemplateId() + "\"> Dashboard </a> | " + "<a href=\"../StdGrid?action=delStd&templateId=" + tMaster.get(i).getTemplateId() + "\" onclick='return confirmDelete();'> Delete </a>" + "]]></cell>");
                        //}
                        if("C".equals(tStatus)){
                            out.print("<cell><![CDATA[" + "<a href=\"DefineSTDInDtl.jsp?templateId=" + tMaster.get(i).getTemplateId() + "\"> Dashboard </a>" + "]]></cell>");
                        }
                        out.print("</row>");
                    }
                    out.print("</rows>");*/
                    //XML Creation End
                }else if("acceptTemplate".equals(action)){
                    short templateId = 0;
                    if(request.getParameter("templateId") != null){
                        templateId = Short.parseShort(request.getParameter("templateId"));
                    }
                    //TemplateMasterImpl tempMstrLst = (TemplateMasterImpl) AppContext.getSpringBean("TemplateMasterService");
                    setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
                    //tempMstrLst.updateStatusOfTemplate("A", templateId);
                    tempMstrList.updateStatusOfTemplate("A", templateId);
                    
                    List<TblConfigClarification> list = new ArrayList<TblConfigClarification>();
                    list.add(new TblConfigClarification(0, (short)templateId, (short)0) );
                    configService.insertConfigClarification(list);
                    
                    try{
                        //code to generate Pdf reports
                        int sectionId=0;
                        String sectionName="";
                        String sectionType="";
                        int FormId = 0;
                        DefineSTDInDtlSrBean defSTDInDtl = new DefineSTDInDtlSrBean();
                        java.util.List<com.cptu.egp.eps.model.table.TblTemplateSections> tblTempSects = defSTDInDtl.getTemplateSection(templateId);
                        int ittId = -1, gccId = -1;
                        GenreatePdfCmd pdfCmd = new GenreatePdfCmd();
                        for(int i=0;i<tblTempSects.size();i++){
                            sectionId =  tblTempSects.get(i).getSectionId();
                            sectionName =  "Section"+(i+1)+"_"+tblTempSects.get(i).getSectionName();
                            sectionType = tblTempSects.get(i).getContentType();

                            if("ITT".equalsIgnoreCase(sectionType)){
                                ittId = sectionId;
                            }else if ("GCC".equalsIgnoreCase(sectionType)){
                                gccId = sectionId;
                            }

                            String folderName = request.getParameter("folderName");
                            String reqURL = request.getParameter("reqURL");
                            String reqQuery = "sectionId="+sectionId+"&templateId="+templateId;

                            if(sectionType.equalsIgnoreCase("TDS")){
                                reqQuery = "sectionId="+ittId+"&templateId="+templateId;
                            }else if (sectionType.equalsIgnoreCase("PCC")){
                                reqQuery = "sectionId="+gccId+"&templateId="+templateId;
                            }

                            //http://localhost:8080/eGPWeb/admin/ViewTemplate.jsp?templateId=12&action=acc
                            //http://localhost:8080/eGPWeb/admin/ITTView.jsp?sectionId=9&templateId=2
                            if("ITT".equalsIgnoreCase(sectionType) || "GCC".equalsIgnoreCase(sectionType)){
                                reqURL =  reqURL.replace("ViewTemplate","ITTView") ;
                                String genId=templateId+"_"+sectionId;
                                pdfCmd.genrateSTDpdf(reqURL, reqQuery, folderName, genId, sectionName);
                            }
                            //http://localhost:8080/eGPWeb/admin/TDSView.jsp?sectionId=166&templateId=15
                            if("TDS".equalsIgnoreCase(sectionType) || "PCC".equalsIgnoreCase(sectionType)){
                                reqURL =  reqURL.replace("ViewTemplate","TDSView") ;
                                String genId=templateId+"_"+sectionId;
                                pdfCmd.genrateSTDpdf(reqURL, reqQuery, folderName, genId, sectionName);
                            }

                         /*   if("Form".equalsIgnoreCase(sectionType)){
                                String strForAllFormPDF = "";
                                //http://localhost:8080/eGPWeb/admin/ViewCompleteForm.jsp?templateId=15&sectionId=168&formId=8
                                java.util.List<com.cptu.egp.eps.model.table.TblTemplateSectionForm> tblTemplateForms = defSTDInDtl.getTemplateSectionForm(templateId, sectionId);
                                String genId=templateId+"_"+sectionId;
                                reqURL =  reqURL.replace("ViewTemplate","TestForm") ;
                                if(tblTemplateForms != null){
                                    if(!tblTemplateForms.isEmpty()){
                                        for(int j=0;j<tblTemplateForms.size();j++){
                                            FormId = tblTemplateForms.get(j).getFormId();
                                            reqQuery = "templateId="+templateId+"&sectionId="+sectionId+"&formId="+FormId;
                                                strForAllFormPDF+= "\"" + reqURL + "?" + reqQuery + "&isPDF=true" + "\" ";
                                        }
                                        //pdfCmd.genrateSTDForm(strForAllFormPDF, "", folderName, genId, sectionName);
                                        pdfCmd.genrateCmdForm(strForAllFormPDF, "", folderName, genId);
                                    }
                                    tblTemplateForms = null;
                                }
                            }*/
                        }//end for loop
                        pdfCmd = null;             
                    }catch(Exception e){
                        LOGGER.error("processRequest : User ID " + logUserId + " " + e.toString());
                    }
                    //  response.sendRedirect("admin/ViewTemplate.jsp?templateId=5&action=accd");
                    response.sendRedirect("admin/ViewTemplate.jsp?templateId="+templateId+"&action=accd");

                }else if("cancelTemplate".equals(action)){
                    short templateId = 0;
                    if(request.getParameter("templateId") != null){
                        templateId = Short.parseShort(request.getParameter("templateId"));
                    }
                    setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
                    tempMstrList.updateStatusOfTemplate("C", templateId);
                    
                    List<TblConfigClarification> getConfigClarification = configService.getTblConfigClarification("templateId", Operation_enum.EQ, (short)templateId );
                    configService.deleteSingleConfigClarification(String.valueOf(getConfigClarification.get(0).getConfigClarificationId()));

                    //TemplateMasterImpl tempMstrLst = (TemplateMasterImpl) AppContext.getSpringBean("TemplateMasterService");
                    //tempMstrLst.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
                    //tempMstrLst.updateStatusOfTemplate("C", templateId);
                    response.sendRedirect("admin/ViewTemplate.jsp?templateId=5&action=cand");
                }else if("delStd".equals(action)){
                    short templateId = 0;
                    if(request.getParameter("templateId") != null){
                        templateId = Short.parseShort(request.getParameter("templateId"));
                    }
                    setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
                    tempMstrList.deleteTemplate(templateId);
                    //TemplateMasterImpl tempMstrLst = (TemplateMasterImpl) AppContext.getSpringBean("TemplateMasterService");
                    //tempMstrLst.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
                    //tempMstrLst.deleteTemplate(templateId);
                    response.sendRedirect("admin/ListSTD.jsp?msg=delSuc");
                }
            }
        } catch(Exception ex){
            LOGGER.error("processRequest : User ID " + logUserId + " " + ex.toString());
        }finally {
            setAuditTrail(null);
            out.close();
        }
        LOGGER.debug("processRequest : User ID " + logUserId + strEnds);
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
