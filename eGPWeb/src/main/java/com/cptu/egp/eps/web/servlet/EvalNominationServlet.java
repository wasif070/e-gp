/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cptu.egp.eps.web.utility.AppContext;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class EvalNominationServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String funName = request.getParameter("funName");
            String postXml = "";
           CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
           
             if (funName != null) {
                
                if (funName.equalsIgnoreCase("dropMember")) {
                    
                  String tenderId = request.getParameter("tenderId");
                  String cuserId = request.getParameter("cuserId");
                  String committeeId = request.getParameter("committeeId");
                  String nomineeUserId = request.getParameter("nomineeUserId");
                  
                  SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                  postXml = "nomineeStatus='dropped'";
                  String whereCondi= "tenderId = "+tenderId+ " and nomineeUserId="+ nomineeUserId;
                  CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_EvalNomination", postXml, whereCondi).get(0);
                   TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                   
                  String action = "dropped";
                   
                  for (SPTenderCommonData data : tenderCommonService.returndata("getCommitteeMemInfo", committeeId, nomineeUserId)) {
                     postXml = "<root><tbl_EvalDroppedMembers committeeId=\"" + data.getFieldName1()
                           + "\" userId=\"" + data.getFieldName2() + "\" createdDate=\"" + data.getFieldName3()+ "\" appStatus=\"" + data.getFieldName4()
                           + "\" appDate=\"" + data.getFieldName5()+ "\" remarks=\"" + data.getFieldName6()+ "\" eSignature=\"" + data.getFieldName7()
                           + "\" digitalSignature=\"" + data.getFieldName8()+ "\" memberRole=\"" + data.getFieldName9()+ "\" memberFrom=\"" + data.getFieldName10()
                           + "\" tenderId=\"" + tenderId+ "\" action=\"" + action
                           + "\" actionBy=\"" + cuserId + "\" actionDt=\"" + format.format(new Date()) + "\" /></root>";
                   }
                  commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_EvalDroppedMembers", postXml, "").get(0);

                  response.sendRedirect("officer/EvalTeamStatus.jsp?tenderid="+tenderId);
                  //response.sendRedirectFilter("EvalTeamStatus.jsp?tenderid="+tenderId);
                }
             }
        }catch(Exception e){
            e.printStackTrace();
        }
        finally {
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
