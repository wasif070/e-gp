/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.webservices;

import com.cptu.egp.eps.dao.storedprocedure.RHDIntegrationPEDetails;
import com.cptu.egp.eps.service.serviceinterface.RHDIntegrationPEDetailsService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.WebServiceUtil;
import com.cptu.egp.eps.web.xmlpojos.WSResponseObject;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.apache.log4j.Logger;

/**
 *
 * @author Sudhir Chavhan
 */
@WebService()
public class RHDPEIntegration {

    private static final Logger LOGGER = Logger.getLogger(RHDPEIntegration.class);
    private String logUserId = "0";
    private static final int WEB_SERVICE_ID = 48;
    private static final String SEARCH_SERVICE_NAME = "RHDIntegrationPEDetailsService";

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
  /**
     * This method gives the List of String Objects of <b>RHDPEIntegration</b> and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br>

     * 1.PE ID <br>
     * 2.PE office Name <br>
     * 3.PE office Address <br>
     * 4.Dept Type <br>
     * 5.Ministry Name <br>
     * 6.Division Name <br>
     * 7.RHDOrganization Name <br>
     * 8.District<br>
     *
     * @param String username
     * @param String password
     * @return WSResponseObject With list data of List<String>
     */
    @WebMethod(operationName = "getSharedPEDetailForRHDIntegration")
    public WSResponseObject getSharedPEDetailForRHDIntegration(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getSharedPEDetailForRHDIntegration : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = "";
        //insert record in log table
        WebServiceUtil.insertLogRecord(WEB_SERVICE_ID, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WEB_SERVICE_ID,
                SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> requiredRHDIntegrationDetailsList = new ArrayList<String>();
            try {
                // get the list of PE Office Detail
                requiredRHDIntegrationDetailsList = getPERequiredList();
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.error("getSharedPEDetailForRHDIntegration : " + logUserId + e);
            }
            if (requiredRHDIntegrationDetailsList != null) {
                if (requiredRHDIntegrationDetailsList.size() <= 0) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(requiredRHDIntegrationDetailsList);
                }
            }//end IF condition with NULL checking
        }//end IF condition with "isListDataRequired" checking
        LOGGER.debug("getSharedPEDetailForRHDIntegration : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    private List<String> getPERequiredList() throws Exception {
        LOGGER.debug("getPERequiredList : " + logUserId + WSResponseObject.LOGGER_START);
        List<String> requiredRHDIntegrationDetailsList = new ArrayList<String>();
        List<RHDIntegrationPEDetails> RHDIntegrationDetailsList = new ArrayList<RHDIntegrationPEDetails>();
        RHDIntegrationPEDetailsService RHDIntegrationDetailsService =
                (RHDIntegrationPEDetailsService) AppContext.getSpringBean(SEARCH_SERVICE_NAME);
        RHDIntegrationDetailsList = RHDIntegrationDetailsService.getSharedPEDetailForRHD();
        if (RHDIntegrationDetailsList != null) {
            for (RHDIntegrationPEDetails rhdIntegrationDetails : RHDIntegrationDetailsList) {
                StringBuilder sbCSV = new StringBuilder();
                sbCSV.append(rhdIntegrationDetails.getPEId());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(rhdIntegrationDetails.getPEOfficeName());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(rhdIntegrationDetails.getOfficeAddr());
               // sbCSV.append(WSResponseObject.APPEND_CHAR);
               // sbCSV.append(rhdIntegrationDetails.getDeptType());
               // sbCSV.append(WSResponseObject.APPEND_CHAR);
               // sbCSV.append(rhdIntegrationDetails.getMinistryName());
               // sbCSV.append(WSResponseObject.APPEND_CHAR);
               // sbCSV.append(rhdIntegrationDetails.getDivisionName());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(rhdIntegrationDetails.getRHDOrgName());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(rhdIntegrationDetails.getDistrictName());
                requiredRHDIntegrationDetailsList.add(sbCSV.toString());
            }//end FOR loop
        }//end IF condition
        LOGGER.debug("getPERequiredList : " + logUserId + WSResponseObject.LOGGER_END);
        return requiredRHDIntegrationDetailsList;
    }
}
