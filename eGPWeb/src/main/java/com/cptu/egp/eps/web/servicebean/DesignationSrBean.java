/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblGradeMaster;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import com.cptu.egp.eps.service.serviceinterface.DesignationMasterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.SelectItem;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * <b>Designation Service Bean</b>
 * @author Administrator
 */
public class DesignationSrBean {
    //List of Departments

    public List<SelectItem> departmentList = new ArrayList<SelectItem>();
    private int userId;
    DesignationMasterService designationMasterService = (DesignationMasterService) AppContext.getSpringBean("DesignationMasterService");
    final Logger logger = Logger.getLogger(DesignationSrBean.class);
    String logUserId = "0";

    public void setLogUserId(String logUserId) {
        designationMasterService.setUserId(logUserId);
        this.logUserId = logUserId;

    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public List<SelectItem> getDepartmentList() {
        logger.debug("getDepartmentList : " + logUserId + " Starts");
        if (departmentList.isEmpty()) {
            List<TblDepartmentMaster> departmentMasters = designationMasterService.getMinistry();
            for (TblDepartmentMaster departmentMaster : departmentMasters) {
                departmentList.add(new SelectItem(departmentMaster.getDepartmentId(), departmentMaster.getDepartmentName()));
            }

        }
        logger.debug("getDepartmentList : " + logUserId + " Ends");
        return departmentList;
    }

    public List<SelectItem> getMinistryforAdmin() {
        logger.debug("getMinistryforAdmin : " + logUserId + " Starts");
        if (departmentList.isEmpty()) {

            /*List<TblDepartmentMaster> departmentMasters = designationMasterService.getMinistryforAdmin(this.userId);
            for (TblDepartmentMaster departmentMaster : departmentMasters) {
                departmentList.add(new SelectItem(departmentMaster.getDepartmentId(), departmentMaster.getDepartmentName()));
            }*/
            List<TblOfficeMaster> departmentMasters = designationMasterService.getOfficeforAdmin(this.userId);
            for (TblOfficeMaster departmentMaster : departmentMasters) {
                departmentList.add(new SelectItem(departmentMaster.getDepartmentId(), departmentMaster.getOfficeName()));
            }

        }
        logger.debug("getMinistryforAdmin : " + logUserId + " Ends");
        return departmentList;
    }
    
    //Get Office
    public List<SelectItem> getOffice() {
        logger.debug("getOfficeName : " + logUserId + " Starts");
        if (departmentList.isEmpty()) {

            List<TblDepartmentMaster> departmentMasters = designationMasterService.getMinistryforAdmin(this.userId);
            for (TblDepartmentMaster departmentMaster : departmentMasters) {
                departmentList.add(new SelectItem(departmentMaster.getDepartmentId(), departmentMaster.getDepartmentName()));
            }

        }
        logger.debug("getMinistryforAdmin : " + logUserId + " Ends");
        return departmentList;
    }
    //List of Grades
    public List<SelectItem> gradeList = new ArrayList<SelectItem>();

    public List<SelectItem> getGradeList() {
        logger.debug("getGradeList : " + logUserId + " Starts");
        if (gradeList.isEmpty()) {

            List<TblGradeMaster> tblGradeMasters = designationMasterService.getGrades();
            for (TblGradeMaster gradeMaster : tblGradeMasters) {
                gradeList.add(new SelectItem(gradeMaster.getGradeId(), (gradeMaster.getGrade() + " - " + gradeMaster.getGradeLevel())));
        }
        }
        logger.debug("getGradeList : " + logUserId + " Ends");
        return gradeList;
    }

    public void setGradeList(List<SelectItem> gradeList) {
        this.gradeList = gradeList;
    }

    public void setDepartmentList(List<SelectItem> departmentList) {
        this.departmentList = departmentList;
    }
}
