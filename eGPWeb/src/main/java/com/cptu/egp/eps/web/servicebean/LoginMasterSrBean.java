package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblCountryMaster;
import com.cptu.egp.eps.model.table.TblHintQuestionMaster;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblNationalityMaster;
import com.cptu.egp.eps.model.table.TblUserTypeMaster;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.UserLoginService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.databean.LoginMasterDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.HashUtil;
import com.cptu.egp.eps.web.utility.MailContentUtility;

import com.cptu.egp.eps.web.utility.SelectItem;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
/**
 * Author TaherT
 * Registeration of New User
 */
public class LoginMasterSrBean
{

    final static Logger logger = Logger.getLogger(LoginMasterSrBean.class);

    private CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
    private UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    private UserLoginService userLoginService = (UserLoginService) AppContext.getSpringBean("UserLoginService");
    List<SelectItem> nationalityList = new ArrayList<SelectItem>();
    private String userId="0";
    private AuditTrail auditTrail;
    private static final String START = " Starts";
    private static final String END = " Ends";

    /**
     * 
     */
    public LoginMasterSrBean() {
    }

    /**
     * Constructor
     * @param userId
     */
    public LoginMasterSrBean(String userId,AuditTrail auditTrail) {
        this.userId = userId;
        userRegisterService.setUserId(userId);
        userRegisterService.setAuditTrail(auditTrail);
        commonService.setUserId(userId);
        userLoginService.setUserId(userId);
    }

    public LoginMasterSrBean(String userId) {
        this.userId = userId;
        userRegisterService.setUserId(userId);
        commonService.setUserId(userId);
        userLoginService.setUserId(userId);
    }
    
/**
 * Method : Nationality List
 * @return List of SelectItem
 */
    public List<SelectItem> getNationalityList()
    {
        logger.debug("getNationalityList : "+userId+START);
        logger.debug("getNationalityList : "+userId+" :  isEmpty "+nationalityList.isEmpty());
        if (nationalityList.isEmpty()) {
            for (TblNationalityMaster nationalityMaster : commonService.nationalityMasterList()) {
                nationalityList.add(new SelectItem(nationalityMaster.getNationalityName(), nationalityMaster.getNationalityName()));
            }
        }
        logger.debug("getNationalityList : "+userId+END);
        return nationalityList;
        }

    /**
     * Setter
     * @param nationalityList
     */
    public void setNationalityList(List<SelectItem> nationalityList)
    {
        this.nationalityList = nationalityList;
    }
    List<SelectItem> hintQueList = new ArrayList<SelectItem>();

    /**
    * Method : HintQuestion List
     * @return List of SelectItem
     */
    public List<SelectItem> getHintQueList()
    {
        logger.debug("getHintQueList : "+userId+START);
        logger.debug("getHintQueList : "+userId+" isEmpty : "+hintQueList.isEmpty());
        if (hintQueList.isEmpty()) {
            int hintQuesCount = 0;
            for (TblHintQuestionMaster hintQuestionMaster : commonService.hintQuestionMasterList()) {
                hintQueList.add(new SelectItem(hintQuestionMaster.getHintQuestion(), hintQuestionMaster.getHintQuestion()));
                hintQuesCount++;
                if(hintQuesCount >= 5 ){
                    break;
                }                
            }
        }
        logger.debug("getHintQueList : "+userId+END);
        return hintQueList;
    }

    /**
     * setter
     * @param hintQueList
     */
    public void setHintQueList(List<SelectItem> hintQueList)
    {
        this.hintQueList = hintQueList;
    }
    List<SelectItem> countryList = new ArrayList<SelectItem>();

    /**
    * Method : Country List
     * @return List of SelectItem
     * @throws Exception
     */
    public List<SelectItem> getCountryList() throws Exception
    {
        logger.debug("getCountryList : "+userId+START);
        if (countryList.isEmpty()) {
            try {
                for (TblCountryMaster countryMaster : commonService.countryMasterList()) {
                    countryList.add(new SelectItem(countryMaster.getCountryName(), countryMaster.getCountryName()));
                }
            }
            catch (Exception ex) {
                logger.error("getCountryList : "+userId+" "+ex);
            }
        }
        logger.debug("getCountryList : "+userId+END);
        return countryList;
    }

    /**
     * setter
     * @param countryList
     */
    public void setCountryList(List<SelectItem> countryList)
    {
        this.countryList = countryList;
    }

    /**
    * Method : New UserRegistration Login Details.
     * @param loginMasterDtBean LoginMasterDtBean object
     * @param url
     * @return boolean 
     */
    public boolean userRegister(LoginMasterDtBean loginMasterDtBean,String url)
    {
        logger.debug("userRegister : "+userId+START);
        boolean flag=false;
        TblLoginMaster loginMaster = _toTblLoginMaster(loginMasterDtBean);
        if (loginMasterDtBean.getHintQuestion().equals("other")) {
            loginMaster.setHintQuestion(loginMasterDtBean.getHintQuestionOwn());
        }
        loginMaster.setTblUserTypeMaster(new TblUserTypeMaster((byte) 2));
        if (!(loginMaster.getRegistrationType().startsWith("c") || loginMaster.getRegistrationType().startsWith("m") || loginMaster.getRegistrationType().startsWith("g"))) {
            loginMaster.setNextScreen("IndividualConsultant");
            loginMaster.setIsJvca("no");
        }
        else {
            loginMaster.setNextScreen("CompanyDetails");
        }
        loginMaster.setIsEmailVerified("no");
        loginMaster.setFailedAttempt((byte) 0);
        loginMaster.setFirstLogin("no");
        loginMaster.setIsPasswordReset("no");
        loginMaster.setResetPasswordCode("");
        loginMaster.setValidUpTo(new Date());
        loginMaster.setStatus("incomplete");
        loginMaster.setRegisteredDate(new Date());
        loginMaster.setTransdate(new Date());
        SendMessageUtil smu = new SendMessageUtil();
        String mailHash = smu.getRandomName().toString();
        flag = userRegisterService.registerUserOnPortal(loginMaster,mailHash);
        mailHash = HashUtil.getHash(mailHash, "SHA-1");
        //String mailHash = HashUtil.getHash(loginMasterDtBean.getEmailId(), "SHA-1");
        logger.debug("SHA-1 Mail hash: " + mailHash);
        //String mailCode = HashUtil.getHash(mailHash, "MD5");
        //logger.debug("MD-5 Mail hash: " + mailCode);
        boolean mailFlag=sendMail(loginMaster, mailHash,url);
        if(mailFlag){
            logger.debug("Mail Sent");
    }
        else{
            logger.debug("Mail Failed");
        }
        logger.debug("userRegister : "+userId+END);
        return flag;
    }

    /**
     *Converting LoginMasterDtBean to TblLoginMaster
     * @param loginMasterDtBean
     * @return TblLoginMaster object
     */
    public TblLoginMaster _toTblLoginMaster(LoginMasterDtBean loginMasterDtBean)
    {
        logger.debug("_toTblLoginMaster : "+userId+START);
        TblLoginMaster loginMaster = new TblLoginMaster();
        BeanUtils.copyProperties(loginMasterDtBean, loginMaster);
        //loginMaster.setPassword(SHA1HashEncryption.encodeStringSHA1(loginMasterDtBean.getPassword()));
        logger.debug("_toTblLoginMaster : "+userId+END);
        return loginMaster;
    }

    private boolean sendMail(TblLoginMaster loginMaster, String hashCode,String url)
    {
        logger.debug("sendMail : "+userId+START);
        boolean mailSent = false;
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            String[] mailTo = {loginMaster.getEmailId()};
            MailContentUtility mailContentUtility = new MailContentUtility();

            List list = mailContentUtility.getLoginMailContent(loginMaster, hashCode,url);
            String mailSub = list.get(0).toString();
            String mailText = list.get(1).toString();
            sendMessageUtil.setEmailTo(mailTo);
            sendMessageUtil.setEmailFrom(XMLReader.getMessage("regEmailId"));
            sendMessageUtil.setEmailSub(mailSub);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
//            sendMessageUtil.setSmsNo("+919978112884");
//            sendMessageUtil.setSmsBody("Msg for e-GP");
//            sendMessageUtil.sendSMS();
            mailSent = true;
        }
        catch (Exception ex) {
            logger.error("sendMail : "+userId+" "+ex);
        }
        logger.debug("sendMail : "+userId+END);
        return mailSent;
    }

    /**
     * Get list of expired user
     * @return
     */
    public List<TblLoginMaster> getExpiredUsers(){

        logger.debug("getExpiredUsers : "+userId+START);
        List<TblLoginMaster> listLoginMaster = null;
        Date curDate = new Date();
        Date newDate = new Date(curDate.getYear(), curDate.getMonth()+1, curDate.getDate());

        /*SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
        String strnewDate = sd.format(newDate);
        Date expiry = new Date();*/

        try{            
            //expiry = sd.parse(strnewDate);
            listLoginMaster =userLoginService.getExpiredUsers(newDate);
        }catch(Exception ex){
            logger.error("getExpiredUsers : "+userId+" "+ex);
        }
        logger.debug("getExpiredUsers : "+userId+END);
        return listLoginMaster;
    }

    /**
     * Register the JVCA user
     * @param loginMasterDtBean
     * @param jvcId
     * @return
     */
    public int jvcaRegister(LoginMasterDtBean loginMasterDtBean,String jvcId)
    {
        logger.debug("jvcaRegister : "+userId+START);
        TblLoginMaster loginMaster = _toTblLoginMaster(loginMasterDtBean);
        if (loginMasterDtBean.getHintQuestion().equals("other")) {
            loginMaster.setHintQuestion(loginMasterDtBean.getHintQuestionOwn());
        }
        loginMaster.setTblUserTypeMaster(new TblUserTypeMaster((byte) 2));        
        loginMaster.setNextScreen("JVCompanyDetails");
        loginMaster.setIsEmailVerified("yes");
        loginMaster.setFailedAttempt((byte) 0);
        loginMaster.setFirstLogin("no");
        loginMaster.setIsPasswordReset("no");
        loginMaster.setResetPasswordCode("");
        loginMaster.setValidUpTo(new Date());
        loginMaster.setStatus("pending");
        loginMaster.setRegisteredDate(new Date());
        logger.debug("jvcaRegister : "+userId+END);
        return userRegisterService.jvcaRegister(loginMaster, jvcId);
    }

    /**
     * Get userRegistration type
     * @param userId
     * @return
     */
    public String getUserRegType(int userId){
       logger.debug("getUserRegType : "+userId+START);
       String strRegType = null;
       try{
           strRegType = userLoginService.getRegType(userId);
       }catch(Exception ex){
           logger.error("getUserRegType : "+userId+" "+ex);
       }
       logger.debug("getUserRegType : "+userId+END);
       return strRegType;
    }
    /**
     * This method is for when registering jvca it will show name of jvca in readonly mode
     * @param jvcId 
     * @return Name of JVCA
     */
    public String  getJVCACompanyName(int jvcId){
        logger.debug("getJVCACompanyName : "+userId+START);
        String jvcaName = "";
        try {
            jvcaName = userLoginService.jvcaName(jvcId);
        } catch (Exception e) {
            logger.error("getJVCACompanyName : "+userId+" : "+e);
        }
        logger.debug("getJVCACompanyName : "+userId+END);
        return jvcaName;
    }   
}