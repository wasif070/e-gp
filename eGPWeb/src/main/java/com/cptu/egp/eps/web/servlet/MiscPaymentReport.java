/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.web.servicebean.GenreatePdfCmd;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.GeneratePdfConstant;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class MiscPaymentReport extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    MakeAuditTrailService auditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String styleClass = "";

        CommonSearchDataMoreService searchService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
          auditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), Integer.parseInt(request.getSession().getAttribute("userId").toString()), "userId", EgpModule.Report.getName(), "Generate Miscellaneous Payment Report", "");
        List<SPCommonSearchDataMore> spSearchData = null;
        //boolean isPDF = Boolean.parseBoolean(request.getParameter("isPDF"));
            
        

        try {
            String action = request.getParameter("action");
            if (action.equalsIgnoreCase("Search")) {

                String paymentDate = "";
                String emailId = "";
                String payeeName = "";
                String payeeNameSearch = "";
                String[] dtArr = null;
                String sortEmail = "";
                String Amount = "";
                String amountSearch = "";
                String sortEmailId = "";


                if (request.getParameter("emailId") != null && !"".equalsIgnoreCase(request.getParameter("emailId"))) {
                    emailId = request.getParameter("emailId");
                }
                if (request.getParameter("sortemailId") != null && !"".equalsIgnoreCase(request.getParameter("sortemailId"))) {
                    sortEmailId = request.getParameter("sortemailId");
                }
                if (request.getParameter("paymentDate") != null && !"".equalsIgnoreCase(request.getParameter("paymentDate"))) {
                    paymentDate = request.getParameter("paymentDate");
                    dtArr = paymentDate.split("/");
                    paymentDate = dtArr[2] + "-" + dtArr[1] + "-" + dtArr[0];
                   // paymentDate = request.getParameter("paymentDate");
                }
                if (request.getParameter("payeeName") != null && !"".equalsIgnoreCase(request.getParameter("payeeName"))) {
                    payeeName = request.getParameter("payeeName");
                }
                if (request.getParameter("payeeNameSearch") != null && !"".equalsIgnoreCase(request.getParameter("payeeNameSearch"))) {
                    payeeNameSearch = request.getParameter("payeeNameSearch");
                }
                if (request.getParameter("amount") != null && !"".equalsIgnoreCase(request.getParameter("amount"))) {
                    Amount = request.getParameter("amount");
                }
                if (request.getParameter("amountSearch") != null && !"".equalsIgnoreCase(request.getParameter("amountSearch"))) {
                    amountSearch = request.getParameter("amountSearch");
                }


                String strPageNo = request.getParameter("pageNo");
                int pageNo = Integer.parseInt(strPageNo);

                String strOffset = request.getParameter("size");
                int recordOffset = Integer.parseInt(strOffset);

                int srNo = 0;
                srNo = (pageNo - 1) * (recordOffset);
                spSearchData = searchService.getCommonSearchData("getMiscPaymentReport", strPageNo, strOffset, emailId, sortEmailId, paymentDate, payeeName, payeeNameSearch, Amount, amountSearch);

                if (spSearchData != null && !spSearchData.isEmpty()) {
                    String link = "";

                    for (int i = srNo; i < srNo+recordOffset && i<spSearchData.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }
                        out.print("<tr class='" + styleClass + "'>");
                        out.print("<td class=\"t-align-center\">" + (i + 1) + "</td>");
                        out.print("<td class=\"t-align-center\">" + spSearchData.get(i).getFieldName6() + "</td>");
                        out.print("<td class=\"t-align-center\">" + spSearchData.get(i).getFieldName7() + "</td>");
                        out.print("<td class=\"t-align-center\">" + spSearchData.get(i).getFieldName4() + "</td>");
                        out.print("<td class=\"t-align-left\">" + spSearchData.get(i).getFieldName5() + "</td>");
                        out.print("<td class=\"t-align-center\">" + spSearchData.get(i).getFieldName3() + "</td>");
                        out.print("<td class=\"t-align-center\">" + spSearchData.get(i).getFieldName8() + "</td>");
                        out.print("<td class=\"t-align-center\">" + spSearchData.get(i).getFieldName9() + "</td>");
                        out.print("<td class=\"t-align-center\"><a href='ViewMiscPaymentReport.jsp?payId=" + spSearchData.get(i).getFieldName1() + "&uId=" + spSearchData.get(i).getFieldName2() + "'>View Details</a></td>");
                        out.print("</tr>");
                    }
                    int totalPages = 1;

                    if (spSearchData.size() > 0) {
                        totalPages = (int) Math.ceil(spSearchData.size() / recordOffset);
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                } else {
                    out.print("<tr>");
                    out.print("<input type=\"hidden\" id=\"1\" value=\"1\">");
                    out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"11\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }

                //if(!isPDF){
                    try {

                    String userId = String.valueOf(request.getSession().getAttribute("userId"));
                    GenreatePdfCmd obj = new GenreatePdfCmd();
                    GeneratePdfConstant constant = new GeneratePdfConstant();
                    String cdate = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date(new Date().getTime()));
                    String folderName = constant.MISCPMTRPT;
                    String genId = cdate + "_" + userId;
                    String reqURL = request.getRequestURL().toString();
                    reqURL = reqURL.replace("MiscPaymentReport", "admin/MiscPaymentRptPDF.jsp");
                    String reqQuery = "param1=getMiscPaymentReport&strPageNo="+strPageNo+"&strOffset="+strOffset+"&emailId="+emailId+"&sortEmailId="+sortEmailId+"&paymentDate="+paymentDate+"&payeeName="+payeeName+"&payeeNameSearch="+payeeNameSearch+"&Amount="+Amount+"&amountSearch="+amountSearch;
                    obj.genrateCmd(reqURL, reqQuery, folderName, genId);
                    } catch (Exception e) {
                    System.out.println("exception-" + e);
                    }

                }
            //}

        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
