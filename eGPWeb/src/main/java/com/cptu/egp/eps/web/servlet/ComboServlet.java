/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonAppData;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import com.cptu.egp.eps.model.table.TblScBankDevPartnerMaster;
import com.cptu.egp.eps.model.table.TblStateMaster;
import com.cptu.egp.eps.model.table.TblTempCompanyMaster;
import com.cptu.egp.eps.model.table.TblTempTendererMaster;
import com.cptu.egp.eps.service.serviceinterface.APPService;
import com.cptu.egp.eps.service.serviceinterface.ContractAwardOfflineService;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.DepartmentCreationService;
import com.cptu.egp.eps.service.serviceinterface.PEOfficeCreationService;
import com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService;
import com.cptu.egp.eps.service.serviceinterface.TenderDashboardOfflineService;
import com.cptu.egp.eps.web.servicebean.TendererMasterSrBean;
import com.cptu.egp.eps.web.servicebean.PEOfficeCreationSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.SelectItem;
import com.cptu.egp.eps.web.utility.XMLReader;
import java.lang.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.cptu.egp.eps.web.servicebean.TendererMasterSrBean;

/**
 *
 * @author Administrator
 */
public class ComboServlet extends HttpServlet {

    String userId = "";
    static final Logger LOGGER = Logger.getLogger(ComboServlet.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String str = "";
        String SubDistrict = "";
        String objectId = request.getParameter("objectId");
        System.out.print(objectId);
        if (request.getSession().getAttribute("userId") != null) {
            userId = request.getSession().getAttribute("userId").toString();
        }
        String funName = request.getParameter("funName");
        try {
            if (funName != null) {
                LOGGER.debug(funName + " : " + userId + " Starts");
                if ("stateComboNOA".equals(funName)) {
                    short countryId = Short.parseShort(objectId);
                    str = getStateNOA(countryId);
                } else if ("stateCombo".equals(funName)) {
                    short countryId = Short.parseShort(objectId);
                    str = getState(countryId);
                }//else if
                else if ("ministryC".equals(funName)) {
                    str = getDeptyType(objectId);
                } else if ("ministryCombo".equals(funName)) {
                    short deptId = Short.parseShort(objectId);
                    str = getMinistry(deptId);
                } else if ("divisionCombo".equals(funName)) {
                    short deptId = Short.parseShort(objectId);
                    str = getDivision(deptId);
                } else if ("stateComboValue".equals(funName)) {
                    short countryId = Short.parseShort(objectId);
                    str = getStateValue(countryId);
                } else if ("subDistrictComboValue".equals(funName)) {
                    String state = objectId;
                    str = getSubDistrictValue(state, userId);
                }else if ("getSubDistrictValueOffSubDistrict".equals(funName)) {
                    String state = objectId;
                    str = getSubDistrictValueOffSubDistrict(state, userId);
                }else if ("getSubDistrictValueCorpOffSubDistrict".equals(funName)) {
                    String state = objectId;
                    str = getSubDistrictValueCorpOffSubDistrict(state, userId);
                }else if ("GetSubdistrictList".equals(funName)) {
                    String state = objectId;
                    SubDistrict = request.getParameter("SubDistrict");
                    str = GetSubdistrictList(state, SubDistrict);
                }else if ("subDistrictComboValue2".equals(funName)) {
                    String state = objectId;
                    str = getSubDistrictValue2(state, userId);
                } else if ("subDistrictComboValue3".equals(funName)) {
                    String state = objectId;
                    str = getSubDistrictValue3(state, userId);
                } else if ("subDistrictComboValue4".equals(funName)) {
                    String state = objectId;
                    str = getSubDistrictValue4(state, userId);
                } // else if ("organizationCombo".equals(funName)) {}
                else if ("AreaCodeSelection".equals(funName)) {
                    String state = objectId;
                    str = getAreaCode(state, userId);
                } else if ("organizationCombo".equals(funName)) {
                    short deptId = Short.parseShort(objectId);
                    str = getOrganization(deptId);
                } else if ("appCombo".equals(funName)) {
                    String financialYear = objectId;
                    str = getAPPDetails(financialYear, userId);
                } else if ("officeCombo".equals(funName)) {
                    String department = request.getParameter("departmentId");
                    Integer deptId = Integer.parseInt(department);
                    str = getDepartmentOffice(deptId);

                } else if ("officeList".equals(funName)) {
                    String department = request.getParameter("departmentId");
                    Integer deptId = Integer.parseInt(department);
                    str = getDepartmentOfficeListAsTable(deptId);

                } else if ("tenderOfficeCombo".equals(funName)) {
                    String department = request.getParameter("departmentId");
                    Integer deptId = Integer.parseInt(department);
                    str = getDepartment(deptId);

                } else if ("branchCombo".equals(funName)) {
                    String bank = request.getParameter("objectId");
                    String str_type = request.getParameter("type");
                    try {
                        int bankId = Integer.parseInt(bank);
                        str = getBranchOffice(bankId, str_type);
                    } catch (NumberFormatException ne) {
                        // In case of bank is any of the online bank
                        str = bank;
                    } catch (Exception ex) {
                        LOGGER.error(funName + " : " + userId + ex);
                    }
                } else if ("paymentType".equals(funName)) {
                    String paymentType = request.getParameter("objectId");
                    try {
                        str = getBankName(paymentType);
                    } catch (Exception ex) {
                        LOGGER.error(funName + " : " + userId + ex);
                    }
                } else if ("branchComboEdit".equals(funName)) {
                    String bank = request.getParameter("objectId");
                    String branch = "0";
                    if (request.getParameter("branchId") != null) {
                        branch = request.getParameter("branchId");
                    }
                    int bankId = Integer.parseInt(bank);
                    int branchId = Integer.parseInt(branch);
                    try {
                        str = getBranchOffice(bankId, branchId);
                    } catch (Exception ex) {
                        LOGGER.error(funName + " : " + userId + ex);
                    }
                } else if ("peofficeCombo".equals(funName)) {

                    String departmentId = request.getParameter("departmentId");
                    String districtId = request.getParameter("districtId");
                    try {
                        str = getpeOffice(Integer.parseInt(departmentId), Integer.parseInt(districtId));
                    } catch (Exception ex) {
                        LOGGER.error(funName + " : " + userId + ex);
                    }

                } else if ("officeComboforpromis".equals(funName)) {
                    String department = request.getParameter("departmentId");
                    Integer deptId = Integer.parseInt(department);
                    str = getDepartmentOfficeForPromis(deptId);

                } else if ("offlineAwardedOrgCombo".equals(funName)) {
                    String department = request.getParameter("ministryName");
                    str = getOfflineAwardedOrganization(objectId);

                } else if ("offlineTenderOrgCombo".equals(funName)) {
                    String department = request.getParameter("ministryName");
                    str = getOfflineTenderOrganization(objectId);

                } else if ("offlineAwardedOrgComboHome".equals(funName)) {
                    String department = request.getParameter("ministryName");
                    str = getOfflineAwardedOrganizationHome(objectId);

                } else if ("officeComboforproject".equals(funName)) {
                    String projectId = request.getParameter("projectId");
                    str = getOfficeProject(projectId);
                }
                LOGGER.debug(funName + " : " + userId + " Ends");
            }
            out.println(str);
            out.flush();
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String getBranchOffice(int bankId, String str_type) {
        String str = "";
        ScBankDevpartnerService scBankDevpartnerService = (ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");
        //ScBankDevpartnerSrBean scBankDevpartnerSrBean = new ScBankDevpartnerSrBean();
        //scBankDevpartnerSrBean.setsBankDevelHeadId(bankId);
        try {
            Object[] values = {
                "sbankDevelHeadId", Operation_enum.EQ, bankId,
                "isBranchOffice", Operation_enum.LIKE, "Yes",
                "sbDevelopName", Operation_enum.ORDERBY, Operation_enum.ASC};

            List<TblScBankDevPartnerMaster> list = scBankDevpartnerService.findBranchList(values);
            str += "<option value=''>-- " + str_type + " Branch --</option>";
            for (TblScBankDevPartnerMaster tblScBankDevPartnerMaster : list) {
                str += "<option value='" + tblScBankDevPartnerMaster.getSbankDevelopId() + "'>" + tblScBankDevPartnerMaster.getSbDevelopName() + "</option>";
            }
        } catch (Exception e) {
            LOGGER.error("getBranchOffice : " + userId + e);
        }
        return str;
    }

    private String getBankName(String paymentType) {
        String str = "";
        try {
            System.out.print("paymentType " + paymentType);
            if (paymentType.trim().equalsIgnoreCase("Online")) {
                str += "<option value=''>-- Select Financial Institute --</option>";
                Collection<String> bankval = null;
                bankval = XMLReader.getBnkMsg().values();
                Object[] valArr = bankval.toArray();

                for (int i = 0; i < valArr.length; i++) {
                    if (i % 2 == 1) {
                        str += "<option value='" + valArr[i].toString() + "'>" + valArr[i].toString() + "</option>";
                    }
                }
            } else if (paymentType.trim().equalsIgnoreCase("Offline")) {
                str += "<option value=''>-- Select Financial Institute --</option>";
                CommonSearchDataMoreService csdms = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                List<SPCommonSearchDataMore> lstBank = csdms.geteGPData("getMainPaymentBanksList", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                for (SPCommonSearchDataMore spcsdm : lstBank) {
                    str += "<option value='" + spcsdm.getFieldName1() + "'>" + spcsdm.getFieldName2() + "</option>";
                }
            } else {
                str += "<option value=''>-- Select Financial Institute --</option>";
                // Online Payment gateway BankName
                Collection<String> bankval = null;
                bankval = XMLReader.getBnkMsg().values();
                Object[] valArr = bankval.toArray();

                for (int i = 0; i < valArr.length; i++) {
                    if (i % 2 == 1) {
                        str += "<option value='" + valArr[i].toString() + "'>" + valArr[i].toString() + "</option>";
                    }
                }

                // Offline Payment Bank name
                CommonSearchDataMoreService csdms = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                List<SPCommonSearchDataMore> lstBank = csdms.geteGPData("getMainPaymentBanksList", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                for (SPCommonSearchDataMore spcsdm : lstBank) {
                    str += "<option value='" + spcsdm.getFieldName1() + "'>" + spcsdm.getFieldName2() + "</option>";
                }
            }
        } catch (Exception e) {
            LOGGER.error("getBranchOffice : " + userId + e);
        }
        return str;
    }

    private String getBranchOffice(int bankId, int branchId) {
        String str = "";
        ScBankDevpartnerService scBankDevpartnerService = (ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");
        //ScBankDevpartnerSrBean scBankDevpartnerSrBean = new ScBankDevpartnerSrBean();
        //scBankDevpartnerSrBean.setsBankDevelHeadId(bankId);
        try {
            Object[] values = {
                "sbankDevelHeadId", Operation_enum.EQ, bankId,
                "isBranchOffice", Operation_enum.LIKE, "Yes",
                "partnerType", Operation_enum.ORDERBY, Operation_enum.ASC};

            List<TblScBankDevPartnerMaster> list = scBankDevpartnerService.findBranchList(values);
            str += "<option value=''>-- Schedule Bank Branch --</option>";
            for (TblScBankDevPartnerMaster tblScBankDevPartnerMaster : list) {
                if (branchId == tblScBankDevPartnerMaster.getSbankDevelopId()) {
                    str += "<option selected='selected' value='" + tblScBankDevPartnerMaster.getSbankDevelopId() + "'>" + tblScBankDevPartnerMaster.getSbDevelopName() + "</option>";
                } else {
                    str += "<option value='" + tblScBankDevPartnerMaster.getSbankDevelopId() + "'>" + tblScBankDevPartnerMaster.getSbDevelopName() + "</option>";
                }
            }
        } catch (Exception e) {
            LOGGER.error("getBranchOffice : " + userId + e);
        }
        return str;
    }

    private String getpeOffice(int deptId, int districtId) {
        String str = "";
        //Integer dptId = Integer.valueOf(deptId);

        // System.out.println(" AAAAAAAAa " + deptId);
        //Object[] objValues = {"tblStateMaster", Operation_enum.EQ, new TblStateMaster((short) districtId),
        //                   "departmentId", Operation_enum.EQ, deptId };
        //System.out.println(" AAAAAAAAa " + districtId);
        PEOfficeCreationService peOfficeCreationService = (PEOfficeCreationService) AppContext.getSpringBean("PEOfficeCreationService");
        try {
            List<TblOfficeMaster> list = peOfficeCreationService.getPEOfficeList(deptId, districtId);
            str += "<option value=''>-- Select --</option>";
            for (TblOfficeMaster tblOfficeMaster : list) {
                str += "<option value='" + tblOfficeMaster.getOfficeId() + "'>" + tblOfficeMaster.getOfficeName() + "</option>";
            }
        } catch (Exception e) {
            LOGGER.error("getpeOffice : " + userId + e);
        }
        return str;
    }

    private String getState(short countryId) {
        String str = "";
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        try {
            List<TblStateMaster> list = commonService.getState(countryId);
            for (TblStateMaster tblStateMaster : list) {
                str += "<option value='" + tblStateMaster.getStateId() + "'>" + tblStateMaster.getStateName() + "</option>";
            }
        } catch (Exception e) {
            LOGGER.error("getState : " + userId + e);
        }
        return str;
    }

    private String getStateNOA(short countryId) {
        String str = "";
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        try {
            List<TblStateMaster> list = commonService.getState(countryId);
            str += "<option value=' '>-Select State-</option>";
            for (TblStateMaster tblStateMaster : list) {
                str += "<option value='" + tblStateMaster.getStateName() + "'>" + tblStateMaster.getStateName() + "</option>";
            }
        } catch (Exception e) {
            LOGGER.error("getState : " + userId + e);
        }
        return str;
    }

    private String getMinistry(short deptId) {
        String str = "";
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        try {
            str += "<option>-- Select Ministry --</option>";
            List<TblDepartmentMaster> list = commonService.getMinistryList(deptId);
            for (TblDepartmentMaster tblDepartmentMaster : list) {
                str += "<option value='" + tblDepartmentMaster.getDepartmentId() + "'>" + tblDepartmentMaster.getDepartmentName() + "</option>";
            }
        } catch (Exception e) {
            LOGGER.error("getMinistry : " + userId + e);
        }
        return str;
    }

    private String getDivision(short deptId) {
        String str = "";
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        try {
            str += "<option>-- Select Division --</option>";
            List<TblDepartmentMaster> list = commonService.getDivisionList(deptId);
            for (TblDepartmentMaster tblDepartmentMaster : list) {
                str += "<option value='" + tblDepartmentMaster.getDepartmentId() + "'>" + tblDepartmentMaster.getDepartmentName() + "</option>";
            }
        } catch (Exception e) {
            LOGGER.error("getDivision : " + userId + e);
        }
        return str;
    }

    private String getOrganization(short deptId) {
        String str = "";
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        try {
            str += "<option>-- Select Organization --</option>";
            List<TblDepartmentMaster> list = commonService.getOrganization(deptId);
            for (TblDepartmentMaster tblDepartmentMaster : list) {
                str += "<option value='" + tblDepartmentMaster.getDepartmentId() + "'>" + tblDepartmentMaster.getDepartmentName() + "</option>";
            }
        } catch (Exception e) {
            LOGGER.error("getOrganization : " + userId + e);
        }
        return str;
    }

    private String getAPPDetails(String financialYear, String userId) {
        String str = "";
        APPService appService = (APPService) AppContext.getSpringBean("APPService");
        List<CommonAppData> appList = new ArrayList<CommonAppData>();
        try {
            for (CommonAppData commonAppData : appService.getAPPDetailsBySP("allapp", financialYear, userId)) {
                appList.add(commonAppData);
            }
        } catch (Exception e) {
            LOGGER.error("getAPPDetails : " + userId + e);
        }
        try {
            str += "<option value=''>-- Select APP --</option>";
            for (CommonAppData commonapp : appList) {
                str += "<option value='" + commonapp.getFieldName1() + "'>" + commonapp.getFieldName2() + "</option>";
            }
        } catch (Exception e) {
            LOGGER.error("getAPPDetails : " + userId + e);
        }
        return str;
    }

    private String getStateValue(short countryId) {
        String str = "";
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        try {
            List<TblStateMaster> list = commonService.getState(countryId);
            for (TblStateMaster tblStateMaster : list) {
                if (tblStateMaster.getStateName().equals("Dhaka")) {
                    str += "<option value='" + tblStateMaster.getStateName() + "' selected>" + tblStateMaster.getStateName() + "</option>";
                } else {
                    str += "<option value='" + tblStateMaster.getStateName() + "'>" + tblStateMaster.getStateName() + "</option>";
                }
            }
        } catch (Exception e) {
            LOGGER.error("getStateValue : " + userId + e);
        }
        return str;
    }
    
    private String GetSubdistrictList(String state, String SubDistrict) {
        String str = "";
        try {
            TendererMasterSrBean tendererMasterSrBean = new TendererMasterSrBean();
            //TblTempCompanyMaster dtBean = tendererMasterSrBean.getCompanyDetails(Integer.parseInt(user));
            //String sub = (dtBean.getSubDistrict()==null)? "" : dtBean.getSubDistrict();
            List<SelectItem> list = tendererMasterSrBean.getSubdistrictList(state);
            for (SelectItem subdistrict : list) {
                if (subdistrict.getObjectValue().equals(SubDistrict)) {
                    str += "<option value='" + subdistrict.getObjectValue() + "' selected>" + subdistrict.getObjectValue() + "</option>";
                } else {
                    str += "<option value='" + subdistrict.getObjectValue() + "'>" + subdistrict.getObjectValue() + "</option>";
                }
            }
        } catch (Exception e) {
            LOGGER.error("getSubDistrictValue : " + userId + e);
        }
        return str;
    }
            
            
    private String getSubDistrictValueOffSubDistrict(String state, String user) {
        String str = "";
        try {
            TendererMasterSrBean tendererMasterSrBean = new TendererMasterSrBean();
            TblTempCompanyMaster dtBean = tendererMasterSrBean.getCompanyDetails(Integer.parseInt(user));
            //String sub = (dtBean.getSubDistrict()==null)? "" : dtBean.getSubDistrict();
            List<SelectItem> list = tendererMasterSrBean.getSubdistrictList(state);
            for (SelectItem subdistrict : list) {
//                if ((dtBean == null) ? false : subdistrict.getObjectValue().equals(dtBean.getRegOffSubDistrict())) {
//                    str += "<option value='" + subdistrict.getObjectValue() + "' selected>" + subdistrict.getObjectValue() + "</option>";
//                } else {
                    str += "<option value='" + subdistrict.getObjectValue() + "'>" + subdistrict.getObjectValue() + "</option>";
//                }
            }
        } catch (Exception e) {
            LOGGER.error("getSubDistrictValue : " + userId + e);
        }
        return str;
    }
    
    private String getSubDistrictValueCorpOffSubDistrict(String state, String user) {
        String str = "";
        try {
            TendererMasterSrBean tendererMasterSrBean = new TendererMasterSrBean();
            TblTempCompanyMaster dtBean = tendererMasterSrBean.getCompanyDetails(Integer.parseInt(user));
            //String sub = (dtBean.getSubDistrict()==null)? "" : dtBean.getSubDistrict();
            List<SelectItem> list = tendererMasterSrBean.getSubdistrictList(state);
            for (SelectItem subdistrict : list) {
                if ((dtBean == null) ? false : subdistrict.getObjectValue().equals(dtBean.getCorpOffSubDistrict())) {
                    str += "<option value='" + subdistrict.getObjectValue() + "' selected>" + subdistrict.getObjectValue() + "</option>";
                } else {
                    str += "<option value='" + subdistrict.getObjectValue() + "'>" + subdistrict.getObjectValue() + "</option>";
                }
            }
        } catch (Exception e) {
            LOGGER.error("getSubDistrictValue : " + userId + e);
        }
        return str;
    }

    private String getSubDistrictValue(String state, String user) {
        String str = "";
        try {
            TendererMasterSrBean tendererMasterSrBean = new TendererMasterSrBean();
            TblTempTendererMaster dtBean = tendererMasterSrBean.getTendererDetails(Integer.parseInt(user));
            //String sub = (dtBean.getSubDistrict()==null)? "" : dtBean.getSubDistrict();
            List<SelectItem> list = tendererMasterSrBean.getSubdistrictList(state);
            for (SelectItem subdistrict : list) {
                if ((dtBean == null) ? false : subdistrict.getObjectValue().equals(dtBean.getSubDistrict())) {
                    str += "<option value='" + subdistrict.getObjectValue() + "' selected>" + subdistrict.getObjectValue() + "</option>";
                } else {
                    str += "<option value='" + subdistrict.getObjectValue() + "'>" + subdistrict.getObjectValue() + "</option>";
                }
            }
        } catch (Exception e) {
            LOGGER.error("getSubDistrictValue : " + userId + e);
        }
        return str;
    }

    private String getAreaCode(String state, String user) {
        TendererMasterSrBean tendererMasterSrBean = new TendererMasterSrBean();
        String areaCode = tendererMasterSrBean.getareaCode(state);
        if (areaCode != null) {
            return areaCode;
        } else {
            return "";
        }
    }

    private String getSubDistrictValue2(String state, String user) {
        String str = "<option value=''>-- Select Dungkhag --</option>";

        String delimiters = ",";
        String[] newState = state.split(delimiters);

        try {
            PEOfficeCreationSrBean tendererMasterSrBean = new PEOfficeCreationSrBean();

            List<SelectItem> list = tendererMasterSrBean.getSubdistrictList(newState[0]);
            for (SelectItem subdistrict : list) {
                str += "<option value='" + subdistrict.getObjectValue() + "'>" + subdistrict.getObjectValue() + "</option>";

            }
        } catch (Exception e) {
            LOGGER.error("getSubDistrictValue : " + userId + e);
        }
        return str;
    }

    private String getSubDistrictValue3(String state, String user) {
        String str = "<option value=''>-- Select Gewog --</option>";

        String delimiters = ",";
        // analyzing the string
        String[] newState = state.split(delimiters);

        try {
            PEOfficeCreationSrBean tendererMasterSrBean = new PEOfficeCreationSrBean();
            List<SelectItem> list = tendererMasterSrBean.getSubdistrictList2(newState[0]);
            for (SelectItem subdistrict : list) {
                str += "<option value='" + subdistrict.getObjectValue() + "'>" + subdistrict.getObjectValue() + "</option>";

            }
        } catch (Exception e) {
            LOGGER.error("getSubDistrictValue : " + userId + e);
        }
        return str;
    }

    private String getSubDistrictValue4(String state, String user) {
        String str = "<option value=''>-- Select Gewog --</option>";
        try {
            PEOfficeCreationSrBean tendererMasterSrBean = new PEOfficeCreationSrBean();
            List<SelectItem> list = tendererMasterSrBean.getSubdistrictList4(state);
            for (SelectItem subdistrict : list) {
                str += "<option value='" + subdistrict.getObjectValue() + "'>" + subdistrict.getObjectValue() + "</option>";

            }
        } catch (Exception e) {
            LOGGER.error("getSubDistrictValue : " + userId + e);
        }
        return str;
    }

    private String getDeptyType(String objectId) {
        String str = "";
        String objectDept;
        DepartmentCreationService departmentCreationService = (DepartmentCreationService) AppContext.getSpringBean("DepartmentCreationService");
        try {
            if ("Division".equals(objectId)) {
                objectDept = "Ministry";
            } else if ("Organization".equals(objectId)) {
                objectDept = "Division";
            } else {
                objectDept = "";
            }
            List<TblDepartmentMaster> list = departmentCreationService.getDeptTypeName(objectDept);
            for (TblDepartmentMaster tblDepartmentMaster : list) {
                str += "<option value='" + tblDepartmentMaster.getDepartmentId() + "'>" + tblDepartmentMaster.getDepartmentName() + "</option>";
            }
        } catch (Exception e) {
            LOGGER.error("getDeptyType : " + userId + e);
        }
        return str;
    }

    private String getDepartmentOffice(int deptId) {
        String str = "";
        try {
            PEOfficeCreationService officeCreationService = (PEOfficeCreationService) AppContext.getSpringBean("PEOfficeCreationService");
            List<TblOfficeMaster> officeMasters = officeCreationService.findOfficeMaster("departmentId", Operation_enum.EQ, deptId);
            StringBuilder list = new StringBuilder();
            //Change 'Select Procuring Entity' to 'Select Procuring Agency' by Proshanto Kumar Saha
            list.append("<option value=\" \">-- Select Procuring Agency --</option>");
            if (officeMasters != null && !officeMasters.isEmpty()) {
                for (TblOfficeMaster officeMaster : officeMasters) {
                    list.append("<option value=\"" + officeMaster.getOfficeId() + "\">" + officeMaster.getOfficeName() + "</option>");
                }
            }
            str = list.toString();
        } catch (Exception ex) {
            LOGGER.error("getDepartmentOffice : " + userId + ex);
        }
        return str;
    }

    private String getDepartment(int deptId) {
        String str = "";
        try {
            PEOfficeCreationService officeCreationService = (PEOfficeCreationService) AppContext.getSpringBean("PEOfficeCreationService");
            List<TblOfficeMaster> officeMasters = officeCreationService.findOfficeMaster("departmentId", Operation_enum.EQ, deptId);
            StringBuilder list = new StringBuilder();
            //Change 'Select Procuring Entity' to 'Select Procuring Agency' by Proshanto Kumar Saha
            list.append("<option value=\" \">-- Select Procuring Agency --</option>");
            if (officeMasters != null && !officeMasters.isEmpty()) {
                for (TblOfficeMaster officeMaster : officeMasters) {
                    list.append("<option value=\"" + officeMaster.getOfficeId() + "\">" + officeMaster.getOfficeName() + "</option>");
                }
                list.append("<option value=\"0\">ALL</option>");
            }

            str = list.toString();
        } catch (Exception ex) {
            LOGGER.error("getDepartmentOffice : " + userId + ex);
        }
        return str;
    }

    private String getDepartmentOfficeForPromis(int deptId) {
        String str = "";
        try {
            PEOfficeCreationService officeCreationService = (PEOfficeCreationService) AppContext.getSpringBean("PEOfficeCreationService");
            List<TblOfficeMaster> officeMasters = officeCreationService.findOfficeMaster("departmentId", Operation_enum.EQ, deptId);
            StringBuilder list = new StringBuilder();
            list.append("<option value=\"0\">-- Select Office --</option>");
            if (officeMasters != null && !officeMasters.isEmpty()) {
                for (TblOfficeMaster officeMaster : officeMasters) {
                    list.append("<option value=\"" + officeMaster.getOfficeId() + "\">" + officeMaster.getOfficeName() + "</option>");
                }
            }
            str = list.toString();
        } catch (Exception ex) {
            LOGGER.error("getDepartmentOffice : " + userId + ex);
        }
        return str;
    }

    private String getOfflineAwardedOrganization(String deptId) {
        String str = "";
        ContractAwardOfflineService contractAwardOffline = (ContractAwardOfflineService) AppContext.getSpringBean("ContractAwardOfflineService");
        try {
            str += "<option>-- Select Organization --</option>";
            List<Object[]> list = contractAwardOffline.getOrgForAwardedContractOffline(deptId);
            for (int i = 0; i < list.size(); i++) {
                str += "<option value='" + list.get(i) + "'>" + list.get(i) + "</option>";
            }
        } catch (Exception e) {
            LOGGER.error("getOrganization : " + userId + e);
        }
        return str;
    }

    private String getOfflineTenderOrganization(String deptId) {
        String str = "";
        TenderDashboardOfflineService tenderDashboardOffline = (TenderDashboardOfflineService) AppContext.getSpringBean("TenderDashboardOfflineService");
        try {
            str += "<option>-- Select Organization --</option>";
            List<Object[]> list = tenderDashboardOffline.getOrgForTenderOffline(deptId);
            for (int i = 0; i < list.size(); i++) {
                str += "<option value='" + list.get(i) + "'>" + list.get(i) + "</option>";
            }
        } catch (Exception e) {
            LOGGER.error("getOrganization : " + userId + e);
        }
        return str;
    }

    private String getOfflineAwardedOrganizationHome(String deptId) {
        String str = "";
        ContractAwardOfflineService contractAwardOffline = (ContractAwardOfflineService) AppContext.getSpringBean("ContractAwardOfflineService");
        try {
            str += "<option>-- Select Organization --</option>";
            List<Object[]> list = contractAwardOffline.getOrgForAwardedContractOfflineHome(deptId);
            for (int i = 0; i < list.size(); i++) {
                str += "<option value='" + list.get(i) + "'>" + list.get(i) + "</option>";
            }
        } catch (Exception e) {
            LOGGER.error("getOrganization : " + userId + e);
        }
        return str;
    }

    private String getOfficeProject(String projectId) {
        String str = "";
        try {
            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
            List<Object[]> officeMasters = commonService.getProjectOffice(projectId);
            StringBuilder list = new StringBuilder();
            list.append("<option value=\"0\">-- Select Office --</option>");
            if (officeMasters != null && !officeMasters.isEmpty()) {
                for (Object[] officeMaster : officeMasters) {
                    list.append("<option value=\"" + officeMaster[0].toString() + "\">" + officeMaster[1].toString() + "</option>");
                }
            }
            str = list.toString();
        } catch (Exception ex) {
            LOGGER.error("getDepartmentOffice : " + userId + ex);
        }
        return str;
    }

    //Show PE Office List as Table created by Nafiul
    private String getDepartmentOfficeListAsTable(int deptId) {

        String str = "";
        try {
            PEOfficeCreationService officeCreationService = (PEOfficeCreationService) AppContext.getSpringBean("PEOfficeCreationService");
            List<TblOfficeMaster> officeMasters = officeCreationService.findOfficeMaster("departmentId", Operation_enum.EQ, deptId);
            StringBuilder list = new StringBuilder();
            //Change 'Select Procuring Entity' to 'Select Procuring Agency' by Proshanto Kumar Saha
            //list.append("<option value=\" \">-- Select Procuring Agency --</option>");
//            list.append("<tr>\n"
//                    + "                    <th width=\"7%\" class=\"t-align-center\">\n"
//                    + "                        Select All\n"
//                    + "			<input type=\"checkbox\" onclick=\"checkUncheck('map',this);\" id=\"mapallchk\"/>\n"
//                    + "                    </th>\n"
//                    + "                    <th class=\"t-align-left\" width=\"5%\">PE Office Name</th>\n"
//                    + "                    <th class=\"t-align-left\" width=\"20%\">District</th>\n"
//                    + "                    <th class=\"t-align-left\" width=\"15%\">Address</th>\n"
//                    + "                </tr>");
            int i = 0;
            if (officeMasters != null && !officeMasters.isEmpty()) {
                for (TblOfficeMaster officeMaster : officeMasters) {
                    list.append(" <tr class=\"peOfficeTableRow\">"
                            + "<td class=\"t-align-center\"><input name=\"officeid\" type=\"checkbox\" value=\""
                            + officeMaster.getOfficeId()
                            + "\" id=\"chkUnmap" + i + "\" chk=\"map\"/></td>\n"
                            + "<td class=\"t-align-left\">" + officeMaster.getOfficeName()
                            + "<input type=\"hidden\" name=\"officeName\" id=\"officeName\" value=\""
                            + officeMaster.getOfficeName() + "\" /></td>\n"
                            + "<td class=\"t-align-left\">" + officeMaster.getCity()
                            + "<input type=\"hidden\" name=\"officeCity\" id=\"officeCity\" value=\""
                            + officeMaster.getCity() + "\" /></td>\n"
                            + "<td class=\"t-align-left\">" + officeMaster.getAddress()
                            + "<input type=\"hidden\" name=\"officeAddress\" id=\"officeAddress\" value=\""
                            + officeMaster.getAddress() + "\" /></td>"
                            + "</tr>"
                    );
                    i++;
                }

            }
            if (i == 0) {
                list.append("<tr><td colspan='4' class='t-align-center'><span style='color:red;'>No Records Found</span></td></tr>");
            }
            str = list.toString();
        } catch (Exception ex) {
            LOGGER.error("getDepartmentOffice : " + userId + ex);
        }
        return str;
    }

}
