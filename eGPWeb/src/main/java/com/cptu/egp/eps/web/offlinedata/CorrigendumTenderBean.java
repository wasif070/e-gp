/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.offlinedata;

import com.cptu.egp.eps.model.table.TblTenderDetailsOffline;
import com.cptu.egp.eps.model.table.TblCorrigendumDetailOffline;
import com.cptu.egp.eps.model.table.TblTenderLotPhasingOffline;
import com.cptu.egp.eps.service.serviceimpl.CorrigendumDetailsOfflineService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CommonUtils;
import java.util.ArrayList;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
//import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import java.lang.String;
import com.cptu.egp.eps.web.utility.DateUtils;
import java.lang.reflect.Method;
import java.math.BigDecimal;

/**
 *
 * @author salahuddin
 */
public class CorrigendumTenderBean {

    
    private static final Logger LOGGER = Logger.getLogger(CorrigendumTenderBean.class);    
    TenderFormExcellBean tenderExcelBean = null;
    List<TblCorrigendumDetailOffline> corrigendumDetailOffline = null;
    private final CorrigendumDetailsOfflineService corrigendumDetailsOfflineService  = (CorrigendumDetailsOfflineService) AppContext.getSpringBean("CorrigendumDetailsOfflineService");
    private String corriStatus = "Pending";
    private int CorNo=0;
    Date date = new Date();

    private String oldDate = "";
    private String newDate = "";


    public void createCorrigendum(TblTenderDetailsOffline tenderDetailsOffline,int tenderOLId, int userID, int corrigendumNo, boolean isEdit) throws Exception {


        LOGGER.debug("createCorri : " + userID + "starts");
        //TblTenderDetails tblTenderDetails = tenderCorriService.getTenderDetails("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(Integer.parseInt(tenderid)));

        CorNo = corrigendumNo;

        OfflineDataSrBean offlineDataSrBean = new OfflineDataSrBean();
        tenderExcelBean = (TenderFormExcellBean) offlineDataSrBean.editTenderWithoutPQForm(tenderOLId);

        List<TblCorrigendumDetailOffline> list = new ArrayList<TblCorrigendumDetailOffline>();

        if(!tenderExcelBean.getProcurementNature().equals(tenderDetailsOffline.getProcurementNature()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "procurementNature",tenderExcelBean.getProcurementNature(), tenderDetailsOffline.getProcurementNature(), corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getProcurementType().equals(tenderDetailsOffline.getProcurementType()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "procurementType",tenderExcelBean.getProcurementType(), tenderDetailsOffline.getProcurementType(), corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getEventType().equals(tenderDetailsOffline.getEventType()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "eventType",tenderExcelBean.getEventType(), tenderDetailsOffline.getEventType(), corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getInvitationFor().equals(tenderDetailsOffline.getInvitationFor()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "invitationFor", tenderExcelBean.getInvitationFor(), tenderDetailsOffline.getInvitationFor(),  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getInvitationRefNo().equals(tenderDetailsOffline.getReoiRfpRefNo()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "reoiRfpRefNo", tenderExcelBean.getInvitationRefNo(), tenderDetailsOffline.getReoiRfpRefNo(),  corriStatus, CorNo, userID, date));
        }

        oldDate = tenderExcelBean.getTenderissuingdate();
        
        DateFormat dtNewDate = new SimpleDateFormat("dd/MM/yyyy");
        newDate = dtNewDate.format(tenderDetailsOffline.getIssueDate());

        if(!oldDate.equals(newDate))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "IssueDate", oldDate, newDate,  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getProcurementMethod().equals(tenderDetailsOffline.getProcurementMethod()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "procurementMethod", tenderExcelBean.getProcurementMethod(), tenderDetailsOffline.getProcurementMethod(),  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getCbobudget().equals(tenderDetailsOffline.getBudgetType()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "budgetType", tenderExcelBean.getCbobudget(), tenderDetailsOffline.getBudgetType(),  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getCbosource().equals(tenderDetailsOffline.getSourceOfFund()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "sourceOfFund", tenderExcelBean.getCbosource(), tenderDetailsOffline.getSourceOfFund(),  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getDevelopmentPartner().equals(tenderDetailsOffline.getDevPartners()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "devPartners", tenderExcelBean.getDevelopmentPartner(), tenderDetailsOffline.getDevPartners(),  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getProjectOrProgrammeCode().equals(tenderDetailsOffline.getProjectCode()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "projectCode", tenderExcelBean.getProjectOrProgrammeCode(), tenderDetailsOffline.getProjectCode(),  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getProjectOrProgrammeName().equals(tenderDetailsOffline.getProjectName()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "projectName", tenderExcelBean.getProjectOrProgrammeName(), tenderDetailsOffline.getProjectName(),  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getTenderPackageNo().equals(tenderDetailsOffline.getPackageNo()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "packageNo", tenderExcelBean.getTenderPackageNo(), tenderDetailsOffline.getPackageNo(),  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getTenderPackageName().equals(tenderDetailsOffline.getPackageName()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "PackageName", tenderExcelBean.getTenderPackageName(), tenderDetailsOffline.getPackageName(),  corriStatus, CorNo, userID, date));
        }

        oldDate = tenderExcelBean.getTenderPublicationDate(); //getPrequalificationPublicationDate();
        newDate = dtNewDate.format(tenderDetailsOffline.getTenderPubDate());

        if(!oldDate.equals(newDate))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "tenderPubDate", oldDate, newDate,  corriStatus, CorNo, userID, date));
        }

        //DateFormat dtOldDate1 = new SimpleDateFormat("dd/MM/yyyy");
        oldDate = tenderExcelBean.getTenderLastSellingDate();
        oldDate = oldDate.substring(0, 10).trim();
        newDate = dtNewDate.format(tenderDetailsOffline.getLastSellingDate());

        if(!oldDate.equals(newDate))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "lastSellingDate", oldDate, newDate,  corriStatus, CorNo, userID, date));
        }

        oldDate = tenderExcelBean.getTenderClosingDate(); //preqExcellBean.getPrequalificationClosingDateandTime();
        DateFormat dtNewDate1 = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        newDate = dtNewDate1.format(tenderDetailsOffline.getClosingDate());

        if(!oldDate.equals(newDate))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "ClosingDate", oldDate, newDate,  corriStatus, CorNo, userID, date));
        }

        oldDate = tenderExcelBean.getTenderOpeningDate();
        newDate = dtNewDate1.format(tenderDetailsOffline.getOpeningDate());        
        if(!oldDate.equals(newDate))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "OpeningDate", oldDate, newDate,  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getPlaceofTenderMeeting().equals(tenderDetailsOffline.getPreTenderReoiplace()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "PreTenderREOIPlace", tenderExcelBean.getPlaceofTenderMeeting(), tenderDetailsOffline.getPreTenderReoiplace(),  corriStatus, CorNo, userID, date));
        }

        oldDate = tenderExcelBean.getDateTimeofTenderMeeting();//preqExcellBean.getDateTimeofPrequalificationMeeting();
        newDate = dtNewDate1.format(tenderDetailsOffline.getPreTenderReoidate());

        if(!oldDate.equals(newDate))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "preTenderREOIDate", oldDate, newDate,  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getSellingTenderDocumentPrincipal().equals(tenderDetailsOffline.getSellingAddPrinciple()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "SellingAddPrinciple", tenderExcelBean.getSellingTenderDocumentPrincipal(), tenderDetailsOffline.getSellingAddPrinciple(),  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getSellingTenderDocumentOthers().equals(tenderDetailsOffline.getSellingAddOthers()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "SellingAddOthers", tenderExcelBean.getSellingTenderDocumentOthers(), tenderDetailsOffline.getSellingAddOthers(),  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getReceivingTenderDocument().equals(tenderDetailsOffline.getReceivingAdd()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "ReceivingAdd", tenderExcelBean.getReceivingTenderDocument(), tenderDetailsOffline.getReceivingAdd(),  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getOpeningTenderDocument().equals(tenderDetailsOffline.getOpeningAdd()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "OpeningAdd", tenderExcelBean.getOpeningTenderDocument(), tenderDetailsOffline.getOpeningAdd(),  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getEligibilityofTender().equals(tenderDetailsOffline.getEligibilityCriteria()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "EligibilityCriteria", tenderExcelBean.getEligibilityofTender(), tenderDetailsOffline.getEligibilityCriteria(),  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getBriefDescriptionofGoodsorWorks().equals(tenderDetailsOffline.getBriefDescription()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "BriefDescription", tenderExcelBean.getBriefDescriptionofGoodsorWorks(), tenderDetailsOffline.getBriefDescription(),  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getBriefDescriptionofRelatedServices().equals(tenderDetailsOffline.getRelServicesOrDeliverables()))
        {
            //String oldValue = preqExcellBean.getBriefDescriptionofRelatedServices().trim();
            //String newValue = tenderDetailsOffline.getRelServicesOrDeliverables().trim();
            
            String text[] = new String[2];
            text[0] = tenderExcelBean.getBriefDescriptionofRelatedServices().trim();
            text[1] = tenderDetailsOffline.getRelServicesOrDeliverables().trim();

            for(int i=0;i<2;i++)
            {
                //IF there is any <p> at the strat of the string                
                if (text[i].indexOf("<p>")==0) {
                    text[i] = text[i].substring(3, text[i].length()).trim();                    
                    text[i] = text[i].substring(0, text[i].length()-4).trim();                
                }                
            }

            if(!text[0].equals(text[1]))
            {
                //list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "RelServicesOrDeliverables", preqExcellBean.getBriefDescriptionofRelatedServices(), tenderDetailsOffline.getRelServicesOrDeliverables(),  corriStatus, userID, date));
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "RelServicesOrDeliverables", tenderExcelBean.getBriefDescriptionofRelatedServices(), text[1],  corriStatus, CorNo, userID, date));
            }
        }

        if(!tenderExcelBean.getTenderDocumentPrice().equals(tenderDetailsOffline.getDocumentPrice().toString()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "DocumentPrice", tenderExcelBean.getTenderDocumentPrice(), tenderDetailsOffline.getDocumentPrice().toString(),  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getNameofOfficialInvitingTender().equals(tenderDetailsOffline.getPeOfficeName()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "peName", tenderExcelBean.getNameofOfficialInvitingTender(), tenderDetailsOffline.getPeOfficeName(),  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getDesignationofOfficialInvitingTender().equals(tenderDetailsOffline.getPeDesignation()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "peDesignation", tenderExcelBean.getDesignationofOfficialInvitingTender(), tenderDetailsOffline.getPeDesignation(),  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getAddressofOfficialInvitingTender().equals(tenderDetailsOffline.getPeAddress()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "peAddress", tenderExcelBean.getAddressofOfficialInvitingTender(), tenderDetailsOffline.getPeAddress(),  corriStatus, CorNo, userID, date));
        }

        if(!tenderExcelBean.getContactdetailsofOfficialInvitingTender().equals(tenderDetailsOffline.getPeContactDetails()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "peContactDetails", tenderExcelBean.getContactdetailsofOfficialInvitingTender(), tenderDetailsOffline.getPeContactDetails(),  corriStatus, CorNo, userID, date));
        }
        
            String[][] lotDetails = {
                                        {"1", "2", "3", "4"},
                                        {"Lot Number", "Lot Identification", "Location", "Completion Time", "Bid Security"}
                                    };
            List<TblTenderLotPhasingOffline> tenderLotsAndPhases = tenderDetailsOffline.getTenderLotsAndPhases();            
                        

                //FIRST LOT
                if(!tenderExcelBean.getLotNo_1().equals(tenderLotsAndPhases.get(0).getLotOrRefNo()))
                {
                   list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][0], lotDetails[1][0] , tenderExcelBean.getLotNo_1(), tenderLotsAndPhases.get(0).getLotOrRefNo(),  corriStatus, CorNo, userID, date));
                }

                if(!tenderExcelBean.getIdentificationOfLot_1().equals(tenderLotsAndPhases.get(0).getLotIdentOrPhasingServ()))
                {
                   list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][0], lotDetails[1][1] , tenderExcelBean.getIdentificationOfLot_1(), tenderLotsAndPhases.get(0).getLotIdentOrPhasingServ(),  corriStatus, CorNo, userID, date));
                }

                if(!tenderExcelBean.getLocation_1().equals(tenderLotsAndPhases.get(0).getLocation()))
                {
                   list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][0], lotDetails[1][2] , tenderExcelBean.getLocation_1(), tenderLotsAndPhases.get(0).getLocation(),  corriStatus, CorNo, userID, date));
                }

                if(!tenderExcelBean.getCompletionTimeInWeeksORmonths_1().equals(tenderLotsAndPhases.get(0).getCompletionDateTime()))
                {
                   list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][0], lotDetails[1][3] , tenderExcelBean.getCompletionTimeInWeeksORmonths_1(), tenderLotsAndPhases.get(0).getCompletionDateTime(),  corriStatus, CorNo, userID, date));
                }

                if(!tenderExcelBean.getTenderSecurityAmount_1().equals(tenderLotsAndPhases.get(0).getTenderSecurityAmt().toString()))
                {
                    list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][0], lotDetails[1][4] , tenderExcelBean.getTenderSecurityAmount_1(), tenderLotsAndPhases.get(0).getTenderSecurityAmt().toString(),  corriStatus, CorNo, userID, date));
                }
            
        if(tenderLotsAndPhases.size()>1)
                {
                //FOR SECOND LOT
            if(!tenderLotsAndPhases.get(1).getLotOrRefNo().equals("") || !tenderLotsAndPhases.get(1).getLotIdentOrPhasingServ().equals("") || !tenderLotsAndPhases.get(1).getLocation().equals("") ||  !tenderLotsAndPhases.get(1).getCompletionDateTime().equals("")||  !tenderLotsAndPhases.get(1).getTenderSecurityAmt().toString().equals(""))
                {
                    String test= CommonUtils.checkNull(tenderExcelBean.getLotNo_2());
                    String test1= CommonUtils.checkNull(tenderLotsAndPhases.get(1).getLotOrRefNo());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][1], lotDetails[1][0] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(tenderExcelBean.getIdentificationOfLot_2());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(1).getLotIdentOrPhasingServ());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][1], lotDetails[1][1] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(tenderExcelBean.getLocation_2());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(1).getLocation());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][1], lotDetails[1][2] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(tenderExcelBean.getCompletionTimeInWeeksORmonths_2());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(1).getCompletionDateTime());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][1], lotDetails[1][3] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(tenderExcelBean.getTenderSecurityAmount_2());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(1).getTenderSecurityAmt().toString());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][1], lotDetails[1][4] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                }
            }
            if(tenderLotsAndPhases.size()>2)
            {
            //FOR THIRD LOT
                if(!tenderLotsAndPhases.get(2).getLotOrRefNo().equals("") || !tenderLotsAndPhases.get(2).getLotIdentOrPhasingServ().equals("") || !tenderLotsAndPhases.get(2).getLocation().equals("") ||  !tenderLotsAndPhases.get(2).getCompletionDateTime().equals("")||  !tenderLotsAndPhases.get(2).getTenderSecurityAmt().toString().equals(""))
                {
                    String test= CommonUtils.checkNull(tenderExcelBean.getLotNo_3());
                    String test1= CommonUtils.checkNull(tenderLotsAndPhases.get(2).getLotOrRefNo());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][2], lotDetails[1][0] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(tenderExcelBean.getIdentificationOfLot_3());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(2).getLotIdentOrPhasingServ());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][2], lotDetails[1][1] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(tenderExcelBean.getLocation_3());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(2).getLocation());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][2], lotDetails[1][2] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(tenderExcelBean.getCompletionTimeInWeeksORmonths_3());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(2).getCompletionDateTime());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][2], lotDetails[1][3] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(tenderExcelBean.getTenderSecurityAmount_3());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(2).getTenderSecurityAmt().toString());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][2], lotDetails[1][4] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                }
        }
            if(tenderLotsAndPhases.size()>3)
            {
            //FOR FORTH LOT
                if(!tenderLotsAndPhases.get(3).getLotOrRefNo().equals("") || !tenderLotsAndPhases.get(3).getLotIdentOrPhasingServ().equals("") || !tenderLotsAndPhases.get(3).getLocation().equals("") ||  !tenderLotsAndPhases.get(3).getCompletionDateTime().equals("")||  !tenderLotsAndPhases.get(3).getTenderSecurityAmt().toString().equals(""))
                {
                    String test= CommonUtils.checkNull(tenderExcelBean.getLotNo_4());
                    String test1= CommonUtils.checkNull(tenderLotsAndPhases.get(3).getLotOrRefNo());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][3], lotDetails[1][0] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(tenderExcelBean.getIdentificationOfLot_4());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(3).getLotIdentOrPhasingServ());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][3], lotDetails[1][1] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(tenderExcelBean.getLocation_4());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(3).getLocation());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][3], lotDetails[1][2] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(tenderExcelBean.getCompletionTimeInWeeksORmonths_4());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(3).getCompletionDateTime());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][3], lotDetails[1][3] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(tenderExcelBean.getTenderSecurityAmount_4());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(3).getTenderSecurityAmt().toString());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][3], lotDetails[1][4] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                }
        }
        
      if (!list.isEmpty())
      {                      
        //corrigendumDetailsOfflineService.insertCorriDetailsOffline(list, userID, tenderOLId, isEdit);
          boolean success;
          if(isEdit)
          {
              corrigendumDetailOffline = offlineDataSrBean.corrigendumDetails(tenderOLId);              

              success= corrigendumDetailsOfflineService.deleteCorriDetailsOffline(corrigendumDetailOffline, userID, tenderOLId);
              corrigendumDetailsOfflineService.insertCorriDetailsOffline(list, userID);
          }
          else
          {
            corrigendumDetailsOfflineService.insertCorriDetailsOffline(list, userID);
          }
      }
        LOGGER.debug("createCorri : " + userID + "ends");
    }

}
