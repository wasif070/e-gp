/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.offlinedata;

import com.cptu.egp.eps.web.utility.CommonUtils;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.poi.hssf.eventusermodel.HSSFEventFactory;
import org.apache.poi.hssf.eventusermodel.HSSFListener;
import org.apache.poi.hssf.eventusermodel.HSSFRequest;
import org.apache.poi.hssf.record.BOFRecord;
import org.apache.poi.hssf.record.BoundSheetRecord;
import org.apache.poi.hssf.record.LabelSSTRecord;
import org.apache.poi.hssf.record.NumberRecord;
import org.apache.poi.hssf.record.Record;
import org.apache.poi.hssf.record.RowRecord;
import org.apache.poi.hssf.record.SSTRecord;
import org.apache.poi.hssf.record.common.UnicodeString;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

/**
 *
 * @author Administrator
 */
public class ExcelParsingbyEventHandler implements HSSFListener{

  
  private Map<String,String> preqData = new HashMap<String,String>();
  private Map<String,String> preqTimeData = new HashMap<String,String>();
  private Map<String,String> tenderformData = new HashMap<String,String>();
  private Map<String,String> tenderTimeData = new HashMap<String,String>();
  private Map<String,String> contractAwarData = new HashMap<String,String>();
  private Map<String,String> eoiData = new HashMap<String,String>();
  private Map<String,String> eoiTimeData = new HashMap<String,String>();

  private SSTRecord sstrec;
  // prequalification sheet cells initialization
  private Map<String , String> prequaMcrsCells = ExcelCells.getPre_qualificationMacros();
  private Map<String,String> prequaFrmShtCells = ExcelCells.getPre_qualificationFormSheet();
  private Map<String,String> preqTimeCells = ExcelCells.getPreq_timeCells();

  // Tender sheet cells initialization
  private Map<String,String> tenderFrmMcrsCells = ExcelCells.getTenderSheetMacrosCells();
  private Map<String,String> tenderFrmShtCells = ExcelCells.getTenderSheetCells();
  private Map<String,String> tenderTimeCells = ExcelCells.getTender_timeCells();

  // Contract Award Sheet Cells Initialization
  private Map<String,String> awardFrmMrcCells = ExcelCells.getContractAwardMacros();
  private Map<String,String> awardFrmShtCells = ExcelCells.getContractAwardCells();

  // EOI Sheet Cells Initialization
  private Map<String,String> eoiFrmMrcCells = ExcelCells.getEOImacros();
  private Map<String,String> eoiFrmShtCells = ExcelCells.getEOISheetCells();
  private Map<String,String> eoiTimeCells = ExcelCells.getEOItimeCells();
  private String fileName;
 
  // special cell values for time
 
    /**
     * This method listens for incoming records and handles them as required.
     * @param record    The record that was found while reading.
     */
    public void processRecord(Record record)
    {
    	//System.out.println("sid >> "+record.getSid()+" class name >> "+record.getClass().getName());
    	switch (record.getSid())
        {
            case NumberRecord.sid:
                 NumberRecord numrec = (NumberRecord) record;
              
                 if(this.getFileName().equals("Pre-quaificatonForm_1.xls") && preqTimeCells.containsKey(numrec.getRow()+"~"+numrec.getColumn())){
                    // if((numrec.getRow()+"~"+numrec.getColumn()).equals("3~26")){
                            String hrsstr = Double.toString(numrec.getValue());
                            String hrs = hrsstr.substring(0,2);
                            getPreqTimeData().put(preqTimeCells.get(numrec.getRow()+"~"+numrec.getColumn()),hrs);
                    // }
                 }else if(this.getFileName().equals("TenderForm_2.xls") && tenderTimeCells.containsKey(numrec.getRow()+"~"+numrec.getColumn())){
                    // if((numrec.getRow()+"~"+numrec.getColumn()).equals("3~26")){
                            String hrsstr = Double.toString(numrec.getValue());
                            String hrs = hrsstr.substring(0,2);
                            getTenderTimeData().put(tenderTimeCells.get(numrec.getRow()+"~"+numrec.getColumn()),hrs);
                    // }
                 }else if(this.getFileName().equals("EOI_3.xls") && eoiTimeCells.containsKey(numrec.getRow()+"~"+numrec.getColumn())){
                    // if((numrec.getRow()+"~"+numrec.getColumn()).equals("3~26")){
                            String hrsstr = Double.toString(numrec.getValue());
                            String hrs = hrsstr.substring(0,2);
                            getEoiTimeData().put(eoiTimeCells.get(numrec.getRow()+"~"+numrec.getColumn()),hrs);
                    // }
                 }
                break;
            case SSTRecord.sid:
                sstrec = (SSTRecord) record;
                break;
           case LabelSSTRecord.sid:
                 LabelSSTRecord lrec = (LabelSSTRecord) record;
                 UnicodeString uniString  = sstrec.getString(lrec.getSSTIndex());
                if(this.getFileName().equals("Pre-quaificatonForm_1.xls")){
                 if(preqTimeCells.containsKey(lrec.getRow()+"~"+lrec.getColumn())){
                     System.out.println(lrec.getRow()+"~"+lrec.getColumn());
                      if(uniString != null && !uniString.toString().equals("null") && !uniString.toString().equals("*")){
                         getPreqTimeData().put(preqTimeCells.get(lrec.getRow()+"~"+lrec.getColumn()),uniString.toString());
                     }
                 }
                 if(prequaMcrsCells.containsKey(lrec.getRow()+"~"+lrec.getColumn())){
                     if(uniString != null && !uniString.toString().equals("null") && !uniString.toString().equals("*")){
                     getPreqData().put(prequaMcrsCells.get(lrec.getRow()+"~"+lrec.getColumn()), uniString.toString());
                     }
                 }
               }else if(this.getFileName().equals("TenderForm_2.xls")){
                 if(tenderTimeCells.containsKey(lrec.getRow()+"~"+lrec.getColumn())){
                     System.out.println(lrec.getRow()+"~"+lrec.getColumn());
                      if(uniString != null && !uniString.toString().equals("null") && !uniString.toString().equals("*")){
                         getTenderTimeData().put(tenderTimeCells.get(lrec.getRow()+"~"+lrec.getColumn()),uniString.toString());
                     }
                 }
                 if(tenderFrmMcrsCells.containsKey(lrec.getRow()+"~"+lrec.getColumn())){
                     if( uniString != null && !uniString.toString().equals("null") && !uniString.toString().equals("*")){
                     getTenderformData().put(tenderFrmMcrsCells.get(lrec.getRow()+"~"+lrec.getColumn()), uniString.toString());
                     }
                 }
               }else if(this.getFileName().equals("Awards_5.xls")){
                 if(awardFrmMrcCells.containsKey(lrec.getRow()+"~"+lrec.getColumn())){
                     if(uniString != null && !uniString.toString().equals("null") && !uniString.toString().equals("*")){
                     getContractAwarData().put(awardFrmMrcCells.get(lrec.getRow()+"~"+lrec.getColumn()), uniString.toString());
                     }
                 }
               }else if(this.getFileName().equals("EOI_3.xls")){
                 if(eoiTimeCells.containsKey(lrec.getRow()+"~"+lrec.getColumn())){
                     System.out.println(lrec.getRow()+"~"+lrec.getColumn());
                      if(uniString != null && !uniString.toString().equals("null") && !uniString.toString().equals("*")){
                         getEoiTimeData().put(eoiTimeCells.get(lrec.getRow()+"~"+lrec.getColumn()),uniString.toString());
                     }
                 }
                 if(eoiFrmMrcCells.containsKey(lrec.getRow()+"~"+lrec.getColumn())){
                     if(uniString != null && !uniString.toString().equals("null") && !uniString.toString().equals("*")){
                     getEoiData().put(eoiFrmMrcCells.get(lrec.getRow()+"~"+lrec.getColumn()), uniString.toString());
                     }
                 }

               }
                break;
            }
    	}


   public Object ParseExcell(String formType,String fileName,InputStream inputStream){
       Object retObj = null;
       PrequalificationExcellBean prqexbean = null;
       TenderFormExcellBean tenderFormExcellBean = null;
       ContractAwardExcellBean awardExcellBean = null;
       EOIExcellBean eOIExcellBean = null;
       ExcelParsingbyEventHandler eventHandler = new ExcelParsingbyEventHandler();
       //String formtype = "";
            try{
                if(formType.equals("TenderWithPQ") || formType.equals("TenderWithoutPQ")){
                    formType = "Form";
                }else if(formType.equals("Award")){
                    formType = "Award";
                }else if(formType.equals("EOI")){
                    formType = "EOI";
                }
                POIFSFileSystem myFileSystem = new POIFSFileSystem(inputStream);
                HSSFWorkbook myWorkBook = new HSSFWorkbook(myFileSystem);
                //System.out.print("get number of sheets >> "+myWorkBook.getNumberOfSheets());
                //System.out.println("cellsList >> "+pre_qualificationForm.size());
                 // get the Workbook (excel part) stream in a InputStream
                InputStream din = myFileSystem.createDocumentInputStream("Workbook");
                // construct out HSSFRequest object
                HSSFRequest req = new HSSFRequest();
                
                eventHandler.setFileName(fileName);
                System.out.println("fileName >> "+fileName);
                // lazy listen for ALL records with the listener shown above
                req.addListenerForAllRecords(eventHandler);
                // create our event factory
                HSSFEventFactory factory = new HSSFEventFactory();
                // process our events based on the document input stream
                factory.processEvents(req, din);
                
               // for(int i =0;i<myWorkBook.getNumberOfSheets();i++){
               // int index = myWorkBook.getActiveSheetIndex();
                //HSSFSheet mySheet = myWorkBook.getSheetAt(index);
                System.out.println("formType >>>>>>>>>>>>>>> "+formType);
                HSSFSheet mySheet = myWorkBook.getSheet(formType);
                if(mySheet != null){
                System.out.println(" mySheet >> "+mySheet.getSheetName());
               // if(mySheet.getSheetName().equals("Form") || mySheet.getSheetName().equals("Award") || mySheet.getSheetName().equals("EOI")){
                    Set secKeys = null;
                   if(eventHandler.getFileName().equals("Pre-quaificatonForm_1.xls")){
                         secKeys = prequaFrmShtCells.keySet();
                   }else if(eventHandler.getFileName().equals("TenderForm_2.xls")){
                         secKeys = tenderFrmShtCells.keySet();
                   }else if(eventHandler.getFileName().equals("Awards_5.xls")){
                         secKeys = awardFrmShtCells.keySet();
                   }else if(eventHandler.getFileName().equals("EOI_3.xls")){
                         secKeys = eoiFrmShtCells.keySet();
                   }
                   
                    Iterator it = secKeys.iterator();
                    while(it.hasNext()){
                       String str = (String) it.next();
                       if(str.contains("~")){
                        String[] rc = str.split("~");
                        HSSFRow row = mySheet.getRow(Integer.parseInt(rc[0]));
                        if(row != null){
                            HSSFCell  cell =row.getCell(Integer.parseInt(rc[1]));
                            //System.out.println(" str >> "+str+" cellType >> "+cell.getCellType());
                                if(cell != null){
                                    if(eventHandler.getFileName().equals("Pre-quaificatonForm_1.xls")){
                                           if(cell.getCellType() == 1){
                                                System.out.println("name >> "+prequaFrmShtCells.get(str)+" cell string value >> "+cell.getStringCellValue());
                                                if(cell.getStringCellValue() != null && !cell.getStringCellValue().equals("*")){
                                                     eventHandler.getPreqData().put(prequaFrmShtCells.get(str), cell.getStringCellValue());
                                                 }
                                            }else if(cell.getCellType() == 0){
                                                System.out.println("name >> "+prequaFrmShtCells.get(str)+" cell string value >> "+cell.getNumericCellValue());
                                               // int db = (int) Math.round(cell.getNumericCellValue()) ;

                                                eventHandler.getPreqData().put(prequaFrmShtCells.get(str), String.valueOf(cell.getNumericCellValue()));
                                            }
                                       }else if(eventHandler.getFileName().equals("TenderForm_2.xls")){
                                           if(cell.getCellType() == 1){
                                               // System.out.println(" cell string value >> "+cell.getStringCellValue());
                                                if(cell.getStringCellValue() != null && !cell.getStringCellValue().equals("*")){
                                                     eventHandler.getTenderformData().put(tenderFrmShtCells.get(str), cell.getStringCellValue());
                                                 }
                                                }else if(cell.getCellType() == 0){
                                                   
                                                   // Double db = cell.getNumericCellValue();
                                                     //int db = (int) Math.round(cell.getNumericCellValue()) ;

                                                    eventHandler.getTenderformData().put(tenderFrmShtCells.get(str), String.valueOf(cell.getNumericCellValue()));
                                                }
                                       }else if(eventHandler.getFileName().equals("Awards_5.xls")){
                                           if(cell.getCellType() == 1){
                                               // System.out.println(" cell string value >> "+cell.getStringCellValue());
                                                if(cell.getStringCellValue() != null && !cell.getStringCellValue().equals("*")){
                                                     eventHandler.getContractAwarData().put(awardFrmShtCells.get(str), cell.getStringCellValue());
                                                 }
                                                }else if(cell.getCellType() == 0){
                                                    //System.out.println(" cell string value >> "+cell.getNumericCellValue());
                                                     // int db = (int) Math.round(cell.getNumericCellValue()) ;
                                                    eventHandler.getContractAwarData().put(awardFrmShtCells.get(str), String.valueOf(cell.getNumericCellValue()));
                                                }
                                       }else if(eventHandler.getFileName().equals("EOI_3.xls")){
                                           if(cell.getCellType() == 1){
                                           // System.out.println(" cell string value >> "+cell.getStringCellValue());
                                            if(cell.getStringCellValue() != null && !cell.getStringCellValue().equals("*")){
                                                 eventHandler.getEoiData().put(eoiFrmShtCells.get(str), cell.getStringCellValue());
                                             }
                                            }else if(cell.getCellType() == 0){
                                                //System.out.println(" cell string value >> "+cell.getNumericCellValue());
                                               int db = (int) Math.round(cell.getNumericCellValue()) ;
                                                eventHandler.getEoiData().put(eoiFrmShtCells.get(str), String.valueOf(cell.getNumericCellValue()));
                                            }
                                       }
                                 
                                }
                           }
                       }
                    }
                //  }
                }
              // }
                System.out.println(" data size >> "+eventHandler.getPreqData().get("IdentificationOfLot_1"));
                // attach time to date field
                 if(eventHandler.getFileName().equals("Pre-quaificatonForm_1.xls")){
                    if(eventHandler.getPreqData().get("PrequalificationClosingDateandTime") != null){
                        String prqclosingDateandTime = eventHandler.getPreqData().get("PrequalificationClosingDateandTime")+" "+eventHandler.getPreqTimeData().get("PrequalificationClosingHrs")+":"+eventHandler.getPreqTimeData().get("PrequalificationClosingMms")+" "+eventHandler.getPreqTimeData().get("PrequalificationClosingAmorPm");
                        System.out.println(" prqclosingDateandTime >> "+prqclosingDateandTime);
                        eventHandler.getPreqData().put("PrequalificationClosingDateandTime", convertDate(prqclosingDateandTime));
                     }
                    if(eventHandler.getPreqData().get("DateTimeofPrequalificationMeeting") != null){
                        String prqualificationMeetingTime = eventHandler.getPreqData().get("DateTimeofPrequalificationMeeting")+" "+eventHandler.getPreqTimeData().get("PrequalificationMeetingHrs")+":"+eventHandler.getPreqTimeData().get("PrequalificationMeetingMms")+" "+eventHandler.getPreqTimeData().get("PrequalificationMeetingAmorPm");
                        System.out.println(" prqPequalificationMeetingTime >> "+prqualificationMeetingTime);
                        eventHandler.getPreqData().put("DateTimeofPrequalificationMeeting", convertDate(prqualificationMeetingTime));
                     }
                    prqexbean = getPrqExcellBean(eventHandler.getPreqData());
                  }else if(eventHandler.getFileName().equals("TenderForm_2.xls")){

                    if(eventHandler.getTenderformData().get("TenderClosingDate") != null){
                        String tenClosingDateandTime = eventHandler.getTenderformData().get("TenderClosingDate")+" "+eventHandler.getTenderTimeData().get("TenderClosingHrs")+":"+eventHandler.getTenderTimeData().get("TenderClosingMins")+" "+eventHandler.getTenderTimeData().get("TenderClosingAmorPm");
                        System.out.println(" tenClosingDateandTime >> "+tenClosingDateandTime);
                        eventHandler.getTenderformData().put("TenderClosingDate", convertDate(tenClosingDateandTime));
                     }
                    if(eventHandler.getTenderformData().get("TenderOpeningDate") != null){
                        String tenOpeningDateTime = eventHandler.getTenderformData().get("TenderOpeningDate")+" "+eventHandler.getTenderTimeData().get("TenderOpeningHrs")+":"+eventHandler.getTenderTimeData().get("TenderOpeningMins")+" "+eventHandler.getTenderTimeData().get("TenderOpeningAmorPm");
                        System.out.println(" tenOpeningDateTime >> "+tenOpeningDateTime);
                        eventHandler.getTenderformData().put("TenderOpeningDate", convertDate(tenOpeningDateTime));
                     }
                    if(eventHandler.getTenderformData().get("DateTimeofTenderMeeting") != null ){
                        String dateTimeofTenderMeeting = eventHandler.getTenderformData().get("DateTimeofTenderMeeting")+" "+eventHandler.getTenderTimeData().get("TenderMeetingHrs")+":"+eventHandler.getTenderTimeData().get("TenderMeetingMms")+" "+eventHandler.getTenderTimeData().get("TenderMeetingAmorPm");
                        System.out.println(" dateTimeofTenderMeeting >> "+dateTimeofTenderMeeting);
                        eventHandler.getTenderformData().put("DateTimeofTenderMeeting", convertDate(dateTimeofTenderMeeting));
                     }
                    
                    
                    tenderFormExcellBean = getTenExcellBean(eventHandler.getTenderformData());
                  }else if(eventHandler.getFileName().equals("Awards_5.xls")){
                     awardExcellBean = getAwardExcellBean(eventHandler.getContractAwarData());
                  }else if(eventHandler.getFileName().equals("EOI_3.xls")){
                      if(eventHandler.getEoiData().get("EoiClosingDateandtime") != null){
                        String eoiClosingDateandTime = eventHandler.getEoiData().get("EoiClosingDateandtime")+" "+eventHandler.getEoiTimeData().get("EOIClosingtimeHrs")+":"+eventHandler.getEoiTimeData().get("EOIClosingTimeMns")+" "+eventHandler.getEoiTimeData().get("EOIClosingTimeAmorPm");
                        System.out.println(" eoiClosingDateandTime >> "+eoiClosingDateandTime);
                        if(eoiClosingDateandTime.contains("."))
                        {
                            int length = eoiClosingDateandTime.indexOf(".");
                            char prev = eoiClosingDateandTime.charAt(length-1);
                             System.out.println(" prev >> "+prev);
                            eoiClosingDateandTime = eoiClosingDateandTime.replace(prev,'0');
                             System.out.println(" eoiClosingDateandTime >> "+eoiClosingDateandTime);
                            eoiClosingDateandTime = eoiClosingDateandTime.replace('.', prev);
                             System.out.println(" eoiClosingDateandTime >> "+eoiClosingDateandTime);

                        }
                        System.out.println(" eoiClosingDateandTime >> "+eoiClosingDateandTime);
                        eventHandler.getEoiData().put("EoiClosingDateandtime", convertDate(eoiClosingDateandTime));
                        eOIExcellBean = getEoiExcellBean(eventHandler.getEoiData());

                     }
                  }
                
                getPreqData().clear();
                getPreqTimeData().clear();
                getTenderformData().clear();
                getTenderTimeData().clear();
                getEoiData().clear();
                getEoiTimeData().clear();
                getContractAwarData().clear();
                din.close();
       }catch(Exception ex){
            getPreqData().clear();
           ex.printStackTrace();
       }

        if(eventHandler.getFileName().equals("Pre-quaificatonForm_1.xls")){
                 retObj =  prqexbean;
           }else if(eventHandler.getFileName().equals("TenderForm_2.xls")){
                 retObj = tenderFormExcellBean;
           }else if(eventHandler.getFileName().equals("Awards_5.xls")){
                 retObj = awardExcellBean;
           }else if(eventHandler.getFileName().equals("EOI_3.xls")){
                 retObj = eOIExcellBean;
           }
    return retObj;
   }

   public PrequalificationExcellBean getPrqExcellBean(Map<String,String> data){
       PrequalificationExcellBean excellBean = new PrequalificationExcellBean();
       try{
               // System.out.println(" data >> ::::::::::::::: "+data.size());
                Class cls = cls = Class.forName("com.cptu.egp.eps.web.offlinedata.PrequalificationExcellBean");;
                Object obj =  cls.newInstance();
                Class[] paramInt = new Class[1];
                paramInt[0] = String.class;
                if(data != null && data.size() > 0){
                   Set set =  data.keySet();
                   Iterator it = set.iterator();
                   while(it.hasNext()){
                      String fieldKey = (String)it.next();
                     // System.out.println("field key >> "+fieldKey);
                      String value = (String)data.get(fieldKey);
                      fieldKey = "set"+fieldKey;
                       Method method = cls.getDeclaredMethod(fieldKey, paramInt);
                       if(value == null || value.equals("<select>") || value.equals("</select>") || value.equals("*"))
                       {
                           method.invoke(obj,"");
                       }else{
                            method.invoke(obj, value);
                       }
                      // System.out.println(" values >>>  "+((ExcellBean)obj).getMinistryName());
                   }
                 }
                 excellBean = (PrequalificationExcellBean)obj;
                
       }catch(Exception ex){
        ex.printStackTrace();
       }
       return excellBean;
   }


   public TenderFormExcellBean getTenExcellBean(Map<String,String> data){
       TenderFormExcellBean tenderFormExcellBean = new TenderFormExcellBean();
       try{
               // System.out.println(" data >> ::::::::::::::: "+data.size());
                Class cls = cls = Class.forName("com.cptu.egp.eps.web.offlinedata.TenderFormExcellBean");;
                Object obj =  cls.newInstance();
                Class[] paramInt = new Class[1];
                paramInt[0] = String.class;
                if(data != null && data.size() > 0){
                   Set set =  data.keySet();
                   Iterator it = set.iterator();
                   while(it.hasNext()){
                      String fieldKey = (String)it.next();
                     // System.out.println("field key >> "+fieldKey);
                      String value = (String)data.get(fieldKey);
                      fieldKey = "set"+fieldKey;
                       Method method = cls.getDeclaredMethod(fieldKey, paramInt);
                       method.invoke(obj, CommonUtils.checkNull(value));
                      // System.out.println(" values >>>  "+((ExcellBean)obj).getMinistryName());
                   }
                 }
                 tenderFormExcellBean = (TenderFormExcellBean)obj;

       }catch(Exception ex){
        ex.printStackTrace();
       }
       return tenderFormExcellBean;
   }

      public EOIExcellBean getEoiExcellBean(Map<String,String> data){
       EOIExcellBean eOIExcellBean = new EOIExcellBean();
       try{
               // System.out.println(" data >> ::::::::::::::: "+data.size());
                Class cls = cls = Class.forName("com.cptu.egp.eps.web.offlinedata.EOIExcellBean");;
                Object obj =  cls.newInstance();
                Class[] paramInt = new Class[1];
                paramInt[0] = String.class;
                if(data != null && data.size() > 0){
                   Set set =  data.keySet();
                   Iterator it = set.iterator();
                   while(it.hasNext()){
                      String fieldKey = (String)it.next();
                     // System.out.println("field key >> "+fieldKey);
                      String value = (String)data.get(fieldKey);
                      fieldKey = "set"+fieldKey;
                       Method method = cls.getDeclaredMethod(fieldKey, paramInt);
                       method.invoke(obj, value);
                      // System.out.println(" values >>>  "+((ExcellBean)obj).getMinistryName());
                   }
                 }
                 eOIExcellBean = (EOIExcellBean)obj;

       }catch(Exception ex){
        ex.printStackTrace();
       }
       return eOIExcellBean;
   }

       public ContractAwardExcellBean getAwardExcellBean(Map<String,String> data){
       ContractAwardExcellBean contractAwardExcellBean = new ContractAwardExcellBean();
       try{
               // System.out.println(" data >> ::::::::::::::: "+data.size());
                Class cls = cls = Class.forName("com.cptu.egp.eps.web.offlinedata.ContractAwardExcellBean");;
                Object obj =  cls.newInstance();
                Class[] paramInt = new Class[1];
                paramInt[0] = String.class;
                if(data != null && data.size() > 0){
                   Set set =  data.keySet();
                   Iterator it = set.iterator();
                   while(it.hasNext()){
                      String fieldKey = (String)it.next();
                     // System.out.println("field key >> "+fieldKey);
                      String value = (String)data.get(fieldKey);
                      fieldKey = "set"+fieldKey;
                       Method method = cls.getDeclaredMethod(fieldKey, paramInt);
                       method.invoke(obj, value);
                      // System.out.println(" values >>>  "+((ExcellBean)obj).getMinistryName());
                   }
                 }
                 contractAwardExcellBean = (ContractAwardExcellBean)obj;

       }catch(Exception ex){
        ex.printStackTrace();
       }
       return contractAwardExcellBean;
   }

       public String convertDate(String dbdate){
       String retDate = null;
       SimpleDateFormat displayFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
       SimpleDateFormat parseFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a");

       Date date;
           try {
                 System.out.println(" dbdate >> "+dbdate);
                  date = parseFormat.parse(dbdate);
                  retDate =  displayFormat.format(date);
                  System.out.println(" retDate >> "+retDate);
            } catch (ParseException e) {
                    e.printStackTrace();
            }
       return retDate;
       }

    /**
     * @return the preqData
     */
    public Map<String, String> getPreqData() {
        return preqData;
    }

    /**
     * @param preqData the preqData to set
     */
    public void setPreqData(Map<String, String> preqData) {
        this.preqData = preqData;
    }

    /**
     * @return the preqTimeData
     */
    public Map<String, String> getPreqTimeData() {
        return preqTimeData;
    }

    /**
     * @param preqTimeData the preqTimeData to set
     */
    public void setPreqTimeData(Map<String, String> preqTimeData) {
        this.preqTimeData = preqTimeData;
    }

    /**
     * @return the tenderformData
     */
    public Map<String, String> getTenderformData() {
        return tenderformData;
    }

    /**
     * @param tenderformData the tenderformData to set
     */
    public void setTenderformData(Map<String, String> tenderformData) {
        this.tenderformData = tenderformData;
    }

    /**
     * @return the contractAwarData
     */
    public Map<String, String> getContractAwarData() {
        return contractAwarData;
    }

    /**
     * @param contractAwarData the contractAwarData to set
     */
    public void setContractAwarData(Map<String, String> contractAwarData) {
        this.contractAwarData = contractAwarData;
    }

    /**
     * @return the eoiData
     */
    public Map<String, String> getEoiData() {
        return eoiData;
    }

    /**
     * @param eoiData the eoiData to set
     */
    public void setEoiData(Map<String, String> eoiData) {
        this.eoiData = eoiData;
    }

    /**
     * @return the eoiTimeData
     */
    public Map<String, String> getEoiTimeData() {
        return eoiTimeData;
    }

    /**
     * @param eoiTimeData the eoiTimeData to set
     */
    public void setEoiTimeData(Map<String, String> eoiTimeData) {
        this.eoiTimeData = eoiTimeData;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return the tenderTimeData
     */
    public Map<String, String> getTenderTimeData() {
        return tenderTimeData;
    }

    /**
     * @param tenderTimeData the tenderTimeData to set
     */
    public void setTenderTimeData(Map<String, String> tenderTimeData) {
        this.tenderTimeData = tenderTimeData;
    }

   
   
}
