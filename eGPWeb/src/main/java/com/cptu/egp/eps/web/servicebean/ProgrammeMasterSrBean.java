/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblProgrammeMaster;
import com.cptu.egp.eps.service.serviceinterface.ProgrammeMasterService;
import com.cptu.egp.eps.web.databean.ProgrammeMasterDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Naresh.Akurathi
 */
public class ProgrammeMasterSrBean {

    private static final Logger LOGGER = Logger.getLogger(ProgrammeMasterSrBean.class);
    private final ProgrammeMasterService prgMasterService = (ProgrammeMasterService) AppContext.getSpringBean("programmeMasterService");
    private final TblProgrammeMaster tblProgrammeMaster = new TblProgrammeMaster();
    private String logUserId = "0";

    /**
     * For Logging Purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        prgMasterService.setUserId(logUserId);
        this.logUserId = logUserId;
    }
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        prgMasterService.setAuditTrail(auditTrail);
    }
    
    
    /**
     * Method for adding the ProgrammeMaster Details.
     * @param prgMasterDtBean
     * @return
     */
    public String addProgrammeMaster(ProgrammeMasterDtBean prgMasterDtBean) {
        LOGGER.debug("addProgrammeMaster : " + logUserId + " Starts");
       tblProgrammeMaster.setProgCode(prgMasterDtBean.getProgCode().trim());
       tblProgrammeMaster.setProgName(prgMasterDtBean.getProgName().trim());
       tblProgrammeMaster.setProgOwner(prgMasterDtBean.getProgOwner().trim());
       String msg = null;
        try {
            msg = prgMasterService.addProgrammeMasterDetails(tblProgrammeMaster); //calling Service Layer method.
        } catch (Exception e) {
            LOGGER.error("addProgrammeMaster : " + logUserId + " : " + e);
        }
        LOGGER.debug("addProgrammeMaster : " + logUserId + " Ends");
           return msg;
       }

   /**
    * Method for Updating ProgrammeMaster details.
    * @param prgMasterDtBean
     * @param id
     * @return  
    */
    public String updateProgrammeMaster(ProgrammeMasterDtBean prgMasterDtBean, int id) {
        LOGGER.debug("updateProgrammeMaster : " + logUserId + " Starts");
        String msg = "";
        try
        {
       tblProgrammeMaster.setProgId(id);
       tblProgrammeMaster.setProgCode(prgMasterDtBean.getProgCode().trim());
       tblProgrammeMaster.setProgName(prgMasterDtBean.getProgName().trim());
       tblProgrammeMaster.setProgOwner(prgMasterDtBean.getProgOwner().trim());
        msg = prgMasterService.updateProgrammeMasterDetails(tblProgrammeMaster); //calling Service Layer method.
        }catch(Exception e)
        {
            LOGGER.error("updateProgrammeMaster : " + logUserId + " : "+e);
   }
        LOGGER.debug("updateProgrammeMaster : " + logUserId + " Ends");
        return msg;
    }

   /**
    * Method for getting the all table details of ProgrammeMaster.
     * @param sord 
     * @param sidx 
     * @return
    */
    public List<TblProgrammeMaster> getAllProgrammeMaster(String sord,String sidx) {
        LOGGER.debug("getAllProgrammeMaster : " + logUserId + " Starts");
        List<TblProgrammeMaster> prgMasterList = null;
        try {
            prgMasterList = prgMasterService.getProgrammeMasterDetails(sord,sidx); //calling Service Layer method.
        } catch (Exception e) {
            LOGGER.error("getAllProgrammeMaster : " + logUserId + " : " + e);
        }
        LOGGER.debug("getAllProgrammeMaster : " + logUserId + " Ends");
                return prgMasterList;
       }

   /**
    * Method for get the details of particular ProgrammeMaster.
    * @param id
    * @return
    */
    public List<TblProgrammeMaster> getData(int id) {
        LOGGER.debug("getData : " + logUserId + " Starts");
        List<TblProgrammeMaster> list = null;
        try {
            list = prgMasterService.getData(id); //calling Service Layer method.
        } catch (Exception e) {
            LOGGER.error("getData : " + logUserId + " : " + e);
        }
        LOGGER.debug("getData : " + logUserId + " Ends");
           return list;
       }

    /**
     * Get ProgrammeMaster using pagging
     * @param fieldName
     * @param operator
     * @param searchString
     * @return list of tbl_programme Master
     */
    public List<TblProgrammeMaster> getProgrammeMasterBySearch(String fieldName, String operator, String searchString) {
        LOGGER.debug("getProgrammeMasterBySearch : " + logUserId + " Starts");
        List<TblProgrammeMaster> list = null;
        try {
            list = prgMasterService.getProgrammeMasterBySearch(fieldName, operator, searchString);
        } catch (Exception e) {
            LOGGER.error("getProgrammeMasterBySearch : " + logUserId + " : " + e);
    }
        LOGGER.debug("getProgrammeMasterBySearch : " + logUserId + " Ends");
        return list;
    }
}
