/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.utility;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.service.serviceimpl.TenderCorriService;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.service.serviceinterface.TenderBidService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DateFormat;

/**
 *
 * @author Administrator
 */
public class CommonScheduledTask
{
    private static final Logger LOGGER = Logger.getLogger(CommonScheduledTask.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND  = "Ends";
    /**
     * Notify Users for Registration Renewal
     * It sends an email at 2:00 AM daily.
     */
    public void sendRegRenewalMail()
    {
        LOGGER.debug("sendRegRenewalMail : "+LOGGERSTART);
        List <SPCommonSearchData> recordCron = null;
        
        CommonSearchService cmnSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
        recordCron = cmnSearchService.searchData("getCronJob", "Notify for Registration Renewal", null, null, null, null, null, null, null, null);

        if(recordCron.isEmpty())
        {
            CommonXMLSPService  xmlservice = (CommonXMLSPService)AppContext.getSpringBean("CommonXMLSPService");
            UserRegisterService  service = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
            
            Date curDate = new Date();
            Date newDate = new Date(curDate.getYear(), curDate.getMonth()+1, curDate.getDate());

            int cronStatus = 1;
            String cronMsg = "Successful Executed";
            String cronName = "Notify for Registration Renewal";
            String  cronStartTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());

            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
            String strnewDate = sd.format(newDate);

            List <SPCommonSearchData> spcommonSearchData = null;
            List <SPCommonSearchData> spcommonSearchDataLaps = null;
            spcommonSearchData = cmnSearchService.searchData("getExpiredRegCustList", strnewDate, null, null, null, null, null, null, null, null);
            spcommonSearchDataLaps = cmnSearchService.searchData("getLapsedRegCustList", sd.format(curDate), null, null, null, null, null, null, null, null);

            try{
                if(!spcommonSearchData.isEmpty())
                {
                    for(SPCommonSearchData userData : spcommonSearchData)
                    {
                        String mails[]={userData.getFieldName1()};
                        SendMessageUtil sendMessageUtil = new SendMessageUtil();
                        MailContentUtility mailContentUtility = new MailContentUtility();
                        MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                        service.contentAdmMsgBox(mails[0], XMLReader.getMessage("emailIdHelp"), "e-GP System: Your Account Renewal", msgBoxContentUtility.regRenewalContent(strnewDate));

                        String mailText = mailContentUtility.regRenewalContent(strnewDate);
                        sendMessageUtil.setEmailTo(mails);
                        sendMessageUtil.setEmailSub("e-GP System: Your Account Renewal");
                        sendMessageUtil.setEmailMessage(mailText);
                        sendMessageUtil.sendEmail();

                        mails=null;
                        sendMessageUtil=null;
                        mailContentUtility=null;
                        msgBoxContentUtility=null;
                    }
                 }
                if(!spcommonSearchDataLaps.isEmpty())
                {
                    for(SPCommonSearchData userDataLapse : spcommonSearchDataLaps)
                    {
                        String mails[]={userDataLapse.getFieldName1()};
                        SendMessageUtil sendMessageUtil = new SendMessageUtil();
                        MailContentUtility mailContentUtility = new MailContentUtility();
                        MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                        service.contentAdmMsgBox(mails[0], XMLReader.getMessage("emailIdHelp"), "e-GP System: Your Account Renewal Period has been Lapsed", msgBoxContentUtility.regRenewalContent(strnewDate));

                        String mailText = mailContentUtility.regRenewalLapsContent(strnewDate);
                        sendMessageUtil.setEmailTo(mails);
                        sendMessageUtil.setEmailSub("e-GP System: Your Account Renewal Period has been Lapsed");
                        sendMessageUtil.setEmailMessage(mailText);
                        sendMessageUtil.sendEmail();

                        mails=null;
                        sendMessageUtil=null;
                        mailContentUtility=null;
                        msgBoxContentUtility=null;
                    }
                }
            }catch(Exception ex){
                cronMsg = ex.toString();
                cronStatus = 0;
                AppExceptionHandler expHandler = new AppExceptionHandler();
                expHandler.stack2string(ex);
            }

            String  cronEndTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());

            String xml = "<root>"
                            + "<tbl_CronJobs "
                                + "cronJobName=\""+cronName+"\" "
                                + "cronJobStartTime=\""+cronStartTime+"\" "
                                + "cronJobEndTime=\""+cronEndTime+"\" "
                                + "cronJobStatus=\""+cronStatus+"\" "
                                + "cronJobMsg=\""+cronMsg+"\"/>"
                        + "</root>";
            try {
                xmlservice.insertDataBySP("insert", "tbl_CronJobs", xml, "");
            } catch (Exception ex) {
                 LOGGER.error("sendRegRenewalMail : " + ex.toString());
                AppExceptionHandler expHandler = new AppExceptionHandler();
                expHandler.stack2string(ex);
            }
        }
        LOGGER.debug("sendRegRenewalMail : "+ LOGGEREND);
    }

    /**
     * send SMS to Approvers
     * It sends an email at 11:00 AM daily.
     */
     public void sendSMSToApprovers()
     {
        LOGGER.debug("sendSMSToApprovers : "+LOGGERSTART);
        List <SPCommonSearchData> recordCron = null;

        CommonSearchService cmnSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
        recordCron = cmnSearchService.searchData("getCronJob", "SMS To Approvers", null, null, null, null, null, null, null, null);

        if(recordCron.isEmpty())
        {
            int cronStatus = 1;
            String cronMsg = "Successful Executed";
            String cronName = "SMS To Approvers";
            String  cronStartTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());
            
            CommonXMLSPService  xmlservice = (CommonXMLSPService)AppContext.getSpringBean("CommonXMLSPService");
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            List <SPCommonSearchData> spcommonSearchData = null;

            try{
                spcommonSearchData = cmnSearchService.searchData("getMobNoforSMS", null, null, null, null, null, null, null, null, null);
                for(SPCommonSearchData searchData :spcommonSearchData)
                {
                    sendMessageUtil.setSmsNo(searchData.getFieldName1());
                    sendMessageUtil.setSmsBody("Dear User, Following file is awaiting your attention: "+
                                               "Module : "+searchData.getFieldName2()+
                                        " Process: "+searchData.getFieldName3()+
                                        " ID: "+searchData.getFieldName4()+",eGP System");
                    
                    sendMessageUtil.sendSMS();
                }
            }catch(Exception ex){
                cronMsg = ex.toString();
                cronStatus = 0;
                AppExceptionHandler expHandler = new AppExceptionHandler();
                expHandler.stack2string(ex);
            }

            String  cronEndTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());

            String xml = "<root>"
                            + "<tbl_CronJobs "
                                + "cronJobName=\""+cronName+"\" "
                                + "cronJobStartTime=\""+cronStartTime+"\" "
                                + "cronJobEndTime=\""+cronEndTime+"\" "
                                + "cronJobStatus=\""+cronStatus+"\" "
                                + "cronJobMsg=\""+cronMsg+"\"/>"
                        + "</root>";
            try {
                xmlservice.insertDataBySP("insert", "tbl_CronJobs", xml, "");
            } catch (Exception ex) {
                LOGGER.error("sendSMSToApprovers : " + ex.toString());
                AppExceptionHandler expHandler = new AppExceptionHandler();
                expHandler.stack2string(ex);
            }
         }
        LOGGER.debug("sendSMSToApprovers : "+ LOGGEREND);
     }

    /**
     * send SMS-Email-MsgBox for Pre Tender Meeting
     * It sends an email at 11:00 AM daily.
     */
     public void smsPreTenderMeeting()
     {
        LOGGER.debug("smsPreTenderMeeting : "+LOGGERSTART);
        CommonSearchService cmnSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
        List <SPCommonSearchData> recordCron = null;
        UserRegisterService  service = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");

        recordCron = cmnSearchService.searchData("getCronJob", "SMS For Pre Tender Meeting", null, null, null, null, null, null, null, null);

         if(recordCron.isEmpty())
         {
            int cronStatus = 1;
            String cronMsg = "Successful Executed";
            String cronName = "SMS For Pre Tender Meeting";
            String  cronStartTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());
            CommonXMLSPService  xmlservice = (CommonXMLSPService)AppContext.getSpringBean("CommonXMLSPService");

            try{
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                List <SPCommonSearchData> spcommonSearchData = null;
                spcommonSearchData = cmnSearchService.searchData("getPrebidEmpMobNo", null, null, null, null, null, null, null, null, null);

                for(SPCommonSearchData searchData :spcommonSearchData)
                {
                    String mails[]={searchData.getFieldName3()};
                    String tenderId = searchData.getFieldName2();
                    String refNo = searchData.getFieldName4();
                    String repTime = searchData.getFieldName5();
                    String mobileNo = searchData.getFieldName2();
                    MailContentUtility mailContentUtility = new MailContentUtility();
                    MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                    service.contentAdmMsgBox(mails[0], XMLReader.getMessage("emailIdHelp"), "e-GP: Pre-Tender Meeting reminder", msgBoxContentUtility.getPreBidMeetingContent(tenderId,refNo,repTime));

                    String mailText = mailContentUtility.getPreBidMeetingContent(tenderId,refNo,repTime);
                    sendMessageUtil.setEmailTo(mails);
                    sendMessageUtil.setEmailSub("e-GP: Pre-Tender Meeting reminder");
                    sendMessageUtil.setEmailMessage(mailText);
                    sendMessageUtil.sendEmail();

                    mails=null;
               //     sendMessageUtil=null;
                    mailContentUtility=null;
                    msgBoxContentUtility=null;

                    if(!"+975".equals(mobileNo)){
                    sendMessageUtil.setSmsNo(searchData.getFieldName1());
                    sendMessageUtil.setSmsBody("Dear User, Information on Pre - Tender Meeting : Date and time: "+repTime+" Tender ID: "+tenderId+" GPPMD Office");
                    sendMessageUtil.sendSMS();
                }
                }
            }catch(Exception ex){
                cronMsg = ex.toString();
                cronStatus = 0;
                AppExceptionHandler expHandler = new AppExceptionHandler();
                expHandler.stack2string(ex);
            }

            String  cronEndTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());

            String xml = "<root>"
                            + "<tbl_CronJobs "
                                + "cronJobName=\""+cronName+"\" "
                                + "cronJobStartTime=\""+cronStartTime+"\" "
                                + "cronJobEndTime=\""+cronEndTime+"\" "
                                + "cronJobStatus=\""+cronStatus+"\" "
                                + "cronJobMsg=\""+cronMsg+"\"/>"
                        + "</root>";
            try{
                xmlservice.insertDataBySP("insert", "tbl_CronJobs", xml, "");
            } catch (Exception ex) {
                LOGGER.error("smsPreTenderMeeting : " + ex.toString());
                AppExceptionHandler expHandler = new AppExceptionHandler();
                expHandler.stack2string(ex);
            }
         }
        LOGGER.debug("smsPreTenderMeeting : "+LOGGEREND);
    }

     /**
     * send SMS-Email-MsgBox for Tender Opening
     * It sends an email at 3:00 AM daily.
     */
     public void sendIntimationOpenCom(){

        LOGGER.debug("sendIntimationOpenCom : "+LOGGERSTART);
        CommonSearchService cmnSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
        List <SPCommonSearchData> recordCron = null;
        UserRegisterService  service = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");

        recordCron = cmnSearchService.searchData("getCronJob", "Intimation for Tender Opening", null, null, null, null, null, null, null, null);

        if(recordCron.isEmpty())
        {
            int cronStatus = 1;
            String cronMsg = "Successful Executed";
            String cronName = "Intimation for Tender Opening";
            String  cronStartTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());
            CommonXMLSPService  xmlservice = (CommonXMLSPService)AppContext.getSpringBean("CommonXMLSPService");

            try{
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                List <SPCommonSearchData> spcommonSearchData = null;
                spcommonSearchData = cmnSearchService.searchData("getTenderOpenEmpMobNo", null, null, null, null, null, null, null, null, null);

                for(SPCommonSearchData searchData :spcommonSearchData)
                {
                    String mails[]={searchData.getFieldName3()};
                    String tenderId = searchData.getFieldName2();
                    String refNo = searchData.getFieldName4();
                    String repTime = searchData.getFieldName5();
                    String mobileNo = searchData.getFieldName1();
                    MailContentUtility mailContentUtility = new MailContentUtility();
                    MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                    service.contentAdmMsgBox(mails[0], XMLReader.getMessage("emailIdNoReply"), "e-GP: Tender Opening reminder", msgBoxContentUtility.getIntTenderOpenContent(tenderId,refNo,repTime));

                    String mailText = mailContentUtility.getIntTenderOpenContent(tenderId,refNo,repTime);
                    sendMessageUtil.setEmailTo(mails);
                    sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                    sendMessageUtil.setEmailSub("e-GP: Tender Opening reminder");
                    sendMessageUtil.setEmailMessage(mailText);
                    sendMessageUtil.sendEmail();

                    if(!"+975".equals(mobileNo)){
                        sendMessageUtil.setSmsNo(mobileNo);
                        sendMessageUtil.setSmsBody("Dear User, Tender Opening is scheduled tomorrow for below mentioned tender: Tender ID: "+tenderId+" Please logon to e-GP System for more details. GPPMD Office");
                        sendMessageUtil.sendSMS();
                    }
                }
            }catch(Exception ex){
                cronMsg = ex.toString();
                cronStatus = 0;
                AppExceptionHandler expHandler = new AppExceptionHandler();
                expHandler.stack2string(ex);
            }
            String  cronEndTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());
            
            String xml = "<root>"
                            + "<tbl_CronJobs "
                                + "cronJobName=\""+cronName+"\" "
                                + "cronJobStartTime=\""+cronStartTime+"\" "
                                + "cronJobEndTime=\""+cronEndTime+"\" "
                                + "cronJobStatus=\""+cronStatus+"\" "
                                + "cronJobMsg=\""+cronMsg+"\"/>"
                        + "</root>";
            try{
                xmlservice.insertDataBySP("insert", "tbl_CronJobs", xml, "");
            } catch (Exception ex) {
                LOGGER.error("sendIntimationOpenCom : " + ex.toString());
                AppExceptionHandler expHandler = new AppExceptionHandler();
                expHandler.stack2string(ex);
            }
        }
        LOGGER.debug("sendIntimationOpenCom : "+LOGGEREND);
     }

      /**
     * send SMS-Email-MsgBox for Tender Evaluation
     * It sends an email at 3:00 AM daily.
     */
     public void sendEvaluation(){

        LOGGER.debug("sendEvaluation : "+LOGGERSTART);
        CommonSearchService cmnSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
        List <SPCommonSearchData> recordCron = null;
        UserRegisterService  service = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");

        recordCron = cmnSearchService.searchData("getCronJob", "Tender Evaluation", null, null, null, null, null, null, null, null);

        if(recordCron.isEmpty())
        {
            int cronStatus = 1;
            String cronMsg = "Successful Executed";
            String cronName = "Tender Evaluation";
            String  cronStartTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());
            CommonXMLSPService  xmlservice = (CommonXMLSPService)AppContext.getSpringBean("CommonXMLSPService");

            try{
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                List <SPCommonSearchData> spcommonSearchData = null;
                spcommonSearchData = cmnSearchService.searchData("getTenderEvalEmpMobNo", null, null, null, null, null, null, null, null, null);

                for(SPCommonSearchData searchData :spcommonSearchData)
                {
                    String mails[]={searchData.getFieldName3()};
                    String tenderId = searchData.getFieldName2();
                    String refNo = searchData.getFieldName4();
                    String repTime = searchData.getFieldName5();
                    String mobileNo = searchData.getFieldName1();
                    MailContentUtility mailContentUtility = new MailContentUtility();
                    MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                    service.contentAdmMsgBox(mails[0], XMLReader.getMessage("emailIdHelp"), "e-GP: Tender Evaluation reminder", msgBoxContentUtility.getIntTenderEvalContent(tenderId,refNo,repTime));

                    String mailText = mailContentUtility.getIntTenderEvalContent(tenderId,refNo,repTime);
                    sendMessageUtil.setEmailTo(mails);
                    sendMessageUtil.setEmailSub("e-GP: Tender Evaluation reminder");
                    sendMessageUtil.setEmailMessage(mailText);
                    sendMessageUtil.sendEmail();                   

                    if(!"+975".equals(mobileNo)){
                        sendMessageUtil.setSmsNo(mobileNo);
                        sendMessageUtil.setSmsBody("Dear User, Tender Evaluation is scheduled tomorrow for below mentioned tender: Tender ID: "+tenderId+" Please logon to e-GP System for more details. GPPMD Office");
                        sendMessageUtil.sendSMS();
                    }
                }
            }catch(Exception ex){
                cronMsg = ex.toString();
                cronStatus = 0;
                AppExceptionHandler expHandler = new AppExceptionHandler();
                expHandler.stack2string(ex);
            }
            String  cronEndTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());

            String xml = "<root>"
                            + "<tbl_CronJobs "
                                + "cronJobName=\""+cronName+"\" "
                                + "cronJobStartTime=\""+cronStartTime+"\" "
                                + "cronJobEndTime=\""+cronEndTime+"\" "
                                + "cronJobStatus=\""+cronStatus+"\" "
                                + "cronJobMsg=\""+cronMsg+"\"/>"
                        + "</root>";
            try{
                xmlservice.insertDataBySP("insert", "tbl_CronJobs", xml, "");
            } catch (Exception ex) {
                 LOGGER.error("sendEvaluation : " + ex.toString());
                AppExceptionHandler expHandler = new AppExceptionHandler();
                expHandler.stack2string(ex);
            }
        }
        LOGGER.debug("sendEvaluation : "+LOGGEREND);
     }

     public boolean sendCancelTender(){
         LOGGER.debug("sendCancelTende : "+LOGGERSTART);
         boolean flag = false;
         try {
            CommonSearchService cmnSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            List <SPCommonSearchData> recordCron = null;
            List <SPCommonSearchData> tenderDetails = null;
            CommonXMLSPService  xmlservice = (CommonXMLSPService)AppContext.getSpringBean("CommonXMLSPService");
            UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");

            recordCron = cmnSearchService.searchData("getCronJob", "Cancel Tender", null, null, null, null, null, null, null, null);

            if(recordCron.isEmpty()){
                int cronStatus = 1;
                String cronMsg = "Successful Executed";
                String cronName = "Cancel Tender";
                String cronStartTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());
                TenderCorriService tenderCorriService = (TenderCorriService) AppContext.getSpringBean("TenderCorriService");
                //TenderService tenderService = (TenderService) AppContext.getSpringBean("TenderService");
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                for (SPCommonSearchData sPCommonSearchData : cmnSearchService.searchData("getCancelledTenders", null, null, null, null, null, null, null, null, null)) {
                    String[] tmp = tenderCorriService.getEmailSMSforCorri(sPCommonSearchData.getFieldName1());
                    String refNo = tmp[2];
                    String peName = tmp[3];
                    if (tmp[0] != null) {
                        String mails[] = tmp[0].split(",");
                        MailContentUtility mailContentUtility = new MailContentUtility();
                        MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                        String mailText = mailContentUtility.cancelTenderContent(sPCommonSearchData.getFieldName1(), refNo,peName);
                        String msgBox = msgBoxContentUtility.cancelTenderContent(sPCommonSearchData.getFieldName1(), refNo,peName);
                        sendMessageUtil.setEmailSub("e-GP: Cancellation of a Tender");
                        sendMessageUtil.setEmailMessage(mailText);
                        for (int i = 0; i < mails.length; i++) {
                            String mail[] = {mails[i]};
                            userRegisterService.contentAdmMsgBox(mails[i], XMLReader.getMessage("msgboxfrom"), sendMessageUtil.getEmailSub(), msgBox);
                            sendMessageUtil.setEmailTo(mail);
                            sendMessageUtil.sendEmail();
                            mail=null;
                        }
                        mailContentUtility = null;
                        msgBoxContentUtility = null;
                    }
                }
                String  cronEndTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());

                String xml = "<root>"
                                + "<tbl_CronJobs "
                                    + "cronJobName=\""+cronName+"\" "
                                    + "cronJobStartTime=\""+cronStartTime+"\" "
                                    + "cronJobEndTime=\""+cronEndTime+"\" "
                                    + "cronJobStatus=\""+cronStatus+"\" "
                                    + "cronJobMsg=\""+cronMsg+"\"/>"
                            + "</root>";
                try{
                    xmlservice.insertDataBySP("insert", "tbl_CronJobs", xml, "");
                } catch (Exception ex) {
                     LOGGER.error("sendEvaluation : " + ex.toString());
                    AppExceptionHandler expHandler = new AppExceptionHandler();
                    expHandler.stack2string(ex);
                }
            }
         } catch (Exception e) {
             LOGGER.error("sendCancelTende : "+LOGGEREND);
            AppExceptionHandler expHandler = new AppExceptionHandler();
            expHandler.stack2string(e);
         }
         LOGGER.debug("sendCancelTende : "+LOGGEREND);
         return flag;
     }
     public boolean NOA(){
         LOGGER.debug("NOA : "+LOGGERSTART);
         boolean flag = false;
         try {
            CommonSearchService cmnSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
            CommonXMLSPService  xmlservice = (CommonXMLSPService)AppContext.getSpringBean("CommonXMLSPService");

            List <SPCommonSearchData> recordCron = null;
            recordCron = cmnSearchService.searchData("getCronJob", "NOA Rejection", null, null, null, null, null, null, null, null);

            if(recordCron.isEmpty()){
                List<SPCommonSearchDataMore> NOAAcceptDt = commonSearchDataMoreService.geteGPData("chkNOAAcceptDt", null,null , null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                if(!NOAAcceptDt.isEmpty()){
                    int cronStatus = 1;
                    String cronMsg = "Successful Executed";
                    String cronName = "NOA Rejection";
                    String cronStartTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());
                    String cronEndTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());

                    String xml = "<root>"
                                    + "<tbl_CronJobs "
                                        + "cronJobName=\""+cronName+"\" "
                                        + "cronJobStartTime=\""+cronStartTime+"\" "
                                        + "cronJobEndTime=\""+cronEndTime+"\" "
                                        + "cronJobStatus=\""+cronStatus+"\" "
                                        + "cronJobMsg=\""+cronMsg+"\"/>"
                                + "</root>";
                    try{
                        xmlservice.insertDataBySP("insert", "tbl_CronJobs", xml, "");
                    } catch (Exception ex) {
                        AppExceptionHandler expHandler = new AppExceptionHandler();
                        expHandler.stack2string(ex);
                         LOGGER.error("NOA : " + ex.toString());
                    }
                }
            }
         } catch (Exception e) {
            AppExceptionHandler expHandler = new AppExceptionHandler();
            expHandler.stack2string(e);
             LOGGER.error("NOA : "+LOGGEREND);
         }
         LOGGER.debug("NOA : "+LOGGEREND);
         return flag;
     }
     
     public void sendPublishPkgMail()
     {
         LOGGER.debug("sendPublishPkgMail : "+LOGGERSTART);
         boolean flag = false;
         try {
            CommonSearchService cmnSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            List <SPCommonSearchData> recordCron = null;
            CommonXMLSPService  xmlservice = (CommonXMLSPService)AppContext.getSpringBean("CommonXMLSPService");

            recordCron = cmnSearchService.searchData("getCronJob", "Publish Package Email", null, null, null, null, null, null, null, null);

            if(recordCron.isEmpty())
            {
                int cronStatus = 1;
                String cronMsg = "Successful Executed";
                String cronName = "Publish Package Email";
                String cronStartTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());

                MailContentUtility mailContentUtility = new MailContentUtility();
                List<SPCommonSearchData> Pkglist = cmnSearchService.searchData("getAppEmailIdForPublishPKGCron", null, null, null, null, null, null, null, null, null);

                for (SPCommonSearchData sPCommonSearchData : Pkglist) 
                {
                    SendMessageUtil sendMessageUtil = new SendMessageUtil();
                    String[] mailTo = {sPCommonSearchData.getFieldName1()};

                    if(sPCommonSearchData.getFieldName2()!=null && sPCommonSearchData.getFieldName3()!=null && sPCommonSearchData.getFieldName4()!=null && sPCommonSearchData.getFieldName5()!=null)
                    {
                        List list = mailContentUtility.conAPPPublish(sPCommonSearchData.getFieldName2(), sPCommonSearchData.getFieldName3(),Integer.parseInt(sPCommonSearchData.getFieldName5()), Integer.parseInt(sPCommonSearchData.getFieldName4()));
                        String mailSub = list.get(0).toString();
                        String mailText = list.get(1).toString();
                        sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                        sendMessageUtil.setEmailTo(mailTo);
                        sendMessageUtil.setEmailSub(mailSub);
                        sendMessageUtil.setEmailMessage(mailText);
                        sendMessageUtil.sendEmail();
                    }
                }
     
                String  cronEndTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());

                String xml = "<root>"
                                + "<tbl_CronJobs "
                                    + "cronJobName=\""+cronName+"\" "
                                    + "cronJobStartTime=\""+cronStartTime+"\" "
                                    + "cronJobEndTime=\""+cronEndTime+"\" "
                                    + "cronJobStatus=\""+cronStatus+"\" "
                                    + "cronJobMsg=\""+cronMsg+"\"/>"
                            + "</root>";
                try{
                    xmlservice.insertDataBySP("insert", "tbl_CronJobs", xml, "");
                } catch (Exception ex) {
                    System.out.println(ex.toString());
                    AppExceptionHandler expHandler = new AppExceptionHandler();
                    expHandler.stack2string(ex);
                }
            }
         } catch (Exception e) {
             LOGGER.error("sendCancelTende : "+LOGGEREND);
            AppExceptionHandler expHandler = new AppExceptionHandler();
            expHandler.stack2string(e);
         }
         LOGGER.debug("sendCancelTende : "+LOGGEREND);
     }
     
     public void sendPublishTenderMail()
     {
         LOGGER.debug("sendPublishTenderMail : "+LOGGERSTART);
         boolean flag = false;
         try {
            CommonSearchService cmnSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            List <SPCommonSearchData> recordCron = null;
            CommonXMLSPService  xmlservice = (CommonXMLSPService)AppContext.getSpringBean("CommonXMLSPService");
           
            recordCron = cmnSearchService.searchData("getCronJob", "Publish Tender Email", null, null, null, null, null, null, null, null);

            if(recordCron.isEmpty())
            {
                int cronStatus = 1;
                String cronMsg = "Successful Executed";
                String cronName = "Publish Tender Email";
                String cronStartTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());

                MailContentUtility mailContentUtility = new MailContentUtility();
                List<SPCommonSearchData> Pkglist = cmnSearchService.searchData("getEmailIdForPublishTenderCron", null, null, null, null, null, null, null, null, null);

                for (SPCommonSearchData sPCommonSearchData : Pkglist) 
                {
                    SendMessageUtil sendMessageUtil = new SendMessageUtil();
                    String[] mailTo = {sPCommonSearchData.getFieldName1()};

                    if(sPCommonSearchData.getFieldName2()!=null && sPCommonSearchData.getFieldName3()!=null && sPCommonSearchData.getFieldName4()!=null && sPCommonSearchData.getFieldName5()!=null && sPCommonSearchData.getFieldName6()!=null && sPCommonSearchData.getFieldName7()!=null)
                    {
                        String mailText = "";
                        String mailSub = "";
                        String tenderCap = "Tender";
                        if("rfp".equalsIgnoreCase(sPCommonSearchData.getFieldName9()) && !"Open".equalsIgnoreCase(sPCommonSearchData.getFieldName10())){
                            tenderCap = "e - LEM";
                        }else if("REOI".equalsIgnoreCase(sPCommonSearchData.getFieldName9())){
                            tenderCap = "e - REOI";
                        }
                        if("rfq".equalsIgnoreCase(sPCommonSearchData.getFieldName7()) || "rfqu".equalsIgnoreCase(sPCommonSearchData.getFieldName7()) || "rfql".equalsIgnoreCase(sPCommonSearchData.getFieldName7())){
                           mailText = mailContentUtility.tenderPubRFQ(sPCommonSearchData.getFieldName2(), sPCommonSearchData.getFieldName3(),sPCommonSearchData.getFieldName6(), sPCommonSearchData.getFieldName4(),sPCommonSearchData.getFieldName5());
                           mailSub = "e-GP: e-LEM  notice of your interest"; 
                        }else if("rfp".equalsIgnoreCase(sPCommonSearchData.getFieldName9()) && !"Open".equalsIgnoreCase(sPCommonSearchData.getFieldName10())){
                            mailSub = "e-GP:  e-Letter of Invitation for e-Request for Proposal (e-RFP)";
                            mailText = mailContentUtility.tenderPubRFP(sPCommonSearchData.getFieldName2(), sPCommonSearchData.getFieldName3(), sPCommonSearchData.getFieldName6(), sPCommonSearchData.getFieldName4(), sPCommonSearchData.getFieldName5(), Pkglist);
                        }else{
                            mailText = mailContentUtility.tenderPub(sPCommonSearchData.getFieldName2(), sPCommonSearchData.getFieldName3(),sPCommonSearchData.getFieldName6(), sPCommonSearchData.getFieldName4(),sPCommonSearchData.getFieldName5(),tenderCap);
                            mailSub = "e-GP: "+tenderCap+" Published of Your Choice";
                        }

                        sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                        sendMessageUtil.setEmailTo(mailTo);
                        sendMessageUtil.setEmailSub(mailSub);
                        sendMessageUtil.setEmailMessage(mailText);
                        sendMessageUtil.sendEmail();
                    }
                }

                String  cronEndTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());

                String xml = "<root>"
                                + "<tbl_CronJobs "
                                    + "cronJobName=\""+cronName+"\" "
                                    + "cronJobStartTime=\""+cronStartTime+"\" "
                                    + "cronJobEndTime=\""+cronEndTime+"\" "
                                    + "cronJobStatus=\""+cronStatus+"\" "
                                    + "cronJobMsg=\""+cronMsg+"\"/>"
                            + "</root>";
                try{
                    xmlservice.insertDataBySP("insert", "tbl_CronJobs", xml, "");
                } catch (Exception ex) {
                    AppExceptionHandler expHandler = new AppExceptionHandler();
                    expHandler.stack2string(ex);
                }
            }
         } catch (Exception e) {
             LOGGER.error("sendPublishTenderMail : "+LOGGEREND);
            AppExceptionHandler expHandler = new AppExceptionHandler();
            expHandler.stack2string(e);
         }
         LOGGER.debug("sendPublishTenderMail : "+LOGGEREND);
     }

    public void FinalSubmissionMailSMS()
    {
    LOGGER.debug("FinalSubmissionMailSMS : "+LOGGERSTART);
    CommonSearchService cmnSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
        List <SPCommonSearchData> recordCron = null;
      //  UserRegisterService  service = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");

        recordCron = cmnSearchService.searchData("getCronJob", "Final Submission To Tenderer", null, null, null, null, null, null, null, null);

         if(recordCron.isEmpty())
         {
            int cronStatus = 1;
            String cronMsg = "Successful Executed";
            String cronName = "Final Submission To Tenderer";
            String  cronStartTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());
            CommonXMLSPService  xmlservice = (CommonXMLSPService)AppContext.getSpringBean("CommonXMLSPService");

            try{
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                List <SPCommonSearchData> spcommonSearchData = null;
                spcommonSearchData = cmnSearchService.searchData("getBidderMobNoForFinalSub", null, null, null, null, null, null, null, null, null);
              //  String data[]= tenderBidService.getTendererMobileNEmail(session.getAttribute("userId").toString());

                for(SPCommonSearchData searchData :spcommonSearchData)
                {
                    String userId =searchData.getFieldName1();
                    String tenderId = searchData.getFieldName2();
                    String mobileNo = searchData.getFieldName3();
                    String mail[] = {searchData.getFieldName4()};
                    String subDt = searchData.getFieldName5();
                    String peOfficeName = searchData.getFieldName6();
                    String refNo = searchData.getFieldName7();
                    String pkgNo = searchData.getFieldName8();
                    String pkgDes = searchData.getFieldName9();
                    MailContentUtility mailContentUtility = new MailContentUtility();
                  //  MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                   // service.contentAdmMsgBox(mail[0], XMLReader.getMessage("emailIdHelp"), "e-GP: Final Submission Reminder", msgBoxContentUtility.getPreBidMeetingContent(tenderId,refNo,repTime));

                    String mailText = mailContentUtility.FinalSubToBidderContent(tenderId,subDt,peOfficeName,refNo,pkgNo,pkgDes);
                    sendMessageUtil.setEmailTo(mail);
                    sendMessageUtil.setEmailSub("e-GP: Deadline for final submission of an e-Tender");
                    sendMessageUtil.setEmailMessage(mailText);
                    sendMessageUtil.sendEmail();

                    mail=null;
               //     sendMessageUtil=null;
                    mailContentUtility=null;
                  //  msgBoxContentUtility=null;

                   // if(!"+975".equals(mobileNo)){
                    sendMessageUtil.setSmsNo(mobileNo);
                    sendMessageUtil.setSmsBody("Dear User, Reminder for Final Submission of an e-Tender: Tender ID: "+tenderId+" Last Date and time: "+subDt+" GPPMD Office");
                    sendMessageUtil.sendSMS();
               // }
                }
            }catch(Exception ex){
                cronMsg = ex.toString();
                cronStatus = 0;
                AppExceptionHandler expHandler = new AppExceptionHandler();
                expHandler.stack2string(ex);
            }

            String  cronEndTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());

            String xml = "<root>"
                            + "<tbl_CronJobs "
                                + "cronJobName=\""+cronName+"\" "
                                + "cronJobStartTime=\""+cronStartTime+"\" "
                                + "cronJobEndTime=\""+cronEndTime+"\" "
                                + "cronJobStatus=\""+cronStatus+"\" "
                                + "cronJobMsg=\""+cronMsg+"\"/>"
                        + "</root>";
            try{
                xmlservice.insertDataBySP("insert", "tbl_CronJobs", xml, "");
            } catch (Exception ex) {
                LOGGER.error("FinalSubmissionMailSMS : " + ex.toString());
                AppExceptionHandler expHandler = new AppExceptionHandler();
                expHandler.stack2string(ex);
            }
         }


    }

    public void FileTransferScheduler() {
        LOGGER.debug("FileTransferScheduler : "+LOGGERSTART);
        CommonSearchService cmnSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
        List <SPCommonSearchData> recordCron = null;

        recordCron = cmnSearchService.searchData("getCronJob", "File Transfer", null, null, null, null, null, null, null, null);

        if(recordCron.isEmpty())
         {
            int cronStatus = 1;
            String cronMsg = "Successful Executed";
            String cronName = "File Transfer";
            String  cronStartTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());
            CommonXMLSPService  xmlservice = (CommonXMLSPService)AppContext.getSpringBean("CommonXMLSPService");
            
            //trasnfer coder start
            File destinationFolder = new File("Z:\\");
            File sourceFolder = new File("E:\\eGP");
            int count = 0;
            try {
                if (Files.notExists(sourceFolder.toPath())) {
                    count = -1;
                }   
                if (Files.notExists(destinationFolder.toPath())) {
                    //try {
                        destinationFolder.mkdir();
                        
                   // }catch(Exception e){
                   //     System.out.println("Error while creating directory");
                   //     count = -1;
                  //      LOGGER.error("FileTransferScheduler : " + e.toString());
                  //  }
                }
                BasicFileAttributes attributes = null;
                File[] listOfFiles = sourceFolder.listFiles();

                if (listOfFiles != null) {
                    for (File child : listOfFiles) {
                        attributes = Files.readAttributes(child.toPath(), BasicFileAttributes.class);
                        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                        String creationDate = df.format(new Date(attributes.creationTime().toMillis()));
                        String todayDate = df.format(new Date());
                        if (creationDate.equalsIgnoreCase(todayDate)) {
                            // Move files to destination folder
                            child.renameTo(new File(destinationFolder + "\\" + child.getName()));
                            count++;
                        }
                    }
                }               
            } catch (Exception e) {
             LOGGER.error("FileTransferScheduler : " + e.toString());
            }
            
            //transfer code end
            if(count>0){
                String  cronEndTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(new Date());

                String xml = "<root>"
                                + "<tbl_CronJobs "
                                    + "cronJobName=\""+cronName+"\" "
                                    + "cronJobStartTime=\""+cronStartTime+"\" "
                                    + "cronJobEndTime=\""+cronEndTime+"\" "
                                    + "cronJobStatus=\""+cronStatus+"\" "
                                    + "cronJobMsg=\""+cronMsg+"\"/>"
                            + "</root>";
                try{
                    xmlservice.insertDataBySP("insert", "tbl_CronJobs", xml, "");
                } catch (Exception ex) {
                    LOGGER.error("FileTransferScheduler : " + ex.toString());
                    AppExceptionHandler expHandler = new AppExceptionHandler();
                    expHandler.stack2string(ex);
                }
            }
         }
    }
}
