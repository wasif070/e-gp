/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Chalapathi.Bavisetti
 */
@XmlRootElement
public class ProcurementMothod {
private byte proucrementMethodId;
private String proucrementMethod;

    /**
     * @return the proucrementMethod
     */
 @XmlElement(nillable = true)
    public String getProucrementMethod() {
        return proucrementMethod;
    }

    /**
     * @param proucrementMethod the proucrementMethod to set
     */
    public void setProucrementMethod(String proucrementMethod) {
        this.proucrementMethod = proucrementMethod;
    }

    /**
     * @return the proucrementMethodId
     */
     @XmlElement(nillable = true)
    public byte getProucrementMethodId() {
        return proucrementMethodId;
    }

    /**
     * @param proucrementMethodId the proucrementMethodId to set
     */
    public void setProucrementMethodId(byte proucrementMethodId) {
        this.proucrementMethodId = proucrementMethodId;
    }

}
