/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblCmsContractTermination;
import com.cptu.egp.eps.model.table.TblCmsDateConfig;
import com.cptu.egp.eps.service.serviceimpl.ConsolodateService;
import com.cptu.egp.eps.service.serviceinterface.AccPaymentService;
import com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService;
import com.cptu.egp.eps.service.serviceinterface.CmsContractTerminationService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.servicebean.CmsCTReasonTypeBean;
import com.cptu.egp.eps.web.servicebean.CmsContractTerminationBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.DateUtils;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;
import org.apache.log4j.Logger;

/**
 *
 * @author Sreenu.Durga
 */
public class ContractTerminationServlet extends HttpServlet {

    static final Logger LOGGER = Logger.getLogger(ContractTerminationServlet.class);
    static final String STARTS = " Starts";
    static final String ENDS = " Ends";
    static final String EMPTY_STRING = "";
    static final String STATUS_PENDING = "pending";
    private String action = "";
    String logUserId = "0";
    private static CmsContractTerminationBean cmsContractTerminationBean = new CmsContractTerminationBean();
    private static CmsCTReasonTypeBean cmsCTReasonTypeBean = new CmsCTReasonTypeBean();
    private int tenderId;
    static final String APPEND_CHARACTER = "^";
    private int lotId;
    static final String APPROVE = "Approve";
    static final String REJECT = "Reject";
    static final String ADD = "add";
    static final String EDIT = "edit";
    private String contextPath;
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    private ConsolodateService c_ConsSrv = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
    private CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
    private CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
    private AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
    private final static CmsContractTerminationService cmsContractTerminationService =
            (CmsContractTerminationService) AppContext.getSpringBean("CmsContractTerminationService");

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.debug("processRequest : " + STARTS);
        contextPath = request.getContextPath();
        HttpSession session = request.getSession();
        if (session.getAttribute("userId") != null) {
            logUserId = session.getAttribute("userId").toString();
            cmsContractTerminationBean.setLogUserId(logUserId);
            cmsCTReasonTypeBean.setLogUserId(logUserId);
            accPaymentService.setLogUserId(logUserId);
            cmsConfigDateService.setLogUserId(logUserId);
            commonservice.setUserId(logUserId);
            registerService.setUserId(logUserId);
            cmsContractTerminationService.setLogUserId(logUserId);
            c_ConsSrv.setLogUserId(logUserId);
        }
        String funName = request.getParameter("funName");
        String userTypeId = "";
        if (request.getSession().getAttribute("userTypeId") != null) {
            userTypeId = request.getSession().getAttribute("userTypeId").toString();
        }
        int conId = c_ConsSrv.getContractId(Integer.parseInt(request.getParameter("tenderId")));
        if (request.getParameter("action") != null && !"".equals(request.getParameter("action"))) {
            action = request.getParameter("action");
        } else {
            action = "";
        }        
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            //if (action.length()!= 0) {
            if (ADD.equalsIgnoreCase(action)) {
                addCT(request, response, Integer.parseInt(userTypeId), request.getParameter("tenderId"));
            } else if (APPROVE.equalsIgnoreCase(request.getParameter("submit"))) {
                updateCT(request, response, request.getParameter("submit"), userTypeId);
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Contract_Termination.getName(), "Process / Publish Contract Termination", "");
            } else if (REJECT.equalsIgnoreCase(request.getParameter("submit"))) {
                updateCT(request, response, request.getParameter("submit"), userTypeId);
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Contract_Termination.getName(), "Process / Publish Contract Termination", "");
            } else if (EDIT.equalsIgnoreCase(action)) {                
                editCT(request, response, Integer.parseInt(userTypeId));
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Contract_Termination.getName(), "Edit Contract Termination", "");
            } else if ("validateTermidate".equalsIgnoreCase(action)) {
                String ContractTermiDate = request.getParameter("param1");
                String tenderId = request.getParameter("tenderId");
                String lotId = request.getParameter("lotId");
                String str = validateTermiDate(ContractTermiDate, tenderId, lotId);
                out.print(str);
            }
            if ("checkDuplicateRecord".equals(funName)) {
                out.print(checkDuplicateRecords(request.getParameter("contractId")));
                out.flush();
            }
            //}
        } catch (Exception ex) {
            LOGGER.debug("Exception : " + ex);
        } finally {
            out.close();
        }
        LOGGER.debug("processRequest : " + ENDS);
    }

    private void addCT(HttpServletRequest request, HttpServletResponse response, int userTypeId, String tenderid) throws IOException {
        try {
            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
            String procnature = commonService.getProcNature(request.getParameter("tenderId")).toString();        
            String strCase = "";
            if("Services".equalsIgnoreCase(procnature))
            {
                strCase = "Consultant";
            }else if("goods".equalsIgnoreCase(procnature)){
                strCase = "Supplier";
            }else{
                strCase = "Contractor";
            }
            HttpSession session = request.getSession();
            LOGGER.debug("addCT : " + STARTS);
            String pageName = "";
            if (request.getParameter("tenderId") != null) {
                tenderId = Integer.parseInt(request.getParameter("tenderId"));
            }
            TblCmsContractTermination tblCmsContractTermination = new TblCmsContractTermination();
            tblCmsContractTermination = readInputs(tblCmsContractTermination, request, response, userTypeId);

            String chkstr = checkDuplicateRecords(Integer.toString(tblCmsContractTermination.getContractId()));
            if (!"0".equalsIgnoreCase(chkstr)) {
                if (userTypeId == 3) {
                    pageName = "officer/TabContractTermination.jsp?tenderId=" + tenderId + "&msg=Duplicate";
                } else {
                    pageName = "tenderer/TabContractTermination.jsp?tenderId=" + tenderId + "&msg=Duplicate";
                }
            } else {
                int contractTerminationId = cmsContractTerminationBean.insertCmsContractTermination(tblCmsContractTermination);
                if (userTypeId == 2) {
                    
                    sendMailForContractTermination(request.getParameter("tenderId"), request.getParameter("lotId"), request.getParameter("contractTerminationId"), logUserId, "e-GP: "+strCase+" has Initiated for Contract Termination", userTypeId, "Terminate");
                }
                if (contractTerminationId != 0) {
                    pageName = "resources/common/ContractTeminationUpload.jsp?contractTerminationId=" + contractTerminationId + "&tenderId=" + tenderId + "&contractUserId=" + logUserId + "&contractSignId=" + tblCmsContractTermination.getContractId() + "&action=upload";
                } else {
                    pageName = "resources/common/ContractTermination.jsp?tenderId=" + tenderId + "&contractUserId=" + logUserId + "&contractSignId=" + tblCmsContractTermination.getContractId() + "&lotId=" + lotId;
                }
            }
                        //  ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
            int ContractId = c_ConsSrv.getContractId(Integer.parseInt(tenderid));            
            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), ContractId, "ContractId", EgpModule.Contract_Termination.getName(), "Terminate Contract", "");
            response.sendRedirect(pageName);

            LOGGER.debug("addCT : " + ENDS);
        } catch (Exception ex) {
            LOGGER.error("Exception " + ex);
        }
    }

    private void updateCT(HttpServletRequest request, HttpServletResponse response, String AppOrReje, String userTypeId) throws IOException {
        LOGGER.debug("processRequest : " + STARTS);
        int contractTerminationId = 0;
        String strStatus = "";
        String norevrs = "";
        if (request.getParameter("tenderId") != null) {
            tenderId = Integer.parseInt(request.getParameter("tenderId"));
        }
        if (request.getParameter("lotId") != null) {
            lotId = Integer.parseInt(request.getParameter("lotId"));
        }
        if (request.getParameter("contractTerminationId") != null) {
            contractTerminationId = Integer.parseInt(request.getParameter("contractTerminationId"));
        }
        if (request.getParameter("norevrs") != null) {
            norevrs = request.getParameter("norevrs");
        }
        TblCmsContractTermination cmsContractTermination = new TblCmsContractTermination();
        cmsContractTermination = cmsContractTerminationBean.getCmsContractTermination(contractTerminationId);
        if ("Approve".equalsIgnoreCase(AppOrReje)) {
            strStatus = "approved";
            if (cmsContractTermination.getUserTypeId() == 2) {
                sendMailForContractTermination(request.getParameter("tenderId"), request.getParameter("lotId"), request.getParameter("contractTerminationId"), logUserId, "e-GP: PE has Accepted Contract Termination Request", Integer.parseInt(userTypeId), "Accept");
            } else {
                sendMailForContractTermination(request.getParameter("tenderId"), request.getParameter("lotId"), request.getParameter("contractTerminationId"), logUserId, "e-GP: Procuring Entity has Terminated Contract", Integer.parseInt(userTypeId), "Terminate");
            }
        } else {
            if (cmsContractTermination.getUserTypeId() == 2) {
                sendMailForContractTermination(request.getParameter("tenderId"), request.getParameter("lotId"), request.getParameter("contractTerminationId"), logUserId, "e-GP: PE has Rejected Contract Termination Request", Integer.parseInt(userTypeId), "Reject");
            }
            strStatus = "rejected";
        }
        if (cmsContractTermination != null) {
            cmsContractTermination = cmsContractTerminationBean.changeCTStatus(cmsContractTermination.getContractId(), "status", strStatus);
            if ("approved".equalsIgnoreCase(strStatus) && "0".equalsIgnoreCase(norevrs)) {
                cmsContractTermination = cmsContractTerminationBean.changeCTStatus(cmsContractTermination.getContractId(), "workflowStatus", strStatus);
            }
            response.sendRedirect("officer/ViewContractTermination.jsp?contractTerminationId=" + contractTerminationId + "&tenderId=" + tenderId
                    + "&lotId=" + lotId + "&contractSignId=" + cmsContractTermination.getContractId() + "&msg=" + strStatus);
        } else {
            response.sendRedirect("SessionTimedOut.jsp");
        }
        LOGGER.error("processRequest : " + ENDS);
    }

    private void editCT(HttpServletRequest request, HttpServletResponse response, int userTypeId) throws IOException {
        LOGGER.debug("editCT : " + STARTS);
        int contractTerminationId = 0;
        boolean isUpdated = false;
        if (request.getParameter("tenderId") != null) {
            tenderId = Integer.parseInt(request.getParameter("tenderId"));
        }
        if (request.getParameter("lotId") != null) {
            lotId = Integer.parseInt(request.getParameter("lotId"));
        }
        if (request.getParameter("contractTerminationId") != null) {
            contractTerminationId = Integer.parseInt(request.getParameter("contractTerminationId"));
        }
        TblCmsContractTermination cmsContractTermination = new TblCmsContractTermination();
        cmsContractTermination = cmsContractTerminationBean.getCmsContractTermination(contractTerminationId);
        if (cmsContractTermination != null) {
            cmsContractTermination = readInputs(cmsContractTermination, request, response, userTypeId);
            isUpdated = cmsContractTerminationBean.updateCmsContractTermination(cmsContractTermination);
            if (isUpdated) {
                response.sendRedirect("officer/ViewContractTermination.jsp?contractTerminationId=" + contractTerminationId + "&tenderId=" + tenderId
                        + "&lotId=" + lotId + "&contractSignId=" + cmsContractTermination.getContractId() + "&msg=update");
            } else {
                response.sendRedirect("officer/ViewContractTermination.jsp?contractTerminationId=" + contractTerminationId + "&tenderId=" + tenderId
                        + "&lotId=" + lotId + "&contractSignId=" + cmsContractTermination.getContractId() + "&msg=error");
            }
        } else {
            response.sendRedirect("SessionTimedOut.jsp");
        }
        LOGGER.debug("editCT : " + ENDS);
    }

    private TblCmsContractTermination readInputs(TblCmsContractTermination cmsContractTermination, HttpServletRequest request, HttpServletResponse response, int userTypeId) {
        LOGGER.debug("readInputs : " + STARTS);
        Date dateOfTermination = Calendar.getInstance().getTime();
        String reasonRemark = EMPTY_STRING;
        StringBuffer reasonType = new StringBuffer();
        int countReasonTypes = (int) cmsCTReasonTypeBean.getCmsCTReasonTypeCount();
        String[] reasonTypesArray = new String[countReasonTypes];
        String status = EMPTY_STRING;
        String workflowStatus = EMPTY_STRING;
        int contractId = 0;
        String releasePg = EMPTY_STRING;
        int createdBy = 0;
        String PaymentSettlementAmt = "0";
        String PaymentSettlementAmtRemarks = "";

        HttpSession session = request.getSession();

        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
            createdBy = Integer.parseInt(session.getAttribute("userId").toString());
        }
        if (request.getParameter("txtDateOfTermination") != null) {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            try {
                dateOfTermination = dateFormat.parse((String) request.getParameter("txtDateOfTermination"));
            } catch (ParseException ex) {
                dateOfTermination = Calendar.getInstance().getTime();
            }
        }
        if (request.getParameter("contractSignId") != null) {
            contractId = Integer.parseInt(request.getParameter("contractSignId"));
        }
        if (request.getParameter("txtReason") != null) {
            reasonRemark = request.getParameter("txtReason");
        }
        if (request.getParameter("PSAtxt") != null && !"".equals(request.getParameter("PSAtxt"))) {
            PaymentSettlementAmt = request.getParameter("PSAtxt");
        }
        if (request.getParameter("txtRemarks") != null && !"".equals(request.getParameter("txtRemarks"))) {
            PaymentSettlementAmtRemarks = request.getParameter("txtRemarks");
        }
        if (request.getParameter("radioReleasePG") != null) {
            releasePg = request.getParameter("radioReleasePG");
        }
        if (request.getParameter("lotId") != null) {
            lotId = Integer.parseInt(request.getParameter("lotId"));
        }
        status = STATUS_PENDING;
        workflowStatus = STATUS_PENDING;
        if (request.getParameter("checkReasonType") != null) {
            reasonTypesArray = request.getParameterValues("checkReasonType");
        }
        if (reasonTypesArray.length != 0) {
            for (int i = 0; i < reasonTypesArray.length; i++) {
                reasonType.append(reasonTypesArray[i]);
                reasonType.append(APPEND_CHARACTER);
            }
        }
        cmsContractTermination.setContractId(contractId);
        cmsContractTermination.setCreatedBy(createdBy);
        cmsContractTermination.setDateOfTermination(dateOfTermination);
        cmsContractTermination.setReason(reasonRemark);
        cmsContractTermination.setReasonType(reasonType.toString());
        cmsContractTermination.setReleasePg(releasePg);
        cmsContractTermination.setStatus(status);
        cmsContractTermination.setWorkflowStatus(workflowStatus);
        cmsContractTermination.setUserTypeId(userTypeId);
        cmsContractTermination.setPaySetAmt(new BigDecimal(PaymentSettlementAmt));
        cmsContractTermination.setPaySetAmtRemarks(PaymentSettlementAmtRemarks);
        LOGGER.debug("readInputs : " + ENDS);
        return cmsContractTermination;
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /** send mail notification to tenderer/PE on terminating the contract by PE/tenderer */
    private boolean sendMailForContractTermination(String tenderId, String lotId, String childId, String logUserId, String mailSubject, int userTypeId, String AcceptReject) {
        boolean flag = false;
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peName = "";
            String strFrom = commonservice.getEmailId(logUserId);

            List<Object[]> AcEmailId = accPaymentService.getAccountantEmailId(Integer.parseInt(tenderId));
            String[] strTo = null;
            int emailIndex = 0;
            if (userTypeId == 3) {
                List<Object[]> WFUserEmailId = cmsContractTerminationService.getWFUserEmailId(Integer.parseInt(childId), Integer.parseInt(tenderId), Integer.parseInt(logUserId));
                List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(logUserId));
                if (peNameObj.get(0) != null) {
                    peName = peNameObj.get(0).toString();
                }
                if ("Terminate".equalsIgnoreCase(AcceptReject)) {
                    strTo = new String[AcEmailId.size() + WFUserEmailId.size() + 1];
                    for (Object[] emailId : AcEmailId) {
                        strTo[emailIndex] = emailId[4].toString();
                        emailIndex++;
                    }
                    for (Object[] emailId : WFUserEmailId) {
                        strTo[emailIndex] = emailId[0].toString();
                        emailIndex++;
                    }
                    strTo[emailIndex] = obj[9].toString();
                } else if ("Accept".equalsIgnoreCase(AcceptReject)) {
                    strTo = new String[AcEmailId.size() + 1];
                    for (Object[] emailId : AcEmailId) {
                        strTo[emailIndex] = emailId[4].toString();
                        emailIndex++;
                    }
                    strTo[emailIndex] = obj[9].toString();
                } else if ("Reject".equalsIgnoreCase(AcceptReject)) {
                    strTo = new String[1];
                    strTo[emailIndex] = obj[9].toString();
                }
            } else {
                strTo = new String[1];
                String peEmailId = "";
                List<Object[]> peEmailObj = accPaymentService.getPEemailID(Integer.parseInt(tenderId));
                if (!peEmailObj.isEmpty()) {
                    Object[] objj = peEmailObj.get(0);
                    peEmailId = objj[0].toString();
                }
                strTo[emailIndex] = peEmailId;
            }
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            sendMessageUtil.setEmailFrom(strFrom);
            sendMessageUtil.setEmailSub(mailSubject);
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String emailId[] = new String[1];
            String mobileno = "";
            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
            String procnature = commonService.getProcNature(tenderId).toString();        
            String strCase = "";
            if("Services".equalsIgnoreCase(procnature))
            {
                strCase = "Consultant";
            }else if("goods".equalsIgnoreCase(procnature)){
                strCase = "Supplier";
            }else{
                strCase = "Contractor";
            }
            for (int i = 0; i < strTo.length; i++) {
                emailId[0] = strTo[i];
                registerService.contentAdmMsgBox(emailId[0], strFrom, mailSubject, msgBoxContentUtility.contractTermination(c_isPhysicalPrComplete, peName, obj, accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])), Integer.toString(userTypeId), AcceptReject));
                String mailText = mailContentUtility.contractTermination(c_isPhysicalPrComplete, peName, obj, accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])), Integer.toString(userTypeId), AcceptReject);
                sendMessageUtil.setEmailTo(emailId);
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                if ("2".equalsIgnoreCase(accPaymentService.getUserTypeId(emailId[0]))) {
                    mobileno = accPaymentService.getMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                } else {
                    mobileno = accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                }
                StringBuilder sb = new StringBuilder();
                sendMessageUtil.setSmsNo(mobileno);
                if (userTypeId == 2) {
                    sb.append("Dear User,%0AThis is to inform you that "+strCase+" has initiated for Contract Termination ");
                    sb.append("for Contract No."+obj[1].toString() +" ("+ obj[15].toString() +")"+" ");
                    sb.append("%0AThanks,%0AeGP System");
                } else {
                    if ("Terminate".equalsIgnoreCase(AcceptReject)) {
                        sb.append("Dear User,%0AThis is to inform you that PE has initiated for Contract Termination ");
                        sb.append("for Contract No."+obj[1].toString() +" ("+ obj[15].toString() +")"+" ");
                        sb.append("%0AThanks,%0AeGP System");
                    } else if ("Accept".equalsIgnoreCase(AcceptReject)) {
                        sb.append("Dear User,%0AThis is to inform you that PE has Accepted request of Contract Termination ");
                        sb.append("for Contract No."+obj[1].toString() +"&nbsp;("+ obj[15].toString() +")"+" ");
                        sb.append("%0AThanks,%0AeGP System");
                    } else if ("Reject".equalsIgnoreCase(AcceptReject)) {
                        sb.append("Dear User,%0AThis is to inform you that PE has rejected request of Contract Termination ");
                        sb.append("for Contract No."+obj[1].toString() +" ("+ obj[15].toString() +")"+" ");
                        sb.append("%0AThanks,%0AeGP System");
                    }
                }
                sendMessageUtil.setSmsBody(sb.toString());
                sendMessageUtil.sendSMS();
            }
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }

    /** it checks duplicate records for contract termination details to database */
    private String checkDuplicateRecords(String contractId) throws Exception {
        LOGGER.debug("checkDuplicateRecords : " + logUserId + " : Starts");
        cmsContractTerminationService.setLogUserId(logUserId);
        String val = "";
        try {
            val = String.valueOf(cmsContractTerminationService.chkDuplicateEntry(contractId));
        } catch (Exception e) {
            LOGGER.error("checkDuplicateRecords : " + logUserId + " : Exception" + e);
        }
        LOGGER.debug("checkDuplicateRecords : " + logUserId + " : Ends");
        return val;
    }

    /** it validates contract termination dates */
    private String validateTermiDate(String ContractTermiDate, String tenderId, String lotId) {
        String str = "";
        LOGGER.error("validateTermiDate : Starts");
        try {
            Date ContractTermiDt = DateUtils.formatStdString(DateUtils.formatStdDate(DateUtils.convertStringtoDate(ContractTermiDate, "dd-MMM-yyyy")));
            CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
            List<TblCmsDateConfig> cmsConfigData = cmsConfigDateService.getConfigDatesdata(Integer.parseInt(tenderId), Integer.parseInt(lotId));
            Date actualStartDate = null;
            if (!cmsConfigData.isEmpty()) {
                actualStartDate = cmsConfigData.get(0).getActualStartDt();
            }
            Date actualStartDt = DateUtils.convertStringtoDate(actualStartDate.toString(), "yyyy-MM-dd HH:mm:ss");
            if (ContractTermiDt.before(actualStartDt)) {
                str = "Contract Termination Date must be greater than Actual Contract Start Date";
            }
        } catch (Exception e) {
            LOGGER.error("validateTermiDate : " + e);
        }
        LOGGER.error("validateTermiDate : Ends");
        return str;
    }
}
