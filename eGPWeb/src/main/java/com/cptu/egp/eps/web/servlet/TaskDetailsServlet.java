/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblToDoList;
import com.cptu.egp.eps.web.servicebean.AddTaskSrBean;
import com.cptu.egp.eps.web.utility.DateUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author test
 */
public class TaskDetailsServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Logger logger = Logger.getLogger(TaskDetailsServlet.class);
        int uid = 0;
        logger.debug("ViewTaskDetails grid data : "+uid+" Starts");
        try {
            response.setContentType("text/xml;charset=UTF-8");
            List tskdetails = null;
            if (request.getSession().getAttribute("userId") != null) {
                Integer ob1 = (Integer) request.getSession().getAttribute("userId");
                uid = ob1.intValue();                
                       }

                Integer recPerPage = Integer.parseInt(request.getParameter("rows"));
            Integer pag = Integer.parseInt(request.getParameter("page"));
            
            int offset = (((pag) - 1) * (recPerPage));

                int totalpages = 0;
                int totalRecords = 0;
            String str = request.getParameter("DateFrom");
            String str1 = request.getParameter("DateTo");
            String status = request.getParameter("select");
                Date date;
                Date date1;
                date  = DateUtils.formatStdString(str);
                date1 = DateUtils.formatStdString(str1);
            if (status.equals("null")) {
                    status = "Pending";
            }
            AddTaskSrBean taskSrBean = new AddTaskSrBean();
            taskSrBean.setLogUserId(""+uid);
            tskdetails = taskSrBean.searchTasks(offset,recPerPage,date, date1, status, uid);
            List count = taskSrBean.getData(uid,status);
//            if (!tskdetails.isEmpty()) {
//
//                totalRecords = count.size();
//                if (totalRecords % recPerPage == 0) {
//                    totalpages = totalRecords / recPerPage;
//                } else {
//                    totalpages = totalRecords / recPerPage + 1;
//                }
//
//            }
                  totalRecords =  count.size();
            if (totalRecords > 0) {
                    if (totalRecords % recPerPage == 0) {
                        totalpages = totalRecords / recPerPage;
                    } else {
                        totalpages = (totalRecords / recPerPage) + 1;
                      }
                } else {
                    totalpages = 0;
                   }

            StringBuilder sb = new StringBuilder();

                                 sb.append("<?xml version='1.0' encoding='utf-8'?>");
                                 sb.append("<rows>");

                                 sb.append("<page>" + pag + "</page>");
                                 sb.append("<total>" + totalpages + "</total>");
            sb.append("<records>" + totalRecords + "</records>");
                                 int j = 1;
            int srNo = offset +1;
            if (!tskdetails.isEmpty()) {
                               for (int i = 0; i < tskdetails.size(); i++) {
                                   TblToDoList tbltodoList = (TblToDoList) tskdetails.get(i);
                                    sb.append("<row>");
                    sb.append("<cell>" + srNo + "</cell>");
                                    sb.append("<cell>" + tbltodoList.getTaskBrief() + "</cell>");
                    sb.append("<cell>" + tbltodoList.getPriority() + "</cell>");
                                    String str3 = DateUtils.customDateFormate(tbltodoList.getStartDate());
                    sb.append("<cell>" + str3 + "</cell>");
                                    String str4 = DateUtils.customDateFormate(tbltodoList.getEndDate());
                    sb.append("<cell>" + str4 + "</cell>");
                    String link1 = "<a  href='ViewTask.jsp?taskid=" + tbltodoList.getTaskId() + "&DateFrom=" + str + "&DateTo=" + str1 + "&select=" + status + "'>View</a>";
                                     sb.append("<cell><![CDATA[" + link1 + "]]></cell>");
                                    sb.append("</row>");
                    j++;srNo++;
                                }
            } else {
                            sb.append("<row>");
                sb.append("<cell>" + j + "</cell>");
                            sb.append("<cell>No Data Found</cell>");
                            sb.append("<cell>No Data Found</cell>");
                            sb.append("<cell>No Data Found</cell>");
                            sb.append("<cell>No Data Found</cell>");
                            sb.append("<cell>No Data Found</cell>");
                            sb.append("</row>");

                   }
                                       sb.append("</rows>");
                                       out.print(sb.toString());
                                  String url = sb.toString();
            logger.debug("ViewTaskDetails grid data : "+uid+" url=" + url);

        }catch(Exception e){
            logger.error("ViewTaskDetails grid data : "+uid,e);
        } finally {
            logger.debug("ViewTaskDetails grid data : "+uid+" Ends");

            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
