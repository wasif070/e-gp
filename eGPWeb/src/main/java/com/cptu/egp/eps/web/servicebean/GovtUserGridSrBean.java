/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
    Changed in search formation by Emtaz on 11/May/2016. JqGrid is removed.
*/

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.CommonAppData;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.service.serviceinterface.CreateProjectService;
import com.cptu.egp.eps.service.serviceinterface.GovtUserCreationService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author parag
 */
//@WebServlet(name="GovtUserGridSrBean", urlPatterns={"/GovtUserGridSrBean"})
public class GovtUserGridSrBean extends HttpServlet {
    static final Logger LOGGER = Logger.getLogger(GovtUserGridSrBean.class);
    String logUserId = "0";

    CreateProjectService projectService = (CreateProjectService) AppContext.getSpringBean("CreateProjectService");
    GovtUserCreationService govtUserCreationService = (GovtUserCreationService) AppContext.getSpringBean("GovtUserCreationService");
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        LOGGER.debug("processRequest "+logUserId+" Starts");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();        
        String action = request.getParameter("action");
        HttpSession session = request.getSession();
        int userTypeId=0;
        int userId=0;
        if( session.getAttribute("userTypeId") != null && !"".equalsIgnoreCase(session.getAttribute("userTypeId").toString())){
            userTypeId = Integer.parseInt(session.getAttribute("userTypeId").toString());
        }
        if(session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())){
            userId = Integer.parseInt(session.getAttribute("userId").toString());
            logUserId = session.getAttribute("userId").toString();

        }
        projectService.setUserId(logUserId);
        govtUserCreationService.setLogUserId(logUserId);
        try {
            //<editor-fold>
            //LOGGER.debug(" viewGovData  "+action+" "+logUserId+" Starts:");
            /*if ("viewGovData".equalsIgnoreCase(action)) {
                response.setContentType("text/xml;charset=UTF-8");
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String sord = request.getParameter("sord");
                String sidx = request.getParameter("sidx");
                String rowList = request.getParameter("rowList");
                String type = request.getParameter("govUserType");

                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                //List<CommonAppData> getGovData = null;
                List<SPCommonSearchData> getGovData = null;
                String searchField = "", searchString = "", searchOper = "";
                String searchOnDept = "";
                 CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                 commonSearchService.setLogUserId(logUserId);
                if(_search){
                    searchField = request.getParameter("searchField");
                    searchString = request.getParameter("searchString");
                    searchOper = request.getParameter("searchOper");
                    //getGovData = projectService.getDetailsBySP("GetEmployee",""+userTypeId,""+userId, searchField, searchString, searchOper);
                   if(searchField.equalsIgnoreCase("role")){
                       searchField = "dbo.getEmpRoles(e.userid)";
                       //Enabling search for HOPA and closing search for HOPE. Emtaz on 26/April/2016
                       if(searchString.equalsIgnoreCase("HOPE"))
                           searchString = "Search Option for Hope is turned off";
                       else if(searchString.equalsIgnoreCase("HOPA"))
                           searchString = "HOPE";
                   }
                    if(searchField.equalsIgnoreCase("organizationName")){
                        searchOnDept = searchField;
                        String field = null;
                        if(searchOper.equalsIgnoreCase("cn")){
                            field = "like";
                            searchField = "employeeid in (select distinct employeeid from tbl_employeeroles er,"
                               + "tbl_departmentmaster dm where er.departmentId=dm.departmentId and departmentName like '%"+searchString+"%' )";
                        }else{
                            field = "=";
                            searchField = "employeeid in (select distinct employeeid from tbl_employeeroles er,"
                               + "tbl_departmentmaster dm where er.departmentId=dm.departmentId and departmentName = '"+searchString+"' )";
                        }
                   
                   }
                    if(searchField.equalsIgnoreCase("status")){
                        if(searchString.equalsIgnoreCase("Complete")){
                            searchString = "approved";
                        } else if(searchString.equalsIgnoreCase("Incomplete")){
                            searchString = "pending";
                        }
                    }
                    getGovData = new ArrayList<SPCommonSearchData>();
                    for( SPCommonSearchData sPCommonSearchData : commonSearchService.searchData("GetEmployee", ""+userTypeId,""+userId, searchField, searchString, searchOper,searchOnDept,"","","")){
                        SPCommonSearchData commonAppData = new SPCommonSearchData();
                        commonAppData.setFieldName1(sPCommonSearchData.getFieldName1());
                        commonAppData.setFieldName2(sPCommonSearchData.getFieldName2());
                        commonAppData.setFieldName3(sPCommonSearchData.getFieldName3());
                        commonAppData.setFieldName4(sPCommonSearchData.getFieldName4());
                        commonAppData.setFieldName5(sPCommonSearchData.getFieldName5());
                        commonAppData.setFieldName6(sPCommonSearchData.getFieldName6());
                         commonAppData.setFieldName7(sPCommonSearchData.getFieldName7());
                        commonAppData.setFieldName8(sPCommonSearchData.getFieldName8());
                        getGovData.add(commonAppData);
                    }
                } else {
                     String orderClause = null;
                     String order = null;
                     if(sidx.equals("")){
                         orderClause = "registereddate";
                         order = "desc";
                     }else if(sidx.equalsIgnoreCase("employeeName")){
                         orderClause = "dbo.f_trim(FieldValue2)";
                         if(sord.equalsIgnoreCase("asc")){
                             order = "ASC";
                         }else if(sord.equalsIgnoreCase("desc")){
                             order = "DESC";
                         }
                     }else if(sidx.equalsIgnoreCase("organizationName")){
                         orderClause = "FieldValue7";
                         if(sord.equalsIgnoreCase("asc")){
                             order = "ASC";
                         }else if(sord.equalsIgnoreCase("desc")){
                             order = "DESC";
                         }
                     }else if(sidx.equalsIgnoreCase("role")){
                         orderClause = "FieldValue8";
                         if(sord.equalsIgnoreCase("asc")){
                             order = "ASC";
                         }else if(sord.equalsIgnoreCase("desc")){
                             order = "DESC";
                         }
                     }else if(sidx.equalsIgnoreCase("emailid")){
                         orderClause = "FieldValue3";
                         if(sord.equalsIgnoreCase("asc")){
                             order = "ASC";
                         }else if(sord.equalsIgnoreCase("desc")){
                             order = "DESC";
                         }
                     }else if(sidx.equalsIgnoreCase("sdate")){
                         orderClause = "registereddate";
                         if(sord.equalsIgnoreCase("asc")){
                             order = "ASC";
                         }else if(sord.equalsIgnoreCase("desc")){
                             order = "DESC";
                         }
                     }else if(sidx.equalsIgnoreCase("status")){
                         orderClause = "FieldValue5";
                         if(sord.equalsIgnoreCase("asc")){
                             order = "ASC";
                         }else if(sord.equalsIgnoreCase("desc")){
                             order = "DESC";
                         }
                     }
                     getGovData = commonSearchService.searchData("GetEmployeeDetails",""+userTypeId,""+userId,orderClause,order,"","","","","");
                    //projectService.getDetailsBySP("GetEmployee",""+userTypeId,""+userId);
                }

                
                int totalPages = 0;
                long totalCount = getGovData.size();
                LOGGER.debug("totalCount : "+totalCount);
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(rows) == 0) {
                        totalPages = (int) (totalCount / Integer.parseInt(rows));
                    } else {
                        totalPages = (int) ((totalCount / Integer.parseInt(rows)) + 1);
                    }
                } else {
                    totalPages = 0;
                }


                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + getGovData.size() + "</records>");
                // be sure to put text data in CDATA
                //(Integer.parseInt(page)-1)*10
                    int j =0;
                    int srNo = 0;
                    int no = Integer.parseInt(request.getParameter("page"));
                    if(no == 1){
                        j = 0;
                        srNo = 1;
                    }else{
                        if(Integer.parseInt(rows) == 30){
                            j = ((no-1)*30);
                            srNo = ((no-1)*30)+1;
                        }else if(Integer.parseInt(rows) == 20){
                            j = ((no-1)*20);
                            srNo = ((no-1)*20)+1;
                        }
                        else{
                            j = ((no-1)*10);
                            srNo = ((no-1)*10)+1;
                        }
                    }
                for (int i = j ; i < getGovData.size(); i++) {
                    String pRole="";
                    out.print("<row id='" + getGovData.get(i).getFieldName1() + "'>");
                    out.print("<cell>" + srNo + "</cell>");
                    out.print("<cell><![CDATA[" + getGovData.get(i).getFieldName2() + "]]> </cell>");
                    out.print("<cell><![CDATA[" + getGovData.get(i).getFieldName7() + "]]> </cell>");
                   if(getGovData.get(i).getFieldName8().trim().contains("HOPE")) 
                        pRole=getGovData.get(i).getFieldName8().replace("HOPE","HOPA");
                   else if(getGovData.get(i).getFieldName8().trim().equalsIgnoreCase("HOPE"))
                        pRole = "HOPA";
                    else
                        pRole = getGovData.get(i).getFieldName8();
                    out.print("<cell><![CDATA[" + pRole + "]]> </cell>");
                    out.print("<cell>" + getGovData.get(i).getFieldName3() + "</cell>");
                    out.print("<cell>" + getGovData.get(i).getFieldName4() + "</cell>");
                    String status="";
                    if("pending".equalsIgnoreCase(getGovData.get(i).getFieldName5())){
                         status="Incomplete";
                    }else{
                         status="Complete";
                    }
                    out.print("<cell>" + status + "</cell>");

                    String editlink="";

                    if ("approved".equalsIgnoreCase(getGovData.get(i).getFieldName5()) ||
                            "deactive".equalsIgnoreCase(getGovData.get(i).getFieldName5())) {
                        out.print("<cell>  ");
                    } else {
                        editlink = "<a href=\"GovtUserCreation.jsp?empId=" + getGovData.get(i).getFieldName1() + "&Edit=Edit\">Edit</a>";
                        out.print("<cell><![CDATA[" + editlink + "]]> ");
                    }
                    String viewlink="<a href=\"ViewGovtUserDetail.jsp?empId="+ getGovData.get(i).getFieldName1() + "&govUserType="+type+"&userTypeId="+userTypeId+"&mode=View\">View</a>";
                    if (!"approved".equalsIgnoreCase(getGovData.get(i).getFieldName5()) && !"deactive".equalsIgnoreCase(getGovData.get(i).getFieldName5())) {
                        out.print(" | ");
                        out.print("<![CDATA[" + viewlink+ "]]> ");
                    }else{
                        editlink = "<a href=\"GovtUserCreation.jsp?empId=" + getGovData.get(i).getFieldName1() + "&Edit=Edit\">Edit</a>";//bug id ::5078
                        out.print("<![CDATA[" + editlink + "]]> ");//bug id ::5078
                        out.print(" | ");//bug id ::5078
                        out.print("<![CDATA[" + viewlink + "]]> ");
                        if(userTypeId == 1 || userTypeId == 5 || userTypeId == 4){
                            String transferLink="<a href=\"GovReplaceUser.jsp?empId="+ getGovData.get(i).getFieldName1() + "\">Transfer</a>";
                            //String transferLink="<a href=\"" + request.getContextPath() + "\"GovReplaceServlet?action=dataFill&empId="+ getGovData.get(i).getFieldName1() + "\">Replace</a>";
                            String activeLink = "";
                            if ("deactive".equalsIgnoreCase(getGovData.get(i).getFieldName5())) {
                                activeLink="<a href=\"ManageGovtPartUser.jsp?empId="+ getGovData.get(i).getFieldName1()+"&userTypeId=" + userTypeId + "&userOldStatus=" + getGovData.get(i).getFieldName5()+"\">Activate</a>";
                            }else if ("approved".equalsIgnoreCase(getGovData.get(i).getFieldName5())) {
                                activeLink="<a href=\"ManageGovtPartUser.jsp?empId="+ getGovData.get(i).getFieldName1()+"&userTypeId=" + userTypeId + "&userOldStatus=" + getGovData.get(i).getFieldName5()+"\">Deactivate</a>";
                    }
                            out.print("<![CDATA[ | " + transferLink + "]]> ");
                            out.print("<![CDATA[ | "+"<BR />" +activeLink + "]]> ");
                        }
                    }

                    boolean isDataAvailableRoles = false;
                        if("pending".equalsIgnoreCase(getGovData.get(i).getFieldName5()) && !"0".equalsIgnoreCase(getGovData.get(i).getFieldName6())){
                            isDataAvailableRoles = true;
                        }
                      // isDataAvailableRoles =govtUserCreationService.isEmployeeDataAvailableInRole(Integer.parseInt(getGovData.get(i).getFieldName1()));
                    String Complete="";

                    if(isDataAvailableRoles){
                        Complete="<a style = 'font-weight:bold;' href=\"ViewGovtUserDetail.jsp?empId="+ getGovData.get(i).getFieldName1() + "&govUserType="+type+"&userTypeId="+userTypeId+"&mode=finalSubmission\"> Complete </a>";
                        out.print(" | <![CDATA[" + Complete+ "]]></cell>");
                    }
                    else{
                        out.print("</cell>");
                    }
                    
                    out.print("</row>");
                    j++;
                    srNo++;
                }
                out.print("</rows>");
            }*/
            //</editor-fold>
            if ("viewGovData".equalsIgnoreCase(action)) {
                String type = request.getParameter("govUserType");

                String _search = request.getParameter("IsSearch");
                //List<CommonAppData> getGovData = null;
                List<SPCommonSearchData> getGovData = null;
                getGovData = new ArrayList<SPCommonSearchData>();
                CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                commonSearchService.setLogUserId(logUserId);
                for( SPCommonSearchData sPCommonSearchData : commonSearchService.searchData("GetEmployee", ""+userTypeId,""+userId, "", "", "","","","",""))
                {
                    SPCommonSearchData commonAppData = new SPCommonSearchData();
                    commonAppData.setFieldName1(sPCommonSearchData.getFieldName1());
                    commonAppData.setFieldName2(sPCommonSearchData.getFieldName2());
                    commonAppData.setFieldName3(sPCommonSearchData.getFieldName3());
                    commonAppData.setFieldName4(sPCommonSearchData.getFieldName4());
                    commonAppData.setFieldName5(sPCommonSearchData.getFieldName5());
                    commonAppData.setFieldName6(sPCommonSearchData.getFieldName6());
                     commonAppData.setFieldName7(sPCommonSearchData.getFieldName7());
                    commonAppData.setFieldName8(sPCommonSearchData.getFieldName8());
                    getGovData.add(commonAppData);
                }
                //String searchField = "", searchString = "", searchOper = "";
                //String searchOnDept = "";
                List<SPCommonSearchData> getGovDataSearched = new ArrayList<SPCommonSearchData>();
                
                
                    String FullName = request.getParameter("FullName");
                    String OrganizationName = request.getParameter("OrganizationName");
                    String Role = request.getParameter("Role");
                    if(Role.equalsIgnoreCase("HOPA"))
                        Role = "HOPE";
                    String EmailID = request.getParameter("EmailID");
                    String Status = request.getParameter("Status");
                    for(int j=0;j<getGovData.size();j++)
                    {
                        boolean ToAdd = true;
                        if(FullName!=null && !FullName.equalsIgnoreCase(""))
                        {
                            if(!getGovData.get(j).getFieldName2().toLowerCase().contains(FullName.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(OrganizationName!=null && !OrganizationName.equalsIgnoreCase(""))
                        {
                            if(!OrganizationName.equalsIgnoreCase(getGovData.get(j).getFieldName7()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(Role!=null && !Role.equalsIgnoreCase(""))
                        {
                            if(Role.equalsIgnoreCase("PE"))
                            {
                                if(getGovData.get(j).getFieldName8().contains(Role))
                                {
                                    ToAdd = false;
                                    int LocationOfRole = getGovData.get(j).getFieldName8().indexOf(Role);
                                    if(LocationOfRole==0)
                                    {
                                        ToAdd = true;
                                    }
                                    else
                                    {
                                        if(getGovData.get(j).getFieldName8().charAt(LocationOfRole-1)==',')
                                        {
                                            ToAdd = true;
                                        }
                                        
                                    }
                                }
                                else
                                {
                                    ToAdd = false;
                                }
                            }
                            else
                            {
                                if(!getGovData.get(j).getFieldName8().contains(Role))
                                {
                                    ToAdd = false;
                                }
                            }
                        }
                        if(EmailID!=null && !EmailID.equalsIgnoreCase(""))
                        {
                            if(!getGovData.get(j).getFieldName3().toLowerCase().contains(EmailID.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(Status!=null && !Status.equalsIgnoreCase(""))
                        {
                            if(!Status.equalsIgnoreCase(getGovData.get(j).getFieldName5()))
                            {
                                ToAdd = false;
                            }
                        }
                        
                        if(ToAdd)
                        {
                            //SPCommonSearchData commonAppData = getGovData.get(j);
                            getGovDataSearched.add(getGovData.get(j));
                        }
                    }
          
                String strPageNo = (request.getParameter("pageNo")!=null ? request.getParameter("pageNo"):"");
                int pageNo =0;
                if(strPageNo!=null && !"".equalsIgnoreCase(strPageNo)){
                    pageNo = Integer.parseInt(strPageNo);
                }
                String strOffset = request.getParameter("size");
                int recordOffset =0;
                if(strOffset!=null && !"".equalsIgnoreCase(strOffset)){
                    recordOffset = Integer.parseInt(strOffset);
                }
                int RecordFrom = (pageNo-1)*recordOffset;
                int k= 0;
                String styleClass = "";
                if (getGovDataSearched != null && !getGovDataSearched.isEmpty()) {
                    for(k=RecordFrom;k<RecordFrom+recordOffset && k<getGovDataSearched.size();k++)
                    {
                        if(k%2==0){
                            styleClass = "bgColor-white";
                        }else{
                            styleClass = "bgColor-Green";
                        }
                        out.print("<tr class='"+styleClass+"'>");
                        out.print("<td width=\"5%\" class=\"t-align-center\">" + (k+1) + "</td>");
                        out.print("<td width=\"10%\" class=\"t-align-center\">" + getGovDataSearched.get(k).getFieldName2() + "</td>");
                        out.print("<td width=\"20%\" class=\"t-align-center\">" + getGovDataSearched.get(k).getFieldName7() + "</td>");
                        String ReplaceHOPE = getGovDataSearched.get(k).getFieldName8();
                        if(ReplaceHOPE.contains("HOPE"))
                        {
                            ReplaceHOPE = ReplaceHOPE.replaceAll("HOPE", "HOPA");
                            //out.print("<td width=\"10%\" class=\"t-align-center\">" + ReplaceHOPE + "</td>");
                        }
                        if(ReplaceHOPE.contains("PE"))
                        {
                            int LocationOfPE = ReplaceHOPE.indexOf("PE");
                            char[] ReplaceHOPEChars = ReplaceHOPE.toCharArray();
                            if(LocationOfPE==0)
                            {
                                
                                ReplaceHOPEChars[LocationOfPE+1] = 'A';
                                ReplaceHOPE = String.valueOf(ReplaceHOPEChars);
                                
                            }
                            else
                            {
                                if(ReplaceHOPE.charAt(LocationOfPE-1)==',')
                                {
                                    ReplaceHOPEChars[LocationOfPE+1] = 'A';
                                    ReplaceHOPE = String.valueOf(ReplaceHOPEChars);
                                }

                            }
                        }
                        
                        out.print("<td width=\"10%\" class=\"t-align-center\">" + ReplaceHOPE + "</td>");
                        out.print("<td width=\"25%\" class=\"t-align-center\">" + getGovDataSearched.get(k).getFieldName3() + "</td>");
                        out.print("<td width=\"10%\" class=\"t-align-center\">" + getGovDataSearched.get(k).getFieldName4() + "</td>");
                        out.print("<td width=\"5%\" class=\"t-align-center\">" + getGovDataSearched.get(k).getFieldName5() + "</td>");
                        out.print("<td width=\"15%\" class=\"t-align-center\">");
                            String editlink="";
                            if ("approved".equalsIgnoreCase(getGovDataSearched.get(k).getFieldName5()) ||
                            "deactive".equalsIgnoreCase(getGovDataSearched.get(k).getFieldName5())) {
                                out.print("");
                            } else {
                                editlink = "<a href=\"GovtUserCreation.jsp?empId=" + getGovDataSearched.get(k).getFieldName1() + "&Edit=Edit\">Edit</a>";
                                if(userTypeId !=1){out.print(editlink);}
                            }
                            String viewlink="<a href=\"ViewGovtUserDetail.jsp?empId="+ getGovDataSearched.get(k).getFieldName1() + "&govUserType="+type+"&userTypeId="+userTypeId+"&mode=View\">View</a>";
                            if (!"approved".equalsIgnoreCase(getGovDataSearched.get(k).getFieldName5()) && !"deactive".equalsIgnoreCase(getGovDataSearched.get(k).getFieldName5())) {
                                if(userTypeId !=1){out.print(" | ");}
                                out.print(viewlink);
                            }else{
                                editlink = "<a href=\"GovtUserCreation.jsp?empId=" + getGovDataSearched.get(k).getFieldName1() + "&Edit=Edit\">Edit</a>";//bug id ::5078
                                if(userTypeId !=1){
                                    out.print(editlink);//bug id ::5078
                                    out.print(" | ");//bug id ::5078
                                }
                                out.print(viewlink);
                                if(userTypeId == 5 || userTypeId == 4){
                                    String transferLink="<a href=\"GovReplaceUser.jsp?empId="+ getGovDataSearched.get(k).getFieldName1() + "\">Transfer</a>";
                                    String activeLink = "";
                                    if ("deactive".equalsIgnoreCase(getGovDataSearched.get(k).getFieldName5())) {
                                        activeLink="<a href=\"ManageGovtPartUser.jsp?empId="+ getGovDataSearched.get(k).getFieldName1()+"&userTypeId=" + userTypeId + "&userOldStatus=" + getGovDataSearched.get(k).getFieldName5()+"\">Activate</a>";
                                    }else if ("approved".equalsIgnoreCase(getGovDataSearched.get(k).getFieldName5())) {
                                        activeLink="<a href=\"ManageGovtPartUser.jsp?empId="+ getGovDataSearched.get(k).getFieldName1()+"&userTypeId=" + userTypeId + "&userOldStatus=" + getGovDataSearched.get(k).getFieldName5()+"\">Deactivate</a>";
                                    }
                                    out.print(" | " + transferLink);
                                    out.print(" | "+"<br />" +activeLink);
                                    out.print("</td>");
                                    out.print("</tr>");
                                   }
                         
                                }
                    }
                   
                }
                else
                {
                    out.print("<tr>");
                    out.print("<td colspan=\"8\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }  
                int totalPages = 1;
                if (getGovDataSearched.size() > 0) {
                    totalPages = (int) (Math.ceil(Math.ceil(getGovDataSearched.size()) / recordOffset));
                    System.out.print("totalPages--"+totalPages+"records "+ getGovDataSearched.size());
                    if (totalPages == 0) {
                        totalPages = 1;
                    }
                }
                out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
            }
            LOGGER.debug(" viewGovData  "+action+" "+logUserId+" Ends:");
        }
            
        catch(Exception e){
            LOGGER.error("processRequest "+logUserId+" :"+e);
        }
        finally {
            out.close();
        }
        LOGGER.error("processRequest "+logUserId+" Ends:");
    }
    

    /**
     * Get government user detail
     * @param userTypeId
     * @return List of <commonAppData>
     */
    public List<CommonAppData> getGovtUserDetail(int userTypeId) {
        LOGGER.debug("getGovtUserDetail "+logUserId+" Starts: ");
        List<CommonAppData> list = null;
        try {
             list = projectService.getDetailsBySP("GetEmployee",""+userTypeId, "0");
           // LOGGER.debug(" Str is ::: " + str);
            
        } catch (Exception e) {
            LOGGER.error(" getGovtUserDetail "+logUserId+" " + e);
        }
       LOGGER.debug("getGovtUserDetail "+logUserId+" Ends: ");
       return list;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
