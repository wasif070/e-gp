/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Chalapathi.Bavisetti
 */
@XmlRootElement
public class OfficeMaster {
    private int officeId;
    private String officeName;

    
    /**
     * @return the officeName
     */
    @XmlElement(nillable = true)
    public String getOfficeName() {
        return officeName;
    }

    /**
     * @param officeName the officeName to set
     */
    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    /**
     * @return the officeId
     */
     @XmlElement(nillable = true)
    public int getOfficeId() {
        return officeId;
    }

    /**
     * @param officeId the officeId to set
     */
    public void setOfficeId(int officeId) {
        this.officeId = officeId;
    }
    

}
