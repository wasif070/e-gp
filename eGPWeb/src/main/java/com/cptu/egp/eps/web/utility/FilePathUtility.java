/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author TaherT
 */
public class FilePathUtility {

    private static Map<String, String> filePath = new HashMap<String, String>();

    /**
     * Get File path.
     * @return
     */
    public static Map<String, String> getFilePath() {
        String drive = XMLReader.getMessage("driveLatter");
        String driveVendorDoc = XMLReader.getMessage("driveLetterVendorDoc");   //  Dohatec - 07.May.15 - XML Param changed only for VendorDocuments
        XMLReader x = new XMLReader();
        File f = null;
        /*if("http://www.eprocure.gov.bd/".equals(x.getMessage("url"))){
            drive = "\\\\egpprdblog\\AppSharedDrive";
        }else if("http://development.eprocure.gov.bd/".equals(x.getMessage("url"))){
            drive = "D:";
        }
        else if("http://staging.eprocure.gov.bd/".equals(x.getMessage("url"))){
            drive = "E:";
       }else if("http://training.eprocure.gov.bd/".equals(x.getMessage("url"))){
            drive = "D:";
        }
*/
        x = null;
        //officerDocs
        f = new File(drive + "\\eGP\\Document\\officerDocuments\\WorkflowDocuments\\");
        if(!f.exists()){
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\TenderDocs\\");
        if(!f.exists()){
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\CorriDocs\\");
        if(!f.exists()){
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\TOSDocs\\");
        if(!f.exists()){
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\TenderPayment\\");
        if(!f.exists()){
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\PreTenderMetting\\");
        if(!f.exists()){
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\PreTenderReply\\");
        if(!f.exists()){
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\TenderDocuments\\");
        if(!f.exists()){
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\PostQualDocument\\");
        if(!f.exists()){
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\EvalRptDocument\\");
        if(!f.exists()){
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\NegotitationDocument\\");
        if(!f.exists()){
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\NOADocument\\");
                if(!f.exists()){
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\NegQueryDocument\\");
                if(!f.exists()){
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\ClarrificationDocument\\");
                if(!f.exists()){
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\ContractSignDocument\\");
                if(!f.exists()){
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\TemplateDocuments\\");
        if(!f.exists()){
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\TemplateDocuments\\GuidelinesDoc\\");
        if(!f.exists()){
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\uplaodReport\\");
        if(!f.exists()){
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\uplaodReport\\");
        if (!f.exists()) {
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\PRDoc\\");
        if (!f.exists()) {
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\WCCDoc\\");
        if (!f.exists()) {
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\CTDoc\\");
        if (!f.exists()) {
            f.mkdir();
        }
//        f = new File(drive + "\\eGP\\Document\\NewsDocuments\\");
//        if (!f.exists()) {
//            f.mkdir();
//        }
        
        //filePath.put("NewsDocuments", drive + "\\\\eGP\\Document\\NewsDocuments\\");
        filePath.put("SBDs", drive + "\\eGP\\Resources\\SBDs\\");
        filePath.put("Manuals", drive + "\\eGP\\Resources\\Manuals\\");
        
        filePath.put("WorkFlowFileUploadServlet", drive + "\\eGP\\Document\\officerDocuments\\WorkflowDocuments\\");
        filePath.put("SevletEngEstUpload", drive + "\\eGP\\Document\\OfficerDocuments\\TenderDocs\\");
        filePath.put("ServletCorriDocUpload", drive + "\\eGP\\Document\\OfficerDocuments\\CorriDocs\\");
        filePath.put("ServletTOSDocUpload", drive + "\\eGP\\Document\\OfficerDocuments\\TOSDocs\\");
        filePath.put("TenderPaymentDocUploadServlet", drive + "\\eGP\\Document\\OfficerDocuments\\TenderPayment\\");
  // Edit By Palash, Dohatec
        filePath.put("AdminReportUploadServlet", drive + "\\eGP\\Document\\OfficerDocuments\\eGPAdmin\\");
        filePath.put("ReportDownloadServlet", drive + "\\eGP\\Document\\OfficerDocuments\\eGPAdmin\\");
  //  End, Palash, Dohatec
        filePath.put("PreTenderMetDocUploadServlet", drive + "\\eGP\\Document\\OfficerDocuments\\PreTenderMetting\\");
        filePath.put("PreTenderReplyUploadServlet", drive + "\\eGP\\Document\\OfficerDocuments\\PreTenderReply\\");
        filePath.put("TenderSecUploadServlet", drive + "\\eGP\\Document\\OfficerDocuments\\TenderDocuments\\");
        filePath.put("PostQualEstDocServlet", drive + "\\eGP\\Document\\OfficerDocuments\\PostQualDocument\\");
        filePath.put("ClarificationRefDocServlet", drive + "\\eGP\\Document\\OfficerDocuments\\EvalRptDocument\\");
        filePath.put("ServletNegotiationDocs", drive + "\\eGP\\Document\\OfficerDocuments\\NegotitationDocument\\");
        filePath.put("ServletNOADoc", drive + "\\eGP\\Document\\OfficerDocuments\\NOADocument\\");
        filePath.put("ServletNegQueryDocs", drive + "\\eGP\\Document\\OfficerDocuments\\NegQueryDocument\\");
        filePath.put("ServletEvalClariDocsOfficer", drive + "\\eGP\\Document\\OfficerDocuments\\ClarrificationDocument\\");
        filePath.put("ServletContractSignDoc", drive + "\\eGP\\Document\\OfficerDocuments\\ContractSignDocument\\");
        filePath.put("TemplateFileUploadServlet", drive + "\\eGP\\Document\\OfficerDocuments\\TemplateDocuments\\");
        filePath.put("STDPDF", drive + "\\eGP\\PDF\\STDPdf");
        filePath.put("guideLinesDoc", drive + "\\eGP\\Document\\OfficerDocuments\\TemplateDocuments\\GuidelinesDoc\\");
        filePath.put("uploadReport", drive + "\\eGP\\Document\\OfficerDocuments\\uplaodReport\\");
        filePath.put("WCCUploadServlet", drive + "\\eGP\\Document\\OfficerDocuments\\WCCDoc\\");
        filePath.put("ProgressReportUploadDocServlet", drive + "\\eGP\\Document\\OfficerDocuments\\PRDoc\\");
        filePath.put("CTUploadServlet", drive + "\\eGP\\Document\\OfficerDocuments\\CTDoc\\");
        filePath.put("AccPaymentDocServlet", drive + "\\eGP\\Document\\AccountantDocuments\\");
        filePath.put("OfficerPaymentDocServlet", drive + "\\eGP\\Document\\OfficerDocuments\\InvoiceDoc\\");
        filePath.put("DeliveryScheduleUploadDocServlet", drive + "\\eGP\\Document\\DeliveryScheduleDoc\\");
        filePath.put("CommencementDocServlet", drive + "\\eGP\\Document\\CommencementDoc\\");
        filePath.put("VariationOrder", drive + "\\eGP\\Document\\VariationOrderDoc\\");
        filePath.put("AccPaymentDocServletforBidder", drive + "\\eGP\\Document\\BidderDocuments\\");
        filePath.put("CSVFiles", drive + "\\eGP\\Document\\OfficerDocuments\\CSVFiles\\");
        filePath.put("WorkSchedule", drive + "\\eGP\\Document\\WorkScheduleDoc\\");
        filePath.put("AttSheetDoc", drive + "\\eGP\\Document\\AttSheetDoc\\");
        filePath.put("ComplaintDocUpload", drive + "\\eGP\\Document\\ComplaintDocuments\\");
        filePath.put("PrivateDissForum", drive + "\\eGP\\Document\\PrivateDissForum\\");
        filePath.put("Debriefing", drive + "\\eGP\\Document\\OfficerDocuments\\Debriefing\\");
        filePath.put("ServletEvalRptDoc", drive + "\\eGP\\Document\\OfficerDocuments\\EvaluationReportDocument\\");

         //Evaluation Report Document
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\EvaluationReportDocument\\");
        if(!f.exists()){
            f.mkdir();
        }
        

        //Debriefing Document
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\Debriefing\\");
        if(!f.exists()){
            f.mkdirs();
        }
        //PrivateDissForum Document
        f = new File(drive + "\\eGP\\Document\\PrivateDissForum\\");
        if(!f.exists()){
            f.mkdirs();
        }

        //AttSheetDoc Document
        f = new File(drive + "\\eGP\\Document\\AttSheetDoc\\");
        if(!f.exists()){
            f.mkdirs();
        }

        // To upload ComplaintManagement Documents
        f = new File(drive + "\\eGP\\Document\\ComplaintDocuments\\");
        if(!f.exists()){
            f.mkdirs();
        }
        
        //WorkSchedule Document
        f = new File(drive + "\\eGP\\Document\\WorkScheduleDoc\\");
        if(!f.exists()){
            f.mkdirs();
        } 
        
        //CSV Documents
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\CSVFiles");
        if(!f.exists()){
            f.mkdirs();
        }

        //DeliverySchedule Document
        f = new File(drive + "\\eGP\\Document\\DeliveryScheduleDoc\\");
        if(!f.exists()){
            f.mkdirs();
        }
        
        //DeliverySchedule Document
        f = new File(drive + "\\eGP\\Document\\CommencementDoc\\");
        if(!f.exists()){
            f.mkdirs();
        }
        
        //VariationOrder Document
        f = new File(drive + "\\eGP\\Document\\VariationOrderDoc\\");
        if(!f.exists()){
            f.mkdirs();
        }

        //Accountant Document
        f = new File(drive + "\\eGP\\Document\\AccountantDocuments\\");
        if(!f.exists()){
            f.mkdirs();
        }

        //Officer Document
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\InvoiceDoc\\");
        if(!f.exists()){
            f.mkdirs();
        }

        //bidder documents
        f = new File(drive + "\\eGP\\Document\\BidderDocuments\\");
        if(!f.exists()){
            f.mkdirs();
        }
        
        //AppDocument
        f = new File(drive + "\\eGP\\Document\\AppDocuments\\");
        if(!f.exists()){
            f.mkdir();
        }
        filePath.put("AppDownloadDocs", drive + "\\eGP\\Document\\AppDocuments\\");

        //Vendor Docs
        f = new File(driveVendorDoc + "\\eGP\\Document\\VendorDocuments\\PreTenderDocs\\");
        if(!f.exists()){
            f.mkdir();
        }
        f = new File(driveVendorDoc + "\\eGP\\Document\\VendorDocuments\\");
        if(!f.exists()){
            f.mkdir();
        }
        f = new File(driveVendorDoc + "\\eGP\\Document\\VendorDocuments\\NegQueryReplyDocument\\");
        if(!f.exists()){
            f.mkdir();
        }
        f = new File(driveVendorDoc + "\\eGP\\Document\\VendorDocuments\\ClarrificationDocument\\");
        if(!f.exists()){
            f.mkdir();
        }
        filePath.put("PreTenderQueryDocServlet", driveVendorDoc + "\\eGP\\Document\\VendorDocuments\\PreTenderDocs\\");
        filePath.put("FileUploadServlet", driveVendorDoc + "\\eGP\\Document\\VendorDocuments\\");
        filePath.put("ReportUploadServlet", driveVendorDoc + "\\eGP\\Document\\VendorDocuments\\Test");
        filePath.put("ServletNegReplyDocs", driveVendorDoc + "\\eGP\\Document\\VendorDocuments\\NegQueryReplyDocument\\");
        filePath.put("ServletEvalClariDocsVendor", driveVendorDoc + "\\eGP\\Document\\VendorDocuments\\ClarrificationDocument\\");

        //Debarment
        f = new File(driveVendorDoc + "\\eGP\\Document\\VendorDocuments\\Debarment\\");
        if(!f.exists()){
            f.mkdir();
        }
        f = new File(drive + "\\eGP\\Document\\OfficerDocuments\\Debarment\\");
        if(!f.exists()){
            f.mkdir();
        }
        filePath.put("DebarmentTenderer", driveVendorDoc + "\\eGP\\Document\\VendorDocuments\\Debarment\\");
        filePath.put("DebarmentOfficer", drive + "\\eGP\\Document\\OfficerDocuments\\Debarment\\");

        return filePath;
    }

    /**
     * Set file path
     * @param filePath
     */
    public static void setFilePath(Map<String, String> filePath) {
        FilePathUtility.filePath = filePath;
    }
}
