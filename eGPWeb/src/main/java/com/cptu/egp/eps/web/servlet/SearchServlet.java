/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonAppSearchData;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.service.serviceinterface.AppAdvSearchService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class SearchServlet extends HttpServlet
{

    private final AppAdvSearchService appAdvSearchService = (AppAdvSearchService) AppContext.getSpringBean("AppAdvSearchService");
    private static final Logger LOGGER = Logger.getLogger(SearchServlet.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND  = "Ends";
    private String logUserId ="0";
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
            appAdvSearchService.setLogUserId(logUserId);
        }
        LOGGER.debug("processRequest : "+logUserId+ LOGGERSTART);
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        List<CommonAppSearchData> commonData = null;
        try {
            String keyword = request.getParameter("keyWord");
            String styleClass = "";

            if (keyword == null) {
                String strUserId = request.getParameter("userId");
                int userId = 0;
                if (strUserId != null) {
                    userId = Integer.parseInt(strUserId);
                }
                String strPageNo = request.getParameter("pageNo");
                //System.out.println("Page No: " + strPageNo);
                int pageNo = Integer.parseInt(strPageNo);
                String strOffset = request.getParameter("size");
                int recordOffset = Integer.parseInt(strOffset);
                String action = request.getParameter("action");
               if (action != null && "Search".equalsIgnoreCase(action)) {

                    LOGGER.debug("processRequest : action : Search : "+logUserId+ LOGGERSTART);
                    String department = "";
                    Integer deptId = 0;
                    if(request.getParameter("departmentId")!=null && !"".equalsIgnoreCase(request.getParameter("departmentId"))){
                        department = request.getParameter("departmentId");
                        deptId = Integer.parseInt(department);
                    }
                    
                    Integer officeId = 0;
                    if(request.getParameter("office")!=null && !"".equalsIgnoreCase(request.getParameter("office").trim())){
                        officeId = Integer.parseInt(request.getParameter("office"));
                    }

                    String project = "";
                    if(request.getParameter("project")!=null && !"".equalsIgnoreCase(request.getParameter("project"))){
                        project = request.getParameter("project");
                    }
                    
                    String financialYear = "";
                    if(request.getParameter("financialYear")!=null && !"".equalsIgnoreCase(request.getParameter("financialYear"))){
                        financialYear = request.getParameter("financialYear");
                    }

                    String budgetType ="";
                    if(request.getParameter("budgetType")!=null && !"".equalsIgnoreCase(request.getParameter("budgetType"))){
                        budgetType = request.getParameter("budgetType");
                    }

                    String cpvCat = "";
                    if(request.getParameter("cpvCat")!=null && !"".equalsIgnoreCase(request.getParameter("cpvCat"))){
                        cpvCat = request.getParameter("cpvCat");
                    }
                    
                    String procNature = "";
                    if(request.getParameter("procNature")!=null && !"".equalsIgnoreCase(request.getParameter("procNature"))){
                        procNature = request.getParameter("procNature");
                    }
                    
                    String procType = "";
                    if(request.getParameter("procType")!=null && !"".equalsIgnoreCase(request.getParameter("procType"))){
                        procType = request.getParameter("procType");
                    }
                    
                    String strAppId = "";
                    Integer appId = 0;
                    if(request.getParameter("appId")!=null && !"".equalsIgnoreCase(request.getParameter("appId"))){
                        strAppId = request.getParameter("appId");
                        appId = Integer.parseInt(strAppId);
                    }
                    
                    String appCode = " ";
                    if(request.getParameter("appCode")!=null && !"".equalsIgnoreCase(request.getParameter("appCode"))){
                        appCode = request.getParameter("appCode");
                    }
                    
                    String pkgNo = " ";
                    if(request.getParameter("pkgNo")!=null && !"".equalsIgnoreCase(request.getParameter("pkgNo"))){
                        pkgNo = request.getParameter("pkgNo");
                    }
                    
                    String value = "";
                    Integer intValue = 0;
                    if(request.getParameter("value")!=null && !"".equalsIgnoreCase(request.getParameter("value"))){
                        value = request.getParameter("value");
                        intValue = Integer.parseInt(value);
                    }

                    String operation = " ";
                    if(request.getParameter("operation")!=null && !"".equalsIgnoreCase(request.getParameter("operation"))){
                        operation = request.getParameter("operation");
                    }

                    Integer intValue2 = 0;
                    if(!"".equalsIgnoreCase(value)){
                        if("Between".equalsIgnoreCase(operation)){
                            if(request.getParameter("value2")!=null && !"".equalsIgnoreCase(request.getParameter("value2"))){
                                intValue2 = Integer.parseInt(request.getParameter("value2"));
                            }
                        }
                    }
                    
                   commonData = appAdvSearchService.getSearchResults(keyword, financialYear, procNature, deptId, 0, cpvCat, project, procType, appId, appCode, pkgNo, new BigDecimal(0), officeId, budgetType, operation, new BigDecimal(intValue), new BigDecimal(intValue2), pageNo, recordOffset, "", userId);
                }
                else{
                    
                   String department = "";
                    Integer deptId = 0;
                    if(request.getParameter("departmentId")!=null && !"".equalsIgnoreCase(request.getParameter("departmentId"))){
                        department = request.getParameter("departmentId");
                        deptId = Integer.parseInt(department);
                    }
                    
                    Integer officeId = 0;
                    if(request.getParameter("office")!=null && !"".equalsIgnoreCase(request.getParameter("office").trim())){
                        officeId = Integer.parseInt(request.getParameter("office"));
                    }

                    String project = "";
                    if(request.getParameter("project")!=null && !"".equalsIgnoreCase(request.getParameter("project"))){
                        project = request.getParameter("project");
                    }
                    
                    String financialYear = "";
                    if(request.getParameter("financialYear")!=null && !"".equalsIgnoreCase(request.getParameter("financialYear"))){
                        financialYear = request.getParameter("financialYear");
                    }

                    String budgetType ="";
                    if(request.getParameter("budgetType")!=null && !"".equalsIgnoreCase(request.getParameter("budgetType"))){
                        budgetType = request.getParameter("budgetType");
                    }

                    String cpvCat = "";
                    if(request.getParameter("cpvCat")!=null && !"".equalsIgnoreCase(request.getParameter("cpvCat"))){
                        cpvCat = request.getParameter("cpvCat");
                    }
                    
                    String procNature = "";
                    if(request.getParameter("procNature")!=null && !"".equalsIgnoreCase(request.getParameter("procNature"))){
                        procNature = request.getParameter("procNature");
                    }
                    
                    String procType = "";
                    if(request.getParameter("procType")!=null && !"".equalsIgnoreCase(request.getParameter("procType"))){
                        procType = request.getParameter("procType");
                    }
                    
                    String strAppId = "";
                    Integer appId = 0;
                    if(request.getParameter("appId")!=null && !"".equalsIgnoreCase(request.getParameter("appId"))){
                        strAppId = request.getParameter("appId");
                        appId = Integer.parseInt(strAppId);
                    }
                    
                    String appCode = " ";
                    if(request.getParameter("appCode")!=null && !"".equalsIgnoreCase(request.getParameter("appCode"))){
                        appCode = request.getParameter("appCode");
                    }
                    
                    String pkgNo = " ";
                    if(request.getParameter("pkgNo")!=null && !"".equalsIgnoreCase(request.getParameter("pkgNo"))){
                        pkgNo = request.getParameter("pkgNo");
                    }
                    
                    String value = "";
                    Integer intValue = 0;
                    if(request.getParameter("value")!=null && !"".equalsIgnoreCase(request.getParameter("value"))){
                        value = request.getParameter("value");
                        intValue = Integer.parseInt(value);
                    }


                    String operation = " ";
                    if(request.getParameter("operation")!=null && !"".equalsIgnoreCase(request.getParameter("operation"))){
                        operation = request.getParameter("operation");
                    }

                    Integer intValue2 = 0;
                    if(!"".equalsIgnoreCase(value)){
                        if("Between".equalsIgnoreCase(operation)){
                            if(request.getParameter("value2")!=null && !"".equalsIgnoreCase(request.getParameter("value2"))){
                                intValue2 = Integer.parseInt(request.getParameter("value2"));
                            }
                        }
                    }
                    //commonData = appAdvSearchService.getSearchResults(keyword, "", "", 0, 0, "", "", "", 0, "", "", new BigDecimal(0), 0, "", "", new BigDecimal(0), new BigDecimal(0), pageNo, recordOffset, "", userId);
                    commonData = appAdvSearchService.getSearchResults(keyword, financialYear, procNature, deptId, 0, cpvCat, project, procType, appId, appCode, pkgNo, new BigDecimal(0), officeId, budgetType, operation, new BigDecimal(intValue), new BigDecimal(intValue2), pageNo, recordOffset, "", userId);
                }
                if (commonData != null && !commonData.isEmpty()) {
                    StringBuilder strProc = new StringBuilder();
                    for (int i = 0; i < commonData.size(); i++) {
                        if(i%2==0){
                            styleClass = "bgColor-white";
                        }else{
                            styleClass = "bgColor-Green";
                        }
                        strProc.delete(0, strProc.length());
                        CommonAppSearchData commonAppSearchData = commonData.get(i);
                        
                        /*GSS APP revised packages */
                        if("BeingRevised".equalsIgnoreCase(commonAppSearchData.getStatus())){
                            CommonSearchService searchService=(CommonSearchService)AppContext.getSpringBean("CommonSearchService");
                            List<SPCommonSearchData> list=searchService.searchData("getTempPkgDetails", String.valueOf(commonAppSearchData.getPackageId()), "", "", "", "", "", "", "", "");
                            if(list!=null && !list.isEmpty()){
                               SPCommonSearchData tempPkgData=list.get(0);
                               commonAppSearchData.setProcurementNature(tempPkgData.getFieldName1());
                               commonAppSearchData.setPackageNo(tempPkgData.getFieldName2());
                               commonAppSearchData.setPackageDesc(tempPkgData.getFieldName3());
                               commonAppSearchData.setEstimatedCost(new BigDecimal(tempPkgData.getFieldName4()));
                               commonAppSearchData.setProcurementMethod(tempPkgData.getFieldName5());
                               
                            }
                        }
                        
                        /* GSS APP revised packages  */

                        out.print("<tr class='"+styleClass+"'>");
                        out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * 10 + (i + 1)) + "</td>");
                        out.print("<td class=\"t-align-center\">" + commonAppSearchData.getAppId() + ",<br/>" + commonAppSearchData.getAppCode() + "</td>");
                        out.print("<td class=\"t-align-left\">" + commonAppSearchData.getDepartmentName() + ",<br />" + commonAppSearchData.getOfficeName() + "</td>");
                       // out.print("<td class=\"t-align-center\">" + commonAppSearchData.getStateName() + "</td>");
                        strProc.append(commonAppSearchData.getProcurementNature());
                        if (strProc.toString().equals("null")) {
                            strProc.delete(0, strProc.length());
                            strProc.append("-");
                        }
                        out.print("<td class=\"t-align-left\">" + strProc + "," + commonAppSearchData.getProjectName() + "</td>");

                        String viewList =  "ViewAppDetails.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "&pkgType=Package&from=home";

                        if("BeingRevised".equalsIgnoreCase(commonAppSearchData.getStatus())){
                        viewList =  "ViewAppDetails.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "&pkgType=RevisePackage&from=home";
                        }
                        out.print("<td class=\"t-align-left\">" + commonAppSearchData.getPackageNo() + ",<br/><a href='"+viewList+"' target = '_blank'>" + commonAppSearchData.getPackageDesc() + "</a></td>");

                        out.print("<td class=\"t-align-center\">" + commonAppSearchData.getEstimatedCost().setScale(2, 0) + ",<br />");
                        if(commonAppSearchData.getProcurementMethod().equalsIgnoreCase("RFQ"))
                            out.print("LEM</td>");
                        else if(commonAppSearchData.getProcurementMethod().equalsIgnoreCase("DPM"))
                            out.print("DCM</td>");
                        else
                            out.print(commonAppSearchData.getProcurementMethod() + "</td>");
                        out.print("</tr>");
                    }
                    int totalPages = 0;
                    if (!commonData.isEmpty()) {
                        totalPages = (int) (Math.ceil(commonData.get(0).getTotalRecords() / recordOffset));
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                }
                else {
                    out.print("<tr>");
                    out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"7\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }
               
                out.flush();
            }else if (keyword != null ) {
                String strPageNo = request.getParameter("pageNo");
                int officeId = Integer.parseInt(request.getParameter("office"));

                int pageNo = Integer.parseInt(strPageNo);
                String strOffset = request.getParameter("size");
                int recordOffset = Integer.parseInt(strOffset);
                //String keyWord=request.getParameter("q");
                commonData = appAdvSearchService.getSearchResults(keyword, "", "", 0, 0, " ", "", "", 0, "", "", new BigDecimal(0), officeId, "", "", new BigDecimal(0), new BigDecimal(0), pageNo, recordOffset, "", 0);

                if (commonData != null && !commonData.isEmpty()) {
                    StringBuilder strProc = new StringBuilder();
                    for (int i = 0; i < commonData.size(); i++) {
                        if(i%2==0){
                            styleClass = "bgColor-white";
                        }else{
                            styleClass = "bgColor-Green";
                        }
                        strProc.delete(0, strProc.length());
                        CommonAppSearchData commonAppSearchData = commonData.get(i);
                        out.print("<tr class='"+styleClass+"'>");
                        out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * 10 + (i + 1)) + "</td>");
                        out.print("<td class=\"t-align-center\">" + commonAppSearchData.getAppId() + ",<br/>" + commonAppSearchData.getAppCode() + "</td>");
                        out.print("<td class=\"t-align-left\">" + commonAppSearchData.getDepartmentName() + ",<br />" + commonAppSearchData.getOfficeName() + "</td>");
                        out.print("<td class=\"t-align-center\">" + commonAppSearchData.getStateName() + "</td>");
                        strProc.append(commonAppSearchData.getProcurementNature());
                        if (strProc.toString().equals("null")) {
                            strProc.delete(0, strProc.length());
                        }
                        out.print("<td class=\"t-align-left\">" + strProc + ",<br />" + commonAppSearchData.getProjectName() + "</td>");
                        String viewList =  "ViewAppDetails.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "&pkgType=Package&from=home";
                        //out.print("<td class=\"t-align-center\">" + commonAppSearchData.getPackageNo() + ",<br/><a target=\"_blank\" href=\"" + viewList + "\">" + commonAppSearchData.getPackageDesc() + "</a></td>");
                        out.print("<td class=\"t-align-center\">" + commonAppSearchData.getPackageNo() + ",<br/><a href='"+viewList+"' target = '_blank'>" + commonAppSearchData.getPackageDesc() + "</a></td>");
                        out.print("<td class=\"t-align-center\">" + commonAppSearchData.getEstimatedCost().setScale(2, 0) + ",<br />");
                        if(commonAppSearchData.getProcurementMethod().equalsIgnoreCase("RFQ"))
                            out.print("LEM</td>");
                        else if(commonAppSearchData.getProcurementMethod().equalsIgnoreCase("DPM"))
                            out.print("DCM</td>");
                        else
                            out.print(commonAppSearchData.getProcurementMethod() + "</td>");
                        out.print("</tr>");
                    }
                    int totalPages = 1;

                    if (!commonData.isEmpty()) {
                        totalPages = (int) (Math.ceil(commonData.get(0).getTotalRecords() / recordOffset));
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                }else {
                    out.print("<tr>");
                    out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"7\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }
            }
            LOGGER.debug("processRequest : action : Search : "+logUserId+ LOGGEREND);

        }catch(Exception ex){

             LOGGER.error("processRequest "+logUserId+" : "+ex.toString());
             
        }finally {
            out.close();
        }
        LOGGER.debug("processRequest : "+logUserId+ LOGGEREND);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
}
