/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblNewsMaster;
import com.cptu.egp.eps.service.serviceimpl.NewsMasterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.XMLReader;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.File;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import com.cptu.egp.eps.web.utility.*;

/**
 *
 * @author rishita
 */
public class NewsMasterSrBean {

    private static final Logger LOGGER = Logger.getLogger(NegotiationProcessSrBean.class);
    private String logUserId ="0";
    private final NewsMasterService newsMasterService = (NewsMasterService)AppContext.getSpringBean("NewsMasterService");
    private AuditTrail auditTrail;

    /**
     * Set user id for log purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        newsMasterService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /**
     * set Audit trail data for Audit Trail Purpose
     * @param auditTrail
     */
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        newsMasterService.setAuditTrail(auditTrail);
    }
    
    
    /**
     * Method is use for add News details
     * @param newsMaster
     * @return true if successfully added else return false
     */
//     public boolean insertNewsDetails(TblNewsMaster newsMaster){
//        LOGGER.debug("insertNewsDetails : "+logUserId+" Starts");
//        boolean flag = false;
//        try{
//            newsMasterService.insertNewsDetails(newsMaster);
//            flag = true;
//        }catch(Exception ex){
//           LOGGER.error("insertNewsDetails : "+logUserId+" : "+ex.toString());
//        }
//        LOGGER.debug("insertNewsDetails : "+logUserId+" Ends");
//        return flag;
//    }
    
    public boolean insertNewsDetails(HttpServletRequest request, String NewsType){
        LOGGER.debug("insertNewsDetails : "+logUserId+" Starts");
        boolean flag = false;
        try{
            String drive = XMLReader.getMessage("driveLatter");
            
            short cmbContentType = 0;
            String IsImportantNews = "No";
            boolean ImportantNews = false;
            String txtNews = "";
            String txtNewsDetl = "";
            char newsType = NewsType.toCharArray()[0];
            
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            List items = upload.parseRequest(request);
            Iterator iterator = items.iterator();
            
            while (iterator.hasNext()) {
                FileItem item = (FileItem) iterator.next();
                if (item.isFormField()) {    
                    if(item.getFieldName().equals("cmbContentType"))
                    {
                        cmbContentType = Short.parseShort(item.getString());
                    }
                    else if(item.getFieldName().equals("chkImportantNews"))
                    {
                        ImportantNews = true;
                    }
                    else if(item.getFieldName().equals("txtNews"))
                    {
                        txtNews = item.getString();
                    }
                    else if(item.getFieldName().equals("txtNewsDetl"))
                    {
                        txtNewsDetl = item.getString();
                    }
                }
            }
            
            if(ImportantNews)
            {
                IsImportantNews = "Yes";
            }
            
            TblNewsMaster newsMaster = new TblNewsMaster(0, cmbContentType, newsType, IsImportantNews, txtNews, txtNewsDetl, new Date(), "No", "No", null, null);
            newsMasterService.insertNewsDetails(newsMaster);
            flag = true;
        }catch(Exception ex){
           LOGGER.error("insertNewsDetails : "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("insertNewsDetails : "+logUserId+" Ends");
        return flag;
    }
     
     public boolean insertCircularDetails(HttpServletRequest request , String NewsType){
        LOGGER.debug("insertNewsDetails : "+logUserId+" Starts");
        boolean flag = false;
        try{
            String drive = XMLReader.getMessage("driveLatter");
            
            String UploadedFIle = "";
            
            File f = null;
            f = new File(drive + "\\eGP\\Document\\NewsDocuments\\");
            if (!f.exists()) {
                f.mkdir();
            }
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            List items = upload.parseRequest(request);
            Iterator iterator = items.iterator();
            Iterator iterator2 = items.iterator();
            
            short cmbContentType = 0;
            String IsImportantNews = "No";
            boolean ImportantNews = false;
            String txtNews = "";
            String txtNewsDetl = "";
            String LetterNumber = "";
            String fileName = "";
            Date Time = null;
            String FileUploadTime = "";
            char newsType = NewsType.toCharArray()[0];
            
            while (iterator.hasNext()) {
                FileItem item = (FileItem) iterator.next();

                if (item.isFormField())
                {
                    if(item.getFieldName().equals("cmbContentType"))
                    {
                        cmbContentType = Short.parseShort(item.getString());
                    }
                    else if(item.getFieldName().equals("chkImportantNews"))
                    {
                        ImportantNews = true;
                    }
                    else if(item.getFieldName().equals("txtNews"))
                    {
                        txtNews = item.getString();
                    }
                    else if(item.getFieldName().equals("txtNewsDetl"))
                    {
                        txtNewsDetl = item.getString();
                    }
                    else if(item.getFieldName().equals("LetterNumber"))
                    {
                        LetterNumber = item.getString();
                    }
                    else if(item.getFieldName().equals("Time"))
                    {
                        Time = DateUtils.convertDateToStr(item.getString());
                        FileUploadTime = item.getString();
                        char[] TimecharArray = FileUploadTime.toCharArray();
                        for(int i=0; i<TimecharArray.length; i++)
                        {
                            if(TimecharArray[i]=='/' || TimecharArray[i]==':')
                            {
                                TimecharArray[i] = '-';
                            }
                        }
                        FileUploadTime = new String(TimecharArray);
                    }
                }
            }
            
            if(ImportantNews)
            {
                IsImportantNews = "Yes";
            }
            
            while (iterator2.hasNext()) 
            {
                FileItem item = (FileItem) iterator2.next();
                if (!item.isFormField()) {
                    if(item.getName()!=null && !item.getName().equals(""))
                    {
                        fileName = txtNews + FileUploadTime + item.getName();
                        File uploadedFile = new File(drive + "\\eGP\\Document\\NewsDocuments\\" + fileName);
                        System.out.println(uploadedFile.getAbsolutePath());
                        item.write(uploadedFile);
                    }
                }
            }
            
            TblNewsMaster newsMaster = new TblNewsMaster(0, cmbContentType, newsType, IsImportantNews, txtNews, txtNewsDetl, Time, "No", "No", fileName, LetterNumber);    
            newsMasterService.insertNewsDetails(newsMaster);
            flag = true;
        }catch(Exception ex){
           LOGGER.error("insertNewsDetails : "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("insertNewsDetails : "+logUserId+" Ends");
        return flag;
    }

     /**
      * Method is use for update News details.
      * @param newsMaster
      * @return true if successfully updated else return false
      */
    public boolean updateNewsDetails(HttpServletRequest request, String NewsType, String NId){
        LOGGER.debug("updateNewsDetails : "+logUserId+" Starts");
        boolean flag = false;
        try{
            String drive = XMLReader.getMessage("driveLatter");
            
            short cmbContentType = 0;
            String IsImportantNews = "No";
            boolean ImportantNews = false;
            String txtNews = "";
            String txtNewsDetl = "";
            char newsType = NewsType.toCharArray()[0];
            int NewsId = Integer.parseInt(NId);
            
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            List items = upload.parseRequest(request);
            Iterator iterator = items.iterator();
            
            while (iterator.hasNext()) {
                FileItem item = (FileItem) iterator.next();
                if (item.isFormField()) {    
                    if(item.getFieldName().equals("cmbContentType"))
                    {
                        cmbContentType = Short.parseShort(item.getString());
                    }
                    else if(item.getFieldName().equals("chkImportantNews"))
                    {
                        ImportantNews = true;
                    }
                    else if(item.getFieldName().equals("txtNews"))
                    {
                        txtNews = item.getString();
                    }
                    else if(item.getFieldName().equals("txtNewsDetl"))
                    {
                        txtNewsDetl = item.getString();
                    }
                }
            }
            
            if(ImportantNews)
            {
                IsImportantNews = "Yes";
            }
            
            TblNewsMaster newsMaster = new TblNewsMaster(NewsId, cmbContentType, newsType, IsImportantNews, txtNews, txtNewsDetl, new Date(), "No", "No", null, null);
            newsMasterService.updateNewsDetails(newsMaster);
            flag = true;
        }catch(Exception ex){
           LOGGER.error("updateNewsDetails : "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("updateNewsDetails : "+logUserId+" Ends");
        return flag;
    }
    public boolean updateCircularDetails(HttpServletRequest request, String NewsType, String NId){
        LOGGER.debug("updateNewsDetails : "+logUserId+" Starts");
        boolean flag = false;
        try{
            String drive = XMLReader.getMessage("driveLatter");
            
            String UploadedFIle = "";
            
            File f = null;
            f = new File(drive + "\\eGP\\Document\\NewsDocuments\\");
            if (!f.exists()) {
                f.mkdir();
            }
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            List items = upload.parseRequest(request);
            Iterator iterator = items.iterator();
            Iterator iterator2 = items.iterator();
            
            short cmbContentType = 0;
            String IsImportantNews = "No";
            boolean ImportantNews = false;
            String txtNews = "";
            String txtNewsDetl = "";
            String LetterNumber = "";
            String fileName = "";
            Date Time = null;
            String FileUploadTime = "";
            char newsType = NewsType.toCharArray()[0];
            int NewsId = Integer.parseInt(NId);
            
            while (iterator.hasNext()) {
                FileItem item = (FileItem) iterator.next();

                if (item.isFormField())
                {
                    if(item.getFieldName().equals("cmbContentType"))
                    {
                        cmbContentType = Short.parseShort(item.getString());
                    }
                    else if(item.getFieldName().equals("chkImportantNews"))
                    {
                        ImportantNews = true;
                    }
                    else if(item.getFieldName().equals("txtNews"))
                    {
                        txtNews = item.getString();
                    }
                    else if(item.getFieldName().equals("txtNewsDetl"))
                    {
                        txtNewsDetl = item.getString();
                    }
                    else if(item.getFieldName().equals("LetterNumber"))
                    {
                        LetterNumber = item.getString();
                    }
                    else if(item.getFieldName().equals("Time"))
                    {
                        Time = DateUtils.convertDateToStr(item.getString());
                        FileUploadTime = item.getString();
                        char[] TimecharArray = FileUploadTime.toCharArray();
                        for(int i=0; i<TimecharArray.length; i++)
                        {
                            if(TimecharArray[i]=='/' || TimecharArray[i]==':')
                            {
                                TimecharArray[i] = '-';
                            }
                        }
                        FileUploadTime = new String(TimecharArray);
                    }
                }
            }
            
            if(ImportantNews)
            {
                IsImportantNews = "Yes";
            }
            
            while (iterator2.hasNext())
            {
                FileItem item = (FileItem) iterator2.next();
                if (!item.isFormField()) {
                    if(item.getName()!=null && !item.getName().equals(""))
                    {
                        fileName = txtNews + FileUploadTime + item.getName();
                        File uploadedFile = new File(drive + "\\eGP\\Document\\NewsDocuments\\" + fileName);
                        System.out.println(uploadedFile.getAbsolutePath());
                        item.write(uploadedFile);
                    }
                }
            }
            TblNewsMaster newsMaster = new TblNewsMaster(NewsId, cmbContentType, newsType, IsImportantNews, txtNews, txtNewsDetl, Time, "No", "No", fileName, LetterNumber);    
            if(fileName!=null && !fileName.equals(""))
            {
                newsMasterService.updateCircularDetails(newsMaster);
            }
            else
            {
                newsMasterService.updateCircularDetailsExceptFile(newsMaster);
            }
            
            flag = true;
        }catch(Exception ex){
           LOGGER.error("updateNewsDetails : "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("updateNewsDetails : "+logUserId+" Ends");
        return flag;
    }
}
