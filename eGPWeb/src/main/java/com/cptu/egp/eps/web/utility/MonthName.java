/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

import java.util.HashMap;

/**
 *
 * @author shreyansh Jogi
 */
public class MonthName {

    private static HashMap<Integer, String> map = null;

    public MonthName() {
        if (map == null) {
            map = new HashMap<Integer, String>();
            map.put(1, "Jan");
            map.put(2, "Feb");
            map.put(3, "Mar");
            map.put(4, "Apr");
            map.put(5, "May");
            map.put(6, "Jun");
            map.put(7, "July");
            map.put(8, "Aug");
            map.put(9, "Sept");
            map.put(10, "Oct");
            map.put(11, "Nov");
            map.put(12, "Dec");
        }
    }

    public static String getMonth(int key) {
        return map.get(key);
    }

    public static int getMonthDays(int month, int year) {
        int days = 0;
        if (year % 4 == 0) {
            if (month == 2) {
                days = 29;
            } else {
                days = 28;
            }
        } else {
        }
        switch (month) {
            case 1:
                days = 31;
                break;
            case 2:
                if (year % 4 == 0) {
                    days = 29;
                } else {
                    days = 28;
                }
                break;
            case 3:
                days = 31;
                break;
            case 4:
                days = 30;
                break;
            case 5:
                days = 31;
                break;
            case 6:
                days = 30;
                break;
            case 7:
                days = 31;
                break;
            case 8:
                days = 31;
                break;
            case 9:
                days = 30;
                break;
            case 10:
                days = 31;
                break;
            case 11:
                days = 30;
                break;
            case 12:
                days = 31;
                break;

        }
        return days;
    }
}
