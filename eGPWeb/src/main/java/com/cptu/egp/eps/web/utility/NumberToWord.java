/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

/**
 *
 * @author Nafiul
 */
public class NumberToWord {
    public static String Convert(String numeric){
    String WordForm = "";
    
    String[] th = {"","THOUSAND","MILLION", "BILLION","TRILLION"};
    String[] dg = {"ZERO","ONE","TWO","THREE","FOUR", "FIVE","SIX","SEVEN","EIGHT","NINE"};
    String[] tn = {"TEN","ELEVEN","TWELVE","THIRTEEN", "FOURTEEN","FIFTEEN","SIXTEEN", "SEVENTEEN","EIGHTEEN","NINETEEN"};
    String[] tw = {"TWENTY","THIRTY","FORTY","FIFTY", "SIXTY","SEVENTY","EIGHTY","NINETY"};

    float numericValue;
    int ShouldConvertFlag = 1;
//    var StringFormatNumber = NumericValue.toString();
//    StringFormatNumber = StringFormatNumber.replace(/[\, ]/g,'');
//    if (StringFormatNumber != parseFloat(StringFormatNumber)) 
//    {
//        WordForm += "NOT A NUMBER";
//        ShouldConvertFlag = 0;
//    }
    
    try{
        numericValue = Float.parseFloat(numeric);
    }
    catch (Exception e){return "NOT A NUMBER";}
    int LengthUptoPrecision = 0;
    if(ShouldConvertFlag==1)
    {
        LengthUptoPrecision = numeric.indexOf('.');
        if (LengthUptoPrecision == -1)
            LengthUptoPrecision = numeric.length();
        if (LengthUptoPrecision > 15)
        {
            WordForm += "TOO BIG NUMBER";
            ShouldConvertFlag = 0;
        }
    }

    if(ShouldConvertFlag==1)
    {
        String[] EachDigit = numeric.split(""); 
        int TraversStart = 0;
        if(EachDigit[0].equalsIgnoreCase("-"))
        {
            TraversStart++;
            WordForm += "MINUS ";
        }
        int sk = 0;
        for (int i=TraversStart;   i < LengthUptoPrecision;  i++) 
        {
            if ((LengthUptoPrecision-i)%3==2) 
            { 
                if (EachDigit[i].equalsIgnoreCase("1")) 
                {
                    WordForm += tn[Integer.parseInt(EachDigit[i+1])] + " ";
                    i++;
                    sk=1;
                } 
                else if (!EachDigit[i].equalsIgnoreCase("0")) 
                {
                    WordForm += tw[Integer.parseInt(EachDigit[i])-2] + " ";
                    sk=1;
                }
            } 
            else if (!EachDigit[i].equalsIgnoreCase("0")) 
            { // 0235
                WordForm += dg[Integer.parseInt(EachDigit[i])] + " ";
                if ((LengthUptoPrecision-i)%3==0) 
                    WordForm += "HUNDRED ";
                sk=1;
            }
            else if (EachDigit[i].equalsIgnoreCase("0") && (LengthUptoPrecision-TraversStart)==1) 
            { // 0.00
                WordForm += "ZERO ";
            }
            if ((LengthUptoPrecision-i)%3==1) 
            {
                if (sk==1)
                    WordForm += th[(LengthUptoPrecision-i-1)/3] + " ";
                sk=0;
            }
        }

        if (LengthUptoPrecision != numeric.length()) 
        {
            int y = numeric.length();
            WordForm += "POINT ";
            for (int i=LengthUptoPrecision+1; i<y; i++)
                WordForm += dg[Integer.parseInt(EachDigit[i])] +" ";
        }
    }
    //WordForm.replace(/\s+/g," ");

    //document.getElementById(msgPlace).innerHTML = WordForm;
    return WordForm;
    }
    
}
