/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblEvalTsccomments;
import com.cptu.egp.eps.service.serviceimpl.EvalTSCNotificationService;
import com.cptu.egp.eps.service.serviceinterface.EvalTscCommentsService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class EvalTscCommentsSrBean {

    final Logger logger = Logger.getLogger(EvalTscCommentsSrBean.class);

    String logUserId = "0";
    

    EvalTscCommentsService evalTscCommentsService = (EvalTscCommentsService)AppContext.getSpringBean("EvalTscCommentsService");
    EvalTSCNotificationService evalTSCNotificationService = (EvalTSCNotificationService) AppContext.getSpringBean("EvalTSCNotificationService");

    /**
     * When doing logging for userId
     * @param logUserId : session userId
     */
    public void setLogUserId(String logUserId) {
        evalTscCommentsService.setLogUserId(logUserId);
    }
    
    /**
     *Get comments for tenderId and formId
     * @param tenderId
     * @param formId
     * @return List of object[] [0] commentsId,[1 ]comments, [2] employeeName, [3] tscUserId
     */
    public List<Object[]> getComments(int tenderId,int formId){
        logger.debug("getComments "+logUserId+" Starts");
        List<Object[]> list = null;
        try {
            list =  evalTscCommentsService.getComments(tenderId,formId);
        } catch (Exception e) {
            logger.error("getComments "+logUserId+" :"+e);
    }
        logger.debug("getComments "+logUserId+" Ends");
        return  list;
    }
    /**
     *For deleting comments
     * @param commentId
     * @return boolean
     */
    public boolean delComment(int commentId){
        logger.debug("delComment "+logUserId+" Starts");
        boolean flag = false;
        try {
                flag = evalTscCommentsService.delRec(commentId);
        } catch (Exception e) {
            logger.error("delComment "+logUserId+" :"+e);
    }
        logger.debug("delComment "+logUserId+" Ends");
        return flag;
    }
    /**
     *get Comments as per commentId
     * @param commentId
     * @return list object[0] Id, object[1] Comments,  
     */
    public TblEvalTsccomments getComments(int commentId){
        logger.debug("getComments "+logUserId+" Starts");
        TblEvalTsccomments tblEvalTsccomments = new TblEvalTsccomments();
        try {
        tblEvalTsccomments = evalTscCommentsService.getComments(commentId);
        } catch (Exception e) {
            logger.error("getComments "+logUserId+" :"+e);
    }
        logger.debug("getComments "+logUserId+" Ends");
        return tblEvalTsccomments;
    }


    /**
     *Notify Button Display Check
     * @param commentId to be checked on
     * @return true or false for show or hide
     */
    public boolean displayNotifyButton(int commentId){
        logger.debug("displayNotifyButton "+logUserId+" Starts");
        boolean flag=false;
        try {
        flag = evalTscCommentsService.displayNotifyButton(commentId);
        } catch (Exception e) {
            logger.error("displayNotifyButton "+logUserId+" :"+e);
    }
        logger.debug("displayNotifyButton "+logUserId+" Ends");
        return flag;
    }


    /**
     *Check whether Member has notified Chairperson
     * @param tenderId from tbl_TenderMaster
     * @return true or false for notified or not notified
     */
    public boolean getNotification(String tenderId){
        logger.debug("getNotification "+logUserId+" Starts");
        boolean flag = false;
            try {
                flag = evalTSCNotificationService.isNotify(tenderId);
            } catch (Exception e) {
                logger.error("getNotification "+logUserId+" :"+e);
            }
            logger.debug("getNotification "+logUserId+" Ends");
        return flag;
    }

}
