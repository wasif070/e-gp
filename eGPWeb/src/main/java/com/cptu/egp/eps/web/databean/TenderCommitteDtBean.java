/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

/**
 *
 * @author Administrator
 */
public class TenderCommitteDtBean {

    private String committeeId;
    private String userid;
    private String tenderid;
    private String committeeName;
    private String minMembers;
    private String maxMembers;
    private String minMemOutSide;
    private String committeeType;
    private String minMemFromPe;
    private String minMemFrmTecPec = "0";
    private String[] memUserIds;
    private String[] memGovIds;
    private String[] memberRoles;
    private String[] memberFroms;

    public String[] getMemGovIds() {
        return memGovIds;
    }

    public void setMemGovIds(String[] memGovIds) {
        this.memGovIds = memGovIds;
    }

    public String getCommitteeId() {
        return committeeId;
    }

    public void setCommitteeId(String committeeId) {
        this.committeeId = committeeId;
    }

    public String getCommitteeName() {
        return committeeName;
    }

    public void setCommitteeName(String committeeName) {
        this.committeeName = committeeName;
    }

    public String getCommitteeType() {
        return committeeType;
    }

    public void setCommitteeType(String committeeType) {
        this.committeeType = committeeType;
    }

    public String getMaxMembers() {
        return maxMembers;
    }

    public void setMaxMembers(String maxMembers) {
        this.maxMembers = maxMembers;
    }

    public String[] getMemUserIds() {
        return memUserIds;
    }

    public void setMemUserIds(String[] memUserIds) {
        this.memUserIds = memUserIds;
    }

    public String[] getMemberFroms() {
        return memberFroms;
    }

    public void setMemberFroms(String[] memberFroms) {
        this.memberFroms = memberFroms;
    }

    public String[] getMemberRoles() {
        return memberRoles;
    }

    public void setMemberRoles(String[] memberRoles) {
        this.memberRoles = memberRoles;
    }

    public String getMinMemFromPe() {
        return minMemFromPe;
    }

    public void setMinMemFromPe(String minMemFromPe) {
        this.minMemFromPe = minMemFromPe;
    }

    public String getMinMemFrmTecPec() {
        return minMemFrmTecPec;
    }

    public void setMinMemFrmTecPec(String minMemFrmTecPec) {
        this.minMemFrmTecPec = minMemFrmTecPec;
    }

    public String getMinMemOutSide() {
        return minMemOutSide;
    }

    public void setMinMemOutSide(String minMemOutSide) {
        this.minMemOutSide = minMemOutSide;
    }

    public String getMinMembers() {
        return minMembers;
    }

    public void setMinMembers(String minMembers) {
        this.minMembers = minMembers;
    }

    public String getTenderid() {
        return tenderid;
    }

    public void setTenderid(String tenderid) {
        this.tenderid = tenderid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}

