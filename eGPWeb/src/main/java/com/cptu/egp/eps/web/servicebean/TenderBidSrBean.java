/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.CommonFormData;
import com.cptu.egp.eps.model.table.TblFinalSubDetail;
import com.cptu.egp.eps.model.table.TblFinalSubmission;
import com.cptu.egp.eps.model.table.TblTenderListCellDetail;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.service.serviceinterface.TenderBidService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class TenderBidSrBean {

    private String logUserId = "0";
    final Logger logger = Logger.getLogger(TenderBidSrBean.class);
    private TenderBidService tenderBidService = (TenderBidService) AppContext.getSpringBean("TenderBidService");

    public void setLogUserId(String logUserId) {
        tenderBidService.setUserId(logUserId);
        this.logUserId = logUserId;
        }

    /**
     * Get Table details like tenderTableId, tableName  ,tableHeader, tablefooter for given Tender Form.
     * @param formId = tbl_TenderTables.tenderFormId
     * @return List object of CommonFormData.
     */
    public List<CommonFormData> getFormTables(int formId) {

        logger.debug("getFormTables : " + logUserId + "starts");
        List<CommonFormData> commonform = null;
        try {
            commonform = tenderBidService.getTenderFormDetails(0, formId, 0, 0, 0, 0, "GetTableDataFormWise");
        } catch (Exception ex) {
            logger.error("getFormTables : " + ex);
    }

        logger.debug("getFormTables : " + logUserId + "ends");
        return commonform;

        }

    /**
     * Get Table details like tenderTableId, tableName  ,tableHeader, tablefooter for given Table Id.
     * @param tableId = tbl_TenderTables.tendertableId
     * @return List object of CommonFormData.
     */
    public List<CommonFormData> getFormTablesDetails(int tableId) {

        logger.debug("getFormTablesDetails : " + logUserId + "starts");
        List<CommonFormData> commonform = null;
        try {
            commonform = tenderBidService.getTenderFormDetails(tableId, 0, 0, 0, 0, 0, "GetTableDataTableWise");
        } catch (Exception ex) {
            logger.error("getFormTablesDetails : " + ex);

    }

        logger.debug("getFormTablesDetails : " + logUserId + "ends");
        return commonform;
        }

    /**
     * Check Column Count Exist for given table Id.
     * @param tableId = tbl_TenderColumns.tenderTableId
     * @return true if column count for tbl_TenderColumns is greater than 0 else return false.
     */
    public Boolean isEntryPresentInTableCols(int tableId) {

        logger.debug("isEntryPresentInTableCols : " + logUserId + "starts");
        Boolean flag = null;
        try {
            if (tenderBidService.getTenderFormDetails(tableId, 0, 0, 0, 0, 0, "GetColumnCount").get(0).getCount() > 0) {
                flag = true;
            } else {
                flag = false;
    }
        } catch (Exception ex) {
            logger.error("isEntryPresentInTableCols : " + ex);

        }

        logger.debug("isEntryPresentInTableCols : " + logUserId + "ends");
        return flag;
    }

    /**
     * Getting table column information like columnId,columnHeader,dataType,filledBy,columnType,showorhide,etc.
     * @param tableId = tbl_TenderColumns.tenderTableId
     * @param isSortOrder = true (to get ordered list of column details) / false( unordered list for column details.)
     * @return List of CommonFormData Object having information for TenderColumn.
     */
    public List<CommonFormData> getTableColumns(int tableId, Boolean isSortOrder) {

        logger.debug("getTableColumns : " + logUserId + "starts");
        List<CommonFormData> tableColumnData = null;
        try {
            if (isSortOrder) {
                tableColumnData = tenderBidService.getTenderFormDetails(tableId, 0, 0, 0, 0, 0, "GetTableColumn");
            } else {
                tableColumnData = tenderBidService.getTenderFormDetails(tableId, 0, 0, 0, 0, 0, "GetColumnData");
            }


        } catch (Exception ex) {
            logger.error("getTableColumns : " + ex);

        }

        logger.debug("getTableColumns : " + logUserId + "ends");
            return tableColumnData;
        }

    /**
     * Getting tender column details for Negotiation.
     * @param tableId =tenderTableId
     * @param bidId =tbl_TenderBidForm.bidId
     * @param isSortOrder = true ( return tender column details from tbl_TenderColumns) /
     *                      false( return tender column & bittable information from tbl_TenderColumns,tbl_TenderBidForm,tbl_TenderBidTable)
     * @return List of CommonFormData Object having information for TenderColumn.
     */
    public List<CommonFormData> getNegTableColumns(int tableId, int bidId, Boolean isSortOrder) {

        logger.debug("getNegTableColumns : " + logUserId + "starts");
        List<CommonFormData> tableColumnData = null;

        try {
            if (isSortOrder) {
                tableColumnData = tenderBidService.getTenderFormDetails(tableId, 0, 0, 0, bidId, 0, "GetNegTableColumn");
            } else {
                tableColumnData = tenderBidService.getTenderFormDetails(tableId, 0, 0, 0, bidId, 0, "GetNegColumns");
            }


        } catch (Exception ex) {
            logger.error("getNegTableColumns : " + ex);

        }
        logger.debug("getNegTableColumns : " + logUserId + "ends");
            return tableColumnData;
        }

    /**
     * Get auto column id, column name of givne table.
     * @param tableId = tbl_TenderColumns.templateTableId
     * @return List of CommonFormData having column id, column name of auto columns.
     */
    public List<CommonFormData> getTableAutoColumns(int tableId) {

        logger.debug("getTableAutoColumns : " + logUserId + "starts");
        List<CommonFormData> commonform = null;
        try {
            commonform = tenderBidService.getTenderFormDetails(tableId, 0, 0, 0, 0, 0, "GetAutoColumn");
        } catch (Exception ex) {
            logger.error("getTableAutoColumns : " + ex);

    }

        logger.debug("getTableAutoColumns : " + logUserId + "ends");
        return commonform;
        }

    /**
     * Get Table Cell values for given tableId.
     * @param tableId = tbl_TenderColumns.tenderTableId
     * @return = List of CommonFormData
     */
    public List<CommonFormData> getTableCells(int tableId) {

        logger.debug("getTableCells : " + logUserId + "starts");
        List<CommonFormData> commonform = null;
        try {
            commonform = tenderBidService.getTenderFormDetails(tableId, 0, 0, 0, 0, 0, "GetCellData");
        } catch (Exception ex) {
            logger.error("getTableCells : " + ex);

    }

        logger.debug("getTableCells : " + logUserId + "ends");
        return commonform;
        }

    /**
     * Get No. Of Columns in Tender Table.
     * @param tableId = tbl_TenderColumns.tenderTableId
     * @return = no. of Column count.
     */
    public Integer getNoOfColsInTable(int tableId) {

        logger.debug("getNoOfColsInTable : " + logUserId + "starts");
        Integer flag = null;
        try {
            flag = tenderBidService.getTenderFormDetails(tableId, 0, 0, 0, 0, 0, "GetColumnNo").get(0).getCount();
        } catch (Exception ex) {
            logger.error("getNoOfColsInTable : " + ex);
    }

        logger.debug("getNoOfColsInTable : " + logUserId + "ends");
        return flag;
        }

    /**
     *Get No. of Rows for given Tender Table.
     * @param tableId = tbl_TenderCells.tenderTableId
     * @param columnId = tbl_TenderCells.columnId
     * @return = no. of row count.
     */
    public Integer getNoOfRowsInTable(int tableId, int columnId) {

        logger.debug("getNoOfRowsInTable : " + logUserId + "starts");
        Integer flag = null;
        try {
            flag = tenderBidService.getTenderFormDetails(tableId, 0, columnId, 0, 0, 0, "GetRowNo").get(0).getCount();
        } catch (Exception ex) {
            logger.error("getNoOfRowsInTable : " + ex);

    }

        logger.debug("getNoOfRowsInTable : " + logUserId + "ends");
        return flag;
        }

    /**
     * Get No. of tables for given Tender Form
     * @param formId = tbl_TenderTables.tenderFormId
     * @return = Count having no of table in form.
     */
    public Integer getNoOfTable(int formId) {

        logger.debug("getNoOfTable : " + logUserId + "starts");
        Integer flag = null;
        try {
            flag = tenderBidService.getTenderFormDetails(0, formId, 0, 0, 0, 0, "GetAllTableCount").get(0).getCount();
        } catch (Exception ex) {
            logger.error("getNoOfTable : " + ex);

    }

        logger.debug("getNoOfTable : " + logUserId + "ends");
        return flag;
        }

    /**
     * Get Formula details.
     * @param tableId = tbl_TenderFormula.tenderTableId
     * @return = List of CommonFormData Object
     */
    public List<CommonFormData> getTableFormulas(int tableId) {

        logger.debug("getTableFormulas : " + logUserId + "starts");
        List<CommonFormData> commonformdate = null;
        try {
            commonformdate = tenderBidService.getTenderFormDetails(tableId, 0, 0, 0, 0, 0, "GetFormula");
        } catch (Exception ex) {
            logger.error("getTableFormulas : " + ex);

    }

        logger.debug("getTableFormulas : " + logUserId + "ends");
        return commonformdate;
        }

    /**
     * Get Table Column Details
     * @param tableId = tbl_TenderColumns.tenderTableId
     * @return = List of CommonFormData Object
     */
    public List<CommonFormData> getTableColumns(int tableId) {

            logger.debug("getTableColumns : " + logUserId + "starts");
            List<CommonFormData> commonformdate = null;
            try {
                commonformdate = tenderBidService.getTenderColumnDetails(tableId, 0, 0, 0, 0, 0, "GetTableColumn");
            } catch (Exception ex) {
                logger.error("getTableColumns : " + ex);

        }
        logger.debug("getTableColumns : " + logUserId + "ends");
        return commonformdate;
        }

    /**
     * Get Tender bid Data including column details and Bid Details.
     * @param tableId = tbl_TenderColumns.tenderTableId
     * @return = List of CommonFormData Object
     */
    public List<CommonFormData> getTableBidCells(int tableId) {

        logger.debug("getTableBidCells : " + logUserId + "starts");
        List<CommonFormData> commonformdate = null;
        try {
            commonformdate = tenderBidService.getTenderFormDetails(tableId, 0, 0, 0, 0, 0, "GetBidCell");

        } catch (Exception ex) {
            logger.error("getTableBidCells : " + ex);

        }

        logger.debug("getTableBidCells : " + logUserId + "ends");
        return commonformdate;
    }

    /**
     * Get Tender Negotiation bid Data including column details and Bid Details.
     * @param tableId = tbl_TenderColumns.tenderTableId
     * @return = List of CommonFormData Object
     */

    public List<CommonFormData> getTableNegBidCells(int tableId) {

        logger.debug("getTableNegBidCells : " + logUserId + "starts");
        List<CommonFormData> commonformdate = null;
        try {
            commonformdate = tenderBidService.getTenderFormDetails(tableId, 0, 0, 0, 0, 0, "GetNegBidCell");

        } catch (Exception ex) {
            logger.error("getTableNegBidCells : " + ex);

        }

        logger.debug("getTableNegBidCells : " + logUserId + "ends");
        return commonformdate;
    }

    /**
     * Perform Insert Negotiation Bid data into tbl_NegotiatedBid.
     * @param xmlStr1 = Field values of tbl_NegotiatedBid in XML Formate
     * @return = true if successfully insertion / false if unsuccessful operation.
     */
    public Boolean insertNegFormData(String xmlStr1) {

        logger.debug("insertNegFormData : " + logUserId + "starts");
        boolean flag = false;
        try {
            flag = tenderBidService.insertFormDetails("insert", "tbl_NegotiatedBid", null, null, null, xmlStr1, null, null, null, "negBidId", null, null, null);
        } catch (Exception e) {
            logger.error("Error in insertFormData() : " + e.toString());

        }

        logger.debug("insertNegFormData : " + logUserId + "ends");
        return flag;
        }

    /**
     * Perform insertion for Tender Bid Submission.
     * @param xmlStr1 = Field values of tbl_TenderBidForm in XML Formate
     * @param xmlStr2 = Field values of tbl_TenderBidTable in XML Formate
     * @param xmlStr3 = Field values of tbl_TenderBidDetail in XML Formate
     * @param xmlStr4 = Field values of tbl_TenderBidSign in XML Formate
     * @return = true if successfully insertion / false if unsuccessful operation.
     */
    public Boolean insertFormData(String xmlStr1, String xmlStr2, String xmlStr3, String xmlStr4) {

        logger.debug("insertFormData : " + logUserId + "starts");
        boolean flag = false;
        try {
            flag = tenderBidService.insertFormDetails("insert", "tbl_TenderBidForm", "tbl_TenderBidTable", "tbl_TenderBidDetail", "tbl_TenderBidSign", xmlStr1, xmlStr2, xmlStr3, xmlStr4, "bidId", "bidTableId", "bidId", null);
        } catch (Exception e) {
            logger.error("Error in insertFormData() : " + e.toString());

    }

        logger.debug("getCompanyDetails : " + logUserId + "ends");
        return flag;

        }

    /**
     * Perform update for Negotiation Bidder Bid Data.
     * @param xmlStr1 = Field values of tbl_NegotiatedBidderBid in XML Formate
     * @param bidTableId = tbl_NegotiatedBidderBid.bidTableId
     * @return = true if operation perform successfully  / false if operation perform unsuccessful
     */
    public Boolean updateBidderNegFormData(String xmlStr1, int bidTableId) {

        logger.debug("updateBidderNegFormData : " + logUserId + "starts");
        boolean flag = false;
        try {
            flag = tenderBidService.insertFormDetails("insdel", "tbl_NegotiatedBidderBid", null, null, null, xmlStr1, null, null, null, "negBidderBidId", null, null, "bidtableId = " + bidTableId);
        } catch (Exception e) {
            logger.error("Error in updateFormData() : " + e.toString());

        }

        logger.debug("updateBidderNegFormData : " + logUserId + "ends");
        return flag;
    }

    /**
     * Perform delete insert for Negotiation Bid Data.
     * @param xmlStr1 = Field values of tbl_NegBidForm in XML Formate
     * @param xmlStr2 = Field values of tbl_NegBidTable in XML Formate
     * @param xmlStr3 = Field values of tbl_NegBidDtlSrv in XML Formate
     * @param bidId =  tbl_NegBidForm.bidId
     * @param negId = tbl_NegBidForm.negId
     * @param userId = tbl_NegBidForm.userId
     * @param formId = tbl_NegBidForm.tenderFormId
     * @return = true if operation perform successfully  / false if operation perform unsuccessful
     */

    public Boolean updateBidderNegFormDataSrv(String xmlStr1, String xmlStr2, String xmlStr3, int bidId,int negId,int userId, int formId) {

        logger.debug("updateFormData : " + logUserId + "starts");
        boolean flag = false;
        String condition=" bidId="+bidId+" and negId="+negId+" and userId="+userId+" and tenderFormId="+formId;
        System.out.println("condition="+condition);
        try {
            flag = tenderBidService.insertFormDetails("insdel","tbl_NegBidForm", "tbl_NegBidTable", "tbl_NegBidDtlSrv",null,xmlStr1, xmlStr2,xmlStr3, null,"negBidFormId","negBidTableId", "negBidDtlId",condition);
          
        } catch (Exception e) {
            logger.error("Error in updateFormData() : " + e.toString());
            e.printStackTrace();

        }

        logger.debug("updateFormData : " + logUserId + "ends");
        return flag;
        }

    /**
     * Perform insert for Negotiation Bid Data.
     * @param xmlStr1 = Field values of tbl_NegBidForm in XML Formate
     * @param xmlStr2 = Field values of tbl_NegBidTable in XML Formate
     * @param xmlStr3 = Field values of tbl_NegBidDtlSrv in XML Formate
     * @param bidId =  tbl_NegBidForm.bidId
     * @param negId = tbl_NegBidForm.negId
     * @param userId = tbl_NegBidForm.userId
     * @param formId = tbl_NegBidForm.tenderFormId
     * @return = true if operation perform successfully  / false if operation perform unsuccessful
     */
    public Boolean insertBidderNegFormDataSrv(String xmlStr1, String xmlStr2, String xmlStr3, int bidId,int negId,int userId, int formId) {

        logger.debug("insertBidderNegFormDataSrv : " + logUserId + "starts");
        boolean flag = false;
        try {
            flag = tenderBidService.insertFormDetails("insert","tbl_NegBidForm", "tbl_NegBidTable", "tbl_NegBidDtlSrv",null,xmlStr1, xmlStr2,xmlStr3, null,"negBidFormId","negBidTableId", "negBidDtlId","");

        } catch (Exception e) {
            logger.error("Error in insertBidderNegFormDataSrv() : " + e.toString());
            e.printStackTrace();

        }

        logger.debug("insertBidderNegFormDataSrv : " + logUserId + "ends");
        return flag;
        }

    /**
     * Delete Negotiation bid data.
     * @param negBidformId =tbl_NegBidForm.negBidformId
     * @return  = true if operation perform successfully  / false if operation perform unsuccessful
     */
     public Boolean deleteBidderNegFormDataSrv(int negBidformId) {

        logger.debug("deleteBidderNegFormDataSrv : " + logUserId + "starts");
        boolean flag = false;
        try {
            flag = tenderBidService.insertFormDetails("delete","tbl_NegBidForm",null,null,null,null, null,null,null, null,null, null," negBidFormId="+negBidformId);

        } catch (Exception e) {
            logger.error("Error in deleteBidderNegFormDataSrv() : " + e.toString());
            e.printStackTrace();

        }

        logger.debug("deleteBidderNegFormDataSrv : " + logUserId + "ends");
        return flag;
        }

    /**
     * Update Negotiated Bid Data.
     * @param xmlStr1 = Field values of tbl_NegotiatedBid in XML Formate
     * @param bidTableId =  tbl_NegotiatedBid.bidtableId
     * @return = true if operation perform successfully  / false if operation perform unsuccessful
     */
     public Boolean updateNegFormData(String xmlStr1, int bidTableId) {

        logger.debug("updateNegFormData : " + logUserId + "starts");
        boolean flag = false;
        try {
            flag = tenderBidService.insertFormDetails("insdel", "tbl_NegotiatedBid", null, null, null, xmlStr1, null, null, null, "negBidId", null, null, "bidtableId = " + bidTableId);
        } catch (Exception e) {
            logger.error("Error in updateFormData() : " + e.toString());

        }

        logger.debug("updateNegFormData : " + logUserId + "ends");
        return flag;
    }

     /**
      * Perform Update operation by deleting first existing records and then perform insert operation.
      * @param xmlStr1 = Field values of tbl_TenderBidForm in XML Formate
      * @param xmlStr2 = Field values of tbl_TenderBidTable in XML Formate
      * @param xmlStr3 = Field values of tbl_TenderBidDetail in XML Formate
      * @param xmlStr4 = Field values of tbl_TenderBidSign in XML Formate
      * @param bidId =  tbl_TenderBidForm.bidId
      * @return = true if operation perform successfully  / false if operation perform unsuccessful
      */
    public Boolean updateFormData(String xmlStr1, String xmlStr2, String xmlStr3, String xmlStr4, int bidId) {

        logger.debug("updateFormData : " + logUserId + "starts");
        boolean flag = false;
        try {
            flag = tenderBidService.insertFormDetails("insdel", "tbl_TenderBidForm", "tbl_TenderBidTable", "tbl_TenderBidDetail", "tbl_TenderBidSign", xmlStr1, xmlStr2, xmlStr3, xmlStr4, "bidId", "bidTableId", "bidId", "bidId = " + bidId);
        } catch (Exception e) {
            logger.error("Error in updateFormData() : " + e.toString());

        }

        logger.debug("updateFormData : " + logUserId + "ends");
        return flag;
        }

    /**
     * Get No. Of bid Table.
     * @param formId = tbl_TenderBidForm.tenderFormId
     * @param userId = tbl_TenderBidForm.userId
     * @return no. of bid table count
     */
    public Integer getNoOfBidTable(int formId, int userId) {

        logger.debug("getNoOfBidTable : " + logUserId + "starts");
        Integer flag = null;
        try {
            flag = tenderBidService.getTenderFormDetails(0, formId, 0, userId, 0, 0, "GetBidCount").get(0).getCount();
        } catch (Exception ex) {
            logger.error("getNoOfBidTable : " + ex);

    }

        logger.debug("getNoOfBidTable : " + logUserId + "ends");
        return flag;
        }

    /**
     * To Get bid sign text.
     * @param bidId = tbl_tenderBidsign.bidId
     * @param formId = tbl_TenderBidForm.tenderFormId
     * @param userId = tbl_TenderBidForm.userId
     * @return bid sign Text as String values.
     */
    public String getBidSign(int bidId, int formId, int userId) {

        logger.debug("getBidSign : " + logUserId + "starts");
        String flag = null;
        try {
            flag = tenderBidService.getTenderFormDetails(0, formId, 0, userId, bidId, 0, "GetBidSignData").get(0).getCellValue();
        } catch (Exception ex) {
            logger.error("getBidSign : " + ex);

        }

        logger.debug("getBidSign : " + logUserId + "ends");
        return flag;
    }

    /**
     * Get Bid Data.
     * @param bidId = tbl_TenderBidForm.bidId
     * @param userId = tbl_TenderBidForm.userId
     * @return List of COmmonFormData Objects.
     */
    public List<CommonFormData> getBidData(int bidId, int userId) {

        logger.debug("getBidData : " + logUserId + "starts");
        List<CommonFormData> commonformdata = null;
        try {
            commonformdata = tenderBidService.getTenderFormDetails(0, 0, 0, userId, bidId, 0, "GetBidData");
        } catch (Exception ex) {
            logger.error("getBidData : " + ex);

        }

        logger.debug("getBidData : " + logUserId + "ends");
        return commonformdata;
    }

    /**
     * Get Negotiated Bid Data
     * @param bidId = tbl_TenderBidForm.bidId
     * @param userId = tbl_TenderBidForm.userId
     * @return List of COmmonFormData Objects.
     */
    public List<CommonFormData> getNegBidderBidData(int bidId, int userId) {

        logger.debug("getNegBidderBidData : " + logUserId + "starts");
        List<CommonFormData> commonformdata = null;
        try {
            commonformdata = tenderBidService.getTenderFormDetails(0, 0, 0, userId, bidId, 0, "GetNegBidderBidData");
        } catch (Exception ex) {
            logger.error("getNegBidderBidData : " + ex);

        }

        logger.debug("getNegBidderBidData : " + logUserId + "ends");
        return commonformdata;
    }

    /**
     * Get Bid Plain Data.
     * @param bidId = tbl_TenderBidForm.bidId
     * @param userId = tbl_TenderBidForm.userId
     * @return List of COmmonFormData Objects.
     */
    public List<CommonFormData> getBidPlainData(int bidId, int userId) {

        logger.debug("getBidPlainData : " + logUserId + "starts");
        List<CommonFormData> commonformdata = null;
        try {
            commonformdata = tenderBidService.getTenderFormDetails(0, 0, 0, userId, bidId, 0, "GetBidPlainData");
        } catch (Exception ex) {
            logger.error("getBidPlainData : " + ex);

        }

        logger.debug("getBidPlainData : " + logUserId + "ends");
        return commonformdata;
    }

    /**
     * Get Negotiated Bid Data
     * @param bidId = tbl_TenderBidForm.bidId
     * @param userId = tbl_TenderBidForm.userId
     * @return = List of COmmonFormData Objects.
     */
    public List<CommonFormData> getNegBidData(int bidId, int userId) {

        logger.debug("getNegBidData : " + logUserId + "starts");
        List<CommonFormData> commonformdata = null;
        try {
            commonformdata = tenderBidService.getTenderFormDetails(0, 0, 0, userId, bidId, 0, "GetNegBidData");
        } catch (Exception ex) {
            logger.error("getNegBidData : " + ex);

        }

        logger.debug("getNegBidData : " + logUserId + "ends");
        return commonformdata;
    }

    /**
     * Get Negotiation Bid Data for Tender Services Case.
     * @param negBidformId =  tbl_NegBidTable.negBidFormId
     * @return = List of COmmonFormData Objects.
     */
     public List<CommonFormData> getNegBidSrcData(int negBidformId) {

        logger.debug("getNegBidData : " + logUserId + "starts");
        List<CommonFormData> commonformdata = null;
        try {
            commonformdata = tenderBidService.getTenderFormDetails(0, negBidformId, 0, 0, 0, 0, "GetNegBidSrcData");
        } catch (Exception ex) {
            logger.error("getNegBidData : " + ex);

        }

        logger.debug("getNegBidData : " + logUserId + "ends");
        return commonformdata;
    }

     /**
      * Get Bid Table Ids.
      * @param bidId = tbl_TenderBidTable.bidId
      * @param tableId =  tbl_TenderBidTable.tenderTableId
      * @param userId = tbl_TenderBidForm.userId
      * @return = List of COmmonFormData Objects.
      */
    public List<CommonFormData> getBidTableId(int bidId, int tableId, int userId) {

        logger.debug("getBidTableId : " + logUserId + "starts");
        List<CommonFormData> commonformdata = null;
        try {
            commonformdata = tenderBidService.getTenderFormDetails(tableId, 0, 0, userId, bidId, 0, "GetBidTableId");
        } catch (Exception ex) {
            logger.error("getBidTableId : " + ex);

        }

        logger.debug("getBidTableId : " + logUserId + "ends");
        return commonformdata;
    }

    /**
     * Get Negotiatied Bid Table Ids.
     * @param negBidformId = tbl_NegBidTable.negBidFormId
     * @param tableId = tbl_NegBidTable.tenderTableId
     * @return = List of COmmonFormData Objects.
     */
     public List<CommonFormData> GetNegBidTableId(int negBidformId, int tableId) {

        logger.debug("getBidTableId : " + logUserId + "starts");
        List<CommonFormData> commonformdata = null;
        try {
            commonformdata = tenderBidService.getTenderFormDetails(tableId, negBidformId, 0, 0, 0, 0, "GetNegBidTableId");
        } catch (Exception ex) {
            logger.error("getBidTableId : " + ex);

        }

        logger.debug("getBidTableId : " + logUserId + "ends");
        return commonformdata;
    }

    /**
     * Get Form Data
     * @param formId = tbl_TenderForms.tenderFormId
     * @return = List of COmmonFormData Objects.
     */
    public List<CommonFormData> getFormData(int formId) {

        logger.debug("getFormData : " + logUserId + "starts");
        List<CommonFormData> commonformdata = null;
        try {
            commonformdata = tenderBidService.getTenderFormDetails(0, formId, 0, 0, 0, 0, "GetFormData");
        } catch (Exception ex) {
            logger.error("getFormData : " + ex);

        }

        logger.debug("getFormData : " + logUserId + "ends");
        return commonformdata;
    }

    /**
     * Get Bid Cell Data.
     * @param tableId = tbl_TenderBidTable.tenderTableId
     * @param userId = tbl_TenderBidForm.userId
     * @param bidId = tbl_TenderBidForm.bidId
     * @return = List of COmmonFormData Objects.
     */
    public List<CommonFormData> getBidCellData(int tableId, int userId, int bidId) {

        logger.debug("getBidCellData : " + logUserId + "starts");
        List<CommonFormData> commonformdata = null;
        try {
            commonformdata = tenderBidService.getTenderFormDetails(tableId, 0, 0, userId, bidId, 0, "GetBidCellData");
        } catch (Exception ex) {
            logger.error("getBidCellData : " + ex);

        }

        logger.debug("getBidCellData : " + logUserId + "ends");
        return commonformdata;
    }

    /**
     * Get Tender Table Cell Count.
     * @param tableId = tbl_TenderCells.tenderTableId
     * @return =  no. of. Cell Count
     */
    public Integer getBidCellCnt(int tableId) {

        logger.debug("getBidCellCnt : " + logUserId + "starts");
        Integer flag = null;
        try {
            flag = tenderBidService.getTenderFormDetails(tableId, 0, 0, 0, 0, 0, "GetBidCellCnt").get(0).getCount();
        } catch (Exception ex) {
            logger.error("getBidCellCnt : " + ex);

        }

        logger.debug("getBidCellCnt : " + logUserId + "ends");
        return flag;
    }

    /**
     * Delete Tender Bid Form Details.
     * @param formId = Tbl_tenderBidForm.tenderFormId
     * @param userId = tbl_TenderBidForm.userId
     * @param bidId = tbl_TenderBidForm.bidId
     * @return  = List of COmmonFormData Objects.
     */
    public Boolean delBidForm(int formId, int userId, int bidId) {

        logger.debug("delBidForm : " + logUserId + "starts");
        Boolean flag = false;
        try {
            flag = tenderBidService.deleteBidFormDetails(formId, userId, bidId);
        } catch (Exception ex) {
            logger.error("delBidForm : " + ex);

        }

        logger.debug("delBidForm : " + logUserId + "ends");
        return flag;
        }

    /**
     * Get Buyer Password and Hash.
     * @param tenderId
     * @return Hash value.
     */
    public String getBuyerPwdHash(int tenderId) {

        logger.debug("getBuyerPwdHash : " + logUserId + "starts");
        String flag = "";
        try {
            flag = tenderBidService.getBuyerPwdHash(tenderId);
        } catch (Exception ex) {
            logger.error("getBuyerPwdHash : " + ex);

        }

        logger.debug("getBuyerPwdHash : " + logUserId + "ends");
        return flag;
        }

    /**
     * Insert Bid Encrypt Details.
     * @param tableName
     * @param xmlBidEncrypt
     * @param whereCond
     * @param isInEditMode
     * @return true if added or false if not.
     */
    public boolean insertBidEncrypt(String tableName, String xmlBidEncrypt, String whereCond, boolean isInEditMode) {
        return tenderBidService.insertBidEncrypt(tableName, xmlBidEncrypt, whereCond, isInEditMode);
    }

    /**
     * Insert in to Final Submission
     * @param tenderId = Tbl_TenderMaster.tenderId
     * @param userId = Tbl_FinalSubmission.userId
     * @return return no of records affected  from insert operation.
     */
    public int insertIntoFS(int tenderId, int userId) {

        logger.debug("insertIntoFS : " + logUserId + "starts");
        int fsId = -1;
        try {
            TblFinalSubmission tblFS = new TblFinalSubmission();
            tblFS.setTblTenderMaster(new TblTenderMaster(tenderId));
            tblFS.setFinalSubmissionDt(null);
            tblFS.setUserId(userId);
            tblFS.setIpAddress("-1");
            tblFS.setBidSubStatus("pending");
            tblFS.setTenderHash("-1");
            tblFS.setReceiptNo("-1");
            fsId = tenderBidService.insertIntoFS(tblFS);

            if (tblFS != null) {
                tblFS = null;
            }
        } catch (Exception ex) {
            logger.error("insertIntoFS : " + ex);
        }

        logger.debug("insertIntoFS : " + logUserId + "ends");
        return fsId;
    }

    /**
     * Insert in to Final Submission details.
     * @param fsId = TblFinalSubmission.FinalSubmissionId
     * @param formId =  TblFinalSubDetail.tenderFormId
     * @param bidId = TblFinalSubDetail.bidId
     * @param ipAddress = TblFinalSubDetail.ipAddress
     * @return true if added or false if not.
     */
    public boolean insertIntoFSDtl(int fsId, int formId, int bidId, String ipAddress) {
        boolean flag = true;
        logger.debug("insertIntoFSDtl : " + logUserId + "starts");
        try {
            TblFinalSubDetail tblFSDtl = new TblFinalSubDetail();
            tblFSDtl.setTblFinalSubmission(new TblFinalSubmission(fsId));
            tblFSDtl.setTenderFormid(formId);
            tblFSDtl.setIpAddress(ipAddress);
            tblFSDtl.setFormHash("-1");
            tblFSDtl.setBidId(bidId);
            tblFSDtl.setNewFormCombinedHash("NULL");
            flag = tenderBidService.insertIntoFSDtl(tblFSDtl);
        } catch (Exception ex) {
            logger.error("insertIntoFSDtl : " + ex);
        }

        logger.debug("insertIntoFSDtl : " + logUserId + "ends");
        return flag;
    }

    /**
     * Get Final Submission Id.
     * @param tenderId
     * @param userId
     * @return Id.
     */
    public int getFinalSubId(int tenderId, int userId) {

        logger.debug("getFinalSubId : " + logUserId + "starts");
        int fsId = -1;
        try {
            fsId = tenderBidService.getFinalSubId(tenderId, userId);
        } catch (Exception ex) {
            logger.error("getFinalSubId : " + ex);
        }

        logger.debug("getFinalSubId : " + logUserId + "ends");
        return fsId;
    }

    /**
     * Get Tender List Cell Details.
     * @param formId
     * @param tableId
     * @param columnId
     * @param cellId
     * @return List of List Cell Details.
     */
    public List<TblTenderListCellDetail> getTenderListCellDetail(int formId,int tableId, short columnId, int cellId) {
        logger.debug("getTenderListCellDetail : " + logUserId + "starts");
        List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
        try {
            listCellDetail = tenderBidService.getTenderListCellDetail(formId, tableId, columnId, cellId);
        } catch (Exception ex) {
            logger.error("getTenderListCellDetail : " + ex);
        }
        logger.debug("getTenderListCellDetail : " + logUserId + "ends");
        return listCellDetail;

    }
    
     /**
     * Get Bid Id from user Id and tender Form Id.
     * @param userId
     * @param tenderFormId
     * @return List of ID.
     */
    public List<Object> getBidId(int userId, int tenderFormId) {
        return tenderBidService.getBidId(userId, tenderFormId);
    }
    
    /**
     * Get Grand Total Caption cell
     * @param tenderFormId
     * @param tenderTableId
     * @return cell Id.
     */ 
    public int getCellForTotalWordsCaption(int tenderFormId, int tenderTableId){
        logger.debug("getCellForTotalWordsCaption : " + logUserId + " Starts");
        int cellId = -1;
        try{
            cellId = tenderBidService.getCellForGrandTotalCaption(tenderFormId,tenderTableId);
        }catch(Exception ex){
            logger.error("getCellForTotalWordsCaption : " + logUserId + ex);
        }
        logger.debug("getCellForTotalWordsCaption : " + logUserId + " Ends");
        return cellId;
    }

    /**
     * Get Negotiated Bid Plain Data
     * @param bidId = tbl_TenderBidForm.bidId
     * @param userId = tbl_TenderBidForm.userId
     * @return = List of CommonFormData Object
     */
    public List<CommonFormData> getNegBidPlainData(int bidId, int userId) {

        logger.debug("getBidPlainData : " + logUserId + "starts");
        List<CommonFormData> commonformdata = null;
        try {
            commonformdata = tenderBidService.getTenderFormDetails(0, 0, 0, userId, bidId, 0, "GetNegBidPlainData");
        } catch (Exception ex) {
            logger.error("getBidPlainData : " + ex);

        }

        logger.debug("getBidPlainData : " + logUserId + "ends");
        return commonformdata;
    }
    
    /**
     * Delete Negotiation Data in case of Service Type Tender.
     * @param tableName 
     * @param bidId
     * @return = true if deleted or false if not.
     */
     public Boolean deleteBidderNegTableSrvData(String tableName, int bidId) {

        logger.debug("deleteBidderNegTableSrvData : " + logUserId + "starts");
        boolean flag = false;
        try {
            flag = tenderBidService.insertFormDetails("delete",tableName, null,null,null, null, null, null,null,"negBidTableId", null, null,"bidId = " + bidId);
          
        } catch (Exception e) {
            logger.error("Error in deleteBidderNegTableSrvData() : " + e.toString());
            e.printStackTrace();

        }

        logger.debug("deleteBidderNegTableSrvData : " + logUserId + "ends");
        return flag;
        }

    /**
     * Check Bid Form Submitted or Not.
     * @param tenderId = tbl_TenderBidForm.TenderId
     * @param formId = tbl_TenderBidForm.tenderFormId
     * @param userId = tbl_TenderBidForm.userId
     * @return no. of record for TenderBidForm
     */
    public long checkBidFormSubmited(int tenderId, int formId, int userId) {
        logger.debug("checkBidFormSubmited : " + logUserId + " Starts");
        long result=0;

        try {
            result= tenderBidService.checkBidFormSubmited(tenderId, formId, userId);
        } catch (Exception ex) {
            logger.error("checkBidFormSubmited : " + logUserId + ex);
        }
        logger.debug("checkBidFormSubmited : " + logUserId + " Ends");
        return result;
    }

    /**
     * Check Form Encripted or Not.
     * @param tenderId = tbl_TenderBidForm.TenderId
     * @param formId = tbl_TenderBidForm.tenderFormId
     * @param userId = tbl_TenderBidForm.userId
     * @return no. of record for TenderBidForm
     */
     public long checkBidFormEncriptedAndSaved(int tenderId, int formId, int userId) {
        logger.debug("checkBidFormSubmited : " + logUserId + " Starts");
        long result=0;

        try {
            result= tenderBidService.checkBidFormEncriptedAndSaved(tenderId, formId, userId);
        } catch (Exception ex) {
            logger.error("checkBidFormSubmited : " + logUserId + ex);
        }
        logger.debug("checkBidFormSubmited : " + logUserId + " Ends");
        return result;
    }
}
