/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblCmsTaxConfig;
import com.cptu.egp.eps.web.utility.AppContext;
import org.apache.log4j.Logger;
import com.cptu.egp.eps.service.serviceinterface.CmsTaxConfigurationService;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sreenu.Durga
 */
public class CmsTaxConfigBean {

    final Logger logger = Logger.getLogger(CmsTaxConfigBean.class);
    private final static CmsTaxConfigurationService cmsTaxConfigurationService =
            (CmsTaxConfigurationService) AppContext.getSpringBean("CmsTaxConfigurationService");
    private String logUserId = "0";
    private static final String LOGGERStart = " Starts";
    private static final String LOGGEREnd = " Ends";

    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        cmsTaxConfigurationService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    public int insertCmsTaxConfiguration(TblCmsTaxConfig tblCmsTaxConfig) {
        logger.debug("insertCmsTaxConfiguration : " + logUserId + LOGGERStart);
        int id = 0;
        try {
            id = cmsTaxConfigurationService.insertCmsTaxConfig(tblCmsTaxConfig);
        } catch (Exception e) {
            logger.error("insertCmsTaxConfiguration : " + logUserId + " : " + e);
        }
        logger.debug("insertCmsTaxConfiguration : " + logUserId + LOGGEREnd);
        return id;
    }

    public boolean updateCmsTaxConfiguration(TblCmsTaxConfig tblCmsTaxConfig) {
        logger.debug("updateCmsTaxConfiguration : " + logUserId + LOGGERStart);
        boolean isUpdate =  false;
        try {
            isUpdate = cmsTaxConfigurationService.updateCmsTaxConfig(tblCmsTaxConfig);
        } catch (Exception e) {
            logger.error("updateCmsTaxConfiguration : " + logUserId + " : " + e);
        }
        logger.debug("updateCmsTaxConfiguration : " + logUserId + LOGGEREnd);
        return isUpdate;
    }

    public boolean deleteCmsTaxConfiguration(TblCmsTaxConfig tblCmsTaxConfig) {
        logger.debug("deleteCmsTaxConfiguration : " + logUserId + LOGGERStart);
        boolean isDeleted = false;
        try {
            isDeleted = cmsTaxConfigurationService.deleteCmsTaxConfig(tblCmsTaxConfig);
        } catch (Exception e) {
            logger.error("deleteCmsTaxConfiguration : " + logUserId + " : " + e);
        }
        logger.debug("deleteCmsTaxConfiguration : " + logUserId + LOGGEREnd);
        return isDeleted;
    }

    public List<TblCmsTaxConfig> getAllCmsTaxConfiguration() {
        logger.debug("getAllCmsTaxConfiguration : " + logUserId + LOGGERStart);
        List<TblCmsTaxConfig> cmsTaxConfigList = new ArrayList<TblCmsTaxConfig>();
        try {
            cmsTaxConfigList = cmsTaxConfigurationService.getAllCmsTaxConfig();
        } catch (Exception e) {
            logger.error("getAllCmsTaxConfiguration : " + logUserId + " : " + e);
        }
        logger.debug("getAllCmsTaxConfiguration : " + logUserId + LOGGEREnd);
        return cmsTaxConfigList;
    }

    public long getCmsTaxConfigurationCount() {
        logger.debug("getCmsTaxConfigurationCount : " + logUserId + LOGGERStart);
        long count = 0;
        try {
            count = cmsTaxConfigurationService.getCmsTaxConfigCount();
        } catch (Exception e) {
            logger.error("getCmsTaxConfigurationCount : " + logUserId + " : " + e);
        }
        logger.debug("getCmsTaxConfigurationCount : " + logUserId + LOGGEREnd);
        return count;
    }

    public TblCmsTaxConfig getCmsTaxConfiguration(int id) {
        logger.debug("getCmsTaxConfiguration : " + logUserId + LOGGERStart);
        TblCmsTaxConfig tblCmsTaxConfig = new TblCmsTaxConfig();
        try {
            tblCmsTaxConfig = cmsTaxConfigurationService.getCmsTaxConfig(id);
        } catch (Exception e) {
            logger.error("getCmsTaxConfiguration : " + logUserId + " : " + e);
        }
        logger.debug("getCmsTaxConfiguration : " + logUserId + LOGGEREnd);
        return tblCmsTaxConfig;
    }

    public TblCmsTaxConfig getLatestCmsTaxConfiguration(int procurementNatureId) {
        logger.debug("getLatestCmsTaxConfiguration : " + logUserId + LOGGERStart);
        TblCmsTaxConfig tblCmsTaxConfig = new TblCmsTaxConfig();
        try {
            tblCmsTaxConfig = cmsTaxConfigurationService.getLatestCmsTaxConfig(procurementNatureId);
        } catch (Exception e) {
            logger.error("getLatestCmsTaxConfiguration : " + logUserId + " : " + e);
        }
        logger.debug("getLatestCmsTaxConfiguration : " + logUserId + LOGGEREnd);
        return tblCmsTaxConfig;
    }
}
