/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.xmlpojos;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Chalapathi.Bavisetti
 */
@XmlRootElement
public class Tender {

 private List<AllTendersXmlElements> tenderDetails = new ArrayList<AllTendersXmlElements>();

 
    /**
     * @return the tender
     */
    public List<AllTendersXmlElements> getTenderDetails() {
        return tenderDetails;
    }

    /**
     * @param tender the tenderDetails to set
     */
    public void setTenderDetails(List<AllTendersXmlElements> tenderDetails) {
        this.tenderDetails = tenderDetails;
    }

     public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("employees:");
        buffer.append(tenderDetails);
        return buffer.toString();
    }

}
