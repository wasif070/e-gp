/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblTenderIttClause;
import com.cptu.egp.eps.model.table.TblTenderIttHeader;
import com.cptu.egp.eps.model.table.TblTenderIttSubClause;
import com.cptu.egp.eps.service.serviceimpl.TenderDocumentService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cptu.egp.eps.service.serviceinterface.CreateSubSectionTender;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author yanki
 */
public class CreateSubSectionSrBeanTender extends HttpServlet {

    TenderDocumentService tDocService = (TenderDocumentService) AppContext.getSpringBean("TenderDocumentService");
    final Logger logger = Logger.getLogger(CreateSubSectionSrBeanTender.class);
    String logUserId = "0";

    public void setLogUserId(String logUserId) {
        tDocService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

//            String action = request.getParameter("action");
//            if(action != null){
//                if(action.equalsIgnoreCase("update")){
//                    int headerId = Integer.parseInt(request.getParameter("headerId"));
//                    String headerName = request.getParameter("subSectionDetail");
//                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
//                    CreateSubSectionTender  createSubSection = (CreateSubSectionTender) AppContext.getSpringBean("CreateSubSectionTenderService");
//                    TblTenderIttHeader tblIttHeader = new TblTenderIttHeader();
//                    tblIttHeader.setTemplateIttHeaderId(headerId);
//                    tblIttHeader.setIttHeaderName(headerName);
//                    tblIttHeader.setTblTenderSection(new TblTenderSection(sectionId));
//                    boolean status = createSubSection.updateSubSection(tblIttHeader);
//                    createSubSection = null;
//                    tblIttHeader = null;
//                    out.print(status);
//                }else if(action.equalsIgnoreCase("delete")){
//                    int headerId = Integer.parseInt(request.getParameter("headerId"));
//                    String headerName = request.getParameter("subSectionDetail");
//                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
//                    CreateSubSectionTender  createSubSection = (CreateSubSectionTender) AppContext.getSpringBean("CreateSubSectionTenderService");
//                    TblTenderIttHeader tblIttHeader = new TblTenderIttHeader();
//                    tblIttHeader.setTemplateIttHeaderId(headerId);
//                    tblIttHeader.setIttHeaderName(headerName);
//                    tblIttHeader.setTblTenderSection(new TblTenderSection(sectionId));
//                    boolean status = createSubSection.deleteSubSection(tblIttHeader);
//                    createSubSection = null;
//                    tblIttHeader = null;
//                    out.print(status);
//                }else if(action.equalsIgnoreCase("add")){
//                    String headerName = request.getParameter("subSectionDetail");
//                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
//                    CreateSubSectionTender  createSubSection = (CreateSubSectionTender) AppContext.getSpringBean("CreateSubSectionTenderService");
//                    TblTenderIttHeader tblIttHeader = new TblTenderIttHeader();
//                    tblIttHeader.setIttHeaderName(headerName);
//                    tblIttHeader.setTblTenderSection(new TblTenderSection(sectionId));
//                    boolean status = createSubSection.createSubSection(tblIttHeader);
//                    int ittHeaderId = tblIttHeader.getTenderIttHeaderId();
//                    createSubSection = null;
//                    tblIttHeader = null;
//                    out.print(status + "," + ittHeaderId);
//                }
//            }else{
//                String templateId = request.getParameter("hdTemplateId");
//                String sectionId = request.getParameter("hdSectionId");
//
//                List subSectionName = new ArrayList();
//                int i=1;
//                while(request.getParameter("txtSubSecName"+i) != null){
//                    subSectionName.add(request.getParameter("txtSubSecName"+i));
//                    i++;
//                }
//                CreateSubSectionTender  createSubSection = (CreateSubSectionTender) AppContext.getSpringBean("CreateSubSectionTenderService");
//                String contentType = request.getParameter("contentType");
//
//                TblTenderIttHeader tblIttHeader;
//                for(i=0;i<subSectionName.size();i++){
//                    tblIttHeader = new TblTenderIttHeader();
//                    tblIttHeader.setTblTenderSection(new TblTenderSection(Integer.parseInt(sectionId)));
//                    tblIttHeader.setIttHeaderName(subSectionName.get(i).toString());
//                    boolean flag = createSubSection.createSubSection(tblIttHeader);                    
//                    tblIttHeader = null;
//                }
//                response.sendRedirect("admin/subSectionDashBoard.jsp?sectionId=" + sectionId +"&templateId=" + templateId + "&flag=1");
//            }
        } catch (Exception ex) {
            logger.error("Exception @ CreateSubSectionSrBeanTender:" + ex);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public List<TblTenderIttHeader> getSubSectionDetail(int sectionId) {
        logger.debug("getSubSectionDetail : " + logUserId + " Starts");
        CreateSubSectionTender  createSubSection = (CreateSubSectionTender) AppContext.getSpringBean("CreateSubSectionTenderService");
        List subSection = createSubSection.getSubSection(sectionId);
        logger.debug("getSubSectionDetail : " + logUserId + " Ends");
        return subSection;
    }

    public List<TblTenderIttHeader> getSubSection_TDSAppl(int sectionId) {
        logger.debug("getSubSection_TDSAppl : " + logUserId + " Starts");

        CreateSubSectionTender  createSubSection = (CreateSubSectionTender) AppContext.getSpringBean("CreateSubSectionTenderService");
        List tblIttHeaderList = new ArrayList<TblTenderIttHeader>();


        for (Object[] objects : createSubSection.getSubSectionWithTDSAppl(sectionId)) {
            tblIttHeaderList.add(new TblTenderIttHeader((Integer) objects[0], (String) objects[1]));
        }
        logger.debug("getSubSection_TDSAppl : " + logUserId + " Ends");
        return tblIttHeaderList;
    }

    public String getContectType(int sectionId) {
        logger.debug("getContectType : " + logUserId + " Starts");
        String templateSections = tDocService.getSectionType(sectionId);
        logger.debug("getContectType : " + logUserId + " Ends");
        return templateSections;
    }

    public boolean clauseCreated(int headerId) {
        logger.debug("clauseCreated : " + logUserId + " Starts");
        boolean flag = false;
        long count = tDocService.getClauseCount(headerId);
       // tDocService = null;
        if (count == 0) {
            flag = false;
        } else {
            flag = true;
        }
        logger.debug("clauseCreated : " + logUserId + " Ends");
        return flag;
    }

    public boolean isTdsSubClauseGenerated(int headerId) {
        logger.debug("isTdsSubClauseGenerated : " + logUserId + " Starts");
        boolean flag = false;
        long count = tDocService.getSubClauseCount(headerId);
        //tDocService = null;
        if (count == 0) {
            flag = false;
        } else {
            flag = true;
        }
        logger.debug("isTdsSubClauseGenerated : " + logUserId + " Ends");

        return flag;
    }

    public String getSubSectionName(int headerId) {
        logger.debug("getSubSectionName : " + logUserId + " Starts");
        logger.debug("getSubSectionName : " + logUserId + " Ends");
        return tDocService.getSubSectionName(headerId);
    }

    public List<TblTenderIttClause> getTenderClauseDetail(int headerId) {
        logger.debug("getTenderClauseDetail : " + logUserId + " Starts");
        logger.debug("getTenderClauseDetail : " + logUserId + " Ends");
        return tDocService.getTenderClauseDetail(headerId);
    }

    public List<TblTenderIttSubClause> getTenderSubClauseDetail(int clauseId) {
        logger.debug("getTenderSubClauseDetail : " + logUserId + " Starts");
        logger.debug("getTenderSubClauseDetail : " + logUserId + " Ends");
        return tDocService.getTenderSubClauseDetail(clauseId);
    }
}
