/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author shreyansh Jogi
 */
public class CompareColumns implements Comparator<Object[]> {

    @Override
    public int compare(Object[] o1, Object[] o2) {
        return ((Integer)o1[0]- (Integer)o2[0]);
    }
    public static void main(String[] args) {

        List<Object[]> list = new ArrayList<Object[]>();
        list.add(new Object[]{"1","2","3","4"});
        list.add(new Object[]{"4","2","3","4"});
        list.add(new Object[]{"6","2","3","4"});
        list.add(new Object[]{"3","2","3","4"});
        list.add(new Object[]{"5","2","3","4"});
        list.add(new Object[]{"2","2","3","4"});
        Collections.sort(list, new CompareColumns());
        for (Object[] objects : list) {
            System.out.println(objects[0]);
        }
    }


}
