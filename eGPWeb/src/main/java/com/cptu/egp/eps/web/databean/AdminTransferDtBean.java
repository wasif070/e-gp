/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

import com.cptu.egp.eps.model.table.TblLoginMaster;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class AdminTransferDtBean {

    public AdminTransferDtBean() {
    }




     private int admintransId;
     private int userId;
     private TblLoginMaster tblLoginMaster;
     private String fullName;
     private String nationalId;
     private short adminId;
     private String emailId;
     private int transferedBy;
     private Date transferDt;
     private String replacedBy;
     private String replacedByEmailId;
     private String comments;
     private String isCurrent;
     private String mobileNo;
    //
   
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getAdmintransId() {
        return (short) admintransId;
    }

    public void setAdmintransId(int admintransId) {
        this.admintransId = admintransId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public short getAdminId() {
        return adminId;
    }

    public void setAdminId(short adminId) {
        this.adminId = adminId;
    }

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public TblLoginMaster getTblLoginMaster() {
        return tblLoginMaster;
    }

    public void setTblLoginMaster(TblLoginMaster tblLoginMaster) {
        this.tblLoginMaster = tblLoginMaster;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public int getTransferedBy() {
        return transferedBy;
    }

    public void setTransferedBy(int transferedBy) {
        this.transferedBy = transferedBy;
    }
 public Date getTransferDt() {
        return transferDt;
    }

    public void setTransferDt(Date transferDt) {
        this.transferDt = transferDt;
    }
 public String getReplacedBy() {
        return replacedBy;
    }

    public void setReplacedBy(String replacedBy) {
        this.replacedBy = replacedBy;
    }
 public String getReplacedByEmailId() {
        return replacedByEmailId;
    }

    public void setReplacedByEmailId(String replacedByEmailId) {
        this.replacedByEmailId = replacedByEmailId;
    }
public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
public String getIsCurrent() {
        return isCurrent;
    }

    public void setIsCurrent(String isCurrent) {
        this.isCurrent = isCurrent;
    }
    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }


}
