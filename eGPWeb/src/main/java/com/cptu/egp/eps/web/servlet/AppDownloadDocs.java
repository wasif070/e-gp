package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.servicebean.APPSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CheckExtension;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class AppDownloadDocs extends HttpServlet {

    private final CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
    private final UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");

    private static final Logger LOGGER = Logger.getLogger(APPReportServlet.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND  = "Ends";
    private String logUserId ="0";
    
    private static final String TMP_DIR_PATH = "c:\\tmp";
    private File tmpDir;
    private static final String DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("AppDownloadDocs");
    private File destinationDir;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        tmpDir = new File(TMP_DIR_PATH);
        if (!tmpDir.isDirectory()) {
            tmpDir.mkdir();
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //PrintWriter out = response.getWriter();
        LOGGER.debug("processRequest : "+logUserId+ LOGGERSTART);
        File file = null;
        String docSizeMsg = "";
        boolean checkret = false;
        boolean flag = false;
        String pckNo = "";
        String desc = "";
        int appId = 0;
        int pckId = 0;
        String fileName = "";
        String documentBrief = "";
        long fileSize = 0;
        String queryString = "";
        //String pageNameFilter = "officer/APPDashboardDocs.jsp";
        String pageName = "officer/APPDashboardDocs.jsp";
        ;
        response.setContentType("text/html");
        boolean dis = false;
        HttpSession session = request.getSession();
        
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
            commonXMLSPService.setLogUserId(logUserId);
            userRegisterService.setUserId(logUserId);
        }

        if (session.getAttribute("userId") == null) {
            response.sendRedirect("SessionTimedOut.jsp");
        } else {

            try {
                try {
                    if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("remove")) {
                        int docId = 0;                       
                        String docName = "";
                        if (request.getParameter("docId") != null) {
                            docId = Integer.parseInt(request.getParameter("docId"));

                        }
                        if (request.getParameter("appId") != null) {
                            appId = Integer.parseInt(request.getParameter("appId"));

                        }
                        if (request.getParameter("pckId") != null) {
                            pckId = Integer.parseInt(request.getParameter("pckId"));

                        }

                        if (request.getParameter("pckNo") != null) {
                            pckNo = request.getParameter("pckNo");

                        }
                        if (request.getParameter("desc") != null) {
                            desc = request.getParameter("desc");

                        }


                        if (request.getParameter("docName") != null) {
                            docName = request.getParameter("docName");
                        }
                        String whereContition = "";
                        whereContition = "appEngEstId= " + docId;
                        int j = docName.lastIndexOf('.');
                        String lst = docName.substring(j + 1);
                        String fst = docName.substring(0, j);
                        fileName = fst + "_" + docId + "." + lst;
                        file = new File(DESTINATION_DIR_PATH + appId + "\\" + fileName);
                        fileName = file.toString();
                        checkret = deleteFile(fileName);
                        if (checkret) {
                            CommonMsgChk commonMsgChk;
                            commonMsgChk = commonXMLSPService.insertDataBySP("delete", "tbl_AppEngEstDoc", "", whereContition).get(0);
                            queryString = "?appId=" + appId + "&pckId=" + pckId + "&pckno=" + pckNo + "&desc=" + desc+"&fq=Removed";
                            response.sendRedirect(pageName + queryString);
                        }

                    } else if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("download")) {
                        String docName = request.getParameter("docName");
                        String docId = request.getParameter("docId");
                        int j = docName.lastIndexOf('.');
                        String lst = docName.substring(j + 1);
                        String fst = docName.substring(0, j);
                        fileName = fst + "_" + docId + "." + lst;
                        file = new File(DESTINATION_DIR_PATH + request.getParameter("appId") + "\\" + fileName);//request.getSession().getAttribute("userId")
                        InputStream fis = new FileInputStream(file);
                        byte[] buf = new byte[Integer.valueOf(request.getParameter("docSize"))];
                        int offset = 0;
                        int numRead = 0;
                        while ((offset < buf.length) && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {
                            offset += numRead;
                        }
                        fis.close();
                        response.setContentType("application/octet-stream");
                        response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
                        ServletOutputStream outputStream = response.getOutputStream();
                        outputStream.write(buf);
                        outputStream.flush();
                        outputStream.close();
                    } else {
                        DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
                        /*
                         *Set the size threshold, above which content will be stored on disk.
                         */
                        fileItemFactory.setSizeThreshold(4 * 1024 * 1024); //1 MB
		/*
                         * Set the temporary directory to store the uploaded files of size above threshold.
                         */
                        fileItemFactory.setRepository(tmpDir);

                        ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
                        /*
                         * Parse the request
                         */
                        List items = uploadHandler.parseRequest(request);
                        Iterator itr = items.iterator();
                        //For Supporting Document
                        while (itr.hasNext()) {
                            FileItem item = (FileItem) itr.next();
                            //For Supporting Document
                    /*
                             * Handle Form Fields.
                             */
                            if (item.isFormField()) {

                                if (item.getFieldName().equals("documentBrief")) {
                                    if (item.getString() == null || item.getString().trim().length() == 0) {
                                        dis = true;
                                        break;
                                    }

                                    documentBrief = item.getString();

                                } else if (item.getFieldName().equals("appId")) {
                                    appId = Integer.parseInt(item.getString());
                                } else if (item.getFieldName().equals("pckId")) {
                                    pckId = Integer.parseInt(item.getString());

                                } else if (item.getFieldName().equals("pckNo")) {
                                    pckNo = item.getString();

                                } else if (item.getFieldName().equals("desc")) {
                                    desc = item.getString();

                                }
                            } else {
                                //Handle Uploaded files.
                                /*
                                 * Write file to the ultimate location.
                                 */
                                if (item.getName().lastIndexOf("\\") == -1) {
                                    fileName = item.getName();
                                } else {
                                    fileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1, item.getName().length());
                                }
                                //fileName  = fileName.replaceAll(" ", "");
                                String realPath = DESTINATION_DIR_PATH + appId;//request.getSession().getAttribute("userId")
                                destinationDir = new File(realPath);
                                if (!destinationDir.isDirectory()) {
                                    destinationDir.mkdir();
                                }
                                docSizeMsg = docSizeMsg(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
                               
                                if (docSizeMsg.equals("ok")){

                                    fileSize = item.getSize();
                                    checkret = checkExnAndSize(fileName, item.getSize(), "officer");

                                    if (!checkret) {

                                        break;
                                    } else {
                                        file = new File(destinationDir, fileName);
                                        if (file.isFile()) {
                                            flag = true;
                                         
                                            break;

                                        }
                                        item.write(file);
                                    }
                                }

                            }

                        }
                    }

                } catch (FileUploadException ex) {
                     LOGGER.error("processRequest "+logUserId+" : "+ex.toString());
                } catch (Exception ex) {
                     LOGGER.error("processRequest "+logUserId+" : "+ex.toString());
                }

//                if (!docSizeMsg.equals("ok")) {
//                    queryString = "?fq=" + docSizeMsg;

            //    } else {
                    if (dis) {
                        docSizeMsg = "Please Enter Discription";
                        queryString = "?fq=" + docSizeMsg + "&appId=" + appId + "&pckId=" + pckId + "&pckno=" + pckNo + "&desc=" + desc;
                        
                        response.sendRedirect(pageName + queryString);
                    } else {
                        if (flag) {
                            docSizeMsg = "File already Exists";
                            queryString = "?fq=" + docSizeMsg + "&appId=" + appId + "&pckId=" + pckId + "&pckno=" + pckNo + "&desc=" + desc;
                            response.sendRedirect(pageName + queryString);
                        } else {

                            if (!checkret) {
                                CheckExtension ext = new CheckExtension();
                                TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("officer");
                                queryString = "?fs=" + configurationMaster.getFileSize() + "&ft="+configurationMaster.getAllowedExtension()+" &appId="+appId+"&pckId=" + pckId + "&pckno=" + pckNo + "&desc=" + desc;
                                response.sendRedirect(pageName + queryString);

                            } else {
                                if (documentBrief != null && !"".equals(documentBrief)) {
                                    APPSrBean appSrBean = new APPSrBean();
                                     int userId = Integer.parseInt(request.getSession().getAttribute("userId").toString());
                                    //int userId = 1;
                                    HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                                   documentBrief = handleSpecialChar.handleSpecialChar(documentBrief);
                                    int docid = appSrBean.addAppEngEstDoc(documentBrief, appId, pckId, userId, fileName, fileSize);
                                    File oldfile = new File(destinationDir + "\\" + fileName);
                                    int j = fileName.lastIndexOf('.');
                                    String lst = fileName.substring(j + 1);
                                    String fst = fileName.substring(0, j);
                                    File newFile = new File(destinationDir + "\\" + fst + "_" + docid + "." + lst);
                                    boolean sus = oldfile.renameTo(newFile);
                                    if (sus) {

                                        response.sendRedirect("officer/APPDashboardDocs.jsp?appId=" + appId + "&pckId=" + pckId + "&pckno=" + pckNo + "&desc=" + desc+"&fq=Uploaded");
                                    }
                                    //    response.sendRedirect(request.getContextPath() + "/officer/APPDashboardDocs.jsp?appId=" + appId + "&pckId=" + pckId + "&pckNo=" + pckNo + "&desc=" + desc);

                                }
                            }
                        }
                    }

              //  }


            } finally {
                //   out.close();
            }
        }
        LOGGER.debug("processRequest  : "+logUserId+ LOGGEREND);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public boolean checkExnAndSize(String extn, long size, String userType) {
        
        LOGGER.debug("checkExnAndSize  : "+logUserId+ LOGGERSTART);
        boolean chextn = false;
        float fsize = 0.0f;
        float dsize = 0.0f;
        int j = extn.lastIndexOf('.');
        String lst = extn.substring(j + 1);
        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        String str = configurationMaster.getAllowedExtension();
        String[] str1 = str.split(",");
        for (int i = 0; i < str1.length; i++) {
            if (str1[i].trim().equalsIgnoreCase(lst)) {
                chextn = true;
            }
        }
        if (chextn) {
            fsize = size / (1024 * 1024);
         
            dsize = configurationMaster.getFileSize();
         
            if (dsize > fsize) {
                chextn = true;
            } else {
                chextn = false;
            }
        }
        LOGGER.debug("checkExnAndSize  : "+logUserId+ LOGGEREND);
        return chextn;
    }

    public String docSizeMsg(int userId) {
        String str = "";
        LOGGER.debug("docSizeMsg  : "+logUserId+ LOGGERSTART);
        try{
            str = userRegisterService.docSizeCheck(userId);
        }catch(Exception ex){
             LOGGER.error("docSizeMsg "+logUserId+" : "+ex.toString());
    }
        LOGGER.debug("docSizeMsg  : "+logUserId+ LOGGEREND);
        return str;
    }

    public boolean deleteFile(String filePath) {
        LOGGER.debug("deleteFile  : "+logUserId+ LOGGERSTART);
        boolean flag = false;
        File f = new File(filePath);
        if (f.delete()) {
            flag = true;
        }
        LOGGER.debug("deleteFile  : "+logUserId+ LOGGEREND);
        return flag;
    }
}

