/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.web.databean.ReportGenerateDtBean;
import java.math.BigDecimal;
import java.util.Comparator;

/**
 *
 * @author TaherT
 */
public class Comparison {
}

class CompareLotBidderL1 implements Comparator<ReportGenerateDtBean> {

    @Override
    public int compare(ReportGenerateDtBean o1, ReportGenerateDtBean o2) {
        int compare = 0;
        if (o1.getGovColumn() != null && o2.getGovColumn() != null) {
            //compare = Integer.parseInt(o1.getGovColumn()) - Integer.parseInt(o2.getGovColumn());
            compare = new BigDecimal(o1.getGovColumn()).compareTo(new BigDecimal(o2.getGovColumn()));
        }
        return compare;
    }
}

class CompareLotBidderH1 implements Comparator<ReportGenerateDtBean> {

    @Override
    public int compare(ReportGenerateDtBean o1, ReportGenerateDtBean o2) {
        int compare = 0;
        if (o1.getGovColumn() != null && o2.getGovColumn() != null) {
            //compare = Integer.parseInt(o2.getGovColumn()) - Integer.parseInt(o1.getGovColumn());
            compare = new BigDecimal(o2.getGovColumn()).compareTo(new BigDecimal(o1.getGovColumn()));
        }
        return compare;
    }
}
