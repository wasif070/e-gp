/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.offlinedata;

/**
 *
 * @author Administrator
 */
public class EOIExcellBean {

private String ministryName;
private String agency;
private String procuringEntityDistrict;
private String interestForSelectionOfConsult;
private String interestForSelectionOfBased;
private String procurementSubmethod;
private String budgetFund;
private String sourceFund;
private String associationwithforeignfirms;
private String procuringEntityName;
private String procuringEntityCode;
private String eoiRefNo;
private String eoiDate;
private String developmentPartner;
private String projectOrProgramCode;
private String projectOrProgrammeName;
private String eoiClosingDateandtime;
private String briefDescriptionoftheAssignment;
private String expResDeliverCapacityRequired;
private String otherDetails;
private String refNo_1;
private String phasingOfServices_1;
private String location_1;
private String indicativeStartDateMonOrYer_1;
private String indicativeCompletionDateMonOrYer_1;
private String refNo_2;
private String phasingOfServices_2;
private String location_2;
private String indicativeStartDateMonOrYer_2;
private String indicativeCompletionDateMonOrYer_2;
private String refNo_3;
private String phasingOfServices_3;
private String location_3;
private String indicativeStartDateMonOrYer_3;
private String indicativeCompletionDateMonOrYer_3;
private String refNo_4;
private String phasingOfServices_4;
private String location_4;
private String indicativeStartDateMonOrYer_4;
private String indicativeCompletionDateMonOrYer_4;
private String refNo_5;
private String phasingOfServices_5;
private String location_5;
private String indicativeStartDateMonOrYer_5;
private String indicativeCompletionDateMonOrYer_5;
private String refNo_6;
private String phasingOfServices_6;
private String location_6;
private String indicativeStartDateMonOrYer_6;
private String indicativeCompletionDateMonOrYer_6;
private String refNo_7;
private String phasingOfServices_7;
private String location_7;
private String indicativeStartDateMonOrYer_7;
private String indicativeCompletionDateMonOrYer_7;
private String refNo_8;
private String phasingOfServices_8;
private String location_8;
private String indicativeStartDateMonOrYer_8;
private String indicativeCompletionDateMonOrYer_8;
private String refNo_9;
private String phasingOfServices_9;
private String location_9;
private String indicativeStartDateMonOrYer_9;
private String indicativeCompletionDateMonOrYer_9;
private String refNo_10;
private String phasingOfServices_10;
private String location_10;
private String indicativeStartDateMonOrYer_10;
private String indicativeCompletionDateMonOrYer_10;
private String nameoftheOfficialInvitingEOI;
private String designationoftheOfficialInvitingEOI;
private String addressoftheOfficialInvitingEOI;
private String contactDetailsoftheOfficialInvitingEOI;
private String fax;
private String email;

    /**
     * @return the ministryName
     */
    public String getMinistryName() {
        return ministryName;
    }

    /**
     * @param ministryName the ministryName to set
     */
    public void setMinistryName(String ministryName) {
        this.ministryName = ministryName;
    }

    /**
     * @return the agency
     */
    public String getAgency() {
        return agency;
    }

    /**
     * @param agency the agency to set
     */
    public void setAgency(String agency) {
        this.agency = agency;
    }

    /**
     * @return the procuringEntityDistrict
     */
    public String getProcuringEntityDistrict() {
        return procuringEntityDistrict;
    }

    /**
     * @param procuringEntityDistrict the procuringEntityDistrict to set
     */
    public void setProcuringEntityDistrict(String procuringEntityDistrict) {
        this.procuringEntityDistrict = procuringEntityDistrict;
    }

    /**
     * @return the interestForSelectionOfConsult
     */
    public String getInterestForSelectionOfConsult() {
        return interestForSelectionOfConsult;
    }

    /**
     * @param interestForSelectionOfConsult the interestForSelectionOfConsult to set
     */
    public void setInterestForSelectionOfConsult(String interestForSelectionOfConsult) {
        this.interestForSelectionOfConsult = interestForSelectionOfConsult;
    }

    /**
     * @return the interestForSelectionOfBased
     */
    public String getInterestForSelectionOfBased() {
        return interestForSelectionOfBased;
    }

    /**
     * @param interestForSelectionOfBased the interestForSelectionOfBased to set
     */
    public void setInterestForSelectionOfBased(String interestForSelectionOfBased) {
        this.interestForSelectionOfBased = interestForSelectionOfBased;
    }

    /**
     * @return the procurementSubmethod
     */
    public String getProcurementSubmethod() {
        return procurementSubmethod;
    }

    /**
     * @param procurementSubmethod the procurementSubmethod to set
     */
    public void setProcurementSubmethod(String procurementSubmethod) {
        this.procurementSubmethod = procurementSubmethod;
    }

    /**
     * @return the budgetFund
     */
    public String getBudgetFund() {
        return budgetFund;
    }

    /**
     * @param budgetFund the budgetFund to set
     */
    public void setBudgetFund(String budgetFund) {
        this.budgetFund = budgetFund;
    }

    /**
     * @return the sourceFund
     */
    public String getSourceFund() {
        return sourceFund;
    }

    /**
     * @param sourceFund the sourceFund to set
     */
    public void setSourceFund(String sourceFund) {
        this.sourceFund = sourceFund;
    }

    /**
     * @return the associationwithforeignfirms
     */
    public String getAssociationwithforeignfirms() {
        return associationwithforeignfirms;
    }

    /**
     * @param associationwithforeignfirms the associationwithforeignfirms to set
     */
    public void setAssociationwithforeignfirms(String associationwithforeignfirms) {
        this.associationwithforeignfirms = associationwithforeignfirms;
    }

    /**
     * @return the procuringEntityName
     */
    public String getProcuringEntityName() {
        return procuringEntityName;
    }

    /**
     * @param procuringEntityName the procuringEntityName to set
     */
    public void setProcuringEntityName(String procuringEntityName) {
        this.procuringEntityName = procuringEntityName;
    }

      /**
     * @return the developmentPartner
     */
    public String getDevelopmentPartner() {
        return developmentPartner;
    }

    /**
     * @param developmentPartner the developmentPartner to set
     */
    public void setDevelopmentPartner(String developmentPartner) {
        this.developmentPartner = developmentPartner;
    }

    /**
     * @return the projectOrProgramCode
     */
    public String getProjectOrProgramCode() {
        return projectOrProgramCode;
    }

    /**
     * @param projectOrProgramCode the projectOrProgramCode to set
     */
    public void setProjectOrProgramCode(String projectOrProgramCode) {
        this.projectOrProgramCode = projectOrProgramCode;
    }

    /**
     * @return the projectOrProgrammeName
     */
    public String getProjectOrProgrammeName() {
        return projectOrProgrammeName;
    }

    /**
     * @param projectOrProgrammeName the projectOrProgrammeName to set
     */
    public void setProjectOrProgrammeName(String projectOrProgrammeName) {
        this.projectOrProgrammeName = projectOrProgrammeName;
    }

 

    /**
     * @return the briefDescriptionoftheAssignment
     */
    public String getBriefDescriptionoftheAssignment() {
        return briefDescriptionoftheAssignment;
    }

    /**
     * @param briefDescriptionoftheAssignment the briefDescriptionoftheAssignment to set
     */
    public void setBriefDescriptionoftheAssignment(String briefDescriptionoftheAssignment) {
        this.briefDescriptionoftheAssignment = briefDescriptionoftheAssignment;
    }

    /**
     * @return the expResDeliverCapacityRequired
     */
    public String getExpResDeliverCapacityRequired() {
        return expResDeliverCapacityRequired;
    }

    /**
     * @param expResDeliverCapacityRequired the expResDeliverCapacityRequired to set
     */
    public void setExpResDeliverCapacityRequired(String expResDeliverCapacityRequired) {
        this.expResDeliverCapacityRequired = expResDeliverCapacityRequired;
    }

    /**
     * @return the otherDetails
     */
    public String getOtherDetails() {
        return otherDetails;
    }

    /**
     * @param otherDetails the otherDetails to set
     */
    public void setOtherDetails(String otherDetails) {
        this.otherDetails = otherDetails;
    }

    /**
     * @return the refNo_1
     */
    public String getRefNo_1() {
        return refNo_1;
    }

    /**
     * @param refNo_1 the refNo_1 to set
     */
    public void setRefNo_1(String refNo_1) {
        this.refNo_1 = refNo_1;
    }

    /**
     * @return the phasingOfServices_1
     */
    public String getPhasingOfServices_1() {
        return phasingOfServices_1;
    }

    /**
     * @param phasingOfServices_1 the phasingOfServices_1 to set
     */
    public void setPhasingOfServices_1(String phasingOfServices_1) {
        this.phasingOfServices_1 = phasingOfServices_1;
    }

    /**
     * @return the location_1
     */
    public String getLocation_1() {
        return location_1;
    }

    /**
     * @param location_1 the location_1 to set
     */
    public void setLocation_1(String location_1) {
        this.location_1 = location_1;
    }

    /**
     * @return the indicativeStartDateMonOrYer_1
     */
    public String getIndicativeStartDateMonOrYer_1() {
        return indicativeStartDateMonOrYer_1;
    }

    /**
     * @param indicativeStartDateMonOrYer_1 the indicativeStartDateMonOrYer_1 to set
     */
    public void setIndicativeStartDateMonOrYer_1(String indicativeStartDateMonOrYer_1) {
        this.indicativeStartDateMonOrYer_1 = indicativeStartDateMonOrYer_1;
    }

    /**
     * @return the indicativeCompletionDateMonOrYer_1
     */
    public String getIndicativeCompletionDateMonOrYer_1() {
        return indicativeCompletionDateMonOrYer_1;
    }

    /**
     * @param indicativeCompletionDateMonOrYer_1 the indicativeCompletionDateMonOrYer_1 to set
     */
    public void setIndicativeCompletionDateMonOrYer_1(String indicativeCompletionDateMonOrYer_1) {
        this.indicativeCompletionDateMonOrYer_1 = indicativeCompletionDateMonOrYer_1;
    }

    /**
     * @return the refNo_2
     */
    public String getRefNo_2() {
        return refNo_2;
    }

    /**
     * @param refNo_2 the refNo_2 to set
     */
    public void setRefNo_2(String refNo_2) {
        this.refNo_2 = refNo_2;
    }

    /**
     * @return the phasingOfServices_2
     */
    public String getPhasingOfServices_2() {
        return phasingOfServices_2;
    }

    /**
     * @param phasingOfServices_2 the phasingOfServices_2 to set
     */
    public void setPhasingOfServices_2(String phasingOfServices_2) {
        this.phasingOfServices_2 = phasingOfServices_2;
    }

    /**
     * @return the location_2
     */
    public String getLocation_2() {
        return location_2;
    }

    /**
     * @param location_2 the location_2 to set
     */
    public void setLocation_2(String location_2) {
        this.location_2 = location_2;
    }

    /**
     * @return the indicativeStartDateMonOrYer_2
     */
    public String getIndicativeStartDateMonOrYer_2() {
        return indicativeStartDateMonOrYer_2;
    }

    /**
     * @param indicativeStartDateMonOrYer_2 the indicativeStartDateMonOrYer_2 to set
     */
    public void setIndicativeStartDateMonOrYer_2(String indicativeStartDateMonOrYer_2) {
        this.indicativeStartDateMonOrYer_2 = indicativeStartDateMonOrYer_2;
    }

    /**
     * @return the indicativeCompletionDateMonOrYer_2
     */
    public String getIndicativeCompletionDateMonOrYer_2() {
        return indicativeCompletionDateMonOrYer_2;
    }

    /**
     * @param indicativeCompletionDateMonOrYer_2 the indicativeCompletionDateMonOrYer_2 to set
     */
    public void setIndicativeCompletionDateMonOrYer_2(String indicativeCompletionDateMonOrYer_2) {
        this.indicativeCompletionDateMonOrYer_2 = indicativeCompletionDateMonOrYer_2;
    }

    /**
     * @return the refNo_3
     */
    public String getRefNo_3() {
        return refNo_3;
    }

    /**
     * @param refNo_3 the refNo_3 to set
     */
    public void setRefNo_3(String refNo_3) {
        this.refNo_3 = refNo_3;
    }

    /**
     * @return the phasingOfServices_3
     */
    public String getPhasingOfServices_3() {
        return phasingOfServices_3;
    }

    /**
     * @param phasingOfServices_3 the phasingOfServices_3 to set
     */
    public void setPhasingOfServices_3(String phasingOfServices_3) {
        this.phasingOfServices_3 = phasingOfServices_3;
    }

    /**
     * @return the location_3
     */
    public String getLocation_3() {
        return location_3;
    }

    /**
     * @param location_3 the location_3 to set
     */
    public void setLocation_3(String location_3) {
        this.location_3 = location_3;
    }

    /**
     * @return the indicativeStartDateMonOrYer_3
     */
    public String getIndicativeStartDateMonOrYer_3() {
        return indicativeStartDateMonOrYer_3;
    }

    /**
     * @param indicativeStartDateMonOrYer_3 the indicativeStartDateMonOrYer_3 to set
     */
    public void setIndicativeStartDateMonOrYer_3(String indicativeStartDateMonOrYer_3) {
        this.indicativeStartDateMonOrYer_3 = indicativeStartDateMonOrYer_3;
    }

    /**
     * @return the indicativeCompletionDateMonOrYer_3
     */
    public String getIndicativeCompletionDateMonOrYer_3() {
        return indicativeCompletionDateMonOrYer_3;
    }

    /**
     * @param indicativeCompletionDateMonOrYer_3 the indicativeCompletionDateMonOrYer_3 to set
     */
    public void setIndicativeCompletionDateMonOrYer_3(String indicativeCompletionDateMonOrYer_3) {
        this.indicativeCompletionDateMonOrYer_3 = indicativeCompletionDateMonOrYer_3;
    }

    /**
     * @return the refNo_4
     */
    public String getRefNo_4() {
        return refNo_4;
    }

    /**
     * @param refNo_4 the refNo_4 to set
     */
    public void setRefNo_4(String refNo_4) {
        this.refNo_4 = refNo_4;
    }

    /**
     * @return the phasingOfServices_4
     */
    public String getPhasingOfServices_4() {
        return phasingOfServices_4;
    }

    /**
     * @param phasingOfServices_4 the phasingOfServices_4 to set
     */
    public void setPhasingOfServices_4(String phasingOfServices_4) {
        this.phasingOfServices_4 = phasingOfServices_4;
    }

    /**
     * @return the location_4
     */
    public String getLocation_4() {
        return location_4;
    }

    /**
     * @param location_4 the location_4 to set
     */
    public void setLocation_4(String location_4) {
        this.location_4 = location_4;
    }

    /**
     * @return the indicativeStartDateMonOrYer_4
     */
    public String getIndicativeStartDateMonOrYer_4() {
        return indicativeStartDateMonOrYer_4;
    }

    /**
     * @param indicativeStartDateMonOrYer_4 the indicativeStartDateMonOrYer_4 to set
     */
    public void setIndicativeStartDateMonOrYer_4(String indicativeStartDateMonOrYer_4) {
        this.indicativeStartDateMonOrYer_4 = indicativeStartDateMonOrYer_4;
    }

    /**
     * @return the indicativeCompletionDateMonOrYer_4
     */
    public String getIndicativeCompletionDateMonOrYer_4() {
        return indicativeCompletionDateMonOrYer_4;
    }

    /**
     * @param indicativeCompletionDateMonOrYer_4 the indicativeCompletionDateMonOrYer_4 to set
     */
    public void setIndicativeCompletionDateMonOrYer_4(String indicativeCompletionDateMonOrYer_4) {
        this.indicativeCompletionDateMonOrYer_4 = indicativeCompletionDateMonOrYer_4;
    }

    /**
     * @return the refNo_5
     */
    public String getRefNo_5() {
        return refNo_5;
    }

    /**
     * @param refNo_5 the refNo_5 to set
     */
    public void setRefNo_5(String refNo_5) {
        this.refNo_5 = refNo_5;
    }

    /**
     * @return the phasingOfServices_5
     */
    public String getPhasingOfServices_5() {
        return phasingOfServices_5;
    }

    /**
     * @param phasingOfServices_5 the phasingOfServices_5 to set
     */
    public void setPhasingOfServices_5(String phasingOfServices_5) {
        this.phasingOfServices_5 = phasingOfServices_5;
    }

    /**
     * @return the location_5
     */
    public String getLocation_5() {
        return location_5;
    }

    /**
     * @param location_5 the location_5 to set
     */
    public void setLocation_5(String location_5) {
        this.location_5 = location_5;
    }

    /**
     * @return the indicativeStartDateMonOrYer_5
     */
    public String getIndicativeStartDateMonOrYer_5() {
        return indicativeStartDateMonOrYer_5;
    }

    /**
     * @param indicativeStartDateMonOrYer_5 the indicativeStartDateMonOrYer_5 to set
     */
    public void setIndicativeStartDateMonOrYer_5(String indicativeStartDateMonOrYer_5) {
        this.indicativeStartDateMonOrYer_5 = indicativeStartDateMonOrYer_5;
    }

    /**
     * @return the indicativeCompletionDateMonOrYer_5
     */
    public String getIndicativeCompletionDateMonOrYer_5() {
        return indicativeCompletionDateMonOrYer_5;
    }

    /**
     * @param indicativeCompletionDateMonOrYer_5 the indicativeCompletionDateMonOrYer_5 to set
     */
    public void setIndicativeCompletionDateMonOrYer_5(String indicativeCompletionDateMonOrYer_5) {
        this.indicativeCompletionDateMonOrYer_5 = indicativeCompletionDateMonOrYer_5;
    }

    /**
     * @return the refNo_6
     */
    public String getRefNo_6() {
        return refNo_6;
    }

    /**
     * @param refNo_6 the refNo_6 to set
     */
    public void setRefNo_6(String refNo_6) {
        this.refNo_6 = refNo_6;
    }

    /**
     * @return the phasingOfServices_6
     */
    public String getPhasingOfServices_6() {
        return phasingOfServices_6;
    }

    /**
     * @param phasingOfServices_6 the phasingOfServices_6 to set
     */
    public void setPhasingOfServices_6(String phasingOfServices_6) {
        this.phasingOfServices_6 = phasingOfServices_6;
    }

    /**
     * @return the location_6
     */
    public String getLocation_6() {
        return location_6;
    }

    /**
     * @param location_6 the location_6 to set
     */
    public void setLocation_6(String location_6) {
        this.location_6 = location_6;
    }

    /**
     * @return the indicativeStartDateMonOrYer_6
     */
    public String getIndicativeStartDateMonOrYer_6() {
        return indicativeStartDateMonOrYer_6;
    }

    /**
     * @param indicativeStartDateMonOrYer_6 the indicativeStartDateMonOrYer_6 to set
     */
    public void setIndicativeStartDateMonOrYer_6(String indicativeStartDateMonOrYer_6) {
        this.indicativeStartDateMonOrYer_6 = indicativeStartDateMonOrYer_6;
    }

    /**
     * @return the indicativeCompletionDateMonOrYer_6
     */
    public String getIndicativeCompletionDateMonOrYer_6() {
        return indicativeCompletionDateMonOrYer_6;
    }

    /**
     * @param indicativeCompletionDateMonOrYer_6 the indicativeCompletionDateMonOrYer_6 to set
     */
    public void setIndicativeCompletionDateMonOrYer_6(String indicativeCompletionDateMonOrYer_6) {
        this.indicativeCompletionDateMonOrYer_6 = indicativeCompletionDateMonOrYer_6;
    }

    /**
     * @return the refNo_7
     */
    public String getRefNo_7() {
        return refNo_7;
    }

    /**
     * @param refNo_7 the refNo_7 to set
     */
    public void setRefNo_7(String refNo_7) {
        this.refNo_7 = refNo_7;
    }

    /**
     * @return the phasingOfServices_7
     */
    public String getPhasingOfServices_7() {
        return phasingOfServices_7;
    }

    /**
     * @param phasingOfServices_7 the phasingOfServices_7 to set
     */
    public void setPhasingOfServices_7(String phasingOfServices_7) {
        this.phasingOfServices_7 = phasingOfServices_7;
    }

    /**
     * @return the location_7
     */
    public String getLocation_7() {
        return location_7;
    }

    /**
     * @param location_7 the location_7 to set
     */
    public void setLocation_7(String location_7) {
        this.location_7 = location_7;
    }

    /**
     * @return the indicativeStartDateMonOrYer_7
     */
    public String getIndicativeStartDateMonOrYer_7() {
        return indicativeStartDateMonOrYer_7;
    }

    /**
     * @param indicativeStartDateMonOrYer_7 the indicativeStartDateMonOrYer_7 to set
     */
    public void setIndicativeStartDateMonOrYer_7(String indicativeStartDateMonOrYer_7) {
        this.indicativeStartDateMonOrYer_7 = indicativeStartDateMonOrYer_7;
    }

    /**
     * @return the indicativeCompletionDateMonOrYer_7
     */
    public String getIndicativeCompletionDateMonOrYer_7() {
        return indicativeCompletionDateMonOrYer_7;
    }

    /**
     * @param indicativeCompletionDateMonOrYer_7 the indicativeCompletionDateMonOrYer_7 to set
     */
    public void setIndicativeCompletionDateMonOrYer_7(String indicativeCompletionDateMonOrYer_7) {
        this.indicativeCompletionDateMonOrYer_7 = indicativeCompletionDateMonOrYer_7;
    }

    /**
     * @return the refNo_8
     */
    public String getRefNo_8() {
        return refNo_8;
    }

    /**
     * @param refNo_8 the refNo_8 to set
     */
    public void setRefNo_8(String refNo_8) {
        this.refNo_8 = refNo_8;
    }

    /**
     * @return the phasingOfServices_8
     */
    public String getPhasingOfServices_8() {
        return phasingOfServices_8;
    }

    /**
     * @param phasingOfServices_8 the phasingOfServices_8 to set
     */
    public void setPhasingOfServices_8(String phasingOfServices_8) {
        this.phasingOfServices_8 = phasingOfServices_8;
    }

    /**
     * @return the location_8
     */
    public String getLocation_8() {
        return location_8;
    }

    /**
     * @param location_8 the location_8 to set
     */
    public void setLocation_8(String location_8) {
        this.location_8 = location_8;
    }

    /**
     * @return the indicativeStartDateMonOrYer_8
     */
    public String getIndicativeStartDateMonOrYer_8() {
        return indicativeStartDateMonOrYer_8;
    }

    /**
     * @param indicativeStartDateMonOrYer_8 the indicativeStartDateMonOrYer_8 to set
     */
    public void setIndicativeStartDateMonOrYer_8(String indicativeStartDateMonOrYer_8) {
        this.indicativeStartDateMonOrYer_8 = indicativeStartDateMonOrYer_8;
    }

    /**
     * @return the indicativeCompletionDateMonOrYer_8
     */
    public String getIndicativeCompletionDateMonOrYer_8() {
        return indicativeCompletionDateMonOrYer_8;
    }

    /**
     * @param indicativeCompletionDateMonOrYer_8 the indicativeCompletionDateMonOrYer_8 to set
     */
    public void setIndicativeCompletionDateMonOrYer_8(String indicativeCompletionDateMonOrYer_8) {
        this.indicativeCompletionDateMonOrYer_8 = indicativeCompletionDateMonOrYer_8;
    }

    /**
     * @return the refNo_9
     */
    public String getRefNo_9() {
        return refNo_9;
    }

    /**
     * @param refNo_9 the refNo_9 to set
     */
    public void setRefNo_9(String refNo_9) {
        this.refNo_9 = refNo_9;
    }

    /**
     * @return the phasingOfServices_9
     */
    public String getPhasingOfServices_9() {
        return phasingOfServices_9;
    }

    /**
     * @param phasingOfServices_9 the phasingOfServices_9 to set
     */
    public void setPhasingOfServices_9(String phasingOfServices_9) {
        this.phasingOfServices_9 = phasingOfServices_9;
    }

    /**
     * @return the location_9
     */
    public String getLocation_9() {
        return location_9;
    }

    /**
     * @param location_9 the location_9 to set
     */
    public void setLocation_9(String location_9) {
        this.location_9 = location_9;
    }

    /**
     * @return the indicativeStartDateMonOrYer_9
     */
    public String getIndicativeStartDateMonOrYer_9() {
        return indicativeStartDateMonOrYer_9;
    }

    /**
     * @param indicativeStartDateMonOrYer_9 the indicativeStartDateMonOrYer_9 to set
     */
    public void setIndicativeStartDateMonOrYer_9(String indicativeStartDateMonOrYer_9) {
        this.indicativeStartDateMonOrYer_9 = indicativeStartDateMonOrYer_9;
    }

    /**
     * @return the indicativeCompletionDateMonOrYer_9
     */
    public String getIndicativeCompletionDateMonOrYer_9() {
        return indicativeCompletionDateMonOrYer_9;
    }

    /**
     * @param indicativeCompletionDateMonOrYer_9 the indicativeCompletionDateMonOrYer_9 to set
     */
    public void setIndicativeCompletionDateMonOrYer_9(String indicativeCompletionDateMonOrYer_9) {
        this.indicativeCompletionDateMonOrYer_9 = indicativeCompletionDateMonOrYer_9;
    }

    /**
     * @return the refNo_10
     */
    public String getRefNo_10() {
        return refNo_10;
    }

    /**
     * @param refNo_10 the refNo_10 to set
     */
    public void setRefNo_10(String refNo_10) {
        this.refNo_10 = refNo_10;
    }

    /**
     * @return the phasingOfServices_10
     */
    public String getPhasingOfServices_10() {
        return phasingOfServices_10;
    }

    /**
     * @param phasingOfServices_10 the phasingOfServices_10 to set
     */
    public void setPhasingOfServices_10(String phasingOfServices_10) {
        this.phasingOfServices_10 = phasingOfServices_10;
    }

    /**
     * @return the location_10
     */
    public String getLocation_10() {
        return location_10;
    }

    /**
     * @param location_10 the location_10 to set
     */
    public void setLocation_10(String location_10) {
        this.location_10 = location_10;
    }

    /**
     * @return the indicativeStartDateMonOrYer_10
     */
    public String getIndicativeStartDateMonOrYer_10() {
        return indicativeStartDateMonOrYer_10;
    }

    /**
     * @param indicativeStartDateMonOrYer_10 the indicativeStartDateMonOrYer_10 to set
     */
    public void setIndicativeStartDateMonOrYer_10(String indicativeStartDateMonOrYer_10) {
        this.indicativeStartDateMonOrYer_10 = indicativeStartDateMonOrYer_10;
    }

    /**
     * @return the indicativeCompletionDateMonOrYer_10
     */
    public String getIndicativeCompletionDateMonOrYer_10() {
        return indicativeCompletionDateMonOrYer_10;
    }

    /**
     * @param indicativeCompletionDateMonOrYer_10 the indicativeCompletionDateMonOrYer_10 to set
     */
    public void setIndicativeCompletionDateMonOrYer_10(String indicativeCompletionDateMonOrYer_10) {
        this.indicativeCompletionDateMonOrYer_10 = indicativeCompletionDateMonOrYer_10;
    }

    /**
     * @return the nameoftheOfficialInvitingEOI
     */
    public String getNameoftheOfficialInvitingEOI() {
        return nameoftheOfficialInvitingEOI;
    }

    /**
     * @param nameoftheOfficialInvitingEOI the nameoftheOfficialInvitingEOI to set
     */
    public void setNameoftheOfficialInvitingEOI(String nameoftheOfficialInvitingEOI) {
        this.nameoftheOfficialInvitingEOI = nameoftheOfficialInvitingEOI;
    }

    /**
     * @return the designationoftheOfficialInvitingEOI
     */
    public String getDesignationoftheOfficialInvitingEOI() {
        return designationoftheOfficialInvitingEOI;
    }

    /**
     * @param designationoftheOfficialInvitingEOI the designationoftheOfficialInvitingEOI to set
     */
    public void setDesignationoftheOfficialInvitingEOI(String designationoftheOfficialInvitingEOI) {
        this.designationoftheOfficialInvitingEOI = designationoftheOfficialInvitingEOI;
    }

    /**
     * @return the addressoftheOfficialInvitingEOI
     */
    public String getAddressoftheOfficialInvitingEOI() {
        return addressoftheOfficialInvitingEOI;
    }

    /**
     * @param addressoftheOfficialInvitingEOI the addressoftheOfficialInvitingEOI to set
     */
    public void setAddressoftheOfficialInvitingEOI(String addressoftheOfficialInvitingEOI) {
        this.addressoftheOfficialInvitingEOI = addressoftheOfficialInvitingEOI;
    }

    /**
     * @return the contactDetailsoftheOfficialInvitingEOI
     */
    public String getContactDetailsoftheOfficialInvitingEOI() {
        return contactDetailsoftheOfficialInvitingEOI;
    }

    /**
     * @param contactDetailsoftheOfficialInvitingEOI the contactDetailsoftheOfficialInvitingEOI to set
     */
    public void setContactDetailsoftheOfficialInvitingEOI(String contactDetailsoftheOfficialInvitingEOI) {
        this.contactDetailsoftheOfficialInvitingEOI = contactDetailsoftheOfficialInvitingEOI;
    }

    /**
     * @return the fax
     */
    public String getFax() {
        return fax;
    }

    /**
     * @param fax the fax to set
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the eoiRefNo
     */
    public String getEoiRefNo() {
        return eoiRefNo;
    }

    /**
     * @param eoiRefNo the eoiRefNo to set
     */
    public void setEoiRefNo(String eoiRefNo) {
        this.eoiRefNo = eoiRefNo;
    }

    /**
     * @return the eoiDate
     */
    public String getEoiDate() {
        return eoiDate;
    }

    /**
     * @param eoiDate the eoiDate to set
     */
    public void setEoiDate(String eoiDate) {
        this.eoiDate = eoiDate;
    }

    /**
     * @return the eoiClosingDateandtime
     */
    public String getEoiClosingDateandtime() {
        return eoiClosingDateandtime;
    }

    /**
     * @param eoiClosingDateandtime the eoiClosingDateandtime to set
     */
    public void setEoiClosingDateandtime(String eoiClosingDateandtime) {
        this.eoiClosingDateandtime = eoiClosingDateandtime;
    }

    /**
     * @return the procuringEntityCode
     */
    public String getProcuringEntityCode() {
        return procuringEntityCode;
    }

    /**
     * @param procuringEntityCode the procuringEntityCode to set
     */
    public void setProcuringEntityCode(String procuringEntityCode) {
        this.procuringEntityCode = procuringEntityCode;
    }


}
