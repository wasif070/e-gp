/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblConfigDocumentFees;
import com.cptu.egp.eps.service.serviceinterface.ConfigDocumentFeesService;
import com.cptu.egp.eps.service.serviceimpl.ConfigDocumentFeesServiceImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.cptu.egp.eps.web.utility.AppExceptionHandler;
import com.cptu.egp.eps.web.utility.GeneratePdfConstant;
import com.cptu.eps.service.audit.AuditTrail;
import org.apache.log4j.Logger;

/**
 *
 * @author Rokibul
 */
public class ConfigDocumentFeesSrBean {
    private String logUserId = "0";
    private static final Logger LOGGER = Logger.getLogger(ConfigDocumentFeesSrBean.class);
    private ConfigDocumentFeesService configDocumentFeesService = (ConfigDocumentFeesService) AppContext.getSpringBean("ConfigDocumentFeesService");
    private AuditTrail auditTrail;
        public void setLogUserId(String logUserId) {
        configDocumentFeesService.setLogUserId(logUserId);        
        this.logUserId = logUserId;
        }
         public float getMaxDocumentPriceInBDT(String procurementType)
            {
             LOGGER.debug("getMaxDocumentPriceInBDT : " + logUserId + " starts");
                float MaxDocumentPriceInBDT = 0.0f;
                try{
                        MaxDocumentPriceInBDT = configDocumentFeesService.getMaxDocumentPriceInBDT(procurementType);
                    }
                catch(Exception e) 
                    {
                      LOGGER.error("getMaxDocumentPriceInBDT : " + e);
                           // e.printStackTrace();
                    }
                 LOGGER.debug("getMaxDocumentPriceInBDT : " + logUserId + " ends");
                return MaxDocumentPriceInBDT;
            }

         public float getMaxDocumentPriceInUSD(String procurementType)
        {
             LOGGER.debug("getMaxDocumentPriceInUSD : " + logUserId + " starts");
             float MaxDocumentPriceInUSD = 0.0f;
             try {
                  MaxDocumentPriceInUSD =  configDocumentFeesService.getMaxDocumentPriceInUSD(procurementType);
                 }
             catch(Exception e)
                {
                    LOGGER.error("getMaxDocumentPriceInBDT : " + e);
                }
                    LOGGER.debug("getMaxDocumentPriceInBDT : " + logUserId + " ends");
             return MaxDocumentPriceInUSD;
         }
}
