/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblEvalReportMaster;
import com.cptu.egp.eps.service.serviceinterface.ClarificationRptService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class ClariRptSrBean {

    ClarificationRptService clarificationRptService = (ClarificationRptService) AppContext.getSpringBean("ClarificationRptService");
    static final Logger logger = Logger.getLogger(ClariRptSrBean.class);
    private String loggerStart = " Starts";
    private String loggerEnd = " Ends";
    private String logUserId = "0";
    private String evalRptName;
    private int evalRptId;

    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        clarificationRptService.setUserId(logUserId);
        this.logUserId = logUserId;
    }

    public int getEvalRptId() {
        return evalRptId;
    }

    public void setEvalRptId(int evalRptId) {
        this.evalRptId = evalRptId;
    }

    public String getEvalRptName() {
        return evalRptName;
    }

    public void setEvalRptName(String evalRptName) {
        this.evalRptName = evalRptName;
    }

    public String forUpdate(int tenderId) {
        logger.debug("forUpdate : " + logUserId + loggerStart);
        String str = "";
        try {
            str = clarificationRptService.forUpdate(tenderId);
        } catch (Exception e) {
            logger.error("forUpdate : " + logUserId,e);
    }
        logger.debug("forUpdate : " + logUserId + loggerEnd);
        return str;
    }

    public void populateInfo(int tenderId) {
        logger.debug("populateInfo : " + logUserId + loggerStart);
        try {
        List<TblEvalReportMaster> list = null;
        list = clarificationRptService.getRptName(tenderId);
        setEvalRptId(list.get(0).getEvalRptId());
        setEvalRptName(list.get(0).getEvalRptName());
        } catch (Exception e) {
            logger.error("populateInfo : " + logUserId,e);
    }
        logger.debug("populateInfo : " + logUserId + loggerEnd);
    }
}
