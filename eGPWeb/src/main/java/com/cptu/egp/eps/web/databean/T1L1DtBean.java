/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

/**
 *
 * @author nishit
 */
public class T1L1DtBean {
    private int bidderId;
    private String companyName;
    private int rank;
    private double amount;
    private double t1Score;
    private double l1Score;
    private double l1price;

    public double getL1price() {
        return l1price;
    }

    public void setL1price(double l1price) {
        this.l1price = l1price;
    }

    public double getL1Score() {
        return l1Score;
    }

    public void setL1Score(double l1Score) {
        this.l1Score = l1Score;
    }

    public double getT1Score() {
        return t1Score;
    }

    public void setT1Score(double t1Score) {
        this.t1Score = t1Score;
    }

    public T1L1DtBean(String companyName, int rank, double amount) {
        this.companyName = companyName;
        this.rank = rank;
        this.amount = amount;
    }

    public T1L1DtBean(int bidderId, String companyName, int rank, double amount, double t1Score, double l1Score, double l1price) {
        this.bidderId = bidderId;
        this.companyName = companyName;
        this.rank = rank;
        this.amount = amount;
        this.t1Score = t1Score;
        this.l1Score = l1Score;
        this.l1price = l1price;
    }
       
    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getBidderId() {
        return bidderId;
    }

    public void setBidderId(int bidderId) {
        this.bidderId = bidderId;
    }    

    public T1L1DtBean() {
    }       
}
