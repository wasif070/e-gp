/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblTenderListBox;
import com.cptu.egp.eps.model.table.TblTenderListDetail;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService;
import com.cptu.egp.eps.service.serviceinterface.TendererComboService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author dixit
 */
public class CreateTenderComboSrBean extends HttpServlet {
   
    /**this servlet handles all the request of createTendercombo.jsp and viewTendercombo.jsp pages
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private TendererComboService tenderercomboservice = (TendererComboService) AppContext.getSpringBean("TendererComboService");
    private static final Logger LOGGER = Logger.getLogger(CreateTenderComboSrBean.class);
    private String logUserId ="0";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        boolean flag = true;
        HttpSession session = request.getSession();
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
            tenderercomboservice.setLogUserId(logUserId);
        }
        LOGGER.debug("processRequest : "+logUserId+" Starts");
        try {
                TblTenderListBox tbltenderlistbox = new TblTenderListBox();
                /* getting combo data from jsp page by request parameter */
                if("addcomboData".equalsIgnoreCase(request.getParameter("action")))
                {
                        LOGGER.debug("processRequest action : add combo value to database : "+logUserId+"Starts");
                        String templetformid = "";
                        if(!"".equals(request.getParameter("formId")))
                        {
                            templetformid = request.getParameter("formId");
                        }
                        String tenderid = "";
                        if(!"".equals(request.getParameter("tenderId")))
                        {
                            tenderid = request.getParameter("tenderId");
                        }
                        String Message = "";
                        if(!"".equals(request.getParameter("message")))
                        {
                            Message = request.getParameter("message");
                        }
                        int pkgOrLotId = -1;
                        if(!"".equals(request.getParameter("porlId")))
                        {
                            pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
                        }
                        int templetformId = Integer.parseInt(templetformid);
                        String comboname = request.getParameter("ListName");
                        String lblname[] =request.getParameterValues("lblField");
                        String lblvalue[] = request.getParameterValues("valueField");
                        String selected = request.getParameter("chkField");
                        String isCalreq = request.getParameter("rad");
                        /* set combo data in to pojos */
                        if("tenderer".equalsIgnoreCase(Message))
                        {
                                tbltenderlistbox.setListBoxName(comboname);
                                tbltenderlistbox.setTenderFormId(templetformId);
                                tbltenderlistbox.setListBoxId(0);
                                tbltenderlistbox.setIsCalcReq(isCalreq);
                                List<TblTenderListDetail> tenderlistboxdetail = new ArrayList<TblTenderListDetail>();
                                for(int i=0; i<(lblname.length); i++)
                                {
                                    TblTenderListDetail tbltenderlistdetail = new TblTenderListDetail();
                                    tbltenderlistdetail.setItemId((short) (i+1));                                    
                                    if("No".equalsIgnoreCase(isCalreq))
                                    {
                                        tbltenderlistdetail.setItemValue(Integer.toString(i+1));
                                    }
                                    else
                                    { 
                                        tbltenderlistdetail.setItemValue(lblvalue[i]);
                                    }
                                    tbltenderlistdetail.setItemText(lblname[i]);
                                    if(Integer.parseInt(selected)==i)
                                    {
                                        tbltenderlistdetail.setIsDefault("yes");
                                    }
                                    else
                                    {
                                        tbltenderlistdetail.setIsDefault("no");
                                    }
                                    tenderlistboxdetail.add(tbltenderlistdetail);
                                    tbltenderlistdetail=null;
                                    LOGGER.debug("name is::"+lblname[i]+ "and value is::"+lblvalue[i]);
                                   
                                }
                                /* adding combo data into database by calling service class method */
                                flag = tenderercomboservice.addComboNameDetailsFromTenderer(tbltenderlistbox,tenderlistboxdetail);
                                LOGGER.debug("processRequest action : add combo value to database : "+logUserId+"Ends");
                                if(!flag)
                                {
                                    response.sendRedirect("officer/CreateTenderCombo.jsp?formId="+templetformid+"&tenderId="+tenderid+"&porlId="+pkgOrLotId+"&msg=fail");
                                }else{
                                response.sendRedirect("officer/ViewTenderCombo.jsp?templetformid="+templetformid+"&tenderId="+tenderid+"&porlId="+pkgOrLotId+"&msg=succ");
                         }
                }
                }
                else if("fetchData".equalsIgnoreCase(request.getParameter("action")))
                {
                    LOGGER.debug("processRequest action : fetching combo value from database : "+logUserId+"Starts");
            /* getting combo data from database by calling service class method and displayed into jqgrid*/
                    response.setContentType("text/xml;charset=UTF-8");

                    String TenderID = request.getParameter("tenderId");
                    List<Object> tenderstatus = tenderercomboservice.getTenderStatus(Integer.parseInt(TenderID));
                    
                    String templetformid = request.getParameter("templetformID");
                    int TempletFormID = Integer.parseInt(templetformid);

                    String ViewMessage = request.getParameter("viewmessage");

                    String rows = request.getParameter("rows");
                    String page = request.getParameter("page");

                    String sord = null;
                    String sidx = null;

                    if(request.getParameter("sidx")!=null && !"".equalsIgnoreCase(request.getParameter("sidx"))){
                        sord = request.getParameter("sord");
                        sidx = request.getParameter("sidx");
                    }

                    boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                    String searchField = ""; String searchString = "";String searchOper = "";

                    int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));

                    int totalPages = 0;
                    int totalCount = 0;

                    
                    List<TblTenderListBox> getcombonameFromTenderer = null;

                    if(_search)
                    {
                        searchField = request.getParameter("searchField");
                        searchString = request.getParameter("searchString");
                        searchOper = request.getParameter("searchOper");
                        if("tenderer".equalsIgnoreCase(ViewMessage))
                        {
                            getcombonameFromTenderer = tenderercomboservice.searchComboNameFromTenderer(offset, Integer.parseInt(rows), TempletFormID, searchField, searchString, searchOper, sord,sidx);
                            totalCount = (int) tenderercomboservice.getSearchCountFromTenderer(TempletFormID,searchField, searchString, searchOper);
                        }
                    }else{
                        if("tenderer".equalsIgnoreCase(ViewMessage))
                        {
                            getcombonameFromTenderer = tenderercomboservice.getComboNameFromTenderer(offset,Integer.parseInt(rows),TempletFormID,sord,sidx);
                            totalCount = (int) tenderercomboservice.getComboCountFromTenderer(TempletFormID);
                        }
                    }


                    if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(rows) == 0) {
                        totalPages = totalCount / Integer.parseInt(rows);
                    } else {
                        totalPages = (totalCount / Integer.parseInt(rows)) + 1;
                    }
                    } else {
                        totalPages = 0;
                    }

                    out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                    out.print("<rows>");
                    out.print("<page>" + request.getParameter("page") + "</page>");
                    out.print("<total>" + totalPages + "</total>");
                    out.print("<records>" + totalCount + "</records>");
                    int srNo = offset +1;
                    int t=0;
                    String strDelete="";
                    if("tenderer".equalsIgnoreCase(ViewMessage))
                    {
                        for (int i=0;i<getcombonameFromTenderer.size();i++)
                        {
                            int tenderlistid = getcombonameFromTenderer.get(i).getTenderListId();
                            ComboSrBean cmbosrbean = new ComboSrBean();
                            List<TblTenderListDetail> getdetail = cmbosrbean.getListBoxDetailFromTenderer(tenderlistid);

                            /* Dohatec: ICT-Goods Start */
                            //Get Currency List to be shown at 'Currency' Combo box
                            List<Object[]> currencyObjectList = null;
                            TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");
                            if(tenderCurrencyService.isTenderListBoxForCurrency(tenderlistid)){
                                currencyObjectList = tenderCurrencyService.getCurrencyTenderwise(Integer.parseInt(TenderID));
                            }

                            //Procurement Type (i.e. NCT/ICT) retrieval
                            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                            List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", TenderID, null);
                            boolean isIctTender = false;
                            if((!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")  || tenderCommonService.getSBDCurrency(Integer.parseInt(TenderID)).equalsIgnoreCase("Yes"))){
                                isIctTender = true;
                            }

                            //Procurement Nature (i.e. Goods/Works/Service) retrival
                            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                            String procurementNature = commonService.getProcNature(TenderID + "").toString();
                            /* Dohatec: ICT-Goods End */

                            StringBuilder listname= new StringBuilder();
                            String isdefault="";
                            String str="";
                            strDelete="<a href='#' onclick=\"deleteFile(" + getcombonameFromTenderer.get(i).getTenderListId() + ")\" >Delete</a>";
                            //String str1="";
                            //str1 = "<a href=CreateCombo.jsp?msg=edit&listboxid="+getcomboname.get(i).getListBoxId()+"><u>Edit</u></a>";
                            listname.append("<center><select style='width:100px;' class='formTxtBox_1' >");
                            /* Dohatec: ICT-Goods Start */
                            if(isIctTender && procurementNature.equalsIgnoreCase("Goods") && currencyObjectList != null){
                                for(Object[] obj :currencyObjectList){
                                    str = obj[3].toString();
                                    listname.append("<option");
                                    if("BTN".equalsIgnoreCase(str)){listname.append(" "+"selected");}
                                    listname.append(">"+obj[3]+"</option>");
                                }
                            }   /* Dohatec: ICT-Goods End */
                            else {
                                for(int j=0; j<getdetail.size(); j++)
                                {
                                    str=getdetail.get(j).getItemText();
                                    isdefault= getdetail.get(j).getIsDefault();
                                    listname.append("<option");
                                    if("yes".equals(isdefault)){listname.append(" "+"selected");}
                                    listname.append(">"+str+"</option>");
                                }
                            }
                            listname.append("</select></center>");
                            out.print("<row id='" + i + "'>");
                            out.print("<cell>" + srNo + "</cell>");
                            out.print("<cell><![CDATA["+getcombonameFromTenderer.get(i).getListBoxName()+"]]></cell>");
                            out.print("<cell><![CDATA["+listname+"]]></cell>");
                            if(tenderstatus.get(0)!=null)
                            {
                               if("pending".equalsIgnoreCase(tenderstatus.get(0).toString()) && (0 == getcombonameFromTenderer.get(i).getListBoxId()))
                               {
                                   out.print("<cell><![CDATA["+strDelete+"]]></cell>");
                               }
                               else
                               {
                                   out.print("<cell><![CDATA["+" - "+"]]></cell>");
                               }
                            }
                            out.print("</row>");
                            srNo++;t++;
                        }
                        //No Record Found Code
                        if(getcombonameFromTenderer.isEmpty()){
                        out.print("<row id='" + t + "'>");
                        out.print("<cell><![CDATA[No data found]]></cell>");
                        out.print("<cell><![CDATA[No data found]]></cell>");
                        out.print("<cell><![CDATA[No data found]]></cell>");
                        out.print("<cell><![CDATA[No data found]]></cell>");
                        out.print("</row>");
                    }
                    }
                    out.print("</rows>");
                    LOGGER.debug("processRequest action : fetching combo value from database : "+logUserId+"Starts");
                }
                else if("delete".equalsIgnoreCase(request.getParameter("action")))
                {
                    String tenderListId = request.getParameter("tenderlistid");
                    String str = deleteCombo(tenderListId);
                    out.print(str);
                    //tenderercomboservice.deleteCombo(Integer.parseInt(tenderListId));
                }
                LOGGER.debug("processRequest : "+logUserId+" Ends");
        } finally {
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String deleteCombo(String tenderListId) {
        LOGGER.debug("deleteCombo : Starts");
        String str="0";
        try {
            boolean flag = tenderercomboservice.deleteCombo(Integer.parseInt(tenderListId));
            if(flag)
            {
                str="1";
            }
        } catch (Exception e) {
            LOGGER.debug("deleteCombo :"+e);
        }
        LOGGER.debug("deleteCombo : Ends");
    return str;
    }

}
