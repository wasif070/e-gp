/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

import java.util.Date;

/**
 *
 * @author Administrator
 */
public class TempCompanyDocumentsDtBean {

    /**
     * Creates a new instance of TempCompanyDocuments
     */
    public TempCompanyDocumentsDtBean() {
    }
    private String documentName;
    private String documentSize;
    private String documentBrief;
    private Date uploadedDate;
    private String documentType;
    private String docHash;
    private String docRefNo;
    private String docDescription;
    private int IsReapplied;
    private String documentTypeId;

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentBrief() {
        return documentBrief;
    }

    public void setDocumentBrief(String documentBrief) {
        this.documentBrief = documentBrief;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDocumentSize() {
        return documentSize;
    }

    public void setDocumentSize(String documentSize) {
        this.documentSize = documentSize;
    }

    public Date getUploadedDate() {
        return uploadedDate;
    }

    public void setUploadedDate(Date uploadedDate) {
        this.uploadedDate = uploadedDate;
    }

    public String getDocHash() {
        return docHash;
    }

    public void setDocHash(String docHash) {
        this.docHash = docHash;
    }

    public String getDocRefNo() {
        return docRefNo;
    }

    public void setDocRefNo(String docRefNo) {
        this.docRefNo = docRefNo;
    }

    public String getDocDescription() {
        return docDescription;
    }

    public void setDocDescription(String docDescription) {
        this.docDescription = docDescription;
    }

    public int getIsReapplied() {
        return IsReapplied;
    }

    public void setIsReapplied(int IsReapplied) {
        this.IsReapplied = IsReapplied;
    }

    public String getDocumentTypeId() {
        return documentTypeId;
    }

    public void setDocumentTypeId(String documentTypeId) {
        this.documentTypeId = documentTypeId;
    }

}
