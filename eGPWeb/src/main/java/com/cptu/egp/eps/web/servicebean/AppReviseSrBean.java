/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblAppreviseConfiguration;
import com.cptu.egp.eps.service.serviceinterface.AppReviseConfigurationService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author aprojit
 */
public class AppReviseSrBean {
    
    private final AppReviseConfigurationService appReviseConService=(AppReviseConfigurationService)AppContext.getSpringBean("AppReviseConfigurationService");
    
    private static final Logger LOGGER = Logger.getLogger(AppReviseSrBean.class);

    private String logUserId = "0";

    private AuditTrail auditTrail;
    
    public AppReviseSrBean(){
     appReviseConService.setLogUserId(logUserId);
     appReviseConService.setAuditTrail(auditTrail); 
    }
    
   public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
   private Date currentDate(){
	   Date date = new Date();
           return date;     
   }
   public String addAppReviseconfiguration (String financialYear, Date startDate, Date endDate, int maxReviseCount, int userID){
     LOGGER.debug("addAppReviseconfiguration "+logUserId+" Starts");
        String msg = "";
        TblAppreviseConfiguration appReviseconfig = new TblAppreviseConfiguration();
        try{
            appReviseconfig.setFinancialYear(financialYear);
            appReviseconfig.setStartDate(startDate);
            appReviseconfig.setEndDate(endDate);
            appReviseconfig.setMaxReviseCount(maxReviseCount);
            appReviseconfig.setUserId(userID);
            appReviseconfig.setLastUpdatedDate(currentDate());
            appReviseConService.setLogUserId(logUserId);
            appReviseConService.setAuditTrail(auditTrail); 
            
            msg = appReviseConService.addTblAppreviseConfiguration(appReviseconfig, userID);
        }catch(Exception e){
            LOGGER.error("addAppReviseconfiguration "+logUserId+" :"+e);
        }
        LOGGER.debug("addAppReviseconfiguration "+logUserId+" Ends");
        return msg;
    }
   
   public String updateAppReviseconfiguration (String financialYear, Date startDate, Date endDate, int maxReviseCount, int userID){
     LOGGER.debug("updateAppReviseconfiguration "+logUserId+" Starts");
        String msg = "";
        TblAppreviseConfiguration appReviseconfig = new TblAppreviseConfiguration();
        try{
            appReviseconfig.setFinancialYear(financialYear);
            appReviseconfig.setStartDate(startDate);
            appReviseconfig.setEndDate(endDate);
            appReviseconfig.setMaxReviseCount(maxReviseCount);
            appReviseconfig.setUserId(userID);
            appReviseconfig.setLastUpdatedDate(currentDate());
            appReviseConService.setLogUserId(logUserId);
            appReviseConService.setAuditTrail(auditTrail); 
            
            msg = appReviseConService.updateTblAppreviseConfiguration(appReviseconfig,userID);
        }catch(Exception e){
            LOGGER.error("updateAppReviseconfiguration "+logUserId+" :"+e);
        }
        LOGGER.debug("updateAppReviseconfiguration "+logUserId+" Ends");
        return msg;
    }
   
   public boolean isexistConfiguration(String financialYear){
   LOGGER.debug("addAppReviseconfiguration "+logUserId+" Starts");
   LOGGER.debug("addAppReviseconfiguration "+logUserId+" Ends");
   return appReviseConService.isexistConfiguration(financialYear);
   }
   
   public List<TblAppreviseConfiguration> getAppConfigurationDetails(String appConfgId){
   LOGGER.debug("addAppReviseconfiguration "+logUserId+" Starts");
   LOGGER.debug("addAppReviseconfiguration "+logUserId+" Ends");
   return appReviseConService.getAppConfigurationDetails(appConfgId);
   }
   
   public List<TblAppreviseConfiguration> getAllConfiguration(){
   LOGGER.debug("addAppReviseconfiguration "+logUserId+" Starts");
   LOGGER.debug("addAppReviseconfiguration "+logUserId+" Ends");
   return appReviseConService.getAllConfiguration();
   }
  
   public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
}
