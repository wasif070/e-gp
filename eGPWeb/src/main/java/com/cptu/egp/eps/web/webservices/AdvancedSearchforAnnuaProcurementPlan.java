/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.webservices;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonAppData;
import com.cptu.egp.eps.dao.storedprocedure.CommonAppSearchData;
import com.cptu.egp.eps.model.table.TblBudgetType;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import com.cptu.egp.eps.model.table.TblProcurementNature;
import com.cptu.egp.eps.model.table.TblProcurementTypes;
import com.cptu.egp.eps.service.serviceimpl.DepartmentCreationServiceImpl;
import com.cptu.egp.eps.service.serviceinterface.APPService;
import com.cptu.egp.eps.service.serviceinterface.AppAdvSearchService;
import com.cptu.egp.eps.service.serviceinterface.ConfigPreTenderRuleService;
import com.cptu.egp.eps.service.serviceinterface.GovtUserCreationService;
import com.cptu.egp.eps.service.serviceinterface.PEOfficeCreationService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.xmlpojos.AnnualProcurementPlan;
import com.cptu.egp.eps.web.xmlpojos.BudgetTypes;
import com.cptu.egp.eps.web.xmlpojos.Departments;
import com.cptu.egp.eps.web.xmlpojos.FinancialYears;
import com.cptu.egp.eps.web.xmlpojos.OfficeMaster;
import com.cptu.egp.eps.web.xmlpojos.ProcurementNature;
import com.cptu.egp.eps.web.xmlpojos.ProcurementTypes;
import com.cptu.egp.eps.web.xmlpojos.Projects;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.apache.log4j.Logger;
import com.cptu.egp.eps.web.utility.WebServiceUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import com.cptu.egp.eps.web.xmlpojos.WSResponseObject;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This web-service for Advance search for annual procurement plans.
 * @author  Chalapathi.Bavisetti
 */
@WebService()
@XmlSeeAlso({FinancialYears.class, ProcurementNature.class, ProcurementNature.class,
    ProcurementTypes.class, Projects.class, BudgetTypes.class, OfficeMaster.class,
    AnnualProcurementPlan.class, Departments.class})
public class AdvancedSearchforAnnuaProcurementPlan {

    private static final Logger LOGGER =
            Logger.getLogger(AdvancedSearchforAnnuaProcurementPlan.class);
    private String logUserId = "0";
    private static final String EMPTY_STRING = "";
    private static final int ZERO_VALUE = 0;
    private static final int WS_ID_FINANCIAL_YEARS = 6;
    private static final String WS_FINANCIAL_YEARS_SEARCH_SERVICE_NAME = "APPService";
    private static final int WS_ID_PROCUREMENT_NATURES = 7;
    private static final String WS_PROCUREMENT_NATURES_SEARCH_SERVICE_NAME = "configPreTenderRuleService";
    private static final int WS_ID_PROCUREMENT_TYPES = 8;
    private static final String WS_PROCUREMENT_TYPES_SEARCH_SERVICE_NAME = "configPreTenderRuleService";
    private static final int WS_ID_BUDGET_TYPES = 9;
    private static final String WS_BUDGET_TYPES_SEARCH_SERVICE_NAME = "GovtUserCreationService";
    private static final int WS_ID_OFFICES_BY_DEPARTMENT_ID = 10;
    private static final String WS_OFFICES_BY_DEPARTMENT_ID_SEARCH_SERVICE_NAME = "PEOfficeCreationService";
    private static final int WS_ID_OFFICES_GET_DEPARTMENTS = 11;
    private static final String WS_OFFICES_GET_DEPARTMENTS_SEARCH_SERVICE_NAME = "DepartmentCreationService";
    private static final int WS_ID_ANNUAL_PROCUREMENT_PLANS_ADVANCE_SEARCH = 12;
    private static final String WS_ANNUAL_PROCUREMENT_PLANS_ADVANCE_SEARCH_SEARCH_SERVICE_NAME = "AppAdvSearchService";
    private static final int WS_ID_PROJECT_LIST = 13;
    private static final String WS_PROJECT_LIST_SEARCH_SERVICE_NAME = "APPService";

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /***
     * This method gives the List of String Objects of <b>Financial years</b> and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br>
     * 1.Financial year Id <br>
     * 2.Financial year <br>
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getFinancialYears")
    public WSResponseObject getFinancialYears(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getFinancialYears : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_FINANCIAL_YEARS, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_FINANCIAL_YEARS,
                WS_FINANCIAL_YEARS_SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> financialYears = new ArrayList<String>();
            try {
                List<FinancialYears> financialYearsTempList = getRequiredFinancialYearsList();
                if (financialYearsTempList != null) {
                    for (FinancialYears financialYear : financialYearsTempList) {
                        StringBuilder sbCSV = new StringBuilder();
                        sbCSV.append(financialYear.getFinancialYearId());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(financialYear.getFinancialYear());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(financialYear.getFinancialYearCurrent());  // Added by Sudhir 09072011 To identify current Financial Year.
                        financialYears.add(sbCSV.toString());
                    }
                }
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.debug("getFinancialYears : " + logUserId + e);
            }
            if (financialYears != null) {
                if (financialYears.isEmpty()) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(financialYears);
                }
            }
        }
        LOGGER.debug("getFinancialYears : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    /***
     * This method return the required financial years as a list of objects
     * @return List<FinancialYears>
     */
    private List<FinancialYears> getRequiredFinancialYearsList() throws Exception {
        LOGGER.debug("getRequiredFinancialYearsList : " + logUserId + WSResponseObject.LOGGER_START);
        List<FinancialYears> financialYearsList = new ArrayList<FinancialYears>();
        APPService appService = (APPService) AppContext.getSpringBean(WS_FINANCIAL_YEARS_SEARCH_SERVICE_NAME);
        List<CommonAppData> appDatas =
                appService.getAPPDetailsBySP("FinancialYear", EMPTY_STRING, EMPTY_STRING);
        for (CommonAppData appData : appDatas) {
            FinancialYears financialYear = new FinancialYears();
            financialYear.setFinancialYearId(Integer.parseInt(appData.getFieldName1()));
            financialYear.setFinancialYear(appData.getFieldName2());
            financialYear.setFinancialYearCurrent(appData.getFieldName3());    // Added by Sudhir 09072011 To identify current Financial Year.
            financialYearsList.add(financialYear);
        }
        LOGGER.debug("getRequiredFinancialYearsList : " + logUserId + WSResponseObject.LOGGER_END);
        return financialYearsList;
    }

    /***
     * This method gives the List of String Objects of <b>ProcurementNatures<b> and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br>
     * 1.ProcurementNatureId <br>
     * 2.ProcurementNature <br>
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getProcurementNatures")
    public WSResponseObject getProcurementNatures(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getProcurementNatures : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_PROCUREMENT_NATURES, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_PROCUREMENT_NATURES,
                WS_PROCUREMENT_NATURES_SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> procNaturesList = new ArrayList<String>();
            try {
                List<ProcurementNature> procNaturesTempList = getRequiredProcurementNatureList();
                if (procNaturesTempList != null) {
                    for (ProcurementNature procurementNature : procNaturesTempList) {
                        StringBuilder sbCSV = new StringBuilder();
                        sbCSV.append(procurementNature.getProcurementNatureId());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(procurementNature.getProcurementNature());
                        //sbCSV.append(WSResponseObject.APPEND_CHAR);  Commended by sudhir 09072011
                        procNaturesList.add(sbCSV.toString());
                    }
                }
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.debug("getProcurementNatures : " + logUserId + e);
            }
            if (procNaturesList != null) {
                if (procNaturesList.isEmpty()) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(procNaturesList);
                }
            }
        }
        LOGGER.debug("getProcurementNatures : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    /***
     * This method return the procurement natures as a list of objects
     * @return List<ProcurementNature>
     */
    private List<ProcurementNature> getRequiredProcurementNatureList() {
        LOGGER.debug("getRequiredProcurementNatureList : " + logUserId + WSResponseObject.LOGGER_START);
        List<ProcurementNature> procNaturesList = new ArrayList<ProcurementNature>();
        ConfigPreTenderRuleService preTenderRulesevice = (ConfigPreTenderRuleService) AppContext.getSpringBean(WS_PROCUREMENT_NATURES_SEARCH_SERVICE_NAME);
        List<TblProcurementNature> tblProcurementNatures = preTenderRulesevice.getProcurementNature();
        for (TblProcurementNature procNature : tblProcurementNatures) {
            ProcurementNature procurementNature = new ProcurementNature();
            procurementNature.setProcurementNature(procNature.getProcurementNature());
            procurementNature.setProcurementNatureId(procNature.getProcurementNatureId());
            procNaturesList.add(procurementNature);
        }
        LOGGER.debug("getRequiredProcurementNatureList : " + logUserId + WSResponseObject.LOGGER_END);
        return procNaturesList;
    }

    /***
     * This method gives the List of String Objects of <b>Procurement Types</b> and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br>
     * 1.ProcurementTypeId <br>
     * 2.ProcurementType <br>
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getProcurementTypes")
    public WSResponseObject getProcurementTypes(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getProcurementTypes : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_PROCUREMENT_TYPES, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_PROCUREMENT_TYPES,
                WS_PROCUREMENT_TYPES_SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> procTypesList = new ArrayList<String>();
            try {
                List<ProcurementTypes> procTypesTempList = new ArrayList<ProcurementTypes>();
                procTypesTempList = getRequiredProcurementTypesList();
                if (procTypesTempList != null) {
                    for (ProcurementTypes procurementType : procTypesTempList) {
                        StringBuilder sbCSV = new StringBuilder();
                        sbCSV.append(procurementType.getProcurementTypeId());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(procurementType.getProcurementType());
                        //sbCSV.append(WSResponseObject.APPEND_CHAR);   Commended by Sudhir 09072011
                        procTypesList.add(sbCSV.toString());
                    }
                }
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.debug("getProcurementTypes : " + logUserId + e);
            }
            if (procTypesList != null) {
                if (procTypesList.isEmpty()) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(procTypesList);
                }
            }
        }
        LOGGER.debug("getProcurementTypes : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    /***
     * This method return the procurement types as a list of objects
     * @return List<ProcurementTypes>
     */
    private List<ProcurementTypes> getRequiredProcurementTypesList() {
        LOGGER.debug("getRequiredProcurementTypesList : " + logUserId + WSResponseObject.LOGGER_START);
        List<ProcurementTypes> procTypesList = new ArrayList<ProcurementTypes>();
        ConfigPreTenderRuleService preTenderRulesevice = (ConfigPreTenderRuleService) AppContext.getSpringBean(WS_PROCUREMENT_TYPES_SEARCH_SERVICE_NAME);
        List<TblProcurementTypes> tblProcurementTypes =
                preTenderRulesevice.getProcurementTypes();
        for (TblProcurementTypes procType : tblProcurementTypes) {
            ProcurementTypes procurementType = new ProcurementTypes();
            procurementType.setProcurementType(procType.getProcurementType());
            procurementType.setProcurementTypeId(procType.getProcurementTypeId());
            procTypesList.add(procurementType);
        }
        LOGGER.debug("getRequiredProcurementTypesList : " + logUserId + WSResponseObject.LOGGER_END);
        return procTypesList;
    }

    /***
     * This method gives the List of String Objects of <b>Projects</b> and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br>
     * 1.Project Id <br>
     * 2.Project Name <br>
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getProjectList")
    public WSResponseObject getProjectList(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getProjectList : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_PROJECT_LIST, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_PROJECT_LIST,
                WS_PROJECT_LIST_SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> projectList = new ArrayList<String>();
            try {
                List<Projects> projectTempList = getRequiredProjectList();
                if (projectTempList != null) {
                    for (Projects project : projectTempList) {
                        StringBuilder sbCSV = new StringBuilder();
                        sbCSV.append(project.getProjectId());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(project.getProjectName());
                        //sbCSV.append(WSResponseObject.APPEND_CHAR);  Commended by sudhir 09072011
                        projectList.add(sbCSV.toString());
                    }
                }
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.debug("getProjectList : " + logUserId + e);
            }
            if (projectList != null) {
                if (projectList.isEmpty()) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(projectList);
                }
            }
        }
        LOGGER.debug("getProjectList : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    /***
     * This method return the projects as a list of objects.
     * @return List<Projects>
     */
    private List<Projects> getRequiredProjectList() throws Exception {
        LOGGER.debug("getRequiredProjectList : " + logUserId + WSResponseObject.LOGGER_START);
        List<Projects> projectList = new ArrayList<Projects>();
        APPService appService = (APPService) AppContext.getSpringBean(WS_PROJECT_LIST_SEARCH_SERVICE_NAME);
        List<CommonAppData> appDatas = appService.getAPPDetailsBySP("Project", EMPTY_STRING, EMPTY_STRING);
        for (CommonAppData appData : appDatas) {
            Projects project = new Projects();
            project.setProjectId(Integer.parseInt(appData.getFieldName1()));
            project.setProjectName(appData.getFieldName2());
            projectList.add(project);
        }
        LOGGER.debug("getRequiredProjectList : " + logUserId + WSResponseObject.LOGGER_END);
        return projectList;
    }

    /***    
     * This method gives the List of String Objects of <b>Budget types </b>and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br>
     * 1.BudgetTypeId<br>
     * 2.BudgetType<br>
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getBudgetTypes")
    public WSResponseObject getBudgetTypes(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getBudgetTypes : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_BUDGET_TYPES, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_BUDGET_TYPES,
                WS_BUDGET_TYPES_SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> budgetTypesList = new ArrayList<String>();
            try {
                List<BudgetTypes> budgetTypesTempList = getRequiredBudgetTypesList();
                if (budgetTypesTempList != null) {
                    for (BudgetTypes budgetType : budgetTypesTempList) {
                        StringBuilder sbCSV = new StringBuilder();
                        sbCSV.append(budgetType.getBudgetTypeId());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(budgetType.getBudgetType());
                        //sbCSV.append(WSResponseObject.APPEND_CHAR); Commended by Sudhir 09072011
                        budgetTypesList.add(sbCSV.toString());
                    }
                }
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.debug("getBudgetTypes : " + logUserId + e);
            }
            if (budgetTypesList != null) {
                if (budgetTypesList.isEmpty()) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(budgetTypesList);
                }
            }
        }
        LOGGER.debug("getBudgetTypes : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    /***
     * This method return the Budget types as a list of objects
     * @return List<BudgetTypes>
     */
    private List<BudgetTypes> getRequiredBudgetTypesList() {
        LOGGER.debug("getRequiredBudgetTypesList : " + logUserId + WSResponseObject.LOGGER_START);
        List<BudgetTypes> budgetTypesList = new ArrayList<BudgetTypes>();
        GovtUserCreationService govtUserCreationService =
                (GovtUserCreationService) AppContext.getSpringBean(WS_BUDGET_TYPES_SEARCH_SERVICE_NAME);
        List<TblBudgetType> budgetTypes = govtUserCreationService.getBudgetTypes();
        for (TblBudgetType btype : budgetTypes) {
            BudgetTypes bTypes = new BudgetTypes();
            bTypes.setBudgetType(btype.getBudgetType());
            bTypes.setBudgetTypeId(btype.getBudgetTypeId());
            budgetTypesList.add(bTypes);
        }
        LOGGER.debug("getRequiredBudgetTypesList : " + logUserId + WSResponseObject.LOGGER_END);
        return budgetTypesList;
    }

    /***
     * This method gives the List of String Objects of <b>Office</b> and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br>
     * 1.Office Id <br>
     * 2.Office Name <br>
     * @param String username
     * @param String password
     * @param Integer departmentId
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getOfficesByDepartmentId")
    public WSResponseObject getOfficesByDepartmentId(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password,
            @WebParam(name = "departmentId") Integer departmentId) {
        LOGGER.debug("getOfficesByDepartmentId : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        if (departmentId == null) {
            departmentId = ZERO_VALUE;
        }
        String searchKey = "DepartmentId:" + departmentId;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_OFFICES_BY_DEPARTMENT_ID, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_OFFICES_BY_DEPARTMENT_ID,
                WS_OFFICES_BY_DEPARTMENT_ID_SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> officesList = new ArrayList<String>();
            try {
                List<OfficeMaster> officesTempList = getRequiredOfficesListByDeptId(departmentId);
                if (officesTempList != null) {
                    for (OfficeMaster officeMaster : officesTempList) {
                        StringBuilder sbCSV = new StringBuilder();
                        sbCSV.append(officeMaster.getOfficeId());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(officeMaster.getOfficeName());
                        //sbCSV.append(WSResponseObject.APPEND_CHAR);  Commented by sudhir 09072011
                        officesList.add(sbCSV.toString());
                    }
                }
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.debug("getOfficesByDepartmentIdEx : " + logUserId + e);
            }
            if (officesList != null) {
                if (officesList.isEmpty()) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(officesList);
                }
            }
        }
        LOGGER.debug("getOfficesByDepartmentId : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    /***
     * This method return the offices as a list of objects with the given department Id
     * @param deptId
     * @return List<OfficeMaster>
     */
    private List<OfficeMaster> getRequiredOfficesListByDeptId(int deptId) {
        List<OfficeMaster> officesList = new ArrayList<OfficeMaster>();
        PEOfficeCreationService officeCreationService = (PEOfficeCreationService) AppContext.getSpringBean(WS_OFFICES_BY_DEPARTMENT_ID_SEARCH_SERVICE_NAME);
        List<TblOfficeMaster> officeMasters =
                officeCreationService.findOfficeMaster("departmentId", Operation_enum.EQ, deptId);
        Iterator it = officeMasters.iterator();
        while (it.hasNext()) {
            TblOfficeMaster tblOfficeMaster = (TblOfficeMaster) it.next();
            OfficeMaster office = new OfficeMaster();
            office.setOfficeId(tblOfficeMaster.getOfficeId());
            office.setOfficeName(tblOfficeMaster.getOfficeName());
            officesList.add(office);
        }
        return officesList;
    }

    /***
     * This method gives the List of String Objects of <b>departments</b> and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br>
     * 1.Department Id<br>
     * 2.Department Name <br>
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getDepartments")
    public WSResponseObject getDepartments(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getDepartments : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_OFFICES_GET_DEPARTMENTS, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_OFFICES_GET_DEPARTMENTS,
                WS_OFFICES_GET_DEPARTMENTS_SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> departsList = new ArrayList<String>();
            try {
                List<Departments> departmentsTempList = getRequiredDepartmentsList();
                if (departmentsTempList != null) {
                    for (Departments department : departmentsTempList) {
                        StringBuilder sbCSV = new StringBuilder();
                        sbCSV.append(department.getDepartmentId());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(department.getDepartmentName());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(department.getDepartmentType()); // Added by sudhir 09072011 To identify for which types of department.
                        departsList.add(sbCSV.toString());
                    }
                }
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.debug("getDepartments : " + logUserId + e);
            }
            if (departsList != null) {
                if (departsList.isEmpty()) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(departsList);
                }
            }
        }
        LOGGER.debug("getDepartments : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    /***
     * This method return the departments as a list of objects
     * @return List<Departments>
     */
    private List<Departments> getRequiredDepartmentsList() {
        LOGGER.debug("getRequiredDepartmentsList : " + logUserId + WSResponseObject.LOGGER_START);
        List<Departments> departsList = new ArrayList<Departments>();
        DepartmentCreationServiceImpl deptTree = (DepartmentCreationServiceImpl) AppContext.getSpringBean(WS_OFFICES_GET_DEPARTMENTS_SEARCH_SERVICE_NAME);
        List<TblDepartmentMaster> deptLst = deptTree.getAllDepartmentMaster();
        Iterator it = deptLst.iterator();
        while (it.hasNext()) {
            TblDepartmentMaster tblDepartmentMaster = (TblDepartmentMaster) it.next();
            if (tblDepartmentMaster.getDepartmentType().equals("Organization")
                    || tblDepartmentMaster.getDepartmentType().equals("Division")
                    || tblDepartmentMaster.getDepartmentType().equals("Ministry")) {
                Departments department = new Departments();
                department.setDepartmentId(tblDepartmentMaster.getDepartmentId());
                department.setDepartmentName(tblDepartmentMaster.getDepartmentName());
                department.setDepartmentType(tblDepartmentMaster.getDepartmentType());        // Added by sudhir 09072011 To identify for which types of department.
                departsList.add(department);
            }
        }
        LOGGER.debug("getRequiredDepartmentsList : " + logUserId + WSResponseObject.LOGGER_END);
        return departsList;
    }

    /***
     * This method gives the List of String Objects of <b>AnnualProcurementPlan</b> and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br>
     * 1.APP ID<br>
     * 2.APP Code<br>
     * 3.Ministry, Division, Organization <br>
     * 4.PE Office<br>
     * 5.District<br>
     * 6.Procurement Nature<br>
     * 7.Project Name<br>
     * 8.Package No.<br>
     * 9.Package Description<br>
     * 10.Official Cost Estimate (in BD Tk.)<br>
     * 11.Procurement Method<br>
     *
     * @param String username
     * @param String password
     * @param String keyWord
     * @param Integer ministry_Division_Organization
     * @param Integer procuringEntity
     * @param String projectName
     * @param String financialYear
     * @param String procurementNature
     * @param String budgetType
     * @param String procurementType
     * @param Integer appId
     * @param String appCode
     * @param String packageNo
     * @param String packageEstimatedCostOperation
     * @param BigDecimal value1
     * @param BigDecimal value2
     * @param String category
     * @param Integer pageNumber
     * @param Integer recordsPerPage
     * @return List<String>
     */
    @WebMethod(operationName = "annualProcurementPlansAdvanceSearch")
    public WSResponseObject annualProcurementPlansAdvanceSearch(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password,
            @WebParam(name = "keyWord") String keyWord,
            @WebParam(name = "ministry_Division_Organization") Integer ministry_Division_Organization,
            @WebParam(name = "procuringEntity") Integer procuringEntity,
            @WebParam(name = "projectName") String projectName,
            @WebParam(name = "financialYear") String financialYear,
            @WebParam(name = "procurementNature") String procurementNature,
            @WebParam(name = "budgetType") String budgetType,
            @WebParam(name = "procurementType") String procurementType,
            @WebParam(name = "appId") Integer appId,
            @WebParam(name = "appCode") String appCode,
            @WebParam(name = "packageNo") String packageNo,
            @WebParam(name = "packageEstimatedCostOperation") String packageEstimatedCostOperation,
            @WebParam(name = "value1") BigDecimal value1,
            @WebParam(name = "value2") BigDecimal value2,
            @WebParam(name = "category") String category /*,
            @WebParam(name = "pageNumber") Integer pageNumber,   commended by sudhir 09072011 for no need to pagination concept for WS.
            @WebParam(name = "pageSize") Integer recordsPerPage  */) {

        LOGGER.debug("annualProcurementPlansAdvanceSearch : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        Integer pageNumber = 1;
        Integer recordsPerPage = 100000;
        int packageId = 0;
        String searchKey = getWholeStringSearchKey(keyWord,
                ministry_Division_Organization, procuringEntity, projectName,
                financialYear, procurementNature, budgetType, procurementType,
                appId, appCode, packageNo, packageEstimatedCostOperation, value1,
                value2, category, pageNumber, recordsPerPage);
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_ANNUAL_PROCUREMENT_PLANS_ADVANCE_SEARCH, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_ANNUAL_PROCUREMENT_PLANS_ADVANCE_SEARCH,
                WS_ANNUAL_PROCUREMENT_PLANS_ADVANCE_SEARCH_SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> requiredAPPList = new ArrayList<String>();
            try {
                List<AnnualProcurementPlan> requiredAPPTempList = getRequiredAPPList(
                        keyWord, ministry_Division_Organization, procuringEntity, projectName,
                        financialYear, procurementNature, budgetType, procurementType,
                        appId, appCode, packageNo, packageEstimatedCostOperation, value1,
                        value2, category, pageNumber, recordsPerPage);
                AppAdvSearchService appAdvSearchService =
                        (AppAdvSearchService) AppContext.getSpringBean(WS_ANNUAL_PROCUREMENT_PLANS_ADVANCE_SEARCH_SEARCH_SERVICE_NAME);
                if (requiredAPPTempList != null) {
                    for (AnnualProcurementPlan procurementPlan : requiredAPPTempList) {
                        packageId = appAdvSearchService.getPackageIdByAppId(procurementPlan.getAppId());
                        StringBuilder sbCSV = new StringBuilder();
                        sbCSV.append(procurementPlan.getAppId());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(procurementPlan.getAppCode());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(procurementPlan.getDepartmentName());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(procurementPlan.getOfficeName());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(procurementPlan.getStateName());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(procurementPlan.getProcurementNature());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(procurementPlan.getProjectName());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(procurementPlan.getPackageNo());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        //sbCSV.append(procurementPlan.getPackageDesc());
                        sbCSV.append("<a href=\"" + XMLReader.getMessage("urltoredirect") + "resources/common/ViewPackageDetail.jsp?appId=" + procurementPlan.getAppId() + "&pkgId=" + packageId + "&pkgType=Package&from=home" + ">" + procurementPlan.getPackageDesc() + "</a>");
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(procurementPlan.getEstimatedCost());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(procurementPlan.getProcurementMethod());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(procurementPlan.getOfficeId());
                        //sbCSV.append(WSResponseObject.APPEND_CHAR);  Commended by sudhir 09072011
                        requiredAPPList.add(sbCSV.toString());
                    }
                }
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.debug("annualProcurementPlansAdvanceSearch : " + logUserId + e);
            }
            if (requiredAPPList != null) {
                if (requiredAPPList.isEmpty()) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(requiredAPPList);
                }
            }
        }
        LOGGER.debug("annualProcurementPlansAdvanceSearch : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    /***
     * This method returns all the APP's
     * @param keyWord
     * @param ministry_Division_Organization
     * @param procuringEntity
     * @param projectName
     * @param financialYear
     * @param procurementNature
     * @param budgetType
     * @param procurementType
     * @param appId
     * @param appCode
     * @param packageNo
     * @param packageEstimatedCostOperation
     * @param value1
     * @param value2
     * @param category
     * @param pageNumber
     * @param recordsPerPage
     * @return List<AnnualProcurementPlan>
     */
    private List<AnnualProcurementPlan> getRequiredAPPList(String keyWord,
            Integer ministry_Division_Organization, Integer procuringEntity, String projectName,
            String financialYear, String procurementNature, String budgetType, String procurementType,
            Integer appId, String appCode, String packageNo, String packageEstimatedCostOperation,
            BigDecimal value1, BigDecimal value2, String category, Integer pageNumber, Integer recordsPerPage) {
        LOGGER.debug("getRequiredAPPList : " + logUserId + WSResponseObject.LOGGER_END);
        int employeeId = ZERO_VALUE;
        String statusString = EMPTY_STRING;
        int userIdVal = ZERO_VALUE;
        BigDecimal decimalValue = new BigDecimal(ZERO_VALUE);
        List<AnnualProcurementPlan> annualProcurementPlansList =
                new ArrayList<AnnualProcurementPlan>();
        List<CommonAppSearchData> commonData = null;
        String keyWordString = WebServiceUtil.getDefaultStringValue(keyWord);
        int ministry_Division_OrganizationVal =
                WebServiceUtil.getDefaultIntValue(ministry_Division_Organization);
        int procuringEntityVal = WebServiceUtil.getDefaultIntValue(procuringEntity);//officeId
        String projectNameString = WebServiceUtil.getDefaultStringValue(projectName);
        String financialYearString = WebServiceUtil.getDefaultStringValue(financialYear);
        String procurementNatureString = WebServiceUtil.getDefaultStringValue(procurementNature);
        String budgetTypeString = WebServiceUtil.getDefaultStringValue(budgetType);
        String procurementTypeString = WebServiceUtil.getDefaultStringValue(procurementType);
        int appIdVal = WebServiceUtil.getDefaultIntValue(appId);
        String appCodeString = WebServiceUtil.getDefaultStringValue(appCode);
        String packageNoString = WebServiceUtil.getDefaultStringValue(packageNo);
        String packageEstimatedCostOperationString =
                WebServiceUtil.getDefaultStringValue(packageEstimatedCostOperation);
        BigDecimal value1Val = WebServiceUtil.getDefaultDecimalValue(value1);
        BigDecimal value2Val = WebServiceUtil.getDefaultDecimalValue(value2);
        String categoryString = WebServiceUtil.getDefaultStringValue(category);
        int pageNumberVal = WebServiceUtil.getDefaultIntValue(pageNumber);
        int recordsPerPageVal = WebServiceUtil.getDefaultIntValue(recordsPerPage);
        AppAdvSearchService appAdvSearchService =
                (AppAdvSearchService) AppContext.getSpringBean(WS_ANNUAL_PROCUREMENT_PLANS_ADVANCE_SEARCH_SEARCH_SERVICE_NAME);
        commonData = appAdvSearchService.getSearchResults(keyWordString,
                financialYearString, procurementNatureString, ministry_Division_OrganizationVal,
                employeeId, categoryString, projectNameString, procurementTypeString,
                appIdVal, appCodeString, packageNoString, decimalValue, procuringEntityVal,
                budgetTypeString, packageEstimatedCostOperationString, value1Val,
                value2Val, pageNumberVal, recordsPerPageVal, statusString, userIdVal);
        if (commonData != null && !commonData.isEmpty()) {
            for (int i = 0; i < commonData.size(); i++) {
                CommonAppSearchData commonAppSearchData = commonData.get(i);
                AnnualProcurementPlan annualProcurementPlan =
                        changeObject(commonAppSearchData);
                annualProcurementPlansList.add(annualProcurementPlan);
            }
        }
        LOGGER.debug("getRequiredAPPList : " + logUserId);
        return annualProcurementPlansList;
    }

    /***
     * This method changes the object from CommonAppSearchData to AnnualProcurementPlan
     * @param CommonAppSearchData object
     * @return AnnualProcurementPlan object
     */
    private AnnualProcurementPlan changeObject(CommonAppSearchData commonAppSearchData) {
        LOGGER.debug("changeObject : " + logUserId + WSResponseObject.LOGGER_START);
        AnnualProcurementPlan annualProcurementPlan = new AnnualProcurementPlan();
        annualProcurementPlan.setAppId(commonAppSearchData.getAppId());
        annualProcurementPlan.setAppCode(commonAppSearchData.getAppCode());
        annualProcurementPlan.setOfficeName(commonAppSearchData.getOfficeName());
        annualProcurementPlan.setStateName(commonAppSearchData.getStateName());
        annualProcurementPlan.setDepartmentName(commonAppSearchData.getDepartmentName());
        annualProcurementPlan.setProcurementNature(commonAppSearchData.getProcurementNature());
        annualProcurementPlan.setProjectName(commonAppSearchData.getProjectName());
        annualProcurementPlan.setPackageNo(commonAppSearchData.getPackageNo());
        annualProcurementPlan.setPackageDesc(commonAppSearchData.getPackageDesc());
        annualProcurementPlan.setEstimatedCost(commonAppSearchData.getEstimatedCost());
        annualProcurementPlan.setProcurementMethod(commonAppSearchData.getProcurementMethod());
        annualProcurementPlan.setOfficeId(commonAppSearchData.getOfficeId());
        LOGGER.debug("changeObject : " + logUserId + WSResponseObject.LOGGER_END);
        return annualProcurementPlan;
    }

    /***
     * This method returns search criteria as a string for logging.
     * @param keyWord
     * @param ministry_Division_Organization
     * @param procuringEntity
     * @param projectName
     * @param financialYear
     * @param procurementNature
     * @param budgetType
     * @param procurementType
     * @param appId
     * @param appCode
     * @param packageNo
     * @param packageEstimatedCostOperation
     * @param value1
     * @param value2
     * @param category
     * @param pageNumber
     * @param recordsPerPage
     * @return String search criteria key
     */
    private String getWholeStringSearchKey(String keyWord, Integer ministry_Division_Organization,
            Integer procuringEntity, String projectName, String financialYear,
            String procurementNature, String budgetType, String procurementType,
            Integer appId, String appCode, String packageNo, String packageEstimatedCostOperation,
            BigDecimal value1, BigDecimal value2, String category, Integer pageNumber, Integer recordsPerPage) {
        LOGGER.debug("getWholeStringSearchKey : " + logUserId + WSResponseObject.LOGGER_START);
        StringBuffer searchString = new StringBuffer();
        if (keyWord != null) {
            searchString.append("Keyword:");
            searchString.append(keyWord);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (ministry_Division_Organization != null) {
            searchString.append("Ministry_Division_Organization:");
            searchString.append(ministry_Division_Organization);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (procuringEntity != null) {
            searchString.append("ProcuringEntity:");
            searchString.append(procuringEntity);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (projectName != null) {
            searchString.append("ProjectName:");
            searchString.append(projectName);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (financialYear != null) {
            searchString.append("FinancialYear:");
            searchString.append(financialYear);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (procurementNature != null) {
            searchString.append("ProcurementNature:");
            searchString.append(procurementNature);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (budgetType != null) {
            searchString.append("BudgetType:");
            searchString.append(budgetType);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (procurementType != null) {
            searchString.append("ProcurementType:");
            searchString.append(procurementType);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (appId != null) {
            searchString.append("APPId:");
            searchString.append(appId);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (appCode != null) {
            searchString.append("APPCode:");
            searchString.append(appCode);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (category != null) {
            searchString.append("Category:");
            searchString.append(category);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (packageNo != null) {
            searchString.append("PackageNo:");
            searchString.append(packageNo);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (packageEstimatedCostOperation != null) {
            searchString.append("PackageEstimatedCostOperation:");
            searchString.append(packageEstimatedCostOperation);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (value1 != null) {
            searchString.append("Value1:");
            searchString.append(value1);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (value2 != null) {
            searchString.append("Value2:");
            searchString.append(value2);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (pageNumber != null) {
            searchString.append("PageNumber:");
            searchString.append(pageNumber);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (recordsPerPage != null) {
            searchString.append("PageSize:");
            searchString.append(recordsPerPage);
        }
        LOGGER.debug("getWholeStringSearchKey : " + logUserId + WSResponseObject.LOGGER_END);
        return searchString.toString();
    }
    /**
     * Web service operation
     */
    /* This method is commented by Sreenu.This method is modified as per the requirements.
     * the modified method is annualProcurementPlansAdvanceSearch(...)
     *
    @WebMethod(operationName = "SearchAnnualProcurementPlans")
    public List<AnnualProcurementPlan> SearchAnnualProcurementPlans(
    @WebParam(name = "departmentID") Integer departmentID,
    @WebParam(name = "officeID") Integer officeID,
    @WebParam(name = "appID") Integer appID,
    @WebParam(name = "projectID") String projectID,
    @WebParam(name = "financialYear") String financialYear,
    @WebParam(name = "budgetType") String budgetType,
    @WebParam(name = "procurementNature") String procurementNature,
    @WebParam(name = "procurementType") String procurementType,
    @WebParam(name = "appCode") String appCode,
    @WebParam(name = "packageNo") String packageNo,
    @WebParam(name = "packageEstimatedCostOperation") String packageEstimatedCostOperation,
    @WebParam(name = "value") Integer value,
    @WebParam(name = "CPVcategory") String CPVcategory,
    @WebParam(name = "pageNo") String pageNo,
    @WebParam(name = "recordsPerPage") String recordsPerPage) {

    AppAdvSearchService appAdvSearchService = (AppAdvSearchService) AppContext.getSpringBean("AppAdvSearchService");
    List<CommonAppSearchData> commonData = null;
    int pageno = 1;
    int recordOffset = 20;
    if (pageNo != null) {
    pageno = Integer.parseInt(pageNo);
    }
    if (recordsPerPage != null) {
    recordOffset = Integer.parseInt(recordsPerPage);
    }
    Integer deptId = 0;
    if (departmentID != null) {
    deptId = departmentID;
    }

    Integer officeId = 0;
    if (officeID != null) {
    officeId = officeID;
    }

    String project = "";
    if (projectID != null && !"".equalsIgnoreCase(projectID)) {
    project = projectID;
    }

    String fincialYear = "";
    if (financialYear != null && !"".equalsIgnoreCase(financialYear)) {
    fincialYear = financialYear;
    }

    String budgType = "";
    if (budgetType != null && !"".equalsIgnoreCase(budgetType)) {
    budgType = budgetType;
    }

    String cpvCat = "";
    if (CPVcategory != null && !"".equalsIgnoreCase(CPVcategory)) {
    cpvCat = CPVcategory;
    }

    String procNature = "";
    if (procurementNature != null && !"".equalsIgnoreCase(procurementNature)) {
    procNature = procurementNature;
    }

    String procType = "";
    if (procurementType != null && !"".equalsIgnoreCase(procurementType)) {
    procType = procurementType;
    }

    Integer appId = 0;
    if (appID != null) {
    appId = appID;
    }

    String appCod = " ";
    if (appCode != null && !"".equalsIgnoreCase(appCode)) {
    appCod = appCode;
    }

    String pkgNo = " ";
    if (packageNo != null && !"".equalsIgnoreCase(packageNo)) {
    pkgNo = packageNo;
    }

    Integer intValue = 0;
    if (value != null) {
    intValue = value;
    }

    String operation = " ";
    if (packageEstimatedCostOperation != null && !"".equalsIgnoreCase(packageEstimatedCostOperation)) {
    operation = packageEstimatedCostOperation;
    }

    Integer intValue2 = 0;

    List<AnnualProcurementPlan> annualProcurementPlans = new ArrayList<AnnualProcurementPlan>();
    commonData = appAdvSearchService.getSearchResults(null, fincialYear, procNature, deptId, 0, cpvCat, project, procType, appId, appCod, pkgNo, new BigDecimal(0), officeId, budgType, operation, new BigDecimal(intValue), new BigDecimal(intValue2), pageno, recordOffset, "", 0);
    LOGGER.debug("commonData" + commonData.size());
    if (commonData != null && !commonData.isEmpty()) {
    for (int i = 0; i < commonData.size(); i++) {
    CommonAppSearchData commonAppSearchData = commonData.get(i);
    AnnualProcurementPlan annualProcurementPlan = new AnnualProcurementPlan();
    annualProcurementPlan.setAppCode(commonAppSearchData.getAppCode());
    annualProcurementPlan.setAppId(commonAppSearchData.getAppId());
    annualProcurementPlan.setDepartmentName(commonAppSearchData.getDepartmentName());
    annualProcurementPlan.setEstimatedCost(commonAppSearchData.getEstimatedCost());
    annualProcurementPlan.setOfficeName(commonAppSearchData.getOfficeName());
    annualProcurementPlan.setPackageDesc(commonAppSearchData.getPackageDesc());
    annualProcurementPlan.setProcurementMethod(commonAppSearchData.getProcurementMethod());
    annualProcurementPlan.setProcurementNature(commonAppSearchData.getProcurementNature());
    annualProcurementPlan.setProjectName(commonAppSearchData.getProjectName());
    annualProcurementPlan.setStateName(commonAppSearchData.getStateName());
    annualProcurementPlans.add(annualProcurementPlan);
    }
    }
    return annualProcurementPlans;
    }
     */
}
