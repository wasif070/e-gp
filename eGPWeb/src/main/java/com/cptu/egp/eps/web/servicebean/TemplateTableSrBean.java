/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblListCellDetail;
import com.cptu.egp.eps.model.table.TblTemplateCells;
import com.cptu.egp.eps.model.table.TblTemplateColumns;
import com.cptu.egp.eps.model.table.TblTemplateFormulas;
import com.cptu.egp.eps.model.table.TblTemplateTables;
import com.cptu.egp.eps.service.serviceimpl.TemplateSectionFormImpl;
import com.cptu.egp.eps.service.serviceimpl.TemplateTableImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author yanki
 */
public class TemplateTableSrBean {

    private String logUserId = "0";
    private String start = " Starts";
    private String end = " Ends";

    final static Logger LOGGER = Logger.getLogger(TemplateTableSrBean.class);
    TemplateSectionFormImpl templateSectionFormImpl = (TemplateSectionFormImpl) AppContext.getSpringBean("AddFormService");
    TemplateTableImpl templateTableImpl = (TemplateTableImpl) AppContext.getSpringBean("AddFormTableService");

    /**
     * Set User Id at Bean Level
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        templateSectionFormImpl.setUserId(logUserId);
        templateTableImpl.setUserId(logUserId);
        this.logUserId = logUserId;
        }

    /**
     * Get All Template Tables
     * @param templateId
     * @param sectionId
     * @param formId
     * @return List Template Tables.
     */
    public List<TblTemplateTables> getTemplateTables(short templateId, int sectionId, int formId) {
        LOGGER.debug("getTemplateTables : " + logUserId + start);
        List<TblTemplateTables> tbltemplate = null;

        try {
            tbltemplate = templateTableImpl.getFormTables(templateId, sectionId, formId);
        } catch (Exception ex) {
            LOGGER.error("getTemplateTables : " + ex);
    }
        LOGGER.debug("getTemplateTables : " + logUserId + end);
        return tbltemplate;
    }

    /**
     * Get All Template Table Details.
     * @param tableId
     * @return List of Template Tables.
     */
    public List<TblTemplateTables> getTemplateTablesDetail(int tableId) {
        LOGGER.debug("getTemplateTablesDetail : " + logUserId + start);
        List<TblTemplateTables> tbltemplatetable = null;
        try {
            tbltemplatetable = templateTableImpl.getFormTablesDetails(tableId);
        } catch (Exception ex) {
            LOGGER.error("getTemplateTablesDetail : " + ex);
        }
        LOGGER.debug("getTemplateTablesDetail : " + logUserId + end);
        return tbltemplatetable;
    }

    /**
     * check if it is price Bid form or not.
     * @param formId
     * @return
     */
    public boolean isPriceBidForm(int formId) {
        LOGGER.debug("isPriceBidForm : " + logUserId + start);
        boolean isPriceBidFrm = false;
        try {

            isPriceBidFrm = templateSectionFormImpl.isBOQForm(formId);

        } catch (Exception ex) {
            LOGGER.error("isPriceBidForm : " + ex);

        }
        LOGGER.debug("isPriceBidForm : " + logUserId + end);
            return isPriceBidFrm;
        }

    /**
     * check if form is multiple filling or not.
     * @param formId
     * @return true if multiple filling or false if not.
     */
    public boolean isFromMultipleFilling(int formId) {
        LOGGER.debug("isFromMultipleFilling : " + logUserId + start);
        boolean isFromMultipleFilling = false;
        try {

            if(templateSectionFormImpl.getSingleForms(formId).get(0).getIsMultipleFilling().equalsIgnoreCase("YES"))
            {
                isFromMultipleFilling = true;
    }
        } catch (Exception ex) {
            LOGGER.error("isFromMultipleFilling : " + ex);
        }
        LOGGER.debug("fromIsMultipleFilling : " + logUserId + end);
        return isFromMultipleFilling;
    }

    /**
     * check if form is mandatory or not.
     * @param formId
     * @return true if mandatory or false if not.
     */
    public boolean isFormMandatory(int formId) {
        LOGGER.debug("isFormMandatory : " + logUserId + start);
        boolean isFormMandatory = false;
        try {

             if(templateSectionFormImpl.getSingleForms(formId).get(0).getIsMandatory().equalsIgnoreCase("YES"))
            {
                isFormMandatory = true;
            }

        } catch (Exception ex) {
            LOGGER.error("isFormMandatory : " + ex);
        }
        LOGGER.debug("isFormMandatory : " + logUserId + end);
        return isFormMandatory;
    }

        /**
         * Get Form Name from form Id.
         * @param formId
         * @return Form Name
         */
        public String getFormName(int formId) {
        LOGGER.debug("getFormName : " + logUserId + start);
        String formName = "";
        try {
            
        } catch (Exception ex) {
            LOGGER.error("getFormName : " + ex);

        }
        LOGGER.debug("getFormName : " + logUserId + end);
            return formName;
        }

        /**
         * Check if entry is present for columns or not
         * @param tableId
         * @return true if present or false if not.
         */
        public boolean isEntryPresentForCols(int tableId) {
        LOGGER.debug("isEntryPresentForCols : " + logUserId + start);
        boolean isEntryPresent = false;
        try {

            isEntryPresent = templateTableImpl.isEntryPresentInTableCols(tableId);

        } catch (Exception ex) {
            LOGGER.error("isEntryPresentForCols : " + ex);

        }
        LOGGER.debug("isEntryPresentForCols : " + logUserId + end);
            return isEntryPresent;
        }

    /**
     * Get No of tables in form
     * @param formId
     * @return no of columns.
     */
    public short getNoOfTablesInForm(int formId) {
        LOGGER.debug("getNoOfTablesInForm : " + logUserId + start);
        short nos = 0;
        try {

            nos = templateTableImpl.getNoOfTablesInForm(formId);

        } catch (Exception ex) {
            LOGGER.error("getNoOfTablesInForm : " + ex);

        }
        LOGGER.debug("getNoOfTablesInForm : " + logUserId + end);
            return nos;
        }

    /**
     * Get No of tables in form
     * @param tableId
     * @return no of columns.
     */
    public short getNoOfColsInTable(int tableId) {
        LOGGER.debug("getNoOfColsInTable : " + logUserId + start);
        short nos = 0;
        try {

            nos = templateTableImpl.getNoOfColsInTable(tableId);

        } catch (Exception ex) {
            LOGGER.error("getNoOfColsInTable : " + ex);

        }
        LOGGER.debug("getNoOfColsInTable : " + logUserId + end);
            return nos;
        }

    /**
     * Get no of rows in table.
     * @param tableId
     * @param columnId
     * @return no of rows in table.
     */
    public short getNoOfRowsInTable(int tableId, short columnId) {
        LOGGER.debug("getNoOfRowsInTable : " + logUserId + start);
        short nos = 0;
        try {

            nos = templateTableImpl.getNoOfRowsInTable(tableId, columnId);

        } catch (Exception ex) {
            LOGGER.error("getNoOfRowsInTable : " + ex);

        }
        LOGGER.debug("getNoOfRowsInTable : " + logUserId + end);
            return nos;
        }

    /**
     * Get column details
     * @param tableId
     * @param inSortOrder
     * @return List of Template columns.
     */
    public List<TblTemplateColumns> getColumnsDtls(int tableId, boolean inSortOrder) {
        LOGGER.debug("getColumnsDtls : " + logUserId + start);
        List<TblTemplateColumns> cols = null;
        try {

            cols = templateTableImpl.getTableColumns(tableId, inSortOrder);

        } catch (Exception ex) {
            LOGGER.error("getColumnsDtls : " + ex);

        }
        LOGGER.debug("getColumnsDtls : " + logUserId + end);
            return cols;
        }

    /**
     * Get List cell details
     * @param tableId
     * @param columnId
     * @param cellId
     * @return List of List cell details.
     */
    public List<TblListCellDetail> getListCellDetail(int tableId, short columnId, int cellId) {
        LOGGER.debug("getListCellDetail : " + logUserId + start);
        List<TblListCellDetail> listCellDetail = new ArrayList<TblListCellDetail>();
        try {

            listCellDetail = templateTableImpl.getListCellDetail(tableId, columnId, cellId);
        } catch (Exception ex) {
            LOGGER.error("getListCellDetail : " + ex);
        }
        LOGGER.debug("getListCellDetail : " + logUserId + end);
        return listCellDetail;
    }

    /**
     * Get auto column details.
     * @param tableId
     * @return List of Template Columns 
     */ 
    public List<TblTemplateColumns> getAutoColumnsDtls(int tableId) {
        LOGGER.debug("getAutoColumnsDtls : " + logUserId + start);
        List<TblTemplateColumns> cols = null;
        try {

            cols = templateTableImpl.getTableAutoColumns(tableId);

        } catch (Exception ex) {
            LOGGER.error("getAutoColumnsDtls : " + ex);

        }
        LOGGER.debug("getAutoColumnsDtls : " + logUserId + end);
            return cols;
        }

    /**
     * Get List cell details in Test Forms
     * @param tableId
     * @return List of Template Cells.
     */
    public List<TblTemplateCells> getCellsDtlsTestForm(int tableId) {
        LOGGER.debug("getCellsDtlsTestForm : " + logUserId + start);
        List<TblTemplateCells> cells = null;
        try {

            cells = templateTableImpl.getTableCellsTestForm(tableId);

        } catch (Exception ex) {
            LOGGER.error("getCellsDtlsTestForm : " + ex);

        }
        LOGGER.debug("getCellsDtlsTestForm : " + logUserId + end);
            return cells;
        }

    /**
     * Get Cell Details.
     * @param tableId
     * @return List Template Cells.
     */
    public List<TblTemplateCells> getCellsDtls(int tableId) {
        LOGGER.debug("getCellsDtls : " + logUserId + start);
        List<TblTemplateCells> cells = null;
        try {

            cells = templateTableImpl.getTableCells(tableId);

        } catch (Exception ex) {
            LOGGER.error("getCellsDtls : " + ex);

        }
        LOGGER.debug("getCellsDtls : " + logUserId + end);
            return cells;
        }

    /**
     * Get all table Formulas
     * @param tableId
     * @return List if Table Formulas
     */
    public List<TblTemplateFormulas> getTableFormulas(int tableId) {
        LOGGER.debug("getTableFormulas : " + logUserId + start);
        List<TblTemplateFormulas> formulas = null;
        try {

            formulas = templateTableImpl.getTableFormulas(tableId);

        } catch (Exception ex) {
            LOGGER.error("getTableFormulas : " + ex);


        }
        LOGGER.debug("getTableFormulas : " + logUserId + end);
            return formulas;
        }

    /**
     * Check if Auto Column is Present or not.
     * @param tableId
     * @return true if present else false if not.
     */
    public boolean isAutoColumnPresent(int tableId) {
        LOGGER.debug("isAutoColumnPresent : " + logUserId + start);
        boolean isAutoColPresent = false;
        try {

            isAutoColPresent = templateTableImpl.isAutoColumnPresent(tableId);

        } catch (Exception ex) {
            LOGGER.error("isAutoColumnPresent : " + ex);

        }
        LOGGER.debug("isAutoColumnPresent : " + logUserId + end);
            return isAutoColPresent;
        }

    /**
     * Check if Fromula is created or not.
     * @param tableId
     * @return true if created or false if not.
     */
    public boolean atLeastOneFormulaCreated(int tableId) {
        LOGGER.debug("atLeastOneFormulaCreated : " + logUserId + start);
        boolean isFormulaCre = false;
        try {

            isFormulaCre = templateTableImpl.atLeastOneFormulaCreated(tableId);

        } catch (Exception ex) {
            LOGGER.error("atLeastOneFormulaCreated : " + ex);

        }
        LOGGER.debug("atLeastOneFormulaCreated : " + logUserId + end);
            return isFormulaCre;
        }

    /**
     * Check if Total Formula Created or not.
     * @param tableId
     * @return true if created or false if not.
     */
    public boolean isTotalFormulaCreated(int tableId) {
        LOGGER.debug("isTotalFormulaCreated : " + logUserId + start);
        boolean isTotFormulaCre = false;
        try {

            isTotFormulaCre = templateTableImpl.isTotalFormulaCreated(tableId);

        } catch (Exception ex) {
            LOGGER.error("isTotalFormulaCreated : " + ex);
            isTotFormulaCre = false;
        }
        LOGGER.debug("isTotalFormulaCreated : " + logUserId + end);
            return isTotFormulaCre;
        }

    /**
     * Get All Grand Total Columns.
     * @param tableId
     * @return Hash Map Containing Grand Total Columns.
     */
    public HashMap<Integer, Integer> getGTColumns(int tableId) {
        LOGGER.debug("getGTColumns : " + logUserId + start);
        HashMap<Integer, Integer> hmCols = null;
        try {

            hmCols = templateTableImpl.getGTColumns(tableId);

        } catch (Exception ex) {
            LOGGER.error("getGTColumns : " + ex);

    }

        LOGGER.debug("getGTColumns : " + logUserId + end);
            return hmCols;
        }

    /**
     * Get Total Formula Count
     * @param tableId
     * @return not of Formula Count.
     */
    public short getFormulaCountOfTotal(int tableId) {
        LOGGER.debug("getFormulaCountOfTotal : " + logUserId + start);

        short cnt = 0;
        try {
            cnt = templateTableImpl.getFormulaCountOfTotal(tableId);

        } catch (Exception ex) {
            LOGGER.error("getFormulaCountOfTotal : " + ex);

        }
        LOGGER.debug("getFormulaCountOfTotal : " + logUserId + end);
            return cnt;
        }
}
