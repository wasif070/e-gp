/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.web.servicebean.ManageSbDpBrOfficeSrBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */

//Search Formation changed by Emtaz on 15/May/2016

public class ManageSbDpBrOfficeServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            //<editor-fold>
           /* if (request.getParameter("action").equals("fetchData")) {
                response.setContentType("text/xml;charset=UTF-8");
                System.out.println(">" + request.getQueryString());
                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String sord = request.getParameter("sord");
                String sidx = request.getParameter("sidx");
                String type=request.getParameter("partnerType");
//                if (sidx.equals("")) {//rishita - default sorting created date
//                    sidx = "name";
//                }

                //dcBean.setColName("");
                ManageSbDpBrOfficeSrBean manageSbDpBrOfficeSrBean=new ManageSbDpBrOfficeSrBean();
                manageSbDpBrOfficeSrBean.setSearch(_search);
                manageSbDpBrOfficeSrBean.setLimit(Integer.parseInt(rows));
                int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));

                manageSbDpBrOfficeSrBean.setOffset(offset);
                manageSbDpBrOfficeSrBean.setSortOrder(sord);
                manageSbDpBrOfficeSrBean.setSortCol(sidx);

                System.out.println("queryString:" + request.getQueryString());
                HttpSession session=request.getSession();
                Object objUserTypeId=session.getAttribute("userTypeId");
                Object objUserId=session.getAttribute("userId");
                List<Object[]> scDpOfficeList = null;
                String searchField = "", searchString = "", searchOper = "";
                if(_search) {
                    searchField = request.getParameter("searchField");
                    searchString = request.getParameter("searchString");
                    searchOper = request.getParameter("searchOper");
                    scDpOfficeList = manageSbDpBrOfficeSrBean.getBranchOfficeList(type,Integer.parseInt(objUserTypeId.toString()),Integer.parseInt(objUserId.toString()), searchField, searchString, searchOper);
                } else {
                     String ascClause=null;
                     if(sidx.equalsIgnoreCase("")){
                          ascClause= "";
                     }else{
                          ascClause= sidx+" "+sord;
                     }
                    scDpOfficeList = manageSbDpBrOfficeSrBean.getBranchOfficeList(type,Integer.parseInt(objUserTypeId.toString()),Integer.parseInt(objUserId.toString()),ascClause);
                }
                        
        
                int totalPages = 0;
                long totalCount = manageSbDpBrOfficeSrBean.getCntHeadOffice(type,Integer.parseInt(objUserTypeId.toString()),Integer.parseInt(objUserId.toString()));
                System.out.println("totalCount : "+totalCount);
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                        totalPages = (int) (totalCount / Integer.parseInt(request.getParameter("rows")));
                    } else {
                        totalPages = (int) ((totalCount / Integer.parseInt(request.getParameter("rows"))) + 1);
                    }
                } else {
                    totalPages = 0;
                }

                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + scDpOfficeList.size() + "</records>");
                // be sure to put text data in CDATA
                int j =0;
                int srNo = 0;
                int no = Integer.parseInt(request.getParameter("page"));
                if(no == 1){
                    j = 0;
                    srNo = 1;
                }else{
                    if(Integer.parseInt(rows) == 30){
                        j = ((no-1)*30);
                        srNo = ((no-1)*30)+1;
                    }else if(Integer.parseInt(rows) == 20){
                        j = ((no-1)*20);
                        srNo = ((no-1)*20)+1;
                    }
                    else{
                        j = ((no-1)*10);
                        srNo = ((no-1)*10)+1;
                    }
                }

                for (int i =  j; i < scDpOfficeList.size(); i++) {
                    Object[] scdpOffice=scDpOfficeList.get(i);
                    out.print("<row id='" + scdpOffice[0] + "'>");
                    out.print("<cell><![CDATA[" + srNo + "]]></cell>");
                    out.print("<cell><![CDATA[" + scdpOffice[1] + "]]></cell>");
                    out.print("<cell><![CDATA[" + scdpOffice[2] + "]]></cell>");
                    out.print("<cell><![CDATA[" + scdpOffice[4] + "]]></cell>");
                    out.print("<cell><![CDATA[" + scdpOffice[3] + "]]></cell>");
                    String editLink="<a href=\"EditSchBankDevPartnerBranch.jsp?partnerType="+ type+"&officeId="+scdpOffice[0]+"&mode=edit\">Edit</a>";
                    String viewLink="<a href=\"ViewSchBankDevPartBrnch.jsp?partnerType="+ type+"&officeId="+scdpOffice[0]+"&mode=view\">View</a>";
                    out.print("<cell><![CDATA[" + editLink+" | "+ viewLink+"]]></cell>");
                    out.print("</row>");
                    j++;
                    srNo++;
                }
                out.print("</rows>");
            }*/
           //</editor-fold>
           if (request.getParameter("action").equals("fetchData")) {
                
                System.out.println(">" + request.getQueryString());
                String type=request.getParameter("partnerType");

                ManageSbDpBrOfficeSrBean manageSbDpBrOfficeSrBean=new ManageSbDpBrOfficeSrBean();
                int pageNo = Integer.parseInt(request.getParameter("pageNo"));
                int Size = Integer.parseInt(request.getParameter("size"));
                manageSbDpBrOfficeSrBean.setLimit(1000000);
                int offset = 0;
                System.out.println("offset:" + +offset);
                manageSbDpBrOfficeSrBean.setOffset(offset);

                System.out.println("queryString:" + request.getQueryString());
                HttpSession session=request.getSession();
                Object objUserTypeId=session.getAttribute("userTypeId");
                Object objUserId=session.getAttribute("userId");
                List<Object[]> scDpOfficeList = null;
                List<Object[]> scDpOfficeListSearched = new ArrayList<Object[]>();
               
                String HeadOfficeName = request.getParameter("HeadOfficeName");
                String RegionalOfficeName = request.getParameter("RegionalOfficeName");
                String Country = request.getParameter("Country");
                String District = request.getParameter("District");
                               
                scDpOfficeList = manageSbDpBrOfficeSrBean.getBranchOfficeList(type,Integer.parseInt(objUserTypeId.toString()),Integer.parseInt(objUserId.toString()),"");
                        
                for(int j=0;j<scDpOfficeList.size();j++)
                {
                    boolean ToAdd = true;
                    if(HeadOfficeName!=null && !HeadOfficeName.equalsIgnoreCase(""))
                    {
                        if(!scDpOfficeList.get(j)[1].toString().toLowerCase().contains(HeadOfficeName.toLowerCase()))
                        {
                            ToAdd = false;
                        }
                    }
                    if(RegionalOfficeName!=null && !RegionalOfficeName.equalsIgnoreCase(""))
                    {
                        if(!scDpOfficeList.get(j)[2].toString().toLowerCase().contains(RegionalOfficeName.toLowerCase()))
                        {
                            ToAdd = false;
                        }
                    }
                    if(Country!=null && !Country.equalsIgnoreCase(""))
                    {
                        if(!Country.equalsIgnoreCase(scDpOfficeList.get(j)[4].toString()))
                        {
                            ToAdd = false;
                        }
                    }
                    if(District!=null && !District.equalsIgnoreCase(""))
                    {
                        if(!District.equalsIgnoreCase(scDpOfficeList.get(j)[3].toString()))
                        {
                            ToAdd = false;
                        }
                    }

                    if(ToAdd)
                    {
                        //SPCommonSearchData commonAppData = getGovData.get(j);
                        scDpOfficeListSearched.add(scDpOfficeList.get(j));
                    }
                }
                 
                int RecordFrom = (pageNo-1)*Size;
                int k= 0;
                String styleClass = "";
                if (scDpOfficeListSearched != null && !scDpOfficeListSearched.isEmpty()) {
                    for(k=RecordFrom;k<RecordFrom+Size && k<scDpOfficeListSearched.size();k++)
                    {
                        if(k%2==0){
                            styleClass = "bgColor-white";
                        }else{
                            styleClass = "bgColor-Green";
                        }
                        out.print("<tr class='"+styleClass+"'>");
                        out.print("<td width=\"5%\" class=\"t-align-center\">" + (k+1) + "</td>");
                        out.print("<td width=\"22%\" class=\"t-align-center\">" + scDpOfficeListSearched.get(k)[1].toString() + "</td>");
                        out.print("<td width=\"23%\" class=\"t-align-center\">" + scDpOfficeListSearched.get(k)[2].toString() + "</td>");
                        out.print("<td width=\"15%\" class=\"t-align-center\">" + scDpOfficeListSearched.get(k)[4].toString() + "</td>");
                        out.print("<td width=\"15%\" class=\"t-align-center\">" + scDpOfficeListSearched.get(k)[3].toString() + "</td>");
                        String editLink="<a href=\"EditSchBankDevPartnerBranch.jsp?partnerType="+ type+"&officeId="+scDpOfficeListSearched.get(k)[0]+"&mode=edit\">Edit</a>";
                        String viewLink="<a href=\"ViewSchBankDevPartBrnch.jsp?partnerType="+ type+"&officeId="+scDpOfficeListSearched.get(k)[0]+"&mode=view\">View</a>";
                        out.print("<td width=\"20%\" class=\"t-align-center\">" + editLink+ " | "+ viewLink + "</td>");
                        out.print("</tr>");
                    }
                }
                else
                {
                    out.print("<tr>");
                    out.print("<td colspan=\"6\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }  
                int totalPages = 1;
                if (scDpOfficeListSearched.size() > 0) {
                    totalPages = (int) (Math.ceil(Math.ceil(scDpOfficeListSearched.size()) / Size));
                    System.out.print("totalPages--"+totalPages+"records "+ scDpOfficeListSearched.size());
                    if (totalPages == 0) {
                        totalPages = 1;
                    }
                }
               out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
               
            }
        } catch (Exception ex) {
            System.out.println("Exception :" + ex);
        } finally {
            out.flush();
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
