/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblTenderColumns;
import com.cptu.egp.eps.model.table.TblTenderEnvelopes;
import com.cptu.egp.eps.model.table.TblTenderForms;
import com.cptu.egp.eps.model.table.TblTenderFormula;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.model.table.TblTenderSection;
import com.cptu.egp.eps.model.table.TblTenderTables;
import com.cptu.egp.eps.service.serviceimpl.SpXMLCommonImpl;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceimpl.TenderEnvelopeService;
import com.cptu.egp.eps.service.serviceimpl.TenderFormService;
import com.cptu.egp.eps.service.serviceimpl.TenderTableService;
import com.cptu.egp.eps.web.servicebean.TenderFormSrBean;
import com.cptu.egp.eps.web.servicebean.TenderTablesSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
//import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author yagnesh
 */
public class CreateTenderFormSrvt extends HttpServlet {

    public static String handleSpecialCharHere(String str){
        str = str.replaceAll("&", "&amp;");
        str = str.replaceAll(">", "&gt;");
        str = str.replaceAll("<", "&lt;");
        str = str.replaceAll("'", "&rsquo;");
        //str = str.replaceAll("'", "&#39;");
        str = str.replaceAll("\"", "quot;");
        return str;
    }

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            HttpSession httpSession = request.getSession();
            if(httpSession.getAttribute("userId") == null){
                response.sendRedirect("SessionTimedOut.jsp");
            }else{
                TenderFormService tenderForm = (TenderFormService) AppContext.getSpringBean("TenderAddFormService");
                    
                String action = request.getParameter("action");
                if (action.equals("formCreation")) {
                    TenderEnvelopeService tenderEnv = (TenderEnvelopeService) AppContext.getSpringBean("TenderEnvelopeService");
                    
                    tenderForm.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), httpSession.getAttribute("sessionId"), httpSession.getAttribute("userTypeId"), request.getHeader("referer")));
                    String operation = request.getParameter("btnCreateEdit");
                    int tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    int formId = 0;
                    TblTenderSection tblTenderSection = new TblTenderSection(sectionId);
                    String formName = request.getParameter("formName");
                    //formName = formName.replace("\r\n", " <br/> ");
                    String formHeader = request.getParameter("formHeader");
                    String formFooter = request.getParameter("formFooter");
                    byte noOfTable = (byte) Integer.parseInt(request.getParameter("noOfTables"));
                    String isMultipleFilling = new String("");
                    String isMandatory = new String("");
                    if (request.getParameter("multipleTimeFill").equals("yes")) {
                        isMultipleFilling = "yes";
                    } else {
                        isMultipleFilling = "no";
                    }
                    if (request.getParameter("isMandatory").equals("yes")) {
                        isMandatory = "yes";
                    } else {
                        isMandatory = "no";
                    }
                    //byte filledBy = Byte.parseByte(httpSession.getAttribute("userId").toString());
                    byte filledBy = 1;
                    String isBoq = new String("");
                    String isEnc = new String("");
                    if (request.getParameter("boqForm").equals("yes")) {
                        isBoq = "yes";
                    } else {
                        isBoq = "no";
                    }
                    isEnc = "yes";
                    int templateFormId = 0;
                    if (request.getParameter("templateFormId")!=null){
                        templateFormId = Integer.parseInt(request.getParameter("templateFormId"));
                    }
                    boolean corriCrtNPending = false;
                    if(request.getParameter("corriCrtNPending")!=null){
                        corriCrtNPending = Boolean.parseBoolean(request.getParameter("corriCrtNPending"));
                    }

                    if(corriCrtNPending){
                        if(request.getParameter("strProcNature")!=null){
                            if("Services".equalsIgnoreCase(request.getParameter("strProcNature"))){
                                templateFormId = Integer.parseInt(request.getParameter("templateFormId"));
                            }
                        }
                    }
                    TblTenderForms tblForm = new TblTenderForms();
                    if(operation.equalsIgnoreCase("Save")){
                        formId = Integer.parseInt(request.getParameter("formId"));
                        tblForm.setTenderFormId(formId);
                    }
                    if(request.getParameter("boqForm") != null && request.getParameter("boqForm").equalsIgnoreCase("yes")){
                        if("true".equalsIgnoreCase(request.getParameter("isPck"))){
                            if(request.getParameter("boqForm") != null && request.getParameter("lotIdSelection") != null){
                                tblForm.setPkgLotId(Integer.parseInt(request.getParameter("lotIdSelection")));
                            }
                        }else{
                            tblForm.setPkgLotId(Integer.parseInt(request.getParameter("lotIdSelection")));
                        }
                    }else if("-1".equalsIgnoreCase(request.getParameter("pkgOrLotId"))){
                        tblForm.setPkgLotId(0);
                    }else if(request.getParameter("pkgOrLotId") == null){
                        tblForm.setPkgLotId(0);
                    }else{
                        tblForm.setPkgLotId(Integer.parseInt(request.getParameter("pkgOrLotId")));
                    }
                    tblForm.setTblTenderSection(tblTenderSection);
                    tblForm.setTemplateFormId(templateFormId);
                    tblForm.setFilledBy(filledBy);
                    tblForm.setFormName(formName);
                    tblForm.setFormHeader(formHeader);
                    tblForm.setFormFooter(formFooter);
                    tblForm.setNoOfTables(noOfTable);
                    tblForm.setIsMultipleFilling(isMultipleFilling);
                    tblForm.setIsEncryption(isEnc);
                    tblForm.setIsPriceBid(isBoq);
                    tblForm.setIsMandatory(isMandatory);
                    if(corriCrtNPending){
                        tblForm.setFormStatus("createp");
                        tblForm.setFormType(String.valueOf(tenderForm.getSingleForms(formId).get(0).getFormType()));
                    }else{
                        tblForm.setFormStatus("p");
                        
                        //Added by Emtaz on 29/March/2018. FormType will remain same after edit for Goods Large tender.
                        if(tenderForm.IsThisGoodsLargeTender(sectionId))
                        {
                            tblForm.setFormType(String.valueOf(tenderForm.getSingleForms(formId).get(0).getFormType()));
                        }
                    }

                    int pkgOrLotId = -1;
                    String lotIDParam = "";
                    if(request.getParameter("porlId")!=null){
                        pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
                        lotIDParam = "&porlId="+pkgOrLotId;
                    }

                    if(operation.equals("Create")){
                        if (tenderForm.addForm(tblForm,tenderId)) {
                            int frmId = tblForm.getTenderFormId();
                            TblTenderEnvelopes tnEnvelops = new TblTenderEnvelopes();
                            tnEnvelops.setTblTenderForms(new TblTenderForms(frmId));
                            tnEnvelops.setTblTenderMaster(new TblTenderMaster(tenderId));
                            if(tenderForm.isTenderEvtTypeRFP(tenderId)){
                                if("yes".equals(isBoq)){
                                    tnEnvelops.setEnvelopeId((byte) 2);
                                }else{
                                    tnEnvelops.setEnvelopeId((byte) 1);
                                }
                            }else{
                                tnEnvelops.setEnvelopeId((byte) 1);
                            }
                            tenderEnv.insertTenderEnvelopes(tnEnvelops);
                            tnEnvelops = null;

                            tblForm = null;
                            response.sendRedirect("officer/CreateTenderFormTable.jsp?tenderId=" + tenderId + "&sectionId=" + sectionId + "&formId=" + frmId + "&noOfTables=" + noOfTable +lotIDParam);
                        } else {

                        }
                    }else{
                        if (tenderForm.editForm(tblForm,tenderId)) {
                            
                            int frmId = tblForm.getTenderFormId();
                            if(tenderForm.isTenderEvtTypeRFP(tenderId)){
                                if("yes".equals(isBoq)){
                                    tenderEnv.updateToTenderEnvelope(tenderId, formId, (byte) 2);
                                }else{
                                    tenderEnv.updateToTenderEnvelope(tenderId, formId, (byte) 1);
                                }
                            }else{
                                tenderEnv.updateToTenderEnvelope(tenderId, formId, (byte) 1);
                            }
                            tblForm = null;
                            response.sendRedirect("officer/TenderTableDashboard.jsp?tenderId=" + tenderId + "&sectionId=" + sectionId + "&formId=" + frmId + "&noOfTables=" + noOfTable+lotIDParam);
                        } else {

                        }
                    }
                }else if(action.equals("dumpForm")){
                    
                    int tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    int formId = Integer.parseInt(request.getParameter("formId"));
                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    boolean corriCrtNPending = false;
                    if(request.getParameter("corriCrtNPending")!=null){
                        corriCrtNPending = Boolean.parseBoolean(request.getParameter("corriCrtNPending"));
                    }
                    int pkgOrLotId = -1;
                    String auditAction = "Copy form";
                    int auditappId = tenderId;
                    
                    if(request.getParameter("porlId")!=null){
                        pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
                    }
                    TenderFormSrBean frmDtl = new TenderFormSrBean();
                    List<TblTenderForms> frm = frmDtl.getFormDetail(formId);

                    int success = -1;
                    TenderCommonService tCommSrv = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    TenderTableService tenderTable = (TenderTableService) AppContext.getSpringBean("TenderAddFormTableService");
                    
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    
                    SPTenderCommonData spTenCommData = null;
                    List<SPTenderCommonData> lstCD = tCommSrv.returndata("DumpForm", formId+"", String.valueOf(corriCrtNPending));
                    
                    AuditTrail auditTrail =  new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), httpSession.getAttribute("sessionId"), httpSession.getAttribute("userTypeId"), request.getHeader("referer"));
                    makeAuditTrailService.generateAudit(auditTrail,auditappId , "tenderId", EgpModule.Tender_Document.getName(), auditAction, frm.get(0).getFormName());
                    auditAction = null;
                    
                    if(lstCD != null){
                        if(lstCD.size() > 0){
                            spTenCommData = lstCD.get(0);
                            success = Integer.parseInt(spTenCommData.getFieldName1().trim());
                        }
                        lstCD = null;
                    }
                    if(pkgOrLotId == -1){
                        if(success == 1){
                            response.sendRedirect("officer/TenderDocPrep.jsp?tenderId=" + tenderId);
                        }else{
                            response.sendRedirect("officer/TenderDocPrep.jsp?tenderId=" + tenderId + "&sectionId=" + sectionId + "&er="+success);
                        }
                    }else{
                        if(success == 1){
                            response.sendRedirect("officer/TenderDocPrep.jsp?tenderId=" + tenderId + "&porlId=" + pkgOrLotId);
                        }else{
                            response.sendRedirect("officer/TenderDocPrep.jsp?tenderId=" + tenderId + "&sectionId=" + sectionId + "&er="+success + "&porlId=" + pkgOrLotId);
                        }
                    }
                }else if(action.equals("delForm")){
                    TenderFormSrBean frmDtl = new TenderFormSrBean();
                    int tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    int formId = Integer.parseInt(request.getParameter("formId"));
                    int corriId=0;
                    int pkgOrLotId = -1;
                    String auditAction = "Delete form";
                    int auditappId = tenderId;

                    List<TblTenderForms> frm = frmDtl.getFormDetail(formId);
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");

                    AuditTrail auditTrail =  new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), httpSession.getAttribute("sessionId"), httpSession.getAttribute("userTypeId"), request.getHeader("referer"));
                    makeAuditTrailService.generateAudit(auditTrail,auditappId , "tenderId", EgpModule.Tender_Document.getName(), auditAction, frm.get(0).getFormName());

                    if(request.getParameter("porlId")!=null){
                        pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
                    }
                    if(request.getParameter("corriId")!=null && !request.getParameter("corriId").equalsIgnoreCase("null")){
                        corriId = Integer.parseInt(request.getParameter("corriId"));
                    }
                    boolean isService = Boolean.parseBoolean(request.getParameter("pn"));
                    TenderFormService delForm = (TenderFormService) AppContext.getSpringBean("TenderAddFormService");
                    if(delForm.deleteForm(formId)){

                        // Code for delete grand summary.
                        int grandSumid=0;
                        if(corriId != 0)
                        {
                            grandSumid=frmDtl.CheckExistInGrandSum(0, formId, tenderId, "CheckExistInCorriGrandSum",corriId);
                            if(grandSumid > 0)
                            {
                                tenderForm.deleteCorriGrandSumDetail(grandSumid,corriId);
                            }
                        }
                        else
                        {
                            grandSumid = frmDtl.CheckExistInGrandSum(0, formId, tenderId, "CheckExistInGrandSum");
                            if(grandSumid > 0)
                            {
                                tenderForm.deleteGrandSum(grandSumid);
                            }
                        }

                        TenderEnvelopeService tenderEnv = (TenderEnvelopeService) AppContext.getSpringBean("TenderEnvelopeService");
                        tenderEnv.deleteFromTenderEnvelope(tenderId, formId);
                        if(isService){
                            delForm.deleteConfigMark(formId);
                        }
                    }
                    if(pkgOrLotId == -1){
                        response.sendRedirect("officer/TenderDocPrep.jsp?tenderId=" + tenderId);
                    }else{
                        response.sendRedirect("officer/TenderDocPrep.jsp?tenderId=" + tenderId + "&porlId=" + pkgOrLotId);
                    }
                }else if(action.equals("cancelForm")){
                    TenderFormSrBean frmDtl = new TenderFormSrBean();
                    int tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    int corriId = 0;
                    if(request.getParameter("corriId")!=null && !request.getParameter("corriId").equalsIgnoreCase("null")){
                        corriId = Integer.parseInt(request.getParameter("corriId"));
                    }
                    int formId = Integer.parseInt(request.getParameter("formId"));
                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    boolean isService = Boolean.parseBoolean(request.getParameter("pn"));
                    int pkgOrLotId = -1;
                    if(request.getParameter("porlId")!=null){
                        pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
                    }
                    TenderFormService cancelForm = (TenderFormService) AppContext.getSpringBean("TenderAddFormService");
                    if(cancelForm.cancelTenderForm(sectionId, formId)){
                        //cancelForm.deleteConfigMark(formId);

                        // Code for delete grand summary.
                        int grandSumid=0;
                        if(corriId != 0)
                        {
                            grandSumid=frmDtl.CheckExistInGrandSum(0, formId, tenderId, "CheckExistInCorriGrandSum",corriId);
                            if(grandSumid > 0)
                            {
                                tenderForm.deleteCorriGrandSumDetail(grandSumid,corriId);
                            }
                        }
                        else
                        {
                            grandSumid = frmDtl.CheckExistInGrandSum(0, formId, tenderId, "CheckExistInGrandSum");
                            if(grandSumid > 0)
                            {
                                tenderForm.deleteGrandSum(grandSumid);
                            }
                        }

                    }
                    if(pkgOrLotId == -1){
                        response.sendRedirect("officer/TenderDocPrep.jsp?tenderId=" + tenderId+"&isCancelled=true");
                    }else{
                        response.sendRedirect("officer/TenderDocPrep.jsp?tenderId=" + tenderId + "&porlId=" + pkgOrLotId+"&isCancelled=true");
                    }
                }else if (action.equals("formTableCreation")) {
                    TenderTableService tenderTable = (TenderTableService) AppContext.getSpringBean("TenderAddFormTableService");
                    tenderTable.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), httpSession.getAttribute("sessionId"), httpSession.getAttribute("userTypeId"), request.getHeader("referer")));
                    
                    String operation = request.getParameter("btnCreateEdit");
                    short noOfTables = Short.parseShort(request.getParameter("TableCount"));
                    int tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    int formId = Integer.parseInt(request.getParameter("formId"));
                    String isMulFilling = new String("");
                    int tableId = 0;
                    int templateTableId = 0;
                    
                    if (request.getParameter("templateTableId")!=null){
                        templateTableId = Integer.parseInt(request.getParameter("templateTableId"));
                    }
                    int pkgOrLotId = -1;
                    if(request.getParameter("porlId")!=null){
                        pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
                    }
                    if(operation.equalsIgnoreCase("Save")){
                        tableId = Integer.parseInt(request.getParameter("tableId"));
                    }
                    
                    for(short i = 1; i<= noOfTables; i++){
                        TblTenderTables tblTable = new TblTenderTables();
                        if(operation.equalsIgnoreCase("Save")){
                            tblTable.setTenderTableId(tableId);
                        }
                        
                        tblTable.setTblTenderForms(new TblTenderForms(formId));
                        tblTable.setTableName(handleSpecialCharHere(request.getParameter("TableName"+i)));
                        tblTable.setTableHeader(request.getParameter("TableHeader"+i));
                        tblTable.setTableFooter(request.getParameter("TableFooter"+i));
                        tblTable.setNoOfCols(Short.parseShort(request.getParameter("NoOfCols"+i)));
                        tblTable.setNoOfRows(Short.parseShort(request.getParameter("NoOfRows"+i)));
                        if("yes".equals(request.getParameter("MultipleFilling"+i))){
                            isMulFilling = "yes";
                        }else{
                            isMulFilling = "no";
                        }
                        tblTable.setIsMultipleFilling(isMulFilling);
                        tblTable.setTemplatetableId(templateTableId);
                        if(operation.equalsIgnoreCase("Save")){
                            tenderTable.editFormTable(tblTable,tenderId);
                        }else{
                            tenderTable.addFormTable(tblTable,tenderId);
                        }
                        tblTable = null;
                    }
                    if(pkgOrLotId == -1){
                    response.sendRedirect("officer/TenderTableDashboard.jsp?tenderId=" + tenderId + "&sectionId=" + sectionId + "&formId=" + formId);
                    }else{
                        response.sendRedirect("officer/TenderTableDashboard.jsp?tenderId=" + tenderId + "&sectionId=" + sectionId + "&formId=" + formId + "&porlId=" + pkgOrLotId);
                    }
                } else if(action.equals("formTableDel")){
                    TenderFormSrBean frmDtl = new TenderFormSrBean();
                    TenderTablesSrBean tenderTableSrBean = new TenderTablesSrBean();
                    int tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    int formId = Integer.parseInt(request.getParameter("formId"));
                    int tableId = Integer.parseInt(request.getParameter("tableId"));
                    int corriId =0;
                    if(request.getParameter("corriId")!=null && !request.getParameter("corriId").equalsIgnoreCase("null")){
                        corriId = Integer.parseInt(request.getParameter("corriId"));
                    }
                    TenderTableService tenderTable = (TenderTableService) AppContext.getSpringBean("TenderAddFormTableService");
                    
                    tenderTable.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), httpSession.getAttribute("sessionId"), httpSession.getAttribute("userTypeId"), request.getHeader("referer")));
                    tenderTable.deleteFormTable(tableId,tenderId);
                    // Delete formula
                    tenderTableSrBean.deleteTotalFormula(tableId);

                    // Code for delete grand summary.
                    int grandSumid=0;
                    if(corriId != 0)
                    {
                        grandSumid=frmDtl.CheckExistInGrandSum(tableId, formId, tenderId, "CheckExistInCorriGrandSum",corriId);
                        if(grandSumid > 0)
                        {
                            tenderForm.deleteCorriGrandSumDetail(grandSumid,corriId);
                        }
                    }
                    else
                    {
                        grandSumid = frmDtl.CheckExistInGrandSum(tableId, formId, tenderId, "CheckExistInGrandSum");
                        if(grandSumid > 0)
                        {
                            tenderForm.deleteGrandSum(grandSumid);
                        }
                    }
                    
                    response.sendRedirect("officer/TenderTableDashboard.jsp?tenderId=" + tenderId + "&sectionId=" + sectionId + "&formId=" + formId);
                } else if(action.equals("tableMatrix")){
                    TenderFormSrBean frmDtl = new TenderFormSrBean();
                    TenderTablesSrBean tenderTableSrBean = new TenderTablesSrBean();
                    SpXMLCommonImpl spXMLCommonImpl = (SpXMLCommonImpl) AppContext.getSpringBean("SpXMLCommonService");
                    AuditTrail auditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), httpSession.getAttribute("sessionId"), httpSession.getAttribute("userTypeId"), request.getHeader("referer"));
                    TenderTableService tenderTable = (TenderTableService) AppContext.getSpringBean("TenderAddFormTableService");
                    int corriId=0;
                    int tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    int formId = Integer.parseInt(request.getParameter("formId"));
                    int tableId = Integer.parseInt(request.getParameter("tableId"));
                    short noOfRows = Short.parseShort(request.getParameter("rows"));
                    short noOfCols = Short.parseShort(request.getParameter("cols"));
                    String isDeleteGs= request.getParameter("hdnTblChange");
                    int templateTableId = 0;
                    if (request.getParameter("corriId")!=null && !request.getParameter("corriId").equalsIgnoreCase("null")){
                        corriId = Integer.parseInt(request.getParameter("corriId"));
                    }
                    if (request.getParameter("templateTableId")!=null){
                        templateTableId = Integer.parseInt(request.getParameter("templateTableId"));
                    }
                    int pkgOrLotId = -1;
                    if(request.getParameter("porlId")!=null){
                        pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
                    }
                    
                    StringBuffer rootColumnStr = new StringBuffer();
                    StringBuffer rootRowStr = new StringBuffer();
                    StringBuffer rootCellDetail = new StringBuffer();
                    String colHeader = "";
                    String cellValue = "";
                    int cellCounter = 0;
                    String comboId = "";
                    int cellDataType = 0;

                    String operation = request.getParameter("subBtnCreateEdit");
                    String isinEditMode = request.getParameter("isInEditMode");
                    
                    rootColumnStr.append("<root>");
                    rootRowStr.append("<root>");
                    rootCellDetail.append("<root>");
                    
                    for(short j = 0; j<noOfCols; j++){
                        if(request.getParameter("Header"+j)!=null && !(request.getParameter("Header"+j).equals(""))){
                            //colHeader =  HandleSpecialChar.handleSpecialChar(request.getParameter("Header"+j));
                            colHeader = handleSpecialCharHere(request.getParameter("Header"+j));
                        }
                        if(request.getParameter("DataType"+(j))!=null){
                            cellDataType = Integer.parseInt(request.getParameter("DataType"+(j)));
                        }
                        rootColumnStr.append("<tbl_TenderColumns ");
                        rootColumnStr.append("tenderTableId=\""+ tableId +"\" ");
                        rootColumnStr.append("columnId=\""+ (j+1) +"\" ");
                        rootColumnStr.append("columnHeader=\""+ colHeader +"\" ");
                        rootColumnStr.append("dataType=\""+ request.getParameter("DataType"+j) +"\" ");
                        rootColumnStr.append("filledBy=\""+ request.getParameter("FillBy"+j) +"\" ");
                        rootColumnStr.append("columnType=\""+ request.getParameter("columnType"+j) +"\" ");
                        rootColumnStr.append("sortOrder=\""+ request.getParameter("SortOrder"+j) +"\"  ");
                        rootColumnStr.append("showorhide=\""+ request.getParameter("ShowOrHide"+j) +"\" ");
                        rootColumnStr.append("templateTableId=\""+ "0" +"\" />");

                        for(short i = 0; i<noOfRows; i++){
                            if(request.getParameter("Cell"+(i+1)+"_"+(j+1))!=null){
                                cellValue = HandleSpecialChar.handleSpecialChar(request.getParameter("Cell"+(i+1)+"_"+(j+1))).trim();
                                //cellValue = handleSpecialCharHere(request.getParameter("Cell"+(i+1)+"_"+(j+1))).trim();
                            }else{
                                cellValue = "";
                            }
                            if(request.getParameter("DataType"+(i+1)+"_"+(j+1))!=null){
                                cellDataType = Integer.parseInt(request.getParameter("DataType"+(i+1)+"_"+(j+1)));
                            }

                            if("2".equals(request.getParameter("FillBy"+j))){
                                if(cellDataType==9 || cellDataType==10){

                                    if(cellDataType==9){
                                        comboId = "selComboWithCalc"+((int)(i)+1)+"_"+((int)(j)+1);
                                    }else if(cellDataType==10){
                                        comboId = "selComboWithOutCalc"+((int)(i)+1)+"_"+((int)(j)+1);
                                    }
                                    //comboId = "comboValue"+((int)(i)+1)+"_"+((int)(j)+1);

                                    rootCellDetail.append("<tbl_TenderListCellDetail ");
                                    rootCellDetail.append("tenderListId=\""+ request.getParameter(comboId) +"\" ");
                                    rootCellDetail.append("tenderTableId=\""+ tableId +"\"  ");
                                    rootCellDetail.append("columnId=\""+ (j+1) +"\"  ");
                                    rootCellDetail.append("cellId=\""+ cellCounter +"\"  ");
                                    rootCellDetail.append("location=\""+ (i+1) +"\" />");
                                }
                            }
                            //System.out.println("RowId ->" + (i+1) + " --ColId -->" + (j+1) + " value is " + request.getParameter("DataType"+(i+1)+"_"+(j+1)));

                            rootRowStr.append("<tbl_TenderCells ");
                            rootRowStr.append("tenderTableId=\""+ tableId +"\" ");
                            rootRowStr.append("rowId=\""+ request.getParameter("rowsort"+(i+1)) +"\" ");
                            rootRowStr.append("cellDatatype=\""+ cellDataType +"\" ");
                            rootRowStr.append("cellvalue=\""+ cellValue +"\" ");
                            rootRowStr.append("columnId=\""+ (j+1) +"\" ");
                            rootRowStr.append("templateTableId=\""+ 0 +"\" ");
                            rootRowStr.append("templateColumnId=\""+ 0 +"\" ");
                            rootRowStr.append("cellId=\""+ cellCounter +"\" />");
                            
                            cellCounter++;
                        }
                    }
                    tenderTable.updateColNRowCnt(tableId, noOfRows, noOfCols);
                    
                    if(templateTableId != 0){
                        boolean isBOQ = false;
                        if(request.getParameter("isBOQForm") != null){
                            isBOQ = Boolean.parseBoolean(request.getParameter("isBOQForm"));
                        }
                        int i_longTextCont = 2;
                        if(isBOQ){
                            short totFormulaCnt = tenderTable.getFormulaCountOfTotal(tableId);
                            if(totFormulaCnt >= 1){
                                if(tenderTable.updateOnlyRowCnt(tableId)){
                                    for(int i=1; i<=noOfCols; i++){
                                        rootRowStr.append("<tbl_TenderCells ");
                                        rootRowStr.append("tenderTableId=\""+ tableId +"\" ");
                                        rootRowStr.append("rowId=\""+ (noOfRows + 1) +"\" ");
                                        if(request.getParameter("DataType"+(i-1))!=null){
                                        rootRowStr.append("cellDatatype=\""+ request.getParameter("DataType"+(i-1)) +"\" ");
                                        }else{
                                            rootRowStr.append("cellDatatype=\""+ i_longTextCont +"\" ");
                                        }
                                        rootRowStr.append("cellvalue=\"\" ");
                                        rootRowStr.append("columnId=\""+ (i) +"\" ");
                                        rootRowStr.append("templateTableId=\""+ templateTableId +"\" ");
                                        rootRowStr.append("templateColumnId=\""+ 0 +"\" ");
                                        rootRowStr.append("cellId=\""+ cellCounter +"\" />");
                                        cellCounter++;
                                    }
                                }
                            }
                        }
                    }
                    else // Following code added for delete formula as per Bug Id: 5482
                    {
                        short obj = tenderTableSrBean.getFormulaCountOfTotal(tableId);
                         if("true".equalsIgnoreCase(isinEditMode)){
                            if (obj > 0) {
                                tenderTableSrBean.deleteTotalFormula(tableId);
                            }
                        }
                    }

                    rootColumnStr.append("</root>");
                    rootRowStr.append("</root>");
                    rootCellDetail.append("</root>");
                    //System.out.println(" root Column " + rootColumnStr);
                    //System.out.println(" root row    " + rootRowStr);
                    
                    boolean insUpdSuccess = false;
                    String auditAction = "";

                    List<TblTenderForms> frm = frmDtl.getFormDetail(formId);
                    if("true".equalsIgnoreCase(isinEditMode)){
                        auditAction = "Edit Table Matrix";
                    }else {
                        auditAction = "Create Table Matrix ";
                    }
                    
                    int auditappId = tenderId;
        
                    if(operation.equals("Save")){
                        insUpdSuccess = spXMLCommonImpl.insertTableMatrix("tbl_TenderColumns", rootColumnStr.toString(), true, true, "tenderTableId = " + tableId);
                        insUpdSuccess = spXMLCommonImpl.insertTableMatrix("tbl_TenderCells", rootRowStr.toString(), false, true, "tenderTableId = " + tableId);
                        insUpdSuccess = spXMLCommonImpl.insertTableMatrix("tbl_TenderListCellDetail", rootCellDetail.toString(), false, true, "tenderTableId = " + tableId);
                    }else{
                        insUpdSuccess = spXMLCommonImpl.insertTableMatrix("tbl_TenderColumns", rootColumnStr.toString(), true, false, "");
                        insUpdSuccess = spXMLCommonImpl.insertTableMatrix("tbl_TenderCells", rootRowStr.toString(), false, false, "");
                        insUpdSuccess = spXMLCommonImpl.insertTableMatrix("tbl_TenderListCellDetail", rootCellDetail.toString(), false, true, "tenderTableId = " + tableId);
                    }

                    if("true".equalsIgnoreCase(isinEditMode) && isDeleteGs.equalsIgnoreCase("true"))
                    {
                        // Code for delete grand summary.
                        int grandSumid=0;
                        if(corriId != 0)
                        {
                            grandSumid=frmDtl.CheckExistInGrandSum(tableId, formId, tenderId, "CheckExistInCorriGrandSum",corriId);
                            if(grandSumid > 0)
                            {
                                tenderForm.deleteCorriGrandSumDetail(grandSumid,corriId);
                            }
                        }
                        else
                        {
                            grandSumid = frmDtl.CheckExistInGrandSum(tableId, formId, tenderId, "CheckExistInGrandSum");
                            if(grandSumid > 0)
                            {
                                tenderForm.deleteGrandSum(grandSumid);
                            }
                        }
                    }

                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    makeAuditTrailService.generateAudit(auditTrail,auditappId , "tenderId", EgpModule.Tender_Document.getName(), auditAction, frm.get(0).getFormName());
                    auditAction = null;
                    
                    rootColumnStr = null;
                    rootRowStr = null;
                    if(pkgOrLotId == -1){
                    if(insUpdSuccess){
                        response.sendRedirect("officer/TenderTableDashboard.jsp?tenderId=" + tenderId + "&sectionId=" + sectionId + "&formId=" + formId);
                    }else{
                        response.sendRedirect("officer/TenderTableDashboard.jsp?tenderId=" + tenderId + "&sectionId=" + sectionId + "&formId=" + formId + "&er=t");
                    }
                    }else{
                        if(insUpdSuccess){
                            response.sendRedirect("officer/TenderTableDashboard.jsp?tenderId=" + tenderId + "&sectionId=" + sectionId + "&formId=" + formId + "&porlId=" + pkgOrLotId);
                        }else{
                            response.sendRedirect("officer/TenderTableDashboard.jsp?tenderId=" + tenderId + "&sectionId=" + sectionId + "&formId=" + formId + "&er=t" + "&porlId=" + pkgOrLotId);
                        }
                    }
                } else if(action.equals("formulaCreation")){
                    
                    TenderTableService tenderTable = (TenderTableService) AppContext.getSpringBean("TenderAddFormTableService");
                    AuditTrail auditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), httpSession.getAttribute("sessionId"), httpSession.getAttribute("userTypeId"), request.getHeader("referer"));

                    int tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    int formId = Integer.parseInt(request.getParameter("formId"));
                    int tableId = Integer.parseInt(request.getParameter("tableId"));
                    int columnId = Integer.parseInt(request.getParameter("SaveTo"));
                    int pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
                    
                    String auditAction = "Create Formula";
                    String formula = request.getParameter("hidFormula");
                    
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    TblTenderFormula tblFormula = new TblTenderFormula();
                    tblFormula.setTblTenderForms(new TblTenderForms(formId));
                    tblFormula.setTenderTableId(tableId);
                    tblFormula.setColumnId(columnId);
                    tblFormula.setFormula(formula);
                    tblFormula.setIsGrandTotal("no");
                    tenderTable.addTableFormula(tblFormula);
                    tblFormula = null;

                    // For total Formulas Start
                    boolean doInsCellMaster = false;
                    String a[] = request.getParameter("AddComboTo").split(",");
                    for(int i=0;i<a.length;i++){
                        if(a[i].length()>6){
                            tblFormula = new TblTenderFormula();
                            tblFormula.setTblTenderForms(new TblTenderForms(formId));
                            tblFormula.setTenderTableId(tableId);
                            tblFormula.setIsGrandTotal("yes");
                            short totFormulaCnt = tenderTable.getFormulaCountOfTotal(tableId);
                            if(totFormulaCnt == 0){
                                doInsCellMaster = true;
                            }
                            tblFormula.setColumnId(Integer.parseInt(a[i].substring(6,a[i].length())));
                            if(a[i]!=null){
                                switch(Integer.parseInt(request.getParameter(a[i]))){
                                    case 1 : tblFormula.setFormula("TOTAL(" + tblFormula.getColumnId() + ")");
                                        break;
                                    case 2 : tblFormula.setFormula("AVG(" + tblFormula.getColumnId() + ")");
                                        break;
                                }
                                
                                tenderTable.addTableFormula(tblFormula);
                            }
                            tblFormula = null;
                        }
                    }
                    if(doInsCellMaster){
                        StringBuffer rootCellsStr = new StringBuffer();
                        short noOfCols = Short.parseShort(request.getParameter("colCnt"));
                        if(tenderTable.updateOnlyRowCnt(tableId)){
                            SpXMLCommonImpl spXMLCommonImpl = (SpXMLCommonImpl) AppContext.getSpringBean("SpXMLCommonService");
                            int cellCounter = tenderTable.getMaxCellCountForTable(tableId);
                            List<TblTenderColumns> listcolumn = new ArrayList<TblTenderColumns>();
                            listcolumn = tenderTable.getTenderColumns(tableId);

                            int rowNumber = tenderTable.getNoOfRowsInTable(tableId, (short) 1);
                            rootCellsStr.append("<root>");
                            for(int i=1; i<=noOfCols; i++){
                                cellCounter++;
                                rootCellsStr.append("<tbl_TenderCells ");
                                rootCellsStr.append("tenderTableId=\""+ tableId +"\" ");
                                rootCellsStr.append("rowId=\""+ (rowNumber + 1) +"\" ");
                                rootCellsStr.append("cellDatatype=\""+listcolumn.get(i-1).getDataType() +"\" ");
                                rootCellsStr.append("cellvalue=\"\" ");
                                rootCellsStr.append("columnId=\""+ (i) +"\" ");
                                rootCellsStr.append("templateTableId=\""+ 0 +"\" ");
                                rootCellsStr.append("templateColumnId=\""+ 0 +"\" ");
                                rootCellsStr.append("cellId=\""+ cellCounter +"\" />");
                            }
                            rootCellsStr.append("</root>");
                            //System.out.println(" root cells str " + rootCellsStr.toString());
                            spXMLCommonImpl.insertTableMatrix("tbl_TenderCells", rootCellsStr.toString(), false, false, "");
                        }
                    }
                    
                    makeAuditTrailService.generateAudit(auditTrail,tenderId , "tenderId", EgpModule.Tender_Document.getName(), auditAction, "");
                    // For total Formulas End                    
                    
                    response.sendRedirect("officer/CreateTenderFormula.jsp?tenderId=" + tenderId + "&sectionId=" + sectionId + "&formId=" + formId + "&tableId=" + tableId + "&porlId=" +pkgOrLotId);
                } else if(action.equals("formulaDel")){
                    
                    TenderTableService tenderTable = (TenderTableService) AppContext.getSpringBean("TenderAddFormTableService");
                    AuditTrail auditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), httpSession.getAttribute("sessionId"), httpSession.getAttribute("userTypeId"), request.getHeader("referer"));
                    
                    int tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    int formId = Integer.parseInt(request.getParameter("formId"));
                    int tableId = Integer.parseInt(request.getParameter("tableId"));
                    int pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
                    String columnsId = "";
                    
                    String auditAction = "Delete Formula";
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    
                    //System.out.println(" 111 " + request.getParameterValues("chkDelete"));
                    if(request.getParameterValues("chkDelete")!=null){
                        String ids[] = request.getParameterValues("chkDelete");
                        //System.out.println(" 222 " + ids.length);
                        for(int i=0;i<ids.length;i++){
                            if(i == (ids.length-1)){
                                columnsId += ids[i];
                            }else{
                                columnsId += ids[i] + ",";
                            }
                        }
                        
                        short totFormulaCntForColSelected = tenderTable.getFormulaCountOfTotal(tableId, columnsId);
                        short totFormulaCntInTable = tenderTable.getFormulaCountOfTotal(tableId);
                        //System.out.println(" 333 " + totFormulaCntForColSelected);
                        boolean flag = false;
                        if(totFormulaCntForColSelected == 1 && totFormulaCntInTable == 1){
                            flag = tenderTable.deleteFormulaEffectsFromTenderCells(tableId);
                            flag = tenderTable.deleteFormulaEffectsFromTenderTables(tableId);
                            if(flag){
                                //System.out.println(" 444 ");
                                tenderTable.deleteTableFormula(columnsId, tableId);
                            }
                        }else{
                            tenderTable.deleteTableFormula(columnsId, tableId);
                        }
                    }

                    String totalColumnsId = "";
                    //System.out.println(" 5555 " + request.getParameterValues("chkTotalDelete"));
                    if(request.getParameterValues("chkTotalDelete")!=null){
                        String totalids[] = request.getParameterValues("chkTotalDelete");
                        //System.out.println(" 666 " + totalids.length);
                        for(int ii=0;ii<totalids.length;ii++){
                            if(ii == (totalids.length-1)){
                                totalColumnsId += totalids[ii];
                            }else{
                                totalColumnsId += totalids[ii] + ",";
                            }
                        }
                        if(totalColumnsId.length() > 0){
                            //System.out.println("   7777  ");
                            short totFormulaCnt = tenderTable.getFormulaCountOfTotal(tableId);
                            //System.out.println(" 8888 " + totFormulaCnt);
                            boolean flag = false;
                            if(totFormulaCnt == 1){
                                flag = tenderTable.deleteFormulaEffectsFromTenderCells(tableId);
                                flag = tenderTable.deleteFormulaEffectsFromTenderTables(tableId);
                                if(flag){
                                    //System.out.println(" 9999 ");
                                    tenderTable.deleteTableFormula(totalColumnsId, tableId);
                                }
                            }
                        }
                        //System.out.println(" 101010 ");
                    }
                    makeAuditTrailService.generateAudit(auditTrail,tenderId , "tenderId", EgpModule.Tender_Document.getName(), auditAction, "");
                    response.sendRedirect("officer/CreateTenderFormula.jsp?tenderId=" + tenderId + "&sectionId=" + sectionId + "&formId=" + formId + "&tableId=" + tableId + "&porlId=" + pkgOrLotId);
                }
                 else if(action.equals("checkGrandSumExist")){
                     
                     int corriId=0;
                     int grandSumid=0;
                     TenderFormSrBean frmDtl = new TenderFormSrBean();
                     int tableId=0,tenderId=0,formId=0;
                     if(request.getParameter("tableId")!= null)
                         tableId=Integer.parseInt(request.getParameter("tableId"));
                     if(request.getParameter("formId")!= null)
                         formId=Integer.parseInt(request.getParameter("formId"));
                     if(request.getParameter("tenderId")!= null)
                         tenderId=Integer.parseInt(request.getParameter("tenderId"));
                     if(request.getParameter("corriId")!=null && !request.getParameter("corriId").equalsIgnoreCase("null")){
                        corriId = Integer.parseInt(request.getParameter("corriId"));
                    }
                     if(corriId != 0)
                         grandSumid=frmDtl.CheckExistInGrandSum(tableId, formId, tenderId, "CheckExistInCorriGrandSum",corriId);
                     else
                         grandSumid=frmDtl.CheckExistInGrandSum(tableId, formId, tenderId, "CheckExistInGrandSum");
                     //System.out.println("-------------------------------grandSumid="+grandSumid);
                     out.println(grandSumid);
                 }
                else if(action.equals("checkGrandSumNeedAtCorri"))
                {
                    String returnMsg="";
                     TenderFormSrBean frmDtl = new TenderFormSrBean();
                     int tenderId=0;
                     int corriId=0;
                     if(request.getParameter("tenderId")!= null)
                         tenderId=Integer.parseInt(request.getParameter("tenderId"));
                     if(request.getParameter("corriId")!=null && !request.getParameter("corriId").equalsIgnoreCase("null")){
                        corriId = Integer.parseInt(request.getParameter("corriId"));
                     returnMsg=frmDtl.checkGrandSumNeedAtCorri(tenderId,corriId, "checkGrandSumNeedAtCorri");
                     out.println(returnMsg);
                 }

                }
                 else if(action.equals("createFixedBOQForm"))
                {

                   String tenderId=(request.getParameter("tenderId")!=null)?request.getParameter("tenderId"):"0";
                   String sectionId=(request.getParameter("sectionId")!=null)?request.getParameter("sectionId"):"0";
                   String boqType=(request.getParameter("boqType")!=null)?request.getParameter("boqType"):"0";
                  boolean isFormCreated= tenderForm.createFixedRateOrSalvageForm(tenderId, sectionId, boqType);

                 response.sendRedirect("officer/TenderDocPrep.jsp?tenderId=" + tenderId +"&isFixedBOQCreated="+isFormCreated);

                 }
            }
        } catch (Exception ex){
            Logger.getLogger(CreateTenderFormSrvt.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
