/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.databean.TempCompanyDocumentsDtBean;
import com.cptu.egp.eps.web.servicebean.DocumentBriefcaseSrBean;
import com.cptu.egp.eps.web.servicebean.TempCompanyDocumentsSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CheckExtension;
import com.cptu.egp.eps.web.utility.FileEncryptDecryptUtil;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import com.cptu.egp.eps.web.utility.HashUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

/**
 *
 * @author administrator
 */
//@WebServlet(name = "FileUploadServlet", urlPatterns = {"/FileUploadServlet"})
public class FileUploadServlet extends HttpServlet {

    static final Logger logger = Logger.getLogger(FileUploadServlet.class);
    private static final String TMP_DIR_PATH = "c:\\tmp";
    private File tmpDir;
    private static final String DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("FileUploadServlet");
    private File destinationDir;
    DataInputStream dis = null;
    BufferedInputStream bis = null;
    FileInputStream fis = null;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        tmpDir = new File(TMP_DIR_PATH);
        if (!tmpDir.isDirectory()) {
            tmpDir.mkdir();
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String hashVal = "";
        String realPath = DESTINATION_DIR_PATH + request.getSession().getAttribute("userId");//request.getSession().getAttribute("userId")
        logger.debug("Real Path " + realPath);
        destinationDir = new File(realPath);
        if (!destinationDir.isDirectory()) {
            destinationDir.mkdir();
        }
        response.setContentType("text/html");
        String logUserId = null;
        try {

            logUserId = request.getSession().getAttribute("userId").toString();
            logger.debug("FileUploadServlet : " + logUserId + " Starts");
            DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
            /*
             *Set the size threshold, above which content will be stored on disk.
             */
            fileItemFactory.setSizeThreshold(1 * 1024 * 1024); //1 MB
            /*
             * Set the temporary directory to store the uploaded files of size above threshold.
             */
            fileItemFactory.setRepository(tmpDir);

            ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
            try {
                /*
                 * Parse the request
                 */
                List items = uploadHandler.parseRequest(request);
                Iterator itr = items.iterator();
                //For Supporting Document
                TempCompanyDocumentsDtBean tempCompanyDocumentsDtBean = new TempCompanyDocumentsDtBean();
                int tendererid = 0;
                String funName = "";
                String docSizeMsg = "";
                int companyId = 0;
                boolean hashfail = false;
                boolean checkExt = false;
                boolean checkSize = false;
                boolean sameFile = false;
                String hashval = "";

                while (itr.hasNext()) {
                    FileItem item = (FileItem) itr.next();
                    //For Supporting Document                   
                    /*
                     * Handle Form Fields.
                     */
                    if (item.isFormField()) {
                        logger.debug("FileUploadServlet : " + logUserId + " File Name = " + item.getFieldName() + ", Value = " + item.getString());
                        if (item.getFieldName().equals("documentBrief")) {
                            if (item.getString() == null) {
                                tempCompanyDocumentsDtBean.setDocumentBrief("");
                            } else {
                                tempCompanyDocumentsDtBean.setDocumentBrief(item.getString());
                            }
                        } else if (item.getFieldName().equals("documentType")) {
                            tempCompanyDocumentsDtBean.setDocumentType(item.getString());
                        } else if (item.getFieldName().equals("tendererId")) {
                            tendererid = Integer.parseInt(item.getString());
                        } else if (item.getFieldName().equals("funName")) {
                            funName = item.getString();
                        } else if (item.getFieldName().equals("docName")) {
                            if (!item.getString().equals("")) {
                                tempCompanyDocumentsDtBean.setDocDescription(item.getString());
                            }
                        } else if (item.getFieldName().equals("docRefNo")) {
                            tempCompanyDocumentsDtBean.setDocRefNo(item.getString());
                        }

                    } else {
                        //Handle Uploaded files.
                        logger.debug("FileUploadServlet : " + logUserId + " Field Name = " + item.getFieldName()
                                + ", File Name = " + item.getName()
                                + ", Content type = " + item.getContentType()
                                + ", File Size = " + item.getSize());

                        /*try{
                            logger.debug("============= Start Hash File ===========");
                            fis = new FileInputStream(item.getName());
                            byte[] buf = new byte[Integer.valueOf(String.valueOf(destinationDir.length()))];

                            int offset = 0;
                            int numRead = 0;
                            while ((offset < buf.length) && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {

                                offset += numRead;
                            }
                            fis.close();
                            hashVal = HashUtil.getHash(new String(buf),"SHA-1");
                            logger.debug("FileUploadServlet : "+logUserId+" Hash : "+hashVal);
                            logger.debug("FileUploadServlet : "+logUserId+" ============= End Hash File ===========");
                            
                            /*
                            bis = new BufferedInputStream(fis);
                            dis = new DataInputStream(bis);
                            while (dis.available() != 0) {
                                logger.debug(dis.readLine());
                            }
                            
                            fis.close();
                            bis.close();
                            dis.close();
                        }catch(Exception e){
                            logger.error("FileUploadServlet : "+logUserId+" Error : "+e);
                        }*/
 /*
                         * Write file to the ultimate location.
                         */
                        String fileName = "";
                        if (item.getName().lastIndexOf("\\") != -1) {
                            fileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1, item.getName().length());
                        } else {
                            fileName = item.getName();
                        }
                        //fileName  = fileName.replaceAll(" ", "");
                        File f = new File(destinationDir + "\\" + fileName);
                        if (f.isFile()) {
                            sameFile = true;
                            break;
                        }
                        docSizeMsg = docSizeMsg(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
                        if (!docSizeMsg.equals("ok")) {
                            break;
                        }
                        checkExt = checkExtension(fileName, item.getSize(), "tenderer");
                        if (!checkExt) {
                            break;
                        }
                        checkSize = checkSize(fileName, item.getSize(), "tenderer");
                        if (!checkSize) {
                            break;
                        }
                        tempCompanyDocumentsDtBean.setDocumentName(fileName);
                        tempCompanyDocumentsDtBean.setDocHash(hashVal);
                        File file = new File(destinationDir, fileName);
                        item.write(file);
                        InputStream fis = new FileInputStream(file);
                        byte[] buf = new byte[(int) item.getSize()];
                        int offset = 0;
                        int numRead = 0;

                        while ((offset < buf.length) && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {
                            offset += numRead;
                        }
                        fis.close();
                        //File Encyrption starts
                        FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                        tempCompanyDocumentsDtBean.setDocumentSize(String.valueOf(fileEncryptDecryptUtil.fileEncryptUtil(file, (int) item.getSize())));
                        fileEncryptDecryptUtil = null;
                        //File Encyrption ends

                        hashval = HashUtil.getHash(new String(buf), "SHA-1");
                        if (hashval == null || "".equals(hashval.trim())) {
                            f.delete();
                            hashfail = true;
                        }
                        tempCompanyDocumentsDtBean.setDocHash(hashval);
                    }
                }
//                if(sameFile){
//                    docSizeMsg="";
//                }
                String queryString = "";
                String pageName = request.getHeader("referer").substring(request.getHeader("referer").lastIndexOf("/") + 1, request.getHeader("referer").lastIndexOf(".jsp") + 4);
                if (sameFile) {
                    queryString = "?sf=y";//IMP note : sf means same file upload message.
                } else if (!docSizeMsg.equals("ok")) {
                    queryString = "?fq=" + docSizeMsg;//IMP note : fq means if total file quota of user increases than it is check through SP p_getappcommondat(checkQuota) and message is given.
                    //response.sendRedirect("SupportingDocuments.jsp?fq="+docSizeMsg);
                } else if (!checkExt) {
                    CheckExtension ext = new CheckExtension();
                    TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("tenderer");
                    queryString = "?ft=" + configurationMaster.getAllowedExtension();//IMP note : fs means single file size and ft means single file type if does'nt match in DB than this message is fired.
                    //response.sendRedirect("SupportingDocuments.jsp?fs="+configurationMaster.getFileSize()+"&ft="+configurationMaster.getAllowedExtension());
                } else {
                    CheckExtension ext = new CheckExtension();
                    TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("tenderer");
                    if (!checkSize) {
                        queryString = "?fs=" + configurationMaster.getFileSize();//IMP note : fs means single file size and ft means single file type if does'nt match in DB than this message is fired.
                    } else //For Supporting Document
                    {
                        if (funName.equals("supportdoc")) {
                            boolean FileExists = false;
                            List items2 = items;
                            itr = items2.iterator();
                            while (itr.hasNext()) {
                                FileItem item3 = (FileItem) itr.next();
                                if (item3.getName() != null) {
                                    String fileName = "";
                                    if (item3.getName().lastIndexOf("\\") != -1) {
                                        fileName = item3.getName().substring(item3.getName().lastIndexOf("\\") + 1, item3.getName().length());
                                    } else {
                                        fileName = item3.getName();
                                    }
                                    File file = new File(destinationDir, fileName);
                                    if (file.isFile()) {
                                        FileExists = true;
                                    } else {
                                        FileExists = false;
                                        queryString = "?msg=fail";
                                        break;
                                    }
                                }
                            }
                            if (FileExists) {
                                TempCompanyDocumentsSrBean tempCompanyDocumentsSrBean = new TempCompanyDocumentsSrBean();
                                if (!tempCompanyDocumentsSrBean.uploadDoc(tempCompanyDocumentsDtBean, tendererid)) {
                                    queryString = "?msg=fail";
                                }
                                tempCompanyDocumentsSrBean = null;
                            }
                            //response.sendRedirect("SupportingDocuments.jsp");
                        }
                        if (funName.equals("supportdocreapply")) {
                            boolean FileExists = false;
                            List items2 = items;
                            itr = items2.iterator();
                            while (itr.hasNext()) {
                                FileItem item3 = (FileItem) itr.next();
                                if (item3.getName() != null) {
                                    String fileName = "";
                                    if (item3.getName().lastIndexOf("\\") != -1) {
                                        fileName = item3.getName().substring(item3.getName().lastIndexOf("\\") + 1, item3.getName().length());
                                        //fileName =  "Reapply-" + fileName;
                                    } else {
                                        fileName = item3.getName();
                                        //fileName = "Reapply-" + fileName;
                                    }
                                    File file = new File(destinationDir, fileName);
                                    if (file.isFile()) {
                                        FileExists = true;
                                    } else {
                                        FileExists = false;
                                        queryString = "?msg=fail";
                                        break;
                                    }
                                }
                            }
                            if (FileExists) {
                                TempCompanyDocumentsSrBean tempCompanyDocumentsSrBean = new TempCompanyDocumentsSrBean();
                                //tempCompanyDocumentsDtBean.setIsReapplied(1);
                                if (!tempCompanyDocumentsSrBean.uploadDocReapply(tempCompanyDocumentsDtBean, tendererid)) {
                                    queryString = "?msg=fail";
                                }
                                tempCompanyDocumentsSrBean = null;
                            }
                            //response.sendRedirect("SupportingDocuments.jsp");
                        } else if (funName.equals("uploaddoc")) {
                            boolean FileExists = false;
                            List items2 = items;
                            itr = items2.iterator();
                            while (itr.hasNext()) {
                                FileItem item3 = (FileItem) itr.next();
                                if (item3.getName() != null) {
                                    String fileName = "";
                                    if (item3.getName().lastIndexOf("\\") != -1) {
                                        fileName = item3.getName().substring(item3.getName().lastIndexOf("\\") + 1, item3.getName().length());
                                    } else {
                                        fileName = item3.getName();
                                    }
                                    File file = new File(destinationDir, fileName);
                                    if (file.exists()) {
                                        FileExists = true;
                                    } else {
                                        FileExists = false;
                                        queryString = "?fup=y";
                                        break;
                                    }
                                }
                            }
                            if (FileExists) {
                                TempCompanyDocumentsSrBean tempCompanyDocumentsSrBean = new TempCompanyDocumentsSrBean();
                                if (!tempCompanyDocumentsSrBean.uploadeSign(tempCompanyDocumentsDtBean, tendererid)) {
                                    queryString = "?msg=fail";
                                }
                                tempCompanyDocumentsSrBean = null;
                            }
                            // response.sendRedirect("Authenticate.jsp");
                        } else if (funName.equals("brifcasedoc") && !hashfail) {
                            boolean FileExists = false;
                            List items2 = items;
                            itr = items2.iterator();
                            while (itr.hasNext()) {
                                FileItem item3 = (FileItem) itr.next();
                                if (item3.getName() != null) {
                                    String fileName = "";
                                    if (item3.getName().lastIndexOf("\\") != -1) {
                                        fileName = item3.getName().substring(item3.getName().lastIndexOf("\\") + 1, item3.getName().length());
                                    } else {
                                        fileName = item3.getName();
                                    }
                                    File file = new File(destinationDir, fileName);
                                    if (file.isFile()) {
                                        FileExists = true;
                                    } else {
                                        FileExists = false;
                                        queryString = "?fup=y";
                                        break;
                                    }
                                }
                            }
                            if (FileExists) {
                                DocumentBriefcaseSrBean documentBriefcaseSrBean = new DocumentBriefcaseSrBean();
                                documentBriefcaseSrBean.uploadDoc(tempCompanyDocumentsDtBean, tendererid);
                                documentBriefcaseSrBean = null;
                            }
                        }
                    }
                }
                if (hashfail) {
                    response.sendRedirect("tenderer/CommonDocLib.jsp?msg=hashfail");
                    //  response.sendRedirectFilter("CommonDocLib.jsp?msg=hashfail");
                }

                if (funName.equals("brifcasedoc")) {
                    response.sendRedirect("tenderer/" + pageName + queryString);
                    //response.sendRedirectFilter(pageName+queryString);
                } else if (funName.equals("supportdocreapply")) {
                    UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                    companyId = userRegisterService.getCompanyIdByTendererId(tendererid);
                    String url = "tenderer/" + pageName + "?tId=" + tendererid + "&cId=" + companyId;
                    if (!queryString.isEmpty()) {
                        url = url + "&" + queryString.substring(1);
                    }

                    response.sendRedirect(url);
                } else {
                    response.sendRedirect(pageName + queryString);
                }
            } catch (FileUploadException ex) {
                logger.error("FileUploadServlet : " + logUserId + " FileUploadException : " + ex);
                log("Error encountered while parsing the request", ex);
            } catch (Exception ex) {
                logger.error("FileUploadServlet : " + logUserId + " FileUploadException : " + ex);
                log("Error encountered while uploading file", ex);
            }

        } finally {
            logger.debug("FileUploadServlet : " + logUserId + " Ends");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public boolean checkExtension(String extn, long size, String userType) {
        boolean chextn = false;
        int j = extn.lastIndexOf('.');
        String lst = extn.substring(j + 1);

        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        String str = configurationMaster.getAllowedExtension();
        String[] str1 = str.split(",");
        for (int i = 0; i < str1.length; i++) {
            if (str1[i].trim().equalsIgnoreCase(lst)) {
                chextn = true;
            }
        }
        return chextn;
    }

    public String docSizeMsg(int userId) {
        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
        return userRegisterService.docSizeCheck(userId);
    }

    public boolean checkSize(String extn, long size, String userType) {
        float fsize = 0.0f;
        float dsize = 0.0f;
        boolean chextn = false;
        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        fsize = size / (1024 * 1024);
        dsize = configurationMaster.getFileSize();
        if (dsize > fsize) {
            chextn = true;
        } else {
            chextn = false;
        }
        return chextn;
    }
}
