/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblMessageFolder;
import com.cptu.egp.eps.service.serviceimpl.MessageBoxServiceImpl;
import com.cptu.egp.eps.web.databean.AddFolderDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Naresh.Akurathi
 */
public class AddFolderSrBean {

    static final Logger logger = Logger.getLogger(AddFolderSrBean.class);
    private MessageBoxServiceImpl messageBoxSrImpl = (MessageBoxServiceImpl) AppContext.getSpringBean("MessageBoxService");

    public AddFolderSrBean() {
    }
    private String loggerStart = " Starts";
    private String loggerEnd = " Ends";
    private String logUserId = "0";

    public String getLogUserId() {
        return logUserId;
    }
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        messageBoxSrImpl.setAuditTrail(auditTrail);
    }

    public void setLogUserId(String logUserId) {
        messageBoxSrImpl.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    //Method for Adding new Folder to MessageBox
    public void addFolderName(AddFolderDtBean folderDtBean, int userId) {
        logger.debug("addFolderName : " + logUserId + loggerStart);
        try {
            TblMessageFolder tblMessageFolder = new TblMessageFolder();   //Model Bean Class.

            tblMessageFolder.setFolderName(folderDtBean.getFolderName());
            tblMessageFolder.setCreationDate(new Date());   //Asigning UserDtBean to Model Bean.

            messageBoxSrImpl.addFolder(tblMessageFolder, userId); //calling Service Layer.

        } catch (Exception e) {
            logger.error("addFolderName : " + logUserId,e);
        }
        logger.debug("addFolderName : " + logUserId + loggerEnd);
        }

    //Checking for FolderName existing in Database or not..!
    public boolean checkFolderName(String folderName, int userId) {
        logger.debug("checkFolderName : " + logUserId + loggerStart);
        boolean isChk = false;
        try {
            List l = null;
            l = messageBoxSrImpl.getMessageFolder(folderName, userId);//Calling Sevice Layer method.
            if (l.isEmpty()) {
                isChk = true;
        }
        } catch (Exception e) {
            logger.error("checkFolderName : " + logUserId,e);
        }
        logger.debug("checkFolderName : " + logUserId + loggerEnd);
        return isChk;
    }
}
