/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.model.table.TblTenderRights;
import com.cptu.egp.eps.service.serviceimpl.CommonServiceImpl;
import com.cptu.egp.eps.service.serviceimpl.TenderSubRightServiceImpl;
import com.cptu.egp.eps.service.serviceinterface.TenderService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.DateUtils;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;

/**
 *
 * @author KETAN
 */
public class TenderSubRightServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     *
     */
    final static Logger LOGGER = Logger.getLogger(AdminActDeactServlet.class);
    TenderSubRightServiceImpl tenderRightsServiceImpl = (TenderSubRightServiceImpl) AppContext.getSpringBean("TenderSubRightService");
    CommonServiceImpl commonServiceImpl = (CommonServiceImpl) AppContext.getSpringBean("CommonService");
    private String logUserId = "0";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        
         // Coad added by Dipal for Audit Trail Log.
        AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer"));
        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
        String idType="userId";
        int auditId=Integer.parseInt(session.getAttribute("userId").toString());
        String auditAction=null;
        String moduleName=EgpModule.My_Account.getName();
        String remarks=null;
        
        if (session.getAttribute("userId") != null) {
            logUserId = session.getAttribute("userId").toString();
            tenderRightsServiceImpl.setLogUserId(logUserId);
            commonServiceImpl.setUserId(logUserId);
        }
        try {

            if (request.getParameter("hdn_action") != null && "assignTenderRights".equalsIgnoreCase(request.getParameter("hdn_action"))) 
            {
                String strTenderId = "";
                String strUserId = "";
                String strRefNo = "";
                String strPEOfficeName = "";
                boolean bSuccess = false;
                strTenderId = request.getParameter("hdn_tenderId");

                strUserId = request.getParameter("sel_assingTo");
                strRefNo = request.getParameter("hdn_refNo");
                strPEOfficeName = request.getParameter("hdn_peOfficeName");
                TblTenderRights tblTenderRights = new TblTenderRights();
                tblTenderRights.setTblTenderMaster(new TblTenderMaster(Integer.valueOf(strTenderId)));
                tblTenderRights.setUserId(Integer.valueOf(strUserId));
                tblTenderRights.setIsCurrent("yes");
                tblTenderRights.setMapDate(new java.sql.Date(new java.util.Date().getTime()));
                tblTenderRights.setMappedBy(Integer.valueOf(logUserId));

                bSuccess = tenderRightsServiceImpl.assingTenderRights(tblTenderRights);
                
                remarks="User Id: "+auditId+" has assigned Tender Submission Rights of Tender Id:"+strTenderId +"to Bidder Id:"+strUserId;
                if (bSuccess) {
                    String fromEmailId = ""; // This will be Company Admin email id
                    fromEmailId = commonServiceImpl.getEmailId(logUserId);

                    String toEmailId = "";
                    toEmailId = commonServiceImpl.getEmailId(strUserId);
                    String[] mailTo = {toEmailId}; //This will be company user's email Id to whom tender rights is to be assign
                    sendMail(fromEmailId, mailTo, strTenderId, strRefNo, strPEOfficeName);
                    
                    auditAction="Assign Tender Submission Rights to Comapny User";
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                    
                    response.sendRedirect("tenderer/TenderSubRight.jsp?succFlag=y");
                    //response.sendRedirectFILTER("tenderer/TenderSubRight.jsp?succFlag=y");
                }
                else
                {
                    
                    auditAction="Error in Assign Tender Submission Rights to Comapny User";
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                    
                }

            } else {

                             
                String funName = request.getParameter("funName");

                String strPageNo = request.getParameter("pageNo");
                String isApprove = request.getParameter("approve");

                int pageNo = Integer.parseInt(strPageNo);
                String strOffset = request.getParameter("size");
                int recordOffset = Integer.parseInt(strOffset);
                String styleClass = "";

                List<CommonTenderDetails> tenderDetailes = null;
                if (funName.equalsIgnoreCase("AllTenders")) {
                    tenderDetailes = searchAllTenders(request);

                    if (tenderDetailes != null && !tenderDetailes.isEmpty()) {
                        LOGGER.debug("processRequest : Record Size: " + tenderDetailes.size());
                        for (int i = 0; i < tenderDetailes.size(); i++) {
                            if (i % 2 == 0) {
                                styleClass = "bgColor-white";
                            } else {
                                styleClass = "bgColor-Green";
                            }
                            CommonTenderDetails tenderDetail = tenderDetailes.get(i);
                            List<SPCommonSearchData> listTenderRightAssignee = null;

                            listTenderRightAssignee = getTenderRightAssignee(logUserId, tenderDetail.getTenderId().toString());
                            String strTRAssigneeName = "";
                            String strTRAssigneeId="";

                            if(listTenderRightAssignee != null && !listTenderRightAssignee.isEmpty())
                            {
                                strTRAssigneeName = listTenderRightAssignee.get(0).getFieldName1();
                                strTRAssigneeId = listTenderRightAssignee.get(0).getFieldName2();
                            }

                            out.print("<tr class='" + styleClass + "'>");
                            out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * 10 + (i + 1)) + "</td>");
                            out.print("<td class=\"t-align-center\">" + tenderDetail.getTenderId() + ",<br/>" + tenderDetail.getReoiRfpRefNo() + "</td>");
                            //String viewLink = "<a href=\""+request.getContextPath()+"/resources/common/ViewTender.jsp?id=" + tenderDetail.getTenderId() + "\">" + tenderDetail.getTenderBrief() + "</a>";
                            String viewLink = "<a onclick=\"javascript:window.open('" + request.getContextPath() + "/resources/common/ViewTender.jsp?id=" + tenderDetail.getTenderId() + "', '', 'resizable=yes,scrollbars=1','');\" href='#'>" + tenderDetail.getTenderBrief() + "</a>";
                            out.print("<td class=\"t-align-left\">" + tenderDetail.getProcurementNature() + ",<br />" + viewLink + "</td>");

                            StringBuilder deptSb = new StringBuilder();
                            String dept = tenderDetail.getMinistry();
                            deptSb.append(dept + ",<br/> ");
                            dept = tenderDetail.getDivision();
                            if (dept != null && !dept.isEmpty()) {
                                deptSb.append(dept + ",<br/> ");
                            }
                            dept = tenderDetail.getAgency();
                            if (dept != null && !dept.isEmpty()) {
                                deptSb.append(dept + ",<br/> ");
                            }
                            out.print("<td class=\"t-align-left\">" + deptSb.toString() + tenderDetail.getPeOfficeName() + "</td>");
                            out.print("<td class=\"t-align-center\">" + tenderDetail.getProcurementType().replace("T", "B") + ",<br /> " + tenderDetail.getProcurementMethod() + "</td>");
                            Date dt = tenderDetail.getTenderPubDt();
                            String pubDt = "", subDt = "";
                            if (dt != null) {
                                pubDt = DateUtils.gridDateToStr(dt) + "$";
                            } else {
                                pubDt = "N/A";
                            }
                            dt = tenderDetail.getSubmissionDt();
                            if (dt != null) {
                                subDt = DateUtils.gridDateToStr(dt) + "$";
                            } else {
                                subDt = "N/A";
                            }

                            out.print("<td class=\"t-align-center\">" + subDt.substring(0, subDt.lastIndexOf("$") - 3) + "</td>");
                            out.print("<td class=\"t-align-center\">" + strTRAssigneeName + "</td>");
//                            if(strTRAssigneeId.equals(session.getAttribute("userId").toString()))
//                            {
                                out.print("<td class=\"t-align-center\">" + "<a href=AssignTenderSubRight.jsp?tenderId=" + tenderDetail.getTenderId() + "&referenceNo=" + tenderDetail.getReoiRfpRefNo() + "&assigneeId="+strTRAssigneeId+ ">" + "Assign" + "</a> </td>");
//                            }
//                            else
//                            {
//                                out.print("<td class=\"t-align-center\">Already Assigned</td>");
//                            }
                            out.print("</tr>");
                        }
                        int totalPages = 0;
                        if (tenderDetailes.size() > 0) {
                            totalPages = (int) (Math.ceil(Math.ceil(tenderDetailes.get(0).getTotalRecords()) / recordOffset));
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                    } else {
                        out.print("<tr>");

                        out.print("<td colspan=\"7\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        out.print("</tr>");
                    }

                } else if (funName.equalsIgnoreCase("MyTenders")) {
                    tenderDetailes = searchMyTenders(request);
                    Object userTypeid = session.getAttribute("userTypeId");
                    if (tenderDetailes != null && !tenderDetailes.isEmpty()) {
                        LOGGER.debug("processRequest : Record Size: " + tenderDetailes.size());
                        for (int i = 0; i < tenderDetailes.size(); i++) {
                            if (i % 2 == 0) {
                                styleClass = "bgColor-white";
                            } else {
                                styleClass = "bgColor-Green";
                            }
                            CommonTenderDetails tenderDetail = tenderDetailes.get(i);
                            out.print("<tr class='" + styleClass + "'>");
                            out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * 10 + (i + 1)) + "</td>");
                            out.print("<td class=\"t-align-center\">" + tenderDetail.getTenderId() + ",<br/>" + tenderDetail.getReoiRfpRefNo() + "</td>");
                            //String viewLink = "<a href=\""+request.getContextPath()+"/resources/common/ViewTender.jsp?id=" + tenderDetail.getTenderId() + "\">" + tenderDetail.getTenderBrief() + "</a>";
                            String viewLink = "<a onclick=\"javascript:window.open('" + request.getContextPath() + "/resources/common/ViewTender.jsp?id=" + tenderDetail.getTenderId() + "', '', 'resizable=yes,scrollbars=1','');\" href='#'>" + tenderDetail.getTenderBrief() + "</a>";
                            out.print("<td class=\"t-align-left\">" + tenderDetail.getProcurementNature() + ",<br />" + viewLink + "</td>");

                            StringBuilder deptSb = new StringBuilder();
                            String dept = tenderDetail.getMinistry();
                            deptSb.append(dept + ",<br/> ");
                            dept = tenderDetail.getDivision();
                            if (dept != null && !dept.isEmpty()) {
                                deptSb.append(dept + ",<br/> ");
                            }
                            dept = tenderDetail.getAgency();
                            if (dept != null && !dept.isEmpty()) {
                                deptSb.append(dept + ",<br/> ");
                            }
                            out.print("<td class=\"t-align-left\">" + deptSb.toString() + tenderDetail.getPeOfficeName() + "</td>");
                            out.print("<td class=\"t-align-center\">" + tenderDetail.getProcurementType() + ",<br /> " + tenderDetail.getProcurementMethod() + "</td>");
                            StringBuilder dates = new StringBuilder();
                            if (tenderDetail.getTenderPubDt() != null) {
                                dates.append(DateUtils.gridDateToStr(tenderDetail.getTenderPubDt()));
                            } else {
                                dates.append("N/A");
                            }
                            dates.append(" |<br /> ");
                            if (tenderDetail.getSubmissionDt() != null) {
                                dates.append(DateUtils.gridDateToStr(tenderDetail.getSubmissionDt()));
                            } else {
                                dates.append("N/A");
                            }
                            out.print("<td class=\"t-align-center\">" + dates.toString() + "</td>");
                            String dashBoardLink = request.getContextPath() + "/tenderer/TendererDashboard.jsp?tenderid=" + tenderDetail.getTenderId();
                            if (userTypeid.toString().equals("3")) {
                                dashBoardLink = request.getContextPath() + "/officer/TenderDashboard.jsp?tenderid=" + tenderDetail.getTenderId();
                            }
                            String imgLink = request.getContextPath() + "/resources/images/Dashboard/dashBoardIcn.png";
                            out.print("<td class=\"t-align-center\"><a href=\"" + dashBoardLink + "\" title=\"Dashboard\"><img src=\"" + imgLink + "\" alt=\"Dashboard\" /></a></td>");
                            out.print("</tr>");
                        }
                        int totalPages = 1;
                        if (tenderDetailes.size() > 0) {
                            totalPages = (int) (Math.ceil(Math.ceil(tenderDetailes.get(0).getTotalRecords()) / recordOffset));
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                            LOGGER.debug("processRequest : Total Pages: " + totalPages);
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                    } else {
                        out.print("<tr>");
                        out.print("<td colspan=\"7\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        out.print("</tr>");
                    }
                }
            }
            out.flush();
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private List<CommonTenderDetails> searchAllTenders(HttpServletRequest request) {
        String keyword = request.getParameter("keyword");

        String strPageNo = request.getParameter("pageNo");
        int pageNo = Integer.parseInt(strPageNo);
        String strOffset = request.getParameter("size");
        int recordOffset = Integer.parseInt(strOffset);

        TenderService tenderService = (TenderService) AppContext.getSpringBean("TenderService");

        if (keyword != null) {
            //Code Added by 2 null values for district and thana by Proshanto Kumar Saha
            return tenderService.gettenderinformationSP("generalsearch", keyword, 0, 0, 0, 0, "", 0, "", null, null, "Approved", "", "", "", "", null, null, pageNo, recordOffset,null,null);
        } else {
            String strUserId = request.getParameter("userId");
            int userId = 0;
            if (strUserId != null) {
                userId = Integer.parseInt(strUserId);
            }

            String department = request.getParameter("departmentId");
            if (department.contains("Select Organization")) {
                department = "";
            }
            String TenderType = "";
            if(request.getParameter("TenderType")!=null && !request.getParameter("TenderType").equalsIgnoreCase(""))
            {
                TenderType = request.getParameter("TenderType");
            }
            String officeId = request.getParameter("office");
            int tenderId = 0;
            String strTenderId = request.getParameter("tenderId");
            if (strTenderId != null & !strTenderId.equals("")) {
                tenderId = Integer.parseInt(strTenderId);
            }
            String procNature = request.getParameter("procNature");
            int procNatureId = 0;
            if (procNature != null && !procNature.equals("")) {
                procNatureId = Integer.parseInt(procNature);
            }
            String procType = request.getParameter("procType");
            String procMethod = request.getParameter("procMethod");
            int procMethodId = 0;
            if (procMethod != null && !procMethod.equals("")) {
                procMethodId = Integer.parseInt(procMethod);
            }

            String pubDtFrm = null;
            if (request.getParameter("pubDtFrm") != null && !"".equals(request.getParameter("pubDtFrm"))) {
                pubDtFrm = DateUtils.formatStrToStr(request.getParameter("pubDtFrm"));
                if (pubDtFrm.equals("")) {
                    pubDtFrm = null;
                }
            }

            String pubDtTo = null;
            if (request.getParameter("pubDtTo") != null && !"".equals(request.getParameter("pubDtTo"))) {
                pubDtTo = DateUtils.formatStrToStr(request.getParameter("pubDtTo"));
                if (pubDtTo.equals("")) {
                    pubDtTo = null;
                }
            }

            String closeDtFrm = null;
            if (request.getParameter("closeDtFrm") != null && !"".equals(request.getParameter("closeDtFrm"))) {
                closeDtFrm = DateUtils.formatStrToStr(request.getParameter("closeDtFrm"));
                if (closeDtFrm.equals("")) {
                    closeDtFrm = null;
                }
            }

            String closeDtTo = null;
            if (request.getParameter("closeDtTo") != null && !"".equals(request.getParameter("closeDtTo"))) {
                closeDtTo = DateUtils.formatStrToStr(request.getParameter("closeDtTo"));
                if (closeDtTo.equals("")) {
                    closeDtTo = null;
                }
            }

            String cpvcat = request.getParameter("cpvCategory");
            String agency = "";
            if (!"".equals(cpvcat) && cpvcat != null) {
                agency = cpvcat;
            }
            String refNo = request.getParameter("refNo");

            String viewType = request.getParameter("viewType");
            if (viewType.equalsIgnoreCase("Live")) {
                viewType = "Live";
            } else if (viewType.equalsIgnoreCase("Archive")) {
                viewType = "Archive";
            } else if (viewType.equalsIgnoreCase("AllTenders")) {
                viewType = "";
            }
             //Code Added by 2 null values for district and thana by Proshanto Kumar Saha
            //return tenderService.gettenderinformationSP("get rightsalltendersbytype", viewType, tenderId, 0, request.getSession().getAttribute("userId")!=null ? Integer.parseInt(request.getSession().getAttribute("userId").toString()) : 0, procNatureId, procType, procMethodId, refNo, pubDtFrm, pubDtTo, "", department, "", agency, officeId, closeDtFrm, closeDtTo, pageNo, recordOffset,null,null);
            //To show like the "Live" tab from all tenders 30th August, 2016
            if(TenderType.equalsIgnoreCase("Limited"))
            {
                return tenderService.gettenderinformationSP("get tendererlimitedtenders", viewType, tenderId, 0, request.getSession().getAttribute("userId")!=null ? Integer.parseInt(request.getSession().getAttribute("userId").toString()) : 0, procNatureId, procType, procMethodId, refNo, pubDtFrm, pubDtTo, "", department, "", agency, officeId, closeDtFrm, closeDtTo, pageNo, recordOffset,null,null);
            }
            return tenderService.gettenderinformationSP("get alltendersbytype", viewType, tenderId, 0, request.getSession().getAttribute("userId")!=null ? Integer.parseInt(request.getSession().getAttribute("userId").toString()) : 0, procNatureId, procType, procMethodId, refNo, pubDtFrm, pubDtTo, "", department, "", agency, officeId, closeDtFrm, closeDtTo, pageNo, recordOffset,null,null);
        }
    }

    private List<CommonTenderDetails> searchMyTenders(HttpServletRequest request) {

        HttpSession session = request.getSession();

        Object objUserId = session.getAttribute("userId");
        int userId = 0;
        if (objUserId != null) {
            userId = Integer.parseInt(objUserId.toString());
        }

        String action = request.getParameter("action");
        String strPageNo = request.getParameter("pageNo");
        LOGGER.debug("searchMyTenders : Page No: " + strPageNo);
        int pageNo = Integer.parseInt(strPageNo);
        String strOffset = request.getParameter("size");
        int recordOffset = Integer.parseInt(strOffset);
        String status = request.getParameter("status");
        String statusTab = "";
        if (request.getParameter("statusTab") != null) {
            statusTab = request.getParameter("statusTab");
        }

        String officeId = request.getParameter("office");
        int tenderId = 0;
        String strTenderId = request.getParameter("tenderId");
        if (strTenderId != null & !strTenderId.equals("")) {
            tenderId = Integer.parseInt(strTenderId);
        }
        String procNature = request.getParameter("procNature");
        int procNatureId = 0;
        if (procNature != null && !procNature.equals("")) {
            procNatureId = Integer.parseInt(procNature);
        }
        String procType = request.getParameter("procType");
        String procMethod = request.getParameter("procMethod");
        int procMethodId = 0;
        if (procMethod != null && !procMethod.equals("")) {
            procMethodId = Integer.parseInt(procMethod);
        }
        String pubDtFrm = null;

        if (request.getParameter("pubDtFrm") != null && !"".equals(request.getParameter("pubDtFrm"))) {
            pubDtFrm = DateUtils.formatStrToStr(request.getParameter("pubDtFrm"));
            if (pubDtFrm.equals("")) {
                pubDtFrm = null;
            }
        }
        String pubDtTo = null;
        if (request.getParameter("pubDtTo") != null && !"".equals(request.getParameter("pubDtTo"))) {
            pubDtTo = DateUtils.formatStrToStr(request.getParameter("pubDtTo"));
            if (pubDtTo.equals("")) {
                pubDtTo = null;
            }
        }
        String refNo = request.getParameter("refNo");

        TenderService tenderService = (TenderService) AppContext.getSpringBean("TenderService");
         //Code Added by 2 null values for district and thana by Proshanto Kumar Saha
        return tenderService.gettenderinformationSP(action, statusTab, tenderId, 0, userId, procNatureId, procType, procMethodId, refNo, pubDtFrm, pubDtTo, status, "", "", "", "", null, null, pageNo, recordOffset,null,null);
    }

    private void sendMail(String from, String[] mailTo, String strTenderId, String strRefNo, String strProcEntity) {
        LOGGER.debug("sendMail" + " : " + logUserId + " Starts");
        SendMessageUtil sendMessageUtil = new SendMessageUtil();
        MailContentUtility mailContentUtility = new MailContentUtility();
        List list = mailContentUtility.getTenderSubRightsMailContent(strTenderId, strRefNo, strProcEntity);
        String mailSub = list.get(0).toString();
        String mailText = list.get(1).toString();
        sendMessageUtil.setEmailFrom(from);
        sendMessageUtil.setEmailTo(mailTo);
        sendMessageUtil.setEmailSub(mailSub);
        sendMessageUtil.setEmailMessage(mailText);
        sendMessageUtil.sendEmail();
        LOGGER.debug("sendMail" + " : " + logUserId + " Ends");
    }

    private List<SPCommonSearchData> getTenderRightAssignee(String logUserId, String strTenderId) {
        List<SPCommonSearchData> listRightAssignee = null;
        LOGGER.debug("getTenderRightAssignee : " + logUserId + "starts");
        listRightAssignee = tenderRightsServiceImpl.getTenderRightAssignee("getTenderRightsName", logUserId, strTenderId);
        LOGGER.debug("getTenderRightAssignee : " + logUserId + " ends ");
        return listRightAssignee;
    }
}
