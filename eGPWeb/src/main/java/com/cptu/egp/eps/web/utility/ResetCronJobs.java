/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.utility;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
/**
 *
 * @author yagnesh
 */
public class ResetCronJobs extends QuartzJobBean {
    final static Logger log = Logger.getLogger(ResetCronJobs.class);
    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        CommonScheduler cs = new CommonScheduler();
        cs.i = 0;
        ScheduleSMSToApprovers sSmsToA = new ScheduleSMSToApprovers();
        sSmsToA.i = 0;
        SchedulePreTenderMeetingSMS sptmSMS = new SchedulePreTenderMeetingSMS();
        sptmSMS.i = 0;
        IntimationOpenCommittee ioc = new IntimationOpenCommittee();
        ioc.i = 0;
        EvalSchedule es = new EvalSchedule();
        es.i = 0;
        CancelTenderScheduler cts = new CancelTenderScheduler();
        cts.i = 0;
        RssDailyScheduler rds = new RssDailyScheduler();
        rds.i = 0;
        RssWeeklyScheduler rws = new RssWeeklyScheduler();
        rws.i = 0;
        NOAScheduler noaS = new NOAScheduler();
        noaS.i = 0;
        PublishPkgScheduler pps = new PublishPkgScheduler();
        pps.i = 0;
        PublishTenderScheduler pts = new PublishTenderScheduler();
        pts.i = 0;
        MailAfter7DaysIfPaid maSevenDays = new MailAfter7DaysIfPaid();
        maSevenDays.i = 0;
        MailAfter14DaysToBlock maFourTeenDays = new MailAfter14DaysToBlock();
        maFourTeenDays.i = 0;
        MailAfter7DaysIfUnPaid maSevenUnpaid = new MailAfter7DaysIfUnPaid();
        maSevenUnpaid.i = 0;
        DelConfigMarksAfterSubDate dCMA = new DelConfigMarksAfterSubDate();
        dCMA.i = 0;
        FinalSubmissionScheduler fss = new FinalSubmissionScheduler();
        fss.i = 0;
        FileTransferScheduler fts = new FileTransferScheduler();
        fts.i=0;
    }
}
