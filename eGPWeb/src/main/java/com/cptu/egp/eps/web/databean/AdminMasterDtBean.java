/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

import com.cptu.egp.eps.model.table.TblLoginMaster;
// code added by palash
import com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService;
import com.cptu.egp.eps.web.utility.AppContext;
// End
/**
 *
 * @author Administrator
 */
public class AdminMasterDtBean {

    public AdminMasterDtBean() {
    }
    
    private short adminId;
    private int userId;
    private TblLoginMaster tblLoginMaster;
    private String fullName;
    private String nationalId;
    private String mobileNo;
    private String phoneNo;
    private int rollId;
    //Code added by palash, dohatec
    private byte userTypeId;
    private String emailId;
    private String officeName;
// End
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public short getAdminId() {
        return adminId;
    }

    public void setAdminId(short adminId) {
        this.adminId = adminId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public TblLoginMaster getTblLoginMaster() {
        return tblLoginMaster;
    }

    public void setTblLoginMaster(TblLoginMaster tblLoginMaster) {
        this.tblLoginMaster = tblLoginMaster;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public int getRollId() {
        return rollId;
    }

    public void setRollId(int rollId) {
        this.rollId = rollId;
    }
// Code added by palash, dohatec
     /**
     * @return the userTypeId
     */
    public byte getUserTypeId() {
        return userTypeId;
    }

    /**
     * @param userTypeId the userTypeId to set
     */
    public void setUserTypeId(byte userTypeId) {
        this.userTypeId = userTypeId;
    }

    /**
     * @return the emailId
     */
    public String getEmailId() {
        return emailId;
    }
    /**
     * @param emailId the emailId to set
     */
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    /**
     * @return the officeName
     */
    public String getOfficeName() {
        return officeName;
    }

    /**
     * @param officeName the officeName to set
     */
    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }
     public void populateInfo(String...values)
    {
        ScBankDevpartnerService scBankDevpartnerService = (ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");
        String userType = null;
        if(values!=null && values.length!=0){
            userType = values[0];
        }
        Object[] adminDetail = null;
        adminDetail = scBankDevpartnerService.retriveAdminInfo(userId, getUserTypeId(), values);
        if (adminDetail != null) {
            setUserId((Integer) adminDetail[0]);
            setRollId((Integer) adminDetail[1]);
            setPhoneNo(String.valueOf(adminDetail[2]));
            setEmailId(String.valueOf(adminDetail[3]));
            setFullName(String.valueOf(adminDetail[4]));
            setMobileNo(String.valueOf(adminDetail[5]));
            setNationalId(String.valueOf(adminDetail[6]));
            setAdminId((Short) adminDetail[7]);

        }

    }  
}
// End, Palash
