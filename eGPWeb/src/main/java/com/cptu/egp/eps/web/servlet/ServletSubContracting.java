package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.service.serviceinterface.SubContractingService;
import com.cptu.egp.eps.web.servicebean.SubContractingSrbean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *For subcontracting module
 * @author ${Nishith}
 */
public class ServletSubContracting extends HttpServlet {

    /** 
     * this servlet is used for handling request to subcontracting.jsp and ProcessSubContractInv.jsp
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    SubContractingSrbean subContractingSrbean = new SubContractingSrbean();
    TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    SubContractingService subContractingService = (SubContractingService) AppContext.getSpringBean("SubContractingService");
    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
    static final Logger logger = Logger.getLogger(ServletSubContracting.class);
    private String logUserId = "0";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession hs = request.getSession();
        PrintWriter out = response.getWriter();
        if (hs.getAttribute("userId") == null) {
            response.sendRedirect("SessionTimedOut.jsp");
        } else {
            
            logUserId = hs.getAttribute("userId").toString();
            subContractingSrbean.setLogUserId(logUserId);
            tenderCS.setLogUserId(logUserId);
            logger.debug("processRequest : "+logUserId+" Starts");
            String lastAcceptDt = "";
            String remarks = "";
            String errMsg = "";
            String tenderId = "";
            
            String usertype = "";
            String emalVal = "";
            String comName = "";
            String toextTenderRefNo = "";
            String name = "";
            String user = hs.getAttribute("userId") + "";
            String s_lorPkgId = "0";
            String auditAction = "";
            List<Object[]> listMailDtl = new ArrayList<Object[]>();
            String pageName = "tenderer/ProcessSubContractInv.jsp";
            try {
                    if(request.getParameter("funName") != null && "chkMail".equalsIgnoreCase(request.getParameter("funName"))){
                        emalVal = request.getParameter("emailId");
                        int id = Integer.parseInt(request.getParameter("tenderId"));
                        s_lorPkgId = request.getParameter("lorpkgId");
                        emalVal = subContractingService.checkUserEmail(emalVal, id,Integer.parseInt(s_lorPkgId));
                        out.print(emalVal);
                        out.flush();
                        out.close();
                    }else if(request.getParameter("funName") != null && "link".equalsIgnoreCase(request.getParameter("funName"))){
                        String isPackage = request.getParameter("isPackage");
                        tenderId = request.getParameter("tenderid");
                        if("true".equalsIgnoreCase(isPackage)){
                             pageName = "tenderer/SubContracting.jsp?tenderid="+tenderId+"&lorpkgId=0";
                        }else{
                             pageName = "tenderer/LotSelection.jsp?tab=3&tenderId="+tenderId;
                        }
                        response.sendRedirect(pageName);
                    }
                            
                /*This part of if hadling the request of ProcessSubContractInv page*/
                    else if (request.getParameter("counter") != null) {
                        boolean flag = true;
                    logger.debug("For page ProcessSubContractInv : "+logUserId+" Starts");
                    /*Handles request form page ProcessSubContractInv */
                    int counter = Integer.parseInt(request.getParameter("counter"));
                    tenderId = request.getParameter("tenderId");
                    toextTenderRefNo = request.getParameter("tenderRefNo");
                    String status = "";
                    String contractId = "";
                    String txtComments = "";
                        if (request.getParameter("contractId_" + counter) != null) {
                            contractId = request.getParameter("contractId_"+counter);
                        }
                        if (request.getParameter("txtComments_" + counter) == null) {
                            errMsg = "Please Enter Comments";
                            flag = false;
                        }
                        if (request.getParameter("lorpkgId") != null) {
                            s_lorPkgId = request.getParameter("lorpkgId");
                        }
                        if (request.getParameter("comName_"+counter) != null) {
                            comName = request.getParameter("comName_"+counter);
                        }
                        if (flag) {
                            /*For mail*/
                            
                            for (SPTenderCommonData sptcd : tenderCS.returndata("getEmailIdfromUserId", user, null)) {
                                name = sptcd.getFieldName1();
                                if (sptcd != null&&tenderCS!=null) {
                                    sptcd = null;
                                    //tenderCS=null;
                                }
                            }
                            emalVal = request.getParameter("emalVal_" + counter);
                            HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                            txtComments = request.getParameter("txtComments_" + counter);
                            txtComments = handleSpecialChar.handleSpecialChar(txtComments);
                            status = request.getParameter("status_" + counter);
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            String updateData = "invAcceptStatus='" + status + "', invAcceptDate=getDate(), acceptComments='" + txtComments + "'";
                            String whereCondition = "subConId=" + contractId;
                            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                            try {
                                auditAction = "Tenderer "+status+" to Invitation";
                                CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_subContracting", updateData, whereCondition).get(0);
                                if(s_lorPkgId!=null && !"0".equalsIgnoreCase(s_lorPkgId)){
                                    listMailDtl=commonService.getLotDetailsByPkgLotId(s_lorPkgId,tenderId);
                                }
                                flag = subContractingSrbean.sendMail(emalVal, tenderId, toextTenderRefNo, status, comName,listMailDtl);
                                if (commonMsgChk.getFlag() && flag) {
                                    if("approved".equalsIgnoreCase(status)){
                                        errMsg = "accepted";
                                    }else{
                                        errMsg = "rejected";
                                }
                                    
                                }
                                if (commonMsgChk != null || handleSpecialChar != null) {
                                    commonMsgChk = null;
                                    handleSpecialChar = null;
                                }
                            } catch (Exception ex) {
                                logger.error("For page ProcessSubContractInv : "+logUserId+" : "+ex);
                            }finally{
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), Integer.parseInt(tenderId), "tenderId", EgpModule.Sub_Contracting.getName(), auditAction, txtComments);
                            }

                        }
                        
                    if (!flag) {
                            response.sendRedirect(pageName + "?tenderid=" + tenderId + "&errMsg=" + errMsg+"&lorpkgId="+s_lorPkgId);
                        } else {
                            response.sendRedirect(pageName + "?tenderid=" + tenderId + "&errMsg=" + errMsg+"&lorpkgId="+s_lorPkgId);
                        }
                    logger.debug("For page ProcessSubContractInv : "+logUserId+" Ends");
                    /*Ends of if*/
                } else {
                        boolean flag = true;
                    /*handles request form page SubContracting*/
                    logger.debug("For page SubContracting : "+logUserId+" Start");
                    pageName = "tenderer/SubContracting.jsp";
                    if (request.getParameter("usertype") == null) {
                        errMsg = "Please Select Company";
                        flag = false;
                        response.sendRedirect(pageName + "?tenderid=" + tenderId + "&errMsg=" + errMsg);
                    }
                    if (request.getParameter("lastAcceptDt") == null) {
                        errMsg = "Please Select the Date";
                        flag = false;
                        response.sendRedirect(pageName + "?tenderid=" + tenderId + "&errMsg=" + errMsg);

                    }
                    if (request.getParameter("remarks") == null) {
                        errMsg = "Please Enter The Comments";
                        flag = false;
                        response.sendRedirect(pageName + "?tenderid=" + tenderId + "&errMsg=" + errMsg);
                    }
                    if (request.getParameter("emalVal") == null) {
                        flag = false;
                    }
                    if (request.getParameter("comName") == null) {
                        flag = false;
                    }
                    

                    if (flag) {
                        for (SPTenderCommonData sptcd : tenderCS.returndata("getEmailIdfromUserId", user, null)) {
                            name = sptcd.getFieldName1();
                        }
                        emalVal = request.getParameter("emalVal");
                        lastAcceptDt = request.getParameter("lastAcceptDt");
                        comName = request.getParameter("comName");
                        remarks = request.getParameter("remarks");
                        toextTenderRefNo = request.getParameter("tenderRefNo");
                        tenderId = request.getParameter("tenderId");
                        String invAcceptStatus = "Pending";
                        usertype = request.getParameter("usertype");
                        if (request.getParameter("lorpkgId") != null && !"-1".equalsIgnoreCase(request.getParameter("lorpkgId"))) {
                            s_lorPkgId = request.getParameter("lorpkgId");
                        }
                        HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                        remarks = handleSpecialChar.handleSpecialChar(remarks);
                        SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
                        SimpleDateFormat sd1 = new SimpleDateFormat("yyyy-MM-dd");
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        try {
                            lastAcceptDt = sd1.format(sd.parse(lastAcceptDt));
                        } catch (ParseException ex) {
                            logger.error("For page SubContracting : "+logUserId+" : "+ex);
                        }
                        auditAction = "Invitation for Subcontracting";
                        String xmldata = "";
                        xmldata = "<tbl_SubContracting tenderId=\"" + tenderId + "\" invToUserId=\"" + usertype + "\" invFromUserId=\"" + hs.getAttribute("userId") + "\" remarks=\"" + remarks + "\" lastAcceptDt=\"" + lastAcceptDt + "\" invSentDt=\"" + format.format(new Date()) + "\" invAcceptStatus=\"" + invAcceptStatus + "\" invAcceptDate=\"\" acceptComments=\"\" pkgLotId=\""+s_lorPkgId + "\"/>";
//                                xmldata = "<tbl_SubContracting tenderId=\"" + tenderId + "\" invToUserId=\"" + usertype + "\" invFromUserId=\"" + hs.getAttribute("userId") + "\" remarks=\"" + remarks + "\" lastAcceptDt=\"" + lastAcceptDt + "\" invSentDt=\"" + format.format(new Date()) + "\" invAcceptStatus=\"\" invAcceptDate=\"\" acceptComments=\"\" />";
                        xmldata = "<root>" + xmldata + "</root>";
                        CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                        CommonMsgChk commonMsgChk;
                        try {

                            commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_SubContracting", xmldata, "").get(0);
                            if(s_lorPkgId!=null && !"0".equalsIgnoreCase(s_lorPkgId)){
                            listMailDtl=commonService.getLotDetailsByPkgLotId(s_lorPkgId,tenderId);
                            }
                            flag = subContractingSrbean.sendMail(emalVal, comName, tenderId, toextTenderRefNo, listMailDtl);
                           
                            if (commonMsgChk.getId() != null && flag) {
                                errMsg = "Ok";
                            }
                            if (commonMsgChk != null || handleSpecialChar != null) {
                                commonMsgChk = null;
                                handleSpecialChar = null;
                            }
                        } catch (Exception ex) {
                           logger.error("For page SubContracting : "+logUserId+" : "+ex);
                           auditAction = "Error in "+auditAction+" "+ex.getMessage();
                        }finally{
                            
                            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), Integer.parseInt(tenderId), "tenderId", EgpModule.Sub_Contracting.getName(), auditAction, remarks);
                        }
                        response.sendRedirect(pageName + "?tenderid=" + tenderId + "&errMsg=" + errMsg+"&lorpkgId="+s_lorPkgId);


                    }
                    logger.debug("For page SubContracting : "+logUserId+" Ends");
                    /*Ends of Else condition*/
                }
            } finally {
                out.close();
             listMailDtl.clear();
             listMailDtl=null;
             auditAction = null;

            }
            logger.debug("For page ProcessReqeust : "+logUserId+" Ends ");
        }
    }
    // <editor-fold defaultstate="collapsed" desc="${servletEditorFold}">

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);




    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);




    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";


    }// </editor-fold>
}
