/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblEvalNomination;
import com.cptu.egp.eps.service.serviceinterface.EvalNominationApprovalService;
import com.cptu.egp.eps.web.utility.AppContext;
import org.apache.log4j.Logger;
/**
 *
 * @author Swati
 */
public class EvalNominationApprovalSrBean {
    
    private EvalNominationApprovalService evalNOminationsevice=(EvalNominationApprovalService)AppContext.getSpringBean("evalNominationApprovalService");

    String logUserId = "0";

    Logger logger = Logger.getLogger(EvalNominationApprovalSrBean.class);

    public void setLogUserId(String logUserId) {
        evalNOminationsevice.setUserId(logUserId);
    }

    public String addNominationApproval(TblEvalNomination evalNomination){
        logger.debug("addNominationApproval "+logUserId+" Starts:");
        String val = "";
        try{
            val =  evalNOminationsevice.addTblEvalNomination(evalNomination);
        }catch(Exception e){
            logger.error("addNominationApproval "+logUserId+" :"+e);
        }
        logger.debug("addNominationApproval "+logUserId+" Ends:");
        return val;
    }

}
