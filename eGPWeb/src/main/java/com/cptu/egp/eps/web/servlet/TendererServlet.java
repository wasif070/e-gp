/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.web.utility.AppContext;
import org.apache.log4j.Logger;
import java.util.List;
import com.cptu.egp.eps.web.databean.MapTendererDtBean;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Ahsan
 */
public class TendererServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException, JSONException {
        //response.setContentType("text/html;charset=UTF-8");
         response.setContentType("application/Json");
        PrintWriter out = response.getWriter();
        try {
            

            String comCountry = "",comState="",comCity="",comThana="",corCountry="",corState="",corCity="",corThana="";
            String perCountry="",perState = "", perCity = "", perThana = "";
            comCountry = request.getParameter("comCountry");
            comState = request.getParameter("comState");
            comCity = request.getParameter("comCity");
            comThana = request.getParameter("comThana");
            corCountry = request.getParameter("corCountry");
            corState = request.getParameter("corState");
            corCity = request.getParameter("corCity");
            corThana = request.getParameter("corThana");
            perCountry = request.getParameter("perCountry");
            perState = request.getParameter("perState");
            perCity = request.getParameter("perCity");
            perThana = request.getParameter("perThana");
            
            TenderCommonService tenderService = (TenderCommonService)AppContext.getSpringBean("TenderCommonService");
           
            StringBuilder searchString = new StringBuilder("");

            if( comCountry !=null && !comCountry.isEmpty())
                searchString.append(" and cm.regOffCountry like '%"+ comCountry +"%'");
            if( comState !=null && !comState.isEmpty()){
                if(comState.contains("'"))
                    comState = comState.replace("'", "''");
                searchString.append(" and cm.regOffState like '%"+ comState +"%'");
            }
            if( comCity !=null && !comCity.isEmpty()){
                if(comCity.contains("'"))
                    comCity = comCity.replace("'", "''");
                searchString.append(" and cm.regOffCity like '%"+ comCity +"%'");
            }
            if( comThana !=null && !comThana.isEmpty()){
                if(comThana.contains("'"))
                    comThana = comThana.replace("'", "''");
                searchString.append(" and cm.regOffUpjilla like '%"+ comThana +"%'");
            }

            if( corCountry !=null && !corCountry.isEmpty())
                searchString.append(" and cm.corpOffCountry like '%"+ corCountry +"%'");
            if( corState !=null && !corState.isEmpty()){
                if(corState.contains("'"))
                    corState = corState.replace("'", "''");
                searchString.append(" and cm.corpOffState like '%"+ corState +"%'");
            }
            if( corCity !=null && !corCity.isEmpty()){
                if(corCity.contains("'"))
                    corCity = corCity.replace("'", "''");
                searchString.append(" and cm.corpOffCity like '%"+ corCity +"%'");
            }
            if( corThana !=null && !corThana.isEmpty()){
                if(corThana.contains("'"))
                    corThana = corThana.replace("'", "''");
                searchString.append(" and cm.corpOffUpjilla like '%"+ corThana +"%'");
            }

            if( perCountry !=null && !perCountry.isEmpty())
                searchString.append(" and tm.country like '%"+ perCountry +"%'");
            if( perState !=null && !perState.isEmpty()){
                if(perState.contains("'"))
                    perState = perState.replace("'", "''");
                searchString.append(" and tm.state like '%"+ perState +"%'");
            }
            if( perCity !=null && !perCity.isEmpty()){
                if(perCity.contains("'"))
                    perCity = perCity.replace("'", "''");
                searchString.append(" and tm.city like '%"+ perCity +"%'");
            }
            if( perThana !=null && !perThana.isEmpty()){
                if(perThana.contains("'"))
                    perThana = perThana.replace("'", "''");
                searchString.append(" and tm.upJilla like '%"+ perThana +"%'");
            }
             
            //if(searchString.length()>0)
               // searchString = new StringBuilder(searchString.substring(0, searchString.length()-5));
             
             List<Object []> getTendererinfo = tenderService.getAllTendererInfo(searchString.toString());

             String strGrid = null;
             strGrid = "{ aaData: [";

             strGrid = "{ iTotalRecords: " + getTendererinfo.size() + ", iTotalDisplayRecords: " + getTendererinfo.size() + ", aaData: [";
                if(!getTendererinfo.isEmpty())
                {
                    for(int i =0;i<getTendererinfo.size();i++)
                    {
                    Object[] details = getTendererinfo.get(i);
                    strGrid = strGrid + "[";
                    //strGrid = strGrid + "\"" + gridDataTable.Rows[row]["ID"].ToString() + "\",";

                    strGrid = strGrid + "\"" + (i+1) + "\",";
                    strGrid = strGrid + "\"" + details[0].toString() + "\",";
                    strGrid = strGrid + "\"" + details[1].toString()+ "\",";
                    strGrid = strGrid + "\"" + details[2].toString()+ "\",";
                    strGrid = strGrid + "\"" + details[3].toString()+ "\",";
                    strGrid = strGrid + "\"" + details[4].toString() + "\",";
                    strGrid = strGrid + "\"" + details[5].toString()+ "\",";
                    strGrid = strGrid + "\"" + details[6].toString()+ "\",";
                   
                    strGrid = strGrid + "]";

                    if (i < getTendererinfo.size() - 1)
                        strGrid = strGrid + ",";
                    }
                }
                 strGrid = strGrid + "] }";

                  JSONObject jsonResponse = new JSONObject(strGrid);
                  response.getWriter().print(jsonResponse.toString());



        } finally { 
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            java.util.logging.Logger.getLogger(TendererServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            java.util.logging.Logger.getLogger(TendererServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
