/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblMultiLangContent;
import com.cptu.egp.eps.service.serviceimpl.MultiLingualService;
import com.cptu.egp.eps.web.servicebean.LanguageContentSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */

//Search Formation changed by Emtaz on 24/May/2016

public class LanguageGrid extends HttpServlet {

    
    MultiLingualService multilingservice = (MultiLingualService) AppContext.getSpringBean("MultiLingualService");
    private static final Logger LOGGER = Logger.getLogger(LanguageGrid.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND = "Ends";
    private String logUserId = "0";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * Servlet used for fet data for Language grid.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            HttpSession session = request.getSession();
            if (session.getAttribute("userId") != null) {
                logUserId = session.getAttribute("userId").toString();
                multilingservice.setLogUserId(logUserId);
            }
            LOGGER.debug("processRequest : " + logUserId + LOGGERSTART);
            String sord = "asc";
            String sidx = "contentId";


            //<editor-fold>
            /*
            if ("fetchData".equals(request.getParameter("action"))) {
                LOGGER.debug("processRequest : action : fetchData : " + logUserId + LOGGERSTART);
                response.setContentType("text/xml;charset=UTF-8");

                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                if(_search){
                    sidx = "";
                }
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");

                if (request.getParameter("sord") != null) {
                    sord = request.getParameter("sord");
                }
                if (request.getParameter("sidx") != null && !"".equalsIgnoreCase(request.getParameter("sidx"))) {
                    sidx = request.getParameter("sidx");
                }


                LanguageContentSrBean languageContentSrBean = new LanguageContentSrBean();
                languageContentSrBean.setSearch(_search);
                languageContentSrBean.setLimit(Integer.parseInt(rows));
                int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));

                languageContentSrBean.setOffset(offset);
                languageContentSrBean.setGetSortOrder(sord);
                languageContentSrBean.setSortCol(sidx);


                String searchField = "", searchString = "", searchOper = "";
                List<TblMultiLangContent> listmultilang = null;
                List<Object[]> list = null;
                if (_search) {

                    searchField = request.getParameter("searchField");
                    searchString = request.getParameter("searchString");
                    searchOper = request.getParameter("searchOper");


                    list = languageContentSrBean.getSearchData(searchField, searchString, searchOper);

                } else {

                    listmultilang = multilingservice.getLanguageData(offset, Integer.parseInt(rows), sidx, sord);
                }

                int totalPages = 0;
                int totalCount = 0;
                if (_search) {
                    totalCount = (int) languageContentSrBean.getSearchCount(searchField, searchString, searchOper);
                } else {
                    totalCount = (int) multilingservice.getTotalCount();
                }

                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(rows) == 0) {
                        totalPages = totalCount / Integer.parseInt(rows);
                    } else {
                        totalPages = (totalCount / Integer.parseInt(rows)) + 1;
                    }
                } else {
                    totalPages = 0;
                }

                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");
                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + totalCount + "</records>");
                int srNo = offset + 1;
                String lang = null;

                if (_search) {
                    for (int i = 0; i < list.size(); i++) {

                        if ("en_US".equals(list.get(i)[3])) {
                            lang = "English";
                        } else if ("bn_IN".equals(list.get(i)[3])) {
                            lang = "Bangla";
                        }

                        out.print("<row id='" + i + "'>");
                        out.print("<cell>" + srNo + "</cell>");
                        out.print("<cell><![CDATA[" + list.get(i)[1] + "]]></cell>");
                        out.print("<cell><![CDATA[" + list.get(i)[2] + "]]></cell>");
                        out.print("<cell><![CDATA[" + lang + "]]></cell>");
                        out.print("<cell><![CDATA[<a href='LanguageContent.jsp?action=Edit&id=" + list.get(i)[0] + "'>Edit</a>&nbsp;|&nbsp;<a href='LanguageContent.jsp?action=View&id=" + list.get(i)[0] + "'>View</a>]]></cell>");
                        out.print("</row>");

                        srNo++;
                    }
                } else {

                    for (int i = 0; i < listmultilang.size(); i++) {

                        if ("en_US".equals(listmultilang.get(i).getLanguage())) {
                            lang = "English";
                        } else if ("bn_IN".equals(listmultilang.get(i).getLanguage())) {
                            lang = "Bangla";
                        }

                        out.print("<row id='" + i + "'>");
                        out.print("<cell>" + srNo + "</cell>");
                        out.print("<cell><![CDATA[" + listmultilang.get(i).getTitle() + "]]></cell>");
                        out.print("<cell><![CDATA[" + listmultilang.get(i).getDisplayTitle() + "]]></cell>");
                        out.print("<cell><![CDATA[" + lang + "]]></cell>");
                        out.print("<cell><![CDATA[<a href='LanguageContent.jsp?action=Edit&id=" + listmultilang.get(i).getContentId() + "'>Edit</a>&nbsp;|&nbsp;<a href='LanguageContent.jsp?action=View&id=" + listmultilang.get(i).getContentId() + "'>View</a>]]></cell>");
                        out.print("</row>");

                        srNo++;
                    }
                }
                out.print("</rows>");
            }*/
            //</editor-fold>
            if ("fetchData".equals(request.getParameter("action"))) {
                LOGGER.debug("processRequest : action : fetchData : " + logUserId + LOGGERSTART);
                //response.setContentType("text/xml;charset=UTF-8");
                int pageNo = Integer.parseInt(request.getParameter("pageNo"));
                int Size = Integer.parseInt(request.getParameter("size"));


                LanguageContentSrBean languageContentSrBean = new LanguageContentSrBean();
                //languageContentSrBean.setSearch(_search);
                languageContentSrBean.setLimit(1000000);
                int offset = 0;
                languageContentSrBean.setOffset(offset);
                
                String Title = request.getParameter("Title");
                String Language = request.getParameter("Language");
                if(Language.equalsIgnoreCase(""))
                {
                    Language = "";
                }
                else if("English".toLowerCase().contains(Language.toLowerCase()))
                {
                    Language = "en_US";
                }
                else if("Dzongkha".toLowerCase().contains(Language.toLowerCase()))
                {
                    Language = "bn_IN";
                }
                
//                else if(Language.equalsIgnoreCase("Bangla"))
//                {
//                    Language = "bn_IN";
//                }
              
                List<TblMultiLangContent> listmultilang = null;
                List<TblMultiLangContent> listmultilangSearched = new ArrayList<TblMultiLangContent>();
               

                listmultilang = multilingservice.getLanguageData(offset, 100000, "", "");
                //List<Object[]> list = null;
                for(int j=0;j<listmultilang.size();j++)
                {
                    boolean ToAdd = true;
                    if(Title!=null && !Title.equalsIgnoreCase(""))
                    {
                        if(!listmultilang.get(j).getDisplayTitle().toString().toLowerCase().contains(Title.toLowerCase()))
                        {
                            ToAdd = false;
                        }
                    }
                    if(Language!=null && !Language.equalsIgnoreCase(""))
                    {
                        if(!Language.equalsIgnoreCase(listmultilang.get(j).getLanguage().toString()))
                        {
                            ToAdd = false;
                        }
                    }
                   
                    if(ToAdd)
                    {
                        //SPCommonSearchData commonAppData = getGovData.get(j);
                        listmultilangSearched.add(listmultilang.get(j));
                    }
                }
                int RecordFrom = (pageNo-1)*Size;
                int k= 0;
                String styleClass = "";
                if (listmultilangSearched != null && !listmultilangSearched.isEmpty()) {
                    for(k=RecordFrom;k<RecordFrom+Size && k<listmultilangSearched.size();k++)
                    {
                        if(k%2==0){
                            styleClass = "bgColor-white";
                        }else{
                            styleClass = "bgColor-Green";
                        }
                        out.print("<tr class='"+styleClass+"'>");
                        out.print("<td width=\"5%\" class=\"t-align-center\">" + (k+1) + "</td>");
                        out.print("<td width=\"20%\" class=\"t-align-center\">" + listmultilangSearched.get(k).getTitle().toString() + "</td>");
                        out.print("<td width=\"40%\" class=\"t-align-center\">" + listmultilangSearched.get(k).getDisplayTitle().toString() + "</td>");
                        String lang="";
                        if ("en_US".equals(listmultilangSearched.get(k).getLanguage())) {
                        lang = "English";
                        } else if ("bn_IN".equals(listmultilangSearched.get(k).getLanguage())) {
                            lang = "Dzongkha";
                        }
                        out.print("<td width=\"15%\" class=\"t-align-center\">" + lang + "</td>");
                        String link = "<a href='LanguageContent.jsp?action=Edit&id=" + listmultilangSearched.get(k).getContentId() + "'>Edit</a>&nbsp;|&nbsp;<a href='LanguageContent.jsp?action=View&id=" + listmultilangSearched.get(k).getContentId() + "'>View</a>";
                        out.print("<td width=\"20%\" class=\"t-align-center\">" + link + "</td>");
                        out.print("</tr>");
                    }

                }
                else
                {
                    out.print("<tr>");
                    out.print("<td colspan=\"5\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }  
                int totalPages = 1;
                if (listmultilangSearched.size() > 0) {
                    totalPages = (int) (Math.ceil(Math.ceil(listmultilangSearched.size()) / Size));
                    System.out.print("totalPages--"+totalPages+"records "+ listmultilangSearched.size());
                    if (totalPages == 0) {
                        totalPages = 1;
                    }
                }
                out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                    
                /*for (int i = 0; i < listmultilang.size(); i++) {

                    if ("en_US".equals(listmultilang.get(i).getLanguage())) {
                        lang = "English";
                    } else if ("bn_IN".equals(listmultilang.get(i).getLanguage())) {
                        lang = "Bangla";
                    }

                    out.print("<row id='" + i + "'>");
                    out.print("<cell>" + srNo + "</cell>");
                    out.print("<cell><![CDATA[" + listmultilang.get(i).getTitle() + "]]></cell>");
                    out.print("<cell><![CDATA[" + listmultilang.get(i).getDisplayTitle() + "]]></cell>");
                    out.print("<cell><![CDATA[" + lang + "]]></cell>");
                    out.print("<cell><![CDATA[<a href='LanguageContent.jsp?action=Edit&id=" + listmultilang.get(i).getContentId() + "'>Edit</a>&nbsp;|&nbsp;<a href='LanguageContent.jsp?action=View&id=" + listmultilang.get(i).getContentId() + "'>View</a>]]></cell>");
                    out.print("</row>");

                    srNo++;
                }
                
                out.print("</rows>");*/
            }
            LOGGER.debug("processRequest : action : fetchData : " + logUserId + LOGGEREND);
        } catch (Exception ex) {
            LOGGER.error("processRequest " + logUserId + " : " + ex.toString());
        } finally {
            out.close();
        }
        LOGGER.debug("processRequest  : " + logUserId + LOGGEREND);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
