/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.List;
import com.cptu.egp.eps.dao.storedprocedure.ContractAwardOfflineDetails;
import com.cptu.egp.eps.service.serviceinterface.ContractAwardOfflineService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.DateUtils;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Ahsan
 */
public class viewContractAwardOfflineServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException, JSONException {
       // response.setContentType("text/html;charset=UTF-8");
        response.setContentType("application/Json");
        HttpSession session = request.getSession();
        Object userTypeid = session.getAttribute("userTypeId");
        PrintWriter out = response.getWriter();
        try {
            System.gc();
            if(request.getParameter("action").equals("Delete"))
            {
            boolean isdeleted;
            String refNo = request.getParameter("contractNo");
            ContractAwardOfflineService offlineService = (ContractAwardOfflineService) AppContext.getSpringBean("ContractAwardOfflineService");
            isdeleted = offlineService.deleteContractAwardOfflineData(refNo);
            //deleteInvoiceDocsDetails

            if(isdeleted == true)
                {
                 response.sendRedirect("offlinedata/SearchAwardedContractOffline.jsp");
                //JSONObject jsonResponse = new JSONObject(BindGrid("false","Pending","","","","","","","",request.getContextPath(),userTypeid.toString()));
               // response.getWriter().print(jsonResponse.toString());
                }

            }

            if(request.getParameter("action").equals("bindGrid"))
            {
                //response.

                String advDateFrom="";
                String advDateTo="";
                String status = request.getParameter("status");
                String procNature = request.getParameter("procNature");
                String procMethod = request.getParameter("procMethod");
                String searchFlag = request.getParameter("searchFlag");
                String refNo = request.getParameter("refNo");
                String value = request.getParameter("value");
                String ministry = request.getParameter("ministry");
                if(!request.getParameter("advDateFrom").equals("") && request.getParameter("advDateFrom") != null)
                    advDateFrom = DateUtils.formatStrToStr(request.getParameter("advDateFrom"));
                if(!request.getParameter("advDateTo").equals("") && request.getParameter("advDateTo") != null)
                    advDateTo = DateUtils.formatStrToStr(request.getParameter("advDateTo"));
               /* String procNature = "";
                String procMethod = "";
                String searchFlag = "false";
                String refNo = "";
                String value = "";
                String advDateFrom = "";
                String advDateTo = "";
                String status = "Pending";*/

                JSONObject jsonResponse = new JSONObject(BindGrid(searchFlag,status,procNature,procMethod,refNo,value,advDateFrom,advDateTo,ministry,request.getContextPath(),userTypeid.toString()));
                response.getWriter().print(jsonResponse.toString());
               // JsonObject jsonResponse = new JsonObject();

               // out.print(BindGrid());
            }
            if(request.getParameter("action").equals("bindGridForHome"))
            {
                //response.

                String advDateFrom="";
                String advDateTo="";
                String status = request.getParameter("status");
                String procNature = request.getParameter("procNature");
                String procMethod = request.getParameter("procMethod");
                String searchFlag = request.getParameter("searchFlag");
                String refNo = request.getParameter("refNo");
                String value = request.getParameter("value");
                String ministry = request.getParameter("ministry");
                if(!request.getParameter("advDateFrom").equals("") && request.getParameter("advDateFrom") != null)
                    advDateFrom = DateUtils.formatStrToStr(request.getParameter("advDateFrom"));
                if(!request.getParameter("advDateTo").equals("") && request.getParameter("advDateTo") != null)
                    advDateTo = DateUtils.formatStrToStr(request.getParameter("advDateTo"));
               /* String procNature = "";
                String procMethod = "";
                String searchFlag = "false";
                String refNo = "";
                String value = "";
                String advDateFrom = "";
                String advDateTo = "";
                String status = "Pending";*/

                JSONObject jsonResponse = new JSONObject(BindGridForHome(searchFlag,status,procNature,procMethod,refNo,value,advDateFrom,advDateTo,ministry,request.getContextPath()));
                response.getWriter().print(jsonResponse.toString());
               // JsonObject jsonResponse = new JsonObject();

               // out.print(BindGrid());
            }
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet viewContractAwardOfflineServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet viewContractAwardOfflineServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(viewContractAwardOfflineServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(viewContractAwardOfflineServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

        public String BindGridForHome(String searchFlag,String status,String procNature,String procMethod,String refNo,String value,String advDateFrom,String advDateTo,String ministry,String contextPath)
       //public String BindGrid(String searchFlag,String status,String procNature,String procMethod)
        {

                //context.Response.ContentType = "json";
                String strGrid = null;
                String type = "";
                strGrid = "{ aaData: [";

                List<ContractAwardOfflineDetails> offlineDetailsList = null;
                ContractAwardOfflineService offlineService = (ContractAwardOfflineService) AppContext.getSpringBean("ContractAwardOfflineService");
                offlineDetailsList = offlineService.getContractAwardOfflineData(searchFlag,status,procNature,procMethod,refNo,value,advDateFrom,advDateTo,ministry,"search","");

                int ab = offlineDetailsList.size();

               // strGrid = "{ aaData: [";
                strGrid = "{ iTotalRecords: " + offlineDetailsList.size() + ", iTotalDisplayRecords: " + offlineDetailsList.size() + ", aaData: [";
                if(!offlineDetailsList.isEmpty())
                {
                    for(int i =0;i<offlineDetailsList.size();i++)
                    {
                    ContractAwardOfflineDetails offlineDetails = offlineDetailsList.get(i);
                    strGrid = strGrid + "[";
                    //strGrid = strGrid + "\"" + gridDataTable.Rows[row]["ID"].ToString() + "\",";

                    strGrid = strGrid + "\"" + (i+1) + "\",";
                    strGrid = strGrid + "\"" + offlineDetails.getMinistry().toString()+"<br> "+ offlineDetails.getDivision().toString()+ "\",";
                    strGrid = strGrid + "\"" + "<a href='"+contextPath+"/resources/common/viewAwardedContractOffline.jsp?contractNo="+offlineDetails.getContarctAIdOL()+"&approve=false' target='_blank'> "+offlineDetails.getRefNo()+" </a><br>" +offlineDetails.getDescriptionofContract()+"<br>"+DateUtils.customDateFormate(offlineDetails.getDateofAdvertisement())+ "\",";
                   // strGrid = strGrid + "\"" + "<a href='CreateREOI.jsp' onclick='window.open('CreateREOI.jsp','popup','width=500,height=500,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false'>About</a>" +"<br/>"+ DateUtils.customDateFormate(offlineDetails.getDateofAdvertisement()) + "\",";
                    strGrid = strGrid + "\"" + offlineDetails.getPeCode()+"<br>"+ offlineDetails.getProcurementMethod()+ "\",";
                    strGrid = strGrid + "\"" + offlineDetails.getPeDistrict()+ "\",";
                    strGrid = strGrid + "\"" + DateUtils.customDateFormate(offlineDetails.getDateofNOA()) + "\",";
                    strGrid = strGrid + "\"" + offlineDetails.getNameofTenderer() + "\",";
                    strGrid = strGrid + "\"" + offlineDetails.getContractValue().toString() + "\",";
                    if(status.equals("Approved"))
                        strGrid = strGrid + "\"" + "<a href='"+contextPath+"/resources/common/viewAwardedContractOffline.jsp?contractNo="+offlineDetails.getContarctAIdOL()+"&approve=false' target='_blank'>View</a><br>" + "\",";

                    strGrid = strGrid + "]";

                    if (i < offlineDetailsList.size() - 1)
                        strGrid = strGrid + ",";
                    }
                }
                 strGrid = strGrid + "] }";

                return strGrid;

            }
         public String BindGrid(String searchFlag,String status,String procNature,String procMethod,String refNo,String value,String advDateFrom,String advDateTo,String ministry,String contextPath,String userTypeId)
       //public String BindGrid(String searchFlag,String status,String procNature,String procMethod)
        {

                //context.Response.ContentType = "json";
                String strGrid = null;
                String type = "";
                strGrid = "{ aaData: [";

                List<ContractAwardOfflineDetails> offlineDetailsList = null;
                ContractAwardOfflineService offlineService = (ContractAwardOfflineService) AppContext.getSpringBean("ContractAwardOfflineService");
                offlineDetailsList = offlineService.getContractAwardOfflineData(searchFlag,status,procNature,procMethod,refNo,value,advDateFrom,advDateTo,ministry,"search","");

                int ab = offlineDetailsList.size();

               // strGrid = "{ aaData: [";
                strGrid = "{ iTotalRecords: " + offlineDetailsList.size() + ", iTotalDisplayRecords: " + offlineDetailsList.size() + ", aaData: [";
                if(!offlineDetailsList.isEmpty())
                {
                    for(int i =0;i<offlineDetailsList.size();i++)
                    {
                    ContractAwardOfflineDetails offlineDetails = offlineDetailsList.get(i);
                    strGrid = strGrid + "[";
                    //strGrid = strGrid + "\"" + gridDataTable.Rows[row]["ID"].ToString() + "\",";

                    strGrid = strGrid + "\"" + (i+1) + "\",";
                    strGrid = strGrid + "\"" + offlineDetails.getMinistry().toString()+"<br> "+ offlineDetails.getDivision().toString()+ "\",";
                    strGrid = strGrid + "\"" + "<a href='"+contextPath+"/offlinedata/viewAwardedContractOffline.jsp?contractNo="+offlineDetails.getContarctAIdOL()+"&approve=false' target='_blank'> "+offlineDetails.getRefNo()+" </a><br>" +offlineDetails.getDescriptionofContract()+"<br>"+DateUtils.customDateFormate(offlineDetails.getDateofAdvertisement())+ "\",";
                   // strGrid = strGrid + "\"" + "<a href='CreateREOI.jsp' onclick='window.open('CreateREOI.jsp','popup','width=500,height=500,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false'>About</a>" +"<br/>"+ DateUtils.customDateFormate(offlineDetails.getDateofAdvertisement()) + "\",";
                    strGrid = strGrid + "\"" + offlineDetails.getPeCode()+"<br>"+ offlineDetails.getProcurementMethod()+ "\",";
                    strGrid = strGrid + "\"" + offlineDetails.getPeDistrict()+ "\",";
                    strGrid = strGrid + "\"" + DateUtils.customDateFormate(offlineDetails.getDateofNOA()) + "\",";
                    strGrid = strGrid + "\"" + offlineDetails.getNameofTenderer() + "\",";
                    strGrid = strGrid + "\"" + offlineDetails.getContractValue().toString() + "\",";
                    if(status.equals("Approved"))
                        strGrid = strGrid + "\"" + "<a href='"+contextPath+"/offlinedata/viewAwardedContractOffline.jsp?contractNo="+offlineDetails.getContarctAIdOL()+"&approve=false' target='_blank'>View</a><br>" + "\",";
                    else
                        if(userTypeId.equals("19"))
                            strGrid = strGrid + "\"" + "<a href='"+contextPath+"/offlinedata/viewAwardedContractOffline.jsp?contractNo="+offlineDetails.getContarctAIdOL()+"&approve=false' target='_blank'>View</a> | "+"<a href='"+contextPath+"/offlinedata/EditContractAward.jsp?action=Edit&awardOLId="+offlineDetails.getContarctAIdOL()+"'>Edit</a> | "+"<a href='"+contextPath+"/viewContractAwardOfflineServlet?action=Delete&contractNo="+offlineDetails.getContarctAIdOL()+"' onclick='return confirmation()'>Delete</a> | "+"<a href='"+contextPath+"/offlinedata/viewAwardedContractOffline.jsp?contractNo="+offlineDetails.getContarctAIdOL()+"&approve=true'>Approve</a>"+ "\" ";
                        else
                        {
                            strGrid = strGrid + "\"" + "<a href='"+contextPath+"/offlinedata/viewAwardedContractOffline.jsp?contractNo="+offlineDetails.getContarctAIdOL()+"&approve=false' target='_blank'>View</a> | "+"<a href='"+contextPath+"/offlinedata/EditContractAward.jsp?action=Edit&awardOLId="+offlineDetails.getContarctAIdOL()+"'>Edit</a> | "+"<a href='"+contextPath+"/viewContractAwardOfflineServlet?action=Delete&contractNo="+offlineDetails.getContarctAIdOL()+"' onclick='return confirmation()'>Delete</a>"+"\" ";
                        }
                    strGrid = strGrid + "]";

                    if (i < offlineDetailsList.size() - 1)
                        strGrid = strGrid + ",";
                    }
                }
                 strGrid = strGrid + "] }";

                return strGrid;

            }
}
