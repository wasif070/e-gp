/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;
import java.util.regex.Pattern;
/**
 *
 * @author Administrator
 */
public class CommonUtils {

    public static String checkNull(String value) {

        if (value == null || value.equals("null")) {
            return "";
        } else {
            return value;
        }
    }
        public static String removeAllSpecialchars(String value){
        if(value==null || "null".equals(value)){
            return "";
        }
        return value.replaceAll("[^a-zA-Z0-9 &./-]+", "").replace("\\'", "").replace("\"","");
    }

    public static String removeXSSAndSpecialChars(String value){
        return cleanXSS(removeAllSpecialchars(value));
    }

   public static String cleanXSS(String paramString)
  {
    if (paramString == null) {
      return "";
    }
    String str = paramString;
  //  str = str.replaceAll("\000", "");
    Pattern localPattern = Pattern.compile("<script>(.*?)</script>", 2);
    str = localPattern.matcher(str).replaceAll("");
    localPattern = Pattern.compile("src[\r\n]*=[\r\n]*\\'(.*?)\\'", 42);
    str = localPattern.matcher(str).replaceAll("");
    localPattern = Pattern.compile("src[\r\n]*=[\r\n]*\\\"(.*?)\\\"", 42);
    str = localPattern.matcher(str).replaceAll("");
    localPattern = Pattern.compile("</script>", 2);
    str = localPattern.matcher(str).replaceAll("");
    localPattern = Pattern.compile("<script(.*?)>", 42);
    str = localPattern.matcher(str).replaceAll("");
    localPattern = Pattern.compile("eval\\((.*?)\\)", 42);
    str = localPattern.matcher(str).replaceAll("");
    localPattern = Pattern.compile("expression\\((.*?)\\)", 42);
    str = localPattern.matcher(str).replaceAll("");
    localPattern = Pattern.compile("javascript:", 2);
    str = localPattern.matcher(str).replaceAll("");
    localPattern = Pattern.compile("vbscript:", 2);
    str = localPattern.matcher(str).replaceAll("");
    localPattern = Pattern.compile("onload(.*?)=", 42);
    str = localPattern.matcher(str).replaceAll("");
    str = str.replaceAll("\\(", "&#40;").replaceAll("\\)", "&#41;");
    str = str.replaceAll("'","&#39;" );
    str = str.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
    return str;
  }
}