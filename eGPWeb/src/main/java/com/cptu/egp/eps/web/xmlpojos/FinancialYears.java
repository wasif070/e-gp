/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.xmlpojos;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Chalapathi.Bavisetti
 */
public class FinancialYears {
     private int financialYearId;
     private String financialYear;
     private String financialYearCurrent;

    /**
     * @return the financialYearId
     */
      @XmlElement(nillable = true)
    public int getFinancialYearId() {
        return financialYearId;
    }

    /**
     * @param financialYearId the financialYearId to set
     */
    public void setFinancialYearId(int financialYearId) {
        this.financialYearId = financialYearId;
    }

    /**
     * @return the financialYear
     */
     @XmlElement(nillable = true)
    public String getFinancialYear() {
        return financialYear;
    }

    /**
     * @param financialYear the financialYear to set
     */
    public void setFinancialYear(String financialYear) {
        this.financialYear = financialYear;
    }
     /**
     * @return the financialYear
     */
    // Added by Sudhir 09072011 To identify current Financial Year.
     @XmlElement(nillable = true)
    public String getFinancialYearCurrent() {
        return financialYearCurrent;
    }

    /**
     * @param financialYear the financialYear to set
     */
    public void setFinancialYearCurrent(String financialYearCurrent) {
        this.financialYearCurrent = financialYearCurrent;
    }
}
