/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;
import com.cptu.egp.eps.dao.storedprocedure.TenderDashboardOfflineDetails;
import com.cptu.egp.eps.service.serviceinterface.TenderDashboardOfflineService;
import com.cptu.egp.eps.service.serviceimpl.TenderDashboardOfflineServiceImpl;

import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import com.cptu.egp.eps.web.utility.CommonUtils;


/**
 *
 * @author Istiak
 */
public class TenderDashboardOfflineSrBean {

    private String logUserId = "0";

    private static final Logger LOGGER = Logger.getLogger(TenderDashboardOfflineSrBean.class);

    TenderDashboardOfflineService offlineService = (TenderDashboardOfflineService) AppContext.getSpringBean("TenderDashboardOfflineService");
       
    public List<TenderDashboardOfflineDetails> getTenderDashboardOfflineDetails(String id) throws Exception {   // Tender Information

        List<TenderDashboardOfflineDetails> tenderDashboardOfflineList = new ArrayList<TenderDashboardOfflineDetails>();
        try {
            for (TenderDashboardOfflineDetails tenderDashboardOfflineDetails : offlineService.getTenderDashboardOfflineData("view", "", "", "", "", id, "", "", "", "")) {
                tenderDashboardOfflineList.add(tenderDashboardOfflineDetails);
            }

        } catch (Exception e) {
            LOGGER.error("getTenderDashboardOfflineDetails : " + e);
        }
        LOGGER.debug("getTenderDashboardOfflineDetails : " + logUserId + "ends");
        return tenderDashboardOfflineList;
    }

    public boolean approveTenderOffline(String id, String comments) // Tender Approval
    {
        boolean isApproved = false;
        try {
            if(!comments.equals(""))
            {
              isApproved = offlineService.approveTenderCORDashboardOfflineData("tenderapprove", id, comments);
            }

        } catch (Exception e) {
            LOGGER.error("ApproveTenderOffline : " + e);
        }
        LOGGER.debug("Approve Tender Offline Data : " + logUserId + "ends");
        return isApproved;
    }

    public boolean approveCOROffline(String id, String comments) // Corrigendum Approval
    {
        boolean isApproved = false;
        try {
            if(!comments.equals(""))
            {
              isApproved = offlineService.approveTenderCORDashboardOfflineData("corapprove", id, comments);
            }

        } catch (Exception e) {
            LOGGER.error("ApproveCorrigendumOffline : " + e);
        }
        LOGGER.debug("Approve Corrigendum Offline Data : " + logUserId + "ends");
        return isApproved;
    }

    public String lotInfo(String id, String tenderFlag) // Lot Information for Tender
    {
        String table="";

        try {
            String moduleFlag = "viewlot";

            List<TenderDashboardOfflineDetails> offlineLotList = null;
            offlineLotList = offlineService.getCORLotInfo(moduleFlag, id);

            if(!offlineLotList.isEmpty())
            {
                table = "<div id='gridContainer'>";
                table = table + "<table id='dataTable' class='tableList_1 t_space' width='100%' cellspacing='0'>";
                table = table + "<tbody>";
                table = table + "<tr style='text-align: center'>";

                if(tenderFlag.equals("reoi"))   //  REOI
                {
                    table = table + "<th class='t-align-center' width='6%'>Ref. No. </th>";
                }
                else
                {
                    table = table + "<th class='t-align-center' width='6%'>Lot No.</th>";
                }
                
                table = table + "<th class='t-align-center' width='40%'>Identification of Lot</th>";
                table = table + "<th class='t-align-center' width='10%'>Location </th>";

                if(tenderFlag.equals("reoi"))   //  REOI
                {
                    table = table + "<th class='t-align-center'>Indicative Start<br> Date </th>";
                    table = table + "<th class='t-align-center'>Indicative Completion<br> Date</th>";
                }
                else if(tenderFlag.equals("wpq"))   //  Without PQ
                {
                    table = table + "<th class='t-align-center'>Tender Security (Amount in Nu.)</th>";
                    table = table + "<th class='t-align-center'>Completion Time in Weeks/Months</th>";
                }
                else
                {
                    table = table + "<th class='t-align-center' width='10%'>Completion Date</th>";
                }
                table = table + "</tr>";

                for(int i =0;i<offlineLotList.size();i++)
                {
                    TenderDashboardOfflineDetails offlineLotDetails = offlineLotList.get(i);

                    if((!offlineLotDetails.getLotOrRefNo().isEmpty() || offlineLotDetails.getLotOrRefNo()!=null) ||
                            (!offlineLotDetails.getLotIdentOrPhasingServ().isEmpty() || offlineLotDetails.getLotIdentOrPhasingServ()!=null) ||
                            (!offlineLotDetails.getLocation().isEmpty() || offlineLotDetails.getLocation()!=null) ||
                            (!offlineLotDetails.getCompletionDateTime().isEmpty() || offlineLotDetails.getCompletionDateTime()!=null))
                    {
                        table = table + "<tr>";
                        table = table + "<td class='t-align-center'>"+ CommonUtils.checkNull(offlineLotDetails.getLotOrRefNo()) +"</td>";
                        table = table + "<td class='t-align-center'>"+ CommonUtils.checkNull(offlineLotDetails.getLotIdentOrPhasingServ()).replaceAll("[\\r\\n]", "").trim() +"</td>";
                        table = table + "<td class='t-align-center'>"+ CommonUtils.checkNull(offlineLotDetails.getLocation()) +"</td>";

                        if(tenderFlag.equals("reoi"))   //  REOI
                        {
                            table = table + "<td class='t-align-center'>"+ CommonUtils.checkNull(offlineLotDetails.getStartDateTime()) +"</td>";
                        }
                        else if(tenderFlag.equals("wpq"))   //  Without PQ
                        {
                            table = table + "<td class='t-align-center'>"+ CommonUtils.checkNull(offlineLotDetails.getTenderSecurityAmt().toString()) +"</td>";
                        }

                        table = table + "<td class='t-align-center'>"+ CommonUtils.checkNull(offlineLotDetails.getCompletionDateTime()) +"</td>";
                        table = table + "</tr>";
                    }
                }

                table = table + "</tbody>";
                table = table + "</table>";
            }
        } catch (Exception e) {
            LOGGER.error("Lot Table : " + e);
        }

        LOGGER.debug("Lot Table : " + logUserId + "ends");
        
        return table;
    }

    public String corInfo(String id, String moduleName)   // Corrigendum Information for Tender
    {
        String moduleFlag = "viewcorrigendum";
        String corTable = "";

        try {

            List<TenderDashboardOfflineDetails> offlineCORList = null;
            offlineCORList = offlineService.getCORLotInfo(moduleFlag, id);

            if(!offlineCORList.isEmpty())
            {
                corTable = "<br/><div id='divHead' class='tableHead_1 t_space' style='background-color: rgb(242, 219, 219); color: rgb(148, 54, 52); line-height: 20px; border: 1px solid rgb(148, 54, 52); font-family: arial,verdana; font-size: 13px; display: block;'>Corrigendum Detail</div>";
               
                int corNo = 0;
                String corStatus = "";

                for(int i = 0; i<offlineCORList.size(); i++)
                {
                    TenderDashboardOfflineDetails offlineCORDetails = offlineCORList.get(i);                    

                    if(i > 0)
                    {
                        if(corNo != offlineCORDetails.getNumberOfCOR())
                        {
                            corTable = corTable + corTableStartEnd("end", "0");
                            if(offlineCORDetails.getCorrigendumStatus().equals("Pending"))
                            {
                                corTable = corTable + corTableStartEnd("start", offlineCORDetails.getCorrigendumStatus());
                            }
                            else
                            {
                                corTable = corTable + corTableStartEnd("start", offlineCORDetails.getNumberOfCOR().toString());
                            }
                        }
                        else
                        {
                            if(!corStatus.equals(offlineCORDetails.getCorrigendumStatus()))
                            {
                                corTable = corTable + corTableStartEnd("end", "0");
                                if(offlineCORDetails.getCorrigendumStatus().equals("Pending"))
                                {
                                    corTable = corTable + corTableStartEnd("start", offlineCORDetails.getCorrigendumStatus());
                                }
                                else
                                {
                                    corTable = corTable + corTableStartEnd("start", offlineCORDetails.getNumberOfCOR().toString());
                                }
                            }
                        }
                    }
                    else
                    {
                        if(offlineCORDetails.getCorrigendumStatus().equals("Pending"))
                        {
                            corTable = corTable + corTableStartEnd("start", offlineCORDetails.getCorrigendumStatus());
                        }
                        else
                        {
                            corTable = corTable + corTableStartEnd("start", offlineCORDetails.getNumberOfCOR().toString());
                        }
                    }

                    corTable = corTable + "<tr>";
                    corTable = corTable + "<td class='t-align-center'>" + corFieldName(offlineCORDetails.getFieldName(), moduleName) + "</td>";
                    corTable = corTable + "<td class='t-align-center'>" + valueCheck(offlineCORDetails.getOldValue()).replaceAll("[\\r\\n]", "").trim() + "</td>";
                    corTable = corTable + "<td class='t-align-center'>" + valueCheck(offlineCORDetails.getNewValue()).replaceAll("[\\r\\n]", "").trim() + "</td>";
                    corTable = corTable + "</tr>";
                    corNo = offlineCORDetails.getNumberOfCOR();
                    corStatus = offlineCORDetails.getCorrigendumStatus();
                }

                corTable = corTable + corTableStartEnd("end", "0");
                
            }
        } catch (Exception e) {
            LOGGER.error("COR Table : " + e);
        }

        LOGGER.debug("COR Table : " + logUserId + "ends");

        return corTable;
    }

    public String corTableStartEnd(String position, String corNo)
    {
        String corTable = "";

        if(position.equals("start"))
        {
            corTable = corTable + "<table class='tableView_1' width='100%' cellspacing='10'>";
            corTable = corTable + "<tbody><tr><td class='ff' width='100%' colspan='2'>";
            corTable = corTable + "<div class='tableHead_1 t_space' style='background-color:#f2dbdb;color: #943634; line-height:10px;border:1px solid #943634;font-family: arial, verdana; font-size: 13px; width: 25%;'> Corrigendum No. : " + corNo + "</div>";
            corTable = corTable + "</td></tr></tbody></table>";

            corTable = corTable + "<table class='tableList_1 t_space' width='100%' cellspacing='0'><tbody><tr>";
            corTable = corTable + "<th class='t-align-center' width='34%'>Field Name</th>";
            corTable = corTable + "<th class='t-align-center' width='33%'>Old Value</th>";
            corTable = corTable + "<th class='t-align-center' width='33%'>New Value</th>";
            corTable = corTable + "</tr>";
        }
        else
        {
            corTable = corTable + "</tbody></table>";
        }

        return corTable;
    }

    public String corFieldName(String fieldName, String moduleName)
    {
        if(moduleName.equals("reoi"))
        {
            return fieldName;
        }
        else
        {
            if(fieldName.equalsIgnoreCase("procurementNature"))
            {
                return "Procurement Nature";
            }
            else if(fieldName.equalsIgnoreCase("procurementType"))
            {
                return "Procurement Type";
            }
            else if(fieldName.equalsIgnoreCase("invitationFor"))
            {
                return "Invitation For";
            }
            else if(fieldName.equalsIgnoreCase("reoiRfpRefNo"))
            {
                return "Invitation Reference No.";
            }
            else if(fieldName.equalsIgnoreCase("IssueDate"))
            {
                return "Date";
            }
            else if(fieldName.equalsIgnoreCase("procurementMethod"))
            {
                return "Procurement Method";
            }
            else if(fieldName.equalsIgnoreCase("budgetType"))
            {
                return "Budget Type";
            }
            else if(fieldName.equalsIgnoreCase("sourceOfFund"))
            {
                return "Source Of Funds";
            }
            else if(fieldName.equalsIgnoreCase("devPartners"))
            {
                return "Development Partner";
            }
            else if(fieldName.equalsIgnoreCase("projectCode"))
            {
                return "Project Code";
            }
            else if(fieldName.equalsIgnoreCase("projectName"))
            {
                return "Project Name";
            }
            else if(fieldName.equalsIgnoreCase("packageNo"))
            {
                return "Tender Package No.";
            }
            else if(fieldName.equalsIgnoreCase("PackageName"))
            {
                return "Tender Package Name";
            }
            else if(fieldName.equalsIgnoreCase("SellingAddPrinciple"))
            {
                return "Selling Pre-qualification Document (Principle)";
            }
            else if(fieldName.equalsIgnoreCase("EligibilityCriteria"))
            {
                return "Eligibility of Bidder/Consultant";//feroz, 21st April, 2016
            }
            else if(fieldName.equalsIgnoreCase("BriefDescription"))
            {
                return "Brief Description of Goods or Works";
            }
            else if(fieldName.equalsIgnoreCase("Lot Number"))
            {
                return "Lot No.";
            }
            else if(fieldName.equalsIgnoreCase("Lot Identification"))
            {
                return "Identification of Lot";
            }
            else if(fieldName.equalsIgnoreCase("Location"))
            {
                return "Location";
            }
            else if(fieldName.equalsIgnoreCase("Completion Time"))
            {
                return "Completion Time in weeks/months";
            }
            else
            {
                if(moduleName.equals("tender"))
                {
                    if(fieldName.equalsIgnoreCase("eventType"))
                    {
                        return "Event Type";
                    }
                    else if(fieldName.equalsIgnoreCase("tenderPubDate"))
                    {
                        return "Tender Last Selling Date";
                    }
                    else if(fieldName.equalsIgnoreCase("ClosingDate"))
                    {
                        return "Tender  Closing Date and Time";
                    }
                    else if(fieldName.equalsIgnoreCase("OpeningDate"))
                    {
                        return "Tender Opening Date and Time";
                    }
                    else if(fieldName.equalsIgnoreCase("SellingAddOthers"))
                    {
                        return "Selling Tender Document(Others)";
                    }
                    else if(fieldName.equalsIgnoreCase("ReceivingAdd"))
                    {
                        return "Receiving Tender Document";
                    }
                    else if(fieldName.equalsIgnoreCase("OpeningAdd"))
                    {
                        return "Opening Tender Document";
                    }
                    else if(fieldName.equalsIgnoreCase("PreTenderREOIPlace"))
                    {
                        return "Place of Pre-Tender Meeting";
                    }
                    else if(fieldName.equalsIgnoreCase("preTenderREOIDate"))
                    {
                        return "Date and Time of Pre-Tender Meeting";
                    }
                    else if(fieldName.equalsIgnoreCase("RelServicesOrDeliverables"))
                    {
                        return "Brief Description of Services";
                    }
                    else if(fieldName.equalsIgnoreCase("DocumentPrice"))
                    {
                        return "Tender Document Price";
                    }
                    else if(fieldName.equalsIgnoreCase("Bid Security"))
                    {
                        return "Tender Security";
                    }
                    else if(fieldName.equalsIgnoreCase("peName"))
                    {
                        return "Name of Official Inviting  Tender";
                    }
                    else if(fieldName.equalsIgnoreCase("peAddress"))
                    {
                        return "Address of Official Inviting Tender";
                    }
                    else if(fieldName.equalsIgnoreCase("peContactDetails"))
                    {
                        return "Contact details of Official Inviting  Tender";
                    }
                    else if(fieldName.equalsIgnoreCase("peDesignation"))
                    {
                        return "Designation of Official Inviting  Tender";
                    }
                }
                else    //  Module = PQ
                {
                    if(fieldName.equalsIgnoreCase("tenderPubDate"))
                    {
                        return "Scheduled Pre-Qualification Publication Date";
                    }
                    else if(fieldName.equalsIgnoreCase("ClosingDate"))
                    {
                        return "Pre-Qualification Closing Date and Time";
                    }
                    else if(fieldName.equalsIgnoreCase("PreTenderREOIPlace"))
                    {
                        return "Place of Pre - Qualification Meeting";
                    }
                    else if(fieldName.equalsIgnoreCase("preTenderREOIDate"))
                    {
                        return "Pre - Qualification Meeting Date and Time";
                    }
                    else if(fieldName.equalsIgnoreCase("SellingAddOthers"))
                    {
                        return "Selling Pre-qualification Document (Others)";
                    }
                    else if(fieldName.equalsIgnoreCase("ReceivingAdd"))
                    {
                        return "Receiving Pre-qualification";
                    }
                    else if(fieldName.equalsIgnoreCase("DocumentPrice"))
                    {
                        return "Pre-Qualification Document Price";
                    }
                    else if(fieldName.equalsIgnoreCase("peName"))
                    {
                        return "Name of Official Inviting  Pre-Qualification";
                    }
                    else if(fieldName.equalsIgnoreCase("peContactDetails"))
                    {
                        return "Contact details of Official Inviting  Pre-Qualification";
                    }
                    else if(fieldName.equalsIgnoreCase("RelServicesOrDeliverables"))
                    {
                        return "Brief Description of Related Services";
                    }
                    else if(fieldName.equalsIgnoreCase("peDesignation"))
                    {
                        return "Designation of Official Inviting  Pre-Qualification";
                    }
                    else if(fieldName.equalsIgnoreCase("peAddress"))
                    {
                        return "Address of Official Inviting Pre-Qualification";
                    }
                }
            }
        }

        return fieldName;
    }

    public String valueCheck(String value)
    {
        if(value.indexOf("Lump") > -1)
        {
            value = "Lump – sum";
        }
        return value;
    }
    public List<Object[]> getMinistryForTenderOffline() throws Exception {
        List<Object[]> ministryList = null;
        LOGGER.debug("getMinistryList : " + logUserId + "starts");
        try {
            ministryList =  offlineService.getMinistryForTenderOffline();
        } catch (Exception e) {
            LOGGER.error("getMinistryList : " + e);
        }

        LOGGER.debug("getMinistryList : " + logUserId + "ends");
        return ministryList;
    }

      public List<Object[]> getOfflineTenderOrganizationByMinistry(String ministry) {
       List<Object[]> orgList = null;
       try {
         orgList = offlineService.getOrgForTenderOffline(ministry);
        } catch (Exception e) {
            LOGGER.error("getOrganization : " + logUserId + e);
        }
        return orgList;
    }
}
