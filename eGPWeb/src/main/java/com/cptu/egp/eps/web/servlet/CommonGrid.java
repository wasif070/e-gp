/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;


import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.web.servicebean.DepartmentCreationSrBean;
import com.cptu.egp.eps.web.servicebean.ManageEmployeeGridSrBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author rishita
 */
public class CommonGrid extends HttpServlet {
   
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
        
        try {
            if(request.getParameter("action").equals("fetchData")){
                response.setContentType("text/xml;charset=UTF-8");
                //q=1&action=fetchData&_search=false&nd=1288106238111&rows=10&page=1&sidx=&sord=asc
                ManageEmployeeGridSrBean manageEmployeeGridSrBean =  new ManageEmployeeGridSrBean();
                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String sord = request.getParameter("sord");
                String sidx = request.getParameter("sidx");
                if(sidx.equals("")){
                    sidx = "departmentId";
                }
                
                //dcBean.setColName("");
                manageEmployeeGridSrBean.setSearch(_search);
                manageEmployeeGridSrBean.setLimit(Integer.parseInt(rows));
                int offset = ((Integer.parseInt(page)-1)*Integer.parseInt(rows));
                System.out.println("offset:" + +offset);
                manageEmployeeGridSrBean.setOffset(offset);
                manageEmployeeGridSrBean.setSortOrder(sord);
                manageEmployeeGridSrBean.setSortCol(sidx);
                
                
                PrintWriter out = response.getWriter();
                System.out.println("queryString:" + request.getQueryString());
                
                List<TblDepartmentMaster> departmentMasterList = manageEmployeeGridSrBean.getDepartmentMasterList("");
                System.out.println("departmentMasterList size:" + departmentMasterList.size());
                int totalPages = 0;
                int totalCount = (int) manageEmployeeGridSrBean.getAllCountOfDept("");//parameter change
                if(totalCount > 0 ) {
                    if(totalCount % Integer.parseInt(request.getParameter("rows")) == 0){
                        totalPages = totalCount/Integer.parseInt(request.getParameter("rows"));
                    }else{
                        totalPages = (totalCount/Integer.parseInt(request.getParameter("rows"))) + 1;
                    }
                    
                } else {
                    totalPages = 0;
                }

                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");
                
                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + departmentMasterList.size() + "</records>");
                // be sure to put text data in CDATA
                for(int i=0;i<departmentMasterList.size();i++){
                    out.print("<row id='" + departmentMasterList.get(i).getDepartmentId() + "'>");
                    out.print("<cell>"+ departmentMasterList.get(i).getDepartmentId() +"</cell>");
                    out.print("<cell>"+ departmentMasterList.get(i).getDepartmentName() +"</cell>");
                    out.print("<cell>" + departmentMasterList.get(i).getAddress() + "</cell>");
                    out.print("<cell>" + departmentMasterList.get(i).getCreatedDate() + "</cell>");
                    out.print("</row>");
                }
                out.print("</rows>");
            }
            
        }catch(Exception ex){
            System.out.println("Exceotion :" + ex);
        }finally {
            //out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
