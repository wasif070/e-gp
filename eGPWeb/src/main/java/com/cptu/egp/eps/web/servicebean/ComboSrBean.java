/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblListBoxDetail;
import com.cptu.egp.eps.model.table.TblListBoxMaster;
import com.cptu.egp.eps.model.table.TblTenderListDetail;
import com.cptu.egp.eps.service.serviceinterface.CreateComboService;
import com.cptu.egp.eps.service.serviceinterface.TendererComboService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class ComboSrBean {

    private final CreateComboService createcomboservice = (CreateComboService) AppContext.getSpringBean("CreateComboService");
    private final TendererComboService tenderercomboservice = (TendererComboService) AppContext.getSpringBean("TendererComboService");
    private static final Logger LOGGER = Logger.getLogger(ComboSrBean.class);
    private static final String LOGGERStart = " Starts";
    private static final String LOGGEREnd = " Ends";
    private String logUserId = "0";

    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        createcomboservice.setLogUserId(logUserId);
        tenderercomboservice.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /**
     * Get List Box Master by form Id.
     * @param formId
     * @return
     */
    public List<TblListBoxMaster> getListBoxMaster(int formId) {
        LOGGER.debug("getListBoxMaster : " + logUserId + LOGGERStart);
        List<TblListBoxMaster> list = new ArrayList<TblListBoxMaster>();
        try {
        list = createcomboservice.getListBoxMaster(formId);
        } catch (Exception e) {
            LOGGER.error("getListBoxMaster : " + logUserId,e);
        }
        LOGGER.debug("getListBoxMaster : " + logUserId + LOGGEREnd);
        return list;
    }

    /**
     * Get ListBox Detail by list box master Id.
     * @param id
     * @return
     */
    public List<TblListBoxDetail> getListBoxDetail(int id) {
        LOGGER.debug("getListBoxDetail : " + logUserId + LOGGERStart);
        List<TblListBoxDetail> list = new ArrayList<TblListBoxDetail>();
        try {
        list = createcomboservice.getListBoxDetail(id);
        } catch (Exception e) {
            LOGGER.error("getListBoxDetail : " + logUserId,e);
        }
        LOGGER.debug("getListBoxDetail : " + logUserId + LOGGEREnd);
        return list;
    }

    public List<TblTenderListDetail> getListBoxDetailFromTenderer(int id) {
        LOGGER.debug("getListBoxDetailFromTenderer : " + logUserId + LOGGERStart);
        List<TblTenderListDetail> list = new ArrayList<TblTenderListDetail>();
        try {
        list = tenderercomboservice.getListBoxDetailFromTenderer(id);
        } catch (Exception e) {
            LOGGER.error("getListBoxDetailFromTenderer : " + logUserId,e);
        }
        LOGGER.debug("getListBoxDetailFromTenderer : " + logUserId + LOGGEREnd);
        return list;
    }

    /**
     * Get Combo Box Category and form wise in String format
     * @param formId
     * @param Category
     * @return
     */
    public String getStringListBoxCatWise(int formId, String Category) {
        LOGGER.debug("getStringListBoxCatWise : " + logUserId + LOGGERStart);
        List<TblListBoxMaster> listBoxMasters = new ArrayList<TblListBoxMaster>();
        StringBuilder strCombo = new StringBuilder();
        try {
            listBoxMasters = createcomboservice.getListBoxCatWise(formId, Category);
            strCombo.append("<option value=''>--Select--</option>");
            if (!listBoxMasters.isEmpty()) {
                for (TblListBoxMaster tblListBoxMaster : listBoxMasters) {
                    strCombo.append("<option value='" + tblListBoxMaster.getListBoxId() + "'>" + tblListBoxMaster.getListBoxName() + "</option>");
                }
            }
        } catch (Exception ex) {
            LOGGER.error("getStringListBoxCatWise : " + logUserId,ex);
        }
        LOGGER.debug("getStringListBoxCatWise : " + logUserId + LOGGEREnd);
        return strCombo.toString();
    }

    /**
     * Get List Box Category Wise
     * @param formId
     * @param Category
     * @return
     */
    public List<Object[]> getListBoxCatWise(int formId, String Category) {
        LOGGER.debug("getListBoxCatWise : " + logUserId + LOGGERStart);

        List<TblListBoxMaster> listBoxMasters = new ArrayList<TblListBoxMaster>();
        List<Object[]> listComboObj = new ArrayList<Object[]>();

        StringBuilder strCombo = new StringBuilder();
        try {
            listBoxMasters = createcomboservice.getListBoxCatWise(formId, Category);
            Object[] cmbText = new Object[listBoxMasters.size() + 1];
            Object[] cmbValue = new Object[listBoxMasters.size() + 1];

            cmbText[0] = "--Select--";
            cmbValue[0] = "0";

            if (!listBoxMasters.isEmpty()) {
                int i = 1;
                for (TblListBoxMaster tblListBoxMaster : listBoxMasters) {
                    cmbText[i] = tblListBoxMaster.getListBoxName();
                    cmbValue[i] = tblListBoxMaster.getListBoxId();
                    strCombo.append("<option value='" + tblListBoxMaster.getListBoxId() + "'>" + tblListBoxMaster.getListBoxName() + "</option>");
                    i++;
            }
            }
            listComboObj.add(cmbText);
            listComboObj.add(cmbValue);
        } catch (Exception ex) {
            LOGGER.error("getListBoxCatWise : " + logUserId,ex);
        }
        LOGGER.debug("getListBoxCatWise : " + logUserId + LOGGEREnd);
        return listComboObj;
    }
}
