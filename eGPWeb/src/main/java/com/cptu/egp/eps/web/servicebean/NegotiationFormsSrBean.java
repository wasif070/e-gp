/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.service.serviceimpl.NegotiationFormsImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class NegotiationFormsSrBean {

    final Logger logger = Logger.getLogger(NegotiationFormsSrBean.class);
    private String logUserId ="0";

    NegotiationFormsImpl negotiationFormsImpl = (NegotiationFormsImpl) AppContext.getSpringBean("NegotiationFormsImpl");

    /**
     * Set user Id at Bean Level.
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        negotiationFormsImpl.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }
    /**
     * check if current form is selected for negotiation or not.
     * @param formId
     * @param negId
     * @return true if selected else false
     */
    public boolean isSelectedForm(int formId, int negId){
        logger.debug("isSelectedForm : "+logUserId+" Starts");
        boolean flag = false;
        try{
            flag = negotiationFormsImpl.isSelectedForm(formId,negId);
        }catch(Exception ex){
            logger.error("isSelectedForm : "+logUserId+" : "+ex.toString());
    }
        logger.debug("isSelectedForm : "+logUserId+" Ends");
        return flag;
    }

}
