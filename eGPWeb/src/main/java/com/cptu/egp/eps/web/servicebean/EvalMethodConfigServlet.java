/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblConfigEvalMethod;
import com.cptu.egp.eps.service.serviceimpl.EvalMethodConfigService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author TaherT
 */
public class EvalMethodConfigServlet extends HttpServlet {


    private static final Logger LOGGER = Logger.getLogger(EvalMethodConfigServlet.class);
    private String logUserId = "0";



    
    private final EvalMethodConfigService configService = (EvalMethodConfigService) AppContext.getSpringBean("EvalMethodConfigService");

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LOGGER.debug("processRequest "+logUserId+" : Starts");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
            configService.setLogUserId(logUserId);
            configService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
        }
        try {
            boolean flag = false;
            String action = request.getParameter("action");
            if ("evalInsert".equals(action)) {
                flag = insertConfigEvalMethod(request.getParameterValues("procNature"), request.getParameterValues("tendType"), request.getParameterValues("procMethod"), request.getParameterValues("noEnv"), request.getParameterValues("evalMethod"));
                if (flag) {
                    response.sendRedirect("admin/ViewEvalMethodConfiguration.jsp?msg=insucc");
                }else{
                    response.sendRedirect("admin/EvalMethodConfiguration.jsp?msg=fail");
                }
            }else if ("evalUpdate".equals(action)) {
                flag = updateConfigEvalMethod(request.getParameterValues("procNature"), request.getParameterValues("tendType"), request.getParameterValues("procMethod"), request.getParameterValues("noEnv"), request.getParameterValues("evalMethod"));
                if (flag) {
                    response.sendRedirect("admin/ViewEvalMethodConfiguration.jsp?msg=updsucc");
                }else {
                    response.sendRedirect("admin/EvalMethodConfiguration.jsp?isedit=y&msg=fail");
                }
            }else if ("delSingleEval".equals(action)) {
                int i = delSingleEval(request.getParameter("evalId"));
                if(i>0){
                    response.sendRedirect("admin/ViewEvalMethodConfiguration.jsp?msg=delsucc");
                }else{
                    response.sendRedirect("admin/ViewEvalMethodConfiguration.jsp?msg=delfail");
                }
            }else if ("editSingleEval".equals(action)) {                
                flag = editSingleEval(new TblConfigEvalMethod(Integer.parseInt(request.getParameter("evalId")), Integer.parseInt(request.getParameter("procMethod")), Integer.parseInt(request.getParameter("tendType")), Integer.parseInt(request.getParameter("procNature")), Integer.parseInt(request.getParameter("noEnv")), Integer.parseInt(request.getParameter("evalMethod"))));
                if(flag)
                {
                    response.sendRedirect("admin/ViewEvalMethodConfiguration.jsp?msg=editsucc");
                }else{
                    response.sendRedirect("admin/ViewEvalMethodConfiguration.jsp?msg=editfail");
                }
            }else if ("ajaxUniqueConfig".equals(action)) {
                out.print(ajaxUniqueConfig(request.getParameter("evalId"),request.getParameter("procNature"),request.getParameter("tendType"),request.getParameter("procMethod")));
                out.flush();
            }

        }catch(Exception e){
            LOGGER.error("processRequest :"+e);
        } finally {
            out.close();
        }

        LOGGER.debug("processRequest "+logUserId+" : Ends");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private boolean insertConfigEvalMethod(String[] procNature, String[] tendType, String[] procMethod, String[] noEnv, String[] evalMethod) {
        LOGGER.debug("insertConfigEvalMethod "+logUserId+" Starts");
        List<TblConfigEvalMethod> list = new ArrayList<TblConfigEvalMethod>();
        try{
        for (int i = 0; i < noEnv.length; i++) {
            list.add(new TblConfigEvalMethod(0, Integer.parseInt(procMethod[i]), Integer.parseInt(tendType[i]), Integer.parseInt(procNature[i]), Integer.parseInt(noEnv[i]), Integer.parseInt(evalMethod[i])));
        }
        }catch(Exception e){
            LOGGER.debug("insertConfigEvalMethod "+logUserId+" :"+e);
        }
        LOGGER.debug("insertConfigEvalMethod "+logUserId+" Ends");
        return configService.insertConfigEvalMethod(list);
    }

    private boolean updateConfigEvalMethod(String[] procNature, String[] tendType, String[] procMethod, String[] noEnv, String[] evalMethod) {
        LOGGER.debug("insertConfigEvalMethod "+logUserId+" Starts");
        List<TblConfigEvalMethod> list = new ArrayList<TblConfigEvalMethod>();
        try{
        for (int i = 0; i < noEnv.length; i++) {
            list.add(new TblConfigEvalMethod(0, Integer.parseInt(procMethod[i]), Integer.parseInt(tendType[i]), Integer.parseInt(procNature[i]), Integer.parseInt(noEnv[i]), Integer.parseInt(evalMethod[i])));
        }
        }catch(Exception e){
                LOGGER.error("insertConfigEvalMethod "+logUserId+" :"+e);
            }
            LOGGER.debug("insertConfigEvalMethod "+logUserId+"  Ends");
        return configService.updateConfigEvalMethod(list);
    }
    private int delSingleEval(String evalId){
        LOGGER.debug("delSingleEval "+logUserId+" Starts");
        int cnt = 0;
        try{
            cnt =  configService.deleteSingleConfigEvalMethod(evalId);
        }catch(Exception e){
            LOGGER.error("delSingleEval "+logUserId+" :"+e);
    }
        LOGGER.debug("delSingleEval "+logUserId+" Ends");
        return cnt;
    }
    private boolean editSingleEval(TblConfigEvalMethod evalMethod){
        LOGGER.debug("editSingleEval "+logUserId+" Starts");
        boolean flag = false;
        try {
             flag = configService.updateSingleConfigEvalMethod(evalMethod);
        } catch (Exception e) {
        LOGGER.error("editSingleEval "+logUserId+" :"+e);
    }
        LOGGER.debug("editSingleEval "+logUserId+" Ends");
         return flag;
    }

    private String ajaxUniqueConfig(String evalId,String pNature,String tendType,String pMethod) throws Exception{
        LOGGER.debug("ajaxUniqueConfig "+logUserId+" Starts");
        String val = "";
        try{

            val = String.valueOf(configService.uniqueConfigCount(evalId,pNature, tendType, pMethod));
        }catch(Exception e){
        LOGGER.error("ajaxUniqueConfig "+logUserId+" :"+e);
    }
        LOGGER.debug("ajaxUniqueConfig "+logUserId+" Ends");
        return val;
    }
}
