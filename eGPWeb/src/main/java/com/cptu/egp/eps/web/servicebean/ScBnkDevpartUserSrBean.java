/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblPartnerAdmin;
import com.cptu.egp.eps.model.table.TblPartnerAdminTransfer;
import com.cptu.egp.eps.model.table.TblUserTypeMaster;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.SHA1HashEncryption;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class ScBnkDevpartUserSrBean extends HttpServlet {

    static final Logger LOGGER = Logger.getLogger(ScBnkDevpartUserSrBean.class);
    private AuditTrail auditTrail;
    String logUserId = "";
    ScBankDevpartnerService scBankDevpartnerService = (ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        scBankDevpartnerService.setAuditTrail(auditTrail);
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ScBnkDevpartUserSrBean</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ScBnkDevpartUserSrBean at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
             */
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String partnerType = request.getParameter("partnerType");
        String action = request.getParameter("action");
        String strM = "0";
        boolean isError = false;
        String mobileNo = request.getParameter("mobileNo");
        HttpSession session = request.getSession();
        if (session.getAttribute("userId") != null) {
            logUserId = session.getAttribute("userId").toString();
        }
        
        LOGGER.debug("doPost " + logUserId + " Starts:");
//        if (mobileNo == null || mobileNo.matches("^[0-9]{20}") || mobileNo.trim().length() == 0) {
//            strM = "4";
//            isError = true;
//        }
//        if (strM.equals("0")) {
//            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
//            String msg = commonService.verifyMobileNo(mobileNo);
//            if (msg.length() > 2) {
//                strM = "5";
//                isError = true;
//            }
//        }
        String mailId = request.getParameter("mailId");
        if ("0".equals(strM)) {
            if (!mailId.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9\\-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                strM = "1";
                isError = true;
                //response.sendRedirect("admin/SchedBankCreateUser.jsp?partnerType=" + partnerType + "&strM=" + strM);
            }
        }
        if ("0".equals(strM)) {
            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
            commonService.setUserId(logUserId);
            String msg = commonService.verifyMail(mailId);
            if (msg.length() > 2) {
                strM = "2";
                isError = true;
                //response.sendRedirect("admin/SchedBankCreateUser.jsp?partnerType=" + partnerType + "&strM=" + strM);
            }
        }
        //if(!"0".equalsIgnoreCase(strM)){
        //    response.sendRedirect("admin/SchedBankCreateUser.jsp?partnerType=" + partnerType + "&strM=" + strM);
       // }
            
        if (isError) {
            if ("create".equals(action)) {
                response.sendRedirect("admin/SchedBankCreateUser.jsp?partnerType=" + partnerType + "&strM=" + strM);
            } else {
                response.sendRedirect("admin/SchedBankEditUser.jsp?partnerType=" + partnerType + "&strM=" + strM);
            }
        } else {

            //if (partnerType.equals("ScheduleBank")) {
                //String nationalId = request.getParameter("ntnlId");
                //CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                //String msg = commonService.verifyNationalId(nationalId);
//                if (msg.length() > 2) {
//                    strM = "3";
//                    if (action.equals("create")) {
//                        response.sendRedirect("admin/SchedBankCreateUser.jsp?partnerType=" + partnerType + "&strM=" + strM);
//                    } else {
//                        response.sendRedirect("admin/SchedBankEditUser.jsp?partnerType=" + partnerType + "&strM=" + strM);
//                    }
//                }
            //}

            StringBuilder userTypeId = new StringBuilder();
            if ("Development".equalsIgnoreCase(partnerType)) {
                userTypeId.append("6");
            } else if ("ScheduleBank".equalsIgnoreCase(partnerType)) {
                userTypeId.append("7");
            }

            TblPartnerAdmin tblPartnerAdmin = createAdmin(request);
            
            scBankDevpartnerService.setUserId(logUserId);
            
            if ("create".equalsIgnoreCase(action)) {
                LOGGER.debug("create action " + logUserId + " Starts:");
                TblLoginMaster tblLoginMaster = createLoginMaster(request);
                setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"),  request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
                String resultget = scBankDevpartnerService.createScBankDevPartnerUser(tblLoginMaster, tblPartnerAdmin);
                String[] splits;
                 TblPartnerAdminTransfer adminTransfer = insertIntoPrtnrAdminTrns(request);
                splits = resultget.split(",");
                int partnetid = Integer.parseInt(splits[0]);
                int flag = Integer.parseInt(splits[1]);
              
                adminTransfer.setPartnerId(partnetid);
                boolean result = scBankDevpartnerService.insertIntopartneradmintransfer(adminTransfer);
                
                if (flag > 0) {
                    //String mailId = request.getParameter("mailId");
                    String password = request.getParameter("password");
                    // String mobileNo = request.getParameter("mobileNo");
                    sendMsg(mailId, password, mobileNo, partnerType);
                    response.sendRedirect("admin/ViewSbDevPartUser.jsp?userId=" + flag + "&partnerType=" + partnerType + "&userTypeId=" + userTypeId.toString() + "&msg=success&from=Created");
                } else {
                    response.sendRedirect("admin/SchedBankCreateUser.jsp?partnerType=" + partnerType + "&msg=fail");
                }
                LOGGER.debug("create action " + logUserId + " Ends:");
            } else if ("update".equalsIgnoreCase(action)) {
                LOGGER.debug("update action " + logUserId + " Starts:");
                boolean flag = scBankDevpartnerService.updateScBankDevPartnerUser(tblPartnerAdmin);

                if (flag) {
                    int userId = Integer.parseInt(request.getParameter("userID"));
                    response.sendRedirect("admin/ViewSbDevPartUser.jsp?userId=" + userId + "&partnerType=" + partnerType + "&userTypeId=" + userTypeId.toString() + "&msg=success&from=Updated");
                } else {
                    response.sendRedirect("admin/SchedBankEditUser.jsp?partnerType=" + partnerType + "&msg=fail");
                }
                LOGGER.debug("update action " + logUserId + " Ends:");
            }
        }
        LOGGER.debug("doPost " + logUserId + " Ends:");
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private TblLoginMaster createLoginMaster(HttpServletRequest request) {
        LOGGER.debug("createLoginMaster " + logUserId + " Starts:");
        TblLoginMaster tblLoginMaster = new TblLoginMaster();
        tblLoginMaster.setIsEmailVerified("Yes");
        tblLoginMaster.setNationality("Bangladesi");
        tblLoginMaster.setIsJvca("No");
        tblLoginMaster.setBusinessCountryName("Bangladesh");
        tblLoginMaster.setFailedAttempt((byte) 0);
        tblLoginMaster.setFirstLogin("Yes");
        tblLoginMaster.setIsPasswordReset("No");
        tblLoginMaster.setResetPasswordCode("0");
        tblLoginMaster.setStatus("Approved");
        tblLoginMaster.setRegisteredDate(new Date());
        tblLoginMaster.setHintAnswer("");
        tblLoginMaster.setHintQuestion("");
        tblLoginMaster.setNextScreen("");

        String partnerType = request.getParameter("partnerType");
        tblLoginMaster.setRegistrationType(partnerType);

        byte userTypeId;
        if ("Development".equals(partnerType)) {
            userTypeId = 6;
        } else {
            userTypeId = 7;
        }
        tblLoginMaster.setTblUserTypeMaster(new TblUserTypeMaster(userTypeId));

        String mailId = request.getParameter("mailId");
        String password = request.getParameter("password");

        tblLoginMaster.setEmailId(mailId);
        tblLoginMaster.setPassword(SHA1HashEncryption.encodeStringSHA1(password));
        LOGGER.debug("createLoginMaster " + logUserId + " Ends:");
        return tblLoginMaster;
    }

    private TblPartnerAdminTransfer insertIntoPrtnrAdminTrns(HttpServletRequest request) {
        LOGGER.debug("insertIntoPrtnrAdminTrns " + logUserId + " Starts:");
        TblPartnerAdminTransfer tblPartnerAdmin = new TblPartnerAdminTransfer();
        String userRole = "";
        int branch = 0;
        if (request.getParameter("userRole") != null) {
            userRole = request.getParameter("userRole");
        }
        tblPartnerAdmin.setComments("");
        tblPartnerAdmin.setReplacedByEmailId("");
        if (userRole.equalsIgnoreCase("BankChecker")) {
            branch = Integer.parseInt(request.getParameter("office"));
            tblPartnerAdmin.setSbankDevelopId(branch);
        } else {
            branch = Integer.parseInt(request.getParameter("branch"));
            tblPartnerAdmin.setSbankDevelopId(branch);
        }
        String partnerType = request.getParameter("partnerType");
        int userTypeId;
        if ("Development".equalsIgnoreCase(partnerType)) {
            userTypeId = 6;
        } else {
            userTypeId = 7;
        }
        tblPartnerAdmin.setTransferedBy(userTypeId);
        tblPartnerAdmin.setTranseredDt(new java.sql.Date(new java.util.Date().getTime()));
        tblPartnerAdmin.setIsCurrent("yes");
        String isAdmin = request.getParameter("isAdmin");
        tblPartnerAdmin.setIsAdmin(isAdmin);
        String name = request.getParameter("fullName");
        String nationalId = "";
        if (request.getParameter("ntnlId") != null && request.getParameter("ntnlId").trim().length() > 0) {
            nationalId = request.getParameter("ntnlId");
        }
        String designation = request.getParameter("designation");
        if (request.getParameter("userRole") != null) {
            userRole = request.getParameter("userRole");
        }
        if (nationalId == null) {
            nationalId = "";
        }

        tblPartnerAdmin.setDesignation(designation);
        tblPartnerAdmin.setFullName(name);
        tblPartnerAdmin.setNationalId(nationalId);
        tblPartnerAdmin.setIsMakerChecker(userRole);
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        String mailId = request.getParameter("mailId");
        String userid = commonService.getUserId(mailId);
        tblPartnerAdmin.setEmailId(mailId);
        tblPartnerAdmin.setReplacedBy("");
       // tblPartnerAdmin.setPartnerId(userTypeId);
        tblPartnerAdmin.setTblLoginMaster(new TblLoginMaster(Integer.parseInt(userid)));

        LOGGER.debug("insertIntoPrtnrAdminTrns " + logUserId + " Ends:");
        return tblPartnerAdmin;
    }

    private TblPartnerAdmin createAdmin(HttpServletRequest request) {
        LOGGER.debug("createAdmin " + logUserId + " Starts:");
        TblPartnerAdmin tblPartnerAdmin = null;
        String userRole = "";
        int branch = 0;
        if (request.getParameter("userRole") != null) {
            userRole = request.getParameter("userRole");
        }
        if (request.getParameter("action").equals("create")) {
            tblPartnerAdmin = new TblPartnerAdmin();
            if (userRole.equalsIgnoreCase("BankChecker")) {
                branch = Integer.parseInt(request.getParameter("office"));
                tblPartnerAdmin.setSbankDevelopId(branch);
            } else {
                branch = Integer.parseInt(request.getParameter("branch"));
                tblPartnerAdmin.setSbankDevelopId(branch);
            }
        } else if (request.getParameter("action").equals("update")) {
            int partnerId = Integer.parseInt(request.getParameter("partnerId"));
            tblPartnerAdmin = new TblPartnerAdmin(partnerId);
            if (userRole.equalsIgnoreCase("BankChecker")) {
                branch = Integer.parseInt(request.getParameter("office"));
                tblPartnerAdmin.setSbankDevelopId(branch);
            } else {
                int sdBankDevId = Integer.parseInt(request.getParameter("officeId"));
                tblPartnerAdmin.setSbankDevelopId(sdBankDevId);
            }

            int userId = Integer.parseInt(request.getParameter("userId"));
            tblPartnerAdmin.setTblLoginMaster(new TblLoginMaster(userId));
        }
        String partnerType = request.getParameter("partnerType");
         byte userTypeId;
        if ("Development".equalsIgnoreCase(partnerType)) {
            userTypeId = 6;
        } else {
            userTypeId = 7;
        }
        String isAdmin = request.getParameter("isAdmin");
        tblPartnerAdmin.setIsAdmin(isAdmin);
        String name = request.getParameter("fullName");
        //String bngName = request.getParameter("bngName");


        String nationalId = "";
        if (request.getParameter("ntnlId") != null && request.getParameter("ntnlId").trim().length() > 0) {
            nationalId = request.getParameter("ntnlId");
        }

        String designation = request.getParameter("designation");
        if (request.getParameter("userRole") != null) {
            userRole = request.getParameter("userRole");
        }
        if (nationalId == null) {
            nationalId = "";
        }
        String mobileNo = "";
        if (request.getParameter("mobileNo") != null && request.getParameter("mobileNo").trim().length() > 0) {
            mobileNo = request.getParameter("mobileNo");
        }
        tblPartnerAdmin.setDesignation(designation);
        tblPartnerAdmin.setFullName(name);
        tblPartnerAdmin.setMobileNo(mobileNo);
        tblPartnerAdmin.setNationalId(nationalId);
        tblPartnerAdmin.setIsMakerChecker(userRole);
        if (!"".equalsIgnoreCase(mobileNo)) {
            if (userTypeId == 7) {
                mobileNo = "+975" + request.getParameter("mobileNo");
            } else {
                mobileNo = request.getParameter("mobileNo");
                String code = getPhoneCode(branch);
                mobileNo = code + mobileNo;
            }
        sendSms(mobileNo);
        }
        LOGGER.debug("createAdmin " + logUserId + " Ends:");
        return tblPartnerAdmin;
    }

    private boolean sendMsg(String mailId, String password, String mobileNo, String partnerType) {
        LOGGER.debug("sendMsg  " + logUserId + " Starts:");
        boolean mailSent = false;
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            String[] mailTo = {mailId};
            MailContentUtility mailContentUtility = new MailContentUtility();

            List list = null;

            if ("Development".equalsIgnoreCase(partnerType)) {
                list = mailContentUtility.getGovtDevelopmentContent(mailId, password);
            } else if ("ScheduleBank".equalsIgnoreCase(partnerType)) {
                list = mailContentUtility.getGovtScheduleBankContent(mailId, password);
            }

            String mailSub = list.get(0).toString();
            String mailText = list.get(1).toString();
            sendMessageUtil.setEmailTo(mailTo);
            sendMessageUtil.setEmailSub(mailSub);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            //sendMessageUtil.setSmsNo(mobileNo);
            //sendMessageUtil.setSmsBody("Dear User,%0AYour profile has been created on e-GP System. Login information is sent to your registered Email ID.%0Ae-GP User Registration Desk");
           // sendMessageUtil.sendSMS();
            mailSent = true;
        } catch (Exception e) {
            LOGGER.error("sendMsg " + logUserId + " :" + e);
        }
        LOGGER.debug("sendMsg  " + logUserId + " Ends:");
        return mailSent;
    }

    private boolean sendSms(String mobileNo) {
        LOGGER.debug("sendSms  " + logUserId + " Starts:");
        boolean sendSms = false;
        try {
        SendMessageUtil sendMessageUtil = new SendMessageUtil();
        sendMessageUtil.setSmsNo(mobileNo);
            sendMessageUtil.setSmsBody("Dear User,%0Aprofile has been created Login information is sent to your registered Email ID.%0Ae-GP User Registration Desk");
            sendMessageUtil.sendSMS();
            sendSms = true;
        } catch (Exception e) {
            LOGGER.error("sendSms " + logUserId + " :" + e);
        }
        LOGGER.debug("sendSms  " + logUserId + " Ends:");
        return sendSms;
    }

    public String getPhoneCode(int branch) {
        LOGGER.debug("getPhoneCode  " + logUserId + " Starts:");
        String i = "";
        ScBankDevpartnerService scBankDevpartnerService = (ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");
        List<Object> obj = scBankDevpartnerService.getPhoneCode(branch);
        if (obj.get(0) != null) {
        i = String.valueOf(obj.get(0));
        }
        obj.clear();
        obj = null;
        LOGGER.debug("getPhoneCode  " + logUserId + " Ends:");
        return i;
    }
}
