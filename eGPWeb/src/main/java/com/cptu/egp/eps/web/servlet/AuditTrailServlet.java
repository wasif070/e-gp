/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.web.servicebean.GenreatePdfCmd;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.DateUtils;
import com.cptu.egp.eps.web.utility.GeneratePdfConstant;
import com.cptu.egp.eps.web.utility.XMLReader;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author rishita
 */
public class AuditTrailServlet extends HttpServlet {

    /** 
     * This servlet is use to get report data for Audit trail Report
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        PrintWriter out = response.getWriter();
        try {
            if (request.getParameter("funName") != null) {
                if (request.getParameter("funName").equals("AuditTrailDetails")) {
                    String typeOfUser = "";
                    if (!"".equals(request.getParameter("typeOfUser"))) {
                        typeOfUser = request.getParameter("typeOfUser");
                    }

                    String ipAddress = "";
                    if (!"".equals(request.getParameter("ipAddress"))) {
                        ipAddress = request.getParameter("ipAddress");
                    }

                    String txtFromDate = "";
                    if (!"".equals(request.getParameter("txtFromDate"))) {
                        txtFromDate=dateFormat.format((Date)formatter.parse(request.getParameter("txtFromDate")));
                    }

                    String tenderId = "";
                    if (!"".equals(request.getParameter("tenderId"))) {
                        tenderId = request.getParameter("tenderId");
                    }
                    String cmbTenderId = "";
                    if (!"".equals(request.getParameter("cmbTenderId"))) {
                        cmbTenderId = request.getParameter("cmbTenderId");
                    }
                    String module = "";
                    if (!"".equals(request.getParameter("module"))) {
                        module = request.getParameter("module");
                    }

                    String txtToDate = "";
                    if (!"".equals(request.getParameter("txtToDate"))) {
                         txtToDate=dateFormat.format((Date)formatter.parse(request.getParameter("txtToDate")));
                    }

                    String ipEmailId = "";
                    if (!"".equals(request.getParameter("ipEmailId"))) {
                        ipEmailId = request.getParameter("ipEmailId");
                    }

                    String activity = "";
                    if (!"".equals(request.getParameter("activity"))) {
                        activity = request.getParameter("activity");
                    }

                    String txtdepartmentid = "";
                    if (!"".equals(request.getParameter("txtdepartmentid"))) {
                        txtdepartmentid = request.getParameter("txtdepartmentid");
                    }

                    String procuringEntity = "";
                    if (!"".equals(request.getParameter("procuringEntity"))) {
                        procuringEntity = request.getParameter("procuringEntity");
                    }
                    String strPageNo = request.getParameter("pageNo");
                    //System.out.println("Page No: " + strPageNo);
                    int pageNo = Integer.parseInt(strPageNo);
                    String strOffset = request.getParameter("size");
                    int recordOffset = Integer.parseInt(strOffset);
                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> auditTrialListing = commonSearchDataMoreService.geteGPDataMore("GeteGPAuditTrail", strPageNo, strOffset, typeOfUser, ipAddress, txtFromDate, txtToDate, ipEmailId, activity, txtdepartmentid, procuringEntity,tenderId,cmbTenderId,module);
                    if (auditTrialListing != null && !auditTrialListing.isEmpty()) {
                        int i = 0;
                        String styleClass = "";
                        for (; i < auditTrialListing.size(); i++) {
                            if (i % 2 == 0) {
                                styleClass = "bgColor-white";
                            } else {
                                styleClass = "bgColor-Green";
                            }
                            StringBuffer activityShow = new StringBuffer();

                            SPCommonSearchDataMore auditTrailList = auditTrialListing.get(i);
                            out.print("<tr class='" + styleClass + "'>");
                            out.print("<td width=\"5%\" class=\"t-align-center\">" + ((pageNo - 1) * recordOffset + (i + 1)) + "</td>");
                            out.print("<td width=\"10%\" class=\"t-align-center\">" + auditTrailList.getFieldName1() + "</td>");
                            out.print("<td width=\"10%\" class=\"t-align-left\">" + auditTrailList.getFieldName2() + "</td>");
                            out.print("<td width=\"10%\" class=\"t-align-left\">" + auditTrailList.getFieldName3() + "</td>");
                            out.print("<td width=\"10%\" class=\"t-align-left\">" + auditTrailList.getFieldName4() + "</td>");
                            out.print("<td width=\"10%\" class=\"t-align-left\">" + auditTrailList.getFieldName5() + "</td>");
                            out.print("<td width=\"10%\" class=\"t-align-left\">" + auditTrailList.getFieldName6() + "</td>");
                            out.print("<td width=\"10%\" class=\"t-align-left\">" + auditTrailList.getFieldName7().replace("STD", "SBD") + "</td>");
                            out.print("<td width=\"10%\" class=\"t-align-left\">" + auditTrailList.getFieldName8() + "</td>");
                            activityShow.append(Character.toUpperCase(auditTrailList.getFieldName10().charAt(0))).append(auditTrailList.getFieldName10().replace("Id", " Id").substring(1));
                            activityShow.append(" : " + auditTrailList.getFieldName9() + "<br/>");
                            String story = auditTrailList.getFieldName11().replaceAll("\\bPE\\b", "PA");
                            activityShow.append("Action : " + story + "<br/>");
                            activityShow.append("Date and Time : " + auditTrailList.getFieldName12() + "<br/>");
                            if (auditTrailList.getFieldName13() != null && !"".equals(auditTrailList.getFieldName13())) {
                                activityShow.append("Remarks : " + auditTrailList.getFieldName13() + "<br/>");
                            }

                            out.print("<td width=\"15%\" class=\"t-align-left\">" + activityShow + "</td>");
                            out.print("</tr>");
                        }
                        int totalPages = 1;
                        if (auditTrialListing.size() > 0) {
                            totalPages = (int) (Math.ceil(Math.ceil(Double.parseDouble(auditTrialListing.get(0).getFieldName14())) / recordOffset));
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                    } else {
                        out.print("<tr>");
                        out.print("<td colspan=\"10\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Record Found. Please Enter Proper Search Criteria and Try Again.</td>");
                        out.print("</tr>");
                    }
                
                    GenreatePdfCmd obj = new GenreatePdfCmd();
                    GeneratePdfConstant constant = new GeneratePdfConstant();
                    String cdate = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date(new Date().getTime()));
                    String folderName = constant.AUDITTRAIL;
                    String genId = cdate + "_" + request.getSession().getAttribute("userId").toString();
                    
                    File destFolder1 = new File(XMLReader.getMessage("driveLatter") +"/eGP/PDF/" + folderName);
                    if(destFolder1.exists()){
                        destFolder1.delete();
                    }
                    int userTypeID = Integer.parseInt(request.getSession().getAttribute("userTypeId").toString());
                    if(userTypeID== 1 || userTypeID== 8) {
                            strPageNo="1";
                            if (auditTrialListing.size() > 0) {
                                strOffset = auditTrialListing.get(0).getFieldName14();
                            }else{
                                strOffset="0";
                            }
                        }
                    String reqURL = request.getRequestURL().toString();
                    reqURL = reqURL.replace("AuditTrailServlet", "admin/AuditRptPDF.jsp");
                    String reqQuery = "typeOfUser="+typeOfUser+"&ipAddress="+ipAddress+"&txtFromDate="+txtFromDate+"&txtToDate="+txtToDate+"&ipEmailId="+ipEmailId+"&activity="+activity+"&txtdepartmentid="+txtdepartmentid+"&procuringEntity="+procuringEntity+"&tenderId="+tenderId+"&cmbTenderId="+cmbTenderId+"&module="+module+"&strPageNo="+strPageNo+"&strOffset="+strOffset;
                    obj.genrateCmd(reqURL, reqQuery, folderName, genId);  
                }else if(("getActivity").equalsIgnoreCase(request.getParameter("funName"))) {
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    StringBuilder str = new StringBuilder();
                    str.append("<option value=''>All</option>");
                    for (Object listing : makeAuditTrailService.getAuditActivity(request.getParameter("moduleName"))) {
                            String story = listing.toString().replaceAll("\\bPE\\b", "PA");
                            story = story.replaceAll("STD", "SBD");
                            str.append("<option value=\"" + listing + "\">" + story + "</option>");
                        }
                    out.print(str.toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
