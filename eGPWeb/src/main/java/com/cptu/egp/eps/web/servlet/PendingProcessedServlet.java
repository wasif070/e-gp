/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonSPWfFileHistory;
import com.cptu.egp.eps.web.servicebean.WorkFlowSrBean;
import com.cptu.egp.eps.web.utility.DateUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author test
 */
//@WebServlet(name="PendingProcessedServlet", urlPatterns={"/PendingProcessedServlet"})
public class PendingProcessedServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final Logger LOGGER = Logger.getLogger(APPWorkflowServlet.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND = "Ends";
    private String logUserId = "0";

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        LOGGER.debug("processRequest : "+logUserId+ LOGGERSTART);
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try 
        {
            response.setContentType("text/xml;charset=UTF-8");
            Integer recPerPage = Integer.parseInt(request.getParameter("rows"));
            Integer pag = Integer.parseInt(request.getParameter("page"));
            String sord = request.getParameter("sord");
            String sidx = request.getParameter("sidx");
            String viewType = request.getParameter("viewtype");

            String moduleName = "";
            String processName = "";
            int workflowId = 0;
            String processesBy = "";
            String processedStDt = "";
            String processedEndDt = "";

            SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            SimpleDateFormat sd1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            if (request.getParameter("module") != null) {
                moduleName = request.getParameter("module");
            }
            if (request.getParameter("process") != null) {
                processName = request.getParameter("process");
            }
            if (request.getParameter("workflowId") != null && !"".equals(request.getParameter("workflowId"))) {
                workflowId = Integer.parseInt(request.getParameter("workflowId"));
            }
            if (request.getParameter("processesBy") != null) {
                processesBy = request.getParameter("processesBy");
            }
            if (request.getParameter("dtStartDate") != null && !"".equals(request.getParameter("dtStartDate"))) {
                processedStDt = request.getParameter("dtStartDate").toString();
                processedStDt = sd1.format(sd.parse(processedStDt));
            }
            if (request.getParameter("dtEndDate") != null && !"".equals(request.getParameter("dtEndDate"))) {
                processedEndDt = request.getParameter("dtEndDate").toString();
                processedEndDt = sd1.format(sd.parse(processedEndDt));
            }

            if ("moduleName".equals(sidx)) {
                sidx = "moduleName";
            } else if ("eventName".equals(sidx)) {
                sidx = "eventName";
            } else if ("objectId".equals(sidx)) {
                sidx = "objectId";
            } else if ("fileSentFrom".equals(sidx)) {
                sidx = "fileSentFrom";
            } else if("processDate".equals(sidx)) {
                sidx = "processDate";
            } else if("action".equals(sidx)) {
                sidx = "action";
            } else if("fileSentTo".equals(sidx)) {
                sidx = "fileSentTo";
            } else {
                sord = "desc";
                sidx = "processDate";
            }
            //new code

            int uid = 0;
            if (request.getSession().getAttribute("userId") != null) {
                Integer ob1 = (Integer) request.getSession().getAttribute("userId");
                uid = ob1.intValue();
            }

            int totalpages = 0;
            int totalRecords = 0;
            WorkFlowSrBean wfsr = new WorkFlowSrBean();
            List<CommonSPWfFileHistory> commonSpWfFileHistory = wfsr.getWfHistory(viewType, 0,
                    0, 0, uid, pag, recPerPage, sidx, sord, moduleName, processName, processesBy, workflowId, processedStDt, processedEndDt);

            request.setAttribute("commonSpWfFileHistory", commonSpWfFileHistory);
            CommonSPWfFileHistory cspwf = null;
            if (!commonSpWfFileHistory.isEmpty()) {
                //cspwf =  commonSpWfFileHistory.get(0);
                totalRecords = commonSpWfFileHistory.get(0).getTotalRecords();
                //  cspwf.getTotalRecords().intValue();
                if (totalRecords % recPerPage == 0) {
                    totalpages = totalRecords / recPerPage;
                } else {
                    totalpages = totalRecords / recPerPage + 1;
                }
            }
            out.print("<?xml version='1.0' encoding='utf-8'?>\n");
            out.print("<rows>");
            out.print("<page>" + pag + "</page>");
            out.print("<total>" + totalpages + "</total>");
            out.print("<records>" + totalRecords + "</records>");
            int j = ((pag - 1) * recPerPage) + 1;

            if (!viewType.equalsIgnoreCase("defaultworkflow")) {
                for (int i = 0; i < commonSpWfFileHistory.size(); i++) {
                    out.print("<row>");
                    out.print("<cell>" + j + "</cell>");
                    out.print("<cell><![CDATA[" + commonSpWfFileHistory.get(i).getModuleName() + "]]></cell>");
                    out.print("<cell><![CDATA[" + commonSpWfFileHistory.get(i).getEventName() + "]]></cell>");
                    out.print("<cell>" + commonSpWfFileHistory.get(i).getObjectiId() + " " + getIdTypeResp(commonSpWfFileHistory.get(i).getActivityId()) + "</cell>");
                    out.print("<cell><![CDATA[" + commonSpWfFileHistory.get(i).getFileSentFrom() + "]]></cell>");
                    String str3 = DateUtils.gridDateToStr(commonSpWfFileHistory.get(i).getProcessDate());
                    out.print("<cell>" + str3 + "</cell>");
                    if ("Approve".equalsIgnoreCase(commonSpWfFileHistory.get(i).getAction())) {
                        out.print("<cell><![CDATA[Approved]]></cell>");
                    } else if ("Forward".equalsIgnoreCase(commonSpWfFileHistory.get(i).getAction())) {
                        out.print("<cell><![CDATA[Forwarded]]></cell>");
                    } else if ("Pull".equalsIgnoreCase(commonSpWfFileHistory.get(i).getAction())) {
                        out.print("<cell><![CDATA[Pulled]]></cell>");
                    } else if ("Return".equalsIgnoreCase(commonSpWfFileHistory.get(i).getAction())) {
                        out.print("<cell><![CDATA[Returned]]></cell>");
                    } else if ("Reject".equalsIgnoreCase(commonSpWfFileHistory.get(i).getAction())) {
                        out.print("<cell><![CDATA[Rejected]]></cell>");
                    } else if ("Conditional Approval".equalsIgnoreCase(commonSpWfFileHistory.get(i).getAction())) {
                        out.print("<cell><![CDATA[Conditional Approved]]></cell>");
                    } else {
                        out.print("<cell>" + commonSpWfFileHistory.get(i).getAction() + "</cell>");
                    }
                    out.print("<cell><![CDATA[" + commonSpWfFileHistory.get(i).getFileSentTo() + "]]></cell>");
                    String link = null;
                    if (viewType.equalsIgnoreCase("pending")) {
                                link = "<a style='text-decoration:underline;' href='"+request.getContextPath()+"/officer/FileProcessing.jsp?objectid="+commonSpWfFileHistory.get(i).getObjectiId()+
                                        "&childid="+commonSpWfFileHistory.get(i).getChildId()+"&eventid="+commonSpWfFileHistory.get(i).getEventId()+
                                        "&activityid="+commonSpWfFileHistory.get(i).getActivityId()+"&fromaction=pending'> Process </a>";
                    } else {
                                    link = "<a style='text-decoration:none;' href='"+request.getContextPath()+"/officer/workFlowHistory.jsp?objectid="+commonSpWfFileHistory.get(i).getObjectiId()+
                                        "&childid="+commonSpWfFileHistory.get(i).getChildId()+"&eventid="+commonSpWfFileHistory.get(i).getEventId()+
                                        "&activityid="+commonSpWfFileHistory.get(i).getActivityId()+"&userid="+commonSpWfFileHistory.get(i).getFromUserId()+"&fraction=process'> History </a>";
                    }
                    out.print("<cell><![CDATA[" + link + "]]></cell>");
                    out.print("</row>");
                    j++;
                }
            } else {
                for (int i = 0; i < commonSpWfFileHistory.size(); i++) {
                    out.print("<row>");
                    out.print("<cell>" + j + "</cell>");
                    out.print("<cell><![CDATA[" + commonSpWfFileHistory.get(i).getModuleName() + "]]></cell>");
                    out.print("<cell><![CDATA[" + commonSpWfFileHistory.get(i).getEventName() + "]]></cell>");
                    out.print("<cell>" + commonSpWfFileHistory.get(i).getObjectiId() + " " + getIdTypeResp(commonSpWfFileHistory.get(i).getActivityId()) + "</cell>");

                    String link = null;

                                link = "<a style='text-decoration:none;' href='"+request.getContextPath()+"/officer/FileProcessing.jsp?objectid="+commonSpWfFileHistory.get(i).getObjectiId()+
                                        "&childid="+commonSpWfFileHistory.get(i).getChildId()+"&eventid="+commonSpWfFileHistory.get(i).getEventId()+
                            "&activityid="+commonSpWfFileHistory.get(i).getActivityId()+"&fromaction=workflow&fromloc=defaultworkflow'>View</a>";

                    out.print("<cell><![CDATA[" + link + "]]></cell>");
                    out.print("</row>");
                    j++;
                }
            }
            out.print("</rows>");

        } catch (Exception ex) {
            LOGGER.error("processRequest " + logUserId + " : " + ex.toString());
        }
        LOGGER.debug("processRequest : " + logUserId + LOGGEREND);
    }

    private String getIdTypeResp(short activityId) {
        String idType = "Tender ID";
        try {
            switch (activityId) {
                case 1:
                    idType = "(APP ID)";
                    break;
                case 2:
                    idType = "(Tender ID)";
                    break;
                case 3:
                    idType = "(Tender ID)";
                    break;
                case 4:
                    idType = "(Tender ID)";
                    break;
                case 5:
                    idType = "(Tender ID)";
                    break;
                case 6:
                    idType = "(Tender ID)";
                    break;
                case 7:
                    idType = "(APP ID)";
                    break;
                case 8:
                    idType = "(Tender ID)";
                    break;
                case 9:
                    idType = "(Tender ID)";
                    break;
                case 10:
                    idType = "(Contract Termination ID)";
                    break;
                case 11:
                    idType = "(Variation Order ID)";
                    break;
                case 12:
                    idType = "(Repeat Order ID)";
                    break;
            }
        } catch (Exception ex) {
            LOGGER.error("getIdTypeResp " + logUserId + " : " + ex.toString());
        }
        return idType;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
