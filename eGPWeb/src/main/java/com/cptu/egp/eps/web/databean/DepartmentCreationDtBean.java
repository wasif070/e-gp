/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

import com.cptu.egp.eps.web.utility.BanglaNameUtils;
import java.util.Date;

/**
 *
 * @author rishita
 */
public class DepartmentCreationDtBean {

    private short departmentId;
    private String departmentName;
    private Short countryId;
    private Short stateId;
    private String address;
    private Date createdDate;
    private String strCreatedDate;
    private String upJilla;
    private String phoneNo;
    private String faxNo;
    private String city;
    private short parentDepartmentId;
    private short deptParentId;
    private String departmentType;
    private byte[] deptNameInBangla;
    private String deptBanglaString;
    private String website;
    private String postCode;
    private String mobileNo;
    private String deptHirarchy;
    private int approvingAuthId;
    private String deptHirchy;
    private String deptHirarchyMD;
    private String organizationType;

    public String getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(String organizationType) {
        this.organizationType = organizationType;
    }

    public String getDeptHirarchyMD() {
        return deptHirarchyMD;
    }

    public void setDeptHirarchyMD(String deptHirarchyMD) {
        this.deptHirarchyMD = deptHirarchyMD;
    }

    public String getDeptHirchy() {
        return deptHirchy;
    }

    public void setDeptHirchy(String deptHirchy) {
        this.deptHirchy = deptHirchy;
    }

    public int getApprovingAuthId() {
        return approvingAuthId;
    }

    public void setApprovingAuthId(int approvingAuthId) {
        this.approvingAuthId = approvingAuthId;
    }

    public short getDeptParentId() {
        return deptParentId;
    }

    public void setDeptParentId(short deptParentId) {
        this.deptParentId = deptParentId;
    }

    public String getDeptHirarchy() {
        return deptHirarchy;
    }

    public void setDeptHirarchy(String deptHirarchy) {
        this.deptHirarchy = deptHirarchy;
    }

    public DepartmentCreationDtBean() {
    }

    public String getStrCreatedDate() {
        return strCreatedDate;
    }

    public void setStrCreatedDate(String strCreatedDate) {
        this.strCreatedDate = strCreatedDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Short getCountryId() {
        return countryId;
    }

    public void setCountryId(Short countryId) {
        this.countryId = countryId;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Short getStateId() {
        return stateId;
    }

    public void setStateId(Short stateId) {
        this.stateId = stateId;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getDeptBanglaString() {
        return deptBanglaString;
    }

    public void setDeptBanglaString(String deptBanglaString) {
        if (deptBanglaString != null) {
            if (!deptBanglaString.equals("")) {
                this.deptNameInBangla = BanglaNameUtils.getBytes(deptBanglaString);
            }
        }
        this.deptBanglaString = deptBanglaString;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public short getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(short departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentType() {
        return departmentType;
    }

    public void setDepartmentType(String departmentType) {
        this.departmentType = departmentType;
    }

    public byte[] getDeptNameInBangla() {
        return deptNameInBangla;
    }

    public void setDeptNameInBangla(byte[] deptNameInBangla) {
        this.deptNameInBangla = deptNameInBangla;
    }

    public String getFaxNo() {
        return faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    public short getParentDepartmentId() {
        return parentDepartmentId;
    }

    public void setParentDepartmentId(short parentDepartmentId) {
        this.parentDepartmentId = parentDepartmentId;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
    public String getUpJilla() {
        return upJilla;
    }

    public void setUpJilla(String upJilla) {
        this.upJilla = upJilla;
    }
}
