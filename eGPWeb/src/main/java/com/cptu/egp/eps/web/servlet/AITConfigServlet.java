/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblCmsAitConfig;
import com.cptu.egp.eps.web.servicebean.CmsAitConfigBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Sreenu.Durga
 */
public class AITConfigServlet extends HttpServlet {

    static final Logger LOGGER = Logger.getLogger(AITConfigServlet.class);
    private String logUserId = "0";
    static final String STARTS = " Starts";
    static final String ENDS = " Ends";
    private static CmsAitConfigBean cmsAitConfigBean = new CmsAitConfigBean();
    private int totalRows = 0;
    private int financialyearId;
    private double slabStartAmt;
    private double slabEndAmt;
    private double slabPercent;
    private int createdBy;
    private double flatPercent;
    private int aitConfigId;
    private static final String INFINITE_STRING = "above";
    final BigDecimal INFINITE_VALUE = new BigDecimal(-1.000);
    private static final String ADD = "add";
    private static final String DELETE = "delete";
    private static final String UPDATE = "update";
    private static final int ZERO = 0;
    private static final int ONE = 1;
    private static final String ERROR = "error";

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.debug("processRequest : " + STARTS);
        HttpSession session = request.getSession();
        if (session.getAttribute("userId") != null) {
            cmsAitConfigBean.setLogUserId(session.getAttribute("userId").toString());
        }
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String action = "";
        try {
            if (request.getParameter("action") != null) {
                action = request.getParameter("action");
            } 
            if (ADD.equalsIgnoreCase(action)) {
                addAitViewConfig(request, response);
            } else if (DELETE.equalsIgnoreCase(action)) {
                deleteAitViewConfig(request, response);
            }
        } finally {
            out.close();
        }
        LOGGER.debug("processRequest : " + ENDS);
    }

    private void addAitViewConfig(HttpServletRequest request, HttpServletResponse response) throws IOException {
        LOGGER.debug("addAitViewConfig : " + STARTS);
        HttpSession session = request.getSession();
        if (request.getParameter("hdnTotalSlabs") != null) {
            totalRows = Integer.parseInt(request.getParameter("hdnTotalSlabs"));
        }
        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
            createdBy = Integer.parseInt(session.getAttribute("userId").toString());
        }
        if (request.getParameter("cmbFinancialYear") != null) {
            financialyearId = Integer.parseInt(request.getParameter("cmbFinancialYear"));
        }
        if (request.getParameter("txtFlatAitPercentage") != null) {
            flatPercent = Double.parseDouble(request.getParameter("txtFlatAitPercentage"));
        }
        if (totalRows != 0) {
            for (int i = 1; i <= totalRows; i++) {
                TblCmsAitConfig tblCmsAitConfig = new TblCmsAitConfig();
                if (request.getParameter("txtStartAmountSlab" + i) != null) {
                    slabStartAmt = Double.parseDouble(request.getParameter("txtStartAmountSlab" + i));
                    tblCmsAitConfig.setSlabStartAmt(BigDecimal.valueOf(slabStartAmt));
                }
                if (request.getParameter("txtEndAmountSlab" + i) != null) {
                    String endAmountSlab = request.getParameter("txtEndAmountSlab" + i);
                    if (INFINITE_STRING.equalsIgnoreCase(endAmountSlab)) {
                        slabEndAmt = INFINITE_VALUE.doubleValue();
                    } else {
                        slabEndAmt = Double.parseDouble(endAmountSlab);
                    }
                    tblCmsAitConfig.setSlabEndAmt(BigDecimal.valueOf(slabEndAmt));
                }
                if (request.getParameter("txtAitPercentageSlab" + i) != null) {
                    slabPercent = Double.parseDouble(request.getParameter("txtAitPercentageSlab" + i));
                    tblCmsAitConfig.setSlabPercent(BigDecimal.valueOf(slabPercent));
                }
                tblCmsAitConfig.setCreatedBy(createdBy);
                tblCmsAitConfig.setFinancialyearId(financialyearId);
                tblCmsAitConfig.setFlatPercent(BigDecimal.valueOf(flatPercent));
                cmsAitConfigBean.insertCmsAitConfiguration(tblCmsAitConfig);
            }// for loop
        }
        response.sendRedirect("/admin/AITViewList.jsp?financialyearId=" + financialyearId);
        LOGGER.debug("addAitViewConfig : " + ENDS);
    }

    private void deleteAitViewConfig(HttpServletRequest request, HttpServletResponse response) throws IOException {
        LOGGER.debug("deleteAitViewConfig : " + STARTS);
        if (request.getParameter("aitConfigId") != null) {
            aitConfigId = Integer.parseInt(request.getParameter("aitConfigId"));
            int finYearId = 0;
            if (aitConfigId != 0) {
                TblCmsAitConfig selectedTblCmsAitConfig = cmsAitConfigBean.getCmsAitConfiguration(aitConfigId);
                finYearId = selectedTblCmsAitConfig.getFinancialyearId();
                TblCmsAitConfig maxAmountObject = getMaxAmountObject(finYearId);
                if (maxAmountObject != null) {
                    if (maxAmountObject.getAitConfigId() == selectedTblCmsAitConfig.getAitConfigId()) {
                        cmsAitConfigBean.deleteCmsAitConfiguration(selectedTblCmsAitConfig);
                        response.sendRedirect("/admin/AITViewList.jsp?financialyearId=" + finYearId + "&message=" + DELETE);
                    } else {
                        response.sendRedirect("/admin/AITViewList.jsp?financialyearId=" + finYearId + "&message=" + ERROR);
                    }
                } else {
                    response.sendRedirect("/admin/AITViewList.jsp?financialyearId=" + finYearId + "&message=" + ERROR);
                }
            }
        }
        LOGGER.debug("deleteAitViewConfig : " + ENDS);
    }

    private TblCmsAitConfig getMaxAmountObject(int financialYearId) {
        LOGGER.debug("getMaxAmountObject : " + STARTS);
        TblCmsAitConfig maxAmountAitConfig = new TblCmsAitConfig();
        maxAmountAitConfig.setSlabEndAmt(BigDecimal.ZERO);
        if (financialYearId != 0) {
            List<TblCmsAitConfig> cmsAitConfigList = cmsAitConfigBean.getCmsAitConfigForFianancialYear(financialYearId);
            if (cmsAitConfigList != null && !cmsAitConfigList.isEmpty()) {
                for (TblCmsAitConfig cmsAitConfig : cmsAitConfigList) {
                    // comapre the value is infinte value. The comparision of BigDecimal is different.
                    // If they are equals it returns ZERO. Less than returns +1. Greater than returns -1
                    if (INFINITE_VALUE.compareTo(cmsAitConfig.getSlabEndAmt()) == ZERO) {
                        maxAmountAitConfig = cmsAitConfig;
                        break;
                    } else if (maxAmountAitConfig.getSlabEndAmt().compareTo(cmsAitConfig.getSlabEndAmt()) < ONE) {
                        maxAmountAitConfig = cmsAitConfig;
                    }
                }// for loop
            }
        }
        LOGGER.debug("getMaxAmountObject : " + ENDS);
        return maxAmountAitConfig;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
