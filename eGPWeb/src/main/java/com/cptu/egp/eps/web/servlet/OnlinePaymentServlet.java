/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.model.table.TblAdvertisement;
import com.cptu.egp.eps.service.serviceinterface.AdvertisementService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.databean.OnlinePaymentDtBean;
import com.cptu.egp.eps.web.utility.*;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import ipgclient2.CShroff2;
import java.io.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author nishit
 */
public class OnlinePaymentServlet extends HttpServlet {

    private String strDesc = "";
    private String strDriveLetter = XMLReader.getMessage("driveLatter") + "/eGP";
    private String pyamentUrl = "";
    private String strPaymentGateway = "";
    private String strBracMerchanrID = XMLReader.getBankMessage("BRAC_MERCHANT_ID");

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private final AdvertisementService advertisementService = (AdvertisementService) AppContext.getSpringBean("AdvertisementService");
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        try {
            /*
             * TODO output your page here. You may use following sample code.
             */
            System.out.println("strBracMerchanrID "+strBracMerchanrID);
            HttpSession session = request.getSession();
            String pageName = "";
            String cardType = "";
            String action = request.getParameter("action");
            String stripAddress = request.getHeader("X-FORWARDED-FOR");
            if ((stripAddress != null && stripAddress.indexOf(".") == -1) || stripAddress == null) {
                stripAddress = request.getRemoteAddr();
            }
            if (request.getLocalAddr().equals("122.170.119.66")) {
                //System.out.println("Local");
                strDriveLetter = "C:";
            }
            if (request.getParameter("BankList") != null) {
                pyamentUrl = XMLReader.getBankMessage(request.getParameter("BankList"));
                strPaymentGateway = request.getParameter("BankList").toString();
                   
                   System.out.println("strPaymentGateway :" + strPaymentGateway + " URL "+XMLReader.getBankMessage(request.getParameter("BankList")));
            }
            if(request.getParameter("cardType")!=null){
                cardType = request.getParameter("cardType").toString();
            }
            
            /*
             * For VAS Service Payment
             */
            String reqFrom = request.getParameter("reqFrom");
            if (reqFrom != null && "regVasFeePayment".equals(reqFrom)) {
                if ("update".equalsIgnoreCase(request.getParameter("action"))) {
                     String op;
                    OnlinePaymentDtBean onlinePaymentDtBean = new OnlinePaymentDtBean("", "", "fromvas", "0", "");
                    if (request.getAttribute("vasPayment") != null) {
                        onlinePaymentDtBean = (OnlinePaymentDtBean) request.getAttribute("vasPayment");
                        onlinePaymentDtBean.setPaymentGateway(strPaymentGateway);
                        request.removeAttribute("vasPayment");
                    }
                    //System.out.println("update action :" + request.getParameter("barkResp"));
                    if (request.getParameter("barkResp") == null && request.getParameter("barkResp") == "") {
                        op = onlinePyamentResult(onlinePaymentDtBean, stripAddress, "0");
                        if (op != null && !op.trim().equals("")) {
                            String cardNumber = "";
                            if (op.indexOf("CARD_NUMBER") >= 0) {
                                cardNumber = op.trim().substring(12);
                                if (op.indexOf("000") > 0 && op.indexOf("OK") > 0) {
                                    MailContentUtility mcu = new MailContentUtility();
                                    List list = mcu.onlineVASPaymentSuccess(onlinePaymentDtBean);
                                    sendMsg(list, "0", onlinePaymentDtBean.getEmailId());
                                }
                            }

                        }
                    } else {
                          int Result = -1;
                                String EReceipt = "";
                                String PTReceipt = "";
                                //System.out.println("BarcBank code executed to update data");
                                //System.out.println("Response of ecnrypted barck bank is :" + EReceipt);
                                EReceipt = request.getParameter("barkResp");
                                if (EReceipt != null) {
                                    if (EReceipt.length() > 0) {
                                        //CShroff2 cShroff2 = new CShroff2("C:\\merchant\\keys\\", "C:\\merchant\\keys\\logs\\");
                                        CShroff2 cShroff2 = new CShroff2(XMLReader.getMessage("driveLatter") + "\\eGP" + "\\merchant\\keys\\", XMLReader.getMessage("driveLatter") + "\\eGP" + "\\merchant\\keys\\logs\\");

                                        Result = cShroff2.getErrorCode();
                                        if (Result == 0) {
                                            Result = cShroff2.setEncryptedReceipt(EReceipt);

                                            if (Result == 0) {
                                                PTReceipt = cShroff2.getPlainTextReceipt();
                                                System.out.println("Response of decrypted barck bank is :" + PTReceipt);
                                            }
                                        }
                                    }
                                }
                                String cardNumber = "xxxxxxx";

                                //System.out.println("Response of decrypted barck bank is 2 PTReceipt:" + PTReceipt);
                                op = updateFinalValueRequest(onlinePaymentDtBean, "0", PTReceipt, cardNumber);
                                if (op != null && !op.trim().equals("")) {

                                    if (op.indexOf("<acc_no>") >= 0) {
                                        cardNumber = op.trim().substring(op.indexOf("<acc_no>"),op.indexOf("</acc_no>"));
                                    }
                                   // auditRemark = strDesc.replaceAll("_", " ") + " Successful";
                                    onlinePaymentDtBean.setCardNumber(cardNumber.replace("<acc_no>", ""));
                                    if (op.indexOf("<txn_status>ACCEPTED</txn_status>") > 0) {
                                        MailContentUtility mcu = new MailContentUtility();
                                        List list = mcu.onlinePaymentSuccess(onlinePaymentDtBean);
                                        sendMsg(list, "0", onlinePaymentDtBean.getEmailId());
                                    }
                                    session.removeAttribute("payment");
                                    onlinePaymentDtBean.setResponse(op);
                                    session.setAttribute("payment", onlinePaymentDtBean);
                                    //System.out.println("Success");
                                }
                    }
                     pageName = request.getContextPath() + "/RegVasFeePayment.jsp?er=1&tansId=" + onlinePaymentDtBean.getOnlineTraId();
                } else {
                    OnlinePaymentDtBean onlinePaymentDtBean = new OnlinePaymentDtBean(request.getParameter("txtemailId"), request.getParameter("txtAmount"), "fromvas", "0", XMLReader.getMessage("ServiceCharge"));
                    String strAmount = "0";
                    String strTranID = null;
                    int onlineId = 0;
					int adId = 0;
                    onlinePaymentDtBean.setPayeeName(request.getParameter("txtPayeeName"));
                    onlinePaymentDtBean.setPayeeAddress(request.getParameter("txtPayeeAddress"));
                    String service = request.getParameter("service");
                    if (service !=null && service.equalsIgnoreCase("Advertisement")) {
                        onlinePaymentDtBean.setPaymentDesc("Payment for Advertisement");
                        if (request.getParameter("adId") != null && !request.getParameter("adId").equals("")) {
                            adId = Integer.parseInt(request.getParameter("adId"));
                        }
                    } else {
                        request.getParameter("txtDescofPay").replaceAll(" ", "_");
                    }
                    onlinePaymentDtBean.setRemarks(request.getParameter("txtremarks"));
                    onlinePaymentDtBean.setPaymentGateway(strPaymentGateway);
                    if (request.getParameter("totalAmount") != null) {
                        strAmount = request.getParameter("totalAmount");
                    }

                    if (onlinePaymentDtBean.getPaymentGateway().equalsIgnoreCase("PaymentGatewayURLDBBL")) {
                        if (strAmount.indexOf(".") == -1) {
                            strAmount = "" + Integer.parseInt(strAmount) * 100;
                        } else if (strAmount.indexOf(".") > 0) {
                            strAmount = new BigDecimal(strAmount).setScale(2, RoundingMode.HALF_EVEN).toString().replace(".", "");

                        }
                    } else if (onlinePaymentDtBean.getPaymentGateway().equalsIgnoreCase("PaymentGatewayURLBRAC")) {
                        strAmount = new BigDecimal(strAmount).setScale(2, RoundingMode.HALF_EVEN).toString();
                    }
                    String op = insertOnlinePayment(onlinePaymentDtBean, request.getParameter("txtDescofPay").replaceAll(" ", "_"), stripAddress, strAmount, "0",cardType);
                    if (!op.equalsIgnoreCase("error")) {
                        String[] abc = op.split(",");

                        if (abc.length == 2) {
                            strTranID = abc[0];
                            onlineId = Integer.parseInt(abc[1]);
                        }
                        if (onlineId > 0 && strTranID != null) {
                            //session.removeAttribute("payment");
                            onlinePaymentDtBean.setOnlineId(onlineId);
                            onlinePaymentDtBean.setOnlineTraId(strTranID);
                            //session.setAttribute("payment", onlinePaymentDtBean);
                            if(adId>0){
	                            List<TblAdvertisement> lst = advertisementService.findTblAdvertisement("adId", Operation_enum.EQ, adId);
    	                        TblAdvertisement tbl = lst.get(0);
        	                    tbl.setPrice(tbl.getPrice());
            	                tbl.setStatus("paid");
                	            tbl.seteMailId(request.getParameter("txtemailId"));
                    	        tbl.setUserName(request.getParameter("txtPayeeName"));
                        	    tbl.setAddress(request.getParameter("txtPayeeAddress"));
                            	tbl.setTransId(strTranID);
            	                tbl.setTransId(strTranID);
                	            advertisementService.updateTblAdvertisement(tbl);
                            }
                            if (onlinePaymentDtBean.getPaymentGateway().equals("PaymentGatewayURLDBBL")) {
                                String[] cardtypeAppend =  pyamentUrl.split("&");
                                cardtypeAppend[0] = cardtypeAppend[0] + cardType;
                                cardtypeAppend[1] = cardtypeAppend[1] + strTranID;
                                pageName =  cardtypeAppend[0] + "&" +cardtypeAppend[1];
                                // pageName = pyamentUrl + strTranID;
                            } else if (onlinePaymentDtBean.getPaymentGateway().equals("PaymentGatewayURLBRAC")) {
                                String PTInvoice = "";
                                int Result = -1;
                                String ErrorMessage = "";
                                String EInvoice = "";
                                //  System.out.println("strAmount data :"+strAmount);
                          //      if ((pageName != null) && (pageName.length() > 0)) {
                                    String returnUrl = XMLReader.getMessage("urltoredirecthttps")+ "/BracPaymentSuccess.jsp?trans_id=" + strTranID;
                                    if(request.getRequestURL().toString().contains("development.eprocure.gov.bd") ||
                                            request.getRequestURL().toString().contains("10.1.3.21") ||
                                            request.getSession().getAttribute("userId") == null
                                       ){
                                        returnUrl = XMLReader.getMessage("urltoredirect")+ "/BracPaymentSuccess.jsp?trans_id=" + strTranID;
                                    }
                                            //System.out.println("Returnurl is :" + returnUrl);
                                            //PTInvoice = PTInvoice + "<ret_url>" + returnUrl + "</ret_url>";

                                   //     }
                                        PTInvoice = "<req>"
                                                + "<mer_id>"+strBracMerchanrID+"</mer_id>"
                                                + "<mer_txn_id>" + strTranID + "</mer_txn_id>"
                                                + "<action>SaleTxn</action>"
                                                + "<txn_amt>" + strAmount + "</txn_amt>"
                                                + "<cur>Nu.</cur>"
                                                + "<ret_url>" + returnUrl + "</ret_url>"
                                                + "<lang>eng</lang>"
                                                + "<mer_var1></mer_var1>"
                                                + "<mer_var2>" + onlinePaymentDtBean.getAmt().toString() + "</mer_var2>"
                                                + "<mer_var3></mer_var3>"
                                                + "<mer_var4>" + ""+((Double.parseDouble(onlinePaymentDtBean.getServiceCharge()) * Double.parseDouble(onlinePaymentDtBean.getAmt())) / 100) + "</mer_var4>"
                                                + "</req>";
                                        System.out.println("PTINVOIce :"+PTInvoice);
                                        CShroff2 cShroff2 = new CShroff2(strDriveLetter + "\\merchant\\keys\\", strDriveLetter + "\\merchant\\keys\\logs\\");
                                        Result = cShroff2.getErrorCode();
                                        if (Result == 0) {
                                            Result = cShroff2.setPlainTextInvoice(PTInvoice);
                                            if (Result == 0) {
                                                EInvoice = cShroff2.getEncryptedInvoice();
                                            } else {
                                                ErrorMessage = cShroff2.getErrorMsg();
                                            }
                                        } else {
                                            ErrorMessage = cShroff2.getErrorMsg();
                                        }
                                        pageName = request.getContextPath() + "/PaymentRedirect.jsp?encryptedInvoicePay=" + EInvoice+"&pgw="+strPaymentGateway;//+"?encryptedInvoicePay="+EInvoice;


//                                        RequestDispatcher rd = request.getRequestDispatcher(pageName);
//                                        request.setAttribute("encryptedInvoicePay", EInvoice);
//                                        rd.forward(request, response);
//                                        pageName = "";
                                    }
                        }
                    } else {
                        pageName = request.getContextPath() + "/RegVasFeePayment.jsp?er=2";
                    }
                }
            } else if ("verifyMail".equals(action)) {
                String mailid = request.getParameter("mailId");
                String msg = verifyMail(mailid);
                out.print(msg);
                out.flush();
                /*
                 * For NUR/Renewal and  DOC Fees
                 */
            } else {
                if (session != null) {
                    OnlinePaymentDtBean onlinePaymentDtBean = (OnlinePaymentDtBean) session.getAttribute("payment");

                    if (onlinePaymentDtBean != null) {

                        onlinePaymentDtBean.setPaymentGateway(strPaymentGateway);
                        String errmsg = ""; // error message
                        String strAmount = onlinePaymentDtBean.getTotal();
                        //String strAmount = "10";
                        String strTranID = null;
                        int onlineId = 0;


                        if ("nur".equalsIgnoreCase(onlinePaymentDtBean.getFromWhere())) {
                            strDesc = "Online_Payment_For_New_User_Registration";
                            pageName = request.getContextPath() + "/OnlinePmtForNUR.jsp?er=1&tenderId="+onlinePaymentDtBean.getTenderId();
                        } else if ("renewal".equalsIgnoreCase(onlinePaymentDtBean.getFromWhere())) {
                            strDesc = "Online_Payment_For_User_Profile_Renewal";
                            pageName = request.getContextPath() + "/OnlinePmtForRenewal.jsp?er=1&tenderId="+onlinePaymentDtBean.getTenderId();
                        } else if ("docFees".equalsIgnoreCase(onlinePaymentDtBean.getFromWhere())) {
                            strDesc = "Online_Payment_For_Tender/Proposal_Document_Fees";
                            pageName = request.getContextPath() + "/OnlinePmtForDocFees.jsp?er=1&tenderId="+onlinePaymentDtBean.getTenderId();
                        }
                        String auditAction = strDesc.replaceAll("_", " ") + " Request";
                        String auditRemark = strDesc.replaceAll("_", " ") + " Pending";
                        if ("update".equalsIgnoreCase(request.getParameter("action"))) {
                            String op;
                            //System.out.println("update action :" + request.getParameter("barkResp"));
                            if (request.getParameter("barkResp") == null || request.getParameter("barkResp").equals("")) {
                                op = onlinePyamentResult(onlinePaymentDtBean, stripAddress, session.getAttribute("userId").toString());
                                System.out.println("op:"+op);
                                auditAction = strDesc.replaceAll("_", " ") + " Response";
                                auditRemark = strDesc.replaceAll("_", " ") + " Fail";
                                if (op != null && !op.trim().equals("")) {
                                    String cardNumber = "";
                                    if (op.indexOf("CARD_NUMBER") >= 0) {
                                        cardNumber = op.trim().substring(12);
                                    }
                                    auditRemark = strDesc.replaceAll("_", " ") + " Successful";
                                    onlinePaymentDtBean.setCardNumber(cardNumber);
                                    if (op.indexOf("000") > 0 && op.indexOf("OK") > 0) {
                                        MailContentUtility mcu = new MailContentUtility();
                                        List list = mcu.onlinePaymentSuccess(onlinePaymentDtBean);
                                        sendMsg(list, session.getAttribute("userId").toString(), "");
                                    }
                                    session.removeAttribute("payment");
                                    onlinePaymentDtBean.setResponse(op);
                                    session.setAttribute("payment", onlinePaymentDtBean);
                                    //System.out.println("Success");
                                }
                            } // Code to update data of bark bank
                            else {
                                int Result = -1;
                                String EReceipt = "";
                                String PTReceipt = "";
                                EReceipt = request.getParameter("barkResp");
                                if (EReceipt != null) {
                                    if (EReceipt.length() > 0) {
                                        //CShroff2 cShroff2 = new CShroff2("C:\\merchant\\keys\\", "C:\\merchant\\keys\\logs\\");
                                        CShroff2 cShroff2 = new CShroff2(XMLReader.getMessage("driveLatter") + "\\eGP" + "\\merchant\\keys\\", XMLReader.getMessage("driveLatter") + "\\eGP" + "\\merchant\\keys\\logs\\");

                                        Result = cShroff2.getErrorCode();
                                        if (Result == 0) {
                                            Result = cShroff2.setEncryptedReceipt(EReceipt);

                                            if (Result == 0) {
                                                PTReceipt = cShroff2.getPlainTextReceipt();
                                            }
                                        }
                                    }
                                }
                                String cardNumber = "xxxxxxx";

                                op = updateFinalValueRequest(onlinePaymentDtBean, session.getAttribute("userId").toString(), PTReceipt, cardNumber);
                                if (op != null && !op.trim().equals("")) {

                                    if (op.indexOf("<acc_no>") >= 0) {
                                        cardNumber = op.trim().substring(op.indexOf("<acc_no>"),op.indexOf("</acc_no>"));
                                    }
                                    auditRemark = strDesc.replaceAll("_", " ") + " Successful";
                                    onlinePaymentDtBean.setCardNumber(cardNumber.replace("<acc_no>", ""));
                                    if (op.indexOf("<txn_status>ACCEPTED</txn_status>") > 0) {
                                        MailContentUtility mcu = new MailContentUtility();
                                        List list = mcu.onlinePaymentSuccess(onlinePaymentDtBean);
                                        sendMsg(list, session.getAttribute("userId").toString(), "");
                                    }
                                    session.removeAttribute("payment");
                                    onlinePaymentDtBean.setResponse(op);
                                    session.setAttribute("payment", onlinePaymentDtBean);
                                }
                            }
                        } else {
                            if (onlinePaymentDtBean.getPaymentGateway().equalsIgnoreCase("PaymentGatewayURLDBBL")) {
                                if (strAmount.indexOf(".") == -1) {
                                    strAmount = "" + Integer.parseInt(strAmount) * 100;
                                } else if (strAmount.indexOf(".") > 0) {
                                    strAmount = new BigDecimal(strAmount).setScale(2, RoundingMode.HALF_EVEN).toString().replace(".", "");

                                }
                            } else if (onlinePaymentDtBean.getPaymentGateway().equalsIgnoreCase("PaymentGatewayURLBRAC")) {
                                strAmount = new BigDecimal(strAmount).setScale(2, RoundingMode.HALF_EVEN).toString();
                            }
                            String op = insertOnlinePayment(onlinePaymentDtBean, strDesc, stripAddress, strAmount, session.getAttribute("userId").toString(),cardType);
                            if (!op.equalsIgnoreCase("error")) {
                                String[] abc = op.split(",");
                                String cardNumber = null;
                                if (abc.length == 2) {
                                    strTranID = abc[0];
                                    onlineId = Integer.parseInt(abc[1]);
                                }
                                //     System.out.println("strTranID :" + strTranID);
                                //     System.out.println("onlineId :" + onlineId);
                                if (onlineId > 0 && strTranID != null) {
                                    session.removeAttribute("payment");
                                    onlinePaymentDtBean.setOnlineId(onlineId);
                                    onlinePaymentDtBean.setOnlineTraId(strTranID);
                                    session.setAttribute("payment", onlinePaymentDtBean);
                                    if (onlinePaymentDtBean.getPaymentGateway().equals("PaymentGatewayURLDBBL")) {
                                        String[] cardtypeAppend =  pyamentUrl.split("&");
                                        cardtypeAppend[0] = cardtypeAppend[0] + cardType;
                                        cardtypeAppend[1] = cardtypeAppend[1] + strTranID;
                                        pageName =  cardtypeAppend[0] + "&" +cardtypeAppend[1];
                                       //pageName = pyamentUrl + strTranID;                                       
                                    } else if (onlinePaymentDtBean.getPaymentGateway().equals("PaymentGatewayURLBRAC")) {
                                        String PTInvoice = "";
                                        int Result = -1;
                                        String ErrorMessage = "";
                                        String EInvoice = "";
                                        String returnUrl = "";
                                        //  System.out.println("strAmount data :"+strAmount);
                                        if ((pageName != null) && (pageName.length() > 0)) {
                                            returnUrl = XMLReader.getMessage("urltoredirecthttps")+ "/BracPaymentSuccess.jsp?trans_id=" + strTranID;
                                            if(request.getRequestURL().toString().contains("development.eprocure.gov.bd") ||
                                                    request.getRequestURL().toString().contains("10.1.3.21") ||
                                                    request.getSession().getAttribute("userId") == null
                                               ){
                                                returnUrl = XMLReader.getMessage("urltoredirect")+ "/BracPaymentSuccess.jsp?trans_id=" + strTranID;
                                            }
                                            System.out.println("Returnurl is :" + returnUrl);
                                            //PTInvoice = PTInvoice + "<ret_url>" + returnUrl + "</ret_url>";
                                        }
                                        PTInvoice = "<req>"
                                                + "<mer_id>"+strBracMerchanrID+"</mer_id>"
                                                + "<mer_txn_id>" + strTranID + "</mer_txn_id>"
                                                + "<action>SaleTxn</action>"
                                                + "<txn_amt>" + strAmount + "</txn_amt>"
                                                + "<cur>Nu.</cur>"
                                                + "<ret_url>" + returnUrl + "</ret_url>"
                                                + "<lang>eng</lang>"
                                                + "<mer_var1></mer_var1>"
                                                + "<mer_var2>" + onlinePaymentDtBean.getAmt().toString() + "</mer_var2>"
                                                + "<mer_var3></mer_var3>"
                                                + "<mer_var4>" + ""+((Double.parseDouble(onlinePaymentDtBean.getServiceCharge()) * Double.parseDouble(onlinePaymentDtBean.getAmt())) / 100) + "</mer_var4>"
                                                + "</req>";
                                            System.out.println("PTINVOIce :"+PTInvoice);
                                        CShroff2 cShroff2 = new CShroff2(strDriveLetter + "\\merchant\\keys\\", strDriveLetter + "\\merchant\\keys\\logs\\");
                                        Result = cShroff2.getErrorCode();
                                        if (Result == 0) {
                                            Result = cShroff2.setPlainTextInvoice(PTInvoice);
                                            if (Result == 0) {
                                                EInvoice = cShroff2.getEncryptedInvoice();
                                            } else {
                                                ErrorMessage = cShroff2.getErrorMsg();
                                            }
                                        } else {
                                            ErrorMessage = cShroff2.getErrorMsg();
                                        }
                                        pageName = request.getContextPath() + "/PaymentRedirect.jsp?encryptedInvoicePay=" + EInvoice+"&pgw="+strPaymentGateway;//+"?encryptedInvoicePay="+EInvoice;


//                                        RequestDispatcher rd = request.getRequestDispatcher(pageName);
//                                        request.setAttribute("encryptedInvoicePay", EInvoice);
//                                        rd.forward(request, response);
//                                        pageName = "";
                                    }
                                }

                            }
                        }
                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(new AuditTrail(stripAddress, session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), Integer.parseInt(session.getAttribute("userId").toString()), "userId", EgpModule.Online_Payment.getName(), auditAction, auditRemark);
                    }
                } else {
                    pageName = request.getContextPath() + "/SessionTimedOut.jsp";
                }
                /*
                 * Complited code for NUR/Renewal and  DOC Fees
                 */
            }
            if (!"".equals(pageName) && pageName != null) {
                response.sendRedirect(pageName);
            }
        } catch (Exception e) {
            e.printStackTrace();
            AppExceptionHandler excpHandle = new AppExceptionHandler();
            excpHandle.stack2string(e);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /**
     *  First step of online payment   <br/>This method will insert data in to online payment table. (From which we come to know that transaction is started.)
     * @param onlinePaymentDtBean Bean Object which will store online payment data
     * @param desc Description/Remarks of payment
     * @param stripAddress Client IP Address
     * @param strAmount Amount to be requested for online payment
     * @param userId Online payment requested user id/ logged in user id
     * @return CSV of Online payment transactionId and inserted id(PK) of online payment table
     */
    private String insertOnlinePayment(OnlinePaymentDtBean onlinePaymentDtBean, String desc, String stripAddress, String strAmount, String userId, String cardType) {
        int onlineId = 0;
        String strTranID = null;
        StringBuilder sb = new StringBuilder();
        boolean onlinePaymentStatusSuccess = false;

        try {
            if (onlinePaymentDtBean.getPaymentGateway().equals("PaymentGatewayURLDBBL")) { //DBBL

               
                String strExecCMD = "java -jar " + strDriveLetter + "/paymentGatewayTest/ecomm_merchant.jar  " + strDriveLetter + "/paymentGatewayTest/merchant.properties -v " + strAmount + " 050 " + stripAddress + " " + cardType+desc;// + " > C:/test/result.trns";
                System.out.print(strExecCMD);
                Writer output = null;
                File file = new File(strDriveLetter + "/paymentGatewayTest/paymetTest.cmd");
                if (file.exists()) {
                    file.delete();
                }
                file.createNewFile();
                output = new BufferedWriter(new FileWriter(file));
                output.write(strExecCMD);
                output.close();

                Process p = Runtime.getRuntime().exec(strDriveLetter + "/paymentGatewayTest/paymetTest.cmd");
                BufferedReader brSucc = new BufferedReader(new InputStreamReader(p.getInputStream()));
                BufferedReader brFail = new BufferedReader(new InputStreamReader(p.getErrorStream()));

                /*Success reponse from Process*/
                try {
                    while ((strTranID = brSucc.readLine()) != null) {
                        // System.out.println("Sucess strTranID = " + strTranID+" strTranID.indexOf(TRANSACTION_ID)  "+strTranID.startsWith("TRANSACTION_ID") );
                        if (strTranID.indexOf("TRANSACTION_ID") >= 0) {
                            onlinePaymentStatusSuccess = true;
                            break;
                        }
                    }
                    if (onlinePaymentStatusSuccess) {
                        //System.out.println("Org strTranID = " + strTranID);
                        strTranID = URLEncoder.encode(strTranID.substring(16), "UTF-8");
                    }
                } catch (IOException e) {
                    onlinePaymentStatusSuccess = false;
                    e.printStackTrace();
                    AppExceptionHandler excpHandle = new AppExceptionHandler();
                    excpHandle.stack2string(e);
                }
                brSucc.close();
                //System.out.println("After success and encode strTranID = " + strTranID);

                /*Fail reponse from Process*/
                if (!onlinePaymentStatusSuccess) {
                    try {
                        while ((strTranID = brFail.readLine()) != null) {
                            onlinePaymentStatusSuccess = false;
                        }
                    } catch (IOException e) {
                        onlinePaymentStatusSuccess = false;
                        e.printStackTrace();
                        AppExceptionHandler excpHandle = new AppExceptionHandler();
                        excpHandle.stack2string(e);
                    }
                    brFail.close();
                }
            } else if (onlinePaymentDtBean.getPaymentGateway().equals("PaymentGatewayURLBRAC")) { //BRAC
                Random random = new Random();
                char[] digits = new char[10];
                digits[0] = (char) (random.nextInt(9) + '1');
                for (int i = 1; i < 10; i++) {
                    digits[i] = (char) (random.nextInt(10) + '0');
                }
                strTranID = new String(digits);
            }
        } catch (IOException e1) {
            onlinePaymentStatusSuccess = false;
            e1.printStackTrace();
            AppExceptionHandler excpHandle = new AppExceptionHandler();
            excpHandle.stack2string(e1);
        }

        if (strTranID != null) {
            onlineId = insertFinalValueRequest(onlinePaymentDtBean, userId, stripAddress, desc, strTranID);
            sb.append(strTranID);
            sb.append(",");
            sb.append(onlineId);
        } else {
            sb.append("error");
        }
        return sb.toString();
    }

    /**
     * Return Online payment response which will return response.
     * @param onlinePaymentDtBean Bean Object which will store online payment data
     * @param stripAddress Client IP Address
     * @param userId Online payment requested user id/ logged in user id
     * @return CSV Result/Response  of online payment.
     */
    private String onlinePyamentResult(OnlinePaymentDtBean onlinePaymentDtBean, String stripAddress, String userId) {
        String op = null;
        try {
            String strExecCMD = "java -jar " + strDriveLetter + "/paymentGatewayTest/ecomm_merchant.jar  " + strDriveLetter + "/paymentGatewayTest/merchant.properties -c " + URLDecoder.decode(onlinePaymentDtBean.getOnlineTraId(), "UTF-8") + " " + stripAddress;
            String strTranID = null;
            Writer output = null;
            String strResultCode = null;
            String cardNumber = null;

            File file = new File(strDriveLetter + "/paymentGatewayTest/paymetResult.cmd");
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            output = new BufferedWriter(new FileWriter(file));
            output.write(strExecCMD);
            output.close();

            Process p = Runtime.getRuntime().exec(strDriveLetter + "/paymentGatewayTest/paymetResult.cmd");
            BufferedReader brSucc = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader brFail = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            
            /*Success reponse from Process*/
            try {
                int i = 0;
                while ((strTranID = brSucc.readLine()) != null) {
                     if (strTranID.indexOf("RESULT_CODE") >= 0 || strTranID.indexOf("RESULT") >= 0 || strTranID.indexOf("APPROVAL_CODE") >= 0 || strTranID.indexOf("RRN") >= 0 || strTranID.indexOf("CARD_NUMBER") >= 0) {
                        i++;
                        if (i == 1) {
                            strResultCode = "";
                        }
                        strResultCode += strTranID + ",";
                        if (strTranID.indexOf("CARD_NUMBER") >= 0) {
                            cardNumber = strTranID.substring(12);
                            
                        }
                    }
                    //if(i>=5){break;}
                }
                /*Fail reponse from Process*/
                //System.out.print("strResultCode:"+strResultCode);
                i = 0;
                if (strResultCode == null || "".equalsIgnoreCase(strResultCode.trim())) {
                    while ((strTranID = brFail.readLine()) != null) {
                        if (strTranID.indexOf("RESULT_CODE") >= 0 || strTranID.indexOf("RESULT") >= 0 || strTranID.indexOf("APPROVAL_CODE") >= 0 || strTranID.indexOf("RRN") >= 0 || strTranID.indexOf("CARD_NUMBER") >= 0) {
                            i++;
                            if (i == 1) {
                                strResultCode = "";
                            }
                            strResultCode += strTranID + ",";
                            if (strTranID.indexOf("CARD_NUMBER") >= 0) {
                                cardNumber = strTranID.substring(12);
                               
                            }
                        }
                        //if(i>=5){break;}
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                AppExceptionHandler excpHandle = new AppExceptionHandler();
                excpHandle.stack2string(e);
            }

            brSucc.close();
            if (strResultCode != null && strResultCode.trim().length() > 0) {
                op = updateFinalValueRequest(onlinePaymentDtBean, userId, strResultCode, cardNumber);
            }
        } catch (IOException e1) {
            e1.printStackTrace();
            AppExceptionHandler excpHandle = new AppExceptionHandler();
            excpHandle.stack2string(e1);
        }
        //  System.out.println("Result of OP " + op);
        return op;
    }

    /**
     * Actual entry in online payment table when transaction started.
     * @param onlinePaymentDtBean Bean Object which will store online payment data
     * @param userId Online payment requested user id/ logged in user id
     * @param stripAddress Client IP Address
     * @param desc Description/Remarks of payment
     * @param transId Online payment TransactionId
     * @return newly inserted record id
     */
    private int insertFinalValueRequest(OnlinePaymentDtBean onlinePaymentDtBean, String userId, String stripAddress, String desc, String transId) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
        double serAmt = (Double.parseDouble(onlinePaymentDtBean.getServiceCharge()) * Double.parseDouble(onlinePaymentDtBean.getAmt())) / 100;
        String transStatus = "Pending";
        String strResp = "Transection started.... waiting for response...";
        int genId = 0;
        try {
            //   System.out.println("payment gateway bank name :" + XMLReader.getBankMessage(onlinePaymentDtBean.getPaymentGateway().replace("URL", "")));
            // commented by shreyansh shah to insert payment gateway
            //String xmldata = "<tbl_OnlinePaymentTransDetails userId=\"" + userId + "\" emailId=\"" + onlinePaymentDtBean.getEmailId() + "\" TransId=\"" + URLDecoder.decode(transId, "UTF-8") + "\" Amount=\"" + onlinePaymentDtBean.getAmt() + "\" serviceChargeAmt=\"" + serAmt + "\" TotalAmount=\"" + onlinePaymentDtBean.getTotal() + "\" Description=\"" + desc.replaceAll("_", " ") + "\"  TransStDateTime=\"" + sdf.format(new Date()) + "\" TransactionStatus=\"" + transStatus + "\" PaymentFor=\"" + onlinePaymentDtBean.getFromWhere() + "\" TenderId=\"" + onlinePaymentDtBean.getTenderId() + "\" ResponseMsg=\"" + strResp + "\" PaymentGateway=\"" + XMLReader.getMessage("PaymentGateway") + "\" IPAddressOfPayment=\"" + stripAddress + "\" />";
            String xmldata = "";
            if (onlinePaymentDtBean.getPaymentDesc()!=null && onlinePaymentDtBean.getPaymentDesc().equalsIgnoreCase("Payment for Advertisement")) {
                xmldata = "<tbl_OnlinePaymentTransDetails userId=\"" + userId + "\" emailId=\"" + onlinePaymentDtBean.getEmailId() + "\" TransId=\"" + URLDecoder.decode(transId, "UTF-8") + "\" Amount=\"" + onlinePaymentDtBean.getAmt() + "\" serviceChargeAmt=\"" + serAmt + "\" TotalAmount=\"" + onlinePaymentDtBean.getTotal() + "\" Description=\"" + "Payment for Advertisement" + "\"  TransStDateTime=\"" + sdf.format(new Date()) + "\" TransactionStatus=\"" + transStatus + "\" PaymentFor=\"" + "Payment for Advertisement" + "\" TenderId=\"" + onlinePaymentDtBean.getTenderId() + "\" ResponseMsg=\"" + strResp + "\" PaymentGateway=\"" + XMLReader.getBankMessage(onlinePaymentDtBean.getPaymentGateway().replace("URL", "")) + "\" IPAddressOfPayment=\"" + stripAddress + "\" />";
            } else {
                xmldata = "<tbl_OnlinePaymentTransDetails userId=\"" + userId + "\" emailId=\"" + onlinePaymentDtBean.getEmailId() + "\" TransId=\"" + URLDecoder.decode(transId, "UTF-8") + "\" Amount=\"" + onlinePaymentDtBean.getAmt() + "\" serviceChargeAmt=\"" + serAmt + "\" TotalAmount=\"" + onlinePaymentDtBean.getTotal() + "\" Description=\"" + desc.replaceAll("_", " ") + "\"  TransStDateTime=\"" + sdf.format(new Date()) + "\" TransactionStatus=\"" + transStatus + "\" PaymentFor=\"" + onlinePaymentDtBean.getFromWhere() + "\" TenderId=\"" + onlinePaymentDtBean.getTenderId() + "\" ResponseMsg=\"" + strResp + "\" PaymentGateway=\"" + XMLReader.getBankMessage(onlinePaymentDtBean.getPaymentGateway().replace("URL", "")) + "\" IPAddressOfPayment=\"" + stripAddress + "\" />";
            }
            xmldata = "<root>" + xmldata + "</root>";
            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
            CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_OnlinePaymentTransDetails", xmldata, "").get(0);
            genId = commonMsgChk.getId();
            if ("fromvas".equalsIgnoreCase(onlinePaymentDtBean.getFromWhere())) {
                String xmlvasdata = "<root><tbl_RegVasfeePayment paymentDesc=\"" + onlinePaymentDtBean.getPaymentDesc().replaceAll("_", " ") + "\" bankName=\"" + XMLReader.getBankMessage(onlinePaymentDtBean.getPaymentGateway().replace("URL", "")) + "\" currancy=\"BTN" + "\" Amount=\"" + onlinePaymentDtBean.getAmt() + "\" paymentType=\"online" + "\" remarks=\"" + onlinePaymentDtBean.getRemarks() + "\" transId=\"" + URLDecoder.decode(transId, "UTF-8") + "\" payeeName=\"" + onlinePaymentDtBean.getPayeeName() + "\" payeeAddress=\"" + onlinePaymentDtBean.getPayeeAddress() + "\" emailId=\"" + onlinePaymentDtBean.getEmailId() + "\" /></root>";
                CommonMsgChk commonvasMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_RegVasfeePayment", xmlvasdata, "").get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
            AppExceptionHandler excpHandle = new AppExceptionHandler();
            excpHandle.stack2string(e);
        }
        return genId;
    }

    /**
     * This method is used to updated all the respective table based on response received from payment gateway.
     * @param onlinePaymentDtBean
     * @param userId logged in userID
     * @param comments Comments for current payment
     * @param cardNumber From which payment has been done
     * @return CSV of actual response from payment gateway
     */
    private String updateFinalValueRequest(OnlinePaymentDtBean onlinePaymentDtBean, String userId, String comments, String cardNumber) {
        int genId = 0;
        StringBuilder sb = new StringBuilder();
        String strDate = "";
        String updateData = "";
        try {
            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
            strDate = sdf.format(new Date());
            if (onlinePaymentDtBean.getPaymentGateway().equals("PaymentGatewayURLDBBL")) {
                updateData = "ResponseMsg='" + comments + "', TransactionStatus='" + (comments.indexOf("000") > 0 && comments.indexOf("OK") > 0 ? "Successful" : "Fail") + "', TransEnDateTime='" + strDate + "'";
            } else if (onlinePaymentDtBean.getPaymentGateway().equals("PaymentGatewayURLBRAC")) {
                updateData = "ResponseMsg='" + comments + "', TransactionStatus='" + (comments.indexOf("<txn_status>ACCEPTED</txn_status>") > 0 ? "Successful" : "Fail") + "', TransEnDateTime='" + strDate + "'";
                if (cardNumber.equals("xxxxxxx") && comments.indexOf("<acc_no>") >= 0) {
                    cardNumber = comments.trim().substring(comments.indexOf("<acc_no>"),comments.indexOf("</acc_no>")).replace("<acc_no>", "");
                }
            }
            String whereCondition = "onlinePaymentID=" + onlinePaymentDtBean.getOnlineId() + " and TransId = '" + URLDecoder.decode(onlinePaymentDtBean.getOnlineTraId(), "UTF-8") + "'";
            CommonMsgChk commonMsgChk = new CommonMsgChk();

            commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_OnlinePaymentTransDetails", updateData, whereCondition).get(0);
            System.out.print(" Online data update : " + commonMsgChk.getFlag());
            if (commonMsgChk.getFlag()) {
                genId = onlinePaymentDtBean.getOnlineId();
            }

            sb.append(genId);
            System.out.println("generated id is :" + genId);
            System.out.println(" -------------Full Response Message----------- " + comments);
            if (genId != 0 && ((comments.indexOf("000") > 0 && comments.indexOf("OK") > 0) || comments.indexOf("<txn_status>ACCEPTED</txn_status>") > 0)) {
                genId = 0;
                String dtXml = "<tbl_RegFeePayment paymentFor=\"Registration Fee\" userId=\"" + userId + "\" createdBy=\"0\" bankName=\"" + XMLReader.getBankMessage(onlinePaymentDtBean.getPaymentGateway().replace("URL", "")) + "\" branchName=\"-\" currency=\"BTN\" amount=\"" + onlinePaymentDtBean.getAmt() + "\" paymentInstType=\"Online\" instRefNumber=\"" + cardNumber + "\" issuanceBank=\" \" issuanceBranch=\" \" issuanceDate=\" \" instValidityDt=\" \" dtOfPayment=\"" + strDate + "\" comments=\"" + strDesc.replaceAll("_", " ") + "\" paymentMode=\"Online\" eSignature=\"\" status=\"Paid\" isVerified=\"yes\" isLive=\"yes\" dtOfAction=\"" + strDate + "\" validityPeriodRef=\"1\" onlineTransId=\"" + URLDecoder.decode(onlinePaymentDtBean.getOnlineTraId(), "UTF-8") + "\" />";
                String tblName = "tbl_RegFeePayment";
                String vDtXml = "";
                String vTblName = "tbl_PaymentVerification";
                boolean isDocFees = false;

                if ("docFees".equalsIgnoreCase(onlinePaymentDtBean.getFromWhere())) {
                    isDocFees = true;
                    dtXml = "<tbl_TenderPayment paymentFor=\"Document Fees\" paymentInstType=\"Online\" instRefNumber=\"" + cardNumber + "\" amount=\"" + onlinePaymentDtBean.getAmt() + "\" instDate=\" \" instValidUpto=\" \" bankName=\"" + XMLReader.getBankMessage(onlinePaymentDtBean.getPaymentGateway().replace("URL", "")) + "\" branchName=\"-\" comments=\"" + strDesc.replaceAll("_", " ") + "\" tenderId=\"" + onlinePaymentDtBean.getTenderId() + "\" userId=\"" + userId + "\" createdBy=\"0\" createdDate=\"" + strDate + "\" pkgLotId=\"0\" eSignature=\"\" status=\"paid\" paymentMode=\"Online\" extValidityRef=\"0\" currency=\"BTN\" issuanceBank=\" \" issuanceBranch=\" \" isVerified=\"Yes\" isLive=\"Yes\" dtOfAction=\"" + strDate + "\" OnlineTransId=\"" + URLDecoder.decode(onlinePaymentDtBean.getOnlineTraId(), "UTF-8") + "\" />";
                    vTblName = "tbl_TenderPaymentVerification";
                    tblName = "tbl_TenderPayment";
                } else if ("renewal".equalsIgnoreCase(onlinePaymentDtBean.getFromWhere())) {
                    tblName = "tbl_RegFeePayment";
                    dtXml = "<tbl_RegFeePayment paymentFor=\"Registration Fee\" userId=\"" + userId + "\" createdBy=\"0\" bankName=\"" + XMLReader.getBankMessage(onlinePaymentDtBean.getPaymentGateway().replace("URL", "")) + "\" branchName=\"-\" currency=\"BTN\" amount=\"" + onlinePaymentDtBean.getAmt() + "\" paymentInstType=\"Online\" instRefNumber=\"" + cardNumber + "\" issuanceBank=\" \" issuanceBranch=\" \" issuanceDate=\" \" instValidityDt=\" \" dtOfPayment=\"" + strDate + "\" comments=\"" + strDesc.replaceAll("_", " ") + "\" paymentMode=\"Online\" eSignature=\"\" status=\"renewed\" isVerified=\"yes\" isLive=\"yes\" dtOfAction=\"" + strDate + "\" validityPeriodRef=\"1\" onlineTransId=\"" + URLDecoder.decode(onlinePaymentDtBean.getOnlineTraId(), "UTF-8") + "\" />";
                    vTblName = "tbl_PaymentVerification";
                }
                System.out.print("From  where " + onlinePaymentDtBean.getFromWhere());
                if ("fromvas".equalsIgnoreCase(onlinePaymentDtBean.getFromWhere())) {
                    updateData = "cardno='" + cardNumber + "', dtofPayment='" + strDate + "'";
                    whereCondition = "transId='" + onlinePaymentDtBean.getOnlineTraId() + "'";
                    commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_RegVasfeePayment", updateData, whereCondition).get(0);
                    System.out.print(" Vas data update : " + commonMsgChk.getFlag());
                    if (commonMsgChk.getFlag()) {
                        genId = onlinePaymentDtBean.getOnlineId();
                    }
                } else {
                    dtXml = "<root>" + dtXml + "</root>";
                    System.out.println(dtXml + " Data " + tblName + " TableName");
                    commonMsgChk = commonXMLSPService.insertDataBySP("insert", tblName, dtXml, "").get(0);
                    genId = commonMsgChk.getId();

                    if ("renewal".equalsIgnoreCase(onlinePaymentDtBean.getFromWhere())) {
                        commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_LoginMaster", "validUpTo=dateadd(m,12,validUpTo)", "userId=" + userId).get(0);
                    }

                    if (genId > 0) {
                        if (isDocFees) {
                            vDtXml = "<tbl_TenderPaymentVerification paymentId=\"" + genId + "\" verifiedBy=\"0\" remarks=\"" + strDesc.replaceAll("_", " ") + "\" verificationDt=\"" + strDate + "\" verifyPartTransId=\"0\" />";
                        } else {
                            vDtXml = "<tbl_PaymentVerification paymentId=\"" + genId + "\" verifiedBy=\"0\" remarks=\"" + strDesc.replaceAll("_", " ") + "\" verificationDt=\"" + strDate + "\" verifyPartTransId=\"0\" />";
                        }
                        vDtXml = "<root>" + vDtXml + "</root>";
                        commonMsgChk = commonXMLSPService.insertDataBySP("insert", vTblName, vDtXml, "").get(0);
                    }

                    if (commonMsgChk.getId() > 0) {
                        sb.append(",");
                        sb.append(genId);
                        sb.append(",");
                        sb.append(cardNumber);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            AppExceptionHandler excpHandle = new AppExceptionHandler();
            excpHandle.stack2string(e);
        }
        System.out.println(sb.toString() + " Final sb To return");
        if (genId > 0) {
            return comments + "PAYMENT_DATETIME :" + strDate;
        } else {
            return "";
        }
    }

    /**
     * To send mail After successful transaction.
     * @param list List of the data to whom send mail
     * @param userId userid to whom send mail
     * @return True in case of sent mail successfully other wise false
     */
    public boolean sendMsg(List list, String userId, String emailID) {
        boolean flag = false;
        String mailId = "";
        SendMessageUtil sendMessageUtil = new SendMessageUtil();
        if (!userId.equals("0") && emailID.trim().equals("")) {
            CommonService urs = (CommonService) AppContext.getSpringBean("CommonService");
            mailId = urs.getEmailId(userId);
        } else {
            mailId = emailID;
        }
        UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
        registerService.contentAdmMsgBox(mailId, XMLReader.getMessage("emailIdNoReply"), (String) list.get(0), (String) list.get(1) + (String) list.get(3));
        sendMessageUtil.setEmailTo(new String[]{mailId});
        sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
        sendMessageUtil.setEmailSub((String) list.get(0));
        sendMessageUtil.setEmailMessage((String) list.get(1) + (String) list.get(2) + (String) list.get(3));
        sendMessageUtil.sendEmail();

        return flag = true;
    }

    /**
     * To Verify all mail id who is requesting for vas service.
     * @param mailId Verify Mailid for VAS Service
     * @return OK in case of new/Email id already exists in case of e-Mail id exists
     */
    private String verifyMail(String mailId) {
        String msg = "";
        if (!mailId.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9\\-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
            return msg = "Please enter valid e-mail ID";
        }
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        msg = commonService.verifyMailforPaymentGateway(mailId);
        return msg;
    }
}
