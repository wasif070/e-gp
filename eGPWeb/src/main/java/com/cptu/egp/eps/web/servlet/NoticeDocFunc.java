/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import java.io.InputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import java.io.FileInputStream;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import javax.servlet.ServletOutputStream;

/**
 *
 * @author rishita
 */
public class NoticeDocFunc extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final String DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("AppDownloadDocs");
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();


        File file = null;
        String docSizeMsg = "";
        boolean checkret = false;
        boolean flag = false;
        String pckNo = "";
        String desc = "";
        String funName = "";
        String FileName = "";
        long FileSize = 0;
        String docsBrief = "";
        int appId = 0;
        int pckId = 0;
        String fileName = "";
        String documentBrief = "";
        String sectionId = "";
        HttpSession hs = request.getSession();
        long fileSize = 0;
        String queryString = "";
        String tenderid = "";
        //String pageName = "officer/Notice.jsp";
        response.setContentType("text/html");
        boolean dis = false;

        if (session.getAttribute("userId") == null) {
            response.sendRedirect("SessionTimedOut.jsp");
        } else {
            try {
                if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("remove")) {
                        String tender = "";
                        int docId = 0;
                        String id = "";
                        String docName = "";
                        if (request.getParameter("docId") != null) {
                            docId = Integer.parseInt(request.getParameter("docId"));

                        }
                        if (request.getParameter("appId") != null) {
                            appId = Integer.parseInt(request.getParameter("appId"));

                        }
                        if (request.getParameter("pckId") != null) {
                            pckId = Integer.parseInt(request.getParameter("pckId"));

                        }

                        if (request.getParameter("pckNo") != null) {
                            pckNo = (request.getParameter("pckNo"));

                        }
                        if (request.getParameter("desc") != null) {
                            desc = (request.getParameter("desc"));

                        }
                        if (request.getParameter("docName") != null) {
                            docName = request.getParameter("docName");
                        }
                        if (request.getParameter("tenderid") != null) {
                            tenderid = request.getParameter("tenderid");
                        }
                        String whereContition = "";
                        whereContition = "appEngEstId= " + docId;
                        System.out.println("whereCondition\t" + whereContition);
                        int j = docName.lastIndexOf('.');
                        String lst = docName.substring(j + 1);
                        String fst = docName.substring(0, j);
                        fileName = fst + "_" + docId + "." + lst;
                        file = new File(DESTINATION_DIR_PATH + appId + "\\" + fileName);
                        System.out.println("fileName" + fileName);
                        fileName = file.toString();
                        checkret = deleteFile(fileName);
                        if (checkret) {
                            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                            CommonMsgChk commonMsgChk = null;
                        try {
                            commonMsgChk = commonXMLSPService.insertDataBySP("delete", "tbl_AppEngEstDoc", "", whereContition).get(0);
                        } catch (Exception ex) {
                            Logger.getLogger(NoticeDocFunc.class.getName()).log(Level.SEVERE, null, ex);
                        }
                            System.out.println("commonMsgChk \t" + commonMsgChk.getMsg());
                            queryString = "?tenderid=" + tenderid;
                            response.sendRedirect("officer/Notice.jsp"+ queryString);
                        }

                    } else if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("download")) {
                        String docName = request.getParameter("docName");
                        String docId = request.getParameter("docId");
                        int j = docName.lastIndexOf('.');
                        String lst = docName.substring(j + 1);
                        String fst = docName.substring(0, j);
                        fileName = fst + "_" + docId + "." + lst;
                        file = new File(DESTINATION_DIR_PATH + request.getParameter("appId") + "\\" + fileName);//request.getSession().getAttribute("userId")
                        InputStream fis = new FileInputStream(file);
                        byte[] buf = new byte[Integer.valueOf(request.getParameter("docSize"))];
                        int offset = 0;
                        int numRead = 0;
                        while ((offset < buf.length)
                                && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {

                            offset += numRead;

                        }
                        fis.close();
                        response.setContentType("application/octet-stream");
                        response.setHeader("Content-Disposition", "attachment;filename=\"" + request.getParameter("docName") + "\"");
                        ServletOutputStream outputStream = response.getOutputStream();
                        outputStream.write(buf);
                        outputStream.flush();
                        outputStream.close();
                    }
            } finally {
                //out.close();
            }
        }
    }
    public boolean deleteFile(String filePath) {

        File f = new File(filePath);
        if (f.delete()) {
            return true;
        } else {
            return false;
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
