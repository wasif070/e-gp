/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblHelpManual;
import com.cptu.egp.eps.service.serviceinterface.HelpManualService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class HelpManualSrBean {

    private final HelpManualService helpManualService = (HelpManualService) AppContext.getSpringBean("HelpManualService");
    private final TblHelpManual tblhelpManual = new TblHelpManual();

    private static final Logger LOGGER = Logger.getLogger(HelpManualSrBean.class);

    private String logUserId="0";
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        helpManualService.setAuditTrail(auditTrail);
    }
    
    

    /**
     * Add User Id at Bean Level.
     * @param logUserId 
     */
    public void setLogUserId(String logUserId) {
        helpManualService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

   
    /**
       Add Help Manual Data
     * @param helpURL
     * @param helpContent
     * @return String 
     */
    public String addHelpManual(String helpURL,String helpContent){
        LOGGER.debug("addHelpManual : "+logUserId+" Starts ");
        String msg = null;
        tblhelpManual.setHelpUrl(helpURL);
        tblhelpManual.setHelpContent(helpContent);
        tblhelpManual.setEntryDate(new java.util.Date());
        tblhelpManual.setIsDeleted("no");   
        try{
            msg = helpManualService.addHelpManual(tblhelpManual);
        }catch(Exception e){
            LOGGER.error("addHelpManual : "+logUserId+" : "+e);
        }
        LOGGER.debug("addHelpManual : "+logUserId+" Ends ");
            return msg;
        }

    /**
     * Change Status of the Help Manual Id
     * @param id
     * @return String 
     */
    public String changeStatus(int id){
        LOGGER.debug("changeStatus : "+logUserId+" Starts ");
        String msg = "";
        try {
        msg = helpManualService.changeStatus(id);
        } catch (Exception e) {
            LOGGER.error("changeStatus : "+logUserId+" : "+e);
        }
        LOGGER.debug("changeStatus : "+logUserId+" Ends ");
        return msg;
    }

    /**
     * Undo status of the Help Manual
     * @param id
     * @return String
     */
    public String undoStatus(int id){
        LOGGER.debug("undoStatus : "+logUserId+" Starts ");
        String msg = "";
        try {
        msg = helpManualService.undoStatus(id);
        } catch (Exception e) {
            LOGGER.error("undoStatus : "+logUserId+" : "+e);
        }
        LOGGER.debug("undoStatus : "+logUserId+" Ends ");
        return msg;
    }
    
    /**
     * Update Help Manual data 
     * @param helpURL
     * @param helpContent
     * @param id
     * @return String
     */
    public String updateManual(String helpURL,String helpContent,int id){
        LOGGER.debug("updateManual : "+logUserId+" Starts ");
        String msg = null;
        try{
            tblhelpManual.setHelpManualId(id);
            tblhelpManual.setHelpUrl(helpURL);
            tblhelpManual.setHelpContent(helpContent);
            tblhelpManual.setEntryDate(new java.util.Date());
            tblhelpManual.setIsDeleted("no");
            msg = helpManualService.updateHelpManualDetails(tblhelpManual);
        }catch(Exception e){
           LOGGER.error("updateManual : "+logUserId+" : "+e);
        }
        LOGGER.debug("updateManual : "+logUserId+" Ends ");
            return msg;
        }
    
    /**
     * Get All Help Manuals in given Order and given offset.
     * @param offset
     * @param row
     * @param orderby
     * @param order
     * @return List of TblHelpManual objects
     */
    public List<TblHelpManual> getAllHelpManuals(int offset,int row,String orderby,String order){
        LOGGER.debug("getAllHelpManuals : "+logUserId+" Starts ");
        List<TblHelpManual> helpManualList = null;
       try{
            helpManualList = helpManualService.getAllHelpManual(offset,row,orderby, order); //calling Service Layer method.    
       }catch(Exception e){
           LOGGER.error("getAllHelpManuals : "+logUserId+" : "+e);
       }
       LOGGER.debug("getAllHelpManuals : "+logUserId+" Ends ");
       return helpManualList;
   }

    /**
     * Search Help Manual Data by given field.
     * @param offset
     * @param row
     * @param searchField
     * @param searchString
     * @param searchOper
     * @param orderby
     * @param order
     * @return List of TblHelpManual objects
     */
    public List<TblHelpManual> searchHelpManual(int offset,int row,String searchField,String searchString,String searchOper,String orderby,String order){
       LOGGER.debug("searchHelpManual : "+logUserId+" Starts ");
       List<TblHelpManual> helpManualList = null;
       try{
            helpManualList = helpManualService.searchHelpManual(offset,row,searchField,searchString,searchOper,orderby, order); //calling Service Layer method.
       }catch(Exception e){
           LOGGER.error("searchHelpManual : "+logUserId+" : "+e);
       }
       LOGGER.debug("searchHelpManual : "+logUserId+" Ends ");
       return helpManualList;
   }
    /**
     * Get Help Manual Data by Page Name
     * @param pageName
     * @return TblHelpManual object
     */
    public TblHelpManual getHelpManuals(String pageName){
         LOGGER.debug("getHelpManuals : "+logUserId+" Starts ");
         TblHelpManual helpManualList = null;
       try{
            helpManualList = helpManualService.getHelpManual(pageName);
            
       }catch(Exception e){
           LOGGER.error("getHelpManuals : "+logUserId+" : "+e);
       }
       LOGGER.debug("getHelpManuals : "+logUserId+" Ends ");
       return helpManualList;
   }

    /**
     * Get Help Manual Data by id.
     * @param id
     * @return List of TblHelpManual objects
     */
    public List<TblHelpManual> getData(int id){
         LOGGER.debug("getData : "+logUserId+" Starts ");
         List<TblHelpManual> listhelp = null;
        try{
            listhelp = helpManualService.getData(id);
        }catch(Exception e){
           LOGGER.error("getData : "+logUserId+" : "+e);
    }
        LOGGER.debug("getData : "+logUserId+" Ends ");
        return listhelp;
    }

    /**
     * Delete Help Manual by given id
     * @param id
     * @return String
     */
    public String deleteHelpManual(int id){
        LOGGER.debug("deleteHelpManual : "+logUserId+" Starts ");
        String msg ="";
        try{
            tblhelpManual.setHelpManualId(id);
            tblhelpManual.setHelpUrl("-");
            tblhelpManual.setHelpContent("-");
            tblhelpManual.setEntryDate(new java.util.Date());
            msg = helpManualService.deleteHelpManual(tblhelpManual);
            
        }catch(Exception e){
            LOGGER.error("deleteHelpManual : "+logUserId+" : "+e);
        }
        LOGGER.debug("deleteHelpManual : "+logUserId+" Ends ");
        return msg;
    }

    /**
     * Get All Help Manual Count
     * @return long value having count for help manual
     */
    public long getHelpCount(){
        LOGGER.debug("getHelpCount : "+logUserId+" Starts ");
        long lng =0;
        try{
            lng = helpManualService.getTblHelpManualCount();
        }
        catch(Exception e){
            LOGGER.error("getHelpCount : "+logUserId+" : "+e);
            lng = 0;
        }
        LOGGER.debug("getHelpCount : "+logUserId+" Ends ");
        return lng;
    }
    /**
     * Get Search Help Manual Count
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return long as search count
     */
    public long getSearchCount(String searchField, String searchString, String searchOper){
         LOGGER.debug("getSearchCount : "+logUserId+" Starts ");
         long lng =0;
        try{
            lng = helpManualService.getSearchCount(searchField, searchString, searchOper);
        }
        catch(Exception e){
            LOGGER.error("getSearchCount : "+logUserId+" : "+e);
            lng = 0;
        }
        LOGGER.debug("getSearchCount : "+logUserId+" Ends ");
        return lng;
    }
}
