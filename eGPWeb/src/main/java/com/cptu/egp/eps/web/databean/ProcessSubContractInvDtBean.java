/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.databean;

import java.util.Date;
/**
 *
 * @author Administrator
 */
public class ProcessSubContractInvDtBean
{
     private Integer subConId;
     private Integer tenderId;
     private Integer invToUserId;
     private Integer invFromUserId;
     private String remarks;
     private Date lastAcceptDt;
     private Date invSentDt;
     private String invAcceptStatus;
     private Date invAcceptDate;
     private String acceptComments;
     private String emailId;

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }


    public String getAcceptComments() {
        return acceptComments;
    }

    public void setAcceptComments(String acceptComments) {
        this.acceptComments = acceptComments;
    }

    public Date getInvAcceptDate() {
        return invAcceptDate;
    }

    public void setInvAcceptDate(Date invAcceptDate) {
        this.invAcceptDate = invAcceptDate;
    }

    public String getInvAcceptStatus() {
        return invAcceptStatus;
    }

    public void setInvAcceptStatus(String invAcceptStatus) {
        this.invAcceptStatus = invAcceptStatus;
    }

    public Integer getInvFromUserId() {
        return invFromUserId;
    }

    public void setInvFromUserId(Integer invFromUserId) {
        this.invFromUserId = invFromUserId;
    }

    public Date getInvSentDt() {
        return invSentDt;
    }

    public void setInvSentDt(Date invSentDt) {
        this.invSentDt = invSentDt;
    }

    public Integer getInvToUserId() {
        return invToUserId;
    }

    public void setInvToUserId(Integer invToUserId) {
        this.invToUserId = invToUserId;
    }

    public Date getLastAcceptDt() {
        return lastAcceptDt;
    }

    public void setLastAcceptDt(Date lastAcceptDt) {
        this.lastAcceptDt = lastAcceptDt;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getSubConId() {
        return subConId;
    }

    public void setSubConId(Integer subConId) {
        this.subConId = subConId;
    }

    public Integer getTenderId() {
        return tenderId;
    }

    public void setTenderId(Integer tenderId) {
        this.tenderId = tenderId;
    }




}
