/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.offlinedata;

/**
 *
 * @author Administrator
 */
public class TenderFormExcellBean {

private String ministryName;
private String agency;
private String procuringEntityDistrict;
private String eventType;
private String procurementNature;
private String invitationFor;
private String procurementType;
private String procurementMethod;
private String cbobudget;
private String cbosource;
private String locationofDeliveryWorksConsultancy;
private String istheContractSignedwiththesamepersonstatedintheNOA;
private String wasthePerformanceSecurityprovidedinduetime;
private String wastheContractSignedinduetime;
private String procuringEntityName;
private String procuringEntityCode;
private String invitationRefNo;
private String tenderissuingdate;
private String developmentPartner;
private String projectOrProgrammeCode;
private String projectOrProgrammeName;
private String tenderPackageNo;
private String tenderPackageName;
private String tenderPublicationDate;
private String tenderLastSellingDate;
private String tenderClosingDate;
private String tenderOpeningDate;
private String sellingTenderDocumentPrincipal;
private String sellingTenderDocumentOthers;
private String receivingTenderDocument;
private String openingTenderDocument;
private String placeofTenderMeeting;
private String dateTimeofTenderMeeting;
private String eligibilityofTender;
private String briefDescriptionofGoodsorWorks;
private String briefDescriptionofRelatedServices;
private String tenderDocumentPrice;
private String lotNo_1;
private String identificationOfLot_1;
private String location_1;
private String tenderSecurityAmount_1;
private String completionTimeInWeeksORmonths_1;
private String lotNo_2;
private String identificationOfLot_2;
private String location_2;
private String tenderSecurityAmount_2;
private String completionTimeInWeeksORmonths_2;
private String lotNo_3;
private String identificationOfLot_3;
private String location_3;
private String tenderSecurityAmount_3;
private String completionTimeInWeeksORmonths_3;
private String lotNo_4;
private String identificationOfLot_4;
private String location_4;
private String tenderSecurityAmount_4;
private String completionTimeInWeeksORmonths_4;
private String nameofOfficialInvitingTender;
private String designationofOfficialInvitingTender;
private String addressofOfficialInvitingTender;
private String contactdetailsofOfficialInvitingTender;
private String faxNo;
private String e_mail;
private String dateofNotificationofAward;
private String dateofContractSigning;
private String proposedDateofContractCompletion;
private String noofTendersProposalsSold;
private String noofTendersProposalsReceived;
private String noofResponsiveTendersProposals;
private String briefDescriptionofContract;
private String contractValue;
private String nameofSupplierContractorConsultant;
private String locationofSupplierContractorConsultant;
private String ifNogivereasonwhy_NOA;
private String ifNoGiveReasonWhy_Performance_Security;
private String ifNoGiveReasonWhy_Contract_signed;

    /**
     * @return the ministryName
     */
    public String getMinistryName() {
        return ministryName;
    }

    /**
     * @param ministryName the ministryName to set
     */
    public void setMinistryName(String ministryName) {
        this.ministryName = ministryName;
    }

    /**
     * @return the agency
     */
    public String getAgency() {
        return agency;
    }

    /**
     * @param agency the agency to set
     */
    public void setAgency(String agency) {
        this.agency = agency;
    }

    /**
     * @return the procuringEntityDistrict
     */
    public String getProcuringEntityDistrict() {
        return procuringEntityDistrict;
    }

    /**
     * @param procuringEntityDistrict the procuringEntityDistrict to set
     */
    public void setProcuringEntityDistrict(String procuringEntityDistrict) {
        this.procuringEntityDistrict = procuringEntityDistrict;
    }

   
    /**
     * @return the cbobudget
     */
    public String getCbobudget() {
        return cbobudget;
    }

    /**
     * @param cbobudget the cbobudget to set
     */
    public void setCbobudget(String cbobudget) {
        this.cbobudget = cbobudget;
    }

    /**
     * @return the cbosource
     */
    public String getCbosource() {
        return cbosource;
    }

    /**
     * @param cbosource the cbosource to set
     */
    public void setCbosource(String cbosource) {
        this.cbosource = cbosource;
    }

    /**
     * @return the locationofDeliveryWorksConsultancy
     */
    public String getLocationofDeliveryWorksConsultancy() {
        return locationofDeliveryWorksConsultancy;
    }

    /**
     * @param locationofDeliveryWorksConsultancy the locationofDeliveryWorksConsultancy to set
     */
    public void setLocationofDeliveryWorksConsultancy(String locationofDeliveryWorksConsultancy) {
        this.locationofDeliveryWorksConsultancy = locationofDeliveryWorksConsultancy;
    }

    /**
     * @return the istheContractSignedwiththesamepersonstatedintheNOA
     */
    public String getIstheContractSignedwiththesamepersonstatedintheNOA() {
        return istheContractSignedwiththesamepersonstatedintheNOA;
    }

    /**
     * @param istheContractSignedwiththesamepersonstatedintheNOA the istheContractSignedwiththesamepersonstatedintheNOA to set
     */
    public void setIstheContractSignedwiththesamepersonstatedintheNOA(String istheContractSignedwiththesamepersonstatedintheNOA) {
        this.istheContractSignedwiththesamepersonstatedintheNOA = istheContractSignedwiththesamepersonstatedintheNOA;
    }

    /**
     * @return the wasthePerformanceSecurityprovidedinduetime
     */
    public String getWasthePerformanceSecurityprovidedinduetime() {
        return wasthePerformanceSecurityprovidedinduetime;
    }

    /**
     * @param wasthePerformanceSecurityprovidedinduetime the wasthePerformanceSecurityprovidedinduetime to set
     */
    public void setWasthePerformanceSecurityprovidedinduetime(String wasthePerformanceSecurityprovidedinduetime) {
        this.wasthePerformanceSecurityprovidedinduetime = wasthePerformanceSecurityprovidedinduetime;
    }

    /**
     * @return the wastheContractSignedinduetime
     */
    public String getWastheContractSignedinduetime() {
        return wastheContractSignedinduetime;
    }

    /**
     * @param wastheContractSignedinduetime the wastheContractSignedinduetime to set
     */
    public void setWastheContractSignedinduetime(String wastheContractSignedinduetime) {
        this.wastheContractSignedinduetime = wastheContractSignedinduetime;
    }

    /**
     * @return the procuringEntityName
     */
    public String getProcuringEntityName() {
        return procuringEntityName;
    }

    /**
     * @param procuringEntityName the procuringEntityName to set
     */
    public void setProcuringEntityName(String procuringEntityName) {
        this.procuringEntityName = procuringEntityName;
    }

    /**
     * @return the procuringEntityCode
     */
    public String getProcuringEntityCode() {
        return procuringEntityCode;
    }

    /**
     * @param procuringEntityCode the procuringEntityCode to set
     */
    public void setProcuringEntityCode(String procuringEntityCode) {
        this.procuringEntityCode = procuringEntityCode;
    }

    /**
     * @return the invitationRefNo
     */
    public String getInvitationRefNo() {
        return invitationRefNo;
    }

    /**
     * @param invitationRefNo the invitationRefNo to set
     */
    public void setInvitationRefNo(String invitationRefNo) {
        this.invitationRefNo = invitationRefNo;
    }

    /**
     * @return the tenderissuingdate
     */
    public String getTenderissuingdate() {
        return tenderissuingdate;
    }

    /**
     * @param tenderissuingdate the tenderissuingdate to set
     */
    public void setTenderissuingdate(String tenderissuingdate) {
        this.tenderissuingdate = tenderissuingdate;
    }

    /**
     * @return the developmentPartner
     */
    public String getDevelopmentPartner() {
        return developmentPartner;
    }

    /**
     * @param developmentPartner the developmentPartner to set
     */
    public void setDevelopmentPartner(String developmentPartner) {
        this.developmentPartner = developmentPartner;
    }

    /**
     * @return the projectOrProgrammeCode
     */
    public String getProjectOrProgrammeCode() {
        return projectOrProgrammeCode;
    }

    /**
     * @param projectOrProgrammeCode the projectOrProgrammeCode to set
     */
    public void setProjectOrProgrammeCode(String projectOrProgrammeCode) {
        this.projectOrProgrammeCode = projectOrProgrammeCode;
    }

    /**
     * @return the projectOrProgrammeName
     */
    public String getProjectOrProgrammeName() {
        return projectOrProgrammeName;
    }

    /**
     * @param projectOrProgrammeName the projectOrProgrammeName to set
     */
    public void setProjectOrProgrammeName(String projectOrProgrammeName) {
        this.projectOrProgrammeName = projectOrProgrammeName;
    }

    /**
     * @return the tenderPackageNo
     */
    public String getTenderPackageNo() {
        return tenderPackageNo;
    }

    /**
     * @param tenderPackageNo the tenderPackageNo to set
     */
    public void setTenderPackageNo(String tenderPackageNo) {
        this.tenderPackageNo = tenderPackageNo;
    }

    /**
     * @return the tenderPackageName
     */
    public String getTenderPackageName() {
        return tenderPackageName;
    }

    /**
     * @param tenderPackageName the tenderPackageName to set
     */
    public void setTenderPackageName(String tenderPackageName) {
        this.tenderPackageName = tenderPackageName;
    }

    /**
     * @return the tenderPublicationDate
     */
    public String getTenderPublicationDate() {
        return tenderPublicationDate;
    }

    /**
     * @param tenderPublicationDate the tenderPublicationDate to set
     */
    public void setTenderPublicationDate(String tenderPublicationDate) {
        this.tenderPublicationDate = tenderPublicationDate;
    }

    /**
     * @return the tenderLastSellingDate
     */
    public String getTenderLastSellingDate() {
        return tenderLastSellingDate;
    }

    /**
     * @param tenderLastSellingDate the tenderLastSellingDate to set
     */
    public void setTenderLastSellingDate(String tenderLastSellingDate) {
        this.tenderLastSellingDate = tenderLastSellingDate;
    }

    /**
     * @return the tenderClosingDate
     */
    public String getTenderClosingDate() {
        return tenderClosingDate;
    }

    /**
     * @param tenderClosingDate the tenderClosingDate to set
     */
    public void setTenderClosingDate(String tenderClosingDate) {
        this.tenderClosingDate = tenderClosingDate;
    }

    /**
     * @return the tenderOpeningDate
     */
    public String getTenderOpeningDate() {
        return tenderOpeningDate;
    }

    /**
     * @param tenderOpeningDate the tenderOpeningDate to set
     */
    public void setTenderOpeningDate(String tenderOpeningDate) {
        this.tenderOpeningDate = tenderOpeningDate;
    }

    /**
     * @return the sellingTenderDocumentPrincipal
     */
    public String getSellingTenderDocumentPrincipal() {
        return sellingTenderDocumentPrincipal;
    }

    /**
     * @param sellingTenderDocumentPrincipal the sellingTenderDocumentPrincipal to set
     */
    public void setSellingTenderDocumentPrincipal(String sellingTenderDocumentPrincipal) {
        this.sellingTenderDocumentPrincipal = sellingTenderDocumentPrincipal;
    }

    /**
     * @return the sellingTenderDocumentOthers
     */
    public String getSellingTenderDocumentOthers() {
        return sellingTenderDocumentOthers;
    }

    /**
     * @param sellingTenderDocumentOthers the sellingTenderDocumentOthers to set
     */
    public void setSellingTenderDocumentOthers(String sellingTenderDocumentOthers) {
        this.sellingTenderDocumentOthers = sellingTenderDocumentOthers;
    }

    /**
     * @return the receivingTenderDocument
     */
    public String getReceivingTenderDocument() {
        return receivingTenderDocument;
    }

    /**
     * @param receivingTenderDocument the receivingTenderDocument to set
     */
    public void setReceivingTenderDocument(String receivingTenderDocument) {
        this.receivingTenderDocument = receivingTenderDocument;
    }

    /**
     * @return the openingTenderDocument
     */
    public String getOpeningTenderDocument() {
        return openingTenderDocument;
    }

    /**
     * @param openingTenderDocument the openingTenderDocument to set
     */
    public void setOpeningTenderDocument(String openingTenderDocument) {
        this.openingTenderDocument = openingTenderDocument;
    }

    /**
     * @return the placeofTenderMeeting
     */
    public String getPlaceofTenderMeeting() {
        return placeofTenderMeeting;
    }

    /**
     * @param placeofTenderMeeting the placeofTenderMeeting to set
     */
    public void setPlaceofTenderMeeting(String placeofTenderMeeting) {
        this.placeofTenderMeeting = placeofTenderMeeting;
    }

    /**
     * @return the dateTimeofTenderMeeting
     */
    public String getDateTimeofTenderMeeting() {
        return dateTimeofTenderMeeting;
    }

    /**
     * @param dateTimeofTenderMeeting the dateTimeofTenderMeeting to set
     */
    public void setDateTimeofTenderMeeting(String dateTimeofTenderMeeting) {
        this.dateTimeofTenderMeeting = dateTimeofTenderMeeting;
    }

    /**
     * @return the eligibilityofTender
     */
    public String getEligibilityofTender() {
        return eligibilityofTender;
    }

    /**
     * @param eligibilityofTender the eligibilityofTender to set
     */
    public void setEligibilityofTender(String eligibilityofTender) {
        this.eligibilityofTender = eligibilityofTender;
    }

    /**
     * @return the briefDescriptionofGoodsorWorks
     */
    public String getBriefDescriptionofGoodsorWorks() {
        return briefDescriptionofGoodsorWorks;
    }

    /**
     * @param briefDescriptionofGoodsorWorks the briefDescriptionofGoodsorWorks to set
     */
    public void setBriefDescriptionofGoodsorWorks(String briefDescriptionofGoodsorWorks) {
        this.briefDescriptionofGoodsorWorks = briefDescriptionofGoodsorWorks;
    }

    /**
     * @return the briefDescriptionofRelatedServices
     */
    public String getBriefDescriptionofRelatedServices() {
        return briefDescriptionofRelatedServices;
    }

    /**
     * @param briefDescriptionofRelatedServices the briefDescriptionofRelatedServices to set
     */
    public void setBriefDescriptionofRelatedServices(String briefDescriptionofRelatedServices) {
        this.briefDescriptionofRelatedServices = briefDescriptionofRelatedServices;
    }

    /**
     * @return the tenderDocumentPrice
     */
    public String getTenderDocumentPrice() {
        return tenderDocumentPrice;
    }

    /**
     * @param tenderDocumentPrice the tenderDocumentPrice to set
     */
    public void setTenderDocumentPrice(String tenderDocumentPrice) {
        this.tenderDocumentPrice = tenderDocumentPrice;
    }

    /**
     * @return the lotNo_1
     */
    public String getLotNo_1() {
        return lotNo_1;
    }

    /**
     * @param lotNo_1 the lotNo_1 to set
     */
    public void setLotNo_1(String lotNo_1) {
        this.lotNo_1 = lotNo_1;
    }

    /**
     * @return the identificationOfLot_1
     */
    public String getIdentificationOfLot_1() {
        return identificationOfLot_1;
    }

    /**
     * @param identificationOfLot_1 the identificationOfLot_1 to set
     */
    public void setIdentificationOfLot_1(String identificationOfLot_1) {
        this.identificationOfLot_1 = identificationOfLot_1;
    }

    /**
     * @return the location_1
     */
    public String getLocation_1() {
        return location_1;
    }

    /**
     * @param location_1 the location_1 to set
     */
    public void setLocation_1(String location_1) {
        this.location_1 = location_1;
    }

    /**
     * @return the completionTimeInWeeksORmonths_1
     */
    public String getCompletionTimeInWeeksORmonths_1() {
        return completionTimeInWeeksORmonths_1;
    }

    /**
     * @param completionTimeInWeeksORmonths_1 the completionTimeInWeeksORmonths_1 to set
     */
    public void setCompletionTimeInWeeksORmonths_1(String completionTimeInWeeksORmonths_1) {
        this.completionTimeInWeeksORmonths_1 = completionTimeInWeeksORmonths_1;
    }

    /**
     * @return the lotNo_2
     */
    public String getLotNo_2() {
        return lotNo_2;
    }

    /**
     * @param lotNo_2 the lotNo_2 to set
     */
    public void setLotNo_2(String lotNo_2) {
        this.lotNo_2 = lotNo_2;
    }

    /**
     * @return the identificationOfLot_2
     */
    public String getIdentificationOfLot_2() {
        return identificationOfLot_2;
    }

    /**
     * @param identificationOfLot_2 the identificationOfLot_2 to set
     */
    public void setIdentificationOfLot_2(String identificationOfLot_2) {
        this.identificationOfLot_2 = identificationOfLot_2;
    }

    /**
     * @return the location_2
     */
    public String getLocation_2() {
        return location_2;
    }

    /**
     * @param location_2 the location_2 to set
     */
    public void setLocation_2(String location_2) {
        this.location_2 = location_2;
    }

    /**
     * @return the completionTimeInWeeksORmonths_2
     */
    public String getCompletionTimeInWeeksORmonths_2() {
        return completionTimeInWeeksORmonths_2;
    }

    /**
     * @param completionTimeInWeeksORmonths_2 the completionTimeInWeeksORmonths_2 to set
     */
    public void setCompletionTimeInWeeksORmonths_2(String completionTimeInWeeksORmonths_2) {
        this.completionTimeInWeeksORmonths_2 = completionTimeInWeeksORmonths_2;
    }

    /**
     * @return the lotNo_3
     */
    public String getLotNo_3() {
        return lotNo_3;
    }

    /**
     * @param lotNo_3 the lotNo_3 to set
     */
    public void setLotNo_3(String lotNo_3) {
        this.lotNo_3 = lotNo_3;
    }

    /**
     * @return the identificationOfLot_3
     */
    public String getIdentificationOfLot_3() {
        return identificationOfLot_3;
    }

    /**
     * @param identificationOfLot_3 the identificationOfLot_3 to set
     */
    public void setIdentificationOfLot_3(String identificationOfLot_3) {
        this.identificationOfLot_3 = identificationOfLot_3;
    }

    /**
     * @return the location_3
     */
    public String getLocation_3() {
        return location_3;
    }

    /**
     * @param location_3 the location_3 to set
     */
    public void setLocation_3(String location_3) {
        this.location_3 = location_3;
    }

    /**
     * @return the completionTimeInWeeksORmonths_3
     */
    public String getCompletionTimeInWeeksORmonths_3() {
        return completionTimeInWeeksORmonths_3;
    }

    /**
     * @param completionTimeInWeeksORmonths_3 the completionTimeInWeeksORmonths_3 to set
     */
    public void setCompletionTimeInWeeksORmonths_3(String completionTimeInWeeksORmonths_3) {
        this.completionTimeInWeeksORmonths_3 = completionTimeInWeeksORmonths_3;
    }

    /**
     * @return the lotNo_4
     */
    public String getLotNo_4() {
        return lotNo_4;
    }

    /**
     * @param lotNo_4 the lotNo_4 to set
     */
    public void setLotNo_4(String lotNo_4) {
        this.lotNo_4 = lotNo_4;
    }

    /**
     * @return the identificationOfLot_4
     */
    public String getIdentificationOfLot_4() {
        return identificationOfLot_4;
    }

    /**
     * @param identificationOfLot_4 the identificationOfLot_4 to set
     */
    public void setIdentificationOfLot_4(String identificationOfLot_4) {
        this.identificationOfLot_4 = identificationOfLot_4;
    }

    /**
     * @return the location_4
     */
    public String getLocation_4() {
        return location_4;
    }

    /**
     * @param location_4 the location_4 to set
     */
    public void setLocation_4(String location_4) {
        this.location_4 = location_4;
    }

    /**
     * @return the completionTimeInWeeksORmonths_4
     */
    public String getCompletionTimeInWeeksORmonths_4() {
        return completionTimeInWeeksORmonths_4;
    }

    /**
     * @param completionTimeInWeeksORmonths_4 the completionTimeInWeeksORmonths_4 to set
     */
    public void setCompletionTimeInWeeksORmonths_4(String completionTimeInWeeksORmonths_4) {
        this.completionTimeInWeeksORmonths_4 = completionTimeInWeeksORmonths_4;
    }

    /**
     * @return the nameofOfficialInvitingTender
     */
    public String getNameofOfficialInvitingTender() {
        return nameofOfficialInvitingTender;
    }

    /**
     * @param nameofOfficialInvitingTender the nameofOfficialInvitingTender to set
     */
    public void setNameofOfficialInvitingTender(String nameofOfficialInvitingTender) {
        this.nameofOfficialInvitingTender = nameofOfficialInvitingTender;
    }

    /**
     * @return the designationofOfficialInvitingTender
     */
    public String getDesignationofOfficialInvitingTender() {
        return designationofOfficialInvitingTender;
    }

    /**
     * @param designationofOfficialInvitingTender the designationofOfficialInvitingTender to set
     */
    public void setDesignationofOfficialInvitingTender(String designationofOfficialInvitingTender) {
        this.designationofOfficialInvitingTender = designationofOfficialInvitingTender;
    }

    /**
     * @return the addressofOfficialInvitingTender
     */
    public String getAddressofOfficialInvitingTender() {
        return addressofOfficialInvitingTender;
    }

    /**
     * @param addressofOfficialInvitingTender the addressofOfficialInvitingTender to set
     */
    public void setAddressofOfficialInvitingTender(String addressofOfficialInvitingTender) {
        this.addressofOfficialInvitingTender = addressofOfficialInvitingTender;
    }

    /**
     * @return the contactdetailsofOfficialInvitingTender
     */
    public String getContactdetailsofOfficialInvitingTender() {
        return contactdetailsofOfficialInvitingTender;
    }

    /**
     * @param contactdetailsofOfficialInvitingTender the contactdetailsofOfficialInvitingTender to set
     */
    public void setContactdetailsofOfficialInvitingTender(String contactdetailsofOfficialInvitingTender) {
        this.contactdetailsofOfficialInvitingTender = contactdetailsofOfficialInvitingTender;
    }

    /**
     * @return the faxNo
     */
    public String getFaxNo() {
        return faxNo;
    }

    /**
     * @param faxNo the faxNo to set
     */
    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    /**
     * @return the e_mail
     */
    public String getE_mail() {
        return e_mail;
    }

    /**
     * @param e_mail the e_mail to set
     */
    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }

    /**
     * @return the dateofNotificationofAward
     */
    public String getDateofNotificationofAward() {
        return dateofNotificationofAward;
    }

    /**
     * @param dateofNotificationofAward the dateofNotificationofAward to set
     */
    public void setDateofNotificationofAward(String dateofNotificationofAward) {
        this.dateofNotificationofAward = dateofNotificationofAward;
    }

    /**
     * @return the dateofContractSigning
     */
    public String getDateofContractSigning() {
        return dateofContractSigning;
    }

    /**
     * @param dateofContractSigning the dateofContractSigning to set
     */
    public void setDateofContractSigning(String dateofContractSigning) {
        this.dateofContractSigning = dateofContractSigning;
    }

    /**
     * @return the proposedDateofContractCompletion
     */
    public String getProposedDateofContractCompletion() {
        return proposedDateofContractCompletion;
    }

    /**
     * @param proposedDateofContractCompletion the proposedDateofContractCompletion to set
     */
    public void setProposedDateofContractCompletion(String proposedDateofContractCompletion) {
        this.proposedDateofContractCompletion = proposedDateofContractCompletion;
    }

    /**
     * @return the noofTendersProposalsSold
     */
    public String getNoofTendersProposalsSold() {
        return noofTendersProposalsSold;
    }

    /**
     * @param noofTendersProposalsSold the noofTendersProposalsSold to set
     */
    public void setNoofTendersProposalsSold(String noofTendersProposalsSold) {
        this.noofTendersProposalsSold = noofTendersProposalsSold;
    }

    /**
     * @return the noofTendersProposalsReceived
     */
    public String getNoofTendersProposalsReceived() {
        return noofTendersProposalsReceived;
    }

    /**
     * @param noofTendersProposalsReceived the noofTendersProposalsReceived to set
     */
    public void setNoofTendersProposalsReceived(String noofTendersProposalsReceived) {
        this.noofTendersProposalsReceived = noofTendersProposalsReceived;
    }

    /**
     * @return the noofResponsiveTendersProposals
     */
    public String getNoofResponsiveTendersProposals() {
        return noofResponsiveTendersProposals;
    }

    /**
     * @param noofResponsiveTendersProposals the noofResponsiveTendersProposals to set
     */
    public void setNoofResponsiveTendersProposals(String noofResponsiveTendersProposals) {
        this.noofResponsiveTendersProposals = noofResponsiveTendersProposals;
    }

    /**
     * @return the briefDescriptionofContract
     */
    public String getBriefDescriptionofContract() {
        return briefDescriptionofContract;
    }

    /**
     * @param briefDescriptionofContract the briefDescriptionofContract to set
     */
    public void setBriefDescriptionofContract(String briefDescriptionofContract) {
        this.briefDescriptionofContract = briefDescriptionofContract;
    }

    /**
     * @return the contractValue
     */
    public String getContractValue() {
        return contractValue;
    }

    /**
     * @param contractValue the contractValue to set
     */
    public void setContractValue(String contractValue) {
        this.contractValue = contractValue;
    }

    /**
     * @return the nameofSupplierContractorConsultant
     */
    public String getNameofSupplierContractorConsultant() {
        return nameofSupplierContractorConsultant;
    }

    /**
     * @param nameofSupplierContractorConsultant the nameofSupplierContractorConsultant to set
     */
    public void setNameofSupplierContractorConsultant(String nameofSupplierContractorConsultant) {
        this.nameofSupplierContractorConsultant = nameofSupplierContractorConsultant;
    }

    /**
     * @return the locationofSupplierContractorConsultant
     */
    public String getLocationofSupplierContractorConsultant() {
        return locationofSupplierContractorConsultant;
    }

    /**
     * @param locationofSupplierContractorConsultant the locationofSupplierContractorConsultant to set
     */
    public void setLocationofSupplierContractorConsultant(String locationofSupplierContractorConsultant) {
        this.locationofSupplierContractorConsultant = locationofSupplierContractorConsultant;
    }

    /**
     * @return the ifNogivereasonwhy_NOA
     */
    public String getIfNogivereasonwhy_NOA() {
        return ifNogivereasonwhy_NOA;
    }

    /**
     * @param ifNogivereasonwhy_NOA the ifNogivereasonwhy_NOA to set
     */
    public void setIfNogivereasonwhy_NOA(String ifNogivereasonwhy_NOA) {
        this.ifNogivereasonwhy_NOA = ifNogivereasonwhy_NOA;
    }

    /**
     * @return the ifNoGiveReasonWhy_Performance_Security
     */
    public String getIfNoGiveReasonWhy_Performance_Security() {
        return ifNoGiveReasonWhy_Performance_Security;
    }

    /**
     * @param ifNoGiveReasonWhy_Performance_Security the ifNoGiveReasonWhy_Performance_Security to set
     */
    public void setIfNoGiveReasonWhy_Performance_Security(String ifNoGiveReasonWhy_Performance_Security) {
        this.ifNoGiveReasonWhy_Performance_Security = ifNoGiveReasonWhy_Performance_Security;
    }

    /**
     * @return the ifNoGiveReasonWhy_Contract_signed
     */
    public String getIfNoGiveReasonWhy_Contract_signed() {
        return ifNoGiveReasonWhy_Contract_signed;
    }

    /**
     * @param ifNoGiveReasonWhy_Contract_signed the ifNoGiveReasonWhy_Contract_signed to set
     */
    public void setIfNoGiveReasonWhy_Contract_signed(String ifNoGiveReasonWhy_Contract_signed) {
        this.ifNoGiveReasonWhy_Contract_signed = ifNoGiveReasonWhy_Contract_signed;
    }

    /**
     * @return the procurementNature
     */
    public String getProcurementNature() {
        return procurementNature;
    }

    /**
     * @param procurementNature the procurementNature to set
     */
    public void setProcurementNature(String procurementNature) {
        this.procurementNature = procurementNature;
    }

    /**
     * @return the invitationFor
     */
    public String getInvitationFor() {
        return invitationFor;
    }

    /**
     * @param invitationFor the invitationFor to set
     */
    public void setInvitationFor(String invitationFor) {
        this.invitationFor = invitationFor;
    }

    /**
     * @return the procurementType
     */
    public String getProcurementType() {
        return procurementType;
    }

    /**
     * @param procurementType the procurementType to set
     */
    public void setProcurementType(String procurementType) {
        this.procurementType = procurementType;
    }

    /**
     * @return the procurementMethod
     */
    public String getProcurementMethod() {
        return procurementMethod;
    }

    /**
     * @param procurementMethod the procurementMethod to set
     */
    public void setProcurementMethod(String procurementMethod) {
        this.procurementMethod = procurementMethod;
    }

    /**
     * @return the eventType
     */
    public String getEventType() {
        return eventType;
    }

    /**
     * @param eventType the eventType to set
     */
    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    /**
     * @return the tenderSecurityAmount_1
     */
    public String getTenderSecurityAmount_1() {
        return tenderSecurityAmount_1;
    }

    /**
     * @param tenderSecurityAmount_1 the tenderSecurityAmount_1 to set
     */
    public void setTenderSecurityAmount_1(String tenderSecurityAmount_1) {
        this.tenderSecurityAmount_1 = tenderSecurityAmount_1;
    }

    /**
     * @return the tenderSecurityAmount_2
     */
    public String getTenderSecurityAmount_2() {
        return tenderSecurityAmount_2;
    }

    /**
     * @param tenderSecurityAmount_2 the tenderSecurityAmount_2 to set
     */
    public void setTenderSecurityAmount_2(String tenderSecurityAmount_2) {
        this.tenderSecurityAmount_2 = tenderSecurityAmount_2;
    }

    /**
     * @return the tenderSecurityAmount_3
     */
    public String getTenderSecurityAmount_3() {
        return tenderSecurityAmount_3;
    }

    /**
     * @param tenderSecurityAmount_3 the tenderSecurityAmount_3 to set
     */
    public void setTenderSecurityAmount_3(String tenderSecurityAmount_3) {
        this.tenderSecurityAmount_3 = tenderSecurityAmount_3;
    }

    /**
     * @return the tenderSecurityAmount_4
     */
    public String getTenderSecurityAmount_4() {
        return tenderSecurityAmount_4;
    }

    /**
     * @param tenderSecurityAmount_4 the tenderSecurityAmount_4 to set
     */
    public void setTenderSecurityAmount_4(String tenderSecurityAmount_4) {
        this.tenderSecurityAmount_4 = tenderSecurityAmount_4;
    }


}
