/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblScBankDevPartnerMaster;
import com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class ManageSbDpOfficeSrBean {

     final Logger logger = Logger.getLogger(ManageSbDpOfficeSrBean.class);

     ScBankDevpartnerService scBankDevpartnerService=(ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");

    private int offset = 0;
    private int limit = 0;
    private String colName = "";
    private String op_ENUM = "";
    private String condition = "";
    private boolean _search = false;
    private String sortOrder = "";
    private String sortCol = "";
    private String logUserId="0";

    public void setLogUserId(String logUserId) {
        scBankDevpartnerService.setUserId(logUserId);
        this.logUserId = logUserId;
    }
    public boolean isSearch()
    {
        return _search;
    }

    public void setSearch(boolean _search)
    {
        this._search = _search;
    }

    public String getColName()
    {
        return colName;
    }

    public void setColName(String colName)
    {
        this.colName = colName;
    }

    public String getCondition()
    {
        return condition;
    }

    public void setCondition(String condition)
    {
        this.condition = condition;
    }

    public String getSortOrder()
    {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder)
    {
        this.sortOrder = sortOrder;
    }

    public int getLimit()
    {
        return limit;
    }

    public void setLimit(int limit)
    {
        this.limit = limit;
    }

    public int getOffset()
    {
        return offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public String getOp_ENUM()
    {
        return op_ENUM;
    }

    public void setOp_ENUM(String op_ENUM)
    {
        this.op_ENUM = op_ENUM;
    }

    public ScBankDevpartnerService getScBankDevpartnerService()
    {
        return scBankDevpartnerService;
    }

    public void setScBankDevpartnerService(ScBankDevpartnerService scBankDevpartnerService)
    {
        this.scBankDevpartnerService = scBankDevpartnerService;
    }

    public String getSortCol()
    {
        return sortCol;
    }

    public void setSortCol(String sortCol)
    {
        this.sortCol = sortCol;
    }

    public long getCntHeadOffice(String type){
        logger.debug("getAllCountOfBranchDept : "+logUserId+" Starts ");
        long lng =0;
        try {
            lng = scBankDevpartnerService.getCntOffice("TblScBankDevPartnerMaster scdpMaster", "scdpMaster.partnerType = '"+type+"' AND scdpMaster.isBranchOffice LIKE 'No'");
        } catch (Exception e) {
            logger.error("getAllCountOfBranchDept : "+logUserId+" : "+e);
    }
        logger.debug("getAllCountOfBranchDept : "+logUserId+" Ends ");
        return lng;
    }
    
    public long getCntHeadOffice(String type, String searchField,String searchString,String searchOper){
        logger.debug("getCntHeadOffice : "+logUserId+" Starts ");
        long lng =0;
        try{
        if(searchOper.equalsIgnoreCase("EQ")) {
            lng = scBankDevpartnerService.getCntOffice("TblScBankDevPartnerMaster scdpMaster", "scdpMaster.partnerType LIKE '"+type+"' AND scdpMaster.isBranchOffice LIKE 'No' and scdpMaster." + searchField + " = '" + searchString+"'");
        }else if(searchOper.equalsIgnoreCase("CN")) {
            lng = scBankDevpartnerService.getCntOffice("TblScBankDevPartnerMaster scdpMaster", "scdpMaster.partnerType LIKE '"+type+"' AND scdpMaster.isBranchOffice LIKE 'No' and scdpMaster." + searchField + " LIKE '%" + searchString+"%'");
        } else {
            lng = 0;
        }
        }catch(Exception e)
        {
            logger.error("getCntHeadOffice : "+logUserId+" : "+e);
    }
        logger.debug("getCntHeadOffice : "+logUserId+" Ends ");
        return lng;
    }

    List<TblScBankDevPartnerMaster> officeList = new ArrayList<TblScBankDevPartnerMaster>();
    public List<Object[]> getHeadOfficeList(String type) 
    {
        logger.debug("getHeadOfficeList : "+logUserId+" Starts ");
        List<Object[]> list = null;
        try{
        String orderClause = "";
        if(getSortCol().equalsIgnoreCase("")){
            orderClause = "tsbdpm.createdDate desc";
        }else if(getSortCol().equalsIgnoreCase("sbDevelopName")){
            orderClause = "tsbdpm.sbDevelopName " + getSortOrder();
        }else if(getSortCol().equalsIgnoreCase("country")){
            orderClause = "tcm.countryName " + getSortOrder();
        }else if(getSortCol().equalsIgnoreCase("state")){
            orderClause = "tsm.stateName " + getSortOrder();
        }
        list = scBankDevpartnerService.findDevPartnerOrg(offset,limit,orderClause,type);
        }catch(Exception e)
        {
              logger.error("getHeadOfficeList : "+logUserId+" : "+e);
    }
        logger.debug("getHeadOfficeList : "+logUserId+" Ends ");
        return list;
    }
//    public List<TblScBankDevPartnerMaster> getHeadOfficeList(String type) throws Exception {
//
//   if (officeList.isEmpty()) {
//    if(getSortOrder()!=null){
//      if(getSortCol().equalsIgnoreCase("sbDevelopName")){
//        if(getSortOrder().equalsIgnoreCase("desc")){
//            Object[] values={"partnerType",Operation_enum.LIKE,type,"isBranchOffice",Operation_enum.LIKE,"No","sbDevelopName",Operation_enum.ORDERBY,Operation_enum.DESC};
//            for (TblScBankDevPartnerMaster tblScBankDevPartnerMaster : scBankDevpartnerService.findOfficeList(offset, limit, values)) {
//                officeList.add(tblScBankDevPartnerMaster);
//                }
//         }else{
//                    Object[] values={"partnerType",Operation_enum.LIKE,type,"isBranchOffice",Operation_enum.LIKE,"No","sbDevelopName",Operation_enum.ORDERBY,Operation_enum.ASC};
//            for (TblScBankDevPartnerMaster tblScBankDevPartnerMaster : scBankDevpartnerService.findOfficeList(offset, limit, values)) {
//                officeList.add(tblScBankDevPartnerMaster);
//                }
//                }
//    }else if(getSortCol().equals("")){
//        Object[] values={"partnerType",Operation_enum.LIKE,type,"isBranchOffice",Operation_enum.LIKE,"No","createdDate",Operation_enum.ORDERBY,Operation_enum.DESC};
//            for (TblScBankDevPartnerMaster tblScBankDevPartnerMaster : scBankDevpartnerService.findOfficeList(offset, limit, values)) {
//                officeList.add(tblScBankDevPartnerMaster);
//            }
//    }
//      //end if(getSortCol())
//   /* if(getSortCol().equalsIgnoreCase("country")){
//        if(getSortOrder().equalsIgnoreCase("desc")){
//            Object[] values={"partnerType",Operation_enum.LIKE,type,"isBranchOffice",Operation_enum.LIKE,"No","tblCountryMaster.countryName",Operation_enum.ORDERBY,Operation_enum.DESC};
//            for (TblScBankDevPartnerMaster tblScBankDevPartnerMaster : scBankDevpartnerService.findOfficeList(offset, limit, values)) {
//                officeList.add(tblScBankDevPartnerMaster);
//                }
//         }else{
//                    Object[] values={"partnerType",Operation_enum.LIKE,type,"isBranchOffice",Operation_enum.LIKE,"No","tblCountryMaster.countryName",Operation_enum.ORDERBY,Operation_enum.ASC};
//            for (TblScBankDevPartnerMaster tblScBankDevPartnerMaster : scBankDevpartnerService.findOfficeList(offset, limit, values)) {
//                officeList.add(tblScBankDevPartnerMaster);
//                }
//                }
//    }//end if(getSortCol())
//
//      if(getSortCol().equalsIgnoreCase("state")){
//        if(getSortOrder().equalsIgnoreCase("desc")){
//            Object[] values={"partnerType",Operation_enum.LIKE,type,"isBranchOffice",Operation_enum.LIKE,"No","tblStateMaster.stateName",Operation_enum.ORDERBY,Operation_enum.DESC};
//            for (TblScBankDevPartnerMaster tblScBankDevPartnerMaster : scBankDevpartnerService.findOfficeList(offset, limit, values)) {
//                officeList.add(tblScBankDevPartnerMaster);
//                }
//         }else{
//                    Object[] values={"partnerType",Operation_enum.LIKE,type,"isBranchOffice",Operation_enum.LIKE,"No","tblStateMaster.stateName",Operation_enum.ORDERBY,Operation_enum.ASC};
//            for (TblScBankDevPartnerMaster tblScBankDevPartnerMaster : scBankDevpartnerService.findOfficeList(offset, limit, values)) {
//                officeList.add(tblScBankDevPartnerMaster);
//                }
//                }
//    }//end if(getSortCol())
//    */
//   }else{
//                Object[] values={"partnerType",Operation_enum.LIKE,type,"isBranchOffice",Operation_enum.LIKE,"No","sbDevelopName",Operation_enum.ORDERBY,Operation_enum.ASC};
//            for (TblScBankDevPartnerMaster tblScBankDevPartnerMaster : scBankDevpartnerService.findOfficeList(offset, limit, values)) {
//                officeList.add(tblScBankDevPartnerMaster);
//            }
//   }
// }
//        
//        return officeList;
//    }

    public List<TblScBankDevPartnerMaster> getHeadOfficeList(String type,String searchField,String searchString,String searchOper){
        logger.debug("getHeadOfficeList : "+logUserId+" Starts ");
        try{
        if (officeList.isEmpty()) {
            logger.debug("officeList is empty");
            if(searchOper.equalsIgnoreCase("EQ")) {
                Object[] values={"partnerType",Operation_enum.LIKE,type,"isBranchOffice",Operation_enum.LIKE,"No", searchField, Operation_enum.LIKE, searchString, "sbankDevelopId", Operation_enum. ORDERBY,Operation_enum.DESC};
                for (TblScBankDevPartnerMaster tblScBankDevPartnerMaster : scBankDevpartnerService.findOfficeList(offset, limit, values)) {
                    officeList.add(tblScBankDevPartnerMaster);
                }
            } else if(searchOper.equalsIgnoreCase("CN")) {
                Object[] values={"partnerType",Operation_enum.LIKE,type,"isBranchOffice",Operation_enum.LIKE,"No", searchField, Operation_enum.LIKE, "%"+searchString+"%", "sbankDevelopId", Operation_enum.ORDERBY, Operation_enum.DESC};
                for (TblScBankDevPartnerMaster tblScBankDevPartnerMaster : scBankDevpartnerService.findOfficeList(offset, limit, values)) {
                    officeList.add(tblScBankDevPartnerMaster);
                }
            }
        }
        }catch(Exception e)
        {
            logger.error("getAllCountOfGrades : "+logUserId+" : "+e);
        }
        logger.debug("PartNer head office List Search: "+officeList);
        logger.debug("getAllCountOfGrades : "+logUserId+" Ends ");
        return officeList;
    }

}
