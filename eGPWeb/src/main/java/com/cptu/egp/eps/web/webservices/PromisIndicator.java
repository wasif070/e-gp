/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.webservices;

import com.cptu.egp.eps.model.table.TblPromisIndicator;
import com.cptu.egp.eps.model.table.TblPromisProcess;
import com.cptu.egp.eps.service.serviceimpl.PromisIndicatorServiceImpl;
import com.cptu.egp.eps.service.serviceimpl.PromisProcessServiceImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.WebServiceUtil;
import com.cptu.egp.eps.web.xmlpojos.WSResponseObject;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.apache.log4j.Logger;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.simple.JSONValue;

/**
 * web services for integration of 45 Indicator for Promis with e-GP System
 * @author Sudhir Chavhan
 */
@WebService()
public class PromisIndicator {

    private static final Logger LOGGER = Logger.getLogger(PromisIndicator.class);
    private String logUserId = "0";
    private static final int WEB_SERVICE_ID = 47;
    private static final String SEARCH_SERVICE_NAME = "PromisIndicatorService";
    private static final String SEARCH_SERVICE_NAME_Process = "PromisProcessService";

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     * This method gives the List of objects of 45 Indicators for Promis integration.

     *
     * @param String username
     * @param String password
     * @return WSResponseObject With list data of List<jsonText>
     */
    @WebMethod(operationName = "getIndicatorForPromis")
    public WSResponseObject getIndicatorForPromis(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {

        LOGGER.debug("getIndicatorForPromis : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = "Sharing Promis Indicators";
        //insert record in log table
        WebServiceUtil.insertLogRecord(WEB_SERVICE_ID, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WEB_SERVICE_ID,
                SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> promisIndicatorList = new ArrayList<String>();
            try {
                // get the list of 45 Indicators for Promis Indicator
                promisIndicatorList = getPromisIndicatorList();
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e.getMessage());
                LOGGER.debug("getIndicatorForPromis : " + logUserId + e);
                System.out.println("Exception in getIndicatorForPromis :-" + e.toString());
            }
            if (promisIndicatorList != null) {
                if (promisIndicatorList.size() <= 0) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(promisIndicatorList);
                }
            }//end IF condition with NULL checking
        }//end IF condition with "isListDataRequired" checking
        LOGGER.debug("getIndicatorForPromis : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    /***
     * This method return the 45 Indicators for Promis as a list of objects
     * @return List<Indicator>
     */
    private List<String> getPromisIndicatorList() {

        LOGGER.debug("getPromisIndicatorList : " + logUserId + WSResponseObject.LOGGER_START);
        Map mapObj=new LinkedHashMap();
        String jsonText =null;
        List<String> indicatorList = new ArrayList<String>();
        List<TblPromisIndicator> promisIndicatorDetailsList = new ArrayList<TblPromisIndicator>();
        try {
            PromisIndicatorServiceImpl promisIndicatorServiceImpl =
                    (PromisIndicatorServiceImpl) AppContext.getSpringBean(SEARCH_SERVICE_NAME);
            promisIndicatorDetailsList = promisIndicatorServiceImpl.getAllTblPromisIndicator();
            if (promisIndicatorDetailsList != null) {
                for (TblPromisIndicator promisIndicator : promisIndicatorDetailsList) {

                    //mapObj.put("Indicator_ID",promisIndicator.getIndicator_ID());
                    mapObj.put("PEOfficeId",promisIndicator.getOfficeId());
                    mapObj.put("PEOffice_Name",promisIndicator.getPEOffice_Name());
                    mapObj.put("PrmsPECode",promisIndicator.getPrmsPECode());
                    mapObj.put("TenderId",promisIndicator.getTenderId());
                    mapObj.put("PublishNewspaper",promisIndicator.getPublishNewspaper());
                    mapObj.put("PublishCPTUWebsite",promisIndicator.getPublishCPTUWebsite());
                    mapObj.put("GoBProcRules",promisIndicator.getGoBProcRules());
                    mapObj.put("DPRules",promisIndicator.getDPRules());
                    mapObj.put("MultiLocSubmission",promisIndicator.getMultiLocSubmission());
                    mapObj.put("DaysBetTenPubTenderClosing",promisIndicator.getDaysBetTenPubTenderClosing());
                    mapObj.put("SuffTenderSubTime",promisIndicator.getSuffTenderSubTime());

                    mapObj.put("TenderDocSold",promisIndicator.getTenderDocSold());
                    mapObj.put("TenderersPartCount",promisIndicator.getTenderersPartCount());
                    mapObj.put("TenderSubVSDocSold",promisIndicator.getTenderSubVSDocSold());
                    mapObj.put("TOCMemberFromTEC",promisIndicator.getTOCMemberFromTEC());
                    mapObj.put("TECFormedByAA",promisIndicator.getTECFormedByAA());
                    mapObj.put("TECExternalMembers",promisIndicator.getTECExternalMembers());
                    mapObj.put("EvaluationTime",promisIndicator.getEvaluationTime());
                    mapObj.put("EvalCompletedInTime",promisIndicator.getEvalCompletedInTime());
                    mapObj.put("ResponsiveTenderersCount",promisIndicator.getResponsiveTenderersCount());
                    mapObj.put("IsReTendered",promisIndicator.getIsReTendered());

                    mapObj.put("IsTenderCancelled",promisIndicator.getIsTenderCancelled());
                    mapObj.put("DaysBtwnTenEvalTenApp",promisIndicator.getDaysBtwnTenEvalTenApp());
                    mapObj.put("IsApprovedByProperAA",promisIndicator.getIsApprovedByProperAA());
                    mapObj.put("SubofEvalReportAA",promisIndicator.getSubofEvalReportAA());
                    mapObj.put("AppofContractByAAInTimr",promisIndicator.getAppofContractByAAInTimr());
                    mapObj.put("TERReviewByOther",promisIndicator.getTERReviewByOther());
                    mapObj.put("ContractAppByHighAuth",promisIndicator.getContractAppByHighAuth());
                    mapObj.put("DaysBtwnContappNOA",promisIndicator.getDaysBtwnContappNOA());
                    mapObj.put("DaysBtwnTenOpenNOA",promisIndicator.getDaysBtwnTenOpenNOA());
                    mapObj.put("DaysBtwnIFTNOA",promisIndicator.getDaysBtwnIFTNOA());

                    mapObj.put("AwardPubCPTUWebsite",promisIndicator.getAwardPubCPTUWebsite());
                    mapObj.put("AwardedInTenValidityPeriod",promisIndicator.getAwardedInTenValidityPeriod());
                    mapObj.put("DeliveredInTime",promisIndicator.getDeliveredInTime());
                    mapObj.put("LiquidatedDamage",promisIndicator.getLiquidatedDamage());
                    mapObj.put("FullyCompleted",promisIndicator.getFullyCompleted());
                    mapObj.put("DaysReleasePayment",promisIndicator.getDaysReleasePayment());
                    mapObj.put("LatePayment",promisIndicator.getLatePayment());
                    mapObj.put("InterestPaid",promisIndicator.getInterestPaid());
                    mapObj.put("ComplaintReceived",promisIndicator.getComplaintReceived());
                    mapObj.put("ResolAwardModification",promisIndicator.getResolAwardModification());

                    mapObj.put("ComplaintResolved",promisIndicator.getComplaintResolved());
                    mapObj.put("RevPanelDecUpheld",promisIndicator.getRevPanelDecUpheld());
                    mapObj.put("IsContAmended",promisIndicator.getIsContAmended());
                    mapObj.put("ContUnresolvDispute",promisIndicator.getContUnresolvDispute());
                    mapObj.put("DetFraudCorruption",promisIndicator.getDetFraudCorruption());
                    //mapObj.put("Ministry_ID",promisIndicator.getMinistry_ID());
                    //mapObj.put("Division_ID",promisIndicator.getDivision_ID());
                    //mapObj.put("Org_ID",promisIndicator.getOrg_ID());
                    mapObj.put("Is_TenderOnline",promisIndicator.getIs_TenderOnline().trim());
                    mapObj.put("Is_Progress",promisIndicator.getIs_Progress().trim());

                    jsonText = JSONValue.toJSONString(mapObj);
                    System.out.print("Indicators jsonText:-"+jsonText);

/*  Commented by sudhir chavhan for to add JSON functionality...
                    StringBuilder sbCSV = new StringBuilder();
                    sbCSV.append(promisIndicator.getIndicator_ID());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getOfficeId());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getPEOffice_Name());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getTenderId());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getPublishNewspaper());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getPublishCPTUWebsite());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getProcurementRules());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getMultiLocSubmission());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getDaysBetTenPubTenderClosing());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getSuffTenderSubTime());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getTenderDocSold());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getTenderersPartCount());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getTenderSubVSDocSold());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getTOCMemberFromTEC());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getTECFormedByAA());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getTECExternalMembers());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getEvaluationTime());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getEvalCompletedInTime());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getResponsiveTenderersCount());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getIsReTendered());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getIsTenderCancelled());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getDaysBtwnTenEvalTenApp());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getIsApprovedByProperAA());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getSubofEvalReportAA());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getAppofContractByAAInTimr());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getTERReviewByOther());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getContractAppByHighAuth());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getDaysBtwnContappNOA());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getDaysBtwnTenOpenNOA());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getDaysBtwnIFTNOA());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getAwardPubCPTUWebsite());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getAwardedInTenValidityPeriod());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getDeliveredInTime());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getLiquidatedDamage());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getFullyCompleted());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getDaysReleasePayment());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getLatePayment());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getInterestPaid());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getComplaintReceived());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getResolAwardModification());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getComplaintResolved());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getRevPanelDecUpheld());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getIsContAmended());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getContUnresolvDispute());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getDetFraudCorruption());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getMinistry_ID());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getDivision_ID());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getOrg_ID());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getIs_TenderOnline().trim());
                    sbCSV.append(WSResponseObject.APPEND_CHAR);
                    sbCSV.append(promisIndicator.getIs_Progress().trim());
                    indicatorList.add(sbCSV.toString());
 */
                    indicatorList.add(jsonText.toString());
                }//end FOR loop
            }//end IF condition
        } catch (Exception e) {
            LOGGER.error("getPromisIndicatorList : " + logUserId + e);
        }
        LOGGER.debug("getPromisIndicatorList : " + logUserId + WSResponseObject.LOGGER_END);
        return indicatorList;
    }
   /**
     * This method gives the List of Process objects of 45 Indicators for Promis integration.
     *
     * @param String username
     * @param String password
     * @return WSResponseObject With list data of List<jsonText>
    // Commented by Sudhir Chavhan instructed by Rajesh Sakiya for Training indicator 15/09/2011
    /*
    @WebMethod(operationName = "getProcessForPromis")
    public WSResponseObject getProcessForPromis(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {

        LOGGER.debug("getProcessForPromis : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = "Sharing Promis Process";
        //insert record in log table
        WebServiceUtil.insertLogRecord(WEB_SERVICE_ID, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WEB_SERVICE_ID,
                SEARCH_SERVICE_NAME, username, password);
        boolean isProcessListDataRequired = wSResponseObject.getIsListRequired();
        if (isProcessListDataRequired) {
            List<String> promisProcessList = new ArrayList<String>();
            try {
                // get the list of Process Indicators for Promis Indicator
                promisProcessList = getPromisProcessList();
                //System.out.println("promisProcessList :-" +promisProcessList);
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e.getMessage());
                LOGGER.debug("getProcessForPromis : " + logUserId + e);
                System.out.println("Exception in getProcessForPromis :-" + e.toString());
            }
            if (promisProcessList != null) {
                if (promisProcessList.size() <= 0) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                    //System.out.println("promisProcessList.size() ==0 ");
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(promisProcessList);
                    //System.out.println("promisProcessList.size() !=0 ");
                }
            }//end IF condition with NULL checking
        }//end IF condition with "isListDataRequired" checking
        LOGGER.debug("getProcessForPromis : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }
*/
     /***
     * This method return the Process Indicators for Promis as a list of objects
     * @return List<Process>
     */
   // Commented by Sudhir Chavhan instructed by Rajesh Sakiya for Training indicator 15/09/2011
  /*  private List<String> getPromisProcessList() {

        LOGGER.debug("getPromisProcessList : " + logUserId + WSResponseObject.LOGGER_START);
        Map mapObj=new LinkedHashMap();
        String jsonText =null;
        List<String> processList = new ArrayList<String>();
        List<TblPromisProcess> promisProcessDetailsList = new ArrayList<TblPromisProcess>();
        try {
            //System.out.print("Process TblPromisProcess 1 ");
            PromisProcessServiceImpl promisProcessServiceImpl =
                    (PromisProcessServiceImpl) AppContext.getSpringBean(SEARCH_SERVICE_NAME_Process);
            promisProcessDetailsList = promisProcessServiceImpl.getAllTblPromisProcess();
            //System.out.print("Process TblPromisProcess 2:-"+promisProcessDetailsList);
            if (promisProcessDetailsList != null) {
                for (TblPromisProcess promisProcess : promisProcessDetailsList) {

                    mapObj.put("Process_ID",promisProcess.getProcess_ID());
                    mapObj.put("PEOfficeId",promisProcess.getOfficeId());
                    mapObj.put("PEOffice_Name",promisProcess.getPEOffice_Name());
                    mapObj.put("PrmsPECode",promisProcess.getPrmsPECode());
                    mapObj.put("AvgTrainedProcStaffPE",promisProcess.getAvgTrainedProcStaffPE());
                    mapObj.put("PerPETrainedProcStaff",promisProcess.getPerPETrainedProcStaff());
                    mapObj.put("TotalProcPersonOrgProcTraining",promisProcess.getTotalProcPersonOrgProcTraining());
                    mapObj.put("Is_TenderOnline",promisProcess.getIs_TenderOnline().trim());
                    //mapObj.put("Ministry_ID",promisProcess.getMinistry_ID());
                    //mapObj.put("Division_ID",promisProcess.getDivision_ID());
                    //mapObj.put("Org_ID",promisProcess.getOrg_ID());

                    jsonText = JSONValue.toJSONString(mapObj);
                    System.out.print("Process jsonText:-"+jsonText);

                    processList.add(jsonText.toString());
                }//end FOR loop
            }//end IF condition
        } catch (Exception e) {
            LOGGER.error("getPromisProcessList : " + logUserId + e);
            System.out.println("getPromisProcessList : " + logUserId + e);
        }
        LOGGER.debug("getPromisProcessList : " + logUserId + WSResponseObject.LOGGER_END);
        return processList;
    } 
 }
   */
}
