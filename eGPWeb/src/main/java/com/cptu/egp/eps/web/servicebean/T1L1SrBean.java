/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblBidderRank;
import com.cptu.egp.eps.model.table.TblEvalServiceWeightage;
import com.cptu.egp.eps.service.serviceimpl.ReportCreationService;
import com.cptu.egp.eps.service.serviceinterface.EvalServiceCriteriaService;
import com.cptu.egp.eps.web.databean.T1L1DtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author nishit
 */
public class T1L1SrBean {
    final Logger logger = Logger.getLogger(T1L1SrBean.class);
    String logUserId = "";

    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }


    ReportCreationService reportCreationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");

    EvalServiceCriteriaService criteriaService = (EvalServiceCriteriaService)AppContext.getSpringBean("EvalServiceCriteriaService");

    public List<T1L1DtBean> getT1L1Data(String tenderId , String rptId, String roundId,String userId){
        logger.debug("getT1L1Data : "+logUserId+" Starts");
        List<T1L1DtBean> finalList = new ArrayList<T1L1DtBean>();
        List<Object[]> t1 = new ArrayList<Object[]>();
        List<TblBidderRank> l1 = new ArrayList<TblBidderRank>();

        try {
             ReportCreationSrBean rptCreSrBean = new ReportCreationSrBean();
             rptCreSrBean.setRoundId(roundId);
            l1 = reportCreationService.findBidderRank(tenderId, rptId,roundId);
            criteriaService.saveT1Data(Integer.parseInt(tenderId), Integer.parseInt(userId));
              t1 = criteriaService.getT1Data(tenderId);
            Collections.sort(t1, new CompareReportT1());
            TblEvalServiceWeightage tblEvalServiceWeightage = new TblEvalServiceWeightage();
            tblEvalServiceWeightage = criteriaService.getTblEvalServiceByTenderId(Integer.parseInt(tenderId));
            int finWeigth = tblEvalServiceWeightage.getFinancialWeight();
            int tecWeigth = tblEvalServiceWeightage.getTechWeight();
            int fixed = 100;

            /*
             * for T1
             * t1obj[0] = bidderMarks
             * t1obj[1] = companyName
             * t1obj[2] = userId
             * T1 Score = Achieved Technical Point * Technical Weight (For e.g. 75%)
                L1 Points = Lowest Financial Score*100/ Actual Financial Proposal
                L1 Score = L1 Points * Financial Weight (For e.g. 25%).
                Total Score = T1 Score+ L1 Score
                Award contract to max. Total Score consultant.
             */
            Map<String,T1L1DtBean> dataMap = new HashMap<String, T1L1DtBean>();
            String companyName=null;
            int rank = 0;
            if(!t1.isEmpty() && !l1.isEmpty() && t1.size() == l1.size()){
            int counter = 1;
            double firstl1price = (l1.get(0).getAmount().doubleValue() * fixed);
            for(TblBidderRank l1obj : l1){
                for (Object[] t1obj : t1) {
                        if(l1obj.getUserId()==Integer.parseInt(t1obj[2].toString())){
                            T1L1DtBean t1L1DtBean = new T1L1DtBean();
                            t1L1DtBean.setBidderId(l1obj.getUserId());
                            t1L1DtBean.setL1price(l1obj.getAmount().doubleValue());
                            String data = null;
                                data = t1obj[1].toString();
                            t1L1DtBean.setCompanyName(data);
                            double amount = 0;
                            double t1score = 0;
                            double l1score = 0;
                            double l1points = 0;
                            t1score = (Double.parseDouble(t1obj[0].toString())*tecWeigth)/fixed;
                            t1L1DtBean.setT1Score(t1score);
                            l1points = firstl1price/l1obj.getAmount().doubleValue();
                            l1score = (l1points * finWeigth)/ fixed;
                            t1L1DtBean.setL1Score(l1score);
                            amount = t1score + l1score;
                            t1L1DtBean.setAmount(amount);
                            finalList.add(t1L1DtBean);
                            counter++;
                        }
                    }
                }
            Collections.sort(finalList, new CompareReportT1L1());
            }

        } catch (Exception e) {
            logger.error("getT1L1Data : "+logUserId+" :"+e);
        }
        logger.debug("getT1L1Data : "+logUserId+" Ends");
       return finalList;
    }


    public List<T1L1DtBean> getT1L1SaveData(String tenderId ,String roundId){
        logger.debug("getT1L1SaveData : "+logUserId+" Starts");
        // @return List<object []> object[0] bidderId, object[1] rank, object[2] amount
        List<TblBidderRank> T1L1list = new ArrayList<TblBidderRank>();
        List<T1L1DtBean> finalList = new ArrayList<T1L1DtBean>();
        try {
            T1L1list = criteriaService.getT1L1SaveData(Integer.parseInt(tenderId), Integer.parseInt(roundId));
            List<Object[]> l1data = criteriaService.getL1Data(tenderId, roundId);
            for (TblBidderRank tblBidderRank : T1L1list) {
            //int bidderId, String companyName, int rank, double amount, double t1Score, double l1Score
                T1L1DtBean dtBean = null;
                for (Object[] l1price : l1data) {
                    if(Integer.parseInt(l1price[0].toString())== tblBidderRank.getUserId()){
                        dtBean = new T1L1DtBean(tblBidderRank.getUserId(), tblBidderRank.getCompanyName(), tblBidderRank.getRank(), Double.parseDouble(tblBidderRank.getAmount().toString()), Double.parseDouble(tblBidderRank.getT1Score().toString()), Double.parseDouble(tblBidderRank.getL1Score().toString()),Double.parseDouble(l1price[1].toString()));
                    }
                }
           finalList.add(dtBean);
        }
        } catch (Exception e) {
            logger.error("getT1L1SaveData : "+logUserId+" "+e);
        }
        logger.debug("getT1L1SaveData : "+logUserId+" Ends");
        return finalList;


    }

}

