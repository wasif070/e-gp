/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.web.utility.AppContext;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cptu.egp.eps.dao.daointerface.TblAdvertisementDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblAdvertisement;
import com.cptu.egp.eps.model.table.TblAdvtConfigurationMaster;
import com.cptu.egp.eps.service.serviceinterface.AdvertisementService;
import com.cptu.egp.eps.service.serviceinterface.AdvtConfigMasterService;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

/**
 *
 * @author Lakshmi.yerramsetti
 */
public class MakeAdvertisement extends HttpServlet {

    private final AdvertisementService advertisementService = (AdvertisementService) AppContext.getSpringBean("AdvertisementService");
    private final AdvtConfigMasterService advtConfigMasterService = (AdvtConfigMasterService) AppContext.getSpringBean("advtconfigMasterService");
    private static final Logger LOGGER = Logger.getLogger(MakeAdvertisement.class);
    static final String STARTS = " Starts";
    static final String ENDS = " Ends";
    static final String EMPTY_STRING = "";
    private String userId = "0";
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    TblAdvertisementDao tblAdvertisementDao;

    public TblAdvertisementDao getTblAdvertisementDao() {
        return tblAdvertisementDao;
    }

    public void setTblAdvertisementDao(TblAdvertisementDao tblAdvertisementDao) {
        this.tblAdvertisementDao = tblAdvertisementDao;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String process = request.getParameter("adminaction");     
        String functionName = request.getParameter("funcName");
        String design = request.getParameter("uploadDocFile");
        String description = request.getParameter("txtdesc");
        String duration = request.getParameter("MyDropDown");
        String location = request.getParameter("MyDropDown1");
        String uname = request.getParameter("txtuser");
        String address = request.getParameter("txtaQuestion");
        String organisation = request.getParameter("txtorg");
        String email = request.getParameter("txtMailId");
        String phone = request.getParameter("txtPhone");
        String url = request.getParameter("txturl");
        try {
            if (functionName != null && functionName.equalsIgnoreCase("image")) {
                String DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("Advertisements");
                String realPath = "";
                realPath = DESTINATION_DIR_PATH + request.getParameter("adId");
                File imageDir = new File(realPath);
                List imageUrlList = new ArrayList();
                for (File imageFile : imageDir.listFiles()) {
                    String fileName = request.getParameter("image");
                    FileInputStream fis = new FileInputStream(new File(realPath + "\\" + imageFile.getName()));
                    BufferedInputStream bis = new BufferedInputStream(fis);
                    response.setContentType("image/jpeg");
                    BufferedOutputStream output = new BufferedOutputStream(response.getOutputStream());
                    for (int data; (data = bis.read()) > -1;) {
                        output.write(data);
                    }
                }
            } else if (("location".equalsIgnoreCase(functionName))) {
                PrintWriter out = response.getWriter();
                List<TblAdvtConfigurationMaster> list1 = advtConfigMasterService.findTblAdvtConfigurationMaster("location", Operation_enum.EQ, (request.getParameter("txtlocation")).toString(), "fee", Operation_enum.GT, 0.00);
                String str = "";
                for (int i = 0; i < list1.size(); i++) {
                    TblAdvtConfigurationMaster items = list1.get(i);
                    str += "<option value='" + items.getAdConfigId() + "'>" + items.getDuration() + "</option>";
                }
                String dimension = "", size = "", inst = "", format = "", price = "";
                if (list1.size() > 0) {
                    TblAdvtConfigurationMaster list2 = advtConfigMasterService.findTblAdvtConfigurationMaster("adConfigId", Operation_enum.EQ, list1.get(0).getAdConfigId()).get(0);
                    size = list2.getBannerSize();
                    dimension = list2.getDimensions();
                    format = list2.getFileFormat();
                    price = "" + list2.getFee();
                    inst = list2.getInstructions();
                }
                out.write(str + "~" + dimension + "~" + size + "~" + format + "~" + price + "~" + inst);
                out.close();
            } else if (("duration".equalsIgnoreCase(functionName))) {
                TblAdvtConfigurationMaster list2 = advtConfigMasterService.findTblAdvtConfigurationMaster("adConfigId", Operation_enum.EQ, Integer.parseInt(request.getParameter("adCId"))).get(0);
                PrintWriter out = response.getWriter();
                request.setAttribute("Advertisement", list2);
                String str = "";
                str += list2.getDimensions() + "~" + list2.getFee() + "~" + list2.getBannerSize() + "~" + list2.getFileFormat() + "~" + list2.getInstructions();
                out.write(str);
                out.close();
            } else if (("getAll".equalsIgnoreCase(functionName))) {
                List<TblAdvtConfigurationMaster> list1 = advtConfigMasterService.findTblAdvtConfigurationMaster("location", Operation_enum.EQ, (request.getParameter("txtlocation")).toString(), "fee", Operation_enum.GT, 0.00);
                String str = "", str1 = "";
                PrintWriter out = response.getWriter();
                for (int i = 0; i < list1.size(); i++) {
                    TblAdvtConfigurationMaster items = list1.get(i);
                    str += "<option value='" + items.getAdConfigId() + "'>" + items.getDuration() + "</option>";
                }
                if (list1.size() > 0) {
                    TblAdvtConfigurationMaster list2 = advtConfigMasterService.findTblAdvtConfigurationMaster("adConfigId", Operation_enum.EQ, list1.get(0).getAdConfigId()).get(0);
                    request.setAttribute("Advertisement", list2);
                    str1 += list2.getDimensions() + "~" + list2.getFee() + "~" + list2.getBannerSize() + "~" + list2.getFileFormat() + "~" + list2.getInstructions();
                }
                out.write(str + "~" + str1);
                out.close();
            } else if (functionName != null && functionName.equalsIgnoreCase("view")) {
                File tmpDir, destinationDir;
                String fileName = "", realPath = "";
                long fileSize = 0;
                String TMP_DIR_PATH = "c:\\tmp";
                tmpDir = new File(TMP_DIR_PATH);
                if (!tmpDir.isDirectory()) {
                    tmpDir.mkdir();
                }
                HttpSession sn = request.getSession();
                int userId = 0;
                Object objUserTypeId = sn.getAttribute("userTypeId");
                String DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("Advertisements");
                DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
                /*
                 *Set the size threshold, above which content will be stored on disk.
                 */
                fileItemFactory.setSizeThreshold(1 * 1024 * 1024); //1 MB
                /*
                 * Set the temporary directory to store the uploaded files of size above threshold.
                 */
                fileItemFactory.setRepository(tmpDir);
                ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
                List items = uploadHandler.parseRequest(request);
                Iterator itr = items.iterator();
                TblAdvertisement tblAdvertisement = new TblAdvertisement();
                //For form fields
                String s = "";
                while (itr.hasNext()) {
                    FileItem item = (FileItem) itr.next();
                    //For Supporting Document
                    /*
                     * Handle Form Fields.
                     */
                    if (item.isFormField()) {
                        if (item.getFieldName().equals("txtMailId")) {
                            tblAdvertisement.seteMailId(item.getString());
                        } else if (item.getFieldName().equals("txtaddress")) {
                            tblAdvertisement.setAddress(item.getString());
                        } else if (item.getFieldName().equals("txtuser")) {
                            tblAdvertisement.setUserName(item.getString());
                        } else if (item.getFieldName().equals("txtorg")) {
                            tblAdvertisement.setOrganization(item.getString());
                        } else if (item.getFieldName().equals("txtdesc")) {
                            tblAdvertisement.setDescription(item.getString());
                        } else if (item.getFieldName().equals("txtPhone")) {
                            tblAdvertisement.setPhoneNumber(item.getString());
                        } else if (item.getFieldName().equals("txturl")) {
                            tblAdvertisement.setUrl(item.getString());
                        } else if (item.getFieldName().equals("txtduration")) {
                            TblAdvtConfigurationMaster list2 = advtConfigMasterService.findTblAdvtConfigurationMaster("adConfigId", Operation_enum.EQ, Integer.parseInt(item.getString())).get(0);
                            tblAdvertisement.setDuration(list2.getDuration());
                            if ((objUserTypeId != null) && (objUserTypeId.toString().equalsIgnoreCase("8")) && sn.getAttribute("userId") != null) {
                                s = list2.getDuration();
                                Calendar c1 = Calendar.getInstance();
                                String[] splits = s.split(",");
                                int day = splits[0].indexOf("days");
                                int month = splits[1].indexOf("months");
                                int year = splits[2].indexOf("years");
                                String s1 = format.format(new Date());
                                String[] splits1 = s1.split(" ");
                                String[] splits2 = splits1[0].split("-");
                                c1.set(Integer.parseInt(splits2[0]), Integer.parseInt(splits2[1]) - 1, Integer.parseInt(splits2[2])); // 1999 jan 20
                                c1.add(Calendar.DATE, Integer.parseInt(splits[0].substring(0, day)));
                                c1.add(Calendar.MONTH, Integer.parseInt(splits[1].substring(0, month)));
                                c1.add(Calendar.YEAR, Integer.parseInt(splits[2].substring(0, year)));
                                Date expdate = c1.getTime();
                                tblAdvertisement.setExpiryDate(expdate);
                            }
                        } else if (item.getFieldName().equals("txtlocation")) {
                            tblAdvertisement.setLocation(item.getString());
                        } else if (item.getFieldName().equals("txtsize")) {
                            tblAdvertisement.setBannerSize(item.getString());
                        } else if (item.getFieldName().equals("txtdimension")) {
                            tblAdvertisement.setDimension(item.getString());
                        } else if (item.getFieldName().equals("txtprice")) {
                            tblAdvertisement.setPrice(item.getString());
                        }
                    } else {
                        if (item.getName().lastIndexOf("\\") != -1) {
                            fileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1, item.getName().length());
                        } else {
                            fileName = item.getName();
                        }
                        tblAdvertisement.setBannerName(fileName);
                        tblAdvertisement.setCreateDate(format.parse(format.format(new Date())));
                        if ((objUserTypeId != null) && (objUserTypeId.toString().equalsIgnoreCase("8")) && sn.getAttribute("userId") != null) {
                            userId = Integer.parseInt(sn.getAttribute("userId").toString());
                            tblAdvertisement.setUserId(userId);
                            tblAdvertisement.setStatus("published");
                            tblAdvertisement.setPublishDate(format.parse(format.format(new Date())));
                            tblAdvertisement.setPaymentDate(format.parse(format.format(new Date())));
                            tblAdvertisement.setApprovalDate(format.parse(format.format(new Date())));
                            tblAdvertisement.setComments("Advertisement by Content Admin");
                        } else {
                            tblAdvertisement.setUserId(userId);
                            tblAdvertisement.setCreateDate(format.parse(format.format(new Date())));
                            tblAdvertisement.setStatus("pending");
                        }
                    }
                }
                int id = advertisementService.addTblAdvertisement(tblAdvertisement);
                //For Supporting Document
                itr = items.iterator();
                while (itr.hasNext()) {
                    FileItem item = (FileItem) itr.next();
                    //For Supporting Document
                    /*
                     * Handle Form Fields.
                     */
                    if (!item.isFormField()) {
                        //Handle Uploaded files.
                        LOGGER.error("Field Name = " + item.getFieldName()
                                + ", File Name = " + item.getName()
                                + ", Content type = " + item.getContentType()
                                + ", File Size = " + item.getSize());
                        /*
                         * Write file to the ultimate location.
                         */
                        if (item.getName().lastIndexOf("\\") != -1) {
                            fileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1, item.getName().length());
                        } else {
                            fileName = item.getName();
                        }
                        fileSize = item.getSize();
                        realPath = DESTINATION_DIR_PATH + id;
                        destinationDir = new File(realPath);
                        if (!destinationDir.isDirectory()) {
                            destinationDir.mkdirs();
                        }
                        File file = new File(destinationDir, fileName);
                        item.write(file);
                        break;
                    }
                }
                MailContentUtility mailContentUtility = new MailContentUtility();
                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                if ((objUserTypeId != null) && (objUserTypeId.toString().equalsIgnoreCase("8")) && sn.getAttribute("userId") != null) {
                    List<TblAdvertisement> list = advertisementService.findTblAdvertisement("adId", Operation_enum.EQ, id);
                    String[] mail = {tblAdvertisement.geteMailId()};
                    String mailText = mailContentUtility.userMail(list.get(0).getAdId(), list.get(0).getStatus(), list.get(0).getPublishDate(), list.get(0).getExpiryDate(),tblAdvertisement.getComments(),"http://"+request.getServerName()+":"+request.getServerPort());
                    sendMessageUtil.setEmailTo(mail);
                    sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                    sendMessageUtil.setEmailSub("eGP: Advertisement published");
                    sendMessageUtil.setEmailMessage(mailText);
                    sendMessageUtil.sendEmail();
                    mail = null;
                }else{
                    for (Object object : advertisementService.contentAdmMailId()) {
                        String[] mail = {(String) object};
                        String mailText = mailContentUtility.contAdmAdMail(id);
                        sendMessageUtil.setEmailTo(mail);
                        sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                        sendMessageUtil.setEmailSub("eGP: Advertisement verification");
                        sendMessageUtil.setEmailMessage(mailText);
                        advertisementService.contentAdmMsgBox((String) object, XMLReader.getMessage("emailIdNoReply"), HandleSpecialChar.handleSpecialChar("Verification of Advertisement (" + id + ")."), mailContentUtility.contAdmAdMail(id));
                        sendMessageUtil.sendEmail();
                        mail = null;
                    }
                }

               response.sendRedirect("/MakeAdvertisements.jsp?msg=create");
            } else if (!(functionName == null) && (functionName.equalsIgnoreCase("Pending") || functionName.equalsIgnoreCase("Approved"))) {
                //preparing the grid
                PrintWriter out = response.getWriter();
                LOGGER.debug("processRequest " + userId + STARTS);
                response.setContentType("text/xml;charset=UTF-8");
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                boolean searchFromGrid = Boolean.parseBoolean(request.getParameter("_search"));
                List<TblAdvertisement> tblAdvertisement = new ArrayList<TblAdvertisement>();
                if (searchFromGrid) {
                    tblAdvertisement = getDataSearchFromGrid(request, response);
                } else {
                    tblAdvertisement = getDataOrderFromGrid(request, response);
                }
                //set pagination
                int totalPages = 0;
                long totalCount = tblAdvertisement.size();
                LOGGER.debug("totalCount : " + totalCount);
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(rows) == 0) {
                        totalPages = (int) (totalCount / Integer.parseInt(rows));
                    } else {
                        totalPages = (int) ((totalCount / Integer.parseInt(rows)) + 1);
                    }
                } else {
                    totalPages = 0;
                }
                //preparing XML for jquery grid
                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + page + "</page>");
                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + tblAdvertisement.size() + "</records>");
                int recordSize = 0;
                int no = Integer.parseInt(page);
                if (no == 1) {
                    recordSize = 0;
                } else {
                    if (Integer.parseInt(rows) == 30) {
                        recordSize = ((no - 1) * 30);
                    } else if (Integer.parseInt(rows) == 20) {
                        recordSize = ((no - 1) * 20);
                    } else {
                        recordSize = ((no - 1) * 10);
                    }
                }
                for (int i = recordSize; i < tblAdvertisement.size(); i++) {
                    if (functionName.equalsIgnoreCase("Pending") || functionName.equalsIgnoreCase("Approved")) {
                        out.print("<row id='" + tblAdvertisement.get(i).getAdId() + "'>");
                        out.print("<cell><![CDATA[" + tblAdvertisement.get(i).getAdId() + "]]> </cell>");
                        out.print("<cell><![CDATA[" + tblAdvertisement.get(i).getUserName() + "]]> </cell>");
                        out.print("<cell><![CDATA[" + tblAdvertisement.get(i).getDescription() + "]]> </cell>");
                        out.print("<cell><![CDATA[" + tblAdvertisement.get(i).getStatus() + "]]> </cell>");
                        out.print("<cell><![CDATA[" + tblAdvertisement.get(i).getDuration() + "]]> </cell>");
                        String viewLink = "";
                        String delLink = "";
                        if (tblAdvertisement.get(i).getStatus().equalsIgnoreCase("paid")) {
                            viewLink = "<a href=\"/MakeAdvertisement?wsOrgId="
                                    + tblAdvertisement.get(i).getAdId() + "&paidaction=paid\"'>Publish</a>";
                        } else if (tblAdvertisement.get(i).getStatus().equalsIgnoreCase("published")) {
                            viewLink = "<a href=\"/MakeAdvertisement?wsOrgId="
                                    + tblAdvertisement.get(i).getAdId() + "&pubaction=published\">View</a>";
                        } else if (tblAdvertisement.get(i).getStatus().equalsIgnoreCase("rejected") || tblAdvertisement.get(i).getStatus().equalsIgnoreCase("accepted")) {
                            viewLink = "<a href=\"/MakeAdvertisement?wsOrgId="
                                    + tblAdvertisement.get(i).getAdId() + "&action1=viewdet\">Processed</a>";
                        }else {
                            viewLink = "<a href=\"/MakeAdvertisement?wsOrgId="
                                    + tblAdvertisement.get(i).getAdId() + "&action1=viewdet\">Process</a>";
                        }
                        delLink= "<a href=\"/MakeAdvertisement?wsOrgId="
                                    + tblAdvertisement.get(i).getAdId() + "&action1=deldet\" onclick='return conform();'>Delete</a>";
                        out.print("<cell>");
                        out.print("<![CDATA[" + viewLink + " | "+ delLink +"]]>");
                        out.print("</cell>");
                        out.print("</row>");
                        recordSize++;
                    }
                }//end for loop
                out.print("</rows>");
                out.close();
            } else if (request.getParameter("action1") != null && request.getParameter("action1").equalsIgnoreCase("deldet")) {
                TblAdvertisement tbl = advertisementService.findTblAdvertisement("adId", Operation_enum.EQ, Integer.parseInt(request.getParameter("wsOrgId"))).get(0);
                advertisementService.deleteTblAdvertisement(tbl);
                response.sendRedirect("/admin/ViewAdvtTopic.jsp");
            } else if (!(request.getParameter("paidaction") == null) && (request.getParameter("paidaction").equalsIgnoreCase("paid"))) {
                String adid = request.getParameter("wsOrgId");
                TblAdvertisement tblAdvertisement = null;
                tblAdvertisement = ((List<TblAdvertisement>) advertisementService.findTblAdvertisement("adId", Operation_enum.EQ, Integer.parseInt(adid))).get(0);
                String stat = "published";
                Date pd = format.parse(format.format(new Date()));
                tblAdvertisement.setStatus(stat);
                tblAdvertisement.setPublishDate(pd);
                Calendar c1 = Calendar.getInstance();
                String s1 = format.format(new Date());
                String[] splits1 = s1.split(" ");
                String[] splits2 = splits1[0].split("-");
                c1.set(Integer.parseInt(splits2[0]), Integer.parseInt(splits2[1]) - 1, Integer.parseInt(splits2[2]));
                String s = tblAdvertisement.getDuration();
                String[] splits = s.split(",");
                int day = splits[0].indexOf("days");
                int month = splits[1].indexOf("months");
                int year = splits[2].indexOf("years");
                c1.add(Calendar.DATE, Integer.parseInt(splits[0].substring(0, day)));
                c1.add(Calendar.MONTH, Integer.parseInt(splits[1].substring(0, month)));
                c1.add(Calendar.YEAR, Integer.parseInt(splits[2].substring(0, year)));
                Date expdate = c1.getTime();
                tblAdvertisement.setExpiryDate(expdate);
                advertisementService.updateTblAdvertisement(tblAdvertisement);
                response.sendRedirect("/admin/ViewAdvtTopic.jsp?msg=succ");
            } else if (!(functionName == null) && (functionName.equalsIgnoreCase("process"))) {
                String comments = request.getParameter("comments");
                String adid = request.getParameter("adid");
                TblAdvertisement tblAdvertisement = null;
                tblAdvertisement = ((List<TblAdvertisement>) advertisementService.findTblAdvertisement("adId", Operation_enum.EQ, Integer.parseInt(adid))).get(0);
                tblAdvertisement.setComments(comments);
                tblAdvertisement.setApprovalDate(format.parse(format.format(new Date())));
                if (process.equalsIgnoreCase("Accepted")) {
                    String stat = "accepted";
                    tblAdvertisement.setStatus(stat);
                }
                if (process.equalsIgnoreCase("Rejected")) {
                    String stat = "rejected";
                    tblAdvertisement.setStatus(stat);
                }
                advertisementService.updateTblAdvertisement(tblAdvertisement);
                MailContentUtility mailContentUtility = new MailContentUtility();
                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                List<TblAdvertisement> list = advertisementService.findTblAdvertisement("adId", Operation_enum.EQ, Integer.parseInt(adid));
                for (TblAdvertisement tblAdvertisement1 : list) {
                    String[] mail = {tblAdvertisement1.geteMailId()};
                    String mailText = mailContentUtility.userMail(tblAdvertisement1.getAdId(), tblAdvertisement1.getStatus(), tblAdvertisement1.getPublishDate(), tblAdvertisement1.getExpiryDate(),tblAdvertisement1.getComments(),"http://"+request.getServerName()+":"+request.getServerPort());
                    sendMessageUtil.setEmailTo(mail);
                    sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                    sendMessageUtil.setEmailSub("eGP: Advertisement process");
                    sendMessageUtil.setEmailMessage(mailText);
                    sendMessageUtil.sendEmail();
                    mail = null;
                }
                response.sendRedirect("/admin/ViewAdvtTopic.jsp");
            } else if (!(request.getParameter("action1") == null) && (request.getParameter("action1").equalsIgnoreCase("viewdet"))) {
                List<TblAdvertisement> list = advertisementService.findTblAdvertisement("adId", Operation_enum.EQ, Integer.parseInt(request.getParameter("wsOrgId")));
                request.setAttribute("items", list);
                request.getRequestDispatcher("/admin/viewAdvt.jsp").forward(request, response);
            } else if (!(request.getParameter("pubaction") == null) && (request.getParameter("pubaction").equalsIgnoreCase("published"))) {
                List<TblAdvertisement> list = advertisementService.findTblAdvertisement("adId", Operation_enum.EQ, Integer.parseInt(request.getParameter("wsOrgId")));
                request.setAttribute("items", list);
                MailContentUtility mailContentUtility = new MailContentUtility();
                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                for (TblAdvertisement tblAdvertisement : list) {
                    String[] mail = {tblAdvertisement.geteMailId()};
                    String mailText = mailContentUtility.userMail(list.get(0).getAdId(), list.get(0).getStatus(), list.get(0).getPublishDate(), list.get(0).getExpiryDate(),tblAdvertisement.getComments(),"http://"+request.getServerName()+":"+request.getServerPort());
                    sendMessageUtil.setEmailTo(mail);
                    sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                    sendMessageUtil.setEmailSub("eGP: Advertisement publish");
                    sendMessageUtil.setEmailMessage(mailText);
                    sendMessageUtil.sendEmail();
                    mail = null;
                }
                request.getRequestDispatcher("/admin/viewAdvt.jsp?pubaction=published").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            LOGGER.error("processRequest " + userId + " : " + ex.getMessage());
        }
        LOGGER.debug("processRequest  : " + userId + ENDS);
    }

    /***
     * This method process the Search request from JQuery Grid.
     * @param request
     * @param response
     * @return List<TblAdvertisement>
     */
    private List<TblAdvertisement> getDataSearchFromGrid(HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.debug("getDataSearchFromGrid " + userId + STARTS);
        List<TblAdvertisement> tblAdvertisement = new ArrayList<TblAdvertisement>();
        String searchField = EMPTY_STRING, searchString = EMPTY_STRING, searchOperation = EMPTY_STRING;
        searchField = request.getParameter("searchField");
        searchString = request.getParameter("searchString");
        searchOperation = request.getParameter("searchOper");
        StringBuffer queryBuffer = new StringBuffer();
        queryBuffer.append(" WHERE ");
        String searchQuery = EMPTY_STRING;
        String column = "";
        if (searchField.equalsIgnoreCase("advtUserAdId")) {
            column = "adId";
            if (searchOperation.equalsIgnoreCase("cn")) {
                searchQuery = "TblAdvertisement.adId LIKE '%" + searchString + "%'";
            } else if (searchOperation.equalsIgnoreCase("eq")) {
                searchQuery = "TblAdvertisement.adId = '" + searchString + "'";
            }
            queryBuffer.append(searchQuery);
        }
        if (searchField.equalsIgnoreCase("advtUserUserName")) {
            column = "userName";
            if (searchOperation.equalsIgnoreCase("cn")) {
                searchQuery = "TblAdvertisement.userName LIKE '%" + searchString + "%'";
            } else if (searchOperation.equalsIgnoreCase("eq")) {
                searchQuery = "TblAdvertisement.userName = '" + searchString + "'";
            }
            queryBuffer.append(searchQuery);
        }
        if (searchField.equalsIgnoreCase("advtUserDescription")) {
            column = "description";
            if (searchOperation.equalsIgnoreCase("cn")) {
                searchQuery = "TblAdvertisement.description LIKE '%" + searchString + "%'";
            } else if (searchOperation.equalsIgnoreCase("eq")) {
                searchQuery = "TblAdvertisement.description = '" + searchString + "'";
            }
            queryBuffer.append(searchQuery);
        }
        if (searchField.equalsIgnoreCase("advtUserPaymentStatus")) {
            column = "status";
            if (searchOperation.equalsIgnoreCase("cn")) {
                searchQuery = "TblAdvertisement.status LIKE '%" + searchString + "%'";
            } else if (searchOperation.equalsIgnoreCase("eq")) {
                searchQuery = "TblAdvertisement.status = '" + searchString + "'";
            }
            queryBuffer.append(searchQuery);
        }
        if (searchField.equalsIgnoreCase("advtUserAdId")) {
            tblAdvertisement = advertisementService.findTblAdvertisement(column, Operation_enum.EQ, Integer.parseInt(searchString));
        } else {
            if (searchOperation.equalsIgnoreCase("cn")) {

                tblAdvertisement = advertisementService.findTblAdvertisement(column, Operation_enum.LIKE, "%" + searchString + "%");
            } else {
                tblAdvertisement = advertisementService.findTblAdvertisement(column, Operation_enum.EQ, searchString);
            }
        }
        LOGGER.debug("getDataSearchFromGrid " + userId + ENDS);
        return tblAdvertisement;
    }

    /***
     * This method process the Sort request from JQuery Grid.
     * @param request
     * @param response
     * @return List<TblAdvertisement>
     */
    private List<TblAdvertisement> getDataOrderFromGrid(HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.debug("getDataOrderFromGrid " + userId + STARTS);
        List<TblAdvertisement> tblAdvertisement = new ArrayList<TblAdvertisement>();
        String orderColumn = EMPTY_STRING;
        String order = EMPTY_STRING;
        String sortOrder = request.getParameter("sord");
        String sortIndex = request.getParameter("sidx");
        String ascending = "ASC";
        String descending = "DESC";
        if (sortIndex.equals("")) {
            orderColumn = "userName";
            order = ascending;
        } else if (sortIndex.equalsIgnoreCase("advtUserAdId")) {
            orderColumn = "adId";
            if (sortOrder.equalsIgnoreCase(ascending)) {
                order = ascending;
            } else if (sortOrder.equalsIgnoreCase(descending)) {
                order = descending;
            }
        } else if (sortIndex.equalsIgnoreCase("advtUserUserName")) {
            orderColumn = "userName";
            if (sortOrder.equalsIgnoreCase(ascending)) {
                order = ascending;
            } else if (sortOrder.equalsIgnoreCase(descending)) {
                order = descending;
            }
        } else if (sortIndex.equalsIgnoreCase("advtUserOrganization")) {
            orderColumn = "organization";
            if (sortOrder.equalsIgnoreCase(ascending)) {
                order = ascending;
            } else if (sortOrder.equalsIgnoreCase(descending)) {
                order = descending;
            }
        } else if (sortIndex.equalsIgnoreCase("advtUserDescription")) {
            orderColumn = "description";
            if (sortOrder.equalsIgnoreCase(ascending)) {
                order = ascending;
            } else if (sortOrder.equalsIgnoreCase(descending)) {
                order = descending;
            }
        } else if (sortIndex.equalsIgnoreCase("advtUserRemarks")) {
            orderColumn = "comments";
            if (sortOrder.equalsIgnoreCase(ascending)) {
                order = ascending;
            } else if (sortOrder.equalsIgnoreCase(descending)) {
                order = descending;
            }
        } else if (sortIndex.equalsIgnoreCase("advtUserPaymentStatus")) {
            orderColumn = "status";
            if (sortOrder.equalsIgnoreCase(ascending)) {
                order = ascending;
            } else if (sortOrder.equalsIgnoreCase(descending)) {
                order = descending;
            }
        }
        if (order.equalsIgnoreCase(ascending)) {
            if (request.getParameter("funcName").equalsIgnoreCase("Pending")) {
                tblAdvertisement = advertisementService.findTblAdvertisement(orderColumn, Operation_enum.ORDERBY, Operation_enum.ASC, "status", Operation_enum.NE, "published");
            } else if (request.getParameter("funcName").equalsIgnoreCase("Approved")) {
                tblAdvertisement = advertisementService.findTblAdvertisement(orderColumn, Operation_enum.ORDERBY, Operation_enum.ASC, "status", Operation_enum.EQ, "published");
            }
        } else {
            if (request.getParameter("funcName").equalsIgnoreCase("Pending")) {
                tblAdvertisement = advertisementService.findTblAdvertisement(orderColumn, Operation_enum.ORDERBY, Operation_enum.DESC, "status", Operation_enum.NE, "published");
            } else if (request.getParameter("funcName").equalsIgnoreCase("Approved")) {
                tblAdvertisement = advertisementService.findTblAdvertisement(orderColumn, Operation_enum.ORDERBY, Operation_enum.DESC, "status", Operation_enum.EQ, "published");
            }
        }
        LOGGER.debug("getDataOrderFromGrid " + userId + ENDS);
        return tblAdvertisement;
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
}
