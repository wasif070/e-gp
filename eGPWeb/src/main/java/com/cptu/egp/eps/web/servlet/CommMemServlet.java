/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.OfficeMemberDtBean;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import com.cptu.egp.eps.service.serviceimpl.CommitteMemberService;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.GovtUserCreationService;
import com.cptu.egp.eps.web.servicebean.TenderCommitteSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
//@WebServlet(name = "CommMemServlet", urlPatterns = {"/CommMemServlet"})
public class CommMemServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final Logger LOGGER = Logger.getLogger(CommMemServlet.class);
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            LOGGER.debug("processRequest :  Starts");
            String funName = request.getParameter("funName");
            if (funName.equals("memberSearch")) {
                out.print(otherPEData(request.getParameter("officeId"),Integer.parseInt(request.getParameter("pe1count")),request.getSession().getAttribute("userId").toString(),request.getParameter("tenderId")));
            }
            if (funName.equals("indiMemSearch")) {
                out.print(indiMemData(request.getParameter("emailId"),Integer.parseInt(request.getParameter("pe2count"))));
            }
            if (funName.equals("Office")) {
                out.print(getOffice(request.getParameter("deptId"), request.getParameter("tendId")));
            }
            if (funName.equals("getuser")) {
                out.print(getUserList("TenderExtRoleName", request.getParameter("tenderId"),request.getParameter("Roles")));
            }
            if (funName.equals("checkBidderexist")) {
                out.print(checkBidder(request.getParameter("tenderId"),request.getParameter("pkgId")));
            }
            LOGGER.debug("processRequest :  Ends");
        } catch (Exception ex) {
            LOGGER.error("processRequest :  exception  : "  +ex);
        } finally {
            out.flush();
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String otherPEData(String officeId,int counter,String userId,String tenderId) {
        CommitteMemberService committeMemberService = (CommitteMemberService) AppContext.getSpringBean("CommitteMemberService");
        List<OfficeMemberDtBean> list = committeMemberService.findOfficeMember(tenderId, officeId, "otheragency");
        List<Object> tecUser = null;
        if(tenderId!=null){
            tecUser = committeMemberService.getCommitteeMemIds(tenderId, "'TEC','PEC'");
        }
        int i=counter;
        StringBuilder sb = new StringBuilder();
       // System.out.println("------------------------------="+list.size());
        if(list != null && list.size() > 0)
        {
            for (OfficeMemberDtBean officeMemberDtBean : list) {
                if(!((officeMemberDtBean.getUserid()==Integer.parseInt(userId)) && officeMemberDtBean.getProcureRole().contains("PE"))){
                    boolean tecBool=true;
                    if(tecUser!=null && !tecUser.isEmpty()){
                        for (Object object : tecUser) {
                            if((Integer)object==officeMemberDtBean.getUserid()){
                                tecBool = false;
                            }
                        }
                    }
                    if(tecBool){
                        
                        String ofcRole="";
                        String ReplaceHOPE = officeMemberDtBean.getProcureRole();
                        if(ReplaceHOPE.contains("HOPE"))
                        {
                            ReplaceHOPE = ReplaceHOPE.replaceAll("HOPE", "HOPA");
                            ofcRole = "HOPA";
                        }
                        /** change by Nitish  
                        when it reads from PA,TOC/POC, TEC/PEC, TC from database
                        then replace A with E and POC and PEC will be vanished **/
                        if(ReplaceHOPE.contains("/POC"))
                        {
                            ReplaceHOPE = ReplaceHOPE.replaceAll("/POC", "");
                        }
                        if(ReplaceHOPE.contains("/PEC"))
                        {
                            ReplaceHOPE = ReplaceHOPE.replaceAll("/PEC", "");
                        }
                        /** change by nitish **/
                        if(ReplaceHOPE.contains("PE"))
                        {
                            int LocationOfPE = ReplaceHOPE.indexOf("PE");
                            char[] ReplaceHOPEChars = ReplaceHOPE.toCharArray();
                            if(LocationOfPE==0)
                            {

                                ReplaceHOPEChars[LocationOfPE+1] = 'A';
                                ReplaceHOPE = String.valueOf(ReplaceHOPEChars);

                            }
                            else
                            {
                                if(ReplaceHOPE.charAt(LocationOfPE-1)==',')
                                {
                                    ReplaceHOPEChars[LocationOfPE+1] = 'A';
                                    ReplaceHOPE = String.valueOf(ReplaceHOPEChars);
                                }

                            }
                        }
                        
                        
                        sb.append("<tr>");
                        sb.append("<td class='t-align-center'>");
                        sb.append("<label><input id='chk"+i+"' type='checkbox' value='"+officeMemberDtBean.getUserid()+"_"+officeMemberDtBean.getGovUserId()+"'/></label></td>");
                        sb.append("<td class='t-align-center'><input type='hidden' id='spn"+i+"' value='"+officeMemberDtBean.getEmpName()+"'/>"+officeMemberDtBean.getEmpName()+"</td>");
                        sb.append("<td class='t-align-center'><input type='hidden' id='pestat"+i+"' value='Other PA'/>"+officeMemberDtBean.getDesgName()+"</td>");
                        sb.append("<td class='t-align-center'>"+ReplaceHOPE+" <input type='hidden' name='ofcRole' id='ofcRole"+i+"' value='"+ofcRole+"' />    </td>");
                        sb.append("</tr>");
                        i++;
                    }
                }
            }
        }
        else
        {
            sb.append("<tr>");
            sb.append("<td class='t-align-center' colspan='4'>");
            sb.append("<label><font color='red'><b>No Record Found</b></font></label></td>");
            sb.append("</tr>");

        }

        sb.append("#"+i);
        //sb.append("<input type='hidden' id='pe2count' value='"+i+"'>");
        return sb.toString();
    }

    private String indiMemData(String emailId,int counter) {
        CommitteMemberService committeMemberService = (CommitteMemberService) AppContext.getSpringBean("CommitteMemberService");
        List<OfficeMemberDtBean> list = committeMemberService.findOfficeMember(emailId, "indconsultant");
        int i=counter;
        StringBuilder sb = new StringBuilder();
       // System.out.println("------------------------------="+list.size());
        if(list != null && list.size() > 0)
        {
            for (OfficeMemberDtBean officeMemberDtBean : list) {
                sb.append("<tr>");
                sb.append("<td class='t-align-center'>");
                sb.append("<label><input id='chk"+i+"' type='checkbox' value='"+officeMemberDtBean.getUserid()+"_"+officeMemberDtBean.getGovUserId()+"'/></label></td>");
                sb.append("<td class='t-align-center'><input type='hidden' id='pestat"+i+"' value='External Member'/>"+officeMemberDtBean.getDesgName()+"</td>");
                sb.append("<td class='t-align-center'><input type='hidden' id='spn"+i+"' value='"+officeMemberDtBean.getEmpName()+"'/>"+officeMemberDtBean.getEmpName()+"</td>");
                sb.append("</tr>");
                i++;
            }
        }
        else
        {
            sb.append("<tr>");
            sb.append("<td class='t-align-center' colspan='3'>");
            sb.append("<label><font color='red'><b>No Record Found</b></font></label></td>");
            sb.append("</tr>");

        }

        sb.append("#"+i);
        return sb.toString();
    }

    private String getOffice(String deptId,String tenderId) {
        GovtUserCreationService govtUserCreationService = (GovtUserCreationService) AppContext.getSpringBean("GovtUserCreationService");
        TenderCommitteSrBean bean = new TenderCommitteSrBean();
        String str = "";
        List<TblOfficeMaster> list = govtUserCreationService.officeMasterList(Integer.parseInt(deptId));
        int count = 0;
        try {
            if (list.size() > 0) {
                for (TblOfficeMaster object : list) {
                    if(!bean.getTenderOfficeId(tenderId).equals(""+object.getOfficeId())){
                        str += "<option value='" + object.getOfficeId() + "'>" + object.getOfficeName() + "</option>";
                    }
                }
            } else {
                str = "<option value='0'> No Office Found.</option>";
            }
        } catch (Exception e) {
        }
        return str;
    }
    private String getUserList(String first,String tenderId,String role) {
        TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        List<SPTenderCommonData> listOfUser = tenderCommonService1.returndata(first, tenderId, role);
        String str = "";
        try {
            if (listOfUser.size() > 0) {
                for (SPTenderCommonData object : listOfUser) {

                        str += "<option value='"+object.getFieldName1()+"-"+object.getFieldName3()+"'>" + object.getFieldName2() + "</option>";

                }
            } else {
                str = "<option value='0'> No User Found.</option>";
            }
        } catch (Exception e) {
        }
        return str;
    }
     private long checkBidder(String tenderId,String pkgId) {

         long result=0;
        try {
            TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            result =  tenderCommonService1.checkBidder(tenderId, pkgId);

        } catch (Exception e) {
        }
        return result;
    }


}
