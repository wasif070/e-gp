/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblConfigDocFees;
import com.cptu.egp.eps.service.serviceimpl.TenPaymentConfigService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author taher
 */
public class TenPaymentConfigServlet extends HttpServlet {
   
     private static final Logger LOGGER = Logger.getLogger(TenPaymentConfigServlet.class);
    private String logUserId = "0";

    private TenPaymentConfigService configService = (TenPaymentConfigService) AppContext.getSpringBean("TenPaymentConfigService");

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        LOGGER.debug("processRequest "+logUserId+" : Starts");
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
            configService.setLogUserId(logUserId);
            configService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
        }
        try {
            boolean flag = false;
            String action = request.getParameter("action");
            if ("evalInsert".equals(action)) {
                flag = insertConfigEvalMethod(request.getParameterValues("procType"), request.getParameterValues("tendType"), request.getParameterValues("procMethod"), request.getParameterValues("isDocFeesReq"), request.getParameterValues("isTenderSecReq"),request.getParameterValues("isPerSecFeesReq"),request.getParameterValues("isTenderValReq"));
                if (flag) {
                    response.sendRedirect("admin/ViewTenPaymentConfig.jsp?msg=insucc");
                }else{
                    response.sendRedirect("admin/TenPaymentConfiguration.jsp?msg=fail");
                }
            }else if ("evalUpdate".equals(action)) {
                flag = updateConfigEvalMethod(request.getParameterValues("procType"), request.getParameterValues("tendType"), request.getParameterValues("procMethod"), request.getParameterValues("isDocFeesReq"), request.getParameterValues("isTenderSecReq"),request.getParameterValues("isPerSecFeesReq"),request.getParameterValues("isTenderValReq"));
                if (flag) {
                    response.sendRedirect("admin/ViewTenPaymentConfig.jsp?msg=updsucc");
                }else {
                    response.sendRedirect("admin/TenPaymentConfiguration.jsp?isedit=y&msg=fail");
                }
            }else if ("delSingleEval".equals(action)) {
                int i = delSingleEval(request.getParameter("docFeesId"));
                if(i>0){
                    response.sendRedirect("admin/ViewTenPaymentConfig.jsp?msg=delsucc");
                }else{
                    response.sendRedirect("admin/ViewTenPaymentConfig.jsp?msg=delfail");
                }
            }else if ("editSingleEval".equals(action)) {
                flag = editSingleEval(new TblConfigDocFees(Integer.parseInt(request.getParameter("docFeesId")),"open", request.getParameter("isDocFeesReq"), Byte.parseByte(request.getParameter("procMethod")), Byte.parseByte(request.getParameter("procType")), Byte.parseByte(request.getParameter("tendType")), request.getParameter("isPerSecFeesReq"), request.getParameter("isTenderSecReq"), request.getParameter("isTenderValReq")));
                if(flag)
                {
                    response.sendRedirect("admin/ViewTenPaymentConfig.jsp?msg=editsucc");
                }else{
                    response.sendRedirect("admin/ViewTenPaymentConfig.jsp?msg=editfail");
                }
            }else if ("ajaxUniqueConfig".equals(action)) {
                out.print(ajaxUniqueConfig(request.getParameter("docFeesId"),request.getParameter("procType"),request.getParameter("tendType"),request.getParameter("procMethod")));
                out.flush();
            }

        }catch(Exception e){
            LOGGER.error("processRequest :"+e);
        }finally {
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private boolean insertConfigEvalMethod(String[] procType, String[] tendType, String[] procMethod, String[] isDocFeesReq, String[] isTenderSecReq,String[] isPerSecFeesReq,String[] isTenderValReq) {
        LOGGER.debug("insertConfigEvalMethod "+logUserId+" Starts");
        List<TblConfigDocFees> list = new ArrayList<TblConfigDocFees>();
        try{
            for (int i = 0; i < isDocFeesReq.length; i++) {
                list.add(new TblConfigDocFees(0, "open", isDocFeesReq[i], Byte.parseByte(procMethod[i]), Byte.parseByte(procType[i]), Byte.parseByte(tendType[i]), isPerSecFeesReq[i], isTenderSecReq[i], isTenderValReq[i]));
            }
        }catch(Exception e){
            LOGGER.debug("insertConfigEvalMethod "+logUserId+" :"+e);
        }
        LOGGER.debug("insertConfigEvalMethod "+logUserId+" Ends");
        return configService.insertConfigTenPayment(list);
    }

    private boolean updateConfigEvalMethod(String[] procType, String[] tendType, String[] procMethod, String[] isDocFeesReq, String[] isTenderSecReq,String[] isPerSecFeesReq,String[] isTenderValReq) {
        LOGGER.debug("updateConfigEvalMethod "+logUserId+" Starts");
        List<TblConfigDocFees> list = new ArrayList<TblConfigDocFees>();
        try{
                for (int i = 0; i < isDocFeesReq.length; i++) {
                list.add(new TblConfigDocFees(0, "open", isDocFeesReq[i], Byte.parseByte(procMethod[i]), Byte.parseByte(procType[i]), Byte.parseByte(tendType[i]), isPerSecFeesReq[i], isTenderSecReq[i], isTenderValReq[i]));
            }
        }catch(Exception e){
                LOGGER.error("updateConfigEvalMethod "+logUserId+" :"+e);
            }
            LOGGER.debug("updateConfigEvalMethod "+logUserId+"  Ends");
            return configService.updateConfigTenPayment(list);
    }
    private int delSingleEval(String docfeesId){
        LOGGER.debug("delSingleEval "+logUserId+" Starts");
        int cnt = 0;
        try{
            cnt =  configService.deleteSingleConfigTenPayment(docfeesId);
        }catch(Exception e){
            LOGGER.error("delSingleEval "+logUserId+" :"+e);
        }
        LOGGER.debug("delSingleEval "+logUserId+" Ends");
        return cnt;
    }
    private boolean editSingleEval(TblConfigDocFees evalMethod){
        LOGGER.debug("editSingleEval "+logUserId+" Starts");
        boolean flag = false;
        try {
             flag = configService.updateSingleConfigTenPayment(evalMethod);
        } catch (Exception e) {
        LOGGER.error("editSingleEval "+logUserId+" :"+e);
        }
        LOGGER.debug("editSingleEval "+logUserId+" Ends");
         return flag;
    }

    private String ajaxUniqueConfig(String docfeesId,String pType,String tendType,String pMethod) throws Exception{
        LOGGER.debug("ajaxUniqueConfig "+logUserId+" Starts");
        String val = "";
        try{

            val = String.valueOf(configService.uniqueConfigCount(docfeesId,pType, tendType, pMethod));
        }catch(Exception e){
        LOGGER.error("ajaxUniqueConfig "+logUserId+" :"+e);
        }
        LOGGER.debug("ajaxUniqueConfig "+logUserId+" Ends");
        return val;
    }
}
