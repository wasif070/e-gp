/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblConfigClarification;
import com.cptu.egp.eps.service.serviceimpl.ConfigClarificationService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author MD. Emtazul Haque
 */
public class ConfigClarificationServlet extends HttpServlet {


    private static final Logger LOGGER = Logger.getLogger(EvalMethodConfigServlet.class);
    private String logUserId = "0";



    
    private final ConfigClarificationService configService = (ConfigClarificationService) AppContext.getSpringBean("ConfigClarificationService");

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LOGGER.debug("processRequest "+logUserId+" : Starts");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
            configService.setLogUserId(logUserId);
            configService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
        }
        try {
            boolean flag = false;
            String action = request.getParameter("action");
            if ("ConfigClarificationInsert".equals(action)) {
                flag = insertConfigClarification(request.getParameterValues("SBDName"), request.getParameterValues("ClarificationDays"));
                if (flag) {
                    response.sendRedirect("admin/ViewConfigureClarificationDays.jsp?msg=insucc");
                }else{
                    response.sendRedirect("admin/ConfigureClarificationDays.jsp?msg=fail");
                }
            }else if ("ConfigClarificationUpdate".equals(action)) {
                flag = updateConfigEvalMethod(request.getParameterValues("SBDName"), request.getParameterValues("ClarificationDays"));
                if (flag) {
                    response.sendRedirect("admin/ViewConfigureClarificationDays.jsp?msg=updsucc");
                }else {
                    response.sendRedirect("admin/ConfigureClarificationDays.jsp?isedit=y&msg=fail");
                }
            }else if ("delSingleConfigClarification".equals(action)) {
                int i = delSingleConfigClarification(request.getParameter("ConfigClarificationId"));
                if(i>0){
                    response.sendRedirect("admin/ViewConfigureClarificationDays.jsp?msg=delsucc");
                }else{
                    response.sendRedirect("admin/ViewConfigureClarificationDays.jsp?msg=delfail");
                }
            }else if ("editSingleConfigClarification".equals(action)) {                
                flag = editSingleConfigClarification(new TblConfigClarification(Integer.parseInt(request.getParameter("ConfigClarificationId")), Short.valueOf(request.getParameter("SBDName")), Short.valueOf(request.getParameter("ClarificationDays"))));
                if(flag)
                {
                    response.sendRedirect("admin/ViewConfigureClarificationDays.jsp?msg=editsucc");
                }else{
                    response.sendRedirect("admin/ViewConfigureClarificationDays.jsp?msg=editfail");
                }
            }else if ("ajaxUniqueConfig".equals(action)) {
                out.print(ajaxUniqueConfig(request.getParameter("ConfigClarificationId"),request.getParameter("SBDName")));
                out.flush();
            }

        }catch(Exception e){
            LOGGER.error("processRequest :"+e);
        } finally {
            out.close();
        }

        LOGGER.debug("processRequest "+logUserId+" : Ends");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private boolean insertConfigClarification(String[] SBDNames, String[] ClarificationDays) {
        LOGGER.debug("insertConfigClarification "+logUserId+" Starts");
        List<TblConfigClarification> list = new ArrayList<TblConfigClarification>();
        try{
        for (int i = 0; i < ClarificationDays.length; i++) {
            list.add(new TblConfigClarification(0, Short.valueOf(SBDNames[i]), Short.valueOf(ClarificationDays[i])));
        }
        }catch(Exception e){
            LOGGER.debug("insertConfigEvalMethod "+logUserId+" :"+e);
        }
        LOGGER.debug("insertConfigClarification "+logUserId+" Ends");
        return configService.insertConfigClarification(list);
    }

    private boolean updateConfigEvalMethod(String[] SBDNames, String[] ClarificationDays) {
        LOGGER.debug("insertConfigClarification "+logUserId+" Starts");
        List<TblConfigClarification> list = new ArrayList<TblConfigClarification>();
        try{
        for (int i = 0; i < ClarificationDays.length; i++) {
            list.add(new TblConfigClarification(0, Short.valueOf(SBDNames[i]), Short.valueOf(ClarificationDays[i])));
        }
        }catch(Exception e){
                LOGGER.error("insertConfigClarification "+logUserId+" :"+e);
            }
            LOGGER.debug("insertConfigClarification "+logUserId+"  Ends");
        return configService.updateConfigClarification(list);
    }
    private int delSingleConfigClarification(String ConfigClarificationId){
        LOGGER.debug("delSingleConfigClarification "+logUserId+" Starts");
        int cnt = 0;
        try{
            cnt =  configService.deleteSingleConfigClarification(ConfigClarificationId);
        }catch(Exception e){
            LOGGER.error("delSingleConfigClarification "+logUserId+" :"+e);
    }
        LOGGER.debug("delSingleConfigClarification "+logUserId+" Ends");
        return cnt;
    }
    private boolean editSingleConfigClarification(TblConfigClarification tblConfigClarification){
        LOGGER.debug("editSingleConfigClarification "+logUserId+" Starts");
        boolean flag = false;
        try {
             flag = configService.updateSingleConfigClarification(tblConfigClarification);
        } catch (Exception e) {
        LOGGER.error("editSingleConfigClarification "+logUserId+" :"+e);
    }
        LOGGER.debug("editSingleConfigClarification "+logUserId+" Ends");
         return flag;
    }

    private String ajaxUniqueConfig(String ConfigClarificationId,String SBDName) throws Exception{
        LOGGER.debug("ajaxUniqueConfig "+logUserId+" Starts");
        String val = "";
        try{
            val = String.valueOf(configService.uniqueConfigCount(ConfigClarificationId,SBDName));
        }catch(Exception e){
        LOGGER.error("ajaxUniqueConfig "+logUserId+" :"+e);
    }
        LOGGER.debug("ajaxUniqueConfig "+logUserId+" Ends");
        return val;
    }
}
