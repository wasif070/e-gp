/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

/**
 *
 * @author nishit
 */
public class AppMessage {

    public static final String schedBankCrateErrMsg = "Scheduled Financial Institute creation failed";
    public static final String createAPPErrMsg = "Create APP failed";
    public static final String addPackageErrMsg = "Create Package failed";
    public static final String addPackageDatesErrMsg = "Add Package Dates failed";
    public static final String delPackageErrMsg = "Package Remove failed";
    public static final String departmentCreationM = "Ministry Creation failed";
    public static final String departmentCreationD = "Division Creation failed";
    public static final String departmentCreationO = "Organization Creation failed";
    public static final String departmentUpdateM = "Ministry Updation failed";
    public static final String departmentUpdateD = "Division Updation failed";
    public static final String departmentUpdateO = "Organization Updation failed";
    public static final String peOfficeCreation = "PE Office Creation failed";
    public static final String peOfficeUpdate = "PE Office Updation failed";
    public static final String orgAdminReg = "Organization Admin Creation failed";
    public static final String orgAdminUpdate = "Organization Admin Updation failed";
    public static final String peAdminReg = "PA Admin Creation failed";
    public static final String peAdminUpdate = "PA Admin Updation failed";
    public static final String devPartnerAdminReg = "Development Partner Admin Creation failed";
    public static final String devPartnerAdminUpdate = "Admin Updation failed";
    public static final String scheduleBankAdminReg = "Scheduled Financial Institute Admin Creation failed";
    public static final String scheduleBankAdminUpdate = "Admin Updation failed";
    public static final String scheduleBranchAdminUpdate = "Admin Updation failed";
    public static final String contentAdminReg = "Content Admin Creation failed";
    public static final String contentAdminUpdate = "Content Admin Updation failed";
    public static final String procurementExpert = " Procurement Expert Creation failed";
    public static final String procurementExpertUpdate = " Procurement Expert Updation failed";
    public static final String OAndMAdminReg = "O & M Admin Creation failed";
    public static final String OAndMAdminUpdate = "O & M Admin Updation failed";
    public static final String OAndMUserReg = "O & M User Creation failed";
    public static final String OAndMUserUpdate = "O & M User Updation failed";

    private static String appPath = "";

    public static void setAppPath(String appPath) {
        AppMessage.appPath = appPath;
    }

    public static String getAppPath() {
        return appPath;
    }
    private static String appUrl = "";

    public static String getAppUrl() {
        return appUrl;
    }

    public static void setAppUrl(String appUrl) {
        AppMessage.appUrl = appUrl;
    }
    

}
