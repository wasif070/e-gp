/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonAppData;
import com.cptu.egp.eps.model.table.TblCascadeLinkDetails;
import com.cptu.egp.eps.model.table.TblCascadeLinkMaster;
import com.cptu.egp.eps.service.serviceinterface.APPService;
import com.cptu.egp.eps.service.serviceinterface.CascadeDetailsService;
import com.cptu.egp.eps.service.serviceinterface.CascadeMasterService;
import com.cptu.egp.eps.web.servicebean.APPSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author tejasree.goriparthi
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

public class CascadeLinkServlet extends HttpServlet {

    private final CascadeDetailsService cascadeDetailsService = (CascadeDetailsService) AppContext.getSpringBean("CascadeDetailsService");
    private final CascadeMasterService cascadeMasterService = (CascadeMasterService) AppContext.getSpringBean("CascadeMasterService");
    private static final Logger LOGGER = Logger.getLogger(CascadeLinkServlet.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND  = "Ends";
    private String logUserId ="0";

    /**
     * This servlet is use to check bussiness rule configure at the time of Tender Creation.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        LOGGER.debug("processRequest : "+logUserId+ LOGGERSTART);
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        StringBuilder cascadeLink=new StringBuilder();
        try {
            HttpSession session = request.getSession();
            if(session.getAttribute("userId")!=null){
                logUserId = session.getAttribute("userTypeId").toString();
            }
            String PageName = request.getParameter("pageName").toString();
            String parentLink = "";
            if((request.getParameter("parentLink")!=null && !request.getParameter("parentLink").trim().equalsIgnoreCase("null"))  && !request.getParameter("parentLink").trim().equals("")){
                parentLink =request.getParameter("parentLink").toString();
            }
            if(PageName.indexOf(".jsp")>-1){
                PageName=PageName.substring(0,PageName.indexOf(".jsp")+4);
            }else if(PageName.indexOf(".htm")>-1){
                PageName=PageName.substring(0,PageName.indexOf(".htm")+4);
            }
            if(PageName.indexOf("/")>-1){
                PageName=PageName.substring(PageName.lastIndexOf("/")+1);
            }
            //System.out.println("pagename is.........."+PageName+"...........loginusertype........"+logUserId+ "........parent......"+parentLink);
            if (PageName != null && PageName.length() > 1) {
                List<TblCascadeLinkDetails> detailsList= cascadeDetailsService.getCascadeDetailsList(logUserId, "Yes",PageName,parentLink);
                if(detailsList.size()>1){
                    throw new Exception("More than one cascade links retrived");
                }
                if(detailsList!=null && detailsList.size()>0){
                    List<TblCascadeLinkMaster> masterList= cascadeMasterService.getCascadeMasterList(logUserId,detailsList.get(0).getParentLinkList());
                    for(int j=0;j<masterList.size();j++){
                        cascadeLink.append(masterList.get(j).getLinkName());
                        if(j+1<masterList.size()){
                            cascadeLink.append(" >> ");
                        }
                    }
                }
                out.print(cascadeLink.toString());
            }
            else
            {
                out.print("");
            }
        }catch(Exception ex){
            LOGGER.error("processRequest "+logUserId+" : "+ex.getMessage());
        }finally {
            out.close();
        }
        LOGGER.debug("processRequest  : "+logUserId+ LOGGEREND);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
