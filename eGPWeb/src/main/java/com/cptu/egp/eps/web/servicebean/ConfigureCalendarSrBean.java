/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblHolidayMaster;
import com.cptu.egp.eps.service.serviceimpl.HolidayMasterServiceImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author yanki
 */
public class ConfigureCalendarSrBean {

    private final HolidayMasterServiceImpl holidayMasterServiceImpl =
            (HolidayMasterServiceImpl) AppContext.getSpringBean("HolidayMasterServiceImpl");
    private static final Logger LOGGER = Logger.getLogger(DesignationSrBean.class);
    private String logUserId = "0";
    private AuditTrail auditTrail;

    public void setLogUserId(String logUserId) {
        holidayMasterServiceImpl.setLogUserId(logUserId);
        this.logUserId = logUserId;

    }

    /**
     * Insert Holidays
     * @param tblHolidayMaster to be inserted
     */
    public void addHolidayMasterData(TblHolidayMaster tblHolidayMaster) {
        LOGGER.debug("addHolidayMasterData : " + logUserId + " Starts");
        LOGGER.debug("addHolidayMasterData : " + logUserId + " Ends");
        holidayMasterServiceImpl.addTblHolidayMaster(tblHolidayMaster);
     }

    /**
     *Get all Holidays
     * @return List of TblHolidayMaster
     */
    public List<TblHolidayMaster> getHolidayMasterData() {
        LOGGER.debug("getHolidayMasterData : " + logUserId + " Starts");
        List<TblHolidayMaster> tblHolidayMasters = null;
       tblHolidayMasters = holidayMasterServiceImpl.getHolidayMasterData();
        LOGGER.debug("getHolidayMasterData : " + logUserId + " Ends");
        return tblHolidayMasters;
    }

    /**
     *Delete Holiday
     * @param tblHolidayMaster to be deleted
     */
    public void deletHolidayMaster(TblHolidayMaster tblHolidayMaster) {
        LOGGER.debug("deletHolidayMaster : " + logUserId + " Starts");
        LOGGER.debug("deletHolidayMaster : " + logUserId + " Ends");
        holidayMasterServiceImpl.deleteHolidayMaster(tblHolidayMaster);
    }

    /**
     *Counts Holidays
     * @return count
     */
    public long countHolidayMasterData() {
        LOGGER.debug("countHolidayMasterData : " + logUserId + " Starts");
        long l = 0;
        l = holidayMasterServiceImpl.countHolidayMasterData();
        LOGGER.debug("countHolidayMasterData : " + logUserId + " Ends");
        return l;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        holidayMasterServiceImpl.setAuditTrail(auditTrail);
        this.auditTrail = auditTrail;
    }
    
}
