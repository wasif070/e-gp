/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblScBankDevPartnerMaster;
import com.cptu.egp.eps.web.servicebean.ManageSbDpOfficeSrBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */

// Search Formation changed by Emtaz on 15/May/2016
public class ManageSbDpOfficeServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            //<editor-fold>
            /*if (request.getParameter("action").equals("fetchData")) {
                response.setContentType("text/xml;charset=UTF-8");
                
                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String sord = request.getParameter("sord");
                String sidx = request.getParameter("sidx");
                String type=request.getParameter("partnerType");
//                if (sidx.equals("")) {
//                    sidx = "sbDevelopName";
//                }
                
                //dcBean.setColName("");
                ManageSbDpOfficeSrBean manageSbDpOfficeSrBean=new ManageSbDpOfficeSrBean();
                manageSbDpOfficeSrBean.setSearch(_search);
                manageSbDpOfficeSrBean.setLimit(Integer.parseInt(rows));
                int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));
                System.out.println("offset:" + +offset);

                manageSbDpOfficeSrBean.setOffset(offset);
                manageSbDpOfficeSrBean.setSortOrder(sord);
                manageSbDpOfficeSrBean.setSortCol(sidx);


                System.out.println("queryString:>>" + request.getQueryString());
                
                String searchField = "", searchString = "", searchOper = "";
                List<TblScBankDevPartnerMaster> scDpOfficeList = null;
                if(_search){
                    searchField = request.getParameter("searchField");
                    searchString = request.getParameter("searchString");
                    searchOper = request.getParameter("searchOper");
                    scDpOfficeList = manageSbDpOfficeSrBean.getHeadOfficeList(type, searchField, searchString, searchOper);
                    System.out.println("scDpOfficeList.size():" + scDpOfficeList.size());
                    int totalPages = 0;
                long totalCount = 0;
                if(_search){
                    totalCount = manageSbDpOfficeSrBean.getCntHeadOffice(type, searchField, searchString, searchOper);
                } else {
                    totalCount = manageSbDpOfficeSrBean.getCntHeadOffice(type);
                }

                System.out.println("totalCount : "+totalCount);
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                        totalPages = (int) (totalCount / Integer.parseInt(request.getParameter("rows")));
                    } else {
                        totalPages = (int) ((totalCount / Integer.parseInt(request.getParameter("rows"))) + 1);
                    }
                } else {
                    totalPages = 0;
                }

                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + scDpOfficeList.size() + "</records>");
                // be sure to put text data in CDATA
                int j = offset+1;
                for (int i = 0; i < scDpOfficeList.size(); i++) {
                    out.print("<row id='" + scDpOfficeList.get(i).getSbankDevelopId() + "'>");
                    out.print("<cell><![CDATA[" + j + "]]></cell>");
                    out.print("<cell><![CDATA[" + scDpOfficeList.get(i).getSbDevelopName() + "]]></cell>");
                    out.print("<cell>" + scDpOfficeList.get(i).getTblCountryMaster().getCountryName() + "</cell>");
                    out.print("<cell>" + scDpOfficeList.get(i).getTblStateMaster().getStateName() + "</cell>");
                    String editLink="<a href=\"EditSchBankDevPartner.jsp?partnerType="+ type+"&officeId="+scDpOfficeList.get(i).getSbankDevelopId()+"&mode=edit\">Edit</a>";
                    String viewLink="<a href=\"ViewSchBankDevPartner.jsp?partnerType="+ type+"&officeId="+scDpOfficeList.get(i).getSbankDevelopId()+"&mode=view\">View</a>";
                    out.print("<cell><![CDATA[" + editLink+" | "+ viewLink+"]]></cell>");
                    out.print("</row>");
                     j++;
                }
                out.print("</rows>");
                } else {
                List<Object[]> sDpOfficeLst = manageSbDpOfficeSrBean.getHeadOfficeList(type);
                    System.out.println(sDpOfficeLst.size());
                int totalPages = 0;
                long totalCount = 0;
                if(_search){
                    totalCount = manageSbDpOfficeSrBean.getCntHeadOffice(type, searchField, searchString, searchOper);
                } else {
                    totalCount = manageSbDpOfficeSrBean.getCntHeadOffice(type);
                }

                System.out.println("totalCount : "+totalCount);
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                        totalPages = (int) (totalCount / Integer.parseInt(request.getParameter("rows")));
                    } else {
                        totalPages = (int) ((totalCount / Integer.parseInt(request.getParameter("rows"))) + 1);
                    }
                } else {
                    totalPages = 0;
                }

                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + sDpOfficeLst.size() + "</records>");
                // be sure to put text data in CDATA
                int j = offset+1;
                for (int i = 0; i < sDpOfficeLst.size(); i++) {
                   
                    out.print("<row id='" + sDpOfficeLst.get(i)[3] + "'>");
                    out.print("<cell><![CDATA[" + j + "]]></cell>");
                    out.print("<cell><![CDATA[" + sDpOfficeLst.get(i)[0] + "]]></cell>");
                    out.print("<cell>" + sDpOfficeLst.get(i)[1] + "</cell>");
                    out.print("<cell>" + sDpOfficeLst.get(i)[2] + "</cell>");
                    String editLink="<a href=\"EditSchBankDevPartner.jsp?partnerType="+ type+"&officeId="+sDpOfficeLst.get(i)[3]+"&mode=edit\">Edit</a>";
                    String viewLink="<a href=\"ViewSchBankDevPartner.jsp?partnerType="+ type+"&officeId="+sDpOfficeLst.get(i)[3]+"&mode=view\">View</a>";
                        out.print("<cell><![CDATA[" + editLink+" | "+ viewLink+"]]></cell>");
                    out.print("</row>");
                     j++;
                }
                out.print("</rows>");
                }
                
            }*/
            //</editor-fold>
            if (request.getParameter("action").equals("fetchData")) {
                
                
                ManageSbDpOfficeSrBean manageSbDpOfficeSrBean=new ManageSbDpOfficeSrBean();
                int pageNo = Integer.parseInt(request.getParameter("pageNo"));
                int Size = Integer.parseInt(request.getParameter("size"));
               
                manageSbDpOfficeSrBean.setLimit(1000000);
                int offset = 0;
                System.out.println("offset:" + +offset);
                manageSbDpOfficeSrBean.setOffset(offset);
              
                String type=request.getParameter("partnerType");
                String DpOfficeName = request.getParameter("DpOfficeName");
                String DpCountry = request.getParameter("DpCountry");
                String DpDistrict = request.getParameter("DpDistrict"); 
               

                System.out.println("queryString:>>" + request.getQueryString());
                
                List<Object[]> sDpOfficeLst = manageSbDpOfficeSrBean.getHeadOfficeList(type);
                List<Object[]> sDpOfficeLstSearched = new ArrayList<Object[]>();
                System.out.println(sDpOfficeLst.size());
                
                for(int j=0;j<sDpOfficeLst.size();j++)
                {
                    boolean ToAdd = true;
                    if(DpOfficeName!=null && !DpOfficeName.equalsIgnoreCase(""))
                    {
                        if(!sDpOfficeLst.get(j)[0].toString().toLowerCase().contains(DpOfficeName.toLowerCase()))
                        {
                            ToAdd = false;
                        }
                    }
                    if(DpCountry!=null && !DpCountry.equalsIgnoreCase(""))
                    {
                        if(!DpCountry.equalsIgnoreCase(sDpOfficeLst.get(j)[1].toString()))
                        {
                            ToAdd = false;
                        }
                    }
                    if(DpDistrict!=null && !DpDistrict.equalsIgnoreCase(""))
                    {
                        if(!DpDistrict.equalsIgnoreCase(sDpOfficeLst.get(j)[2].toString()))
                        {
                            ToAdd = false;
                        }
                    }

                    if(ToAdd)
                    {
                        //SPCommonSearchData commonAppData = getGovData.get(j);
                        sDpOfficeLstSearched.add(sDpOfficeLst.get(j));
                    }
                }
                    
                
                int RecordFrom = (pageNo-1)*Size;
                int k= 0;
                String styleClass = "";
                if (sDpOfficeLstSearched != null && !sDpOfficeLstSearched.isEmpty()) {
                    for(k=RecordFrom;k<RecordFrom+Size && k<sDpOfficeLstSearched.size();k++)
                    {
                        if(k%2==0){
                            styleClass = "bgColor-white";
                        }else{
                            styleClass = "bgColor-Green";
                        }
                        out.print("<tr class='"+styleClass+"'>");
                        out.print("<td width=\"5%\" class=\"t-align-center\">" + (k+1) + "</td>");
                        out.print("<td width=\"45%\" class=\"t-align-center\">" + sDpOfficeLstSearched.get(k)[0].toString() + "</td>");
                        out.print("<td width=\"15%\" class=\"t-align-center\">" + sDpOfficeLstSearched.get(k)[1].toString() + "</td>");
                        out.print("<td width=\"15%\" class=\"t-align-center\">" + sDpOfficeLstSearched.get(k)[2].toString() + "</td>");
                        String editLink="<a href=\"EditSchBankDevPartner.jsp?partnerType="+ type+"&officeId="+sDpOfficeLstSearched.get(k)[3]+"&mode=edit\">Edit</a>";
                        String viewLink="<a href=\"ViewSchBankDevPartner.jsp?partnerType="+ type+"&officeId="+sDpOfficeLstSearched.get(k)[3]+"&mode=view\">View</a>";
                        out.print("<td width=\"20%\" class=\"t-align-center\">" + editLink+ " | "+ viewLink + "</td>");
                        out.print("</tr>");
                    }
                }
                else
                {
                    out.print("<tr>");
                    out.print("<td colspan=\"5\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }  
                int totalPages = 1;
                if (sDpOfficeLstSearched.size() > 0) {
                    totalPages = (int) (Math.ceil(Math.ceil(sDpOfficeLstSearched.size()) / Size));
                    System.out.print("totalPages--"+totalPages+"records "+ sDpOfficeLstSearched.size());
                    if (totalPages == 0) {
                        totalPages = 1;
                    }
                }
               out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                    
            }
        }catch (Exception ex) {
            System.out.println("Exceotion :" + ex);
        } finally {
            out.flush();
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
