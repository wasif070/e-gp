/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import sun.misc.BASE64Decoder;

/**
 *
 * @author dipti
 */
public class TestEncDec {

    String encrypt = null;
    /**
     * Checks the Java Platform environment with its version
     * @return
     */
//    public int checkEnvironment() {
//        String java_version = System.getProperty("java.version");
//        if (!java_version.substring(0, 3).equals("1.6")) {
//            return (1);
//        }
//
//        return (0);
//    }
//    private String messageDigestType = "SHA1"; // Encryption Algorithm
    /**
     * Set Encrypt Algorithm
     * @param Algorithm Type
     */
//    public void setMessageDigestType(String type) {
//        messageDigestType = type;
//    }
    /**
     * Get Message sign string with SHA1 algorithm
     * @return Sign String value with SHA1 Algorithm
     */
//    public String getMessageDigest() {
//        String digest = null;
//        try {
//            MessageDigest md = MessageDigest.getInstance(messageDigestType);
//            byte[] badigest = md.digest(data.getBytes());
//            StringBuilder buf = new StringBuilder();
//            for (int i = 0; i < badigest.length; i++) {
//                String hex = Integer.toHexString(0xff & badigest[i]);
//                if (hex.length() == 1) {
//                    buf.append("0");
//                }
//                buf.append(hex);
//            }
//            digest = buf.toString();
//        } catch (NoSuchAlgorithmException ex) {
//            Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return (digest);
//    }
    /**
     * Get Policy checking status - get applet permission
     * @return boolean true / false
     */
//    public boolean policyTest() {
//        byte[] tData = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
//        boolean flag = true;
//
//        SecretKeySpec key64 = new SecretKeySpec(
//                new byte[]{0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07},
//                "Blowfish");
//
//        try {
//            Cipher c = Cipher.getInstance("Blowfish/ECB/NoPadding");
//
//            c.init(Cipher.ENCRYPT_MODE, key64);
//
//            c.doFinal(tData);
//        } catch (SecurityException e) {
//            if (e.getMessage() == null ? "Unsupported keysize or algorithm parameters" == null : e.getMessage().equals("Unsupported keysize or algorithm parameters")) {
//                flag = false;
//            } else {
//                flag = false;
//            }
//        } catch (GeneralSecurityException e) {
//            flag = false;
//        }
//
//        SecretKeySpec key192 = new SecretKeySpec(
//                new byte[]{0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
//                    0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
//                    0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17},
//                "Blowfish");
//
//        try {
//            Cipher c = Cipher.getInstance("Blowfish/ECB/NoPadding");
//
//            c.init(Cipher.ENCRYPT_MODE, key192);
//
//            c.doFinal(tData);
//
//        } catch (SecurityException e) {
//            if (e.getMessage() == null ? "Unsupported keysize or algorithm parameters" == null : e.getMessage().equals("Unsupported keysize or algorithm parameters")) {
//                flag = false;
//            } else {
//                flag = false;
//            }
//        } catch (GeneralSecurityException e) {
//            flag = false;
//        }
//
//        return flag;
//    }
    String password;

    /**
     * Get Symmetric Encrypt
     * @param password2
     * @return Encrypted String
     */
//    public String getSymEncrypt(String password2) {
//        this.password = password2;
//        String encrypted = AccessController.doPrivileged(new PrivilegedAction<String>() {
//
//            @Override
//            public String run() {
//                try {
//                    char[] bpassword = password.toCharArray();
//                    byte[] salt = new byte[8];
//                    Random r = new Random();
//                    r.nextBytes(salt);
//                    PBEKeySpec kspec = new PBEKeySpec(bpassword);
//                    SecretKeyFactory kfact = SecretKeyFactory.getInstance("pbewithmd5anddes");
//                    SecretKey key = kfact.generateSecret(kspec);
//                    PBEParameterSpec pspec = new PBEParameterSpec(salt, 1000);
//                    Cipher cipher = Cipher.getInstance("pbewithmd5anddes");
//                    cipher.init(Cipher.ENCRYPT_MODE, key, pspec);
//                    byte[] enc = cipher.doFinal(data.getBytes());
//                    BASE64Encoder encoder = new BASE64Encoder();
//                    String saltString = encoder.encode(salt);
//                    String ciphertextString = encoder.encode(enc);
//                    return saltString + ciphertextString;
//                } catch (IllegalBlockSizeException ex) {
//                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
//                } catch (BadPaddingException ex) {
//                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
//                } catch (InvalidKeyException ex) {
//                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
//                } catch (InvalidAlgorithmParameterException ex) {
//                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
//                } catch (NoSuchPaddingException ex) {
//                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
//                } catch (InvalidKeySpecException ex) {
//                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
//                } catch (NoSuchAlgorithmException ex) {
//                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
//                }
//                return null;
//            }
//        });
//        return encrypted;
//    }
    /**
     * Get Symmetric Decrypted String
     * @param password2
     * @return Decrypt String
     */
    public String getSymDecrypt(String password2) {
        this.password = password2;
        String test = "-1";
        try {
            String salt = encrypt.substring(0, 12);
            String ciphertext = encrypt.substring(12, encrypt.length());
            BASE64Decoder decoder = new BASE64Decoder();
            byte[] saltArray = decoder.decodeBuffer(salt);
            byte[] ciphertextArray = decoder.decodeBuffer(ciphertext);
            PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray());
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("pbewithmd5anddes");
            SecretKey key = keyFactory.generateSecret(keySpec);
            PBEParameterSpec paramSpec = new PBEParameterSpec(saltArray, 1000);
            Cipher cipher = Cipher.getInstance("pbewithmd5anddes");
            cipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
            byte[] plaintextArray = cipher.doFinal(ciphertextArray);
            test = new String(plaintextArray);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadPaddingException ex) {
            Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidAlgorithmParameterException ex) {
            Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return test;
    }

    public static void main(String[] args) {
        try {
            TestEncDec t = new TestEncDec();
            //t.encrypt = "tjyKiWCv62s=oK1CrxXXc1MmVmJWeX/fEuaqRyeEGb6IHdft+GKNyW2unldas97fvTV0N2wdpn0g/sctj2d/FGkd 0WqOEXAqKxtwAxJBb7eWCnYNtjAkh56DdriaZxEsgYgfqtskbUQPdppPPEADVXgRqGCWpguEqIfi kVDy2brD9Mut/MM1rerMXdQU5kKxJMfmgAPjYyXcByyLvEIgNvHNBkfiN8zhnuaZhTp06S33ad/N Hv70qKemrq1IMe6Vse1r6Kx4rlocPmWT4frGCBkpAWdQQSrcaa2nVm0wn9tQrhKgRq+CFgrCQfpz KCrZrEQyZGb3EREX4Rhk/gIySfcoaxhkU6vEup0M3XsGSP2qs/VUb2LO3A6J79z7NaPNZFmJHPHV oxWO+/xwZ/OrhE7E2gDiiMKwgNTIOWoMht0qV8sOrqgkyp75gbcXyo8UAfeeKURd/RBEcVtBPS34 l3seb2b6vKlPE2bhpMsrfNlO2njcjOmUIsEPswcWE3Nc69MtyxAYDhbZPhEOM3LfOkdsN1X49ilF 3hz6jq0+uKK4uDU18Z6NFa/JvVJyi7gGrBfxn0kNfEYvScnONyRFRSwgLjHEWNw1FB0UhnEvoHYn esWNZg9PGzz6iRjzxy69VjxAhTiHXm3E7Tt/IG+DR0/uGbQmmz6gII0WZK+Qw5fKKp47AivdR9mC vqpr7X1NLiFDXrXENAVq88jTcTHu12lsjCiZ0ibojkWUiaPk5Lx1fcBNaf9kBGmdUQE8ROAP6+gK H8bvqIzYzBG34uHRUgFxG4nudnx7AWmO7TJ5+6fzZhDnn+KHQ00juR6TRAEYRIWcPygomzcHmAos uU+EwaFyXhF9u4kh6GMsWTdJig2zSWeRSaS1yeoaDqZ8VtVDUrVvwyGCbKMah4SvJ+YZmfRwAhko a0VO8juLTAj3uvnOW94nsYJ5IqPIW3pDuXDgeh5FNA2svn++R19ta2ITFPJhk/8nMglYzt6Gf060 ypalgvjludjEztX3xZ4QDdWAtinWzXXdhJsTpi+zuF5gy+32BHPgIqxNnBKka4S/wDH0mzhzJuth fJv//riGCloAPSu4OqofeZ63srmJPB4ol3r3UofYYaqYMPLIuK874hcrw3u6PHZZarMY2uPCVDEX ne3bhsY5CzO7+7u6fouODt0oVCqQ2qOyyX4vkwXqPtE39bKCAEv7X5xSasfssy+Jfy23AZnEfg5N omec74jh58/F3lAjQtX/QmZrKeVE4rNpwWtMquNwsUPed9tTjieZc7LlmwM8Na3pV3F30jYOhBH+ ZncIRPd0AQ==".replaceAll(" ", "");
            //t.encrypt = "uUdBdsbTg9w=EDxfEOdzKQs=".replaceAll(" ", "");
            //t.encrypt = "nZ9X2wiGYi8=2fcMqJf3ttl=".replaceAll(" ", "");
            //t.encrypt = "ZTBajQ4cp8A=TammLYBua2c=".replaceAll(" ", "");
            //t.encrypt = "YtvJhUV3X1U=OZ9jEmgjyp3I3xRRX+wRAJSinH4QmL8lyW9mB3kk3+VS0+E5sEL/T1mX0hSMSRWFvr3W1bRLO2aHeCUZCvHsjvF4kdjosVtRi9X6b3TCzQXm5ijhGpxXk8hLbfYNLBUJ6Hk47T/skiK1WunkjHJke4mPYu6ZEf5YhgRRQp8sln16YK/x93WxqmPjSNDa7DfnidnPOEIgq9f1SmrtsUt2u5tiH8anoOP1zC9OllsMZAQMIsejxLkgE+he9Za8PRuyEnJ/Knx90FwzDCK5/F8z7t2N3b8t8iXYRjjiP+XVjr0HKqdlzHV3o9TNb20NHMvIOwqSmxXSN9IXgBdo1MRfIkf8G4xBUkak+Zxn8KPT72DHdQrKKk5tuNArefFFwQ6lQYnpAOnm394qf+xuDifyBbT6NyWCcXxYdQ1+mu4/YxRfED23tMKm+YlfBqjWCYxIRwh0Wxzx8oX2XXj8h9MQTVjlGeksHHkunKTioZ8mP2R6UNVm7seYC0NScuNAcby2GpwjByo70eWNiVMUrU/7m25ClQlfaJt4sT9zhwdLjALTC2mQyjKQsl2bFQB/FMO1C9TaG5l3hNOxF7VLh4PASdfSRhlgCinGRPTw3egxjSv9DvbMJXICFIryeIdlWeB7qZ0aw/xI1jihkeCtdPsgMOClAVDx0pFQO2AV0hanqLQrP2ffhhAp1sZEAs+DHuCCapoS72A8PM17XCmmC+j03s5s63gjtelznIA6iU+sSQ3h+racrsRGuYdi3fRC7bL70lhN9qxNos6zqY9DEPLZ4ji+4VhCC1onqH8UfKAo2t/IejpoO//qD/kvjuVd/6pD1erRkVHq+6sAa9e169fUg1FK2pcS3ZptA5w2azXbNd72OYRNyahaMtCMF++0Vy9t9SRwi3YuNWvSY2mpmp2VnF6jIrlt7y8oEjAs3sdUQ5SqV3yc+79cah9VLuudODqadbKqXdr34EY1ZmW6T03isvwggxHKl9lmM/6NnBBuvUV3crKMxhAteahR7Ja50ICen7og/oSzbvCQWuQXeOocy6nRcEhFmXrd9K+o+u2ALAdasvl0lVEvfk3Ax7ZcyKIYRoY3zlVekMt46HxwKBWm1PW5HnMD8qpJIqwmvjjWsmN1JrdaKHC5bK4Fg293gkooh95ZbFNb7j5MWOM3oUadkm60qRwi9QBIj3aVFQaY6KuDpCahgV8OD9uJhkf9CKX8bpcHg4E5ur5Rar0JPVPl/SVxi7GcGmv0ZpB9N/pu73Z7PHIVpyKf7mMmaik7JREO7sYYqE7NxMSif+aoj5eNVw==".replaceAll(" ", "");
           String data[] = {"YtvJhUV3X1U=OZ9jEmgjyp3I3xRRX+wRAJSinH4QmL8lyW9mB3kk3+VS0+E5sEL/T1mX0hSMSRWFvr3W1bRLO2aHeCUZCvHsjvF4kdjosVtRi9X6b3TCzQXm5ijhGpxXk8hLbfYNLBUJ6Hk47T/skiK1WunkjHJke4mPYu6ZEf5YhgRRQp8sln16YK/x93WxqmPjSNDa7DfnidnPOEIgq9f1SmrtsUt2u5tiH8anoOP1zC9OllsMZAQMIsejxLkgE+he9Za8PRuyEnJ/Knx90FwzDCK5/F8z7t2N3b8t8iXYRjjiP+XVjr0HKqdlzHV3o9TNb20NHMvIOwqSmxXSN9IXgBdo1MRfIkf8G4xBUkak+Zxn8KPT72DHdQrKKk5tuNArefFFwQ6lQYnpAOnm394qf+xuDifyBbT6NyWCcXxYdQ1+mu4/YxRfED23tMKm+YlfBqjWCYxIRwh0Wxzx8oX2XXj8h9MQTVjlGeksHHkunKTioZ8mP2R6UNVm7seYC0NScuNAcby2GpwjByo70eWNiVMUrU/7m25ClQlfaJt4sT9zhwdLjALTC2mQyjKQsl2bFQB/FMO1C9TaG5l3hNOxF7VLh4PASdfSRhlgCinGRPTw3egxjSv9DvbMJXICFIryeIdlWeB7qZ0aw/xI1jihkeCtdPsgMOClAVDx0pFQO2AV0hanqLQrP2ffhhAp1sZEAs+DHuCCapoS72A8PM17XCmmC+j03s5s63gjtelznIA6iU+sSQ3h+racrsRGuYdi3fRC7bL70lhN9qxNos6zqY9DEPLZ4ji+4VhCC1onqH8UfKAo2t/IejpoO//qD/kvjuVd/6pD1erRkVHq+6sAa9e169fUg1FK2pcS3ZptA5w2azXbNd72OYRNyahaMtCMF++0Vy9t9SRwi3YuNWvSY2mpmp2VnF6jIrlt7y8oEjAs3sdUQ5SqV3yc+79cah9VLuudODqadbKqXdr34EY1ZmW6T03isvwggxHKl9lmM/6NnBBuvUV3crKMxhAteahR7Ja50ICen7og/oSzbvCQWuQXeOocy6nRcEhFmXrd9K+o+u2ALAdasvl0lVEvfk3Ax7ZcyKIYRoY3zlVekMt46HxwKBWm1PW5HnMD8qpJIqwmvjjWsmN1JrdaKHC5bK4Fg293gkooh95ZbFNb7j5MWOM3oUadkm60qRwi9QBIj3aVFQaY6KuDpCahgV8OD9uJhkf9CKX8bpcHg4E5ur5Rar0JPVPl/SVxi7GcGmv0ZpB9N/pu73Z7PHIVpyKf7mMmaik7JREO7sYYqE7NxMSif+aoj5eNVw==",
              "nWAFiz9B1dM=sQwNvR7NY+U=",
              "QrD7/7qGFO0=UCxj5b21vRGVRLEiXdN1nvt4x2zChUvX",
              "yHkdEKx8W+E=HfXCEAfw6dw=",
              "5kW11+qZtJw=/yGk0u7SGrd/dRu4adF5YhgR2x2o1KKFu/d4I3eDwF1KXvEszZdFpODrsMGY/Uqdlf1qwg+gsYCp368QgBIVqtfjyTc+uu1k7Q0UbgCmWgymswuQ2DjfGY3enEV1ow7p3vf0nYFS8TyOeLfn1NCZGumW4lHIlh/1y3k/xa0w84gbJRp0S2KuhvfGwTxoJ/yR70Fn61TQQKu8ClEXnPYYAgx29FUemxXs",
              "cCk0N6BW8J0=VSwP7fXAyoCbS6sq3p/3EDsaxtj1VpS/bmoymWeK/mFV+OluESxZcwrMFAecSqJ1YSaUER6NEOKGG9PDocgW3eNyJSfigYomrK/HY+Oa2tAzOzC7riBseVEy906FVfcCRgqQEdSOe/ThaiVoOlByvdEe92CEwfiYmjbn3aoS3sxOUPJyx3R/y5KYEukg/3dUlUeyjTsqNW6ic5anuMjwNFzzw3bOxaJFne9SLvf6M7eZCRt8o53ywUOdBFGJjfGX1Ybg749C5a3Q115DHAv6fRhX0yTodIOXJSSmJXR6QeoHLxnznoNuq/xvQ+RkeCRZDTp3mnQ4mV39rxGsdW6yd0r2NxjLLHrQlEaPQO04nRgt1cRIDJNoOLGDGkdIzpCwKGZYqAUfb69qJo8aIq8bs5FDUH1M5DCtrvQOKflLYGGpxrFsEMgK61qkilhv4PN6nDfe7qkC+YXKjG4tC7VTRNnKv5mFaQiqyyxTpiXj7oVHVsOoSxy9IojInT3HHKe3y5cIMeCIM6HvSizGqrtiDR/YyFt93dn5HrS1vhjnk13JbeSkmrY8X6THhgBnx1sPRqDiiIVzxqucZaK41gycLO1CNFg8dJ8aHm8tv5EfpMK2oXG/j+937vZr0W8iZcCCoxfFl9K++C0SFjXrfnfzNO9Vkez+zIf2apx8TSYoqsl3FNoc7DmqB2wZM/MB9s6XRTFieqxgjZ64S5s2k5sHStD1Uom+0n6y0lC6WVsx6yhAbvjfUwB2e7o2soc/FNOfUzvF/dpGhVKdxaNQcj71XakmwkfQXYunUAeilN9sDo3L8RhjcAZakwulav8NfvFMHIxf3Pcidq5yT9CZEDv2P71VWv16meriA6mLFjn18HrtdlgZS6W6/rkaicyi+DYLX8TBnVbvMHlG/hysKJmN5DM7B/HxAIuMeusQFJQ5HFVV94+ECy7ogMCm1ZMQ585b7edxHQ8m/qNJaQU1Ao+sCvm/e7NxgUpkzMlmxkVCn88Fyfp6TiGfRJxReubxqOOhoD0JayehB7vv99uMSJvOodljdSCmVrcL/PEoMKl4CNcaM7WaOPuuvL/yjgSFTUxU06H9OYyVKyVS5rxiH51Dq/cIYNtZVjib9i4c73jGUtXa91G2fjKW1eo8Mzbglgw2IRQQYlF9cpo2QODvV3v/Tlx3tSHgXzdKPBidqp/DaFJPizYjJ4SO6DxZs2kVvr3oDWa+pCtFT/I=",
              "QFReklir0pk=njFoxtme1po=",
              "kF/W4spC0DQ=rZERQbTa4+h4dP83mjwJxA==",
              "aDmx/YPmYD0=CH5X3JBHVd7V3oSpT564c+mG4+2ZSTQqf5Za38lvRmS8t6W4eS1/W8O3f9PxJVivrQRS6XhJeY1eV7QSMZz+GxhS3eltqKWPk1kueY262Ao1gVOrWzS0R5PtZhByC4EuJ5PKlPpoWP5SBe8rb3vTZygq343d3JZKSx3VWqlbTQsh36dH5M8y9h4QzruwvNMp8kV43iAsvKS7I+fHuqzdRY64Zst2PQLVGubgv6NNvGrn+AHV/QLQiy20yK8y8dA0JhFwBeZXrXEjmu7KRRQjH0jjBoTYwfcgZNcAGpv7b2FoKupBF37FLeAF+pXVolj2RsqxyV1JKsUL1u3hevrjVqgWEsPyOSJcSbMfDJ7fgJDy5ZYPORxbOtb3KeVlNIq44q3hkkYejn1P+qYuYl1IxQ==",
              "pzLnw9/51Do=jhzFUd7CZIZT/U05gGCOmbHjgdJmDp3F",
              "UY3s/2LIw0A=ZTAEjX8RxQrPq0mkekpjaSzkk2UiI4h+3w4PdaE9OxY=",
              "0TChG4P1RAE=PanxnRzHsk12/d6ncjhHzBNVjDsOMNpvuREsk6IZZ4tUEBd3QiUPLn+ghG11yaJiH/Dyj7qkSfB3fwd4o6hwLqjosh5pDCzPW04NVgiwRQt0qiHtcfhdDhs8xNzWGOfs1HC13qk/bKumNaP1TtlWBAD1AwCyCjPp1kcHx26glRh2BtvuG6HoRxeUnUAQJtF+d4IgIbLuUCLAXwBjqF/+n511VYqz0yRO4a9CAaMuzpmEEHYQ/KvO9EUit3104tsStOBrFhhh/i3oVxkF6NNb2Wk9OuAOMIN+v2KlSJEGP5UKb9PdHZEjtVdQDBS3pw754m/y98fHAErUekfMXZPOGdDltEKWfoyayacwsQ9RjDYvUD9UTnKD9wwxUFQTaAzuohVS2UhINx1qWb5nJcMK+ahcTtW2/hde4uqgkCvsZ+gGihi6HL5tow==",
              "ya39zqo/gNo=QCSWQef0elg=",
              "owpQfDxSBpk=qz9xlbjpKNsJFZx6CkILgZ66lfWNiVSTwYBFIXEvZAA=",
              "m3kcEI+EHXg=55hs5I3JbherMCZ3b6nRJ++AIwV9Hvm5QoEunnPOlntgfpr3UwrS6C4dHN8sRlUShVPufxzN6VyVxjs8OdGAkaWylw6W9wRGpxiWNLG9MfsGFUR+1vPdQrfzZ1u72S6kS3zlYBtTbFfztNkkq6Gf0IXXkGLetWjbfBY8lcLLvaeqgSB2oMd5sj55Juk1Mz8hz1jrGt58XisTaYxL15QOEED7c0hugM0/5Q1eKIsaqemHbt4+3JIRjDXA2xCaDL2orD9BWryyHNEPW/XoyxoeIKBzBb4YJXGsPVgVkcm7H5uiHTnVSLRztVoMqwKqTs79AOVfKvPWJdtDwcyb/2wweLDgweBHF6RImYyyzdpHT6hTnueqX69WJ4qN4LwTyRYooRiEdzt6V1OIgt3NV/5iFxGKOOnpiMJgRTDZjT/jYVbYiZyrhYAC9LHQPPd6/czY+e0ML+lbVnAGW7JhmxY3N9yi8lO95qFWjoz+MYgCQDsXv7huXlpqf3s7aq/DsdcJYken0WvOk5Sri5jsTs/u6cuhoXMr270L",
              "wi3tMpFkllM=5Uw/ppCw45A=",
              "UyTAoJwnrwc=xGIhbZTgkEbSfFNqqXbBJA==",
              "OKNacdsdlSs=T2sHEzxEkUT6m9qzJoKchD9HWkmOZ0EdUOJtl9Zm/ULZ1D9iVNJ1EmzhQb6rd+gETh5E0YwIODXJcPBAxSgxFWrZQYB6xr68QjkpSglxXagpvlCLEjPpwl0R/5oV2Qa4eF/wQLUMjuOseto/m/fMzzVSz4vjnXzIA4CGEi2fT4jJSPbT/GMVg+SXVIpXmOWeccwUQXuxRx4hGFQ5e6uaUjXFdKHHaTaFu1c8WS7gzQfXPt4jaXXnYFXJ+YpRzUsNMcVYn/ECX77GQPeB0Cj78dRfvjXLp7Gu5SuH2+Xj9+yva5lQGo5NeejXDOo80JlMB+ISCUI8FcJqP6v8Hgzk/n+TDK8hXTwoEux7LUSPylKt4B7AQOede8xhvAt6SYILcr5Ul4ugctgEGRLpxAq9q3eoQygAc5JkCv69ucpqpUe9AGERcqQjySi1RhrqPDP09t5f6mFJkbEnC9U3WBrWdMZE2tFkIfqdXo/QD92xDUXYKAoSZ9kmlNgiqyJJWfjrVO0MGEE/OpfFAvfJd+tvMw0o2JaN94mEShtQMo35ApjkOw0UtgaaeI1y2WGI/bsuKYNFdbtQYvSDgpvuLHY70D9GWoomevqqvSODwu+/RpqrUP9PwNs+unxvwSuKUyOPJuHilvVZwtxlMz4Iht0A+iAFO2cs6FpjR7mcYjZJQ89AwMWWIxLTdsTL4io8EOzaR8X2Fp1jLSJXywkb25rkPmoJYNFxcLCsNJ9NuWypI8mlyjHT9UZSVum75cnL/ZKiAsnCAHrmy5ajoiXNW1U+2PnTlxnWjBfVgnW3t7CR+Sm2Oea7gL/OQDTjoDY+NTE72ndIxPyLMeyrvyxDYG+eoa8LhWuB2vX7"
           };
            System.out.println(data.length);
           String myData="";
           int i=0;
            for (String string : data) {
                
                t.encrypt = string.replaceAll(" ", "");
                //System.out.println(t.encrypt);
                myData += "<tbl_testdata testdata=\""+HandleSpecialChar.handleSpecialChar(t.mYmethod("4badd63133e520d718a8d001db03e9a4d086934e"))+"\"/>";

                //System.out.println("Decrypted Data : "+myData);

                try {
                    //myData = URLEncoder.encode(myData, "UTF-8");
                    //System.out.println("UTF : "+myData);
                } catch (Exception e) {
                    System.out.println(e+" "+i);
                }
                i++;
            }
            System.out.println("Done : "+myData);
        } catch (Exception e) {
            System.out.println("Errro : " + e);
        }
    }
    public String mYmethod(String paa) {
        String password = paa;
        String sata = "";
        try {
            String salt = encrypt.substring(0, 12);
            String ciphertext = encrypt.substring(12, encrypt.length());
            BASE64Decoder decoder = new BASE64Decoder();
            byte[] saltArray = decoder.decodeBuffer(salt);
            byte[] ciphertextArray = decoder.decodeBuffer(ciphertext);
            PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray());
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("pbewithmd5anddes");
            SecretKey key = keyFactory.generateSecret(keySpec);
            PBEParameterSpec paramSpec = new PBEParameterSpec(saltArray, 1000);
            Cipher cipher = Cipher.getInstance("pbewithmd5anddes");
            cipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
            byte[] plaintextArray = cipher.doFinal(ciphertextArray);
            sata = new String(plaintextArray);
        } catch (Exception e) {
            System.out.println("eee : " + e+" "+encrypt);
        }
        return sata;
    }
}
