/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblWsOrgMaster;
import com.cptu.egp.eps.service.serviceinterface.WsOrgMasterService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 * This Class is prepare JQuery grid for list of web-service consumers
 * @author Sreenu
 */
public class WebServiceConsumerDetailsServlet extends HttpServlet {

    static final Logger LOGGER = Logger.getLogger(WebServiceConsumerDetailsServlet.class);
    String logUserId = "0";
    static final String STARTS = " Starts";
    static final String ENDS = " Ends";
    static final String EMPTY_STRING = "";
    private WsOrgMasterService wsOrgMasterService = null;
    
    /***
     * This method process the requests from WSConsumerList.jsp and
     * prepare the JQuery grid.
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.debug("processRequest " + logUserId + STARTS);
        wsOrgMasterService = (WsOrgMasterService) AppContext.getSpringBean("WsOrgMasterService");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
            logUserId = session.getAttribute("userId").toString();
        }
        wsOrgMasterService.setLogUserId(logUserId);

        //preparing the grid
        response.setContentType("text/xml;charset=UTF-8");
        String rows = request.getParameter("rows");
        String page = request.getParameter("page");
        boolean searchFromGrid = Boolean.parseBoolean(request.getParameter("_search"));
        List<TblWsOrgMaster> webServiceConsumerList = new ArrayList<TblWsOrgMaster>();
        if (searchFromGrid) {
            webServiceConsumerList = getDataSearchFromGrid(request, response);
        } else {
            webServiceConsumerList = getDataOrderFromGrid(request, response);
        }
        //set pagination
        int totalPages = 0;
        long totalCount = webServiceConsumerList.size();
        LOGGER.debug("totalCount : " + totalCount);
        if (totalCount > 0) {
            if (totalCount % Integer.parseInt(rows) == 0) {
                totalPages = (int) (totalCount / Integer.parseInt(rows));
            } else {
                totalPages = (int) ((totalCount / Integer.parseInt(rows)) + 1);
            }
        } else {
            totalPages = 0;
        }
        //preparing XML for jquery grid
        out.print("<?xml version='1.0' encoding='utf-8'?>\n");
        out.print("<rows>");
        out.print("<page>" + request.getParameter("page") + "</page>");
        out.print("<total>" + totalPages + "</total>");
        out.print("<records>" + webServiceConsumerList.size() + "</records>");
        int recordSize = 0;
        int no = Integer.parseInt(request.getParameter("page"));
        if (no == 1) {
            recordSize = 0;
        } else {
            if (Integer.parseInt(rows) == 30) {
                recordSize = ((no - 1) * 30);
            } else if (Integer.parseInt(rows) == 20) {
                recordSize = ((no - 1) * 20);
            } else {
                recordSize = ((no - 1) * 10);
            }
        }        
        //'e-Mail','Organization Name','Contact Person(Mobile)','status','Action'
        for (int i = recordSize; i < webServiceConsumerList.size(); i++) {
            out.print("<row id='" + webServiceConsumerList.get(i).getOrgId() + "'>");
            out.print("<cell><![CDATA[" + webServiceConsumerList.get(i).getEmailId() + "]]> </cell>");
            out.print("<cell><![CDATA[" + webServiceConsumerList.get(i).getOrgNameEn() + "]]> </cell>");
            out.print("<cell><![CDATA[" + webServiceConsumerList.get(i).getContactPerson() + " "
                    + webServiceConsumerList.get(i).getMobileNo() + "]]> </cell>");
            String status = "";
            String statusChange = "";
            if ("N".equalsIgnoreCase(webServiceConsumerList.get(i).getIsDeleted())) {
                status = "Active";
                statusChange = "Deactivate";
            } else {
                status = "Deactivate";
                statusChange = "Active";
            }            
            out.print("<cell><![CDATA[" + status + "]]> </cell>");            
            String editLink = "<a href=\"WSConsumerEdit.jsp?wsOrgId="
                    + webServiceConsumerList.get(i).getOrgId() + "&action=edit\">Edit</a>";
            String viewLink = "<a href=\"WSConsumerView.jsp?wsOrgId="
                    + webServiceConsumerList.get(i).getOrgId() + "&action=view\">View</a>";
            String statusLink = "<a href=\"../WebServiceConsumerControlServlet?wsOrgId="
                    + webServiceConsumerList.get(i).getOrgId() + "&action=statusChange&status="
                    + statusChange + "\">" + statusChange + "</a>";
            out.print("<cell>");
            out.print("<![CDATA[" + editLink + "]]>");
            out.print(" | ");
            out.print("<![CDATA[" + viewLink + "]]>");
            out.print(" | ");
            out.print("<![CDATA[" + statusLink + "]]>");
            out.print("</cell>");
            out.print("</row>");
            recordSize++;
        }//end for loop
        out.print("</rows>");
        LOGGER.debug("processRequest " + logUserId + ENDS);
    }

    /***
     * This method process the Search request from JQuery Grid.
     * @param request
     * @param response
     * @return List<TblWsOrgMaster>
     */
    private List<TblWsOrgMaster> getDataSearchFromGrid(HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.debug("getDataSearchFromGrid " + logUserId + STARTS);
        List<TblWsOrgMaster> webServiceConsumerList = new ArrayList<TblWsOrgMaster>();
        String searchField = EMPTY_STRING, searchString = EMPTY_STRING, searchOperation = EMPTY_STRING;
        searchField = request.getParameter("searchField");
        searchString = request.getParameter("searchString");
        searchOperation = request.getParameter("searchOper");
        StringBuffer queryBuffer = new StringBuffer();
        queryBuffer.append(" WHERE ");
        String searchQuery = EMPTY_STRING;
        if (searchField.equalsIgnoreCase("wsConsumerEmail")) {
            if (searchOperation.equalsIgnoreCase("cn")) {
                searchQuery = "wsOrgMaster.emailId LIKE '%" + searchString + "%'";
            } else if (searchOperation.equalsIgnoreCase("eq")) {
                searchQuery = "wsOrgMaster.emailId = '" + searchString + "'";
            }
            queryBuffer.append(searchQuery);
        }
        if (searchField.equalsIgnoreCase("wsConsumerOrgName")) {
            if (searchOperation.equalsIgnoreCase("cn")) {
                searchQuery = "wsOrgMaster.orgNameEn LIKE '%" + searchString + "%'";
            } else if (searchOperation.equalsIgnoreCase("eq")) {
                searchQuery = "wsOrgMaster.orgNameEn = '" + searchString + "'";
            }
            queryBuffer.append(searchQuery);
        }
        if (searchField.equalsIgnoreCase("wsConsumerContact")) {
            if (searchOperation.equalsIgnoreCase("cn")) {
                searchQuery = "wsOrgMaster.contactPerson LIKE '%" + searchString + "%'";
            } else if (searchOperation.equalsIgnoreCase("eq")) {
                searchQuery = "wsOrgMaster.contactPerson = '" + searchString + "'";
            }
            queryBuffer.append(searchQuery);
        }        
        webServiceConsumerList = wsOrgMasterService.getRequiredTblWsOrgMasterList(queryBuffer.toString());
        LOGGER.debug("getDataSearchFromGrid " + logUserId + ENDS);
        return webServiceConsumerList;
    }

    /***
     * This method process the Sort request from JQuery Grid.
     * @param request
     * @param response
     * @return List<TblWsOrgMaster>
     */
    private List<TblWsOrgMaster> getDataOrderFromGrid(HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.debug("getDataOrderFromGrid " + logUserId + STARTS);
        List<TblWsOrgMaster> webServiceConsumerList = new ArrayList<TblWsOrgMaster>();
        String orderColumn = EMPTY_STRING;
        String order = EMPTY_STRING;
        String sortOrder = request.getParameter("sord");
        String sortIndex = request.getParameter("sidx");
        String ascending = "ASC";
        String descending = "DESC";
        if (sortIndex.equals("")) {
            orderColumn = "wsOrgMaster.emailId";
            order = ascending;
        } else if (sortIndex.equalsIgnoreCase("wsConsumerEmail")) {
            orderColumn = "wsOrgMaster.emailId";
            if (sortOrder.equalsIgnoreCase(ascending)) {
                order = ascending;
            } else if (sortOrder.equalsIgnoreCase(descending)) {
                order = descending;
            }
        } else if (sortIndex.equalsIgnoreCase("wsConsumerOrgName")) {
            orderColumn = "wsOrgMaster.orgNameEn";
            if (sortOrder.equalsIgnoreCase(ascending)) {
                order = ascending;
            } else if (sortOrder.equalsIgnoreCase(descending)) {
                order = descending;
            }
        } else if (sortIndex.equalsIgnoreCase("wsConsumerContact")) {
            orderColumn = "wsOrgMaster.contactPerson";
            if (sortOrder.equalsIgnoreCase(ascending)) {
                order = ascending;
            } else if (sortOrder.equalsIgnoreCase(descending)) {
                order = descending;
            }
        } else if (sortIndex.equalsIgnoreCase("wsConsumerStatus")) {
            orderColumn = "wsOrgMaster.isDeleted";
            if (sortOrder.equalsIgnoreCase(ascending)) {
                order = ascending;
            } else if (sortOrder.equalsIgnoreCase(descending)) {
                order = descending;
            }
        }
        String searchQuery = " ORDER BY " + orderColumn + " " + order;
        webServiceConsumerList = wsOrgMasterService.getRequiredTblWsOrgMasterList(searchQuery);
        LOGGER.debug("getDataOrderFromGrid " + logUserId + ENDS);
        return webServiceConsumerList;
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
