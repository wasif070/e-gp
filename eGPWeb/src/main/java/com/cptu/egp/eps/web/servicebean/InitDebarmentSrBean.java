/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblDebarmentComMembers;
import com.cptu.egp.eps.model.table.TblDebarmentComments;
import com.cptu.egp.eps.model.table.TblDebarmentCommittee;
import com.cptu.egp.eps.model.table.TblDebarmentDetails;
import com.cptu.egp.eps.model.table.TblDebarmentDocs;
import com.cptu.egp.eps.model.table.TblDebarmentReq;
import com.cptu.egp.eps.model.table.TblDebarmentResp;
import com.cptu.egp.eps.model.table.TblDebarmentTypes;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.service.serviceinterface.InitDebarmentService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author TaherT
 */
public class InitDebarmentSrBean {

    InitDebarmentService initDebarmentService = (InitDebarmentService) AppContext.getSpringBean("InitDebarmentService");
    CommonSearchService commonService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
    CommonSearchDataMoreService commonServiceDataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
    final Logger logger = Logger.getLogger(InitDebarmentSrBean.class);
    private String logUserId = "0";
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        initDebarmentService.setAuditTrail(auditTrail);
        this.auditTrail = auditTrail;
    }

    public void setLogUserId(String logUserId) {
        initDebarmentService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /**
     * Gives list of companyName and userId
     *
     * @param searchString where conditions are placed on
     * companyRegNumber,companyName,emailId
     * @return list of companyName and userId
     */
    public List<SPTenderCommonData> searchCompanyForDebar(String field, String value) {
        logger.debug("searchCompanyForDebar : " + logUserId + " Starts ");
        List<SPTenderCommonData> list = null;
        try {
            list = initDebarmentService.searchCompanyForDebar(field + " like '%" + value + "%'");
        } catch (Exception e) {
            logger.error("searchCompanyForDebar : " + logUserId + " : " + e);
        }
        logger.debug("searchCompanyForDebar : " + logUserId + " Ends");
        return list;
    }

    /**
     * Get Response of tenderer
     *
     * @param userId tendererId
     * @param debarId debarmentId of TblDebarmentReq
     * @return Response of tenderer
     * @throws Exception response fetching error
     */
    public List<Object[]> findDebarmentReqs(String userId, String debarId) {
        logger.debug("findDebarmentReqs : " + logUserId + " Starts ");
        List<Object[]> list = null;
        try {
            list = initDebarmentService.findDebarReqForResp(userId, debarId);
        } catch (Exception ex) {
            logger.error("findDebarmentReqs : " + logUserId + " : " + ex);
        }
        logger.debug("findDebarmentReqs : " + logUserId + " Ends");
        return list;
    }

    /**
     * List of TblDebarmentTypes
     *
     * @return List of TblDebarmentTypes
     */
    public List<TblDebarmentTypes> getAllDebars() {
        logger.debug("getAllDebars : " + logUserId + " Starts ");
        List<TblDebarmentTypes> list = null;
        try {
            list = initDebarmentService.getDebarTypes();
        } catch (Exception e) {
            logger.error("getAllDebars : " + logUserId + " : " + e);
        }
        logger.debug("getAllDebars : " + logUserId + " Ends");
        return list;
    }

    /**
     * Return the List of data based on debarmentType
     *
     * @param debarType
     * 1(getsingletender),2(getPackage),3(getProject),4(getProcuringEntity),5(getorganisation)
     * @param userId peId or hopeId
     * @return List of data based on debarmentType
     * @throws Exception data fetching problem from SP
     */
    public List<SPTenderCommonData> searchDataForDebarType(String debarType, String userId) {
        logger.debug("searchDataForDebarType : " + logUserId + " Starts ");
        List<SPTenderCommonData> list = null;
        try {
            list = initDebarmentService.searchDataForDebarType(debarType, userId);
        } catch (Exception e) {
            logger.error("searchDataForDebarType : " + logUserId + " : " + e);
        }
        logger.debug("searchDataForDebarType : " + logUserId + " Ends");
        return list;
    }

    /**
     * Checks whether tenderer response is available or not
     *
     * @param debarId debarmentId of TblDebarmentReq
     * @return count 0 or 1
     * @throws Exception
     */
    public long isResponseAvail(String debarId) {
        logger.debug("isResponseAvail : " + logUserId + " Starts ");
        long lng = 0;
        try {
            lng = initDebarmentService.isResponseAvail(debarId);
        } catch (Exception e) {
            logger.error("isResponseAvail : " + logUserId + " : " + e);
        }
        logger.debug("isResponseAvail : " + logUserId + " Ends");
        return lng;
    }

    /**
     * Debarment Clarification of Tenderer,PE
     *
     * @param debarId debarmentId of TblDebarmentReq
     * @param status debarmentStatus
     * @return ebarment Clarification of Tenderer,PE
     * @throws Exception
     */
    public List<Object[]> viewTendererDebarClari(String debarId, String status) {
        logger.debug("viewTendererDebarClari : " + logUserId + " Starts ");
        List<Object[]> list = null;
        try {
            list = initDebarmentService.viewTendererDebarClari(debarId, status);
        } catch (Exception e) {
            logger.error("viewTendererDebarClari : " + logUserId + " : " + e);
        }
        logger.debug("viewTendererDebarClari : " + logUserId + " Ends");
        return list;
    }

    /**
     * List of Tenderer Clarification
     *
     * @param debarId debarmentId of TblDebarmentReq
     * @param status debarmentStatus
     * @return
     * @throws Exception
     */
    public List<SPTenderCommonData> viewTendererDebarClari(int debarId, String status) {
        logger.debug("viewTendererDebarClari : " + logUserId + " Starts ");
        List<SPTenderCommonData> list = null;
        try {
            list = initDebarmentService.viewTendererDebarClari(debarId, status);
        } catch (Exception e) {
            logger.error("viewTendererDebarClari : " + logUserId + " : " + e);
        }
        logger.debug("viewTendererDebarClari : " + logUserId + " Ends");
        return list;
    }

    /**
     * This method updates debarment status and send mail to hope
     *
     * @param debarId debarmentId of TblDebarmentReq
     * @param debarStatus status which is to be updated
     * @param comments from pe
     * @param hopeId to which debar Request is send
     * @param cmpName Company which is Debarred
     * @param peName name of the PE who initiates Debarment
     * @return
     */
    public int updateDebarByPE(String debarId, String debarStatus, String comments, String hopeId, String cmpName, String peName) {
        logger.debug("updateDebarByPE : " + logUserId + " Starts ");
        int i = 0;
        try {
            i = initDebarmentService.updateDebarByPE(debarId, debarStatus, comments, hopeId);
            if (i > 0) {
                if ("sendtohope".equals(debarStatus)) {
                    //List<Object> list = initDebarmentService.getDebarEmailIds(debarId);
                    MailContentUtility mailContentUtility = new MailContentUtility();
                    MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                    sendMailMsgBox(initDebarmentService.getHopeEmailIdForDebar(debarId).toString(), "e-GP: Debarment request from PE", mailContentUtility.debarmentRequestToHope(peName, cmpName), msgBoxContentUtility.debarmentRequestToHope(peName, cmpName), initDebarmentService.getPEEmailId(debarId).toString());
                    mailContentUtility = null;
                    msgBoxContentUtility = null;
                }
            }
        } catch (Exception e) {
            logger.error("updateDebarByPE : " + logUserId + " : " + e);
        }
        logger.debug("updateDebarByPE : " + logUserId + " Ends");
        return i;
    }

    /**
     * List of TblDebarmentDetails Criteria query in TblDebarmentDetails
     *
     * @param values variable parameter for query
     * @return List of TblDebarmentDetails
     * @throws Exception
     */
    public List<TblDebarmentDetails> getDebarDetailsForHope(Object... values) {
        logger.debug("getDebarDetailsForHope : " + logUserId + " Starts ");
        List<TblDebarmentDetails> list = null;
        try {
            list = initDebarmentService.getDebarDetails(values);
        } catch (Exception e) {
            logger.error("getDebarDetailsForHope : " + logUserId + " : " + e);
        }
        logger.debug("getDebarDetailsForHope : " + logUserId + " Ends");
        return list;
    }

    /**
     * This method updates debarment status and send mail to eGPAdmin
     *
     * @param debarId debarmentId of TblDebarmentReq
     * @param debarIds of TblDebarmentDetails
     * @param debarStatus of TblDebarmentReq
     * @param comments of TblDebarmentComments
     * @param debarBy of TblDebarmentDetails
     * @param debarStart of TblDebarmentReq
     * @param debarEnd of TblDebarmentReq
     * @param debarTypeId of TblDebarmentReq
     * @param userName name of the user
     * @param cmpName user's Company Name
     */
    public void updateDebarByHope(String debarId, String debarIds, String debarStatus, String comments, String debarBy, Date debarStart, Date debarEnd, String debarTypeId, String userName, String cmpName) {
        logger.debug("updateDebarByHope : " + logUserId + " Starts ");
        try {
            if (initDebarmentService.updateDebarByHope(debarId, debarIds, debarStatus, comments, debarBy, debarStart, debarEnd, debarTypeId)) {
                if ("sendtoegp".equals(debarStatus)) {
                    //List<Object> list1 = initDebarmentService.getDebarEmailIds(debarId);
                    MailContentUtility mailContentUtility = new MailContentUtility();
                    MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                    sendMailMsgBox(initDebarmentService.getEgpAdmin().toString(), "e-GP: Debarment request", mailContentUtility.hopeNotifiesForDebarment(cmpName), msgBoxContentUtility.hopeNotifiesForDebarment(cmpName), initDebarmentService.getHOPEEmailId(debarId).toString());
                    mailContentUtility = null;
                    msgBoxContentUtility = null;
                }
            }
        } catch (Exception e) {
            logger.error("updateDebarByHope : " + logUserId + " : " + e);
        }
        logger.debug("updateDebarByHope : " + logUserId + " Ends");
    }

    /**
     * using tables tbl_DebarmentCommittee,tbl_DebarmentComMembers
     *
     * @param commName Name of the committee
     * @param createdBy Created By HopeId
     * @param debarId debarmentId of TblDebarmentReq
     * @param userId committee member's userIds
     * @param memType committee member's types(Chairperson,Member)
     */
    public void createDebarCommittee(String commName, String createdBy, String debarId, String[] userId, String[] memType) {
        logger.debug("createDebarCommittee : " + logUserId + " Starts ");
        try {
            TblDebarmentCommittee committee = new TblDebarmentCommittee(0, new TblDebarmentReq(Integer.parseInt(debarId)), commName, Integer.parseInt(createdBy), new Date(), "pending");
            List<TblDebarmentComMembers> list = new ArrayList<TblDebarmentComMembers>();
            for (int i = 0; i < userId.length; i++) {
                list.add(new TblDebarmentComMembers(0, null, Integer.parseInt(userId[i]), memType[i]));
            }
            initDebarmentService.createDebarCommittee(committee, list);
        } catch (Exception e) {
            logger.error("createDebarCommittee : " + logUserId + " : " + e);
        }
        logger.debug("createDebarCommittee : " + logUserId + " Ends");
    }

    /**
     * Committee Existence check and committee details fetching
     *
     * @param debarId debarmentId of TblDebarmentReq
     * @return List of data from TblDebarmentCommittee
     */
    public List<Object[]> isCommitteeExist(String debarId) {
        logger.debug("isCommitteeExist : " + logUserId + " Starts ");
        List<Object[]> list = null;
        try {
            list = initDebarmentService.isCommitteeExist(debarId);
        } catch (Exception e) {
            logger.error("isCommitteeExist : " + logUserId + " : " + e);
        }
        logger.debug("isCommitteeExist : " + logUserId + " Ends");
        return list;
    }

    /**
     * SP returning committee user's with their details based on
     * debarCommitteeId
     *
     * @param debarComId of TblDebarmentCommittee
     * @return Data from SP
     */
    public List<SPTenderCommonData> savedDebarmentComUser(String debarComId) {
        logger.debug("savedDebarmentComUser : " + logUserId + " Starts ");
        List<SPTenderCommonData> list = null;
        try {
            list = initDebarmentService.savedDebarmentComUser(debarComId);
        } catch (Exception e) {
            logger.error("savedDebarmentComUser : " + logUserId + " : " + e);
        }
        logger.debug("savedDebarmentComUser : " + logUserId + " Ends");
        return list;
    }

    /**
     * Update Debarment committee details
     *
     * @param comId of TblDebarmentCommittee
     * @param comName committee name from TblDebarmentCommittee
     * @param userId committee member's userIds
     * @param memType committee member's types(Chairperson,Member)
     */
    public void updateDebarCommittee(String commName, String comId, String[] userId, String[] memType) {
        logger.debug("updateDebarCommittee : " + logUserId + " Starts ");
        try {
            List<TblDebarmentComMembers> list = new ArrayList<TblDebarmentComMembers>();
            for (int i = 0; i < userId.length; i++) {
                list.add(new TblDebarmentComMembers(0, new TblDebarmentCommittee(Integer.parseInt(comId)), Integer.parseInt(userId[i]), memType[i]));
            }
            initDebarmentService.updateDebarCommittee(comId, commName, list);
        } catch (Exception e) {
            logger.error("updateDebarCommittee : " + logUserId + " : " + e);
        }
        logger.debug("updateDebarCommittee : " + logUserId + " Ends");
    }

    /**
     * Publishes committee and sends a notification mail to all members
     *
     * @param comId committee member Id
     * @param comName committee name
     * @param debarId debarmentId of TblDebarmentReq
     * @param userName name of user which is debared
     * @param cmpName name if company which is debared
     * @param debarBy Id of Hope which performs action
     */
    public void publishDebarCommitte(String comId, String comName, String debarId, String userName, String cmpName, String debarBy) {
        logger.debug("publishDebarCommitte : " + logUserId + " Starts ");
        try {
            if (initDebarmentService.publishDebarCommitte(comId)) {
                List<Object> list = initDebarmentService.getDebarCommitteeEmailIds(debarId);
                for (Object object : list) {
                    MailContentUtility mailContentUtility = new MailContentUtility();
                    MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                    sendMailMsgBox(object.toString(), "e-GP: Debarment Committee Member", mailContentUtility.hopeFormsDC(userName, initDebarmentService.getOfficeName(debarBy).toString(), cmpName, comName), msgBoxContentUtility.hopeFormsDC(userName, initDebarmentService.getOfficeName(debarBy).toString(), cmpName, comName), initDebarmentService.getHopeEmailIdForDebar(debarId).toString());
                    mailContentUtility = null;
                    msgBoxContentUtility = null;
                }
            }
        } catch (Exception e) {
            logger.error("publishDebarCommitte : " + logUserId + " : " + e);
        }
        logger.debug("publishDebarCommitte : " + logUserId + " Ends");
    }

    /**
     * Get List of details related to hope
     *
     * @param debarId of TblDebarmentReq
     * @return List of details related to hope
     */
    public List<Object[]> findHopeAns(String debarId) {
        logger.debug("findHopeAns : " + logUserId + " Starts ");
        List<Object[]> list = null;
        try {
            list = initDebarmentService.findHopeAns(debarId);
        } catch (Exception e) {
            logger.error("findHopeAns : " + logUserId + " : " + e);
        }
        logger.debug("findHopeAns : " + logUserId + " Ends");
        return list;
    }

    /**
     * Get List of details related to hope
     *
     * @param debarId of TblDebarmentReq
     * @return List of details related to hope
     */
    public List<Object[]> findSendHopeAns(String debarId) {
        logger.debug("findHopeAns : " + logUserId + " Starts ");
        List<Object[]> list = null;
        try {
            list = initDebarmentService.findSendHopeAns(debarId);
        } catch (Exception e) {
            logger.error("findHopeAns : " + logUserId + " : " + e);
        }
        logger.debug("findHopeAns : " + logUserId + " Ends");
        return list;
    }

    /**
     * /**
     * List of data related to committee member
     *
     * @param userId member id
     * @param debarId of TblDebarmentReq
     * @return List of data related to committee member
     */
    public List<Object[]> getCommitteDetails(String userId, String debarId) {
        logger.debug("getCommitteDetails : " + logUserId + " Starts ");
        List<Object[]> list = null;
        try {
            list = initDebarmentService.getCommitteDetails(userId, debarId);
        } catch (Exception e) {
            logger.error("getCommitteDetails : " + logUserId + " : " + e);
        }
        logger.debug("getCommitteDetails : " + logUserId + " Ends");
        return list;
    }

    /**
     * Inserting Committee Members comments
     *
     * @param debarId of TblDebarmentReq
     * @param comments of TblDebarmentComments
     * @param userId tenderer userId
     * @param commentsBy officer userId
     * @return flag true(success) or false(failure)
     */
    public boolean committeMemComments(String debarId, String comments, String userId, String commentsBy) {
        logger.debug("committeMemComments : " + logUserId + " Starts ");
        boolean flag = false;
        try {
            flag = initDebarmentService.committeMemComments(debarId, comments, userId, commentsBy);
        } catch (Exception e) {
            logger.error("committeMemComments : " + logUserId + " : " + e);
        }
        logger.debug("committeMemComments : " + logUserId + " Ends");
        return flag;
    }

    /**
     * Getting comments of committee members
     *
     * @param debarId of TblDebarmentReq
     * @param userId tenderer userId
     * @param commentsBy officer userId
     * @return List<TblDebarmentComments>
     * @throws Exception
     */
    public List<TblDebarmentComments> getCommitteeMemComments(String debarId, String userId, String commentsBy) {
        logger.debug("committeMemComments : " + logUserId + " Starts ");
        List<TblDebarmentComments> list = null;
        try {
            list = initDebarmentService.getCommitteeMemComments(debarId, userId, commentsBy);
        } catch (Exception e) {
            logger.error("committeMemComments : " + logUserId + " : " + e);
        }
        logger.debug("committeMemComments : " + logUserId + " Ends");
        return list;
    }

    /**
     *
     * @param debarId
     * @return
     */
    public List<Object[]> getWholeCommitteComments(String debarId) {
        logger.debug("getWholeCommitteComments : " + logUserId + " Starts ");
        List<Object[]> list = null;
        try {
            list = initDebarmentService.getWholeCommitteComments(debarId);
        } catch (Exception e) {
            logger.error("getWholeCommitteComments : " + logUserId + " : " + e);
        }
        logger.debug("getWholeCommitteComments : " + logUserId + " Ends");
        return list;
    }

    /**
     * If 1 than it is chairperson
     *
     * @param debarId of TblDebarmentReq
     * @param userId officer userId
     * @return
     */
    public long isChairperson(String debarId, String userId) {
        logger.debug("isChairperson : " + logUserId + " Starts ");
        long lng = 0;
        try {
            lng = initDebarmentService.isChairperson(debarId, userId);
        } catch (Exception e) {
            logger.error("isChairperson : " + logUserId + " : " + e);
        }
        logger.debug("isChairperson : " + logUserId + " Ends");
        return lng;
    }

    /**
     * Inserting chairperson review in TblDebarmentComments
     *
     * @param debarId of TblDebarmentReq
     * @param userId tenderer userId
     * @param comments chairperson comments
     * @param debarStatus status related to debarment
     * @param commentsBy chairperson userId
     * @return flag true(success) or false(failure)
     */
    public boolean chaipersonReview(String debarId, String userId, String comments, String debarStatus, String commentsBy) {
        logger.debug("chaipersonReview : " + logUserId + " Starts ");
        boolean flag = false;
        try {
            if (initDebarmentService.chaipersonReview(debarId, userId, comments, debarStatus, commentsBy)) {
                if ("egpadmin".equals(commentsBy) && "appdebaregp".equals(debarStatus)) {
                    MailContentUtility mailContentUtility = new MailContentUtility();
                    MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                    TblDebarmentDetails tdds = initDebarmentService.getDebarDetails("tblDebarmentReq", Operation_enum.EQ, new TblDebarmentReq(Integer.parseInt(debarId)), "debarStatus", Operation_enum.EQ, "byhope").get(0);
                    String pageHead1 = null;
                    String pageHead2 = null;
                    if (tdds.getDebarTypeId() == 1) {
                        pageHead1 = "Ref No.";
                        pageHead2 = "Tender Brief";
                    } else if (tdds.getDebarTypeId() == 2) {
                        pageHead1 = "Letter Ref. No.";
                        pageHead2 = "Package No.";
                    } else if (tdds.getDebarTypeId() == 3) {
                        pageHead1 = "Project Name";
                        pageHead2 = "Project Code";
                    } else if (tdds.getDebarTypeId() == 4) {
                        pageHead1 = "PE Office Name";
                        pageHead2 = "PE Code";
                    } else if (tdds.getDebarTypeId() == 5) {
                        pageHead1 = "Department Name";
                        pageHead2 = "Department Type";
                    }

                    StringBuilder data = new StringBuilder();
                    String[] debarIds = tdds.getDebarIds().split(",");
                    data.append("<table width='100%' cellspacing='0' class='tableList_1 t_space' border='1'>");
                    data.append("<tr><th class='t-align-center' width='30%'><label id='dlbHead1'>" + pageHead1 + "</label></th><th class='t-align-center' width='50%'><label id='dlbHead2'>" + pageHead2 + "</label></th></tr>");
                    data.append("<tbody>");
                    List<SPTenderCommonData> list = searchDataForDebarType(String.valueOf(tdds.getDebarTypeId()), String.valueOf(tdds.getDebarmentBy()));
                    for (int j = 0; j < debarIds.length; j++) {
                        for (SPTenderCommonData sptcd : list) {
                            if (sptcd.getFieldName1().equals(debarIds[j])) {
                                data.append("<tr><td><input type='hidden' value='" + sptcd.getFieldName1() + "' id='debId" + j + "'/>" + sptcd.getFieldName2() + "</td><td>" + sptcd.getFieldName3() + "</td></tr>");
                            }
                        }
                    }
                    data.append("</tbody></table>");
                    String data2 = null;
                    List<TblDebarmentTypes> debarTypes = getAllDebars();
                    for (TblDebarmentTypes types : debarTypes) {
                        if (tdds.getDebarTypeId() == types.getDebarTypeId()) {
                            data2 = types.getDebarType();
                        }
                    }
                    sendMailMsgBox(initDebarmentService.getEgpAdmin().toString(), "e-GP: Debarment request", mailContentUtility.debaresTCJ(data2, data.toString()), msgBoxContentUtility.debaresTCJ(data2, data.toString()), initDebarmentService.getTendererEmailId(debarId).toString());
                    mailContentUtility = null;
                    msgBoxContentUtility = null;

                }
                flag = true;

            } else {
                flag = false;
            }
        } catch (Exception e) {
            logger.error("chaipersonReview : " + logUserId + " : " + e);
        }
        logger.debug("chaipersonReview : " + logUserId + " Ends");
        return flag;
    }

    /**
     * Hope Id from TblDebarmentReq
     *
     * @param debarId from TblDebarmentReq
     * @return HopeId
     */
    public String getHopeId(String debarId) {
        logger.debug("getHopeId : " + logUserId + " Starts");
        String str = "";
        try {
            str = initDebarmentService.getHopeIdForDebar(debarId);
        } catch (Exception e) {
            logger.error("getHopeId : " + logUserId + " : " + e);
        }
        logger.debug("getHopeId : " + logUserId + " Ends");
        return str;
    }

    /**
     * Get Tenderer Response for Debarment
     *
     * @param values variable arguments
     * @return List of TblDebarmentResp
     * @throws Exception
     */
    public List<TblDebarmentResp> findDebarResponse(Object... values) {
        logger.debug("findDebarResponse : " + logUserId + " Starts");
        List<TblDebarmentResp> list = null;
        try {
            list = initDebarmentService.findDebarResponse(values);
        } catch (Exception e) {
            logger.error("findDebarResponse : " + logUserId + " : " + e);
        }
        logger.debug("findDebarResponse : " + logUserId + " Ends");
        return list;
    }

    /**
     * Returns number of committee members
     *
     * @param debarId of TblDebarmentReq
     * @return number of committee members
     * @throws Exception
     */
    public long countCommitteeMember(String debarId) {
        logger.debug("countCommitteeMember : " + logUserId + " Starts");
        long lng = 0;
        try {
            lng = initDebarmentService.countCommitteeMember(debarId);
        } catch (Exception e) {
            logger.error("countCommitteeMember : " + logUserId + " : " + e);
        }
        logger.debug("countCommitteeMember : " + logUserId + " Ends");
        return lng;
    }

    /**
     * Get Documents from TblDebarmentDocs
     *
     * @param values variable arguments
     * @return List of TblDebarmentDocs
     */
    public List<TblDebarmentDocs> getAllDocs(Object... values) {
        logger.debug("getAllDocs : " + logUserId + " Starts");
        List<TblDebarmentDocs> list = null;
        try {
            list = initDebarmentService.getDebarDocs(values);
        } catch (Exception e) {
            logger.error("getAllDocs : " + logUserId + " : " + e);
        }
        logger.debug("getAllDocs : " + logUserId + " Ends");
        return list;
    }

    /**
     * Updating TblDebarmentReq debarmentStatus while passing debarment from one
     * officer to another
     *
     * @param debarId of TblDebarmentReq
     * @param status to be saved in debarmentStatus
     * @return count of rows updated return
     */
    public int updateDebarStatus(String debarId, String status, String lastRespDate, String userId) {
        logger.debug("updateDebarStatus : " + logUserId + " Starts");
        int i = 0;
        try {
            i = initDebarmentService.updateDebarStatus(debarId, status);
            if (i > 0) {
                //List<Object> list = initDebarmentService.getDebarEmailIds(debarId);
                MailContentUtility mailContentUtility = new MailContentUtility();
                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                sendMailMsgBox(initDebarmentService.getTendererEmailId(debarId).toString(), "e-GP: Debarment process related clarification", mailContentUtility.peClariToTenderer(initDebarmentService.getOfficeName(userId).toString(), lastRespDate), msgBoxContentUtility.peClariToTenderer(initDebarmentService.getOfficeName(userId).toString(), lastRespDate), initDebarmentService.getPEEmailId(debarId).toString());
                mailContentUtility = null;
                msgBoxContentUtility = null;
            }
        } catch (Exception e) {
            logger.error("updateDebarStatus : " + logUserId + " : " + e);
        }
        logger.debug("updateDebarStatus : " + logUserId + " Ends");
        return i;
    }

    /**
     * Object Array of [0 userId,1 debarmentBy,2 hopeId,3
     * tblDebarmentTypes.debarTypeId]
     *
     * @param debarId of TblDebarmentReq
     * @return Object Array of [0 userId,1 debarmentBy,2 hopeId,3
     * tblDebarmentTypes.debarTypeId]
     */
    public Object[] findPETenderHopeId(String debarId) {
        logger.debug("findPETenderHopeId : " + logUserId + " Starts");
        Object[] obj = null;
        try {
            obj = initDebarmentService.findPETenderHopeId(debarId);
        } catch (Exception e) {
            logger.error("findPETenderHopeId : " + logUserId + " : " + e);
        }
        logger.debug("findPETenderHopeId : " + logUserId + " Ends");
        return obj;
    }

    /**
     * Send e-Mail and MsgBox
     *
     * @param emailId to whom mail is to be sent
     * @param sub subject of email
     * @param mailsText MailText message body
     * @param msgBox MsgBox containing text
     * @param msgBoxFrom from emailId
     * @return flag whether mail and message box is sent
     */
    public boolean sendMailMsgBox(String emailId, String sub, String mailsText, String msgBox, String msgBoxFrom) {
        logger.debug("sendMailMsgBox : " + logUserId + " Starts");
        boolean flag = false;
        try {
            UserRegisterService service = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
            String mails[] = {emailId};
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            service.contentAdmMsgBox(mails[0], msgBoxFrom, sub, msgBox);
            String mailText = mailsText;
            sendMessageUtil.setEmailTo(mails);
            sendMessageUtil.setEmailSub(sub);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            mails = null;
            sendMessageUtil = null;
            flag = true;
        } catch (Exception e) {
            logger.error("sendMailMsgBox : " + logUserId + " : " + e);
            flag = false;
        }
        logger.debug("sendMailMsgBox : " + logUserId + " Ends");
        return flag;
    }

    /**
     * It gives PE emailId based on Debarment Id
     *
     * @param debarId from TblDebarmentReq
     * @return emailId from TblLoginMaster
     */
    public List<Object[]> findPeSatisfy(String debarId) {
        logger.debug("findPeSatisfy : " + logUserId + " Starts ");
        List<Object[]> list = null;
        try {
            list = initDebarmentService.findPeSatisfy(debarId);
        } catch (Exception e) {
            logger.error("findPeSatisfy : " + logUserId + " : " + e);
        }
        logger.debug("findPeSatisfy : " + logUserId + " Ends");
        return list;
    }

    public List<SPCommonSearchData> getWorkCategory(String debarUserId) {
        logger.debug("getAllDebars : " + logUserId + " Starts ");
        List<SPCommonSearchData> list = null;
        try {
            list = commonService.searchData("GetWorkCategory", debarUserId, null, null, null, null, null, null, null, null);
        } catch (Exception e) {
            logger.error("getAllDebars : " + logUserId + " : " + e);
        }
        logger.debug("getAllDebars : " + logUserId + " Ends");
        return list;
    }
    
    public List<SPCommonSearchData> getUserCompanyInfo(String debarUserId){
        logger.debug("getAllDebars : " + logUserId + " Starts ");
        List<SPCommonSearchData> list = null;
        try {
            list = commonService.searchData("GetUserCompanyInfo", debarUserId, null, null, null, null, null, null, null, null);
        } catch (Exception e) {
            logger.error("getAllDebars : " + logUserId + " : " + e);
        }
        logger.debug("getAllDebars : " + logUserId + " Ends");
        return list;
    }

    public List<SPCommonSearchData> getDebarmentInfoByUser(String debarUserId) {
                logger.debug("getAllDebars : " + logUserId + " Starts ");
        List<SPCommonSearchData> list = null;
        try {
            list = commonService.searchData("GetDebarmentInfoByUser", debarUserId, null, null, null, null, null, null, null, null);
        } catch (Exception e) {
            logger.error("getAllDebars : " + logUserId + " : " + e);
        }
        logger.debug("getAllDebars : " + logUserId + " Ends");
        return list;
    }
    public List<SPCommonSearchDataMore> getDebaredBiddingPermissionInfo(String debarUserId, String category) {
                logger.debug("getDebaredBiddingPermissionInfo : " + logUserId + " Starts ");
        List<SPCommonSearchDataMore> list = null;
        try {
            list = commonServiceDataMore.getCommonSearchData("GetDebaredBiddingPermissionInfo", debarUserId, category);
        } catch (Exception e) {
            logger.error("getDebaredBiddingPermissionInfo : " + logUserId + " : " + e);
        }
        logger.debug("getDebaredBiddingPermissionInfo : " + logUserId + " Ends");
        return list;
    }
}
