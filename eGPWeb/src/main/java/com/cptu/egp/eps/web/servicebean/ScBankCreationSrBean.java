/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblCountryMaster;
import com.cptu.egp.eps.model.table.TblStateMaster;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.SelectItem;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class ScBankCreationSrBean {

    static final Logger LOGGER = Logger.getLogger(ScBankCreationSrBean.class);
    private String logUserId = "0";
    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
        commonService.setUserId(logUserId);
    }
    private List<SelectItem> countryList = new ArrayList<SelectItem>();

    public List<SelectItem> getCountryList() throws Exception {
        LOGGER.debug("getCountryList : "+logUserId+" Starts");
        if (countryList.isEmpty()) {
            List<TblCountryMaster> countryMasters = commonService.countryMasterList();
            for (TblCountryMaster countryMaster : countryMasters) {
                countryList.add(new SelectItem(countryMaster.getCountryId(), countryMaster.getCountryName()));
            }
        }
        LOGGER.debug("getCountryList : "+logUserId+" Starts");
        return countryList;
    }

    public void setCountryList(List<SelectItem> countryList) {
        this.countryList = countryList;
    }
    private List<SelectItem> stateList = new ArrayList<SelectItem>();

    public List<SelectItem> getStateList() {
        LOGGER.debug("getStateList : "+logUserId+" Starts");
        if (stateList.isEmpty()) {
            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
            //Change 150 from 136 by Proshanto
            List<TblStateMaster> stateMasters = commonService.getState((short) 150);
            for (TblStateMaster stateMaster : stateMasters) {
                stateList.add(new SelectItem(stateMaster.getStateId(), stateMaster.getStateName()));
            }
        }
        LOGGER.debug("getStateList : "+logUserId+" Ends");
        return stateList;
    }

    public void setStateList(List<SelectItem> stateList) {
        this.stateList = stateList;
    }
}
