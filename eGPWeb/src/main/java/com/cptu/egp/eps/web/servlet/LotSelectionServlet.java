/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblBidderLots;
import com.cptu.egp.eps.model.table.TblTenderLotSecurity;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.TenderService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class LotSelectionServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * This class is use for handling the request when clicking on tender prep tab and
     * for update and insert in to TblBidderLots
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
             
             List<TblBidderLots> listForInsert = new ArrayList<TblBidderLots>();/*If only one lot is there there for this listis used*/
             TenderService tenderService = (TenderService)AppContext.getSpringBean("TenderService");
             
             
             
              int tenderId= Integer.parseInt(request.getParameter("tenderId"));
              String action = "";
              boolean flag = false;
              int finalsubflag = 0;
              HttpSession session = request.getSession();
              int userId = Integer.parseInt(session.getAttribute("userId").toString());
              List<TblBidderLots> listForChk = tenderService.viewBidderLots(tenderId, userId);
            if(request.getParameter("action")!=null && "linkSet".equalsIgnoreCase(request.getParameter("action"))){
                List<TblTenderLotSecurity> list = null;
                list = isMultilpleLot(tenderId);
                List<SPTenderCommonData> listView = new ArrayList<SPTenderCommonData>();
                TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                List<SPTenderCommonData> finalSubCond = tenderCS.returndata("getBidderLotSelectionLinkStatus", tenderId+"", session.getAttribute("userId").toString());
                
                
                if(!finalSubCond.isEmpty()){
                    finalsubflag =  Integer.parseInt(finalSubCond.get(0).getFieldName1());
                }
                if(listForChk.isEmpty()){
                if(finalSubCond.isEmpty() || finalsubflag==1){
                listView = tenderCS.returndata("getLotDescriptonForDocViewMore", tenderId+"", userId+"");
                        if(list!=null && !listView.isEmpty()){
                            List<SPTenderCommonData> listavail=new  ArrayList<SPTenderCommonData>(); 
                                for(int i=0;i<listView.size();i++){
                                    if(!"0".equalsIgnoreCase(listView.get(i).getFieldName4())){/*If it is free or doc fees are paid*/
                                        listavail.add(listView.get(i));
                                    }
                                }
                                    if(listavail.size()==1){
                                        if(listForChk.isEmpty()){
                                /*When tender has a single lot*/
                                    listForInsert = addSingleData(listavail, userId,tenderId);
                                    flag = tenderService.insertAll(listForInsert);                                  
                                    }else{
                                            flag = true;
                                        }
                                    }else{
                                        flag = false;
                                    }                            
                        }/*If no record found*/
                    }/*If finalSubmisstion not Done*/else{
                       flag = true;
                   }
                if(listView!=null){
                    listView.clear();
                    listView = null;
                }
            }
            else {  flag = true; }
            }/*For redirecting Handle request of tender Prerp tab*/else{
                action = request.getParameter("action");
               
                if(action.equalsIgnoreCase("add")){
                    listForInsert = addData(request);
                    if(listForInsert!=null && !listForInsert.isEmpty()){
                        
                        flag = tenderService.insertAll(listForInsert);
                    }
                }/*For Editing purpose*/else{
                      listForInsert = addData(request);
                      if(listForInsert!=null && !listForInsert.isEmpty()){
                          flag = tenderService.forUpdateTblBidderLot(listForInsert, tenderId, userId);
                      }
                    }
            }
             if(flag){
                 response.sendRedirect("tenderer/LotTendPrep.jsp?tab=4&tenderId="+tenderId+"");
                 //response.sendRedirectFilter("LotTendPrep.jsp?tab=4&tenderId="+tenderId);
             }else{
                 response.sendRedirect("tenderer/LotSelection.jsp?tab=4&tenderId="+tenderId);
                 // response.sendRedirectFilter("LotSelection.jsp?tab=4&tenderId="+tenderId);
             }
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LotSelectionServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LotSelectionServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
              if(tenderService!=null){
                  tenderService = null;
              }
              if(listForInsert!=null){
                  listForInsert.clear();
                  listForInsert = null;
              }
              
        } finally { 
            out.close();
        }
       
    }
     /*
     *This method is used for redirect the reqest to page lotsection or tenderDocPrep
     *Parameter int tenderId
     *Return List<TblBidderLots>
     */
     private List<TblTenderLotSecurity> isMultilpleLot(int tenderId){
         TenderService tenderService = (TenderService)AppContext.getSpringBean("TenderService");
         return tenderService.isMultipleLot(tenderId);
    }
      /*
     *This method is used for insert data in to TblBidderLots
     *Parameter request
     *Return List<TblBidderLots>
     */
     private List<TblBidderLots> addData(HttpServletRequest request){
         
         List<TblBidderLots> addList = new ArrayList<TblBidderLots>();
         int counter = Integer.parseInt(request.getParameter("counter"));
         int tenderId = Integer.parseInt(request.getParameter("tenderId"));
         HttpSession session = request.getSession();
         int userId = Integer.parseInt(session.getAttribute("userId").toString());
         for(int i =1;i<=counter;i++){
             if(request.getParameter("lotchkBox_"+i)!=null){
                 TblBidderLots tblBidderLots = new TblBidderLots();
                tblBidderLots.setPkgLotId(Integer.parseInt(request.getParameter("lotchkBox_"+i)));
                tblBidderLots.setInsdate(new Date());
                tblBidderLots.setTenderId(tenderId);
                tblBidderLots.setUserId(userId);
             addList.add(tblBidderLots);
             }
             
         }
         return addList;
     }
     /*This method is called when Tender has a single lot...*/
     private List<TblBidderLots> addSingleData(List<SPTenderCommonData> list,int userId,int tenderId){
         List<TblBidderLots> list_add = new ArrayList<TblBidderLots>();
         TblBidderLots tblBidderLots = new TblBidderLots();
         tblBidderLots.setPkgLotId(Integer.parseInt(list.get(0).getFieldName3()));
         tblBidderLots.setTenderId(tenderId);
         tblBidderLots.setInsdate(new Date());
         tblBidderLots.setUserId(userId);
         list_add.add(tblBidderLots);
         return list_add;
     }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
