/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblEvalTscnotification;
import com.cptu.egp.eps.service.serviceimpl.EvalTSCNotificationService;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author shreyansh
 */
public class EvalTscNotifyServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @SuppressWarnings("empty-statement")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            EvalTSCNotificationService etscns = (EvalTSCNotificationService) AppContext.getSpringBean("EvalTSCNotificationService");
            String tenderId = request.getParameter("tenderId");
            TblEvalTscnotification tet = new TblEvalTscnotification();
            HttpSession session = request.getSession();
            etscns.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
            String userId = "";
            userId = session.getAttribute("userId").toString();
            tet.setTenderId(Integer.parseInt(tenderId));
            tet.setSentBy(Integer.parseInt(userId));
            tet.setSentDt(new Date());
            boolean flag = etscns.addEvalNotification(tet);
            if (flag) {
                CommonSearchDataMoreService csdms = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                List<SPCommonSearchDataMore> dataMore = csdms.geteGPData("GetTECMemberEmail", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                List<SPTenderCommonData> datas = tenderCommonService1.returndata("tenderinfobar", request.getParameter("tenderId"), null);
                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                MailContentUtility mailContentUtility = new MailContentUtility();

                for (int k = 0; k < dataMore.size(); k++) {

                    if (!userId.equalsIgnoreCase(dataMore.get(k).getFieldName2().toString())) {
                        String frommailid = XMLReader.getMessage("emailIdNoReply");
                        String tomailId[] = {dataMore.get(k).getFieldName1()};
                        String peoffice = tenderCommonService1.getPEOfficeName(request.getParameter("tenderId"));
                        String mailText = mailContentUtility.MessageToEvalTSCNotification(Integer.parseInt(tenderId), datas.get(0).getFieldName2().toString(), peoffice);
                        sendMessageUtil.setEmailTo(tomailId);
                        sendMessageUtil.setSendBy(frommailid);
                        sendMessageUtil.setEmailSub("e-GP:  Recommendation from TSC");
                        sendMessageUtil.setEmailMessage(mailContentUtility.EmailToEvalTSCNotification(Integer.parseInt(tenderId), datas.get(0).getFieldName2().toString(), peoffice));
                        userRegisterService.contentAdmMsgBox(tomailId[0], frommailid, HandleSpecialChar.handleSpecialChar("e-GP:  Recommendation from TSC"), mailText);
                        sendMessageUtil.sendEmail();
                    }

                }
                response.sendRedirect("officer/Evalclarify.jsp?tenderId=" + tenderId + "&st=cl&comType=TEC&msg=not");
            } else {
            }



            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EvalTscNotifyServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EvalTscNotifyServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
             */
        } catch (Exception ex) {
            Logger.getLogger(EvalTscNotifyServlet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
