/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblMultiLangContent;
import com.cptu.egp.eps.service.serviceimpl.MultiLingualService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class LanguageContentSrBean {

    final Logger logger = Logger.getLogger(LanguageContentSrBean.class);
    private MultiLingualService multilingualservice = (MultiLingualService) AppContext.getSpringBean("MultiLingualService");
    //private TblMultiLangContent tblMultiLangContent = new TblMultiLangContent();
    private String logUserId = "0";
    private int offset = 0;
    private int limit = 0;
    private String colName = "";
    private String op_ENUM = "";
    private String condition = "";
    private boolean _search = false;
    private String getSortOrder = "";
    private String sortCol = "";
    private AuditTrail auditTrail;

    public void setLogUserId(String logUserId) {
        multilingualservice.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    public boolean isSearch() {
        return _search;
    }

    public void setSearch(boolean _search) {
        this._search = _search;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getGetSortOrder() {
        return getSortOrder;
    }

    public void setGetSortOrder(String getSortOrder) {
        this.getSortOrder = getSortOrder;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getOp_ENUM() {
        return op_ENUM;
    }

    public void setOp_ENUM(String op_ENUM) {
        this.op_ENUM = op_ENUM;
    }

    public String getSortCol() {
        return sortCol;
    }

    public void setSortCol(String sortCol) {
        this.sortCol = sortCol;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        multilingualservice.setAuditTrail(auditTrail);
    }
    
    

    /**
     * Update Language Content
     * @param title
     * @param subtitle
     * @param language
     * @param displaytitle
     * @param value
     * @param id
     * @return
     */
    public String updatelangContent(String title, String subtitle, String language, String displaytitle, byte[] value, int id) {
        logger.debug("updatelangContent : " + logUserId + " Starts ");
        String msg = null;
        try {
            msg = multilingualservice.updateLangContent(title, subtitle, language, displaytitle, value, id);
        } catch (Exception e) {
            logger.error("updatelangContent : " + logUserId + " : " + e);
        }
        logger.debug("updatelangContent : " + logUserId + " Ends");
        return msg;
    }

    /**
     * Get Language Data by id.
     * @param id
     * @return Multilingual Content.
     */
    public List<TblMultiLangContent> getData(int id) {
        logger.debug("getData : " + logUserId + " Starts ");
        List<TblMultiLangContent> listlangcontent = null;
        try {
            listlangcontent = multilingualservice.getData(id);

        } catch (Exception e) {
            logger.error("getData : " + logUserId + " : " + e);
        }
        logger.debug("getData : " + logUserId + " Ends");
        return listlangcontent;
    }

    /**
     * Fetch Search Data by \searc
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return
     */
    public List<Object[]> getSearchData(String searchField, String searchString, String searchOper) {
        logger.debug("getSearchData : " + logUserId + " Starts");
        List<Object[]> listSearchData = null;
        try {
            String orderClause = null;
            Object e = null;
            if (getSortCol().equals("")) {
                orderClause = "LM.contentId";
                e = Operation_enum.DESC;
            } else if (getSortCol().equalsIgnoreCase("displayTitle")) {
                if (getSortOrder.equalsIgnoreCase("asc")) {
                    orderClause = "LM.displayTitle";
                    e = Operation_enum.ASC;
                } else if (getSortOrder.equalsIgnoreCase("desc")) {
                    orderClause = "LM.displayTitle";
                    e = Operation_enum.DESC;
                }
            } else if (getSortCol().equalsIgnoreCase("language")) {
                if (getSortOrder.equalsIgnoreCase("asc")) {
                    orderClause = "LM.language";
                    e = Operation_enum.ASC;
                } else if (getSortOrder.equalsIgnoreCase("desc")) {
                    orderClause = "LM.language";
                    e = Operation_enum.DESC;
                }
            } else if (getSortCol().equalsIgnoreCase("title")) {
                if (getSortOrder.equalsIgnoreCase("asc")) {
                    orderClause = "LM.title";
                    e = Operation_enum.ASC;
                } else if (getSortOrder.equalsIgnoreCase("desc")) {
                    orderClause = "LM.title";
                    e = Operation_enum.DESC;
                }
            }
            if (searchField.equalsIgnoreCase("displayTitle")) {
                searchField = "LM.displayTitle ";
            } else if (searchField.equalsIgnoreCase("language")) {
                searchField = "LM.language ";
            }
            if (searchOper.equalsIgnoreCase("CN")) {
                searchString = "Like '%" + searchString + "%'";
            } else if (searchOper.equalsIgnoreCase("EQ")) {
                String languageField = "LM.language";
                if(HandleSpecialChar.trim(searchField,"").equalsIgnoreCase(languageField)){
                    if("English".equalsIgnoreCase(searchString))
                    {
                        searchString = "en_US";
                    }
                    else if("Bangla".equalsIgnoreCase(searchString))
                    {
                        searchString = "bn_IN";
                    }
                }
                searchString = "= '" + searchString + "'";
            }
            listSearchData = new ArrayList<Object[]>();
            if (listSearchData.isEmpty()) {
                listSearchData = multilingualservice.getSeachLangGrid(getOffset(), getLimit(),orderClause, e.toString(), searchField, searchString);
            }//if userlist is empty
            logger.debug("User List: " + listSearchData.size());
//        if(listSearchData.size()>0){
//            return listSearchData;
//        }
        } catch (Exception e) {
            logger.error("getSearchData : " + logUserId + " : " + e);
        }
        logger.debug("getSearchData : " + logUserId + " Ends");
        return listSearchData;
    }

    /**
     * Get Search Count of Language Content
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return
     */
    public long getSearchCount(String searchField,String searchString,String searchOper) {
        logger.debug("getSearchCount : "+logUserId+" Starts ");
        long lng = 0;
        try {
            if("language".equalsIgnoreCase(searchField)){
                if("English".equalsIgnoreCase(searchString)){
                    searchString = "en_US";
                }
                else if("Bangla".equalsIgnoreCase(searchString))
                {
                    searchString = "bn_IN";
                }
            }
        if(searchOper.equalsIgnoreCase("CN")){
            searchString = "%" + searchString + "%";
        }
                lng = multilingualservice.getSearchCount("TblMultiLangContent LM", searchField + " LIKE '" + searchString + "'");
        } catch (Exception e) {
            logger.error("getSearchCount : "+logUserId+" : "+e);
    }
        logger.debug("getSearchCount : "+logUserId+" Ends ");
        return lng;
    }
}
