/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.web.databean.T1L1DtBean;
import java.math.BigDecimal;
import java.util.Comparator;

/**
 *
 * @author nishit
 */
public class CompareReportT1L1 implements Comparator<T1L1DtBean> {

    @Override
    public int compare(T1L1DtBean o1, T1L1DtBean o2) {
        int i = 0;
        i = new BigDecimal(o2.getAmount()).compareTo(new BigDecimal(o1.getAmount()));
        return i;
    }
}
