/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.xmlpojos;

import java.util.Date;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Chalapathi.Bavisetti
 */

public class AllTendersXmlElements {

    private Integer tenderId;
    private String refNo;
    private String procurementNature;
    private String tenderDescription;
    private String ministry;
    private String division;
    private String agency;
    private String peOfficeName;
    private String procurementType;
    private String procurementMethod;
    private Date tenderPubDt;
    private Date submissionDt;
    private Integer officeId;

    /**
     * @return the tenderId
     */
    @XmlElement(nillable = true)
    public Integer getTenderId() {
        return tenderId;
    }

    /**
     * @param tenderId the tenderId to set
     */
    public void setTenderId(Integer tenderId) {
        this.tenderId = tenderId;
    }

    /**
     * @return the refNo
     */
    @XmlElement(nillable = true)
    public String getRefNo() {
        return refNo;
    }

    /**
     * @param refNo the refNo to set
     */
    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    /**
     * @return the procurementNature
     */
    @XmlElement(nillable = true)
    public String getProcurementNature() {
        return procurementNature;
    }

    /**
     * @param procurementNature the procurementNature to set
     */
    public void setProcurementNature(String procurementNature) {
        this.procurementNature = procurementNature;
    }

    /**
     * @return the ministry
     */
    @XmlElement(nillable = true)
    public String getMinistry() {
        return ministry;
    }

    /**
     * @param ministry the ministry to set
     */
    public void setMinistry(String ministry) {
        this.ministry = ministry;
    }

    /**
     * @return the division
     */
    @XmlElement(nillable = true)
    public String getDivision() {
        return division;
    }

    /**
     * @param division the division to set
     */
    public void setDivision(String division) {
        this.division = division;
    }

    /**
     * @return the agency
     */
    @XmlElement(nillable = true)
    public String getAgency() {
        return agency;
    }

    /**
     * @param agency the agency to set
     */
    public void setAgency(String agency) {
        this.agency = agency;
    }

    /**
     * @return the peOfficeName
     */
    @XmlElement(nillable = true)
    public String getPeOfficeName() {
        return peOfficeName;
    }

    /**
     * @param peOfficeName the peOfficeName to set
     */
    public void setPeOfficeName(String peOfficeName) {
        this.peOfficeName = peOfficeName;
    }

    /**
     * @return the procurementType
     */
    @XmlElement(nillable = true)
    public String getProcurementType() {
        return procurementType;
    }

    /**
     * @param procurementType the procurementType to set
     */
    public void setProcurementType(String procurementType) {
        this.procurementType = procurementType;
    }


    /**
     * @return the tenderPubDt
     */
    @XmlElement(nillable = true)
    public Date getTenderPubDt() {
        return tenderPubDt;
    }

    /**
     * @param tenderPubDt the tenderPubDt to set
     */
    public void setTenderPubDt(Date tenderPubDt) {
        this.tenderPubDt = tenderPubDt;
    }

    /**
     * @return the submissionDt
     */
    @XmlElement(nillable = true)
    public Date getSubmissionDt() {
        return submissionDt;
    }

    /**
     * @param submissionDt the submissionDt to set
     */
    public void setSubmissionDt(Date submissionDt) {
        this.submissionDt = submissionDt;
    }

    /**
     * @return the procurementMethod
     */
     @XmlElement(nillable = true)
    public String getProcurementMethod() {
        return procurementMethod;
    }

    /**
     * @param procurementMethod the procurementMethod to set
     */
    public void setProcurementMethod(String procurementMethod) {
        this.procurementMethod = procurementMethod;
    }

     /**
     * @return the tender description
     */
     @XmlElement(nillable = true)
    public String getTenderDescription() {
        return tenderDescription;
    }

    /**
     * @param tender description to set
     */
    public void setTenderDescription(String tenderDescription) {
        this.tenderDescription = tenderDescription;
    }

    public Integer getOfficeId() {
        return officeId;
    }

    public void setOfficeId(Integer officeId) {
        this.officeId = officeId;
    }

    

    
}
