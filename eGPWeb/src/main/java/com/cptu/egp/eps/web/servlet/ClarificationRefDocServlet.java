/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

/**
 *
 * @author Administrator
 */
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CheckExtension;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class ClarificationRefDocServlet extends HttpServlet {

    private static final String TMP_DIR_PATH = "c:\\tmp";
    private File tmpDir;
    private static final String DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("ClarificationRefDocServlet");
    private File destinationDir;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        tmpDir = new File(TMP_DIR_PATH);
        if (!tmpDir.isDirectory()) {
            tmpDir.mkdir();
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();

        int postQualId = 0;
        int tenderId = 0;
        File file = null;
        String docSizeMsg = "";
        boolean checkret = false;
        boolean flag = false;
        String documentBrief = "";
        String fileName = "";
        long fileSize = 0;
        String queryString = "";
        String pageName = "officer/EvalRptApp.jsp";
        response.setContentType("text/html");
        int evalRptId = 0;
        boolean dis = false;
        if (session.getAttribute("userId") == null) {
            response.sendRedirect("SessionTimedOut.jsp");
        } else {
            HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
            try {
                try {
                    if (request.getParameter("funName") != null && request.getParameter("funName").equals("approval")) {

                        if (request.getParameter("evalRptId") != null || request.getParameter("evalRptId").equals("")) {
                            evalRptId = Integer.parseInt(request.getParameter("evalRptId"));
                            if (request.getParameter("tenderId") != null || request.getParameter("tenderId").equals("")) {
                                tenderId = Integer.parseInt(request.getParameter("tenderId"));
                            }
                            if (request.getParameter("txtcomments") != null || request.getParameter("txtcomments").equals("")) {
                                documentBrief = request.getParameter("txtcomments");
                            }
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            documentBrief = handleSpecialChar.handleSpecialChar(documentBrief);
                            fileName = "signed";
                            String xmldata = "<tbl_EvalReportSign evalRptId=\"" + evalRptId + "\" userId=\"" + session.getAttribute("userId") + "\" signDt=\"" + format.format(new Date()) + "\" comments=\"" + documentBrief + "\" signStatus=\"" + fileName + "\"/>";
                            xmldata = "<root>" + xmldata + "</root>";
                            System.out.println(xmldata);
                            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                            CommonMsgChk commonMsgChk;
                            commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_EvalReportSign", xmldata, "").get(0);
                            System.out.println(commonMsgChk.getMsg());
                            
                            if (commonMsgChk.getId() == null) {
                                docSizeMsg = "Problem whie Processing Your Request";
                                queryString = "?tenderId=" + tenderId + "&evalRptId=" + evalRptId + "fs=" + docSizeMsg;
                                response.sendRedirect(pageName + queryString);
                            } else {

                                pageName = "officer/ClarificationRefDoc.jsp";
                                queryString = "?tenderId=" + tenderId + "&evalRptId=" + evalRptId;
                                response.sendRedirect(pageName + queryString);
                            }
                        } else {

                            queryString = "?tenderId=" + tenderId + "&evalRptId=" + evalRptId;
                            response.sendRedirect(pageName + queryString);
                        }

                    } else {
                        String action = "";
                        pageName = "officer/ClarificationRefReprot.jsp";
                        if (request.getParameter("funName") != null && request.getParameter("funName").equals("report")) {
                            documentBrief = request.getParameter("reportName");
                            if (request.getParameter("tenderId") == null || request.getParameter("tenderId").equals("")) {
                                response.sendRedirect(pageName + queryString);
                            }
                            if (documentBrief == null || documentBrief.trim().length() == 0) {
                                    docSizeMsg = "Please Enter Report Name";
                                    queryString = "?evalRptId=" + evalRptId + "&fq=" + docSizeMsg + "&tenderId=" + tenderId;
                                    response.sendRedirect(pageName + queryString);
                                } else {
                            if(request.getParameter("action")!=null){
                                    action  = request.getParameter("action");
                            }
                            if(action.equalsIgnoreCase("create")){
                                tenderId = Integer.parseInt(request.getParameter("tenderId"));
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    documentBrief = handleSpecialChar.handleSpecialChar(documentBrief);
                                    String evalRptStatus = "pending";
                                    String xmldata = "<tbl_EvalReportMaster evalRptName=\"" + documentBrief + "\" createdBy=\"" + session.getAttribute("userId") + "\" createdDate=\"" + format.format(new Date()) + "\" evalRptStatus=\"" + evalRptStatus + "\" tenderid=\"" + tenderId + "\"/>";
                                    xmldata = "<root>" + xmldata + "</root>";
                                    System.out.println(xmldata);
                                    CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                    CommonMsgChk commonMsgChk;

                                    commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_EvalReportMaster", xmldata, "").get(0);
                                    System.out.println(commonMsgChk.getMsg());
                                    if (commonMsgChk.getId() != null) {
                                        evalRptId = commonMsgChk.getId();
                                        System.out.println(commonMsgChk.getId());
                                        pageName = "officer/ClarificationRefDoc.jsp";
                                        queryString = "?evalRptId=" + evalRptId+ "&tenderId=" + tenderId;
                                        response.sendRedirect(pageName + queryString);
                                    } else {
                                        System.out.println(commonMsgChk.getId());
                                        queryString = "?evalRptId=" + evalRptId + "&tenderId=" + tenderId;
                                        response.sendRedirect(pageName + queryString);
                                    }
                                
                            }else{
                                String rptId = request.getParameter("rptId");
                                tenderId = Integer.parseInt(request.getParameter("tenderId"));
                                String updateData = "evalRptName='"+documentBrief+"'";
                                String whereCon = "evalRptId="+rptId;
                                CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                    CommonMsgChk commonMsgChk;
                                    commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_EvalReportMaster", updateData, whereCon).get(0);
                                    System.out.println("MSg"+commonMsgChk.getMsg());
                                     if (commonMsgChk.getFlag()) {
                                        pageName = "officer/ClarificationRefDoc.jsp";
                                        queryString = "?evalRptId=" + rptId+ "&tenderId=" + tenderId;
                                        response.sendRedirect(pageName + queryString);
                                    } else {
                                        System.out.println(commonMsgChk.getId());
                                        queryString = "?evalRptId=" + rptId + "&tenderId=" + tenderId;
                                        response.sendRedirect(pageName + queryString);
                                    }

                            }
                            }
                        } else {
                            pageName = "officer/ClarificationRefDoc.jsp";

                            if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("remove")) {

                                int docId = 0;
                                String id = "";
                                String docName = "";
                                if (request.getParameter("docId") != null) {
                                    docId = Integer.parseInt(request.getParameter("docId"));

                                }
                                if (request.getParameter("evalRptId") != null) {
                                    evalRptId = Integer.parseInt(request.getParameter("evalRptId"));

                                }
                                if (request.getParameter("docName") != null) {
                                    docName = request.getParameter("docName");
                                }
                                 if (request.getParameter("tenderId") != null) {
                                    tenderId = Integer.parseInt(request.getParameter("tenderId"));
                                }
                                String whereContition = "";
                                whereContition = "evalRptDocId= " + docId;
                                System.out.println("whereCondition\t" + whereContition);
                                fileName = DESTINATION_DIR_PATH + evalRptId + "\\" + docName;
                                System.out.println("fileName" + fileName);
                                checkret = deleteFile(fileName);
                                if (checkret) {
                                    CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                    CommonMsgChk commonMsgChk;
                                    commonMsgChk = commonXMLSPService.insertDataBySP("delete", "tbl_EvalRptDocs", "", whereContition).get(0);
                                    System.out.println("commonMsgChk \t" + commonMsgChk.getMsg());
                                    queryString = "?evalRptId=" + evalRptId+"&tenderId="+tenderId;
                                    response.sendRedirect(pageName + queryString);
                                }

                            } else if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("download")) {
                                System.out.println("download" + request.getParameter("evalRptId"));
                                file = new File(DESTINATION_DIR_PATH + request.getParameter("evalRptId") + "\\" + request.getParameter("docName"));//request.getSession().getAttribute("userId")
                                InputStream fis = new FileInputStream(file);
                                byte[] buf = new byte[Integer.valueOf(request.getParameter("docSize"))];
                                int offset = 0;
                                int numRead = 0;
                                while ((offset < buf.length)
                                        && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {

                                    offset += numRead;

                                }
                                fis.close();
                                response.setContentType("application/octet-stream");
                                response.setHeader("Content-Disposition", "attachment;filename=\"" + request.getParameter("docName") + "\"");
                                ServletOutputStream outputStream = response.getOutputStream();
                                outputStream.write(buf);
                                outputStream.flush();
                                outputStream.close();
                            } else {
                                DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
                                /*
                                 *Set the size threshold, above which content will be stored on disk.
                                 */
                                fileItemFactory.setSizeThreshold(4 * 1024 * 1024); //1 MB
		/*
                                 * Set the temporary directory to store the uploaded files of size above threshold.
                                 */
                                fileItemFactory.setRepository(tmpDir);

                                ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
                                /*
                                 * Parse the request
                                 */
                                List items = uploadHandler.parseRequest(request);
                                Iterator itr = items.iterator();
                                //For Supporting Document
                                while (itr.hasNext()) {
                                    FileItem item = (FileItem) itr.next();
                                    //For Supporting Document
                    /*
                                     * Handle Form Fields.
                                     */
                                    if (item.isFormField()) {

                                        if (item.getFieldName().equals("documentBrief")) {
                                            if (item.getString() == null || item.getString().trim().length() == 0) {
                                                dis = true;
                                                break;
                                            }

                                            documentBrief = item.getString();

                                        } else if (item.getFieldName().equals("evalRptId")) {
                                            evalRptId = Integer.parseInt(item.getString());

                                        }else if (item.getFieldName().equals("tenderId")) {
                                            tenderId = Integer.parseInt(item.getString());

                                        }
                                    } else {
                                        if (item.getName().lastIndexOf("\\") != -1) {
                                            fileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1, item.getName().length());
                                        } else {
                                            fileName = item.getName();
                                        }
                                        //fileName  = fileName.replaceAll(" ", "");
                                        String realPath = DESTINATION_DIR_PATH + evalRptId;//request.getSession().getAttribute("userId")
                                        System.out.println("Real Path " + realPath);
                                        destinationDir = new File(realPath);
                                        if (!destinationDir.isDirectory()) {
                                            destinationDir.mkdir();
                                        }
                                        docSizeMsg = docSizeMsg(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
                                        // docSizeMsg = docSizeMsg(1);//userID.
                                        if (!docSizeMsg.equals("ok")) {
                                            //response.sendRedirect("SupportingDocuments.jsp?fq="+docSizeMsg);
                                        } else {
                                            fileSize = item.getSize();
                                            checkret = checkExnAndSize(fileName, item.getSize(), "common");

                                            if (!checkret) {

                                                break;
                                            } else {
                                                file = new File(destinationDir, fileName);
                                                if (file.isFile()) {
                                                    flag = true;
                                                    System.out.println("flag\t" + flag);
                                                    break;

                                                }
                                                item.write(file);
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                } catch (FileUploadException ex) {
                    log("Error encountered while parsing the request", ex);
                } catch (Exception ex) {
                    log("Error encountered while uploading file", ex);
                }


                if (!docSizeMsg.equals("ok")) {
                    queryString = "?fq=" + docSizeMsg;

                } else {
                    if (dis) {
                        docSizeMsg = "File already Exists";
                        queryString = "?fq=" + docSizeMsg + "&evalRptId=" + evalRptId+"&tenderId="+tenderId;
                        response.sendRedirect(pageName + queryString);
                    } else {
                        if (flag) {
                            docSizeMsg = "File already Exists";
                            queryString = "?fq=" + docSizeMsg + "&evalRptId=" + evalRptId+"&tenderId="+tenderId;
                            response.sendRedirect(pageName + queryString);
                        } else {

                            if (!checkret) {
                                CheckExtension ext = new CheckExtension();
                                TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("officer");
                                queryString = "?fs=" + configurationMaster.getFileSize() + "&ft=" + configurationMaster.getAllowedExtension();
                                response.sendRedirect(pageName + queryString);

                            } else {
                                if (documentBrief != null && documentBrief.trim().length() != 0) {
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    String xmldata = "";
                                    xmldata = "<tbl_EvalRptDocs evalRptId=\"" + evalRptId + "\" documentName=\"" + fileName + "\" docDescription=\"" + documentBrief + "\" docSize=\"" + fileSize + "\" uploadedDate=\"" + format.format(new Date()) + "\" uploadedBy=\"" + session.getAttribute("userId") + "\" />";
                                    xmldata = "<root>" + xmldata + "</root>";
                                    System.out.println(xmldata);
                                    documentBrief = handleSpecialChar.handleSpecialChar(documentBrief);
                                    CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                    CommonMsgChk commonMsgChk;
                                    try {
                                        commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_EvalRptDocs", xmldata, "").get(0);
                                        System.out.println(commonMsgChk.getMsg());
                                        queryString = "?evalRptId=" + evalRptId+"&tenderId="+tenderId;
                                        response.sendRedirect(pageName + queryString);
                                        if (commonMsgChk != null || handleSpecialChar != null) {
                                            commonMsgChk = null;
                                            handleSpecialChar = null;
                                        }
                                    } catch (Exception ex) {
                                        Logger.getLogger(PostQualEstDocServlet.class.getName()).log(Level.SEVERE, null, ex);
                                    }

                                }
                            }

                        }
                    }
                }
            } finally {
                //   out.close();
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public boolean checkExnAndSize(String extn, long size, String userType) {
        boolean chextn = false;
        float fsize = 0.0f;
        float dsize = 0.0f;
        int j = extn.lastIndexOf('.');
        String lst = extn.substring(j + 1);
        System.out.println("Size" + size);
        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        String str = configurationMaster.getAllowedExtension();
        String[] str1 = str.split(",");
        for (int i = 0; i < str1.length; i++) {
            if (str1[i].trim().equalsIgnoreCase(lst)) {
                chextn = true;
            }
        }
        if (chextn) {
            fsize = size / (1024 * 1024);
            System.out.println("fsize" + fsize);
            dsize = configurationMaster.getFileSize();
            System.out.println("DataSize" + dsize);
            if (dsize > fsize) {
                chextn = true;
            } else {
                chextn = false;
            }
        }
        return chextn;
    }

    public String docSizeMsg(int userId) {
        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
        return userRegisterService.docSizeCheck(userId);
    }

    public boolean deleteFile(String filePath) {

        File f = new File(filePath);
        if (f.delete()) {
            return true;
        } else {
            return false;
        }

    }
}

   
    
