/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblUserPrefrence;
import com.cptu.egp.eps.service.serviceinterface.UserPrefService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author parag
 */
public class UserPrefSrBean extends HttpServlet {

    /**this servlet handles all the request of userpreference.jsp page
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final Logger LOGGER = Logger.getLogger(UserPrefSrBean.class);
    private String logUserId = "0";
    private UserPrefService userPrefService = (UserPrefService) AppContext.getSpringBean("UserPrefService");
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        userPrefService.setAuditTrail(auditTrail);
    }

    /**
     * Handles request of userPrefrance request
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            LOGGER.debug("processRequest : " + logUserId + " Starts");
            String email = "";
            String sms = "";
            String query = "";
            String msg1 = null;
            int hiddenid = 0;
            String button = request.getParameter("submitbtn");
            String userid = "";
            String[] emailvalue = request.getParameterValues("check_list2");
            String[] smsvalue = request.getParameterValues("check_list4");
            HttpSession session = request.getSession();
            userid =session.getAttribute("userId").toString();
            //System.out.println("---uid---"+userid);

            if (session.getAttribute("userId") != null) {
                logUserId = session.getAttribute("userId").toString();
                userPrefService.setLogUserId(logUserId);
            }
            
            // FOR SELECTED EMAIL PREFERANCE
            for (int i = 0; i < emailvalue.length; i++) {
                query = emailvalue[i];
                //System.out.println(query + ",");
                if(query!=null) {
                    email += query + ",";
                }
            }

            // FOR SELECTED SMS PREFERANCE
            query = "";
            for (int i = 0; i < smsvalue.length; i++) {
                String query1 = smsvalue[i];
                //System.out.println("sms value info" + query1 + ",");
                if(query1!=null) {
                    sms += query1 + ",";
                }
            }
            if(sms != null && !"".equals(sms)){
                sms = sms.substring(0,sms.length()-1);
                System.out.println("smssmssms"+sms);
            }
            if(email != null && !"".equals(email)){
                email = email.substring(0,email.length()-1);
                   System.out.println("emailemail"+email);
            }
            TblUserPrefrence tbluserpreference = new TblUserPrefrence();
            userid = request.getSession().getAttribute("userId").toString();
            tbluserpreference.setUserId(Integer.parseInt(userid));
            tbluserpreference.setEmailAlert(email);
            tbluserpreference.setSmsAlert(sms);
            /* adding userpreference to databse*/
            if ("Submit".equals(button)) {
                setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
                LOGGER.debug("processRequest : action : submit : " + logUserId + " Starts");
                msg1 = "submit";
                userPrefService.addemailsmsAlert(tbluserpreference);
                LOGGER.debug("processRequest : action : submit : " + logUserId + " Ends");
            } else if ("Update".equals(button)) {/* updating userpreference from databse*/
                setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
                LOGGER.debug("processRequest : action : update : " + logUserId + " Starts");
                msg1 = "update";
                hiddenid = Integer.parseInt(request.getParameter("hiddenprefid"));
                tbluserpreference.setUserPrefId(hiddenid);
                userPrefService.updateemailsmsAlert(tbluserpreference);
                LOGGER.debug("processRequest : action : update : " + logUserId + " Ends");
            }
           response.sendRedirect("admin/UserPreference.jsp?msg=" + msg1);
        } catch (Exception e) {
            LOGGER.error("error isssss   " + e);
            response.sendRedirect("admin/UserPreference.jsp?msg=fail");
        }
        LOGGER.debug("processRequest : " + logUserId + " Ends");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
