/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.service.serviceimpl.PreTendQueryImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author parag
 */
public class PreTendQueryDtBean {

    private String logUserId = "0";
    private static final Logger LOGGER = Logger.getLogger(PreTendQueryDtBean.class);
     PreTendQueryImpl postQuery = (PreTendQueryImpl) AppContext.getSpringBean("PreTendQueryImpl");

    public void setLogUserId(String logUserId) {
        postQuery.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }
     private int  userId;
     private int tenderId;

    public int getTenderId() {
        return tenderId;
    }

    public void setTenderId(int tenderId) {
        this.tenderId = tenderId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * Get Data form Stored Procedure.
     * @param actionName
     * @param tenderId
     * @param userId
     * @return list of tender common data.
     * @throws Exception
     */
    public List<SPTenderCommonData> getDataFromSP(String actionName, int tenderId, int userId) throws Exception {
        LOGGER.debug("getDataFromSP : "+logUserId+" Starts");
        List<SPTenderCommonData> postQueryData = new ArrayList<SPTenderCommonData>();
        try {
            postQueryData = postQuery.getDataPreTenderQueryFromSP("" + actionName, "" + tenderId, "" + userId);
        } catch (Exception e) {
            LOGGER.debug("getDataFromSP : "+logUserId+e);
        }
        LOGGER.debug("getDataFromSP : "+logUserId+" Ends");
          return postQueryData;
     }

    /**
     * Fetching information from TblPreTenderMetDocs
     * @param tenderid
     * @return true or false
     */
    public boolean isPrebidPublished(int tenderId) {
        LOGGER.debug("isPrebidPublished : " + logUserId + " Starts");
        boolean flag = false;
        try {
            flag = postQuery.isPrebidPublished(tenderId);
        } catch (Exception e) {
            LOGGER.error("isPrebidPublished : " + logUserId + e);
     }
        LOGGER.debug("isPrebidPublished : "+logUserId+" Ends");
        return flag;
    }
//      private boolean sendMailAndSMS(String mailId,String mobNo)
//    {
//        boolean mailSent = false;
//        try {
//            SendMessageUtil sendMessageUtil = new SendMessageUtil();
//            String[] mailTo = {mailId};
//            MailContentUtility mailContentUtility = new MailContentUtility();
//
//           // List list = mailContentUtility.getGovtMailContent(mailId);
//           // String mailSub = list.get(0).toString();
//           // String mailText = list.get(1).toString();
//            sendMessageUtil.setEmailTo(mailTo);
//            sendMessageUtil.setEmailSub(mailSub);
//            sendMessageUtil.setEmailMessage(mailText);
//            try{
//                sendMessageUtil.sendEmail();
//            }
//            catch(Exception e){
//            }
//
//            try{
//               // sendMessageUtil.setSmsNo(""+mobNo);
//              //  sendMessageUtil.setSmsBody("Msg for e-GP");
//              //  sendMessageUtil.sendSMS();
//            }catch(Exception e){
//               Logger.getLogger(PreTendQueryDtBean.class.getName()).log(Level.SEVERE, null,e);
//            }
//            mailSent = true;
//        }
//        catch (Exception ex) {
//            Logger.getLogger(PreTendQueryDtBean.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return mailSent;
//    }
}
