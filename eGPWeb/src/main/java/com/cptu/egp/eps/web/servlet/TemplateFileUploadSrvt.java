/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.model.table.TblTemplateSectionDocs;
import com.cptu.egp.eps.service.serviceimpl.TemplateDocumentsService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CheckExtension;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

/**
 *
 * @author yagnesh
 */
//@WebServlet(name="TemplateFileUploadSrvt", urlPatterns={"/TemplateFileUploadSrvt"})
public class TemplateFileUploadSrvt extends HttpServlet {

    private static final String TMP_DIR_PATH = "c:\\tmp";
    private File tmpDir;
    private static final String DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("TemplateFileUploadServlet");
    private File destinationDir;
    private String logUserId = "0";
    final Logger LOGGER   = Logger.getLogger(TemplateFileUploadSrvt.class);

    @Override
    public void init(ServletConfig config) throws ServletException {
        LOGGER.debug("init: "+ logUserId + "starts");
        super.init(config);
        tmpDir = new File(TMP_DIR_PATH);
        if (!tmpDir.isDirectory()) {
            tmpDir.mkdir();
        }
        LOGGER.debug("init: "+ logUserId + "Ends");
    }

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * Use to upload,download,remove Template Files.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String realPath = "";
        response.setContentType("text/html");
        String action = "";
        if(request.getParameter("action")!=null){
            action = request.getParameter("action");
        }
        try {
            HttpSession httpSession = request.getSession();
            if(httpSession.getAttribute("userId") == null){
                response.sendRedirect("SessionTimedOut.jsp");
            }else{
                if(action.equals("removeDoc")){
                    LOGGER.debug("processRequest : action : "+action + logUserId + "starts");
                    TemplateDocumentsService templateDocument = (TemplateDocumentsService) AppContext.getSpringBean("TemplateDocumentService");
                    int sectionId = 0;
                    short templateId = 0;
                    int sectionDocId = 0;
                    String docName = "";
                    if(request.getParameter("templateId")!=null){
                        templateId = Short.parseShort(request.getParameter("templateId"));
                    }
                    if(request.getParameter("sectionId")!=null){
                        sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    }
                    if(request.getParameter("sDocId")!=null){
                        sectionDocId = Integer.parseInt(request.getParameter("sDocId"));
                    }
                    if(request.getParameter("docName")!=null){
                        docName = request.getParameter("docName");
                    }
                    String fileName = DESTINATION_DIR_PATH+sectionId+"\\"+docName;
                    if(deleteFile(fileName)){
                    templateDocument.deleteTemplateDocument(sectionDocId);
                    }else{
                        response.sendRedirect("admin/TemplateDocument.jsp?templateId=" + templateId + "&sectionId=" + sectionId + "&msg=0");
                    }
                    LOGGER.debug("processRequest : action : "+action + logUserId + "Ends");
                    response.sendRedirect("admin/TemplateDocument.jsp?templateId=" + templateId + "&sectionId=" + sectionId + "&msg=success");
                }else if(action.equals("downloadDoc")){
                    LOGGER.debug("processRequest : action : "+action + logUserId + "Starts");
                    File file = new File(DESTINATION_DIR_PATH+request.getParameter("sectionId")+"\\"+request.getParameter("docName"));
                    InputStream fis = new FileInputStream(file);
                    byte[] buf = new byte[Integer.valueOf(request.getParameter("docSize"))];
                    int offset = 0;
                    int numRead = 0;
                    while((offset < buf.length) && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)){
                        offset += numRead;
                    }
                    fis.close();
                    response.setContentType("application/octet-stream");
                    response.setHeader("Content-Disposition", "attachment;filename=\"" + request.getParameter("docName")+ "\"");
                    ServletOutputStream outputStream =  response.getOutputStream();
                    outputStream.write(buf);
                    outputStream.flush();
                    outputStream.close();
                    LOGGER.debug("processRequest : action : "+action + logUserId + "Ends");
                }else{
                    LOGGER.debug("processRequest : action : "+action + logUserId + "Starts");
                    int sectionId = 0;
                    short templateId = 0;
                    String fileName = "";
                    long fileSize = 0;
                    boolean isExtNSizeOK = false;
                    String documentBrief = "";
                    DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
                    /*
                     *Set the size threshold, above which content will be stored on disk.
                     */
                    fileItemFactory.setSizeThreshold(1 * 1024 * 1024); //1 MB
                        /*
                     * Set the temporary directory to store the uploaded files of size above threshold.
                     */
                    fileItemFactory.setRepository(tmpDir);
                    ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
                    try {
                        /*
                         * Parse the request
                         */
                        List items = uploadHandler.parseRequest(request);
                        Iterator itr = items.iterator();
                        //For Supporting Document
                        while (itr.hasNext()) {
                            FileItem item = (FileItem) itr.next();
                            //For Supporting Document
                            /*
                             * Handle Form Fields.
                             */
                            if (item.isFormField()) {
                                LOGGER.debug("File Name = " + item.getFieldName() + ", Value = " + item.getString());
                                if(item.getFieldName().equals("sectionId")) {
                                    sectionId = Integer.parseInt(item.getString());
                                }else if (item.getFieldName().equals("templateId")) {
                                    templateId = Short.parseShort(item.getString());
                                }else if (item.getFieldName().equals("documentBrief")) {
                                    documentBrief = item.getString();
                                }else if (item.getFieldName().equals("")) {

                                }

                            } else {
                                //Handle Uploaded files.
                                LOGGER.debug("Field Name = " + item.getFieldName()
                                        + ", File Name = " + item.getName()
                                        + ", Content type = " + item.getContentType()
                                        + ", File Size = " + item.getSize());
                                /*
                                 * Write file to the ultimate location.
                                 */
                                if (item.getName().lastIndexOf("\\") != -1) {
                                     fileName = item.getName().substring(item.getName().lastIndexOf("\\")+1, item.getName().length());
                                } else {
                                    fileName = item.getName();
                                }
                                //fileName  = fileName.replaceAll(" ", "");
                                fileSize = item.getSize();
                                realPath = DESTINATION_DIR_PATH + sectionId;
                                destinationDir = new File(realPath);
                                if (!destinationDir.isDirectory()) {
                                    destinationDir.mkdir();
                                }

                                isExtNSizeOK = checkExnAndSize(fileName, item.getSize(), "egpadmin");
                                if(!isExtNSizeOK){
                                    break;
                                }
                                
                                File file = new File(destinationDir, fileName);
                                item.write(file);
                            }
                        }
                        TemplateDocumentsService templateDocument = (TemplateDocumentsService) AppContext.getSpringBean("TemplateDocumentService");
                        TblTemplateSectionDocs tDocs = new TblTemplateSectionDocs();
                        tDocs.setDescription(documentBrief);
                        tDocs.setDocName(fileName);
                        tDocs.setDocSize(fileSize+"");
                        tDocs.setSectionId((short) sectionId);
                        tDocs.setTemplateId(templateId);
                        tDocs.setStatus("A");
                        if(isExtNSizeOK){
                            templateDocument.insertTemplateDocument(tDocs);
                        }
                        tDocs = null;
                        if(isExtNSizeOK){
                            response.sendRedirect("admin/TemplateDocument.jsp?templateId=" + templateId + "&sectionId=" + sectionId);
                        }else{
                            CheckExtension ext = new CheckExtension();
                            TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("egpadmin");
                            response.sendRedirect("admin/TemplateDocument.jsp?templateId=" + templateId + "&sectionId=" + sectionId + "&fs="+configurationMaster.getFileSize()+"&ft="+configurationMaster.getAllowedExtension());
                            if(ext != null){
                                ext = null;
                            }
                            if(configurationMaster != null){
                                configurationMaster = null;
                            }
                        }
                    } catch (FileUploadException ex) {
                        LOGGER.debug("processRequest : " + logUserId + ":"+ ex);
                    } catch (Exception ex) {
                        LOGGER.debug("processRequest : " + logUserId + ":"+ ex);
                    }
                }
                LOGGER.debug("processRequest : action : "+action + logUserId + "Ends");
            }
        } finally {
            
        }
    } 

    /**
     * Check File Extention and File Size
     * @param extn = File Extention
     * @param size = File size
     * @param userType = User Type
     * @return true if file is valid else false.
     */
    public boolean checkExnAndSize(String extn, long size, String userType) {
        LOGGER.debug("checkExnAndSize Starts");
        boolean chextn = false;
        float fsize = 0.0f;
        float dsize = 0.0f;
        int j = extn.lastIndexOf('.');
        String lst = extn.substring(j + 1);

        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        String str = configurationMaster.getAllowedExtension();
        String[] str1 = str.split(",");
        for (int i = 0; i < str1.length; i++) {
            if (str1[i].trim().equalsIgnoreCase(lst)) {
                chextn = true;
            }
        }
        if (chextn) {
            fsize = size / (1024 * 1024);
            dsize = configurationMaster.getFileSize();
            if (dsize > fsize) {
                chextn = true;
            } else {
                chextn = false;
            }
        }
        LOGGER.debug("checkExnAndSize Ends");
        return chextn;
    }

    /**
     * Delete Physically file from server.
     * @param filePath
     * @return true if file successfully deleted else return false.
     */
    public boolean deleteFile(String filePath) {
        File f = new File(filePath);
        if (f.delete()) {
            return true;
        } else {
            return false;
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
