/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.offlinedata;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.List;
import com.cptu.egp.eps.dao.storedprocedure.TenderDashboardOfflineDetails;
import com.cptu.egp.eps.service.serviceinterface.TenderDashboardOfflineService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.DateUtils;
import com.cptu.egp.eps.web.utility.CommonUtils;

/**
 *
 * @author Istiak
 */
public class TenderDashboardOfflineServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException, JSONException {
        //response.setContentType("text/html;charset=UTF-8");
        response.setContentType("application/Json");
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        try {
            System.gc();
            if(request.getParameter("action").equals("bindGrid"))   //  Dashboard
            {
                String moduleFlag = request.getParameter("moduleFlag");
                String searchFlag = request.getParameter("searchFlag");
                String procNature = request.getParameter("procNature");
                String procType = request.getParameter("procType");
                String procMethod = request.getParameter("procMethod");
                String id = request.getParameter("id");
                String status = request.getParameter("status");
                String refNo = request.getParameter("refNo");
                String pubDateFrom = request.getParameter("pubDateFrom");
                String pubDateTo = request.getParameter("pubDateTo");

                JSONObject jsonResponse = new JSONObject(BindGrid(session, request.getContextPath(), moduleFlag, searchFlag, procNature, procType, procMethod, id, status, refNo, pubDateFrom, pubDateTo));
                response.getWriter().print(jsonResponse.toString());  
            }
            else if(request.getParameter("action").equals("tenderdelete"))  //  Delete Tender
            {
                String moduleFlag = "tenderdelete";
                String id = request.getParameter("ID");

                boolean deleteFlag = false;
                TenderDashboardOfflineService offlineService = (TenderDashboardOfflineService) AppContext.getSpringBean("TenderDashboardOfflineService");
                deleteFlag = offlineService.deleteTenderCORDashboardOfflineData(moduleFlag, id);

                if(deleteFlag)
                {
                    response.sendRedirect("offlinedata/TenderDashboardOfflineApproval.jsp");
                }
            }
            else if(request.getParameter("action").equals("deletecorrigendum")) //  Delete Corrigendum
            {
                String moduleFlag = "deletecorrigendum";
                String id = request.getParameter("ID");

                boolean deleteFlag = false;
                TenderDashboardOfflineService offlineService = (TenderDashboardOfflineService) AppContext.getSpringBean("TenderDashboardOfflineService");
                deleteFlag = offlineService.deleteTenderCORDashboardOfflineData(moduleFlag, id);

                if(deleteFlag)
                {
                    response.sendRedirect("offlinedata/TenderDashboardOfflineApproval.jsp");
                }

            }
            else if(request.getParameter("action").equals("bindGridHome"))   //  Home Page
            {
                String moduleFlag = request.getParameter("moduleFlag");
                String searchFlag = request.getParameter("searchFlag");
                String procNature = request.getParameter("procNature");
                String procType = request.getParameter("procType");
                String procMethod = request.getParameter("procMethod");
                String id = request.getParameter("id");
                String status = "Approved";
                String refNo = request.getParameter("refNo");
                String pubDateFrom = request.getParameter("pubDateFrom");
                String pubDateTo = request.getParameter("pubDateTo");

                JSONObject jsonResponse = new JSONObject(BindGridHome(request.getContextPath(), moduleFlag, searchFlag, procNature, procType, procMethod, id, status, refNo, pubDateFrom, pubDateTo,request));
                response.getWriter().print(jsonResponse.toString());
            }
            
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet TenderDashboardOfflineServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet TenderDashboardOfflineServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
        } finally {
            out.close();
        }
    }

    public String BindGrid(HttpSession session, String contextPath, String moduleFlag, String searchFlag, String procNature, String procType, String procMethod, String id, String status, String refNo, String pubDateFrom, String pubDateTo)
    {
        String strGrid = null;
        
        try
        {
            List<TenderDashboardOfflineDetails> offlineDetailsList = null;
            TenderDashboardOfflineService offlineService = (TenderDashboardOfflineService) AppContext.getSpringBean("TenderDashboardOfflineService");
            offlineDetailsList = offlineService.getTenderDashboardOfflineData(moduleFlag, searchFlag, procNature, procType, procMethod, id, status, refNo, pubDateFrom, pubDateTo);

            if(!offlineDetailsList.isEmpty())
            {
                strGrid = "{ iTotalRecords: " + offlineDetailsList.size() + ", iTotalDisplayRecords: " + offlineDetailsList.size() + ", aaData: [";

                for(int i =0;i<offlineDetailsList.size();i++)
                {
                    TenderDashboardOfflineDetails offlineDetails = offlineDetailsList.get(i);

                    String tenderPubDate = "";
                    String tenderClosingDate = "";
                    String tenderStatus = "";
                    String pageNameEdit = "";
                    String pageNameView = "";
                    String pageNameCOR = "";
                    String action = "";
                    String title = "";
                    int corNo = 0;

                    String  reoiRfpRefNo = CommonUtils.checkNull(offlineDetails.getReoiRfpRefNo());
                    String  procurementNature = CommonUtils.checkNull(offlineDetails.getProcurementNature());
                    String  packageName = CommonUtils.checkNull(offlineDetails.getPackageName());
                    String  ministryDivision = CommonUtils.checkNull(offlineDetails.getMinistryOrDivision());
                    String  agency = CommonUtils.checkNull(offlineDetails.getAgency());
                    String  peName = CommonUtils.checkNull(offlineDetails.getPeName());
                    String  procurementType = CommonUtils.checkNull(offlineDetails.getProcurementType());
                    String  procurementMethod = CommonUtils.checkNull(offlineDetails.getProcurementMethod());
                    String  briefDescription = CommonUtils.checkNull(offlineDetails.getBriefDescription());

                    if(offlineDetails.getTenderPubDate() != null)
                    {
                        tenderPubDate = DateUtils.customDateFormate(offlineDetails.getTenderPubDate());
                    }
                    if(offlineDetails.getClosingDate() != null)
                    {
                        tenderClosingDate = DateUtils.customDateFormate(offlineDetails.getClosingDate());
                    }
                    if(offlineDetails.getEventType() != null)
                    {
                        if(offlineDetails.getEventType().equalsIgnoreCase("Tender With PQ"))
                        {
                            pageNameEdit = "EditTenderWithPQ.jsp";
                            pageNameView = "ViewTenderWithPQ.jsp";
                            pageNameCOR = "CorrigendumTenderWithPQ.jsp";
                            title = packageName;
                        }
                        else if(offlineDetails.getEventType().equalsIgnoreCase("REOI"))
                        {
                            pageNameEdit = "EditREOI.jsp";
                            pageNameView = "ViewREOI.jsp";
                            pageNameCOR = "CorrigendumREOI.jsp";
                            procurementNature = "Services";

                            if(!briefDescription.equals("") && briefDescription != null)
                            {
                                briefDescription = briefDescription.replaceAll("\\<.*?>", " ").replaceAll("[\\r\\n]", "").trim();
                                if(briefDescription.length() > 50)
                                    briefDescription = briefDescription.substring(0, 49);
                            }

                            title = briefDescription.trim();
                        }
                        else
                        {
                            pageNameEdit = "EditTenderWithoutPQ.jsp";
                            pageNameView = "ViewTenderWithoutPQ.jsp";
                            pageNameCOR = "CorrigendumTender.jsp";
                            title = packageName;
                        }
                    }
                    if(offlineDetails.getTenderStatus() != null && session.getAttribute("userId")!= null)
                    {
                        String userId = session.getAttribute("userTypeId").toString();

                        if(offlineDetails.getTenderStatus().equalsIgnoreCase("pending"))    // Pending
                        {
                            tenderStatus = "Pending";
                            action = "<a href='"+ pageNameView +"?ID="+ offlineDetails.getTenderOfflineId() +"&approve=false' class='link' target='_blank'>View</a> ";
                            action = action + "| <a href='"+ pageNameEdit +"?action=Edit&tenderOLId="+ offlineDetails.getTenderOfflineId() +"' class='link'>Edit</a> ";
                            action = action + "| <a href='" + contextPath + "/TenderDashboardOfflineServlet?action=tenderdelete&ID=" + offlineDetails.getTenderOfflineId() + "' class='link' onclick='return confirmation()'>Delete</a> ";

                            if(userId.equals("19"))   // Admin
                            {
                                action = action + "| <a href='" + pageNameView + "?ID=" + offlineDetails.getTenderOfflineId() + "&approve=true' class='link'>Approve</a> ";
                            }
                        }
                        else    // Approved
                        {
                            tenderStatus = "Approved";
                            action = "<a href='"+ pageNameView +"?ID="+ offlineDetails.getTenderOfflineId() +"&approve=false' class='link' target='_blank'>View</a> ";
                            
                            List<TenderDashboardOfflineDetails> corList = null;
                            TenderDashboardOfflineService cor = (TenderDashboardOfflineService) AppContext.getSpringBean("TenderDashboardOfflineService");
                            corList = cor.corrigendumTenderDashboardOfflineData("corrigendum", offlineDetails.getTenderOfflineId().toString());
                            boolean corFlag = corList.get(0).isFlag();
                            
                            if(corList.get(0).getNumberOfCOR() != null)
                                corNo = corList.get(0).getNumberOfCOR();
                            
                            if(corFlag)   // If Corrigendum
                            {
                                action = action + "| <a href='" + pageNameCOR + "?action=Edit&tenderOLId=" + offlineDetails.getTenderOfflineId() + "&corNo=" + corNo + "'  class='link'>COR Edit</a> ";
                                action = action + "| <a href='" + contextPath + "/TenderDashboardOfflineServlet?action=deletecorrigendum&ID=" + offlineDetails.getTenderOfflineId() + "'  class='link' onclick='return confirmation()'>COR Delete</a> ";

                                if(userId.equals("19")) // Admin
                                {
                                    action = action + "| <a href='" + pageNameView + "?ID=" + offlineDetails.getTenderOfflineId() + "&approve=trueCOR' class='link'>COR Approve</a> ";
                                }
                            }
                            else    // If no Corrigendum is Pending
                            {
                                action = action + "| <a href='"+ pageNameCOR +"?action=Create&tenderOLId="+ offlineDetails.getTenderOfflineId() +"&corNo=" + corNo + "' class='link'>COR Entry</a> ";
                            }
                        }

                        strGrid = strGrid + "[";
                        strGrid = strGrid + "\"" + (i+1) + "\",";
                        strGrid = strGrid + "\"" + reoiRfpRefNo + ",<br/><label style='color: red; font-weight: bold;'>" + tenderStatus + "</label>" + "\",";
                        strGrid = strGrid + "\"" + procurementNature +"<br/><a href='" + pageNameView + "?ID="+ offlineDetails.getTenderOfflineId() + "&approve=false' class='link' target='_blank'>" + title + "</a>" + "\",";
                        strGrid = strGrid + "\"" + ministryDivision +"<br/>"+ agency + "<br/>" + peName +"\",";
                        strGrid = strGrid + "\"" + procurementType + "<br/>" + procurementMethod +"\",";
                        strGrid = strGrid + "\"" + tenderPubDate + "<br/>" + tenderClosingDate +"\",";
                        strGrid = strGrid + "\"" + action + "\" ";
                        strGrid = strGrid + "]";

                        if (i < offlineDetailsList.size() - 1)
                            strGrid = strGrid + ",";
                    }
                }
            }
            else
            {
                strGrid = "{ iTotalRecords: " + 0 + ", iTotalDisplayRecords: " + 0 + ", aaData: [";
                strGrid = strGrid + "], \"Error\": \"No data is Found\"}";
            }

            strGrid = strGrid + "] }";
        }
        catch(Exception e)
        {
            strGrid = "{ iTotalRecords: " + 0 + ", iTotalDisplayRecords: " + 0 + ", aaData: [";
            strGrid = strGrid + "], \"Error\": \"An error occurred\"}";
        }
        
        return strGrid;

    }

    public String BindGridHome( String contextPath, String moduleFlag, String searchFlag, String procNature, String procType, String procMethod, String id, String status, String refNo, String pubDateFrom, String pubDateTo, HttpServletRequest request)
    {
        String strGrid = null;

        try
        {
            List<TenderDashboardOfflineDetails> offlineDetailsList = null;
            TenderDashboardOfflineService offlineService = (TenderDashboardOfflineService) AppContext.getSpringBean("TenderDashboardOfflineService");
            offlineDetailsList = offlineService.getTenderDashboardOfflineData(moduleFlag, searchFlag, procNature, procType, procMethod, id, status, refNo, pubDateFrom, pubDateTo);

            if(!offlineDetailsList.isEmpty())
            {
                strGrid = "{ iTotalRecords: " + offlineDetailsList.size() + ", iTotalDisplayRecords: " + offlineDetailsList.size() + ", aaData: [";

                for(int i =0;i<offlineDetailsList.size();i++)
                {
                    TenderDashboardOfflineDetails offlineDetails = offlineDetailsList.get(i);

                    String tenderPubDate = "";
                    String tenderClosingDate = "";
                    String pageNameView = "";
                    String action = "";
                    String title = "";

                    String  reoiRfpRefNo = CommonUtils.checkNull(offlineDetails.getReoiRfpRefNo());
                    String  procurementNature = CommonUtils.checkNull(offlineDetails.getProcurementNature());
                    String  packageName = CommonUtils.checkNull(offlineDetails.getPackageName());
                    String  ministryDivision = CommonUtils.checkNull(offlineDetails.getMinistryOrDivision());
                    String  agency = CommonUtils.checkNull(offlineDetails.getAgency());
                    String  peName = CommonUtils.checkNull(offlineDetails.getPeName());
                    String  procurementType = CommonUtils.checkNull(offlineDetails.getProcurementType());
                    String  procurementMethod = CommonUtils.checkNull(offlineDetails.getProcurementMethod());
                    String  briefDescription = offlineDetails.getBriefDescription();

                    if(offlineDetails.getTenderPubDate() != null)
                    {
                        tenderPubDate = DateUtils.customDateFormate(offlineDetails.getTenderPubDate());
                    }
                    if(offlineDetails.getClosingDate() != null)
                    {
                        tenderClosingDate = DateUtils.customDateFormate(offlineDetails.getClosingDate());
                    }

                    
                            if(briefDescription != null && !briefDescription.equals(""))
                            {
                                briefDescription = briefDescription.replaceAll("\\<.*?>", " ").replaceAll("[\\r\\n]", "").trim();
                                if(briefDescription.length() > 50)
                                    briefDescription = briefDescription.substring(0, 49);
                            }

                            title = briefDescription.trim();
                       

                         strGrid = strGrid + "[\"" + (i+1) + "\",\"" + offlineDetails.getTenderOfflineId() + "\",\"" + reoiRfpRefNo + "\",\"" + procurementNature +
                                 "<br/><a href='"+request.getContextPath()+"/resources/common/ViewTender.jsp?id="+ offlineDetails.getTenderOfflineId() + "&h=t' class='link' target='_blank'>" + title + "</a>"
                                + "\",\"" + ministryDivision +"<br/>"+ agency + "<br/>" + peName +"\",\"" + procurementType + "<br/>" + procurementMethod +"\",\"" + tenderPubDate + "<br/>" + tenderClosingDate +"\",\""  +
                                "<a href='" + request.getContextPath()+ "OpeningReports.jsp?tenderId="+ offlineDetails.getTenderOfflineId()+"&lotId=0&comType=null' class='link' target='_blank'>View</a>" + "\"]";
                    if (i < offlineDetailsList.size() - 1)
                        strGrid = strGrid + ",";
                    }
            }
            else
            {
                strGrid = "{ iTotalRecords: " + 0 + ", iTotalDisplayRecords: " + 0 + ", aaData: [";
                strGrid = strGrid + "], \"Error\": \"No data is Found\"}";
            }

            strGrid = strGrid + "] }";
        }
        catch(Exception e)
        {
            System.out.println(e.getStackTrace());
            strGrid = "{ iTotalRecords: " + 0 + ", iTotalDisplayRecords: " + 0 + ", aaData: [";
            strGrid = strGrid + "], \"Error\": \"An error occurred\"}";
        }
        
        return strGrid;

    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(TenderDashboardOfflineServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(TenderDashboardOfflineServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
