/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblNegNotifyTenderer;
import com.cptu.egp.eps.model.table.TblNegQuery;
import com.cptu.egp.eps.model.table.TblNegReply;
import com.cptu.egp.eps.model.table.TblNegotiation;
import com.cptu.egp.eps.service.serviceimpl.CommonServiceImpl;
import com.cptu.egp.eps.service.serviceimpl.NegotiationProcessImpl;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.*;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Rikin
 */
public class NegotiationProcessSrBean {

    final Logger logger = Logger.getLogger(NegotiationProcessSrBean.class);
    private String logUserId ="0";
    NegotiationProcessImpl negotiationProcessImpl = (NegotiationProcessImpl) AppContext.getSpringBean("NegotiationProcessImpl");
    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
     
    public void setLogUserId(String logUserId) {
        negotiationProcessImpl.setLogUserId(logUserId);
        tenderCommonService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }
    
    
    /**
     * Get company name of tender 
     * @param tenderId
     * @return List of Company Name
     */
    public List<SPTenderCommonData> getNegotiationCompanyName(int tenderId){

        logger.debug("getNegotiationCompanyName : "+logUserId+" Starts");
        List<SPTenderCommonData> sPTenderCommonDatas = null;
        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        try{
            sPTenderCommonDatas = tenderCommonService.returndata("getFinalSubComp",""+tenderId, null);
        }catch(Exception ex){
            logger.error("getNegotiationCompanyName : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getNegotiationCompanyName : "+logUserId+" Ends");
        return sPTenderCommonDatas;
    }

    /**
     * Get the Negotiation details from negotiation id.
     * @param negId
     * @return List of negotiation details.
     */
    public List<SPTenderCommonData> getAllNegotiationDetails(int negId){
        logger.debug("getAllNegotiationDetails : "+logUserId+" Starts");
        List<SPTenderCommonData> sPTenderCommonDatas = null;
       
        try{
            sPTenderCommonDatas = tenderCommonService.returndata("getAllNegdetails",""+negId, null);
        }catch(Exception ex){
            logger.error("getAllNegotiationDetails : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getAllNegotiationDetails : "+logUserId+" Ends");
        return sPTenderCommonDatas;        
    }    

    /**
     * Get Negotiation details from tender Id
     * @param tenderId
     * @return List of Negotiation details
     */
    public List<SPTenderCommonData> getNegotiationDetails(int tenderId){
        logger.debug("getNegotiationDetails : "+logUserId+" Starts");
        List<SPTenderCommonData> sPTenderCommonDatas = null;
        try{
            sPTenderCommonDatas = tenderCommonService.returndata("getNegDetails",""+tenderId, null);
        }catch(Exception ex){
            logger.error("getNegotiationDetails : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getNegotiationDetails : "+logUserId+" Ends");
        return sPTenderCommonDatas;        
    }

    /**
     * Get bidder negotiation details
     * @param tenderId
     * @param userId
     * @return List of Negotiation details
     */
    public List<SPTenderCommonData> getBidderNegotiationDetails(int tenderId, int userId){
        logger.debug("getBidderNegotiationDetails : "+logUserId+" Starts");
        List<SPTenderCommonData> sPTenderCommonDatas = null;
        try{
            sPTenderCommonDatas = tenderCommonService.returndata("getBidderNegDetails",""+tenderId,""+userId);
        }catch(Exception ex){
            logger.error("getBidderNegotiationDetails : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getBidderNegotiationDetails : "+logUserId+" Ends");
        return sPTenderCommonDatas;
    }

    /**
     * Get no of available docs for particular negId.
     * @param negId
     * @return List of negotiation details.
     */
    public List<SPTenderCommonData> getCountOfDocs(int negId){
        logger.debug("getCountOfDocs : "+logUserId+" Starts");
        List<SPTenderCommonData> sPTenderCommonDatas = null;
        try{
            sPTenderCommonDatas = tenderCommonService.returndata("getNegdocDetails",""+negId,"");
        }catch(Exception ex){
            logger.error("getCountOfDocs : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getCountOfDocs : "+logUserId+" Ends");
        return sPTenderCommonDatas;
    }
    /**
     * Check if any Negotiation is pending or not.
     * @param tenderId
     * @return return 1 if pending or 0 if not.
     */
    public int isAnyNegPending(int tenderId){
        logger.debug("isAnyNegPending : "+logUserId+" Starts");
        int i = 0;       
        try{
            i = negotiationProcessImpl.isAnyNegPending(tenderId);
        }catch(Exception ex){
            logger.error("isAnyNegPending : "+logUserId+" : "+ex.toString());
    }
        logger.debug("isAnyNegPending : "+logUserId+" Ends");
        return i;        
    }


    /**
     * Get Bidder Negotiation details by tenderId and negId
     * @param tenderId
     * @param negId
     * @return List of Negotiation Details.
     */
    public List<SPTenderCommonData> getBidderNegDetailsById(int tenderId,int negId){
        logger.debug("getBidderNegDetailsById : "+logUserId+" Starts");
        List<SPTenderCommonData> sPTenderCommonDatas = null;
        try{
            sPTenderCommonDatas = tenderCommonService.returndata("getBidderNegDetailsById",""+tenderId,""+negId);
        }catch(Exception ex){
            logger.error("getBidderNegDetailsById : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getBidderNegDetailsById : "+logUserId+" Ends");
        return sPTenderCommonDatas;        
    }

    /**
     * View Queries by tenderId and negId
     * @param tenderId
     * @param negId
     * @return List of Negotiation Queries.
     */
    public List<TblNegQuery> viewQueries(int tenderId,int negId){
        logger.debug("viewQueries : "+logUserId+" Starts");
        List<TblNegQuery> listQuery = null;
        try{
            listQuery = negotiationProcessImpl.viewAllQuery(tenderId,negId);
        }catch(Exception ex){
            logger.error("viewQueries : "+logUserId+" : "+ex.toString());
        }
        logger.debug("viewQueries : "+logUserId+" Ends");
        return listQuery;
    }

    /**
     * check if Doc is available for particular Query or not.
     * @param queryId
     * @return true if Doc available or false if not
     */
    public boolean isDOCAvail(int queryId){
        logger.debug("isDOCAvail : "+logUserId+" Starts");
        boolean flag =false;
        try{
            flag = negotiationProcessImpl.isDOCAvail(queryId);
        }catch(Exception ex){
            logger.error("isDOCAvail : "+logUserId+" : "+ex.toString());
    }
        logger.debug("isDOCAvail : "+logUserId+" Ends");
        return flag;
    }
    /**
     * Check if Negotiation Doc available or not.
     * @param negId
     * @return true if Neg Doc available and false if not available.
     */
    public boolean isNegDOCAvail(int negId){
        logger.debug("isNegDOCAvail : "+logUserId+" Starts");
        boolean flag = false;
        try{
            flag = negotiationProcessImpl.isNegDOCAvail(negId);
        }catch(Exception ex){
            logger.error("isNegDOCAvail : "+logUserId+" : "+ex.toString());
    }
        logger.debug("isNegDOCAvail : "+logUserId+" Ends");
        return flag;
    }

    /**
     * Get Query Information by tenderId, neg Id and Query Id.
     * @param tenderId
     * @param negId
     * @param queryId
     * @return List of Query information
     */
    public List<TblNegQuery> getQueryById(int tenderId,int negId,int queryId){

        logger.debug("getQueryById : "+logUserId+" Starts");
        List<TblNegQuery> listQuery = null;
        try{
            listQuery = negotiationProcessImpl.getQueryById(tenderId, negId, queryId);
        }catch(Exception ex){
            logger.error("getQueryById : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getQueryById : "+logUserId+" Ends");
        return listQuery;        
    }

    /**
     * get Reply information by Query Id.
     * @param queryId
     * @return List of Negotiation Reply.
     */
    public List<TblNegReply> viewReply(int queryId){
        logger.debug("viewReply : "+logUserId+" Starts");
        List<TblNegReply> detailsReply = null;
        try{
            detailsReply =  negotiationProcessImpl.getReplyByQueryId(queryId);
        }catch(Exception ex){
            logger.error("viewReply : "+logUserId+" : "+ex.toString());
        }
        logger.debug("viewReply : "+logUserId+" Ends");
        return detailsReply;        
    }

    /**
     * Get final Status of the negId.
     * @param negId
     * @return final Status.
     */
    public String getFinalSubStatus(int negId){
        logger.debug("getFinalSubStatus : "+logUserId+" Starts");
        List<TblNegotiation> listNeg = null;
        String status = "";
        try{
        listNeg = negotiationProcessImpl.getFinalSubStatus(negId);
        if(!listNeg.isEmpty()){
                status = listNeg.get(0).getNegFinalSub();
        }
        }catch(Exception ex){
            logger.error("getFinalSubStatus : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getFinalSubStatus : "+logUserId+" Ends");
        return status;
    }

    /**
     * Get Tender Details by Tender Id.
     * @param tenderId
     * @return Object Array of Tender Details.
     */
    public Object[] getTenderDetails(int tenderId){
        logger.debug("getTenderDetails : "+logUserId+" Starts");
        Object[] objects = null;
        try{
        objects  = negotiationProcessImpl.getTenderDetails(tenderId);
        }catch(Exception ex){
            logger.error("getTenderDetails : "+logUserId+" : "+ex.toString());
        }
         logger.debug("getTenderDetails : "+logUserId+" Ends");
        return objects;
    }
    
    /**
     * Send email and SMS
     * @param mailId
     * @param countryCode
     * @param mobNo
     * @return return true if successfully send mail and SMS or false if not.
     */
    public boolean sendMailAndSMS(String mailId,String countryCode,String mobNo,String tenderId,String tenRefNo,String sDate)
    {
        logger.debug("sendMailAndSMS : "+logUserId+" Starts");
        boolean mailSent = false;
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            String[] mailTo = {mailId};
            MailContentUtility mailContentUtility = new MailContentUtility();

            List list = mailContentUtility.getNegMailContent(tenderId,tenRefNo,sDate);
            String mailSub = list.get(0).toString();
            String mailText = list.get(1).toString()+list.get(2).toString();
            sendMessageUtil.setEmailTo(mailTo);
            sendMessageUtil.setEmailSub(mailSub);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
            registerService.contentAdmMsgBox(mailId, XMLReader.getMessage("emailIdNoReply"), mailSub, list.get(1).toString());
            try{
                sendMessageUtil.sendEmail();
                CommonServiceImpl csImpl = new CommonServiceImpl();
                csImpl.sendMsg(mailId, mailSub, mailText);
            }
            catch(Exception ex){
                logger.error("viewQueries : "+logUserId+" : "+ex.toString());
            }

            /*try{
               sendMessageUtil.setSmsNo(""+mobNo);
               sendMessageUtil.setSmsBody(list.get(2).toString());
               sendMessageUtil.sendSMS();
            }catch(Exception ex){
               logger.error("sendMailAndSMS : "+logUserId+" : "+ex.toString());
            }*/
         mailSent = true;
        }
        catch (Exception ex) {
            logger.error("sendMailAndSMS : "+logUserId+" : "+ex.toString());
        }
        logger.debug("sendMailAndSMS : "+logUserId+" Ends");
        return mailSent;
    }

    /**
     * Check if member is TECPEC or not.
     * @param tenderId
     * @param userId
     * @return true if member is TESPEC or false if not.
     */
    public boolean isTECPECMember(int tenderId,int userId){
        logger.debug("isTECPECMember : "+logUserId+" Starts");
        boolean flag = false;
        List<SPTenderCommonData> listTenderData= null;       
        try{
            listTenderData = tenderCommonService.returndata("IsTECMember ",""+tenderId,""+userId);
        if(listTenderData!=null){
                if(!listTenderData.isEmpty()){
                    flag = true;
            }
        }
        }catch(Exception ex){
            logger.error("isTECPECMember : "+logUserId+" : "+ex.toString());
    }
         logger.debug("isTECPECMember : "+logUserId+" Ends");
        return flag;
    }
    
    /**
     * Check if Bid is agree or not.
     * @param negId
     * @return true if Bid is Agree or false if not.
     */
    public boolean isBidAgree(int negId){
        logger.debug("isBidAgree : "+logUserId+" Starts");
        boolean flag = false;
        try{
            flag = negotiationProcessImpl.isBidAgreed(negId);
        }catch(Exception ex){
            logger.error("isBidAgree : "+logUserId+" : "+ex.toString());
    }
        logger.debug("isBidAgree : "+logUserId+" Ends");
        return flag;
    }

    /**
     * Fetch final remarks of negotiation
     * @param negId
     * @return final Remarks.
     */
    public String getFinalRemarks(int negId){
        logger.debug("getFinalRemarks : "+logUserId+" Starts");
        String str = "";
        try{
            str = negotiationProcessImpl.getFinalRemarks(negId);
        }catch(Exception ex){
            logger.error("getFinalRemarks : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getFinalRemarks : "+logUserId+" Ends");
        return str;
    }

    /**
     * Check Final Submission done for given negotiation.
     * @param negId
     * @return String having value "" indicated submission not done/ "Yes" indicate final submission done
     */
     public String checkFinalNegoFinalSubmissionDone(int negId)
     {
         logger.debug("checkFinalNegoFinalSubmissionDone : "+logUserId+" Starts");
         String status = null;
         status = negotiationProcessImpl.checkFinalNegoFinalSubmissionDone(negId);
         logger.debug("checkFinalNegoFinalSubmissionDone : "+logUserId+" End");
        
        return status;
     }
    

    /**
     * Get Negotiation Agree status of Chair person.
     * @param negId
     * @return String having values as 'Accept' / 'Reject' / ''
     */
    public String getOfficerAgreeStatus(int negId){
        logger.debug("getFinalRemarks : "+logUserId+" Starts");
        List<TblNegotiation> list= new ArrayList<TblNegotiation>();
        List<TblNegNotifyTenderer> listTenderer= new ArrayList<TblNegNotifyTenderer>();
        String str = "";
        try{
            list = negotiationProcessImpl.getFinalSubStatus(negId);
            if(list!=null && !list.isEmpty()){
                str = list.get(0).getNegOfficeAggree();
            }
            
        }catch(Exception ex){
            logger.error("getFinalRemarks : "+logUserId+" : "+ex.toString());
            ex.printStackTrace();
        }
        logger.debug("getFinalRemarks : "+logUserId+" Ends");
        return str;
    }
    
    public String getTenderAgreeStatus(int negId){
        logger.debug("getTenderAgreeStatus : "+logUserId+" Starts");
        List<TblNegNotifyTenderer> listTenderer= new ArrayList<TblNegNotifyTenderer>();
        String str = "";
        try {
            listTenderer = negotiationProcessImpl.getFinalSubStatusTender(negId);
            if(listTenderer!=null && !listTenderer.isEmpty()){
                str = listTenderer.get(0).getAcceptStatus();
            }
        }catch(Exception ex){
            logger.error("getTenderAgreeStatus : "+logUserId+" : "+ex.toString());
            ex.printStackTrace();
        }
        logger.debug("getTenderAgreeStatus : "+logUserId+" Ends");
        return str;
    }
    
    /**
     * Get Tenderer Negotiation Details by Tender id.
     * @param tenderId
     * @return Object array.
     */
    public Object[] getNegoTenderInfoFromTenderId(int tenderId)
    {
        logger.debug("getNegIdFromTenderId : "+logUserId+" Starts");
        Object[] objNego = null;
        try
        {
            objNego = negotiationProcessImpl.getNegoTenderInfoFromTenderId(tenderId);
        }
        catch(Exception ex){
            logger.error("getNegIdFromTenderId : "+logUserId+" : "+ex.toString());
            ex.printStackTrace();
        }
        logger.debug("getNegIdFromTenderId : "+logUserId+" Ends");
        return objNego;
    }

    /**
     * Get Tenderer Negotiation Details by user id.
     * @param userId
     * @return Object array.
     */
     public Object[] getTendererInfoFromUserId(int userId)
    {
        logger.debug("getTendererInfoFromUserId : "+logUserId+" Starts");
        Object[] objNego = null;
        try
        {
            objNego = negotiationProcessImpl.getTendererInfoFromUserId(userId);
        }
        catch(Exception ex){
            logger.error("getTendererInfoFromUserId : "+logUserId+" : "+ex.toString());
            ex.printStackTrace();
        }
        logger.debug("getTendererInfoFromUserId : "+logUserId+" Ends");
        return objNego;
    }
}
