/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblTemplateMaster;
import com.cptu.egp.eps.service.serviceinterface.TemplateMaster;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author dipti
 */
//@WebServlet(name="CreateTemplateSrBean", urlPatterns={"/CreateTemplateSrBean"})
public class CreateTemplateSrBean extends HttpServlet {

    private String logUserId = "0";
    final Logger logger = Logger.getLogger(CreateTemplateSrBean.class);
    TemplateMaster templateMaster = (TemplateMaster) AppContext.getSpringBean("TemplateMasterService");
    private AuditTrail auditTrail;
    public void setAuditTrail(AuditTrail auditTrail) {
            this.auditTrail  = auditTrail;
            templateMaster.setAuditTrail(auditTrail);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        Object objUserId = session.getAttribute("userId");
        if (objUserId != null) {
            logUserId = session.getAttribute("userId").toString();
        }



        logger.debug("processRequest : " + logUserId + "starts");
        templateMaster.setUserId(logUserId);

        //boolean flag = scBankDevpartnerService.createScBankDevPartner(tblScBankDevPartnerMaster);

        try {
            if (request.getParameter("action") != null) {
                String action = request.getParameter("action");
        
                if (action.equalsIgnoreCase("insert")) {
                    setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
                     logger.debug("processRequest : action : "+action + logUserId + "starts");
        try {
                        String procType = request.getParameter("txtProcType").trim();
                        String txtSection = request.getParameter("txtSection").trim();
                        byte txtSectionNo = Byte.parseByte(request.getParameter("txtSectionNo"));
                        String stdfor = "";
                        String devPartnerId = "";

                        if(request.getParameter("rdoprocurmentRulesfollow")!=null && !"".equalsIgnoreCase(request.getParameter("rdoprocurmentRulesfollow"))){
                            stdfor = request.getParameter("rdoprocurmentRulesfollow");
                        }
                        if(request.getParameter("selDevPartner")!=null && !"".equalsIgnoreCase(request.getParameter("selDevPartner"))){
                            devPartnerId = request.getParameter("selDevPartner");
                        }
                        TblTemplateMaster tblTemplateMaster = new TblTemplateMaster();

                        tblTemplateMaster.setProcType(procType);
                        tblTemplateMaster.setTemplateName(txtSection);
                        tblTemplateMaster.setNoOfSections(txtSectionNo);
                        tblTemplateMaster.setStatus("P");
                        tblTemplateMaster.setStdFor(stdfor);
                        tblTemplateMaster.setDevPartnerId(devPartnerId);
                        
                        boolean b = templateMaster.createTemplate(tblTemplateMaster);

                        if (b == true) {
                           
                            response.sendRedirect("admin/DefineSTDSections.jsp?templateId=" + tblTemplateMaster.getTemplateId() + "&noOfSection=" + tblTemplateMaster.getNoOfSections() + "&templateName=" + txtSection + "&");
                        } else {
                            response.sendRedirect("admin/CreateTemplate.jsp?er=tr");
                        }
                    } catch (Exception ex) {
                       
                        throw new Exception(ex);
                    }
                     logger.debug("processRequest : action : "+action + logUserId + "ends");
                } else if (action.equalsIgnoreCase("varifyName")) {
                   logger.debug("processRequest : action : "+action + logUserId + "starts");
                    try {

                        String txtSection = request.getParameter("txtSection").trim();
                        List<TblTemplateMaster> tblTemplateMaster = templateMaster.searchTemplateByName(txtSection);

                        if (tblTemplateMaster == null) {
                            out.print("valid");
                        } else if (tblTemplateMaster.isEmpty()) {
                            out.print("valid");
                        } else {
                            out.print("invalid");
                        }
                        tblTemplateMaster = null;
                    } catch (Exception ex) {
                        logger.error("processRequest : " + ex);
                    } 
                   logger.debug("processRequest : action : "+action + logUserId + "ends");

                    }
                }
        } catch (Exception ex) {
            logger.error("processRequest : " + ex);
            response.sendRedirect("error.jsp");
        } finally {
            out.close();
        }
        logger.debug("processRequest : " + logUserId + "ends");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
