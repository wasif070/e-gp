/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblIttClause;
import com.cptu.egp.eps.model.table.TblIttHeader;
import com.cptu.egp.eps.model.table.TblIttSubClause;
import com.cptu.egp.eps.service.serviceinterface.SectionClause;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author yanki
 */
public class SectionClauseSrBean extends HttpServlet {
    private String logUserId = "0";
    static final Logger LOGGER = Logger.getLogger(SectionClauseSrBean.class);
    SectionClause sectionClause = (SectionClause) AppContext.getSpringBean("SectionClauseService");

    public void setLogUserId(String logUserId) {
        sectionClause.setUserId(logUserId);
        this.logUserId = logUserId;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
           
           if(request.getParameter("action") != null){
               LOGGER.debug(request.getParameter("action") + " : " + logUserId + " Starts");
               if (request.getSession().getAttribute("userId") != null) {
                    logUserId = request.getSession().getAttribute("userId").toString();
                    sectionClause.setUserId(logUserId);
               }
               if(request.getParameter("action").equalsIgnoreCase("updateClause")){
                   //&headerId=<%=ittHeaderId%>&clauseInfo=" + clauseInfo + "&clauseId=" + clauseId + "&",
                   int headerId = Integer.parseInt(request.getParameter("headerId"));
                   String clauseName = request.getParameter("clauseInfo");
                   if(clauseName != null){
                       clauseName = handleSpecialCharHere(clauseName);
                   }
                   int clauseId = Integer.parseInt(request.getParameter("clauseId"));
                   
                   TblIttClause tblIttClause = new TblIttClause(clauseId, new TblIttHeader(headerId), clauseName);
                   //SectionClause sectionClause = (SectionClause) AppContext.getSpringBean("SectionClauseService");
                   boolean flag = sectionClause.updateClause(tblIttClause);
                   out.print(flag);
               } else if(request.getParameter("action").equalsIgnoreCase("updateSubClause")){
                   int clauseId = Integer.parseInt(request.getParameter("clauseId"));
                   int subClauseId = Integer.parseInt(request.getParameter("subClauseId"));
                   String clauseName = request.getParameter("subclauseName");
                   if(clauseName != null){
                       clauseName = handleSpecialCharHere(clauseName);
                   }
                   String isTdsApplicable = request.getParameter("isTdsApplicable");
                   if(isTdsApplicable.equalsIgnoreCase("true")){
                       isTdsApplicable = "yes";
                   }else{
                       isTdsApplicable = "no";
                   }
                   TblIttSubClause tblIttSubClause = new TblIttSubClause(subClauseId, new TblIttClause(clauseId),clauseName, isTdsApplicable);
                   //SectionClause sectionClause = (SectionClause) AppContext.getSpringBean("SectionClauseService");
                   boolean flag = sectionClause.updateSubClause(tblIttSubClause);
                   out.print(flag);
               } else if(request.getParameter("action").equalsIgnoreCase("saveClause")){
                   //action=saveClause&clauseName=Clause%201&subClauseName_1=Clause%201%20-%20SC1&isTDSAppl_1=false&
                   int ittHeaderId = Integer.parseInt(request.getParameter("ittHeaderId"));
                   String clauseName = request.getParameter("clauseName").replaceAll("%20", " ");
                   if(clauseName != null){
                       clauseName = handleSpecialCharHere(clauseName);
                   }
                   TblIttClause tblIttClause = new TblIttClause();
                   tblIttClause.setTblIttHeader(new TblIttHeader(ittHeaderId));
                   tblIttClause.setIttClauseName(clauseName);
                   //SectionClause sectionClause = (SectionClause) AppContext.getSpringBean("SectionClauseService");
                   String ret = "";
                   if(sectionClause.createSectionClause(tblIttClause)){
                       int ittClauseId = tblIttClause.getIttClauseId();
                       ret = ittClauseId + ",";
                       int i=1;
                       try{
                           while(request.getParameter("subClauseName_" + i) != null){
                               TblIttSubClause tblIttSubClause = new TblIttSubClause();
                               tblIttSubClause.setTblIttClause(new TblIttClause(ittClauseId));
                               tblIttSubClause.setIttSubClauseName(handleSpecialCharHere(request.getParameter("subClauseName_" + i).replaceAll("%20", " ")));
                               String tds = "no";
                               if(request.getParameter("isTDSAppl_" + i).equalsIgnoreCase("true")){
                                   tds = "yes";
                               }
                               tblIttSubClause.setIsTdsApplicable(tds);
                               boolean flag = sectionClause.createSubSectionClause(tblIttSubClause);
                               ret = ret + tblIttSubClause.getIttSubClauseId() + ",";
                               tblIttSubClause = null;
                               i++;
                           }
                       }catch(Exception ex){
                           LOGGER.error(request.getParameter("action") + " : " + logUserId + ex);
                       }
                   }
                   out.print("true," + ret);
               } else if(request.getParameter("action").equalsIgnoreCase("saveSubClause")) {
                   int clauseId = Integer.parseInt(request.getParameter("clauseId"));
                   String subclauseName = request.getParameter("subclauseName");
                   if(subclauseName != null){
                       subclauseName = handleSpecialCharHere(subclauseName);
                   }
                   String isTdsApplicable = request.getParameter("isTdsApplicable");
                   TblIttSubClause tblIttSubClause = new TblIttSubClause();
                   tblIttSubClause.setIttSubClauseName(subclauseName);
                   tblIttSubClause.setTblIttClause(new TblIttClause(clauseId));
                   if(isTdsApplicable.equalsIgnoreCase("true")){
                       isTdsApplicable = "yes";
                   } else {
                       isTdsApplicable = "no";
                   }
                   tblIttSubClause.setIsTdsApplicable(isTdsApplicable);

                   //SectionClause sectionClause = (SectionClause) AppContext.getSpringBean("SectionClauseService");
                   boolean flag = sectionClause.createSubSectionClause(tblIttSubClause);
                   String ret = flag + "";
                   if(flag){
                       ret = ret + "," + tblIttSubClause.getIttSubClauseId();
                   }
                   out.print(ret);
               } else if(request.getParameter("action").equalsIgnoreCase("removeClause")){
                   int clauseId = Integer.parseInt(request.getParameter("clauseId"));
                   int ittHeaderId = Integer.parseInt(request.getParameter("ittHeaderId"));
                   //SectionClause sectionClause = (SectionClause) AppContext.getSpringBean("SectionClauseService");
                   TblIttClause tblIttClause = new TblIttClause(clauseId);
                   tblIttClause.setTblIttHeader(new TblIttHeader(ittHeaderId));
                   tblIttClause.setIttClauseName("");
                   boolean flag = sectionClause.deleteClause(tblIttClause);
                   out.print(flag);

               } else if(request.getParameter("action").equalsIgnoreCase("removeSubClause")){
                   int clauseId = Integer.parseInt(request.getParameter("clauseId"));
                   int subClauseId = Integer.parseInt(request.getParameter("ittSubClauseId"));
                   //SectionClause sectionClause = (SectionClause) AppContext.getSpringBean("SectionClauseService");
                   TblIttSubClause tblIttSubClause = new TblIttSubClause(subClauseId);
                   tblIttSubClause.setTblIttClause(new TblIttClause(clauseId));
                   tblIttSubClause.setIsTdsApplicable("no");
                   tblIttSubClause.setIttSubClauseName("");
                   boolean flag = sectionClause.deleteSubClause(tblIttSubClause);

                   out.print(flag);
               }
               LOGGER.debug(request.getParameter("action") + " : " + logUserId + " Ends");
           }else{
               LOGGER.debug("createSubSectionClause : " + logUserId + " Starts");
               TblIttClause tblIttClause;
               TblIttSubClause tblIttSubClause;
               int i=1;
               int totalClause = Integer.parseInt(request.getParameter("totalClauseCount"));

               int sectionId = Integer.parseInt(request.getParameter("sectionId"));
               int templateId = Integer.parseInt(request.getParameter("templateId"));
               for(i=1;i<=totalClause;i++){
                   if(request.getParameter("subSectionClause_"+i) != null){
                       tblIttClause = new TblIttClause();
                       tblIttClause.setIttClauseName(request.getParameter("subSectionClause_"+i));
                       tblIttClause.setTblIttHeader(new TblIttHeader(Integer.parseInt(request.getParameter("ittHeaderId"))));
                       boolean b = sectionClause.createSectionClause(tblIttClause);
                       int totalSubClauseCount = Integer.parseInt(request.getParameter("totalSubClauseCount_"+i));
                       LOGGER.debug("totalSubClauseCount : " + totalSubClauseCount);
                       for(int j=1;j<=totalSubClauseCount;j++){
                           if(request.getParameter("subSectionSubClause_" + i + "_"+j) != null){
                               tblIttSubClause = new TblIttSubClause();
                               String subClauseName = request.getParameter("subSectionSubClause_" + i + "_"+j);
                               if(subClauseName != null){
                                   subClauseName = handleSpecialCharHere(subClauseName);
                               }
                               tblIttSubClause.setIttSubClauseName(subClauseName);
                               String isTdsApplicable = "";
                               if(request.getParameter("chkTDCApplicable_"+i+"_"+j) == null){
                                    isTdsApplicable = "no";
                               }else{
                                    isTdsApplicable = "yes";
                               }
                               //String isTdsApplicable = request.getParameter("chkSelSubClause_"+i+"_"+j);
                               LOGGER.debug("chkTDCApplicable_"+i+"_"+j+ " : " + request.getParameter("chkTDCApplicable_"+i+"_"+j));
                               tblIttSubClause.setIsTdsApplicable(isTdsApplicable);
                               tblIttSubClause.setTblIttClause(tblIttClause);
                               boolean b1 = sectionClause.createSubSectionClause(tblIttSubClause);
                               LOGGER.debug("Create section sub clause status:" + b1 + ", i:" + i + ", j:" + j);
                           }
                       }
                   }
               }
               LOGGER.debug("createSubSectionClause : " + logUserId + " Ends");
               response.sendRedirect("admin/subSectionDashBoard.jsp?sectionId="+ sectionId +"&templateId=" + templateId + "&flag=1");
               //response.sendRedirectFliter("subSectionDashBoard.jsp?sectionId="+ sectionId +"&templateId=" + templateId + "&flag=1");
           }
        } catch(Exception ex){
            LOGGER.error("SectionClauseSrBean : " + logUserId + ex);
        } finally {
            out.close();
        }
    }

    public String getSubSectionName(int ittHeaderId){
        LOGGER.debug("getSubSectionName : " + logUserId + " Starts");
        String subSectionName = "";
        try {
            subSectionName = sectionClause.getSectionName(ittHeaderId);
        } catch (Exception e) {
            LOGGER.error("getSubSectionName : " + logUserId + e);
        }
        LOGGER.debug("getSubSectionName : " + logUserId + " Ends");
        return subSectionName;
    }

    public List<TblIttClause> getClauseDetail_TDS(int ittHeaderId){
        LOGGER.debug("getClauseDetail_TDS : " + logUserId + " Starts");
        List<Object[]> tblIttClauseObj;
        List<TblIttClause> tblIttClauseList = new ArrayList<TblIttClause>();
        try {
        tblIttClauseObj = sectionClause.getClauseDetailsTDS(ittHeaderId);
        for(int i=0; i< tblIttClauseObj.size(); i++){
                TblIttClause tblIttClause = new TblIttClause();
            tblIttClause.setIttClauseId( (Integer) tblIttClauseObj.get(i)[0]);
            tblIttClause.setIttClauseName(tblIttClauseObj.get(i)[1].toString());
            tblIttClauseList.add(tblIttClause);
        }
        } catch (Exception e) {
            LOGGER.error("getClauseDetail_TDS : " + logUserId + e);
        }
        LOGGER.debug("getClauseDetail_TDS : " + logUserId + " Ends");
        return tblIttClauseList;
    }

     public List<TblIttClause> getClauseDetail(int ittHeaderId){
        List<TblIttClause> tblIttClause = null;
        LOGGER.debug("getClauseDetail : " + logUserId + " Starts");
         try {
        tblIttClause = sectionClause.getClauseDetails(ittHeaderId);
         } catch (Exception e) {
             LOGGER.error("getClauseDetail : " + logUserId + e);
         }
        LOGGER.debug("getClauseDetail : " + logUserId + " Ends");
        return tblIttClause;
    }
    

    public long getClauseCount(int ittHeaderId){
        LOGGER.debug("getClauseCount : " + logUserId + " Starts");
        long clauseCount = 0;
        try {
        clauseCount = sectionClause. getClauseCount(ittHeaderId);
        } catch (Exception e) {
            LOGGER.error("getClauseCount : " + logUserId + e);
        }
        LOGGER.debug("getClauseCoun : " + logUserId + " Ends");
        return clauseCount;
    }

    public List<TblIttSubClause> getSubClauseDetail_TDS(int ittClauseId){
        LOGGER.debug("getSubClauseDetail_TDS : " + logUserId + " Starts");
        List<TblIttSubClause> tblIttSubClause = null;
        try {
        tblIttSubClause = sectionClause.getSubClauseDetailsTDS(ittClauseId);
        } catch (Exception e) {
            LOGGER.error("getSubClauseDetail_TDS : " + logUserId + e);
        }
        LOGGER.debug("getSubClauseDetail_TDS : " + logUserId + " Ends");
        return tblIttSubClause;
    }

    public List<TblIttSubClause> getSubClauseDetail(int ittClauseId){
        LOGGER.debug("getSubClauseDetail : " + logUserId + " Starts");
        List<TblIttSubClause> tblIttSubClause = null;
        try {
        tblIttSubClause = sectionClause.getSubClauseDetails(ittClauseId);
        } catch (Exception e) {
            LOGGER.error("getSubClauseDetail : " + logUserId + e);
        }
        LOGGER.debug("getSubClauseDetail : " + logUserId + " Ends");
        return tblIttSubClause;
    }

    public static String handleSpecialCharHere(String str){
        LOGGER.debug("handleSpecialCharHere : " + " Starts");
        String msg = "";
        try {
            msg = str.replaceAll("'", "&rsquo;");
        } catch (Exception e) {
            LOGGER.error("handleSpecialCharHere : " + e);
    }
        LOGGER.debug("handleSpecialCharHere : " + " Ends");
        return msg;
    }
    

    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
