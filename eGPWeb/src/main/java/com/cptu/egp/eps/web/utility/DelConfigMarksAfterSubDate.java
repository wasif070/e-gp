/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 *
 * @author nishit
 */
public class DelConfigMarksAfterSubDate extends QuartzJobBean {
    public static int i = 0;
    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        if(i == 0){
            i++;
            CommonSearchService cmnSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            List<SPCommonSearchData> recordCron = null;
            recordCron = cmnSearchService.searchData("getCronJob", "Reset Service Eval Config Marks", null, null, null, null, null, null, null, null);
            if (recordCron.isEmpty()) {
                CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                int cronStatus = 1;
                String cronMsg = "Successful Executed";
                String cronName = "Reset Service Eval Config Marks";
                String  cronStartTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
                CommonXMLSPService  xmlservice = (CommonXMLSPService)AppContext.getSpringBean("CommonXMLSPService");
                    List<SPCommonSearchDataMore> resultList = commonSearchDataMoreService.geteGPDataMore("deleteUnpubCorriConfigMarks");
                    if (resultList != null && !resultList.isEmpty() && resultList.get(0).getFieldName1().equalsIgnoreCase("Error")) {
                        cronStatus = 0;
                        System.out.println("Error in Spring Cron(DelConfigMarksAfterSubDate.java) : " + resultList.get(0).getFieldName2());
                        cronMsg = resultList.get(0).getFieldName2();
                }

                String  cronEndTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
                String xml = "<root>"
                        + "<tbl_CronJobs "
                        + "cronJobName=\"" + cronName + "\" "
                        + "cronJobStartTime=\"" + cronStartTime + "\" "
                        + "cronJobEndTime=\"" + cronEndTime + "\" "
                        + "cronJobStatus=\"" + cronStatus + "\" "
                        + "cronJobMsg=\"" + cronMsg + "\"/>"
                        + "</root>";
                try {
                    xmlservice.insertDataBySP("insert", "tbl_CronJobs", xml, "");
                } catch (Exception ex) {
                    ex.printStackTrace();
                    AppExceptionHandler expHandler = new AppExceptionHandler();
                    expHandler.stack2string(ex);
                }
            }
        }
    }
}

