/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.model.table.TblBiddingPermission;
import com.cptu.egp.eps.model.table.TblCompanyMaster;
import com.cptu.egp.eps.model.table.TblCountryMaster;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblStateMaster;
import com.cptu.egp.eps.model.table.TblTempCompanyJointVenture;
import com.cptu.egp.eps.model.table.TblTempCompanyMaster;
import com.cptu.egp.eps.model.table.TblTempBiddingPermission;
import com.cptu.egp.eps.model.table.TblTempTendererMaster;
import com.cptu.egp.eps.model.table.TblTendererAuditTrail;
import com.cptu.egp.eps.model.table.TblTendererMaster;
import com.cptu.egp.eps.model.table.TblUserPrefrence;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.databean.JvcaCompListDtBean;
import com.cptu.egp.eps.web.databean.TempCompanyMasterDtBean;
import com.cptu.egp.eps.web.databean.TempBiddingPermission;
import com.cptu.egp.eps.web.databean.TendererMasterDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SelectItem;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.hibernate.Transaction;
//import org.hibernate.cfg.Configuration;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author TaherT
 */
public class TendererMasterSrBean {

    static final Logger logger = Logger.getLogger(TendererMasterSrBean.class);
    private UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    private CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
    List<SelectItem> countryList = new ArrayList<SelectItem>();
    List<SelectItem> stateList = new ArrayList<SelectItem>();
    List<SelectItem> subDistrictList = new ArrayList<SelectItem>();
    List<SelectItem> areaCode = new ArrayList<SelectItem>();
    private String logUserId = "0";
    private AuditTrail auditTrail;
    private static final String START = " Starts";
    private static final String END = " Ends";

    public String getLogUserId() {
        return logUserId;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        userRegisterService.setAuditTrail(auditTrail);
        this.auditTrail = auditTrail;
    }

    public void setLogUserId(String userId) {
        userRegisterService.setUserId(userId);
        commonService.setUserId(userId);
        this.logUserId = userId;
    }

    /**
     * List of Sub-district
     *
     * @param cntName name of the Sub-district
     * @return List of SelectItem(stateid,statename)
     */
    public List<SelectItem> getSubdistrictList(String stateName) {
        logger.debug("getSub-districtList : " + logUserId + START);
        subDistrictList.clear();
        subDistrictList.add(new SelectItem("", "--Select Dungkhag--"));
        for (TblDepartmentMaster subdistrictMaster : commonService.getSubdistrictByStateId(stateName)) {
            subDistrictList.add(new SelectItem(subdistrictMaster.getDepartmentName(), subdistrictMaster.getDepartmentName()));
        }
        logger.debug("getSub-districtList : " + logUserId + END);
        return subDistrictList;
    }

    public List<SelectItem> getGewogList(String stateName) {
        logger.debug("getGewogList : " + logUserId + START);
        subDistrictList.clear();
        subDistrictList.add(new SelectItem("", "--Select Gewog--"));
        for (TblDepartmentMaster subdistrictMaster : commonService.getSubdistrictByStateId2(stateName)) {
            subDistrictList.add(new SelectItem(subdistrictMaster.getDepartmentName(), subdistrictMaster.getDepartmentName()));
        }
        logger.debug("getGewogList : " + logUserId + END);
        return subDistrictList;
    }

    public List<SelectItem> getGewogList2(String stateName) {
        logger.debug("getGewogList : " + logUserId + START);
        subDistrictList.clear();
        subDistrictList.add(new SelectItem("", "--Select Gewog--"));
        for (TblDepartmentMaster subdistrictMaster : commonService.getSubdistrictByStateId4(stateName)) {
            subDistrictList.add(new SelectItem(subdistrictMaster.getDepartmentName(), subdistrictMaster.getDepartmentName()));
        }
        logger.debug("getGewogList : " + logUserId + END);
        return subDistrictList;
    }

    public void setSubdistrictList(List<SelectItem> stateList) {
        this.stateList = stateList;
    }

    public String getareaCode(String stateName) {
        logger.debug("getAreaCode : " + logUserId + START);
        areaCode.clear();
        for (TblStateMaster areaCodeMaster : commonService.getareaCode(stateName)) {
            areaCode.add(new SelectItem(areaCodeMaster.getareaCode(), areaCodeMaster.getareaCode()));
        }
        logger.debug("getAreaCode : " + logUserId + END);
        return areaCode.get(0).getObjectValue();
    }

    /**
     * List of State
     *
     * @param cntName name of the country
     * @return List of SelectItem(stateid,statename)
     */
    public List<SelectItem> getStateList(String cntName) {
        logger.debug("getStateList : " + logUserId + START);
        stateList.clear();
        for (TblStateMaster stateMaster : commonService.getStateByCntName(cntName)) {
            stateList.add(new SelectItem(stateMaster.getStateName(), stateMaster.getStateName()));
        }
        logger.debug("getStateList : " + logUserId + END);
        return stateList;
    }

    public void setStateList(List<SelectItem> stateList) {
        this.stateList = stateList;
    }

    /**
     * List of Country
     *
     * @return List of SelectItem(countryid,countryname)
     */
    public List<SelectItem> getCountryList() {
        logger.debug("getCountryList : " + logUserId + START);
        if (countryList.isEmpty()) {
            try {
                for (TblCountryMaster countryMaster : commonService.countryMasterList()) {
                    countryList.add(new SelectItem(countryMaster.getCountryId(), countryMaster.getCountryName()));
                }
            } catch (Exception ex) {
                logger.error("getCountryList : " + logUserId, ex);
            }
        }
        logger.debug("getCountryList : " + logUserId + END);
        return countryList;
    }

    public void setCountryList(List<SelectItem> countryList) {
        this.countryList = countryList;
    }

    /**
     * Method : IndividualConsultant,media and other registration.
     *
     * @param tendererMasterDtBean TendererMasterDtBean object
     * @param userid userid from session
     */
    public boolean registerIndividualClient(TendererMasterDtBean tendererMasterDtBean, int userid) {
        logger.debug("registerIndividualClient : " + logUserId + START);
        boolean flag = true;
        try {
            tendererMasterDtBean.setComments("registration");
            tendererMasterDtBean.setIsAdmin("no");
            tendererMasterDtBean.setDesignation("");
            tendererMasterDtBean.setDepartment("");
            //tendererMasterDtBean.setFullNameInBangla(tendererMasterDtBean.getBanglaName().getBytes());
            TblTempTendererMaster tempTendererMaster = _toTblTempTendererMaster(tendererMasterDtBean);
            userRegisterService.registerIndvidualClient(tempTendererMaster, userid, true);
        } catch (Exception ex) {
            logger.error("registerIndividualClient : " + logUserId, ex);
            flag = false;
        }
        logger.debug("registerIndividualClient : " + logUserId + END);
        return flag;
    }

    /**
     * Converting TendererMasterDtBean to TblTempTendererMaster
     *
     * @param tendererMasterDtBean TendererMasterDtBean object
     * @return TblTempTendererMaster
     */
    public TblTempTendererMaster _toTblTempTendererMaster(TendererMasterDtBean tendererMasterDtBean) {
        logger.debug("_toTblTempTendererMaster : " + logUserId + START);
        TblTempTendererMaster tblTempTendererMaster = new TblTempTendererMaster();
        BeanUtils.copyProperties(tendererMasterDtBean, tblTempTendererMaster);
        logger.debug("_toTblTempTendererMaster : " + logUserId + END);
        return tblTempTendererMaster;
    }

    /**
     * Method : Contractor,Supplier Personal Detail Registration.
     *
     * @param tendererMasterDtBean TendererMasterDtBean object
     * @param userid userid from session
     * @return pageName for redirection
     * @throws Exception
     */
    public String registerPersonalDetail(TendererMasterDtBean tendererMasterDtBean, int userid) throws Exception {
        logger.debug("registerPersonalDetail : " + logUserId + START);
        String pageName = "";
        //Transaction tx = null;
        //SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        //Session session = sessionFactory.openSession();
        try {
            //tx = session.beginTransaction();
            tendererMasterDtBean.setComments("registration");
            tendererMasterDtBean.setIsAdmin("no");
            tendererMasterDtBean.setSpecialization("");
            tendererMasterDtBean.setTinNo("");
            tendererMasterDtBean.setTinDocName("");
            TblTempTendererMaster tempTendererMaster = _toTblTempTendererMaster(tendererMasterDtBean);
            if (tendererMasterDtBean.getCmpId() == 0) {
                TblTempCompanyMaster master = userRegisterService.companyRegNoCount("", userid).get(0);
                tempTendererMaster.setTblTempCompanyMaster(new TblTempCompanyMaster(master.getCompanyId()));
            } else {
                tempTendererMaster.setTblTempCompanyMaster(new TblTempCompanyMaster(tendererMasterDtBean.getCmpId()));
            }
            pageName = userRegisterService.registerIndvidualClient(tempTendererMaster, userid, false);
            //tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            //tx.rollback();
        }

        logger.debug("registerPersonalDetail : " + logUserId + END);

        return pageName;
    }
//    public String registerPersonalDetail(TendererMasterDtBean tendererMasterDtBean, int userid) throws Exception {
//        logger.debug("registerPersonalDetail : " + logUserId + START);
//        String pageName = "";
//        tendererMasterDtBean.setComments("registration");
//        tendererMasterDtBean.setIsAdmin("no");
//        tendererMasterDtBean.setSpecialization("");
//        tendererMasterDtBean.setTinNo("");
//        tendererMasterDtBean.setTinDocName("");
//
//        TblTempTendererMaster tempTendererMaster = _toTblTempTendererMaster(tendererMasterDtBean);
//        if (tendererMasterDtBean.getCmpId() == 0) {
//            TblTempCompanyMaster master = userRegisterService.companyRegNoCount("", userid).get(0);
//            tempTendererMaster.setTblTempCompanyMaster(new TblTempCompanyMaster(master.getCompanyId()));
//        } else {
//            tempTendererMaster.setTblTempCompanyMaster(new TblTempCompanyMaster(tendererMasterDtBean.getCmpId()));
//        }
//        pageName = userRegisterService.registerIndvidualClient(tempTendererMaster, userid, false);
//        logger.debug("registerPersonalDetail : " + logUserId + END);
//
//        return pageName;
//    }

    /**
     * Converting TempCompanyMasterDtBean to TblTempCompanyMaster
     *
     * @param tempCompanyMasterDtBean TempCompanyMasterDtBean object
     * @return TblTempCompanyMaster
     */
    public TblTempCompanyMaster _toTblTempCompanyMaster(TempCompanyMasterDtBean tempCompanyMasterDtBean) {
        logger.debug("_toTblTempCompanyMaster : " + logUserId + START);
        TblTempCompanyMaster tblTempCompanyMaster = new TblTempCompanyMaster();
        BeanUtils.copyProperties(tempCompanyMasterDtBean, tblTempCompanyMaster);
        logger.debug("_toTblTempCompanyMaster : " + logUserId + END);
        return tblTempCompanyMaster;
    }

    public TblTempBiddingPermission _toTblTempBiddingPermission(TempBiddingPermission tempBiddingPermission) {
        logger.debug("_toTblTempBiddingPermission : " + logUserId + START);
        TblTempBiddingPermission tblTempBiddingPermission = new TblTempBiddingPermission();
        BeanUtils.copyProperties(tempBiddingPermission, tblTempBiddingPermission);
        logger.debug("_toTblTempBiddingPermission : " + logUserId + END);
        return tblTempBiddingPermission;
    }

    /**
     * Method : Company Details Registration.
     *
     * @param tempCompanyMasterDtBean TempCompanyMasterDtBean object
     * @param userId userid from session
     * @return true or false for success or fail
     * @throws Exception
     */
    public boolean registerCompanyDetail(TempCompanyMasterDtBean tempCompanyMasterDtBean, List<TempBiddingPermission> tempBiddingPermissionlist, TblUserPrefrence tblUserPreference, int userId) throws Exception {
        logger.debug("registerCompanyDetail : " + logUserId + START);
        boolean flag = false;
        TblTempCompanyMaster tblTempCompanyMaster = new TblTempCompanyMaster();
        List<TblTempBiddingPermission> tblTempBiddingPermissionList = new ArrayList<TblTempBiddingPermission>();
//        Transaction tx = null;
//        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
//        Session session = sessionFactory.openSession();
        try {
//            tx = session.beginTransaction();
            tblTempCompanyMaster = _toTblTempCompanyMaster(tempCompanyMasterDtBean);
            //for(TempBiddingPermission tempBiddingPermission : tempBiddingPermissionlist)
            TempBiddingPermission tempBiddingPermission = new TempBiddingPermission();
            for (int i = 0; i < tempBiddingPermissionlist.size(); i++) {
                tempBiddingPermission = tempBiddingPermissionlist.get(i);
                TblTempBiddingPermission tblTempBiddingPermission = _toTblTempBiddingPermission(tempBiddingPermission);
                tblTempBiddingPermissionList.add(tblTempBiddingPermission);
            }
            flag = userRegisterService.registerCompanyDetails(tblTempCompanyMaster, tblTempBiddingPermissionList, tblUserPreference, userId);
            //flag = true;
//            if (!tx.wasCommitted()) {
//                tx.commit();
//            }
        } catch (Exception ex) {
//            if (tx != null) {
//                tx.rollback();
//            }
            ex.printStackTrace();

        }
        logger.debug("registerCompanyDetail : " + logUserId + END);
        //return userRegisterService.registerCompanyDetails(tblTempCompanyMaster, tblTempBiddingPermissionList, tblUserPreference, userId); //abcSessionBean.getUserId()
        return flag;
    }

    /**
     * Inserting Joint Venture Details
     *
     * @param role jvrole
     * @param jvCompanyId jvcompanyid
     * @param userid userid from session
     * @return success message
     * @throws Exception
     */
    public String addJvcaDetail(String role, int jvCompanyId, int userid) throws Exception {
        logger.debug("addJvcaDetail : " + logUserId + START);
        String msg = null;
        CommonMsgChk commonMsgChk = userRegisterService.insertJvcaDetailsBySP(userid, jvCompanyId, role).get(0);
        if (commonMsgChk.getFlag()) {
            msg = commonMsgChk.getMsg() + "1";
        } else {
            msg = commonMsgChk.getMsg() + "0";
        }
        logger.debug("addJvcaDetail : " + logUserId + END);
        return msg;
    }

    /**
     * Getting Details of JV companies for Display
     *
     * @param userId
     * @return
     * @throws Exception
     */
    public List<JvcaCompListDtBean> getCompanyJointVentures(int userId) throws Exception {
        logger.debug("getCompanyJointVentures : " + logUserId + START);
        List<JvcaCompListDtBean> companyJointVentures = new ArrayList<JvcaCompListDtBean>();
        for (Object[] objects : userRegisterService.getJvCompanyDetail(userId)) {
            companyJointVentures.add(new JvcaCompListDtBean((Integer) objects[0], (Integer) objects[1], (String) objects[2], (String) objects[3], (String) objects[4]));
        }
        logger.debug("getCompanyJointVentures : " + logUserId + END);
        return companyJointVentures;
    }

    /**
     * Method : Delete JV company Details.
     *
     * @param ventureId ventureId for deletion
     */
    public void delteJvcaDetail(int ventureId) {
        logger.debug("delteJvcaDetail : " + logUserId + START);
        TblTempCompanyJointVenture tcjv = new TblTempCompanyJointVenture();
        tcjv.setVentureId(ventureId);
        tcjv.setTblTempCompanyMaster(new TblTempCompanyMaster(999));
        tcjv.setJvRole("");
        tcjv.setTblCompanyMaster(new TblCompanyMaster(999));
        userRegisterService.deleteJvcaDetail(tcjv);
        logger.debug("delteJvcaDetail : " + logUserId + END);
    }

    /**
     * Method : Getting Tenderer details from LoginMaster.
     *
     * @param userId userid from session
     * @return TblTempTendererMaster
     * @throws Exception
     */
    public TblTempTendererMaster getTendererDetails(int userId) throws Exception {
        logger.debug("getTendererDetails : " + logUserId + START);
        TblTempTendererMaster tttm = null;
        List<TblTempTendererMaster> tendererMaster = userRegisterService.findTendererByCriteria("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId));
        if (!tendererMaster.isEmpty()) {
            tttm = tendererMaster.get(0);
        }
        logger.debug("getTendererDetails : " + logUserId + START);
        return tttm;
    }

    /**
     * Method : update IndividualClient Details
     *
     * @param tempTendererMaster TempTendererMaster object
     * @param isIndiCli boolean for checking IndividualClient
     * @return true or false for success or fail
     */
    public boolean updateIndividualClient(TendererMasterDtBean tempTendererMaster, boolean isIndiCli) {
        logger.debug("updateIndividualClient : " + logUserId + START);
        TblTempTendererMaster tblTempTendererMaster = new TblTempTendererMaster();
        //Transaction tx = null;
        //SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        //Session session = sessionFactory.openSession();
        try {
            //tx = session.beginTransaction();
            tblTempTendererMaster = _toTblTempTendererMaster(tempTendererMaster);
            logger.debug("updateIndividualClient : " + logUserId + " isIndiCli : " + isIndiCli);
            if (isIndiCli) {
                tblTempTendererMaster.setDepartment("");
                tblTempTendererMaster.setDesignation("");
            } else {
                tblTempTendererMaster.setSpecialization("");
                tblTempTendererMaster.setTinNo("");
                tblTempTendererMaster.setTinDocName("");
            }
            //tblTempTendererMaster.setFullNameInBangla(tempTendererMaster.getBanglaName().getBytes());
            tblTempTendererMaster.setTblLoginMaster(new TblLoginMaster(tempTendererMaster.getUserId()));
            tblTempTendererMaster.setTblTempCompanyMaster(new TblTempCompanyMaster(tempTendererMaster.getCmpId()));
            //tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            //tx.rollback();
        }
        logger.debug("updateIndividualClient : " + logUserId + END);
        return userRegisterService.updateTendererDetail(tblTempTendererMaster);
    }

    /**
     * Get TblTempCompanyMaster by userid
     *
     * @param userId from session
     * @return TblTempCompanyMaster
     * @throws Exception
     */
    public TblTempCompanyMaster getCompanyDetails(int userId) throws Exception {
        logger.debug("getCompanyDetails : " + logUserId + START);
        TblTempCompanyMaster companyMaster = null;
        List<TblTempCompanyMaster> list = userRegisterService.companyRegNoCount("", userId);
        if (!list.isEmpty()) {
            companyMaster = list.get(0);
        }
        logger.debug("getCompanyDetails : " + logUserId + END);
        return companyMaster;
    }

    public List<TblTempBiddingPermission> getBiddingPermission(int userId) throws Exception {
        logger.debug("getBiddingPermission : " + logUserId + START);
        List<TblTempBiddingPermission> biddingPermission = null;
        List<TblTempBiddingPermission> list = userRegisterService.biddingPermissionCount(userId);
        if (!list.isEmpty()) {
            return list;
        } else {
            logger.debug("getBiddingPermission : " + logUserId + END);
            return biddingPermission;
        }
    }

    /**
     * Company Already Exist by Ajax
     *
     * @param cmpRegNo companyRegNo
     * @return true or false
     * @throws Exception
     */
    public String cmpAlreadyExist(String cmpRegNo) throws Exception {
        logger.debug("cmpAlreadyExist : " + logUserId + START);
        String cmpName = null;
        List<TblCompanyMaster> list = userRegisterService.companyRegNoCheck(cmpRegNo, 0);
        if (!list.isEmpty()) {
            cmpName = list.get(0).getCompanyName();
        }
        logger.debug("cmpAlreadyExist : " + logUserId + END);
        return cmpName;
    }

    /**
     * Update TempCompanyDetails
     *
     * @param companyMasterDtBean CompanyMasterDtBean object
     * @return true or false for success or fail
     */
    public boolean updateCompanyDetails(TempCompanyMasterDtBean companyMasterDtBean, List<TempBiddingPermission> tempBiddingPermissionList, TblUserPrefrence tblUserPreference) {
        logger.debug("updateCompanyDetails : " + logUserId + START);
        TblTempCompanyMaster tblTempCompanyMaster = new TblTempCompanyMaster();
        List<TblTempBiddingPermission> tblTempBiddingPermissionList = new ArrayList<TblTempBiddingPermission>();
        //Transaction tx = null;
        //SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        //Session session = sessionFactory.openSession();
        try {
            //tx = session.beginTransaction();
            tblTempCompanyMaster = _toTblTempCompanyMaster(companyMasterDtBean);
            // bidding permission dtbean convert to tbl
            TempBiddingPermission tempBiddingPermission = new TempBiddingPermission();

            for (int i = 0; i < tempBiddingPermissionList.size(); i++) {
                tempBiddingPermission = tempBiddingPermissionList.get(i);
                TblTempBiddingPermission tblTempBiddingPermission = _toTblTempBiddingPermission(tempBiddingPermission);
                tblTempBiddingPermissionList.add(tblTempBiddingPermission);
            }
            //tblTempCompanyMaster.setCompanyNameInBangla(companyMasterDtBean.getBanglaName().getBytes());
            tblTempCompanyMaster.setTblLoginMaster(new TblLoginMaster(companyMasterDtBean.getUserId()));
            //tx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            //tx.rollback();
        }
        logger.debug("updateCompanyDetails : " + logUserId + END);
        return userRegisterService.updateTempCompanyDetails(tblTempCompanyMaster, tblTempBiddingPermissionList, tblUserPreference);
    }

    /**
     * Update Next Screen Details in TblLoginMaster
     *
     * @param userId from session
     * @param pageName page for updation
     * @return 1(success) or 0(failure)
     * @throws Exception
     */
    public int updateNextScreenDetail(int userId, String pageName) throws Exception {
        logger.debug("updateNextScreenDetail : " + logUserId + START);
        logger.debug("updateNextScreenDetail : " + logUserId + END);
        return userRegisterService.updateNextScreenInLogin(userId, pageName);
    }

    /**
     * Updates Tenderer Details
     *
     * @param tempTendererMaster to be updated
     * @param isIndiCli flag indicating individual consultant
     * @return true or false for success or fail
     */
    public boolean updateTblTenderMaster(TendererMasterDtBean tempTendererMaster, boolean isIndiCli) {
        logger.debug("updateTblTenderMaster : " + logUserId + START);
        logger.debug("updateTblTenderMaster : " + logUserId + " isIndiCli : " + isIndiCli);
        TblTendererMaster tblTendererMaster = _toTblTendererMaster(tempTendererMaster);
        if (tempTendererMaster.getLastName() != null) {
            tblTendererMaster.setLastName(tempTendererMaster.getLastName());
        }
        if (isIndiCli) {
            tblTendererMaster.setDepartment("");
            tblTendererMaster.setDesignation("");
        } else {
            tblTendererMaster.setSpecialization("");
            tblTendererMaster.setTinNo("");
            tblTendererMaster.setTinDocName("");
        }
        tblTendererMaster.setTblLoginMaster(new TblLoginMaster(tempTendererMaster.getUserId()));
        tblTendererMaster.setTblCompanyMaster(new TblCompanyMaster(tempTendererMaster.getCmpId()));
        logger.debug("updateTblTenderMaster : " + logUserId + END);
        return userRegisterService.updateTblTenderMaster(tblTendererMaster);
    }

    public TblTendererMaster _toTblTendererMaster(TendererMasterDtBean tendererMasterDtBean) {
        TblTendererMaster tblTendererMaster = new TblTendererMaster();
        BeanUtils.copyProperties(tendererMasterDtBean, tblTendererMaster);
        tblTendererMaster.setLastName(tendererMasterDtBean.getLasatName());
        //tblTendererMaster.setLastName("");
        return tblTendererMaster;
    }

    public TblCompanyMaster _toTblCompanyMaster(TempCompanyMasterDtBean tempCompanyMasterDtBean) {
        TblCompanyMaster tblCompanyMaster = new TblCompanyMaster();
        BeanUtils.copyProperties(tempCompanyMasterDtBean, tblCompanyMaster);
        return tblCompanyMaster;
    }

    public TblBiddingPermission _toTblBiddingPermission(TempBiddingPermission tempBiddingPermission) {
        TblBiddingPermission tblBiddingPermission = new TblBiddingPermission();
        BeanUtils.copyProperties(tempBiddingPermission, tblBiddingPermission);
        return tblBiddingPermission;
    }

    /**
     * Updates Bidder Company Details
     *
     * @param companyMasterDtBean to be updated
     * @return true or false for success or fail
     */
    public boolean updateTblCompanyMaster(TempCompanyMasterDtBean companyMasterDtBean) {
        logger.debug("updateTblCompanyMaster : " + logUserId + START);
        TblCompanyMaster tblCompanyMaster = _toTblCompanyMaster(companyMasterDtBean);
        tblCompanyMaster.setTblLoginMaster(new TblLoginMaster(companyMasterDtBean.getUserId()));
        logger.debug("updateTblCompanyMaster : " + logUserId + END);
        return userRegisterService.updateTblCompanyMaster(tblCompanyMaster);
    }

    /**
     * Reapply Bidder Permission
     *
     * @param tempBiddingPermissionlist to be updated
     * @return true or false for success or fail
     */
    public boolean reapplyTblBiddingPermission(List<TempBiddingPermission> tempBiddingPermissionlist, int companyID, int userID) {
        logger.debug("updateTblBiddingPermission : " + logUserId + START);

        List<TblBiddingPermission> tblBiddingPermissionList = new ArrayList<TblBiddingPermission>();
        TempBiddingPermission tempBiddingPermission = new TempBiddingPermission();
        for (int i = 0; i < tempBiddingPermissionlist.size(); i++) {
            tempBiddingPermission = tempBiddingPermissionlist.get(i);
            TblBiddingPermission tblTempBiddingPermission = _toTblBiddingPermission(tempBiddingPermission);
            tblBiddingPermissionList.add(tblTempBiddingPermission);
        }
        logger.debug("updateTblBiddingPermission : " + logUserId + END);

        return userRegisterService.updateTblBiddingPermission(tblBiddingPermissionList, companyID, userID);
    }

    /**
     * insert data into TblLoginMaster ,TblTendererMaster
     *
     * @param tendererMasterDtBean userId password
     * @param userId
     * @param password
     * @return id array containing {userId,tendererId}
     */
    public int[] createCompanyUser(TendererMasterDtBean tendererMasterDtBean, int userId, String password) {
        logger.debug("create; : " + logUserId + START);
        int[] ids = null;
        try {
            TblLoginMaster tblLoginMaster = new TblLoginMaster();
            tblLoginMaster.setEmailId(tendererMasterDtBean.getEmailId());
            tblLoginMaster.setPassword(tendererMasterDtBean.getPassword());
            tblLoginMaster.setHintAnswer("");
            tblLoginMaster.setHintQuestion("");
            tblLoginMaster.setNextScreen("");
            tblLoginMaster.setIsEmailVerified("yes");
            tblLoginMaster.setFailedAttempt((byte) 0);
            //tblLoginMaster.setValidUpTo(new Date());
            tblLoginMaster.setStatus("approved");
            tblLoginMaster.setFirstLogin("yes");
            tblLoginMaster.setIsPasswordReset("no");
            tblLoginMaster.setResetPasswordCode("");
            tblLoginMaster.setRegisteredDate(new Date());
            tblLoginMaster.setNationality(tendererMasterDtBean.getNationality());
            TblTendererMaster tblTendererMaster = _toTblTendererMaster(tendererMasterDtBean);
            tblTendererMaster.setLastName(tendererMasterDtBean.getLasatName());
            tblTendererMaster.setLasatName(null);
            tblTendererMaster.setNationality("");
            tblTendererMaster.setIsAdmin("no");
            tblTendererMaster.setSpecialization("");
            tblTendererMaster.setComments("");
            tblTendererMaster.setTinDocName("");
            tblTendererMaster.setTinNo("");
            if (tendererMasterDtBean.getPhoneNo() != null && !tendererMasterDtBean.getPhoneNo().trim().equals("")) {
                tblTendererMaster.setPhoneNo(tendererMasterDtBean.getPhoneCode() + "-" + tendererMasterDtBean.getPhoneNo());
            }
            if (tendererMasterDtBean.getFaxNo() != null && !tendererMasterDtBean.getFaxNo().trim().equals("")) {
                tblTendererMaster.setFaxNo(tendererMasterDtBean.getFaxCode() + "-" + tendererMasterDtBean.getFaxNo());
            }
            tblTendererMaster.setMobileNo(/*tendererMasterDtBean.getMobileCode() + "-" + */tendererMasterDtBean.getMobileNo());
            if (userRegisterService.createUserByCompanyAdmin(tblLoginMaster, tblTendererMaster, userId)) {
                Object[] obj = userRegisterService.getCompanyAdminDetails(userId);
                if (sendMail(tblLoginMaster, password, obj)) {
                    ids = new int[2];
                    ids[0] = tblLoginMaster.getUserId();
                    ids[1] = tblTendererMaster.getTendererId();
                }
            }
        } catch (Exception e) {
            logger.error("createCompanyUser : " + logUserId + e);
        }
        logger.debug("createCompanyUser : " + logUserId + END);
        return ids;
    }

    /**
     * updating information into TblTendererMaster
     *
     * @param tendererMasterDtBean userId tendererId
     * @param userId
     * @param tendererId
     * @return true or false
     */
    public boolean updateCompanyUser(TendererMasterDtBean tendererMasterDtBean, int userId, int tendererId) {
        logger.debug("updateCompanyUser : " + logUserId + START);
        boolean flag = false;
        TblTendererMaster tblTendererMaster = _toTblTendererMaster(tendererMasterDtBean);
        tblTendererMaster.setTendererId(tendererId);
        tblTendererMaster.setLastName(tendererMasterDtBean.getLasatName());
        tblTendererMaster.setTblLoginMaster(new TblLoginMaster(userId));
        tblTendererMaster.setLasatName(null);
        tblTendererMaster.setNationality("");
        tblTendererMaster.setIsAdmin("no");
        tblTendererMaster.setSpecialization("");
        tblTendererMaster.setComments("");
        tblTendererMaster.setTinDocName("");
        tblTendererMaster.setTinNo("");
        if (tendererMasterDtBean.getPhoneNo() != null && !tendererMasterDtBean.getPhoneNo().trim().equals("")) {
            tblTendererMaster.setPhoneNo(tendererMasterDtBean.getPhoneCode() + "-" + tendererMasterDtBean.getPhoneNo());
        }
        if (tendererMasterDtBean.getFaxNo() != null && !tendererMasterDtBean.getFaxNo().trim().equals("")) {
            tblTendererMaster.setFaxNo(tendererMasterDtBean.getFaxCode() + "-" + tendererMasterDtBean.getFaxNo());
        }
        tblTendererMaster.setMobileNo(/*tendererMasterDtBean.getMobileCode() + "-" + */ tendererMasterDtBean.getMobileNo());
        tblTendererMaster.setTblCompanyMaster(new TblCompanyMaster(tendererMasterDtBean.getCompanyId()));
        if (userRegisterService.updateCompanyUser(tblTendererMaster, userId, tendererId, tendererMasterDtBean.getNationality())) {
            flag = true;
        }
        logger.debug("updateCompanyUser : " + logUserId + END);
        return flag;
    }

    /**
     * Sends mail to the Newly registered Bidder
     *
     * @param loginMaster new bidder data
     * @param password bidder password
     * @param obj
     * @return true or false for success or fail
     */
    private boolean sendMail(TblLoginMaster loginMaster, String password, Object[] obj) {
        logger.debug("sendMail : " + logUserId + START);
        boolean mailSent = false;
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            String[] mailTo = {loginMaster.getEmailId()};
            MailContentUtility mailContentUtility = new MailContentUtility();

            String mailSub = "e-GP: Your Account Details";
            String mailText = mailContentUtility.getLoginMailContentMultipleUser(loginMaster, password, obj);
            sendMessageUtil.setEmailTo(mailTo);
            sendMessageUtil.setEmailFrom(XMLReader.getMessage("regEmailId"));
            sendMessageUtil.setEmailSub(mailSub);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            mailSent = true;
        } catch (Exception ex) {
            logger.error("sendMail : " + logUserId + " " + ex);
        }
        logger.debug("sendMail : " + logUserId + END);
        return mailSent;
    }

    /**
     * Get TblLoginMaster by userid
     *
     * @param userId
     * @return information from TblLoginMaster
     */
    public Object[] getDetailsLogin(int userId) {
        logger.debug("getDetailsLogin : " + logUserId + START);
        Object[] objects = null;
        try {
            objects = userRegisterService.getLoginDetails(userId);
        } catch (Exception e) {
            logger.error("getDetailsLogin : " + logUserId + e);
        }
        logger.debug("getDetailsLogin : " + logUserId + END);
        return objects;
    }

    /**
     * Fetching information from TblTendererMaster by tendererId
     *
     * @param tendererId
     * @return information from TblTendererMaster
     */
    public List<TblTendererMaster> getDetailsMultipleUsers(int tendererId) {
        logger.debug("getDetailsMultipleUsers : " + logUserId + START);
        List<TblTendererMaster> list = new ArrayList<TblTendererMaster>();
        try {
            if (list.isEmpty()) {
                list = userRegisterService.getTendererDetails(tendererId);
            }
        } catch (Exception e) {
            logger.error("getDetailsMultipleUsers : " + logUserId + e);
        }
        logger.debug("getDetailsMultipleUsers : " + logUserId + END);
        return list;
    }

    /**
     * for suspend Company user
     *
     * @param userId activity activityBy status
     * @param activity
     * @param status suspend or resume
     * @param activityBy session userId
     * @return true or false
     */
    public boolean suspendUser(int userId, String activity, String activityBy, String status) {
        boolean flag = false;
        logger.debug("suspendUser : " + logUserId + START);
        try {
            TblTendererAuditTrail tblTendererAuditTrail = new TblTendererAuditTrail();
            tblTendererAuditTrail.setUserId(userId);
            tblTendererAuditTrail.setActivityDate(new Date());
            tblTendererAuditTrail.setActivity(activity);
            tblTendererAuditTrail.setActivityBy(activityBy);
            if (userRegisterService.updateUserStatus(tblTendererAuditTrail, userId, status)) {
                MailContentUtility mailContentUtility = new MailContentUtility();
                String mailSub = "e-GP: Your Account has been " + (status.equals("approved") ? "Resumed" : "Suspended");
                String mailText = mailContentUtility.bidderAccSuspendResume(status.equals("approved") ? "Resumed" : "Suspended");
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                sendMessageUtil.setEmailTo(new String[]{commonService.getEmailId(String.valueOf(userId))});
                sendMessageUtil.setEmailFrom(XMLReader.getMessage("regEmailId"));
                sendMessageUtil.setEmailSub(mailSub);
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                flag = true;
            }
        } catch (Exception e) {
            logger.error("suspendUser : " + logUserId + e);
        }
        logger.debug("suspendUser : " + logUserId + END);
        return flag;
    }

    /**
     * Registers JVCA company Details
     *
     * @param tempCompanyMasterDtBean to be inserted
     * @param userId
     * @param jvcaId
     * @return true or false for success or fail
     * @throws Exception
     */
    public boolean regJVCompanyDetail(TempCompanyMasterDtBean tempCompanyMasterDtBean, int userId, int jvcaId) throws Exception {
        logger.debug("regJVCompanyDetail : " + logUserId + START);
        TblCompanyMaster tblCompanyMaster = _toTblCompanyMaster(tempCompanyMasterDtBean);
        logger.debug("regJVCompanyDetail: " + logUserId + END);
        return userRegisterService.regJVCompanyDetail(tblCompanyMaster, userId, jvcaId);
    }

    /**
     * Updates JVCA Company Details
     *
     * @param companyMasterDtBean to be updated
     * @return true or false for success or fail
     */
    public boolean updateJVCompanyDetails(TempCompanyMasterDtBean companyMasterDtBean) {
        logger.debug("updateJVCompanyDetails : " + logUserId + START);
        TblCompanyMaster tblCompanyMaster = _toTblCompanyMaster(companyMasterDtBean);
        tblCompanyMaster.setTblLoginMaster(new TblLoginMaster(companyMasterDtBean.getUserId()));
        logger.debug("updateJVCompanyDetails : " + logUserId + END);
        return userRegisterService.updateJVCompanyDetails(tblCompanyMaster);
    }

    /**
     * Registers JVCA PersonalDetails of tenderer
     *
     * @param tendererMasterDtBean to be inserted
     * @param userid
     * @return true or false for success or fail
     * @throws Exception
     */
    public boolean regJVPersonalDetail(TendererMasterDtBean tendererMasterDtBean, int userid) throws Exception {
        logger.debug("regJVPersonalDetail : " + logUserId + START);
        tendererMasterDtBean.setComments("registration");
        tendererMasterDtBean.setIsAdmin("yes");
        tendererMasterDtBean.setSpecialization("");
        tendererMasterDtBean.setTinNo("");
        tendererMasterDtBean.setTinDocName("");
        TblTendererMaster tempTendererMaster = _toTblTendererMaster(tendererMasterDtBean);
        logger.debug("regJVPersonalDetail : " + logUserId + END);
        return userRegisterService.regJVPersonalDetail(tempTendererMaster, userid);
    }

    /**
     * Updates JVCA PersonalDetails of tenderer
     *
     * @param tempTendererMaster to be updated
     * @return true or false for success or fail
     */
    public boolean updateJVPersonalDetail(TendererMasterDtBean tempTendererMaster) {
        logger.debug("updateJVPersonalDetail : " + logUserId + START);
        TblTendererMaster tblTempTendererMaster = _toTblTendererMaster(tempTendererMaster);
        tblTempTendererMaster.setSpecialization("");
        tblTempTendererMaster.setTinNo("");
        tblTempTendererMaster.setTinDocName("");
        tblTempTendererMaster.setTblLoginMaster(new TblLoginMaster(tempTendererMaster.getUserId()));
        tblTempTendererMaster.setTblCompanyMaster(new TblCompanyMaster(tempTendererMaster.getCmpId()));
        logger.debug("updateJVPersonalDetail : " + logUserId + END);
        return userRegisterService.updateJVTendererDetail(tblTempTendererMaster);
    }

    /**
     * JVCA company finalsubmission
     *
     * @param userId finalsubmission to be done
     * @return true or false for success or fail
     */
    public boolean finalSubJVCompany(String userId) {
        logger.debug("finalSubJVCompany : " + logUserId + START);
        logger.debug("finalSubJVCompany : " + logUserId + END);
        List<Object[]> list = userRegisterService.jvCompMailDetails(userId);
        List<Object[]> data = new ArrayList<Object[]>();
        List<Object> mail = new ArrayList<Object>();
        String email = commonService.getEmailId(userId);
        Object jvName = null;
        String from = null;

        //0. lm.emailId ,1. jv.JVName,2. tm.firstName,3. tm.lastName,4. cm.companyId,5. cm.companyName,6. jp.isNominatedPartner,7. jp.jvRole,8. jv.jvAcceptRejStatus
        for (Object[] object : list) {
            jvName = object[1];
            if (object[6].toString().equalsIgnoreCase("Yes")) {
                from = object[0].toString();
            }

            mail.add(object[0]);

            if (object[4].toString().equals("1")) {
                Object[] obj = {object[2] + " " + object[3], object[7], object[8], object[6]};
                data.add(obj);
                obj = null;
            } else {
                Object[] obj = {object[5], object[7], object[8], object[6]};
                data.add(obj);
                obj = null;
            }
        }

        MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
        String msgText = msgBoxContentUtility.jvcaFinalSubMsgBox(email, jvName, data);
        for (Object object : mail) {
            userRegisterService.contentAdmMsgBox(object.toString(), from, "JVCA Registration Completed Successfully", msgText);
        }
        msgBoxContentUtility = null;
        return userRegisterService.updateUserStatus(userId, "approved");
    }
}
