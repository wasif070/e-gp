/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails;
import com.cptu.egp.eps.dao.storedprocedure.EvalCommonSearchData;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import com.cptu.egp.eps.service.serviceinterface.TenderService;
import com.cptu.egp.eps.service.serviceimpl.ContentAdminService;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.DateUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.text.DateFormat;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class TenderDetailsServlet extends HttpServlet
{

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * This method is used to get tender list for Tender Dashboard like all tender, my tenders, etc.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        Object userTypeid = session.getAttribute("userTypeId");
        boolean isShowLiveStatus = true;
        if(request.getParameter("h") != null && request.getParameter("h").equalsIgnoreCase("f"))
        {
            isShowLiveStatus = false;
        }
        //System.out.print("isShowLiveStatus "+isShowLiveStatus+ " request.getParameter(h) "+request.getParameter("h"));
        try {
            String funName = request.getParameter("funName");

            String strPageNo = (request.getParameter("pageNo")!=null ? request.getParameter("pageNo"):"");
            String isApprove = request.getParameter("approve");
            int pageNo =0;
            if(strPageNo!=null && !"".equalsIgnoreCase(strPageNo)){
                pageNo = Integer.parseInt(strPageNo);
            }
            
            String strOffset = request.getParameter("size");
            int recordOffset =0;
            if(strOffset!=null && !"".equalsIgnoreCase(strOffset)){
                recordOffset = Integer.parseInt(strOffset);
            }
            
            String styleClass = "";

            List<CommonTenderDetails> tenderDetailes = null;
            List<CommonTenderDetails> tenderDetailesOwnCategory = new ArrayList<CommonTenderDetails>();
            List<EvalCommonSearchData> tenderDashboardDetails=null;
            
            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            Date CurrentDate = new Date();
            if (funName.equalsIgnoreCase("AllTenders")) {
                boolean status = false;
                String pubDt = "", subDt = "";
                
                if(request.getParameter("homeWSearch") != null && "homeWSearch".equals(request.getParameter("homeWSearch"))){
                    status = true;
                }
                tenderDetailes = searchAllTenders(request);
                //out.print("<script type=\"text/javascript\"> SetCurrentDate(\""+ CurrentDate.getTime() +"\"); </script>");
                //out.print("<script type=\"text/javascript\"> setInterval(UpdateCurrentTimeByOneSec,1000); </script>");
                if (tenderDetailes != null && !tenderDetailes.isEmpty()) {
                    //System.out.println("Record Size: " + tenderDetailes.size());
                    int i = 0;
//                    out.print("<input type=\"hidden\" id=\"CurrentTime\" value=\"" + CurrentDate.getTime() + "\">");
                    for (; i < tenderDetailes.size(); i++) {
                        if(i%2==0){
                            styleClass = "bgColor-white";
                        }else{
                            styleClass = "bgColor-Green";
                        }
                        CommonTenderDetails tenderDetail = tenderDetailes.get(i);
                        
                        out.print("<tr class='"+styleClass+"'>");
                        out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * 10 + (i + 1)) + "</td>");
                        String tenderDtlStatus = "";
                        TenderService tenderService = (TenderService) AppContext.getSpringBean("TenderService");
                        ContentAdminService cas = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
                         long cntForCT = tenderService.getTenderCorriCnt(tenderDetail.getTenderId());
                         long cntForBeingP = tenderService.getBeingProcessCnt(tenderDetail.getTenderId());
                       // if(status){
//                            System.out.println("tenderID "+tenderDetail.getTenderId()+ "\n tenderDetail.getSubmissionDt()  "+tenderDetail.getSubmissionDt().getTime() +"\n tenderDetail.getTenderPubDt() "+tenderDetail.getTenderPubDt().getTime() +"\n Date "+new Date().getTime());
//                            System.out.println("tenderID " +tenderDetail.getTenderId()+ " Process Time "+(( new Date().getTime() > tenderDetail.getSubmissionDt().getTime() )&& (cntForBeingP == 0)));
//                            System.out.println("tenderID " +tenderDetail.getTenderId()+ " Live Time "+( (new Date().getTime() >= tenderDetail.getTenderPubDt().getTime()) && (new Date().getTime() < tenderDetail.getSubmissionDt().getTime())));
//                            System.out.println("tenderID " +tenderDetail.getTenderId()+ " STATUS "+tenderDetail.getTenderStatus());
//                            System.out.println("tenderID " +tenderDetail.getTenderId()+ " EVAL STATUS "+tenderDetail.getTenderevalstatus());
//                             System.out.println("***************tenderID " +tenderDetail.getTenderId()+ " Result "+((new Date().getTime() > tenderDetail.getSubmissionDt().getTime()) && (cntForBeingP == 0) &&("Cancelled".equals(tenderDetail.getTenderStatus()))));
                            //if(tenderDetail.getSubmissionDt().before(new Date())){
                            if( (new Date().getTime() >= tenderDetail.getTenderPubDt().getTime()) &&
                                 (new Date().getTime() < tenderDetail.getSubmissionDt().getTime() &&
                                 !("Cancelled".equalsIgnoreCase(tenderDetail.getTenderStatus().trim())))
                             ){
                                if(isShowLiveStatus)
                                {
                                    tenderDtlStatus = "<label style=\"color: red;font-weight: bold;\">Live</label>";
                                }
                                if(cntForCT != 0){
                                    tenderDtlStatus = "<span style=\"color: red;font-weight: bold;\"><blink>Amendment/Corrigendum issued : " + cntForCT + "</blink></span>";
                                }
                            }else if( (new Date().getTime() > tenderDetail.getSubmissionDt().getTime()) &&
                                        (cntForBeingP == 0) &&
                                        !("Cancelled".equalsIgnoreCase(tenderDetail.getTenderStatus().trim())) &&
                                        !("Re-Tendered".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) &&
                                        !("Rejected".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) &&
                                        !("Re-Tender".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) &&
                                        !("Awarded".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim()))
                              ){
                                    tenderDtlStatus = "<span style=\"color: red;font-weight: bold;\">Being processed</span>";
                            }  else if ("Re-Tender".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                tenderDtlStatus = "<span style=\"color: red;font-weight: bold;\"><blink>Re-Tendered</blink></span>";
                            }  else if ("Re-Tendered".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                tenderDtlStatus = "<span style=\"color: red;font-weight: bold;\"><blink>To be Re-Tendered</blink></span>";
                            }  else if ("Rejected".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                tenderDtlStatus = "<span style=\"color: red;font-weight: bold;\">Rejected</span>";
                            }  else if ("Awarded".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                tenderDtlStatus = "<span style=\"color: red;font-weight: bold;\"><blink>Contract Awarded</blink></span>";
                            }else if("Cancelled".equalsIgnoreCase(tenderDetail.getTenderStatus().trim())){
                                //tenderStatus = "<a style=\"color: red;\" onclick=\"javascript:window.open('"+request.getContextPath()+"/officer/ViewCancelTender.jsp?tenderid=" + tenderDetail.getTenderId() + "', '', 'width=350px,height=400px,scrollbars=1','');\" href='javascript:void(0);'>" + tenderDetail.getTenderStatus() + "</a>";
                                tenderDtlStatus = "<span style=\"color: red;font-weight: bold;\"><blink>Cancelled</blink></span>";
                                //out.print("<td class=\"t-align-left\">" + tenderDetail.getProcurementNature() + ",<br />" + viewLink  + tenderStatus + "</td>");
                            }
                        //}
                        /*else if(tenderDetail.getTenderStatus() != null){
                        out.print("<td class=\"t-align-left\">" + tenderDetail.getProcurementNature() + ",<br />" + viewLink + "</td>");
                        }*/
                        //tenderStatus = "<a onclick=\"javascript:window.open('"+request.getContextPath()+"/resources/common/ViewTender.jsp?id=" + tenderDetail.getTenderId() + "', '', 'resizable=yes,scrollbars=1','');\" href='#'>" + tenderDetail.getTenderBrief() + "</a>";
                        //if(status){
                            out.print("<td class=\"t-align-center\">" + tenderDetail.getTenderId() + ",<br/>" + tenderDetail.getReoiRfpRefNo() +  ",<br/>" + tenderDtlStatus + "</td>");
                        //}else{
                       //     out.print("<td class=\"t-align-center\">" + tenderDetail.getTenderId() + ",<br/>" + tenderDetail.getReoiRfpRefNo() + "</td>");
                        //}
                        //String viewLink = "<a href=\""+request.getContextPath()+"/resources/common/ViewTender.jsp?id=" + tenderDetail.getTenderId() + "\">" + tenderDetail.getTenderBrief() + "</a>";
                        String viewLink = "<a onclick=\"javascript:window.open('"+request.getContextPath()+"/resources/common/ViewNotice.jsp?id=" + tenderDetail.getTenderId() + "&h="+request.getParameter("h")+"', '', 'resizable=yes,scrollbars=1','');\" href='javascript:void(0);'><span id='tenderBrief_"+ i +"'>" + tenderDetail.getTenderBrief() + "</span></a>";
                        out.print("<td class=\"t-align-left\">" + tenderDetail.getProcurementNature() + ",<br />" + viewLink  + "</td>");
                        String procureType=""; //Edit by aprojit
                        StringBuilder deptSb=new StringBuilder();
                        String dept=tenderDetail.getMinistry();
                        deptSb.append(dept+",<br/> ");
                        dept=tenderDetail.getDivision();
                        if(dept!=null && !dept.isEmpty())
                            deptSb.append(dept +",<br/> ");
                        dept=tenderDetail.getAgency();
                        if(tenderDetail.getProcurementType().equalsIgnoreCase("NCT")) //aprojit-Start
                            procureType="NCB";
                        else if (tenderDetail.getProcurementType().equalsIgnoreCase("ICT"))
                            procureType="ICB";                                        //aprojit-End
                        if(dept!=null && !dept.isEmpty())
                            deptSb.append(dept +",<br/> ");
                        out.print("<td class=\"t-align-left\">" + deptSb.toString()+tenderDetail.getPeOfficeName() + "</td>");
                        out.print("<td class=\"t-align-center\">" + procureType + ",<br /> ");
                        
                        if(tenderDetail.getProcurementMethod().equalsIgnoreCase("RFQ"))
                            out.print("LEM</td>");
                        else if(tenderDetail.getProcurementMethod().equalsIgnoreCase("DPM"))
                            out.print("DCM</td>");
                        else
                            out.print(tenderDetail.getProcurementMethod() + "</td>");
                        
                        
                        Date dt = tenderDetail.getTenderPubDt();
                        
                        if (dt != null) {
                            pubDt = DateUtils.gridDateToStr(dt)+"$";
                        }
                        else {
                            pubDt = "N/A";
                        }
                        Date ClosingDate = new Date();
                        dt = tenderDetail.getSubmissionDt();
                        if (dt != null) {
                            subDt = DateUtils.gridDateToStr(dt)+"$";
                            ClosingDate = dt;
                        }
                        else {
                            subDt = "N/A";
                        }
                        Date OpeningDate = new Date();
                        dt = tenderDetail.getOpeningDt();
                        if (dt != null) {
                            subDt = DateUtils.gridDateToStr(dt)+"$";
                            OpeningDate = dt;
                        }
                        
                        //DateFormat dateFormat = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
                       

                        //System.out.println(dateFormat.format(date))
                        

                        out.print("<td class=\"t-align-center\">" + pubDt.substring(0, pubDt.lastIndexOf("$")-3) + " |<br />" + subDt.substring(0, subDt.lastIndexOf("$")-3) + "</td>");

                        Object objUsrTypeId = session.getAttribute("userTypeId");
                        Object objUserId = session.getAttribute("userId");
                        //Date SubmitDate = tenderDetail.getSubmissionDt();
                        
                        //System.out.println(" test**************** "+cas.getUserApprovedTenderId(objUserId.toString()).toString());
                        //if ((objUsrTypeId != null && objUsrTypeId.toString().equals("2")) && "true".equalsIgnoreCase(isApprove)) {
                        if ((objUsrTypeId != null && objUsrTypeId.toString().equals("2")) && "Approved".equalsIgnoreCase(tenderDetail.getTenderStatus()) && "[Approved]".equalsIgnoreCase(cas.getUserApprovedTenderId(objUserId.toString()).toString())) {
                            String dashBoardLink = request.getContextPath() + "/tenderer/TendererDashboard.jsp?tenderid=" + tenderDetail.getTenderId();
                            String imgLink = request.getContextPath() + "/resources/images/Dashboard/dashBoardIcn.png";
                              
//                            long Day =0, Hour =0, Minute =0, Seconds=0;
//                            long diff = ClosingDate.getTime() - CurrentDate.getTime();
//                            if(diff > 0)
//                            {
//                                long DifferenceInSeconds = diff/1000;
//                                Day = DifferenceInSeconds/(60*60*24);
//                                DifferenceInSeconds = DifferenceInSeconds % (60*60*24);
//                                Hour = DifferenceInSeconds/(60*60);
//                                DifferenceInSeconds = DifferenceInSeconds % (60*60);
//                                Minute = DifferenceInSeconds/(60);
//                                Seconds = DifferenceInSeconds % (60);
//                            }
                            if(request.getParameter("viewType").equalsIgnoreCase("Archive"))
                            {
                                out.print("<td class=\"t-align-center\"><a href=\"" + dashBoardLink + "\" title=\"Dashboard\"><img src=\"" + imgLink + "\" alt=\"Dashboard\" /></a><br/><br/><span style=\"color: #483D8B;\" id=\"TenderOpeningTimer"+ i +"\"></span></td>");
                                out.print("<script type=\"text/javascript\"> clearInterval(ShowOpeningTimerVar"+ i +"); </script>");
                                out.print("<script type=\"text/javascript\"> var ShowOpeningTimerVar"+ i +" = setInterval( function(){ TimelineShow(document.getElementById(\"TenderOpeningTimer"+ i +"\"),\""+ OpeningDate.getTime() +"\"); },1000); </script>");
                            }
                            else
                            {
                                out.print("<td class=\"t-align-center\"><a href=\"" + dashBoardLink + "\" title=\"Dashboard\"><img src=\"" + imgLink + "\" alt=\"Dashboard\" /></a><br/><br/><span style=\"color: #CD5C5C\" id=\"TenderSubmissionTimer"+ i +"\"></span><br/><br/><span style=\"color: #483D8B;\" id=\"TenderOpeningTimer"+ i +"\"></span></td>");
                                out.print("<script type=\"text/javascript\"> clearInterval(ShowTimerVar"+ i +"); </script>");
                                out.print("<script type=\"text/javascript\"> var ShowTimerVar"+ i +" = setInterval( function(){ TimelineShow(document.getElementById(\"TenderSubmissionTimer"+ i +"\"),\""+ ClosingDate.getTime() +"\"); },1000); </script>");
                                out.print("<script type=\"text/javascript\"> clearInterval(ShowOpeningTimerVar"+ i +"); </script>");
                                out.print("<script type=\"text/javascript\"> var ShowOpeningTimerVar"+ i +" = setInterval( function(){ TimelineShow(document.getElementById(\"TenderOpeningTimer"+ i +"\"),\""+ OpeningDate.getTime() +"\"); },1000); </script>");
                            }
                            
                                
                        }   
                        else if(objUsrTypeId != null && objUsrTypeId.toString().equals("2") && "Cancelled".equalsIgnoreCase(tenderDetail.getTenderStatus().trim()))
                        {
                          out.print("<td class=\"t-align-center\"><span style=\"color: red;font-weight: bold;\"><blink>Cancelled</blink></span></td>");
                        }

                        out.print("</tr>");
                        
                    }
                    
                    out.print("<script type=\"text/javascript\"> SetCurrentDate(\""+ CurrentDate.getTime() +"\"); </script>");
                    out.print("<script type=\"text/javascript\"> clearInterval(IntervalVar); </script>");
                    out.print("<script type=\"text/javascript\"> var IntervalVar = setInterval(UpdateCurrentTimeByOneSec,1000); </script>");
                    int totalPages = 0;
                    if (tenderDetailes.size() > 0) {
                        totalPages = (int) (Math.ceil(Math.ceil(tenderDetailes.get(0).getTotalRecords()) / recordOffset));
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    
                    out.print("<input type=\"hidden\" id=\"cntTenBrief\" value=\"" + i + "\">");
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                    
                    
                }
                else {
                    out.print("<tr>");
                    out.print("<td colspan=\"7\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No records found</td>");
                    out.print("</tr>");
                }

            }
            
            if (funName.equalsIgnoreCase("ownCategory")) {
                boolean status = false;
                String pubDt = "", subDt = "";
                
                if(request.getParameter("homeWSearch") != null && "homeWSearch".equals(request.getParameter("homeWSearch"))){
                    status = true;
                }
                
                tenderDetailes = searchAllTenders(request);
                //out.print("<script type=\"text/javascript\"> SetCurrentDate(\""+ CurrentDate.getTime() +"\"); </script>");
                //out.print("<script type=\"text/javascript\"> setInterval(UpdateCurrentTimeByOneSec,1000); </script>");
                if (tenderDetailes != null && !tenderDetailes.isEmpty()) {
                    //System.out.println("Record Size: " + tenderDetailes.size());
                    int i = 0;
//                    out.print("<input type=\"hidden\" id=\"CurrentTime\" value=\"" + CurrentDate.getTime() + "\">");
                    for (; i < tenderDetailes.size(); i++) {
                        CommonTenderDetails tenderDetail = tenderDetailes.get(i);
                        
                        if(request.getParameter("from")!= null || request.getParameter("from")!= "null" ){
                            if(request.getParameter("from").equalsIgnoreCase("ownCategory")){
                                boolean isRightTender = false;
                                String userId = request.getParameter("userid");
                                String tenderId = String.valueOf(tenderDetail.getTenderId());
                                List<SPTenderCommonData> lstUserBidPermision = tenderCommonService.returndata("getUserBidPermissionDetails", tenderId, userId);
                                List<SPTenderCommonData> lstAppPermision = tenderCommonService.returndata("getAppPermissionDetails", tenderId, userId);
                                List<SPTenderCommonData> ServicesType = tenderCommonService.returndata("getServicesType", tenderId, userId);

                                if((!lstAppPermision.isEmpty() && lstAppPermision.size()>0) && (!lstUserBidPermision.isEmpty() && lstUserBidPermision.size()>0) && !tenderDetail.getProcurementNature().equalsIgnoreCase("Goods")){
                                    for(SPTenderCommonData appPermission : lstAppPermision){
                                        String workType = appPermission.getFieldName1();
                                        String workCategory = appPermission.getFieldName2();

                                        for(SPTenderCommonData bidPermission : lstUserBidPermision){
                                            if(tenderDetail.getProcurementNature().equalsIgnoreCase(bidPermission.getFieldName1()) && workType.equalsIgnoreCase(bidPermission.getFieldName2()) && workCategory.equalsIgnoreCase(bidPermission.getFieldName3())){
                                                   isRightTender = true;
                                                   break;

                                            }
                                        }
                                        if(isRightTender) break;
                                    }
                                }else if(tenderDetail.getProcurementNature().equalsIgnoreCase("Goods")){
                                    for(SPTenderCommonData bidPermission : lstUserBidPermision){
                                            if(tenderDetail.getProcurementNature().equalsIgnoreCase(bidPermission.getFieldName1())){
                                                   isRightTender = true;
                                                   break;

                                            }
                                        }
                                }else if(tenderDetail.getProcurementNature().equalsIgnoreCase("Services")){
                                    for(SPTenderCommonData bidPermission : lstUserBidPermision){
                                            if(tenderDetail.getProcurementNature().equalsIgnoreCase(bidPermission.getFieldName1()) && ServicesType.get(0).getFieldName1().equalsIgnoreCase(bidPermission.getFieldName2())){
                                                   isRightTender = true;
                                                   break;
                                            }
                                        }
                                }
                                if(!isRightTender){
                                   continue;
                                } 
                            }
                         }
                        
                        tenderDetailesOwnCategory.add(tenderDetail);
                      }
                    }
                   
                if(tenderDetailesOwnCategory != null && !tenderDetailesOwnCategory.isEmpty()){
                        
                        int i = 0, j;
                        int RecordFrom = (pageNo-1) * recordOffset;
                        for (j=RecordFrom; j < tenderDetailesOwnCategory.size() && j < RecordFrom + recordOffset; i++,j++) {
                        
                        if(j%2==0){
                            styleClass = "bgColor-white";
                        }else{
                            styleClass = "bgColor-Green";
                        }
                        CommonTenderDetails tenderDetail = tenderDetailesOwnCategory.get(j);
                        
                        out.print("<tr class='"+styleClass+"'>");
                        out.print("<td class=\"t-align-center\">" + (j+1) + "</td>");
                        String tenderDtlStatus = "";
                        TenderService tenderService = (TenderService) AppContext.getSpringBean("TenderService");
                        ContentAdminService cas = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
                         long cntForCT = tenderService.getTenderCorriCnt(tenderDetail.getTenderId());
                         long cntForBeingP = tenderService.getBeingProcessCnt(tenderDetail.getTenderId());
                       // if(status){
//                            System.out.println("tenderID "+tenderDetail.getTenderId()+ "\n tenderDetail.getSubmissionDt()  "+tenderDetail.getSubmissionDt().getTime() +"\n tenderDetail.getTenderPubDt() "+tenderDetail.getTenderPubDt().getTime() +"\n Date "+new Date().getTime());
//                            System.out.println("tenderID " +tenderDetail.getTenderId()+ " Process Time "+(( new Date().getTime() > tenderDetail.getSubmissionDt().getTime() )&& (cntForBeingP == 0)));
//                            System.out.println("tenderID " +tenderDetail.getTenderId()+ " Live Time "+( (new Date().getTime() >= tenderDetail.getTenderPubDt().getTime()) && (new Date().getTime() < tenderDetail.getSubmissionDt().getTime())));
//                            System.out.println("tenderID " +tenderDetail.getTenderId()+ " STATUS "+tenderDetail.getTenderStatus());
//                            System.out.println("tenderID " +tenderDetail.getTenderId()+ " EVAL STATUS "+tenderDetail.getTenderevalstatus());
//                             System.out.println("***************tenderID " +tenderDetail.getTenderId()+ " Result "+((new Date().getTime() > tenderDetail.getSubmissionDt().getTime()) && (cntForBeingP == 0) &&("Cancelled".equals(tenderDetail.getTenderStatus()))));
                            //if(tenderDetail.getSubmissionDt().before(new Date())){
                            if( (new Date().getTime() >= tenderDetail.getTenderPubDt().getTime()) &&
                                 (new Date().getTime() < tenderDetail.getSubmissionDt().getTime() &&
                                 !("Cancelled".equalsIgnoreCase(tenderDetail.getTenderStatus().trim())))
                             ){
                                if(isShowLiveStatus)
                                {
                                    tenderDtlStatus = "<label style=\"color: red;font-weight: bold;\">Live</label>";
                                }
                                if(cntForCT != 0){
                                    tenderDtlStatus = "<span style=\"color: red;font-weight: bold;\"><blink>Amendment/Corrigendum issued : " + cntForCT + "</blink></span>";
                                }
                            }else if( (new Date().getTime() > tenderDetail.getSubmissionDt().getTime()) &&
                                        (cntForBeingP == 0) &&
                                        !("Cancelled".equalsIgnoreCase(tenderDetail.getTenderStatus().trim())) &&
                                        !("Re-Tendered".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) &&
                                        !("Rejected".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) &&
                                        !("Re-Tender".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) &&
                                        !("Awarded".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim()))
                              ){
                                    tenderDtlStatus = "<span style=\"color: red;font-weight: bold;\">Being processed</span>";
                            }  else if ("Re-Tender".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                tenderDtlStatus = "<span style=\"color: red;font-weight: bold;\"><blink>Re-Tendered</blink></span>";
                            }  else if ("Re-Tendered".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                tenderDtlStatus = "<span style=\"color: red;font-weight: bold;\"><blink>To be Re-Tendered</blink></span>";
                            }  else if ("Rejected".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                tenderDtlStatus = "<span style=\"color: red;font-weight: bold;\">Rejected</span>";
                            }  else if ("Awarded".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                tenderDtlStatus = "<span style=\"color: red;font-weight: bold;\"><blink>Contract Awarded</blink></span>";
                            }else if("Cancelled".equalsIgnoreCase(tenderDetail.getTenderStatus().trim())){
                                //tenderStatus = "<a style=\"color: red;\" onclick=\"javascript:window.open('"+request.getContextPath()+"/officer/ViewCancelTender.jsp?tenderid=" + tenderDetail.getTenderId() + "', '', 'width=350px,height=400px,scrollbars=1','');\" href='javascript:void(0);'>" + tenderDetail.getTenderStatus() + "</a>";
                                tenderDtlStatus = "<span style=\"color: red;font-weight: bold;\"><blink>Cancelled</blink></span>";
                                //out.print("<td class=\"t-align-left\">" + tenderDetail.getProcurementNature() + ",<br />" + viewLink  + tenderStatus + "</td>");
                            }
                        //}
                        /*else if(tenderDetail.getTenderStatus() != null){
                        out.print("<td class=\"t-align-left\">" + tenderDetail.getProcurementNature() + ",<br />" + viewLink + "</td>");
                        }*/
                        //tenderStatus = "<a onclick=\"javascript:window.open('"+request.getContextPath()+"/resources/common/ViewTender.jsp?id=" + tenderDetail.getTenderId() + "', '', 'resizable=yes,scrollbars=1','');\" href='#'>" + tenderDetail.getTenderBrief() + "</a>";
                        //if(status){
                            out.print("<td class=\"t-align-center\">" + tenderDetail.getTenderId() + ",<br/>" + tenderDetail.getReoiRfpRefNo() +  ",<br/>" + tenderDtlStatus + "</td>");
                        //}else{
                       //     out.print("<td class=\"t-align-center\">" + tenderDetail.getTenderId() + ",<br/>" + tenderDetail.getReoiRfpRefNo() + "</td>");
                        //}
                        //String viewLink = "<a href=\""+request.getContextPath()+"/resources/common/ViewTender.jsp?id=" + tenderDetail.getTenderId() + "\">" + tenderDetail.getTenderBrief() + "</a>";
                        String viewLink = "<a onclick=\"javascript:window.open('"+request.getContextPath()+"/resources/common/ViewNotice.jsp?id=" + tenderDetail.getTenderId() + "&h="+request.getParameter("h")+"', '', 'resizable=yes,scrollbars=1','');\" href='javascript:void(0);'><span id='tenderBrief_"+ j +"'>" + tenderDetail.getTenderBrief() + "</span></a>";
                        out.print("<td class=\"t-align-left\">" + tenderDetail.getProcurementNature() + ",<br />" + viewLink  + "</td>");
                        String procureType=""; //Edit by aprojit
                        StringBuilder deptSb=new StringBuilder();
                        String dept=tenderDetail.getMinistry();
                        deptSb.append(dept+",<br/> ");
                        dept=tenderDetail.getDivision();
                        if(dept!=null && !dept.isEmpty())
                            deptSb.append(dept +",<br/> ");
                        dept=tenderDetail.getAgency();
                        if(tenderDetail.getProcurementType().equalsIgnoreCase("NCT")) //aprojit-Start
                            procureType="NCB";
                        else if (tenderDetail.getProcurementType().equalsIgnoreCase("ICT"))
                            procureType="ICB";                                        //aprojit-End
                        if(dept!=null && !dept.isEmpty())
                            deptSb.append(dept +",<br/> ");
                        out.print("<td class=\"t-align-left\">" + deptSb.toString()+tenderDetail.getPeOfficeName() + "</td>");
                        out.print("<td class=\"t-align-center\">" + procureType + ",<br /> ");
                        
                        if(tenderDetail.getProcurementMethod().equalsIgnoreCase("RFQ"))
                            out.print("LEM</td>");
                        else if(tenderDetail.getProcurementMethod().equalsIgnoreCase("DPM"))
                            out.print("DCM</td>");
                        else
                            out.print(tenderDetail.getProcurementMethod() + "</td>");
                        
                        
                        Date dt = tenderDetail.getTenderPubDt();
                        
                        if (dt != null) {
                            pubDt = DateUtils.gridDateToStr(dt)+"$";
                        }
                        else {
                            pubDt = "N/A";
                        }
                        Date ClosingDate = new Date();
                        dt = tenderDetail.getSubmissionDt();
                        if (dt != null) {
                            subDt = DateUtils.gridDateToStr(dt)+"$";
                            ClosingDate = dt;
                        }
                        else {
                            subDt = "N/A";
                        }
                        
                        Date OpeningDate = new Date();
                        dt = tenderDetail.getOpeningDt();
                        if (dt != null) {
                            subDt = DateUtils.gridDateToStr(dt)+"$";
                            OpeningDate = dt;
                        }
                        //DateFormat dateFormat = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
                       

                        //System.out.println(dateFormat.format(date))
                        

                        out.print("<td class=\"t-align-center\">" + pubDt.substring(0, pubDt.lastIndexOf("$")-3) + " |<br />" + subDt.substring(0, subDt.lastIndexOf("$")-3) + "</td>");

                        Object objUsrTypeId = session.getAttribute("userTypeId");
                        Object objUserId = session.getAttribute("userId");
                        //Date SubmitDate = tenderDetail.getSubmissionDt();
                        
                        //System.out.println(" test**************** "+cas.getUserApprovedTenderId(objUserId.toString()).toString());
                        //if ((objUsrTypeId != null && objUsrTypeId.toString().equals("2")) && "true".equalsIgnoreCase(isApprove)) {
                        if ((objUsrTypeId != null && objUsrTypeId.toString().equals("2")) && "Approved".equalsIgnoreCase(tenderDetail.getTenderStatus()) && "[Approved]".equalsIgnoreCase(cas.getUserApprovedTenderId(objUserId.toString()).toString())) {
                            String dashBoardLink = request.getContextPath() + "/tenderer/TendererDashboard.jsp?tenderid=" + tenderDetail.getTenderId();
                            String imgLink = request.getContextPath() + "/resources/images/Dashboard/dashBoardIcn.png";
                              
//                            long Day =0, Hour =0, Minute =0, Seconds=0;
//                            long diff = ClosingDate.getTime() - CurrentDate.getTime();
//                            if(diff > 0)
//                            {
//                                long DifferenceInSeconds = diff/1000;
//                                Day = DifferenceInSeconds/(60*60*24);
//                                DifferenceInSeconds = DifferenceInSeconds % (60*60*24);
//                                Hour = DifferenceInSeconds/(60*60);
//                                DifferenceInSeconds = DifferenceInSeconds % (60*60);
//                                Minute = DifferenceInSeconds/(60);
//                                Seconds = DifferenceInSeconds % (60);
//                            }
                            if(request.getParameter("viewType").equalsIgnoreCase("Archive")){
                                out.print("<td class=\"t-align-center\"><a href=\"" + dashBoardLink + "\" title=\"Dashboard\"><img src=\"" + imgLink + "\" alt=\"Dashboard\" /></a><br/><br/><span style=\"color: #483D8B;\" id=\"TenderOpeningTimer"+ i +"\"></span></td>");
                                out.print("<script type=\"text/javascript\"> clearInterval(ShowOpeningTimerVar"+ i +"); </script>");
                                out.print("<script type=\"text/javascript\"> var ShowOpeningTimerVar"+ i +" = setInterval( function(){ TimelineShow(document.getElementById(\"TenderOpeningTimer"+ i +"\"),\""+ OpeningDate.getTime() +"\"); },1000); </script>");
                            }
                            else
                            {
                                out.print("<td class=\"t-align-center\"><a href=\"" + dashBoardLink + "\" title=\"Dashboard\"><img src=\"" + imgLink + "\" alt=\"Dashboard\" /></a><br/><br/><span style=\"color: #CD5C5C\" id=\"TenderSubmissionTimer"+ i +"\"></span><br/><br/><span style=\"color: #483D8B;\" id=\"TenderOpeningTimer"+ i +"\"></span></td>");
                                out.print("<script type=\"text/javascript\"> clearInterval(ShowTimerVar"+ i +"); </script>");
                                out.print("<script type=\"text/javascript\"> var ShowTimerVar"+ i +" = setInterval( function(){ TimelineShow(document.getElementById(\"TenderSubmissionTimer"+ i +"\"),\""+ ClosingDate.getTime() +"\"); },1000); </script>");
                                out.print("<script type=\"text/javascript\"> clearInterval(ShowOpeningTimerVar"+ i +"); </script>");
                                out.print("<script type=\"text/javascript\"> var ShowOpeningTimerVar"+ i +" = setInterval( function(){ TimelineShow(document.getElementById(\"TenderOpeningTimer"+ i +"\"),\""+ OpeningDate.getTime() +"\"); },1000); </script>");
                            }
                            
                                
                        }   
                        else if(objUsrTypeId != null && objUsrTypeId.toString().equals("2") && "Cancelled".equalsIgnoreCase(tenderDetail.getTenderStatus().trim()))
                        {
                          out.print("<td class=\"t-align-center\"><span style=\"color: red;font-weight: bold;\"><blink>Cancelled</blink></span></td>");
                        }

                        out.print("</tr>");
                        
                        }
                    
                    out.print("<script type=\"text/javascript\"> SetCurrentDate(\""+ CurrentDate.getTime() +"\"); </script>");
                    out.print("<script type=\"text/javascript\"> clearInterval(IntervalVar); </script>");
                    out.print("<script type=\"text/javascript\"> var IntervalVar = setInterval(UpdateCurrentTimeByOneSec,1000); </script>");
                    int totalPages = 0;
                    if (tenderDetailesOwnCategory.size() > 0) {
                        totalPages = (int) (Math.ceil(Math.ceil(tenderDetailesOwnCategory.size()) / recordOffset));
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    
                    out.print("<input type=\"hidden\" id=\"cntTenBrief\" value=\"" + i + "\">");
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                    
                    
                }
                else {
                    out.print("<tr>");
                    out.print("<td colspan=\"7\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No records found</td>");
                    out.print("</tr>");
                }

            }

             // start bidder Dashboard added by Sristy 15.02.2016
             if (funName.equalsIgnoreCase("TendererDashboard")) {
                boolean status = false;
                if(request.getParameter("homeWSearch") != null && "homeWSearch".equals(request.getParameter("homeWSearch"))){
                    status = true;
                }
                tenderDashboardDetails = TendererDashboard(request);
                System.out.println("Dash board Record Size: " + tenderDashboardDetails.size());
                String printLine="";
                if (tenderDashboardDetails != null && !tenderDashboardDetails.isEmpty()) {
                    int i = 0;
                    for (; i < tenderDashboardDetails.size(); i++) {
                        if(i%2==0){
                            styleClass = "white";
                        }else{
                            styleClass = "Green";
                        }
                        EvalCommonSearchData tenderDetail = tenderDashboardDetails.get(i);
                        printLine="<tr bgColor=\""+styleClass+"\">";
                        System.out.println(printLine);
                        out.print("<tr bgColor=\"" + styleClass + "\">");
                        out.print("<td class=\"t-align-center\"  width=\"7%\">" + ((pageNo - 1) * 10 + (i + 1)) + "</td>");
                        //System.out.println("pageNo: " + ((pageNo - 1) * 10 + (i + 1)) +"  //I: "+i);
                        String tenderDtlStatus = "";
                        TenderService tenderService = (TenderService) AppContext.getSpringBean("TenderService");
                        ContentAdminService cas = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
                         long cntForCT = tenderService.getTenderCorriCnt(Integer.parseInt(tenderDetail.getFieldName1()));
                         long cntForBeingP = tenderService.getBeingProcessCnt(Integer.parseInt(tenderDetail.getFieldName1()));

                                if(isShowLiveStatus)
                                {
                                    tenderDtlStatus = "<label style=\"color: red;font-weight: bold;\">Pending</label>";
                                }
                                if(cntForCT != 0){
                                    tenderDtlStatus = "<span style=\"color: red;font-weight: bold;\"><blink>Amendment/Corrigendum issued : " + cntForCT + "</blink></span>";
                                }
                           
                        //tenderDtlStatus = "<span style=\"color: red;font-weight: bold;\"><blink>Test for Dashboard</blink></span>";
                        //out.print("<td class=\"t-align-center\">" + tenderDetail.getFieldName1() + ",<br/>" + tenderDetail.getFieldName2()+  ",<br/>" + tenderDtlStatus + "</td>");
                        out.print("<td class=\"t-align-center\" width=\"10%\">" + tenderDetail.getFieldName1() + "</td>");
                        String viewLink = "<a onclick=\"javascript:window.open('"+request.getContextPath()+"/resources/common/ViewNotice.jsp?id=" + tenderDetail.getFieldName1() + "&h="+request.getParameter("h")+"', '', 'resizable=yes,scrollbars=1','');\" href='javascript:void(0);'><span id='tenderBrief_"+ i +"'>" + tenderDetail.getFieldName4() + "</span></a>";
                        out.print("<td class=\"t-align-left\" width=\"39%\">" + tenderDetail.getFieldName3() + ",<br />" + viewLink  + "</td>");

                        //StringBuilder deptSb=new StringBuilder();
                        //String dept=tenderDetail.getFieldName5();
                        //deptSb.append(dept+",<br/> ");
                        //dept=tenderDetail.getFieldName6();
                       // if(dept!=null && !dept.isEmpty())
                            //deptSb.append(dept +",<br/> ");
                        //dept=tenderDetail.getFieldName7();
                        //if(dept!=null && !dept.isEmpty())
                            //deptSb.append(dept +",<br/> ");
                        //out.print("<td class=\"t-align-left\">" + deptSb.toString()+tenderDetail.getFieldName8() + "</td>");
                        String ProcType = tenderDetail.getFieldName9();
                        if (ProcType.equalsIgnoreCase("NCT"))
                                out.print("<td class=\"t-align-center\" width=\"10%\">" + "NCB" + ", ");
                        else        
                            out.print("<td class=\"t-align-center\" width=\"10%\">" + "ICB" + ", ");
                        
                        String ProcMethod = tenderDetail.getFieldName10();
                        if (ProcMethod.equalsIgnoreCase("RFQ"))
                                out.print("LEM</td>");
                        else if (ProcMethod.equalsIgnoreCase("DPM"))
                                out.print("DCM</td>");
                        else        
                            out.print(tenderDetail.getFieldName10() + "</td>");
                        
                        String pubDt = "", subDt = "";
                        pubDt = tenderDetail.getFieldName11();
                        subDt = tenderDetail.getFieldName12();
                        

                        out.print("<td class=\"t-align-center\" width=\"12%\">" + pubDt + "</td>");
                         out.print("<td class=\"t-align-center\" width=\"12%\">" + subDt + "</td>");
                        //out.print("<td class=\"t-align-center\">" + pubDt.substring(0, pubDt.lastIndexOf("$")-3) + ",<br />" + subDt.substring(0, subDt.lastIndexOf("$")-3) + "</td>");
                        Object objUsrTypeId = session.getAttribute("userTypeId");
                        Object objUserId = session.getAttribute("userId");
                        
                       // if ((objUsrTypeId != null && objUsrTypeId.toString().equals("2")) && "Approved".equalsIgnoreCase(tenderDetail.getFieldName1()) && "[Approved]".equalsIgnoreCase(cas.getUserApprovedTenderId(objUserId.toString()).toString())) {
                       //     String dashBoardLink = request.getContextPath() + "/tenderer/TendererDashboard.jsp?tenderid=" + tenderDetail.getFieldName1();
                        //    String imgLink = request.getContextPath() + "/resources/images/Dashboard/dashBoardIcn.png";
                        //    out.print("<td class=\"t-align-center\"><a href=\"" + dashBoardLink + "\" title=\"Dashboard\"><img src=\"" + imgLink + "\" alt=\"Dashboard\" /></a></td>");
                       // }

                        out.print("</tr>");
                    }
                    int totalPages = 0;
                    if (tenderDashboardDetails.size() > 0) {
                        totalPages = (int) (Math.ceil(Math.ceil(Integer.parseInt(tenderDashboardDetails.get(0).getFieldName14())) / recordOffset));
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"cntTenBrief\" value=\"" + i + "\">");
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                }
                else {
                    out.print("<tr>");

                    out.print("<td colspan=\"7\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No records found</td>");
                    out.print("</tr>");
                }

            }
            //end bidder dashboard

            else if (funName.equalsIgnoreCase("MyTenders")) {
                tenderDetailes = searchMyTenders(request);
                System.out.print(("My tender size: "+tenderDetailes.size()));
                if (tenderDetailes != null && !tenderDetailes.isEmpty()) {
                    int i = 0;
                    for (; i < tenderDetailes.size(); i++) {
                        if(i%2==0){
                            styleClass = "bgColor-white";
                        }else{
                            styleClass = "bgColor-Green";
                        }
                        CommonTenderDetails tenderDetail = tenderDetailes.get(i);
                        out.print("<tr class='"+styleClass+"'>");
                        out.print("<td width=\"5%\" class=\"t-align-center\">" + ((pageNo - 1) * 10 + (i + 1)) + "</td>");
                        String tenderDtlStatus = "";
                        TenderService tenderService = (TenderService) AppContext.getSpringBean("TenderService");
                        long cntForCT = tenderService.getTenderCorriCnt(tenderDetail.getTenderId());
                        long cntForBeingP = tenderService.getBeingProcessCnt(tenderDetail.getTenderId());
//                        System.out.println("********************* ELSE *********************");
//                        System.out.println("tenderID "+tenderDetail.getTenderId()+ "\n tenderDetail.getSubmissionDt()  "+tenderDetail.getSubmissionDt().getTime() +"\n tenderDetail.getTenderPubDt() "+tenderDetail.getTenderPubDt().getTime() +"\n Date "+new Date().getTime());
//                        System.out.println("tenderID " +tenderDetail.getTenderId()+ " Process Time "+(( new Date().getTime() > tenderDetail.getSubmissionDt().getTime() )&& (cntForBeingP == 0)));
//                        System.out.println("tenderID " +tenderDetail.getTenderId()+ " Live Time "+( (new Date().getTime() >= tenderDetail.getTenderPubDt().getTime()) && (new Date().getTime() < tenderDetail.getSubmissionDt().getTime())));
//                        System.out.println("tenderID " +tenderDetail.getTenderId()+ " STATUS "+tenderDetail.getTenderStatus());
//                        System.out.println("tenderID " +tenderDetail.getTenderId()+ " EVAL STATUS "+tenderDetail.getTenderevalstatus());
//                        System.out.println("***************tenderID " +tenderDetail.getTenderId()+ " Result "+((new Date().getTime() > tenderDetail.getSubmissionDt().getTime()) && (cntForBeingP == 0) &&("Cancelled".equals(tenderDetail.getTenderStatus()))));
                        if(tenderDetail.getTenderPubDt() != null && tenderDetail.getSubmissionDt()!=null && !"pending".equalsIgnoreCase(tenderDetail.getTenderStatus()))
                        {
                                 if (userTypeid.toString().equals("3"))
                                {
                                    if( (new Date().getTime() >= tenderDetail.getTenderPubDt().getTime()) &&
                                         (new Date().getTime() < tenderDetail.getSubmissionDt().getTime() && !("Cancelled".equalsIgnoreCase(tenderDetail.getTenderStatus().trim())))
                                     ){
                                        if(cntForCT != 0){
                                            tenderDtlStatus = "Amendment/Corrigendum issued : " + cntForCT ;
                                        }
                                    } else if("Cancelled".equalsIgnoreCase(tenderDetail.getTenderStatus().trim())){
                                        //tenderStatus = "<a style=\"color: red;\" onclick=\"javascript:window.open('"+request.getContextPath()+"/officer/ViewCancelTender.jsp?tenderid=" + tenderDetail.getTenderId() + "', '', 'width=350px,height=400px,scrollbars=1','');\" href='javascript:void(0);'>" + tenderDetail.getTenderStatus() + "</a>";
                                        tenderDtlStatus = "Cancelled";
                                        //out.print("<td class=\"t-align-left\">" + tenderDetail.getProcurementNature() + ",<br />" + viewLink  + tenderStatus + "</td>");
                                    }  else if ("Re-Tendered".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                        tenderDtlStatus = "To be Re-Tendered";
                                    }  else if ("Re-Tender".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                        tenderDtlStatus = "Re-Tendered";
                                    }  else if ("Rejected".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                        tenderDtlStatus = "Rejected";
                                    }  else if ("Awarded".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                        tenderDtlStatus = "Contract Awarded";
                                    }  else if ("Approved".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                        tenderDtlStatus = "Approved";
                                    }  else if ("NOA declined".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                        tenderDtlStatus = "NOA declined";
                                    }  else if ("Being evaluated".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                        tenderDtlStatus = "Being evaluated";
                                    }  else if ("Being Re-evaluated".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                        tenderDtlStatus = "Being Re-evaluated";
                                    }  else if ("Clarification Requested".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                        tenderDtlStatus = "Clarification Requested";
                                    }  else if ("Opening Completed".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                        tenderDtlStatus = "Opening Completed";
                                    }else if( (new Date().getTime() > tenderDetail.getSubmissionDt().getTime()) &&
                                                (cntForBeingP == 0)
                                                //&& !("Cancelled".equalsIgnoreCase(commonTenderDetails.getTenderStatus().trim())) &&
                                                //!("Re-Tendered".equalsIgnoreCase(commonTenderDetails.getTenderevalstatus().trim()))
                                      ){
                                            tenderDtlStatus = "Being processed";
                                      }
                                }
                                else
                                {
                                    if( (new Date().getTime() >= tenderDetail.getTenderPubDt().getTime()) &&
                                         (new Date().getTime() < tenderDetail.getSubmissionDt().getTime() &&
                                         !("Cancelled".equalsIgnoreCase(tenderDetail.getTenderStatus().trim())))
                                     ){
                                          //  tenderDtlStatus = "<label style=\"color: red;font-weight: bold;text-decoration: blink;\">Live</label>";
                                        if(cntForCT != 0){
                                            tenderDtlStatus = "Amendment/Corrigendum issued : " + cntForCT;
                                        }
                                    }else if( (new Date().getTime() > tenderDetail.getSubmissionDt().getTime()) &&
                                                (cntForBeingP == 0) &&
                                                !("Cancelled".equalsIgnoreCase(tenderDetail.getTenderStatus().trim())) &&
                                                !("Re-Tendered".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) &&
                                                !("Re-Tender".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) &&
                                                !("Rejected".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) &&
                                                !("Awarded".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim()))
                                      ){
                                            tenderDtlStatus = "Being processed";
                                    }  else if ("Re-Tendered".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                        tenderDtlStatus = "To be Re-Tendered";
                                    }   else if ("Re-Tender".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                        tenderDtlStatus = "Re-Tendered";
                                    }   else if ("Rejected".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                        tenderDtlStatus = "Rejected";
                                    }  else if ("Awarded".equalsIgnoreCase(tenderDetail.getTenderevalstatus().trim())) {
                                        tenderDtlStatus = "Contract Awarded";
                                    }else if("Cancelled".equalsIgnoreCase(tenderDetail.getTenderStatus().trim())){
                                        //tenderStatus = "<a style=\"color: red;\" onclick=\"javascript:window.open('"+request.getContextPath()+"/officer/ViewCancelTender.jsp?tenderid=" + tenderDetail.getTenderId() + "', '', 'width=350px,height=400px,scrollbars=1','');\" href='javascript:void(0);'>" + tenderDetail.getTenderStatus() + "</a>";
                                        tenderDtlStatus = "Cancelled";
                                        //out.print("<td class=\"t-align-left\">" + tenderDetail.getProcurementNature() + ",<br />" + viewLink  + tenderStatus + "</td>");
                                    }
                            }
                    }else  {
                        tenderDtlStatus ="";
                    }
                        out.print("<td width=\"10%\" class=\"t-align-center\">" + tenderDetail.getTenderId() + ",<br/>" + tenderDetail.getReoiRfpRefNo() + "<br/><span style=\"color: red;font-weight: bold;\">" + tenderDtlStatus + "</span></td>");
                        //String viewLink = "<a href=\""+request.getContextPath()+"/resources/common/ViewTender.jsp?id=" + tenderDetail.getTenderId() + "\">" + tenderDetail.getTenderBrief() + "</a>";
                        String viewLink = "<a onclick=\"javascript:window.open('"+request.getContextPath()+"/resources/common/ViewNotice.jsp?id=" + tenderDetail.getTenderId() + "&h=f', '', 'resizable=yes,scrollbars=1','');\" href='#'><span id='tenderBrief_"+ i +"'>" + tenderDetail.getTenderBrief() + "</span></a>";
                        out.print("<td id=\"tdTenderBrief\" width=\"25%\" class=\"t-align-left\">" + tenderDetail.getProcurementNature() + ",<br />" + viewLink + "</td>");

                        StringBuilder deptSb=new StringBuilder();
                        String dept=tenderDetail.getMinistry();
                        deptSb.append(dept+",<br/> ");
                        dept=tenderDetail.getDivision();
                        if(dept!=null && !dept.isEmpty())
                            deptSb.append(dept +",<br/> ");
                        dept=tenderDetail.getAgency();
                        if(dept!=null && !dept.isEmpty())
                            deptSb.append(dept +",<br/> ");
                        out.print("<td width=\"15%\" class=\"t-align-left\">" + deptSb.toString()+tenderDetail.getPeOfficeName() + "</td>");
                        
                        //change NCT to NCB, ICT to ICB
                        if(tenderDetail.getProcurementType().equalsIgnoreCase("NCT"))
                            out.print("<td width=\"10%\" class=\"t-align-center\">" + "NCB" + ",<br /> ");
                        else if (tenderDetail.getProcurementType().equalsIgnoreCase("ICT"))
                            out.print("<td width=\"10%\" class=\"t-align-center\">" + "ICB" + ",<br /> ");
                        
                        if(tenderDetail.getProcurementMethod().equalsIgnoreCase("DPM"))
                            out.print("DCM</td>");
                        else if(tenderDetail.getProcurementMethod().equalsIgnoreCase("RFQ"))
                            out.print("LEM</td>");
                        else
                            out.print(tenderDetail.getProcurementMethod() + "</td>");
                        
                        StringBuilder dates = new StringBuilder();
                        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, d MM, yyyy HH:mm:ss");
                        if (tenderDetail.getTenderPubDt() != null) {
                            dates.append(DateUtils.gridDateToStr(tenderDetail.getTenderPubDt()));
                        }
                        else {
                            dates.append("N/A");
                        }
                        dates.append(" |<br /> ");
                        Date ClosingDate = new Date();
                        Date OpeningDate = new Date();
                        if (tenderDetail.getSubmissionDt() != null) {
                            dates.append(DateUtils.gridDateToStr(tenderDetail.getSubmissionDt()));
                            ClosingDate = tenderDetail.getSubmissionDt();
                        }
                        else {
                            dates.append("N/A");
                        }
                        if (tenderDetail.getOpeningDt()!= null) {
                            //dates.append(DateUtils.gridDateToStr(tenderDetail.getOpeningDt()));
                            OpeningDate = tenderDetail.getOpeningDt();
                        }
//                        else {
//                            dates.append("N/A");
//                        }
                        out.print("<td width=\"20%\" class=\"t-align-center\">" + dates.toString() + "</td>");
                        String dashBoardLink = request.getContextPath() + "/tenderer/TendererDashboard.jsp?tenderid=" + tenderDetail.getTenderId();
                        if (userTypeid.toString().equals("3")) {
                            dashBoardLink = request.getContextPath() + "/officer/TenderDashboard.jsp?tenderid=" + tenderDetail.getTenderId();
                        }
                        String imgLink = request.getContextPath() + "/resources/images/Dashboard/dashBoardIcn.png";
                        if(request.getParameter("statusTab").equalsIgnoreCase("Live"))
                        {
                            out.print("<td width=\"15%\" class=\"t-align-center\"><a href=\"" + dashBoardLink + "\" title=\"Dashboard\"><img src=\"" + imgLink + "\" alt=\"Dashboard\" /></a><br/><br/><span style=\"color: #CD5C5C;\" id=\"TenderSubmissionTimer"+ i +"\"></span><br/><br/><span style=\"color: #483D8B;\" id=\"TenderClosingTimer"+ i +"\"></span></td>");
                            out.print("<script type=\"text/javascript\"> clearInterval(ShowTimerVar"+ i +"); </script>");
                            out.print("<script type=\"text/javascript\"> var ShowTimerVar"+ i +"= setInterval( function(){ TimelineShow(document.getElementById(\"TenderSubmissionTimer"+ i +"\"),\""+ ClosingDate.getTime() +"\"); },1000); </script>");
//                            out.print("<a>ddd</a><br/><br/><span id=\"TenderClosingTimer"+ i +"\"></span></td>");
                            out.print("<script type=\"text/javascript\"> clearInterval(ShowOpeningTimerVar"+ i +"); </script>");
                            out.print("<script type=\"text/javascript\"> var ShowOpeningTimerVar"+ i +"= setInterval( function(){ TimelineShow(document.getElementById(\"TenderClosingTimer"+ i +"\"),\""+ OpeningDate.getTime() +"\"); },1000); </script>");
                        }
                        else if(request.getParameter("statusTab").equalsIgnoreCase("Archive"))
                        {
                            out.print("<td width=\"15%\" class=\"t-align-center\"><a href=\"" + dashBoardLink + "\" title=\"Dashboard\"><img src=\"" + imgLink + "\" alt=\"Dashboard\" /></a><br/><br/><span style=\"color: #483D8B;\" id=\"TenderClosingTimer"+ i +"\"></span></td>");
                            out.print("<script type=\"text/javascript\"> clearInterval(ShowOpeningTimerVar"+ i +"); </script>");
                            out.print("<script type=\"text/javascript\"> var ShowOpeningTimerVar"+ i +"= setInterval( function(){ TimelineShow(document.getElementById(\"TenderClosingTimer"+ i +"\"),\""+ OpeningDate.getTime() +"\"); },1000); </script>");
                        }
                        else
                        {
                            out.print("<td width=\"15%\" class=\"t-align-center\"><a href=\"" + dashBoardLink + "\" title=\"Dashboard\"><img src=\"" + imgLink + "\" alt=\"Dashboard\" /></a></td>");
                        }
                        //out.print("<td width=\"8%\" class=\"t-align-center\"><a href=\"" + dashBoardLink + "\" title=\"Dashboard\"><img src=\"" + imgLink + "\" alt=\"Dashboard\" /></a></td>");
                        out.print("</tr>");
                    }
                    out.print("<script type=\"text/javascript\"> SetCurrentDate(\""+ CurrentDate.getTime() +"\"); </script>");
                    out.print("<script type=\"text/javascript\"> clearInterval(IntervalVar); </script>");
                    out.print("<script type=\"text/javascript\"> var IntervalVar = setInterval(UpdateCurrentTimeByOneSec,1000); </script>");
                    int totalPages = 1;
                    if (tenderDetailes.size() > 0) {
                        totalPages = (int) (Math.ceil(Math.ceil(tenderDetailes.get(0).getTotalRecords()) / recordOffset));
                        System.out.print("totalPages--"+totalPages+"records "+ tenderDetailes.get(0).getTotalRecords());
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"cntTenBrief\" value=\"" + i + "\">");
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                }
                else {
                    out.print("<tr>");
                    out.print("<td colspan=\"7\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }
            }else if("checkTenderStatus".equalsIgnoreCase(funName)){
                TenderService tenderService = (TenderService) AppContext.getSpringBean("TenderService");
                List<TblTenderDetails> list =tenderService.getTenderStatus(Integer.parseInt(request.getParameter("tenderId")));
                out.print(list.get(0).getTenderStatus());
                out.flush();
                out.close();
            }
            out.flush();
        }
        finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

    /**
     * Getting list of all Tenders.
     * @param request
     * @return
     */
    private List<CommonTenderDetails> searchAllTenders(HttpServletRequest request)
    {

        String keyword = request.getParameter("keyword");

        String strPageNo = request.getParameter("pageNo");
        //System.out.println("Page No: " + strPageNo);
        int pageNo = Integer.parseInt(strPageNo);
        String strOffset = request.getParameter("size");
        int recordOffset = Integer.parseInt(strOffset);

        TenderService tenderService = (TenderService) AppContext.getSpringBean("TenderService");

        if (keyword != null) {
            return tenderService.gettenderinformationSP("generalsearch", keyword, 0, 0, 0, 0, "", 0, "", null, null, "Approved", "", "", "", "", null, null, pageNo, recordOffset,null,null);
        }
        else {
            String strUserId = request.getParameter("userId");
            int userId = 0;
            if (strUserId != null) {
                userId = Integer.parseInt(strUserId);
            }

            String department = request.getParameter("departmentId");
            if(department.contains("Select Organization"))
                department = "";
            String officeId = request.getParameter("office");
            int tenderId = 0;
            String strTenderId = request.getParameter("tenderId");
            if (strTenderId != null & !strTenderId.equals("")) {
                tenderId = Integer.parseInt(strTenderId);
            }
            String procNature = request.getParameter("procNature");
            int procNatureId = 0;
            if (procNature != null && !procNature.equals("")) {
                procNatureId = Integer.parseInt(procNature);
            }
            String procType = request.getParameter("procType");
            String procMethod = request.getParameter("procMethod");
            int procMethodId = 0;
            if (procMethod != null && !procMethod.equals("")) {
                procMethodId = Integer.parseInt(procMethod);
            }

            String pubDtFrm = null;
            if(request.getParameter("pubDtFrm") != null && !"".equals(request.getParameter("pubDtFrm"))){
                pubDtFrm = DateUtils.formatStrToStr(request.getParameter("pubDtFrm"));
                if (pubDtFrm.equals("")) {
                    pubDtFrm = null;
                }
            }

            String pubDtTo = null;
            if(request.getParameter("pubDtTo") != null && !"".equals(request.getParameter("pubDtTo"))){
                pubDtTo = DateUtils.formatStrToStr(request.getParameter("pubDtTo"));
                if (pubDtTo.equals("")) {
                    pubDtTo = null;
                }
            }

            String closeDtFrm = null;
            if(request.getParameter("closeDtFrm") != null && !"".equals(request.getParameter("closeDtFrm"))){
                closeDtFrm = DateUtils.formatStrToStr(request.getParameter("closeDtFrm"));
                if (closeDtFrm.equals("")) {
                    closeDtFrm = null;
                }
            }

            String closeDtTo = null;
            if(request.getParameter("closeDtTo") != null && !"".equals(request.getParameter("closeDtTo"))){
                closeDtTo = DateUtils.formatStrToStr(request.getParameter("closeDtTo"));
                if (closeDtTo.equals("")) {
                    closeDtTo = null;
                }
            }

            //Code start by Proshanto Kumar Saha
            String district="";
            String District=request.getParameter("district");
            if(!"".equals(District) && District != null)
                district = District;

            String thana="";
            String Thana=request.getParameter("thana");
            if(!"".equals(Thana) && Thana != null)
                thana = Thana;
            //Code End by Proshanto Kumar Saha

            String cpvcat = request.getParameter("cpvCategory");
            String agency = "";
            if(!"".equals(cpvcat) && cpvcat != null)
                agency = cpvcat;
            String refNo = request.getParameter("refNo");

            String viewType = request.getParameter("viewType");
            if(viewType.equalsIgnoreCase("Live")){
                viewType = "Live";
            }else if(viewType.equalsIgnoreCase("Archive")){
                viewType = "Archive";
            }else if(viewType.equalsIgnoreCase("Progress")){
                viewType = "Progress";
            } else if (viewType.equalsIgnoreCase("Complete")) {
                viewType = "Complete";
            }else if(viewType.equalsIgnoreCase("AllTenders")){
                viewType = "";
            }else if(viewType.equalsIgnoreCase("Cancel")){
                viewType = "Cancelled";
            }
            HttpSession session = request.getSession();
            String methodName = "get alltendersbytype";
            if(request.getParameter("from")!= null){
                if(request.getParameter("from").equalsIgnoreCase("ownCategory")){
                    methodName = "ownCategoryget alltendersbytype";
                }
            }                  
            if(session.getAttribute("userTypeId") != null && !"".equals(session.getAttribute("userTypeId").toString())){
                if(session.getAttribute("userTypeId").toString().equals("3")){
                    methodName = "get officeralltendersbytype";
                }
            }
            String status="";
            if(viewType.equalsIgnoreCase("Progress"))
            {
                System.out.print(userId);
                
                methodName = "get tenderermytenders";
                status="Approved";
                System.out.println(status);
                return tenderService.gettenderinformationSP(methodName, viewType, tenderId, 0, userId, procNatureId, procType, procMethodId, refNo, pubDtFrm, pubDtTo, status, "", "", "", "", null, null, pageNo, recordOffset,district,thana);
                       

            }
            return tenderService.gettenderinformationSP(methodName, viewType, tenderId, 0, 0, procNatureId, procType, procMethodId, refNo, pubDtFrm, pubDtTo, status, department, "", agency, officeId, closeDtFrm, closeDtTo, pageNo, recordOffset,district,thana);

        }
    }

    /**
     * Getting List of Logged in user Tenders.
     * @param request
     * @return
     */
    private List<CommonTenderDetails> searchMyTenders(HttpServletRequest request)
    {

        HttpSession session = request.getSession();

        Object objUserId = session.getAttribute("userId");
        int userId = 0;
        if (objUserId != null) {
            userId = Integer.parseInt(objUserId.toString());
        }

        String action = request.getParameter("action");
        String strPageNo = request.getParameter("pageNo");
        System.out.println("Page No: " + strPageNo);
        int pageNo = Integer.parseInt(strPageNo);
        String strOffset = request.getParameter("size");
        System.out.println("strOffset: " + strOffset);
        int recordOffset = Integer.parseInt(strOffset);
        String status = request.getParameter("status");
        String statusTab = "";
        if(request.getParameter("statusTab") != null){
            statusTab = request.getParameter("statusTab");
        }

        String officeId = request.getParameter("office");
        int tenderId = 0;
        String strTenderId = request.getParameter("tenderId");
        if (strTenderId != null & !strTenderId.equals("")) {
            tenderId = Integer.parseInt(strTenderId);
        }
        String procNature = request.getParameter("procNature");
        int procNatureId = 0;
        if (procNature != null && !procNature.equals("")) {
            procNatureId = Integer.parseInt(procNature);
        }
        String procType = request.getParameter("procType");
        String procMethod = request.getParameter("procMethod");
        int procMethodId = 0;
        if (procMethod != null && !procMethod.equals("")&& !procMethod.equals("null")) {
            procMethodId = Integer.parseInt(procMethod);
        }
        String pubDtFrm = null;

        if(request.getParameter("pubDtFrm") != null && !"".equals(request.getParameter("pubDtFrm"))){
         pubDtFrm= DateUtils.formatStrToStr(request.getParameter("pubDtFrm"));
        if (pubDtFrm.equals("")) {
            pubDtFrm = null;
        }
        }
        String pubDtTo = null;
        if(request.getParameter("pubDtTo") != null && !"".equals(request.getParameter("pubDtTo"))){
         pubDtTo = DateUtils.formatStrToStr(request.getParameter("pubDtTo"));
        if (pubDtTo.equals("")) {
            pubDtTo = null;
        }
        }
        String refNo = request.getParameter("refNo");

        TenderService tenderService = (TenderService) AppContext.getSpringBean("TenderService");
        if ("yes".equalsIgnoreCase(request.getParameter("rfp"))) 
            action="get tendererlimitedrfp";
        else if ("no".equalsIgnoreCase(request.getParameter("rfp"))) 
            action = "get tendererlimitedtender";
        return tenderService.gettenderinformationSP(action, statusTab, tenderId, 0, userId, procNatureId, procType, procMethodId, refNo, pubDtFrm, pubDtTo, status, "", "", "", "", null, null, pageNo, recordOffset,null,null);
    }

     /**
     * Getting list of TendererDashboard
     * @param request
     * @return
     */
    private List<EvalCommonSearchData> TendererDashboard(HttpServletRequest request)
    {

        HttpSession session = request.getSession();

        Object objUserId = session.getAttribute("userId");
        int userId = 0;
        if (objUserId != null) {
            userId = Integer.parseInt(objUserId.toString());
        }
        System.out.println("****TendererDashboard HttpServletRequest****" );
        String action = request.getParameter("action");
        String strPageNo = request.getParameter("pageNo");
        System.out.println("Page No: " + strPageNo);
        int pageNo = Integer.parseInt(strPageNo);
        String strOffset = request.getParameter("size");
        System.out.println("size No: " + strOffset);
        int recordOffset = Integer.parseInt(strOffset);
        //String status = request.getParameter("status");
        //String statusTab = "";
        //if(request.getParameter("statusTab") != null){
         //   statusTab = request.getParameter("statusTab");
        //}

        //String officeId = request.getParameter("office");
        int tenderId = 0;
        String strTenderId = request.getParameter("tenderId");
        if (strTenderId != null & !strTenderId.equals("")) {
            tenderId = Integer.parseInt(strTenderId);
        }
        String procNature = request.getParameter("procNature");
        int procNatureId = 0;
        if (procNature != null && !procNature.equals("")) {
            procNatureId = Integer.parseInt(procNature);
        }
        String procType = request.getParameter("procType");
        String procMethod = request.getParameter("procMethod");
        int procMethodId = 0;
        if (procMethod != null && !procMethod.equals("")&& !procMethod.equals("null")) {
            procMethodId = Integer.parseInt(procMethod);
        }
        String pubDtFrm = null;

        if(request.getParameter("pubDtFrm") != null && !"".equals(request.getParameter("pubDtFrm"))){
         pubDtFrm= DateUtils.formatStrToStr(request.getParameter("pubDtFrm"));
        if (pubDtFrm.equals("")) {
            pubDtFrm = null;
        }
        }
        String pubDtTo = null;
        if(request.getParameter("pubDtTo") != null && !"".equals(request.getParameter("pubDtTo"))){
         pubDtTo = DateUtils.formatStrToStr(request.getParameter("pubDtTo"));
        if (pubDtTo.equals("")) {
            pubDtTo = null;
        }
        }
        //String refNo = request.getParameter("refNo");

        TenderService tenderService = (TenderService) AppContext.getSpringBean("TenderService");
        //return tenderService.gettenderinformationSP(action, statusTab, tenderId, 0, userId, procNatureId, procType, procMethodId, refNo, pubDtFrm, pubDtTo, status, "", "", "", "", null, null, pageNo, recordOffset);

            String status = request.getParameter("status");
            if(status.equalsIgnoreCase("Pending")){
                status = "Pending";
            }else if(status.equalsIgnoreCase("Progress")){
                status = "Progress";
            } else if (status.equalsIgnoreCase("Complete")) {
                status = "Complete";
            }
            //String methodName = "TendererDashboard";

            return tenderService.gettenderDashboardinformationSP(action, status, userId,strPageNo, strOffset, String.valueOf(procNatureId), procMethod, procType, String.valueOf(tenderId) , pubDtFrm, pubDtTo);
            //return tenderService.gettenderinformationSP(methodName, status, tenderId, 0, userId, procNatureId, procType, procMethodId, refNo, pubDtFrm, pubDtTo, status, "", "", "", "", null, null, pageNo, recordOffset);

        }
    }

