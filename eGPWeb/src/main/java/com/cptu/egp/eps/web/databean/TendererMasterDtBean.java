/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

import com.cptu.egp.eps.web.utility.BanglaNameUtils;
import com.cptu.egp.eps.web.utility.SHA1HashEncryption;

/**
 *
 * @author Administrator
 */
public class TendererMasterDtBean {

    /** Creates a new instance of TendererMasterDataBean */
    public TendererMasterDtBean() {
    }
    private int tendererId;
    private String isAdmin;
    private String title;
    private String firstName;
    private String middleName="";
    private String lasatName="";
    private byte[] fullNameInBangla;
    private String nationalIdNo="";
    private String address1;
    private String address2="";
    private String country;
    private String state;
    private String subDistrict;
    private String city="";
    private String upJilla="";
    private String postcode="";
    private String phoneNo="";
    private String mobileNo;
    private String faxNo="";
    private String tinNo;
    private String tinDocName;
    private String comments;
    private String specialization;
    private String website="";
    private String designation;
    private String department="";
    private String banglaName="";
    private int cmpId=0;
    private int userId;
    private String otherDoc;
    private String phoneSTD="";
    private String faxSTD="";
    private String nationality;
    private String emailId;
    private String password;
    private int companyId;
    private String phoneCode;
    private String faxCode;
    private String mobileCode;
    private String emailAddress;
    private int UserType;

    public String getFaxCode() {
        return faxCode;
    }

    public void setFaxCode(String faxCode) {
        this.faxCode = faxCode;
    }

    public String getMobileCode() {
        return mobileCode;
    }

    public void setMobileCode(String mobileCode) {
        this.mobileCode = mobileCode;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }
    
    private String lastName;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password =  SHA1HashEncryption.encodeStringSHA1(password);
    }

    public String getFaxSTD() {
        return faxSTD;
    }

    public void setFaxSTD(String faxSTD) {
        this.faxSTD = faxSTD;
    }

    public String getPhoneSTD() {
        return phoneSTD;
    }

    public void setPhoneSTD(String phoneSTD) {
        this.phoneSTD = phoneSTD;
    }

    public String getOtherDoc() {
        return otherDoc;
    }

    public void setOtherDoc(String otherDoc) {
        if(otherDoc!=null){
            setTinDocName(tinDocName+"$"+otherDoc);
        }
        this.otherDoc = otherDoc;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCmpId() {
        return cmpId;
    }

    public void setCmpId(int cmpId) {
        this.cmpId = cmpId;
    }

    public String getBanglaName() {
        return banglaName;
    }

    public void setBanglaName(String banglaName) {
        this.fullNameInBangla = BanglaNameUtils.getBytes(banglaName);
        this.banglaName = banglaName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getFaxNo() {
        return faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = getFaxSTD()+"-"+faxNo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public byte[] getFullNameInBangla() {
        return fullNameInBangla;
    }

    public void setFullNameInBangla(byte[] fullNameInBangla) {
        this.fullNameInBangla = fullNameInBangla;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getLasatName() {
        return lasatName;
    }

    public void setLasatName(String lasatName) {
        this.lasatName = lasatName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getNationalIdNo() {
        return nationalIdNo;
    }

    public void setNationalIdNo(String nationalIdNo) {
        this.nationalIdNo = nationalIdNo;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = getPhoneSTD()+"-"+phoneNo;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    public String getSubDistrict() {
        return subDistrict;
    }

    public void setSubDistrict(String subDistrict) {
        this.subDistrict = subDistrict;
    }
    public int getTendererId() {
        return tendererId;
    }

    public void setTendererId(int tendererId) {
        this.tendererId = tendererId;
    }

    public String getTinDocName() {
        return tinDocName;
    }

    public void setTinDocName(String tinDocName) {
        this.tinDocName = tinDocName;
    }

    public String getTinNo() {
        return tinNo;
    }

    public void setTinNo(String tinNo) {
        this.tinNo = tinNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpJilla() {
        return upJilla;
    }

    public void setUpJilla(String upJilla) {
        this.upJilla = upJilla;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     * @return the emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * @param emailAddress the emailAddress to set
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * @return the UserType
     */
    public int getUserType() {
        return UserType;
    }

    /**
     * @param UserType the UserType to set
     */
    public void setUserType(int UserType) {
        this.UserType = UserType;
    }

    
}
