/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SearchNOAData;
import com.cptu.egp.eps.service.serviceimpl.SearchNOAService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.NumberFormat;

/**
 *
 * @author rishita
 */
public class SearchNoaServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String keyword = request.getParameter("keyword");
            String strPageNo = request.getParameter("pageNo");
            String styleClass = "";
            //System.out.println("Page No: " + strPageNo);
            int pageNo = Integer.parseInt(strPageNo);
            String strOffset = request.getParameter("size");
            NumberFormat formatter = new DecimalFormat("######.##");
            int recordOffset = Integer.parseInt(strOffset);
            SearchNOAService noaService = (SearchNOAService) AppContext.getSpringBean("SearchNOAService");
            //Added 2 parameters "" by Proshanto Kumar Saha
            List<SearchNOAData> noaList = noaService.returndata(keyword, "", "", 0, 0, "", 0, "", "", "", 0, pageNo, recordOffset,"","","","","","","");
            if (!noaList.isEmpty()) {
                for (int i = 0; i < noaList.size(); i++) {
                    if(i%2==0){
                        styleClass = "bgColor-white";
                    }else{
                        styleClass = "bgColor-Green";
                    }
                    
                    SearchNOAData searchNOAData = noaList.get(i);
                    out.print("<tr class='" + styleClass + "'>");
                    out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * 10 + (i + 1)) + "</td>");
                    out.print("<td class=\"t-align-center\">" + searchNOAData.getMDADetails() + "</td>");
                    String linkTenderId[] = searchNOAData.getKeyword().split(",");
                    String[] RefTitleAdv = searchNOAData.getTenderRefNo().split(",");
                    String thisContractNo = searchNOAData.getContractNo();
                    for(int j=1;j<linkTenderId.length-3;j++){
                        linkTenderId[0]+=linkTenderId[j];
                    }
                    //out.print("<td class=\"t-align-center\"><a href=\"javascript:void(0);\" onclick=\"javascript:window.open('" + request.getContextPath() + "/resources/common/ViewAwardedContracts.jsp?pkgLotId=" + linkTenderId[linkTenderId.length-2] + "&tenderid=" + linkTenderId[linkTenderId.length-3] + "&userId=" + linkTenderId[linkTenderId.length-1] + "', '', 'resizable=yes,scrollbars=1','');\">" + RefTitleAdv[0] + "</a><br/>" + linkTenderId[0] + "<br/>" + RefTitleAdv[1] + "</td>");
                    out.print("<td class=\"t-align-center\"><a href=\"javascript:void(0);\" onclick=\"javascript:window.open('" + request.getContextPath() + "/resources/common/ViewContracts.jsp?contractNo="+thisContractNo+"&pkgLotId=" + linkTenderId[linkTenderId.length-2] + "&tenderid=" + linkTenderId[linkTenderId.length-3] + "&userId=" + linkTenderId[linkTenderId.length-1] + "', '', 'resizable=yes,scrollbars=1','');\">" + linkTenderId[linkTenderId.length-3]+", "+RefTitleAdv[0] + "</a><br/>" + " <span class=\"more\"> "+ linkTenderId[0] +" </span>" + RefTitleAdv[1] + "</td>");
                    out.print("<td class=\"t-align-center\">" + searchNOAData.getProcuringEntity() + "</td>");
//                    out.print("<td class=\"t-align-left\">" + searchNOAData.getDistrict() + "</td>");
                    out.print("<td class=\"t-align-center\">" + searchNOAData.getNOADate() + "</td>");
                    out.print("<td class=\"t-align-center\">" + searchNOAData.getContractAwardedTo() + "</td>");
                    /*Dohatec Start To Display 3 digit after decimal point*/
                    DecimalFormat df2 = new DecimalFormat( "0.000" );
                    String ContractAmountString = df2.format(Double.parseDouble(searchNOAData.getContractAmt())/1000000).toString();
                    out.print("<td class=\"t-align-center\">" + ContractAmountString + "</td>");
                    /*dohatec End To Display 3 digit after decimal point*/
                    out.print("</tr>");
                }
            } else {
                out.print("<tr>");
                out.print("<td id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\" colspan=\"8\">No Record Found!</td>");
                out.print("</tr>");
            }
            int totalPages = 1;
            if (noaList.size() > 0) {
                int rowCount = Integer.parseInt(noaList.get(0).getTotalRowCount());
                totalPages = (int) Math.ceil(Math.ceil(rowCount)/ recordOffset);
                if (totalPages == 0) {
                    totalPages = 1;
                }
            }
            out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
