/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.LoginReportDtBean;
import com.cptu.egp.eps.service.serviceimpl.LoginReportImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cptu.egp.eps.web.servicebean.GenreatePdfCmd;
import com.cptu.egp.eps.web.utility.DateUtils;
import com.cptu.egp.eps.web.utility.GeneratePdfConstant;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author rishita
 */
public class LoginReportServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String action = request.getParameter("action");
            if (action.equalsIgnoreCase("fetchData")) {

                String userId = String.valueOf(request.getSession().getAttribute("userId"));
                String userTypeId = String.valueOf(request.getSession().getAttribute("userId"));
                String TypeUser = request.getParameter("TypeUser");
                String searchDate = request.getParameter("searchDate");
                String dateFrom = request.getParameter("dateFrom");
                String dateTo = request.getParameter("dateTo");

                String emailId = request.getParameter("emailID");
                String ipaddress = request.getParameter("ipaddress");

                String strPageNo = request.getParameter("pageNo");
                String styleClass = "";
                int pageNo = Integer.parseInt(strPageNo);
                String strOffset = request.getParameter("size");
                int recordOffset = Integer.parseInt(strOffset);
                
                LoginReportImpl loginReport = (LoginReportImpl) AppContext.getSpringBean("LoginReportImpl");
                loginReport.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
                List<LoginReportDtBean> reportDetails = loginReport.loginReportDetails(TypeUser, searchDate, dateFrom, dateTo, emailId, Integer.parseInt(userId), Integer.parseInt(userTypeId), pageNo, recordOffset,ipaddress);

                int StartForm = (pageNo-1)*recordOffset;
                
                if (!reportDetails.isEmpty()) {
                    for (int i = StartForm; i < StartForm+recordOffset && i<reportDetails.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }

                        LoginReportDtBean listing = reportDetails.get(i);
                        out.print("<tr class='" + styleClass + "'>");
                        out.print("<td class=\"t-align-center\">" + (i+1) + "</td>");
                        out.print("<td class=\"t-align-left\">" + reportDetails.get(i).getEmailId() + "</td>");
                        out.print("<td class=\"t-align-center\">" + DateUtils.gridDateToStrWithoutSec(reportDetails.get(i).getSessionStartDt()) + "</td>");
                        out.print("<td class=\"t-align-center\">" + DateUtils.gridDateToStrWithoutSec(reportDetails.get(i).getSessionEndDt())  + "</td>");
                        out.print("<td class=\"t-align-center\">" + reportDetails.get(i).getIpAddress()  + "</td>");
                        out.print("</tr>");
                    }
                } else {
                    out.print("<tr>");
                    out.print("<td id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-left\" style=\"color: red;font-weight: bold\" colspan=\"5\">No Record Found!</td>");
                    out.print("</tr>");
                }
                int totalPages = 1;
                if (reportDetails.size() > 0) {
                    int rowCount = reportDetails.get(0).getTotalRecords();
                    totalPages = (int) Math.ceil(Math.ceil(rowCount) / recordOffset);
                    if (totalPages == 0) {
                        totalPages = 1;
                    }
                }
                out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                try {
                    GenreatePdfCmd obj = new GenreatePdfCmd();
                    GeneratePdfConstant constant = new GeneratePdfConstant();
                    String cdate = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date(new Date().getTime()));
                    String folderName = constant.LOGINREPORT;
                    String genId = cdate + "_" + userId;
                    int MILLIS_IN_DAY = 1000 * 60 * 60 * 24;
                    String prevDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date().getTime() - MILLIS_IN_DAY);
                    String prevgenId = prevDate + "_" + userId;

                    File destFolder1 = new File("c:/eGP/PDF/" + folderName);
                    if (destFolder1.exists() == true) {
                        File[] childFiles = destFolder1.listFiles();
                        for (File child : childFiles) {

                            if(child.getName().contains(cdate)){
                                
                            }else{
                                File[] pdfChildFiles = child.listFiles();
                                for (File pdfChild : pdfChildFiles) {
                                            pdfChild.delete();
                                    }
                                
                                child.delete();
                            }
                            
                        }
                    }

                    int userTypeID = Integer.parseInt(request.getSession().getAttribute("userTypeId").toString());
                        if(userTypeID== 1 || userTypeID== 8) {
                            pageNo = 0;
                        }
                    String reqURL = request.getRequestURL().toString();
                    reqURL = reqURL.replace("LoginReportServlet", "resources/common/LoginReportPdf.jsp");
                    String reqQuery = "userid=" + Integer.parseInt(userId) + "&usertypeId=" + Integer.parseInt(userTypeId) + "&searchDate=" + searchDate + "&dateFrom=" + dateFrom + "&dateTo=" + dateTo + "&emailId=" + emailId + "&page=" + pageNo + "&limit=" + recordOffset + "&ipaddress=" + ipaddress+ "&TypeUser=" + TypeUser;

                    obj.genrateCmd(reqURL, reqQuery, folderName, genId);
                } catch (Exception e) {
                    System.out.println("exception-" + e);
                }
            }
        }
        catch (Exception e) {
            System.out.println("exception-" + e);
        }
        finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
