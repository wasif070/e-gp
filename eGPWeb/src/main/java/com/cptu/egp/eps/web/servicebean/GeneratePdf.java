/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.web.utility.AppExceptionHandler;
import com.cptu.egp.eps.web.utility.XMLReader;
import java.io.*;
import java.net.URLDecoder;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Swati Patel
 * Created On 21st dec. 2010
 * 
 */
public class GeneratePdf extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String logUserId = "";
    private String strDriveLetter = XMLReader.getMessage("driveLatter");
    private boolean isDevelopment = false;
    private boolean isStaging = false;
    private boolean isProduction = false;
    private boolean isTraining = false;
    private boolean isInHouse = false;
    public String path = "c:/eGP/PDF";
    final Logger logger = Logger.getLogger(GeneratePdf.class);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.debug("processRequest " + logUserId + " Starts");

        HttpSession session = request.getSession();
        if (session.getAttribute("userId") != null) {
            logUserId = session.getAttribute("userId").toString();
        }
        String reqURL = request.getRequestURL().toString();
        if(reqURL.indexOf("192.168.100.152") > 0 || reqURL.indexOf("www.eprocure.gov.bd") > 0 || reqURL.indexOf("development.eprocure.gov.bd") > 0 || reqURL.indexOf("inhouse.eprocure.gov.bd") > 0){
            reqURL = reqURL.replace("http", "https");
            reqURL = reqURL.replace("httpss", "https");
        }
        String encodedpdfName = "";
        String decodedpdfName = "";
        if (request.getParameter("pdfName") != null) {
            encodedpdfName = request.getParameter("pdfName");
            decodedpdfName = URLDecoder.decode(encodedpdfName, "UTF-8");
        }

        try {

            // Check the server information
            if(reqURL.contains("development.eprocure.gov.bd") || reqURL.contains("10.1.3.21")){
                isDevelopment = true;
            }
            if(reqURL.contains("staging.eprocure.gov.bd") || reqURL.contains("10.1.4.21")){
                isStaging = true;
            }
            if(reqURL.contains("training.eprocure.gov.bd") || reqURL.contains("10.1.40.25")){
                isTraining = true;
            }
            if(reqURL.contains("www.eprocure.gov.bd") || reqURL.contains("10.1.2.26") || reqURL.contains("10.1.2.27")){
                isProduction = true;
            }
            if(reqURL.contains("192.168.100.131/") || reqURL.contains("192.168.100.152/") || reqURL.contains("122.170.119.66/") || reqURL.contains("inhouse.eprocure.gov.bd/")){
                isInHouse = true;
            }
            //isDevelopment = (reqURL.contains("development.eprocure.gov.bd"));
            //isStaging = reqURL.contains("staging.eprocure.gov.bd");
            //isProduction = reqURL.contains("www.eprocure.gov.bd");
            //isTraining = reqURL.contains("training.eprocure.gov.bd");

//            if (isDevelopment || isTraining) {
//                strDriveLetter = "D:";
//            } else if (isStaging) {
//                strDriveLetter = "E:";
//            } else if (isProduction) {
//                strDriveLetter = "\\\\egpprdblog\\AppSharedDrive";
//            }

            path = strDriveLetter + "/eGP/PDF/";
            System.out.println("path" + path);

            String folderName = request.getParameter("folderName");
            String id = request.getParameter("id");
            logger.debug("processRequest " + logUserId + " " + folderName + " Starts");
            logger.debug("processRequest reqURL : " + reqURL + " folderName : " + folderName + " pdfName : " +  decodedpdfName + " id : " + id);
            System.out.println("processRequest reqURL : " + reqURL + " folderName : " + folderName + " pdfName : " + decodedpdfName + " id : " + id);

            try {

                //to prompt user for Open and Save
                FileInputStream fis = null;
                if (!"".equalsIgnoreCase(decodedpdfName)) {
                    fis = new FileInputStream(path + folderName + "/" + id + "/" + decodedpdfName + ".pdf");
                    response.setContentType("application/pdf");
                    response.setHeader("Content-Disposition", "attachment;filename=\"" + decodedpdfName + ".pdf\"");
                } else {
                    File file = new File(path + folderName + "/" + id + "/" + folderName + ".pdf");
                    if(!file.exists()){
                        if(folderName.equalsIgnoreCase("TenderNotice")){
                            GenreatePdfCmd obj = new GenreatePdfCmd();
                            String tmpURL = reqURL.replace("GeneratePdf", "resources/common/ViewTender.jsp");
                            String reqQuery="id="+id;
                            obj.genrateCmd(tmpURL, reqQuery, folderName, id);
                        }
                        if(folderName.equalsIgnoreCase("NOA")){
                            GenreatePdfCmd obj = new GenreatePdfCmd();
                            String tmpURL = reqURL.replace("GeneratePdf", "tenderer/ViewNOA.jsp");
                            String reqQuery="tenderid="+request.getParameter("tId")+"&NOAID="+request.getParameter("NOAID")+"&app=no&id="+id+"&userid="+logUserId+"&userNameNOA="+request.getParameter("userNameNOA");
                            obj.genrateCmd(tmpURL, reqQuery, folderName, id);
                        }else if(folderName.equalsIgnoreCase("TENDERDETAIL")){
                            GenreatePdfCmd obj = new GenreatePdfCmd();
                            String tmpURL = reqURL.replace("GeneratePdf", "admin/ViewTenderPaymentDetails.jsp");
                            //String reqQuery="tenderid="+request.getParameter("tId")+"&NOAID="+request.getParameter("NOAID")+"&app=no&id="+id+"&userid="+logUserId+"&userNameNOA="+request.getParameter("userNameNOA");
                            String reqQuery="payId=" + request.getParameter("payId") + "&uId=" + request.getParameter("uId") + "&tenderId=" + request.getParameter("tenderId") + "&lotId=" + request.getParameter("lotId") + "&payTyp=df&id=" + request.getParameter("id");
                            obj.genrateCmd(tmpURL, reqQuery, folderName, id);
                        }else if(folderName.equalsIgnoreCase("RegistrationPayment")){
                            GenreatePdfCmd obj = new GenreatePdfCmd();
                            String tmpURL = reqURL.replace("GeneratePdf", "admin/TendererPaymentDetail.jsp");
                            //String reqQuery="tenderid="+request.getParameter("tId")+"&NOAID="+request.getParameter("NOAID")+"&app=no&id="+id+"&userid="+logUserId+"&userNameNOA="+request.getParameter("userNameNOA");
                            String reqQuery=request.getParameter("reqQuery");
                            reqQuery = reqQuery.replaceAll("@@", "=");
                            reqQuery = reqQuery.replaceAll("!!", "&");
                            obj.genrateCmd(tmpURL, reqQuery, folderName, id);
                        }else if(folderName.equalsIgnoreCase("NoaForms")){
                            GenreatePdfCmd obj = new GenreatePdfCmd();
                            String tmpURL = reqURL.replace("GeneratePdf", "resources/common/ViewBOQForms.jsp");
                            //String reqQuery="tenderid="+request.getParameter("tId")+"&NOAID="+request.getParameter("NOAID")+"&app=no&id="+id+"&userid="+logUserId+"&userNameNOA="+request.getParameter("userNameNOA");
                            String reqQuery=request.getParameter("reqQuery");
                            reqQuery = reqQuery.replaceAll("@@", "=");
                            reqQuery = reqQuery.replaceAll("!!", "&");
                            obj.genrateCmd(tmpURL, reqQuery, folderName, id);
                        }
                        else if(folderName.equalsIgnoreCase("MiscPaymentReport")){
                            GenreatePdfCmd obj = new GenreatePdfCmd();
                            String tmpURL = reqURL.replace("GeneratePdf", "admin/MiscPaymentRptPDF.jsp");
                            //String reqQuery="tenderid="+request.getParameter("tId")+"&NOAID="+request.getParameter("NOAID")+"&app=no&id="+id+"&userid="+logUserId+"&userNameNOA="+request.getParameter("userNameNOA");
                            String reqQuery=request.getParameter("reqQuery");
                            reqQuery = reqQuery.replaceAll("@@", "=");
                            reqQuery = reqQuery.replaceAll("!!", "&");
                            obj.genrateCmd(tmpURL, reqQuery, folderName, id);
                        }
                        else if(folderName.equalsIgnoreCase("DocFeesPaymentReport")){
                            GenreatePdfCmd obj = new GenreatePdfCmd();
                            String tmpURL = reqURL.replace("GeneratePdf", "report/TenderPaymentReport.jsp");
                            //String reqQuery="tenderid="+request.getParameter("tId")+"&NOAID="+request.getParameter("NOAID")+"&app=no&id="+id+"&userid="+logUserId+"&userNameNOA="+request.getParameter("userNameNOA");
                            String reqQuery="id="+id;
                            obj.genrateCmd(tmpURL, reqQuery, folderName, id);
                        }
                        
                    }
                    String pdfName = "";
                    if(folderName.equals("TenderDetail")){
                        pdfName = "DocumentFeesReceipt";
                    }else if(folderName.equalsIgnoreCase("RegistrationPayment")){
                            pdfName = "RegistrationFees";
                    }else{
                        pdfName = folderName;
                    }
                    fis = new FileInputStream(path + folderName + "/" + id + "/" + pdfName + ".pdf");
                    response.setContentType("application/pdf");
                    response.setHeader("Content-Disposition", "attachment;filename=\"" + pdfName + ".pdf\"");
                }
                ServletOutputStream outputStream = response.getOutputStream();

                byte[] buf = new byte[4 * 1024]; // 4K buffer

                int bytesRead;
                while ((bytesRead = fis.read(buf)) != -1) {
                    outputStream.write(buf);
                }
                outputStream.flush();
                fis.close();

            } catch (IOException e1) {
                e1.printStackTrace();
                logger.error("processRequest " + logUserId + " :" + e1);
                AppExceptionHandler expHandle = new AppExceptionHandler();
                expHandle.stack2string(e1);
            } catch (Exception e) {
                logger.error("processRequest " + logUserId + " :" + e);
                AppExceptionHandler expHandle = new AppExceptionHandler();
                expHandle.stack2string(e);
            }

        } catch (Exception e) {
            logger.error("processRequest " + logUserId + " :" + e);
            AppExceptionHandler expHandle = new AppExceptionHandler();
            expHandle.stack2string(e);
        }
        logger.debug("processRequest " + logUserId + " Ends");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
