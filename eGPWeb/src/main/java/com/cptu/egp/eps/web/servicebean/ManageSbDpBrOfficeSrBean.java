/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class ManageSbDpBrOfficeSrBean {

final Logger logger = Logger.getLogger(ManageSbDpBrOfficeSrBean.class);

ScBankDevpartnerService scBankDevpartnerService=(ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");

    private int offset = 0;
    private int limit = 0;
    private String colName = "";
    private String op_ENUM = "";
    private String condition = "";
    private boolean _search = false;
    private String sortOrder = "";
    private String sortCol = "";
    private String logUserId="0";

     public void setLogUserId(String logUserId) {
        scBankDevpartnerService.setUserId(logUserId);
        this.logUserId = logUserId;
     }
    public boolean isSearch()
    {
        return _search;
    }

    public void setSearch(boolean _search)
    {
        this._search = _search;
    }

    public String getColName()
    {
        return colName;
    }

    public void setColName(String colName)
    {
        this.colName = colName;
    }

    public String getCondition()
    {
        return condition;
    }

    public void setCondition(String condition)
    {
        this.condition = condition;
    }

    public String getSortOrder()
    {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder)
    {
        this.sortOrder = sortOrder;
    }

    public int getLimit()
    {
        return limit;
    }

    public void setLimit(int limit)
    {
        this.limit = limit;
    }

    public int getOffset()
    {
        return offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public String getOp_ENUM()
    {
        return op_ENUM;
    }

    public void setOp_ENUM(String op_ENUM)
    {
        this.op_ENUM = op_ENUM;
    }

    public ScBankDevpartnerService getScBankDevpartnerService()
    {
        return scBankDevpartnerService;
    }

    public void setScBankDevpartnerService(ScBankDevpartnerService scBankDevpartnerService)
    {
        this.scBankDevpartnerService = scBankDevpartnerService;
    }

    public String getSortCol()
    {
        return sortCol;
    }

    public void setSortCol(String sortCol)
    {
        this.sortCol = sortCol;
    }

       public long getCntHeadOffice(String type,int userTypeId,int userId)  {
        logger.debug("getCntHeadOffice : "+logUserId+" Starts ");
        long lng = 0;
           try {
               if(userTypeId!=1){
                   ScBankDevpartnerSrBean scBankCreationSrBean = new ScBankDevpartnerSrBean();
                    scBankCreationSrBean.getBankName(userId, (byte) userTypeId);
                    userId = scBankCreationSrBean.getsBankDevelopId();
                lng = scBankDevpartnerService.getCntOffice("TblScBankDevPartnerMaster scdpMaster", "scdpMaster.partnerType LIKE '"+type+"' AND scdpMaster.isBranchOffice LIKE 'Yes' And scdpMaster.sbankDevelHeadId="+userId);
               }else{
                lng = scBankDevpartnerService.getCntOffice("TblScBankDevPartnerMaster scdpMaster", "scdpMaster.partnerType LIKE '"+type+"' AND scdpMaster.isBranchOffice LIKE 'Yes'");
    }
           } catch (Exception e) {
                logger.error("getCntHeadOffice : "+logUserId+" : "+e);
           }
           logger.debug("getCntHeadOffice : "+logUserId+" Ends ");
           return lng;
    }

    List<Object[]> officeList = new ArrayList<Object[]>();
    public List<Object[]> getBranchOfficeList(String type,int userTypeId,int userId,String ascClause) {
         logger.debug("getBranchOfficeList : "+logUserId+" Starts ");
         try {
        if (officeList.isEmpty()) {
                    if(userTypeId!=1){
                        ScBankDevpartnerSrBean scBankCreationSrBean = new ScBankDevpartnerSrBean();
                        scBankCreationSrBean.getBankName(userId, (byte) userTypeId);
                        userId = scBankCreationSrBean.getsBankDevelopId();
                    }
            officeList=scBankDevpartnerService.findBranchOfficeList(type, userTypeId,userId,ascClause);
        }
        } catch (Exception e)
        {
             logger.error("getBranchOfficeList : "+logUserId+" : "+e);
        }
        logger.debug("Branch Office List: "+officeList);
        logger.debug("getBranchOfficeList : "+logUserId+" Ends ");
        return officeList;
    }

    public List<Object[]> getBranchOfficeList(String type,int userTypeId,int userId,String searchField,String searchString,String searchOper) {
        logger.debug("getBranchOfficeList : "+logUserId+" Starts ");
        try {
        if (officeList.isEmpty()) {
                    if(userTypeId!=1){
                        ScBankDevpartnerSrBean scBankCreationSrBean = new ScBankDevpartnerSrBean();
                        scBankCreationSrBean.getBankName(userId, (byte) userTypeId);
                        userId = scBankCreationSrBean.getsBankDevelopId();
                    }
            officeList=scBankDevpartnerService.findBranchOfficeList(type, userTypeId, userId, searchField, searchString, searchOper);
        }
        } catch (Exception e) {
             logger.error("getBranchOfficeList : "+logUserId+" : "+e);
        }
        logger.debug("Branch Office List: "+officeList);
        logger.debug("getBranchOfficeList : "+logUserId+" Ends ");
        return officeList;
    }
}
