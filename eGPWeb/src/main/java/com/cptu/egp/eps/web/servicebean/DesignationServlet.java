/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblDesignationMaster;
import com.cptu.egp.eps.model.table.TblGradeMaster;
import com.cptu.egp.eps.service.serviceinterface.DesignationMasterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class DesignationServlet extends HttpServlet
{
static final Logger LOGGER = Logger.getLogger(DesignationServlet.class);

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        
        }

    // <editor-fold defaultstate="expanded" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        PrintWriter out = response.getWriter();
        String funName = request.getParameter("funName");
        DesignationMasterService designationMasterService = (DesignationMasterService) AppContext.getSpringBean("DesignationMasterService");
        designationMasterService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
        String logUserId = "0";
        try {
            if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
                designationMasterService.setUserId(logUserId);
            }
            if (funName != null && funName.equalsIgnoreCase("verifyDesignation")) {
                LOGGER.debug(funName+" : "+logUserId+" Starts");
                String desigName = request.getParameter("designation");
                String department = request.getParameter("department");
                Short departmentId = Short.parseShort(department);

                boolean flag = designationMasterService.verifyDesignation(desigName, departmentId);
                if (flag) {
                    out.print("Designation already exist");
                }
                else {
                    out.print("OK");
                }
                LOGGER.debug(funName+" : "+logUserId+" Ends");
                out.flush();
            }
            else {
                String action = request.getParameter("action");
                String department = request.getParameter("txtdepartmentid");
                Short departmentId = 0;
                int createdBy=0;
                Date createdDate=new Date();
                LOGGER.debug(action+" : "+logUserId+" Starts");
                if (action != null && !action.isEmpty() && action.equalsIgnoreCase("update")) {
                    departmentId = Short.parseShort(department);
                    createdBy=Integer.parseInt(request.getParameter("createdBy"));
                    String strDate=request.getParameter("createdDate");
                   SimpleDateFormat sd=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                    try {
                        createdDate = sd.parse(strDate);
                    }
                    catch (ParseException ex) {
                        LOGGER.error(action+" : "+logUserId,ex);
                    }
                }
                else {
                    departmentId = Short.parseShort(request.getParameter("txtdepartmentid"));
                    HttpSession session=request.getSession();
                    Object objCreatedBy=session.getAttribute("userId");
                    createdBy=Integer.parseInt(objCreatedBy.toString());
                }
                Short grade = Short.parseShort(request.getParameter("grade"));
                String designation = request.getParameter("designation");

                int designationId = 0;

                if (action != null && !action.isEmpty() && action.equalsIgnoreCase("update")) {
                    String strDesignationId = request.getParameter("designationId");
                    if (strDesignationId != null && !strDesignationId.isEmpty()) {
                        designationId = Integer.parseInt(strDesignationId);
                    }
                }
                TblDesignationMaster tblDesignationMaster = null;

                if (action != null && !action.isEmpty() && action.equalsIgnoreCase("update")) {
                    tblDesignationMaster = new TblDesignationMaster(designationId);
                }
                else {
                    tblDesignationMaster = new TblDesignationMaster();
                }

                tblDesignationMaster.setDesignationName(designation);
                tblDesignationMaster.setTblDepartmentMaster(new TblDepartmentMaster(departmentId));
                tblDesignationMaster.setTblGradeMaster(new TblGradeMaster(grade));
                tblDesignationMaster.setCreatedBy(createdBy);
                tblDesignationMaster.setCreatedDate(createdDate);
                try {
                    if (action != null && !action.isEmpty() && action.equalsIgnoreCase("update")) {
                        designationMasterService.updateDesignation(tblDesignationMaster);
                        response.sendRedirect("admin/ViewDesignation.jsp?designationId=" + designationId + "&msg=success&from=updated");
                    }
                    else {
                        designationId = designationMasterService.createDesignation(tblDesignationMaster);
                        response.sendRedirect("admin/ViewDesignation.jsp?designationId=" + designationId + "&msg=success&from=created");
                    }
                }
                catch (Exception ex) {
                    LOGGER.error(action+" : "+logUserId,ex);
                    if (action != null && !action.isEmpty() && action.equalsIgnoreCase("update")) {
                        response.sendRedirect("admin/EditDesignation.jsp?designationId=" + designationId + "&msg=fail");
                    }
                    response.sendRedirect("admin/CreateDesignation.jsp?msg=fail");
                }
                LOGGER.debug(action+" : "+logUserId+" Ends");
            }
        }
        finally {
            out.close();
            designationMasterService = null;
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
}
