/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.daointerface.TblLoginMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblEmailVerificationCode;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblSessionMaster;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.ForgotPasswordService;
import com.cptu.egp.eps.service.serviceinterface.ResetCodeService;
import com.cptu.egp.eps.service.serviceinterface.UserLoginService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.*;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author parag
 */
public class LoginSrBean extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    static final Logger logger = Logger.getLogger(LoginSrBean.class);
    UserLoginService userLoginService = (UserLoginService) AppContext.getSpringBean("UserLoginService");
    UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    ResetCodeService resetCodeService = (ResetCodeService) AppContext.getSpringBean("ResetCodeService");
    private ForgotPasswordService forgotPasswordService = (ForgotPasswordService) AppContext.getSpringBean("ForgotPasswordService");
    private String logUserId = "0";
    private String strUserId="userId";
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        userLoginService.setAuditTrail(auditTrail);
    }
    
    


    /**
     * getter
     * @return
     */
    public UserLoginService getUserLoginService() {
        return userLoginService;
    }

    /**
     * setter
     * @param userLoginService
     */
    public void setUserLoginService(UserLoginService userLoginService) {
        this.userLoginService = userLoginService;
    }
    TblLoginMasterDao loginMasterDao = (TblLoginMasterDao) AppContext.getSpringBean("TblLoginMasterDao");

    /**
     * getter
     * @return
     */
    public TblLoginMasterDao getLoginMasterDao() {
        return loginMasterDao;
    }

    /**
     * setter
     * @param loginMasterDao
     */
    public void setLoginMasterDao(TblLoginMasterDao loginMasterDao) {
        this.loginMasterDao = loginMasterDao;
    }

    /**
     * Servlet to handle the longin reqeust
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String action = request.getParameter("action");

        String param1 = "";
        if (request.getParameter("param1") != null && !"".equals(request.getParameter("param1"))) {
            param1 = request.getParameter("param1");
        }

        String param2 = "";
        if (request.getParameter("param2") != null && !"".equals(request.getParameter("param2"))) {
            param2 = request.getParameter("param2");
        }

        String funName = "";
        if (request.getParameter("funName") != null && !"".equals(request.getParameter("funName"))) {
            funName = request.getParameter("funName");
        }

        HttpSession session;
        List<TblLoginMaster> loginMasters = new ArrayList<TblLoginMaster>();
        try {
            if(request.getSession().getAttribute(strUserId)!=null){
                logUserId = request.getSession().getAttribute(strUserId).toString();
                userRegisterService.setUserId(logUserId);
                userLoginService.setUserId(logUserId);
                forgotPasswordService.setLogUserId(logUserId);
            }
            logger.debug(action+" : "+logUserId+" Starts");
            if ("checkLogin".equalsIgnoreCase(action)) {
                session = request.getSession(false);
                
                if(session.getAttribute("userTypeId") != null)
                {
                    response.sendRedirect("Index.jsp?ExistsSession=y");
                    return;
                }
                String returnValue = "";
                String emailId = request.getParameter("emailId");
                String password = request.getParameter("password");
                String ipAddress = request.getHeader("X-FORWARDED-FOR");
              //  if(funName.equalsIgnoreCase("verifyCodeLogin"))
              //  {
              //       resetCodeService.verifyResetCode(emailId, request.getParameter("verificationCode"));

              //  }
                if(!funName.equalsIgnoreCase("verifyCodeLogin") || resetCodeService.verifyResetCode(emailId, request.getParameter("verificationCode")))
                {
                if (ipAddress == null) {
                    ipAddress = request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr();
                }
                try {
                    if (emailId != null && password != null) {
                        returnValue = userLoginService.checkLoginStatus(emailId, SHA1HashEncryption.encodeStringSHA1(password), ipAddress);
                    }
                    if (returnValue.contains(".jsp") && !returnValue.equals("Index.jsp")) {
                        boolean isApproved = false;
                        boolean isInComplete = false;
                        session = request.getSession(true);
                        loginMasters = loginMasterDao.findTblLoginMaster("emailId", Operation_enum.EQ, emailId);
                        for (TblLoginMaster tblLoginMaster : loginMasters) {
                            if(tblLoginMaster.getTblUserTypeMaster().getUserTypeId() == 3)
                            {
                                CommonService commonService_media = (CommonService) AppContext.getSpringBean("CommonService");
                                String role = "";
                                role = commonService_media.getUserRoleByUserId(tblLoginMaster.getUserId());
                                if(role.contains("HOPA") && !loginMasters.get(0).getFirstLogin().equalsIgnoreCase("yes"))
                                {
                                    returnValue = "resources/common/PaDashboard.jsp?viewtype=pending";
                                }
                            }
                            if("approved".equalsIgnoreCase(tblLoginMaster.getStatus())){
                                isApproved = true;
                            }
                            if("incomplete".equalsIgnoreCase(tblLoginMaster.getStatus())){
                                isInComplete = true;
                            }
                            if(isApproved || isInComplete){
                                session.setAttribute("emailId", emailId);
                                session.setAttribute(strUserId, tblLoginMaster.getUserId());
                                logUserId = session.getAttribute(strUserId).toString();
                                session.setAttribute("userTypeId", tblLoginMaster.getTblUserTypeMaster().getUserTypeId());
                                Date lastLoginDt = userLoginService.getLastLoginTime(tblLoginMaster.getUserId());
                                if (lastLoginDt != null) {
                                    //String lastLoginStr=DateUtils.formatDate(lastLoginDt);
                                    session.setAttribute("lastLogin", lastLoginDt);
                                } else {
                                    session.setAttribute("lastLogin", new Date());
                                    //session.setAttribute("lastLogin", DateUtils.convertStrToDate(new Date()));
                                }
                                int sessionId = insrtSessionMaster(tblLoginMaster.getUserId(), ipAddress);
                                session.setAttribute("sessionId", sessionId);
                            }

                            if(isApproved){
                                CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                commonService.setUserId(logUserId);
                                String name = commonService.getUserName(tblLoginMaster.getUserId(), tblLoginMaster.getTblUserTypeMaster().getUserTypeId());
                                int userTypeId = tblLoginMaster.getTblUserTypeMaster().getUserTypeId();
                                String govUserId=null;
                              //  System.out.println(" usernmae="+name+" userTypeId="+userTypeId+" value="+name.indexOf("%$"));
                                if(name.length()!=0 &&  (userTypeId==3 || userTypeId==6 || userTypeId==7 || userTypeId==15 || userTypeId==16 || userTypeId==17 || userTypeId==18)){
                                    govUserId = name.substring(0, name.indexOf("%$"));
                                    name = name.substring(name.indexOf("%$")+2,name.length());

                                }
                                session.setAttribute("userName", name);
                                if(userTypeId==14){
                                    session.setAttribute("govUserId",0);
                                }
                                if(govUserId!=null){
                                    session.setAttribute("govUserId", govUserId);
                                }
//                                Date lastLoginDt = userLoginService.getLastLoginTime(tblLoginMaster.getUserId());
//                                if (lastLoginDt != null) {
//                                    //String lastLoginStr=DateUtils.formatDate(lastLoginDt);
//                                    session.setAttribute("lastLogin", lastLoginDt);
//                                } else {
//                                    session.setAttribute("lastLogin", new Date());
//                                    //session.setAttribute("lastLogin", DateUtils.convertStrToDate(new Date()));
//                                }
                                /*int sessionId = insrtSessionMaster(tblLoginMaster.getUserId(), ipAddress);
                                session.setAttribute("sessionId", sessionId);*/
                            }
                            //logger.debug(" userId is  :: " + session.getAttribute(strUserId).toString());
                        }
                        try {
                            if(session.getAttribute("userTypeId") != null && "3".equalsIgnoreCase(session.getAttribute("userTypeId").toString())){
                                    TenderCommonService tenCommonServiceAfterLoginTop = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                    tenCommonServiceAfterLoginTop.setLogUserId(logUserId);
                                    List<SPTenderCommonData> details = tenCommonServiceAfterLoginTop.returndata("GetEmpRoles", session.getAttribute(strUserId).toString(), "");
                                    String chk = details.get(0).getFieldName1();
                                    session.setAttribute("procurementRole", chk);
                                    details = null;
                                    tenCommonServiceAfterLoginTop = null;
                                }

                        } catch (Exception e) {
                             logger.error(action+" : "+logUserId+" : "+e);
                        }
                        
                        try {
                            if(session.getAttribute("userTypeId") != null && "8".equalsIgnoreCase(session.getAttribute("userTypeId").toString())){
                                
                                ServletContext scxt = session.getServletContext();
                                String webInfPath = scxt.getRealPath("resources");
                                
                                File file = new File(webInfPath + "\\ContentAdminViewDocs\\"+ session.getAttribute("userId").toString());
                                
                                if (file.exists()){
                                    File[] contents = file.listFiles();
                                    if (contents != null) {
                                        for (File fl : contents) {
                                            File[] cont = fl.listFiles();
                                            if (cont != null) {
                                                for (File f : cont) {
                                                    f.delete();
                                                }
                                            }
                                            fl.delete();
                                        }
                                    }       
                                    file.delete();                                
                                }
                            }

                        } catch (Exception e) {
                             logger.error(action+" : "+logUserId+" : "+e);
                        }
                        
                       // System.out.println("url"+request.getRequestURL().toString());
                        if (request.getRequestURL().toString().contains("training.eprocure.gov.bd")
                             || request.getRequestURL().toString().contains("staging.eprocure.gov.bd")
                             || request.getRequestURL().toString().contains("development.eprocure.gov.bd")
                             || request.getRequestURL().toString().contains("192.168.100.7")
                             || request.getRequestURL().toString().contains("122.170.119.66")
                             ) {
                            if (request.getRequestURL().toString().contains("training.eprocure.gov.bd")
                             || request.getRequestURL().toString().contains("staging.eprocure.gov.bd")
                             || request.getRequestURL().toString().contains("development.eprocure.gov.bd")
                             || returnValue.contains("CompanyDetails.jsp") || returnValue.contains("PersonalDetails.jsp")
                             || returnValue.contains("SupportingDocuments.jsp") || returnValue.contains("FinalSubmission.jsp")
                             || returnValue.contains("IndividualConsultant.jsp")
                             || request.getRequestURL().toString().contains("192.168.100.7")
                             || request.getRequestURL().toString().contains("122.170.119.66:8090")
                               ) {
                                System.out.println("*---->urltoredirect");
                                if(request.getRequestURL().toString().contains("192.168.100.7"))
                                {
                                    response.sendRedirect("http://192.168.100.7:8080/" + returnValue);
                                }else if(request.getRequestURL().toString().contains("122.170.119.66:8090")) {
                                    response.sendRedirect("http://61.17.38.85:8090/" + returnValue);
                                }else if(request.getRequestURL().toString().contains("development.eprocure.gov.bd")) {
                                    response.sendRedirect("http://development.eprocure.gov.bd/" + returnValue);
                                }
                               // else if(request.getRequestURL().toString().contains("staging.eprocure.gov.bd/eGP")) {
                                   // response.sendRedirect("http://staging.eprocure.gov.bd/eGP/" + returnValue);
                               // }
                                else if(request.getRequestURL().toString().contains("staging.eprocure.gov.bd")) {
                                    response.sendRedirect("https://www.staging.eprocure.gov.bd/" + returnValue);
                                }
                                else if(request.getRequestURL().toString().contains("training.eprocure.gov.bd")) {
                                    response.sendRedirect("https://www.training.eprocure.gov.bd/" + returnValue);
                                }
                                else {
                                    response.sendRedirect(XMLReader.getMessage("urltoredirect") + returnValue);
                                }
                            } else {
                                if( request.getRequestURL().toString().contains("122.170.119.66") )
                                {
                                    response.sendRedirect("https://122.170.119.66/" + returnValue);
                                }
                                else if ( request.getRequestURL().toString().contains("www.eprocure.gov.bd"))
                                {
                                    response.sendRedirect("https://www.eprocure.gov.bd/" + returnValue);
                                }
                                else
                                {
                                    response.sendRedirect(XMLReader.getMessage("urltoredirecthttps") + returnValue);
                                }
                            }
                        } else {
                            response.sendRedirect(returnValue);
                        }
                    } else {
                        //response.sendRedirect("Index.jsp?returnValue="+returnValue);
                        if(returnValue.equals("4")){
                            String result = sendVerifySMS(emailId,"Dear User, Someone has been trying to login into your e-GP account.","");
                            response.sendRedirect("Index.jsp?returnValue="+returnValue);
                            //if(!result.equals("Error occured"))
                            //    response.sendRedirect("/VerifyCode.jsp?msg=send");
                        }
                        else if (returnValue.equals("4a")){
                           String result = sendVerifySMS(emailId,"Dear User, Your e-GP account login verification code is:","code");
                           if(result.equals("ok"))
                                {
                                session = request.getSession(false);
                                session.setAttribute("pass", request.getParameter("password"));
                                session.setAttribute("email",request.getParameter("emailId"));
                                response.sendRedirect("/VerifyCode.jsp?msg=send");
                                }
                           else
                                response.sendRedirect("Index.jsp?returnValue="+result);
                               //response.sendRedirect("/VerifyCode.jsp?msg=send");
                        }
                        else
                        {
                        String[] item = request.getHeader("Referer").split("\\?");
                        response.sendRedirect(item[0] + "?returnValue=" + returnValue);
                        }
                    }
                } catch (Exception e) {
                    logger.error(action+" : "+logUserId+" : "+e);
                    e.printStackTrace();
                }
              }
              else if(!resetCodeService.verifyResetCode(emailId, request.getParameter("verificationCode"))){
                  session = request.getSession(false);
                  session.setAttribute("pass", request.getParameter("password"));
                  session.setAttribute("email",request.getParameter("emailId"));
                  response.sendRedirect("/VerifyCode.jsp?msg=failSend");
                    
              }
            } else if ("changePassword".equalsIgnoreCase(action)) {
                //session = request.getSession(true);
                session = request.getSession();
                int userId = Integer.parseInt(session.getAttribute(strUserId).toString());
                try {
                    loginMasters = loginMasterDao.findTblLoginMaster(strUserId, Operation_enum.EQ, userId);
                    if(session.getAttribute("sessionId")!=null){
                        setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
                    }
                    TblLoginMaster master = new TblLoginMaster();
                    for (TblLoginMaster tblLoginMaster : loginMasters) {
                        BeanUtils.copyProperties(tblLoginMaster, master);
                    }
                    master = userRegisterService.cloneTblLoginMaster(master);
                    String password = request.getParameter("NewPass");
                    // TblLoginMaster tblLoginMaster = new TblLoginMaster();
                    master.setPassword(SHA1HashEncryption.encodeStringSHA1(password));
                    if (request.getParameter("HintQuestion") != null) {
                        master.setHintQuestion(request.getParameter("HintQuestion"));
                    }if("other".equals(request.getParameter("HintQuestion")) && request.getParameter("OtherHint") != null){
                            master.setHintQuestion(request.getParameter("OtherHint"));
                        }
                    if (request.getParameter("HintAns") != null) {
                        master.setHintAnswer(request.getParameter("HintAns"));
                    }
                    master.setFirstLogin("no");
                    master.setTblTempTendererMasters(null);
                    boolean flag = userLoginService.updateLoginMaster(master,"changePass");
                    master = null;
                    if (flag) {
                        if (loginMasters.get(0).getFirstLogin().equalsIgnoreCase("yes")) {
                             //SendMessageUtil sendMessageUtil = new SendMessageUtil();
                             MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                             int userTypeId =  0;
                             String userRegType = "";

                             if(!loginMasters.isEmpty()){
                                  userRegType = loginMasters.get(0).getRegistrationType();
                             }
                             
                             if(request.getSession()!=null){
                                userTypeId=Integer.parseInt(request.getSession().getAttribute("userTypeId").toString());
                             }
                             
                             if(loginMasters.get(0).getTblUserTypeMaster().getUserTypeId()==3){
                                    userRegisterService.contentAdmMsgBox(loginMasters.get(0).getEmailId(), XMLReader.getMessage("msgboxfrom"), "Welcome to e-GP Portal.", msgBoxContentUtility.firstGovtLogin());
                             }else if(loginMasters.get(0).getTblUserTypeMaster().getUserTypeId()==4 || loginMasters.get(0).getTblUserTypeMaster().getUserTypeId()==5 ||
                                      loginMasters.get(0).getTblUserTypeMaster().getUserTypeId()==6 || loginMasters.get(0).getTblUserTypeMaster().getUserTypeId()==7 ||
                                      loginMasters.get(0).getTblUserTypeMaster().getUserTypeId()==8 || loginMasters.get(0).getTblUserTypeMaster().getUserTypeId()==15){
                                 userRegisterService.contentAdmMsgBox(loginMasters.get(0).getEmailId(), XMLReader.getMessage("msgboxfrom"), "Welcome to e-GP Portal.", msgBoxContentUtility.firstAdminLogin(userTypeId));
                             }else if(userTypeId == 14){
                                UserRegisterService service = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                                //MsgBoxContentUtility msgBoxContentUtilityExt = new MsgBoxContentUtility();
                                service.contentAdmMsgBox(loginMasters.get(0).getEmailId(), XMLReader.getMessage("msgboxfrom"), "Welcome to e-GP System", msgBoxContentUtility.firstAdminLogin(userTypeId));
                            }else if(userTypeId == 2){
                                if("media".equalsIgnoreCase(userRegType)){
                                    userRegisterService.contentAdmMsgBox(loginMasters.get(0).getEmailId(), XMLReader.getMessage("msgboxfrom"), "Welcome to e-GP Portal.", msgBoxContentUtility.getMediaUserContent());
                                }else{
                                userRegisterService.contentAdmMsgBox(loginMasters.get(0).getEmailId(), XMLReader.getMessage("msgboxfrom"), "Welcome to e-GP Portal.", msgBoxContentUtility.multipleUsers());
                                }
                            } else {
                                userRegisterService.contentAdmMsgBox(loginMasters.get(0).getEmailId(), XMLReader.getMessage("msgboxfrom"), "Welcome to e-GP Portal.", msgBoxContentUtility.firstLogin());
                            }

                            //SendMessageUtil sendMessageUtil = new SendMessageUtil();
//                          MailContentUtility mailContentUtility = new MailContentUtility();
//                          String url=request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/"));
//                          String mailText = mailContentUtility.firstLoing();
//                          sendMessageUtil.setEmailTo(mails);
//                          sendMessageUtil.setEmailSub("Welcome to e-GP Portal.");
//                          sendMessageUtil.setEmailMessage(mailText);
//                          sendMessageUtil.sendEmail();
                        }
                            session.invalidate();
                        
                        response.sendRedirect("Index.jsp?returnValue=Pass");
                    } else {
                        response.sendRedirect("ChangePassword.jsp");
                    }
                    // logger.debug("  in changePassword  updated-------------------->>");
                } catch (Exception e) {
                    logger.error(action+" : "+logUserId+" : "+e);
                }
            } else if ("checkHintQusAns".equalsIgnoreCase(action)) {
                session = request.getSession();
                if(session.getAttribute("sessionId")!=null){
                        setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                    }
                int userId = Integer.parseInt(session.getAttribute(strUserId).toString());
                try {
                    loginMasters = loginMasterDao.findTblLoginMaster(strUserId, Operation_enum.EQ, userId);

                    TblLoginMaster master = new TblLoginMaster();
                    for (TblLoginMaster tblLoginMaster : loginMasters) {
                        BeanUtils.copyProperties(tblLoginMaster, master);
                    }
                    master = userRegisterService.cloneTblLoginMaster(master);

                    if (request.getParameter("HintQuestion") != null) {
                        master.setHintQuestion(request.getParameter("HintQuestion"));
                    }
                    if ("other".equals(request.getParameter("HintQuestion")) && request.getParameter("OtherHint") != null) {
                            master.setHintQuestion(request.getParameter("OtherHint"));
                        }
                    if (request.getParameter("HintAns") != null) {
                        master.setHintAnswer(request.getParameter("HintAns"));
                    }
                    master.setTblTempTendererMasters(null);
                    //logger.debug("  in changePassword -------------------->>");
                    boolean flag = userLoginService.updateLoginMaster(master,"hintAns");
                    master = null;
                    if (flag) {
                        response.sendRedirect("resources/common/Dashboard.jsp?msg=hitsuccess");

                    } else {
                        response.sendRedirect("ChangeHintQusAns.jsp");
                    }
                    // logger.debug("  in changePassword  updated-------------------->>");
                } catch (Exception e) {
                    logger.error(action+" : "+logUserId+" : "+e);
                }

            } else if ("checkPassword".equalsIgnoreCase(action)) {
                //session = request.getSession(true);
                session = request.getSession();
                int userId = Integer.parseInt(session.getAttribute(strUserId).toString());
                try {
                    loginMasters = loginMasterDao.findTblLoginMaster(strUserId, Operation_enum.EQ, userId);
                    String txtCurPass = "";
                    if (request.getParameter("txtCurPass") != null) {
                        txtCurPass = request.getParameter("txtCurPass").toString();
                    }
                    if (SHA1HashEncryption.encodeStringSHA1(txtCurPass).equals(loginMasters.get(0).getPassword())) {
                        out.print("Valid password");
                        out.flush();
                    } else {
                        out.print("Invalid password");
                        out.flush();
                    }
                } catch (Exception e) {
                    logger.error(action+" : "+logUserId+" : "+e);
                }
            } else if ("forgotPassword".equalsIgnoreCase(action)) {
                String mailId = request.getParameter("mailId");                
                try {
                    loginMasters = loginMasterDao.findTblLoginMaster("emailId", Operation_enum.LIKE, mailId);
                    TblLoginMaster loginMaster = loginMasters.get(0);
                    String password = request.getParameter("newPass");
                    loginMaster.setPassword(SHA1HashEncryption.encodeStringSHA1(password));
                    loginMasterDao.updateTblLoginMaster(loginMaster);
                    forgotPasswordService.deleteRandomeCode(mailId);
                    response.sendRedirect("Index.jsp?msg=success");
                } catch (Exception ex) {
                   logger.error(action+" : "+logUserId+" : "+ex);
                    session = request.getSession(true);
                    session.setAttribute("emailId", mailId);
                    response.sendRedirect("admin/ChngPassword.jsp?mId=" + request.getParameter("mId") + "msg=fail");
                }
            } else if ("resetPassword".equalsIgnoreCase(action)) {
                String mailId = request.getParameter("mailId");
                session = request.getSession();
                session.removeAttribute("previous");
                try {
                    loginMasters = loginMasterDao.findTblLoginMaster("emailId", Operation_enum.LIKE, mailId);
                    TblLoginMaster loginMaster = loginMasters.get(0);
                    String password = request.getParameter("newPass");
                    loginMaster.setPassword(SHA1HashEncryption.encodeStringSHA1(password));

                    String otherHint = request.getParameter("otherHint");
                    String hintQuestion = "";
                    if (otherHint != null && !otherHint.equals("")) {
                        hintQuestion = otherHint;
                    } else {
                        hintQuestion = request.getParameter("hintQuestion");
                    }
                    String hintAns = request.getParameter("hintAns");
                    if(hintAns!=null || hintQuestion!=null){
                    loginMaster.setHintQuestion(hintQuestion);
                    loginMaster.setHintAnswer(hintAns);
                    }

                    loginMasterDao.updateTblLoginMaster(loginMaster);
                    response.sendRedirect("Index.jsp?returnValue=Pass");
                } catch (Exception ex) {
                    logger.error(action+" : "+logUserId+" : "+ex);
                    //session = request.getSession();
                    session = request.getSession(true);
                    session.setAttribute("emailId", mailId);
                    session.setAttribute("previous", "reset");
                    response.sendRedirect("admin/ChngPassword.jsp?msg=fail");
                }
            } else if ("verifyMail".equalsIgnoreCase(action)) {
                String mailid = request.getParameter("emailId");
                String password = request.getParameter("password");
                String verificationCode = request.getParameter("verificationCode");
                String message = verifyMail(mailid, password, verificationCode);
                String userId = "";
                if (!message.startsWith("Error")) {
                    userId = message.substring(message.indexOf('-') + 1, message.length());
                    message = "succ";
                    session = request.getSession(true);
                    session.setAttribute(strUserId, userId);
                    session.setAttribute("userTypeId", 2);
                    //if("Continue registration process".equalsIgnoreCase(request.getParameter("submit"))){
                    //session=request.getSession(true);
                    //session.setAttribute(strUserId, userId);

                    // message=message.substring(0,message.indexOf("-"))+".jsp";
                    // }else if("No Thanks. I will register later on".equalsIgnoreCase(request.getParameter("submit"))){
                    //   message="Index.jsp";
                    //}
                    String mails[] = {mailid};
                    SendMessageUtil sendMessageUtil = new SendMessageUtil();
                    MailContentUtility mailContentUtility = new MailContentUtility();
                    sendMessageUtil.setEmailTo(mails);
                    sendMessageUtil.setEmailFrom(XMLReader.getMessage("regEmailId"));
                    sendMessageUtil.setEmailSub("e-GP System:  User Registration – Email Verification and Profile Submission Information");
                    sendMessageUtil.setEmailMessage(mailContentUtility.emailVerificationMail(request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/"))));
                    sendMessageUtil.sendEmail();
                    sendMessageUtil = null;
                    mailContentUtility = null;
                    mails = null;
                }
                response.sendRedirect("EmailVerification.jsp?msg=" + message);
            }else if("ChkChngPasswordBidder4Tender".equals(action)){
                Object userId = request.getSession().getAttribute("userId");
                String data = "0";
                if(userId!=null){
                    CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> list = dataMoreService.geteGPDataMore("ChkChngPasswordBidder4Tender",userId.toString());
                    if(list!=null && !list.isEmpty() && list.get(0).getFieldName1().equals("1")){
                        data = "1";
                    }
                }
                out.print(data);
            }else if ("verifyPWD".equalsIgnoreCase(funName)) {
                String hashPwd = HashUtil.getHash(param1, "SHA-1");
                if (this.verifyPWD(hashPwd, param2)) {
                    out.write("1_" + hashPwd);
                } else {
                    out.write("0");
                }
            } else if ("cmpltVeriPrcs".equalsIgnoreCase(funName)) {
                session = request.getSession();
                int uId = 0;
                if (session.getAttribute(strUserId) != null) {
                    uId = Integer.parseInt(session.getAttribute(strUserId).toString());
                }
                try {
                    String nextURL = "Index";
                    List<TblLoginMaster> lMasterLst = loginMasterDao.findTblLoginMaster(strUserId, Operation_enum.EQ, uId);
                    if (lMasterLst != null) {
                        if (!lMasterLst.isEmpty()) {
                            nextURL = lMasterLst.get(0).getNextScreen();
                        }
                        lMasterLst = null;
                    }
                    response.sendRedirect(nextURL + ".jsp");
                } catch (Exception ex) {
                    logger.error(funName+" : "+logUserId+" Complete Verification Process - "+ex);
                    response.sendRedirect("Index.jsp");
                }
            }else if ("userstatus".equalsIgnoreCase(funName)) {
                session = request.getSession();
                session.invalidate();
                response.sendRedirect("UserStatus.jsp?status=succ");
                
            }else if ("resendCode".equalsIgnoreCase(funName)) {
               String result = sendVerifySMS(request.getParameter("emailId").toString(),"Dear User, Your e-GP account login verification code is:","code");
               session = request.getSession(true);
               session.setAttribute("email",request.getParameter("emailId"));
               out.print(result);
            }
            if("".equals(funName)){
                logger.debug(action+" : "+logUserId+" Starts");
            }else{
                logger.debug(funName+" : "+logUserId+" Starts");
            }
        } finally {
            out.close();
            loginMasters = null;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String verifyMail(String mailId, String password, String verificationCode) {
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        commonService.setUserId(logUserId);
        String msg = null;
        //userLoginService.getVeriCode(mailId);
        String verifyMailMsg = commonService.verifyMail(mailId);
        // boolean flag = verifyPWDforVerification(HashUtil.getHash(password, "SHA-1"), mailId);
        int userId = userLoginService.verifyPwdforVerification(HashUtil.getHash(password, "SHA-1"), mailId);
        List<TblEmailVerificationCode> emailVerificationCodes = null;
        if (verifyMailMsg.length() > 2) {
            if (userId != 0) {
                if(userId!= -1){
                emailVerificationCodes = userLoginService.getVeriCode(userId);
                if (emailVerificationCodes != null && (!emailVerificationCodes.isEmpty())) {
                    String verifyCode = emailVerificationCodes.get(0).getRandCode();
                    int emailVerificationId = emailVerificationCodes.get(0).getEmailVerificationId();
                    emailVerificationCodes.clear();
                    emailVerificationCodes = null;
                    String mailCode = HashUtil.getHash(verifyCode, "SHA-1");
                    //mailCode = HashUtil.getHash(mailCode.toString(), "MD5");
                    if (mailCode.equals(verificationCode)) {
                        msg = userLoginService.verifyEmail(mailId, HashUtil.getHash(password, "SHA-1"));
                        deleteVerifyCode(emailVerificationId);                        
                    } else {
                        msg = "Error:Please enter correct Verification Code";
                    }
                } else {
                    msg = "Error: Please enter correct Password";
                }
                } else {
                    msg = "Error: e-mail ID already verified";
                }
            }
            else {
                msg = "Error: Please enter correct Password";
            }
        } else {
            msg = "Error: e-mail ID is not registered with e-GP";

        }
        return msg;
    }

    private int insrtSessionMaster(int userId, String ipAddress) {
        TblSessionMaster tblSessionMaster = new TblSessionMaster();
        tblSessionMaster.setTblLoginMaster(new TblLoginMaster(userId));
        tblSessionMaster.setIpAddress(ipAddress);
        tblSessionMaster.setSessionStartDt(new Date());
        tblSessionMaster.setSessionEndDt(new Date());
        return userLoginService.insertSessionMaster(tblSessionMaster);
    }

    private Boolean verifyPWD(String password, String userId) {

        return userLoginService.verifyPwd(password, Integer.parseInt(userId));
    }

    /*private int verifyPWDforVerification(String password, String mailId) {
        //int userId = 0;
        return userLoginService.verifyPwdforVerification(password, mailId);
    }*/

    private void deleteVerifyCode(int emailVerificationId) {
        userLoginService.deleteVerifyCode(emailVerificationId);
    }

    private String sendVerifySMS(String emailId,String sms,String smsType){
     logger.debug("Send Verify SMS : Starts");
     String result="";
     SendMessageUtil sendMessageUtil = new SendMessageUtil();
     CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
     
            try{
                
                String mail[] = {emailId};
                SPCommonSearchDataMore spcommonSearchData = dataMoreService.getCommonSearchData("getMobNoForSendVerifySMS",emailId).get(0);
                String userId = spcommonSearchData.getFieldName1();
                String mobileNo = spcommonSearchData.getFieldName2();
                int countSMS = Integer.parseInt(spcommonSearchData.getFieldName3());
                
                if(smsType.equals("code")){
                    String resetCode = sendMessageUtil.getRandomName().toString();
                    MailContentUtility mailContentUtility = new MailContentUtility();
                    List list = mailContentUtility.getResetMailContent(resetCode);
                    sendMessageUtil.setEmailTo(mail);
                    sendMessageUtil.setEmailSub(list.get(0).toString());
                    sendMessageUtil.setEmailMessage(list.get(1).toString());
                    sendMessageUtil.sendEmail();
                   
                    //sendMessageUtil.setSmsNo(mobileNo);
                    resetCodeService.storeResetCode(Integer.parseInt(userId), resetCode);
                    //sendMessageUtil.setSmsBody(sms+" "+resetCode+" e-GP Help Desk");
                    //sendMessageUtil.sendSMS();
                    result = "ok";
                    sendMessageUtil=null;
                    mailContentUtility=null;
                    }
                else if (smsType.equals("") && (countSMS<2)){
                    resetCodeService.storeResetCode(Integer.parseInt(userId), "0");
                    //sendMessageUtil.setSmsNo(mobileNo);
                    //sendMessageUtil.setSmsBody(sms+" e-GP Help Desk");
                    //sendMessageUtil.sendSMS();
                    }
                //else if(smsType.equals("code") && countSMS>=5)
                //     result = "over";
       
            }catch(Exception ex){
                AppExceptionHandler expHandler = new AppExceptionHandler();
                expHandler.stack2string(ex);
                result = "error";
            }
            return result;
    }
}
