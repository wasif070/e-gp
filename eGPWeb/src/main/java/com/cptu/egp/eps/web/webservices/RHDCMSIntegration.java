/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.webservices;

import com.cptu.egp.eps.dao.storedprocedure.RHDIntegrationCMSBoQDetails;
import com.cptu.egp.eps.dao.storedprocedure.RHDIntegrationCMSDetails;
import com.cptu.egp.eps.service.serviceinterface.RHDIntegrationCMSDetailsService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.WebServiceUtil;
import com.cptu.egp.eps.web.xmlpojos.WSResponseObject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.apache.log4j.Logger;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.simple.JSONValue;

/**
 ** web services for RHD integration of upto CMS Data with e-GP System
 * @author Sudhir Chavhan
 */
@WebService()
public class RHDCMSIntegration {

    private static final Logger LOGGER = Logger.getLogger(RHDCMSIntegration.class);
    private static final int ZERO_VALUE = 0;
    private String logUserId = "0";
    private static final String EMPTY_STRING = "";
    private static final int WEB_SERVICE_ID = 49;
    private static final String SEARCH_SERVICE_NAME = "RHDIntegrationCMSDetailsService";
 
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    /**
     * This method gives the List of objects of upto CMS data for RHD integration.

     *
     * @param String username
     * @param String password
     * @return WSResponseObject With list data of List<String>
     */
    @WebMethod(operationName = "getCMSDataForRHD")
    public WSResponseObject getCMSDataForRHD(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {

        LOGGER.debug("getCMSDataForRHD : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = "Shared upto CMS data";
        //insert record in log table
        WebServiceUtil.insertLogRecord(WEB_SERVICE_ID, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WEB_SERVICE_ID,
                SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> requiredRHDIntegrationDetailsList = new ArrayList<String>();
            try {
                // get the list of upto CMS data Detail
                requiredRHDIntegrationDetailsList = getPERequiredList();
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.error("getCMSDataForRHD : " + logUserId + e);
            }
            if (requiredRHDIntegrationDetailsList != null) {
                if (requiredRHDIntegrationDetailsList.size() <= 0) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(requiredRHDIntegrationDetailsList);
                }
            }//end IF condition with NULL checking
        }//end IF condition with "isListDataRequired" checking
        LOGGER.debug("getCMSDataForRHD : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

      private List<String> getPERequiredList() throws Exception {

        LOGGER.debug("getPERequiredList : " + logUserId + WSResponseObject.LOGGER_START);
        Map mapObj=new LinkedHashMap();
        String jsonText =null;
        List<String> cmsList = new ArrayList<String>();
        List<RHDIntegrationCMSDetails> RHDIntegrationDetailsList = new ArrayList<RHDIntegrationCMSDetails>();
        try {
        RHDIntegrationCMSDetailsService RHDIntegrationDetailsService =
                (RHDIntegrationCMSDetailsService) AppContext.getSpringBean(SEARCH_SERVICE_NAME);
        RHDIntegrationDetailsList = RHDIntegrationDetailsService.getSharedCMSDetailForRHD();
        if (RHDIntegrationDetailsList != null) {
            for (RHDIntegrationCMSDetails rhdIntegrationDetails : RHDIntegrationDetailsList) {

                    mapObj.put("PE_Id",rhdIntegrationDetails.getPE_Id());
                    mapObj.put("PE_Name",rhdIntegrationDetails.getPE_Name());
                    mapObj.put("PE_Code",rhdIntegrationDetails.getPE_Code());
                    mapObj.put("NoA_Name",rhdIntegrationDetails.getNoA_Name());
                    mapObj.put("Tender_Id",rhdIntegrationDetails.getTender_Id());
                    mapObj.put("Proc_Nature",rhdIntegrationDetails.getProc_Nature());
                    mapObj.put("Proc_Method",rhdIntegrationDetails.getProc_Method());
                    mapObj.put("Proc_Type",rhdIntegrationDetails.getProc_Type());
                    mapObj.put("Budget_Type",rhdIntegrationDetails.getBudget_Type());
                    mapObj.put("Source_Of_Funds",rhdIntegrationDetails.getSource_Of_Funds());
                    mapObj.put("Project_Code",rhdIntegrationDetails.getProject_Code());
                    mapObj.put("Project_Name",rhdIntegrationDetails.getProject_Name());
                    mapObj.put("Tender_RefNo",rhdIntegrationDetails.getTender_RefNo());
                    mapObj.put("Tender_InvitationFor",rhdIntegrationDetails.getTender_InvitationFor());
                    mapObj.put("Tender_PkgNo",rhdIntegrationDetails.getTender_PkgNo());
                    mapObj.put("Tender_PkgDesc",rhdIntegrationDetails.getTender_PkgDesc());
                    mapObj.put("Tender_Category",rhdIntegrationDetails.getTender_Category());
                    mapObj.put("Tender_PubDt",rhdIntegrationDetails.getTender_PubDt());
                    mapObj.put("Tender_DocLstSelD",rhdIntegrationDetails.getTender_DocLstSelD());
                    mapObj.put("Tender_PreMeetingStrtDt",rhdIntegrationDetails.getTender_PreMeetingStrtDt());
                    mapObj.put("Tender_PreMeetingEndDt",rhdIntegrationDetails.getTender_PreMeetingEndDt());
                    mapObj.put("Tender_ClosingDt",rhdIntegrationDetails.getTender_ClosingDt());
                    mapObj.put("Tender_OpeningDt",rhdIntegrationDetails.getTender_OpeningDt());
                    mapObj.put("Tender_SecLstSubDt",rhdIntegrationDetails.getTender_SecLstSubDt());
                    mapObj.put("Tender_BriefDesc",rhdIntegrationDetails.getTender_BriefDesc());
                    mapObj.put("Tender_EvaluationType",rhdIntegrationDetails.getTender_EvaluationType());
                    mapObj.put("Tender_DocAvailable",rhdIntegrationDetails.getTender_DocAvailable());
                    mapObj.put("Tender_DocFees",rhdIntegrationDetails.getTender_DocFees());
                    mapObj.put("Tender_DocPrice",rhdIntegrationDetails.getTender_DocPrice());
                    mapObj.put("Tender_SecAmt",rhdIntegrationDetails.getTender_SecAmt());
                    mapObj.put("Tender_SecPayMode",rhdIntegrationDetails.getTender_SecPayMode());
                    mapObj.put("Tender_SecValidUpToDt",rhdIntegrationDetails.getTender_SecValidUpToDt());
                    mapObj.put("Tender_ValidUpToDt",rhdIntegrationDetails.getTender_ValidUpToDt());
                    mapObj.put("Per_SecValidUpToDt",rhdIntegrationDetails.getPer_SecValidUpToDt());
                    mapObj.put("Per_SecBankName",rhdIntegrationDetails.getPer_SecBankName());
                    mapObj.put("Per_SecBankBranchName",rhdIntegrationDetails.getPer_SecBankBranchName());
                    mapObj.put("Per_PayAmount",rhdIntegrationDetails.getPer_PayAmount());
                    mapObj.put("Per_PayPaidDt",rhdIntegrationDetails.getPer_PayPaidDt());
                    mapObj.put("Per_SecPayMode",rhdIntegrationDetails.getPer_SecPayMode());
                    mapObj.put("Per_PayInstrumentNo",rhdIntegrationDetails.getPer_PayInstrumentNo());
                    mapObj.put("Per_PayIssuingBank",rhdIntegrationDetails.getPer_PayIssuingBank());
                    mapObj.put("Per_PayIssuingBranch",rhdIntegrationDetails.getPer_PayIssuingBranch());
                    mapObj.put("Per_PayIssuanceDt",rhdIntegrationDetails.getPer_PayIssuanceDt());
                    mapObj.put("Contract_No",rhdIntegrationDetails.getContract_No());
                    mapObj.put("Contract_LotNo",rhdIntegrationDetails.getContract_LotNo());
                    mapObj.put("Contract_DescOfLot",rhdIntegrationDetails.getContract_DescOfLot());
                    mapObj.put("Contract_StartDt",rhdIntegrationDetails.getContract_StartDt());
                    mapObj.put("Contract_CompletionDt",rhdIntegrationDetails.getContract_CompletionDt());
                    mapObj.put("Contract_Value",rhdIntegrationDetails.getContract_Value());
                    mapObj.put("NoA_Dt",rhdIntegrationDetails.getNoA_Dt());
                    mapObj.put("NoA_PEName",rhdIntegrationDetails.getNoA_PEName());

                    jsonText = JSONValue.toJSONString(mapObj);
                    System.out.print("upto CMS data jsonText:-"+jsonText);

                    cmsList.add(jsonText.toString());
                }//end FOR loop
            }//end IF condition
        } catch (Exception e) {
            LOGGER.error("getrhdIntegrationDetailsList : " + logUserId + e.toString());
        }
        LOGGER.debug("getrhdIntegrationDetailsList : " + logUserId + WSResponseObject.LOGGER_END);
        return cmsList;
    }
    @WebMethod(operationName = "getBoQByTenderIdForRHD")
    public WSResponseObject getBoQByTenderIdForRHD(
            @WebParam(name = "tenderId") String tenderId) {

        LOGGER.debug("getBoQByTenderIdForRHD : " + logUserId + WSResponseObject.LOGGER_START);
        //System.out.println("getBoQByTenderIdForRHD : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String username = "BoQ for RHD";
        String searchKey = "Shared BoQ data for RHD";
        //insert record in log table
        WebServiceUtil.insertLogRecord(WEB_SERVICE_ID, username, searchKey);
        //checking verification
        wSResponseObject.setIsListRequired(true);
   //     wSResponseObject = WebServiceUtil.initialVerification(WEB_SERVICE_ID,
   //             SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> requiredRHDIntegrationDetailsListBoQ = new ArrayList<String>();
            try {
                // get the list of upto CMS data Detail
                requiredRHDIntegrationDetailsListBoQ = getRequiredListBoQ(tenderId);
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.error("getBoQByTenderIdForRHD : " + logUserId + e);
                //System.out.println("Exception getBoQByTenderIdForRHD : " + logUserId + e);
            }
            if (requiredRHDIntegrationDetailsListBoQ != null) {
                if (requiredRHDIntegrationDetailsListBoQ.size() <= 0) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(requiredRHDIntegrationDetailsListBoQ);
                }
            }//end IF condition with NULL checking
        }//end IF condition with "isListDataRequired" checking
        LOGGER.debug("getBoQByTenderIdForRHD : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

      private List<String> getRequiredListBoQ(String tenderId) throws Exception {

        LOGGER.debug("getRequiredListBoQ : " + logUserId + WSResponseObject.LOGGER_START);
        System.out.println("getRequiredListBoQ : " + logUserId + WSResponseObject.LOGGER_START);
        Map mapObj=new LinkedHashMap();
        BigDecimal totalRate;
        String jsonText =null;
        List<String> BoQList = new ArrayList<String>();
        List<RHDIntegrationCMSBoQDetails> RHDIntegrationDetailsBoQList = new ArrayList<RHDIntegrationCMSBoQDetails>();
        try {
        RHDIntegrationCMSDetailsService RHDIntegrationDetailsService =
                (RHDIntegrationCMSDetailsService) AppContext.getSpringBean(SEARCH_SERVICE_NAME);
        RHDIntegrationDetailsBoQList = RHDIntegrationDetailsService.getBoQForRHD(tenderId);
        if (RHDIntegrationDetailsBoQList != null) {
            for (RHDIntegrationCMSBoQDetails rhdIntegrationDetailsBoQ : RHDIntegrationDetailsBoQList) {
                totalRate = rhdIntegrationDetailsBoQ.getBoQ_ItemQnty().multiply(rhdIntegrationDetailsBoQ.getBoQ_UnitRate());
                    mapObj.put("Tender_Id",tenderId);
                    mapObj.put("BoQ_ItemGroup",rhdIntegrationDetailsBoQ.getBoQ_ItemGroup());
                    mapObj.put("BoQ_ItemSrNo",rhdIntegrationDetailsBoQ.getBoQ_ItemSrNo());
                    mapObj.put("BoQ_ItemDesc",rhdIntegrationDetailsBoQ.getBoQ_ItemDesc());
                    mapObj.put("BoQ_UnitofMeasur",rhdIntegrationDetailsBoQ.getBoQ_UnitofMeasur());
                    mapObj.put("BoQ_ItemQnty",rhdIntegrationDetailsBoQ.getBoQ_ItemQnty());
                    mapObj.put("BoQ_WPstartDt",rhdIntegrationDetailsBoQ.getBoQ_WPstartDt());
                    mapObj.put("BoQ_WPEndDt",rhdIntegrationDetailsBoQ.getBoQ_WPEndDt());
                    mapObj.put("BoQ_WPNoOfDays",rhdIntegrationDetailsBoQ.getBoQ_WPNoOfDays());
                    mapObj.put("wpRate",rhdIntegrationDetailsBoQ.getBoQ_UnitRate());
                    mapObj.put("TotalRate",totalRate.setScale(3, BigDecimal.ROUND_HALF_UP));
                    jsonText = JSONValue.toJSONString(mapObj);
                    System.out.print("BoQ CMS data jsonText:-"+jsonText);

                    BoQList.add(jsonText.toString());
                }//end FOR loop
            }//end IF condition
        } catch (Exception e) {
            //System.out.println("Exception getRequiredListBoQ :-"+e.toString());
            LOGGER.error("getRequiredListBoQ : " + logUserId + e.toString());
        }
        LOGGER.debug("getRequiredListBoQ : " + logUserId + WSResponseObject.LOGGER_END);
        return BoQList;
    }

}
