/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class EmailUtil {

    /** Creates a new instance of SendMessageUtil */
    public EmailUtil() {
    }
    private static final Logger LOGGER = Logger.getLogger(EmailUtil.class);
    public static void sendSingleEMAILByThread(String emailMessage, String emailTo, String emailSub) {
        SendMessageUtil msgClass = new SendMessageUtil();
        msgClass.setSendBy("EMAIL");
        String eM[] = {emailMessage};
        String eT[] = {emailTo};
        String eS[] = {emailSub};
        /*msgClass.setEmailMessage(eM);
        msgClass.setEmailSub(eS);
        msgClass.setEmailTo(eT);*/

        Thread sender = new Thread(msgClass);
        sender.start();
    }

    public static void sendMutipleEMAILByThread(String[] emailMessage, String[] emailTo, String emailSub[]) {
        SendMessageUtil msgClass = new SendMessageUtil();
        msgClass.setSendBy("EMAIL");
        /*msgClass.setEmailMessage(emailMessage);
        msgClass.setEmailSub(emailSub);*/
        msgClass.setEmailTo(emailTo);
        Thread sender = new Thread(msgClass);
        sender.start();
    }
    public static int encryptMail(String email) {
        int i = email.indexOf("@");
        String mail= email.substring(i+1, email.length())+"$"+email.substring(0, i);
        LOGGER.error(mail);
        return mail.hashCode();
    }
}
