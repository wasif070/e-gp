/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.model.table.TblCmsInvoiceMaster;
import com.cptu.egp.eps.model.table.TblCmsSrvAttSheetDtl;
import com.cptu.egp.eps.model.table.TblCmsSrvAttSheetMaster;
import com.cptu.egp.eps.model.table.TblCmsSrvCchistory;
import com.cptu.egp.eps.model.table.TblCmsSrvCcvari;
import com.cptu.egp.eps.model.table.TblCmsSrvCnsltComp;
import com.cptu.egp.eps.model.table.TblCmsSrvFormMap;
import com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch;
import com.cptu.egp.eps.model.table.TblCmsSrvPshistory;
import com.cptu.egp.eps.model.table.TblCmsSrvPsvari;
import com.cptu.egp.eps.model.table.TblCmsSrvReExHistory;
import com.cptu.egp.eps.model.table.TblCmsSrvReExpense;
import com.cptu.egp.eps.model.table.TblCmsSrvRevari;
import com.cptu.egp.eps.model.table.TblCmsSrvSalaryRe;
import com.cptu.egp.eps.model.table.TblCmsSrvSrvari;
import com.cptu.egp.eps.model.table.TblCmsSrvSshistory;
import com.cptu.egp.eps.model.table.TblCmsSrvSsvari;
import com.cptu.egp.eps.model.table.TblCmsSrvStaffDaysCnt;
import com.cptu.egp.eps.model.table.TblCmsSrvStaffSch;
import com.cptu.egp.eps.model.table.TblCmsSrvTchistory;
import com.cptu.egp.eps.model.table.TblCmsSrvTcvari;
import com.cptu.egp.eps.model.table.TblCmsSrvTeamComp;
import com.cptu.egp.eps.model.table.TblCmsSrvWorkPlan;
import com.cptu.egp.eps.model.table.TblCmsSrvWphistory;
import com.cptu.egp.eps.model.table.TblCmsSrvWpvari;
import com.cptu.egp.eps.model.table.TblCmsVariContractVal;
import com.cptu.egp.eps.model.table.TblCmsVariationOrder;
import com.cptu.egp.eps.model.table.TblCmsWpDetail;
import com.cptu.egp.eps.model.table.TblCmsWpMaster;
import com.cptu.egp.eps.model.table.TblCmsWptenderBoqmap;
import com.cptu.egp.eps.model.table.TblReviewPanel;
import com.cptu.egp.eps.model.table.TblTenderCells;
import com.cptu.egp.eps.service.serviceimpl.CMSService;
import com.cptu.egp.eps.service.serviceimpl.ConsolodateService;
import com.cptu.egp.eps.service.serviceinterface.AccPaymentService;
import com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.ComplaintMgmtService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.servicebean.TenderTablesSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.DateUtils;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.cptu.egp.eps.web.utility.CalculateMonths;

/**
 *
 * @author shreyansh Jogi
 */
public class CMSSerCaseServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
    private ConsolodateService c_ConsSrv = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
    private CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
    private UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    private AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            int tenderId = 0;
            int lotId = 0;
            int wpId = 0;
            int formMapId = 0;
            int srvBoqId = 0;
            int srvFormMapId = 0;
            int varOrdId = 0;
            if (request.getParameter("tenderId") != null) {
                tenderId = Integer.parseInt(request.getParameter("tenderId"));
            }
            if (request.getParameter("lotId") != null) {
                lotId = Integer.parseInt(request.getParameter("lotId"));
            }
            if (request.getParameter("wpId") != null) {
                wpId = Integer.parseInt(request.getParameter("wpId"));
            }
            if (request.getParameter("srvFormMapId") != null) {
                srvFormMapId = Integer.parseInt(request.getParameter("srvFormMapId"));
                formMapId = Integer.parseInt(request.getParameter("srvFormMapId"));
            }
            if (request.getParameter("SrvFormMapId") != null) {
                formMapId = Integer.parseInt(request.getParameter("SrvFormMapId"));
                srvFormMapId = Integer.parseInt(request.getParameter("SrvFormMapId"));
            }
            if (request.getParameter("srvBoqId") != null) {
                srvBoqId = Integer.parseInt(request.getParameter("srvBoqId"));
            }
            if (request.getParameter("varOrdId") != null) {
                varOrdId = Integer.parseInt(request.getParameter("varOrdId"));
            }
            String srvType = commonService.getServiceTypeForTender(tenderId);
            String action = request.getParameter("action");
            HttpSession session = request.getSession();
            Object user = session.getAttribute("userId");
            Object userType = session.getAttribute("userTypeId");
            String userId = null;
            String userTypeId = null;
            if (user == null) {
                userId = "";
            } else {

                userId = user.toString();
            }
            if (userType == null) {
                userTypeId = "";
            } else {

                userTypeId = userType.toString();
            }
            int conId = c_ConsSrv.getContractId(tenderId);
            c_ConsSrv.setLogUserId(userId);
            cmss.setLogUserId(userId);
            if ("SrvReExpenses".equalsIgnoreCase(action)) {
                try {
                    String Quantity[] = request.getParameterValues("qtytxt");
                    String unitcost[] = request.getParameterValues("unitCosttxt");
                    String muliply[] = request.getParameterValues("muliplytxt");
                    String srvReId[] = request.getParameterValues("srvReId");
                    List<TblCmsSrvReExHistory> historys = new ArrayList<TblCmsSrvReExHistory>();
                    List<Object[]> os = cmss.getSrvREHistDistCount(srvFormMapId);
                    List<TblCmsSrvReExpense> reExpenses = cmss.getSrvReExpensesData(srvFormMapId);
                    Object[] osObj = os.get(0);
                    int cnt = (Integer) osObj[0] + 1;
                    for (int j = 0; j < srvReId.length; j++) {
                        TblCmsSrvReExpense expense = reExpenses.get(j);
                        if (!expense.getQty().toString().equals(Quantity[j].toString())
                                || !expense.getUnitCost().toString().equals(unitcost[j].toString())) {
                            TblCmsSrvReExHistory srvReExHistory = new TblCmsSrvReExHistory();
                            srvReExHistory.setCatagoryDesc(expense.getCatagoryDesc());
                            srvReExHistory.setDescription(expense.getDescription());
                            srvReExHistory.setHistCnt(cnt);
                            srvReExHistory.setQty(new BigDecimal(Quantity[j].toString()));
                            srvReExHistory.setSrNo(expense.getSrNo());
                            srvReExHistory.setSrvFormMapId(srvFormMapId);
                            srvReExHistory.setSrvReid(expense.getSrvReid());
                            srvReExHistory.setTotalAmt(new BigDecimal(muliply[j].toString()));
                            srvReExHistory.setUnit(expense.getUnit());
                            srvReExHistory.setUnitCost(new BigDecimal(unitcost[j].toString()));
                            srvReExHistory.setCreatedDate(new java.sql.Date(new java.util.Date().getTime()));
                            historys.add(srvReExHistory);
                        }
                        cmss.updateSrvReExpensesData(Integer.parseInt(srvReId[j].toString()), Quantity[j].toString(), unitcost[j].toString(), muliply[j].toString());
                    }
                    cmss.AddToREHistory(historys);
                    sendMailforEditWorkSch(Integer.toString(tenderId), Integer.toString(lotId), user.toString(), Integer.toString(srvFormMapId));
                    response.sendRedirect("officer/SrvReExpense.jsp?tenderId=" + tenderId + "&lotId=" + lotId + "&srvFormMapId=" + srvFormMapId + "&msg=succ");
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), "Edit Reimbursable Expenses", "");
                }

            } else if ("placeOrder".equalsIgnoreCase(action)) {
                long Varno = cmss.countVariationOrderForSrv(Integer.parseInt(request.getParameter("tenderId"))) + 1;
                TblCmsVariationOrder order = new TblCmsVariationOrder();
                order.setCreatedBy(Integer.parseInt(userId));
                order.setTenderId(Integer.parseInt(request.getParameter("tenderId")));
                order.setVariOrdCreationDt(new Date());
                order.setVariOrdStatus("pending");
                order.setVariOrdWFStatus("pending");
                order.setVariationNo("Variation Order - " + Varno);
                service.addToVariationTable(order);
                response.sendRedirect("officer/WorkScheduleMain.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=placed");

            } else if ("finalise".equalsIgnoreCase(action)) {
                try{
                int varId = Integer.parseInt(request.getParameter("varId"));
                cmss.updateVarOrderWithStatus(varId, "finalise");
                sendMailForVariationOrderFromPE(Integer.toString(tenderId), Integer.toString(lotId), user.toString());
                response.sendRedirect("officer/WorkScheduleMain.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=finlise");
                }finally{
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), "Finalize Variation Order", "");
                }
            } else if ("SrvConsltCompo".equalsIgnoreCase(action)) {
                try {
                    String forms = request.getParameter("forms");
                    int flag = 0;
                    String inNoOfKeyProfstafftxt[] = request.getParameterValues("inNoOfKeyProfstafftxt");
                    String monthstxt[] = request.getParameterValues("monthstxt");
                    String noOffStafftxt[] = request.getParameterValues("noOffStafftxt");
                    String noOffSupportStafftxt[] = request.getParameterValues("noOffSupportStafftxt");
                    String Consltnametxt[] = request.getParameterValues("Consltnametxt");
                    String NoOffKeyPrStafftxt[] = request.getParameterValues("NoOffKeyPrStafftxt");
                    String NoOffSupportPrStafftxt[] = request.getParameterValues("NoOffSupportPrStafftxt");
                    String srvCCId[] = request.getParameterValues("srvCCId");
                    List<Object[]> list = cmss.getSrvCCHistDistCount(srvFormMapId);
                    List<TblCmsSrvCnsltComp> comps = cmss.getSrvConsltCompoData(srvFormMapId);
                    List<TblCmsSrvCchistory> list1 = new ArrayList<TblCmsSrvCchistory>();
                    TblCmsSrvCchistory cchistory = null;
                    for (int j = 0; j < srvCCId.length; j++) {
                        TblCmsSrvCnsltComp cnsltComp = comps.get(j);
                        int cnt = 0;
                        if (!cnsltComp.getConsultantName().equals(Consltnametxt[j].toString())) {
                            cnt++;
                        }
                        if (cnsltComp.getKeyNos() != Integer.parseInt(NoOffKeyPrStafftxt[j].toString().trim())) {
                            cnt++;
                        }
                        if (cnsltComp.getKeyNosPe() != Integer.parseInt(noOffStafftxt[j].toString().trim())) {
                            cnt++;
                        }
                        if (cnsltComp.getSupportNos() != Integer.parseInt(NoOffSupportPrStafftxt[j].toString().trim())) {
                            cnt++;
                        }
                        if (cnsltComp.getSupportNosPe() != Integer.parseInt(noOffSupportStafftxt[j].toString().trim())) {
                            cnt++;
                        }
                        if ("timebase".equalsIgnoreCase(forms)) {
                            if (cnsltComp.getMonths().equals(new BigDecimal(monthstxt[j].toString()))) {
                                cnt++;
                            }
                            if (cnsltComp.getTotalNosPe() != Integer.parseInt(inNoOfKeyProfstafftxt[j].toString().trim())) {
                                cnt++;
                            }
                        }
                        if (cnt != 0) {
                            cchistory = new TblCmsSrvCchistory();
                            cchistory.setConsultantCtg(request.getParameter("cnlcat_" + j));
                            cchistory.setConsultantName(Consltnametxt[j].toString());
                            if (list == null || list.isEmpty()) {
                                cchistory.setHistCnt(0);
                            } else {
                                Object[] cchistObj = list.get(0);
                                cchistory.setHistCnt((Integer) cchistObj[0] + 1);
                            }
                            cchistory.setKeyNos(Integer.parseInt(NoOffKeyPrStafftxt[j].toString()));
                            cchistory.setKeyNosPe(Integer.parseInt(noOffStafftxt[j].toString()));
                            cchistory.setSrNo(request.getParameter("srno_" + j));
                            cchistory.setSrvCcid(Integer.parseInt(srvCCId[j].toString()));
                            cchistory.setSrvFormMapId(srvFormMapId);
                            cchistory.setSupportNos(Integer.parseInt(NoOffSupportPrStafftxt[j].toString()));
                            cchistory.setSupportNosPe(Integer.parseInt(noOffSupportStafftxt[j].toString()));
                            cchistory.setCreatedDate(new java.sql.Date(new java.util.Date().getTime()));
                            if ("timebase".equalsIgnoreCase(forms)) {
                                cchistory.setMonths(new BigDecimal(monthstxt[j].toString()));
                                cchistory.setTotalNosPe(Integer.parseInt(inNoOfKeyProfstafftxt[j].toString()));
                            }
                            list1.add(cchistory);
                        }
                        if ("timebase".equalsIgnoreCase(forms)) {
                            cmss.updateSrvConsltCompoDataTimebase(Integer.parseInt(srvCCId[j].toString()), new BigDecimal(monthstxt[j].toString()), Integer.parseInt(noOffStafftxt[j].toString()), Integer.parseInt(noOffSupportStafftxt[j].toString()), Consltnametxt[j].toString(), Integer.parseInt(NoOffKeyPrStafftxt[j].toString()), Integer.parseInt(NoOffSupportPrStafftxt[j].toString()), Integer.parseInt(inNoOfKeyProfstafftxt[j].toString()));

                        } else {
                            cmss.updateSrvConsltCompoDataLumpSum(Integer.parseInt(srvCCId[j].toString()), Integer.parseInt(noOffStafftxt[j].toString()), Integer.parseInt(noOffSupportStafftxt[j].toString()), Consltnametxt[j].toString(), Integer.parseInt(NoOffKeyPrStafftxt[j].toString()), Integer.parseInt(NoOffSupportPrStafftxt[j].toString()));
                        }
                    }
                    cmss.AddToCCHistory(list1);
                    sendMailforEditWorkSch(Integer.toString(tenderId), Integer.toString(lotId), user.toString(), Integer.toString(srvFormMapId));
                    response.sendRedirect("officer/SrvConsltCompo.jsp?tenderId=" + tenderId + "&lotId=" + lotId + "&srvFormMapId=" + srvFormMapId + "&msg=succ");
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), "Edit Consultant Composition", "");
                }
            } else if ("SrvPaymentSchedule".equalsIgnoreCase(action)) {
                try {
                    List<TblCmsSrvPaymentSch> list = cmss.getPaymentScheduleData(srvFormMapId);
                    List<TblCmsSrvPshistory> srvpshistory = new ArrayList<TblCmsSrvPshistory>();
                    List<Object[]> MaxCount = cmss.getSrvPsHistDistCount(srvFormMapId);
                    String forms = request.getParameter("forms");
                    String datePropByPE[] = request.getParameterValues("datePropByPE");
                    String datePropByCon[] = request.getParameterValues("datePropByCon");
                    String SrvPsid[] = request.getParameterValues("SrvPsid");
                    if ("lumpsum".equalsIgnoreCase(forms)) {
                        for (int j = 0; j < SrvPsid.length; j++) {
                            cmss.updateSrvPaymentScheduleData(Integer.parseInt(SrvPsid[j].toString()), DateUtils.convertStringtoDate(datePropByPE[j].toString(), "dd-MMM-yyyy"), DateUtils.convertStringtoDate(datePropByCon[j].toString(), "dd-MMM-yyyy"));
                            if (!datePropByPE[j].equals(DateUtils.gridDateToStrWithoutSec(list.get(j).getPeenddate()).split(" ")[0])
                                    || !datePropByCon[j].equals(DateUtils.gridDateToStrWithoutSec((Date) list.get(j).getEndDate()).split(" ")[0])) {
                                Date peEndDate = DateUtils.convertStringtoDate(datePropByPE[j], "dd-MMM-yyyy");
                                Date conEndDate = DateUtils.convertStringtoDate(datePropByCon[j], "dd-MMM-yyyy");
                                int maxNo = 0;
                                if (MaxCount.get(0) != null) {
                                    Object[] maxcountObj = MaxCount.get(0);
                                    maxNo = Integer.parseInt(maxcountObj[0].toString()) + 1;
                                    TblCmsSrvPshistory pslist = new TblCmsSrvPshistory(0, Integer.parseInt(SrvPsid[j].toString()), srvFormMapId, peEndDate, conEndDate, maxNo, new java.sql.Date(new java.util.Date().getTime()));
                                    srvpshistory.add(pslist);
                                }

                            }
                        }
                        cmss.addToPaymentScheduleHistory(srvpshistory);
                    }
                    sendMailforEditWorkSch(Integer.toString(tenderId), Integer.toString(lotId), user.toString(), Integer.toString(srvFormMapId));
                    response.sendRedirect("officer/SrvPaymentSchedule.jsp?tenderId=" + tenderId + "&lotId=" + lotId + "&srvFormMapId=" + srvFormMapId + "&msg=succ");
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), "Edit Payment Schedule", "");
                }
            } else if ("SrvTeamCompoTaskAssig".equalsIgnoreCase(action)) {
                try{
                String forms = request.getParameter("forms");
                String Srno[] = request.getParameterValues("Srno");
                String PositionAssigned[] = request.getParameterValues("PositionAssigned");
                String nameOfStafftxt[] = request.getParameterValues("nameOfStafftxt");
                String FirmOrganizationtxt[] = request.getParameterValues("FirmOrganizationtxt");
                String StaffCat[] = request.getParameterValues("StaffCat");
                String PositionConsulttxt[] = request.getParameterValues("PositionConsulttxt");
                String ConsultCategorytxt[] = request.getParameterValues("ConsultCategorytxt");
                String AreaOfExpertisetxt[] = request.getParameterValues("AreaOfExpertisetxt");
                String TaskAssignedtxt[] = request.getParameterValues("TaskAssignedtxt");
                String srvTCId[] = request.getParameterValues("srvTCId");

                List<TblCmsSrvTeamComp> listTblCmsSrvTeamComp = new ArrayList<TblCmsSrvTeamComp>();
                List<TblCmsSrvTchistory> listTblCmsSrvTchistory = new ArrayList<TblCmsSrvTchistory>();

                for (int j = 0; j < srvTCId.length; j++) {
                    TblCmsSrvTeamComp tblCmsSrvTeamComp = new TblCmsSrvTeamComp();
                    tblCmsSrvTeamComp.setSrvTcid(Integer.parseInt(srvTCId[j].toString()));
                    tblCmsSrvTeamComp.setSrno(Srno[j].toString());
                    tblCmsSrvTeamComp.setPositionAssigned(PositionAssigned[j].toString());
                    tblCmsSrvTeamComp.setEmpName(nameOfStafftxt[j].toString());
                    tblCmsSrvTeamComp.setOrganization(FirmOrganizationtxt[j].toString());
                    tblCmsSrvTeamComp.setStaffCat(StaffCat[j].toString());
                    tblCmsSrvTeamComp.setPositionDefined(PositionConsulttxt[j].toString());
                    tblCmsSrvTeamComp.setConsultPropCat(ConsultCategorytxt[j].toString());
                    tblCmsSrvTeamComp.setAreaOfExpertise(AreaOfExpertisetxt[j].toString());
                    tblCmsSrvTeamComp.setTaskAssigned(TaskAssignedtxt[j].toString());
                    tblCmsSrvTeamComp.setSrvFormMapId(srvFormMapId);
                    listTblCmsSrvTeamComp.add(tblCmsSrvTeamComp);
                }
                List<TblCmsSrvTeamComp> existingData = cmss.getSrvTeamCompoTaskAssigData(srvFormMapId);
                List<Object> getMaxCount = cmss.getSrvTCHistMaxCount(srvFormMapId);
                String strCount = "";
                int hCount = 0;
                int counter = 0;
                for (TblCmsSrvTeamComp tblCmsSrvTeamComp1 : existingData) {
                    TblCmsSrvTeamComp tblCmsSrvTeamComp2 = listTblCmsSrvTeamComp.get(counter);
                    if (!tblCmsSrvTeamComp1.equals(tblCmsSrvTeamComp2)) {
                        TblCmsSrvTchistory tblCmsSrvTchistory = new TblCmsSrvTchistory();
                        tblCmsSrvTchistory.setSrvTcid(tblCmsSrvTeamComp2.getSrvTcid());
                        tblCmsSrvTchistory.setEmpName(tblCmsSrvTeamComp2.getEmpName());
                        tblCmsSrvTchistory.setOrganization(tblCmsSrvTeamComp2.getOrganization());
                        tblCmsSrvTchistory.setPositionDefined(tblCmsSrvTeamComp2.getPositionDefined());
                        tblCmsSrvTchistory.setConsultPropCat(tblCmsSrvTeamComp2.getConsultPropCat());
                        tblCmsSrvTchistory.setAreaOfExpertise(tblCmsSrvTeamComp2.getAreaOfExpertise());
                        tblCmsSrvTchistory.setTaskAssigned(tblCmsSrvTeamComp2.getTaskAssigned());
                        tblCmsSrvTchistory.setSrvFormMapId(srvFormMapId);
                        tblCmsSrvTchistory.setCreatedDate(new java.sql.Date(new java.util.Date().getTime()));
                        if (getMaxCount == null || getMaxCount.isEmpty()) {
                            tblCmsSrvTchistory.setHistCnt(0);
                        } else {
                            tblCmsSrvTchistory.setHistCnt((Integer) getMaxCount.get(0) + 1);
                        }
                        listTblCmsSrvTchistory.add(tblCmsSrvTchistory);
                    }
                    counter++;
                }
                if (cmss.addSrvTeamCompoTaskAssigData(listTblCmsSrvTeamComp) && cmss.addSrvTChistoryData(listTblCmsSrvTchistory)) {
                    sendMailforEditWorkSch(Integer.toString(tenderId), Integer.toString(lotId), user.toString(), Integer.toString(srvFormMapId));
                    response.sendRedirect("officer/SrvTeamCompoTaskAssig.jsp?tenderId=" + tenderId + "&lotId=" + lotId + "&srvFormMapId=" + srvFormMapId + "&msg=succ");
                } else {
                    response.sendRedirect("officer/SrvTeamCompoTaskAssig.jsp?tenderId=" + tenderId + "&lotId=" + lotId + "&srvFormMapId=" + srvFormMapId + "&msg=fail");
                }
                } finally {
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), "Edit Team Composition", "");
                }
            } else if ("UpdateSrvPSPr".equalsIgnoreCase(action)) {
                try {
                    String SrvPsid = request.getParameter("SrvPsid");
                    cmss.updateLumpSumPr(Integer.parseInt(SrvPsid));
                    response.sendRedirect("officer/SrvLumpSumUploadDoc.jsp?tenderId=" + tenderId + "&keyId=" + SrvPsid + "&docx=LumpSumPR&module=PR&msg=updated");
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Progress_Report.getName(), "MileStone Completed", "");
                }
                //response.sendRedirect("officer/SrvLumpSumPr.jsp?tenderId=" + tenderId + "&lotId=" + lotId + "&srvFormMapId=" + srvFormMapId + "&msg=Updated");
            } else if ("GenerateInvoiceTenSide".equalsIgnoreCase(action)) {
                String remarks = "";
                try{
                HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                String TillPrid = "0";
                boolean Sheetflag = false;
                if (request.getParameter("SrvPsid") != null) {
                    TillPrid = request.getParameter("SrvPsid");
                    remarks = request.getParameter("textarea" + TillPrid);
                }
                if (request.getParameter("sheetId") != null) {
                    Sheetflag = true;
                    TillPrid = request.getParameter("sheetId");
                    remarks = request.getParameter("textarea" + TillPrid);
                }
                long lng = service.getInvoiceCountByLotId(lotId);
                String isAdvInv = request.getParameter("isAdvInv");
                BigDecimal totalamount = new BigDecimal(request.getParameter("totalamount"));
                TblCmsInvoiceMaster tcim = new TblCmsInvoiceMaster();
                tcim.setCreatedBy(Integer.parseInt(userId));
                tcim.setCreatedDate(new java.util.Date());
                tcim.setInvStatus("createdbyten");
                tcim.setTotalInvAmt((totalamount).setScale(3, 0));
                tcim.setTenderId(tenderId);
                tcim.setTblCmsWpMaster(new TblCmsWpMaster(wpId));
                tcim.setTillPrid(Integer.parseInt(TillPrid));
                if (isAdvInv != null && !"".equalsIgnoreCase(isAdvInv) && !"null".equalsIgnoreCase(isAdvInv)) {
                    tcim.setIsAdvInv(isAdvInv);
                } else {
                    tcim.setIsAdvInv("No");
                }
                tcim.setConRemarks(handleSpecialChar.handleSpecialChar(remarks));
                tcim.setInvoiceNo("Invoice" + String.valueOf(lng + 1));
                boolean flag = service.addToInvoiceMaster(tcim);
                sendMailForInvoiceGeneratedbySupplier(Integer.toString(tenderId), Integer.toString(lotId), user.toString(), tcim.getInvoiceId());
                if (flag) {
                    //response.sendRedirect("tenderer/InvoiceServiceCase.jsp?tenderId=" + tenderId + "&lotId=" + lotId + "&msg=InvGenerated");
                    response.sendRedirect("tenderer/ViewInvoice.jsp?tenderId=" + tenderId + "&lotId=" + lotId + "&invoiceId=" + tcim.getInvoiceId() + "&wpId=" + wpId + "&srvPsId=" + TillPrid + "&msg=InvGenerated&Sheetflag=" + Sheetflag + "");
                }
                } finally {
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Payment.getName(), "Generate Invoice (Tenderer side)", remarks);
                }
            } else if ("dumpForm".equalsIgnoreCase(action)) {

                boolean isNegotiationDone = true;
                String flagHandle = "";
                List<Object[]> forms = cmss.getAllFormsForService(tenderId);
                List<SPCommonSearchDataMore> more = cmss.getQualifiedUser(tenderId + "", lotId+"");
                List<TblCmsSrvWorkPlan> srvWorkPlans = new ArrayList<TblCmsSrvWorkPlan>();
                List<TblCmsSrvTchistory> tchistorys = new ArrayList<TblCmsSrvTchistory>();
                List<TblCmsSrvWphistory> wphistorys = new ArrayList<TblCmsSrvWphistory>();
                List<TblCmsSrvTeamComp> srvTeamComps = new ArrayList<TblCmsSrvTeamComp>();
                List<TblCmsSrvReExpense> reExpenses = new ArrayList<TblCmsSrvReExpense>();
                List<TblCmsSrvStaffSch> srvStaffSchs = new ArrayList<TblCmsSrvStaffSch>();
                List<TblCmsSrvSalaryRe> salaryRes = new ArrayList<TblCmsSrvSalaryRe>();
                List<TblCmsSrvPaymentSch> paymentSchs = new ArrayList<TblCmsSrvPaymentSch>();
                List<TblCmsSrvCnsltComp> comps = new ArrayList<TblCmsSrvCnsltComp>();
                List<TblCmsSrvStaffDaysCnt> srvStaffDaysCnts = new ArrayList<TblCmsSrvStaffDaysCnt>();
                List<TblCmsSrvReExHistory> reExHistorys = new ArrayList<TblCmsSrvReExHistory>();
                List<TblCmsSrvSshistory> sshistorys = new ArrayList<TblCmsSrvSshistory>();
                List<TblCmsSrvCchistory> cchistorys = new ArrayList<TblCmsSrvCchistory>();
                List<TblCmsWpDetail> details = new ArrayList<TblCmsWpDetail>();
                List<TblCmsSrvPshistory> pshistorys = new ArrayList<TblCmsSrvPshistory>();
                int uId = 0;
                if (more != null && !more.isEmpty()) {
                    uId = Integer.parseInt(more.get(0).getFieldName1());
                }
                TblCmsSrvWorkPlan plan = null;
                TblCmsSrvTeamComp comp = null;
                TblCmsSrvTchistory tchistory = null;
                TblCmsSrvReExpense expense = null;
                TblCmsSrvStaffSch staffSch = null;
                TblCmsSrvSalaryRe re = null;
                TblCmsSrvPaymentSch sch = null;
                TblCmsSrvWphistory wphistory = null;
                TblCmsSrvCnsltComp cnsltComp = null;
                TblCmsSrvReExHistory reExHistory = null;
                TblCmsSrvSshistory sshistory = null;
                TblCmsSrvCchistory cchistory = null;
                TblCmsSrvPshistory pshistory = null;
                if (forms != null && !forms.isEmpty()) {
                    for (Object obj[] : forms) {

                        boolean isNegForForm = false;
                        if (isNegotiationDone) {
                            List<Object> data = null;
                            if (!isNegForForm) {
                                //for original plan data
                                //data = cmss.getDataForDump(uId, (Integer) obj[1]);
                                data = cmss.getDataForDumpFrmNEG(uId, (Integer) obj[1]);
                            }
                            if (data != null && !data.isEmpty()) {

                                int i = 0;
                                int j = 0;
                                try {
                                    TblCmsWpMaster cmsWpMaster = new TblCmsWpMaster();
                                    TblCmsWptenderBoqmap tcwb = new TblCmsWptenderBoqmap();
                                    boolean inMaster = false;
                                    try {
                                        cmsWpMaster.setCreatedBy(Integer.parseInt(userId));
                                        cmsWpMaster.setCreatedDate(new java.util.Date());
                                        cmsWpMaster.setWpLotId(lotId);
                                        cmsWpMaster.setWpName(obj[2].toString());
                                        cmsWpMaster.setIsRepeatOrder("no");
                                        cmsWpMaster.setIsDateEdited("no");
                                        inMaster = service.add(cmsWpMaster);

                                        tcwb.setWpTenderFormId((Integer) obj[1]);
                                        tcwb.setTblCmsWpMaster(new TblCmsWpMaster(cmsWpMaster.getWpId()));
                                        boolean inchild = service.addinchild(tcwb);
                                    } catch (Exception e) {
                                        // to do
                                    }
                                    TblCmsSrvFormMap map = new TblCmsSrvFormMap();
                                    map.setCreatedBy(Integer.parseInt(userId));
                                    map.setCreatedDate(new Date());
                                    map.setLotId(lotId);
                                    map.setTenderId(tenderId);
                                    map.setFormId((Integer) obj[1]);
                                    map.setSrvBoqId((Integer) obj[2]);
                                    cmss.addToFormMap(map);
                                    if ((Integer) obj[2] == 19) { // for work plan
                                        for (Object objd : data) {
                                            if (i == 0) {
                                                plan = new TblCmsSrvWorkPlan();
                                                wphistory = new TblCmsSrvWphistory();
                                                plan.setSrNo(objd.toString());
                                                wphistory.setSrNo(objd.toString());
                                                wphistory.setHistCnt(0);
                                                wphistory.setCreatedDate(new Date());
                                            } else if (i == 1) {
                                                plan.setActivity(objd.toString());
                                                wphistory.setActivity(objd.toString());
                                            } else if (i == 2) {
                                                plan.setStartDt(DateUtils.convertStringtoDate(objd.toString(), "dd-MMM-yyyy"));
                                                wphistory.setStartDt(DateUtils.convertStringtoDate(objd.toString(), "dd-MMM-yyyy"));
                                            } else if (i == 3) {
                                                plan.setNoOfDays(Integer.parseInt(objd.toString()));
                                                wphistory.setNoOfDays(Integer.parseInt(objd.toString()));
                                            } else if (i == 4) {
                                                plan.setEndDt(DateUtils.convertStringtoDate(objd.toString(), "dd-MMM-yyyy"));
                                                wphistory.setEndDt(DateUtils.convertStringtoDate(objd.toString(), "dd-MMM-yyyy"));
                                            } else {
                                                plan.setRemarks(objd.toString());
                                                wphistory.setRemarks(objd.toString());
                                                wphistory.setSrvFormMapId(map.getSrvFormMapId());
                                                plan.setSrvFormMapId(map.getSrvFormMapId());
                                            }
                                            i++;
                                            if (i == 6) {
                                                i = 0;
                                                srvWorkPlans.add(plan);
                                                wphistorys.add(wphistory);
                                            }
                                        }

                                        try {
                                            TblCmsWpMaster cmsWp = new TblCmsWpMaster();
                                            cmsWp.setCreatedBy(Integer.parseInt(userId));
                                            cmsWp.setCreatedDate(new java.util.Date());
                                            cmsWp.setWpLotId(lotId);
                                            cmsWp.setWpName("ATS");
                                            cmsWp.setIsRepeatOrder("no");
                                            cmsWp.setIsDateEdited("no");
                                            inMaster = service.add(cmsWp);
                                        } catch (Exception e) {
                                            // to do
                                        }
                                    } else if ((Integer) obj[2] == 13) {
                                        //team composition
                                        int colId = 3;

                                        /*
                                         * Before it was second column where homefield was present.
                                        if (tenderId == 3001) {
                                        colId = 2;
                                        }
                                         */
                                        List<SPCommonSearchDataMore> mores = cmss.getListBoxItemName((Integer) obj[1], colId);
                                        HashMap<String, String> hm = new HashMap<String, String>();
                                        if (mores != null && !mores.isEmpty()) {
                                            for (SPCommonSearchDataMore spcsdm : mores) {
                                                hm.put(spcsdm.getFieldName1(), spcsdm.getFieldName2());
                                            }
                                        }

                                        colId = 5;
                                        List<SPCommonSearchDataMore> stffCat = cmss.getListBoxItemName((Integer) obj[1], colId);
                                        HashMap<String, String> hmStaffcat = new HashMap<String, String>();
                                        if (stffCat != null && !stffCat.isEmpty()) {
                                            for (SPCommonSearchDataMore spcsdm : stffCat) {
                                                hmStaffcat.put(spcsdm.getFieldName1(), spcsdm.getFieldName2());
                                            }
                                        }
                                        for (Object objd : data) {
                                            int x = 1, y = 2;
                                            /*if (tenderId == 3001) {
                                            x = 2;
                                            y = 1;
                                            }*/
                                            if (i == 0) {
                                                tchistory = new TblCmsSrvTchistory();
                                                comp = new TblCmsSrvTeamComp();
                                                comp.setSrno(objd.toString());
                                                tchistory.setHistCnt(0);
                                            } else if (i == y) {
                                                comp.setPositionAssigned(hm.get(objd.toString()));

                                            } else if (i == x) {
                                                comp.setEmpName(objd.toString());
                                                tchistory.setEmpName(objd.toString());
                                                tchistory.setCreatedDate(new Date());
                                            } else if (i == 3) {
                                                comp.setOrganization(objd.toString());
                                                tchistory.setOrganization(objd.toString());
                                            } else if (i == 4) {
                                                // comp.setStaffCat(objd.toString().equals("1") ? "Key" : "Support");
                                                comp.setStaffCat(hmStaffcat.get(objd.toString()));
                                            } else if (i == 5) {
                                                comp.setPositionDefined(objd.toString());
                                                tchistory.setPositionDefined(objd.toString());
                                            } else if (i == 6) {
                                                comp.setConsultPropCat(objd.toString());
                                                tchistory.setConsultPropCat(objd.toString());
                                            } else if (i == 7) {
                                                comp.setAreaOfExpertise(objd.toString());
                                                tchistory.setAreaOfExpertise(objd.toString());
                                            } else {
                                                comp.setTaskAssigned(objd.toString());
                                                tchistory.setTaskAssigned(objd.toString());
                                                comp.setSrvFormMapId(map.getSrvFormMapId());
                                                tchistory.setSrvFormMapId(map.getSrvFormMapId());
                                            }
                                            i++;
                                            if (i == 9) {
                                                i = 0;
                                                srvTeamComps.add(comp);
                                                tchistorys.add(tchistory);
                                            }
                                        }

                                    } else if ((Integer) obj[2] == 16) {
                                        //for reimbursable expenses
                                        List<SPCommonSearchDataMore> mores = cmss.getListBoxItemName((Integer) obj[1], 2);
                                        HashMap<String, String> hm = new HashMap<String, String>();
                                        if (mores != null && !more.isEmpty()) {
                                            for (SPCommonSearchDataMore spcsdm : mores) {
                                                hm.put(spcsdm.getFieldName1(), spcsdm.getFieldName2());
                                            }
                                        }



                                        TblCmsWpDetail wpDetail = null;
                                        List<Object> tendertableId = service.countTable(obj[1].toString());
                                        int rowCounter = 1;
                                        for (j = 0; j < (data.size() - 7); j++) {

                                            if (i == 0) {
                                                wpDetail = new TblCmsWpDetail();
                                                wpDetail.setWpRowId(rowCounter);
                                                rowCounter++;
                                                reExHistory = new TblCmsSrvReExHistory();
                                                expense = new TblCmsSrvReExpense();
                                                expense.setSrNo(data.get(j).toString());
                                                reExHistory.setSrNo(data.get(j).toString());
                                                reExHistory.setHistCnt(0);
                                                wpDetail.setWpNoOfDays(0);
                                                wpDetail.setTenderTableId((Integer) tendertableId.get(0));
                                                wpDetail.setUserTypeId(Integer.parseInt(userTypeId));
                                                wpDetail.setWpSrNo(data.get(j).toString());
                                                wpDetail.setWpItemStatus("pending");
                                                wpDetail.setWpQualityCheckReq("no");
                                                wpDetail.setWpEndDate(null);
                                                wpDetail.setWpStartDate(null);
                                                wpDetail.setAmendmentFlag("original");
                                                wpDetail.setCreatedBy(Integer.parseInt(userId));
                                                wpDetail.setGroupId(null);
                                                wpDetail.setIsRepeatOrder("no");
                                                wpDetail.setItemInvGenQty(BigDecimal.ZERO);
                                                wpDetail.setItemInvStatus("pending");
                                                wpDetail.setTblCmsWpMaster(new TblCmsWpMaster(cmsWpMaster.getWpId()));
                                            } else if (i == 1) {
                                                expense.setCatagoryDesc(hm.get(data.get(j).toString()));
                                                reExHistory.setCatagoryDesc(hm.get(data.get(j).toString()));
                                                reExHistory.setCreatedDate(new Date());
                                            } else if (i == 2) {
                                                expense.setDescription(data.get(j).toString());
                                                reExHistory.setDescription(data.get(j).toString());
                                                wpDetail.setWpDescription(data.get(j).toString());
                                            } else if (i == 3) {
                                                expense.setUnit(data.get(j).toString());
                                                wpDetail.setWpUom(data.get(j).toString());
                                                reExHistory.setUnit(data.get(j).toString());
                                            } else if (i == 4) {
                                                expense.setQty(new BigDecimal(data.get(j).toString()));
                                                wpDetail.setWpQty(new BigDecimal(data.get(j).toString()));
                                                reExHistory.setQty(new BigDecimal(data.get(j).toString()));
                                            } else if (i == 5) {
                                                reExHistory.setUnitCost(new BigDecimal(data.get(j).toString()));
                                                expense.setUnitCost(new BigDecimal(data.get(j).toString()));
                                                wpDetail.setWpRate(new BigDecimal(data.get(j).toString()));
                                            } else {
                                                reExHistory.setTotalAmt(new BigDecimal(data.get(j).toString()));
                                                expense.setTotalAmt(new BigDecimal(data.get(j).toString()));
                                                expense.setSrvFormMapId(map.getSrvFormMapId());
                                                reExHistory.setSrvFormMapId(map.getSrvFormMapId());
                                            }
                                            i++;
                                            if (i == 7) {
                                                i = 0;
                                                reExpenses.add(expense);
                                                details.add(wpDetail);
                                                reExHistorys.add(reExHistory);
                                            }

                                        }
                                        try {
                                            service.addToWpDetails(details);
                                        } catch (Exception ex) {
                                            Logger.getLogger(CMSSerCaseServlet.class.getName()).log(Level.SEVERE, null, ex);
                                        }

                                    } else if ((Integer) obj[2] == 14) {
                                        //for staffing schedule
                                        for (Object objd : data) {
                                            if (i == 0) {
                                                sshistory = new TblCmsSrvSshistory();
                                                staffSch = new TblCmsSrvStaffSch();
                                                staffSch.setSrno(objd.toString());
                                                sshistory.setSrno(objd.toString());
                                                sshistory.setHistCnt(0);
                                                sshistory.setCreatedDate(new Date());
                                            } else if (i == 1) {
                                                staffSch.setEmpName(objd.toString());
                                            } else if (i == 2) {
                                                i++;
                                                continue;
                                                // position assigned not require
                                            } else if (i == 3) {
                                                staffSch.setWorkFrom(objd.toString().equals("1") ? "Home" : "Field");
                                                sshistory.setWorkFrom(objd.toString().equals("1") ? "Home" : "Field");
                                            } else if (i == 4) {
                                                staffSch.setStartDt(DateUtils.convertStringtoDate(objd.toString(), "dd-MMM-yyyy"));
                                                sshistory.setStartDt(DateUtils.convertStringtoDate(objd.toString(), "dd-MMM-yyyy"));
                                            } else if (i == 5) {
                                                staffSch.setNoOfDays(Integer.parseInt(objd.toString().trim()));
                                                sshistory.setNoOfDays(Integer.parseInt(objd.toString().trim()));
                                            } else {
                                                sshistory.setEndDt(DateUtils.convertStringtoDate(objd.toString(), "dd-MMM-yyyy"));
                                                sshistory.setSrvFormMapId(map.getSrvFormMapId());
                                                sshistory.setSrvTcid(0);
                                                staffSch.setEndDt(DateUtils.convertStringtoDate(objd.toString(), "dd-MMM-yyyy"));
                                                staffSch.setSrvFormMapId(map.getSrvFormMapId());
                                                staffSch.setSrvTcid(0);
                                            }
                                            i++;
                                            if (i == 7) {
                                                i = 0;
                                                sshistorys.add(sshistory);
                                                srvStaffSchs.add(staffSch);
                                            }

                                        }

                                    } else if ((Integer) obj[2] == 15) {
                                        //for salary reimbursable
                                        for (j = 0; j < (data.size() - 7); j++) {
                                            int x = 1, y = 2;
                                            /*
                                            if (tenderId == 3001) {
                                            x = 2;
                                            y = 1;
                                            }
                                             */
                                            if (i == 0) {
                                                re = new TblCmsSrvSalaryRe();
                                                re.setSrNo(data.get(j).toString());
                                                re.setNoOfDays(0);
                                                re.setVariOrdId(0);

                                            } else if (i == x) {
                                                re.setEmpName(data.get(j).toString());
                                            } else if (i == y) {
                                                i++;
                                                continue;
                                                // position assigned not require

                                            } else if (i == 3) {
                                                re.setWorkFrom(data.get(j).toString().equals("1") ? "Home" : "Field");
                                            } else if (i == 4) {
                                                re.setWorkMonths(new BigDecimal(data.get(j).toString()));
                                            } else if (i == 5) {
                                                re.setEmpMonthSalary(new BigDecimal(data.get(j).toString()));
                                            } else {
                                                re.setTotalSalary(new BigDecimal(data.get(j).toString()));
                                                re.setSrvFormMapId(map.getSrvFormMapId());
                                                re.setSrvTcid(0);
                                            }
                                            i++;
                                            if (i == 7) {
                                                i = 0;
                                                salaryRes.add(re);
                                            }

                                        }

                                    } else if ((Integer) obj[2] == 18) {
                                        //for consultant composition

                                        List<Object> counttable = service.countTable(obj[1].toString());
                                        short rows = 0;
                                        if (counttable != null && !counttable.isEmpty()) {
                                            TenderTablesSrBean bean = new TenderTablesSrBean();
                                            rows = bean.getNoOfRowsInTable(Integer.parseInt(counttable.get(0).toString()), (short) 1);
                                        }
                                        if (!"time based".equalsIgnoreCase(srvType)) {
                                            java.util.ListIterator<com.cptu.egp.eps.model.table.TblTenderCells> tblCellsDtl = cmss.getDataForCC((Integer) obj[1], 4).listIterator();
                                            for (int k = 1; k <= rows; k++) {
                                                cnsltComp = new TblCmsSrvCnsltComp();
                                                cchistory = new TblCmsSrvCchistory();
                                                for (int is = 0; is < 4; is++) {
                                                    if (tblCellsDtl.hasNext()) {
                                                        TblTenderCells cells = tblCellsDtl.next();
                                                        if (is == 0) {
                                                            cnsltComp.setSrNo(cells.getCellvalue());
                                                            cchistory.setSrNo(cells.getCellvalue());
                                                            cchistory.setHistCnt(0);
                                                            cchistory.setCreatedDate(new Date());
                                                        } else if (is == 1) {
                                                            cnsltComp.setConsultantCtg(cells.getCellvalue());
                                                            cchistory.setConsultantCtg(cells.getCellvalue());
                                                        } else if (is == 2) {
                                                            cnsltComp.setKeyNosPe(Integer.parseInt(cells.getCellvalue()));
                                                            cchistory.setKeyNosPe(Integer.parseInt(cells.getCellvalue()));

                                                        } else if (is == 3) {
                                                            cnsltComp.setSupportNosPe(Integer.parseInt(cells.getCellvalue()));
                                                            cchistory.setSupportNosPe(Integer.parseInt(cells.getCellvalue()));
                                                            cnsltComp.setSrvFormMapId(map.getSrvFormMapId());
                                                            cchistory.setSrvFormMapId(map.getSrvFormMapId());
                                                        }
                                                    }
                                                }
                                                List<Object> os = cmss.getDataForDumpForCC(uId, (Integer) obj[1], k);
                                                if (os != null && !os.isEmpty()) {
                                                    int rowCount = 0;
                                                    for (Object objd : os) {
                                                        if (rowCount == 0) {
                                                            cnsltComp.setConsultantName(objd.toString());
                                                            cchistory.setConsultantName(objd.toString());
                                                        } else if (rowCount == 1) {
                                                            cnsltComp.setKeyNos(Integer.parseInt(objd.toString().trim()));
                                                            cchistory.setKeyNos(Integer.parseInt(objd.toString().trim()));
                                                        } else {
                                                            cnsltComp.setSupportNos(Integer.parseInt(objd.toString().trim()));
                                                            cchistory.setSupportNos(Integer.parseInt(objd.toString().trim()));
                                                        }
                                                        rowCount++;
                                                        if (rowCount == 3) {
                                                            rowCount = 0;
                                                            comps.add(cnsltComp);
                                                            cchistorys.add(cchistory);
                                                        }
                                                    }
                                                }
                                            }

                                        } else {
                                            java.util.ListIterator<com.cptu.egp.eps.model.table.TblTenderCells> tblCellsDtl = cmss.getDataForCC((Integer) obj[1], 6).listIterator();
                                            for (int k = 1; k <= rows; k++) {
                                                cnsltComp = new TblCmsSrvCnsltComp();
                                                cchistory = new TblCmsSrvCchistory();
                                                for (int is = 0; is < 6; is++) {
                                                    if (tblCellsDtl.hasNext()) {
                                                        TblTenderCells cells = tblCellsDtl.next();
                                                        if (is == 0) {
                                                            cnsltComp.setSrNo(cells.getCellvalue());
                                                            cchistory.setSrNo(cells.getCellvalue());
                                                            cchistory.setHistCnt(0);
                                                            cchistory.setCreatedDate(new Date());
                                                        } else if (is == 1) {
                                                            cnsltComp.setConsultantCtg(cells.getCellvalue());
                                                            cchistory.setConsultantCtg(cells.getCellvalue());
                                                        } else if (is == 2) {
                                                            cnsltComp.setTotalNosPe(Integer.parseInt(cells.getCellvalue()));
                                                            cchistory.setTotalNosPe(Integer.parseInt(cells.getCellvalue()));
                                                        } else if (is == 3) {
                                                            cnsltComp.setMonths(new BigDecimal(cells.getCellvalue()));
                                                            cchistory.setMonths(new BigDecimal(cells.getCellvalue()));

                                                        } else if (is == 4) {
                                                            cnsltComp.setKeyNosPe(Integer.parseInt(cells.getCellvalue()));
                                                            cchistory.setKeyNosPe(Integer.parseInt(cells.getCellvalue()));

                                                        } else if (is == 5) {
                                                            cnsltComp.setSupportNosPe(Integer.parseInt(cells.getCellvalue()));
                                                            cchistory.setSupportNosPe(Integer.parseInt(cells.getCellvalue()));
                                                            cnsltComp.setSrvFormMapId(map.getSrvFormMapId());
                                                            cchistory.setSrvFormMapId(map.getSrvFormMapId());
                                                        }
                                                    }
                                                }
                                                List<Object> os = cmss.getDataForDumpForCC(uId, (Integer) obj[1], k);
                                                if (os != null && !os.isEmpty()) {
                                                    int rowCount = 0;
                                                    for (Object objd : os) {
                                                        if (rowCount == 0) {
                                                            cnsltComp.setConsultantName(objd.toString());
                                                            cchistory.setConsultantName(objd.toString());
                                                        } else if (rowCount == 1) {
                                                            cnsltComp.setKeyNos(Integer.parseInt(objd.toString().trim()));
                                                            cchistory.setKeyNos(Integer.parseInt(objd.toString().trim()));
                                                        } else {
                                                            cnsltComp.setSupportNos(Integer.parseInt(objd.toString().trim()));
                                                            cchistory.setSupportNos(Integer.parseInt(objd.toString().trim()));
                                                        }
                                                        rowCount++;
                                                        if (rowCount == 3) {
                                                            rowCount = 0;
                                                            comps.add(cnsltComp);
                                                            cchistorys.add(cchistory);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    } else if ((Integer) obj[2] == 12) {

                                        //for payment schedule

                                        List<Object> counttable = service.countTable(obj[1].toString());
                                        short rows = 0;
                                        if (counttable != null && !counttable.isEmpty()) {
                                            TenderTablesSrBean bean = new TenderTablesSrBean();
                                            rows = bean.getNoOfRowsInTable(Integer.parseInt(counttable.get(0).toString()), (short) 1);
                                        }
                                        if ("time based".equalsIgnoreCase(srvType)) {
                                            java.util.ListIterator<com.cptu.egp.eps.model.table.TblTenderCells> tblCellsDtl = cmss.getDataForCC((Integer) obj[1], 3).listIterator();
                                            for (int k = 1; k <= rows; k++) {
                                                sch = new TblCmsSrvPaymentSch();

                                                for (int is = 0; is < 3; is++) {

                                                    if (tblCellsDtl.hasNext()) {
                                                        TblTenderCells cells = tblCellsDtl.next();
                                                        if (is == 0) {
                                                            sch.setMilestone("");
                                                            sch.setPercentOfCtrVal(BigDecimal.ZERO);
                                                            sch.setSrNo(cells.getCellvalue());
                                                            sch.setStatus("pending");
                                                        } else if (is == 1) {
                                                            sch.setDescription(cells.getCellvalue());
                                                        } else if (is == 2) {
                                                            sch.setSrvFormMapId(map.getSrvFormMapId());
                                                            sch.setPeenddate(DateUtils.convertStringtoDate(cells.getCellvalue(), "dd-MMM-yyyy"));
                                                        }
                                                    }
                                                }
                                                List<Object> os = cmss.getDataForDumpForCC(uId, (Integer) obj[1], k);
                                                if (os != null && !os.isEmpty()) {
                                                    int rowCount = 0;
                                                    for (Object objd : os) {
                                                        if (rowCount == 0) {
                                                            sch.setEndDate(DateUtils.convertStringtoDate(objd.toString(), "dd-MMM-yyyy"));
                                                        }
                                                        rowCount++;
                                                        if (rowCount == 1) {
                                                            rowCount = 0;
                                                            paymentSchs.add(sch);
                                                        }
                                                    }
                                                }
                                            }

                                        } else {
                                            java.util.ListIterator<com.cptu.egp.eps.model.table.TblTenderCells> tblCellsDtl = cmss.getDataForCC((Integer) obj[1], 5).listIterator();
                                            for (int k = 1; k <= rows; k++) {
                                                sch = new TblCmsSrvPaymentSch();
                                                pshistory = new TblCmsSrvPshistory();
                                                for (int is = 0; is < 5; is++) {
                                                    if (tblCellsDtl.hasNext()) {
                                                        TblTenderCells cells = tblCellsDtl.next();
                                                        if (is == 0) {
                                                            sch.setSrNo(cells.getCellvalue());
                                                            sch.setStatus("pending");
                                                        } else if (is == 1) {
                                                            sch.setMilestone(cells.getCellvalue());
                                                        } else if (is == 2) {
                                                            sch.setDescription(cells.getCellvalue());
                                                        } else if (is == 3) {
                                                            sch.setPercentOfCtrVal(new BigDecimal(cells.getCellvalue()));
                                                        } else {
                                                            sch.setPeenddate(DateUtils.convertStringtoDate(cells.getCellvalue(), "dd-MMM-yyyy"));
                                                            pshistory.setPeEndDate(DateUtils.convertStringtoDate(cells.getCellvalue(), "dd-MMM-yyyy"));
                                                            sch.setSrvFormMapId(map.getSrvFormMapId());
                                                            pshistory.setSrvFormMapId(map.getSrvFormMapId());
                                                            pshistory.setHistCnt(0);
                                                            pshistory.setCreatedDate(new Date());
                                                        }
                                                    }
                                                }

                                                List<Object> os = cmss.getDataForDumpForCC(uId, (Integer) obj[1], k);
                                                if (os != null && !os.isEmpty()) {
                                                    int rowCount = 0;
                                                    for (Object objd : os) {
                                                        if (rowCount == 0) {
                                                            sch.setEndDate(DateUtils.convertStringtoDate(objd.toString(), "dd-MMM-yyyy"));
                                                            pshistory.setEndDate(DateUtils.convertStringtoDate(objd.toString(), "dd-MMM-yyyy"));
                                                        }
                                                        rowCount++;
                                                        if (rowCount == 1) {
                                                            rowCount = 0;
                                                            paymentSchs.add(sch);
                                                            pshistorys.add(pshistory);
                                                        }
                                                    }
                                                }
                                            }

                                        }

                                    }


                                } catch (NumberFormatException ne) {
                                    response.sendRedirect("officer/NOA.jsp?tenderId=" + tenderId + "&msg=num");
                                }

                            }

                        }
                    }
                    //in workplan
                    try {
                        cmss.addToWorkPlan(srvWorkPlans);
                        for (int i = 0; i < srvWorkPlans.size(); i++) {
                            wphistorys.get(i).setSrvWorkPlanId(srvWorkPlans.get(i).getSrvWorkPlanId());
                        }
                        //in workplan history
                        cmss.addToWorkPlanHistory(wphistorys);
                        //in reimbursable expense
                        cmss.addToRE(reExpenses);
                        for (int i = 0; i < reExpenses.size(); i++) {
                            reExHistorys.get(i).setSrvReid(reExpenses.get(i).getSrvReid());
                        }
                        //in reimbursable expense history
                        cmss.AddToREHistory(reExHistorys);
                        cmss.addToPaymentSch(paymentSchs);
                        cmss.addToConComp(comps);
                        for (int jj = 0; jj < comps.size(); jj++) {
                            cchistorys.get(jj).setSrvCcid(comps.get(jj).getSrvCcid());
                        }
                        cmss.AddToCCHistory(cchistorys);
                        if (!"time based".equalsIgnoreCase(srvType)) {
                            for (int jj = 0; jj < paymentSchs.size(); jj++) {
                                pshistorys.get(jj).setSrvPsid(paymentSchs.get(jj).getSrvPsid());
                            }
                            cmss.addToPaymentScheduleHistory(pshistorys);
                        }
                        cmss.addToTeamComp(srvTeamComps);
                        HashMap map = new HashMap();
                        for (int id = 0; id < srvTeamComps.size(); id++) {
                            map.put(srvTeamComps.get(id).getEmpName(), srvTeamComps.get(id).getSrvTcid());
                            tchistorys.get(id).setSrvTcid(srvTeamComps.get(id).getSrvTcid());
                        }

                        cmss.addSrvTChistoryData(tchistorys);
                        for (int id = 0; id < srvTeamComps.size(); id++) {
                            int onSitecount = 0;
                            int offSitecount = 0;
                            // 1 -> HOme
                            // 2 -> field
                            for (int jj = 0; jj < srvStaffSchs.size(); jj++) {
                                if (srvTeamComps.get(id).getEmpName().equals(srvStaffSchs.get(jj).getEmpName())) {
                                    if (srvStaffSchs.get(jj).getWorkFrom().trim().equals("Home")) {
                                        offSitecount = offSitecount + srvStaffSchs.get(jj).getNoOfDays();
                                    } else {
                                        onSitecount = onSitecount + srvStaffSchs.get(jj).getNoOfDays();
                                    }
                                }

                            }
                            TblCmsSrvStaffDaysCnt daysCnt = new TblCmsSrvStaffDaysCnt();
                            daysCnt.setLotId(lotId);
                            daysCnt.setTenderId(tenderId);
                            daysCnt.setOffSiteCnt(offSitecount);
                            daysCnt.setOnSiteCnt(onSitecount);
                            daysCnt.setTotalCnt(offSitecount + onSitecount);
                            daysCnt.setSrvTcid(srvTeamComps.get(id).getSrvTcid());
                            daysCnt.setUsedOffSiteCnt(0);
                            daysCnt.setusedOnSiteCnt(0);
                            srvStaffDaysCnts.add(daysCnt);
                        }
                        cmss.addToStaffdayCount(srvStaffDaysCnts);
                        for (int jj = 0; jj < srvStaffSchs.size(); jj++) {

                            srvStaffSchs.get(jj).setSrvTcid((Integer) map.get(srvStaffSchs.get(jj).getEmpName()));
                        }
                        for (int jj = 0; jj < salaryRes.size(); jj++) {
                            salaryRes.get(jj).setSrvTcid((Integer) map.get(salaryRes.get(jj).getEmpName()));
                        }
                        cmss.addToStaffSch(srvStaffSchs);
                        for (int jj = 0; jj < srvStaffSchs.size(); jj++) {
                            sshistorys.get(jj).setSrvSsid(srvStaffSchs.get(jj).getSrvSsid());
                            sshistorys.get(jj).setSrvTcid(srvStaffSchs.get(jj).getSrvTcid());
                        }
                        cmss.addToStaffScheduleHistory(sshistorys);
                        cmss.addToSalaryRE(salaryRes);

                    } catch (Exception e) {
                        cmss.deleteInCaseOfService(tenderId);
                        response.sendRedirect("officer/NOA.jsp?tenderId=" + tenderId + "&msg=prob");

                    }
                }
                response.sendRedirect("officer/NOA.jsp?tenderId=" + tenderId);



            } else if ("serviceworkplan".equalsIgnoreCase(action)) {
                String max = request.getParameter("size");
                String styleClass = "";
                String type = request.getParameter("type");
                List<Object[]> listt = cmss.getAllServiceWorkPlan(formMapId);

                try {

                    if (listt != null && !listt.isEmpty()) {
                        int i = 0;
                        BigDecimal grandTotal = new BigDecimal(0);

                        for (i = 0; i < listt.size(); i++) {
                            if (i % 2 == 0) {
                                styleClass = "bgColor-white";
                            } else {
                                styleClass = "bgColor-Green";
                            }

                            out.print("<tr class='" + styleClass + "'>");
                            out.print("<td  class=\"t-align-left\"><input style=\"width:80%\" type=text class=formTxtBox_1 "
                                    + "name=srNo_" + i + " id=srNo_" + i + " value=\"" + listt.get(i)[0] + "\"  /></td>");
                            out.print("<td  class=\"t-align-left\"><input  type=text class=formTxtBox_1 style=\"width:90%\""
                                    + "name=Activity_" + i + " id=Activity_" + i + " value=\"" + listt.get(i)[1] + "\"  /></td>");
                            out.print("<td  class=\"t-align-left\"><input  type=text class=formTxtBox_1 "
                                    + "name=startDt_" + i + " id=startDt_" + i + " readonly=true value=\"" + DateUtils.gridDateToStrWithoutSec((Date) listt.get(i)[2]).split(" ")[0] + "\"  onclick =GetCal('startDt_" + i + "','startDt_" + i + "'," + i + ") /> "
                                    + "&nbsp;<a href=javascript:void(0) title=Calender "
                                    + "><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png alt=Select a Date border= style=vertical-align:middle onclick=GetCal('startDt_" + i + "','calc_" + i + "'," + i + ") /></a></td>");
                            out.print("<td  class=\"t-align-left\"><input style=\"width:80%\"  type=text class=formTxtBox_1 "
                                    + "name=noOfDays_" + i + " id=noOfDays_" + i + " value=\"" + listt.get(i)[3] + "\" onblur=f_date(" + i + ") onchange=checkDays(" + i + ") /></td>");
                            out.print("<td  class=\"t-align-left\"><input  type=text class=formTxtBox_1 "
                                    + "name=endDt_" + i + " id=endDt_" + i + " readonly=true value=\"" + DateUtils.gridDateToStrWithoutSec((Date) listt.get(i)[4]).split(" ")[0] + "\"  /></td>");
                            out.print("<td  class=\"t-align-left\"><input  type=text class=formTxtBox_1 style=\"width:90%\""
                                    + "name=remarks_" + i + " id=remarks_" + i + " value=\"" + listt.get(i)[5] + "\"  /></td>");
                            out.print("<input type=hidden name=srvwplanid_" + i + " id=srvwplanid_" + i + " value=" + listt.get(i)[7] + " />");
                            out.print("</tr>");
                        }
                        out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");

                        int totalPages = 1;

                        if (listt.size() > 0) {
                            int cc = (int) listt.size();
                            totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                    } else {
                        out.print("<tr>");
                        out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"9\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        out.print("</tr>");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if ("preparepr".equals(action)) {
                String max = request.getParameter("size");
                String first = request.getParameter("pageNo");
                boolean forSecondPr = service.forSecondPR(Integer.parseInt(request.getParameter("wpId")));
                try {
                    List<Object[]> list = null;
                    List<SPCommonSearchDataMore> listt = null;
                    listt = service.getListForEditForWorks(request.getParameter("wpId"), Integer.parseInt(first), Integer.parseInt(max));
                    if ("".equalsIgnoreCase(request.getParameter("isedit"))) {
                        list = service.getDatesByWpIdForWorks(request.getParameter("wpId"), Integer.parseInt(first), Integer.parseInt(max));
                    }
                    long rowcount = service.getRowcountForBoq(request.getParameter("wpId"));
                    String styleClass = "bgColor-White";
                    if ("".equalsIgnoreCase(request.getParameter("isedit"))) {
                        if (list != null && !list.isEmpty()) {
                            String link = "";
                            int i = 0;
                            for (i = 0; i < list.size(); i++) {


                                if (list.get(i)[12].toString().equalsIgnoreCase("variationI")) {
                                    out.print("<tr class='" + styleClass + "' style=background-color:#FFFF99>");
                                } else if (list.get(i)[12].toString().equalsIgnoreCase("variationU")) {
                                    out.print("<tr class='" + styleClass + "' style=background-color:#CDC0B0>");
                                } else {
                                    out.print("<tr class='" + styleClass + "'>");
                                }
                                out.print("<td class=\"t-align-center\">" + list.get(i)[0] + "</td>");
                                out.print("<td class=\"t-align-center\">" + list.get(i)[2] + "</td>");
                                out.print("<td style=\"text-align :right;\">" + list.get(i)[3] + "</td>");
                                out.print("<td style=\"text-align :right;\">" + list.get(i)[4] + "</td>");
                                if (forSecondPr) {
                                    out.print("<td style=\"text-align :right;\"><label name=disp_" + i + " id=disp_" + i + ">" + listt.get(i).getFieldName6() + "</label> </td>");

                                } else {
                                    out.print("<td style=\"text-align :right;\"><label name=disp_" + i + " id=disp_" + i + ">0</label> </td>");
                                }
                                if (!listt.isEmpty()) {
                                    if (list.get(i)[4].toString().equals(listt.get(i).getFieldName6())) {
                                        out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " style=\"display:none\" readonly  /> </td>");

                                    } else {
                                        out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " onkeypress=\"checkEnter(event," + i + ")\" onchange=\"checkPending(" + i + ")\" /> </td>");
                                    }
                                } else {
                                    if (list.get(i)[4].toString().toString().equals("0.000")) {
                                        out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " style=\"display:none\" readonly  /> </td>");
                                    } else {
                                        out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " onkeypress=\"checkEnter(event," + i + ")\" onchange=\"checkPending(" + i + ")\" /> </td>");
                                    }

                                }
                                out.print("<td class=\"t-align-center\"><label id=Qtypending_" + i + " name=Qtypending_" + i + "></label></td>");

                                if (!listt.isEmpty()) {
                                    if (list.get(i)[4].toString().equals(listt.get(i).getFieldName6())) {
                                        out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                                + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                + "name=txtdate_" + i + " style=\"display:none\" id=txtdate_" + i + " /></td>");

                                    } else {
                                        out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                                + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                + "name=txtdate_" + i + " id=txtdate_" + i + " />"
                                                + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtdate_" + i + "','calc_" + i + "') /></a></td>");
                                    }
                                } else {
                                    if (list.get(i)[4].toString().toString().equals("0.000")) {
                                        out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                                + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                + "name=txtdate_" + i + " style=\"display:none\" id=txtdate_" + i + " /></td>");
                                    } else {
                                        out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                                + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                + "name=txtdate_" + i + " id=txtdate_" + i + " />"
                                                + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtdate_" + i + "','calc_" + i + "') /></a></td>");
                                    }

                                }

                                if (!listt.isEmpty()) {
                                    if (list.get(i)[4].toString().equals(listt.get(i).getFieldName6())) {
                                        out.print("<td class=\"t-align-center\"><input type=text  class=formTxtBox_1 "
                                                + "name=remarks_" + i + " style=\"display:none\" id=remarks_" + i + "> </input> </td>");

                                    } else {
                                        out.print("<td class=\"t-align-center\"><input type=text  class=formTxtBox_1 "
                                                + "name=remarks_" + i + " id=remarks_" + i + " onblur=\"validateRemarks(" + i + ")\"> </input> </td>");
                                    }
                                } else {
                                    if (list.get(i)[4].toString().equals("0.000")) {
                                        out.print("<td class=\"t-align-center\"><input type=text  class=formTxtBox_1 "
                                                + "name=remarks_" + i + " style=\"display:none\" id=remarks_" + i + "> </input> </td>");
                                    } else {
                                        out.print("<td class=\"t-align-center\"><input type=text  class=formTxtBox_1 "
                                                + "name=remarks_" + i + " id=remarks_" + i + " onblur=\"validateRemarks(" + i + ")\"> </input> </td>");
                                    }

                                }
                                out.print("<input type=hidden name=detailid_" + i + " id=detailid_" + i + " value=\"" + list.get(i)[5] + "\" />");
                                out.print("<input type=hidden name=tableId_" + i + " id=tableId_" + i + " value=\"" + list.get(i)[10] + "\" />");
                                out.print("<input type=hidden name=qty_" + i + " id=qty_" + i + " value=\"" + list.get(i)[4] + "\" />");
                                out.print("<input type=hidden name=row_" + i + " id=row_" + i + " value=\"" + list.get(i)[9] + "\" />");
                                if (!listt.isEmpty()) {
                                    out.print("<input type=hidden name=prRepDetId_" + i + " id=prRepDetId_" + i + " value=\"" + listt.get(i).getFieldName12() + "\" />");

                                }
                                if (forSecondPr && !listt.isEmpty()) {
                                    out.print("<input type=hidden name=Qtyaccepted_" + i + " id=Qtyaccepted_" + i + " value=\"" + listt.get(i).getFieldName6() + "\" /> ");

                                } else {
                                    out.print("<input type=hidden name=Qtyaccepted_" + i + " id=Qtyaccepted_" + i + " value=0 />");
                                }
                                out.print("</tr>");

                            }
                            if (!listt.isEmpty()) {
//                                out.print("<input type=hidden name=prId id=prId value=\"" + listt.get(0)[11].toString() + "\" />");
//                                out.print("<input type=hidden name=prReqDate id=prReqDate value=\"" + listt.get(0)[13].toString() + "\" />");
                            }
                            out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");
                            int totalPages = 1;

                            if (list.size() > 0) {
                                int cc = (int) rowcount;
                                totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                                if (totalPages == 0) {
                                    totalPages = 1;
                                }
                            }

                            out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                        }
                    } else {
                        if (listt != null && !listt.isEmpty()) {
                            String link = "";
                            int i = 0;
                            String setblank = "";
                            for (SPCommonSearchDataMore oob : listt) {

                                out.print("<tr class='" + styleClass + "'>");


                                out.print("<td class=\"t-align-center\">" + oob.getFieldName1() + "</td>");
                                out.print("<td class=\"t-align-center\">" + oob.getFieldName3() + "</td>");
                                out.print("<td class=\"t-align-center\">" + oob.getFieldName4() + "</td>");
                                out.print("<td style=\"text-align :right;\">" + oob.getFieldName5() + "</td>");
                                out.print("<td style=\"text-align :right;\"><label name=disp_" + i + " id=disp_" + i + ">" + oob.getFieldName6() + "</label> </td>");
                                if (oob.getFieldName6() != null) {
                                    if (oob.getFieldName5().equals(oob.getFieldName6())) {
                                        out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " style=\"display:none\" readonly  /> </td>");

                                    } else {
                                        if (oob.getFieldName7().toString().equals("0.000")) {
                                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                    + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " onkeypress=\"checkEnter(event," + i + ")\" onchange=\"checkPending(" + i + ")\"  /> </td>");
                                        } else {
                                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                    + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " onkeypress=\"checkEnter(event," + i + ")\" onchange=\"checkPending(" + i + ")\" value=\"" + oob.getFieldName7() + "\" /> </td>");
                                        }
                                    }
                                }

                                out.print("<td style=\"text-align :right;\"><label id=Qtypending_" + i + " name=Qtypending_" + i + ">" + oob.getFieldName9() + "</label></td>");

                                if (oob.getFieldName6() != null) {
                                    if (oob.getFieldName5().equals(oob.getFieldName6())) {
                                        out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text value=\"" + oob.getFieldName10() + "\" class=formTxtBox_1 "
                                                + "readonly style=\"display:none\" onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                + "name=txtdate_" + i + " id=txtdate_" + i + " ></input></td>");
                                    } else {
                                        if (oob.getFieldName7().toString().equals("0.000")) {
                                            if (!"".equalsIgnoreCase(oob.getFieldName10())) {
                                                out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text value=\"" + oob.getFieldName10().split(" ")[0] + "\" class=formTxtBox_1 "
                                                        + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                        + "name=txtdate_" + i + " id=txtdate_" + i + " ></input>"
                                                        + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                        + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtdate_" + i + "','calc_" + i + "') /></a></td>");

                                            } else {
                                                out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                                        + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                        + "name=txtdate_" + i + " id=txtdate_" + i + " ></input>"
                                                        + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                        + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtdate_" + i + "','calc_" + i + "') /></a></td>");

                                            }
                                        } else {
                                            if (!"".equalsIgnoreCase(oob.getFieldName10())) {
                                                out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text value=\"" + oob.getFieldName10().split(" ")[0] + "\" class=formTxtBox_1 "
                                                        + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                        + "name=txtdate_" + i + " id=txtdate_" + i + " ></input>"
                                                        + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                        + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtdate_" + i + "','calc_" + i + "') /></a></td>");

                                            } else {
                                                out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                                        + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                        + "name=txtdate_" + i + " id=txtdate_" + i + " ></input>"
                                                        + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                        + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtdate_" + i + "','calc_" + i + "') /></a></td>");

                                            }
                                        }
                                    }
                                }

                                if (oob.getFieldName6() != null) {
                                    if (oob.getFieldName5().equals(oob.getFieldName6())) {
                                        out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                + "name=remarks_" + i + " style=\"display:none\" id=remarks_" + i + " ></input> </td>");
                                    } else {
                                        if (oob.getFieldName7().toString().equals("0.000")) {
                                            if ("".equalsIgnoreCase(oob.getFieldName11())) {
                                                out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                        + "name=remarks_" + i + " id=remarks_" + i + " onblur=\"validateRemarks(remarks_" + i + ")\"></input> </td>");

                                            } else {
                                                out.print("<td class=\"t-align-center\"><input type=text value=\"" + oob.getFieldName11() + "\" class=formTxtBox_1 "
                                                        + "name=remarks_" + i + " id=remarks_" + i + " onblur=\"validateRemarks(" + i + ")\"></input> </td>");

                                            }
                                        } else {
                                            if ("".equalsIgnoreCase(oob.getFieldName11())) {
                                                out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                        + "name=remarks_" + i + " id=remarks_" + i + " onblur=\"validateRemarks(" + i + ")\"></input> </td>");

                                            } else {
                                                out.print("<td class=\"t-align-center\"><input type=text value=\"" + oob.getFieldName11() + "\" class=formTxtBox_1 "
                                                        + "name=remarks_" + i + " id=remarks_" + i + " onblur=\"validateRemarks(" + i + ")\"></input> </td>");

                                            }
                                        }
                                    }
                                }
                                out.print("<input type=hidden name=tableId_" + i + " id=tableId_" + i + " value=\"" + oob.getFieldName8() + "\" />");
                                out.print("<input type=hidden name=qty_" + i + " id=qty_" + i + " value=\"" + oob.getFieldName5() + "\" />");
                                out.print("<input type=hidden name=row_" + i + " id=row_" + i + " value=\"" + oob.getFieldName14() + "\" />");

                                out.print("<input type=hidden name=Qtyaccepted_" + i + " id=Qtyaccepted_" + i + " value=\"" + oob.getFieldName6() + "\" />");

                                out.print("</tr>");
                                out.print("<input type=hidden name=prId id=prId value=\"" + listt.get(i).getFieldName13() + "\" />");
                                out.print("<input type=hidden name=prRepDetId_" + i + " id=prRepDetId_" + i + " value=\"" + listt.get(i).getFieldName12() + "\" />");
                                i++;
                            }

                            if (i == 0) {
                                i++;
                            }
                            out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");
                            int totalPages = 1;

                            if (listt.size() > 0) {
                                int cc = (int) rowcount;
                                totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                                if (totalPages == 0) {
                                    totalPages = 1;
                                }
                            }
                            out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if ("creatAttSheet".equalsIgnoreCase(action)) {
                TblCmsSrvAttSheetMaster sheetMaster = new TblCmsSrvAttSheetMaster();
                sheetMaster.setContractId(0);
                sheetMaster.setCreatedBy(Integer.parseInt(userId));
                sheetMaster.setCreatedDt(new Date());
                sheetMaster.setTenderId(tenderId);
                sheetMaster.setMonthId(Integer.parseInt(request.getParameter("month")));
                sheetMaster.setYear(Integer.parseInt(request.getParameter("year")));
                sheetMaster.setStatus("pending");
                cmss.addToAttMaster(sheetMaster);
                int count = Integer.parseInt(request.getParameter("listcount"));
                List<TblCmsSrvAttSheetDtl> srvAttSheetDtls = new ArrayList<TblCmsSrvAttSheetDtl>();
                for (int i = 0; i < count; i++) {
                    TblCmsSrvAttSheetDtl sheetDtl = new TblCmsSrvAttSheetDtl();
                    String remarks = request.getParameter("remarks_" + i);
                    sheetDtl.setNoOfDays(Integer.parseInt(request.getParameter("nod_" + i)));
                    sheetDtl.setRate(new BigDecimal(request.getParameter("rate_" + i)));
                    sheetDtl.setTblCmsSrvAttSheetMaster(new TblCmsSrvAttSheetMaster(sheetMaster.getAttSheetId()));
                    sheetDtl.setSrvTcid(Integer.parseInt(request.getParameter("tcId_" + i)));
                    sheetDtl.setWorkFrom(request.getParameter("WF_" + i));
                    if ("home".equalsIgnoreCase(request.getParameter("WF_" + i))) {
                        cmss.updateDaysCnt(request.getParameter("tcId_" + i), "0", request.getParameter("nod_" + i), "");
                    } else {
                        cmss.updateDaysCnt(request.getParameter("tcId_" + i), request.getParameter("nod_" + i), "0", "");
                    }
                    sheetDtl.setRemarksByPe(remarks);
                    srvAttSheetDtls.add(sheetDtl);
                }
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Progress_Report.getName(), "Create Attendance Sheet", "");
                cmss.addToAttSheetDetail(srvAttSheetDtls);
                sendMailforCreateAttansheet(Integer.toString(tenderId), Integer.toString(lotId), user.toString(), userTypeId.toString());
                response.sendRedirect("tenderer/AttanshtUploadDoc.jsp?tenderId=" + tenderId + "&keyId=" + sheetMaster.getAttSheetId() + "&docx=AttSheet&module=PR&msg=fin");
                //response.sendRedirect("tenderer/ProgressReportMain.jsp?tenderId=" + tenderId + "&msg=fin");
            } else if ("editsheet".equalsIgnoreCase(action)) {
                int count = Integer.parseInt(request.getParameter("listcount"));
                String status = request.getParameter("status");
                cmss.updateSheetMaster(request.getParameter("attsheetId"), status);
                for (int i = 0; i < count; i++) {
                    cmss.updateSheetDtls(request.getParameter("attdtlsheetId_" + i), status, request.getParameter("remarks_" + i));
                }
                sendMailforCreateAttansheet(Integer.toString(tenderId), Integer.toString(lotId), user.toString(), userTypeId.toString());
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Progress_Report.getName(), "Edit Attendance Sheet", "");
                response.sendRedirect("officer/AttanshtUploadDoc.jsp?tenderId=" + tenderId + "&keyId=" + request.getParameter("attsheetId") + "&docx=AttSheet&module=PR&msg=updated");
                //response.sendRedirect("officer/ProgressReportMain.jsp?tenderId=" + tenderId + "&msg=updated");

            } else if ("editsheettenside".equalsIgnoreCase(action)) {
                int count = Integer.parseInt(request.getParameter("listcount"));
                cmss.updateSheetMaster(request.getParameter("attsheetId"), "pending");
                for (int i = 0; i < count; i++) {
                    if ("home".equalsIgnoreCase(request.getParameter("WF_" + i))) {
                        cmss.updateDaysCnt(request.getParameter("tcId_" + i), "0", request.getParameter("nod_" + i), "edit");
                    } else {
                        cmss.updateDaysCnt(request.getParameter("tcId_" + i), request.getParameter("nod_" + i), "0", "edit");
                    }
                    cmss.updateSheetDtlsForTend(request.getParameter("attdtlsheetId_" + i), request.getParameter("nod_" + i));

                }
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Progress_Report.getName(), "Edit Attendance Sheet", "");
                response.sendRedirect("tenderer/AttanshtUploadDoc.jsp?tenderId=" + tenderId + "&keyId=" + request.getParameter("attsheetId") + "&&msg=updated");
                //response.sendRedirect("tenderer/ProgressReportMain.jsp?tenderId=" + tenderId + "&msg=updated");


            } //            else if("sendtconslnt".equalsIgnoreCase(action)){
            //                cmss.sendToConsultant(request.getParameter("sheetId"));
            //                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Progress_Report.getName(), "Send to Consultant Attendance Sheet", "");
            //                response.sendRedirect("officer/ProgressReportMain.jsp?tenderId=" + tenderId + "&msg=sendtcons");
            //            }
            else if ("addserviceworkplan".equalsIgnoreCase(action)) {

                int conNewRec = Integer.parseInt(request.getParameter("addcount"));
                List<TblCmsSrvWorkPlan> srvworkplan = new ArrayList<TblCmsSrvWorkPlan>();
                List<TblCmsSrvWphistory> srvwphistory = new ArrayList<TblCmsSrvWphistory>();
                List<Object[]> listt = cmss.getAllServiceWorkPlan(formMapId);
                String type = request.getParameter("type");
                try {
                    for (int i = 0; i < conNewRec; i++) {
                        TblCmsSrvWorkPlan swplist = new TblCmsSrvWorkPlan();
                        if (request.getParameter("srNo_" + i) != null) {
                            if (request.getParameter("srvwplanid_" + i) != null && !request.getParameter("srvwplanid_" + i).equals("") && !request.getParameter("srvwplanid_" + i).equals("null")) {
                                swplist.setSrvWorkPlanId(Integer.parseInt(request.getParameter("srvwplanid_" + i)));
                            }
                            swplist.setSrNo(request.getParameter("srNo_" + i));
                            swplist.setActivity(request.getParameter("Activity_" + i));
                            swplist.setStartDt(DateUtils.convertStringtoDate(request.getParameter("startDt_" + i), "dd-MMM-yyyy"));
                            swplist.setNoOfDays(Integer.parseInt(request.getParameter("noOfDays_" + i)));
                            swplist.setEndDt(DateUtils.convertStringtoDate(request.getParameter("endDt_" + i), "dd-MMM-yyyy"));
                            swplist.setRemarks(request.getParameter("remarks_" + i));
                            swplist.setSrvFormMapId(formMapId);
                            srvworkplan.add(swplist);
                        }
                    }
                    cmss.addToWorkPlan(srvworkplan);
                    List<Object> getMaxCount = cmss.getSrvWpHistMaxCount(formMapId);
                    for (int i = 0; i < conNewRec; i++) {
                        TblCmsSrvWphistory swphistlist = new TblCmsSrvWphistory();
                        if (!listt.get(i)[0].equals(request.getParameter("srNo_" + i))
                                || !listt.get(i)[1].equals(request.getParameter("Activity_" + i))
                                || !DateUtils.gridDateToStrWithoutSec((Date) listt.get(i)[2]).split(" ")[0].equals(request.getParameter("startDt_" + i))
                                || !listt.get(i)[3].equals(Integer.parseInt(request.getParameter("noOfDays_" + i).toString()))
                                || !DateUtils.gridDateToStrWithoutSec((Date) listt.get(i)[4]).split(" ")[0].equals(request.getParameter("endDt_" + i))
                                || !listt.get(i)[5].equals(request.getParameter("remarks_" + i))) {
                            swphistlist.setSrvWorkPlanId(Integer.parseInt(request.getParameter("srvwplanid_" + i)));
                            swphistlist.setSrNo(request.getParameter("srNo_" + i));
                            swphistlist.setActivity(request.getParameter("Activity_" + i));
                            swphistlist.setStartDt(DateUtils.convertStringtoDate(request.getParameter("startDt_" + i), "dd-MMM-yyyy"));
                            swphistlist.setNoOfDays(Integer.parseInt(request.getParameter("noOfDays_" + i)));
                            swphistlist.setEndDt(DateUtils.convertStringtoDate(request.getParameter("endDt_" + i), "dd-MMM-yyyy"));
                            swphistlist.setRemarks(request.getParameter("remarks_" + i));
                            swphistlist.setCreatedDate(new java.sql.Date(new java.util.Date().getTime()));
                            if (getMaxCount.get(0) != null) {
                                swphistlist.setHistCnt(Integer.parseInt(getMaxCount.get(0).toString()) + 1);
                            }
                            swphistlist.setSrvFormMapId(formMapId);
                            srvwphistory.add(swphistlist);
                        }
                    }
                    cmss.addToWorkPlanHistory(srvwphistory);
                    sendMailforEditWorkSch(Integer.toString(tenderId), Integer.toString(lotId), user.toString(), Integer.toString(srvFormMapId));
                    response.sendRedirect("officer/ServiceWorkPlan.jsp?tenderId=" + tenderId + "&formMapId=" + formMapId + "&srvBoqId=" + srvBoqId + "&Type=" + type + "&lotId=" + lotId + "&msg=succ");
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (type.equalsIgnoreCase("edit")) {
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), "Edit Service WorkPlan", "");
                    } else {
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), "Add Service WorkPlan", "");
                    }
                }

            } else if ("checkAttExist".equals(action)) {
                out.print(checkAttExist(request.getParameter("monthId"), request.getParameter("yearId"), request.getParameter("tenderId")));
                out.flush();
            } else if ("checkDaysFromAtt".equals(action)) {
                out.print(checkDaysFromAtt(request.getParameter("tcId"), request.getParameter("wrkfrm"), request.getParameter("nod")));
                out.flush();
            } else if ("srvsalreimbursable".equalsIgnoreCase(action)) {
                String max = request.getParameter("size");
                String styleClass = "";
                List<TblCmsSrvSalaryRe> listt = cmss.getSalaryReimbursData(formMapId);
                try {
                    if (listt != null && !listt.isEmpty()) {
                        int i = 0;
                        BigDecimal grandTotal = new BigDecimal(0);

                        for (i = 0; i < listt.size(); i++) {
                            if (i % 2 == 0) {
                                styleClass = "bgColor-white";
                            } else {
                                styleClass = "bgColor-Green";
                            }

                            out.print("<tr class='" + styleClass + "'>");
                            out.print("<td  class=\"t-align-left\">" + listt.get(i).getSrNo() + "</td>");
                            out.print("<td  class=\"t-align-left\">" + listt.get(i).getEmpName() + "</td>");
                            out.print("<td  class=\"t-align-left\">" + listt.get(i).getWorkFrom() + "</td>");
                            out.print("<td  class=\"t-align-left\"><input style=\"width:80%\"  type=text class=formTxtBox_1 "
                                    + "name=workMonths_" + i + " id=workMonths_" + i + " value=\"" + listt.get(i).getWorkMonths().setScale(2) + "\"  onchange=checkDays(" + i + ") /></td>");
                            out.print("<td  class=\"t-align-right\" style=\"text-align: right;\">" + listt.get(i).getEmpMonthSalary() + "</td>");
                            out.print("<td  class=\"t-align-right\" style=\"text-align: right;\"> <span id=muliply_" + i + ">" + listt.get(i).getTotalSalary() + "</span></td>");
                            out.print("<input type=hidden name=srvSrId_" + i + " id=srvSrId_" + i + " value=" + listt.get(i).getSrvSrid() + " />");
                            out.print("<input type=hidden name=monthRate_" + i + " id=monthRate_" + i + " value=" + listt.get(i).getEmpMonthSalary() + " />");
                            out.print("<input type=hidden name=totalSalary_" + i + " id=totalSalary_" + i + " value=" + listt.get(i).getTotalSalary() + " />");
                            out.print("</tr>");
                        }
                        out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");

                        int totalPages = 1;

                        if (listt.size() > 0) {
                            int cc = (int) listt.size();
                            totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                    } else {
                        out.print("<tr>");
                        out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"9\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        out.print("</tr>");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if ("updatesrvsalreimbursable".equalsIgnoreCase(action)) {
                int conNewRec = Integer.parseInt(request.getParameter("addcount"));
                //    List<TblCmsSrvSalaryRe> srvsalreimburse = new ArrayList<TblCmsSrvSalaryRe>();
                try {
                    for (int i = 0; i < conNewRec; i++) {
                        String totSalary = request.getParameter("totalSalary_" + i);
                        int srvSrId = Integer.parseInt(request.getParameter("srvSrId_" + i));
                        float worksmonth = new Float(request.getParameter("workMonths_" + i).toString());
                        cmss.updateSalReimbursable(srvSrId, worksmonth, totSalary);
                    }
                    //         cmss.addToSalaryRE(srvsalreimburse);
                    sendMailforEditWorkSch(Integer.toString(tenderId), Integer.toString(lotId), user.toString(), Integer.toString(srvFormMapId));
                    response.sendRedirect("officer/SrvSalReimbursable.jsp?tenderId=" + tenderId + "&formMapId=" + formMapId + "&srvBoqId=" + srvBoqId + "&lotId=" + lotId + "&Type=Edit&msg=succ");
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), "Edit Salary Reimbursable", "");
                }

            } else if ("srvstaffschedule".equalsIgnoreCase(action)) {
                String max = request.getParameter("size");
                String styleClass = "";
                List<TblCmsSrvStaffSch> listt = cmss.getStaffScheduleData(formMapId);
                try {
                    if (listt != null && !listt.isEmpty()) {
                        int i = 0;
                        BigDecimal grandTotal = new BigDecimal(0);

                        for (i = 0; i < listt.size(); i++) {
                            if (i % 2 == 0) {
                                styleClass = "bgColor-white";
                            } else {
                                styleClass = "bgColor-Green";
                            }
                            out.print("<tr class='" + styleClass + "'>");
                            out.print("<td  class=\"t-align-left\"> " + listt.get(i).getSrno() + "</td>");
                            out.print("<td  class=\"t-align-left\"> " + listt.get(i).getEmpName() + "</td>");
                            if (listt.get(i).getWorkFrom().equalsIgnoreCase("home")) {
                                out.print("<td  class=\"t-align-left\"><select style=\"width:50%\" class=formTxtBox_1 name=workFrom_" + i + " id=workFrom_" + i + " >"
                                        + " <option value=Home selected > Home </option>"
                                        + " <option value=Field > Field </option> </select> </td>");
                            } else {
                                out.print("<td  class=\"t-align-left\"><select style=\"width:50%\" class=formTxtBox_1 name=workFrom_" + i + " id=workFrom_" + i + " >"
                                        + " <option value=Home >Home </option>"
                                        + " <option value=Field selected > Field </option> </select> </td>");
                            }
                            out.print("<td  class=\"t-align-left\"><input  type=text class=formTxtBox_1 width=80% "
                                    + "name=startDt_" + i + " id=startDt_" + i + " readonly=true value=\"" + DateUtils.gridDateToStrWithoutSec((Date) listt.get(i).getStartDt()).split(" ")[0] + "\" onclick =GetCal('startDt_" + i + "','startDt_" + i + "') /> "
                                    + "&nbsp;<a href=javascript:void(0) title=Calender "
                                    + "><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png alt=Select a Date border= style=vertical-align:middle onclick=GetCal('startDt_" + i + "','calc_" + i + "') /></a></td>");
                            out.print("<td  class=\"t-align-left\"><input style=\"width:80%\"  type=text class=formTxtBox_1 "
                                    + "name=noOfDays_" + i + " id=noOfDays_" + i + " value=\"" + listt.get(i).getNoOfDays() + "\" onblur=f_date(" + i + ") onchange=checkDays(" + i + ") /></td>");
                            out.print("<td  class=\"t-align-left\"><input  type=text class=formTxtBox_1 "
                                    + "name=endDt_" + i + " id=endDt_" + i + " readonly=true value=\"" + DateUtils.gridDateToStrWithoutSec((Date) listt.get(i).getEndDt()).split(" ")[0] + "\"  /></td>");
                            out.print("<input type=hidden name=srvSsId_" + i + " id=srvSsId_" + i + " value=" + listt.get(i).getSrvSsid() + " />");
                            out.print("<input type=hidden name=srvTcId_" + i + " id=srvTcId_" + i + " value=" + listt.get(i).getSrvTcid() + " />");
                            out.print("</tr>");
                        }
                        out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");

                        int totalPages = 1;

                        if (listt.size() > 0) {
                            int cc = (int) listt.size();
                            totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                    } else {
                        out.print("<tr>");
                        out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"9\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        out.print("</tr>");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if ("TCvari".equalsIgnoreCase(action)) {
                String addEditAction = "Add Team Composition variation";
                if(request.getParameter("isEdit")!=null){
                    addEditAction = "Add/Edit Team Composition variation";
                }
                try{
                    int count = Integer.parseInt(request.getParameter("count"));
                    List<TblCmsSrvTcvari> tcvaris = new ArrayList<TblCmsSrvTcvari>();
                    for (int i = 0; i < count; i++) {
                        TblCmsSrvTcvari tcvari = new TblCmsSrvTcvari();
                        if (request.getParameter("sno_" + i) == null) {
                            continue;
                        }
                        if (request.getParameter("varDtlId" + i) != null) {
                            tcvari.setSrvTcvariId(Integer.parseInt(request.getParameter("varDtlId" + i)));
                        }
                        tcvari.setAreaOfExpertise(request.getParameter("AOE_" + i));
                        tcvari.setConsultPropCat(request.getParameter("cnsltcat_" + i));
                        tcvari.setEmpName(request.getParameter("name_" + i));
                        tcvari.setOrganization(request.getParameter("firm_" + i));
                        tcvari.setPositionAssigned(request.getParameter("position_" + i));
                        tcvari.setPositionDefined(request.getParameter("position_" + i));
                        tcvari.setSrno(request.getParameter("sno_" + i));
                        tcvari.setSrvFormMapId(srvFormMapId);
                        tcvari.setStaffCat(request.getParameter("staffcat_" + i));
                        tcvari.setTaskAssigned(request.getParameter("TA_" + i));
                        tcvari.setVariOrdId(Integer.parseInt(request.getParameter("varId")));
                        tcvaris.add(tcvari);
                    }
                    cmss.addToTcVari(tcvaris);
                    response.sendRedirect("officer/WorkScheduleMain.jsp?tenderId=" + tenderId + "&msg=done");
                }
                finally{
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), addEditAction, "");
                }
            } else if ("SSvari".equalsIgnoreCase(action)) {
                String addEditAction = "Add Staffing Schedule variation";
                if(request.getParameter("isEdit")!=null){
                    addEditAction = "Add/Edit Staffing Schedule variation";
                }
                try{
                int count = Integer.parseInt(request.getParameter("count"));
                List<TblCmsSrvSsvari> ssvaris = new ArrayList<TblCmsSrvSsvari>();
                for (int i = 0; i < count; i++) {
                    TblCmsSrvSsvari ssvari = new TblCmsSrvSsvari();
                    if (request.getParameter("sno_" + i) == null) {
                        continue;
                    }
                    if (request.getParameter("varDtlId" + i) != null) {
                        ssvari.setSrvSsvariId(Integer.parseInt(request.getParameter("varDtlId" + i)));
                    }
                    ssvari.setEmpName("");
                    ssvari.setEndDt(DateUtils.convertStringtoDate(request.getParameter("edate_" + i), "dd-MMM-yyyy"));
                    ssvari.setNoOfDays(Integer.parseInt(request.getParameter("nod_" + i)));
                    ssvari.setSrvFormMapId(srvFormMapId);
                    ssvari.setSrvTcvariId(Integer.parseInt(request.getParameter("name_" + i)));
                    ssvari.setStartDt(DateUtils.convertStringtoDate(request.getParameter("sdate_" + i), "dd-MMM-yyyy"));
                    ssvari.setWorkFrom(request.getParameter("WF_" + i));
                    ssvari.setSrno(request.getParameter("sno_" + i));
                    ssvari.setSrvFormMapId(srvFormMapId);
                    ssvari.setVariOrdId(Integer.parseInt(request.getParameter("varId")));
                    ssvaris.add(ssvari);
                    if (request.getParameter("varDtlId" + i) != null) {
                        BigDecimal bdoff = new BigDecimal(request.getParameter("nod_" + i)).divide(new BigDecimal(30), 2, RoundingMode.HALF_UP);
                        cmss.setMonthInSalaryReForVari(Integer.parseInt(request.getParameter("name_" + i)), bdoff.setScale(2, 0), request.getParameter("WF_" + i));
                    }

                }
                cmss.addToSsVari(ssvaris);
                response.sendRedirect("officer/WorkScheduleMain.jsp?tenderId=" + tenderId + "&msg=done");
                }finally {
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), addEditAction, "");
                }
            } else if ("SRvari".equalsIgnoreCase(action)) {
                String addEditAction = "Add Breakdown of Staff Remuneration variation";
                if(request.getParameter("isEdit")!=null){
                    addEditAction = "Edit Breakdown of Staff Remuneration variation";
                }
                try{
                int count = Integer.parseInt(request.getParameter("count"));
                List<TblCmsSrvSrvari> ssvaris = new ArrayList<TblCmsSrvSrvari>();
                for (int i = 0; i < count; i++) {
                    TblCmsSrvSrvari ssvari = new TblCmsSrvSrvari();
                    if (request.getParameter("sno_" + i) == null) {
                        continue;
                    }
                    if (request.getParameter("varDtlId" + i) != null) {
                        ssvari.setSrvSrvariId(Integer.parseInt(request.getParameter("varDtlId" + i)));
                    }
                    ssvari.setEmpName(request.getParameter("ename_" + i));
                    ssvari.setNoOfDays(0);
                    ssvari.setSrvTcvariId(Integer.parseInt(request.getParameter("name_" + i)));
                    ssvari.setWorkFrom(request.getParameter("WF_" + i));
                    ssvari.setWorkMonths(new BigDecimal(request.getParameter("month_" + i)));
                    ssvari.setEmpMonthSalary(new BigDecimal(request.getParameter("rate_" + i)));
                    ssvari.setTotalSalary(new BigDecimal(request.getParameter("rate_" + i)).multiply(new BigDecimal(request.getParameter("month_" + i))));
                    ssvari.setSrNo(request.getParameter("sno_" + i));
                    ssvari.setSrvFormMapId(srvFormMapId);
                    ssvari.setVariOrdId(Integer.parseInt(request.getParameter("varId")));
                    ssvaris.add(ssvari);
                }
                cmss.addToSrVari(ssvaris);
                response.sendRedirect("officer/WorkScheduleMain.jsp?tenderId=" + tenderId + "&msg=done");
                }finally {
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), addEditAction, "");
                }
            } else if ("acceptVariOrder".equalsIgnoreCase(action)) {
                try {
                    int varId = Integer.parseInt(request.getParameter("varId"));
                    List<TblCmsSrvWorkPlan> srvWorkPlans = new ArrayList<TblCmsSrvWorkPlan>();
                    List<TblCmsSrvTeamComp> srvTeamComps = new ArrayList<TblCmsSrvTeamComp>();
                    List<TblCmsSrvReExpense> reExpenses = new ArrayList<TblCmsSrvReExpense>();
                    List<TblCmsSrvStaffSch> srvStaffSchs = new ArrayList<TblCmsSrvStaffSch>();
                    List<TblCmsSrvSalaryRe> salaryRes = new ArrayList<TblCmsSrvSalaryRe>();
                    List<TblCmsSrvPaymentSch> paymentSchs = new ArrayList<TblCmsSrvPaymentSch>();
                    List<TblCmsSrvCnsltComp> comps = new ArrayList<TblCmsSrvCnsltComp>();
                    List<TblCmsSrvStaffDaysCnt> srvStaffDaysCnts = new ArrayList<TblCmsSrvStaffDaysCnt>();
                    TblCmsSrvWorkPlan plan = null;
                    TblCmsSrvTeamComp comp = null;
                    TblCmsSrvReExpense expense = null;
                    TblCmsSrvStaffSch staffSch = null;
                    TblCmsSrvSalaryRe re = null;
                    TblCmsSrvPaymentSch sch = null;
                    TblCmsSrvCnsltComp cnsltComp = null;
                    List<TblCmsSrvWpvari> plans = cmss.getSrvWpVariationOrder(varId);
                    List<TblCmsSrvTcvari> tcvaris = cmss.getDetailOfTCForVari(varId);
                    List<TblCmsSrvSrvari> srvaris = cmss.getDetailOfSRForVari(varId);
                    List<TblCmsSrvSsvari> ssvaris = cmss.getDetailOfSSForVari(varId);
                    List<TblCmsSrvRevari> revaris = cmss.getSrvReVariationOrder(varId);
                    List<TblCmsSrvPsvari> psvaris = cmss.getSrvPsVariationOrder(varId);
                    List<TblCmsSrvCcvari> ccvaris = cmss.getSrvCcVariationOrder(varId);
                    if (!plans.isEmpty()) {
                        for (TblCmsSrvWpvari srvWpvari : plans) {
                            plan = new TblCmsSrvWorkPlan();
                            plan = new TblCmsSrvWorkPlan();
                            plan.setSrNo(srvWpvari.getSrNo());
                            plan.setActivity(srvWpvari.getActivity());
                            plan.setStartDt(srvWpvari.getStartDt());
                            plan.setNoOfDays(srvWpvari.getNoOfDays());
                            plan.setEndDt(srvWpvari.getEndDt());
                            plan.setRemarks(srvWpvari.getRemarks());
                            plan.setSrvFormMapId(srvWpvari.getSrvFormMapId());
                            plan.setVariOrdId(varId);
                            srvWorkPlans.add(plan);
                        }
                        cmss.addToWorkPlan(srvWorkPlans);
                    }
                    if (!psvaris.isEmpty()) {
                        if ("time based".equalsIgnoreCase(srvType)) {
                            for (TblCmsSrvPsvari srvPsvari : psvaris) {
                                sch = new TblCmsSrvPaymentSch();
                                sch.setMilestone("");
                                sch.setPercentOfCtrVal(srvPsvari.getPercentOfCtrVal());
                                sch.setSrNo(srvPsvari.getSrNo());
                                sch.setStatus("pending");
                                sch.setDescription(srvPsvari.getDescription());
                                sch.setSrvFormMapId(srvPsvari.getSrvFormMapId());
                                sch.setPeenddate(srvPsvari.getPeenddate());
                                sch.setEndDate(srvPsvari.getEndDate());
                                sch.setVariOrdId(varId);
                                paymentSchs.add(sch);
                            }
                            cmss.addToPaymentSch(paymentSchs);
                        } else {
                            BigDecimal perCent = new BigDecimal(BigInteger.ZERO);
                            for (TblCmsSrvPsvari srvPsvari : psvaris) {
                                sch = new TblCmsSrvPaymentSch();
                                sch.setMilestone(srvPsvari.getMilestone());
                                sch.setPercentOfCtrVal(srvPsvari.getPercentOfCtrVal());
                                sch.setSrNo(srvPsvari.getSrNo());
                                sch.setStatus("pending");
                                sch.setDescription(srvPsvari.getDescription());
                                sch.setSrvFormMapId(srvPsvari.getSrvFormMapId());
                                sch.setPeenddate(srvPsvari.getPeenddate());
                                sch.setVariOrdId(varId);
                                sch.setEndDate(srvPsvari.getEndDate());
                                paymentSchs.add(sch);
                                perCent = perCent.add(srvPsvari.getPercentOfCtrVal());
                            }

                            List<Object> CV = cmss.getContractValue(tenderId);
                            BigDecimal F_cv = null;
                            if (CV != null && !CV.isEmpty()) {
                                F_cv = (perCent.divide(new BigDecimal(100)).multiply((BigDecimal) CV.get(0))).add((BigDecimal) CV.get(0));
                            }
                            TblCmsVariContractVal contractVals = new TblCmsVariContractVal();
                            contractVals.setContractValue(F_cv);
                            contractVals.setLotId(Integer.parseInt(request.getParameter("lotId")));
                            contractVals.setVariOrdCntValId(varId);
                            contractVals.setIsCurrent("yes");
                            service.countContVal(contractVals);
                            cmss.addToPaymentSch(paymentSchs);
                        }
                    }
                    if (!ccvaris.isEmpty()) {
                        if (!"time based".equalsIgnoreCase(srvType)) {
                            for (TblCmsSrvCcvari srvCcvari : ccvaris) {
                                cnsltComp = new TblCmsSrvCnsltComp();
                                cnsltComp.setSrNo(srvCcvari.getSrNo());
                                cnsltComp.setConsultantCtg(srvCcvari.getConsultantCtg());
                                cnsltComp.setKeyNosPe(srvCcvari.getKeyNosPe());
                                cnsltComp.setSupportNosPe(srvCcvari.getSupportNosPe());
                                cnsltComp.setSrvFormMapId(srvCcvari.getSrvFormMapId());
                                cnsltComp.setConsultantName(srvCcvari.getConsultantName());
                                cnsltComp.setKeyNos(srvCcvari.getKeyNos());
                                cnsltComp.setSupportNos(srvCcvari.getSupportNos());
                                cnsltComp.setVariOrdId(varId);
                                comps.add(cnsltComp);
                            }
                            cmss.addToConComp(comps);
                        } else {
                            for (TblCmsSrvCcvari srvCcvari : ccvaris) {
                                cnsltComp = new TblCmsSrvCnsltComp();
                                cnsltComp.setSrNo(srvCcvari.getSrNo());
                                cnsltComp.setConsultantCtg(srvCcvari.getConsultantCtg());
                                cnsltComp.setTotalNosPe(srvCcvari.getTotalNosPe());
                                cnsltComp.setMonths(srvCcvari.getMonths());
                                cnsltComp.setKeyNosPe(srvCcvari.getKeyNosPe());
                                cnsltComp.setSupportNosPe(srvCcvari.getSupportNosPe());
                                cnsltComp.setSrvFormMapId(srvCcvari.getSrvFormMapId());
                                cnsltComp.setConsultantName(srvCcvari.getConsultantName());
                                cnsltComp.setKeyNos(srvCcvari.getKeyNos());
                                cnsltComp.setVariOrdId(varId);
                                cnsltComp.setSupportNos(srvCcvari.getSupportNos());
                                comps.add(cnsltComp);
                            }
                            cmss.addToConComp(comps);
                        }
                    }
                    HashMap<Integer, Integer> hashMapTeamCompostion = new HashMap<Integer, Integer> ();
                    if (!tcvaris.isEmpty()) {
                        for (TblCmsSrvTcvari srvTcvari : tcvaris) {
                            comp = new TblCmsSrvTeamComp();
                            comp.setSrno(srvTcvari.getSrno());
                            comp.setPositionAssigned(srvTcvari.getPositionAssigned());
                            comp.setEmpName(srvTcvari.getEmpName());
                            comp.setOrganization(srvTcvari.getOrganization());
                            comp.setStaffCat(srvTcvari.getStaffCat());
                            comp.setPositionDefined(srvTcvari.getPositionDefined());
                            comp.setConsultPropCat(srvTcvari.getConsultPropCat());
                            comp.setAreaOfExpertise(srvTcvari.getAreaOfExpertise());
                            comp.setTaskAssigned(srvTcvari.getTaskAssigned());
                            comp.setSrvFormMapId(srvTcvari.getSrvFormMapId());
                            comp.setVariOrdId(varId);
                            srvTeamComps.add(comp);
                        }
                        cmss.addToTeamComp(srvTeamComps);
                        //Added to put correct srvTCId in Staffing schedule and Salary Reimbursible - START
                        for (int i=0; i<tcvaris.size(); i++) {
                            hashMapTeamCompostion.put(tcvaris.get(i).getSrvTcvariId(), srvTeamComps.get(i).getSrvTcid());
                        }
                        //Added to put correct srvTCId in Staffing schedule and Salary Reimbursible - END
                    }
                    if (!ssvaris.isEmpty()) {
                        for (int i = 0; i < ssvaris.size(); i++) {
                            staffSch = new TblCmsSrvStaffSch();
                            staffSch.setSrno(ssvaris.get(i).getSrno());
                            staffSch.setEmpName(ssvaris.get(i).getEmpName());
                            staffSch.setWorkFrom(ssvaris.get(i).getWorkFrom());
                            staffSch.setStartDt(ssvaris.get(i).getStartDt());
                            staffSch.setNoOfDays(ssvaris.get(i).getNoOfDays());
                            staffSch.setEndDt(ssvaris.get(i).getEndDt());
                            staffSch.setSrvFormMapId(ssvaris.get(i).getSrvFormMapId());
                            //staffSch.setSrvTcid(srvTeamComps.get(i).getSrvTcid());
                            //Added to put correct srvTCId in Staffing schedule
                            staffSch.setSrvTcid(hashMapTeamCompostion.get(ssvaris.get(i).getSrvTcvariId()));
                            staffSch.setVariOrdId(varId);
                            srvStaffSchs.add(staffSch);
                        }
                        cmss.addToStaffSch(srvStaffSchs);
                    }
                    if (!srvaris.isEmpty()) {
                        for (int i = 0; i < srvaris.size(); i++) {
                            re = new TblCmsSrvSalaryRe();
                            re.setSrNo(srvaris.get(i).getSrNo());
                            re.setNoOfDays(srvaris.get(i).getNoOfDays());
                            re.setVariOrdId(varId);
                            re.setEmpName(srvaris.get(i).getEmpName());
                            re.setWorkFrom(srvaris.get(i).getWorkFrom());
                            re.setWorkMonths(srvaris.get(i).getWorkMonths());
                            re.setEmpMonthSalary(srvaris.get(i).getEmpMonthSalary());
                            re.setTotalSalary(srvaris.get(i).getTotalSalary());
                            re.setSrvFormMapId(srvaris.get(i).getSrvFormMapId());
                            //re.setSrvTcid(srvTeamComps.get(i).getSrvTcid());
                            //Added to put correct srvTCId in Salary Reimbursible
                            re.setSrvTcid(hashMapTeamCompostion.get(srvaris.get(i).getSrvTcvariId()));
                            salaryRes.add(re);
                        }
                        cmss.addToSalaryRE(salaryRes);
                    }
                    if (!revaris.isEmpty()) {
                        List<Object[]> wpIdd = cmss.getWpID(tenderId, 16);
                        Object[] strWpId = null;
                        if (wpIdd != null && !wpIdd.isEmpty()) {
                            strWpId = wpIdd.get(0);
                        }
                        TblCmsWpDetail wpDetail = null;
                        List<TblCmsWpDetail> wpdd = service.getDetailwp(strWpId[0].toString());
                        List<TblCmsWpDetail> details = new ArrayList<TblCmsWpDetail>();
                        System.out.println(wpIdd.size());
                        System.out.println(wpdd.get(wpdd.size() - 1).getWpRowId());
                        int rowCounter = wpdd.get(wpdd.size() - 1).getWpRowId() + 1;
                        for (TblCmsSrvRevari revari : revaris) {
                            wpDetail = new TblCmsWpDetail();
                            wpDetail.setWpRowId(rowCounter);
                            rowCounter++;
                            wpDetail.setWpNoOfDays(0);
                            wpDetail.setTenderTableId(wpdd.get(0).getTenderTableId());
                            wpDetail.setUserTypeId(Integer.parseInt(userTypeId));
                            wpDetail.setWpSrNo(revari.getSrNo());
                            wpDetail.setWpDescription(revari.getDescription());
                            wpDetail.setWpQty(revari.getQty());
                            wpDetail.setWpRate(revari.getUnitCost());
                            wpDetail.setWpUom(revari.getUnit());
                            wpDetail.setWpItemStatus("pending");
                            wpDetail.setWpQualityCheckReq("no");
                            wpDetail.setWpEndDate(null);
                            wpDetail.setWpStartDate(null);
                            wpDetail.setAmendmentFlag("original");
                            wpDetail.setCreatedBy(Integer.parseInt(userId));
                            wpDetail.setGroupId(null);
                            wpDetail.setIsRepeatOrder("no");
                            wpDetail.setItemInvGenQty(BigDecimal.ZERO);
                            wpDetail.setItemInvStatus("pending");
                            wpDetail.setTblCmsWpMaster(new TblCmsWpMaster(Integer.parseInt(strWpId[0].toString())));
                            expense = new TblCmsSrvReExpense();
                            expense.setSrNo(revari.getSrNo());
                            expense.setCatagoryDesc(revari.getCatagoryDesc());
                            expense.setDescription(revari.getDescription());
                            expense.setUnit(revari.getUnit());
                            expense.setQty(revari.getQty());
                            expense.setUnitCost(revari.getUnitCost());
                            expense.setTotalAmt(revari.getTotalAmt());
                            expense.setSrvFormMapId(revari.getSrvFormMapId());
                            expense.setVariOrdId(varId);
                            reExpenses.add(expense);
                            details.add(wpDetail);
                        }
                        cmss.addToRE(reExpenses);
                        service.addToWpDetails(details);
                    }
                    TblCmsSrvStaffDaysCnt cnt = null;
                    List<TblCmsSrvStaffDaysCnt> staffDaysCnts = new ArrayList<TblCmsSrvStaffDaysCnt>();
                    if (!ssvaris.isEmpty()) {
                        int offsite = 0;
                        int onsite = 0;
                        for (int i = 0; i < ssvaris.size(); i++) {
                            if ("Home".equalsIgnoreCase(ssvaris.get(i).getWorkFrom())) {
                                offsite = ssvaris.get(i).getNoOfDays();
                            } else {
                                onsite = ssvaris.get(i).getNoOfDays();
                            }
                        }
                        for (int j = 0; j < srvTeamComps.size(); j++) {
                            cnt = new TblCmsSrvStaffDaysCnt();
                            cnt.setLotId(0);
                            cnt.setSrvTcid(srvTeamComps.get(j).getSrvTcid());
                            cnt.setTenderId(tenderId);
                            cnt.setUsedOffSiteCnt(0);
                            cnt.setusedOnSiteCnt(0);
                            cnt.setOffSiteCnt(offsite);
                            cnt.setTotalCnt(offsite + onsite);
                            cnt.setOnSiteCnt(onsite);
                            staffDaysCnts.add(cnt);
                        }
                        cmss.addToStaffdayCount(staffDaysCnts);
                    }


                    cmss.updateVarOrderWithStatus(varId, "accepted");
                    sendMailForVariationOrderAcceptedFromTenderer(Integer.toString(tenderId), Integer.toString(lotId), user.toString());
                    response.sendRedirect("tenderer/WorkScheduleMain.jsp?tenderId=" + tenderId + "&msg=accept");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                finally{
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), "Accept Variation Order", "");
                }
            } else if ("deleterowforvariationForTC".equalsIgnoreCase(action)) {
                try {
                    String val = request.getParameter("val");
                    int len = val.length();
                    String str = val.substring(0, len - 1);
                    cmss.deleteFromTCVari(str);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if ("deleterowforvariationForSS".equalsIgnoreCase(action)) {
                try {
                    String val = request.getParameter("val");
                    int len = val.length();
                    String str = val.substring(0, len - 1);
                    cmss.deleteFromSSVari(str);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if ("deleterowforvariationForSR".equalsIgnoreCase(action)) {
                try {
                    String val = request.getParameter("val");
                    int len = val.length();
                    String str = val.substring(0, len - 1);
                    cmss.deleteFromSRVari(str);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            ////////////////////added for complaint management/////////////////

            if (action.equals("fetchData")) {
                response.setContentType("text/xml;charset=UTF-8");
                ComplaintMgmtService mgmtService = (ComplaintMgmtService) AppContext.getSpringBean("complaintMgmtService");
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));
                List<TblReviewPanel> rPanel = null;
                try {
                    rPanel = mgmtService.getReviewPanels();
                } catch (Exception ex) {
                    Logger.getLogger(CMSSerCaseServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
                int totalPages = 0;
                long totalCount = rPanel.size();
                //                  totalCount = manageSbDpOfficeSrBean.getCntHeadOffice(type);
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                        totalPages = (int) (totalCount / Integer.parseInt(request.getParameter("rows")));
                    } else {
                        totalPages = (int) ((totalCount / Integer.parseInt(request.getParameter("rows"))) + 1);
                    }
                } else {
                    totalPages = 0;
                }
                if (rPanel != null && !rPanel.isEmpty()) {
                    out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                    out.print("<rows>");
                    out.print("<page>" + request.getParameter("page") + "</page>");

                    out.print("<total>" + totalPages + "</total>");
                    out.print("<records>" + rPanel.size() + "</records>");
                    // be sure to put text data in CDATA
                    int j = 0;
                    int srNo = 0;
                    int no = Integer.parseInt(request.getParameter("page"));
                    if (no == 1) {
                        j = 0;
                        srNo = 1;
                    } else {
                        if (Integer.parseInt(rows) == 30) {
                            j = ((no - 1) * 30);
                            srNo = ((no - 1) * 30) + 1;
                        } else if (Integer.parseInt(rows) == 20) {
                            j = ((no - 1) * 20);
                            srNo = ((no - 1) * 20) + 1;
                        } else {
                            j = ((no - 1) * 10);
                            srNo = ((no - 1) * 10) + 1;
                        }
                    }
                    String emailId = "";
                    for (int i = j; i < rPanel.size(); i++) {
                        try {
                            emailId = commonService.getEmailId(rPanel.get(i).getUserId() + "");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        out.print("<row id='" + j + "'>");
                        out.print("<cell><![CDATA[" + srNo + "]]></cell>");
                        out.print("<cell><![CDATA[" + emailId + "]]></cell>");
                        out.print("<cell>" + rPanel.get(i).getReviewPanelName() + "</cell>");
                        String editLink = "<a href=\"reviewPanel.htm?rpId=" + rPanel.get(i).getReviewPanelId() + "&isEdit=edit\">Edit</a>";
                        String viewLink = "<a href=\"ViewReviewPanelDetail.htm?rpId=" + rPanel.get(i).getReviewPanelId() + "\">View</a>";
                        out.print("<cell><![CDATA[" + editLink + " | " + viewLink + "]]></cell>");
                        out.print("</row>");
                        j++;
                        srNo++;
                    }
                    out.print("</rows>");
                }

            } else if ("updatestaffschedule".equalsIgnoreCase(action)) {
                CalculateMonths calcMonths = new CalculateMonths();
                int conNewRec = Integer.parseInt(request.getParameter("addcount"));
                List<Object[]> MaxCount = cmss.getSrvSsHistDistCount(formMapId);
                List<TblCmsSrvStaffSch> listt = cmss.getStaffScheduleData(formMapId);
                List<TblCmsSrvSshistory> srvsshistory = new ArrayList<TblCmsSrvSshistory>();
                try {
                    HashMap map = new HashMap();
                    int count = 0;
                    for (int id = 0; id < conNewRec; id++) {
                        if (!map.containsValue(request.getParameter("srvTcId_" + id))) {
                            map.put(count, request.getParameter("srvTcId_" + id));
                            count++;
                        }
                    }
                    for (int i = 0; i < conNewRec; i++) {
                        int maxNo = 0;
                        if (MaxCount.get(0) != null) {
                            Object[] maxcObj = MaxCount.get(0);
                            maxNo = Integer.parseInt(maxcObj[0].toString()) + 1;
                        }
                        int srvSsId = Integer.parseInt(request.getParameter("srvSsId_" + i));
                        String workFrom = request.getParameter("workFrom_" + i);
                        Date stDate = DateUtils.convertStringtoDate(request.getParameter("startDt_" + i), "dd-MMM-yyyy");
                        int noOfDays = Integer.parseInt(request.getParameter("noOfDays_" + i));
                        Date endDate = DateUtils.convertStringtoDate(request.getParameter("endDt_" + i), "dd-MMM-yyyy");
                        cmss.updateStaffingSchedule(srvSsId, workFrom, stDate, noOfDays, endDate);


                        if (!listt.get(i).getWorkFrom().equals(workFrom)
                                || !DateUtils.gridDateToStrWithoutSec((Date) listt.get(i).getStartDt()).split(" ")[0].equals(request.getParameter("startDt_" + i))
                                || listt.get(i).getNoOfDays() != (Integer.parseInt(request.getParameter("noOfDays_" + i).toString()))
                                || !DateUtils.gridDateToStrWithoutSec((Date) listt.get(i).getEndDt()).split(" ")[0].equals(request.getParameter("endDt_" + i))) {
                            TblCmsSrvSshistory tbhistory = new TblCmsSrvSshistory(0, srvSsId, formMapId, "", Integer.parseInt(request.getParameter("srvTcId_" + i)), workFrom, stDate, endDate, noOfDays, maxNo, new java.sql.Date(new java.util.Date().getTime()));
                            srvsshistory.add(tbhistory);
                        }
                    }
                    cmss.addToStaffScheduleHistory(srvsshistory);
                    for (int id = 0; id < map.size(); id++) {
                        int tcid = Integer.parseInt(map.get(id).toString());
                        // List<Object> totalDays = cmss.getTotalNoOfDaysFrmSS(tcid, );
                        int onSitecount = 0;
                        int offSitecount = 0;
                        // 1 -> HOme
                        // 2 -> field
                        for (int jj = 0; jj < conNewRec; jj++) {
                            Date stDate = DateUtils.convertStringtoDate(request.getParameter("startDt_" + jj), "dd-MMM-yyyy");
                            Date endDate = DateUtils.convertStringtoDate(request.getParameter("endDt_" + jj), "dd-MMM-yyyy");
                            if (tcid == Integer.parseInt(request.getParameter("srvTcId_" + jj))) {
                                if (request.getParameter("workFrom_" + jj).equalsIgnoreCase("Home")) {
                                    offSitecount = offSitecount + Integer.parseInt(request.getParameter("noOfDays_" + jj));
                                    cmss.setMonthInSalaryRe(tcid, calcMonths.getMonthsBetweenTwoDates(stDate, endDate), "Home");
                                } else {
                                    onSitecount = onSitecount + Integer.parseInt(request.getParameter("noOfDays_" + jj));
                                    cmss.setMonthInSalaryRe(tcid, calcMonths.getMonthsBetweenTwoDates(stDate, endDate), "Field");
                                }
                            }
                        }

                        cmss.updateStaffDayCount(tcid, onSitecount, offSitecount, onSitecount + offSitecount);

                    }
                    sendMailforEditWorkSch(Integer.toString(tenderId), Integer.toString(lotId), user.toString(), Integer.toString(srvFormMapId));
                    response.sendRedirect("officer/SrvStaffingSchedule.jsp?tenderId=" + tenderId + "&formMapId=" + formMapId + "&srvBoqId=" + srvBoqId + "&lotId=" + lotId + "&Type=Edit&msg=succ");
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), "Edit Staffing Schedule", "");
                }
            } else if ("addsrvwpvari".equalsIgnoreCase(action)) {
                String addEditAction = "Add Service WorkPlan Variation Order";
                if(request.getParameter("isEdit")!=null){
                    addEditAction = "Add/Edit Service WorkPlan Variation Order";
                }
                int conNewRec = Integer.parseInt(request.getParameter("addcount"));
                List<TblCmsSrvWpvari> srvwpvari = new ArrayList<TblCmsSrvWpvari>();
                List<TblCmsSrvWphistory> srvwphistory = new ArrayList<TblCmsSrvWphistory>();
                try {
                    for (int i = 0; i < conNewRec; i++) {
                        TblCmsSrvWpvari srvwpvarilist = new TblCmsSrvWpvari();
                        if (request.getParameter("srNo_" + i) != null) {
                            if (request.getParameter("srvWpvariId_" + i) != null && !request.getParameter("srvWpvariId_" + i).equals("") && !request.getParameter("srvWpvariId_" + i).equals("null")) {
                                srvwpvarilist.setSrvWpvariId(Integer.parseInt(request.getParameter("srvWpvariId_" + i)));
                            }
                            srvwpvarilist.setSrNo(request.getParameter("srNo_" + i));
                            srvwpvarilist.setActivity(request.getParameter("Activity_" + i));
                            srvwpvarilist.setStartDt(DateUtils.convertStringtoDate(request.getParameter("startDt_" + i), "dd-MMM-yyyy"));
                            srvwpvarilist.setNoOfDays(Integer.parseInt(request.getParameter("noOfDays_" + i)));
                            srvwpvarilist.setEndDt(DateUtils.convertStringtoDate(request.getParameter("endDt_" + i), "dd-MMM-yyyy"));
                            srvwpvarilist.setRemarks(request.getParameter("remarks_" + i));
                            srvwpvarilist.setSrvFormMapId(formMapId);
                            srvwpvarilist.setVariOrdId(varOrdId);
                            srvwpvari.add(srvwpvarilist);
                        }
                    }
                    cmss.AddToSrvWpVari(srvwpvari);
                    response.sendRedirect("officer/WorkScheduleMain.jsp?tenderId=" + tenderId + "&msg=done");
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), addEditAction, "");
                }

            } else if ("deletesrvwpvariation".equalsIgnoreCase(action)) {
                try {
                    String val = request.getParameter("val");
                    int len = val.length();
                    String str = val.substring(0, len - 1);
                    cmss.deleteSrvWpVariationOrder(str);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), "Delete Service WorkPlan Variation Order", "");
                }
            } else if ("addsrvpsvari".equalsIgnoreCase(action)) {
                String addEditAction = "Add Payment Schedule Variation Order";
                if(request.getParameter("isEdit")!=null){
                    addEditAction = "Add/Edit Payment Schedule Variation Order";
                }
                int conNewRec = Integer.parseInt(request.getParameter("addcount"));
                List<TblCmsSrvPsvari> srvpsvari = new ArrayList<TblCmsSrvPsvari>();
                try {
                    for (int i = 0; i < conNewRec; i++) {
                        TblCmsSrvPsvari srvpsvarilist = new TblCmsSrvPsvari();
                        if (request.getParameter("srNo_" + i) != null) {
                            if (request.getParameter("srvPsVarId_" + i) != null && !request.getParameter("srvPsVarId_" + i).equals("") && !request.getParameter("srvPsVarId_" + i).equals("null")) {
                                srvpsvarilist.setSrvPsvariId(Integer.parseInt(request.getParameter("srvPsVarId_" + i)));
                            }
                            String forms = request.getParameter("forms");
                            srvpsvarilist.setSrNo(request.getParameter("srNo_" + i));
                            if (forms.equalsIgnoreCase("lumpsum")) {
                                srvpsvarilist.setMilestone(request.getParameter("Milestone_" + i));
                                srvpsvarilist.setPercentOfCtrVal(new BigDecimal(request.getParameter("PercOfVal_" + i)));
                            }
                            srvpsvarilist.setDescription(request.getParameter("Descritpion_" + i));
                            srvpsvarilist.setPeenddate(DateUtils.convertStringtoDate(request.getParameter("datePropByPE_" + i), "dd-MMM-yyyy"));
                            srvpsvarilist.setEndDate(DateUtils.convertStringtoDate(request.getParameter("datePropByCon_" + i), "dd-MMM-yyyy"));
                            srvpsvarilist.setSrvFormMapId(formMapId);
                            srvpsvarilist.setVariOrdId(varOrdId);
                            srvpsvari.add(srvpsvarilist);
                        }
                    }
                    cmss.AddToSrvPsVari(srvpsvari);
                    response.sendRedirect("officer/WorkScheduleMain.jsp?tenderId=" + tenderId + "&msg=done");
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), addEditAction, "");
                }

            } else if ("deletesrvpsvariation".equalsIgnoreCase(action)) {
                try {
                    String val = request.getParameter("val");
                    int len = val.length();
                    String str = val.substring(0, len - 1);
                    cmss.deleteSrvPsVariationOrder(str);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), "Delete Payment Schedule Variation Order", "");
                }

            } else if ("addsrvccvari".equalsIgnoreCase(action)) {
                String addEditAction = "Add Consultant Composition Variation Order";
                if(request.getParameter("isEdit")!=null){
                    addEditAction = "Add/Edit Consultant Composition Variation Order";
                }
                int conNewRec = Integer.parseInt(request.getParameter("addcount"));
                List<TblCmsSrvCcvari> srvccvari = new ArrayList<TblCmsSrvCcvari>();
                try {
                    for (int i = 0; i < conNewRec; i++) {
                        TblCmsSrvCcvari srvccvarilist = new TblCmsSrvCcvari();
                        if (request.getParameter("srNo_" + i) != null) {
                            if (request.getParameter("srvCcVarId_" + i) != null && !request.getParameter("srvCcVarId_" + i).equals("") && !request.getParameter("srvCcVarId_" + i).equals("null")) {
                                srvccvarilist.setSrvCcvariId(Integer.parseInt(request.getParameter("srvCcVarId_" + i)));
                            }
                            String forms = request.getParameter("forms");
                            srvccvarilist.setSrNo(request.getParameter("srNo_" + i));
                            if (forms.equalsIgnoreCase("timebase")) {
                                srvccvarilist.setTotalNosPe(Integer.parseInt(request.getParameter("inNoOfKeyProfstafftxt_" + i)));
                                srvccvarilist.setMonths(new BigDecimal(request.getParameter("monthstxt_" + i)));
                            }
                            srvccvarilist.setConsultantCtg(request.getParameter("cnlcat_" + i));
                            srvccvarilist.setKeyNosPe(Integer.parseInt(request.getParameter("noOffStafftxt_" + i)));
                            srvccvarilist.setSupportNosPe(Integer.parseInt(request.getParameter("noOffSupportStafftxt_" + i)));
                            srvccvarilist.setConsultantName(request.getParameter("Consltnametxt_" + i));
                            srvccvarilist.setKeyNos(Integer.parseInt(request.getParameter("NoOffKeyPrStafftxt_" + i)));
                            srvccvarilist.setSupportNos(Integer.parseInt(request.getParameter("NoOffSupportPrStafftxt_" + i)));
                            srvccvarilist.setSrvFormMapId(formMapId);
                            srvccvarilist.setVariOrdId(varOrdId);
                            srvccvari.add(srvccvarilist);

                        }
                    }
                    cmss.AddToSrvCcVari(srvccvari);
                    response.sendRedirect("officer/WorkScheduleMain.jsp?tenderId=" + tenderId + "&msg=done");
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), addEditAction, "");
                }

            } else if ("deletesrvccvariation".equalsIgnoreCase(action)) {
                try {
                    String val = request.getParameter("val");
                    int len = val.length();
                    String str = val.substring(0, len - 1);
                    cmss.deleteSrvCcVariationOrder(str);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), "Delete Consultant Composition Variation Order", "");
                }
            } else if ("addsrvrevari".equalsIgnoreCase(action)) {
                String addEditAction = "Add Reimbursable Expenses Variation Order";
                if(request.getParameter("isEdit")!=null){
                    addEditAction = "Add/Edit Reimbursable Expenses Variation Order";
                }
                int conNewRec = Integer.parseInt(request.getParameter("addcount"));
                List<TblCmsSrvRevari> srvrevari = new ArrayList<TblCmsSrvRevari>();
                HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                try {
                    for (int i = 0; i < conNewRec; i++) {
                        TblCmsSrvRevari srvrevarilist = new TblCmsSrvRevari();
                        if (request.getParameter("srNo_" + i) != null) {
                            if (request.getParameter("srvReVarId_" + i) != null && !request.getParameter("srvReVarId_" + i).equals("") && !request.getParameter("srvReVarId_" + i).equals("null")) {
                                srvrevarilist.setSrvRevariId(Integer.parseInt(request.getParameter("srvReVarId_" + i)));
                            }

                            srvrevarilist.setSrNo(request.getParameter("srNo_" + i));
                            srvrevarilist.setCatagoryDesc(request.getParameter("categoryDesc_" + i));
                            srvrevarilist.setDescription(request.getParameter("Description_" + i));
                            srvrevarilist.setUnit(request.getParameter("Unit_" + i));
                            srvrevarilist.setQty(new BigDecimal(handleSpecialChar.handleSpecialChar(request.getParameter("qtytxt_" + i))));
                            srvrevarilist.setUnitCost(new BigDecimal(request.getParameter("unitCosttxt_" + i)));
                            srvrevarilist.setTotalAmt(new BigDecimal(request.getParameter("muliplytxt" + i)));
                            srvrevarilist.setRowId(0);
                            srvrevarilist.setTenderTableId(0);
                            srvrevarilist.setSrvFormMapId(formMapId);
                            srvrevarilist.setVariOrdId(varOrdId);
                            srvrevari.add(srvrevarilist);
                        }
                    }
                    cmss.AddToSrvReVari(srvrevari);
                    response.sendRedirect("officer/WorkScheduleMain.jsp?tenderId=" + tenderId + "&msg=done");
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), addEditAction, "");
                }

            } else if ("deletesrvrevariation".equalsIgnoreCase(action)) {
                try {
                    String val = request.getParameter("val");
                    int len = val.length();
                    String str = val.substring(0, len - 1);
                    cmss.deleteSrvReVariationOrder(str);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), "Delete Reimbursable Expenses Variation Order", "");
                }
            } else if ("sendtoten".equalsIgnoreCase(action)) {
                try{
                int varId = Integer.parseInt(request.getParameter("VariOId"));
                cmss.updateVarOrderWithStatus(varId, "sendtoten");
                response.sendRedirect("officer/WorkScheduleMain.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=approved");
                }finally{
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Schedule.getName(), "Send to Consultant", "");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(CMSSerCaseServlet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CMSSerCaseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CMSSerCaseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";


    }// </editor-fold>

    private long checkAttExist(String monId, String yearId, String tenderId) {
        long val = 0;
        val = cmss.checkAttExist(monId, yearId, tenderId);


        return val;


    }

    private long checkDaysFromAtt(String tcId, String nod, String workfrm) {
        long val = 0;
        val = cmss.checkDaysFromAtt(tcId, nod, workfrm);


        return val;

    }

    /** send mail notification to tenderer on editing the work schedule (forms) by PE */
    private boolean sendMailforEditWorkSch(String tenderId, String lotId, String logUserId, String srvFormMapId) {
        boolean flag = false;
        String procnature = commonService.getProcNature(tenderId).toString();
        String srvBoqType = cmss.getsrvBoqType(Integer.parseInt(srvFormMapId));
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peName = "";
            String strTo[] = {obj[9].toString()};
            String strFrom = commonService.getEmailId(logUserId);
            List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(logUserId));
            if (peNameObj.get(0) != null) {
                peName = peNameObj.get(0).toString();
            }
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String mailSubject = "e-GP: PE has Edited Work Schedule";
            registerService.contentAdmMsgBox(strTo[0], strFrom, mailSubject, msgBoxContentUtility.MailforEditWorkSch(srvBoqType, c_isPhysicalPrComplete, peName, obj));
            String mailText = mailContentUtility.MailforEditWorkSch(srvBoqType, c_isPhysicalPrComplete, peName, obj);
            sendMessageUtil.setEmailTo(strTo);
            sendMessageUtil.setEmailFrom(strFrom);
            sendMessageUtil.setEmailSub(mailSubject);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            sendMessageUtil.setSmsNo(accPaymentService.getMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
            StringBuilder sb = new StringBuilder();
            sb.append("Dear User,%0AThis is to inform you that PE has edited Work Schedule");
            sb.append("for Contract No." + obj[1].toString() + " (" + obj[15].toString() + ")" + "");
            sb.append("%0AThanks,%0AeGP System");
            sendMessageUtil.setSmsBody(sb.toString());
            sendMessageUtil.sendSMS();
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }

    /** send mail notification to tenderer on placing the variation oreder in services case by PE */
    private boolean sendMailForVariationOrderFromPE(String tenderId, String lotId, String logUserId) {
        boolean flag = false;
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peName = "";
            String strTo[] = {obj[9].toString()};
            String strFrom = commonService.getEmailId(logUserId);
            List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(logUserId));
            if (peNameObj.get(0) != null) {
                peName = peNameObj.get(0).toString();
            }
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String mailSubject = "e-GP: Variation Order Placed by PE";
            registerService.contentAdmMsgBox(strTo[0], strFrom, mailSubject, msgBoxContentUtility.variationOrderFromPEToContractor(c_isPhysicalPrComplete, peName, obj));
            String mailText = mailContentUtility.variationOrderFromPEToContractor(c_isPhysicalPrComplete, peName, obj);
            sendMessageUtil.setEmailTo(strTo);
            sendMessageUtil.setEmailFrom(strFrom);
            sendMessageUtil.setEmailSub(mailSubject);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            sendMessageUtil.setSmsNo(accPaymentService.getMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
            StringBuilder sb = new StringBuilder();
            sb.append("Dear User,%0AThis is to inform you that PE has placed Variation Order");
            sb.append("for Contract No." + obj[1].toString() + " (" + obj[15].toString() + ")" + "");
            sb.append("%0AThanks,%0AeGP System");
            sendMessageUtil.setSmsBody(sb.toString());
            sendMessageUtil.sendSMS();
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }

    /** send mail notification to PE on accepting the variation order by supplier */
    private boolean sendMailForVariationOrderAcceptedFromTenderer(String tenderId, String lotId, String logUserId) {
        boolean flag = false;
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peEmailId = "";
            String peName = "";
            String strFrom = commonService.getEmailId(logUserId);
            List<Object[]> peEmailObj = accPaymentService.getPEemailID(Integer.parseInt(tenderId));
            if (!peEmailObj.isEmpty()) {
                Object[] objj = peEmailObj.get(0);
                peEmailId = objj[0].toString();
            }
            if ("1".equals(obj[12].toString())) {
                peName = obj[10].toString() + " " + obj[11].toString();
            } else {
                peName = obj[8].toString();
            }
            String strTo[] = {peEmailId};
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String mailSubject = "e-GP: Acceptance of Variation Order by Consultant";
            registerService.contentAdmMsgBox(strTo[0], strFrom, mailSubject, msgBoxContentUtility.variationOrderFromContractorToPE(c_isPhysicalPrComplete, peName, obj));
            String mailText = mailContentUtility.variationOrderFromContractorToPE(c_isPhysicalPrComplete, peName, obj);
            sendMessageUtil.setEmailTo(strTo);
            sendMessageUtil.setEmailFrom(strFrom);
            sendMessageUtil.setEmailSub(mailSubject);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
            StringBuilder sb = new StringBuilder();
            sb.append("Dear User,%0AThis is to inform you that Consultant has accepted Variation Order ");
            sb.append("for Contract No." + obj[1].toString() + " (" + obj[15].toString() + ")" + "");
            sb.append("%0AThanks,%0AeGP System");
            sendMessageUtil.setSmsBody(sb.toString());
            sendMessageUtil.sendSMS();
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }

    /** send mail notification to PE on generating invoice by tenderer */
    private boolean sendMailForInvoiceGeneratedbySupplier(String tenderId, String lotId, String logUserId, int invoiceId) {
        boolean flag = false;
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        Object[] invmstobj = null;
        String InvoiceAmt = "";
        String MileStoneName = "";
        String InvoiceNo = "";
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peEmailId = "";
            List<Object[]> peEmailObj = accPaymentService.getPEemailID(Integer.parseInt(tenderId));
            if (!peEmailObj.isEmpty()) {
                Object[] objj = peEmailObj.get(0);
                peEmailId = objj[0].toString();
            }
            String strTo[] = {peEmailId};
            String strFrom = commonService.getEmailId(logUserId);
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            List<Object[]> getInvAmtMstObj = c_ConsSrv.getInvAmtAndMilestoneName(invoiceId);
            List<Object[]> getInvAmt = c_ConsSrv.getInvoiceTotalAmt(Integer.toString(invoiceId));
           /* if (!getInvAmt.isEmpty()) {
                InvoiceAmt = getInvAmt.get(0)[0].toString();
            }*/
            List<Object> getInvNo = c_ConsSrv.getInvoiceNo(Integer.toString(invoiceId));
            if(getInvNo.get(0)!=null)
            {
                InvoiceNo = getInvNo.get(0).toString();
            }
            String serviceType = commonService.getServiceTypeForTender(Integer.parseInt(tenderId));
            if (!"Time based".equalsIgnoreCase(serviceType.toString())) {
                if (!getInvAmtMstObj.isEmpty()) {
                    invmstobj = getInvAmtMstObj.get(0);
                    InvoiceAmt = invmstobj[0].toString();
                    MileStoneName = invmstobj[1].toString();
                    InvoiceNo = invmstobj[2].toString();
                }
            }
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String mailSubject = "e-GP: Invoice Generated by Consultant";
            registerService.contentAdmMsgBox(strTo[0], strFrom, mailSubject, msgBoxContentUtility.invoiceGeneratedbySupplie("has Generated", c_isPhysicalPrComplete, obj,InvoiceAmt,MileStoneName,InvoiceNo));
            String mailText = mailContentUtility.invoiceGeneratedbySupplie("has Generated", c_isPhysicalPrComplete, obj,InvoiceAmt,MileStoneName,InvoiceNo);
            sendMessageUtil.setEmailTo(strTo);
            sendMessageUtil.setEmailFrom(strFrom);
            sendMessageUtil.setEmailSub(mailSubject);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
            StringBuilder sb = new StringBuilder();
            sb.append("Dear User,%0AThis is to inform you that Consultant has Generated Invoice ");
            sb.append("for Contract No." + obj[1].toString() + " (" + obj[15].toString() + ")" + " ");
            sb.append("%0AThanks,%0AeGP System");
            sendMessageUtil.setSmsBody(sb.toString());
            sendMessageUtil.sendSMS();
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }

    /** send mail notification to tenderer on creating the attandancesheet by PE */
    private boolean sendMailforCreateAttansheet(String tenderId, String lotId, String logUserId, String userTypeId) {
        boolean flag = false;
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peEmailId = "";
            String peName = "";
            List<Object[]> peEmailObj = accPaymentService.getPEemailID(Integer.parseInt(tenderId));
            if (!peEmailObj.isEmpty()) {
                Object[] objj = peEmailObj.get(0);
                peEmailId = objj[0].toString();
            }
            String strTo[] = new String[1];
            String strFrom = commonService.getEmailId(logUserId);
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String mailSubject = "";

            if ("2".equalsIgnoreCase(userTypeId)) {
                strTo[0] = peEmailId;
                mailSubject = "e-GP: Attedance Sheet created by Consultant";
            } else {
                strTo[0] = obj[9].toString();
                List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(logUserId));
                if (peNameObj.get(0) != null) {
                    peName = peNameObj.get(0).toString();
                }
                mailSubject = "e-GP: Attedance Sheet Accepted / Rejected by PE";
            }
            registerService.contentAdmMsgBox(strTo[0], strFrom, mailSubject, msgBoxContentUtility.CreatedAttanSheetbySupplier(c_isPhysicalPrComplete, peName, obj, userTypeId));
            String mailText = mailContentUtility.CreatedAttanSheetbySupplier(c_isPhysicalPrComplete, peName, obj, userTypeId);
            sendMessageUtil.setEmailTo(strTo);
            sendMessageUtil.setEmailFrom(strFrom);
            sendMessageUtil.setEmailSub(mailSubject);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
            StringBuilder sb = new StringBuilder();
            if ("2".equalsIgnoreCase(userTypeId)) {
                sb.append("Dear User,%0AThis is to inform you that Consultant has prepared Attendance Sheet ");
                sb.append("for Contract No." + obj[1].toString() + " (" + obj[15].toString() + ")" + " ");
                sb.append("%0AThanks,%0AeGP System");
            } else {
                sb.append("Dear User,%0AThis is to inform you that PE has Accepted / Rejected Attendance Sheet ");
                sb.append("for Contract No." + obj[1].toString() + " (" + obj[15].toString() + ")" + "");
                sb.append("%0AThanks,%0AeGP System");
            }
            sendMessageUtil.setSmsBody(sb.toString());
            sendMessageUtil.sendSMS();
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }
}
