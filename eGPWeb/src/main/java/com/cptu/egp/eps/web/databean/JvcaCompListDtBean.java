/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

/**
 *
 * @author Administrator
 */
public class JvcaCompListDtBean {

    private int ventureId;
    private int jvCompanyId;
    private String companyName;
    private String companyRegNumber;
    private String jvcaRole;

    public int getVentureId() {
        return ventureId;
    }

    public void setVentureId(int ventureId) {
        this.ventureId = ventureId;
    }

    public int getJvCompanyId() {
        return jvCompanyId;
    }

    public void setJvCompanyId(int jvCompanyId) {
        this.jvCompanyId = jvCompanyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyRegNumber() {
        return companyRegNumber;
    }

    public void setCompanyRegNumber(String companyRegNumber) {
        this.companyRegNumber = companyRegNumber;
    }

    public String getJvcaRole() {
        return jvcaRole;
    }

    public void setJvcaRole(String jvcaRole) {
        this.jvcaRole = jvcaRole;
    }

    public JvcaCompListDtBean(int ventureId, int jvCompanyId, String companyName, String companyRegNumber, String jvcaRole) {
        this.ventureId = ventureId;
        this.jvCompanyId = jvCompanyId;
        this.companyName = companyName;
        this.companyRegNumber = companyRegNumber;
        this.jvcaRole = jvcaRole;
    }

}
