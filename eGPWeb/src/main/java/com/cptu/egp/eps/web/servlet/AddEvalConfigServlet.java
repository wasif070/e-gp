package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.web.servicebean.EvalConfigSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.*;

/**
 *
 * @author pulkit
 */
public class AddEvalConfigServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
      //  System.out.println("session.getAttribute(userId)=======================>" + (session.getAttribute("userId")));
        if (session.getAttribute("userId") != null || session.getAttribute("userName") != null) {
            try {
                boolean isDelMapping=false;
                int tenderId = 0;
                String tecTEC = "";
                String tscTEC = "";
                String userId = "";
                String evalCommittee = "";
                String configType = "";
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String ispost = "";
                String tscRequired = "";
                String dbOperation = "";
                String previousisTSCReq = "";
                if(request.getParameter("action")!=null && request.getParameter("action").trim().length()!=0){
                  dbOperation=request.getParameter("action");
                }

                if (request.getParameter("tenderid") != null) {
                    tenderId = Integer.parseInt(request.getParameter("tenderid"));
                }

                if (request.getParameter("ind") != null) {
                    configType = request.getParameter("ind");
                }

                if (request.getParameter("TSCyes") != null && request.getParameter("TSCyes").trim().length() != 0) {
                    tscRequired = request.getParameter("TSCyes");
                } 

                String temp_TecMem = request.getParameter("allTecMem");
                //String temp_TscMem = request.getParameter("temp_TscMem");
                if (request.getParameter("TecTEC") != null) {
                    tecTEC = request.getParameter("TecTEC");
                } else {
                    tecTEC = "";
                }

                if (request.getParameter("TscTEC") != null) {
                    tscTEC = request.getParameter("TscTEC");
                } else {
                    tscTEC = "";
                }

                userId = (String) (session.getAttribute("userId").toString());

                //String userName = (String) (session.getAttribute("userName"));


                if (request.getParameter("ispost") != null && request.getParameter("ispost").trim().length() != 0) {
                    ispost = request.getParameter("ispost");
                }
                
                if (request.getParameter("hdnOldTSCyes") != null) {
                    if("yes".equalsIgnoreCase(request.getParameter("hdnOldTSCyes"))){
                        previousisTSCReq="yes";
                        if ("no".equalsIgnoreCase(tscRequired)){
                            isDelMapping=true;
                        }
                    }
                }


               /* System.out.print(evalCommittee);
                System.out.println("tenderId====================>>>>>>>>>>" + tenderId);
                // System.out.println("USerName====================>" + userName);
                System.out.println("configType====================>" + configType);
                
                System.out.println("TecTEC====================>" + tecTEC);
                System.out.println("TscTEC====================>" + tscTEC);
                System.out.println("tecMemberId====================>" + request.getParameter("tecTEC_UserId") );
                System.out.println("tscMemberId====================>" + request.getParameter("tscTEC_UserId") );
                System.out.println("TSCyes====================>" + tscRequired);

                System.out.println("userId====================>" + userId);
                System.out.println("temp_TecMem===============>"+temp_TecMem);
                System.out.println("action ==============>"+request.getParameter("action"));
                System.out.println("-------------------------------------------------------------------------");
               */
                int tecMemberId = 0;
                int tscMemberId = 0;

                if (configType.equalsIgnoreCase("ind")) {
                    evalCommittee = temp_TecMem.trim();
                    tecMemberId = 0;
                    tscMemberId = 0;
                    tecTEC="";
                    tscTEC="";
                    System.out.println("inside the ind entry");
                   

                } else if (configType.equalsIgnoreCase("team")) {
                     System.out.println("inside the team1 entry with tec and tsc");
                     
                        if(tscRequired.equalsIgnoreCase("yes")){
                           evalCommittee = evalCommittee + tecTEC.trim() + "," + tscTEC.trim()+",";
                           tecMemberId = Integer.parseInt(request.getParameter("tecTEC_UserId"));
                           tscMemberId = Integer.parseInt(request.getParameter("tscTEC_UserId"));
                        }else if(tscRequired.equalsIgnoreCase("no")){
                           evalCommittee = evalCommittee + tecTEC.trim()+ ",";
                           tecMemberId = Integer.parseInt(request.getParameter("tecTEC_UserId"));
                           tscMemberId = 0;
                        }
                        

                 }
                

                
/*
                System.out.print("evalCommittee==============>" + evalCommittee);
                System.out.println("tenderId====================>>>>>>>>>>" + tenderId);
                // System.out.println("USerName====================>" + userName);
                System.out.println("configType====================>" + configType);
                System.out.println("TSCyes====================>" + tscRequired);

                System.out.println("userId====================>" + userId);
                System.out.println("TecTEC====================>" + tecTEC);
                System.out.println("TscTEC====================>" + tscTEC);
                System.out.println("tecMemberId====================>" + tecMemberId + "  " + temp_TecMem);
  */
                
                String documentBrief = "";
                String queryString;
                int uId = Integer.parseInt(userId);
                HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                //  documentBrief = handleSpecialChar.handleSpecialChar(documentBrief);

                String xmldata = "";
                CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                CommonMsgChk commonMsgChk = null;


                //xmldata = "<tbl_EvalConfig   tenderId=\"" + tenderId + "\" configType=\"" + configType + "\" evalCommittee=\"" + evalCommittee + "\" configBy=\"" + uId + "\" configDate=\"" + sdf.format(new Date()) + "\" isPostQualReq=\"" + ispost + "\" tecMemberId=\"" + tecMemberId + "\"  tscMemberId=\"" + tscMemberId + "\"  isTscReq =\"" + TSCyes + "\"/>";
              //  xmldata = "<root>" + xmldata + "</root>";


                System.out.println(xmldata);

                try {
                    if(dbOperation.equalsIgnoreCase("add")){
                      System.out.println(dbOperation);
                       xmldata = "<tbl_EvalConfig   tenderId=\"" + tenderId + "\" configType=\"" + configType + "\" evalCommittee=\"" + evalCommittee + "\" configBy=\"" + uId + "\" configDate=\"" + sdf.format(new Date()) + "\" isPostQualReq=\"" + ispost + "\" tecMemberId=\"" + tecMemberId + "\"  tscMemberId=\"" + tscMemberId + "\"  isTscReq =\"" + tscRequired + "\"/>";
                       xmldata = "<root>" + xmldata + "</root>";
                    System.out.println(xmldata);
                       commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_EvalConfig", xmldata, "").get(0);
                    
                    }else if(dbOperation.equalsIgnoreCase("update")){
                        System.out.println(dbOperation);

                        if(isDelMapping){
                        // Delete Mapping
                            commonMsgChk = commonXMLSPService.insertDataBySP("delete", "tbl_EvalMapForms", null, "tenderId = "+tenderId).get(0);
                        }
                        
                       //  xmldata = "tenderId=\"" + tenderId  ;
                         xmldata = "configType='"+configType+"',evalCommittee='"+evalCommittee+"',";
                         xmldata = xmldata+"configBy = "+uId+",configDate = '"+sdf.format(new Date())+"',";
                         xmldata = xmldata+"isPostQualReq ='"+ ispost+"',tecMemberId="+tecMemberId+",tscMemberId="+tscMemberId+",";
                         xmldata = xmldata+"isTscReq ='"+tscRequired+"'";
                        //System.out.println("xmldata:    "+xmldata);
                        commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_EvalConfig", xmldata, "tenderId = "+tenderId).get(0);
                    }

                    //System.out.println(commonMsgChk.getMsg());
                    queryString = "?tenderid=" + tenderId + "&ispost="+ispost + "&msgId=configdone";
                   response.sendRedirect("officer/EvalComm.jsp" + queryString);
                    //response.sendRedirectFilter("EvalComm.jsp" + queryString);
                    
                    if (commonMsgChk != null || handleSpecialChar != null) {
                        commonMsgChk = null;
                        handleSpecialChar = null;
                    }
                } catch (Exception ex) {
                    Logger.getLogger(ServletNegotiationDocs.class.getName()).log(Level.SEVERE, null, ex);
                }

            } catch (Exception ex) {
                System.out.println("Exception ::" + ex);
            } finally {
                out.close();
            }
        } else {
            response.sendRedirect(request.getContextPath() + "/Logout.jsp");
        }

    }

     @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
