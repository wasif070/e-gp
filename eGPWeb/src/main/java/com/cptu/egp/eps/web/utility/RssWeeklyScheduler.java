/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 *
 * @author nishit
 */

public class RssWeeklyScheduler extends QuartzJobBean {
     private static final Logger LOGGER = Logger.getLogger(RssWeeklyScheduler.class);
        private static final String LOGGERSTART = "Starts";
        private static final String LOGGEREND  = "Ends";
        public static int i = 0;
        /**
         * Spring scheduler for rss Weekly feed
         */
        public RssWeeklyScheduler() {
    }

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        if(i == 0){
            LOGGER.debug("executeInternal " + LOGGERSTART);
            List<SPCommonSearchData> recordCron = null;

            CommonSearchService cmnSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            recordCron = cmnSearchService.searchData("getCronJob", "RSS Weekly Scheduler", null, null, null, null, null, null, null, null);


            if (recordCron.isEmpty()) {
                CommonXMLSPService xmlservice = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");

                int cronStatus = 1;
                String cronMsg = "Successful Executed";
                String cronName = "RSS Weekly Scheduler";
                String cronStartTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
                try {
                    RssXmlWriter rssXmlWriter  = new RssXmlWriter();
                    rssXmlWriter.getFeed("weekly");
                } catch (Exception e) {
                    cronStatus=0;
                    cronMsg = e.toString();
                }

                String cronEndTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());

                String xml = "<root>"
                        + "<tbl_CronJobs "
                        + "cronJobName=\"" + cronName + "\" "
                        + "cronJobStartTime=\"" + cronStartTime + "\" "
                        + "cronJobEndTime=\"" + cronEndTime + "\" "
                        + "cronJobStatus=\"" + cronStatus + "\" "
                        + "cronJobMsg=\"" + cronMsg + "\"/>"
                        + "</root>";
                try {
                    xmlservice.insertDataBySP("insert", "tbl_CronJobs", xml, "");
                } catch (Exception ex) {
                    LOGGER.error("Rss Daily Feed : " + ex.toString());
                }
                LOGGER.debug("executeInternal " + LOGGEREND);
            }
        }
    }
}