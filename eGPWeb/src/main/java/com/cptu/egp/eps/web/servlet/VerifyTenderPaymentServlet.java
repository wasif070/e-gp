/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.service.serviceinterface.AddUpdateOpeningEvaluation;
//import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
//import com.cptu.egp.eps.dao.storedprocedure.UserApprovalBean;
//import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.model.table.TblCmsCompansiateAmt;
import com.cptu.egp.eps.model.table.TblPaymentActionRequest;
import com.cptu.egp.eps.service.serviceimpl.ConsolodateService;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
//import com.cptu.egp.eps.web.utility.DateUtils;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
//import com.cptu.egp.eps.service.serviceimpl.CommonSearchDataMoreImpl;
import com.cptu.egp.eps.service.serviceinterface.AccPaymentService;
import com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.PaymentService;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.math.BigDecimal;
import java.net.URLEncoder;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

import com.cptu.egp.eps.model.table.TblPaymentExtensionActionRequest;
import com.cptu.egp.eps.web.servicebean.CommonSearchServiceSrBean;
import java.math.RoundingMode;
import java.text.NumberFormat;
/**
 * this servlet handles all the request for the TodaysRecievedTenPayments.jsp page
 * @author Administrator
 */
public class VerifyTenderPaymentServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
    private AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
    private UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    private ConsolodateService c_ConsSrv = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
    private CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
    AddUpdateOpeningEvaluation addUpdate = (AddUpdateOpeningEvaluation) AppContext.getSpringBean("AddUpdateOpeningEvaluation");
    private static final Logger LOGGER = Logger.getLogger(VerifyTenderPaymentServlet.class);
    private static String logUserId = "0";
    private PaymentService paymentService = (PaymentService) AppContext.getSpringBean("PaymentService");

    // Dohatec Start
    private TenderCommonService objTSC = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    private CommonSearchDataMoreService objTSDetails = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
    // Dohatec End

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        if (request.getSession().getAttribute("userId") != null) {
            logUserId = request.getSession().getAttribute("userId").toString();
            cmsConfigDateService.setLogUserId(logUserId);
            accPaymentService.setLogUserId(logUserId);
            commonservice.setUserId(logUserId);
            registerService.setUserId(logUserId);
        }
        String userTypeId = "";
        if (request.getSession().getAttribute("userTypeId") != null) {
            userTypeId = request.getSession().getAttribute("userTypeId").toString();
        }
        LOGGER.debug("processRequest : " + logUserId + " Starts");
        try {
            String strAction = "";
            if (request.getParameter("action") != null) {
                strAction = request.getParameter("action");
            }


            if ("fetchData".equals(strAction)) {

                response.setContentType("text/xml;charset=UTF-8");
                //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String userId = request.getParameter("userId");
                String emailId = request.getParameter("emailId");
                String isVerified = request.getParameter("isVerified");
                String branchId = request.getParameter("branchId");
                String branchMakerId = request.getParameter("branchMakerId");
                String fromDt = request.getParameter("fromDt");
                String toDt = request.getParameter("toDt");
                String tenderId = request.getParameter("tenderId");
                String tendererNm = request.getParameter("tendererNm");
                String paymentFor = request.getParameter("paymentFor");
                String strReqId = "";

                String sord = "desc";
                String sidx = "tenderPaymentId";
                int totalPages = 0;
                //int totalCount = 0;


                if (!"".equalsIgnoreCase(request.getParameter("sidx"))) {
                    sord = request.getParameter("sord");
                    sidx = request.getParameter("sidx");
                }
                //if(request.getParameter("sidx")!=null && !"".equalsIgnoreCase(request.getParameter("sidx")))


                if (fromDt != null) {
                    if (!"".equalsIgnoreCase(fromDt)) {
                        String[] dtFrmArr = fromDt.split("/");
                        fromDt = dtFrmArr[2] + "-" + dtFrmArr[1] + "-" + dtFrmArr[0];
                    }
                }

                if (toDt != null) {
                    if (!"".equalsIgnoreCase(toDt)) {
                        String[] dtToArr = toDt.split("/");
                        toDt = dtToArr[2] + "-" + dtToArr[1] + "-" + dtToArr[0];
                    }
                }
                TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                CommonSearchDataMoreService objCommonSearchDataMoreImpl = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                objCommonSearchDataMoreImpl.setLogUserId(logUserId);
                List<SPCommonSearchDataMore> getList = objCommonSearchDataMoreImpl.getCommonSearchData("getTenderPaymentListingWithSorting", page, rows, userId, emailId, isVerified, branchId, branchMakerId, fromDt, toDt, tenderId, tendererNm, paymentFor, sidx, sord, null, null, null, null, null);

                if (getList.isEmpty()) {
                    //totalCount = Integer.parseInt(getList.get(0).getFieldName8());
                    totalPages = 0;
                } else {
                    //totalCount=0;
                    totalPages = Integer.parseInt(getList.get(0).getFieldName7());
                }

                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + getList.size() + "</records>");
                //int srNo = 1;
                int rowId = 0;
                if (!getList.isEmpty()) {
                    //for (SPCommonSearchData details : getList) {
                    for (SPCommonSearchDataMore details : getList) {
                        out.print("<row id='" + rowId + "'>");
                        out.print("<cell class=\"t-align-center\"><![CDATA[" + details.getFieldName9() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName14() + details.getFieldName17() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName3() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName11() + "]]></cell>");
                        if ("Bank Guarantee".equalsIgnoreCase(details.getFieldName12())) {
                            out.print("<cell><![CDATA[" + "New Perfomance Security" + "]]></cell>");
                        } else {
                            out.print("<cell><![CDATA[" + details.getFieldName12() + "]]></cell>");
                        }
                        out.print("<cell><![CDATA[" + details.getFieldName13() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName6() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName4() + "]]></cell>");
                        List<SPTenderCommonData> lstPaymentActionRequest = tenderCommonService.returndata("getRequestActionFromPaymentId", details.getFieldName1(), null);
                        List<SPTenderCommonData> lstPaymentActionRequest1 = tenderCommonService.returndata("getExtensionRequestFromPaymentId", details.getFieldName1(), userId);
                        if (!lstPaymentActionRequest.isEmpty()) {
                            strReqId = lstPaymentActionRequest.get(0).getFieldName1();
                        }
                        if ("no".equalsIgnoreCase(details.getFieldName10())) {
                            out.print("<cell><![CDATA[<a href='VerifyTenderPayDetails.jsp?reqId="+strReqId+"&payId=" + details.getFieldName1() + "&uId=" + details.getFieldName2() + "&tenderId=" + details.getFieldName14() + "&lotId=" + details.getFieldName15() + "&payTyp=" + details.getFieldName16() + "'>Verify</a> &nbsp;|&nbsp; <a href='TenderPayment.jsp?payId=" + details.getFieldName1() + "&uId=" + details.getFieldName2() + "&tenderId=" + details.getFieldName14() + "&lotId=" + details.getFieldName15() + "&payTyp=" + details.getFieldName16() + "&action=edit'>Edit</a> &nbsp;|&nbsp; <a href='TenderPaymentDetails.jsp?payId=" + details.getFieldName1() + "&uId=" + details.getFieldName2() + "&tenderId=" + details.getFieldName14() + "&lotId=" + details.getFieldName15() + "&payTyp=" + details.getFieldName16() + "'>View</a>]]></cell>");
                        } else {
                            String strLink = "<a href='TenderPaymentDetails.jsp?reqId="+strReqId+"&payId=" + details.getFieldName1() + "&uId=" + details.getFieldName2() + "&tenderId=" + details.getFieldName14() + "&lotId=" + details.getFieldName15() + "&payTyp=" + details.getFieldName16() + "'>View</a>";

                            if ("ts".equalsIgnoreCase(details.getFieldName16()) || "ps".equalsIgnoreCase(details.getFieldName16()) || "bg".equalsIgnoreCase(details.getFieldName16())) {
                                if (!lstPaymentActionRequest.isEmpty()) {
                                    if (!"0".equalsIgnoreCase(lstPaymentActionRequest.get(0).getFieldName1())) {
                                        if ("Forfeit".equalsIgnoreCase(lstPaymentActionRequest.get(0).getFieldName2())) {
                                            strLink = strLink
                                                    + "&nbsp;&nbsp;|&nbsp;&nbsp;<a href='PostPaymentProcess.jsp?reqId=" + lstPaymentActionRequest.get(0).getFieldName1() + "&payId=" + details.getFieldName1() + "&uId=" + details.getFieldName2() + "&tenderId=" + details.getFieldName14() + "&lotId=" + details.getFieldName15() + "&payTyp=" + details.getFieldName16() + "'>Compensate</a>";
                                        } else if ("Release".equalsIgnoreCase(lstPaymentActionRequest.get(0).getFieldName2())) {
                                            strLink = strLink
                                                    + "&nbsp;&nbsp;|&nbsp;&nbsp;<a href='PostPaymentProcess.jsp?reqId=" + lstPaymentActionRequest.get(0).getFieldName1() + "&payId=" + details.getFieldName1() + "&uId=" + details.getFieldName2() + "&tenderId=" + details.getFieldName14() + "&lotId=" + details.getFieldName15() + "&payTyp=" + details.getFieldName16() + "'>" + lstPaymentActionRequest.get(0).getFieldName2() + "</a>";
                                        }
                                    }
                                }
                                    if (!lstPaymentActionRequest1.isEmpty()) {
                                    strReqId = lstPaymentActionRequest1.get(0).getFieldName1();
                                     if (!"0".equalsIgnoreCase(lstPaymentActionRequest1.get(0).getFieldName1())) {
                                         if ("Extend".equalsIgnoreCase(lstPaymentActionRequest1.get(0).getFieldName2())) {
                                            strLink = strLink
                                            +"&nbsp;|&nbsp;<a href='TendererPaymentExtension.jsp?action=extendValidityps&payId="+details.getFieldName1()+"&rqId="+lstPaymentActionRequest1.get(0).getFieldName1()+"&uId="+details.getFieldName2()+"&tenderId="+details.getFieldName14()+"&lotId="+ details.getFieldName15()+"&payTyp="+details.getFieldName16() +"&view=CMS'>Extension Request</a>";
                                        }
                                     }
                                    }
                            }

                            out.print("<cell>"
                                    + "<![CDATA["
                                    + strLink
                                    + "]]>"
                                    + "</cell>");
                        }

                        out.print("</row>");
                         out.print("<userdata></userdata>");
                        //srNo++;
                        rowId++;
                    }
                } else {
                    String noDataMsg = "No record found";
                    out.print("<row id='" + rowId + "'>");
                    out.print("<cell class=\"t-align-center\"><![CDATA[1]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("</row>");
                    noDataMsg = null;
                }

                out.print("</rows>");
                // Nullify the List objects
                rows = null;
                page = null;
                userId = null;
                emailId = null;
                isVerified = null;
                branchId = null;
                branchMakerId = null;
                fromDt = null;
                toDt = null;
                tenderId = null;
                tendererNm = null;
                paymentFor = null;

            }
            /*getting data from database and displaying in TenPaymentListing.jsp and verifyTenderPayment.jsp jqgrid*/

            if ("fetchDataForReport".equals(strAction)) {

                response.setContentType("text/xml;charset=UTF-8");
                //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String userId = request.getParameter("userId");
               // String emailId = request.getParameter("emailId");
                String isVerified = request.getParameter("isVerified");
                String branchId = request.getParameter("branchId");
                String branchMakerId = request.getParameter("branchMakerId");
                String fromDt = request.getParameter("fromDt");
                String toDt = request.getParameter("toDt");
                String tenderId = request.getParameter("tenderId");
               // String tendererNm = request.getParameter("tendererNm");
                String paymentFor = request.getParameter("paymentFor");
                String strReqId = "";

                String sord = "";// for report no sorting parameter passed
                String sidx = ""; // for report no sorting parameter passed
                int totalPages = 0;
                //int totalCount = 0;


                //if (!"".equalsIgnoreCase(request.getParameter("sidx"))) {
                 //   sord = request.getParameter("sord");
                //    sidx = request.getParameter("sidx");
               // }
                //if(request.getParameter("sidx")!=null && !"".equalsIgnoreCase(request.getParameter("sidx")))


                if (fromDt != null) {
                    if (!"".equalsIgnoreCase(fromDt)) {
                        String[] dtFrmArr = fromDt.split("/");
                        fromDt = dtFrmArr[2] + "-" + dtFrmArr[1] + "-" + dtFrmArr[0];
                    }
                }

                if (toDt != null) {
                    if (!"".equalsIgnoreCase(toDt)) {
                        String[] dtToArr = toDt.split("/");
                        toDt = dtToArr[2] + "-" + dtToArr[1] + "-" + dtToArr[0];
                    }
                }
                TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                CommonSearchDataMoreService objCommonSearchDataMoreImpl = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                objCommonSearchDataMoreImpl.setLogUserId(logUserId);
                List<SPCommonSearchDataMore> getList = objCommonSearchDataMoreImpl.getCommonSearchData("getTenderPaymentListingForReport", page, rows, userId, "",isVerified, branchId, branchMakerId, fromDt, toDt, tenderId, "", paymentFor, sidx, sord, null, null, null, null, null);
                double bdtSum=0.00;
                double otherSum=0.00;
                if (getList.isEmpty()) {
                    //totalCount = Integer.parseInt(getList.get(0).getFieldName8());
                    totalPages = 0;
                } else {
                    //totalCount=0;
                    if(getList.get(0).getFieldName13().equals("0"))
                        totalPages = 0;
                    else
                        totalPages = Integer.parseInt(getList.get(0).getFieldName7());
                }

                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + getList.size() + "</records>");
                //int srNo = 1;
                int rowId = 0;
                if (totalPages > 0) {
                    //for (SPCommonSearchData details : getList) {
                    for (SPCommonSearchDataMore details : getList) {
                        out.print("<row id='" + rowId + "'>");
                        out.print("<cell class=\"t-align-center\"><![CDATA[" + details.getFieldName9() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName14() + details.getFieldName17() + "]]></cell>");
                        if(details.getFieldName10().toString().equalsIgnoreCase("yes"))
                             out.print("<cell><![CDATA[" + "Verified" + "]]></cell>");
                        else
                              out.print("<cell><![CDATA[" + "Pending" + "]]></cell>");

                        out.print("<cell><![CDATA[" + details.getFieldName11() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName4() + "]]></cell>");

                        if ("Bank Guarantee".equalsIgnoreCase(details.getFieldName12())) {
                            out.print("<cell><![CDATA[" + "New Perfomance Security" + "]]></cell>");
                        } else {
                            out.print("<cell><![CDATA[" + details.getFieldName12() + "]]></cell>");
                        }
                        out.print("<cell><![CDATA[" + details.getFieldName6() + "]]></cell>");
                        
                        if(details.getFieldName3().toString().equals("BTN")){
                            out.print("<cell><![CDATA[" + details.getFieldName13() + "]]></cell>");
                             bdtSum = bdtSum + Double.parseDouble(details.getFieldName13().toString());
                        }
                        else{
                            out.print("<cell><![CDATA[" + "0" + "]]></cell>");
                             bdtSum = bdtSum + 0.00;
                        }


                        if(!details.getFieldName3().toString().equals("BTN")){
                            out.print("<cell><![CDATA[" + details.getFieldName13() + "]]></cell>");
                            otherSum = otherSum + Double.parseDouble(details.getFieldName13().toString());
                        }
                        else{
                            out.print("<cell><![CDATA[" + "0" + "]]></cell>");
                            otherSum = otherSum + 0.00;
                        }


                       
                        out.print("</row>");
                        out.print("<userdata></userdata>");
                        //srNo++;
                        rowId++;
                    }
                    out.print("<row id='" + rowId + "'>");
                    out.print("<cell class=\"t-align-center\"><![CDATA[]]></cell>");
                    out.print("<cell><![CDATA[" + "" + "]]></cell>");
                    out.print("<cell><![CDATA[" + "" + "]]></cell>");
                    out.print("<cell><![CDATA[" + "" + "]]></cell>");
                    out.print("<cell><![CDATA[" + "" + "]]></cell>");
                    out.print("<cell><![CDATA[" + "" + "]]></cell>");
                    out.print("<cell><![CDATA[" + "Total" + "]]></cell>");
                    out.print("<cell><![CDATA[" + BigDecimal.valueOf(bdtSum).setScale(2,RoundingMode.CEILING) + "]]></cell>");
                    out.print("<cell><![CDATA[" + BigDecimal.valueOf(otherSum).setScale(2,RoundingMode.CEILING) + "]]></cell>");

                    out.print("</row>");
                    
                } else {
                    String noDataMsg = "No record found";
                    out.print("<row id='" + rowId + "'>");
                    out.print("<cell class=\"t-align-center\"><![CDATA[1]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("</row>");
                    noDataMsg = null;
                }

                out.print("</rows>");
                // Nullify the List objects
                rows = null;
                page = null;
                userId = null;
             //   emailId = null;
                isVerified = null;
                branchId = null;
                branchMakerId = null;
                fromDt = null;
                toDt = null;
                tenderId = null;
               // tendererNm = null;
                paymentFor = null;

            } /*getting data from database and displaying in TenPaymentDailyTrans.jsp  jqgrid*/ else if ("fetchDailyData".equals(strAction)) {

                response.setContentType("text/xml;charset=UTF-8");
                //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String userId = request.getParameter("userId");
                String emailId = request.getParameter("emailId");
                String isVerified = request.getParameter("isVerified");
                String branchId = request.getParameter("branchId");
                String branchMakerId = request.getParameter("branchMakerId");
                String fromDt = request.getParameter("fromDt");
                String toDt = request.getParameter("toDt");
                String tenderId = request.getParameter("tenderId");
                String tendererNm = request.getParameter("tendererNm");
                String paymentFor = request.getParameter("paymentFor");


                String sord = "desc";
                String sidx = "tenderPaymentId";
                int totalPages = 0;
                //int totalCount = 0;


                if (!"".equalsIgnoreCase(request.getParameter("sidx"))) {
                    sord = request.getParameter("sord");
                    sidx = request.getParameter("sidx");
                }
                //if(request.getParameter("sidx")!=null && !"".equalsIgnoreCase(request.getParameter("sidx")))


                if (fromDt != null) {
                    if (!"0".equalsIgnoreCase(fromDt) && !"".equalsIgnoreCase(fromDt)) {
                        String[] dtFrmArr = fromDt.split("/");
                        fromDt = dtFrmArr[2] + "-" + dtFrmArr[1] + "-" + dtFrmArr[0];
                    }
                }

                if (toDt != null) {
                    if (!"0".equalsIgnoreCase(toDt) && !"".equalsIgnoreCase(toDt)) {
                        String[] dtToArr = toDt.split("/");
                        toDt = dtToArr[2] + "-" + dtToArr[1] + "-" + dtToArr[0];
                    }
                }

                CommonSearchDataMoreService objCommonSearchDataMoreImpl = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                objCommonSearchDataMoreImpl.setLogUserId(logUserId);
                List<SPCommonSearchDataMore> getList = objCommonSearchDataMoreImpl.getCommonSearchData("getTodaysTenderPaymentListing", page, rows, userId, emailId, isVerified, branchId, branchMakerId, fromDt, toDt, tenderId, tendererNm, paymentFor, sidx, sord, null, null, null, null, null);

                if (getList.isEmpty()) {
                    //totalCount = Integer.parseInt(getList.get(0).getFieldName8());
                    totalPages = 0;
                } else {
                    //totalCount=0;
                    totalPages = Integer.parseInt(getList.get(0).getFieldName7());
                }

                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + getList.size() + "</records>");
                //int srNo = 1;
                int rowId = 0;
                if (!getList.isEmpty()) {
                    //for (SPCommonSearchData details : getList) {
                    for (SPCommonSearchDataMore details : getList) {
                        out.print("<row id='" + rowId + "'>");
                        out.print("<cell class=\"t-align-center\"><![CDATA[" + details.getFieldName9() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName14() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName3() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName11() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName12() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName13() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName6() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName4() + "]]></cell>");

                        if ("no".equalsIgnoreCase(details.getFieldName10())) {
                            out.print("<cell><![CDATA[<a href='TenderPaymentDetails.jsp?payId=" + details.getFieldName1() + "&uId=" + details.getFieldName2() + "&tenderId=" + details.getFieldName14() + "&lotId=" + details.getFieldName15() + "&payTyp=" + details.getFieldName16() + "'>View</a>]]></cell>");
                        } else {
                            out.print("<cell><![CDATA[<a href='TenderPaymentDetails.jsp?payId=" + details.getFieldName1() + "&uId=" + details.getFieldName2() + "&tenderId=" + details.getFieldName14() + "&lotId=" + details.getFieldName15() + "&payTyp=" + details.getFieldName16() + "'>View</a>]]></cell>");
                        }

                        out.print("</row>");
                        //srNo++;
                        rowId++;
                    }
                } else {
                    String noDataMsg = "No record found";
                    out.print("<row id='" + rowId + "'>");
                    out.print("<cell class=\"t-align-center\"><![CDATA[1]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("</row>");
                    noDataMsg = null;
                }

                out.print("</rows>");
                // Nullify the List objects
                rows = null;
                page = null;
                userId = null;
                emailId = null;
                isVerified = null;
                branchId = null;
                branchMakerId = null;
                fromDt = null;
                toDt = null;
                tenderId = null;
                tendererNm = null;
                paymentFor = null;

            } /*getting data from database and displaying in TodaysRecievedTenPayments.jsp jqgrid*/ else if ("fetchReceivedDailyData".equals(strAction)) {

                response.setContentType("text/xml;charset=UTF-8");
                //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String userId = request.getParameter("userId");


                String sord = "desc";
                String sidx = "tenderPaymentId";
                int totalPages = 0;
                //int totalCount = 0;


                if (!"".equalsIgnoreCase(request.getParameter("sidx"))) {
                    sord = request.getParameter("sord");
                    sidx = request.getParameter("sidx");
                }

                CommonSearchDataMoreService objCommonSearchDataMoreImpl = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");

                List<SPCommonSearchDataMore> getList = objCommonSearchDataMoreImpl.getCommonSearchData("getTodaysReceivedTenderPaymentListing", page, rows, userId, sidx, sord, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                objCommonSearchDataMoreImpl.setLogUserId(logUserId);
                if (getList.isEmpty()) {
                    //totalCount = Integer.parseInt(getList.get(0).getFieldName8());
                    totalPages = 0;
                } else {
                    //totalCount=0;
                    totalPages = Integer.parseInt(getList.get(0).getFieldName7());
                }

                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + getList.size() + "</records>");
                //int srNo = 1;
                int rowId = 0;
                if (!getList.isEmpty()) {
                    //for (SPCommonSearchData details : getList) {
                    for (SPCommonSearchDataMore details : getList) {
                        out.print("<row id='" + rowId + "'>");
                        out.print("<cell class=\"t-align-center\"><![CDATA[" + details.getFieldName9() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName14() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName3() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName11() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName12() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName13() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName6() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName4() + "]]></cell>");

                        if ("no".equalsIgnoreCase(details.getFieldName10())) {
                            //out.print("<cell><![CDATA[<a href='VerifyTenderPayDetails.jsp?payId="+details.getFieldName1()+"&uId=" + details.getFieldName2()+"&tenderId="+details.getFieldName14()+"&lotId="+details.getFieldName15()+"&payTyp="+details.getFieldName16()+"'>Verify</a> &nbsp;|&nbsp; <a href='TenderPaymentDetails.jsp?payId="+details.getFieldName1()+"&uId=" + details.getFieldName2()+"&tenderId="+details.getFieldName14()+"&lotId="+details.getFieldName15()+"&payTyp="+details.getFieldName16()+"'>View</a>]]></cell>");
                            out.print("<cell><![CDATA[<a href='TenderPaymentDetails.jsp?payId=" + details.getFieldName1() + "&uId=" + details.getFieldName2() + "&tenderId=" + details.getFieldName14() + "&lotId=" + details.getFieldName15() + "&payTyp=" + details.getFieldName16() + "'>View</a>]]></cell>");
                        } else {
                            out.print("<cell><![CDATA[<a href='TenderPaymentDetails.jsp?payId=" + details.getFieldName1() + "&uId=" + details.getFieldName2() + "&tenderId=" + details.getFieldName14() + "&lotId=" + details.getFieldName15() + "&payTyp=" + details.getFieldName16() + "'>View</a>]]></cell>");
                        }

                        out.print("</row>");
                        //srNo++;
                        rowId++;
                    }
                } else {
                    String noDataMsg = "No record found";
                    out.print("<row id='" + rowId + "'>");
                    out.print("<cell class=\"t-align-center\"><![CDATA[1]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("</row>");
                    noDataMsg = null;
                }

                out.print("</rows>");
                // Nullify the List objects
                rows = null;
                page = null;
                userId = null;
            } /*getting data from database and displaying in TodaysVerifiedTenPayments.jsp jqgrid*/ else if ("fetchDailyVerifiedTenPayData".equals(strAction)) {

                response.setContentType("text/xml;charset=UTF-8");
                //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String userId = request.getParameter("userId");


                String sord = "desc";
                String sidx = "tenderPaymentId";
                int totalPages = 0;
                //int totalCount = 0;


                if (!"".equalsIgnoreCase(request.getParameter("sidx"))) {
                    sord = request.getParameter("sord");
                    sidx = request.getParameter("sidx");
                }

                CommonSearchDataMoreService objCommonSearchDataMoreImpl = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                objCommonSearchDataMoreImpl.setLogUserId(logUserId);
                List<SPCommonSearchDataMore> getList = objCommonSearchDataMoreImpl.getCommonSearchData("getTodaysVerifiedTenPaymentListing", page, rows, userId, sidx, sord, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

                if (getList.isEmpty()) {
                    //totalCount = Integer.parseInt(getList.get(0).getFieldName8());
                    totalPages = 0;
                } else {
                    //totalCount=0;
                    totalPages = Integer.parseInt(getList.get(0).getFieldName7());
                }

                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + getList.size() + "</records>");
                //int srNo = 1;
                int rowId = 0;
                if (!getList.isEmpty()) {
                    //for (SPCommonSearchData details : getList) {
                    for (SPCommonSearchDataMore details : getList) {
                        out.print("<row id='" + rowId + "'>");
                        out.print("<cell class=\"t-align-center\"><![CDATA[" + details.getFieldName9() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName14() + details.getFieldName17() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName3() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName11() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName12() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName13() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName6() + "]]></cell>");
                        out.print("<cell><![CDATA[" + details.getFieldName4() + "]]></cell>");

                        if ("no".equalsIgnoreCase(details.getFieldName10())) {
                            out.print("<cell><![CDATA[<a href='TenderPaymentDetails.jsp?payId=" + details.getFieldName1() + "&uId=" + details.getFieldName2() + "&tenderId=" + details.getFieldName14() + "&lotId=" + details.getFieldName15() + "&payTyp=" + details.getFieldName16() + "'>View</a>]]></cell>");
                        } else {
                            out.print("<cell><![CDATA[<a href='TenderPaymentDetails.jsp?payId=" + details.getFieldName1() + "&uId=" + details.getFieldName2() + "&tenderId=" + details.getFieldName14() + "&lotId=" + details.getFieldName15() + "&payTyp=" + details.getFieldName16() + "'>View</a>]]></cell>");
                        }

                        out.print("</row>");
                        //srNo++;
                        rowId++;
                    }
                } else {
                    /*
                    String noDataMsg="No record found";
                    out.print("<row id='" + rowId + "'>");
                    out.print("<cell class=\"t-align-center\"><![CDATA[1]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("</row>");
                    noDataMsg=null;
                     */
                }

                out.print("</rows>");
                // Nullify the List objects
                rows = null;
                page = null;
                userId = null;

            } /*getting data from database and displaying in jqgrid*/ else if (strAction != null) {
                if ("getBranchMembers".equalsIgnoreCase(strAction)) {
                    String branchId = request.getParameter("branchId");
                    String str = "";
                    try {
                        str = getBranchMembers(branchId);
                    } catch (Exception e) {
                        LOGGER.error("Error in getBranchMembers : " + e.toString());
                    }
                    out.write(str);

                } else if ("verfiyPayment".equalsIgnoreCase(strAction)) {

                    if (request.getParameter("btnVerify") != null) {

                        HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
                        String strPartTransId = "0", userId = "";

                        HttpSession hs = request.getSession();
                        if (hs.getAttribute("userId") == null) {
                            response.sendRedirect("SessionTimedOut.jsp");
                            //response.sendRedirectFilter("SessionTimedOut.jsp");
                        } else {
                            userId = hs.getAttribute("userId").toString();
                        }

                        if (hs.getAttribute("govUserId") != null) {
                            strPartTransId = hs.getAttribute("govUserId").toString();
                        }

                        String paymentId = request.getParameter("payId");
                        String tenderRefNo = request.getParameter("tenderRefNo");
                        String tenderId = request.getParameter("tenderId");
                        String lotId = request.getParameter("lotId");
                        String payTyp = request.getParameter("payTyp");
                        String pageNm = "";
                        String querString = "";
                        String path = "";
                        String remarks = "";
                        String fnlAmt = "";
                        String status = "";
                        String strBidderUserId = "0";
                        String paymentFor = "";
                        String auditAction = "";

                        fnlAmt = request.getParameter("hdnAmount");
                        remarks = request.getParameter("txtComments");
                        status = request.getParameter("hdnPayStatus");
                        strBidderUserId = request.getParameter("hdnBidderUserId");

                        /* START: CODE TO HANDLE SPECIAL CHARACTERS  */
                        remarks = URLEncoder.encode(remarks,"UTF-8");
                        /* END CODE TO HANDLE SPECIAL CHARACTERS  */



                        //  Dohatec Start

                        boolean isIctTender = false;
                        String[] payIdArray = new String[4];
                        int totalRegPayId = 0;

                        if("ps".equalsIgnoreCase(payTyp) || "bg".equalsIgnoreCase(payTyp))
                        {
                            // Tender is ICT or not
                            List<SPTenderCommonData> listICT = objTSC.returndata("chkDomesticPreference", tenderId, null);
                            if(!listICT.isEmpty() && listICT.get(0).getFieldName1().equalsIgnoreCase("true")){
                                isIctTender = true;
                            }
                        }

                        if(isIctTender) // ICT and PS or BG
                        {
                            //  Get Performance Security or New Performance Security Payment IDs which have to verify
                            strBidderUserId = request.getParameter("uId");
                            System.out.print("TenderID : " + tenderId.toString() + " \nuserID : " + strBidderUserId);

                            List<SPCommonSearchDataMore> lstPerSecPayIDDetail = null;
                            if("ps".equalsIgnoreCase(payTyp))
                                lstPerSecPayIDDetail = objTSDetails.geteGPData("getPerSecPaymentIDDetail", tenderId.toString(), strBidderUserId);
                            else if("bg".equalsIgnoreCase(payTyp))
                                lstPerSecPayIDDetail = objTSDetails.geteGPData("getNewPerSecPaymentIDDetail", tenderId.toString(), lotId, strBidderUserId);

                            System.out.print("Old Paymend id list size " + lstPerSecPayIDDetail.size());
                            if(!lstPerSecPayIDDetail.isEmpty())
                            {
                                totalRegPayId = lstPerSecPayIDDetail.size();
                                for(int i = 0; i< lstPerSecPayIDDetail.size(); i++)
                                {
                                    payIdArray[i] = lstPerSecPayIDDetail.get(i).getFieldName1();
                                    System.out.print("Old Payment Id : " + payIdArray[i]);
                                }
                            }

                        }


                        try {

                            if(("ps".equalsIgnoreCase(payTyp) || "bg".equalsIgnoreCase(payTyp)) && isIctTender)    // New Added for PS or BG ICT
                            {
                                String paymentIdMail = "";
                                fnlAmt="";
                                String psAmntCurrency = "";
                                String txtCurrencySymbol = "";
                                String currencyType = "";
                                String perSecAmount = "";

                                List<SPCommonSearchDataMore> lstPerSecPayDetail = null;
                                if("ps".equalsIgnoreCase(payTyp))
                                    lstPerSecPayDetail = objTSDetails.geteGPData("getPerformanceSecurityPaymentDetail", tenderId.toString());
                                else if("bg".equalsIgnoreCase(payTyp))
                                    lstPerSecPayDetail = objTSDetails.geteGPData("getNewPerSecDetail", tenderId.toString(), lotId, strBidderUserId);

                                if(!lstPerSecPayDetail.isEmpty())
                                {
                                    System.out.print("\n List Size : " + lstPerSecPayDetail.size());
                                    for(int i = 0; i<lstPerSecPayDetail.size(); i++)
                                    {
                                        if("ps".equalsIgnoreCase(payTyp))
                                        {
                                            txtCurrencySymbol = lstPerSecPayDetail.get(i).getFieldName13();
                                            currencyType = lstPerSecPayDetail.get(i).getFieldName12();
                                            perSecAmount = lstPerSecPayDetail.get(i).getFieldName4();
                                        }
                                        else if("bg".equalsIgnoreCase(payTyp))
                                        {
                                            perSecAmount = lstPerSecPayDetail.get(i).getFieldName1();
                                            currencyType = lstPerSecPayDetail.get(i).getFieldName2();
                                            txtCurrencySymbol = lstPerSecPayDetail.get(i).getFieldName3();
                                        }
                                        psAmntCurrency = psAmntCurrency + currencyType + "@" + perSecAmount + "-";
                                    }
                                }
                                psAmntCurrency = psAmntCurrency.substring(0, (psAmntCurrency.length()-1)).replaceAll("@", " ").replace("-", ", ");
                                System.out.print("PS Amount Currency : " + psAmntCurrency);

                                for(int j=0;j<totalRegPayId; j++)
                                {
                                    //String dtXml = "";
                                    //dtXml = "<tbl_TenderPaymentVerification paymentId=\"" + payIdArray[j] + "\" verifiedBy=\"" + userId + "\" remarks=\"" + remarks + "\" verificationDt=\"" + format.format(new Date()) + "\" verifyPartTransId=\"" + strPartTransId + "\" />";
                                    //dtXml = "<root>" + dtXml + "</root>";

                                    //CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                    CommonMsgChk commonMsgChk;

                                    if("ps".equalsIgnoreCase(payTyp))
                                        paymentFor = "Performance Security";
                                    else if("bg".equalsIgnoreCase(payTyp))
                                        paymentFor = "New Performance Security";

                                    auditAction = "Verify " + paymentFor + " Payment";
                                    commonMsgChk = addUpdate.addUpdOpeningEvaluation("InsertPaymentVerificationAndUpdatePayment",payIdArray[j],userId,remarks,format.format(new Date()),strPartTransId).get(0); 
                                    //commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_TenderPaymentVerification", dtXml, "").get(0);

                                    if (commonMsgChk.getFlag().equals(true)) 
                                    {
                                        if ("paid".equalsIgnoreCase(status) || "extended".equalsIgnoreCase(status)) 
                                        {
                                            paymentIdMail = payIdArray[j] + ","  + paymentIdMail;

                                            /* START : CODE TO SEND MAIL */
                                            boolean mailSent = false;
                                            if(j == (totalRegPayId -1))
                                            {
                                                paymentIdMail = paymentIdMail.substring(0, (paymentIdMail.length()-1));
                                                fnlAmt = psAmntCurrency;

                                                mailSent = SendMail(paymentIdMail, status, paymentFor, strBidderUserId, tenderId, tenderRefNo, fnlAmt, userId);
                                            }
                                            /* END : CODE TO SEND MAIL */
                                        }

                                        pageNm = "TenderPaymentDetails.jsp";
                                        querString = "payId=" + payIdArray[0] + "&uId=" + strBidderUserId + "&tenderId=" + tenderId + "&lotId=" + lotId + "&payTyp=" + payTyp + "&msgId=verified";
                                    } 
                                    else 
                                    {
                                        pageNm = "VerifyTenderPayDetails.jsp";
                                        querString = "payId=" + payIdArray[0] + "&uId=" + strBidderUserId + "&tenderId=" + tenderId + "&lotId=" + lotId + "&payTyp=" + payTyp + "&msgId=error";
                                        break;
                                    }
                                }
                            }
                            else    // Old Version (inside try)
                            {
                                //String dtXml = "";
                                //dtXml = "<tbl_TenderPaymentVerification paymentId=\"" + paymentId + "\" verifiedBy=\"" + userId + "\" remarks=\"" + remarks + "\" verificationDt=\"" + format.format(new Date()) + "\" verifyPartTransId=\"" + strPartTransId + "\" />";
                                //dtXml = "<root>" + dtXml + "</root>";

                                //CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                CommonMsgChk commonMsgChk;
                                commonMsgChk = addUpdate.addUpdOpeningEvaluation("InsertPaymentVerificationAndUpdatePayment",paymentId,userId,remarks,format.format(new Date()),strPartTransId).get(0); 

                                if ("df".equalsIgnoreCase(payTyp)) {
                                    paymentFor = "Document Fees";
                                } else if ("ts".equalsIgnoreCase(payTyp)) {
                                    paymentFor = "Bid Security";
                                } else if ("ps".equalsIgnoreCase(payTyp)) {
                                    paymentFor = "Performance Security";
                                }else if("bg".equalsIgnoreCase(payTyp)){
                                    paymentFor = "New Performance Security";
                                }
                                auditAction = "Verify " + paymentFor + " Payment";
                                //commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_TenderPaymentVerification", dtXml, "").get(0);

                                if (commonMsgChk.getFlag().equals(true)) 
                                {
                                    if ("paid".equalsIgnoreCase(status) || "extended".equalsIgnoreCase(status)) 
                                    {
                                        /* START : CODE TO SEND MAIL */
                                        boolean mailSent = false;
                                        mailSent = SendMail(paymentId, status, paymentFor, strBidderUserId, tenderId, tenderRefNo, fnlAmt, userId);
                                        /* END : CODE TO SEND MAIL */
                                    }

                                    pageNm = "TenderPaymentDetails.jsp";
                                    querString = "payId=" + paymentId + "&uId=" + strBidderUserId + "&tenderId=" + tenderId + "&lotId=" + lotId + "&payTyp=" + payTyp + "&msgId=verified"; 
                                } 
                                else 
                                {
                                    pageNm = "VerifyTenderPayDetails.jsp";
                                    querString = "payId=" + paymentId + "&uId=" + strBidderUserId + "&tenderId=" + tenderId + "&lotId=" + lotId + "&payTyp=" + payTyp + "&msgId=error";
                                }
                            }

                            path = request.getContextPath() + "/partner/" + pageNm + "?" + querString;

                            response.sendRedirect(path);

                        } catch (Exception e) {
                            LOGGER.error("Error in Payment Verification Process: " + e.toString());
                            auditAction = "Error in " + auditAction + " :" + e.getMessage();
                        } finally {
                            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), Integer.parseInt(tenderId), "tenderId", EgpModule.Bank_Module.getName(), auditAction, remarks);
                            // Nullify the List objects
                            userId = null;
                            auditAction = null;
                            strPartTransId = null;
                            paymentId = null;
                            tenderRefNo = null;
                            tenderId = null;
                            lotId = null;
                            payTyp = null;
                            pageNm = null;
                            querString = null;
                            path = null;
                            remarks = null;
                            fnlAmt = null;
                            status = null;
                            strBidderUserId = null;
                            paymentFor = null;
                        }

                        //  Dohatec End


                    }

                } else if ("requestReleaseForfeit".equalsIgnoreCase(strAction)) {

                    if (request.getParameter("btnSubmit") != null) {
                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        int conId = c_ConsSrv.getContractId(Integer.parseInt(request.getParameter("tenderId")));

                        HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        String strPartTransId = "0", userId = "";


                        HttpSession hs = request.getSession();
                        if (hs.getAttribute("userId") == null) {
                            response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp");
                        } else {
                            userId = hs.getAttribute("userId").toString();
                        }

                        if (hs.getAttribute("govUserId") != null) {
                            strPartTransId = hs.getAttribute("govUserId").toString();
                        }

                        // Dohatec Start
                        boolean isIctTender = false;
                        String[] payIdArray = new String[4];
                        int totalPayId = 0;
                        String[] compAmtIct = new String[4];
                        String[] balanceAmtIct =new String[4];

                        if("ps".equalsIgnoreCase(request.getParameter("payTyp")) || "bg".equalsIgnoreCase(request.getParameter("payTyp")))
                        {
                            // Tender is ICT or not
                            List<SPTenderCommonData> listICT = objTSC.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                            if(!listICT.isEmpty() && listICT.get(0).getFieldName1().equalsIgnoreCase("true")){
                                isIctTender = true;
                            }
                        }
                        //  Dohatec End

                        boolean sendMail = false;
                        String paymentId = request.getParameter("payId");
                        System.out.print("\nPayment Id : " + paymentId);
                        String tenderRefNo = request.getParameter("tenderRefNo");
                        String tenderId = request.getParameter("tenderId");
                        String lotId = request.getParameter("lotId");
                        String pageNm = "";
                        String querString = "";
                        String path = "";
                        String CompansiateAmt = "0";
                        String balanceAmt = "";
                        String remarks = "";
                        String fnlAmt = "";
                        String status = "";
                        String strBidderUserId = "0";
                        String view = "";
                        String strRequestType = request.getParameter("requesttype");

                        // Dohatec Start
                        if(isIctTender && "bg".equalsIgnoreCase(request.getParameter("payTyp")))
                        {
                            compAmtIct = request.getParameterValues("compAmt");
                            balanceAmtIct = request.getParameterValues("balanceAmt");

                            List<SPCommonSearchDataMore> lst = objTSDetails.geteGPData("getNewPerSecPaymentIDDetail", tenderId.toString(), lotId, request.getParameter("hdnBidderUserId"));
                            System.out.print("Old Paymend id list size " + lst.size());
                            if(!lst.isEmpty())
                            {
                                totalPayId = lst.size();
                                for(int i = 0; i< lst.size(); i++)
                                {
                                    payIdArray[i] = lst.get(i).getFieldName1();
                                    System.out.print("Old Payment Id : " + payIdArray[i]);
                                }
                            }
                        }
                        else    // Old Version
                        {
                            CompansiateAmt = request.getParameter("compAmt");
                            balanceAmt = request.getParameter("balanceAmt");
                            System.out.print("Compansare : " + CompansiateAmt +"\nBalance : " + balanceAmt);
                        }
                        // Dohatec End

                        fnlAmt = request.getParameter("hdnAmount");
                        remarks = request.getParameter("txtComments");
                        status = request.getParameter("hdnPayStatus");
                        strBidderUserId = request.getParameter("hdnBidderUserId");
                        view = request.getParameter("view");
                        /* START: CODE TO HANDLE SPECIAL CHARACTERS  */
                        remarks = handleSpecialChar.handleSpecialChar(remarks);
                        /* END CODE TO HANDLE SPECIAL CHARACTERS  */

                        String auditObjectType=null;
                        int auditObjectId=0;
                        String auditAction=null;
                        String payTyp = request.getParameter("payTyp");

                        String strXML = "";
                        String strRequestTo = "";
                        String strRequestAction = "";
                        String strMsg = "";

                        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                        CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                        CommonMsgChk commonMsgChk;

                        List<SPTenderCommonData> lstPayerInfo = tenderCommonService.returndata("getPaymentPaidByUserId", paymentId, null);
                        System.out.print("\nSize >>>>>"+lstPayerInfo.size()+"\nPayment ID>>>>" + paymentId);

                        if (!lstPayerInfo.isEmpty()) {
                            if (strRequestType.equalsIgnoreCase("requestrelease")) {
                                // Sent Forfeit request to the Maker
                                //strRequestTo = lstPayerInfo.get(0).getFieldName1();
                                strRequestTo = lstPayerInfo.get(0).getFieldName2();
                            } else if (strRequestType.equalsIgnoreCase("requestforfeit")) {
                                // Sent Forfeit request to the Checker
                                strRequestTo = lstPayerInfo.get(0).getFieldName2();
                            }
                        }

                        if (strRequestType.equalsIgnoreCase("requestrelease")) {
                            strRequestAction = "release";
                            strMsg = "releaserequested";
                            sendMail = true;
                            if ("ts".equalsIgnoreCase(payTyp))
                            {
                                auditAction = "Release Request For Bid Security";
                                auditObjectId=Integer.parseInt(tenderId);
                                auditObjectType="tenderId";
                            }
                            else if ("ps".equalsIgnoreCase(payTyp))
                            {
                                auditAction = "Release Request For Performance Security";
                                auditObjectId=Integer.parseInt(tenderId);
                                auditObjectType="tenderId";
                            }
                            else if ("bg".equalsIgnoreCase(payTyp))
                            {
                                auditAction = "Release Request For New Performance Security";
                                auditObjectId=conId;
                                auditObjectType="contractId";
                            }
                            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), auditObjectId, auditObjectType, EgpModule.Payment.getName(), auditAction, "");

                        } else if (strRequestType.equalsIgnoreCase("requestforfeit")) {
                            if ("ps".equalsIgnoreCase(payTyp) || "bg".equalsIgnoreCase(payTyp)){

                                // Dohatec Start
                                if(isIctTender && "bg".equalsIgnoreCase(payTyp))
                                {
                                    for(int i = 0; i<totalPayId; i++)
                                    {
                                        TblCmsCompansiateAmt tblCmsCompansiateAmt = new TblCmsCompansiateAmt();
                                        tblCmsCompansiateAmt.setPaymentId(Integer.parseInt(payIdArray[i]));
                                        tblCmsCompansiateAmt.setCompAmt(new BigDecimal(compAmtIct[i]));
                                        tblCmsCompansiateAmt.setBalanceAmt(new BigDecimal(balanceAmtIct[i]));
                                        accPaymentService.addCompansiateAmt(tblCmsCompansiateAmt);
                                    }
                                }
                                else    // Old Version
                                {
                                    System.out.print("ComAmt : "+CompansiateAmt+"\nBalance : "+balanceAmt);
                                    TblCmsCompansiateAmt tblCmsCompansiateAmt = new TblCmsCompansiateAmt();
                                    tblCmsCompansiateAmt.setPaymentId(Integer.parseInt(paymentId));
                                    tblCmsCompansiateAmt.setCompAmt(new BigDecimal(CompansiateAmt));
                                    tblCmsCompansiateAmt.setBalanceAmt(new BigDecimal(balanceAmt));
                                    accPaymentService.addCompansiateAmt(tblCmsCompansiateAmt);
                                }
                                // Dohatec End
                            }
                            strRequestAction = "forfeit";
                            strMsg = "forfeitrequested";
                            sendMail = true;
                            if ("ts".equalsIgnoreCase(payTyp))
                            {
                                auditAction = "Compensate Request For Bid Security";
                                auditObjectId=Integer.parseInt(tenderId);
                                auditObjectType="tenderId";
                            }
                            else if ("ps".equalsIgnoreCase(payTyp))
                            {
                                auditAction = "Compensate Request For Performance Security";
                                auditObjectId=Integer.parseInt(tenderId);
                                auditObjectType="tenderId";
                            }
                            else if ("bg".equalsIgnoreCase(payTyp))
                            {
                                auditAction = "Compensate Request For New Performance Security";
                                auditObjectId=conId;
                                auditObjectType="contractId";
                            }
                            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), auditObjectId, auditObjectType, EgpModule.Payment.getName(), auditAction, "");
                        }

                        strXML = "<tbl_PaymentActionRequest paymentId=\"" + paymentId + "\" requestBy=\"" + userId + "\" requestTo=\"" + strRequestTo + "\" requestAction=\"" + strRequestAction + "\" requestComments=\"" + remarks + "\" actionStatus=\"pending\" actionCompDt=\"\" actionComments=\"\" requestDate=\"" + format.format(new Date()) + "\" />";
                        strXML = "<root>" + strXML + "</root>";

                        try {
                            // Added by shreyansh shah To insert PE payment release by database instead of xml
                            boolean successType;
                            
                            if(isIctTender && "bg".equalsIgnoreCase(payTyp)) //  Added by Dohatec
                            {
                                int flag = 0;
                                for(int i = 0; i < totalPayId; i++){
                                    TblPaymentActionRequest pymtRequest = new TblPaymentActionRequest();
                                    pymtRequest.setPaymentId(Integer.parseInt(payIdArray[i]));
                                    pymtRequest.setRequestBy(Integer.parseInt(userId));
                                    pymtRequest.setRequestTo(Integer.parseInt(strRequestTo));
                                    pymtRequest.setRequestAction(strRequestAction);
                                    pymtRequest.setRequestComments(remarks);
                                    pymtRequest.setActionStatus("Pending");
                                    pymtRequest.setActionCompDt(new Date());
                                    pymtRequest.setActionComments("");
                                    pymtRequest.setRequestDate(new Date());
                                  //  commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_PaymentActionRequest", strXML, "").get(0);
                                    successType = paymentService.addPEPaymentReleaseRequest(pymtRequest);
                                    if(successType)
                                        flag++;
                                }
                                if(flag == totalPayId)
                                    successType = true;
                                else
                                    successType = false;
                            }
                            else    //  NCT
                            {
                                TblPaymentActionRequest pymtRequest = new TblPaymentActionRequest();
                                pymtRequest.setPaymentId(Integer.parseInt(paymentId));
                                pymtRequest.setRequestBy(Integer.parseInt(userId));
                                pymtRequest.setRequestTo(Integer.parseInt(strRequestTo));
                                pymtRequest.setRequestAction(strRequestAction);
                                pymtRequest.setRequestComments(remarks);
                                pymtRequest.setActionStatus("Pending");
                                pymtRequest.setActionCompDt(new Date());
                                pymtRequest.setActionComments("");
                                pymtRequest.setRequestDate(new Date());
                              //  commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_PaymentActionRequest", strXML, "").get(0);
                                successType = paymentService.addPEPaymentReleaseRequest(pymtRequest);
                            }

                            //if (commonMsgChk.getFlag().equals(true)) {
                            if (successType == true) {
                                /* START : CODE TO SEND MAIL */
                                boolean mailSent = false;
                                String paymentFor = "";

                                if ("df".equalsIgnoreCase(payTyp)) {
                                    paymentFor = "Document Fees";
                                } else if ("ts".equalsIgnoreCase(payTyp)) {
                                    paymentFor = "Bid Security";
                                } else if ("ps".equalsIgnoreCase(payTyp)) {
                                    paymentFor = "Performance Security";
                                }else if("ps".equalsIgnoreCase(payTyp)) {
                                     if (strRequestType.equalsIgnoreCase("requestrelease")) {

                                     }else{

                                     }
                                }

                                if("ps".equalsIgnoreCase(payTyp) || "bg".equalsIgnoreCase(payTyp)){
                                    if (strRequestType.equalsIgnoreCase("requestrelease")) {
                                        sendMailForReleaseandRequest(tenderId, lotId, strRequestTo, logUserId, Integer.parseInt(userTypeId),payTyp);
                                    } else {
                                        sendMailForForefietRequest(tenderId, lotId, strRequestTo, logUserId, Integer.parseInt(userTypeId),payTyp);
                                    }
                                }else{
                                    SendRequestReleaseForfeitMail(paymentId, paymentFor, strBidderUserId, tenderId, tenderRefNo, lotId, userId, strRequestTo, strRequestAction, remarks,fnlAmt);
                                }
                                /* END : CODE TO SEND MAIL */

                                pageNm = "TenderPaymentDetails.jsp";
                                querString = "payId=" + paymentId + "&uId=" + strBidderUserId + "&tenderId=" + tenderId + "&lotId=" + lotId + "&payTyp=" + payTyp + "&msgId=" + strRequestAction + "requested&view="+view+"";

                                path = request.getContextPath() + "/partner/" + pageNm + "?" + querString;

                                response.sendRedirect(path);
                            }

                        } catch (Exception e) {
                            LOGGER.error("Error in Payment Release/Forfeit Request Process: " + e.toString());
                        } finally {
                            // Nullify the List objects
                            userId = null;
                            strPartTransId = null;
                            paymentId = null;
                            tenderRefNo = null;
                            tenderId = null;
                            lotId = null;
                            payTyp = null;
                            pageNm = null;
                            querString = null;
                            path = null;
                            remarks = null;
                            fnlAmt = null;
                            status = null;
                            strBidderUserId = null;
                        }


                    }

                }
                /*Dohatec Start For Performance Security Validity Extension*/
                else if ("requestExtendValidityPS".equalsIgnoreCase(strAction)) {

                    if (request.getParameter("btnSubmit") != null) {
                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        int conId = c_ConsSrv.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                        System.out.println(" conId "+ String.valueOf(conId));
                        HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        String strPartTransId = "0", userId = "";
                        HttpSession hs = request.getSession();
                        if (hs.getAttribute("userId") == null) {
                            response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp");
                        } else {
                            userId = hs.getAttribute("userId").toString();
                        }
                        if (hs.getAttribute("govUserId") != null) {
                            strPartTransId = hs.getAttribute("govUserId").toString();
                        }
                        
                        boolean isIctTender = false;
                        String[] payIdArray = new String[4];
                        int totalPayId = 0;

                        if("ps".equalsIgnoreCase(request.getParameter("payTyp")) || "bg".equalsIgnoreCase(request.getParameter("payTyp")))
                        {
                            // Tender is ICT or not
                            List<SPTenderCommonData> listICT = objTSC.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                            if(!listICT.isEmpty() && listICT.get(0).getFieldName1().equalsIgnoreCase("true")){
                                isIctTender = true;
                            }
                        }                        

                        boolean sendMail = false;
                        String paymentId = request.getParameter("payId");
                        System.out.print("\nPayment Id : " + paymentId);
                        String tenderRefNo = request.getParameter("tenderRefNo");
                        String tenderId = request.getParameter("tenderId");
                        String lotId = request.getParameter("lotId");
                        String pageNm = "";
                        String querString = "";
                        String path = "";
                        String remarks = "";
                        String fnlAmt = "";
                        String status = "";
                        String strBidderUserId = "0";
                        String view = "";
                        String strRequestType = request.getParameter("requesttype");
                        String ValidityExtendedDate = "";
                        String OldValidityDate = "";
                        if ( request.getParameter("extValidityDate") != null){
                            ValidityExtendedDate = request.getParameter("extValidityDate");
                        }
                        if ( request.getParameter("txtValidityDate") != null){
                            OldValidityDate = request.getParameter("txtValidityDate");
                        }

                        if(isIctTender && "bg".equalsIgnoreCase(request.getParameter("payTyp")))
                        {
                            List<SPCommonSearchDataMore> lst = objTSDetails.geteGPData("getNewPerSecPaymentIDDetail", tenderId.toString(), lotId, request.getParameter("hdnBidderUserId"));
                            System.out.print("Old Paymend id list size " + lst.size());
                            if(!lst.isEmpty())
                            {
                                totalPayId = lst.size();
                                for(int i = 0; i< lst.size(); i++)
                                {
                                    payIdArray[i] = lst.get(i).getFieldName1();
                                }
                            }
                        }

                        fnlAmt = request.getParameter("hdnAmount");
                        remarks = request.getParameter("txtComments");
                        status = request.getParameter("hdnPayStatus");
                        strBidderUserId = request.getParameter("hdnBidderUserId");
                        view = request.getParameter("view");
                        
                        /* START: CODE TO HANDLE SPECIAL CHARACTERS  */
                        remarks = handleSpecialChar.handleSpecialChar(remarks);
                        /* END CODE TO HANDLE SPECIAL CHARACTERS  */

                        String auditObjectType=null;
                        int auditObjectId=0;
                        String auditAction=null;
                        String payTyp = request.getParameter("payTyp");


                        String strRequestTo = "";
                        String strRequestAction = "";
                        String strMsg = "";

                        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                        List<SPTenderCommonData> lstPayerInfo = tenderCommonService.returndata("getPaymentPaidByUserId", paymentId, null);
                        System.out.print("\nSize >>>>>"+lstPayerInfo.size()+"\nPayment ID>>>>" + paymentId);

                        if (!lstPayerInfo.isEmpty()) {
                            if (strRequestType.equalsIgnoreCase("extendValidityps")) {
                                // Sent Forfeit request to the Maker
                                strRequestTo = lstPayerInfo.get(0).getFieldName2();
                            }
                        }

                        if (strRequestType.equalsIgnoreCase("extendValidityps")) {
                            strRequestAction = "extend";
                            strMsg = "extensionrequested";
                            sendMail = true;
                            if ("ts".equalsIgnoreCase(payTyp))
                            {
                                auditAction = "Validity Extension Request For Bid Security";
                                auditObjectId=Integer.parseInt(tenderId);
                                auditObjectType="tenderId";
                            }
                            else if ("ps".equalsIgnoreCase(payTyp))
                            {
                                auditAction = "Validity Extension Request For Performance Security";
                                auditObjectId=Integer.parseInt(tenderId);
                                auditObjectType="tenderId";
                            }
                            else if ("bg".equalsIgnoreCase(payTyp))
                            {
                                auditAction = "Validity Extension Request For New Performance Security";
                                auditObjectId=conId;
                                auditObjectType="contractId";
                            }
                            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), auditObjectId, auditObjectType, EgpModule.Payment.getName(), auditAction, "");

                        }

                        try {
                            
                            boolean successType;

                            if(isIctTender && "bg".equalsIgnoreCase(payTyp)) //  Added by Dohatec
                            {
                                int flag = 0;
                                for(int i = 0; i < totalPayId; i++){
                                    
                                    /*added by dohatec to resolve issue 8715 Start*/
                                    TblPaymentExtensionActionRequest pymtRequest = new TblPaymentExtensionActionRequest();
                                    pymtRequest.setPaymentId(Integer.parseInt(payIdArray[i]));
                                    pymtRequest.setRequestBy(Integer.parseInt(userId));
                                    pymtRequest.setRequestTo(Integer.parseInt(strBidderUserId));
                                    pymtRequest.setRequestAction(strRequestAction);
                                    pymtRequest.setRequestComments(remarks);
                                    pymtRequest.setActionStatus("Pending");
                                    pymtRequest.setActionCompDt(new Date());
                                    pymtRequest.setActionComments("");
                                    pymtRequest.setRequestDate(new Date());
                                    pymtRequest.setOldValidityDate(new SimpleDateFormat("dd/MM/yyyy").parse(OldValidityDate));
                                    pymtRequest.setNewValidityDate(new SimpleDateFormat("dd/MM/yyyy").parse(ValidityExtendedDate));
                                    successType = paymentService.addPEPaymentExtendRequest(pymtRequest);
                                    /*added by dohatec to resolve issue 8715 End*/
                                    if(successType)
                                        flag++;
                                }
                                if(flag == totalPayId)
                                    successType = true;
                                else
                                    successType = false;
                            }
                            else    //  NCT
                            {
                              
                                /*added by dohatec to resolve issue 8715 Start*/
                                    TblPaymentExtensionActionRequest pymtRequest = new TblPaymentExtensionActionRequest();
                                    pymtRequest.setPaymentId(Integer.parseInt(paymentId));
                                    pymtRequest.setRequestBy(Integer.parseInt(userId));
                                    pymtRequest.setRequestTo(Integer.parseInt(strBidderUserId));
                                    pymtRequest.setRequestAction(strRequestAction);
                                    pymtRequest.setRequestComments(remarks);
                                    pymtRequest.setActionStatus("Pending");
                                    pymtRequest.setActionCompDt(new Date());
                                    pymtRequest.setActionComments("");
                                    pymtRequest.setRequestDate(new Date());
                                    pymtRequest.setOldValidityDate(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(OldValidityDate + " 23:59:59"));
                                    pymtRequest.setNewValidityDate(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(ValidityExtendedDate + " 23:59:59"));
                                    successType = paymentService.addPEPaymentExtendRequest(pymtRequest);
                                    /*added by dohatec to resolve issue 8715 End*/
                            }

                            //if (commonMsgChk.getFlag().equals(true)) {
                            if (successType == true) {
                                /* START : CODE TO SEND MAIL */
                                boolean mailSent = false;
                                String paymentFor = "";

                                if ("ts".equalsIgnoreCase(payTyp)) {
                                    paymentFor = "Bid Security";
                                } else if ("ps".equalsIgnoreCase(payTyp)) {
                                    paymentFor = "Performance Security";
                                }else if("ps".equalsIgnoreCase(payTyp)) {
                                     if (strRequestType.equalsIgnoreCase("requestrelease")) {

                                     }else{

                                     }
                                }

                                if("ps".equalsIgnoreCase(payTyp) || "bg".equalsIgnoreCase(payTyp)){                                   
                                    if (strRequestType.equalsIgnoreCase("extendValidityps")) {
                                        sendMailForValidityExtendRequest(tenderId, lotId, strRequestTo, logUserId, Integer.parseInt(userTypeId),payTyp);
                                    } 
                                }else{
                                    SendRequestReleaseForfeitMail(paymentId, paymentFor, strBidderUserId, tenderId, tenderRefNo, lotId, userId, strRequestTo, strRequestAction, remarks,fnlAmt);
                                }
                                /* END : CODE TO SEND MAIL */

                                pageNm = "ViewTenderPaymentExtensionDetails.jsp";
                                querString = "payId=" + paymentId + "&uId=" + strBidderUserId + "&tenderId=" + tenderId + "&lotId=" + lotId + "&payTyp=" + payTyp + "&msgId=" + strRequestAction + "requested&view="+view+"";

                                path = request.getContextPath() + "/partner/" + pageNm + "?" + querString;

                                response.sendRedirect(path);
                            }

                        } catch (Exception e) {
                            LOGGER.error("Error in Payment Release/Forfeit Request Process: " + e.toString());
                        } finally {
                            // Nullify the List objects
                            userId = null;
                            strPartTransId = null;
                            paymentId = null;
                            tenderRefNo = null;
                            tenderId = null;
                            lotId = null;
                            payTyp = null;
                            pageNm = null;
                            querString = null;
                            path = null;
                            remarks = null;
                            fnlAmt = null;
                            status = null;
                            strBidderUserId = null;
                        }


                    }

                }

                /*tenderer portion */
                 else if ("BidderAcceptExtValidity".equalsIgnoreCase(strAction)) {
                    if (request.getParameter("btnSubmit") != null) {
                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        int conId = c_ConsSrv.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                        System.out.println(" conId "+ String.valueOf(conId));
                        HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        String strReqTransId = "0", userId = "";
                        String strRequestUserFrom = "";
                        if(request.getParameter("RequestUserFrom")!= null){
                            strRequestUserFrom=request.getParameter("RequestUserFrom");
                        }
                        HttpSession hs = request.getSession();
                        if (hs.getAttribute("userId") == null) {
                            response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp");
                        } else {
                            userId = hs.getAttribute("userId").toString();
                        }
                        if(request.getParameter("rqId")!= null){
                            strReqTransId=request.getParameter("rqId");
                        }

                        boolean isIctTender = false;
                        String[] payIdArray = new String[4];
                        int totalPayId = 0;

                        if("ps".equalsIgnoreCase(request.getParameter("payTyp")) || "bg".equalsIgnoreCase(request.getParameter("payTyp")))
                        {
                            // Tender is ICT or not
                            List<SPTenderCommonData> listICT = objTSC.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                            if(!listICT.isEmpty() && listICT.get(0).getFieldName1().equalsIgnoreCase("true")){
                                isIctTender = true;
                            }
                        }

                        boolean sendMail = false;
                        String paymentId = request.getParameter("payId");
                        System.out.print("\nPayment Id : " + paymentId);
                        String tenderRefNo = request.getParameter("tenderRefNo");
                        String tenderId = request.getParameter("tenderId");
                        String lotId = request.getParameter("lotId");
                        String pageNm = "";
                        String querString = "";
                        String path = "";
                        String remarks = "";
                        String fnlAmt = "";
                        String status = "";
                        String strBidderUserId = "0";
                        String view = "";
                        String strRequestType = request.getParameter("requesttype");
                        String ValidityExtendedDate = "";
                        String OldValidityDate = "";
                        if ( request.getParameter("extValidityDate") != null){
                            ValidityExtendedDate = request.getParameter("extValidityDate");
                        }
                        if ( request.getParameter("txtValidityDate") != null){
                            OldValidityDate = request.getParameter("txtValidityDate");
                        }


                        if(isIctTender && "bg".equalsIgnoreCase(request.getParameter("payTyp")))
                        {
                            List<SPCommonSearchDataMore> lst = objTSDetails.geteGPData("getNewPerSecPaymentIDDetail", tenderId.toString(), lotId, request.getParameter("hdnBidderUserId"));
                            System.out.print("Old Paymend id list size " + lst.size());
                            if(!lst.isEmpty())
                            {
                                totalPayId = lst.size();
                                for(int i = 0; i< lst.size(); i++)
                                {
                                    payIdArray[i] = lst.get(i).getFieldName1();
                                }
                            }
                        }

                        fnlAmt = request.getParameter("hdnAmount");
                        remarks = request.getParameter("txtComments");
                        status = request.getParameter("hdnPayStatus");
                        strBidderUserId = request.getParameter("hdnBidderUserId");
                        view = request.getParameter("view");

                        /* START: CODE TO HANDLE SPECIAL CHARACTERS  */
                        remarks = handleSpecialChar.handleSpecialChar(remarks);
                        /* END CODE TO HANDLE SPECIAL CHARACTERS  */

                        String auditObjectType=null;
                        int auditObjectId=0;
                        String auditAction=null;
                        String payTyp = request.getParameter("payTyp");


                        String strRequestTo = "";
                        String strRequestAction = "";
                        String strMsg = "";

                        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                        List<SPTenderCommonData> lstPayerInfo = tenderCommonService.returndata("getPaymentPaidByUserId", paymentId, null);
                        System.out.print("\nSize >>>>>"+lstPayerInfo.size()+"\nPayment ID>>>>" + paymentId);

                        if (!lstPayerInfo.isEmpty()) {
                            if (strRequestType.equalsIgnoreCase("extendValidityps")) {
                                // Sent Forfeit request to the Maker
                                strRequestTo = lstPayerInfo.get(0).getFieldName2();
                            }
                        }

                        if (strRequestType.equalsIgnoreCase("extendValidityps")) {
                            strRequestAction = "extend";
                            strMsg = "extensionrequested";
                            sendMail = true;
                            if ("ts".equalsIgnoreCase(payTyp))
                            {
                                auditAction = "Validity Extension Accept For Bid Security";
                                auditObjectId=Integer.parseInt(tenderId);
                                auditObjectType="tenderId";
                            }
                            else if ("ps".equalsIgnoreCase(payTyp))
                            {
                                auditAction = "Validity Extension Accept For Performance Security";
                                auditObjectId=Integer.parseInt(tenderId);
                                auditObjectType="tenderId";
                            }
                            else if ("bg".equalsIgnoreCase(payTyp))
                            {
                                auditAction = "Validity Extension Accept For New Performance Security";
                                auditObjectId=conId;
                                auditObjectType="contractId";
                            }
                            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), auditObjectId, auditObjectType, EgpModule.Payment.getName(), auditAction, "");

                        }

                        try {

                            boolean successType;

                            if(isIctTender && "bg".equalsIgnoreCase(payTyp)) //  Added by Dohatec
                            {
                                int flag = 0;
                                for(int i = 0; i < totalPayId; i++){                                                                        
                                    successType = paymentService.updatePaymentExtendRequest(payIdArray[i],remarks,strReqTransId);
                                    if(successType && strRequestUserFrom.equalsIgnoreCase("Checker"))
                                        {
                                        successType = paymentService.updateTenderPaymentExtendRequest(payIdArray[i],ValidityExtendedDate,userId );
                                    } else  if (successType && strRequestUserFrom.equalsIgnoreCase("Bidder"))
                                     {
                                         TblPaymentExtensionActionRequest pymtRequest = new TblPaymentExtensionActionRequest();
                                    pymtRequest.setPaymentId(Integer.parseInt(payIdArray[i]));
                                    pymtRequest.setRequestBy(Integer.parseInt(userId));
                                    pymtRequest.setRequestTo(Integer.parseInt(strRequestTo));
                                    pymtRequest.setRequestAction(strRequestAction);
                                    pymtRequest.setRequestComments(remarks);
                                    pymtRequest.setActionStatus("Pending");
                                    pymtRequest.setActionCompDt(new Date());
                                    pymtRequest.setActionComments("");
                                    pymtRequest.setRequestDate(new Date());
                                    pymtRequest.setOldValidityDate(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(OldValidityDate + " 23:59:59"));
                                    pymtRequest.setNewValidityDate(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(ValidityExtendedDate + " 23:59:59"));
                                    successType = paymentService.addPEPaymentExtendRequest(pymtRequest);
                                    }
                                    if(successType)
                                        flag++;
                                }
                                if(flag == totalPayId)
                                    successType = true;
                                else
                                    successType = false;
                            }
                            else    //  NCT
                            {
                                     successType = paymentService.updatePaymentExtendRequest(paymentId,remarks,strReqTransId );

                                     if(successType && strRequestUserFrom.equalsIgnoreCase("Checker"))
                                        {
                                        successType = paymentService.updateTenderPaymentExtendRequest(paymentId,ValidityExtendedDate,userId );
                                    } else  if (successType && strRequestUserFrom.equalsIgnoreCase("Bidder"))
                                    { TblPaymentExtensionActionRequest pymtRequest = new TblPaymentExtensionActionRequest();
                                    pymtRequest.setPaymentId(Integer.parseInt(paymentId));
                                    pymtRequest.setRequestBy(Integer.parseInt(userId));
                                    pymtRequest.setRequestTo(Integer.parseInt(strRequestTo));
                                    pymtRequest.setRequestAction(strRequestAction);
                                    pymtRequest.setRequestComments(remarks);
                                    pymtRequest.setActionStatus("Pending");
                                    pymtRequest.setActionCompDt(new Date());
                                    pymtRequest.setActionComments("");
                                    pymtRequest.setRequestDate(new Date());
                                    System.out.println("OldValidityDate "+ OldValidityDate + "  ValidityExtendedDate "+ValidityExtendedDate);
                                    pymtRequest.setOldValidityDate(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(OldValidityDate + " 23:59:59"));
                                    pymtRequest.setNewValidityDate(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(ValidityExtendedDate + " 23:59:59"));
                                    successType = paymentService.addPEPaymentExtendRequest(pymtRequest);
                                     }

                            }

                            //if (commonMsgChk.getFlag().equals(true)) {
                            if (successType == true) {
                                /* START : CODE TO SEND MAIL */
                                boolean mailSent = false;
                                String paymentFor = "";

                                if ("ts".equalsIgnoreCase(payTyp)) {
                                    paymentFor = "Bid Security";
                                } else if ("ps".equalsIgnoreCase(payTyp)) {
                                    paymentFor = "Performance Security";
                                }else if("ps".equalsIgnoreCase(payTyp)) {
                                     if (strRequestType.equalsIgnoreCase("requestrelease")) {

                                     }else{

                                     }
                                }

                                if("ps".equalsIgnoreCase(payTyp) || "bg".equalsIgnoreCase(payTyp)){
                                    if (strRequestType.equalsIgnoreCase("extendValidityps")) {
                                        sendMailForValidityExtendRequest(tenderId, lotId, strRequestTo, logUserId, Integer.parseInt(userTypeId),payTyp);
                                    }
                                }else{
                                    
                                }
                                /* END : CODE TO SEND MAIL */

                                pageNm = "ViewTenderPaymentExtensionDetails.jsp";
                                if(strRequestUserFrom.equalsIgnoreCase("Checker")){
                                querString = "payId=" + paymentId + "&uId=" + strBidderUserId + "&tenderId=" + tenderId + "&lotId=" + lotId + "&payTyp=" + payTyp + "&msgId=" + strRequestAction + "requested";
                                } else if(strRequestUserFrom.equalsIgnoreCase("Bidder")){
                                querString = "payId=" + paymentId + "&uId=" + strBidderUserId + "&tenderId=" + tenderId + "&lotId=" + lotId + "&payTyp=" + payTyp + "&msgId=" + strRequestAction + "requested&view="+view+"";
                                }
                                path = request.getContextPath() + "/partner/" + pageNm + "?" + querString;

                                response.sendRedirect(path);
                            }

                        } catch (Exception e) {
                            LOGGER.error("Error in Payment Release/Forfeit Request Process: " + e.toString());
                        } finally {
                            // Nullify the List objects
                            userId = null;                            
                            paymentId = null;
                            tenderRefNo = null;
                            tenderId = null;
                            lotId = null;
                            payTyp = null;
                            pageNm = null;
                            querString = null;
                            path = null;
                            remarks = null;
                            fnlAmt = null;
                            status = null;
                            strBidderUserId = null;
                        }


                    }

                }
                /*tenderer portion*/

                /*Dohatec End For Performance Security Validity Extension*/


            }
        } finally {
            out.close();
        }
        LOGGER.debug("processRequest : " + logUserId + " Ends");
    }

    /**
     * this method is for getting the branch member data from database
     * @param branchId
     * @return String
     * @throws Exception
     */
    public String getBranchMembers(String branchId) throws Exception {
        LOGGER.debug("getBranchMembers :" + logUserId + " : Starts");
        StringBuilder str = new StringBuilder();
        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        tenderCommonService.setLogUserId(logUserId);
        try {
            str.append("<option value=''>- Select Branch Maker -</option>");

            for (SPTenderCommonData sptcd : tenderCommonService.returndata("getBankBranchOrMemberList", branchId, "getBranchMemberList")) {
                str.append("<option value='").append(sptcd.getFieldName1()).append("'>").append(sptcd.getFieldName2()).append("</option>");
            }
            //return str.toString();
        } catch (Exception e) {
            LOGGER.error("Error in getBranchMembers : " + e.toString());
            str.append("");
        } finally {
            tenderCommonService = null;
        }
        LOGGER.debug("getBranchMembers : " + logUserId + " : Ends");
        return str.toString();
    }

    /**
     * this method is for sending mail
     * @param paymentId
     * @param payStatus
     * @param paymentFor
     * @param payUserId
     * @param tenderId
     * @param TenderRefNo
     * @param amount
     * @param verifiedByUserId
     * @return boolean, true - if send successfully, false - if failed
     * @throws Exception
     */
    /** send mail notification to PE/tenderer on forfeiting/releasing the ps/bg/ts by maker */
    public boolean SendMail(String paymentId, String payStatus, String paymentFor, String payUserId, String tenderId, String TenderRefNo, String amount, String verifiedByUserId) throws Exception {
        LOGGER.debug("SendMail :" + logUserId + " : Starts");
        boolean isSent = false;
        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        tenderCommonService.setLogUserId(payUserId);
        try {
            String mailSubject = "";
            String actionTaken = "";
            String strFrmEmailId = "";
            String peOfficerUserId = "";
            String tendererCompanyNm = "";
            String tendererMobileNo = "";
            String bankNm = "";
            String branchNm = "";

            if ("paid".equalsIgnoreCase(payStatus)) {
                mailSubject = "e-GP: " + paymentFor + " Payment";
                actionTaken = "paid";
            } else if ("extended".equalsIgnoreCase(payStatus)) {
                mailSubject = "e-GP: " + paymentFor + " Payment";
                actionTaken = "extended";
            }

            /* START : CODE TO SEND MAIL */
            List<SPTenderCommonData> lstPaymentBankInfo =
                    tenderCommonService.returndata("getPaymentBankInfofromPaymentId", paymentId, null);

            if (!lstPaymentBankInfo.isEmpty()) {
                bankNm = lstPaymentBankInfo.get(0).getFieldName1();
                branchNm = lstPaymentBankInfo.get(0).getFieldName2();
            }

            List<SPTenderCommonData> lstOfficerUserId =
                    tenderCommonService.returndata("getPEOfficerUserIdfromTenderId", tenderId, null);
            peOfficerUserId = lstOfficerUserId.get(0).getFieldName1();

            List<SPTenderCommonData> frmEmail =
                    tenderCommonService.returndata("getEmailIdfromUserId", verifiedByUserId, null);
            strFrmEmailId = frmEmail.get(0).getFieldName1();

            List<SPTenderCommonData> peOfficerEmail =
                    tenderCommonService.returndata("getEmailIdfromUserId", peOfficerUserId, null);

            List<SPTenderCommonData> lstTendererCompany =
                    tenderCommonService.returndata("getTendererCompanyName", payUserId, null);
            tendererCompanyNm = lstTendererCompany.get(0).getFieldName1();
            tendererMobileNo = lstTendererCompany.get(0).getFieldName2();

            List<SPTenderCommonData> emails =
                    tenderCommonService.returndata("getEmailIdfromUserId", payUserId, null);

            UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
            //String [] mail={emails.get(0).getFieldName1(), peOfficerEmail.get(0).getFieldName1()};
            String[] mail = {emails.get(0).getFieldName1()};

            // Code To Send Mail to Bidder
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();

            String mailText = mailContentUtility.contTenderPaymentVerification(tenderId, TenderRefNo, paymentFor, payStatus, actionTaken, tendererCompanyNm, bankNm, branchNm, amount);

            //sendMessageUtil.setEmailFrom(strFrmEmailId);
            sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailId"));
            sendMessageUtil.setEmailTo(mail);
            sendMessageUtil.setEmailSub(HandleSpecialChar.handleSpecialChar(mailSubject));
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            // ==========================
            // Code To Send SMS to Bidder
            //sendMessageUtil.setSmsNo(tendererMobileNo);
            //sendMessageUtil.setSmsBody("Sms Content Pending");
            //sendMessageUtil.sendSMS();
            // ==========================
            // Code To Send Message to Bidder
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            userRegisterService.contentAdmMsgBox(mail[0], XMLReader.getMessage("emailIdNoReply"), HandleSpecialChar.handleSpecialChar(mailSubject), msgBoxContentUtility.contTenderPaymentVerification(tenderId, TenderRefNo, paymentFor, payStatus, actionTaken, tendererCompanyNm, bankNm, branchNm, amount));
            // ==========================
            sendMessageUtil = null;
            mailContentUtility = null;
            mail = null;
            msgBoxContentUtility = null;
            /* END : CODE TO SEND MAIL */
            // Nullify the List objects
            emails = null;
            lstTendererCompany = null;
            peOfficerEmail = null;
            frmEmail = null;
            lstOfficerUserId = null;
            lstPaymentBankInfo = null;
            // Nullify the String objects
            mailSubject = null;
            actionTaken = null;
            strFrmEmailId = null;
            peOfficerUserId = null;
            tendererCompanyNm = null;
            tendererMobileNo = null;
            bankNm = null;
            branchNm = null;

            isSent = true;
            //return true;
        } catch (Exception e) {
            LOGGER.error("Error in SendMail : " + e.toString());
            //return false;
            isSent = false;
        }
        LOGGER.debug("sendmail : " + logUserId + " : Ends");
        return isSent;
    }

    /** send mail notification to PE/tenderer on forfeiting/releasing the ps/bg/ts by maker */
    public boolean SendRequestReleaseForfeitMail(String paymentId, String paymentFor, String payUserId, String tenderId, String TenderRefNo, String lotId, String requestByUserId, String requestToUserId, String requestAction, String remarks,String Amount) throws Exception {
        LOGGER.debug("SendRequestReleaseForfeitMail :" + logUserId + " : Starts");
        boolean isSent = false;
        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        tenderCommonService.setLogUserId(payUserId);
//        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
//        Object[] obj = null;
//        if (!GetCinfoBar.isEmpty()) {
//            obj = GetCinfoBar.get(0);
//        }
        try {
            String mailSubject = "";
            String actionTaken = "";
            String strFrmEmailId = "";
            String peOfficerUserId = "";
            String tendererCompanyNm = "";
            String tendererMobileNo = "";
            String bankNm = "";
            String branchNm = "";
            String actionToTake = "";
            String strLotNo = "";
            String strLotDescription = "";
            String strPEOffice = "";

            if ("release".equalsIgnoreCase(requestAction)) {
                //mailSubject="Release request of "+ paymentFor +" on e-GP";
                //mailSubject = "Request from PE to Return " + paymentFor;
                actionToTake = "released";
            } else if ("forfeit".equalsIgnoreCase(requestAction)) {
                //mailSubject="Forfeit request of "+ paymentFor +" on e-GP";

                // mailSubject="Request from PE to Forfeit "+ paymentFor;
                //mailSubject = "e-GP: Request for Forfeiture of " + paymentFor;
                actionToTake = "forfeited";
            }

            if (lotId != null && lotId != "" && lotId != "null") {
                // Get Lot No and Lot Description
                for (SPTenderCommonData commonTenderDetails : tenderCommonService.returndata("gettenderlotbytenderidForPayment", tenderId, "ts")) {

                    if (lotId.equalsIgnoreCase(commonTenderDetails.getFieldName3())) {
                        strLotNo = commonTenderDetails.getFieldName1();
                        strLotDescription = commonTenderDetails.getFieldName2();
                    }
                }
            }

            // Get PE Office Name
            List<SPTenderCommonData> lstOfficerUserId =
                    tenderCommonService.returndata("getPEOfficerUserIdfromTenderId", tenderId, null);
            if (!lstOfficerUserId.isEmpty()) {
                //peOfficerUserId = lstOfficerUserId.get(0).getFieldName1(); // Get PE UserId
                strPEOffice = lstOfficerUserId.get(0).getFieldName3(); // Get PE Office Name
            }

            /* START : CODE TO SEND MAIL */
            List<SPTenderCommonData> lstPayerInfo =
                    tenderCommonService.returndata("getEmailIdfromUserId", requestToUserId, null);

            List<SPTenderCommonData> lstTendererCompany =
                    tenderCommonService.returndata("getTendererCompanyName", payUserId, null);
            tendererCompanyNm = lstTendererCompany.get(0).getFieldName1();

            UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
            String[] mail = null;
            int emailIndex = 0;
            mail = new String[2];
            mail[emailIndex] = accPaymentService.getEmailId(payUserId);
            mail[emailIndex + 1] = lstPayerInfo.get(0).getFieldName1();
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
            String peName = "";
            List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(logUserId));
            if (peNameObj.get(0) != null) {
                peName = peNameObj.get(0).toString();
            }
            String bankName = accPaymentService.getBankName(paymentId);
            String branchName = accPaymentService.getBranchName(paymentId);
            String emailId[] = new String[1];
            for (int i = 0; i < mail.length; i++) {
                if ("release".equalsIgnoreCase(requestAction)) {
                    mailSubject = "Request from PE to Return " + paymentFor;
                }else if("forfeit".equalsIgnoreCase(requestAction)){
                    mailSubject = "e-GP: Request for Forfeiture of " + paymentFor;
                }
                if(!"7".equalsIgnoreCase(accPaymentService.getUserTypeId(emailId[0])))
                {
                    if ("release".equalsIgnoreCase(requestAction)) {
                        mailSubject = "Release of "+paymentFor+"";
                    }
                }
                sendMessageUtil.setEmailSub(HandleSpecialChar.handleSpecialChar(mailSubject));
                emailId[0] = mail[i];
                userRegisterService.contentAdmMsgBox(emailId[0], XMLReader.getMessage("emailIdNoReply"), HandleSpecialChar.handleSpecialChar(mailSubject), msgBoxContentUtility.contRequest_ReleaseForfeit(tenderId, TenderRefNo, strLotNo, strLotDescription, paymentFor, actionToTake, tendererCompanyNm, remarks, strPEOffice,accPaymentService.getUserTypeId(emailId[0]),peName,bankName,branchName,Amount));
                String mailText = mailContentUtility.contRequest_ReleaseForfeit(tenderId, TenderRefNo, strLotNo, strLotDescription, paymentFor, actionToTake, tendererCompanyNm, remarks, strPEOffice,accPaymentService.getUserTypeId(emailId[0]),peName,bankName,branchName,Amount);
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.setEmailTo(emailId);
                sendMessageUtil.sendEmail();
            }
            sendMessageUtil = null;
            mailContentUtility = null;
            mail = null;
            msgBoxContentUtility = null;

            lstTendererCompany = null;
            lstPayerInfo = null;
            lstOfficerUserId = null;

            /* END : CODE TO SEND MAIL */

            isSent = true;
            //return true;
        } catch (Exception e) {
            LOGGER.error("Error in SendRequestReleaseForfeitMail : " + e.toString());
            //return false;
            isSent = false;
        }
        LOGGER.debug("SendRequestReleaseForfeitMail : " + logUserId + " : Ends");
        return isSent;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /** send mail notification to PE/tenderer on releasing the ps/bg/ts by maker */
    private boolean sendMailForReleaseandRequest(String tenderId, String lotId, String requestToUserId, String logUserId, int userTypeId,String payTyp) {
        boolean flag = false;
        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        tenderCommonService.setLogUserId(logUserId);
        List<SPTenderCommonData> lstPayerInfo =
                tenderCommonService.returndata("getEmailIdfromUserId", requestToUserId, null);

        //Code Comment Start by Proshanto Kumar Saha,dohatec
         //List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        //Code Comment End by Proshanto Kumar Saha,dohatec

        //Code Start by Proshanto Kumar Saha,Dohatec
        //This portion is used to collect GetCinfoBar with or without contract sign by checking contract sign is not done,performance security is given and tender validity is over.
        String mailText=null;
        List<Object[]> GetCinfoBar=null;
        boolean checkStatus = false;
        CommonSearchServiceSrBean objCommonSearchServiceSrBean=new CommonSearchServiceSrBean();
        checkStatus=objCommonSearchServiceSrBean.getCSignTValidityAndPSecurityStatus(tenderId, logUserId);
        if(checkStatus)
        {
        GetCinfoBar = cmsConfigDateService.getContractInfoBarWithoutCSign(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        }
        else
        {
        GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        }
        //Code End by Proshanto Kumar Saha,Dohatec

        Object[] obj = null;
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peName = "";
            List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(logUserId));
            if (peNameObj.get(0) != null) {
                peName = peNameObj.get(0).toString();
            }
            String strFrom = commonservice.getEmailId(logUserId);
            List<Object[]> AcEmailId = accPaymentService.getAccountantEmailId(Integer.parseInt(tenderId));
            String[] strTo = null;
            int emailIndex = 0;
            if (userTypeId == 3) {
                strTo = new String[AcEmailId.size() + 2];
                for (Object[] emailId : AcEmailId) {
                    strTo[emailIndex] = emailId[4].toString();
                    emailIndex++;
                }
                strTo[emailIndex] = obj[9].toString();
                strTo[emailIndex + 1] = lstPayerInfo.get(0).getFieldName1();
            }
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            sendMessageUtil.setEmailFrom(strFrom);

            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String emailId[] = new String[1];
            String mailSubject = "";
            String mobileno = "";
            String strfor = "Performance Security";
            if("ts".equalsIgnoreCase(payTyp)){
                strfor = "Bid Security";
            }
            for (int i = 0; i < strTo.length; i++) {
                emailId[0] = strTo[i];
                if ("3".equalsIgnoreCase(accPaymentService.getUserTypeId(emailId[0]))) {
                    mailSubject = "e-GP: "+strfor+" Released";
                } else if ("7".equalsIgnoreCase(strFrom)) {
                    mailSubject = "e-GP: Request for Releasing "+strfor+"";
                } else {
                    mailSubject = "e-GP: Request for Releasing "+strfor+" Released";
                }

                //Code Comment Start by Proshanto Kumar Saha,Dohatec
                //registerService.contentAdmMsgBox(emailId[0], strFrom, mailSubject, msgBoxContentUtility.mailForReleaseRequest(c_isPhysicalPrComplete, peName, obj, accPaymentService.getUserTypeId(emailId[0]), Integer.toString(userTypeId), accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])),strfor));
                //String mailText = mailContentUtility.mailForReleaseRequest(c_isPhysicalPrComplete, peName, obj, accPaymentService.getUserTypeId(emailId[0]), Integer.toString(userTypeId), accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])),strfor);
                //Code Comment End by Proshanto Kumar Saha,Dohatec

                //Code Start By Proshanto Kumar Saha,Dohatec
                //This portion is used to add mailmessage and set mailtext with or without contract sign by checking contract sign is not done,performance security is given and tender validity is over.
                if(checkStatus)
                {
                registerService.contentAdmMsgBox(emailId[0], strFrom, mailSubject, msgBoxContentUtility.mailForReleaseRequestWithoutConSign(c_isPhysicalPrComplete, peName, obj, accPaymentService.getUserTypeId(emailId[0]), Integer.toString(userTypeId), accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])),strfor,tenderId));
                mailText = mailContentUtility.mailForReleaseRequestWithoutConSign(c_isPhysicalPrComplete, peName, obj, accPaymentService.getUserTypeId(emailId[0]), Integer.toString(userTypeId), accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])),strfor,tenderId);
                }
                else
                {
                registerService.contentAdmMsgBox(emailId[0], strFrom, mailSubject, msgBoxContentUtility.mailForReleaseRequest(c_isPhysicalPrComplete, peName, obj, accPaymentService.getUserTypeId(emailId[0]), Integer.toString(userTypeId), accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])),strfor));
                mailText = mailContentUtility.mailForReleaseRequest(c_isPhysicalPrComplete, peName, obj, accPaymentService.getUserTypeId(emailId[0]), Integer.toString(userTypeId), accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])),strfor);
                }
                //Code End by Proshanto Kumar Saha,Dohatec

                sendMessageUtil.setEmailTo(emailId);
                sendMessageUtil.setEmailSub(mailSubject);
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                if ("2".equalsIgnoreCase(accPaymentService.getUserTypeId(emailId[0]))) {
                    mobileno = accPaymentService.getMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                } else if ("7".equalsIgnoreCase(accPaymentService.getUserTypeId(emailId[0]))) {
                    mobileno = accPaymentService.getBankUserMobileNowithCountryCode(accPaymentService.getUserId(emailId[0]));
                } else {
                    mobileno = accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                }
                sendMessageUtil.setSmsNo(mobileno);
                StringBuilder sb = new StringBuilder();
                sb.append("Dear User,%0AThis is to inform you that PA has requested Financial Institute for releasing "+strfor+" ");

                //Code Comment Start by Proshanto Kumar Saha,Dohatec
                //sb.append("for Contract No."+obj[1].toString() +" ("+ obj[15].toString() +")"+" ");
                //Code Comment End by Proshanto Kumar Saha,Dohatec

                //Code Start by Proshanto Kumar Saha,Dohatec
                //This portion is used to sent message in mobile with tenderId or with contract no by checking contract sign is not done,performance security is given and tender validity is over.
                if(checkStatus)
                {
                sb.append("for Tender ID :"+tenderId+" ");
                }
                else
                {
                sb.append("for Contract No."+obj[1].toString() +" ("+ obj[15].toString() +")"+" ");
                }
                //Code End by Proshanto Kumar Saha,Dohatec

                sb.append("%0AThanks,%0Ae-GP System");
                sendMessageUtil.setSmsBody(sb.toString());
                if (!"".equalsIgnoreCase(mobileno) && mobileno != null) {
                    sendMessageUtil.sendSMS();
                }
            }
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }

    /** send mail notification to PE/tenderer on forfeiting the ps/bg/ts by maker */
    private boolean sendMailForForefietRequest(String tenderId, String lotId, String requestToUserId, String logUserId, int userTypeId,String payTyp) {
        boolean flag = false;
        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        tenderCommonService.setLogUserId(logUserId);
        List<SPTenderCommonData> lstPayerInfo =
                tenderCommonService.returndata("getEmailIdfromUserId", requestToUserId, null);
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peName = "";
            List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(logUserId));
            if (peNameObj.get(0) != null) {
                peName = peNameObj.get(0).toString();
            }
            String strFrom = commonservice.getEmailId(logUserId);
            List<Object[]> AcEmailId = accPaymentService.getAccountantEmailId(Integer.parseInt(tenderId));
            String[] strTo = null;
            int emailIndex = 0;
            if (userTypeId == 3) {
                strTo = new String[AcEmailId.size() + 2];
                for (Object[] emailId : AcEmailId) {
                    strTo[emailIndex] = emailId[4].toString();
                    emailIndex++;
                }
                strTo[emailIndex] = obj[9].toString();
                strTo[emailIndex + 1] = lstPayerInfo.get(0).getFieldName1();
            }
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            sendMessageUtil.setEmailFrom(strFrom);

            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String emailId[] = new String[1];
            String mailSubject = "";
            String mobileno = "";
            String strfor = "Performance Security";
            String strtyp = "Compensate of Performance Security";
            if("ts".equalsIgnoreCase(payTyp)){
                strfor = "Bid Security";
                strtyp = "Forfiet of Bid Security";
            }
            for (int i = 0; i < strTo.length; i++) {
                emailId[0] = strTo[i];
                if ("3".equalsIgnoreCase(accPaymentService.getUserTypeId(emailId[0]))) {
                    mailSubject = "e-GP: "+strtyp+" or New "+strfor+"";
                } else if ("7".equalsIgnoreCase(strFrom)) {
                    mailSubject = "e-GP: Request to "+strtyp+" or New "+strfor+"";
                } else {
                    mailSubject = "e-GP: "+strtyp+" or New "+strfor+"";
                }
                if("ts".equalsIgnoreCase(payTyp)){
                mailSubject = "e-GP: Forfiet of Bid Security";
                }
                registerService.contentAdmMsgBox(emailId[0], strFrom, mailSubject, msgBoxContentUtility.mailForForfeitRequest(c_isPhysicalPrComplete, peName, obj, accPaymentService.getUserTypeId(emailId[0]), Integer.toString(userTypeId), accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])),strfor,strtyp));
                String mailText = mailContentUtility.mailForForfeitRequest(c_isPhysicalPrComplete, peName, obj, accPaymentService.getUserTypeId(emailId[0]), Integer.toString(userTypeId), accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])),strfor,strtyp);
                sendMessageUtil.setEmailTo(emailId);
                sendMessageUtil.setEmailSub(mailSubject);
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                if ("2".equalsIgnoreCase(accPaymentService.getUserTypeId(emailId[0]))) {
                    mobileno = accPaymentService.getMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                } else if ("7".equalsIgnoreCase(accPaymentService.getUserTypeId(emailId[0]))) {
                    mobileno = accPaymentService.getBankUserMobileNowithCountryCode(accPaymentService.getUserId(emailId[0]));
                } else {
                    mobileno = accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                }
                sendMessageUtil.setSmsNo(mobileno);
                StringBuilder sb = new StringBuilder();
                sb.append("Dear User,%0AThis is to inform you that PE has requested to "+strtyp+" ");
                sb.append("for Contract No."+obj[1].toString() +" ("+ obj[15].toString() +")"+" ");
                sb.append("%0AThanks,%0AeGP System");
                sendMessageUtil.setSmsBody(sb.toString());
                if (!"".equalsIgnoreCase(mobileno) && mobileno != null) {
                    sendMessageUtil.sendSMS();
                }
            }
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }

    /*Dohatec Start for Extend validity of performance security*/
        /** send mail notification to tenderer on extending validity of the ps by PE */
    private boolean sendMailForValidityExtendRequest(String tenderId, String lotId, String requestToUserId, String logUserId, int userTypeId,String payTyp) {
        boolean flag = false;
        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        tenderCommonService.setLogUserId(logUserId);
       /* List<SPTenderCommonData> lstPEInfo =
                tenderCommonService.returndata("getTenderPEUserIDName", tenderId, "");*/
         List<Object[]> lstPEInfo = accPaymentService.getPEemailID(Integer.parseInt(tenderId));
         Object[] obj1 = null;
         String PEemail="";
          if (!lstPEInfo.isEmpty()) {
              obj1 = lstPEInfo.get(0);
              PEemail = obj1[0].toString();
          }
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peName = "";
            
            String strFrom = commonservice.getEmailId(logUserId);
            String smsBody = "";
            
            String[] strTo = null;
            int emailIndex = 0;
            if (userTypeId == 3) {
                List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(logUserId));
                if (peNameObj.get(0) != null) {
                peName = peNameObj.get(0).toString();
                }
                strTo = new String[1];
                strTo[emailIndex] = obj[9].toString();
                smsBody = "PA has requested for Extension of ";
            }
            else if(userTypeId == 2) {
                smsBody = "Bidder has accepted request of Validity Extension of ";
                strTo = new String[2];
                strTo[emailIndex] = commonservice.getEmailId(requestToUserId);
                strTo[emailIndex + 1] = PEemail;//commonservice.getEmailId(lstPEInfo.get(0).getFieldName1());
            }
            else if(userTypeId == 7) {
                smsBody = "Bank checker has extended Validity of ";
                List<Object[]> AcEmailId = accPaymentService.getAccountantEmailId(Integer.parseInt(tenderId));
                strTo = new String[AcEmailId.size() + 2];
                for (Object[] emailId : AcEmailId) {
                    strTo[emailIndex] = emailId[4].toString();
                    emailIndex++;
                }
                strTo[emailIndex] = obj[9].toString();
                strTo[emailIndex + 1] =PEemail; //commonservice.getEmailId(lstPEInfo.get(0).getFieldName1());
            }
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            sendMessageUtil.setEmailFrom(strFrom);

            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String emailId[] = new String[1];
            String mailSubject = "";
            String mobileno = "";
            String strfor = "Performance Security";
            if("ts".equalsIgnoreCase(payTyp)){
                strfor = "Bid Security";
            }
            for (int i = 0; i < strTo.length; i++) {
                emailId[0] = strTo[i];
                if ("3".equalsIgnoreCase(accPaymentService.getUserTypeId(emailId[0]))) {
                    mailSubject = "e-GP: Validity Extension of "+strfor;
                } else if ("7".equalsIgnoreCase(accPaymentService.getUserTypeId(strFrom))) {
                    mailSubject = "e-GP: Validity Extended of "+strfor+" ";
                }  else if ("2".equalsIgnoreCase(accPaymentService.getUserTypeId(strFrom))) {
                    mailSubject = "e-GP: Validity extension of "+strfor+" ";
                }  else {
                    mailSubject = "e-GP: Validity extension of "+strfor+ " ";
                }
                registerService.contentAdmMsgBox(emailId[0], strFrom, mailSubject, msgBoxContentUtility.mailForExtendValidity(c_isPhysicalPrComplete, peName, obj, accPaymentService.getUserTypeId(emailId[0]), Integer.toString(userTypeId), accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])),strfor,tenderId));
                String mailText = mailContentUtility.mailForExtendingValidity(c_isPhysicalPrComplete, peName, obj, accPaymentService.getUserTypeId(emailId[0]), Integer.toString(userTypeId), accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])),strfor,tenderId);
                sendMessageUtil.setEmailTo(emailId);
                sendMessageUtil.setEmailSub(mailSubject);
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                if ("2".equalsIgnoreCase(accPaymentService.getUserTypeId(emailId[0]))) {
                    mobileno = accPaymentService.getMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                } else if ("7".equalsIgnoreCase(accPaymentService.getUserTypeId(emailId[0]))) {
                    mobileno = accPaymentService.getBankUserMobileNowithCountryCode(accPaymentService.getUserId(emailId[0]));
                } else {
                    mobileno = accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                }
                sendMessageUtil.setSmsNo(mobileno);
                StringBuilder sb = new StringBuilder();
                sb.append("Dear User,%0AThis is to inform you that ");
                sb.append(smsBody);
                sb.append(strfor);
                sb.append(" for TenderID:"+tenderId +" ");
                sb.append("%0AThanks,%0AeGP System");
                sendMessageUtil.setSmsBody(sb.toString());
                if (!"".equalsIgnoreCase(mobileno) && mobileno != null) {
                    sendMessageUtil.sendSMS();
                }
            }
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }
    /*Dohatec End for Extend validity of performance security*/
}
