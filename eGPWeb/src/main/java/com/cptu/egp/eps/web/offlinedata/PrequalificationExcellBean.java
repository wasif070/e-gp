/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.offlinedata;

import com.cptu.egp.eps.model.table.TblTenderLotPhasingOffline;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class PrequalificationExcellBean {

  private  String    ministryName;
  private  String    agency;
  private  String    procuringEntityName;
  private  String    procuringEntityCode;
  private  String    procuringEntityDistrict;
  private  String    invitation1;
  private  String    procurementNature;
  private  String    invitationFor;
  private  String    invitationRefNo;
  private  String    tenderissuingdate;
  private  String    procurementType;
  private  String    procurementMethod;
  private  String    cbobudget;
  private  String    cbosource;
  private  String    developmentPartner;
  private  String    projectOrProgrammeCode;
  private  String    projectOrProgrammeName;
  private  String    tenderPackageNo;
  private  String    tenderPackageName;
  private  String    prequalificationPublicationDate;
  private  String    PrequalificationClosingDateandTime;
  private  String    sellingPrequalificationDocumentPrincipal;
  private  String    sellingPrequalificationDocumentOthers;
  private  String    receivingPrequalification;
  private  String    place_Date_TimeofPrequalificationMeeting;
  private  String    dateTimeofPrequalificationMeeting;
  private  String    eligibilityofApplicant;
  private  String    briefDescriptionofGoodsorWorks;
  private  String    briefDescriptionofRelatedServices;
  private  String    prequalificationDocumentPrice;
 // private  List<TblTenderLotPhasingOffline>   lotsList;
  private  String    lotNo_1;
  private  String    identificationOfLot_1;
  private  String    location_1;
  private  String    completionTimeInWeeksORmonths_1;
  private  String    lotNo_2;
  private  String    identificationOfLot_2;
  private  String    location_2;
  private  String    completionTimeInWeeksORmonths_2;
  private  String    lotNo_3;
  private  String    identificationOfLot_3;
  private  String    location_3;
  private  String    completionTimeInWeeksORmonths_3;
  private  String    lotNo_4;
  private  String    identificationOfLot_4;
  private  String    location_4;
  private  String    completionTimeInWeeksORmonths_4;
  private  String    nameofOfficialInvitingPrequalification;
  private  String    designationofOfficialInvitingPrequalification;
  private  String    addressofOfficialInvitingPrequalification;
  private  String    contactdetailsofOfficialInvitingPrequalification;
  private  String    faxNo;
  private  String    e_mail;
  private  String    locationofDeliveryWorksConsultancy;
  private  String    istheContractSignedwiththesamepersonstatedintheNOA;
  private  String    wasthePerformanceSecurityprovidedinduetime;
  private  String    wastheContractSignedinduetime;
  private  String    dateofNotificationofAward;
  private  String    dateofContractSigning;
  private  String    proposedDateofContractCompletion;
  private  String    noofTendersProposalsSold;
  private  String    noofTendersProposalsReceived;
  private  String    noofResponsiveTendersProposals;
  private  String    briefDescriptionofContract;
  private  String    contractValue;
  private  String    nameofSupplierContractorConsultant;
  private  String    locationofSupplierContractorConsultant;
  private  String    ifNogivereasonwhy_NOA;
  private  String    ifNoGiveReasonWhy_Performance_Security;
  private  String    ifNoGiveReasonWhy_Contract_signed;

    /**
     * @return the ministryName
     */
    public String getMinistryName() {
        return ministryName;
    }

    /**
     * @param ministryName the ministryName to set
     */
    public void setMinistryName(String ministryName) {
        this.ministryName = ministryName;
    }

   

    /**
     * @return the procuringEntityName
     */
    public String getProcuringEntityName() {
        return procuringEntityName;
    }

    /**
     * @param procuringEntityName the procuringEntityName to set
     */
    public void setProcuringEntityName(String procuringEntityName) {
        this.procuringEntityName = procuringEntityName;
    }

    /**
     * @return the procuringEntityCode
     */
    public String getProcuringEntityCode() {
        return procuringEntityCode;
    }

    /**
     * @param procuringEntityCode the procuringEntityCode to set
     */
    public void setProcuringEntityCode(String procuringEntityCode) {
        this.procuringEntityCode = procuringEntityCode;
    }

    /**
     * @return the procuringEntityDistrict
     */
    public String getProcuringEntityDistrict() {
        return procuringEntityDistrict;
    }

    /**
     * @param procuringEntityDistrict the procuringEntityDistrict to set
     */
    public void setProcuringEntityDistrict(String procuringEntityDistrict) {
        this.procuringEntityDistrict = procuringEntityDistrict;
    }

    /**
     * @return the invitation1
     */
    public String getInvitation1() {
        return invitation1;
    }

    /**
     * @param invitation1 the invitation1 to set
     */
    public void setInvitation1(String invitation1) {
        this.invitation1 = invitation1;
    }

    /**
     * @return the invitationRefNo
     */
    public String getInvitationRefNo() {
        return invitationRefNo;
    }

    /**
     * @param invitationRefNo the invitationRefNo to set
     */
    public void setInvitationRefNo(String invitationRefNo) {
        this.invitationRefNo = invitationRefNo;
    }

    /**
     * @return the tenderissuingdate
     */
    public String getTenderissuingdate() {
        return tenderissuingdate;
    }

    /**
     * @param tenderissuingdate the tenderissuingdate to set
     */
    public void setTenderissuingdate(String tenderissuingdate) {
        this.tenderissuingdate = tenderissuingdate;
    }

  
    /**
     * @return the cbobudget
     */
    public String getCbobudget() {
        return cbobudget;
    }

    /**
     * @param cbobudget the cbobudget to set
     */
    public void setCbobudget(String cbobudget) {
        this.cbobudget = cbobudget;
    }

    /**
     * @return the cbosource
     */
    public String getCbosource() {
        return cbosource;
    }

    /**
     * @param cbosource the cbosource to set
     */
    public void setCbosource(String cbosource) {
        this.cbosource = cbosource;
    }

    /**
     * @return the developmentPartner
     */
    public String getDevelopmentPartner() {
        return developmentPartner;
    }

    /**
     * @param developmentPartner the developmentPartner to set
     */
    public void setDevelopmentPartner(String developmentPartner) {
        this.developmentPartner = developmentPartner;
    }

    /**
     * @return the projectOrProgrammeCode
     */
    public String getProjectOrProgrammeCode() {
        return projectOrProgrammeCode;
    }

    /**
     * @param projectOrProgrammeCode the projectOrProgrammeCode to set
     */
    public void setProjectOrProgrammeCode(String projectOrProgrammeCode) {
        this.projectOrProgrammeCode = projectOrProgrammeCode;
    }

    /**
     * @return the projectOrProgrammeName
     */
    public String getProjectOrProgrammeName() {
        return projectOrProgrammeName;
    }

    /**
     * @param projectOrProgrammeName the projectOrProgrammeName to set
     */
    public void setProjectOrProgrammeName(String projectOrProgrammeName) {
        this.projectOrProgrammeName = projectOrProgrammeName;
    }

    /**
     * @return the tenderPackageNo
     */
    public String getTenderPackageNo() {
        return tenderPackageNo;
    }

    /**
     * @param tenderPackageNo the tenderPackageNo to set
     */
    public void setTenderPackageNo(String tenderPackageNo) {
        this.tenderPackageNo = tenderPackageNo;
    }

    /**
     * @return the tenderPackageName
     */
    public String getTenderPackageName() {
        return tenderPackageName;
    }

    /**
     * @param tenderPackageName the tenderPackageName to set
     */
    public void setTenderPackageName(String tenderPackageName) {
        this.tenderPackageName = tenderPackageName;
    }

    /**
     * @return the prequalificationPublicationDate
     */
    public String getPrequalificationPublicationDate() {
        return prequalificationPublicationDate;
    }

    /**
     * @param prequalificationPublicationDate the prequalificationPublicationDate to set
     */
    public void setPrequalificationPublicationDate(String prequalificationPublicationDate) {
        this.prequalificationPublicationDate = prequalificationPublicationDate;
    }

    /**
     * @return the PrequalificationClosingDateandTime
     */
    public String getPrequalificationClosingDateandTime() {
        return PrequalificationClosingDateandTime;
    }

    /**
     * @param PrequalificationClosingDateandTime the PrequalificationClosingDateandTime to set
     */
    public void setPrequalificationClosingDateandTime(String PrequalificationClosingDateandTime) {
        this.PrequalificationClosingDateandTime = PrequalificationClosingDateandTime;
    }

    /**
     * @return the sellingPrequalificationDocumentPrincipal
     */
    public String getSellingPrequalificationDocumentPrincipal() {
        return sellingPrequalificationDocumentPrincipal;
    }

    /**
     * @param sellingPrequalificationDocumentPrincipal the sellingPrequalificationDocumentPrincipal to set
     */
    public void setSellingPrequalificationDocumentPrincipal(String sellingPrequalificationDocumentPrincipal) {
        this.sellingPrequalificationDocumentPrincipal = sellingPrequalificationDocumentPrincipal;
    }

    /**
     * @return the sellingPrequalificationDocumentOthers
     */
    public String getSellingPrequalificationDocumentOthers() {
        return sellingPrequalificationDocumentOthers;
    }

    /**
     * @param sellingPrequalificationDocumentOthers the sellingPrequalificationDocumentOthers to set
     */
    public void setSellingPrequalificationDocumentOthers(String sellingPrequalificationDocumentOthers) {
        this.sellingPrequalificationDocumentOthers = sellingPrequalificationDocumentOthers;
    }

    /**
     * @return the receivingPrequalification
     */
    public String getReceivingPrequalification() {
        return receivingPrequalification;
    }

    /**
     * @param receivingPrequalification the receivingPrequalification to set
     */
    public void setReceivingPrequalification(String receivingPrequalification) {
        this.receivingPrequalification = receivingPrequalification;
    }

    /**
     * @return the place_Date_TimeofPrequalificationMeeting
     */
    public String getPlace_Date_TimeofPrequalificationMeeting() {
        return place_Date_TimeofPrequalificationMeeting;
    }

    /**
     * @param place_Date_TimeofPrequalificationMeeting the place_Date_TimeofPrequalificationMeeting to set
     */
    public void setPlace_Date_TimeofPrequalificationMeeting(String place_Date_TimeofPrequalificationMeeting) {
        this.place_Date_TimeofPrequalificationMeeting = place_Date_TimeofPrequalificationMeeting;
    }

    /**
     * @return the dateTimeofPrequalificationMeeting
     */
    public String getDateTimeofPrequalificationMeeting() {
        return dateTimeofPrequalificationMeeting;
    }

    /**
     * @param dateTimeofPrequalificationMeeting the dateTimeofPrequalificationMeeting to set
     */
    public void setDateTimeofPrequalificationMeeting(String dateTimeofPrequalificationMeeting) {
        this.dateTimeofPrequalificationMeeting = dateTimeofPrequalificationMeeting;
    }

    /**
     * @return the eligibilityofApplicant
     */
    public String getEligibilityofApplicant() {
        return eligibilityofApplicant;
    }

    /**
     * @param eligibilityofApplicant the eligibilityofApplicant to set
     */
    public void setEligibilityofApplicant(String eligibilityofApplicant) {
        this.eligibilityofApplicant = eligibilityofApplicant;
    }

    /**
     * @return the briefDescriptionofGoodsorWorks
     */
    public String getBriefDescriptionofGoodsorWorks() {
        return briefDescriptionofGoodsorWorks;
    }

    /**
     * @param briefDescriptionofGoodsorWorks the briefDescriptionofGoodsorWorks to set
     */
    public void setBriefDescriptionofGoodsorWorks(String briefDescriptionofGoodsorWorks) {
        this.briefDescriptionofGoodsorWorks = briefDescriptionofGoodsorWorks;
    }

    /**
     * @return the briefDescriptionofRelatedServices
     */
    public String getBriefDescriptionofRelatedServices() {
        return briefDescriptionofRelatedServices;
    }

    /**
     * @param briefDescriptionofRelatedServices the briefDescriptionofRelatedServices to set
     */
    public void setBriefDescriptionofRelatedServices(String briefDescriptionofRelatedServices) {
        this.briefDescriptionofRelatedServices = briefDescriptionofRelatedServices;
    }

    /**
     * @return the prequalificationDocumentPrice
     */
    public String getPrequalificationDocumentPrice() {
        return prequalificationDocumentPrice;
    }

    /**
     * @param prequalificationDocumentPrice the prequalificationDocumentPrice to set
     */
    public void setPrequalificationDocumentPrice(String prequalificationDocumentPrice) {
        this.prequalificationDocumentPrice = prequalificationDocumentPrice;
    }

    /**
     * @return the nameofOfficialInvitingPrequalification
     */
    public String getNameofOfficialInvitingPrequalification() {
        return nameofOfficialInvitingPrequalification;
    }

    /**
     * @param nameofOfficialInvitingPrequalification the nameofOfficialInvitingPrequalification to set
     */
    public void setNameofOfficialInvitingPrequalification(String nameofOfficialInvitingPrequalification) {
        this.nameofOfficialInvitingPrequalification = nameofOfficialInvitingPrequalification;
    }

    /**
     * @return the designationofOfficialInvitingPrequalification
     */
    public String getDesignationofOfficialInvitingPrequalification() {
        return designationofOfficialInvitingPrequalification;
    }

    /**
     * @param designationofOfficialInvitingPrequalification the designationofOfficialInvitingPrequalification to set
     */
    public void setDesignationofOfficialInvitingPrequalification(String designationofOfficialInvitingPrequalification) {
        this.designationofOfficialInvitingPrequalification = designationofOfficialInvitingPrequalification;
    }

    /**
     * @return the addressofOfficialInvitingPrequalification
     */
    public String getAddressofOfficialInvitingPrequalification() {
        return addressofOfficialInvitingPrequalification;
    }

    /**
     * @param addressofOfficialInvitingPrequalification the addressofOfficialInvitingPrequalification to set
     */
    public void setAddressofOfficialInvitingPrequalification(String addressofOfficialInvitingPrequalification) {
        this.addressofOfficialInvitingPrequalification = addressofOfficialInvitingPrequalification;
    }

    /**
     * @return the contactdetailsofOfficialInvitingPrequalification
     */
    public String getContactdetailsofOfficialInvitingPrequalification() {
        return contactdetailsofOfficialInvitingPrequalification;
    }

    /**
     * @param contactdetailsofOfficialInvitingPrequalification the contactdetailsofOfficialInvitingPrequalification to set
     */
    public void setContactdetailsofOfficialInvitingPrequalification(String contactdetailsofOfficialInvitingPrequalification) {
        this.contactdetailsofOfficialInvitingPrequalification = contactdetailsofOfficialInvitingPrequalification;
    }

    /**
     * @return the faxNo
     */
    public String getFaxNo() {
        return faxNo;
    }

    /**
     * @param faxNo the faxNo to set
     */
    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    /**
     * @return the e_mail
     */
    public String getE_mail() {
        return e_mail;
    }

    /**
     * @param e_mail the e_mail to set
     */
    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }

    /**
     * @return the locationofDeliveryWorksConsultancy
     */
    public String getLocationofDeliveryWorksConsultancy() {
        return locationofDeliveryWorksConsultancy;
    }

    /**
     * @param locationofDeliveryWorksConsultancy the locationofDeliveryWorksConsultancy to set
     */
    public void setLocationofDeliveryWorksConsultancy(String locationofDeliveryWorksConsultancy) {
        this.locationofDeliveryWorksConsultancy = locationofDeliveryWorksConsultancy;
    }

    /**
     * @return the istheContractSignedwiththesamepersonstatedintheNOA
     */
    public String getIstheContractSignedwiththesamepersonstatedintheNOA() {
        return istheContractSignedwiththesamepersonstatedintheNOA;
    }

    /**
     * @param istheContractSignedwiththesamepersonstatedintheNOA the istheContractSignedwiththesamepersonstatedintheNOA to set
     */
    public void setIstheContractSignedwiththesamepersonstatedintheNOA(String istheContractSignedwiththesamepersonstatedintheNOA) {
        this.istheContractSignedwiththesamepersonstatedintheNOA = istheContractSignedwiththesamepersonstatedintheNOA;
    }

    /**
     * @return the wasthePerformanceSecurityprovidedinduetime
     */
    public String getWasthePerformanceSecurityprovidedinduetime() {
        return wasthePerformanceSecurityprovidedinduetime;
    }

    /**
     * @param wasthePerformanceSecurityprovidedinduetime the wasthePerformanceSecurityprovidedinduetime to set
     */
    public void setWasthePerformanceSecurityprovidedinduetime(String wasthePerformanceSecurityprovidedinduetime) {
        this.wasthePerformanceSecurityprovidedinduetime = wasthePerformanceSecurityprovidedinduetime;
    }

    /**
     * @return the wastheContractSignedinduetime
     */
    public String getWastheContractSignedinduetime() {
        return wastheContractSignedinduetime;
    }

    /**
     * @param wastheContractSignedinduetime the wastheContractSignedinduetime to set
     */
    public void setWastheContractSignedinduetime(String wastheContractSignedinduetime) {
        this.wastheContractSignedinduetime = wastheContractSignedinduetime;
    }

    /**
     * @return the dateofNotificationofAward
     */
    public String getDateofNotificationofAward() {
        return dateofNotificationofAward;
    }

    /**
     * @param dateofNotificationofAward the dateofNotificationofAward to set
     */
    public void setDateofNotificationofAward(String dateofNotificationofAward) {
        this.dateofNotificationofAward = dateofNotificationofAward;
    }

    /**
     * @return the dateofContractSigning
     */
    public String getDateofContractSigning() {
        return dateofContractSigning;
    }

    /**
     * @param dateofContractSigning the dateofContractSigning to set
     */
    public void setDateofContractSigning(String dateofContractSigning) {
        this.dateofContractSigning = dateofContractSigning;
    }

    /**
     * @return the proposedDateofContractCompletion
     */
    public String getProposedDateofContractCompletion() {
        return proposedDateofContractCompletion;
    }

    /**
     * @param proposedDateofContractCompletion the proposedDateofContractCompletion to set
     */
    public void setProposedDateofContractCompletion(String proposedDateofContractCompletion) {
        this.proposedDateofContractCompletion = proposedDateofContractCompletion;
    }

    /**
     * @return the noofTendersProposalsSold
     */
    public String getNoofTendersProposalsSold() {
        return noofTendersProposalsSold;
    }

    /**
     * @param noofTendersProposalsSold the noofTendersProposalsSold to set
     */
    public void setNoofTendersProposalsSold(String noofTendersProposalsSold) {
        this.noofTendersProposalsSold = noofTendersProposalsSold;
    }

    /**
     * @return the noofTendersProposalsReceived
     */
    public String getNoofTendersProposalsReceived() {
        return noofTendersProposalsReceived;
    }

    /**
     * @param noofTendersProposalsReceived the noofTendersProposalsReceived to set
     */
    public void setNoofTendersProposalsReceived(String noofTendersProposalsReceived) {
        this.noofTendersProposalsReceived = noofTendersProposalsReceived;
    }

    /**
     * @return the noofResponsiveTendersProposals
     */
    public String getNoofResponsiveTendersProposals() {
        return noofResponsiveTendersProposals;
    }

    /**
     * @param noofResponsiveTendersProposals the noofResponsiveTendersProposals to set
     */
    public void setNoofResponsiveTendersProposals(String noofResponsiveTendersProposals) {
        this.noofResponsiveTendersProposals = noofResponsiveTendersProposals;
    }

    /**
     * @return the briefDescriptionofContract
     */
    public String getBriefDescriptionofContract() {
        return briefDescriptionofContract;
    }

    /**
     * @param briefDescriptionofContract the briefDescriptionofContract to set
     */
    public void setBriefDescriptionofContract(String briefDescriptionofContract) {
        this.briefDescriptionofContract = briefDescriptionofContract;
    }

    /**
     * @return the contractValue
     */
    public String getContractValue() {
        return contractValue;
    }

    /**
     * @param contractValue the contractValue to set
     */
    public void setContractValue(String contractValue) {
        this.contractValue = contractValue;
    }

    /**
     * @return the nameofSupplierContractorConsultant
     */
    public String getNameofSupplierContractorConsultant() {
        return nameofSupplierContractorConsultant;
    }

    /**
     * @param nameofSupplierContractorConsultant the nameofSupplierContractorConsultant to set
     */
    public void setNameofSupplierContractorConsultant(String nameofSupplierContractorConsultant) {
        this.nameofSupplierContractorConsultant = nameofSupplierContractorConsultant;
    }

    /**
     * @return the locationofSupplierContractorConsultant
     */
    public String getLocationofSupplierContractorConsultant() {
        return locationofSupplierContractorConsultant;
    }

    /**
     * @param locationofSupplierContractorConsultant the locationofSupplierContractorConsultant to set
     */
    public void setLocationofSupplierContractorConsultant(String locationofSupplierContractorConsultant) {
        this.locationofSupplierContractorConsultant = locationofSupplierContractorConsultant;
    }

    /**
     * @return the ifNogivereasonwhy_NOA
     */
    public String getIfNogivereasonwhy_NOA() {
        return ifNogivereasonwhy_NOA;
    }

    /**
     * @param ifNogivereasonwhy_NOA the ifNogivereasonwhy_NOA to set
     */
    public void setIfNogivereasonwhy_NOA(String ifNogivereasonwhy_NOA) {
        this.ifNogivereasonwhy_NOA = ifNogivereasonwhy_NOA;
    }

    /**
     * @return the ifNoGiveReasonWhy_Performance_Security
     */
    public String getIfNoGiveReasonWhy_Performance_Security() {
        return ifNoGiveReasonWhy_Performance_Security;
    }

    /**
     * @param ifNoGiveReasonWhy_Performance_Security the ifNoGiveReasonWhy_Performance_Security to set
     */
    public void setIfNoGiveReasonWhy_Performance_Security(String ifNoGiveReasonWhy_Performance_Security) {
        this.ifNoGiveReasonWhy_Performance_Security = ifNoGiveReasonWhy_Performance_Security;
    }

    /**
     * @return the ifNoGiveReasonWhy_Contract_signed
     */
    public String getIfNoGiveReasonWhy_Contract_signed() {
        return ifNoGiveReasonWhy_Contract_signed;
    }

    /**
     * @param ifNoGiveReasonWhy_Contract_signed the ifNoGiveReasonWhy_Contract_signed to set
     */
    public void setIfNoGiveReasonWhy_Contract_signed(String ifNoGiveReasonWhy_Contract_signed) {
        this.ifNoGiveReasonWhy_Contract_signed = ifNoGiveReasonWhy_Contract_signed;
    }

    /**
     * @return the lotNo_1
     */
    public String getLotNo_1() {
        return lotNo_1;
    }

    /**
     * @param lotNo_1 the lotNo_1 to set
     */
    public void setLotNo_1(String lotNo_1) {
        this.lotNo_1 = lotNo_1;
    }

    /**
     * @return the identificationOfLot_1
     */
    public String getIdentificationOfLot_1() {
        return identificationOfLot_1;
    }

    /**
     * @param identificationOfLot_1 the identificationOfLot_1 to set
     */
    public void setIdentificationOfLot_1(String identificationOfLot_1) {
        this.identificationOfLot_1 = identificationOfLot_1;
    }

    /**
     * @return the location_1
     */
    public String getLocation_1() {
        return location_1;
    }

    /**
     * @param location_1 the location_1 to set
     */
    public void setLocation_1(String location_1) {
        this.location_1 = location_1;
    }

    /**
     * @return the completionTimeInWeeksORmonths_1
     */
    public String getCompletionTimeInWeeksORmonths_1() {
        return completionTimeInWeeksORmonths_1;
    }

    /**
     * @param completionTimeInWeeksORmonths_1 the completionTimeInWeeksORmonths_1 to set
     */
    public void setCompletionTimeInWeeksORmonths_1(String completionTimeInWeeksORmonths_1) {
        this.completionTimeInWeeksORmonths_1 = completionTimeInWeeksORmonths_1;
    }

    /**
     * @return the lotNo_2
     */
    public String getLotNo_2() {
        return lotNo_2;
    }

    /**
     * @param lotNo_2 the lotNo_2 to set
     */
    public void setLotNo_2(String lotNo_2) {
        this.lotNo_2 = lotNo_2;
    }

    /**
     * @return the identificationOfLot_2
     */
    public String getIdentificationOfLot_2() {
        return identificationOfLot_2;
    }

    /**
     * @param identificationOfLot_2 the identificationOfLot_2 to set
     */
    public void setIdentificationOfLot_2(String identificationOfLot_2) {
        this.identificationOfLot_2 = identificationOfLot_2;
    }

    /**
     * @return the location_2
     */
    public String getLocation_2() {
        return location_2;
    }

    /**
     * @param location_2 the location_2 to set
     */
    public void setLocation_2(String location_2) {
        this.location_2 = location_2;
    }

    /**
     * @return the completionTimeInWeeksORmonths_2
     */
    public String getCompletionTimeInWeeksORmonths_2() {
        return completionTimeInWeeksORmonths_2;
    }

    /**
     * @param completionTimeInWeeksORmonths_2 the completionTimeInWeeksORmonths_2 to set
     */
    public void setCompletionTimeInWeeksORmonths_2(String completionTimeInWeeksORmonths_2) {
        this.completionTimeInWeeksORmonths_2 = completionTimeInWeeksORmonths_2;
    }

    /**
     * @return the lotNo_3
     */
    public String getLotNo_3() {
        return lotNo_3;
    }

    /**
     * @param lotNo_3 the lotNo_3 to set
     */
    public void setLotNo_3(String lotNo_3) {
        this.lotNo_3 = lotNo_3;
    }

    /**
     * @return the identificationOfLot_3
     */
    public String getIdentificationOfLot_3() {
        return identificationOfLot_3;
    }

    /**
     * @param identificationOfLot_3 the identificationOfLot_3 to set
     */
    public void setIdentificationOfLot_3(String identificationOfLot_3) {
        this.identificationOfLot_3 = identificationOfLot_3;
    }

    /**
     * @return the location_3
     */
    public String getLocation_3() {
        return location_3;
    }

    /**
     * @param location_3 the location_3 to set
     */
    public void setLocation_3(String location_3) {
        this.location_3 = location_3;
    }

    /**
     * @return the completionTimeInWeeksORmonths_3
     */
    public String getCompletionTimeInWeeksORmonths_3() {
        return completionTimeInWeeksORmonths_3;
    }

    /**
     * @param completionTimeInWeeksORmonths_3 the completionTimeInWeeksORmonths_3 to set
     */
    public void setCompletionTimeInWeeksORmonths_3(String completionTimeInWeeksORmonths_3) {
        this.completionTimeInWeeksORmonths_3 = completionTimeInWeeksORmonths_3;
    }

    /**
     * @return the lotNo_4
     */
    public String getLotNo_4() {
        return lotNo_4;
    }

    /**
     * @param lotNo_4 the lotNo_4 to set
     */
    public void setLotNo_4(String lotNo_4) {
        this.lotNo_4 = lotNo_4;
    }

    /**
     * @return the identificationOfLot_4
     */
    public String getIdentificationOfLot_4() {
        return identificationOfLot_4;
    }

    /**
     * @param identificationOfLot_4 the identificationOfLot_4 to set
     */
    public void setIdentificationOfLot_4(String identificationOfLot_4) {
        this.identificationOfLot_4 = identificationOfLot_4;
    }

    /**
     * @return the location_4
     */
    public String getLocation_4() {
        return location_4;
    }

    /**
     * @param location_4 the location_4 to set
     */
    public void setLocation_4(String location_4) {
        this.location_4 = location_4;
    }

    /**
     * @return the completionTimeInWeeksORmonths_4
     */
    public String getCompletionTimeInWeeksORmonths_4() {
        return completionTimeInWeeksORmonths_4;
    }

    /**
     * @param completionTimeInWeeksORmonths_4 the completionTimeInWeeksORmonths_4 to set
     */
    public void setCompletionTimeInWeeksORmonths_4(String completionTimeInWeeksORmonths_4) {
        this.completionTimeInWeeksORmonths_4 = completionTimeInWeeksORmonths_4;
    }

    /**
     * @return the agency
     */
    public String getAgency() {
        return agency;
    }

    /**
     * @param agency the agency to set
     */
    public void setAgency(String agency) {
        this.agency = agency;
    }

    /**
     * @return the procurementNature
     */
    public String getProcurementNature() {
        return procurementNature;
    }

    /**
     * @param procurementNature the procurementNature to set
     */
    public void setProcurementNature(String procurementNature) {
        this.procurementNature = procurementNature;
    }

    /**
     * @return the invitationFor
     */
    public String getInvitationFor() {
        return invitationFor;
    }

    /**
     * @param invitationFor the invitationFor to set
     */
    public void setInvitationFor(String invitationFor) {
        this.invitationFor = invitationFor;
    }

    /**
     * @return the procurementType
     */
    public String getProcurementType() {
        return procurementType;
    }

    /**
     * @param procurementType the procurementType to set
     */
    public void setProcurementType(String procurementType) {
        this.procurementType = procurementType;
    }

    /**
     * @return the procurementMethod
     */
    public String getProcurementMethod() {
        return procurementMethod;
    }

    /**
     * @param procurementMethod the procurementMethod to set
     */
    public void setProcurementMethod(String procurementMethod) {
        this.procurementMethod = procurementMethod;
    }

  

}
