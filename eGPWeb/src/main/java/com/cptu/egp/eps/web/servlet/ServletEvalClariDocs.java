/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CheckExtension;
import com.cptu.egp.eps.web.utility.FileEncryptDecryptUtil;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *This file used when uploading evaluation file upload,download and deleting file
 * insert in to tbl_EvalClariDocs
 * @author Administrator
 */
public class ServletEvalClariDocs extends HttpServlet {

    private static final String TMP_DIR_PATH = "c:\\tmp";
    private File tmpDir;
    private static final String DESTINATION_DIR_PATH_OFFICER = FilePathUtility.getFilePath().get("ServletEvalClariDocsOfficer");
    private static final String DESTINATION_DIR_PATH_VENDOR = FilePathUtility.getFilePath().get("ServletEvalClariDocsVendor");
    private File destinationDir;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        tmpDir = new File(TMP_DIR_PATH);
        if (!tmpDir.isDirectory()) {
            tmpDir.mkdir();
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String formId = "";
        int tenderId = 0;
        int evalCount=0;
        String docType = "";
        File file = null;
        String docSizeMsg = "";
        boolean checkret = false;
        boolean flag = false;
        String err = "";
        String documentBrief = "";
        String fileName = "";
        long fileSize = 0;
        String queryString = "";
        String pageName = "resources/common/evalClariDoc.jsp";
        response.setContentType("text/html");
        boolean dis = false;
        String pageId = "office";
        String uId = "";
        String st = "";
        String userTypeId = session.getAttribute("userTypeId").toString();
        String userId = session.getAttribute("userId") + "";
        if (session.getAttribute("userId") == null) {
            response.sendRedirect("SessionTimedOut.jsp");
        } else {

            try {
                try {
                    if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("remove")) {

                        int docId = 0;
                        String id = "";
                        String docName = "";

                        if (request.getParameter("docId") != null) {
                            docId = Integer.parseInt(request.getParameter("docId"));

                        }
                        if (request.getParameter("tenderId") != null) {
                            tenderId = Integer.parseInt(request.getParameter("tenderId"));

                        }
                        if (request.getParameter("formId") != null) {
                            formId = request.getParameter("formId");

                        }
                        if (request.getParameter("docName") != null) {
                            docName = request.getParameter("docName");
                        }
                        if (request.getParameter("uId") != null) {
                            uId = request.getParameter("uId");
                        }if (request.getParameter("st") != null) {
                            st = request.getParameter("st");
                        }
                        if (request.getParameter("evalCount") != null) {
                            evalCount = Integer.parseInt(request.getParameter("evalCount"));
                        }
                        String whereContition = "";
                        whereContition = "evalClrDocId= " + docId;
                        if (request.getParameter("docType").equalsIgnoreCase("officer")) {
                            fileName = DESTINATION_DIR_PATH_OFFICER + request.getParameter("fs") + "\\" + request.getParameter("docName");
                        } else {
                            fileName = DESTINATION_DIR_PATH_VENDOR + request.getParameter("fs") + "\\" + request.getParameter("docName");
                        }
                        checkret = deleteFile(fileName);
                        if (checkret) {
                            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                            CommonMsgChk commonMsgChk;
                            commonMsgChk = commonXMLSPService.insertDataBySP("delete", "tbl_EvalClariDocs", "", whereContition).get(0);
                            if ((request.getParameter("pId") != null) && request.getParameter("pId").equals("ef")) {
                                pageName = "tenderer/EvalFormQuestions.jsp";
                                queryString = "?formId=" + formId + "&tenderId=" + tenderId + "&lotId=" + request.getParameter("lotId");
                            }else{
                            queryString = "?formId=" + formId + "&tenderId=" + tenderId + "&fq=Removed" + "&pId=" + pageId + "&uId=" + uId+"&st="+st;
                            }
                            response.sendRedirect(pageName + queryString);
                        }

                    } else if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("download")) {
                        if (request.getParameter("docType").equalsIgnoreCase("officer")) {
                            file = new File(DESTINATION_DIR_PATH_OFFICER + request.getParameter("fs") + "\\" + request.getParameter("docName"));
                        } else {
                            file = new File(DESTINATION_DIR_PATH_VENDOR + request.getParameter("fs") + "\\" + request.getParameter("docName"));
                        }
                        InputStream fis = new FileInputStream(file);
                        byte[] buf = new byte[Integer.valueOf(request.getParameter("docSize"))];
                        int offset = 0;
                        int numRead = 0;
                        while ((offset < buf.length)
                                && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {

                            offset += numRead;

                        }
                        fis.close();
                        FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                        buf = fileEncryptDecryptUtil.fileDecryptUtil(buf);
                        response.setContentType("application/octet-stream");
                        response.setHeader("Content-Disposition", "attachment;filename=\"" + request.getParameter("docName") + "\"");
                        ServletOutputStream outputStream = response.getOutputStream();
                        outputStream.write(buf);
                        outputStream.flush();
                        outputStream.close();
                    } else {
                        DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
                        /*
                         *Set the size threshold, above which content will be stored on disk.
                         */
                        fileItemFactory.setSizeThreshold(4 * 1024 * 1024); //1 MB
		/*
                         * Set the temporary directory to store the uploaded files of size above threshold.
                         */
                        fileItemFactory.setRepository(tmpDir);

                        ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
                        /*
                         * Parse the request
                         */
                        List items = uploadHandler.parseRequest(request);
                        Iterator itr = items.iterator();
                        //For Supporting Document
                        while (itr.hasNext()) {
                            FileItem item = (FileItem) itr.next();
                            //For Supporting Document
                    /*
                             * Handle Form Fields.
                             */
                            if (item.isFormField()) {

                                if (item.getFieldName().equals("documentBrief")) {
                                    if (item.getString() == null || item.getString().trim().length() == 0) {
                                        dis = true;
                                        break;
                                    }

                                    documentBrief = item.getString();

                                } else if (item.getFieldName().equals("tenderId")) {
                                    tenderId = Integer.parseInt(item.getString());

                                } else if (item.getFieldName().equals("formId")) {
                                    formId = item.getString();

                                } else if (item.getFieldName().equals("uId")) {
                                    uId = item.getString();
                                } else if (item.getFieldName().equals("st")) {
                                    st = item.getString();
                                } else if (item.getFieldName().equals("evalCount")) {
                                    evalCount = Integer.parseInt(item.getString());
                                }
                            } else {
                                if (item.getName().lastIndexOf("\\") != -1) {
                                    fileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1, item.getName().length());
                                } else {
                                    fileName = item.getName();
                                }
                                //fileName  = fileName.replaceAll(" ", "");
                                String realPath = "";
                                if (userTypeId.equals("2")) {
                                    docType = "Tenderer";
                                    pageId = "bid";
                                    uId = userId;
                                    realPath = DESTINATION_DIR_PATH_VENDOR + userId + "_" + formId;
                                } else {
                                    docType = "Officer";
                                    realPath = DESTINATION_DIR_PATH_OFFICER + userId + "_" + uId + "_" + formId;
                                }
                                destinationDir = new File(realPath);
                                if (!destinationDir.isDirectory()) {
                                    destinationDir.mkdir();
                                }
                                docSizeMsg = docSizeMsg(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
                                // docSizeMsg = docSizeMsg(1);//userID.
                                if (!docSizeMsg.equals("ok")) {
                                    //response.sendRedirect("SupportingDocuments.jsp?fq="+docSizeMsg);
                                } else {
                                    fileSize = item.getSize();
                                    err = checkExnAndSize(fileName, item.getSize(), "common");

                                    if (!err.equals("")) {
                                        break;
                                    } else {
                                        file = new File(destinationDir, fileName);
                                        if (file.isFile()) {
                                            flag = true;
                                            break;

                                        }
                                        item.write(file);
                                        FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                                        fileSize = fileEncryptDecryptUtil.fileEncryptUtil(file, (int)fileSize);
                                    }
                                }
                            }

                        }
                    }

                } catch (FileUploadException ex) {
                    log("Error encountered while parsing the request", ex);
                } catch (Exception ex) {
                    log("Error encountered while uploading file", ex);
                }


                if (!docSizeMsg.equals("ok")) {
                    queryString = "?fq=" + docSizeMsg;

                } else {
                    if (dis) {
                        docSizeMsg = "Please Enter Discription";
                        queryString = "?fq=" + docSizeMsg + "&formId=" + formId + "&tenderId=" + tenderId + "&pId=" + pageId + "&uId=" + uId+"&st="+st;

                        response.sendRedirect(pageName + queryString);
                    } else {
                        if (flag) {
                            docSizeMsg = "File already Exists";
                            queryString = "?fq=" + docSizeMsg + "&formId=" + formId + "&tenderId=" + tenderId + "&pId=" + pageId + "&uId=" + uId+"&st="+st;
                            response.sendRedirect(pageName + queryString);
                        } else {

                            if (!err.equals("")) {
                                if (err.equals("Exn")) {
                                    CheckExtension ext = new CheckExtension();
                                    TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("officer");
                                    queryString = "?ft=" + configurationMaster.getAllowedExtension() + "&formId=" + formId + "&tenderId=" + tenderId + "&pId=" + pageId + "&uId=" + uId+"&st="+st;
                                    response.sendRedirect(pageName + queryString);
                                } else {
                                    CheckExtension ext = new CheckExtension();
                                    TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("officer");
                                    queryString = "?ft=" + configurationMaster.getAllowedExtension() + "&formId=" + formId + "&tenderId=" + tenderId + "&pId=" + pageId + "&uId=" + uId;
                                    response.sendRedirect(pageName + queryString);
                                }

                            } else {
                                if (documentBrief != null && documentBrief != "") {
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                                    documentBrief = handleSpecialChar.handleSpecialChar(documentBrief);
                                    String xmldata = "";
                                    xmldata = "<tbl_EvalClariDocs tenderId=\"" + tenderId + "\" userId=\"" + uId + "\" formId=\"" + formId + "\" docType=\"" + docType + "\" documentName=\"" + fileName + "\" docDescription=\"" + documentBrief + "\" uploadedBy=\"" + userId + "\" uploadedDt=\"" + format.format(new Date()) + "\" docSize=\"" + fileSize + "\" evalCount=\"" + evalCount + "\" />";
                                    xmldata = "<root>" + xmldata + "</root>";
                                    CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                    CommonMsgChk commonMsgChk;
                                    try {
                                        commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_EvalClariDocs", xmldata, "").get(0);
                                        queryString = "?formId=" + formId + "&tenderId=" + tenderId + "&fq=Uploaded&pId=" + pageId + "&uId=" + uId+"&st="+st;
                                        response.sendRedirect(pageName + queryString);
                                        if (commonMsgChk != null || handleSpecialChar != null) {
                                            commonMsgChk = null;
                                            handleSpecialChar = null;
                                        }
                                    } catch (Exception ex) {
                                        Logger.getLogger(ServletEvalClariDocs.class.getName()).log(Level.SEVERE, null, ex);
                                    }

                                }
                            }

                        }
                    }
                }
            } finally {
                //   out.close();
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
/*for extesion and docsize message*/
    public String checkExnAndSize(String extn, long size, String userType) {
        String chextn = "Exn";
        float fsize = 0.0f;
        float dsize = 0.0f;
        int j = extn.lastIndexOf('.');
        String lst = extn.substring(j + 1);
        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        String str = configurationMaster.getAllowedExtension();
        String[] str1 = str.split(",");
        for (int i = 0; i < str1.length; i++) {
            if (str1[i].trim().equalsIgnoreCase(lst)) {
                chextn = "";
            }
        }
        if (chextn.equals("")) {
            fsize = size / (1024 * 1024);
            dsize = configurationMaster.getFileSize();
            if (dsize > fsize) {
                chextn = "";
            } else {
                chextn = "Size";
            }
        }
        return chextn;
    }
/*for docsize message*/
    public String docSizeMsg(int userId) {
        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
        return userRegisterService.docSizeCheck(userId);
    }
/*For deleting file*/
    public boolean deleteFile(String filePath) {

        File f = new File(filePath);
        if (f.delete()) {
            return true;
        } else {
            return false;
        }

    }
}
