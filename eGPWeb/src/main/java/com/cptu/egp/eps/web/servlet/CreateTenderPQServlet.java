/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.TenderService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class CreateTenderPQServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String strLoggedUserName = request.getSession().getAttribute("userName").toString();
            TenderService tenderService = (TenderService)AppContext.getSpringBean("TenderService");
            if("mapqualifiedbidders".equalsIgnoreCase(request.getParameter("action")))
            {
                String tenderId = request.getParameter("tenderId");
                String userIds [] = request.getParameterValues("chkName");
                StringBuilder uIds = new StringBuilder();
                for(String str : userIds){
                    if(str!=null){
                        uIds.append(str).append(",");
                    }
                }
                CommonMsgChk msgChk = tenderService.createTenderFromPQ(Integer.parseInt(tenderId), "INSERT", "REOI",uIds.substring(0, uIds.length()-1).toString(),strLoggedUserName);
                if(msgChk.getFlag()){
                    response.sendRedirect("officer/CreateTender.jsp?id="+msgChk.getId());
                }else{
                    System.out.println("Errororr : in createTenderFromPQ Servlet : "+msgChk.getMsg());
                    response.sendRedirect("officer/Notice.jsp?tenderid="+tenderId);
                }
            }else{
                String event=null;
                int tenderId=Integer.parseInt(request.getParameter("tId"));
                if("1".equals(request.getParameter("eType"))){
                    event = "PQ";
                }else if("2".equals(request.getParameter("eType"))){
                    event = "REOI";
                }else if("3".equals(request.getParameter("eType"))){
                    event = "1 stage-TSTM";
                }
                else if("4".equals(request.getParameter("eType"))){
                    event = "Rejected/Re-Tendering";
                }
                if("REOI".equalsIgnoreCase(event)){
                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    List<SPTenderCommonData> qualifiedTenders = tenderCommonService.returndata("getREOIQualifiedTenderer",request.getParameter("tId"),null);
                    if(qualifiedTenders.size()>7)
                    {
                        response.sendRedirect("officer/MapQualifiedBidders.jsp?tenderId="+tenderId);
                    }else{
                        int uId = Integer.parseInt(request.getSession().getAttribute("userId").toString());
                        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                        int originalUid = commonService.getOriginalUid(tenderId);
                        int updateResult = commonService.updateTenderMaster(uId, tenderId);//the user who is clicking on the "Create Tender" link should be the creator
                        CommonMsgChk msgChk = tenderService.createTenderFromPQ(tenderId, "INSERT", event,"",strLoggedUserName);
                        int updateWithOriginal = commonService.updateTenderMaster(originalUid, tenderId);//restoring original data
                        if(msgChk.getFlag() && updateResult>=1 && updateWithOriginal>=1){
                            response.sendRedirect("officer/CreateTender.jsp?id="+msgChk.getId());
                            //response.sendRedirectFilter("CreateTender.jsp?id="+msgChk.getId());
                        }else{
                            System.out.println("Errororr : in createTenderFromPQ Servlet : "+msgChk.getMsg());
                            response.sendRedirect("officer/Notice.jsp?tenderid="+tenderId);
                            //response.sendRedirectFilter("Notice.jsp?tenderid="+tenderId);
                        }
                    }
                }else{
                    CommonMsgChk msgChk = tenderService.createTenderFromPQ(tenderId, "INSERT", event,"",strLoggedUserName);
                    if(msgChk.getFlag()){
                        response.sendRedirect("officer/CreateTender.jsp?id="+msgChk.getId());
                        //response.sendRedirectFilter("CreateTender.jsp?id="+msgChk.getId());
                    }else{
                        System.out.println("Errororr : in createTenderFromPQ Servlet : "+msgChk.getMsg());
                        response.sendRedirect("officer/Notice.jsp?tenderid="+tenderId);
                        //response.sendRedirectFilter("Notice.jsp?tenderid="+tenderId);
                    }
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        finally { 
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
