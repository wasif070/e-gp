/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.web.servicebean.EvaluationConfigurationSrBean;
import java.io.IOException;
import java.io.PrintWriter;
//import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import com.cptu.egp.eps.web.utility.AppContext;

/**
 *
 * @author Karan
 */
public class EvalConfigurationServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EvalConfigurationServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EvalConfigurationServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
            HttpSession objHS = request.getSession();
            String tenderId="";
            String refNo="";
            String userId="";
            String pathNm="";
            String pageNm="";
            String queryString="";
            String msgTxt="";
            String referer = "";
            String strFunc="";

            if (request.getParameter("func") != null) {
                strFunc = request.getParameter("func");
            }

            if (objHS.getAttribute("userId") != null) {
                userId = objHS.getAttribute("userId").toString();
            } else {
                response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp");
            }

            if (request.getHeader("referer") != null) {
                referer = request.getHeader("referer");
            }

             // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), objHS.getAttribute("sessionId"), objHS.getAttribute("userTypeId"), request.getHeader("referer"));
            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            String idType = "tenderId";
            int auditId = 0;
            String auditAction =null;
            String moduleName = EgpModule.Evaluation.getName();
            String remarks = "";

            if (strFunc != null) {
                if ("requesttsc".equalsIgnoreCase(strFunc)) {
                    tenderId=request.getParameter("tenderId");
                    refNo=request.getParameter("refNo");
                    EvaluationConfigurationSrBean srBean= new EvaluationConfigurationSrBean();
                    boolean mailSent=false;

                    auditId = Integer.parseInt(tenderId);
                    auditAction ="Request for Technical Sub Committee Formation";


                    try {
                        mailSent = srBean.Request_TSCFormation(tenderId, refNo, userId);

                        if(mailSent){
                            msgTxt="tscrequested";
                        }else{
                            auditAction="Error in "+auditAction;
                            msgTxt="tscrequesterr";
                        }

                        if(referer.indexOf("EvalConfiguration.jsp")>0){
                            pageNm=request.getContextPath()+"/officer/EvalConfiguration.jsp";
                            queryString="?tenderid="+tenderId+"&action="+request.getParameter("action")+"&msgId="+msgTxt;
                        } else if(referer.indexOf("ViewConfiguration.jsp")>0){
                            pageNm=request.getContextPath()+"/officer/ViewConfiguration.jsp";
                            queryString="?tenderid="+tenderId+"&msgId="+msgTxt;
                        } else {
                            pageNm=request.getContextPath()+"/officer/EvalComm.jsp";
                            queryString="?tenderid="+tenderId+"&msgId="+msgTxt;
                        }

                        pathNm=pageNm+queryString;
                        response.sendRedirect(pathNm);

                    }
                    catch (Exception e)
                    {
                        auditAction="Error in "+auditAction;
                        System.out.println("Error in Request_TSCFormation Mail: " + e.toString());
                    }
                    finally
                    {
                        makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                    }
                } else if ("configuration".equalsIgnoreCase(strFunc)) {
                    // Configuration Add/Update Logic
                    tenderId=request.getParameter("tenderId");
                    refNo =request.getParameter("hdnTenderRefNo");
                    String action=request.getParameter("hdnAction");
                    String configType=request.getParameter("hdnEvalType");
                    String evalCommMembers=request.getParameter("hdnEvalCommittee");
                    String selectedTECUserId=request.getParameter("hdnTECMemberId");
                    String isTECReq=request.getParameter("hdnTSCReq");
                    String strTECUserId =  request.getParameter("rbTSCMember");

                    // Done by Darshan Shah on 20-Feb-2013 for fixing the issue of getting blank nominated team member id
                    if((selectedTECUserId == null || selectedTECUserId.trim().equals("") || selectedTECUserId.trim().equals("0")) && strTECUserId != null && !strTECUserId.trim().equals(""))
                    {
                        System.out.println("strTECUserId "+strTECUserId);
                        selectedTECUserId = strTECUserId.replace(",", "");
                    }

                    auditId = Integer.parseInt(tenderId);
                    auditAction ="Configuration of Evaluation Process";
                    if (configType != null && "team".equalsIgnoreCase(configType))
                    {
                            auditAction= auditAction+" - Team";
                    }
                    else
                    {
                            auditAction= auditAction+" - Individual";
                    }
                    if ("add".equalsIgnoreCase(action)) {
                        // Insert Case
                        int newEvalConfigId = 0;
                        EvaluationConfigurationSrBean srBean = new EvaluationConfigurationSrBean();
                        try
                        {
                            newEvalConfigId = srBean.Insert_Configuration(tenderId, configType, evalCommMembers, userId, selectedTECUserId, isTECReq, refNo);

                            if (newEvalConfigId != 0)
                            {
                                msgTxt = "configdone";
                            }
                            else
                            {
                                auditAction="Error in "+auditAction;
                                msgTxt = "configfailed";
                            }

                            //response.sendRedirectFilter pageNm = request.getContextPath() + "/officer/EvalComm.jsp";
                            pageNm = "officer/EvalComm.jsp";
                            queryString = "?tenderid=" + tenderId + "&msgId=" + msgTxt;

                            pathNm = pageNm + queryString;
                            response.sendRedirect(pathNm);

                        } catch (Exception e)
                        {
                            auditAction="Error in "+auditAction;
                            System.out.println("Error in while doing Evaluation Configuration: " + e.toString());
                        }
                        finally
                        {
                            //System.out.println("--------------------------------------------------------------");
                           // System.out.println(idType+"="+auditId+" ModuleName="+moduleName+"Audit action="+auditAction+"remarks="+remarks);
                            makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                            //System.out.println("--------------------------------------------------------------");
                        }
                    }

                    //Nullify String objects
                    action = null;
                    configType = null;
                    evalCommMembers = null;
                    selectedTECUserId = null;
                    isTECReq = null;

                } else if ("modifyconfig".equalsIgnoreCase(strFunc)) {
                    tenderId=request.getParameter("tenderId");
                    refNo=request.getParameter("refNo");
                    EvaluationConfigurationSrBean srBean= new EvaluationConfigurationSrBean();
                    boolean isSuccess=false;
                    try{
                        isSuccess=srBean.Modify_Configuration(tenderId, refNo, userId);

                        if(isSuccess){
                            msgTxt="configchanged";
                        }else{
                            msgTxt="configchangefailed";
                        }

                     pageNm=request.getContextPath()+"/officer/EvalComm.jsp";
                     queryString="?tenderid="+tenderId+"&msgId="+msgTxt;

                     pathNm=pageNm+queryString;
                     response.sendRedirect(pathNm);

                    }catch (Exception e) {
                        System.out.println("Error in while modifying Evaluation Configuration: " + e.toString());
                    }

                }
            }

            //Nullify String objects
            tenderId = null;
            refNo = null;
            userId = null;
            pathNm = null;
            pageNm = null;
            queryString = null;
            msgTxt = null;
            referer = null;
            strFunc = null;

        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
