/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.databean;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.service.serviceimpl.OfficerTabServiceImpl;
import com.cptu.egp.eps.service.serviceimpl.TendererTabServiceImpl;
import com.cptu.egp.eps.web.servicebean.OfficerTabSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author parag
 */
public class TendererTabDtBean {


    private int tendererId;
    private int tenderId;

    public int getTenderId() {
        return tenderId;
    }

    public void setTenderId(int tenderId) {
        this.tenderId = tenderId;
    }

    public int getTendererId() {
        return tendererId;
    }

    public void setTendererId(int tendererId) {
        this.tendererId = tendererId;
    }

    /**
     * Give declartion status of tenderer as per userId and tenderId
     * @return String accepted or rejected
     */
    public String isTendererDeclarationStepOver(){
        TendererTabServiceImpl tabServiceImpl = (TendererTabServiceImpl) AppContext.getSpringBean("TendererTabServiceImpl");
        return tabServiceImpl.isTendererDeclarationStepOver(tendererId,tenderId) ;
    }

    /**
     * Get Procurement nature using tenderId
     * @param tenderId
     * @return List of TblTenderDetails
     */
    public List<TblTenderDetails> getProcurementNature(int tenderId) {
        OfficerTabServiceImpl tabServiceImpl = (OfficerTabServiceImpl) AppContext.getSpringBean("OfficerTabServiceImpl");
        List<TblTenderDetails> tblTenderDetails = new ArrayList<TblTenderDetails>();
        try {
            if (tblTenderDetails.isEmpty()) {
                for (TblTenderDetails tDetails : tabServiceImpl.getProcurementNature(tenderId)) {
                    tblTenderDetails.add(tDetails);
                }
            }
            return tblTenderDetails;
            //return tabServiceImpl.getProcurementNature(tenderId);
        } catch (Exception ex) {
            Logger.getLogger(OfficerTabSrBean.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
            tblTenderDetails = null;
        }
    }

    /**
     * Get Debarment Detials of userid and tenderId
     * @param userId
     * @param tenderId
     * @return SPCommonSearchService
     */
    public List<SPCommonSearchData> getDebarmentDetails(String userId,String tenderId){
        CommonSearchService commonSearchService = (CommonSearchService)AppContext.getSpringBean("CommonSearchService");
        return commonSearchService.searchData("GetDebarmentUserStatus", tenderId, userId, "1", null, null, null, null, null, null);
    }

    /**
     * Get SubContract Count as per userid and tenderId
     * @param tenderId
     * @param userId
     * @return count of record
     */
    public long subContractCount(String tenderId,String userId){
        OfficerTabServiceImpl tabServiceImpl = (OfficerTabServiceImpl) AppContext.getSpringBean("OfficerTabServiceImpl");
        return tabServiceImpl.subContractCount(tenderId, userId);
    }
    
    /**
     * Get subcontracting is done for tenderId and userId
     * @param tenderId
     * @param userId
     * @return true if done else return false
     */
    public boolean subContractCondition(String tenderId,String userId){
             boolean b_isSubContract = true;
             CommonSearchService commonSearchService = (CommonSearchService)AppContext.getSpringBean("CommonSearchService");
            List<SPCommonSearchData> subContractCond = commonSearchService.searchData("isSubContractingAccepted", tenderId, userId, null, null, null, null, null, null, null);
                    if(subContractCond!=null && !subContractCond.isEmpty() && Integer.parseInt(subContractCond.get(0).getFieldName1())>0){
                        b_isSubContract = false;
                    }
                    return b_isSubContract;
    }
    
     /**
     * Get tenderClosed or not
     * @param tenderId tenderId 
     * @return boolean true or false
     */
    public boolean isTenderClosed(String tenderId){
        OfficerTabServiceImpl tabServiceImpl = (OfficerTabServiceImpl) AppContext.getSpringBean("OfficerTabServiceImpl");
        return tabServiceImpl.isTenderClosed(tenderId);
    }
}
