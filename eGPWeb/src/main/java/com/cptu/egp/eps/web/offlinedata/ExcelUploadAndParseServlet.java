/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.offlinedata;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author salahuddin
 */
public class ExcelUploadAndParseServlet extends HttpServlet {
   private File tmpDir;
    DataInputStream dis = null;
    BufferedInputStream bis = null;
    FileInputStream fis = null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
                  System.out.println("endered");
	                try {
	                    DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();

	                    fileItemFactory.setSizeThreshold(1 * 1024 * 1024); //1 MB

	                    fileItemFactory.setRepository(tmpDir);

	                    ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);

	                    try {

	                        List items = uploadHandler.parseRequest(request);
	                        Iterator itr = items.iterator();
                                HttpSession hs = request.getSession();

                                String formtype =(String) hs.getAttribute("formType");
                                System.out.println("formtype >> "+formtype);

	                        while (itr.hasNext()) {
	                            FileItem item = (FileItem) itr.next();
	                            if (item.isFormField()) {
	                                System.out.println("Field Name = " + item.getFieldName() + ",Field Value = " + item.getString());
	                            } else {
                                        System.out.println(" File Name >> "+item.getName());
                                        String fileName = item.getName();
	                                InputStream inputStream = item.getInputStream();

                                        //
                                        if(formtype.equals("TenderWithPQ"))
                                        {
                                            if(!fileName.contains(".xls"))
                                                response.sendRedirect("/offlinedata/CreateTenderWithPQ.jsp?alert=true");
                                            else
                                                fileName = fileName.replace(fileName, "Pre-quaificatonForm_1.xls");

                                        }
                                        else if(formtype.equals("TenderWithoutPQ"))
                                        {
                                            if(!fileName.contains(".xls"))
                                                response.sendRedirect("/offlinedata/CreateTenderWithoutPQ.jsp?alert=true");
                                            else
                                                fileName = fileName.replace(fileName, "TenderForm_2.xls");
                                        }
                                        else if(formtype.equals("Award"))
                                        {
                                            if(!fileName.contains(".xls"))
                                                response.sendRedirect("/offlinedata/CreateContractAward.jsp?alert=true");
                                            else
                                                fileName = fileName.replace(fileName, "Awards_5.xls");
                                        }
                                        else if(formtype.equals("EOI"))
                                        {
                                            if(!fileName.contains(".xls"))
                                                response.sendRedirect("/offlinedata/CreateREOI.jsp?alert=true");
                                            else
                                                fileName = fileName.replace(fileName, "EOI_3.xls");
                                        }
                                        //

                                       // PrequalificationExcellBean exbean =  (PrequalificationExcellBean)eventHandler.ParseExcell(formtype , fileName,inputStream);


                                        ExcelParsingbyEventHandler eventHandler = new ExcelParsingbyEventHandler();
                                       // OfflineDataSrBean offlineDataSrBean = new OfflineDataSrBean();
                                         if(fileName.equals("Pre-quaificatonForm_1.xls")){
                                                PrequalificationExcellBean exbean =  (PrequalificationExcellBean)eventHandler.ParseExcell(formtype , fileName,inputStream);
                                                //exbean.setMinistryName("Ministry of Testing");
                                                //exbean.setAgency("gfgf");
                                               // System.out.println("Final Data Bean >>> "+exbean);
                                              //  System.out.println(" ministry name  >> "+exbean.getMinistryName());
                                              //  System.out.println(" agency name  >> "+exbean.getAgency());
                                             //   System.out.println("lotnumber >> "+exbean.getLotNo_1());
                                             //   System.out.println("invitation for >> "+exbean.getInvitationFor());
                                             //   System.out.println("invitation1 >> "+exbean.getInvitation1());
                                                if(exbean!=null && exbean.getInvitation1()!=null){
                                                    if(exbean.getInvitation1().equals("Pre-qualification"))
                                                    {
                                                        RequestDispatcher rd = request.getRequestDispatcher("/offlinedata/EditTenderWithPQ.jsp");
                                                        request.setAttribute("prequalificationDataBean", exbean);
                                                        rd.forward(request, response);
                                                    }
                                                    else
                                                    {
                                                       response.sendRedirect("/offlinedata/CreateTenderWithPQ.jsp?alert=true");
                                                    }
                                                }
                                                else
                                                {
                                                   response.sendRedirect("/offlinedata/CreateTenderWithPQ.jsp?alert=true");
                                                }
                                           }else if(fileName.equals("TenderForm_2.xls")){
                                                TenderFormExcellBean tenexbean =  (TenderFormExcellBean)eventHandler.ParseExcell(formtype,fileName,inputStream);
                                               // System.out.println("Final Data Bean >>> "+tenexbean);
                                               // System.out.println(" ministry name  >> "+tenexbean.getMinistryName());
                                              //  System.out.println(" agency name  >> "+tenexbean.getAgency());
                                              //  System.out.println("invitation for >> "+tenexbean.getInvitationFor());
                                             //   System.out.println("invitation1 >> "+tenexbean.getEventType());
                                                if(tenexbean!=null && tenexbean.getEventType()!=null){
                                                    if(tenexbean.getEventType().equals("Tender") || tenexbean.getEventType().equals("Re-Tender"))
                                                    {
                                                        RequestDispatcher rd = request.getRequestDispatcher("/offlinedata/EditTenderWithoutPQ.jsp");
                                                        request.setAttribute("tenderDataBean", tenexbean);
                                                        rd.forward(request, response);
                                                    }
                                                    else
                                                    {
                                                       response.sendRedirect("/offlinedata/CreateTenderWithoutPQ.jsp?alert=true");
                                                    }
                                                }
                                                else
                                                {
                                                   response.sendRedirect("/offlinedata/CreateTenderWithoutPQ.jsp?alert=true");
                                                }
                                           }else if(fileName.equals("Awards_5.xls")){
                                                ContractAwardExcellBean exbean = (ContractAwardExcellBean)eventHandler.ParseExcell(formtype ,fileName, inputStream);
                                              //  System.out.println("Final Data Bean >>> "+exbean);
                                             //   System.out.println(" ministry name  >> "+exbean.getMinistryName());
                                             //   System.out.println(" agency name  >> "+exbean.getAgency());
                                              //  System.out.println("invitation1 >> "+exbean.getContractawardfor());
                                                if(exbean!=null && exbean.getContractawardfor()!=null){
                                                    if(exbean.getContractawardfor().equals("Goods")||exbean.getContractawardfor().equals("Works")||exbean.getContractawardfor().equals("Services"))
                                                    {
                                                        RequestDispatcher rd = request.getRequestDispatcher("/offlinedata/EditContractAward.jsp");
                                                        request.setAttribute("awardDataBean", exbean);
                                                        rd.forward(request, response);
                                                    }
                                                    else
                                                    {
                                                       response.sendRedirect("/offlinedata/CreateContractAward.jsp?alert=true");
                                                    }
                                                }
                                                else
                                                {
                                                   response.sendRedirect("/offlinedata/CreateContractAward.jsp?alert=true");
                                                }
                                               
                                           }else if(fileName.equals("EOI_3.xls")){
                                                EOIExcellBean exbean = (EOIExcellBean)eventHandler.ParseExcell(formtype,fileName, inputStream);
                                               // System.out.println("Final Data Bean >>> "+exbean);
                                                //System.out.println(" ministry name  >> "+exbean.getMinistryName());
                                                //System.out.println(" agency name  >> "+exbean.getAgency());
                                                // System.out.println(" EOI  >> "+exbean.getInterestForSelectionOfConsult());

                                                if(exbean!=null && exbean.getInterestForSelectionOfConsult()!=null){
                                                if(exbean.getInterestForSelectionOfConsult().equals("Individual Consultant") || exbean.getInterestForSelectionOfConsult().equals("Consulting Firm") )
                                                    {
                                                    RequestDispatcher rd = request.getRequestDispatcher("/offlinedata/EditREOI.jsp");
                                                    request.setAttribute("EOIDataBean", exbean);
                                                    rd.forward(request, response);
                                                    }
                                                else
                                                    {
                                                       response.sendRedirect("/offlinedata/CreateREOI.jsp?alert=true");

                                                    }
                                                }
                                                else
                                                {
                                                   response.sendRedirect("/offlinedata/CreateREOI.jsp?alert=true");

                                                }
                                           }
                                        // once all the events are processed close our file input stream
                                        inputStream.close();
                                        // and our document input stream (don't want to leak these!)
                                       // RequestDispatcher rd = request.getRequestDispatcher("ExcelUploadAndReadServlet.jsp");
	                                //request.setAttribute("prequalificationDataBean", exbean);
                                       // rd.forward(request, response);
                                       // response.sendRedirect("index.jsp");
	                            }
	                        }
	                    } catch (Exception e) {
                                e.printStackTrace();
	                    }

	                } catch (Exception e) {
	                    System.out.println(e);
	                }
        } finally {
            out.close();
        }
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}