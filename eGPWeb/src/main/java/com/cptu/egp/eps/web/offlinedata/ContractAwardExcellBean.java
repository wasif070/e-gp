/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.offlinedata;

/**
 *
 * @author Administrator
 */
public class ContractAwardExcellBean {

private String ministryName;
private String agency;
private String procuringEntityDistrict;
private String contractawardfor;
private String procurementMethod;
private String budgetFund;
private String sourceFund;
private String istheContractSignedwiththesamepersonstatedintheNOA;
private String wasthePerformanceSecurityprovidedinduetime;
private String wastheContractSignedinduetime;
private String procuringEntityName;
private String procuringEntityCode;
private String invitPropRefNo;
private String developmentPartner;
private String projectOrProgrammeCode;
private String projectOrProgrammeName;
private String tenderPackageNo;
private String tenderPackageName;
private String dateOfAdvertisement;
private String dateofNotificationofAward;
private String dateofContractSigning;
private String proposedDateofContractCompletion;
private String noofTendersProposalsSold;
private String noofTendersProposalsReceived;
private String noofResponsiveTendersProposals;
private String briefDescriptionofContract;
private String contractValue;
private String nameofSupplierORContractorORConsultant;
private String locationofSupplierORContractorORConsultant;
private String locationofDeliveryORWorksORConsultancy;
private String ifNogivereasonwhy_NOA;
private String ifNoGiveReasonWhy_Performance_Security;
private String ifNoGiveReasonWhy_Contract_signed;
private String NameofAuthorisedOfficer;
private String DesignationofAuthorisedOfficer;

    /**
     * @return the ministryName
     */
    public String getMinistryName() {
        return ministryName;
    }

    /**
     * @param ministryName the ministryName to set
     */
    public void setMinistryName(String ministryName) {
        this.ministryName = ministryName;
    }

    /**
     * @return the agency
     */
    public String getAgency() {
        return agency;
    }

    /**
     * @param agency the agency to set
     */
    public void setAgency(String agency) {
        this.agency = agency;
    }

    /**
     * @return the procuringEntityDistrict
     */
    public String getProcuringEntityDistrict() {
        return procuringEntityDistrict;
    }

    /**
     * @param procuringEntityDistrict the procuringEntityDistrict to set
     */
    public void setProcuringEntityDistrict(String procuringEntityDistrict) {
        this.procuringEntityDistrict = procuringEntityDistrict;
    }

    /**
     * @return the contractawardfor
     */
    public String getContractawardfor() {
        return contractawardfor;
    }

    /**
     * @param contractawardfor the contractawardfor to set
     */
    public void setContractawardfor(String contractawardfor) {
        this.contractawardfor = contractawardfor;
    }

    /**
     * @return the procurementMethod
     */
    public String getProcurementMethod() {
        return procurementMethod;
    }

    /**
     * @param procurementMethod the procurementMethod to set
     */
    public void setProcurementMethod(String procurementMethod) {
        this.procurementMethod = procurementMethod;
    }

    /**
     * @return the budgetFund
     */
    public String getBudgetFund() {
        return budgetFund;
    }

    /**
     * @param budgetFund the budgetFund to set
     */
    public void setBudgetFund(String budgetFund) {
        this.budgetFund = budgetFund;
    }

    /**
     * @return the sourceFund
     */
    public String getSourceFund() {
        return sourceFund;
    }

    /**
     * @param sourceFund the sourceFund to set
     */
    public void setSourceFund(String sourceFund) {
        this.sourceFund = sourceFund;
    }

    /**
     * @return the istheContractSignedwiththesamepersonstatedintheNOA
     */
    public String getIstheContractSignedwiththesamepersonstatedintheNOA() {
        return istheContractSignedwiththesamepersonstatedintheNOA;
    }

    /**
     * @param istheContractSignedwiththesamepersonstatedintheNOA the istheContractSignedwiththesamepersonstatedintheNOA to set
     */
    public void setIstheContractSignedwiththesamepersonstatedintheNOA(String istheContractSignedwiththesamepersonstatedintheNOA) {
        this.istheContractSignedwiththesamepersonstatedintheNOA = istheContractSignedwiththesamepersonstatedintheNOA;
    }

    /**
     * @return the wasthePerformanceSecurityprovidedinduetime
     */
    public String getWasthePerformanceSecurityprovidedinduetime() {
        return wasthePerformanceSecurityprovidedinduetime;
    }

    /**
     * @param wasthePerformanceSecurityprovidedinduetime the wasthePerformanceSecurityprovidedinduetime to set
     */
    public void setWasthePerformanceSecurityprovidedinduetime(String wasthePerformanceSecurityprovidedinduetime) {
        this.wasthePerformanceSecurityprovidedinduetime = wasthePerformanceSecurityprovidedinduetime;
    }

    /**
     * @return the wastheContractSignedinduetime
     */
    public String getWastheContractSignedinduetime() {
        return wastheContractSignedinduetime;
    }

    /**
     * @param wastheContractSignedinduetime the wastheContractSignedinduetime to set
     */
    public void setWastheContractSignedinduetime(String wastheContractSignedinduetime) {
        this.wastheContractSignedinduetime = wastheContractSignedinduetime;
    }

    /**
     * @return the procuringEntityName
     */
    public String getProcuringEntityName() {
        return procuringEntityName;
    }

    /**
     * @param procuringEntityName the procuringEntityName to set
     */
    public void setProcuringEntityName(String procuringEntityName) {
        this.procuringEntityName = procuringEntityName;
    }

    /**
     * @return the procuringEntityCode
     */
    public String getProcuringEntityCode() {
        return procuringEntityCode;
    }

    /**
     * @param procuringEntityCode the procuringEntityCode to set
     */
    public void setProcuringEntityCode(String procuringEntityCode) {
        this.procuringEntityCode = procuringEntityCode;
    }

    /**
     * @return the invitPropRefNo
     */
    public String getInvitPropRefNo() {
        return invitPropRefNo;
    }

    /**
     * @param invitPropRefNo the invitPropRefNo to set
     */
    public void setInvitPropRefNo(String invitPropRefNo) {
        this.invitPropRefNo = invitPropRefNo;
    }

    /**
     * @return the developmentPartner
     */
    public String getDevelopmentPartner() {
        return developmentPartner;
    }

    /**
     * @param developmentPartner the developmentPartner to set
     */
    public void setDevelopmentPartner(String developmentPartner) {
        this.developmentPartner = developmentPartner;
    }

    /**
     * @return the projectOrProgrammeCode
     */
    public String getProjectOrProgrammeCode() {
        return projectOrProgrammeCode;
    }

    /**
     * @param projectOrProgrammeCode the projectOrProgrammeCode to set
     */
    public void setProjectOrProgrammeCode(String projectOrProgrammeCode) {
        this.projectOrProgrammeCode = projectOrProgrammeCode;
    }

    /**
     * @return the projectOrProgrammeName
     */
    public String getProjectOrProgrammeName() {
        return projectOrProgrammeName;
    }

    /**
     * @param projectOrProgrammeName the projectOrProgrammeName to set
     */
    public void setProjectOrProgrammeName(String projectOrProgrammeName) {
        this.projectOrProgrammeName = projectOrProgrammeName;
    }

    /**
     * @return the tenderPackageNo
     */
    public String getTenderPackageNo() {
        return tenderPackageNo;
    }

    /**
     * @param tenderPackageNo the tenderPackageNo to set
     */
    public void setTenderPackageNo(String tenderPackageNo) {
        this.tenderPackageNo = tenderPackageNo;
    }

    /**
     * @return the tenderPackageName
     */
    public String getTenderPackageName() {
        return tenderPackageName;
    }

    /**
     * @param tenderPackageName the tenderPackageName to set
     */
    public void setTenderPackageName(String tenderPackageName) {
        this.tenderPackageName = tenderPackageName;
    }

    /**
     * @return the dateOfAdvertisement
     */
    public String getDateOfAdvertisement() {
        return dateOfAdvertisement;
    }

    /**
     * @param dateOfAdvertisement the dateOfAdvertisement to set
     */
    public void setDateOfAdvertisement(String dateOfAdvertisement) {
        this.dateOfAdvertisement = dateOfAdvertisement;
    }

    /**
     * @return the dateofNotificationofAward
     */
    public String getDateofNotificationofAward() {
        return dateofNotificationofAward;
    }

    /**
     * @param dateofNotificationofAward the dateofNotificationofAward to set
     */
    public void setDateofNotificationofAward(String dateofNotificationofAward) {
        this.dateofNotificationofAward = dateofNotificationofAward;
    }

    /**
     * @return the dateofContractSigning
     */
    public String getDateofContractSigning() {
        return dateofContractSigning;
    }

    /**
     * @param dateofContractSigning the dateofContractSigning to set
     */
    public void setDateofContractSigning(String dateofContractSigning) {
        this.dateofContractSigning = dateofContractSigning;
    }

    /**
     * @return the proposedDateofContractCompletion
     */
    public String getProposedDateofContractCompletion() {
        return proposedDateofContractCompletion;
    }

    /**
     * @param proposedDateofContractCompletion the proposedDateofContractCompletion to set
     */
    public void setProposedDateofContractCompletion(String proposedDateofContractCompletion) {
        this.proposedDateofContractCompletion = proposedDateofContractCompletion;
    }

    /**
     * @return the noofTendersProposalsSold
     */
    public String getNoofTendersProposalsSold() {
        return noofTendersProposalsSold;
    }

    /**
     * @param noofTendersProposalsSold the noofTendersProposalsSold to set
     */
    public void setNoofTendersProposalsSold(String noofTendersProposalsSold) {
        this.noofTendersProposalsSold = noofTendersProposalsSold;
    }

    /**
     * @return the noofTendersProposalsReceived
     */
    public String getNoofTendersProposalsReceived() {
        return noofTendersProposalsReceived;
    }

    /**
     * @param noofTendersProposalsReceived the noofTendersProposalsReceived to set
     */
    public void setNoofTendersProposalsReceived(String noofTendersProposalsReceived) {
        this.noofTendersProposalsReceived = noofTendersProposalsReceived;
    }

    /**
     * @return the noofResponsiveTendersProposals
     */
    public String getNoofResponsiveTendersProposals() {
        return noofResponsiveTendersProposals;
    }

    /**
     * @param noofResponsiveTendersProposals the noofResponsiveTendersProposals to set
     */
    public void setNoofResponsiveTendersProposals(String noofResponsiveTendersProposals) {
        this.noofResponsiveTendersProposals = noofResponsiveTendersProposals;
    }

    /**
     * @return the briefDescriptionofContract
     */
    public String getBriefDescriptionofContract() {
        return briefDescriptionofContract;
    }

    /**
     * @param briefDescriptionofContract the briefDescriptionofContract to set
     */
    public void setBriefDescriptionofContract(String briefDescriptionofContract) {
        this.briefDescriptionofContract = briefDescriptionofContract;
    }

    /**
     * @return the contractValue
     */
    public String getContractValue() {
        return contractValue;
    }

    /**
     * @param contractValue the contractValue to set
     */
    public void setContractValue(String contractValue) {
        this.contractValue = contractValue;
    }

    /**
     * @return the nameofSupplierORContractorORConsultant
     */
    public String getNameofSupplierORContractorORConsultant() {
        return nameofSupplierORContractorORConsultant;
    }

    /**
     * @param nameofSupplierORContractorORConsultant the nameofSupplierORContractorORConsultant to set
     */
    public void setNameofSupplierORContractorORConsultant(String nameofSupplierORContractorORConsultant) {
        this.nameofSupplierORContractorORConsultant = nameofSupplierORContractorORConsultant;
    }

    /**
     * @return the locationofSupplierORContractorORConsultant
     */
    public String getLocationofSupplierORContractorORConsultant() {
        return locationofSupplierORContractorORConsultant;
    }

    /**
     * @param locationofSupplierORContractorORConsultant the locationofSupplierORContractorORConsultant to set
     */
    public void setLocationofSupplierORContractorORConsultant(String locationofSupplierORContractorORConsultant) {
        this.locationofSupplierORContractorORConsultant = locationofSupplierORContractorORConsultant;
    }

    /**
     * @return the ifNogivereasonwhy_NOA
     */
    public String getIfNogivereasonwhy_NOA() {
        return ifNogivereasonwhy_NOA;
    }

    /**
     * @param ifNogivereasonwhy_NOA the ifNogivereasonwhy_NOA to set
     */
    public void setIfNogivereasonwhy_NOA(String ifNogivereasonwhy_NOA) {
        this.ifNogivereasonwhy_NOA = ifNogivereasonwhy_NOA;
    }

    /**
     * @return the ifNoGiveReasonWhy_Performance_Security
     */
    public String getIfNoGiveReasonWhy_Performance_Security() {
        return ifNoGiveReasonWhy_Performance_Security;
    }

    /**
     * @param ifNoGiveReasonWhy_Performance_Security the ifNoGiveReasonWhy_Performance_Security to set
     */
    public void setIfNoGiveReasonWhy_Performance_Security(String ifNoGiveReasonWhy_Performance_Security) {
        this.ifNoGiveReasonWhy_Performance_Security = ifNoGiveReasonWhy_Performance_Security;
    }

    /**
     * @return the ifNoGiveReasonWhy_Contract_signed
     */
    public String getIfNoGiveReasonWhy_Contract_signed() {
        return ifNoGiveReasonWhy_Contract_signed;
    }

    /**
     * @param ifNoGiveReasonWhy_Contract_signed the ifNoGiveReasonWhy_Contract_signed to set
     */
    public void setIfNoGiveReasonWhy_Contract_signed(String ifNoGiveReasonWhy_Contract_signed) {
        this.ifNoGiveReasonWhy_Contract_signed = ifNoGiveReasonWhy_Contract_signed;
    }

    /**
     * @return the locationofDeliveryORWorksORConsultancy
     */
    public String getLocationofDeliveryORWorksORConsultancy() {
        return locationofDeliveryORWorksORConsultancy;
    }

    /**
     * @param locationofDeliveryORWorksORConsultancy the locationofDeliveryORWorksORConsultancy to set
     */
    public void setLocationofDeliveryORWorksORConsultancy(String locationofDeliveryORWorksORConsultancy) {
        this.locationofDeliveryORWorksORConsultancy = locationofDeliveryORWorksORConsultancy;
    }

    /**
     * @return the NameofAuthorisedOfficer
     */
    public String getNameofAuthorisedOfficer() {
        return NameofAuthorisedOfficer;
    }

    /**
     * @param NameofAuthorisedOfficer the NameofAuthorisedOfficer to set
     */
    public void setNameofAuthorisedOfficer(String NameofAuthorisedOfficer) {
        this.NameofAuthorisedOfficer = NameofAuthorisedOfficer;
    }

    /**
     * @return the DesignationofAuthorisedOfficer
     */
    public String getDesignationofAuthorisedOfficer() {
        return DesignationofAuthorisedOfficer;
    }

    /**
     * @param DesignationofAuthorisedOfficer the DesignationofAuthorisedOfficer to set
     */
    public void setDesignationofAuthorisedOfficer(String DesignationofAuthorisedOfficer) {
        this.DesignationofAuthorisedOfficer = DesignationofAuthorisedOfficer;
    }


   
}
