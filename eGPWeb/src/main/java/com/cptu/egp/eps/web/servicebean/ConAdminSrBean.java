/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblAdminMaster;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblUserTypeMaster;
import com.cptu.egp.eps.service.serviceinterface.AdminRegistrationService;
import com.cptu.egp.eps.web.databean.AdminMasterDtBean;
import com.cptu.egp.eps.web.databean.LoginMasterDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.AppExceptionHandler;
import com.cptu.egp.eps.web.utility.FileEncryptDecryptUtil;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author Administrator
 */
public class ConAdminSrBean {

    private AdminRegistrationService adminRegService= (AdminRegistrationService) AppContext.getSpringBean("AdminRegistrationService");
    static final Logger LOGGER = Logger.getLogger(ConAdminSrBean.class);
    private String loggerStart = " Starts";
    private String loggerEnd = " Ends";
    private String logUserId = "0";

    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        adminRegService.setAuditTrail(auditTrail);
    }

    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        adminRegService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /**
     * copies properties
     * @param adminMasterDtBean
     * @return Object with copied properties
     */
    public TblAdminMaster _toAdminMaster(AdminMasterDtBean adminMasterDtBean){
        LOGGER.debug("_toAdminMaster : " + logUserId + loggerStart);
        TblAdminMaster adminMaster = new TblAdminMaster();
        try {
        BeanUtils.copyProperties(adminMasterDtBean, adminMaster);
        } catch (Exception e) {
            LOGGER.error("_toAdminMaster : " + logUserId,e);
            e.printStackTrace();
            AppExceptionHandler excpHandle = new AppExceptionHandler();
            excpHandle.stack2string(e);
        }
        LOGGER.debug("_toAdminMaster : " + logUserId + loggerEnd);
        return adminMaster;
    }

    /**
     * copies properties
     * @param loginMasterDtBean
     * @return Object with copied properties
     */
    public TblLoginMaster _toLoginMaster(LoginMasterDtBean loginMasterDtBean){
        LOGGER.debug("_toLoginMaster : " + logUserId + loggerStart);
        TblLoginMaster loginMaster = new TblLoginMaster();
        try {
        BeanUtils.copyProperties(loginMasterDtBean, loginMaster);
        } catch (Exception e) {
            LOGGER.error("_toLoginMaster : " + logUserId,e);
            e.printStackTrace();
            AppExceptionHandler excpHandle = new AppExceptionHandler();
            excpHandle.stack2string(e);
        }
        LOGGER.debug("_toLoginMaster : " + logUserId + loggerEnd);
        //loginMaster.setPassword(""+loginMasterDtBean.getPassword().hashCode());
        return loginMaster;
    }

    /**
     * Register Content and Procurement Admin
     * @param adminMasterDtBean
     * @param loginMasterDtBean
     * @param pwd
     * @param userTypeId
     * @return userId if register successfully
     */

    public Integer conAdminReg (AdminMasterDtBean adminMasterDtBean,LoginMasterDtBean loginMasterDtBean,String pwd,Byte userTypeId,String... auditDetails){
        LOGGER.debug("conAdminReg : " + logUserId + loggerStart);
        int userId = 0;
        try {
        TblLoginMaster loginMaster = _toLoginMaster(loginMasterDtBean);
        loginMaster.setBusinessCountryName("");
        loginMaster.setRegistrationType("officer");
        loginMaster.setIsJvca("no");
        loginMaster.setBusinessCountryName("bangladesh");
        loginMaster.setNextScreen("ChangePassword");
        loginMaster.setIsEmailVerified("yes");
        loginMaster.setFailedAttempt((byte)0);
        loginMaster.setValidUpTo(null);
        loginMaster.setFirstLogin("yes");
        loginMaster.setIsPasswordReset("no");
        loginMaster.setResetPasswordCode("0");
        loginMaster.setStatus("approved");
        loginMaster.setRegisteredDate(new Date());
        loginMaster.setNationality("bangladeshi");
        loginMaster.setHintQuestion("");
        loginMaster.setHintAnswer("");
        loginMaster.setTblUserTypeMaster(new TblUserTypeMaster((byte)userTypeId));
        TblAdminMaster adminMaster = _toAdminMaster(adminMasterDtBean);
        System.out.println("Data set in Login and Admin Master");
            
            userId = adminRegService.registerContentAdmin(adminMaster, loginMaster,auditDetails);
            System.out.println("Created User id "+userId +"for type id "+userTypeId);
            if(userId > 0){
                if(sendMailAndSMS(loginMasterDtBean.getEmailId(),pwd, adminMasterDtBean.getMobileNo(),userTypeId)){
                    LOGGER.debug("conAdminReg : " + logUserId + " sendMailAndSMS success");
                }else{
                    LOGGER.debug("conAdminReg : " + logUserId + " sendMailAndSMS failure");
                }
            }
        } catch (Exception ex) {
            LOGGER.error("conAdminReg : " + logUserId,ex);
            ex.printStackTrace();
            AppExceptionHandler excpHandle = new AppExceptionHandler();
            excpHandle.stack2string(ex);
        }
        LOGGER.debug("conAdminReg : " + logUserId + loggerEnd);
        return userId;
    }

    /**
     * Send Sms and mail
     * @param mailId
     * @param password
     * @param mobNo
     * @return true if message and mail sent successfully
     */
    private boolean sendMailAndSMS(String mailId,String password,String mobNo, Byte userTypeId)
    {
        LOGGER.debug("sendMailAndSMS : " + logUserId + loggerStart);
        boolean mailSent = false;
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            String[] mailTo = {mailId};
            MailContentUtility mailContentUtility = new MailContentUtility();

            List list = mailContentUtility.getconAdminRegContent(mailId,password,userTypeId);
            String mailSub = list.get(0).toString();
            String mailText = list.get(1).toString();
            sendMessageUtil.setEmailTo(mailTo);
            sendMessageUtil.setEmailSub(mailSub);
            sendMessageUtil.setEmailMessage(mailText);
            try{
                sendMessageUtil.sendEmail();
            }
            catch(Exception e){
                LOGGER.error("sendMailAndSMS : " + logUserId,e);
                e.printStackTrace();
                AppExceptionHandler excpHandle = new AppExceptionHandler();
                excpHandle.stack2string(e);
            }

            try{
                sendMessageUtil.setSmsNo("+975"+mobNo);
                sendMessageUtil.setSmsBody("Dear User,%0Aprofile has been created Login information is sent to your registered Email ID.%0Ae-GP User Registration Desk");
                sendMessageUtil.sendSMS();
            }catch(Exception e){
                LOGGER.error("sendMailAndSMS : " + logUserId,e);
                e.printStackTrace();
                AppExceptionHandler excpHandle = new AppExceptionHandler();
                excpHandle.stack2string(e);
            }
            mailSent = true;
        }
        catch (Exception ex) {
            LOGGER.error("sendMailAndSMS : " + logUserId,ex);
            ex.printStackTrace();
            AppExceptionHandler excpHandle = new AppExceptionHandler();
            excpHandle.stack2string(ex);
        }
        LOGGER.debug("sendMailAndSMS : " + logUserId + loggerEnd);
        return mailSent;
    }

    public String fileName(HttpServletRequest request, String uId, String docName, String docSize) throws FileNotFoundException, IOException{
    
        String filePath = "";
        
        try{
        
            ServletContext scxt = request.getSession().getServletContext();
            String webInfPath = scxt.getRealPath("resources");
            
            String curConAdminUId = request.getSession().getAttribute("userId").toString();

            String distDirectory = webInfPath + "\\ContentAdminViewDocs\\"+ curConAdminUId + "\\" + uId  ;
            
            File destinationDir = new File(distDirectory);
            if (!destinationDir.isDirectory()) {
                destinationDir.mkdirs();
            }

            File file = null;
            file = new File(FilePathUtility.getFilePath().get("FileUploadServlet") + uId + "\\" + docName);

            InputStream fis = new FileInputStream(file);
            byte[] buf = new byte[Integer.valueOf(docSize)];

            int offset = 0;
            int numRead = 0;
            while ((offset < buf.length) && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {
                offset += numRead;
            }

            fis.close();
            FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
            buf = fileEncryptDecryptUtil.fileDecryptUtil(buf);
            fileEncryptDecryptUtil = null;

            try {

                OutputStream os = new FileOutputStream(destinationDir+ "\\" + docName, false);
                os.write(buf);           
                os.close();

                filePath = "../resources/ContentAdminViewDocs/"+ curConAdminUId + "/" + uId + "/" + docName;

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        
        return filePath;
    }
}
