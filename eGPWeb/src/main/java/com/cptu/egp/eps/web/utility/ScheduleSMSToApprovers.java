/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.utility;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 *
 * @author Administrator
 */
public class ScheduleSMSToApprovers extends QuartzJobBean {

    CommonScheduledTask commonScheduledTask;
    public static int i = 0;
    public ScheduleSMSToApprovers() {
    }

    public CommonScheduledTask getCommonScheduledTask() {
        return commonScheduledTask;
    }

    public void setCommonScheduledTask(CommonScheduledTask commonScheduledTask) {
        this.commonScheduledTask = commonScheduledTask;
    }

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        if(i == 0){
            i++;
            commonScheduledTask.sendSMSToApprovers();
        }
    }
}
