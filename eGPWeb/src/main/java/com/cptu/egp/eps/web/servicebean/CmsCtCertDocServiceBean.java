/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblCmsCtcertDoc;
import com.cptu.egp.eps.service.serviceinterface.CmsCtcertDocService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Sreenu.Durga
 */
public class CmsCtCertDocServiceBean {

    final Logger logger = Logger.getLogger(CmsCtCertDocServiceBean.class);
    private final static CmsCtcertDocService cmsCtcertDocService =
            (CmsCtcertDocService) AppContext.getSpringBean("CmsCtcertDocService");
    private String logUserId = "0";
    private static final String STARTS = " Starts";
    private static final String ENDS = " Ends";
    private final static String SPACE = "   ";

    public void setLogUserId(String logUserId) {
        cmsCtcertDocService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /***
     * This method inserts a TblCmsCtcertDoc object in database
     * @param tblCmsCtcertDoc
     * @return int
     */
    public int insertCmsCtcertDoc(TblCmsCtcertDoc tblCmsCtcertDoc) {
        logger.debug("insertCmsCtcertDoc : " + logUserId + STARTS);
        int flag = 0;
        try {
            flag = cmsCtcertDocService.insertCmsCtcertDoc(tblCmsCtcertDoc);
        } catch (Exception ex) {
            logger.error("insertCmsCtcertDoc : " + logUserId + SPACE + ex);
            flag = 0;
        }
        logger.debug("insertCmsCtcertDoc : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method updates an TblCmsCtcertDoc object in DB
     * @param tblCmsCtcertDoc
     * @return boolean
     */
    public boolean updateCmsCtcertDoc(TblCmsCtcertDoc tblCmsCtcertDoc) {
        logger.debug("updateCmsCtcertDoc : " + logUserId + STARTS);
        boolean flag;
        try {
            flag = cmsCtcertDocService.updateCmsCtcertDoc(tblCmsCtcertDoc);
        } catch (Exception ex) {
            logger.error("updateCmsCtcertDoc : " + logUserId + SPACE + ex);
            flag = false;
        }
        logger.debug("updateCmsCtcertDoc : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method deletes an TblCmsCtcertDoc object in DB
     * @param tblCmsCtcertDoc
     * @return boolean
     */
    public boolean deleteCmsCtcertDoc(TblCmsCtcertDoc tblCmsCtcertDoc) {
        logger.debug("deleteCmsCtcertDoc : " + logUserId + STARTS);
        boolean flag;
        try {
            flag = cmsCtcertDocService.deleteCmsCtcertDoc(tblCmsCtcertDoc);
        } catch (Exception ex) {
            logger.error("deleteCmsCtcertDoc : " + logUserId + SPACE + ex);
            flag = false;
        }
        logger.debug("deleteCmsCtcertDoc : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method returns all TblCmsCtcertDoc objects from database
     * @return List<TblCmsCtcertDoc>
     */
    public List<TblCmsCtcertDoc> getAllCmsCtcertDoc() {
        logger.debug("getAllCmsCtcertDoc : " + logUserId + STARTS);
        List<TblCmsCtcertDoc> cmsCtCertDocList = new ArrayList<TblCmsCtcertDoc>();
        try {
            cmsCtCertDocList = cmsCtcertDocService.getAllCmsCtcertDoc();
        } catch (Exception ex) {
            logger.error("getAllCmsCtcertDoc : " + logUserId + SPACE + ex);
        }
        logger.debug("getAllCmsCtcertDoc : " + logUserId + ENDS);
        return cmsCtCertDocList;
    }

    /***
     *  This method returns no. of TblCmsCtcertDoc objects from database
     * @return long
     */
    public long getCmsCtcertDocCount() {
        logger.debug("getCmsCtcertDocCount : " + logUserId + STARTS);
        long cmsCtCertDocCount = 0;
        try {
            cmsCtCertDocCount = cmsCtcertDocService.getCmsCtcertDocCount();
        } catch (Exception ex) {
            logger.error("getCmsCtcertDocCount : " + logUserId + SPACE + ex);
        }
        logger.debug("getCmsCtcertDocCount : " + logUserId + ENDS);
        return cmsCtCertDocCount;
    }

    /***
     * This method returns TblCmsCtcertDoc for the given Id
     * @param int ctCertDocId
     * @return TblCmsCtcertDoc
     */
    public TblCmsCtcertDoc getCmsCtcertDoc(int ctCertDocId) {
        logger.debug("getCmsCtcertDoc : " + logUserId + STARTS);
        TblCmsCtcertDoc tblCmsCtCertDoc = null;
        try {
            tblCmsCtCertDoc = cmsCtcertDocService.getCmsCtcertDoc(ctCertDocId);
        } catch (Exception ex) {
            logger.error("getCmsCtcertDoc : " + logUserId + SPACE + ex);
        }

        logger.debug("getCmsCtcertDoc : " + logUserId + ENDS);
        return tblCmsCtCertDoc;
    }

    /***
     * This method gives the list of TblCmsCtcertDoc objects for the given wcCertificateId
     * @param int contractTerminationId
     * @return List<TblCmsCtcertDoc>
     */
    public List<TblCmsCtcertDoc> getAllCmsCtcertDocForCtId(int contractTerminationId) {
        logger.debug("getAllCmsCtcertDocForCtId : " + logUserId + STARTS);
        List<TblCmsCtcertDoc> cmsCtCertDocList = new ArrayList<TblCmsCtcertDoc>();
        try {
            cmsCtCertDocList = cmsCtcertDocService.getAllCmsCtcertDocForCtId(contractTerminationId);
        } catch (Exception ex) {
            logger.error("getAllCmsCtcertDocForCtId : " + logUserId + SPACE + ex);
        }
        logger.debug("getAllCmsCtcertDocForCtId : " + logUserId + ENDS);
        return cmsCtCertDocList;
    }
}
