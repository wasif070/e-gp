/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.service.serviceimpl.TenderOpeningServiceImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class SearchTenPaymentServlet extends HttpServlet {

    /**this servlet handles all the request for the SearchTenPayments.jsp page
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final Logger LOGGER = Logger.getLogger(SearchTenPaymentServlet.class);
    /**
     * This servlet is useful for search and grid of tender payment
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String logUserId = "0";
        try {
            if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
            }
            LOGGER.debug("processRequest : "+logUserId+" Starts");

            /*getting data from database and displaying in SearchTenPayments.jsp jqgrid*/

            String ReqTenderId = request.getParameter("TenderID");
            String ReferenceNo = request.getParameter("ReferenceNo");
            String strPageNo = request.getParameter("pageNo");
            String styleClass = "";
            

            int pageNo = Integer.parseInt(strPageNo);
            String strOffset = request.getParameter("size");
            int recordOffset = Integer.parseInt(strOffset);
            TenderOpeningServiceImpl tenderCommonService1 =
            (TenderOpeningServiceImpl) AppContext.getSpringBean("TenderOpeningService");
            tenderCommonService1.setLogUserId(logUserId);

            String TenderId = "";
            String RefNO = "";

            //if (!ReqTenderId.equals("") && !ReferenceNo.equals("")) {
            if (!"".equals(ReqTenderId) && !"".equals(ReferenceNo)) {
                TenderId = ReqTenderId;
                RefNO = ReferenceNo;
            //} else if (ReqTenderId.equals("") && !ReferenceNo.equals("")) {
            } else if ("".equals(ReqTenderId) && !"".equals(ReferenceNo)) {
                TenderId = null;
                RefNO = ReferenceNo;

            //} else if (!ReqTenderId.equals("") && ReferenceNo.equals("")) {
            } else if (!"".equals(ReqTenderId) && "".equals(ReferenceNo)) {
                TenderId = ReqTenderId;
                RefNO = null;
            //} else if (ReqTenderId.equals("") && ReferenceNo.equals("")) {
            } else if ("".equals(ReqTenderId) && "".equals(ReferenceNo)) {
                TenderId = null;
                RefNO = null;
            }

            int j = 1;
            List<SPCommonSearchData> spcommonSearchData = null;
            spcommonSearchData = tenderCommonService1.getTenderEncriptionData("SearchTenPayment", TenderId, RefNO, strPageNo, strOffset, null, null, null, null, null);
            LOGGER.debug("SearchTenPayment : "+logUserId+" Starts");
            
            /*getting data from database and displaying in SearchTenPayments.jsp jqgrid*/

            if (!spcommonSearchData.isEmpty()) {
                for (int i = 0; i < spcommonSearchData.size(); i++) {
                    if (i % 2 == 0) {
                        styleClass = "bgColor-white";
                    } else {
                        styleClass = "bgColor-Green";
                    }
                    
                    
                    SPCommonSearchData searchdata = spcommonSearchData.get(i);
                    String[] form =searchdata.getFieldName1().split(",");
                    String newDate=searchdata.getFieldName5().replace("01-Jan-1900", "N.A");
                    
                    //Emtaz on 23/April/2016 start
                    String ReplacedStringICTtoICB = searchdata.getFieldName4().replaceAll("ICT", "ICB");
                    String ReplacedStringNCTtoNCB = ReplacedStringICTtoICB.replaceAll("NCT", "NCB");
                    //Emtaz end
                    
                    out.print("<tr class='" + styleClass + "'>");
                    out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * 10 + (i + 1))+ "</td>");
                    out.print("<td class=\"t-align-center\">" + searchdata.getFieldName1() + "</td>");
                    out.print("<td class=\"t-align-center\">" + searchdata.getFieldName2() + "</td>");
                    out.print("<td class=\"t-align-center\">" + searchdata.getFieldName3() + "</td>");
                    out.print("<td class=\"t-align-center\">" + ReplacedStringNCTtoNCB + "</td>");
                    out.print("<td class=\"t-align-center\">"+ newDate + "</td>");
                    out.print("<td class=\"t-align-center\"><a href=\"ForTenderPayment.jsp?tab=8&tenderId="+form[0]+"\" title=\"Dashboard\"><img src=\"../resources/images/Dashboard/dashBoardIcn.png\" alt=\"Dashboard\" /></a></td>");
                    //out.print("<td class=\"t-align-center\"><a href=\"ForPayment.jsp?tab=8&tenderId="+form[0]+"\" title=\"Dashboard\"><img src=\"../resources/images/Dashboard/dashBoardIcn.png\" alt=\"Dashboard\" /></a></td>");
                    out.print("</tr>");
                }
            } else {
                out.print("<tr>");
                out.print("<td id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-left\" style=\"color: red;font-weight: bold\" colspan=\"7\">No Record Found!</td>");
                out.print("</tr>");
            }
            int totalPages = 1;
            if (!spcommonSearchData.isEmpty()) {
                int rowCount = Integer.parseInt(spcommonSearchData.get(0).getFieldName9());
                totalPages = (int) Math.ceil(Math.ceil(rowCount) / recordOffset);
                if (totalPages == 0) {
                    totalPages = 1;
                }
            }
            LOGGER.debug("SearchTenPayment : "+logUserId+" Ends");
            out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
        } finally {
            out.close();
        }
        LOGGER.debug("ProcessRequest : "+logUserId+" Ends");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
