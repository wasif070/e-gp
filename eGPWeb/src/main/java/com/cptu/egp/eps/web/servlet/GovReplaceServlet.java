/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblEmployeeTrasfer;
// Added by Palash
import com.cptu.egp.eps.model.table.TblAdminTransfer;
import com.cptu.egp.eps.model.table.TblAdminMaster;
//End
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.service.serviceimpl.TransferEmployeeServiceImpl;
import com.cptu.egp.eps.service.serviceinterface.GovtUserCreationService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.BanglaNameUtils;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.RandomPassword;
import com.cptu.egp.eps.web.utility.SHA1HashEncryption;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita,TaherT
 */
public class GovReplaceServlet extends HttpServlet {

    private TransferEmployeeServiceImpl transferEmployeeServiceImpl = (TransferEmployeeServiceImpl) AppContext.getSpringBean("TransferEmployeeServiceImpl");
    GovtUserCreationService govtUserCreationService = (GovtUserCreationService) AppContext.getSpringBean("GovtUserCreationService");
    final static Logger LOGGER = Logger.getLogger(GovReplaceServlet.class);
    private String logUserId = "0";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            transferEmployeeServiceImpl.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
            if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
                transferEmployeeServiceImpl.setLogUserId(logUserId);
            }
            LOGGER.debug(request.getParameter("funName") + " : " + logUserId + " Starts");
            if ("retriveInfo".equals(request.getParameter("funName"))) {
                try {
                    // Added by palash, Dohatec
                    //System.out.print(request.getParameter("userTypeId").toString());
                    if (!request.getParameter("userTypeId").equals("4")){
// End
                    int userTypeId = 3;
                    /*if (request.getSession().getAttribute("userId") != null) {
                    userTypeId = Integer.parseInt(request.getSession().getAttribute("userId").toString());
                    }*/
                    out.print(emailInfo(request.getParameter("emailId"), userTypeId));
                    }
               // Added By palash
                    else {
                int userTypeId = 4;
                out.print(emailInfo1(request.getParameter("emailId"), userTypeId));
                }
                    // End
                } catch (Exception e) {
                    LOGGER.error(request.getParameter("funName") + " : " + logUserId + e);
                }
            } else if ("insertGov".equals(request.getParameter("funName"))) {

               // System.out.print(request.getParameter("empId").toString());
               // System.out.print(request.getParameter("adminId").toString());
                // Added by Palash, Dohatec
                if(!request.getParameter("empId").toString().equals("0")){
                    if (transferEmployee(request)) {
                        response.sendRedirect("admin/ManageGovtUser.jsp?msg=pass");
                        //response.sendRedirectFilter("ManageGovtUser.jsp?msg=pass");
                    } else {
                        response.sendRedirect(request.getHeader("referer") + "&msg=fail");
                    }
                }else if (!request.getParameter("adminId").toString().equals("0")){
                    if (transferAdmin(request)) {
                        response.sendRedirect("admin/PEAdminGrid.jsp?userTypeid=4&msg=pass");
                        //response.sendRedirectFilter("ManageGovtUser.jsp?msg=pass");
                    } else {
                        response.sendRedirect(request.getHeader("referer") + "&msg=fail");
                    }
                }


            }
            // End
            LOGGER.debug(request.getParameter("funName") + " : " + logUserId + " Ends");
        } finally {
            out.close();
        }
    }

    private String emailInfo(String emailId, int userTypeId) {
        LOGGER.debug("emailInfo : " + logUserId + " Starts");
        StringBuilder stringBuilder = new StringBuilder();
        List<Object[]> obj = null;
        try {
            obj = transferEmployeeServiceImpl.getDetailsFromEmailId(emailId, userTypeId);
            for (Object[] objects : obj) {
                stringBuilder.append("<tr>");
                stringBuilder.append("<td class='ff' width='200'>Full Name : <span>*</span></td>");
                stringBuilder.append("<td><input class='formTxtBox_1' id='txtFullName' name='fullName' style='width: 200px;' maxlength='101' value ='" + objects[0] + "' type='text'/></td>");
                stringBuilder.append("</tr>");
                stringBuilder.append("<tr>");
//                stringBuilder.append("<td class='ff' width='200'>Name in Dzongkha :</td>");
//                String temp="";
//                if(objects[1] != null){
//                    temp = BanglaNameUtils.getUTFString((byte[]) objects[1]);
//                }
//                stringBuilder.append("<td><input type='text' class='formTxtBox_1' id='txtNameBangla' name='nameBangla' style='width: 200px;' maxlength='101' value ='" + temp + "' /></td>");
                stringBuilder.append("</tr>");
                stringBuilder.append("<tr>");
                stringBuilder.append("<td class='ff' width='200'>CID ID :</td>");
                stringBuilder.append("<td><input class='formTxtBox_1' id='txtNationalID' name='nationalID' style='width: 200px;' maxlength='101' value ='" + objects[2] + "' type='text'/></td>");
                stringBuilder.append("</tr>");
                //employee id
                stringBuilder.append("<tr>");
                stringBuilder.append("<td class='ff' width='200'>Employee ID :</td>");
                stringBuilder.append("<td><input class='formTxtBox_1' id='txtEmployeeID' name='employeeID' style='width: 200px;' maxlength='101' value ='" + objects[4] + "' type='text'/></td>");
                stringBuilder.append("</tr>");
                //end
                stringBuilder.append("<tr>");
                stringBuilder.append("<td class='ff' width='200'>Mobile No. :</td>");
                stringBuilder.append("<td><input class='formTxtBox_1' id='txtMobileNo' name='mobileNo' style='width: 200px;' maxlength='101' value ='" + objects[3] + "' type='text'/></td>");
                stringBuilder.append("</tr>");
                //contact address
                stringBuilder.append("<tr>");
                stringBuilder.append("<td class='ff' width='200'>Contact Address :</td>");
                stringBuilder.append("<td><input class='formTxtBox_1' id='txtContactAddress' name='contactAddress' style='width: 200px;' maxlength='101' value ='" + objects[5] + "' type='text'/></td>");
                stringBuilder.append("</tr>");
                //end
            }
        } catch (Exception e) {
            LOGGER.error("emailInfo : " + logUserId + e);
        }
        LOGGER.debug("emailInfo : " + logUserId + " Ends");
        return stringBuilder.toString();
    }
  private String emailInfo1(String emailId, int userTypeId) {
        LOGGER.debug("emailInfo : " + logUserId + " Starts");
        StringBuilder stringBuilder = new StringBuilder();
        List<Object[]> obj = null;
        try {
            obj = transferEmployeeServiceImpl.getDetailsFromEmailId1(emailId, userTypeId);
            for (Object[] objects : obj) {
                stringBuilder.append("<tr>");
                stringBuilder.append("<td class='ff' width='200'>Full Name : <span>*</span></td>");
                stringBuilder.append("<td><input class='formTxtBox_1' id='txtFullName' name='fullName' style='width: 200px;' maxlength='101' value ='" + objects[0] + "' type='text'/></td>");
                stringBuilder.append("</tr>");
               // stringBuilder.append("<tr>");
                //stringBuilder.append("<td class='ff' width='200'>Name in Bangla :</td>");
              //  String temp="";
              //  if(objects[1] != null){
                 //   temp = BanglaNameUtils.getUTFString((byte[]) objects[1]);
               // }
              //  stringBuilder.append("<td><input type='text' class='formTxtBox_1' id='txtNameBangla' name='nameBangla' style='width: 200px;' maxlength='101' value ='" + temp + "' /></td>");
              //  stringBuilder.append("</tr>");
                stringBuilder.append("<tr>");
                stringBuilder.append("<td class='ff' width='200'>National ID :</td>");
                stringBuilder.append("<td><input class='formTxtBox_1' id='txtNationalID' name='nationalID' style='width: 200px;' maxlength='101' value ='" + objects[2] + "' type='text'/></td>");
                stringBuilder.append("</tr>");
                stringBuilder.append("<tr>");
                stringBuilder.append("<td class='ff' width='200'>Mobile No. :</td>");
                stringBuilder.append("<td><input class='formTxtBox_1' id='txtMobileNo' name='mobileNo' style='width: 200px;' maxlength='101' value ='" + objects[3] + "' type='text'/></td>");
                stringBuilder.append("</tr>");
            }
        } catch (Exception e) {
            LOGGER.error("emailInfo : " + logUserId + e);
        }
        LOGGER.debug("emailInfo : " + logUserId + " Ends");
        return stringBuilder.toString();
    }
    private boolean transferEmployee(HttpServletRequest request) {
        boolean flag = false;
        try {
            LOGGER.debug("transferEmployee : " + logUserId + " Starts");
            TblEmployeeTrasfer trasfer = new TblEmployeeTrasfer();
            trasfer.setAction("Transfer");
            trasfer.setCreatedBy(Integer.parseInt(logUserId));
            trasfer.setEmailId(request.getParameter("hdnEmailId"));
            trasfer.setEmployeeId(Integer.parseInt(request.getParameter("empId")));
            trasfer.setEmployeeName(request.getParameter("fullName"));
            if (request.getParameter("nameBangla") != null && !"".equals(request.getParameter("nameBangla"))) {
                String data = request.getParameter("nameBangla");
                if(data.length()%2!=0){
                    data+=" ";
                }
                trasfer.setEmployeeNameBangla(BanglaNameUtils.getBytes(data));
            }
            trasfer.setFinPowerBy("user");
            trasfer.setIsCurrent("yes");
            trasfer.setMobileNo(request.getParameter("mobileNo"));
            trasfer.setNationalId(request.getParameter("nationalID"));
            trasfer.setUserEmployeeId(request.getParameter("employeeID"));
            trasfer.setContactAddress(request.getParameter("contactAddress"));
            trasfer.setRemarks(request.getParameter("comment"));
            trasfer.setReplacedBy(request.getParameter("replacedBy"));
            trasfer.setReplacedByEmailId(request.getParameter("emailRetrive"));
            trasfer.setTblLoginMaster(new TblLoginMaster(Integer.parseInt(request.getParameter("hdnUserId"))));
            trasfer.setTransferDt(new Date());
            String password = RandomPassword.getRandomString(10);
            if (transferEmployeeServiceImpl.transferEmployee(trasfer, SHA1HashEncryption.encodeStringSHA1(password))) {
                String deptName = request.getParameter("hdndeptName");
                List<Object[]> otherDetails = null;
                otherDetails = govtUserCreationService.getOfficeDetailsforMail(Integer.parseInt(request.getParameter("hdnUserId")));
                StringBuilder PENameAndAddress = new StringBuilder();
                for (Object[] obj : otherDetails) {
                    PENameAndAddress.append(obj[1]);
                    PENameAndAddress.append("<br />");
                    PENameAndAddress.append(obj[2]);
                    PENameAndAddress.append("<br /><br />");
                }
                String emailId[] = new String[2];
                emailId[0] = request.getParameter("hdnEmailId");
                emailId[1] = request.getParameter("emailRetrive");
                if (sendMailAndSMS(emailId, password, deptName, PENameAndAddress, request.getParameter("hdnEmailId"))) {
                    flag = true;
                }
            }
        } catch (Exception e) {
            LOGGER.error("transferEmployee : " + logUserId + e);
        }
        LOGGER.debug("transferEmployee : " + logUserId + " Ends");
        return flag;
    }
// Code Added by palash, dohatec for PE admin transfer.
     private boolean transferAdmin(HttpServletRequest request) {
        boolean flag = false;
        try {
            LOGGER.debug("transferAdmin : " + logUserId + " Starts");
            TblAdminTransfer transfer = new TblAdminTransfer();
            transfer.setTransferedBy(Integer.parseInt(logUserId));
            transfer.setEmailId(request.getParameter("hdnEmailId"));
            transfer.setFullName(request.getParameter("fullName"));
            transfer.setIsCurrent("yes");
            transfer.setTblAdminMaster(new TblAdminMaster(Short.parseShort(request.getParameter("adminId"))));         
            transfer.setNationalId(request.getParameter("nationalID"));
            transfer.setComments(request.getParameter("comment"));
            transfer.setReplacedBy(request.getParameter("replacedBy"));
            transfer.setReplacedByEmailId(request.getParameter("emailRetrive"));
            transfer.setMobileNo(request.getParameter("mobileNo"));
            transfer.setTblLoginMaster(new TblLoginMaster(Integer.parseInt(request.getParameter("hdnUserId"))));
            transfer.setTransferDt(new Date());
            String password = RandomPassword.getRandomString(10);
            if (transferEmployeeServiceImpl.transferAdmin(transfer, SHA1HashEncryption.encodeStringSHA1(password))) {
                String mobileNo = request.getParameter("mobileNo");
               
                
                List<Object[]> otherDetails = null;
                //Object[] otherDetails = null;
                otherDetails = govtUserCreationService.getPEAdminOfficeDetailsforMail(Integer.parseInt(request.getParameter("hdnUserId")));
                
                StringBuilder PEANameAndAddress = new StringBuilder();
                for (Object[] obj : otherDetails) {
                    PEANameAndAddress.append(obj[2]);
                    PEANameAndAddress.append("<br />");
                    PEANameAndAddress.append(obj[3]);
                    PEANameAndAddress.append("<br /><br />");
                }
                String emailId[] = new String[1];
                emailId[0] = request.getParameter("hdnEmailId");
                //emailId[1] = request.getParameter("emailRetrive");
                if (sendMailAndSMSPEAdmin(emailId, password, mobileNo, PEANameAndAddress, request.getParameter("hdnEmailId"))) {
                                                   flag = true;
                }
            }
        }catch (Exception e) {
            LOGGER.error("transferAdmin : " + logUserId + e);
        }
        LOGGER.debug("transferAdmin : " + logUserId + " Ends");
        return flag;

    }
//  End by Palash, Dohatec
    private boolean sendMailAndSMS(String[] emailId, String password, String deptName, StringBuilder PENameAndAddress, String loginId) {
        LOGGER.debug("sendMailAndSMS " + logUserId + " Starts:");
        boolean mailSent = false;
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            for (int i = 0; i < emailId.length; i++) {
                if (!("").equals(emailId[i].trim())) {
                    String mailText = mailContentUtility.transferGovUserContent(loginId, password, deptName, PENameAndAddress);
                    sendMessageUtil.setEmailTo(emailId);
                    sendMessageUtil.setEmailSub("e-GP System: Your new login account");
                    sendMessageUtil.setEmailMessage(mailText);
                    mailSent = true;
                    sendMessageUtil.sendEmail();
                    mailText = null;
                }
            }
            mailContentUtility = null;
            sendMessageUtil = null;
        } catch (Exception ex) {
            LOGGER.error("sendMailAndSMS " + logUserId + " :" + ex);
        }
        LOGGER.debug("sendMailAndSMS " + logUserId + " Ends:");
        return mailSent;
    }
// Code added by Palash, Dohatec
 private boolean sendMailAndSMSPEAdmin(String[] emailId, String password, String mobileNo, StringBuilder PEANameAndAddress, String loginId) {
        LOGGER.debug("sendMailAndSMSPEAdmin " + logUserId + " Starts:");
        boolean mailSent = false;
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            for (int i = 0; i < emailId.length; i++) {
                if (!("").equals(emailId[i].trim())) {
                    String mailText = mailContentUtility.transferPEAdminContent(loginId, password, mobileNo, PEANameAndAddress);
                    sendMessageUtil.setEmailTo(emailId);
                    sendMessageUtil.setEmailSub("e-GP System: Your new login account");
                    sendMessageUtil.setEmailMessage(mailText);
                    mailSent = true;
                    sendMessageUtil.sendEmail();
                    mailText = null;
                }
            }
            mailContentUtility = null;
            sendMessageUtil = null;
        } catch (Exception ex) {
            LOGGER.error("sendMailAndSMSPEAdmin " + logUserId + " :" + ex);
        }
        LOGGER.debug("sendMailAndSMSPEAdmin " + logUserId + " Ends:");
        return mailSent;
    }
 // End by Palash.
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
