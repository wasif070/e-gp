/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

import com.cptu.egp.eps.model.table.TblWsLog;
import com.cptu.egp.eps.model.table.TblWsMaster;
import com.cptu.egp.eps.model.table.TblWsOrgMaster;
import com.cptu.egp.eps.service.serviceinterface.WsLogService;
import com.cptu.egp.eps.service.serviceinterface.WsMasterService;
import com.cptu.egp.eps.service.serviceinterface.WsOrgMasterService;
import com.cptu.egp.eps.web.xmlpojos.WSResponseObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;
import java.net.InetAddress;

/**
 * This is a utility class for web services.
 * This class methods are used in the web-services only.
 * @author Sreenu
 */
public class WebServiceUtil {

    private static final Logger LOGGER =
            Logger.getLogger(WebServiceUtil.class);
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";
    private static final String WSMASTERSERVICE = "WsMasterService";
    private static final String WSORGMASTERSERVICE = "WsOrgMasterService";
    private static final String WSLOGSERVICE = "WsLogService";
    public static final String APPEND_CHARACTER = "^";

    /***
     * This method inserts a log record in the data base while any one uses any
     * of the web-service.
     * @param int webServiceId
     * @param String emailId
     * @param String searchKey
     */
    public static boolean insertLogRecord(int webServiceId, String emailId,
            String searchKey) {
        LOGGER.debug("insertLogRecord : " + LOGGERSTART);
        boolean isProperInsertion = false;
        WsMasterService wsMasterService = null;
        WsLogService wsLogService = null;
        WsOrgMasterService wsOrgMasterService = null;
        TblWsOrgMaster wsOrgMaster = null;
        TblWsLog tblWsLog = new TblWsLog();
        TblWsMaster tblWsMaster = null;
        String searchKeyString = checkNullInSearchString(searchKey);
        try {
            wsMasterService = (WsMasterService) AppContext.getSpringBean(WSMASTERSERVICE);
            wsLogService = (WsLogService) AppContext.getSpringBean(WSLOGSERVICE);
            wsOrgMasterService = (WsOrgMasterService) AppContext.getSpringBean(WSORGMASTERSERVICE);
            wsOrgMaster = wsOrgMasterService.getTblWsOrgMaster(emailId);
            tblWsMaster = wsMasterService.getTblWsMaster(webServiceId);
            if (wsOrgMaster != null && tblWsMaster != null) {
                String lgIpMac = emailId + APPEND_CHARACTER + getSytemIp();
                tblWsLog.setIsAuthenticate(tblWsMaster.getIsAuthenticate());
                tblWsLog.setLgIpMac(lgIpMac);
                tblWsLog.setTblWsMaster(tblWsMaster);
                tblWsLog.setTblWsOrgMaster(wsOrgMaster);
                tblWsLog.setVisitDate(getCurrentDate());
                tblWsLog.setVisitKey(searchKeyString);
                tblWsLog.setWsDesc(tblWsMaster.getWsDesc());
                wsLogService.insertWsLog(tblWsLog);
                isProperInsertion = true;
            }
        } catch (Exception e) {
            LOGGER.error("insertLogRecord : " + e);
        }
        LOGGER.debug("insertLogRecord : " + LOGGEREND);
        return isProperInsertion;
    }

    /***
     * This method checks the connection is available or not, with
     * the given search service name as a parameter.
     * if Connection is available return TRUE
     * if Connection is not available return FALSE
     * @param String searchServiceName
     * @return boolean
     */
    public static boolean isConnectionAvailable(String searchServiceName) {
        LOGGER.debug("isConnectionAvailable : " + LOGGERSTART);
        boolean isAvailable = false;
        Object serviceObj = null;
        try {
            serviceObj = AppContext.getSpringBean(searchServiceName);
        } catch (Exception e) {
            isAvailable = false;
        }
        if (serviceObj != null) {
            isAvailable = true;
        }
        LOGGER.debug("isConnectionAvailable : " + LOGGEREND);
        return isAvailable;
    }

    /***
     * This method takes web-service id as a parameter and returns the following
     * If that web-service need authentication returns TRUE else
     * returns FALSE
     * @param int webServiceId
     * @return boolean
     */
    public static boolean isAuthenticationRequired(int webServiceId) {
        LOGGER.debug("isAuthenticationRequired : " + LOGGERSTART);
        boolean isRequired = false;
        TblWsMaster tblWsMaster = null;
        WsMasterService wsMasterService = null;
        try {
            wsMasterService = (WsMasterService) AppContext.getSpringBean(WSMASTERSERVICE);
            tblWsMaster = wsMasterService.getTblWsMaster(webServiceId);
        } catch (Exception e) {
            LOGGER.error("isAuthenticationRequired : " + e);
        }
        if (tblWsMaster != null) {
            String isRequiredString = tblWsMaster.getIsAuthenticate();
            if ("Y".equalsIgnoreCase(isRequiredString)) {
                isRequired = true;
            } else {
                isRequired = false;
            }
        } else {
            isRequired = false;
        }
        LOGGER.debug("isAuthenticationRequired : " + LOGGEREND);
        return isRequired;
    }

    /***
     * This method takes email-id and password as parameters and checks authentication.
     * If authentication success it returns TRUE else returns FALSE
     * @param String username
     * @param String password
     * @return boolean
     */
    public static boolean checkAuthentication(String username, String password) {
        LOGGER.debug("checkAuthentication : " + LOGGERSTART);
        boolean isValidUser = false;
        if (username != null && password != null
                && username.length() != 0 && password.length() != 0) {
            // get the object from database
            WsOrgMasterService wsOrgMasterService = null;
            TblWsOrgMaster wsOrgMaster = null;
            try {
                wsOrgMasterService = (WsOrgMasterService) AppContext.getSpringBean(WSORGMASTERSERVICE);
                wsOrgMaster = wsOrgMasterService.getTblWsOrgMaster(username);
            } catch (Exception e) {
                LOGGER.error("checkAuthentication : " + e);
            }
            // checking the user name is valid or not
            if (wsOrgMaster == null) {
                isValidUser = false;
            } else if ("N".equalsIgnoreCase(wsOrgMaster.getIsDeleted())) {// checking status
                if (wsOrgMaster.getLoginPass().equals(SHA1HashEncryption.encodeStringSHA1(password))) {
                    isValidUser = true;
                }
            } else {
                isValidUser = false;
            }//wsOrgMaster checking null
        } else {
            isValidUser = false;
        }//user name is null or not
        LOGGER.debug("checkAuthentication : " + LOGGEREND);
        return isValidUser;
    }

    /***
     * This method takes web-service id as a parameter and checks that web service
     * is active or not. If it is active it returns TRUE else returns FALSE
     * @param int webServiceId
     * @return boolean
     */
    public static boolean isActive(int webServiceId) {
        LOGGER.debug("isActive : " + LOGGERSTART);
        boolean isActive = false;
        TblWsMaster tblWsMaster = null;
        WsMasterService wsMasterService = null;
        try {
            wsMasterService = (WsMasterService) AppContext.getSpringBean(WSMASTERSERVICE);
            tblWsMaster = wsMasterService.getTblWsMaster(webServiceId);
        } catch (Exception e) {
            LOGGER.error("isActive : " + e);
        }
        if (tblWsMaster != null) {
            String isRequiredString = tblWsMaster.getIsActive();
            if ("Y".equalsIgnoreCase(isRequiredString)) {
                isActive = true;
            } else {
                isActive = false;
            }
        } else {
            isActive = false;
        }
        LOGGER.debug("isActive : " + LOGGEREND);
        return isActive;
    }

    /***
     * This method take user's mail id and web-service id as a parameters and
     * checks that the user have to access this web service or not.
     * If that user have access it returns TRUE else returns FALSE
     * @param String emailId
     * @param int webServiceId
     * @return
     */
    public static boolean isThisOrgHaveWSRights(String emailId, int webServiceId) {
        boolean haveRights = false;
        String webServiceIdString = webServiceId + "";
        LOGGER.debug("isThisOrgHaveWSRights : " + LOGGERSTART);
        // get the object from database
        WsOrgMasterService wsOrgMasterService = null;
        TblWsOrgMaster wsOrgMaster = null;
        try {
            wsOrgMasterService = (WsOrgMasterService) AppContext.getSpringBean(WSORGMASTERSERVICE);
            wsOrgMaster = wsOrgMasterService.getTblWsOrgMaster(emailId);
        } catch (Exception e) {
            LOGGER.error("isThisOrgHaveWSRights : " + e);
        }
        // checking the user name is valid or not
        if (wsOrgMaster == null) {
            haveRights = false;
        } else {
            String wsRights[] = getStringArray(wsOrgMaster.getWsRights());
            for (int i = 0; i < wsRights.length; i++) {
                String tempString = wsRights[i];
                if (tempString.equals(webServiceIdString)) {
                    haveRights = true;
                    break;
                }
            }
        }
        LOGGER.debug("isThisOrgHaveWSRights : " + LOGGEREND);
        return haveRights;
    }

    /***
     * This method takes web-service id as a parameter and checks that web service
     * is need to ask the password or not. If it is need it returns TRUE else returns FALSE
     * @param webServiceId
     * @return boolean
     */
    public static boolean isNeedToPromtForPassword(int webServiceId) {
        LOGGER.debug("isNeedToPromtForPassword : " + LOGGERSTART);
        boolean isNeed = false;
        boolean isWSActive = WebServiceUtil.isActive(webServiceId);
        if (isWSActive) {
            isNeed = WebServiceUtil.isAuthenticationRequired(webServiceId);
        }
        LOGGER.debug("isNeedToPromtForPassword : " + LOGGEREND);
        return isNeed;
    }

    /***
     * This method take care about the Secondary verification of a web-service i.e.
     * initially it checks the connection availability and activity of web-services.
     * then it checks of user's rights regarding that web-service, and sets proper
     * constants and proper response messages to the wsResponseObject.
     * <b>If the password verification is not required then this method can be used</b>
     * @param webServiceId
     * @param springServiceName
     * @param username
     * @return WSResponseObject
     */
    public static WSResponseObject secondaryVerification(int webServiceId, String springServiceName,
            String username) {
        LOGGER.debug("secondaryVerification : " + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        boolean isListDataRequired = false;
        boolean isConnAvailble = WebServiceUtil.isConnectionAvailable(springServiceName);
        if (isConnAvailble) {
            boolean isWSActive = WebServiceUtil.isActive(webServiceId);
            if (isWSActive) {
                boolean orgHaveRights = WebServiceUtil.isThisOrgHaveWSRights(username, webServiceId);
                if (orgHaveRights) {
                    isListDataRequired = true;
                } else {// if user have not rights
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RIGHTS_TO_ACCESS_WEB_SERVICE);
                    wSResponseObject.setResponseMessage(WSResponseObject.ACCESS_RIGHTS_FAIL);
                }
            } else {//if service is inactive
                wSResponseObject.setResponseConstant(WSResponseObject.WEB_SERVICE_INACTIVE);
                wSResponseObject.setResponseMessage(WSResponseObject.WEB_SERVICE_IS_INACTIVE);
            }
        } else {//if connection is not avalable
            wSResponseObject.setResponseConstant(WSResponseObject.CONNECTION_ERROR);
            wSResponseObject.setResponseMessage(WSResponseObject.CONNECTION_ERROR_MSG);
        }
        wSResponseObject.setIsListRequired(isListDataRequired);
        LOGGER.debug("secondaryVerification : " + LOGGEREND);
        return wSResponseObject;
    }

    /***
     * This method take care about the initial verification of a web-service i.e.
     * initial it checks the connection availability and activity of web-services.
     * then it checks regarding authentication required or not, 
     * if required it checks the authentication, later it goes to checking of user's
     * rights regarding that web-service, and sets proper constants and proper
     * response messages to the wsResponseObject.
     * @param int webServiceId
     * @param String springServiceName
     * @param String username
     * @param String password
     * @return WSResponseObject
     */
    public static WSResponseObject initialVerification(int webServiceId, String springServiceName,
            String username, String password) {
        LOGGER.debug("initialVerification : " + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        boolean isListDataRequired = false;
        boolean isConnAvailble = WebServiceUtil.isConnectionAvailable(springServiceName);
        if (isConnAvailble) {
            boolean isWSActive = WebServiceUtil.isActive(webServiceId);
            if (isWSActive) {
                boolean isAuthRequired = WebServiceUtil.isAuthenticationRequired(webServiceId);
                if (isAuthRequired) {
                    boolean isValidUser = WebServiceUtil.checkAuthentication(username, password);
                    if (isValidUser) {
                        boolean orgHaveRights = WebServiceUtil.isThisOrgHaveWSRights(username, webServiceId);
                        if (orgHaveRights) {
                            isListDataRequired = true;
                        } else {// if user have not rights
                            wSResponseObject.setResponseConstant(WSResponseObject.NO_RIGHTS_TO_ACCESS_WEB_SERVICE);
                            wSResponseObject.setResponseMessage(WSResponseObject.ACCESS_RIGHTS_FAIL);
                        }
                    } else {//if authentication fails
                        wSResponseObject.setResponseConstant(WSResponseObject.AUTHENTICATION_FAIL_EXCEPTION);
                        wSResponseObject.setResponseMessage(WSResponseObject.AUTHENTICATION_FAIL_ERROR_MSG);
                    }
                } else {//if authentication not requied
                    isListDataRequired = true;
                }
            } else {//if service is inactive
                wSResponseObject.setResponseConstant(WSResponseObject.WEB_SERVICE_INACTIVE);
                wSResponseObject.setResponseMessage(WSResponseObject.WEB_SERVICE_IS_INACTIVE);
            }
        } else {//if connection is not avalable
            wSResponseObject.setResponseConstant(WSResponseObject.CONNECTION_ERROR);
            wSResponseObject.setResponseMessage(WSResponseObject.CONNECTION_ERROR_MSG);
        }
        wSResponseObject.setIsListRequired(isListDataRequired);
        LOGGER.debug("initialVerification : " + LOGGEREND);
        return wSResponseObject;
    }

    /***
     * This util method checks any null values in the string
     * and replaces those null values with an empty string
     * @param filedString
     * @return String length
     */
    public static String checkNullInSearchString(String searchString) {
        LOGGER.debug("checkNullInSearchString : " + LOGGERSTART);
        String tempString = searchString.replace("null^", "");
        LOGGER.debug("checkNullInSearchString : " + LOGGEREND);
        return tempString;
    }

    /**
     * This method takes the Integer as a parameter and converts into int value.
     * If Integer is a null then it returns ZERO value
     * @param Integer
     * @return int
     */
    public static int getDefaultIntValue(Integer value) {
        LOGGER.debug("getDefaultIntValue : " + LOGGERSTART);
        int integerValue = 0;
        try {
            if (value != null) {
                integerValue = value.intValue();
            }
        } catch (NumberFormatException exception) {
            integerValue = 0;
        }
        LOGGER.debug("getDefaultIntValue : " + LOGGEREND);
        return integerValue;
    }

    /**
     * This method takes the String as a parameter and checks the string is
     * empty or not.If String is a null then it returns emptyString
     * @param String
     * @return int
     */
    public static String getDefaultStringValue(String valueInString) {
        LOGGER.debug("getDefaultStringValue : " + LOGGERSTART);
        String tempString = "";
        if (valueInString != null && !"".equalsIgnoreCase(valueInString)) {
            tempString = valueInString;
        }
        LOGGER.debug("getDefaultStringValue : " + LOGGEREND);
        return tempString;
    }

    /**
     * This method takes the BigDecimal as a parameter and checks NULL or not
     * If BigDecimal is a null then it returns ZERO value
     * @param BigDecimal value
     * @return BigDecimal
     */
    public static BigDecimal getDefaultDecimalValue(BigDecimal value) {
        LOGGER.debug("getDefaultDecimalValue : " + LOGGERSTART);
        BigDecimal decimalValue = new BigDecimal(0);
        try {
            if (value != null) {
                decimalValue = value;
            }
        } catch (NumberFormatException exception) {
            decimalValue = new BigDecimal(0);
        }
        LOGGER.debug("getDefaultDecimalValue : " + LOGGEREND);
        return decimalValue;
    }

    /***
     * This method returns whether the mainString contains the key string or not
     * @param String mainString
     * @param String keyString
     * @return
     */
    public static boolean isStringContainsKey(String mainString, String keyString) {
        LOGGER.debug("isStringContainsKey : " + LOGGERSTART);
        boolean isContains = false;
        mainString = mainString.toUpperCase();
        keyString = keyString.toUpperCase();
        int index = mainString.indexOf(keyString);
        //indexOf function returns -1 if keystring is not conain in the mainString
        //else it retruns the index. that +ve value
        if (index == -1) {
            isContains = false;
        } else {
            isContains = true;
        }
        LOGGER.debug("getProcuremisStringContainsKeyentNatures : " + LOGGEREND);
        return isContains;
    }

    /**
     * This method takes the String as a parameter and converts into Date value
     * and returns as a String.If Date is a null then it returns NULL value
     * @param String Date
     * @return String
     */
    public static String getDefaultDate(String date) {
        LOGGER.debug("getDefaultDate : " + LOGGERSTART);
        String tempDate = null;
        if (date != null && !"".equalsIgnoreCase(date)) {
            tempDate = DateUtils.formatStrToStr(date);
            if (tempDate.equals("")) {
                tempDate = null;
            }
        }
        LOGGER.debug("getDefaultDate : " + LOGGEREND);
        return tempDate;
    }

    /***
     * This method returns the client system IP.
     * To get the client IP we use a third party web-service here.
     * some times it may fail and returns the null string.
     * That time using "InetAddress" we can get IP of client.
     * But "InetAddress" may fails if the client system is connected
     * in a local LAN it returns the local IP only.
     * @return String IPAddress
     */
    public static String getSytemIp() {
        LOGGER.debug("getSytemIp : " + LOGGERSTART);
        URL whatismyip = null;
        String ip = null;
        try {
            LOGGER.debug("getSytemIp(URL1) : " + LOGGERSTART);
            String UrlName = "http://automation.whatismyip.com/n09230945.asp";
            whatismyip = new URL(UrlName);
            BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
            ip = in.readLine();
        } catch (Exception ex) {
            LOGGER.error("getSytemIp(URL1) : " + ex);
        }
        if (ip == null || ip.length() == 0) {
            LOGGER.debug("getSytemIp(URL2) : " + LOGGERSTART);
            String UrlName = "http://xcski.net/cgi-bin/you_are.cgi";
            try {
                whatismyip = new URL(UrlName);
                BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
                ip = in.readLine();
            } catch (Exception ex) {
                LOGGER.error("getSytemIp(URL2) : " + ex);
            }
        }
        if (ip == null || ip.length() == 0) {
            try {
                LOGGER.debug("getSytemIp(InetAddress) : " + LOGGERSTART);
                InetAddress ownIP = InetAddress.getLocalHost();
                ip = ownIP.getHostAddress();
            } catch (Exception ex) {
                LOGGER.error("getSytemIp(InetAddress) : " + ex);
            }
        }
        LOGGER.debug("getSytemIp : " + LOGGEREND);
        return ip;
    }

    /***
     * This method returns the current Date 
     * @return Date
     */
    public static Date getCurrentDate() {
        LOGGER.debug("getCurrentDate : " + LOGGERSTART);
        Calendar currentDate = Calendar.getInstance();
        Date dateNow = (currentDate.getTime());
        LOGGER.debug("getCurrentDate : " + LOGGEREND);
        return dateNow;
    }

    /***
     * This the splits a String, into array of String with String token
     * @param wsRightsString
     * @return String[]
     */
    public static String[] getStringArray(String wsRightsString) {
        LOGGER.debug("getStringArray : " + LOGGERSTART);
        String[] wsRights = wsRightsString.split("\\^");
        LOGGER.debug("getStringArray : " + LOGGEREND);
        return wsRights;
    }

    /**
     * This method takes the String as a parameter and converts into int value.
     * If String is a null then it returns ZERO value
     * @param String
     * @return int
     */
    public static int getIntegerValue(String valueInString) {
        LOGGER.debug("getIntegerValue : " + LOGGERSTART);
        valueInString = valueInString.trim();
        int intValue = 0;
        try {
            if (valueInString != null && !"".equalsIgnoreCase(valueInString)) {
                intValue = Integer.parseInt(valueInString);
            }
        } catch (NumberFormatException exception) {
            intValue = 0;
        }
        LOGGER.debug("getIntegerValue : " + LOGGERSTART);
        return intValue;
    }
}
