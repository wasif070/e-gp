/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblNegotiationForms;
import com.cptu.egp.eps.service.serviceimpl.CommonServiceImpl;
import com.cptu.egp.eps.service.serviceimpl.NegotiationFormsImpl;
import com.cptu.egp.eps.service.serviceimpl.NegotiationProcessImpl;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.*;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 *
 * @author Administrator
 */
public class NegotiationFormsSrBean extends HttpServlet {

    NegotiationFormsImpl negotiationFormsImpl = (NegotiationFormsImpl) AppContext.getSpringBean("NegotiationFormsImpl");
    NegotiationProcessImpl negotiationProcessImpl = (NegotiationProcessImpl) AppContext.getSpringBean("NegotiationProcessImpl");
    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
    TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
     private UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
         // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer"));
            String idType="tenderId";
            int auditId=0;
            String auditAction=null;
            String moduleName=EgpModule.Negotiation.getName();
            String auditremarks= "";
                
                
        try {

            String contextPath = request.getContextPath();
            int userId = 0;
            userId = Integer.parseInt(request.getSession().getAttribute("userId").toString());

            if("negotiateform".equalsIgnoreCase(request.getParameter("action"))){

                String  mappedDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()) ;

                int negId = 0;
                int formId = 0;
                int tenderId = 0;
                String setData = "";
                String whereCond = "";
                auditAction = "Process Negotiation (Form Selection) by TEC-CP";
                TblNegotiationForms tblNegotiationForms = new TblNegotiationForms();

                if(request.getParameter("hidnegId")!=null){
                    negId = Integer.parseInt(request.getParameter("hidnegId"));
                }
                if(request.getParameter("hidtenderId")!=null){
                    tenderId = Integer.parseInt(request.getParameter("hidtenderId"));
                }

                String[] forms = request.getParameterValues("nameform");
                String xml = "<root>";

                for(int i=0;i<forms.length;i++)
                {
                    formId = Integer.parseInt(forms[i]);
                    xml += "<tbl_NegotiationForms "
                        + "formId=\""+formId+"\" "
                        + "mappedBy=\""+ userId +"\" "
                        + "mappedDate=\""+ mappedDate +"\" "
                        + "negId=\""+ negId +"\" "
                        + "/>";
                }

                xml += "</root>";
                whereCond = "negId ="+negId;

                try{
                    List<CommonMsgChk> listspTendorCommon = negotiationFormsImpl.InsUpdateNegOperationDetailsByXML("insdel", "tbl_NegotiationForms", xml, whereCond);
                } catch (Exception e) {
                    System.out.println("Exception caught at NegotiationFormsSrBean "+ e);
                    auditAction = "Error in "+auditAction+" : "+e.getMessage();
                }finally{
                    makeAuditTrailService.generateAudit(objAuditTrail, tenderId, idType, moduleName, auditAction, auditremarks); 
                }

                response.sendRedirect(contextPath+"/officer/NegForms.jsp?tenderId="+tenderId+"&uId="+request.getParameter("negUserId")+"&negId="+negId);
            }

            if("negotiateformbid".equalsIgnoreCase(request.getParameter("action"))){

                int negId = 0;
                int tenderId = 0;

                String setData = "";
                String whereCond = "";
                String remarks = "";
                String formType = "";

                if(request.getParameter("hidnegId")!=null){
                    negId = Integer.parseInt(request.getParameter("hidnegId"));
                }
                if(request.getParameter("hidtenderId")!=null){
                    tenderId = Integer.parseInt(request.getParameter("hidtenderId"));
                }
                if(request.getParameter("txtRemarks")!=null){
                    remarks = request.getParameter("txtRemarks");
                }

                setData = "negFinalSub='Yes', negFinalSubRemarks = '"+remarks+"'";
                whereCond = "";
                whereCond = "negId = "+negId;

                try{
                    List<CommonMsgChk> listspTendorCommon = negotiationProcessImpl.InsUpdateNegOperationDetailsByXML("updatetable", "tbl_Negotiation", setData, whereCond);
                } catch (Exception e) {
                    System.out.println("Exception caught at NegotiationSrBean "+ e);
                }

                response.sendRedirect(contextPath+"/officer/NegForms.jsp?tenderId="+tenderId+"&uId="+request.getParameter("negUserId")+"&negId="+negId);
            }

            if("approveServiceFormBid".equalsIgnoreCase(request.getParameter("action")))
            {
                int negId = 0;
                int tenderId = 0;

                String setData = "";
                String whereCond = "";
                String remarks = "";
                String formType = "";
                
               

                if(request.getParameter("hidnegId")!=null){
                    negId = Integer.parseInt(request.getParameter("hidnegId"));
                }
                if(request.getParameter("hidtenderId")!=null){
                    tenderId = Integer.parseInt(request.getParameter("hidtenderId"));
                }

                auditId=tenderId;
                auditAction="Bidder has done Final Submission for Negotiation";
                auditremarks="Bidder(User id): "+request.getSession().getAttribute("userId")+" has done Final Submission for Negotiation Id:"+ negId;
                
                setData = "bidAggree='Yes'";
                whereCond = "";
                whereCond = "negId = "+negId;

                try{
                    List<CommonMsgChk> listspTendorCommon = negotiationProcessImpl.InsUpdateNegOperationDetailsByXML("updatetable", "tbl_NegNotifyTenderer", setData, whereCond);
                    ///--------------------- code for send mail and message -------------------------
                   // System.out.println("ref no="+request.getParameter("tenderRefNo"));
                      sendMailTendererCompliteNegFillFormRequest(tenderId+"", request.getParameter("tenderRefNo"));
                    /// -------------------- code end for send mail and message ----------------------
                } catch (Exception e) {
                    System.out.println("Exception caught at NegotiationSrBean "+ e);
                    auditAction="Error in "+auditAction;
                }
                finally
                {
                   
                    makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, auditremarks);
                }
                
                response.sendRedirect(contextPath+"/tenderer/NegoTendererProcess.jsp?tenderId="+tenderId);
            }

            if("approveTenderNegBid".equalsIgnoreCase(request.getParameter("action")))
            {
                int negId = 0;
                int tenderId = 0;

                String setData = "";
                String whereCond = "";
                String remarks = "";
                String formType = "";
                String Status = "Reject";
                String officerremarks = "";
                String negOfficerAggreeDt= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()) ;
                String finalStatus = "";
                String usrId = "";
                String tendRefNo = "";
                

                if(request.getParameter("hidnegId")!=null){
                    negId = Integer.parseInt(request.getParameter("hidnegId"));
                }
                if(request.getParameter("hidtenderId")!=null){
                    tenderId = Integer.parseInt(request.getParameter("hidtenderId"));
                }
                if(request.getParameter("tendRefNo")!=null){
                    tendRefNo = request.getParameter("tendRefNo");
                }
                if(request.getParameter("finalSubStatus")!=null){
                    finalStatus = request.getParameter("finalSubStatus");
                }
                if("yes".equalsIgnoreCase(finalStatus)){
                    if(request.getParameter("nameStatus")!=null){
                        Status = request.getParameter("nameStatus");
                    }
                }
                if(request.getParameter("txtRemarks")!=null){
                    officerremarks = request.getParameter("txtRemarks");
                }
                if(request.getParameter("usrId")!=null){
                    usrId = request.getParameter("usrId");
                }
                Object[] objects =negotiationProcessImpl.getTendererMailAndMobileNo(Integer.parseInt(usrId));
                String mail = (String) objects[0];
                sendMessageNeg(String.valueOf(tenderId), tendRefNo, Status, mail,finalStatus);
                auditAction = "TEC-CP "+Status+" Negotiated/Revised Form";
                if("resend".equalsIgnoreCase(Status)){
                    negotiationProcessImpl.updateWhenResend(negId,officerremarks);
                    setData = "negOfficeAggree = '"+Status+"'";
                    whereCond = "negId = "+negId;
                    negotiationProcessImpl.InsUpdateNegOperationDetailsByXML("updatetable", "tbl_Negotiation", setData, whereCond);
                }else{
                    setData = "negOfficeAggree = '"+Status+"', negOfficerAggreeRemarks = '"+officerremarks+"', negOfficerAggreeDt = '"+negOfficerAggreeDt+"'";
                    whereCond = "";
                    whereCond = "negId = "+negId;

                    try{
                        List<CommonMsgChk> listspTendorCommon = negotiationProcessImpl.InsUpdateNegOperationDetailsByXML("updatetable", "tbl_Negotiation", setData, whereCond);
                    } catch (Exception e) {
                        System.out.println("Exception caught at NegotiationSrBean "+ e);
                        auditAction = "Error in "+auditAction+" : "+e.getMessage();
                    }finally{
                        makeAuditTrailService.generateAudit(objAuditTrail, tenderId, idType, moduleName, auditAction, officerremarks); 
                    }
                    // This function is call to dump Bidd data into Neg tables.
                    /*if("accept".equalsIgnoreCase(Status))
                    {
                        negotiationProcessImpl.dumpNegForms(tenderId+"", usrId, negId+"");
                    }*/
                }
                response.sendRedirect(contextPath+"/officer/NegotiationProcess.jsp?tenderId="+tenderId);
            }
        }catch(Exception ex){
            System.out.println("Exception Found in NegotiationFormsSrBean :"+ex);
        }finally {
            out.close();
        }
    }
    
     public void sendMailTendererCompliteNegFillFormRequest(String tenderId,String refNo)
    {
        try
        {
            List<SPTenderCommonData> commonData = tenderCS.returndata("GetTecCpEmail", tenderId, null);
            // System.out.println("------------------------ sednding mail----------"+refNo);
            if(commonData != null && !commonData.isEmpty())
            {
                String mails[]  = {commonData.get(0).getFieldName1()};
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                sendMessageUtil.setEmailTo(mails);
                MailContentUtility mailContentUtility = new MailContentUtility();
                String mailText = mailContentUtility.mailSendTOCPForTendererCompliteNegFillForm(tenderId, refNo);
                sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                sendMessageUtil.setEmailSub("e-GP: Forms revised during Negotiation");
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                
                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                registerService.contentAdmMsgBox(mails[0], XMLReader.getMessage("emailIdNoReply"), "e-GP: Forms revised during Negotiation",msgBoxContentUtility.messageSendTOCPForTendererCompliteNegFillForm(tenderId, refNo));              

             //   System.out.println("------------------------mail send----------");
            }
            else
            {
                System.out.println("email id not found");
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    public boolean sendMessageNeg(String tenderId,String tendRefNo,String status,String mailId,String finalStatus){
        boolean mailSent = false;
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            String[] mailTo = {mailId};
            MailContentUtility mailContentUtility = new MailContentUtility();
            String mailStatus = "";
            if("Resend".equalsIgnoreCase(status)){
                mailStatus = "This is to inform you that PE has resent the forms for revision during Negotiation for below mentioned tender: <br />";
            }else
            {
                if(status.equalsIgnoreCase("Reject"))
                    status="Rejected";
                else
                    status="Accepted";
                if("yes".equalsIgnoreCase(finalStatus))
                {
                    mailStatus=  "This is to inform you that PE has "+status+" form's revision during Negotiation for below mentioned tender: <br />";
                }else{
                    mailStatus = "This is to inform you that PE has "+status+" Negotiation for below mentioned tender:<br/>";
                }
            }
            List list = mailContentUtility.getNegAppRejContent(tenderId,tendRefNo,mailStatus,status);
            String mailSub = list.get(0).toString();
            String mailText = list.get(1).toString()+list.get(2).toString()+list.get(3).toString()+list.get(4).toString()+list.get(5).toString();
            sendMessageUtil.setEmailTo(mailTo);
            sendMessageUtil.setEmailSub(mailSub);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
            registerService.contentAdmMsgBox(mailId, XMLReader.getMessage("emailIdNoReply"), mailSub, list.get(1).toString()+ list.get(3).toString()+list.get(5).toString());
            try{
                sendMessageUtil.sendEmail();
                CommonServiceImpl csImpl = new CommonServiceImpl();
                csImpl.sendMsg(mailId, mailSub, mailText);
            }
            catch(Exception ex){
                ex.printStackTrace();
            }

            /*try{
               sendMessageUtil.setSmsNo(""+mobNo);
               sendMessageUtil.setSmsBody(list.get(2).toString());
               sendMessageUtil.sendSMS();
            }catch(Exception ex){
               logger.error("sendMailAndSMS : "+logUserId+" : "+ex.toString());
            }*/
            mailSent = true;
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return mailSent;
    }

}
