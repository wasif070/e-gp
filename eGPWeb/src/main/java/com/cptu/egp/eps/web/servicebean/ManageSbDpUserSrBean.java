/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.view.VwGetSbDevPartner;
import com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * <b>ManageSbDpUserSrBean</b> <Description Goes Here> Nov 20, 2010 11:47:03 AM
 * @author Administrator
 */
public class ManageSbDpUserSrBean {

    final Logger logger = Logger.getLogger(ManageSbDpUserSrBean.class);
    ScBankDevpartnerService scBankDevpartnerService=(ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");

    private int offset = 0;
    private int limit = 0;
    private String colName = "";
    private String op_ENUM = "";
    private String condition = "";
    private boolean _search = false;
    private String getSortOrder = "";
    private String sortCol = "";
    private String logUserId="0";

     public void setLogUserId(String logUserId) {
        scBankDevpartnerService.setUserId(logUserId);
        this.logUserId = logUserId;
     }

    public String getSortOrder() {
        return getSortOrder;
    }

    public void setSortOrder(String getSortOrder) {
        this.getSortOrder = getSortOrder;
    }

    public String getSortCol() {
        return sortCol;
    }

    public void setSortCol(String sortCol) {
        this.sortCol = sortCol;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public boolean isSearch() {
        return _search;
    }

    public void setSearch(boolean _search) {
        this._search = _search;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getOp_ENUM() {
        return op_ENUM;
    }

    public void setOp_ENUM(String op_ENUM) {
        this.op_ENUM = op_ENUM;
    }

    public long getAllCountOfDept(String type,int userId,int userTypeId) {
        logger.debug("getAllCountOfDept : "+logUserId+" Starts ");
        long count =0;
        long count1 =0;
        try{
        if(userTypeId!=1){
            ScBankDevpartnerSrBean scBankCreationSrBean = new ScBankDevpartnerSrBean();
                scBankCreationSrBean.getBankName(userId, (byte) userTypeId);
                userId = scBankCreationSrBean.getsBankDevelopId();
        //return scBankDevpartnerService.getCntOfficeAdmin("VwGetSbDevPartner sbdp","id.isAdmin='No' and id.partnerType='"+type+"' and id.sbankDevelHeadId="+userId);
                count = scBankDevpartnerService.getCntOfficeAdmin("VwGetSbDevPartner sbdp","id.isAdmin='No' and id.partnerType='"+type+"' and id.sbankDevelHeadId="+userId);
                count1 = scBankDevpartnerService.getCntOfficeAdmin("VwGetSbDevPartner sbdp","id.isAdmin='No' and id.partnerType='"+type+"' and id.sbankDevelopId="+userId);
                count = count+count1;
        }else{
        count = scBankDevpartnerService.getCntOfficeAdmin("VwGetSbDevPartner sbdp", "sbdp.id.partnerType = '"+type+"' AND sbdp.id.isAdmin = 'No'");
        }
        }catch(Exception e)
        {
            logger.error("getAllCountOfDept : "+logUserId+" : "+e);
        }
        logger.debug("getAllCountOfDept : "+logUserId+" Ends ");
        return count;
        //return scBankDevpartnerService.getCntOfficeAdmin("VwGetSbDevPartner sbdp", "sbdp.id.partnerType = '"+type+"' AND sbdp.id.isAdmin = 'No' AND id.createdBy=" + userId);
        //return scBankDevpartnerService.getCntOfficeAdmin("VwGetSbDevPartner sbdp", "sbdp.id.partnerType = '"+type+"' AND sbdp.id.isAdmin = 'No'");
    }

    public long getAllCountOfDept(String type, String searchField,String searchString,String searchOper) {
        logger.debug("getAllCountOfDept : "+logUserId+" Starts ");
        long lng = 0;
        try {
        if(searchOper.equalsIgnoreCase("CN")){
            searchString = "%" + searchString + "%";
        }
                lng = scBankDevpartnerService.getCntOfficeAdmin("VwGetSbDevPartner sbdp", "sbdp.id.partnerType = '"+type+"' AND sbdp.id.isAdmin = 'No' AND sbdp.id." + searchField + " LIKE '" + searchString + "'");
        } catch (Exception e) {
            logger.error("getAllCountOfDept : "+logUserId+" : "+e);
    }
        logger.debug("getAllCountOfDept : "+logUserId+" Ends ");
        return lng;
    }

    List<VwGetSbDevPartner> userList = new ArrayList<VwGetSbDevPartner>();
    public List<VwGetSbDevPartner> getUserList(String type,int userTypeId,int userId) {
        logger.debug("getUserList : "+logUserId+" Starts ");
        try{
        String orderClause = null;
        Object e = null;
        if(getSortCol().equals("")){
            orderClause = "id.partnerId";
            e = Operation_enum.DESC;
        }else if(getSortCol().equalsIgnoreCase("fullName")){
            if(getSortOrder.equalsIgnoreCase("asc")){
                orderClause = "id.fullName";
                e = Operation_enum.ASC;
            }
            else if(getSortOrder.equalsIgnoreCase("desc")){
                orderClause = "id.fullName";
                e = Operation_enum.DESC;
            }
        }else if(getSortCol().equalsIgnoreCase("emailId")){
            if(getSortOrder.equalsIgnoreCase("asc")){
                orderClause = "id.emailId";
                e = Operation_enum.ASC;
            }
            else if(getSortOrder.equalsIgnoreCase("desc")){
                orderClause = "id.emailId";
                e = Operation_enum.DESC;
            }
        }else if(getSortCol().equalsIgnoreCase("sbDevelopName")){
            if(getSortOrder.equalsIgnoreCase("asc")){
                orderClause = "id.sbDevelopName";
                e = Operation_enum.ASC;
            }
            else if(getSortOrder.equalsIgnoreCase("desc")){
                orderClause = "id.sbDevelopName";
                e = Operation_enum.DESC;
            }
        }
         else if(getSortCol().equalsIgnoreCase("isMakerChecker")){
            if(getSortOrder.equalsIgnoreCase("asc")){
                orderClause = "id.isMakerChecker";
                e = Operation_enum.ASC;
            }
            else if(getSortOrder.equalsIgnoreCase("desc")){
                orderClause = "id.isMakerChecker";
                e = Operation_enum.DESC;
            }
        }
        if (userList.isEmpty()) {
            Object[] values={"id.isAdmin",Operation_enum.LIKE,"No","id.partnerType",Operation_enum.LIKE,type,orderClause,Operation_enum.ORDERBY,e};
                for (VwGetSbDevPartner vwGetSbDevPartner : scBankDevpartnerService.findAdminList(getOffset(), getLimit(), values)) {
                  userList.add(vwGetSbDevPartner);
                }
          
        }//if userlist is empty
        }catch(Exception e)
        {
            logger.error("getUserList : "+logUserId+" : "+e);
        }
        logger.debug("User List: "+userList.size());
        logger.debug("getUserList : "+logUserId+" Ends");
        return userList;
    }

    public List<Object[]> getUserListAdmin(String type,int userTypeId,int userId,String searchField, String searchString,String searchOper) {
        logger.debug("getUserListAdmin : "+logUserId+" Starts");
        List<Object[]> userList1 = null;
        try{
            if("Branch Maker".equalsIgnoreCase(searchString)){
                searchString = "BranchMaker";
            }
            if("Branch Checker".equalsIgnoreCase(searchString)){
                searchString = "BranchChecker";
            }
        String orderClause = null;
        Object e = null;
        if(getSortCol().equals("")){
            orderClause = "LM.userId";
            e = Operation_enum.DESC;
        }else if(getSortCol().equalsIgnoreCase("fullName")){
            if(getSortOrder.equalsIgnoreCase("asc")){
                orderClause = "PA.fullName";
                e = Operation_enum.ASC;
            }
            else if(getSortOrder.equalsIgnoreCase("desc")){
                orderClause = "PA.fullName";
                e = Operation_enum.DESC;
            }
        }else if(getSortCol().equalsIgnoreCase("emailId")){
            if(getSortOrder.equalsIgnoreCase("asc")){
                orderClause = "LM.emailId";
                e = Operation_enum.ASC;
            }
            else if(getSortOrder.equalsIgnoreCase("desc")){
                orderClause = "LM.emailId";
                e = Operation_enum.DESC;
            }
        }else if(getSortCol().equalsIgnoreCase("sbDevelopName")){
            if(getSortOrder.equalsIgnoreCase("asc")){
                orderClause = "SBP.sbDevelopName";
                e = Operation_enum.ASC;
            }
            else if(getSortOrder.equalsIgnoreCase("desc")){
                orderClause = "SBP.sbDevelopName";
                e = Operation_enum.DESC;
            }
         }else if (getSortCol().equalsIgnoreCase("isMakerChecker")) {
            if(getSortOrder.equalsIgnoreCase("asc")){
                orderClause = "PA.isMakerChecker";
                e = Operation_enum.ASC;
            }
            else if(getSortOrder.equalsIgnoreCase("desc")){
                orderClause = "PA.isMakerChecker";
                e = Operation_enum.DESC;
            }
        }if(searchField.equalsIgnoreCase("sbDevelopName")){
                            searchField = "and SBP.sbDevelopName ";
                        }
                        else if(searchField.equalsIgnoreCase("fullName")){
                            searchField = "and PA.fullName ";
                        }else if(searchField.equalsIgnoreCase("emailId")){
                            searchField = "and LM.emailId ";
                        }else if(searchField.equalsIgnoreCase("isMakerChecker")){
                            searchField = "and PA.isMakerChecker ";
                        }
                        if(searchOper.equalsIgnoreCase("CN")){
                            searchString = "Like '%"+searchString+"%'";
                        }
                        else if(searchOper.equalsIgnoreCase("EQ")){
                            searchString = "= '"+searchString+"'";
                        }
                         userList1 = new ArrayList<Object[]>();
        if (userList1.isEmpty()) {
                ScBankDevpartnerSrBean scBankCreationSrBean = new ScBankDevpartnerSrBean();
                scBankCreationSrBean.getBankName(userId, (byte) userTypeId);
                userId = scBankCreationSrBean.getsBankDevelopId();
                userList1 = scBankDevpartnerService.getUserListGrid(getOffset(), getLimit(), userId, type,orderClause,e.toString(),searchField,searchString);
        }//if userlist is empty
        logger.debug("User List: "+userList1.size());
//        if(userList1.size()>0){
//            return userList1;
//        }
        }
        catch(Exception e)
        {
            logger.error("getUserListAdmin : "+logUserId+" : "+e);
        }
        logger.debug("getUserListAdmin : "+logUserId+" Ends");
        return userList1;
    }
    

    public List<VwGetSbDevPartner> getUserList(String type,int userTypeId,int userId, String searchField,String searchString,String searchOper)  {
        logger.debug("getUserList : "+logUserId+" Starts");
        try{
            if("Branch Maker".equalsIgnoreCase(searchString)){
                searchString = "BranchMaker";
            }
            if("Branch Checker".equalsIgnoreCase(searchString)){
                searchString = "BranchChecker";
            }
        if (userList.isEmpty()) {
            if(searchOper.equalsIgnoreCase("CN")){
                searchString = "%" + searchString + "%";
            }
            Object[] values={"id.isAdmin",Operation_enum.LIKE,"No","id.partnerType",Operation_enum.LIKE,type, "id."+searchField, Operation_enum.LIKE, searchString ,"id.partnerId",Operation_enum.ORDERBY,Operation_enum.DESC};
            if(userTypeId!=1)
            {
                Object[] adminValues={"id.isAdmin",Operation_enum.LIKE,"No","id.partnerType",Operation_enum.LIKE,type,"id.createdBy",Operation_enum.EQ,userId, "id."+searchField, Operation_enum.LIKE, searchString};
                values=adminValues;
            }
            for (VwGetSbDevPartner vwGetSbDevPartner : scBankDevpartnerService.findAdminList(getOffset(), getLimit(), values)) {
                userList.add(vwGetSbDevPartner);
            }
        }
        }catch(Exception e)
        {
            logger.error("getUserList : "+logUserId+" : "+e);
        }
        logger.debug("User List: "+userList);
        logger.debug("getUserList : "+logUserId+" Ends");
        return userList;
    }

    public List<VwGetSbDevPartner> getUserList()
    {
        return userList;
    }

    public void setUserList(List<VwGetSbDevPartner> userList)
    {
        this.userList = userList;
    }

    public String getGetSortOrder()
    {
        return getSortOrder;
    }

    public void setGetSortOrder(String getSortOrder)
    {
        this.getSortOrder = getSortOrder;
    }

    public ScBankDevpartnerService getScBankDevpartnerService()
    {
        return scBankDevpartnerService;
    }

    public void setScBankDevpartnerService(ScBankDevpartnerService scBankDevpartnerService)
    {
        this.scBankDevpartnerService = scBankDevpartnerService;
    }

}
