/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.

database table is tbl_AppPqTenderDates   :::nitish 

 */
package com.cptu.egp.eps.web.databean;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonAppPkgDetails;
import com.cptu.egp.eps.model.table.TblAppMaster;
import com.cptu.egp.eps.model.table.TblAppPackages;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.service.serviceinterface.APPService;
import com.cptu.egp.eps.service.serviceinterface.AppViewPkgService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author Malhar
 */
public class AppViewPkgDtBean
{
    private static final Logger LOGGER = Logger.getLogger(AppViewPkgDtBean.class);
    private String pkgType;
    private Integer appId;
    private String appCode;
    private String financialYear;
    private String budgetType;
    private String projectName;
    private String procurementnature;
    private String servicesType;
    private String packageNo;
    private String packageDesc;
    private String bidderCategory;
    private String workCategory;
    private String depoplanWork;
    private String entrustingAgency;
    private String timeFrame;
    private String pkgUrgency;
    private String cpvCategory;
    private BigDecimal allocateBudget;
    private BigDecimal pkgEstCost;
    private String isPQRequired;
    private String reoiRfaRequired;
    private Short procurementMethodId;
    private String procurementType;
    private String sourceOfFund;
    private String advtDt;
    private String subDt;
    private String tenderAdvertDt;
    private String tenderSubDt;
    private String tenderOpenDt;
    //Code by Proshanto
    private String tenderLetterIntentDt;
    //end
    private String tenderNoaIssueDt;
    private String tenderContractSignDt;
    private String tenderContractCompDt;
    private String PE;
    private String district;
    private String techSubCmtRptDt;
    private String tenderEvalRptDt;
    private String tenderEvalRptAppDt;
    private String tenderContractAppDt;
    private String reoiReceiptDt;
    private String rfpTechEvalDt;
    private String rfpFinancialOpenDt;
    private String rfpNegCompDt;
    private String rfpContractAppDt;
    private String rfaAdvertDt;
    private String rfaReceiptDt;
    private String rfaEvalDt;
    private String rfaInterviewDt;
    private String rfaFinalSelDt;
    private String rfaEvalRptSubDt;
    private String rfaAppConsultantDt;
    private String actSubDt;
    private String actAdvtDt;
    private String actEvalRptDt;
    private String actAppLstDt;
    private String actTenderAdvertDt;
    private String actTenderSubDt;
    private String actTenderOpenDt;
    private String actTechSubCmtRptDt;
    private String actTenderEvalRptDt;
    private String acttenderEvalRptAppDt;
    private String actTenderContractAppDt;
    private String actTenderNoaIssueDt;
    private String actTenderContractSignDt;
    private String actTenderContractCompDt;
    private String actReoiReceiptDt;
    private String actRfpTechEvalDt;
    private String actRfpFinancialOpenDt;
    private String actRfpNegComDt;
    private String actRfpContractAppDt;
    private String actRfaAdvertDt;
    private String actRfaReceiptDt;
    private String actRfaEvalDt;
    private String actRfaInterviewDt;
    private String actRfaFinalSelDt;
    private String actRfaEvalRptSubDt;
    private String actRfaAppConsultantDt;
    private String evalRptDt;
    private Integer packageId;
    private String lotNo;
    private String lotDesc;
    private Integer quantity;
    private String unit;
    private BigDecimal lotEstCost;
    private List<CommonAppPkgDetails> pkgLotDeail = new ArrayList<CommonAppPkgDetails>();
    private String emeType;
    private Integer aaEmpId;
    private String cpvCode;
    private Integer projectId;
    private String appLstDt;
    private Short subDays;
    private Short advtDays;
    private Short openDays;
    private String openDt;
    private Short evalRptDays;
    private Short tenderAdvertDays;
    private Short tenderSubDays;
    private Short tenderOpenDays;
    private Short techSubCmtRptDays;
    private Short tenderEvalRptdays;
    private Short tenderEvalRptAppDays;
    private Short tenderContractAppDays;
    //Code by Proshanto
    private Short tnderLetterIntentDays;
    //end
    private Short tenderNoaIssueDays;
    private Short tenderContractSignDays;
    private Short rfaAdvertDays;
    private Short rfaReceiptDays;
    private Short rfaEvalDays;
    private Short rfaInterviewDays;
    private Short rfaFinalSelDays;
    private Short rfaEvalRptSubDays;
    private Short rfpContractAppDays;
    private Short rfpNegCompDays;
    private Short rfpFinancialOpenDays;
    private Short rfpTechEvalDays;
    private Short reoiReceiptDays;
    private Integer pqDtId;
    private boolean inWatchList;
    private int userId;
    private int watchId;
    private boolean dataExists;
    private String[] procurementMethod = {"RFQ", "OTM", "LTM", "DP", "TSTM", "QCBS", "LCS", "SFB", "DC", "SBCQ", "SSS", "IC", "CSO","DPM","OSTETM","RFQU","RFQL","FC"};
    private String[] procurementMethodFullname = {"Request For Quotation", "Open Tendering Method", "Limited Tendering Method", "DP", "Two Stage Tendering Method", "Quality Cost Based Selection", "Least Cost Selection", "Selection under a Fixed Budget", "Design Contest", "Selection Based on Consultants Qualification", "Single Source Selection", "Individual Consultant", "Community Service Organisation","Direct Procurement","One stage Two Envelopes Tendering Method","Request For Quotation Unit Rate","Request For Quotation Lump Sum","Framework Contract"};
    private String pkgStatus;
    private AuditTrail auditTrail;
    
    public String getPkgStatus() {
        return pkgStatus;
    }

    public void setPkgStatus(String pkgStatus) {
        this.pkgStatus = pkgStatus;
    }

    public boolean isDataExists()
    {
        return dataExists;
    }

    public void setDataExists(boolean dataExists)
    {
        this.dataExists = dataExists;
    }

    public int getWatchId()
    {
        return watchId;
    }

    public void setWatchId(int watchId)
    {
        this.watchId = watchId;
    }

    public boolean isInWatchList()
    {
        return inWatchList;
    }

    public void setInWatchList(boolean inWatchList)
    {
        this.inWatchList = inWatchList;
    }

    public int getUserId()
    {
        return userId;
    }

    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    public Integer getPqDtId()
    {
        return pqDtId;
    }

    public void setPqDtId(Integer pqDtId)
    {
        this.pqDtId = pqDtId;
    }

    public List<CommonAppPkgDetails> getPkgLotDeail()
    {
        return pkgLotDeail;
    }

    public void setPkgLotDeail(List<CommonAppPkgDetails> pkgLotDeail)
    {
        this.pkgLotDeail = pkgLotDeail;
    }

    public String getProcMethodName(short methodId)
    {
        return procurementMethod[methodId - 1];
    }

    public String getProcMethodFullName(short methodId)
    {
        return procurementMethodFullname[methodId - 1];
    }

    public String getPkgType()
    {
        return pkgType;
    }

    public void setPkgType(String pkgType)
    {
        this.pkgType = pkgType;
    }

    public String getLotDesc()
    {
        return lotDesc;
    }

    public void setLotDesc(String lotDesc)
    {
        this.lotDesc = lotDesc;
    }

    public BigDecimal getLotEstCost()
    {
        return lotEstCost;
    }

    public void setLotEstCost(BigDecimal lotEstCost)
    {
        this.lotEstCost = lotEstCost;
    }

    public String getLotNo()
    {
        return lotNo;
    }

    public void setLotNo(String lotNo)
    {
        this.lotNo = lotNo;
    }

    public Integer getQuantity()
    {
        return quantity;
    }

    public void setQuantity(Integer quantity)
    {
        this.quantity = quantity;
    }

    public String getUnit()
    {
        return unit;
    }

    public void setUnit(String unit)
    {
        this.unit = unit;
    }

    public String getPE()
    {
        return PE;
    }

    public void setPE(String PE)
    {
        this.PE = PE;
    }

    public String getActAdvtDt()
    {
        return actAdvtDt;
    }


   
    public void setActAppLstDt(String actAppLstDt)
    {
        this.actAppLstDt = actAppLstDt;
    }
    //Code by Proshanto Kumar Saha
    public String getActAppLstDt()
    {
        return actAppLstDt;
    }
    //End

    public String getActEvalRptDt()
    {
        return actEvalRptDt;
    }

    public void setActEvalRptDt(String actEvalRptDt)
    {
        this.actEvalRptDt = actEvalRptDt;
    }

    public String getActReoiReceiptDt()
    {
        return actReoiReceiptDt;
    }

    public void setActReoiReceiptDt(String actReoiReceiptDt)
    {
        this.actReoiReceiptDt = actReoiReceiptDt;
    }

    public String getActRfaAdvertDt()
    {
        return actRfaAdvertDt;
    }

    public void setActRfaAdvertDt(String actRfaAdvertDt)
    {
        this.actRfaAdvertDt = actRfaAdvertDt;
    }

    public String getActRfaAppConsultantDt()
    {
        return actRfaAppConsultantDt;
    }

    public void setActRfaAppConsultantDt(String actRfaAppConsultantDt)
    {
        this.actRfaAppConsultantDt = actRfaAppConsultantDt;
    }

    public String getActRfaEvalDt()
    {
        return actRfaEvalDt;
    }

    public void setActRfaEvalDt(String actRfaEvalDt)
    {
        this.actRfaEvalDt = actRfaEvalDt;
    }

    public String getActRfaEvalRptSubDt()
    {
        return actRfaEvalRptSubDt;
    }

    public void setActRfaEvalRptSubDt(String actRfaEvalRptSubDt)
    {
        this.actRfaEvalRptSubDt = actRfaEvalRptSubDt;
    }

    public String getActRfaFinalSelDt()
    {
        return actRfaFinalSelDt;
    }

    public void setActRfaFinalSelDt(String actRfaFinalSelDt)
    {
        this.actRfaFinalSelDt = actRfaFinalSelDt;
    }

    public String getActRfaInterviewDt()
    {
        return actRfaInterviewDt;
    }

    public void setActRfaInterviewDt(String actRfaInterviewDt)
    {
        this.actRfaInterviewDt = actRfaInterviewDt;
    }

    public String getActRfaReceiptDt()
    {
        return actRfaReceiptDt;
    }

    public void setActRfaReceiptDt(String actRfaReceiptDt)
    {
        this.actRfaReceiptDt = actRfaReceiptDt;
    }

    public String getActRfpContractAppDt()
    {
        return actRfpContractAppDt;
    }

    public void setActRfpContractAppDt(String actRfpContractAppDt)
    {
        this.actRfpContractAppDt = actRfpContractAppDt;
    }

    public String getActRfpFinancialOpenDt()
    {
        return actRfpFinancialOpenDt;
    }

    public void setActRfpFinancialOpenDt(String actRfpFinancialOpenDt)
    {
        this.actRfpFinancialOpenDt = actRfpFinancialOpenDt;
    }

    public String getActRfpNegComDt()
    {
        return actRfpNegComDt;
    }

    public void setActRfpNegComDt(String actRfpNegComDt)
    {
        this.actRfpNegComDt = actRfpNegComDt;
    }

    public String getActRfpTechEvalDt()
    {
        return actRfpTechEvalDt;
    }

    public void setActRfpTechEvalDt(String actRfpTechEvalDt)
    {
        this.actRfpTechEvalDt = actRfpTechEvalDt;
    }

    public String getActSubDt()
    {
        return actSubDt;
    }

    public void setActSubDt(String actSubDt)
    {
        this.actSubDt = actSubDt;
    }

    public String getActTechSubCmtRptDt()
    {
        return actTechSubCmtRptDt;
    }

    public void setActTechSubCmtRptDt(String actTechSubCmtRptDt)
    {
        this.actTechSubCmtRptDt = actTechSubCmtRptDt;
    }

    public String getActTenderAdvertDt()
    {
        return actTenderAdvertDt;
    }

    public void setActTenderAdvertDt(String actTenderAdvertDt)
    {
        this.actTenderAdvertDt = actTenderAdvertDt;
    }

    public String getActTenderContractAppDt()
    {
        return actTenderContractAppDt;
    }

    public void setActTenderContractAppDt(String actTenderContractAppDt)
    {
        this.actTenderContractAppDt = actTenderContractAppDt;
    }

    public String getActTenderContractCompDt()
    {
        return actTenderContractCompDt;
    }

    public void setActTenderContractCompDt(String actTenderContractCompDt)
    {
        this.actTenderContractCompDt = actTenderContractCompDt;
    }

    public String getActTenderContractSignDt()
    {
        return actTenderContractSignDt;
    }

    public void setActTenderContractSignDt(String actTenderContractSignDt)
    {
        this.actTenderContractSignDt = actTenderContractSignDt;
    }

    public String getActTenderEvalRptDt()
    {
        return actTenderEvalRptDt;
    }

    public void setActTenderEvalRptDt(String actTenderEvalRptDt)
    {
        this.actTenderEvalRptDt = actTenderEvalRptDt;
    }

    public String getActTenderNoaIssueDt()
    {
        return actTenderNoaIssueDt;
    }

    public void setActTenderNoaIssueDt(String actTenderNoaIssueDt)
    {
        this.actTenderNoaIssueDt = actTenderNoaIssueDt;
    }

    public String getActTenderOpenDt()
    {
        return actTenderOpenDt;
    }

    public void setActTenderOpenDt(String actTenderOpenDt)
    {
        this.actTenderOpenDt = actTenderOpenDt;
    }

    public String getActTenderSubDt()
    {
        return actTenderSubDt;
    }

    public void setActTenderSubDt(String actTenderSubDt)
    {
        this.actTenderSubDt = actTenderSubDt;
    }

    public String getActtenderEvalRptAppDt()
    {
        return acttenderEvalRptAppDt;
    }

    public void setActtenderEvalRptAppDt(String acttenderEvalRptAppDt)
    {
        this.acttenderEvalRptAppDt = acttenderEvalRptAppDt;
    }

    public String getAdvtDt()
    {
        //return advtDt;
        if(advtDt!=null){
            SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat sd1 = new SimpleDateFormat("dd-MMM-yyyy");
            try {
                return sd.format(sd1.parse(advtDt));
            } catch (ParseException ex) {
               LOGGER.error("AppViewPkgDtBean : " + ex);
                return null;
            }
        }else{
            return advtDt;
        }
    }

    public void setAdvtDt(String advtDt)
    {
        this.advtDt = advtDt;
    }

    public BigDecimal getAllocateBudget()
    {
        return allocateBudget;
    }

    public void setAllocateBudget(BigDecimal allocateBudget)
    {
        this.allocateBudget = allocateBudget;
    }

    public String getAppCode()
    {
        return appCode;
    }

    public void setAppCode(String appCode)
    {
        this.appCode = appCode;
    }

    public Integer getAppId()
    {
        return appId;
    }

    public void setAppId(Integer appId)
    {
        this.appId = appId;
    }

    public String getBudgetType()
    {
        return budgetType;
    }

    public void setBudgetType(String budgetType)
    {
        this.budgetType = budgetType;
    }

    public String getDistrict()
    {
        return district;
    }

    public void setDistrict(String district)
    {
        this.district = district;
    }

    public String getEvalRptDt()
    {
        return evalRptDt;
    }

    public void setEvalRptDt(String evalRptDt)
    {
        this.evalRptDt = evalRptDt;
    }

    public String getFinancialYear()
    {
        return financialYear;
    }

    public void setFinancialYear(String financialYear)
    {
        this.financialYear = financialYear;
    }

    public String getIsPQRequired()
    {
        return isPQRequired;
    }

    public void setIsPQRequired(String isPQRequired)
    {
        this.isPQRequired = isPQRequired;
    }

    public String getPackageDesc()
    {
        return packageDesc;
    }

    public void setPackageDesc(String packageDesc)
    {
        this.packageDesc = packageDesc;
    }

    public Integer getPackageId()
    {
        return packageId;
    }

    public void setPackageId(Integer packageId)
    {
        this.packageId = packageId;
    }

    public String getPackageNo()
    {
        return packageNo;
    }

    public void setPackageNo(String packageNo)
    {
        this.packageNo = packageNo;
    }

    public BigDecimal getPkgEstCost()
    {
        return pkgEstCost;
    }

    public void setPkgEstCost(BigDecimal pkgEstCost)
    {
        this.pkgEstCost = pkgEstCost;
    }

    public Short getProcurementMethodId()
    {
        return procurementMethodId;
    }

    public void setProcurementMethodId(Short procurementMethodId)
    {
        this.procurementMethodId = procurementMethodId;
    }

    public String getProcurementType()
    {
        return procurementType;
    }

    public void setProcurementType(String procurementType)
    {
        this.procurementType = procurementType;
    }

    public String getProcurementnature()
    {
        return procurementnature;
    }

    public void setProcurementnature(String procurementnature)
    {
        this.procurementnature = procurementnature;
    }

    public String getProjectName()
    {
        return projectName;
    }

    public void setProjectName(String projectName)
    {
        this.projectName = projectName;
    }

    public String getReoiReceiptDt()
    {
        return reoiReceiptDt;
    }

    public void setReoiReceiptDt(String reoiReceiptDt)
    {
        this.reoiReceiptDt = reoiReceiptDt;
    }

    public String getReoiRfaRequired()
    {
        return reoiRfaRequired;
    }

    public void setReoiRfaRequired(String reoiRfaRequired)
    {
        this.reoiRfaRequired = reoiRfaRequired;
    }

    public String getRfaAdvertDt()
    {
        if(rfaAdvertDt!=null && !"".equalsIgnoreCase(rfaAdvertDt)){

            SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat sd1 = new SimpleDateFormat("dd-MMM-yyyy");
            try {
                return sd.format(sd1.parse(rfaAdvertDt));
            } catch (ParseException ex) {
                LOGGER.error("AppViewPkgDtBean : " + ex);
                return null;
            }
        }else{
            return rfaAdvertDt;
        }
        
    }

    public void setRfaAdvertDt(String rfaAdvertDt)
    {
        this.rfaAdvertDt = rfaAdvertDt;
    }

    public String getRfaAppConsultantDt()
    {
        return rfaAppConsultantDt;
    }

    public void setRfaAppConsultantDt(String rfaAppConsultantDt)
    {
        this.rfaAppConsultantDt = rfaAppConsultantDt;
    }

    public String getRfaEvalDt()
    {
        return rfaEvalDt;
    }

    public void setRfaEvalDt(String rfaEvalDt)
    {
        this.rfaEvalDt = rfaEvalDt;
    }

    public String getRfaEvalRptSubDt()
    {
        return rfaEvalRptSubDt;
    }

    public void setRfaEvalRptSubDt(String rfaEvalRptSubDt)
    {
        this.rfaEvalRptSubDt = rfaEvalRptSubDt;
    }

    public String getRfaFinalSelDt()
    {
        return rfaFinalSelDt;
    }

    public void setRfaFinalSelDt(String rfaFinalSelDt)
    {
        this.rfaFinalSelDt = rfaFinalSelDt;
    }

    public String getRfaInterviewDt()
    {
        return rfaInterviewDt;
    }

    public void setRfaInterviewDt(String rfaInterviewDt)
    {
        this.rfaInterviewDt = rfaInterviewDt;
    }

    public String getRfaReceiptDt()
    {
        return rfaReceiptDt;
    }

    public void setRfaReceiptDt(String rfaReceiptDt)
    {
        this.rfaReceiptDt = rfaReceiptDt;
    }

    public String getRfpContractAppDt()
    {
        return rfpContractAppDt;
    }

    public void setRfpContractAppDt(String rfpContractAppDt)
    {
        this.rfpContractAppDt = rfpContractAppDt;
    }

    public String getRfpFinancialOpenDt()
    {
        return rfpFinancialOpenDt;
    }

    public void setRfpFinancialOpenDt(String rfpFinancialOpenDt)
    {
        this.rfpFinancialOpenDt = rfpFinancialOpenDt;
    }

    public String getRfpNegCompDt()
    {
        return rfpNegCompDt;
    }

    public void setRfpNegCompDt(String rfpNegCompDt)
    {
        this.rfpNegCompDt = rfpNegCompDt;
    }

    public String getRfpTechEvalDt()
    {
        return rfpTechEvalDt;
    }

    public void setRfpTechEvalDt(String rfpTechEvalDt)
    {
        this.rfpTechEvalDt = rfpTechEvalDt;
    }

    public String getServicesType()
    {
        return servicesType;
    }

    public void setServicesType(String servicesType)
    {
        this.servicesType = servicesType;
    }

    public String getSourceOfFund()
    {
        return sourceOfFund;
    }

    public void setSourceOfFund(String sourceOfFund)
    {
        this.sourceOfFund = sourceOfFund;
    }

    public String getSubDt()
    {
        return subDt;
    }

    public void setSubDt(String subDt)
    {
        this.subDt = subDt;
    }

    public String getTechSubCmtRptDt()
    {
        return techSubCmtRptDt;
    }

    public void setTechSubCmtRptDt(String techSubCmtRptDt)
    {
        this.techSubCmtRptDt = techSubCmtRptDt;
    }

    public String getTenderAdvertDt()
    {

        if(tenderAdvertDt!=null && !"".equalsIgnoreCase(tenderAdvertDt)){

            SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat sd1 = new SimpleDateFormat("dd-MMM-yyyy");
            try {
                return sd.format(sd1.parse(tenderAdvertDt));
            } catch (ParseException ex) {
                LOGGER.error("AppViewPkgDtBean : " + ex);
                return null;
            }
        }else{
            return tenderAdvertDt;
        }
    }

    public void setTenderAdvertDt(String tenderAdvertDt)
    {
        this.tenderAdvertDt = tenderAdvertDt;
    }

    public String getTenderContractAppDt()
    {
        return tenderContractAppDt;
    }

    public void setTenderContractAppDt(String tenderContractAppDt)
    {
        this.tenderContractAppDt = tenderContractAppDt;
    }

    public String getTenderContractCompDt()
    {
        return tenderContractCompDt;
    }

    public void setTenderContractCompDt(String tenderContractCompDt)
    {
        this.tenderContractCompDt = tenderContractCompDt;
    }

    public String getTenderContractSignDt()
    {
        return tenderContractSignDt;
    }

    public void setTenderContractSignDt(String tenderContractSignDt)
    {
        this.tenderContractSignDt = tenderContractSignDt;
    }

    public String getTenderEvalRptAppDt()
    {
        return tenderEvalRptAppDt;
    }

    public void setTenderEvalRptAppDt(String tenderEvalRptAppDt)
    {
        this.tenderEvalRptAppDt = tenderEvalRptAppDt;
    }

    public String getTenderEvalRptDt()
    {
        return tenderEvalRptDt;
    }

    public void setTenderEvalRptDt(String tenderEvalRptDt)
    {
        this.tenderEvalRptDt = tenderEvalRptDt;
    }

    public String getTenderNoaIssueDt()
    {
        return tenderNoaIssueDt;
    }

    public void setTenderNoaIssueDt(String tenderNoaIssueDt)
    {
        this.tenderNoaIssueDt = tenderNoaIssueDt;
    }

    public String getTenderOpenDt()
    {
        return tenderOpenDt;
    }

    public void setTenderOpenDt(String tenderOpenDt)
    {
        this.tenderOpenDt = tenderOpenDt;
    }

    public String getTenderSubDt()
    {
        return tenderSubDt;
    }

    public void setTenderSubDt(String tenderSubDt)
    {
        this.tenderSubDt = tenderSubDt;
    }

    public String getEmeType()
    {
        return emeType;
    }

    public void setEmeType(String emeType)
    {
        this.emeType = emeType;
    }

    public Integer getAaEmpId()
    {
        return aaEmpId;
    }

    public void setAaEmpId(Integer aaEmpId)
    {
        this.aaEmpId = aaEmpId;
    }

    public Short getAdvtDays()
    {
        return advtDays;
    }

    public void setAdvtDays(Short advtDays)
    {
        this.advtDays = advtDays;
    }

    public String getAppLstDt()
    { 
        //Code by Proshanto
        return appLstDt; 
    }

    public void setAppLstDt(String appLstDt)
    {
        this.appLstDt = appLstDt;
    }

    public String getOpenDt()
    {
        return openDt;
    }

    public void setOpenDt(String openDt)
    {
        this.openDt = openDt;
    }

    public Short getEvalRptDays()
    {
        return evalRptDays;
    }

    public void setEvalRptDays(Short evalRptDays)
    {
        this.evalRptDays = evalRptDays;
    }

    public Short getOpenDays()
    {
        return openDays;
    }

    public void setOpenDays(Short openDays)
    {
        this.openDays = openDays;
    }

//    public String[] getProcurementMethod()
//    {
//        return procurementMethod;
//    }
//
//    public void setProcurementMethod(String[] procurementMethod)
//    {
//        this.procurementMethod = procurementMethod;
//    }

    public Short getReoiReceiptDays()
    {
        return reoiReceiptDays;
    }

    public void setReoiReceiptDays(Short reoiReceiptDays)
    {
        this.reoiReceiptDays = reoiReceiptDays;
    }

    public Short getRfaAdvertDays()
    {
        return rfaAdvertDays;
    }

    public void setRfaAdvertDays(Short rfaAdvertDays)
    {
        this.rfaAdvertDays = rfaAdvertDays;
    }

    public Short getRfaEvalDays()
    {
        return rfaEvalDays;
    }

    public void setRfaEvalDays(Short rfaEvalDays)
    {
        this.rfaEvalDays = rfaEvalDays;
    }

    public Short getRfaEvalRptSubDays()
    {
        return rfaEvalRptSubDays;
    }

    public void setRfaEvalRptSubDays(Short rfaEvalRptSubDays)
    {
        this.rfaEvalRptSubDays = rfaEvalRptSubDays;
    }

    public Short getRfaFinalSelDays()
    {
        return rfaFinalSelDays;
    }

    public void setRfaFinalSelDays(Short rfaFinalSelDays)
    {
        this.rfaFinalSelDays = rfaFinalSelDays;
    }

    public Short getRfaInterviewDays()
    {
        return rfaInterviewDays;
    }

    public void setRfaInterviewDays(Short rfaInterviewDays)
    {
        this.rfaInterviewDays = rfaInterviewDays;
    }

    public Short getRfaReceiptDays()
    {
        return rfaReceiptDays;
    }

    public void setRfaReceiptDays(Short rfaReceiptDays)
    {
        this.rfaReceiptDays = rfaReceiptDays;
    }

    public Short getRfpContractAppDays()
    {
        return rfpContractAppDays;
    }

    public void setRfpContractAppDays(Short rfpContractAppDays)
    {
        this.rfpContractAppDays = rfpContractAppDays;
    }

    public Short getRfpFinancialOpenDays()
    {
        return rfpFinancialOpenDays;
    }

    public void setRfpFinancialOpenDays(Short rfpFinancialOpenDays)
    {
        this.rfpFinancialOpenDays = rfpFinancialOpenDays;
    }

    public Short getRfpNegCompDays()
    {
        return rfpNegCompDays;
    }

    public void setRfpNegCompDays(Short rfpNegCompDays)
    {
        this.rfpNegCompDays = rfpNegCompDays;
    }

    public Short getRfpTechEvalDays()
    {
        return rfpTechEvalDays;
    }

    public void setRfpTechEvalDays(Short rfpTechEvalDays)
    {
        this.rfpTechEvalDays = rfpTechEvalDays;
    }

    public Short getSubDays()
    {
        return subDays;
    }

    public void setSubDays(Short subDays)
    {
        this.subDays = subDays;
    }

    public Short getTechSubCmtRptDays()
    {
        return techSubCmtRptDays;
    }

    public void setTechSubCmtRptDays(Short techSubCmtRptDays)
    {
        this.techSubCmtRptDays = techSubCmtRptDays;
    }

    public Short getTenderAdvertDays()
    {
        return tenderAdvertDays;
    }

    public void setTenderAdvertDays(Short tenderAdvertDays)
    {
        this.tenderAdvertDays = tenderAdvertDays;
    }

    public Short getTenderContractAppDays()
    {
        return tenderContractAppDays;
    }

    public void setTenderContractAppDays(Short tenderContractAppDays)
    {
        this.tenderContractAppDays = tenderContractAppDays;
    }

    public Short getTenderContractSignDays()
    {
        return tenderContractSignDays;
    }

    public void setTenderContractSignDays(Short tenderContractSignDays)
    {
        this.tenderContractSignDays = tenderContractSignDays;
    }

    public Short getTenderEvalRptAppDays()
    {
        return tenderEvalRptAppDays;
    }

    public void setTenderEvalRptAppDays(Short tenderEvalRptAppDays)
    {
        this.tenderEvalRptAppDays = tenderEvalRptAppDays;
    }

    public Short getTenderEvalRptdays()
    {
        return tenderEvalRptdays;
    }

    public void setTenderEvalRptdays(Short tenderEvalRptdays)
    {
        this.tenderEvalRptdays = tenderEvalRptdays;
    }

    public Short getTenderNoaIssueDays()
    {
        return tenderNoaIssueDays;
    }

    public void setTenderNoaIssueDays(Short tenderNoaIssueDays)
    {
        this.tenderNoaIssueDays = tenderNoaIssueDays;
    }

    public Short getTenderOpenDays()
    {
        return tenderOpenDays;
    }

    public void setTenderOpenDays(Short tenderOpenDays)
    {
        this.tenderOpenDays = tenderOpenDays;
    }

    public Short getTenderSubDays()
    {
        return tenderSubDays;
    }

    public void setTenderSubDays(Short tenderSubDays)
    {
        this.tenderSubDays = tenderSubDays;
    }

    public String getCpvCode()
    {
        return cpvCode;
    }

    public void setCpvCode(String cpvCode)
    {
        this.cpvCode = cpvCode;
    }

    public Integer getProjectId()
    {
        return projectId;
    }

    public void setProjectId(Integer projectId)
    {
        this.projectId = projectId;
    }

    public String getPkgUrgency() {
        return pkgUrgency;
    }

    public void setPkgUrgency(String pkgUrgency) {
        this.pkgUrgency = pkgUrgency;
    }
    
    public AuditTrail getAuditTrail() {
        return auditTrail;
     }

     public void setAuditTrail(AuditTrail auditTrail) {
         this.auditTrail = auditTrail;
     }

     /**
      * Getting App Details.
      * @param appId
      * @param pkgId
      * @param pkgType
      * @param userId
      */
    public void populateInfo(int appId, int pkgId, String pkgType, int userId)
    {
        setAppId(appId);
        setPackageId(pkgId);
        setPkgType(pkgType);
        AppViewPkgService appViewPkgService = (AppViewPkgService) AppContext.getSpringBean("AppViewPkgService");
        appViewPkgService.setAuditTrail(auditTrail);
        List<CommonAppPkgDetails> appPkgDeatils = appViewPkgService.getAppPkgDetails(getAppId(), getPackageId(), getPkgType());
        if (appPkgDeatils != null && !appPkgDeatils.isEmpty()) {
            dataExists = true;
            CommonAppPkgDetails appPkgDetail = appPkgDeatils.get(0);
            if ("AppPackage".equalsIgnoreCase(pkgType)) {
                getPkgDtl(appPkgDetail);
            }
            else {
               // BeanUtils.copyProperties(appPkgDetail, this);
                getPackageDetails(appPkgDetail);
            }
            pkgLotDeail = appViewPkgService.getAppPkgDetails(getAppId(), getPackageId(), "Lot");

            //for revised package old lot data
            if("RevisePackage".equalsIgnoreCase(pkgType)){

              pkgLotDeail = appViewPkgService.getAppPkgDetails(getAppId(), getPackageId(), "ReviseLot");
            }

            if (userId != 0) {
                inWatchList = checkForWatchList(userId, appId, pkgId);
            }
        }
        else {
            dataExists = false;
        }
        
    }

    /**
     * Getting App Package Dates Information.
     * @param appId
     * @param pkgId
     * @param pkgType
     */
    public void populateAPPDates(int appId, int pkgId, String pkgType)
    {
        setAppId(appId);
        setPackageId(pkgId);
        setPkgType(pkgType);
        AppViewPkgService appViewPkgService = (AppViewPkgService) AppContext.getSpringBean("AppViewPkgService");
        List<CommonAppPkgDetails> appPkgDeatils = appViewPkgService.getAppPkgDetails(getAppId(), getPackageId(), getPkgType());
        if (appPkgDeatils != null && !appPkgDeatils.isEmpty()) {
            dataExists = true;
            CommonAppPkgDetails appPkgDetail = appPkgDeatils.get(0);
            //getPackageDetails(appPkgDetail);
            BeanUtils.copyProperties(appPkgDetail, this);
        }
        else {
            dataExists = false;
        }
    }

    private void getPackageDetails(CommonAppPkgDetails appPkgDetail)
    {
        BeanUtils.copyProperties(appPkgDetail, this);
        this.setAppCode(appPkgDetail.getAppCode().isEmpty()?"Not Applicable":appPkgDetail.getAppCode());
        this.setFinancialYear(appPkgDetail.getFinancialYear().isEmpty()?"Not Applicable":appPkgDetail.getFinancialYear());
        this.setBudgetType(appPkgDetail.getBudgetType().isEmpty()?"Not Applicable":appPkgDetail.getBudgetType());
        this.setProjectName(appPkgDetail.getProjectName().isEmpty()?"Not Applicable":appPkgDetail.getProjectName());
        this.setPE(appPkgDetail.getPe().isEmpty()?"Not Applicable":appPkgDetail.getPe());
        this.setDistrict(appPkgDetail.getDistrict().isEmpty()?"Not Applicable":appPkgDetail.getDistrict());
        this.setProcurementnature(appPkgDetail.getProcurementnature().isEmpty()?"Not Applicable":appPkgDetail.getProcurementnature());
        this.setServicesType(appPkgDetail.getServicesType().isEmpty()?"Not Applicable":appPkgDetail.getServicesType());
        this.setPackageNo(appPkgDetail.getPackageNo().isEmpty()?"Not Applicable":appPkgDetail.getPackageNo());
        this.setPackageDesc(appPkgDetail.getPackageDesc().isEmpty()?"Not Applicable":appPkgDetail.getPackageDesc());
     
        this.setAllocateBudget(appPkgDetail.getAllocateBudget()==null?new BigDecimal(0):appPkgDetail.getAllocateBudget());
        this.setPkgEstCost(appPkgDetail.getPkgEstCost()==null?new BigDecimal(0):appPkgDetail.getPkgEstCost());
        //this.setIsPQRequired(appPkgDetail.getIsPQRequired().isEmpty()?"Not Applicable":appPkgDetail.getIsPQRequired());
        //Code by Proshanto Kumar Saha
        this.setIsPQRequired(appPkgDetail.getIsPQRequired().isEmpty()?"Not Applicable":appPkgDetail.getIsPQRequired());
        
        this.setPkgUrgency(appPkgDetail.getPkgUrgency());
        this.setCpvCode(appPkgDetail.getCpvCode());
        
        //this.setReoiRfaRequired(appPkgDetail.getReoiRfaRequired().isEmpty()?"Not Applicable":appPkgDetail.getReoiRfaRequired());
        this.setProcurementType(appPkgDetail.getProcurementType().isEmpty()?"Not Applicable":appPkgDetail.getProcurementType());
        this.setSourceOfFund(appPkgDetail.getSourceOfFund().isEmpty()?"Not Applicable":appPkgDetail.getSourceOfFund());
        this.setProcurementMethodId(appPkgDetail.getProcurementMethodId()==null?(short)0:appPkgDetail.getProcurementMethodId());
        this.setProjectName(appPkgDetail.getProjectName().isEmpty()?"Not Applicable":appPkgDetail.getProjectName());
        this.setProjectId(appPkgDetail.getProjectId()==null?0:appPkgDetail.getProjectId());
        this.setAaEmpId(appPkgDetail.getAaEmpId()==null?0:appPkgDetail.getAaEmpId());

        
//        this.setActAdvtDt(appPkgDetail.getActAdvtDt().equals("01/01/1900")?"-":appPkgDetail.getActAdvtDt());
//        this.setActAppLstDt(appPkgDetail.getActAppLstDt().equals("01/01/1900")?"-":appPkgDetail.getActAppLstDt());
//        this.setActEvalRptDt(appPkgDetail.getActEvalRptDt().equals("01/01/1900")?"-":appPkgDetail.getActEvalRptDt());
        
        //actdate valu badhu set karvu.....ternary operator no use kari ne =="tenderContractSignDt"?"-":act.getact;
//        this.setAdvtDt(appPkgDetail.getAdvtDt());
//        this.setSubDt(appPkgDetail.getSubDt());
//
////        this.setProcurementMethodId(appPkgDetail.getProcurementMethodId());
//        this.setPqDtId(appPkgDetail.getPqDtId());
//
//
//        this.setTenderAdvertDt(appPkgDetail.getTenderAdvertDt());
//        this.setTenderAdvertDays(appPkgDetail.getTenderAdvertDays());
//        this.setTenderSubDt(appPkgDetail.getTenderSubDt());
//        this.setTenderSubDays(appPkgDetail.getTenderSubDays());
//        this.setTenderOpenDt(appPkgDetail.getTenderOpenDt());
//
//        this.setTenderNoaIssueDt(appPkgDetail.getTenderNoaIssueDt());
//        this.setTenderContractSignDt(appPkgDetail.getTenderContractSignDt());
//
//        this.setTenderContractCompDt(appPkgDetail.getTenderContractCompDt());
//
//
//    //    this.setRfpTechEvalDt(appPkgDetail.getRfpTechEvalDt());
//
//
//        this.setRfpFinancialOpenDt(appPkgDetail.getRfpFinancialOpenDt());
//
//        this.setRfaAdvertDt(appPkgDetail.getRfaAdvertDt());
//
//
//        this.setRfaReceiptDt(appPkgDetail.getRfaReceiptDt());
//
//
//
//        this.setRfaEvalDt(appPkgDetail.getRfaEvalDt());
//
//        this.setRfaEvalDays(appPkgDetail.getRfaEvalDays());
//
//        this.setRfaInterviewDt(appPkgDetail.getRfaInterviewDt());
//
//        this.setRfaInterviewDays(appPkgDetail.getRfaInterviewDays());
//
//
//        this.setRfaFinalSelDt(appPkgDetail.getRfaFinalSelDt());
//
//        this.setRfaFinalSelDays(appPkgDetail.getRfaFinalSelDays());
//
//        this.setRfaEvalRptSubDt(appPkgDetail.getRfaEvalRptSubDt());
//
//        this.setRfaEvalRptSubDays(appPkgDetail.getRfaEvalRptSubDays());
//
//        this.setRfaAppConsultantDt(appPkgDetail.getRfaAppConsultantDt());
//
//
//        this.setTenderEvalRptAppDt(appPkgDetail.getTenderEvalRptAppDt());
//
//        this.setTenderEvalRptAppDays(appPkgDetail.getTenderEvalRptAppDays());
//
//        this.setTenderEvalRptDt(appPkgDetail.getTenderEvalRptDt());
//
//        this.setTenderEvalRptdays(appPkgDetail.getTenderEvalRptdays());
//
//
//        this.setRfpContractAppDt(appPkgDetail.getRfpContractAppDt());
//
//        this.setRfpContractAppDays(appPkgDetail.getRfpContractAppDays());
//
//        this.setRfpNegCompDt(appPkgDetail.getRfpNegCompDt());
//
//        this.setRfpNegCompDays(appPkgDetail.getRfpNegCompDays());
//
//
//        this.setAppLstDt(appPkgDetail.getAppLstDt());
//
//
//        this.setOpenDt(appPkgDetail.getOpenDt());
//        this.setReoiReceiptDt(appPkgDetail.getReoiReceiptDt());
//
//        this.setRfpTechEvalDt(appPkgDetail.getRfpTechEvalDt());
//        this.setEvalRptDt(appPkgDetail.getEvalRptDt());
//
//
//
//        this.setActAdvtDt(appPkgDetail.getActAdvtDt());
//
//
//        this.setActSubDt(appPkgDetail.getActAdvtDt());
//
//
//        this.setActTenderOpenDt(appPkgDetail.getActTenderOpenDt());
//
//        this.setActTenderEvalRptDt(appPkgDetail.getActEvalRptDt());
//
//
//        this.setActAppLstDt(appPkgDetail.getActAppLstDt());
//
//
//        this.setActTenderContractSignDt(appPkgDetail.getActTenderContractSignDt());
//
//        this.setActTenderContractCompDt(appPkgDetail.getActTenderContractCompDt());
//
//
//        this.setActTenderNoaIssueDt(appPkgDetail.getActTenderNoaIssueDt());
//
//
//        this.setActRfaAdvertDt(appPkgDetail.getActRfaAdvertDt());
//
//
//        this.setRfaReceiptDt(appPkgDetail.getActRfaReceiptDt());
//
//
//        this.setRfpTechEvalDt(appPkgDetail.getActRfpTechEvalDt());
//
//
//        this.setActRfpFinancialOpenDt(appPkgDetail.getActRfpFinancialOpenDt());
//
//
//
//        this.setTenderContractAppDt(appPkgDetail.getTenderContractAppDt());
//        this.setTenderContractAppDays(appPkgDetail.getTenderContractAppDays());
//
//        setAdvtDays(appPkgDetail.getAdvtDays());
//        setSubDays(appPkgDetail.getSubDays());
//        setTenderOpenDays(appPkgDetail.getTenderOpenDays());
//        setEvalRptDays(appPkgDetail.getEvalRptDays());
//        setTenderContractAppDays(appPkgDetail.getTenderContractAppDays());
//        setTenderContractSignDays(appPkgDetail.getTenderContractSignDays());
//        setTenderContractAppDays(appPkgDetail.getTenderContractAppDays());
//        setTenderNoaIssueDays(appPkgDetail.getTenderNoaIssueDays());
//        setRfaAdvertDays(appPkgDetail.getRfaAdvertDays());
//        setRfaReceiptDays(appPkgDetail.getRfaReceiptDays());
//        setRfpTechEvalDays(appPkgDetail.getRfpTechEvalDays());
//        setRfpFinancialOpenDays(appPkgDetail.getRfpFinancialOpenDays());
    }

    private void getPkgDtl(CommonAppPkgDetails appPkgDetail)
    {
        try{
            this.setAppCode(appPkgDetail.getAppCode());
            this.setFinancialYear(appPkgDetail.getFinancialYear());
            this.setBudgetType(appPkgDetail.getBudgetType());
            this.setProjectName(appPkgDetail.getProjectName());
            this.setPE(appPkgDetail.getPe());
            this.setDistrict(appPkgDetail.getDistrict());
            this.setProcurementnature(appPkgDetail.getProcurementnature());
            this.setServicesType(appPkgDetail.getServicesType());
            this.setPackageNo(appPkgDetail.getPackageNo());
            this.setPackageDesc(appPkgDetail.getPackageDesc());
          //aprojit-Start
            this.setBidderCategory(appPkgDetail.getBidderCategory());
            this.setWorkCategory(appPkgDetail.getWorkCategory());
            this.setDepoplanWork(appPkgDetail.getDepoplanWork());
            this.setEntrustingAgency(appPkgDetail.getEntrustingAgency());
            this.setTimeFrame(appPkgDetail.getTimeFrame());
          //aprojit-End
            this.setAllocateBudget(appPkgDetail.getAllocateBudget());
            this.setPkgEstCost(appPkgDetail.getPkgEstCost());
            this.setIsPQRequired(appPkgDetail.getIsPQRequired());
            this.setReoiRfaRequired(appPkgDetail.getReoiRfaRequired());
            this.setProcurementType(appPkgDetail.getProcurementType());
            this.setSourceOfFund(appPkgDetail.getSourceOfFund());
            this.setProcurementMethodId(appPkgDetail.getProcurementMethodId());
            this.setProjectName(appPkgDetail.getProjectName());
            this.setAaEmpId(appPkgDetail.getAaEmpId());
            this.setEmeType(appPkgDetail.getEmeType());
            this.setCpvCode(appPkgDetail.getCpvCode());
            this.setProjectId(appPkgDetail.getProjectId());
        }catch(Exception e){
            LOGGER.error("Error "+e.toString());
        }
    }

    /**
     * Checking App Watch List for given user.
     * @param userId
     * @param appId
     * @param pkgId
     * @return
     */
    private boolean checkForWatchList(int userId, int appId, int pkgId)
    {
        APPService appService = (APPService) AppContext.getSpringBean("APPService");
        Object[] values = {"tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(userId), "tblAppMaster", Operation_enum.EQ, new TblAppMaster(appId), "tblAppPackages", Operation_enum.EQ, new TblAppPackages(pkgId)};
        watchId = appService.checkInWatchList(values);
        if (watchId != 0) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Getting Approving Authority for APP.
     * @param aaEmpId
     * @return
     */
    public String getApprovingAuthority(int aaEmpId){
        String str = "";
        APPService appService = (APPService) AppContext.getSpringBean("APPService");
        try{
            str = appService.getAppAuthority(aaEmpId);
        }catch(Exception e){
            LOGGER.error("Error in getApprovingAuthority() : "+e.toString());
        }
        return str;
    }

    /**
     * Get App Package Status Information.
     * @param appId
     * @param pkgId
     * @return
     */
    public String getPkgStatus(int appId,int pkgId){
        String str = "";
        APPService appService = (APPService) AppContext.getSpringBean("APPService");
        try{
            str = appService.getPkgStatus(appId, pkgId);
        }catch(Exception e){
            LOGGER.error("Error in getApprovingAuthority() : "+e.toString());
        }
        return str;
    }

    /**
     * @return the bidderCategory
     */
    public String getBidderCategory() {
        return bidderCategory;
    }

    /**
     * @param bidderCategory the bidderCategory to set
     */
    public void setBidderCategory(String bidderCategory) {
        this.bidderCategory = bidderCategory;
    }

    /**
     * @return the workCategory
     */
    public String getWorkCategory() {
        return workCategory;
    }

    /**
     * @param workCategory the workCategory to set
     */
    public void setWorkCategory(String workCategory) {
        this.workCategory = workCategory;
    }

    /**
     * @return the depoplanWork
     */
    public String getDepoplanWork() {
        return depoplanWork;
    }

    /**
     * @param depoplanWork the depoplanWork to set
     */
    public void setDepoplanWork(String depoplanWork) {
        this.depoplanWork = depoplanWork;
    }

    /**
     * @return the entrustingAgency
     */
    public String getEntrustingAgency() {
        return entrustingAgency;
    }

    /**
     * @param entrustingAgency the entrustingAgency to set
     */
    public void setEntrustingAgency(String entrustingAgency) {
        this.entrustingAgency = entrustingAgency;
    }

    /**
     * @return the timeFrame
     */
    public String getTimeFrame() {
        return timeFrame;
    }

    /**
     * @param timeFrame the timeFrame to set
     */
    public void setTimeFrame(String timeFrame) {
        this.timeFrame = timeFrame;
    }

    //Code By Proshanto Kumar Saha
    /**
     * @return the tnderLetterIntentDays
     */
    public Short getTnderLetterIntentDays() {
        return tnderLetterIntentDays;
    }

    /**
     * @param tnderLetterIntentDays the tnderLetterIntentDays to set
     */
    public void setTnderLetterIntentDays(Short tnderLetterIntentDays) {
        this.tnderLetterIntentDays = tnderLetterIntentDays;
    }

    public String getTenderLetterIntentDt()
    {
        return tenderLetterIntentDt;
    }

    public void setTenderLetterIntentDt(String tenderLetterIntentDt)
    {
        this.tenderLetterIntentDt = tenderLetterIntentDt;
    }
    //Code End By Proshanto Kumar Saha
}
