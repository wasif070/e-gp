/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.CommonAppData;
import com.cptu.egp.eps.service.serviceimpl.CreateProjectServiceImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author parag
 */
public class PostQueryGridSrBean extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    CreateProjectServiceImpl postQuery = (CreateProjectServiceImpl) AppContext.getSpringBean("CreateProjectService");
    final Logger logger = Logger.getLogger(PostQueryGridSrBean.class);
    private String logUserId ="0";
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        logger.debug("processRequest : "+logUserId+" Starts");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String action = request.getParameter("action");
        try {
            HttpSession session = request.getSession();
            if(session.getAttribute("userId")!=null){
                logUserId = session.getAttribute("userId").toString();
                postQuery.setUserId(logUserId);
            }
            if ("myQueries".equalsIgnoreCase(action)) {
                logger.debug("processRequest action : myQueries : "+logUserId+" Starts");
                response.setContentType("text/xml;charset=UTF-8");
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                List<CommonAppData> postQueryData = postQuery.getDetailsBySP("GetMyPreBidQuery","","");

                int totalPages = 0;
                long totalCount = postQueryData.size();
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(rows) == 0) {
                        totalPages = (int) (totalCount / Integer.parseInt(rows));
                    } else {
                        totalPages = (int) ((totalCount / Integer.parseInt(rows)) + 1);
                    }
                } else {
                    totalPages = 0;
                }

                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + postQueryData.size() + "</records>");

                for (int i = 0 ; i < postQueryData.size(); i++) {
                    out.print("<row id='" + postQueryData.get(i).getFieldName1() + "'>");
                    out.print("<cell>" + (i+1) + "</cell>");
                    out.print("<cell>" + postQueryData.get(i).getFieldName2() + "</cell>");
                    out.print("<cell>" + postQueryData.get(i).getFieldName2() + "</cell>");
                    out.print("</row>");
                }
                out.print("</rows>");
                logger.debug("processRequest action : myQueries : "+logUserId+" Ends");
            }
        } catch(Exception ex){
           logger.error("processRequest : "+logUserId+" : "+ex.toString());
        } finally {
            out.close();
        }
        logger.debug("processRequest : "+logUserId+" Ends");
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
