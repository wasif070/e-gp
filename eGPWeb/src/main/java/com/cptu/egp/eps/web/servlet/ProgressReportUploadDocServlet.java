/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblCmsPrDocument;
import com.cptu.egp.eps.model.table.TblCmsPrMaster;
import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.service.serviceimpl.ConsolodateService;
import com.cptu.egp.eps.service.serviceinterface.ProgressReportUploadDocService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CheckExtension;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

/**
 *
 * @author dixit
 */
public class ProgressReportUploadDocServlet extends HttpServlet {
   
    ProgressReportUploadDocService prudS = (ProgressReportUploadDocService) AppContext.getSpringBean("ProgressReportUploadDocService");
    private final UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    private ConsolodateService c_ConsSrv = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
    private static final Logger LOGGER = Logger.getLogger(ProgressReportUploadDocServlet.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND  = "Ends";
    private String logUserId ="0";

    private static final String TMP_DIR_PATH = "c:\\tmp";
    private File tmpDir;
    private File destinationDir;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        tmpDir = new File(TMP_DIR_PATH);
        if (!tmpDir.isDirectory()) {
            tmpDir.mkdir();
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        //PrintWriter out = response.getWriter();        
        LOGGER.debug("processRequest : "+logUserId+ LOGGERSTART);
        String DESTINATION_DIR_PATH = "";
        File file = null;
        String tenderId = "";
        String docSizeMsg = "";
        boolean checkret = false;
        boolean flag = false;
        boolean addDocsData = false;
        int prRepId = 0;
        String wpId = "";
        String lotId = "";
        String isedit = "";
        String fileName = "";
        String documentBrief = "";
        long fileSize = 0;
        String queryString = "";
        String pageName = "officer/ProgressReportUploadDoc.jsp";
        response.setContentType("text/html");
        boolean dis = false;
        String userTypeId = "";
        int ContractId = 0;
        HttpSession session = request.getSession();
        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
            prudS.setLogUserId(logUserId);
            userRegisterService.setUserId(logUserId);
        }
        if(request.getSession().getAttribute("userTypeId")!=null){
            userTypeId = request.getSession().getAttribute("userTypeId").toString();
        }
        if(request.getParameter("lotId")!=null)
        {
            lotId = request.getParameter("lotId");
        }
        if(request.getParameter("isedit")!=null)
        {
            isedit = request.getParameter("isedit");
        }
        if (request.getParameter("tenderId") != null) {
            tenderId = request.getParameter("tenderId");
        }
        
        DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("ProgressReportUploadDocServlet");

        if (session.getAttribute("userId") == null) {
            response.sendRedirect("SessionTimedOut.jsp");
        } else {
            try {
                try {
                    if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("remove")) {
                        int prDocId = 0;
                        String docName = "";                        
                        if (request.getParameter("prDocId") != null) {
                            prDocId = Integer.parseInt(request.getParameter("prDocId"));
                        }
                        if (request.getParameter("prRepId") != null) {
                            prRepId = Integer.parseInt(request.getParameter("prRepId"));
                        }
                        if (request.getParameter("wpId") != null) {
                            wpId = request.getParameter("wpId");
                        }
                        if (request.getParameter("docName") != null) {
                            docName = request.getParameter("docName");
                        }
                        ContractId = c_ConsSrv.getContractId(Integer.parseInt(tenderId));
                        try{
                        file = new File(DESTINATION_DIR_PATH + prRepId + "\\" + docName);
                        fileName = file.toString();
                        checkret = deleteFile(fileName);
                        if (checkret) {
                            prudS.deletePrDocsDetails(prDocId);
                            queryString = "?tenderId=" + tenderId + "&prRepId=" + prRepId +"&wpId="+wpId+"&lotId="+lotId+"&isedit="+isedit+"&fq=Removed";
                            response.sendRedirect(pageName + queryString);
                        }
                        }catch(Exception e)
                        {
                            e.printStackTrace();
                        }finally{
                            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), ContractId, "ContractId", EgpModule.Progress_Report.getName(), "Remove document", "");
                        }    
                    } else if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("download")) {                        
                        String docName = request.getParameter("docName");
                        String prDocId = request.getParameter("prDocId");
                        ContractId = c_ConsSrv.getContractId(Integer.parseInt(tenderId));
                        try{
                            file = new File(DESTINATION_DIR_PATH + request.getParameter("prRepId") + "\\" + docName);
                            FileInputStream fis = new FileInputStream(file);
                            byte[] buf = new byte[Integer.valueOf(request.getParameter("docSize"))];
                            int offset = 0;
                            int numRead = 0;
                            while ((offset < buf.length) && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {
                                offset += numRead;
                            }
                            fis.close();
                            response.setContentType("application/octet-stream");
                            response.setHeader("Content-Disposition", "attachment;filename=\"" + docName + "\"");
                            ServletOutputStream outputStream = response.getOutputStream();
                            outputStream.write(buf);
                            outputStream.flush();
                            outputStream.close();
                        }catch(Exception e){
                            e.printStackTrace();
                        }finally{
                            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), ContractId, "ContractId", EgpModule.Progress_Report.getName(), "Download Document", "");
                        }
                    } else {
                        DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
                        /*
                         *Set the size threshold, above which content will be stored on disk.
                         */
                        fileItemFactory.setSizeThreshold(4 * 1024 * 1024); //1 MB
                        /*
                         * Set the temporary directory to store the uploaded files of size above threshold.
                         */
                        fileItemFactory.setRepository(tmpDir);

                        ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
                        /*
                         * Parse the request
                         */
                        List items = uploadHandler.parseRequest(request);
                        Iterator itr = items.iterator();
                        //For Supporting Document
                        while (itr.hasNext()) {
                            FileItem item = (FileItem) itr.next();
                            //For Supporting Document
                            /*
                             * Handle Form Fields.
                             */
                            if (item.isFormField()) {

                                if (item.getFieldName().equals("documentBrief")) {
                                    if (item.getString() == null || item.getString().trim().length() == 0) {
                                        dis = true;
                                        break;
                                    }
                                    documentBrief = item.getString();
                                } else if (item.getFieldName().equals("prRepId")) {
                                    prRepId = Integer.parseInt(item.getString());
                                }else if (item.getFieldName().equals("tenderId")) {
                                    tenderId = item.getString();
                                }else if (item.getFieldName().equals("wpId")) {
                                    wpId = item.getString();
                                }else if (item.getFieldName().equals("lotId")) {
                                    lotId = item.getString();
                                }else if (item.getFieldName().equals("isedit")) {
                                    isedit = item.getString();
                                }
                                ContractId = c_ConsSrv.getContractId(Integer.parseInt(tenderId));
                            } else {
                                //Handle Uploaded files.
                                /*
                                 * Write file to the ultimate location.
                                 */
                                if (item.getName().lastIndexOf("\\") == -1) {
                                    fileName = item.getName();
                                } else {
                                    fileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1, item.getName().length());
                                }
                                fileName  = fileName.replaceAll(" ", "");
                                String realPath = DESTINATION_DIR_PATH + prRepId;
                                destinationDir = new File(realPath);
                                if (!destinationDir.isDirectory()) {
                                    destinationDir.mkdir();
                                }
                                docSizeMsg = docSizeMsg(Integer.parseInt(request.getSession().getAttribute("userId").toString()));

                                if (docSizeMsg.equals("ok")){

                                    fileSize = item.getSize();
                                    checkret = checkExnAndSize(fileName, item.getSize(), "officer");

                                    if (!checkret) {
                                        break;
                                    } else {
                                        file = new File(destinationDir, fileName);
                                        if (file.isFile()) {
                                            flag = true;
                                            break;
                                        }
                                        try{
                                        item.write(file);
                                        }catch(Exception e)
                                        {
                                            e.printStackTrace();
                                        }finally {
                                            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), ContractId, "ContractId", EgpModule.Progress_Report.getName(), "Upload document", "");
                                        }    
                                    }
                                }

                            }

                        }
                    }

                } catch (FileUploadException ex) {
                     LOGGER.error("processRequest "+logUserId+" : "+ex.toString());
                } catch (Exception ex) {
                     LOGGER.error("processRequest "+logUserId+" : "+ex.toString());
                }

                    if (dis) {
                        docSizeMsg = "Please Enter Discription";
                        queryString = "?fq=" + docSizeMsg + "&prRepId=" + prRepId + "&tenderId="+ tenderId +"&wpId="+wpId+"&lotId="+lotId+"&isedit="+isedit+"";
                        response.sendRedirect(pageName + queryString);
                    } else {
                        if (flag) {
                            docSizeMsg = "File already Exists";
                            queryString = "?fq=" + docSizeMsg + "&prRepId=" + prRepId + "&tenderId="+ tenderId +"&wpId="+wpId+"&lotId="+lotId+"&isedit="+isedit+"";
                            response.sendRedirect(pageName + queryString);
                        } else {
                            if (!checkret) {
                                CheckExtension ext = new CheckExtension();
                                TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("officer");
                                queryString ="?fs="+configurationMaster.getFileSize()+"&ft="+configurationMaster.getAllowedExtension()+"&prRepId="+prRepId+"&tenderId="+tenderId +"&wpId="+wpId+"&lotId="+lotId+"&isedit="+isedit+"";
                                response.sendRedirect(pageName + queryString);
                            } else {
                                if (documentBrief != null && !"".equals(documentBrief)) {
                                    if (file.isFile()) { // for Document Upload added by Dohatec
                                    HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                                    documentBrief = handleSpecialChar.handleSpecialChar(documentBrief);
                                    int userId = Integer.parseInt(request.getSession().getAttribute("userId").toString());
                                    TblCmsPrDocument tblCmsPrDocument = new TblCmsPrDocument();
                                    tblCmsPrDocument.setTblCmsPrMaster(new TblCmsPrMaster(prRepId));
                                    tblCmsPrDocument.setDocumentName(fileName);
                                    tblCmsPrDocument.setDocSize(Integer.toString((int)fileSize));
                                    tblCmsPrDocument.setDocDescription(documentBrief);
                                    tblCmsPrDocument.setUploadedBy(userId);
                                    tblCmsPrDocument.setUploadedDate(new java.sql.Date(new java.util.Date().getTime()));
                                    addDocsData = prudS.addPrDocsDetails(tblCmsPrDocument);
                                    if (addDocsData) {
                                        response.sendRedirect("officer/ProgressReportUploadDoc.jsp?tenderId=" + tenderId + "&prRepId=" + prRepId +"&wpId="+wpId+"&lotId="+lotId+"&isedit="+isedit+"&fq=Uploaded");
                                    }
                                    }
                                    /* for Document Upload added by Dohatec Start */
                                else{
                                        docSizeMsg = "Error occurred during file upload.";
                                        queryString = "?fq=" + docSizeMsg + "&prRepId=" + prRepId + "&tenderId="+ tenderId +"&wpId="+wpId+"&lotId="+lotId+"&isedit="+isedit+"";
                                        response.sendRedirect(pageName + queryString);
                                    }                                 
                                    /* for Document Upload added by Dohatec End */
                                }
                            }
                        }
                    }

        } finally { 
            //out.close();
        }

      }
        LOGGER.debug("processRequest  : "+logUserId+ LOGGEREND);
    } 
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public boolean checkExnAndSize(String extn, long size, String userType) {

        LOGGER.debug("checkExnAndSize  : "+logUserId+ LOGGERSTART);
        boolean chextn = false;
        float fsize = 0.0f;
        float dsize = 0.0f;
        int j = extn.lastIndexOf('.');
        String lst = extn.substring(j + 1);
        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        String str = configurationMaster.getAllowedExtension();
        String[] str1 = str.split(",");
        for (int i = 0; i < str1.length; i++) {
            if (str1[i].trim().equalsIgnoreCase(lst)) {
                chextn = true;
            }
        }
        if (chextn) {
            fsize = size / (1024 * 1024);

            dsize = configurationMaster.getFileSize();

            if (dsize > fsize) {
                chextn = true;
            } else {
                chextn = false;
            }
        }
        LOGGER.debug("checkExnAndSize  : "+logUserId+ LOGGEREND);
        return chextn;
    }

    public String docSizeMsg(int userId) {
        String str = "";
        LOGGER.debug("docSizeMsg  : "+logUserId+ LOGGERSTART);
        try{
            str = userRegisterService.docSizeCheck(userId);
        }catch(Exception ex){
             LOGGER.error("docSizeMsg "+logUserId+" : "+ex.toString());
    }
        LOGGER.debug("docSizeMsg  : "+logUserId+ LOGGEREND);
        return str;
    }

    public boolean deleteFile(String filePath) {
        LOGGER.debug("deleteFile  : "+logUserId+ LOGGERSTART);
        boolean flag = false;
        File f = new File(filePath);
        if (f.delete()) {
            flag = true;
        }
        LOGGER.debug("deleteFile  : "+logUserId+ LOGGEREND);
        return flag;
    }
}


