/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import com.cptu.egp.eps.model.view.VwGetSbDevPartner;
import org.apache.log4j.Logger;

/**
 *This class is used to manage the grid of admin list of scheduled and development partner
 * @author Malhar_Nishith
 */
public class ManageSbDpAdminSrBean {

    final Logger logger = Logger.getLogger(ManageSbDpAdminSrBean.class);

    ScBankDevpartnerService scBankDevpartnerService=(ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");

    private int offset = 0;
    private int limit = 0;
    private String colName = "";
    private String op_ENUM = "";
    private String condition = "";
    private boolean _search = false;
    private String getSortOrder = "";
    private String sortCol = "";
    private String logUserId="0";

     public void setLogUserId(String logUserId) {
        scBankDevpartnerService.setUserId(logUserId);
        this.logUserId = logUserId;
     }
    /**
     *sort Order getter
     * @return sortOrder
     */
    public String getSortOrder() {
        return getSortOrder;
    }

    /**
     *sort Order setter
     * @param getSortOrder
     */
    public void setSortOrder(String getSortOrder) {
        this.getSortOrder = getSortOrder;
    }

    /**
     *Sort on coloum getter
     * @return give sort coloum name
     */
    public String getSortCol() {
        return sortCol;
    }

    /**
     *Sort on coloum setter
     * @param sortCol
     */
    public void setSortCol(String sortCol) {
        this.sortCol = sortCol;
    }

    /**
     *getter of colom name
     * @return
     */
    public String getColName() {
        return colName;
    }

    /**
     *setter of colom name
     * @param colName
     */
    public void setColName(String colName) {
        this.colName = colName;
    }

    /**
     *return search is true or not
     * @return
     */
    public boolean isSearch() {
        return _search;
    }

    /**
     *Search setter
     * @param _search
     */
    public void setSearch(boolean _search) {
        this._search = _search;
    }

    /**
     *Getter for Condition when searching
     * @return
     */
    public String getCondition() {
        return condition;
    }

    /**
     *Setter of Condition
     * @param condition
     */
    public void setCondition(String condition) {
        this.condition = condition;
    }

    /**
     *Getter of limit
     * @return
     */
    public int getLimit() {
        return limit;
    }

    /**
     *seter of limit
     * @param limit
     */
    public void setLimit(int limit) {
        this.limit = limit;
    }

    /**
     *getter of offset
     * @return
     */
    public int getOffset() {
        return offset;
    }

    /**
     *setter of offset
     * @param offset
     */
    public void setOffset(int offset) {
        this.offset = offset;
    }

    /**
     *getter of Enumber
     * @return
     */
    public String getOp_ENUM() {
        return op_ENUM;
    }

    /**
     *setter of Enumber
     * @param op_ENUM
     */
    public void setOp_ENUM(String op_ENUM) {
        this.op_ENUM = op_ENUM;
    }

    /**
     *Give count of Department
     * @param type schedule bank or development partner
     * @return count of record
     * @throws Exception
     */
    public long getAllCountOfDept(String type) {
        logger.debug("getAllCountOfDept : "+logUserId+" Starts ");
        long lng = 0;
        try {
            lng = scBankDevpartnerService.getCntOfficeAdmin("VwGetSbDevPartner sbdp", "sbdp.id.partnerType = '"+type+"' AND sbdp.id.isAdmin = 'Yes' AND sbdp.id.isMakerChecker != 'BranchAdmin'");
        } catch (Exception e) {
            logger.error("getAllCountOfDept : "+logUserId+" : "+e);
    }
        logger.debug("getAllCountOfDept : "+logUserId+" Ends ");
        return lng;
    }
    
    /**
     *Give count of department when using search of jquery
     * @param type schedule bank or development partner
     * @param searchField on which field
     * @param searchString search string
     * @param searchOper search operator
     * @return count of records
     * @throws Exception
     */
    public long getAllCountOfDept(String type, String searchField,String searchString,String searchOper) {
        logger.debug("getAllCountOfDept : "+logUserId+" Starts ");
        long lng = 0;
        try {
        if(searchOper.equalsIgnoreCase("CN")){
            searchString = "%" + searchString + "%";
        }
        lng = scBankDevpartnerService.getCntOfficeAdmin("VwGetSbDevPartner sbdp", "sbdp.id.partnerType = '"+type+"' AND sbdp.id.isAdmin = 'Yes' AND sbdp.id.isMakerChecker != 'BranchAdmin' and " + searchField + " LIKE '" + searchString + "' ");
        } catch (Exception e) {
            logger.error("getAllCountOfDept : "+logUserId+" : "+e);
    }
        logger.debug("getAllCountOfDept : "+logUserId+" Ends ");
        return lng;
    }

    List<VwGetSbDevPartner> adminList = new ArrayList<VwGetSbDevPartner>();
    /**
     *Give list of admin when grid is load and user is egpadmin
     * @param type schedule bank or development partner
     * @return List of records
     * @throws Exception
     */
    public List<VwGetSbDevPartner> getAdminList(String type){
        logger.debug("getAdminList : "+logUserId+" Starts ");
        List<VwGetSbDevPartner> list=null;
        try{
        if (adminList.isEmpty()) {            
            if(getSortCol().equalsIgnoreCase("")){                
                //list = scBankDevpartnerService.findAdminList(getOffset(), getLimit(),"id.isAdmin",Operation_enum.LIKE,"Yes","id.partnerType",Operation_enum.LIKE,type,"id.sbankDevelopId",Operation_enum.ORDERBY,Operation_enum.DESC);
                list = scBankDevpartnerService.findAdminList(getOffset(), getLimit(),"id.isAdmin",Operation_enum.LIKE,"Yes","id.partnerType",Operation_enum.LIKE,type,"id.isMakerChecker",Operation_enum.NE,"BranchAdmin","id.partnerId",Operation_enum.ORDERBY,Operation_enum.DESC);
            }else{
                String orderClause = null;
                if(getSortCol().equalsIgnoreCase("fullName")){
                    orderClause = "id.fullName";
                }else if(getSortCol().equalsIgnoreCase("emailId")){
                    orderClause = "id.emailId";
                }else if(getSortCol().equalsIgnoreCase("sbDevelopName")){
                    orderClause = "id.sbDevelopName";
                }
                if(getSortOrder.equalsIgnoreCase("asc")){
                    list = scBankDevpartnerService.findAdminList(getOffset(), getLimit(),"id.isAdmin",Operation_enum.LIKE,"Yes","id.isMakerChecker",Operation_enum.NE,"BranchAdmin","id.partnerType",Operation_enum.LIKE,type,orderClause,Operation_enum.ORDERBY,Operation_enum.ASC);
                }else if(getSortOrder.equalsIgnoreCase("desc")){
                    list = scBankDevpartnerService.findAdminList(getOffset(), getLimit(),"id.isAdmin",Operation_enum.LIKE,"Yes","id.isMakerChecker",Operation_enum.NE,"BranchAdmin","id.partnerType",Operation_enum.LIKE,type,orderClause,Operation_enum.ORDERBY,Operation_enum.DESC);
                }
            }
            //Object[] values={"id.isAdmin",Operation_enum.LIKE,"Yes","id.partnerType",Operation_enum.LIKE,type,"id.partnerId",Operation_enum.ORDERBY,Operation_enum.DESC};
           /* for (VwGetSbDevPartner vwGetSbDevPartner : ) {
                adminList.add(vwGetSbDevPartner);
            }*/
        }
        }catch(Exception e)
        {
            logger.error("getAdminList : "+logUserId+" : "+e);
        }
         logger.debug("getAdminList : "+logUserId+" Ends ");
        return list;
    }

    /**
     *Give admin List when using jquery search functionality
     * @param type schedule bank or development partner
     * @param searchField search field
     * @param searchString search string
     * @param searchOper search operator
     * @return list of records
     * @throws Exception
     */
    public List<VwGetSbDevPartner> getAdminList(String type, String searchField,String searchString,String searchOper){
        logger.debug("getAdminList : "+logUserId+" Starts ");
        try{
        if (adminList.isEmpty()) {
            if(searchOper.equalsIgnoreCase("CN")){
                searchString = "%" + searchString + "%";
            }
            Object[] values={"id.isAdmin",Operation_enum.LIKE,"Yes","id.partnerType",Operation_enum.LIKE,type,"id.isMakerChecker",Operation_enum.NE,"BranchAdmin", "id."+searchField, Operation_enum.LIKE, searchString, "id.partnerId",Operation_enum.ORDERBY,Operation_enum.DESC};
            for (VwGetSbDevPartner vwGetSbDevPartner : scBankDevpartnerService.findAdminList(getOffset(), getLimit(), values)) {
                adminList.add(vwGetSbDevPartner);
            }
        }
        }catch(Exception e)
        {
            logger.error("getAdminList : "+logUserId+" : "+e);
        }
        logger.debug("adminlist "+adminList);
        logger.debug("getAdminList : "+logUserId+" Ends ");
        return adminList;
    }
    

    /**
     *getter of admin list
     * @return
     */
    public List<VwGetSbDevPartner> getAdminList()
    {
        return adminList;
    }

    /**
     *setter of adminList
     * @param adminList
     */
    public void setAdminList(List<VwGetSbDevPartner> adminList)
    {
        this.adminList = adminList;
    }

    /**
     *Getter of sort order
     * @return
     */
    public String getGetSortOrder()
    {
        return getSortOrder;
    }

    /**
     *setter of sortOrder
     * @param getSortOrder
     */
    public void setGetSortOrder(String getSortOrder)
    {
        this.getSortOrder = getSortOrder;
    }

    /**
     *ScBankDevpartnerService getter
     * @return
     */
    public ScBankDevpartnerService getScBankDevpartnerService()
    {
        return scBankDevpartnerService;
    }

    /**
     *ScBankDevpartnerService setter
     * @param scBankDevpartnerService
     */
    public void setScBankDevpartnerService(ScBankDevpartnerService scBankDevpartnerService)
    {
        this.scBankDevpartnerService = scBankDevpartnerService;
    }
    
    /**
     *give record list of branch admin 
     * @param type scheduled bank
     * @return List of record
     * @throws Exception
     */
    public List<VwGetSbDevPartner> getBranchAdminList(String type)  {
        logger.debug("getBranchAdminList : "+logUserId+" Starts ");
        List<VwGetSbDevPartner> list=null;
        try{
        if (adminList.isEmpty()) {
            if(getSortCol().equalsIgnoreCase("")){
                //list = scBankDevpartnerService.findAdminList(getOffset(), getLimit(),"id.isAdmin",Operation_enum.LIKE,"Yes","id.partnerType",Operation_enum.LIKE,type,"id.sbankDevelopId",Operation_enum.ORDERBY,Operation_enum.DESC);
                list = scBankDevpartnerService.findAdminList(getOffset(), getLimit(),"id.isAdmin",Operation_enum.LIKE,"Yes","id.partnerType",Operation_enum.LIKE,type,"id.partnerId",Operation_enum.ORDERBY,Operation_enum.DESC,"id.isMakerChecker",Operation_enum.EQ,"BranchAdmin");
            }else{
                String orderClause = null;
                if(getSortCol().equalsIgnoreCase("fullName")){
                    orderClause = "id.fullName";
                }else if(getSortCol().equalsIgnoreCase("emailId")){
                    orderClause = "id.emailId";
                }else if(getSortCol().equalsIgnoreCase("sbDevelopName")){
                    orderClause = "id.sbDevelopName";
                }
                if(getSortOrder.equalsIgnoreCase("asc")){
                    list = scBankDevpartnerService.findAdminList(getOffset(), getLimit(),"id.isAdmin",Operation_enum.LIKE,"Yes","id.partnerType",Operation_enum.LIKE,type,orderClause,Operation_enum.ORDERBY,Operation_enum.ASC,"id.isMakerChecker",Operation_enum.EQ,"BranchAdmin");
                }else if(getSortOrder.equalsIgnoreCase("desc")){
                    list = scBankDevpartnerService.findAdminList(getOffset(), getLimit(),"id.isAdmin",Operation_enum.LIKE,"Yes","id.partnerType",Operation_enum.LIKE,type,orderClause,Operation_enum.ORDERBY,Operation_enum.DESC,"id.isMakerChecker",Operation_enum.EQ,"BranchAdmin");
                }
            }
            //Object[] values={"id.isAdmin",Operation_enum.LIKE,"Yes","id.partnerType",Operation_enum.LIKE,type,"id.partnerId",Operation_enum.ORDERBY,Operation_enum.DESC};
           /* for (VwGetSbDevPartner vwGetSbDevPartner : ) {
                adminList.add(vwGetSbDevPartner);
            }*/
        }
        }catch(Exception e)
        {
            logger.error("getBranchAdminList : "+logUserId+" : "+e);
        }
        logger.debug("getBranchAdminList : "+logUserId+" Ends ");
        return list;
    }
    /**
     *
     *give record list of branch admin when using jquery search
     * @param type scheduled bank
     * @param searchField 
     * @param searchString 
     * @param searchOper
     * @return List of record
     * @throws Exception
     */
    public List<VwGetSbDevPartner> getBranchAdminList(String type, String searchField,String searchString,String searchOper) {
        logger.debug("getBranchAdminList : "+logUserId+" Starts ");
        try{
        if (adminList.isEmpty()) {
            if(searchOper.equalsIgnoreCase("CN")){
                searchString = "%" + searchString + "%";
            }
            Object[] values={"id.isAdmin",Operation_enum.LIKE,"Yes","id.partnerType",Operation_enum.LIKE,type,"id.isMakerChecker",Operation_enum.EQ,"BranchAdmin" ,"id."+searchField, Operation_enum.LIKE, searchString, "id.partnerId",Operation_enum.ORDERBY,Operation_enum.DESC};
            for (VwGetSbDevPartner vwGetSbDevPartner : scBankDevpartnerService.findAdminList(getOffset(), getLimit(), values)) {
                adminList.add(vwGetSbDevPartner);
            }
        }
        }catch(Exception e)
        {
             logger.error("getBranchAdminList : "+logUserId+" : "+e);
        }
         logger.debug("adminList "+adminList);
        logger.debug("getBranchAdminList : "+logUserId+" Ends ");
        return adminList;
    }

     /**
      *count of branchadmin when using search
      * @param type schedulebank
      * @param searchField search field
      * @param searchString search string
      * @param searchOper search operator
      * @return count of records
      * @throws Exception
      */
     public long getAllCountOfBranchDept(String type, String searchField,String searchString,String searchOper){
         logger.debug("getAllCountOfBranchDept : "+logUserId+" Starts ");
         long lng =0;
         try {
        if(searchOper.equalsIgnoreCase("CN")){
            searchString = "%" + searchString + "%";
        }
                lng = scBankDevpartnerService.getCntOfficeAdmin("VwGetSbDevPartner sbdp", "sbdp.id.partnerType = '"+type+"' AND sbdp.id.isAdmin = 'Yes' and sbdp.id.isMakerChecker='BranchAdmin' and " + searchField + " LIKE '" + searchString + "' ");
         } catch (Exception e) {
            logger.error("getAllCountOfBranchDept : "+logUserId+" : "+e);
    }
         logger.debug("getAllCountOfBranchDept : "+logUserId+" Ends ");
         return lng;
    }
     /**
      *Count of record of branchadmin
      * @param type schedulebank
      * @return count of record
      * @throws Exception
      */
     public long getAllCountOfBranchDept(String type) {
        logger.debug("getAllCountOfBranchDept : "+logUserId+" Starts ");
        long lng =0;
         try {
             lng = scBankDevpartnerService.getCntOfficeAdmin("VwGetSbDevPartner sbdp", "sbdp.id.partnerType = '"+type+"' AND sbdp.id.isAdmin = 'Yes' and sbdp.id.isMakerChecker='BranchAdmin'");
         } catch (Exception e) {
             logger.error("getAllCountOfBranchDept : "+logUserId+" : "+e);
    }
         logger.debug("getAllCountOfBranchDept : "+logUserId+" Ends ");
         return lng;
    }
     /**
      *Count of record for scheduled bank admin view scheduled branch admin
      * @param type schedulebank
      * @param userId session userId
      * @return count of record
      * @throws Exception
      */
     public long getAllCountOfBranchAdmin(String type,int userId){
         logger.debug("getAllCountOfBranchAdmin : "+logUserId+" Starts ");
         long count = 0;
         try {
            ScBankDevpartnerSrBean scBankCreationSrBean = new ScBankDevpartnerSrBean();
                scBankCreationSrBean.getBankName(userId,(byte)7);
                userId = scBankCreationSrBean.getsBankDevelopId();
                count = scBankDevpartnerService.getCntOfficeAdmin("VwGetSbDevPartner sbdp","id.isAdmin='Yes' and id.isMakerChecker='BranchAdmin' and id.partnerType='"+type+"' and id.sbankDevelHeadId="+userId);
                long count1 = scBankDevpartnerService.getCntOfficeAdmin("VwGetSbDevPartner sbdp","id.isAdmin='No' and id.isMakerChecker='BranchAdmin' and id.partnerType='"+type+"' and id.sbankDevelopId="+userId);
                count = count+count1;
         } catch (Exception e) {
             logger.error("getAllCountOfBranchAdmin : "+logUserId+" : "+e);
         }
         logger.debug("getAllCountOfBranchAdmin : "+logUserId+" Ends ");
                return count;
     }

     /**
      *This method is used when scheduled bank admin view the List
      * @param type
      * @param userId
      * @param searchField
      * @param searchString
      * @param searchOper
      * @return List of object
      * @throws Exception
      */
     public List<Object[]> getUserListAdmin(String type,int userId,String searchField, String searchString,String searchOper) {
       logger.debug("getUserListAdmin : "+logUserId+" Starts ");
       List<Object[]> userList1 = null;
       try{
        String orderClause = null;
        Object e = null;
        if(getSortCol().equals("")){
            orderClause = "LM.userId";
            e = Operation_enum.DESC;
        }else if(getSortCol().equalsIgnoreCase("fullName")){
            if(getSortOrder.equalsIgnoreCase("asc")){
                orderClause = "PA.fullName";
                e = Operation_enum.ASC;
            }
            else if(getSortOrder.equalsIgnoreCase("desc")){
                orderClause = "PA.fullName";
                e = Operation_enum.DESC;
            }
        }else if(getSortCol().equalsIgnoreCase("emailId")){
            if(getSortOrder.equalsIgnoreCase("asc")){
                orderClause = "LM.emailId";
                e = Operation_enum.ASC;
            }
            else if(getSortOrder.equalsIgnoreCase("desc")){
                orderClause = "LM.emailId";
                e = Operation_enum.DESC;
            }
        }else if(getSortCol().equalsIgnoreCase("sbDevelopName")){
            if(getSortOrder.equalsIgnoreCase("asc")){
                orderClause = "SBP.sbDevelopName";
                e = Operation_enum.ASC;
            }
            else if(getSortOrder.equalsIgnoreCase("desc")){
                orderClause = "SBP.sbDevelopName";
                e = Operation_enum.DESC;
            }
         }else if (getSortCol().equalsIgnoreCase("isMakerChecker")) {
            if(getSortOrder.equalsIgnoreCase("asc")){
                orderClause = "PA.isMakerChecker";
                e = Operation_enum.ASC;
            }
            else if(getSortOrder.equalsIgnoreCase("desc")){
                orderClause = "PA.isMakerChecker";
                e = Operation_enum.DESC;
            }
        }if(searchField.equalsIgnoreCase("sbDevelopName")){
                            searchField = "and SBP.sbDevelopName ";
                        }
                        else if(searchField.equalsIgnoreCase("fullName")){
                            searchField = "and PA.fullName ";
                        }else if(searchField.equalsIgnoreCase("emailId")){
                            searchField = "and LM.emailId ";
                        }else if(searchField.equalsIgnoreCase("isMakerChecker")){
                            searchField = "and PA.isMakerChecker ";
                        }
                        if(searchOper.equalsIgnoreCase("CN")){
                            searchString = "Like '%"+searchString+"%'";
                        }
                        else if(searchOper.equalsIgnoreCase("EQ")){
                            searchString = "= '"+searchString+"'";
                        }
                        userList1 = new ArrayList<Object[]>();
        if (userList1.isEmpty()) {
                ScBankDevpartnerSrBean scBankCreationSrBean = new ScBankDevpartnerSrBean();
                scBankCreationSrBean.getBankName(userId, (byte) 7);
                userId = scBankCreationSrBean.getsBankDevelopId();
                userList1 = scBankDevpartnerService.getBranchAdminListGrid(getOffset(), getLimit(), userId, type,orderClause,e.toString(),searchField,searchString);
        }
        }catch(Exception e)
        {
            logger.error("getUserListAdmin : "+logUserId+" : "+e);
        }
        logger.debug("getUserListAdmin : "+logUserId+" Ends ");
        return userList1;
    }

}
