/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.web.servicebean.GenreatePdfCmd;
import com.cptu.egp.eps.web.utility.XMLReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class AllQueryRespServlet extends HttpServlet {

    String logUserId = "";
    private String strDriveLetter =  XMLReader.getMessage("driveLatter");
    private boolean isDevelopment = false;
    private boolean isStaging = false;
    private boolean isProduction = false;
    private boolean isTraining = false;
    public String path = strDriveLetter+"/eGP/PDF";
    final Logger logger = Logger.getLogger(AllQueryRespServlet.class);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        //PrintWriter out = response.getWriter();
        try {
            logger.debug("processRequest " + logUserId + " Starts");
            HttpSession session = request.getSession();
            if (session.getAttribute("userId") != null) {
                logUserId = session.getAttribute("userId").toString();
            }
            String reqURL = request.getRequestURL().toString();
            String encodedpdfName = "";
            String decodedpdfName = "";
            if (request.getParameter("pdfName") != null) {
                encodedpdfName = request.getParameter("pdfName");
                decodedpdfName = URLDecoder.decode(encodedpdfName, "UTF-8");
            }
            try {

            // Check the server information
            if(reqURL.contains("development.eprocure.gov.bd") || reqURL.contains("10.1.3.21")){
                isDevelopment = true;
            }
            if(reqURL.contains("staging.eprocure.gov.bd") || reqURL.contains("10.1.4.21")){
                isStaging = true;
            }
            if(reqURL.contains("training.eprocure.gov.bd") || reqURL.contains("10.1.40.25")){
                isTraining = true;
            }
            if(reqURL.contains("www.eprocure.gov.bd") || reqURL.contains("10.1.2.26") || reqURL.contains("10.1.2.27")){
                isProduction = true;
            }
            
         /*   if (isDevelopment || isTraining) {
                strDriveLetter = "D:";
            } else if (isStaging) {
                strDriveLetter = "E:";
            } else if (isProduction) {
                strDriveLetter = "L:";
            }
*/
            //path = strDriveLetter + "/eGP/PDF/";
            //System.out.println("path" + path);

            String folderName = request.getParameter("folderName");
            String isPDF = request.getParameter("isPDF");
            String id = request.getParameter("id");
            logger.debug("processRequest " + logUserId + " " + folderName + " Starts");
            logger.debug("processRequest reqURL : " + reqURL + " folderName : " + folderName + " pdfName : " +  decodedpdfName + " id : " + id);

            try {

                //to prompt user for Open and Save
                FileInputStream fis = null;
                if (!"".equalsIgnoreCase(decodedpdfName)) {
                    fis = new FileInputStream(path +"/"+ folderName + "/" + id + "/" + decodedpdfName + ".pdf");
                    response.setContentType("application/pdf");
                    response.setHeader("Content-Disposition", "attachment;filename=\"" + decodedpdfName + ".pdf\"");
                } else {
                    File file = new File(path +"/"+ folderName + "/" + id + "/" + folderName + ".pdf");
                    if(file.exists()){
                       file.delete(); 
                    }
                    if(!file.exists() && isPDF.equalsIgnoreCase("true")){
                        //if(folderName.equalsIgnoreCase("TenderNotice")){
                            GenreatePdfCmd obj = new GenreatePdfCmd();
                            String tmpURL = reqURL.replace("AllQueryRespServlet", "officer/AllQeryResp.jsp");
                            String reqQuery="tenderid="+ request.getParameter("id") +"&comType=TEC&id="+id+"&isPDF=true";
                            obj.genrateCmd(tmpURL, reqQuery, folderName, id);
                        //}
                    }
                    fis = new FileInputStream(path +"/"+ folderName + "/" + id + "/" + folderName + ".pdf");
                    response.setContentType("application/pdf");
                    response.setHeader("Content-Disposition", "attachment;filename=\"" + folderName + ".pdf\"");
                }
                ServletOutputStream outputStream = response.getOutputStream();

                byte[] buf = new byte[4 * 1024 * 1024]; // 4K buffer

                //byte[] buf = new byte[Integer.valueOf(request.getParameter("docSize"))];
                int offset = 0;
                int numRead = 0;
                while ((offset < buf.length)
                        && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {
                    offset += numRead;

                }
                //ServletOutputStream outputStream =  response.getOutputStream();
                fis.close();
                outputStream.write(buf);
                outputStream.flush();
                outputStream.close();

            } catch (IOException e1) {
                logger.error("processRequest " + logUserId + " :" + e1);
                e1.printStackTrace();
            } catch (Exception e) {
                logger.error("processRequest " + logUserId + " :" + e);
                e.printStackTrace();
            }

        } catch (Exception e) {
            logger.error("processRequest " + logUserId + " :" + e);
            e.printStackTrace();
        }
        logger.debug("processRequest " + logUserId + " Ends");

        } finally {
            //out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
