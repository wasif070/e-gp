/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.databean;

/**
 *
 * @author Naresh.Akurathi
 */
public class ProgrammeMasterDtBean {

    String progCode;
    String progName;
    String ProgOwner;
    int createdBy;

    public String getProgCode() {
        return progCode;
    }

    public void setProgCode(String progCode) {
        this.progCode = progCode;
    }

    public String getProgName() {
        return progName;
    }

    public void setProgName(String progName) {
        this.progName = progName;
    }

    public String getProgOwner() {
        return ProgOwner;
    }

    public void setProgOwner(String ProgOwner) {
        this.ProgOwner = ProgOwner;
    }
    
    public int getcreatedBy() {
        return createdBy;
    }

    public void setcreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }
}
