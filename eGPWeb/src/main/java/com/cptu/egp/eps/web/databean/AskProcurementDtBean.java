/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author rishita
 */
public class AskProcurementDtBean {

    private int procQueId;
    private int queCatId;
    private String question;
    private String postedDateString;
    private Date postedDate;
    private int postedBy;
    private String answer;
    private String answerDateString;
    private Date answerDate;
    private Integer answeredBy;
    private String answeredByName;
    private String postedByName;
    private Integer userTypeId;

    public Integer getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(Integer userTypeId) {
        this.userTypeId = userTypeId;
    }

    public String getPostedByName() {
        return postedByName;
    }

    public void setPostedByName(String postedByName) {
        this.postedByName = postedByName;
    }

    public String getAnsweredByName() {
        return answeredByName;
    }

    public void setAnsweredByName(String answeredByName) {
        this.answeredByName = answeredByName;
    }
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd whh:mm:ss");

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Date getAnswerDate() {
        return answerDate;
    }

    public void setAnswerDate(Date answerDate) {
        this.answerDate = answerDate;
    }

    public String getAnswerDateString() {
        return answerDateString;
    }

    public void setAnswerDateString(String answerDateString) throws ParseException {
        this.answerDateString = answerDateString;
        setAnswerDate(dateFormat.parse(answerDateString));
    }

    public Integer getAnsweredBy() {
        return answeredBy;
    }

    public void setAnsweredBy(Integer answeredBy) {
        this.answeredBy = answeredBy;
    }

    public int getPostedBy() {
        return postedBy;
    }

    public void setPostedBy(int postedBy) {
        this.postedBy = postedBy;
    }

    public Date getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    public String getPostedDateString() {
        return postedDateString;
    }

    public void setPostedDateString(String postedDateString) throws ParseException {
        this.postedDateString = postedDateString;
        setPostedDate(dateFormat.parse(postedDateString));
    }

    public int getProcQueId() {
        return procQueId;
    }

    public void setProcQueId(int procQueId) {
        this.procQueId = procQueId;
    }

    public int getQueCatId() {
        return queCatId;
    }

    public void setQueCatId(int queCatId) {
        this.queCatId = queCatId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
}
