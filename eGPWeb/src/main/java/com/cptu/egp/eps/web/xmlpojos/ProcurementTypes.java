/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Chalapathi.Bavisetti
 */
@XmlRootElement
public class ProcurementTypes {
private byte procurementTypeId;
private String procurementType;

    /**
     * @return the procurementTypeId
     */
 @XmlElement(nillable = true)
    public byte getProcurementTypeId() {
        return procurementTypeId;
    }

    /**
     * @param procurementTypeId the procurementTypeId to set
     */
    public void setProcurementTypeId(byte procurementTypeId) {
        this.procurementTypeId = procurementTypeId;
    }

    /**
     * @return the procurementType
     */
     @XmlElement(nillable = true)
    public String getProcurementType() {
        return procurementType;
    }

    /**
     * @param procurementType the procurementType to set
     */
    public void setProcurementType(String procurementType) {
        this.procurementType = procurementType;
    }

   
   
}
