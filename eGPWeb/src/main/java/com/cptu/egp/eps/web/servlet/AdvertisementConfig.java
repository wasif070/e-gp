/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.daointerface.TblAdvtConfigurationMasterDao;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblAdvtConfigurationMaster;
import com.cptu.egp.eps.service.serviceinterface.AdvertisementService;
import com.cptu.egp.eps.service.serviceinterface.AdvtConfigMasterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
/**
 *
 * @author spandana.vaddi
 */
public class AdvertisementConfig extends HttpServlet {

    private final AdvtConfigMasterService advtConfigMasterService = (AdvtConfigMasterService) AppContext.getSpringBean("advtconfigMasterService");
    private final AdvertisementService advertisementService = (AdvertisementService) AppContext.getSpringBean("AdvertisementService");
    static final Logger LOGGER = Logger.getLogger(AdvertisementConfig.class);
    private String logUserId = "0";
    TblAdvtConfigurationMasterDao advertisementConfigDao;

    public TblAdvtConfigurationMasterDao getAdvertisementConfigDao() {
        return advertisementConfigDao;
    }

    public void setAdvertisementConfigDao(TblAdvtConfigurationMasterDao advertisementConfigDao) {
        this.advertisementConfigDao = advertisementConfigDao;
    }

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        Object objUserId = session.getAttribute("userId");
        try {
            boolean flag = false;
            String funct = request.getParameter("funct");
            String functionName = request.getParameter("funcName");
            if (objUserId != null) {
                logUserId = objUserId.toString();
            }
            if (request.getParameter("button") != null) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                TblAdvtConfigurationMaster tblAdvertisementConfig = new TblAdvtConfigurationMaster();
                tblAdvertisementConfig.setUserId("0");
                tblAdvertisementConfig.setCreateDate(format.parse(format.format(new Date())));
                tblAdvertisementConfig.setDimensions(request.getParameter("dimension1") + "x" + request.getParameter("dimension2"));
                tblAdvertisementConfig.setFileFormat(request.getParameter("fileFormat"));
                tblAdvertisementConfig.setInstructions(request.getParameter("instructions"));
                tblAdvertisementConfig.setLocation(request.getParameter("location"));
                tblAdvertisementConfig.setFee(0);
                tblAdvertisementConfig.setBannerSize(request.getParameter("bannerSize"));
                String durationta = request.getParameter("durationt1");
                String durationtb = request.getParameter("durationt2");
                String durationtc = request.getParameter("durationt3");
                String durationt1;
                String durationt2;
                String durationt3;
                if (durationta == null) {
                    durationt1 = "0";
                } else {
                    durationt1 = durationta;
                }
                if (durationtb == null) {
                    durationt2 = "0";
                } else {
                    durationt2 = durationtb;
                }
                if (durationtc == null) {
                    durationt3 = "0";
                } else {
                    durationt3 = durationtc;
                }
                tblAdvertisementConfig.setDuration(durationt1 + "days," + durationt2 + "months," + durationt3 + "years");
                advtConfigMasterService.insertConfigMaster(tblAdvertisementConfig);
                MailContentUtility mailContentUtility = new MailContentUtility();
                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                String[] mail = {XMLReader.getMessage("msgboxfrom")};
                String mailText = mailContentUtility.adminMail();
                sendMessageUtil.setEmailTo(mail);
                sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                sendMessageUtil.setEmailSub("eGP: Advertisement publish");
                sendMessageUtil.setEmailMessage(mailText);
                advertisementService.contentAdmMsgBox(XMLReader.getMessage("msgboxfrom"), XMLReader.getMessage("emailIdNoReply"), HandleSpecialChar.handleSpecialChar("Configuration of price"), mailContentUtility.adminMail());
                sendMessageUtil.sendEmail();
                mail = null;
                response.sendRedirect("/admin/ConfigureAdvt.jsp");
            } else if (request.getParameter("funct") != null && request.getParameter("funct").equalsIgnoreCase("edit")) {
                List<TblAdvtConfigurationMaster> list1 = advtConfigMasterService.findTblAdvtConfigurationMaster("adConfigId", Operation_enum.EQ, Integer.parseInt(request.getParameter("adConfigId")));
                request.setAttribute("Advertisement", list1);
                RequestDispatcher rd = request.getRequestDispatcher("/admin/EditAdvtConfig.jsp");
                rd.forward(request, response);
            } else if (!(functionName == null) && ("location".equalsIgnoreCase(functionName))) {
                                System.out.println("loc....."+request.getParameter("location")+"..."+request.getParameter("duration"));

                List<TblAdvtConfigurationMaster> list1 = advtConfigMasterService.findTblAdvtConfigurationMaster("location", Operation_enum.EQ, (request.getParameter("location")).toString(),"duration", Operation_enum.EQ, (request.getParameter("duration")).toString());
                System.out.println(".........list1.........."+list1);
                String str = "";
                if(list1 !=null && list1.size()<=0){
                   str = "succ";
                }
                else{
                   str = "fail";
                }
                out.write(str);
                out.close();
            } else if ("submitEdit".equalsIgnoreCase(funct)) {
                String location = request.getParameter("txtlocation");
                String duration = request.getParameter("txtperiod");
                double fee = Double.parseDouble(request.getParameter("txtprice"));
                String size = request.getParameter("txtsize");
                String dimension = request.getParameter("txtdimension");
                String adConfigId = request.getParameter("adConfigId");
                TblAdvtConfigurationMaster tblAdvtConfigurationMaster = null;
                tblAdvtConfigurationMaster = ((List<TblAdvtConfigurationMaster>) advtConfigMasterService.findTblAdvtConfigurationMaster("adConfigId", Operation_enum.EQ, Integer.parseInt(request.getParameter("adConfigId")))).get(0);
                tblAdvtConfigurationMaster.setDuration(duration);
                tblAdvtConfigurationMaster.setLocation(location);
                tblAdvtConfigurationMaster.setFee(fee);
                tblAdvtConfigurationMaster.setBannerSize(size);
                tblAdvtConfigurationMaster.setDimensions(dimension);
                advtConfigMasterService.updateTblAdvtConfigurationMaster(tblAdvtConfigurationMaster);
                response.sendRedirect("/AdvertisementConfig?msg=editsucc&funct=EGPadminConf");
            } else if ("EGPadminConf".equalsIgnoreCase(funct)) {
                List<TblAdvtConfigurationMaster> list = advtConfigMasterService.getAllTblAdvtConfigurationMaster();
                request.setAttribute("items", list); // It's now available as ${items} in EL.
                RequestDispatcher rd = request.getRequestDispatcher("/admin/Advertisement.jsp");
                rd.forward(request, response);
            } else if (request.getParameter("action") != null && request.getParameter("action").equalsIgnoreCase("delete")) {
                TblAdvtConfigurationMaster tbl = advtConfigMasterService.findTblAdvtConfigurationMaster("adConfigId", Operation_enum.EQ, Integer.parseInt(request.getParameter("id"))).get(0);
                advtConfigMasterService.deleteTblAdvtConfigurationMaster(tbl);
                response.sendRedirect("/admin/ConfigureAdvt.jsp");
            } else {
                response.sendRedirect("/admin/ConfigureAdvt.jsp");
            }
        } catch (Exception exp) {
            exp.printStackTrace();
            LOGGER.error("processRequest : " + logUserId + " : " + exp);
        } finally {
            out.close();
        }



        LOGGER.debug("processRequest : " + logUserId + " Ends");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
