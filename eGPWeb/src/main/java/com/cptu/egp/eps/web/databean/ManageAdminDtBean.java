/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

/**
 *
 * @author rishita
 */
public class ManageAdminDtBean {

    private byte userTypeId;
    private int userId;
    private short adminId;
    private String userType;
    private String mobileNo;
    private String fullName;
    private String nationalId;
    private String emailId;
    private String departmentName;
    private String phoneNo;
    private int officeId;
    private String officeName;
    private int departmentId;
    private int stateId;
    private String stateName;
    private String status;
    private int rollId;

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }
    

//    public ManageAdminDtBean(String officeName, int officeId, int departmentId) {
//        this.officeName = officeName;
//        this.officeId = officeId;
//        this.departmentId = departmentId;
//    }

//    public ManageAdminDtBean(String officeName, int officeId,String departmentName) {
//        this.officeName = officeName;
//        this.officeId = officeId;
//        this.departmentName = departmentName;
//    }
    

    public ManageAdminDtBean(String officeName, String departmentName, int officeId) {
        this.officeName = officeName;
        this.departmentName = departmentName;
        this.officeId = officeId;
    }

      public ManageAdminDtBean(String officeName, String departmentName, int officeId, int departmentId) {
        this.officeName = officeName;
        this.departmentName = departmentName;
        this.officeId = officeId;
        this.departmentId = departmentId;
    }

    public ManageAdminDtBean(Integer officeId, String officeName) {
        this.officeName = officeName;
        this.officeId = officeId;
    }

    public ManageAdminDtBean(int userId,String emailId, String fullName) {
        this.emailId = emailId;
        this.fullName = fullName;
        this.userId = userId;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public int getOfficeId() {
        return officeId;
    }

    public void setOfficeId(int officeId) {
        this.officeId = officeId;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String strStatus) {
        this.status = strStatus;
    }

    public ManageAdminDtBean(int userId, String emailId, String fullName, short adminId, String nationalId, String mobileNo, String phoneNo) {
        this.userId = userId;
        this.emailId = emailId;
        this.fullName = fullName;
        this.adminId = adminId;
        this.nationalId = nationalId;
        this.mobileNo = mobileNo;
        this.phoneNo = phoneNo;
    }

    public ManageAdminDtBean(int userId, String emailId, String fullName, short adminId, String nationalId, String mobileNo, String phoneNo,String officeName) {
        this.userId = userId;
        this.emailId = emailId;
        this.fullName = fullName;
        this.adminId = adminId;
        this.nationalId = nationalId;
        this.mobileNo = mobileNo;
        this.phoneNo = phoneNo;
        this.officeName = officeName;
       
    }
    public ManageAdminDtBean(int userId, String emailId, String fullName, short adminId, String nationalId, String mobileNo, String phoneNo,String officeName,String status) {
        this.userId = userId;
        this.emailId = emailId;
        this.fullName = fullName;
        this.adminId = adminId;
        this.nationalId = nationalId;
        this.mobileNo = mobileNo;
        this.phoneNo = phoneNo;
        this.officeName = officeName;
        this.status = status;
    }


    public ManageAdminDtBean(int userId, String emailId, String fullName, short adminId, String nationalId, String mobileNo, String phoneNo, int officeId, String officeName, int stateId, String stateName) {
        this.userId = userId;
        this.emailId = emailId;
        this.fullName = fullName;
        this.adminId = adminId;
        this.nationalId = nationalId;
        this.mobileNo = mobileNo;
        this.phoneNo = phoneNo;
        this.stateId = stateId;
        this.stateName = stateName;
        this.officeId = officeId;
        this.officeName = officeName;
    }

    public ManageAdminDtBean(int userId, String userType, String mobileNo, String nationalId, String fullName, byte userTypeId, String emailId, short adminId, String phoneNo) {
        this.userId = userId;
        this.userType = userType;
        this.mobileNo = mobileNo;
        this.nationalId = nationalId;
        this.fullName = fullName;
        this.userTypeId = userTypeId;
        this.emailId = emailId;
        this.adminId = adminId;
        this.phoneNo = phoneNo;
    }
    public ManageAdminDtBean(int userId, String userType, String mobileNo, String nationalId, String fullName, byte userTypeId, String emailId, short adminId, String phoneNo, String status) {
        this.userId = userId;
        this.userType = userType;
        this.mobileNo = mobileNo;
        this.nationalId = nationalId;
        this.fullName = fullName;
        this.userTypeId = userTypeId;
        this.emailId = emailId;
        this.adminId = adminId;
        this.phoneNo = phoneNo;
        this.status = status;
    }

     public ManageAdminDtBean(int userId, String userType, String mobileNo, String nationalId, String fullName, byte userTypeId, String emailId, short adminId, String phoneNo, String status, int rollId) {
        this.userId = userId;
        this.userType = userType;
        this.mobileNo = mobileNo;
        this.nationalId = nationalId;
        this.fullName = fullName;
        this.userTypeId = userTypeId;
        this.emailId = emailId;
        this.adminId = adminId;
        this.phoneNo = phoneNo;
        this.status = status;
        this.rollId = rollId;
    }

    public short getAdminId() {
        return adminId;
    }

    public void setAdminId(short adminId) {
        this.adminId = adminId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public byte getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(byte userTypeId) {
        this.userTypeId = userTypeId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public int getRollId() {
        return rollId;
    }

    public void setRollId(int rollId) {
        this.rollId = rollId;
    }

    
    public ManageAdminDtBean(int userId, String userType, String mobileNo, String nationalId, String fullName, byte userTypeId, String emailId, String departmentName, short adminId, String phoneNo) {
        this.userId = userId;
        this.userType = userType;
        this.mobileNo = mobileNo;
        this.nationalId = nationalId;
        this.fullName = fullName;
        this.userTypeId = userTypeId;
        this.emailId = emailId;
        this.departmentName = departmentName;
        this.adminId = adminId;
        this.phoneNo = phoneNo;
    }

    public ManageAdminDtBean(int userId, String userType, String mobileNo, String nationalId, String fullName, byte userTypeId, String emailId, String departmentName, short adminId, String phoneNo,String status) {
        this.userId = userId;
        this.userType = userType;
        this.mobileNo = mobileNo;
        this.nationalId = nationalId;
        this.fullName = fullName;
        this.userTypeId = userTypeId;
        this.emailId = emailId;
        this.departmentName = departmentName;
        this.adminId = adminId;
        this.phoneNo = phoneNo;
        this.status = status;
    }

    public ManageAdminDtBean() {
    }

    public ManageAdminDtBean(int userId,String emailId, String fullName,String officeName) {
        this.emailId = emailId;
        this.fullName = fullName;
        this.userId = userId;
        this.officeName = officeName;
    }
    //for pe admin transfer done by palash
        public ManageAdminDtBean(int userId,String emailId, String fullName,String officeName,short adminId) {
        this.emailId = emailId;
        this.fullName = fullName;
        this.userId = userId;
        this.officeName = officeName;
        this.adminId = adminId;
    }
}
// End, Palash
