/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CheckExtension;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import com.cptu.egp.eps.web.utility.CreateZipFile;
import com.cptu.egp.eps.web.utility.XMLReader;

/**
 *
 * @author Administrator
 */
public class TenderSecUploadServlet extends HttpServlet {

    private static final String TMP_DIR_PATH = "c:\\tmp";
    private File tmpDir;
    private static final String DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("TenderSecUploadServlet");
    private File destinationDir;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        tmpDir = new File(TMP_DIR_PATH);
        if (!tmpDir.isDirectory()) {
            tmpDir.mkdir();
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();

        int tenderId = 0;
        File file = null;
        String docSizeMsg = "";
        boolean checkret = false;
        boolean flag = false;
        String documentBrief = "";
        String sectionId = "";
        String inCorri = "no";
        String fileName = "";
        long fileSize = 0;
        String queryString = "";
        String pageName = "officer/TenderDocUpload.jsp";
        response.setContentType("text/html");
        boolean dis = false;
        int pkgOrLotId = -1;
        String folderName = "";
        String lotNo = "";
        if(session.getAttribute("userId")==null){
        response.sendRedirect("SessionTimedOut.jsp");
        }
        else{

        try {
            try {
                if((request.getParameter("funName")!=null) && ("zipdownload".equalsIgnoreCase(request.getParameter("funName")))){
                    file = new File(FilePathUtility.getFilePath().get("TenderSecUploadServlet")+"\\"+request.getParameter("tenderId")+"\\ eTenderDocs_"+request.getParameter("tenderId")+"_"+request.getParameter("lotNo").replace("^", "&") +".zip");
                    if(!file.exists()){
                        file.createNewFile();
                        CreateZipFile createZipFile = new CreateZipFile();
                    createZipFile.zipDirectory(new File(FilePathUtility.getFilePath().get("TenderSecUploadServlet")+request.getParameter("tenderId")+"\\"+request.getParameter("lotNo").replace("^", "&")), file);
                    }
                    InputStream fis = new FileInputStream(file);
                    byte[] buf = new byte[(int)file.length()];
                    int offset = 0;
                    int numRead = 0;
                        while ((offset < buf.length)
                                && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {
                            offset += numRead;
                        }
                    fis.close();
                    response.setContentType("application/octet-stream");
                    response.setHeader("Content-Disposition", "attachment;filename=\"" +file.getName()+ "\"");
                    ServletOutputStream outputStream = response.getOutputStream();
                    outputStream.write(buf);
                    outputStream.flush();
                    outputStream.close();
                }else if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("cancelDoc")) {
                    tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    
                    if(request.getParameter("porlId")!=null){
                        pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
                    }
                    //sectionId = request.getParameter("sectionId");
                    int docId = Integer.parseInt(request.getParameter("docId"));
                    CommonXMLSPService cancelDoc = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                    cancelDoc.insertDataBySP("updatetable", "tbl_TenderSectionDocs", "status='cp'", "tenderSectionDocId="+docId);
                    if(pkgOrLotId == -1){
                        response.sendRedirect("officer/TenderDocPrep.jsp?tenderId=" + tenderId);
                    }else{
                        response.sendRedirect("officer/TenderDocPrep.jsp?tenderId=" + tenderId + "&porlId=" + pkgOrLotId);
                    }
                } else if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("remove")) {
                    String tender = "";
                    int docId = 0;
                    String id = "";
                    String docName = "";
                    if (request.getParameter("docId") != null) {
                        docId = Integer.parseInt(request.getParameter("docId"));

                    }
                    if (request.getParameter("tenderId") != null) {
                        tender = (request.getParameter("tenderId"));

                    }
                    if (request.getParameter("sectionId") != null) {
                        sectionId = (request.getParameter("sectionId"));

                    }

                    if (request.getParameter("docName") != null) {
                        docName = request.getParameter("docName");
                    }
                    
                    if(request.getParameter("porlId")!=null){
                        pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
                    }
                    if(request.getParameter("folderName")!=null){
                        folderName = request.getParameter("folderName").replace("^", "&");
                    }
                    if(request.getParameter("from")!=null){
                        pageName = "officer/TenderDocPrep.jsp";
                    }
                    if(request.getParameter("lotNo")!=null){
                        lotNo = request.getParameter("lotNo").replace("^", "&");
                    }
                    String whereContition = "";
                    whereContition = "tenderSectionDocId= " + docId;
                    fileName = DESTINATION_DIR_PATH + tender + "\\"+request.getParameter("lotNo").replace("^", "&") + "\\" +folderName + "\\" + docName;
                    checkret = deleteFile(fileName);
                    if (checkret) {
                        CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                        CommonMsgChk commonMsgChk;
                        commonMsgChk = commonXMLSPService.insertDataBySP("delete", "tbl_TenderSectionDocs", "", whereContition).get(0);
                        queryString = "?tenderId="+tender+"&sectionId="+sectionId+"&porlId="+pkgOrLotId+"&folderName="+folderName.replace("&", "^")+"&lotNo="+lotNo.replace("^", "&")+"&rm=su";;
                        response.sendRedirect(pageName+queryString);
                    }
                    else
                    {
                        CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                        CommonMsgChk commonMsgChk;
                        commonMsgChk = commonXMLSPService.insertDataBySP("delete", "tbl_TenderSectionDocs", "", whereContition).get(0);
                        response.sendRedirect(pageName+"?tenderId="+tender+"&removeSucc=true");
                    }

                } else if ((request.getParameter("funName") != null) && request.getParameter("funName").equalsIgnoreCase("download")) {
                   /* if(request.getParameter("templateSectionDocId")!=null){
                    file = new File("C:\\eGP\\Document\\OfficerDocuments\\TemplateDocuments\\" + request.getParameter("sectionId") + "\\" + request.getParameter("docName"));//request.getSession().getAttribute("userId")
                    InputStream fis = new FileInputStream(file);
                    byte[] buf = new byte[Integer.valueOf(request.getParameter("docSize"))];
                    int offset = 0;
                    int numRead = 0;
                    while ((offset < buf.length)
                            && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {

                        offset += numRead;

                    }
                    fis.close();
                    //FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                    //buf = fileEncryptDecryptUtil.fileDecryptUtil(buf);
                    response.setContentType("application/octet-stream");
                    response.setHeader("Content-Disposition", "attachment;filename=" + request.getParameter("docName") + "");
                    ServletOutputStream outputStream = response.getOutputStream();
                    outputStream.write(buf);
                    outputStream.flush();
                    outputStream.close();
                    }else{*/
                    file = new File(DESTINATION_DIR_PATH + request.getParameter("tenderId")+ "\\"+request.getParameter("lotNo").replace("^", "&") +"\\" +request.getParameter("folderName").replace("^", "&") + "\\" + request.getParameter("docName"));//request.getSession().getAttribute("userId")
                    if(file.length()>0)
                    {
                        InputStream fis = new FileInputStream(file);
                        byte[] buf = new byte[(int)file.length()];
                        int offset = 0;
                        int numRead = 0;
                        while ((offset < buf.length)
                                && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {

                            offset += numRead;

                        }
                        fis.close();
                        response.setContentType("application/octet-stream");
                        response.setHeader("Content-Disposition", "attachment;filename=\"" + file.getName() + "\"");
                        ServletOutputStream outputStream = response.getOutputStream();
                        outputStream.write(buf);
                        outputStream.flush();
                        outputStream.close();
                    }
                    else
                    {
                        String tender = "";
                        if (request.getParameter("tenderId") != null) {
                            tender = (request.getParameter("tenderId"));
                        }
                        if (request.getParameter("officerPage") != null) {
                            response.sendRedirect("officer/TenderDocPrep.jsp?tenderId="+tender+"&downForm=false");
                        }
                        if (request.getParameter("bidderPage") != null) {
                            response.sendRedirect("tenderer/TenderDocView.jsp?tenderId="+tender+"&downForm=false");
                        }
                        
                    }
                   // }
                }else if ((request.getParameter("funName") != null) && request.getParameter("funName").equalsIgnoreCase("downloadNewDocuments")) {
                   
                    String drive = XMLReader.getMessage("driveLatter");
                    String FileName = request.getParameter("FileName");
                    file = new File(drive + "\\eGP\\Document\\NewsDocuments\\" + FileName);
                    InputStream fis = new FileInputStream(file);
                    byte[] buf = new byte[(int)file.length()];
                    int offset = 0;
                    int numRead = 0;
                    while ((offset < buf.length)
                            && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {

                        offset += numRead;

                    }
                    fis.close();
                    response.setContentType("application/octet-stream");
                    response.setHeader("Content-Disposition", "attachment;filename=\"" + file.getName() + "\"");
                    ServletOutputStream outputStream = response.getOutputStream();
                    outputStream.write(buf);
                    outputStream.flush();
                    outputStream.close();
                   // }
                } else {
                    DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
                    /*
                     *Set the size threshold, above which content will be stored on disk.
                     */
                    fileItemFactory.setSizeThreshold(4 * 1024 * 1024); //1 MB
		/*
                     * Set the temporary directory to store the uploaded files of size above threshold.
                     */
                    fileItemFactory.setRepository(tmpDir);

                    ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
                    /*
                     * Parse the request
                     */
                    List items = uploadHandler.parseRequest(request);
                    Iterator itr = items.iterator();
                    //For Supporting Document
                    while (itr.hasNext()) {
                        FileItem item = (FileItem) itr.next();
                        //For Supporting Document
                    /*
                         * Handle Form Fields.
                         */
                        if (item.isFormField()) {
                          
                            if (item.getFieldName().equals("documentBrief")) {
                                if (item.getString() == null || item.getString().trim().length() == 0) {
                                    dis = true;
                                    break;
                                }
                               
                                documentBrief = item.getString();

                            } else if (item.getFieldName().equals("tenderId")) {
                                tenderId = Integer.parseInt(item.getString());
                            } else if (item.getFieldName().equals("sectionId")) {
                                sectionId = item.getString();
                            } else if (item.getFieldName().equals("inCorri")) {
                                inCorri = item.getString();
                            }
                            else if(item.getFieldName().equals("porlId")){
                                pkgOrLotId = Integer.parseInt(item.getString());
                            }
                            else if(item.getFieldName().equals("folderName")){
                                folderName = item.getString();
                            }
                            else if(item.getFieldName().equals("lotNo")){
                                lotNo = item.getString();
                            }
                            
                        }else {
                                //Handle Uploaded files.
                                
                                /*
                                 * Write file to the ultimate location.
                                 */

                               if (item.getName().lastIndexOf("\\") != -1) {
                                fileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1, item.getName().length());
                            } else {
                                    fileName = item.getName();
                                }
                               //fileName  = fileName.replaceAll(" ", "");
                                String realPath = DESTINATION_DIR_PATH +tenderId+"\\"+ lotNo + "\\" +folderName;//request.getSession().getAttribute("userId")
                                destinationDir = new File(realPath);
                                if (!destinationDir.isDirectory()) {
                                    destinationDir.mkdirs();
                                }
                                docSizeMsg = docSizeMsg(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
                               // docSizeMsg = docSizeMsg(1);//userID.
                                if (!docSizeMsg.equals("ok")) {
                                    //response.sendRedirect("SupportingDocuments.jsp?fq="+docSizeMsg);
                                } else {
                                    fileSize = item.getSize();
                                    checkret = checkExnAndSize(fileName, item.getSize(), "officer");

                                    if (!checkret) {
                                       
                                        break;
                                    } else {
                                        file = new File(destinationDir, fileName);
                                        if (file.isFile()) {
                                            flag = true;
                                            break;

                                        }
                                        item.write(file);
                                        //FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                                        //fileSize = fileEncryptDecryptUtil.fileEncryptUtil(file, (int)fileSize);
                                    }
                                }
                            }

                        }
                    }

                }
             catch (FileUploadException ex) {
                log("Error encountered while parsing the request", ex);
            } catch (Exception ex) {
                log("Error encountered while uploading file", ex);
            }

            if (!docSizeMsg.equals("ok")) {
                } else {
                if (dis) {
                    docSizeMsg = "File already Exists";
                    queryString = "?fq=" + docSizeMsg + "&tenderId=" + tenderId+"&sectionId="+sectionId+"&porlId="+pkgOrLotId+"&folderName="+folderName.replace("&", "^")+"&lotNo="+lotNo.replace("^", "&");
                    response.sendRedirect(pageName + queryString);
                } else {
                    if (flag) {
                        docSizeMsg = "File already Exists";
                        queryString = "?fq=" + docSizeMsg + "&tenderid=" + tenderId+"&sectionId="+sectionId+"&porlId="+pkgOrLotId+"&folderName="+folderName.replace("&", "^")+"&lotNo="+lotNo.replace("^", "&");
                        response.sendRedirect(pageName + queryString);
                    } else {

                        if (!checkret) {
                            CheckExtension ext = new CheckExtension();
                            TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("officer");
                            queryString = "?fs=" + configurationMaster.getFileSize() + "&ft=" + configurationMaster.getAllowedExtension()+"&sectionId="+sectionId+"&tenderId="+tenderId+"&porlId="+pkgOrLotId+"&folderName="+folderName.replace("&", "^")+"&lotNo="+lotNo.replace("^", "&");
                            response.sendRedirect(pageName + queryString);

                        } else {
                            if (documentBrief != null && documentBrief != "") {
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                               
                                String templateSectionDocId = "0";
                                String status = "A";
                                if("yes".equals(inCorri)){
                                    status = "createp";
                                }
                                HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                                    documentBrief = handleSpecialChar.handleSpecialChar(documentBrief);
                                String xmldata = "";
                                xmldata = "<tbl_TenderSectionDocs description=\"" + documentBrief + "\" docName=\"" + fileName + "\" docSize=\"" + fileSize + "\" tenderSectionId=\"" + sectionId + "\" templateSectionDocId=\"" + templateSectionDocId + "\" status=\"" + status + "\" tenderId=\"" + tenderId + "\" />";
                                xmldata = "<root>" + xmldata + "</root>";
                                
                                CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                CommonMsgChk commonMsgChk;
                                try {
                                    commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_TenderSectionDocs", xmldata, "").get(0);
                                   
                                    queryString = "?tenderId="+tenderId+"&sectionId="+sectionId+"&inCorri="+inCorri+"&porlId="+pkgOrLotId+"&folderName="+folderName.replace("&", "^")+"&lotNo="+lotNo.replace("^", "&")+"&up=su";
                                    response.sendRedirect(pageName+queryString);
                                     if(commonMsgChk!=null||handleSpecialChar!=null){
                                            commonMsgChk = null;
                                            handleSpecialChar = null;
                                        }
                                } catch (Exception ex) {
                                    Logger.getLogger(TenderSecUploadServlet.class.getName()).log(Level.SEVERE, null, ex);
                                }

                            }
                        }
                    
                }
                }
            }
        } finally {
            //   out.close();
        }
    }
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    public boolean checkExnAndSize(String extn, long size, String userType) {
        boolean chextn = false;
        float fsize = 0.0f;
        float dsize = 0.0f;
        int j = extn.lastIndexOf('.');
        String lst = extn.substring(j + 1);
        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        String str = configurationMaster.getAllowedExtension();
        String[] str1 = str.split(",");
        for (int i = 0; i < str1.length; i++) {
            if (str1[i].trim().equalsIgnoreCase(lst)) {
                chextn = true;
            }
        }
        if (chextn) {
            fsize = size / (1024 * 1024);
            dsize = configurationMaster.getFileSize();
            if (dsize > fsize) {
                chextn = true;
            } else {
                chextn = false;
            }
        }
        return chextn;
    }

    public String docSizeMsg(int userId) {
        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
        return userRegisterService.docSizeCheck(userId);
    }

    public boolean deleteFile(String filePath) {

        File f = new File(filePath);
        if (f.delete()) {
            return true;
        } else {
            return false;
        }

    }
}

