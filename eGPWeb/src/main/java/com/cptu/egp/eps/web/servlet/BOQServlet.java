/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.AddUpdateOpeningEvaluation;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author Rased
 */
public class BOQServlet extends HttpServlet
{
    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    private static final Logger LOGGER = Logger.getLogger(BOQServlet.class);
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();        
        try {
            String funName = request.getParameter("funName");
            String logUserId = "0";
            if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
                tenderCommonService.setLogUserId(logUserId);
            }
            LOGGER.debug("processRequest : "+logUserId+" Starts");
            if("BOQList".equalsIgnoreCase(funName))
            {
                LOGGER.debug("BOQList : "+logUserId+" Starts");
                String styleClass = "";
                String strPageNo = request.getParameter("pageNo");
                
                int pageNo = Integer.parseInt(strPageNo);
                String pageSize = request.getParameter("size");
                
                List<SPTenderCommonData> list =null;
                list = tenderCommonService.GetBOQ(funName,strPageNo , pageSize);
                if (list != null && !list.isEmpty()) {
                    
                    for (int i = 0; i < list.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }                            
                            String category = list.get(i).getFieldName3();
                            String subCategory = list.get(i).getFieldName4();
                            String code = list.get(i).getFieldName5();
                            String description = list.get(i).getFieldName6();
                            String unit = list.get(i).getFieldName7();
                            String action = "<a href=EditBOQ.jsp?id="+list.get(i).getFieldName2()+">Edit</a>"+
                            "&nbsp;|&nbsp;<a onClick=\"javascript: return ConfirmDelete("+list.get(i).getFieldName2()+");\" name=ancDelete id =ancDelete href=#>Delete</a>";
                            out.print("<tr class='" + styleClass + "'>");
                            out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * 10 + (i + 1)) + "</td>");
                            out.print("<td class=\"t-align-left\">" + category + "</td>");
                            out.print("<td class=\"t-align-left\">" + subCategory + "</td>");
                            out.print("<td class=\"t-align-left\">" + code + "</td>");
                            out.print("<td class=\"t-align-left\">" + description + "</td>");
                            out.print("<td class=\"t-align-left\">" + unit + "</td>");
                            out.print("<td class=\"t-align-center\">" + action + "</td>");
                            out.print("</tr>");                                               
                    }
                    int totalPages = 0;
                    if (list.size() > 0) {                        
                        totalPages = (int)Math.ceil(Double.parseDouble(list.get(0).getFieldName1())/Double.parseDouble(pageSize));
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                 
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                } else {
                    out.print("<tr>");

                    out.print("<td colspan=\"6\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }
                LOGGER.debug("viewNewsEventsList : "+logUserId+" Ends");
            }
            else if("SelectBOQ".equalsIgnoreCase(funName))
            {
                LOGGER.debug("SelectBOQ : "+logUserId+" Starts");
                String styleClass = "";
                String category = request.getParameter("c");
                
                //String subCategory = request.getParameter("sc");
                
                List<SPTenderCommonData> list =null;
                list = tenderCommonService.GetBOQ("SelectBOQ", category , null);
                if (list != null && !list.isEmpty()) {
                    
                    for (int i = 0; i < list.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }   
                            String cat = list.get(i).getFieldName2();
                            String subCategory = list.get(i).getFieldName3();
                            String code = list.get(i).getFieldName4();
                            String description = list.get(i).getFieldName5();
                            String unit = list.get(i).getFieldName6();
                            out.print("<tr class='" + styleClass + "'>");                            
                            out.print("<td class=\"t-align-center\"><input type=\"checkbox\" id=\"chk_"+list.get(i).getFieldName1()+"\"></td>");                            
                            
                            if (category.length() == 0) 
                            {
                                out.print("<td class=\"t-align-left\">" + cat + "</td>");
                            }                           
                            
                            out.print("<td class=\"t-align-left\">" + subCategory + "</td>");
                            out.print("<td class=\"t-align-left\">" + code + "</td>");
                            out.print("<td class=\"t-align-left\">" + description + "</td>");
                            out.print("<td class=\"t-align-left\">" + unit + "</td>");
                            out.print("</tr>");                                               
                    }
                } else {
                    out.print("<tr>");

                    out.print("<td colspan=\"6\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }
                LOGGER.debug("SelectBOQ : "+logUserId+" Ends");
            }
            else if("SelectBOQCategory".equalsIgnoreCase(funName))
            {
                LOGGER.debug("SelectBOQCategory : "+logUserId+" Starts");                
                List<SPTenderCommonData> list =null;
                list = tenderCommonService.GetBOQ("SelectBOQCategory", null , null);
                for (int i = 0; i < list.size(); i++) 
                {                           
                    String cat = list.get(i).getFieldName1();
                    out.print("<option value=\""+cat+"\">"+cat+"</option>");
                }
                LOGGER.debug("SelectBOQCategory : "+logUserId+" Ends");
            }
            else if("delete".equalsIgnoreCase(funName))
            {
                String id = request.getParameter("id");
                AddUpdateOpeningEvaluation addUpdate = (AddUpdateOpeningEvaluation) AppContext.getSpringBean("AddUpdateOpeningEvaluation");                              
                CommonMsgChk commonMsgChk = addUpdate.addUpdOpeningEvaluation("DeleteBOQ",id,HandleSpecialChar.handleSpecialChar(request.getParameter("txtComments"))).get(0); 
                out.print(commonMsgChk.getFlag());
            }
           LOGGER.debug("processRequest : "+logUserId+" Starts");

        } finally { 
            out.close();
        }
    }
    
    @Override
protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    processRequest(request, response);
}
}
