/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblListBoxDetail;
import com.cptu.egp.eps.model.table.TblListBoxMaster;
import com.cptu.egp.eps.service.serviceinterface.CreateComboService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author parag
 */
public class CreateComboSrBean extends HttpServlet {

    /**this servlet handles all the request of createcombo.jsp and viewcombo.jsp pages
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private CreateComboService createcomboservice = (CreateComboService) AppContext.getSpringBean("CreateComboService");
    private static final Logger LOGGER = Logger.getLogger(CreateComboSrBean.class);
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String logUserId = "0";
        boolean flag = true;
        try { 
            if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
                createcomboservice.setLogUserId(logUserId);
            }
            LOGGER.debug("processRequest : "+logUserId+" Starts");
                TblListBoxMaster tbllistboxmaster = new TblListBoxMaster();
                /* getting combo data from jsp page by request parameter */
            
            if ("addcomboData".equalsIgnoreCase(request.getParameter("action"))) {
                LOGGER.debug("CreateCombosrbean : action : combo value adding to db" + logUserId + " Starts"); 
                        String templetformid = "";
                if (request.getParameter("formId")!=null && !"".equals(request.getParameter("formId"))) {
                templetformid = request.getParameter("formId");}
                        String templetid = "";
                if (request.getParameter("templateId")!=null && !"".equals(request.getParameter("templateId"))) {
                templetid = request.getParameter("templateId");}
                        String Message = "";
                if (!"".equals(request.getParameter("message"))) {
                            Message = request.getParameter("message");
                        }
                        int templetformId = Integer.parseInt(templetformid);
                        String comboname = request.getParameter("ListName");
                String lblname[] = request.getParameterValues("lblField");
                        String lblvalue[] = request.getParameterValues("valueField");
                        String selected = request.getParameter("chkField");
                        String isCalreq = request.getParameter("rad");
                        /* set combo data in to pojos */
                LOGGER.debug("CreateCombosrbean : " + logUserId + " set combo data in to pojos");
                if ("std".equalsIgnoreCase(Message)) {
                                tbllistboxmaster.setListBoxName(comboname);
                                tbllistboxmaster.setTemplateFormId(templetformId);
                                tbllistboxmaster.setIsCalcReq(isCalreq);
                    List<TblListBoxDetail> listboxdetail = new ArrayList<TblListBoxDetail>();                 
                    for (int i = 0; i < (lblname.length); i++) {
                                    TblListBoxDetail tbllistboxdetail = new TblListBoxDetail();
                                    tbllistboxdetail.setItemId((short) (i + 1));
                                    if("No".equalsIgnoreCase(isCalreq))
                                    {
                                        tbllistboxdetail.setItemValue(Integer.toString(i+1));
                                    }else{
                                        tbllistboxdetail.setItemValue(lblvalue[i]);
                                    }
                                    tbllistboxdetail.setItemText(lblname[i]);
                        if (Integer.parseInt(selected) == i) {
                                        tbllistboxdetail.setIsDefault("yes");
                        } else {
                                        tbllistboxdetail.setIsDefault("no");
                                    }
                                    listboxdetail.add(tbllistboxdetail);
                        LOGGER.debug("name is::" + lblname[i] + "and value is::" + lblvalue[i]);
                                }
                                /* adding combo data into database by calling service class method */
                    flag = createcomboservice.addComboNameDetails(tbllistboxmaster, listboxdetail);
                    LOGGER.debug("CreateCombosrbean : action : combo value adding to db" + logUserId + " Ends");
                    if(!flag){
                        response.sendRedirect("admin/CreateCombo.jsp?formId="+templetformid+"&templateId="+templetid+"&msg=fail");
                    }else{
                        response.sendRedirect("admin/ViewCombo.jsp?templetformid=" + templetformid + "&templateId=" + templetid+"&msg=succ");
                         }
                }
            }            
            else if("fetchData".equalsIgnoreCase(request.getParameter("action")))
            {
                LOGGER.debug("CreateCombosrbean : action : " + logUserId + " getting combo data from database by calling service class method and displayed into jqgrid : Starts");
          /* getting combo data from database by calling service class method and displayed into jqgrid*/
                    response.setContentType("text/xml;charset=UTF-8");

                String strtempletID = request.getParameter("templetID");

                List<Object> stdStatuslist = createcomboservice.getSTDStatus(Integer.parseInt(strtempletID));
                
                String templetformidstr = request.getParameter("templetformID");
                int TempletFormID = Integer.parseInt(templetformidstr);

                    String ViewMessage = request.getParameter("viewmessage");

                    String rows = request.getParameter("rows");
                    String page = request.getParameter("page");

                    String sord = null;
                    String sidx = null;

                if (request.getParameter("sidx") != null && !"".equalsIgnoreCase(request.getParameter("sidx"))) {
                        sord = request.getParameter("sord");
                        sidx = request.getParameter("sidx");
                    }

                    boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                String searchField = "";
                String searchString = "";
                String searchOper = "";

                    int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));

                    int totalPages = 0;
                    int totalCount = 0;

                    List<TblListBoxMaster> getcomboname = null;

                if (_search) {
                        searchField = request.getParameter("searchField");
                        searchString = request.getParameter("searchString");
                        searchOper = request.getParameter("searchOper");
                    if ("std".equalsIgnoreCase(ViewMessage)) {
                        getcomboname = createcomboservice.searchComboName(offset, Integer.parseInt(rows), TempletFormID, searchField, searchString, searchOper, sord, sidx);
                        totalCount = (int) createcomboservice.getSearchCount(TempletFormID, searchField, searchString, searchOper);
                        }
                } else {
                    if ("std".equalsIgnoreCase(ViewMessage)) {
                        getcomboname = createcomboservice.getComboName(offset, Integer.parseInt(rows), TempletFormID, sord, sidx);
                        totalCount = (int) createcomboservice.getComboCount(TempletFormID);
                        }
                    }


                    if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(rows) == 0) {
                        totalPages = totalCount / Integer.parseInt(rows);
                    } else {
                        totalPages = (totalCount / Integer.parseInt(rows)) + 1;
                    }
                    } else {
                        totalPages = 0;
                    }

                    out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                    out.print("<rows>");
                    out.print("<page>" + request.getParameter("page") + "</page>");
                    out.print("<total>" + totalPages + "</total>");
                    out.print("<records>" + totalCount + "</records>");
                int srNo = offset + 1;
                int t = 0;
                String strDelete="";
                if ("std".equalsIgnoreCase(ViewMessage)) {
                    for (int i = 0; i < getcomboname.size(); i++) {
                            int listboxid = getcomboname.get(i).getListBoxId();
                            ComboSrBean cmbosrbean = new ComboSrBean();
                            List<TblListBoxDetail> getdetail = cmbosrbean.getListBoxDetail(listboxid);

                            long cnt = createcomboservice.getUsedComboCountinStd(listboxid, TempletFormID);
                            if(cnt>0){
                                strDelete = "<a href='#' onclick=\"deleteUsedCombo()\" >Delete</a>";
                            }else{
                                strDelete="<a href='#' onclick=\"deleteCombo(" + getcomboname.get(i).getListBoxId() + ")\" >Delete</a>";
                            }

                        StringBuilder listname = new StringBuilder();
                        String isdefault = "";
                        String str = "";
                        
                            //String str1="";
                            //str1 = "<a href=CreateCombo.jsp?msg=edit&listboxid="+getcomboname.get(i).getListBoxId()+"><u>Edit</u></a>";
                        listname.append("<center><select style='width:100px;' class='formTxtBox_1' >");
                        for (int j = 0; j < getdetail.size(); j++) {
                            str = getdetail.get(j).getItemText();
                            isdefault = getdetail.get(j).getIsDefault();
                            listname.append("<option");
                            if ("yes".equals(isdefault)) {
                                listname.append(" " + "selected");
                            }
                            listname.append(">" + str + "</option>");
                        }
                        listname.append("</select></center>");
                            out.print("<row id='" + i + "'>");
                            out.print("<cell>" + srNo + "</cell>");
                        out.print("<cell><![CDATA[" + getcomboname.get(i).getListBoxName() + "]]></cell>");
                        out.print("<cell><![CDATA[" + listname.toString() + "]]></cell>");
                        if(stdStatuslist.get(0)!=null)
                        {   
                            if("P".equalsIgnoreCase(stdStatuslist.get(0).toString()))
                            {
                                out.print("<cell><![CDATA[" + strDelete + "]]></cell>");
                            }
                            else
                            {
                                out.print("<cell><![CDATA["+" - "+"]]></cell>");
                            }
                        }
                            
                            out.print("</row>");
                        srNo++;t++;
                        }
                    //No Record Found Code
                if(getcomboname.isEmpty()){
                    out.print("<row id='" + t + "'>");
                    out.print("<cell><![CDATA[No data found]]></cell>");
                    out.print("<cell><![CDATA[No data found]]></cell>");
                    out.print("<cell><![CDATA[No data found]]></cell>");                    
                    out.print("<cell><![CDATA[No data found]]></cell>");                    
                    out.print("</row>");
                    }
                }
                    out.print("</rows>");

                }
                else if("delete".equalsIgnoreCase(request.getParameter("action")))
                {

                      String strlistboxid = request.getParameter("listBoxId");
                      String str = deleteCombo(strlistboxid);
                      out.print(str);                      
                }
            LOGGER.debug("CreateCombosrbean : action : " + logUserId + " getting combo data from database by calling service class method and displayed into jqgrid : Ends");
        } catch (Exception e) {
            LOGGER.error("CreateCombosrbean : " + logUserId + " " + e);
        }
        LOGGER.debug("processRequest : "+logUserId+" Ends");
        }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String deleteCombo(String strlistboxid) {
        LOGGER.debug("deleteCombo : Starts");
        String str="0";
        try {
            boolean flag = createcomboservice.deleteCombo(Integer.parseInt(strlistboxid));
            if(flag)
            {
                str="1";
            }
        } catch (Exception e) {
            LOGGER.debug("deleteCombo :"+e);
        }
        LOGGER.debug("deleteCombo : Ends");
    return str;
    }
}
