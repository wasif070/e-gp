/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

/**
 *
 * @author dipti
 */
public class HandleSpecialChar {

    public static String handleSpecialChar(String str) {
        if(str!=null){
            str = str.replaceAll("&", "&amp;");
            str = str.replaceAll(">", "&gt;");
            str = str.replaceAll("<", "&lt;");
            //str = str.replaceAll("'", "&rsquo;");
            str = str.replaceAll("'", "&#39;");
            //str = str.replaceAll("’", "&#146;");
            //str = str.replaceAll("’", "&#39;");
            str = str.replaceAll("\"", "&#34;");
            //str = str.replaceAll("-", "&#8211;");
        }
        return str;
    }

   /* remove leading whitespace */
    public static String ltrim(String source) {
        return source.replaceAll("^\\s+","");
    }

    /* remove trailing whitespace */
    public static String rtrim(String source) {
        return source.replaceAll("\\s+$","");
    }

     /*remove leading & trailing whitespaces */
    public static String lrtrim(String source){
        return ltrim(rtrim(source));
    }

    /* replace multiple whitespaces between words with string specified by replaceWith */
    public static String itrim(String source,String replaceWith) {
        return source.replaceAll("\\b\\s{1,}\\b",replaceWith);
    }

    /* replace leading and trailing whitespace and replace or remove one or more whitespaces
       betwwen words with string specified by replaceWith*/
    public static String trim(String source,String replaceWith) {
        return itrim(ltrim(rtrim(source)),replaceWith);
    }
}
