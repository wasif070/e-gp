/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 *
 * @author taher
 */
public class MailAfter7DaysIfUnPaid extends QuartzJobBean {
    public static int i = 0;
    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        if(i == 0){
            i++;
            CommonSearchService cmnSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            List<SPCommonSearchData> recordCron = null;
            recordCron = cmnSearchService.searchData("getCronJob", "New User Registration 7 days unpaid", null, null, null, null, null, null, null, null);
            if (recordCron.isEmpty()) {
                int cronStatus = 1;
                String cronMsg = "Successful Executed";
                String cronName = "New User Registration 7 days unpaid";
                String  cronStartTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
                CommonXMLSPService  xmlservice = (CommonXMLSPService)AppContext.getSpringBean("CommonXMLSPService");
                try {
                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> list = commonSearchDataMoreService.geteGPDataMore("eMailAfter7DaysIfUnPaid");
                    if(!list.isEmpty()){
                        SendMessageUtil sendMessageUtil = new SendMessageUtil();
                        MailContentUtility mailContentUtility = new MailContentUtility();
                        String mailText = mailContentUtility.MailAfter7DaysIfUnPaid();
                        sendMessageUtil.setEmailFrom(XMLReader.getMessage("regEmailId"));
                        sendMessageUtil.setEmailSub("e-GP System:  User Registration - Removal of your Profile");
                        sendMessageUtil.setEmailMessage(mailText);
                        StringBuilder userIds = new StringBuilder();
                        for (SPCommonSearchDataMore mails : list) {
                            userIds.append(mails.getFieldName2()+",");
                            sendMessageUtil.setEmailTo(new String[]{mails.getFieldName1()});
                            sendMessageUtil.sendEmail();
                        }
                        mailContentUtility = null;
                        sendMessageUtil = null;
                        System.out.println("****************mail ids for delete ******************** "+userIds);
                        List<SPCommonSearchDataMore> jobList = commonSearchDataMoreService.geteGPDataMore("eMailAfter7DaysIfUnPaidDelete",userIds.substring(0, userIds.length()-1));
                        if(jobList!=null && !jobList.isEmpty() && !jobList.get(0).getFieldName1().equals("Records deleted")){
                            cronMsg = jobList.get(0).getFieldName2();
                            cronStatus = 0;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    AppExceptionHandler expHandler = new AppExceptionHandler();
                    expHandler.stack2string(e);
                    cronMsg = e.toString();
                    cronStatus = 0;
                }
                String  cronEndTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
                String xml = "<root>"
                        + "<tbl_CronJobs "
                        + "cronJobName=\"" + cronName + "\" "
                        + "cronJobStartTime=\"" + cronStartTime + "\" "
                        + "cronJobEndTime=\"" + cronEndTime + "\" "
                        + "cronJobStatus=\"" + cronStatus + "\" "
                        + "cronJobMsg=\"" + cronMsg + "\"/>"
                        + "</root>";
                try {
                    xmlservice.insertDataBySP("insert", "tbl_CronJobs", xml, "");
                } catch (Exception ex) {
                    ex.printStackTrace();
                    AppExceptionHandler expHandler = new AppExceptionHandler();
                    expHandler.stack2string(ex);
                }
            }
        }
    }
}
