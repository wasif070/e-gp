/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.webservices;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import com.cptu.egp.eps.model.table.TblProcurementMethod;
import com.cptu.egp.eps.model.table.TblProcurementNature;
import com.cptu.egp.eps.model.table.TblProcurementTypes;
import com.cptu.egp.eps.service.serviceimpl.DepartmentCreationServiceImpl;
import com.cptu.egp.eps.service.serviceimpl.TenderServiceImpl;
import com.cptu.egp.eps.service.serviceinterface.ConfigPreTenderRuleService;
import com.cptu.egp.eps.service.serviceinterface.PEOfficeCreationService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.xmlpojos.AllTendersXmlElements;
import com.cptu.egp.eps.web.xmlpojos.Departments;
import com.cptu.egp.eps.web.xmlpojos.OfficeMaster;
import com.cptu.egp.eps.web.xmlpojos.ProcurementMothod;
import com.cptu.egp.eps.web.xmlpojos.ProcurementNature;
import com.cptu.egp.eps.web.xmlpojos.ProcurementTypes;
import com.cptu.egp.eps.web.xmlpojos.WSResponseObject;
import com.cptu.egp.eps.web.utility.WebServiceUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import org.apache.log4j.Logger;

/**
 * These web-service is for Advance Search of tenders.
 * @author Chalapathi.Bavisetti
 */
@WebService()
@XmlSeeAlso({ProcurementNature.class, ProcurementMothod.class, ProcurementTypes.class,
    Departments.class, OfficeMaster.class, AllTendersXmlElements.class})
public class AdvanceSearchTenders {

    private static final Logger LOGGER = Logger.getLogger(AdvanceSearchTenders.class);
    private String logUserId = "0";
    private static final String EMPTY_STRING = "";
    private static final int ZERO_VALUE = 0;
    private static final int WS_ID_PROCUREMENT_NATURES = 14;
    private static final String WS_PROCUREMENT_NATURES_SEARCH_SERVICE_NAME = "configPreTenderRuleService";
    private static final int WS_ID_PROCUREMENT_METHODS = 15;
    private static final String WS_PROCUREMENT_METHODS_SEARCH_SERVICE_NAME = "configPreTenderRuleService";
    private static final int WS_ID_PROCUREMENT_TYPES = 16;
    private static final String WS_PROCUREMENT_TYPES_SEARCH_SERVICE_NAME = "configPreTenderRuleService";
    private static final int WS_ID_DEPARTMENTS = 17;
    private static final String WS_DEPARTMENTS_SEARCH_SERVICE_NAME = "DepartmentCreationService";
    private static final int WS_ID_OFFICES_BY_DEPARTMENT_ID = 18;
    private static final String WS_OFFICES_BY_DEPARTMENT_ID_SEARCH_SERVICE_NAME = "PEOfficeCreationService";
    private static final int WS_ID_TENDERS_ADVANCE_SEARCH = 19;
    private static final String WS_TENDERS_ADVANCE_SEARCH_SERVICE_NAME = "TenderService";

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

     /***
     * This method gives the List of String Objects of <b>ProcurementNatures</b> and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br>
     * 1.ProcurementNatureId <br>
     * 2.ProcurementNature <br>
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getProcurementNatures")
    public WSResponseObject getProcurementNatures(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getProcurementNatures : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_PROCUREMENT_NATURES, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_PROCUREMENT_NATURES,
                WS_PROCUREMENT_NATURES_SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> procNaturesList = new ArrayList<String>();
            try {
                List<ProcurementNature> procNaturesTempList = new ArrayList<ProcurementNature>();
                procNaturesTempList = getRequiredProcurementNaturesList();
                if (procNaturesTempList != null) {
                    for (ProcurementNature procurementNature : procNaturesTempList) {
                        StringBuilder sbCSV = new StringBuilder();
                        sbCSV.append(procurementNature.getProcurementNatureId());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(procurementNature.getProcurementNature());
                        //sbCSV.append(WSResponseObject.APPEND_CHAR);  Commented by sudhir 09072011
                        procNaturesList.add(sbCSV.toString());
                    }
                }
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.debug("getProcurementNatures : " + logUserId + e);
            }
            if (procNaturesList != null) {
                if (procNaturesList.isEmpty()) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(procNaturesList);
                }
            }
        }
        LOGGER.debug("getProcurementNatures : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    /***
     * This method return the procurement natures as a list of objects
     * @return List<ProcurementNature>
     */
    private List<ProcurementNature> getRequiredProcurementNaturesList() {
        LOGGER.debug("getRequiredProcurementNaturesList : " + logUserId + WSResponseObject.LOGGER_START);
        List<ProcurementNature> procNaturesList = new ArrayList<ProcurementNature>();
        ConfigPreTenderRuleService preTenderRulesevice = (ConfigPreTenderRuleService) AppContext.getSpringBean(WS_PROCUREMENT_NATURES_SEARCH_SERVICE_NAME);
        List<TblProcurementNature> tblProcurementNatures =
                preTenderRulesevice.getProcurementNature();
        for (TblProcurementNature procNature : tblProcurementNatures) {
            ProcurementNature procurementNature = new ProcurementNature();
            procurementNature.setProcurementNature(procNature.getProcurementNature());
            procurementNature.setProcurementNatureId(procNature.getProcurementNatureId());
            procNaturesList.add(procurementNature);
        }
        LOGGER.debug("getRequiredProcurementNaturesList : " + logUserId + WSResponseObject.LOGGER_END);
        return procNaturesList;
    }

   /***
     * This method gives the List of String Objects of <b>Procurement Methods</b> and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br>
     * 1.Procurement Method Id <br>
     * 2.Procurement Method Name <br>
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getProcurementMethods")
    public WSResponseObject getProcurementMethods(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getProcurementMethods : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_PROCUREMENT_METHODS, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_PROCUREMENT_METHODS,
                WS_PROCUREMENT_METHODS_SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> procMethodsList = new ArrayList<String>();
            try {                
                 List<ProcurementMothod> procMethodsTempList = new ArrayList<ProcurementMothod>();
                procMethodsTempList = getRequiredProcurementMethodsList();
                if (procMethodsTempList != null) {
                    for (ProcurementMothod procurementMothod : procMethodsTempList) {
                        StringBuilder sbCSV = new StringBuilder();
                        sbCSV.append(procurementMothod.getProucrementMethodId());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(procurementMothod.getProucrementMethod());
                        //sbCSV.append(WSResponseObject.APPEND_CHAR);   Commented by sudhir 09072011
                        procMethodsList.add(sbCSV.toString());
                    }
                }
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.debug("getProcurementMethods : " + logUserId + e);
            }
            if (procMethodsList != null) {
                if (procMethodsList.isEmpty()) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(procMethodsList);
                }
            }
        }
        LOGGER.debug("getProcurementMethods : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    /***
     * This method return the procurement methods as a list of objects
     * @return List<ProcurementMethod>
     */
    private List<ProcurementMothod> getRequiredProcurementMethodsList() {
        LOGGER.debug("getRequiredProcurementMethodsList : " + logUserId + WSResponseObject.LOGGER_START);
        List<ProcurementMothod> procMethodsList = new ArrayList<ProcurementMothod>();
        ConfigPreTenderRuleService preTenderRulesevice = (ConfigPreTenderRuleService) AppContext.getSpringBean(WS_PROCUREMENT_METHODS_SEARCH_SERVICE_NAME);
        List<TblProcurementMethod> tblProcurementMethods =
                preTenderRulesevice.getProcurementMethod();
        for (TblProcurementMethod procMethod : tblProcurementMethods) {
            ProcurementMothod procurementMethod = new ProcurementMothod();
            procurementMethod.setProucrementMethod(procMethod.getProcurementMethod());
            procurementMethod.setProucrementMethodId(procMethod.getProcurementMethodId());
            procMethodsList.add(procurementMethod);
        }
        LOGGER.debug("getRequiredProcurementMethodsList : " + logUserId + WSResponseObject.LOGGER_END);
        return procMethodsList;
    }

    /***
     * This method gives the List of String Objects of <b>Procurement Types</b> and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br>
     * 1.ProcurementTypeId <br>
     * 2.ProcurementType <br>
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getProcurementTypes")
    public WSResponseObject getProcurementTypes(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getProcurementTypes : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_PROCUREMENT_TYPES, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_PROCUREMENT_TYPES,
                WS_PROCUREMENT_TYPES_SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> procTypesList = new ArrayList<String>();
            try {
                List<ProcurementTypes> procTypesTempList = new ArrayList<ProcurementTypes>();
                procTypesTempList = getRequiredProcurementTypesList();
                if(procTypesTempList != null){
                     for (ProcurementTypes procurementType : procTypesTempList) {
                        StringBuilder sbCSV = new StringBuilder();
                        sbCSV.append(procurementType.getProcurementTypeId());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(procurementType.getProcurementType());
                        //sbCSV.append(WSResponseObject.APPEND_CHAR);  Commented by sudhir 09072011
                        procTypesList.add(sbCSV.toString());
                    }
                }
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.debug("getProcurementTypes : " + logUserId + e);
            }
            if (procTypesList != null) {
                if (procTypesList.isEmpty()) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(procTypesList);
                }
            }
        }
        LOGGER.debug("getProcurementTypes : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    /***
     * This method return the procurement types as a list of objects
     * @return List<ProcurementTypes>
     */
    private List<ProcurementTypes> getRequiredProcurementTypesList() {
        LOGGER.debug("getRequiredProcurementTypesList : " + logUserId + WSResponseObject.LOGGER_START);
        List<ProcurementTypes> procTypesList = new ArrayList<ProcurementTypes>();
        ConfigPreTenderRuleService preTenderRulesevice = (ConfigPreTenderRuleService) AppContext.getSpringBean(WS_PROCUREMENT_TYPES_SEARCH_SERVICE_NAME);
        List<TblProcurementTypes> tblProcurementTypes =
                preTenderRulesevice.getProcurementTypes();
        for (TblProcurementTypes procType : tblProcurementTypes) {
            ProcurementTypes procurementType = new ProcurementTypes();
            procurementType.setProcurementType(procType.getProcurementType());
            procurementType.setProcurementTypeId(procType.getProcurementTypeId());
            procTypesList.add(procurementType);
        }
        LOGGER.debug("getRequiredProcurementTypesList : " + logUserId + WSResponseObject.LOGGER_END);
        return procTypesList;
    }

    /***
     * This method gives the List of String Objects of <b>Department</b> and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br>
     * 1.Department Id <br>
     * 2.Department Name <br>
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getDepartments")
    public WSResponseObject getDepartments(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getDepartments : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_DEPARTMENTS, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_DEPARTMENTS,
                WS_DEPARTMENTS_SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> departmentsList = new ArrayList<String>();
            try {
                 List<Departments> departmentsTempList = new ArrayList<Departments>();
                 departmentsTempList = getRequiredDepartmentsList();
                 if(departmentsTempList != null){
                       for (Departments department : departmentsTempList) {
                         StringBuilder sbCSV = new StringBuilder();
                         sbCSV.append(department.getDepartmentId());
                         sbCSV.append(WSResponseObject.APPEND_CHAR);
                         sbCSV.append(department.getDepartmentName());
                         sbCSV.append(WSResponseObject.APPEND_CHAR);
                         sbCSV.append(department.getDepartmentType());   // Added by sudhir 09072011 To identify for which types of department.
                         departmentsList.add(sbCSV.toString());
                     }
                 }
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.debug("getDepartments : " + logUserId + e);
            }
            if (departmentsList != null) {
                if (departmentsList.isEmpty()) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(departmentsList);
                }
            }
        }
        LOGGER.debug("getDepartments : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    /***
     * This method return the departments as a list of objects
     * @return List<Departments>
     */
    private List<Departments> getRequiredDepartmentsList() {
        LOGGER.debug("getRequiredDepartmentsList : " + logUserId + WSResponseObject.LOGGER_START);
        List<Departments> departmentsList = new ArrayList<Departments>();
        List<TblDepartmentMaster> deptLst;
        DepartmentCreationServiceImpl deptTree = (DepartmentCreationServiceImpl) AppContext.getSpringBean(WS_DEPARTMENTS_SEARCH_SERVICE_NAME);
        deptLst = deptTree.getAllDepartmentMaster();
        Iterator it = deptLst.iterator();
        while (it.hasNext()) {
            TblDepartmentMaster tblDepartmentMaster = (TblDepartmentMaster) it.next();
            if (tblDepartmentMaster.getDepartmentType().equals("Organization")
                    || tblDepartmentMaster.getDepartmentType().equals("Division")
                    || tblDepartmentMaster.getDepartmentType().equals("Ministry")) {
                Departments department = new Departments();
                department.setDepartmentId(tblDepartmentMaster.getDepartmentId());
                department.setDepartmentName(tblDepartmentMaster.getDepartmentName());
                department.setDepartmentType(tblDepartmentMaster.getDepartmentType());    // Added by sudhir 09072011 To identify for which types of department.
                departmentsList.add(department);
            }
        }
        LOGGER.debug("getRequiredDepartmentsList : " + logUserId + WSResponseObject.LOGGER_END);
        return departmentsList;
    }

    /***
     * This method gives the List of String Objects of <b>Office</b> and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br>
     * 1.Office Id <br>
     * 2.Office Name <br>
     * @param String username
     * @param String password
     * @param Integer departmentId
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getOfficesByDepartmentId")
    public WSResponseObject getOfficesByDepartmentId(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password,
            @WebParam(name = "departmentId") Integer departmentId) {
        LOGGER.debug("getOfficesByDepartmentId : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        if (departmentId == null) {
            departmentId = ZERO_VALUE;
        }
        String searchKey = "DepartmentId:" + departmentId;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_OFFICES_BY_DEPARTMENT_ID, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_OFFICES_BY_DEPARTMENT_ID,
                WS_OFFICES_BY_DEPARTMENT_ID_SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> officesList = new ArrayList<String>();
            try {
                List<OfficeMaster> officesTempList = new ArrayList<OfficeMaster>();
                officesTempList = getRequiredOfficesListByDepartmentId(departmentId);
                if (officesTempList != null) {
                    for (OfficeMaster officeMaster : officesTempList) {
                        StringBuilder sbCSV = new StringBuilder();
                        sbCSV.append(officeMaster.getOfficeId());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(officeMaster.getOfficeName());
                        //sbCSV.append(WSResponseObject.APPEND_CHAR);  Commented by sudhir 09072011
                        officesList.add(sbCSV.toString());
                    }
                }
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.debug("getOfficesByDepartmentId : " + logUserId + e);
            }
            if (officesList != null) {
                if (officesList.isEmpty()) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(officesList);
                }
            }
        }
        LOGGER.debug("getOfficesByDepartmentId : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    /***
     * This method return the offices as a list of objects with the given department Id
     * @param departmentId
     * @return List<OfficeMaster>
     */
    private List<OfficeMaster> getRequiredOfficesListByDepartmentId(int departmentId) {
        LOGGER.debug("getRequiredOfficesListByDepartmentId : " + logUserId + WSResponseObject.LOGGER_END);
        List<OfficeMaster> officesList = new ArrayList<OfficeMaster>();

        PEOfficeCreationService officeCreationService = (PEOfficeCreationService) AppContext.getSpringBean(WS_OFFICES_BY_DEPARTMENT_ID_SEARCH_SERVICE_NAME);
        List<TblOfficeMaster> officeMasters =
                officeCreationService.findOfficeMaster("departmentId", Operation_enum.EQ, departmentId);
        Iterator it = officeMasters.iterator();
        while (it.hasNext()) {
            TblOfficeMaster tblOfficeMaster = (TblOfficeMaster) it.next();
            OfficeMaster office = new OfficeMaster();
            office.setOfficeId(tblOfficeMaster.getOfficeId());
            office.setOfficeName(tblOfficeMaster.getOfficeName());
            officesList.add(office);
        }
        LOGGER.debug("getRequiredOfficesListByDepartmentId : " + logUserId + WSResponseObject.LOGGER_END);
        return officesList;
    }

    /***
     * This method gives the List of String Objects of <b>Tender</b> and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br> 
     * 1.Tender ID<br>
     * 2.Reference No.<br>
     * 3.Procurement Nature<br>
     * 4.Title (Brief Description)<br>
     * 5.Ministry/Division/Organization selection<br>
     * 6.Procuring Entity<br>
     * 7.Procurement Type<br>
     * 8.Procurement Method<br>
     * 9.Publishing Date and Time<br>
     * 10.Closing Date and Time<br>
     *
     * @param String username
     * @param String password
     * @param String keyWord
     * @param String viewType as Live or Archive or AllTenders
     * @param String ministry_Division_OrganizationName
     * @param String procuringEntityOfficeName
     * @param Integer procurementNatureId
     * @param String procurementType
     * @param Integer procurementMethod
     * @param Integer tenderID
     * @param String referenceNo
     * @param String publishDateFrom as dd/mm/yyyy
     * @param String publishDateTo as dd/mm/yyyy
     * @param String closingDateFrom as dd/mm/yyyy
     * @param String closingDateTo as dd/mm/yyyy
     * @param String category
     * @param Integer pageNumber
     * @param Integer recordsPerPage
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "tendersAdvanceSearch")
    public WSResponseObject tendersAdvanceSearch(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password,
            @WebParam(name = "keyWord") String keyWord,
            @WebParam(name = "viewType") String viewType,
            @WebParam(name = "ministry_Division_OrganizationName") String ministry_Division_OrganizationName,
            @WebParam(name = "procuringEntityOfficeName") String procuringEntityOfficeName,
            @WebParam(name = "procurementNatureId") Integer procurementNatureId,
            @WebParam(name = "procurementType") String procurementType,
            @WebParam(name = "ProcurementMethod") Integer procurementMethod,
            @WebParam(name = "tenderID") Integer tenderID,
            @WebParam(name = "referenceNo") String referenceNo,
            @WebParam(name = "publishDateFrom") String publishDateFrom,
            @WebParam(name = "publishDateTo") String publishDateTo,
            @WebParam(name = "closingDateFrom") String closingDateFrom,
            @WebParam(name = "closingDateTo") String closingDateTo,
            @WebParam(name = "category") String category  /*,
            @WebParam(name = "pageNumber") Integer pageNumber,       Commended by sudhir 09072011 for no need to pagination concept for WS.
            @WebParam(name = "pageSize") Integer recordsPerPage  */) {
        LOGGER.debug("tendersAdvanceSearch : " + logUserId + WSResponseObject.LOGGER_START);
        //generate search key for log table
        Integer pageNumber = 1;
        Integer recordsPerPage = 199999;
        String searchKey = getWholeStringSearchKey(keyWord,
                viewType, ministry_Division_OrganizationName, procuringEntityOfficeName,
                procurementNatureId, procurementType, procurementMethod,
                tenderID, referenceNo, publishDateFrom, publishDateTo,
                closingDateFrom, closingDateTo, category, pageNumber, recordsPerPage);
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_TENDERS_ADVANCE_SEARCH, username, searchKey);
        //getting data
        WSResponseObject wSResponseObject = new WSResponseObject();
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_TENDERS_ADVANCE_SEARCH,
                WS_TENDERS_ADVANCE_SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> requiredTendersList = new ArrayList<String>();
            try {
                List<AllTendersXmlElements> requiredTendersTempList = new ArrayList<AllTendersXmlElements>();
                requiredTendersTempList = getAdvanceSearchTendersList(keyWord,
                        viewType, ministry_Division_OrganizationName, procuringEntityOfficeName,
                        procurementNatureId, procurementType, procurementMethod,
                        tenderID, referenceNo, publishDateFrom, publishDateTo,
                        closingDateFrom, closingDateTo, category, pageNumber, recordsPerPage);
                if (requiredTendersTempList != null) {
                    for (AllTendersXmlElements tendersXmlElements : requiredTendersTempList) {
                        StringBuilder sbCSV = new StringBuilder();
                        sbCSV.append(tendersXmlElements.getTenderId());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                      //  sbCSV.append(tendersXmlElements.getRefNo());
                        sbCSV.append("<a href=\""+ XMLReader.getMessage("urltoredirect") +"resources/common/ViewTender.jsp?id="+tendersXmlElements.getTenderId()+"&h=t\" >" + tendersXmlElements.getRefNo()+ "</a>");
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(tendersXmlElements.getProcurementNature());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(tendersXmlElements.getTenderDescription());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(tendersXmlElements.getMinistry());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(tendersXmlElements.getDivision());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(tendersXmlElements.getAgency());   //added by sudhir 19072011 for Organisation
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(tendersXmlElements.getPeOfficeName());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(tendersXmlElements.getProcurementType());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(tendersXmlElements.getProcurementMethod());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(tendersXmlElements.getTenderPubDt());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(tendersXmlElements.getSubmissionDt());
                        sbCSV.append(WSResponseObject.APPEND_CHAR);
                        sbCSV.append(tendersXmlElements.getOfficeId());
                        //sbCSV.append(WSResponseObject.APPEND_CHAR);    Commented by sudhir 09072011
                        requiredTendersList.add(sbCSV.toString());
                    }
                }
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.debug("tendersAdvanceSearch : " + logUserId + e);
            }
            if (requiredTendersList != null) {
                if (requiredTendersList.isEmpty()) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(requiredTendersList);
                }
            }
        }
        //p_get_tenderinformation "get alltenders"
        LOGGER.debug("tendersAdvanceSearch : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    /**
     * This method returns search criteria as a string for logging.
     * @param keyWord
     * @param viewType
     * @param ministry_Division_OrganizationName
     * @param procuringEntityOfficeName
     * @param procurementNatureId
     * @param procurementType
     * @param procurementMethod
     * @param tenderID
     * @param referenceNo
     * @param publishDateFrom
     * @param publishDateTo
     * @param closingDateFrom
     * @param closingDateTo
     * @param category
     * @param pageNumber
     * @param recordsPerPage
     * @return String search criteria key
     */
    private String getWholeStringSearchKey(String keyWord, String viewType,
            String ministry_Division_OrganizationName, String procuringEntityOfficeName,
            Integer procurementNatureId, String procurementType, Integer procurementMethod,
            Integer tenderID, String referenceNo, String publishDateFrom, String publishDateTo,
            String closingDateFrom, String closingDateTo, String category, Integer pageNumber,
            Integer recordsPerPage) {
        LOGGER.debug("getWholeStringSearchKey : " + logUserId + WSResponseObject.LOGGER_START);
        StringBuffer searchString = new StringBuffer();
        if (keyWord != null) {
            searchString.append("Keyword:");
            searchString.append(keyWord);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (viewType != null) {
            searchString.append("ViewType:");
            searchString.append(viewType);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (ministry_Division_OrganizationName != null) {
            searchString.append("Ministry_Division_OrganizationName:");
            searchString.append(ministry_Division_OrganizationName);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (procuringEntityOfficeName != null) {
            searchString.append("ProcuringEntityOfficeName:");
            searchString.append(procuringEntityOfficeName);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (procurementNatureId != null) {
            searchString.append("ProcurementNatureId:");
            searchString.append(procurementNatureId);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (procurementType != null) {
            searchString.append("ProcurementType:");
            searchString.append(procurementType);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (procurementMethod != null) {
            searchString.append("ProcurementMethod:");
            searchString.append(procurementMethod);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (tenderID != null) {
            searchString.append("TenderID:");
            searchString.append(tenderID);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (referenceNo != null) {
            searchString.append("ReferenceNo:");
            searchString.append(referenceNo);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (publishDateFrom != null) {
            searchString.append("PublishDateFrom:");
            searchString.append(publishDateFrom);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (publishDateTo != null) {
            searchString.append("PublishDateTo:");
            searchString.append(publishDateTo);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (closingDateFrom != null) {
            searchString.append("ClosingDateFrom:");
            searchString.append(closingDateFrom);
        }
        if (closingDateTo != null) {
            searchString.append("ClosingDateTo:");
            searchString.append(closingDateTo);
        }
        if (category != null) {
            searchString.append("Category:");
            searchString.append(category);
        }
        if (pageNumber != null) {
            searchString.append("PageNumber:");
            searchString.append(pageNumber);
        }
        if (recordsPerPage != null) {
            searchString.append("RecordsPerPage:");
            searchString.append(recordsPerPage);
        }
        LOGGER.debug("getWholeStringSearchKey : " + logUserId + WSResponseObject.LOGGER_END);
        return searchString.toString();
    }

    /***
     * This method return the Tenders as a list of objects
     * @param keyWord
     * @param viewType
     * @param ministry_Division_OrganizationName
     * @param procuringEntityOfficeName
     * @param procurementNatureId
     * @param procurementType
     * @param procurementMethod
     * @param tenderID
     * @param referenceNo
     * @param publishDateFrom
     * @param publishDateTo
     * @param closingDateFrom
     * @param closingDateTo
     * @param category
     * @param pageNumber
     * @param recordsPerPage
     * @return List<AllTendersXmlElements>
     */
    private List<AllTendersXmlElements> getAdvanceSearchTendersList(String keyWord, String viewType,
            String ministry_Division_OrganizationName, String procuringEntityOfficeName,
            Integer procurementNatureId, String procurementType, Integer procurementMethod,
            Integer tenderID, String referenceNo, String publishDateFrom, String publishDateTo,
            String closingDateFrom, String closingDateTo, String category, Integer pageNumber,
            Integer recordsPerPage) {
        LOGGER.debug("getAdvanceSearchTendersList : " + logUserId + WSResponseObject.LOGGER_START);
        List<AllTendersXmlElements> requiredTendersList = new ArrayList<AllTendersXmlElements>();
        int appId = ZERO_VALUE;
        int userId = ZERO_VALUE;
        String status = EMPTY_STRING;//Approved;
        String division = EMPTY_STRING;
        boolean isKeyWordEmpty = false;
        String keyWordString = WebServiceUtil.getDefaultStringValue(keyWord);
        String ministry_Division_OrganizationNameString =
                WebServiceUtil.getDefaultStringValue(ministry_Division_OrganizationName);
        String procuringEntityOfficeNameString =
                WebServiceUtil.getDefaultStringValue(procuringEntityOfficeName);
        int procurementNatureIdVal = WebServiceUtil.getDefaultIntValue(procurementNatureId);
        String procurementTypeString = WebServiceUtil.getDefaultStringValue(procurementType);
        int procurementMethodVal = WebServiceUtil.getDefaultIntValue(procurementMethod);
        int tenderIDVal = WebServiceUtil.getDefaultIntValue(tenderID);
        String referenceNoString = WebServiceUtil.getDefaultStringValue(referenceNo);
        String publishDateFromString = WebServiceUtil.getDefaultDate(publishDateFrom);
        String publishDateToString = WebServiceUtil.getDefaultDate(publishDateTo);
        String closingDateFromString = WebServiceUtil.getDefaultDate(closingDateFrom);
        String closingDateToString = WebServiceUtil.getDefaultDate(closingDateTo);
        String categoryString = WebServiceUtil.getDefaultStringValue(category);
        String agency = categoryString;// category=agency
        String viewTypeString = WebServiceUtil.getDefaultStringValue(viewType);
        if ("Live".equalsIgnoreCase(viewTypeString)) {
            viewTypeString = "Live";
        } else if ("Archive".equalsIgnoreCase(viewTypeString)) {
            viewTypeString = "Archive";
        } else if ("AllTenders".equalsIgnoreCase(viewTypeString)) {
            viewTypeString = EMPTY_STRING;
        }
        TenderServiceImpl tenderService = (TenderServiceImpl) AppContext.getSpringBean(WS_TENDERS_ADVANCE_SEARCH_SERVICE_NAME);
        int pageNumberVal = WebServiceUtil.getDefaultIntValue(pageNumber);
        int recordsPerPageVal = WebServiceUtil.getDefaultIntValue(recordsPerPage);
        List<CommonTenderDetails> tenderDetailsList =
                new ArrayList<CommonTenderDetails>();
        tenderDetailsList.clear();
        //Code add as 2 null vlue by Proshanto Kumar Saha
        tenderDetailsList = tenderService.gettenderinformationSP("get alltendersbytype",
                viewTypeString, tenderIDVal, appId, userId, procurementNatureIdVal,
                procurementTypeString, procurementMethodVal, referenceNoString,
                publishDateFromString, publishDateToString, status,
                ministry_Division_OrganizationNameString, division, agency,
                procuringEntityOfficeNameString, closingDateFromString,
                closingDateToString, pageNumberVal, recordsPerPageVal,null,null);
        isKeyWordEmpty = EMPTY_STRING.equalsIgnoreCase(keyWordString);
        requiredTendersList.clear();
        if ((tenderDetailsList != null) && (tenderDetailsList.size() > 0)) {
            for (int i = 0; i < tenderDetailsList.size(); i++) {
                CommonTenderDetails commonTenderDetails = tenderDetailsList.get(i);
                //if keyword is not mentioned then all tenders we have to return.
                //keyWord filter.
                if (isKeyWordEmpty) {
                    requiredTendersList.add(changeObject(commonTenderDetails));
                } else {
                    if (WebServiceUtil.isStringContainsKey(commonTenderDetails.getTenderBrief(), keyWordString)) {
                        requiredTendersList.add(changeObject(commonTenderDetails));
                    }
                }
            }//closing for loop
        }//closing if condition
        LOGGER.debug("getAdvanceSearchTendersList : " + logUserId + WSResponseObject.LOGGER_END);
        return requiredTendersList;
    }

    /***
     * This method changes the object from CommonTenderDetails to AllTendersXmlElements
     * @param CommonTenderDetails object
     * @return AllTendersXmlElements object
     */
    private AllTendersXmlElements changeObject(CommonTenderDetails commonTenderDetails) {
        LOGGER.debug("changeObject : " + logUserId + WSResponseObject.LOGGER_START);
        AllTendersXmlElements allTendersXmlElements = new AllTendersXmlElements();
        allTendersXmlElements.setTenderId(commonTenderDetails.getTenderId());
        allTendersXmlElements.setRefNo(commonTenderDetails.getReoiRfpRefNo());
        allTendersXmlElements.setProcurementNature(commonTenderDetails.getProcurementNature());
        allTendersXmlElements.setTenderDescription(commonTenderDetails.getTenderBrief());
        allTendersXmlElements.setMinistry(commonTenderDetails.getMinistry());
        allTendersXmlElements.setDivision(commonTenderDetails.getDivision());
        allTendersXmlElements.setAgency(commonTenderDetails.getAgency());  //added by sudhir 19072011 for Organisation
        allTendersXmlElements.setPeOfficeName(commonTenderDetails.getPeOfficeName());
        allTendersXmlElements.setProcurementType(commonTenderDetails.getProcurementType());
        allTendersXmlElements.setProcurementMethod(commonTenderDetails.getProcurementMethod());
        allTendersXmlElements.setTenderPubDt(commonTenderDetails.getTenderPubDt());
        allTendersXmlElements.setSubmissionDt(commonTenderDetails.getSubmissionDt());
        allTendersXmlElements.setOfficeId(commonTenderDetails.getOfficeId());
        LOGGER.debug("changeObject : " + logUserId + WSResponseObject.LOGGER_END);
        return allTendersXmlElements;
    }
    //This method is commented by Sreenu.This method is modified as per the requirements.
    // The modified method is tendersAdvanceSearch(...)
    /**
     * Web service operation
     */
    /*
    @WebMethod(operationName = "SearchTenders")
    public Tender SearchTenders(@WebParam(name = "ProcurementNature")
    String ProcurementNature, @WebParam(name = "ProcurementType")
    String ProcurementType, @WebParam(name = "TenderID")
    String TenderID, @WebParam(name = "ProcurementMethod")
    String ProcurementMethod, @WebParam(name = "ReferenceNo")
    String ReferenceNo, @WebParam(name = "PublishDateFrom")
    String PublishDateFrom, @WebParam(name = "PublishDateTo")
    String PublishDateTo, @WebParam(name = "ClosingDateFrom")
    String ClosingDateFrom, @WebParam(name = "ClosingDateTo")
    String ClosingDateTo, @WebParam(name = "Page")
    String page , @WebParam(name = "size")
    String size) {

    TenderService tenderService = (TenderService) AppContext.getSpringBean("TenderService");
    int pageNo = 1;
    int recordOffset = 10;
    String strUserId = "";
    List<CommonTenderDetails> tenderDetailes;
    Tender details = new Tender();
    List l = new ArrayList();
    String department = "0";
    String officeId = "";
    int tenderId = 0;
    String strTenderId = TenderID;
    if (strTenderId != null && !strTenderId.equals("")) {
    tenderId = Integer.parseInt(strTenderId);
    }
    String procNature = ProcurementNature;
    int procNatureId = 0;
    if (procNature != null && !procNature.equals("") ) {
    procNatureId = Integer.parseInt(procNature);
    }
    String procType = ProcurementType;
    String procMethod = ProcurementMethod;
    int procMethodId = 0;
    if (procMethod != null && !procMethod.equals("")) {
    procMethodId = Integer.parseInt(procMethod);
    }
    String pubDtFrm = null;
    if(PublishDateFrom != null && !"".equals(PublishDateFrom)){
    pubDtFrm = DateUtils.formatStrToStr(PublishDateFrom);

    }
    String pubDtTo = null;
    if(PublishDateTo != null && !"".equals(PublishDateTo) ){
    pubDtTo = DateUtils.formatStrToStr(PublishDateTo);

    }
    String closeDtFrm = ClosingDateFrom;
    if (closeDtFrm.equals("")) {
    closeDtFrm = null;
    }
    String closeDtTo = ClosingDateTo;
    if (closeDtTo.equals("")) {
    closeDtTo = null;
    }
    String cpvcat = "";
    String agency = "";
    //if(!"".equals(cpvcat) && cpvcat != null)
    //   agency = cpvcat;
    String refNo = "";
    if(ReferenceNo != null)
    refNo = ReferenceNo;
    tenderDetailes = tenderService.gettenderinformationSP("get alltenders", "", tenderId, 0, 0, procNatureId, procType, procMethodId, refNo, pubDtFrm, pubDtTo, "", department, "", agency, officeId, closeDtFrm, closeDtTo, pageNo, recordOffset);

    if(tenderDetailes.size()>0){
    Iterator it = tenderDetailes.iterator();
    while(it.hasNext()){
    AllTendersXmlElements allTendersXmlElements = new AllTendersXmlElements();
    CommonTenderDetails commonTenderDetails = (CommonTenderDetails)it.next();
    allTendersXmlElements.setTenderId(commonTenderDetails.getTenderId());
    allTendersXmlElements.setRefNo(commonTenderDetails.getReoiRfpRefNo());
    allTendersXmlElements.setProcurementNature(commonTenderDetails.getProcurementNature());
    allTendersXmlElements.setMinistry(commonTenderDetails.getMinistry());
    allTendersXmlElements.setDivision(commonTenderDetails.getDivision());
    allTendersXmlElements.setAgency(commonTenderDetails.getAgency());
    allTendersXmlElements.setPeOfficeName(commonTenderDetails.getPeOfficeName());
    allTendersXmlElements.setProcurementType(commonTenderDetails.getProcurementType());
    allTendersXmlElements.setProcurementMethod(commonTenderDetails.getProcurementMethod());
    allTendersXmlElements.setTenderPubDt(commonTenderDetails.getTenderPubDt());
    allTendersXmlElements.setSubmissionDt(commonTenderDetails.getSubmissionDt());
    l.add(allTendersXmlElements);
    }

    details.setTenderDetails(l);
    }
    LOGGER.debug("listsize== "+l.size());
    return details;
    }
     */
}
