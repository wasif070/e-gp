/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblContentVerification;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblPublicProcureForum;
import com.cptu.egp.eps.model.table.TblUserTypeMaster;
import com.cptu.egp.eps.service.serviceimpl.PublicForumPostService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class PublicProcForumServlet extends HttpServlet {

    static final Logger LOGGER = Logger.getLogger(PublicProcForumServlet.class);
    private String logUserId = "0";
    PublicForumPostService publicForumPostService = (PublicForumPostService) AppContext.getSpringBean("PublicForumPostService");

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
        publicForumPostService.setUserId(logUserId);
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.debug("uploadDoc : " + logUserId + " Starts");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String action = request.getParameter("action");
            HttpSession session = request.getSession();
            Object objUserId = session.getAttribute("userId");
            if (objUserId != null) {
                logUserId = objUserId.toString();
            }
            setLogUserId(logUserId);
            String username = "";
            String userTypeId = "";
            String userId = "";
            userId = logUserId;
            Object objUserTypeId = session.getAttribute("userTypeId");
            if (objUserTypeId != null) {
                userTypeId = objUserTypeId.toString();
            }
            if (session.getAttribute("userName") != null) {
                username = session.getAttribute("userName").toString();
            }
            if (username == null) {
                username = "";
            }
            username = request.getParameter("txtuname");

            if ("1".equals(userTypeId)) {
                username = "e-GP Admin";
            }
            if (action.equalsIgnoreCase("postReply")) {
                try {
                    String topicSubject = request.getParameter("topicSubject");
                    String ppfid = request.getParameter("ppfid");
                    TblPublicProcureForum tblpublicProcureForum = new TblPublicProcureForum();                    
                    tblpublicProcureForum.setPostedBy(Integer.parseInt(logUserId));
                    tblpublicProcureForum.setSubject(topicSubject);
                    //below is  Reply Description, not the Topic Description
                    tblpublicProcureForum.setDescription(request.getParameter("txtarea_replydetails").toString());
                    tblpublicProcureForum.setPostDate(new Date());
                    tblpublicProcureForum.setPpfParentId(Integer.valueOf(ppfid));
                    tblpublicProcureForum.setPostDate(new Date());
                    tblpublicProcureForum.setFullName(username);
                    tblpublicProcureForum.setStatus("Pending");
                    publicForumPostService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
                    publicForumPostService.saveForum(tblpublicProcureForum);
                    if (userTypeId.equalsIgnoreCase("8")) {//Topic replied by content admin
                        response.sendRedirect(request.getContextPath() + "/ViewAllTopic.jsp?succFlag=y&activity=postReply");
                    } else {//Topic replied by others
                        response.sendRedirect(request.getContextPath() + "/viewTopic.jsp?succMsg=y&ppfid=" + ppfid+"&ViewType="+request.getParameter("ViewType"));
                    }
                } catch (Exception ex) {
                    LOGGER.error("processRequest : " + logUserId + " : " + ex);
                    response.sendRedirect(request.getContextPath() + "/viewTopic.jsp&ViewType="+request.getParameter("ViewType"));
                }
            } else if (action.equalsIgnoreCase("postTopic")) {
                try {
                    String topicSubject = request.getParameter("txtsubject");
                    String topicDetail = request.getParameter("topicdetail");

                    String contentVerificationStatus = "";
                    String contentVerificationId = "1";

                    TblPublicProcureForum tblpublicProcureForum = new TblPublicProcureForum();                    
                    tblpublicProcureForum.setPostedBy(Integer.parseInt(logUserId));
                    tblpublicProcureForum.setSubject(topicSubject);
                    tblpublicProcureForum.setDescription(topicDetail);
                    tblpublicProcureForum.setPostDate(new Date());
                    tblpublicProcureForum.setPpfParentId(Integer.valueOf(0));
                    tblpublicProcureForum.setPostDate(new Date());
                    tblpublicProcureForum.setFullName(username);

                    List<TblContentVerification> colContentVerification = publicForumPostService.getContentVerficationStatus(contentVerificationId);
                    for (TblContentVerification contentVerification : colContentVerification) {
                        contentVerificationStatus = contentVerification.getIsPublicForum();
                    }
                    if (contentVerificationStatus.equalsIgnoreCase("yes")) {
                        tblpublicProcureForum.setStatus("Pending");
                    } else {
                        tblpublicProcureForum.setStatus("Accepted");
                    }
                    publicForumPostService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
                    publicForumPostService.saveForum(tblpublicProcureForum);
                    if (userTypeId.equalsIgnoreCase("8")) {//Topic posted by content admin
                        response.sendRedirect(request.getContextPath() + "/ViewAllTopic.jsp?succFlag=y&activity=postTopic&verificationRequired=" + contentVerificationStatus);
                    } else {//Topic posted by others
                        if (contentVerificationStatus.equalsIgnoreCase("yes")) {
                            // Redirecting to MyTopics
                            response.sendRedirect(request.getContextPath() + "/procForum.jsp?succFlag=y&verificationRequired=no");
                        } else {
                            // Redirecting to AllTopics
                            response.sendRedirect(request.getContextPath() + "/procForum.jsp?succFlag=y&verificationRequired=no");
                        }
                    }
                } catch (Exception ex) {
                    LOGGER.error("processRequest : " + logUserId + " : " + ex);
                    response.sendRedirect(request.getContextPath() + "/StackTrace.jsp");
                }
            } else if (action.equalsIgnoreCase("verifyForum")) {
                String forumType = request.getParameter("forumType");
                String postedBy = request.getParameter("postedBy");
                String activity = "";
                //String topicHead = null;
                if (forumType.equalsIgnoreCase("reply")) {
                    activity = "verifyReply";
                    //topicHead = request.getParameter("topicSubject");
                } else {
                    activity = "verifyTopic";
                }                
                String forumStatus = request.getParameter("adminaction");
                DateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                java.util.Date postedDateUtil = formater.parse(request.getParameter("postedDate"));
                java.sql.Date postedDateSQL = new java.sql.Date(postedDateUtil.getTime());
                TblPublicProcureForum tblPublicProcureForum = new TblPublicProcureForum();
                tblPublicProcureForum.setPpfId(Integer.parseInt(request.getParameter("ppfid")));
                tblPublicProcureForum.setPostedBy(Integer.parseInt(logUserId));
                tblPublicProcureForum.setSubject(request.getParameter("topicSubject"));
                tblPublicProcureForum.setDescription(request.getParameter("topicDetail"));
                tblPublicProcureForum.setPostDate(postedDateSQL);
                tblPublicProcureForum.setPpfParentId(Integer.parseInt(request.getParameter("parentppfid")));
                tblPublicProcureForum.setFullName(request.getParameter("auother"));
                tblPublicProcureForum.setStatus(forumStatus);
                tblPublicProcureForum.setComments(new HandleSpecialChar().handleSpecialChar(request.getParameter("comments")));
                publicForumPostService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
                publicForumPostService.updateForum(tblPublicProcureForum/*,topicHead*/);
                response.sendRedirect(request.getContextPath() + "/ViewAllTopic.jsp?succFlag=y&activity=" + activity + "&forumStatus=" + forumStatus);
            }
        } catch (Exception ex) {
            LOGGER.error("processRequest : " + logUserId + " : " + ex);
        } finally {
            out.close();
        }
        LOGGER.debug("processRequest : " + logUserId + " Ends");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
