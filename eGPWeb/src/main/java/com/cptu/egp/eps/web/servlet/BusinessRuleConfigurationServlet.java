/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.service.serviceinterface.ConfigPreTenderRuleService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class BusinessRuleConfigurationServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final Logger LOGGER = Logger.getLogger(BusinessRuleConfigurationServlet.class);
    private String logUserId = "0";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
            }
        LOGGER.debug("processRequest : "+logUserId+" : Starts");
        try {
            String funName = request.getParameter("funName");
            if ("TOCajaxUniqueConfig".equals(funName)) {
                out.print(tocajaxUniqueConfig(request.getParameter("configtecId"),request.getParameter("commitType"),request.getParameter("procNature")));
                out.flush();
            }
            if ("TECajaxUniqueConfig".equals(funName)) {
                out.print(tecajaxUniqueConfig(request.getParameter("configtecId"),request.getParameter("commitType"),request.getParameter("procNature"),request.getParameter("minvalue"),request.getParameter("maxvalue")));
                out.flush();
            }
            if ("TCajaxUniqueConfig".equals(funName)) {
                out.print(tcajaxUniqueConfig(request.getParameter("configtecId"),request.getParameter("commitType"),request.getParameter("procNature")));
                out.flush();
            }
            if ("ProcMethodRule".equals(funName)) {
                out.print(procajaxUniqueConfig(request.getParameter("configProcurementId"),request.getParameter("budgetType"),request.getParameter("tenderType"),request.getParameter("procMethod"),request.getParameter("procType"),request.getParameter("procNature"),request.getParameter("typeofImergency"),request.getParameter("minvalue"),request.getParameter("maxvalue")));
                out.flush();
            }
            if ("ProcMethodRuleForEdit".equals(funName)) {
                out.print(procajaxUniqueConfigEdit(request.getParameter("configProcurementId"),request.getParameter("budgetType"),request.getParameter("tenderType"),request.getParameter("procMethod"),request.getParameter("procType"),request.getParameter("procNature"),request.getParameter("typeofImergency"),request.getParameter("minvalue"),request.getParameter("maxvalue")));
                out.flush();
            }
            if ("ProcMethodRule2".equals(funName)) {
                out.print(procajaxUniqueConfig2(request.getParameter("budgetType"),request.getParameter("tenderType"),request.getParameter("procMethod"),request.getParameter("procType"),request.getParameter("procNature"),request.getParameter("typeofImergency"),request.getParameter("minvalue"),request.getParameter("maxvalue")));
                out.flush();
            }
            if ("PreTenderMettingRule".equals(funName)) {
                out.print(preajaxUniqueConfig(request.getParameter("preTenderMeetId"),request.getParameter("tenderType"),request.getParameter("procMethod"),request.getParameter("procType")));
                out.flush();
            }
            if ("AmendmentRule".equals(funName)) {
                out.print(amendmentajaxUniqueConfig(request.getParameter("configAmendmentId"),request.getParameter("procType"),request.getParameter("procMethod")));
                out.flush();
            }
            if ("TSCFormationRule".equals(funName)) {
                out.print(tscformationajaxUniqueConfig(request.getParameter("configtecId"),request.getParameter("commityType"),request.getParameter("procNature")));
                out.flush();
            }
            if ("STDSelectionRule".equals(funName)) {
                out.print(stdselectionajaxUniqueConfig(request.getParameter("configStdId"),request.getParameter("tenderType"),request.getParameter("procNature"),request.getParameter("procMethod"),request.getParameter("procType"),request.getParameter("operator"),request.getParameter("valueinbdtaka"),request.getParameter("stdname")));
                out.flush();
            }
            if ("NOABusinessRule".equals(funName)) {
                out.print(noabusinessajaxUniqueConfig(request.getParameter("configNoaId"),request.getParameter("procNature"),request.getParameter("procMethod"),request.getParameter("procType"),request.getParameter("minvalue"),request.getParameter("maxvalue")));
                out.flush();
            }
        } catch (Exception ex) {
            LOGGER.error("processRequest : "+logUserId+" : Exception" +ex);
        } finally { 
            out.close();
        }
        LOGGER.debug("processRequest : "+logUserId+" : Ends");
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String tocajaxUniqueConfig(String confid,String comitype,String pNature) throws Exception{
        LOGGER.debug("ajaxUniqueConfig : "+logUserId+" : Starts");
        ConfigPreTenderRuleService configPretenderRuleService = (ConfigPreTenderRuleService) AppContext.getSpringBean("configPreTenderRuleService");
        configPretenderRuleService.setLogUserId(logUserId);
        String val = "";
        try{
            if(pNature.equals("3"))
            {
                val = String.valueOf(configPretenderRuleService.tocuniqueConfigCount(confid,"POC",pNature));
            }
            else
            {
                val = String.valueOf(configPretenderRuleService.tocuniqueConfigCount(confid,"TOC",pNature));
            }
            
        }catch(Exception e){
            LOGGER.error("ajaxUniqueConfig : "+logUserId+" : Exception" +e);
        }
        LOGGER.debug("ajaxUniqueConfig : "+logUserId+" : Ends");
        return val;
    }

    private String tecajaxUniqueConfig(String confid,String comitype,String pNature,String minvalue,String maxvalue) throws Exception{
        LOGGER.debug("ajaxUniqueConfig : "+logUserId+" : Starts");
        ConfigPreTenderRuleService configPretenderRuleService = (ConfigPreTenderRuleService) AppContext.getSpringBean("configPreTenderRuleService");
        configPretenderRuleService.setLogUserId(logUserId);
        String val = "";
        try{
            if(pNature.equals("3"))
            {
                val = String.valueOf(configPretenderRuleService.tecuniqueConfigCount(confid,"PEC",pNature,minvalue,maxvalue));
            }
            else
            {
                val = String.valueOf(configPretenderRuleService.tecuniqueConfigCount(confid,"TEC",pNature,minvalue,maxvalue));
            }
            
        }catch(Exception e){
            LOGGER.error("TECajaxUniqueConfig : "+logUserId+" : Exception" +e);
        }
        LOGGER.debug("ajaxUniqueConfig : "+logUserId+" : Ends");
        return val;
    }
    
    private String tcajaxUniqueConfig(String confid,String comitype,String pNature) throws Exception{
        LOGGER.debug("ajaxUniqueConfig : "+logUserId+" : Starts");
        ConfigPreTenderRuleService configPretenderRuleService = (ConfigPreTenderRuleService) AppContext.getSpringBean("configPreTenderRuleService");
        configPretenderRuleService.setLogUserId(logUserId);
        String val = "";
        try{
            if(pNature.equals("3"))
            {
                val = String.valueOf(configPretenderRuleService.tscformationMethodUniqueConfigCount(confid,"PC",pNature));
            }
            else
            {
                val = String.valueOf(configPretenderRuleService.tscformationMethodUniqueConfigCount(confid,"TC",pNature));
            }
            
        }catch(Exception e){
            LOGGER.error("TECajaxUniqueConfig : "+logUserId+" : Exception" +e);
        }
        LOGGER.debug("ajaxUniqueConfig : "+logUserId+" : Ends");
        return val;
    }
    

    private String procajaxUniqueConfig(String configProcurementId, String budgetType, String tenderType, String procMethod, String procType, String procNature, String typeofImergency,String minvalue,String maxvalue) throws Exception{
        LOGGER.debug("ProcajaxUniqueConfig : "+logUserId+" : Starts");
        ConfigPreTenderRuleService configPretenderRuleService = (ConfigPreTenderRuleService) AppContext.getSpringBean("configPreTenderRuleService");
        configPretenderRuleService.setLogUserId(logUserId);
        String val = "";
        try{
            val = String.valueOf(configPretenderRuleService.procMethodUniqueConfigCount(configProcurementId, budgetType, tenderType, procMethod, procType, procNature, typeofImergency,minvalue,maxvalue));
        }catch(Exception e){
            LOGGER.error("ProcajaxUniqueConfig : "+logUserId+" : Exception" +e);
        }
         LOGGER.debug("ProcajaxUniqueConfig : "+logUserId+" : Ends");
        return val;
    }
    private String procajaxUniqueConfigEdit(String configProcurementId, String budgetType, String tenderType, String procMethod, String procType, String procNature, String typeofImergency,String minvalue,String maxvalue) throws Exception{
        LOGGER.debug("ProcajaxUniqueConfig : "+logUserId+" : Starts");
        ConfigPreTenderRuleService configPretenderRuleService = (ConfigPreTenderRuleService) AppContext.getSpringBean("configPreTenderRuleService");
        configPretenderRuleService.setLogUserId(logUserId);
        String val = "";
        try{
            val = String.valueOf(configPretenderRuleService.procMethodUniqueConfigCountEdit(configProcurementId, budgetType, tenderType, procMethod, procType, procNature, typeofImergency,minvalue,maxvalue));
        }catch(Exception e){
            LOGGER.error("ProcajaxUniqueConfig : "+logUserId+" : Exception" +e);
        }
         LOGGER.debug("ProcajaxUniqueConfig : "+logUserId+" : Ends");
        return val;
    }
    private String procajaxUniqueConfig2(String budgetType, String tenderType, String procMethod, String procType, String procNature, String typeofImergency,String minvalue,String maxvalue) throws Exception{
        LOGGER.debug("ProcajaxUniqueConfig : "+logUserId+" : Starts");
        ConfigPreTenderRuleService configPretenderRuleService = (ConfigPreTenderRuleService) AppContext.getSpringBean("configPreTenderRuleService");
        configPretenderRuleService.setLogUserId(logUserId);
        String val = "";
        try{
            val = String.valueOf(configPretenderRuleService.procMethodUniqueConfigCount2(budgetType, tenderType, procMethod, procType, procNature, typeofImergency,minvalue,maxvalue));
        }catch(Exception e){
            LOGGER.error("ProcajaxUniqueConfig : "+logUserId+" : Exception" +e);
        }
         LOGGER.debug("ProcajaxUniqueConfig : "+logUserId+" : Ends");
        return val;
    }
    
    private String preajaxUniqueConfig(String preTenderMeetId, String tenderType, String procMethod, String procType) throws Exception{
        LOGGER.debug("preajaxUniqueConfig : "+logUserId+" : Starts");
        ConfigPreTenderRuleService configPretenderRuleService = (ConfigPreTenderRuleService) AppContext.getSpringBean("configPreTenderRuleService");
        configPretenderRuleService.setLogUserId(logUserId);
        String val = "";
        try{
            val = String.valueOf(configPretenderRuleService.preMethodUniqueConfigCount(preTenderMeetId,tenderType,procMethod,procType));
        }catch(Exception e){
            LOGGER.error("preajaxUniqueConfig : "+logUserId+" : Exception" +e);
        }
         LOGGER.debug("preajaxUniqueConfig : "+logUserId+" : Ends");
        return val;
    }
    private String amendmentajaxUniqueConfig(String configAmendmentId,String procType, String procMethod) throws Exception{
        LOGGER.debug("amendmentajaxUniqueConfig : "+logUserId+" : Starts");
        ConfigPreTenderRuleService configPretenderRuleService = (ConfigPreTenderRuleService) AppContext.getSpringBean("configPreTenderRuleService");
        configPretenderRuleService.setLogUserId(logUserId);
        String val = "";
        try{
            val = String.valueOf(configPretenderRuleService.amendmentMethodUniqueConfigCount(configAmendmentId,procType,procMethod));
        }catch(Exception e){
            LOGGER.error("amendmentajaxUniqueConfig : "+logUserId+" : Exception" +e);
        }
         LOGGER.debug("amendmentajaxUniqueConfig : "+logUserId+" : Ends");
        return val;
    }
    private String tscformationajaxUniqueConfig(String configtecId,String commityType, String procNature) throws Exception{
        LOGGER.debug("tscformationajaxUniqueConfig : "+logUserId+" : Starts");
        ConfigPreTenderRuleService configPretenderRuleService = (ConfigPreTenderRuleService) AppContext.getSpringBean("configPreTenderRuleService");
        configPretenderRuleService.setLogUserId(logUserId);
        String val = "";
        try{
            if(procNature.equals("3"))
            {
                val = String.valueOf(configPretenderRuleService.tscformationMethodUniqueConfigCount(configtecId,"PC",procNature));
            }
            else
            {
                val = String.valueOf(configPretenderRuleService.tscformationMethodUniqueConfigCount(configtecId,"TC",procNature));
            }
            
        }catch(Exception e){
            LOGGER.error("tscformationajaxUniqueConfig : "+logUserId+" : Exception" +e);
        }
         LOGGER.debug("tscformationajaxUniqueConfig : "+logUserId+" : Ends");
        return val;
    }
    private String stdselectionajaxUniqueConfig(String configStdId,String tenderType, String procNature,String procMethod,String procType, String operator,String valueinbdtaka,String stdname) throws Exception{
        LOGGER.debug("tscformationajaxUniqueConfig : "+logUserId+" : Starts");
        ConfigPreTenderRuleService configPretenderRuleService = (ConfigPreTenderRuleService) AppContext.getSpringBean("configPreTenderRuleService");
        configPretenderRuleService.setLogUserId(logUserId);
        String val = "";
        try{
            val = String.valueOf(configPretenderRuleService.stdselectionMethodUniqueConfigCount(configStdId,tenderType,procNature,procMethod,procType,operator,valueinbdtaka,stdname));
        }catch(Exception e){
            LOGGER.error("tscformationajaxUniqueConfig : "+logUserId+" : Exception" +e);
        }
         LOGGER.debug("tscformationajaxUniqueConfig : "+logUserId+" : Ends");
        return val;
    }
    private String noabusinessajaxUniqueConfig(String configNoaId,String procNature, String procMethod,String procType,String minvalue,String maxvalue) throws Exception{
        LOGGER.debug("tscformationajaxUniqueConfig : "+logUserId+" : Starts");
        ConfigPreTenderRuleService configPretenderRuleService = (ConfigPreTenderRuleService) AppContext.getSpringBean("configPreTenderRuleService");
        configPretenderRuleService.setLogUserId(logUserId);
        String val = "";
        try{
            val = String.valueOf(configPretenderRuleService.noabusinessMethodUniqueConfigCount(configNoaId,procNature,procMethod,procType,minvalue,maxvalue));
        }catch(Exception e){
            LOGGER.error("tscformationajaxUniqueConfig : "+logUserId+" : Exception" +e);
        }
         LOGGER.debug("tscformationajaxUniqueConfig : "+logUserId+" : Ends");
        return val;
    }

}

