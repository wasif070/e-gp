/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.model.table.TblMediaContent;
import com.cptu.egp.eps.service.serviceimpl.MediaContentService;
import com.cptu.egp.eps.service.serviceimpl.PublicForumPostService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author shreyansh
 */
public class GetPressRelease extends HttpServlet {

    static final Logger LOGGER = Logger.getLogger(GetPressRelease.class);
    private String logUserId = "0";
    MediaContentService mediaContentService = (MediaContentService) AppContext.getSpringBean("MediaContentService");
    PublicForumPostService publicForumPostService = (PublicForumPostService) AppContext.getSpringBean("PublicForumPostService");

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
        mediaContentService.setLogUserId(logUserId);
        publicForumPostService.setUserId(logUserId);
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.debug("processRequest : " + logUserId + " Starts");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        Object objUserId = session.getAttribute("userId");
        if (objUserId != null) {
            logUserId = objUserId.toString();
        }
        setLogUserId(logUserId);
        String strAction = request.getParameter("hdn_action");

        if ("postMediaContent".equals(strAction)) {
            String strMediaType = request.getParameter("hdn_MediaType");
            TblMediaContent tblMediaContent = new TblMediaContent();
            tblMediaContent.setContentText(request.getParameter("txtarea_MediaDetail"));
            tblMediaContent.setContentType(strMediaType);
            tblMediaContent.setCreatedBy(Integer.parseInt(logUserId));
            tblMediaContent.setCreatedDate(new java.sql.Date(new java.util.Date().getTime()));
            tblMediaContent.setContentSub(request.getParameter("txt_MediaSubject"));
            mediaContentService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
            mediaContentService.saveMediaContent(tblMediaContent);
            response.sendRedirect("admin/MediaContList.jsp?contentType=" + strMediaType + "&succFlag=y");
            //response.sendRedirectFilter("MediaContList.jsp?contentType=" + strMediaType + "&succFlag=y");

        } else if ("searchMediaContent".equals(strAction)) {
             int i = 0;

        try {
            String styleClass = "";
            int totalRecord = 0;
            List<SPCommonSearchData> colResult = null;
            colResult = searchAllPosts(request);//define the method to get all the data
            int totalpage = 0;

                String strViewPath = request.getContextPath() + "/admin/ViewMediaContent.jsp?MediaId=";
            //displaying the data


            if (colResult != null && !colResult.isEmpty()) {
                for (SPCommonSearchData data : colResult) {

                    if (i % 2 == 0) {
                        styleClass = "bgColor-white";
                    } else {
                        styleClass = "bgColor-Green";
                    }
                    out.print("<tr class='" + styleClass + "'>");
                        out.print("<td class=\"t-align-center \">" + data.getFieldName1() + "</td>");
                        out.print("<td class=\"t-align-left\" style=\"WORD-BREAK:BREAK-ALL\">" + data.getFieldName8() + "</td>");
                        out.print("<td width=12% class=\"t-align-center\">" + data.getFieldName5() + "</td>");
                        out.print("<td class=\"t-align-center\">" + "<a href =" + strViewPath + data.getFieldName2() + ">View</a> </td>");
                    out.print("</tr>");
                    totalpage = Integer.parseInt(data.getFieldName6());
                    totalRecord = Integer.parseInt(data.getFieldName7());
                    i++;
                }

            } else {
                out.print("<tr>");

                    out.print("<td colspan=\"4\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                out.print("</tr>");
            }
            if (totalpage == 0) {
                totalpage = 1;
            } else {
                if (totalRecord % 10 != 0) {
                    totalpage = totalpage + 1;
                }
            }
            out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalpage + "\">");

        } catch (Exception exp) {
                LOGGER.error("processRequest : " + logUserId + " : " + exp);
        }


        }
        LOGGER.debug("processRequest : " + logUserId + " Ends");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /**
     * Method for search all posts of Media content
     * @param request
     * @return List of SPCommonSearchData objects
     */
    private List<SPCommonSearchData> searchAllPosts(HttpServletRequest request) {
        LOGGER.debug("searchAllPosts : " + logUserId + " Starts");
        List<SPCommonSearchData> list = null;

//getting the data from jsp

        String keyword = request.getParameter("keyword");
        String pageNo = request.getParameter("pageNo");
        String recordOffset = request.getParameter("size");
        String content = request.getParameter("viewType");
        String dop = request.getParameter("textfield5");
        if (dop == null) {
            dop = "";
        }
//calling the procedure
        if (!"".equals(keyword) || !"".equals(dop)) {
            list =  publicForumPostService.getpost("search_MediaContent",
                    keyword, dop, pageNo, recordOffset, content, "", "", "", "");
        } else {
            list =  publicForumPostService.getpost("search_MediaContent", "",
                    "", pageNo, recordOffset, content, "", "", "", "");
        }
        LOGGER.debug("searchAllPosts : " + logUserId + " ends");
        return list;
    }

}
