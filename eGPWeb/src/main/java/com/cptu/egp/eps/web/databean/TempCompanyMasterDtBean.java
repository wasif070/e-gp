/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

import com.cptu.egp.eps.model.table.TblTempCompanyMaster;
import com.cptu.egp.eps.web.utility.BanglaNameUtils;
import com.cptu.egp.eps.web.utility.DateUtils;
import java.util.Date;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author Administrator
 */
public class TempCompanyMasterDtBean {

    /**
     * Creates a new instance of TempCompanyMasterDataBean
     */
    public TempCompanyMasterDtBean() {
    }
    private String companyName;
    private byte[] companyNameInBangla;
    private String companyRegNumber;
    private String establishmentYear = "";
    private Date licIssueDate;
    private Date licExpiryDate;
    private String regOffAddress;
    private String regOffCountry;
    private String regOffState;
    private String regOffCity = "";
    private String regOffUpjilla = "";
    private String regOffPostcode = "";
    private String regOffPhoneNo = "";
    private String regOffFaxNo = "";
    private String corpOffAddress;
    private String corpOffCountry;
    private String corpOffState;
    private String corpOffCity = "";
    private String corpOffUpjilla = "";
    private String corpOffPostcode = "";
    private String corpOffPhoneno = "";
    private String corpOffFaxNo = "";
    private String specialization = "";
    private String legalStatus;
    private String tinNo = "";
    private String website = "";
    private String banglaName;
    private String licIssueDateStr;
    private String licExpiryDateStr;
    private int companyId;
    private int userId;
    private String tinDocName = "";
    private String otherDoc = "";
    private String corpPhoneSTD = "";
    private String corpFaxSTD = "";
    private String regPhoneSTD = "";
    private String regFaxSTD = "";
    private String statutoryCertificateNo = "";
    private String originCountry = "";
    private String workCategory = "";
    private String tradeLicenseNumber = "";
    private String regOffMobileNo;
    private String corpOffMobMobileNo;
    private String regOffSubDistrict;
    private String corpOffSubDistrict;

    public String getCorpFaxSTD() {
        return corpFaxSTD;
    }

    public void setCorpFaxSTD(String corpFaxSTD) {
        this.corpFaxSTD = corpFaxSTD;
    }

    public String getCorpPhoneSTD() {
        return corpPhoneSTD;
    }

    public void setCorpPhoneSTD(String corpPhoneSTD) {
        this.corpPhoneSTD = corpPhoneSTD;
    }

    public String getRegFaxSTD() {
        return regFaxSTD;
    }

    public void setRegFaxSTD(String regFaxSTD) {
        this.regFaxSTD = regFaxSTD;
    }

    public String getRegPhoneSTD() {
        return regPhoneSTD;
    }

    public void setRegPhoneSTD(String regPhoneSTD) {
        this.regPhoneSTD = regPhoneSTD;
    }

    public String getOtherDoc() {
        return otherDoc;
    }

    public void setOtherDoc(String otherDoc) {
        if (otherDoc != null) {
            setTinDocName(tinDocName + "$" + otherDoc);
        }
        this.otherDoc = otherDoc;
    }

    public String getTinDocName() {
        return tinDocName;
    }

    public void setTinDocName(String tinDocName) {
        this.tinDocName = tinDocName;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getLicExpiryDateStr() {
        return licExpiryDateStr;
    }

    public void setLicExpiryDateStr(String licExpiryDateStr) {
        this.licExpiryDate = DateUtils.formatStdString(licExpiryDateStr);
        this.licExpiryDateStr = licExpiryDateStr;
    }

    public String getLicIssueDateStr() {
        return licIssueDateStr;
    }

    public void setLicIssueDateStr(String licIssueDateStr) {
        this.licIssueDate = DateUtils.formatStdString(licIssueDateStr);
        this.licIssueDateStr = licIssueDateStr;
    }

    public String getBanglaName() {
        return banglaName;
    }

    public void setBanglaName(String banglaName) {
        if (banglaName != null) {
            if (!banglaName.equals("")) {
                this.companyNameInBangla = BanglaNameUtils.getBytes(banglaName);
            }
        }
        this.banglaName = banglaName;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getLegalStatus() {
        return legalStatus;
    }

    public void setLegalStatus(String legalStatus) {
        this.legalStatus = legalStatus;
    }

    public String getTinNo() {
        return tinNo;
    }

    public void setTinNo(String tinNo) {
        this.tinNo = tinNo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public byte[] getCompanyNameInBangla() {
        return companyNameInBangla;
    }

    public void setCompanyNameInBangla(byte[] companyNameInBangla) {
        this.companyNameInBangla = companyNameInBangla;
    }

    public String getCompanyRegNumber() {
        return companyRegNumber;
    }

    public void setCompanyRegNumber(String companyRegNumber) {
        this.companyRegNumber = companyRegNumber;
    }

    public String getTradeLicenseNumber() {
        return this.tradeLicenseNumber;
    }

    public void setTradeLicenseNumber(String tradeLicenseNumber) {
        this.tradeLicenseNumber = tradeLicenseNumber;
    }

    public String getCorpOffAddress() {
        return corpOffAddress;
    }

    public void setCorpOffAddress(String corpOffAddress) {
        this.corpOffAddress = corpOffAddress;
    }

    public String getCorpOffCity() {
        return corpOffCity;
    }

    public void setCorpOffCity(String corpOffCity) {
        this.corpOffCity = corpOffCity;
    }

    public String getCorpOffCountry() {
        return corpOffCountry;
    }

    public void setCorpOffCountry(String corpOffCountry) {
        this.corpOffCountry = corpOffCountry;
    }

    public String getCorpOffFaxNo() {
        return corpOffFaxNo;
    }

    public void setCorpOffFaxNo(String corpOffFaxNo) {
        this.corpOffFaxNo = getCorpFaxSTD() + "-" + corpOffFaxNo;
    }

    public String getCorpOffPhoneno() {
        return corpOffPhoneno;
    }

    public void setCorpOffPhoneno(String corpOffPhoneno) {
        this.corpOffPhoneno = getCorpPhoneSTD() + "-" + corpOffPhoneno;
    }

    public String getCorpOffPostcode() {
        return corpOffPostcode;
    }

    public void setCorpOffPostcode(String corpOffPostcode) {
        this.corpOffPostcode = corpOffPostcode;
    }

    public String getCorpOffState() {
        return corpOffState;
    }

    public void setCorpOffState(String corpOffState) {
        this.corpOffState = corpOffState;
    }

    public String getCorpOffUpjilla() {
        return corpOffUpjilla;
    }

    public void setCorpOffUpjilla(String corpOffUpjilla) {
        this.corpOffUpjilla = corpOffUpjilla;
    }

    public String getEstablishmentYear() {
        return establishmentYear;
    }

    public void setEstablishmentYear(String establishmentYear) {
        this.establishmentYear = establishmentYear;
    }

    public Date getLicExpiryDate() {
        return licExpiryDate;
    }

    public void setLicExpiryDate(Date licExpiryDate) {
        this.licExpiryDate = licExpiryDate;
    }

    public Date getLicIssueDate() {
        return licIssueDate;
    }

    public void setLicIssueDate(Date licIssueDate) {
        this.licIssueDate = licIssueDate;
    }

    public String getRegOffAddress() {
        return regOffAddress;
    }

    public void setRegOffAddress(String regOffAddress) {
        this.regOffAddress = regOffAddress;
    }

    public String getRegOffCity() {
        return regOffCity;
    }

    public void setRegOffCity(String regOffCity) {
        this.regOffCity = regOffCity;
    }

    public String getRegOffCountry() {
        return regOffCountry;
    }

    public void setRegOffCountry(String regOffCountry) {
        this.regOffCountry = regOffCountry;
    }

    public String getRegOffFaxNo() {
        return regOffFaxNo;
    }

    public void setRegOffFaxNo(String regOffFaxNo) {
        this.regOffFaxNo = getRegFaxSTD() + "-" + regOffFaxNo;
    }

    public String getRegOffPhoneNo() {
        return regOffPhoneNo;
    }

    public void setRegOffPhoneNo(String regOffPhoneNo) {
        this.regOffPhoneNo = getRegPhoneSTD() + "-" + regOffPhoneNo;
    }

    public String getRegOffPostcode() {
        return regOffPostcode;
    }

    public void setRegOffPostcode(String regOffPostcode) {
        this.regOffPostcode = regOffPostcode;
    }

    public String getRegOffState() {
        return regOffState;
    }

    public void setRegOffState(String regOffState) {
        this.regOffState = regOffState;
    }

    public String getRegOffUpjilla() {
        return regOffUpjilla;
    }

    public void setRegOffUpjilla(String regOffUpjilla) {
        this.regOffUpjilla = regOffUpjilla;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    /**
     * @return the statutoryCertificateNo
     */
    public String getStatutoryCertificateNo() {
        return statutoryCertificateNo;
    }

    /**
     * @param statutoryCertificateNo the statutoryCertificateNo to set
     */
    public void setStatutoryCertificateNo(String statutoryCertificateNo) {
        this.statutoryCertificateNo = statutoryCertificateNo;
    }

    /**
     * @return the originCountry
     */
    public String getOriginCountry() {
        return originCountry;
    }

    /**
     * @param originCountry the originCountry to set
     */
    public void setOriginCountry(String originCountry) {
        this.originCountry = originCountry;
    }

    public String getWorkCategory() {
        return workCategory;
    }

    public void setWorkCategory(String workCategory) {
        this.workCategory = workCategory;
    }

    public String getRegOffMobileNo() {
        return this.regOffMobileNo;
    }

    public void setRegOffMobileNo(String regOffMobileNo) {
        this.regOffMobileNo = regOffMobileNo;
    }

    public String getCorpOffMobMobileNo() {
        return this.corpOffMobMobileNo;
    }

    public void setCorpOffMobMobileNo(String corpOffMobMobileNo) {
        this.corpOffMobMobileNo = corpOffMobMobileNo;
    }

    public String getRegOffSubDistrict() {
        return this.regOffSubDistrict;
    }

    public void setRegOffSubDistrict(String regOffSubDistrict) {
        this.regOffSubDistrict = regOffSubDistrict;
    }

    public String getCorpOffSubDistrict() {
        return this.corpOffSubDistrict;
    }

    public void setCorpOffSubDistrict(String corpOffSubDistrict) {
        this.corpOffSubDistrict = corpOffSubDistrict;
    }
}
