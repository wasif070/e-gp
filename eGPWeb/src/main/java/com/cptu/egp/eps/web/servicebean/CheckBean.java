/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class CheckBean {

    private UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    static final Logger logger = Logger.getLogger(CheckBean.class);
    private String loggerStart = " Starts";
    private String loggerEnd = " Ends";
    private String logUserId = "0";

    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        userRegisterService.setUserId(logUserId);
        this.logUserId = logUserId;
    }

    public UserRegisterService getUserRegisterService() {
        return userRegisterService;
    }

    public void setUserRegisterService(UserRegisterService userRegisterService) {
        this.userRegisterService = userRegisterService;
    }
    List<TblLoginMaster> loginMaster = new ArrayList<TblLoginMaster>();

    public List<TblLoginMaster> getLoginMaster() {
        logger.debug("getLoginMaster : " + logUserId + loggerStart);
        if (loginMaster.isEmpty()) {
            try {
//                loginMaster = getLoginMasterDao().findTblLoginMaster("userId", Operation_enum.ORDERBY, Operation_enum.ASC);
                loginMaster = getUserRegisterService().getAllUsers();
            } catch (Exception ex) {
                logger.error("getLoginMaster : " + logUserId,ex);
            }
        }
        logger.debug("getLoginMaster : " + logUserId + loggerEnd);
        return loginMaster;
    }

    public void setLoginMaster(List<TblLoginMaster> loginMaster) {
        this.loginMaster = loginMaster;
    }
}
