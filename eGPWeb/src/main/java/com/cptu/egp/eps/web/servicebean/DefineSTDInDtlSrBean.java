/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblIttHeader;
import com.cptu.egp.eps.model.table.TblTemplateGuildeLines;
import com.cptu.egp.eps.model.table.TblTemplateMaster;
import com.cptu.egp.eps.model.table.TblTemplateSectionDocs;
import com.cptu.egp.eps.model.table.TblTemplateSectionForm;
import com.cptu.egp.eps.model.table.TblTemplateSections;
import com.cptu.egp.eps.service.serviceimpl.TemplateDocumentsService;
import com.cptu.egp.eps.service.serviceimpl.TemplateMasterImpl;
import com.cptu.egp.eps.service.serviceimpl.TemplateSectionFormImpl;
import com.cptu.egp.eps.service.serviceimpl.TemplateSectionImpl;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author yanki
 */
public class DefineSTDInDtlSrBean extends HttpServlet {
    private String logUserId = "0";
    static final Logger LOGGER = Logger.getLogger(DefineSTDInDtlSrBean.class);
   
    /**
     * 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
            
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>


    /**
     * Get Template Master
     * @param templateId
     * @return List of Template Master.
     */
    public List<TblTemplateMaster> getTemplateMaster(short templateId) {
        LOGGER.debug("getTemplateMaster "+logUserId+" Starts:");
        List<TblTemplateMaster> list = new ArrayList<TblTemplateMaster>();
        TemplateMasterImpl templateMaster = (TemplateMasterImpl) AppContext.getSpringBean("TemplateMasterService");
        try {
                templateMaster.setUserId(logUserId);
                list = templateMaster.getTemplateDtl(templateId);
        } catch (Exception e) {
            LOGGER.error("getTemplateMaster "+logUserId+" :"+e);
    }
        LOGGER.debug("getTemplateMaster "+logUserId+" Ends:");
        return list;
    }

    /**
     * Get Template Sections
     * @param templateId
     * @return List of Template Section
     */
    public List<TblTemplateSections> getTemplateSection(short templateId) {
        LOGGER.debug("getTemplateSection "+logUserId+" Starts:");
        List<TblTemplateSections> list = new ArrayList<TblTemplateSections>();
        TemplateSectionImpl templateSectionImpl = (TemplateSectionImpl)  AppContext.getSpringBean("TemplateSectionService");
        try {
                templateSectionImpl.setUserId(logUserId);
                list = templateSectionImpl.getTemplateSection(templateId);
        } catch (Exception e) {
            LOGGER.error("getTemplateSection "+logUserId+" :"+e);
    }
        LOGGER.debug("getTemplateSection "+logUserId+" Ends:");
        
        return list;
    }

    /**
     * Get Template Section forms
     * @param templateId
     * @param sectionId
     * @return List of Template Section forms
     */
    public List<TblTemplateSectionForm> getTemplateSectionForm(short templateId, int sectionId) {
        LOGGER.debug("getTemplateSectionForm "+logUserId+" Starts:");
        List<TblTemplateSectionForm> list = new ArrayList<TblTemplateSectionForm>();
        TemplateSectionFormImpl templateSectionFormImpl = (TemplateSectionFormImpl)  AppContext.getSpringBean("AddFormService");
        try {
                 templateSectionFormImpl.setUserId(logUserId);
                list  = templateSectionFormImpl.getForms(templateId, sectionId);
        } catch (Exception e) {
            LOGGER.error("getTemplateSectionForm "+logUserId+" :"+e);
    }
        LOGGER.debug("getTemplateSectionForm "+logUserId+" Ends:");
        return list;
    }

    /**
     * Get Template Section Documents.
     * @param templateId
     * @param sectionId
     * @return List of Template Section Docs.
     */
    public List<TblTemplateSectionDocs> getTemplateSectionDocs(short templateId, short sectionId) {
        LOGGER.debug("getTemplateSectionForm "+logUserId+" Starts:");
        List<TblTemplateSectionDocs> list = new ArrayList<TblTemplateSectionDocs>();
        TemplateDocumentsService templateDocument = (TemplateDocumentsService) AppContext.getSpringBean("TemplateDocumentService");
        try {
            templateDocument.setUserId(logUserId);
            list = templateDocument.getAllDocForSection(templateId, sectionId);
        } catch (Exception e) {
            LOGGER.error("getTemplateSectionForm "+logUserId+" :"+e);
    }
        LOGGER.debug("getTemplateSectionForm "+logUserId+" Ends:");
        return list;
    }

    /**
     * Get Guide Line Docs.
     * @param templateId
     * @return List of Guide Line Docs.
     */
    public List<TblTemplateGuildeLines> getGuideLinesDocs(int templateId) {
        LOGGER.debug("getGuideLinesDocs "+logUserId+" Starts:");
        List<TblTemplateGuildeLines> list = new ArrayList<TblTemplateGuildeLines>();
        TemplateDocumentsService templateDocument = (TemplateDocumentsService) AppContext.getSpringBean("TemplateDocumentService");
        try {
            list = templateDocument.getAllDocForGuideLines(templateId);
        } catch (Exception e) {
            LOGGER.error("getGuideLinesDocs "+logUserId+" :"+e);
        }
        LOGGER.debug("getGuideLinesDocs "+logUserId+" Ends:");
        return list;
    }


    /**
     * Check if Sub Section is created or not.
     * @param sectionId
     * @return true if created or false if not.
     */
    public boolean checkSubSectionCreate(int sectionId){
        /*
         * returns true if any sub section found under section
         */
        LOGGER.debug("checkSubSectionCreate "+logUserId+" Starts:");
        boolean check = false;
        try {
        CreateSubSectionSrBean createSubSectionSrBean = new CreateSubSectionSrBean();
        List<TblIttHeader> tblIttHeaderList = createSubSectionSrBean.getSubSectionDetail(sectionId);
                createSubSectionSrBean.setLogUserId(logUserId);
        if(tblIttHeaderList != null){
            if(tblIttHeaderList.size() > 0){
                check = true;
            }
        }
        createSubSectionSrBean = null;
        tblIttHeaderList = null;
        } catch (Exception e) {
            LOGGER.error("checkSubSectionCreate "+logUserId+" :"+e);
        }
        LOGGER.debug("checkSubSectionCreate "+logUserId+" Ends:");
        return check;
    }


    /**
     * Generate PDF on page Load.
     * @param templateId
     * @param reqURL
     * @param folderName
     * @return true if generated else false 
     */
    public boolean generatePDFOnLoad(short templateId, String reqURL, String folderName){
       
        LOGGER.debug("generatePDFOnLoad "+logUserId+" Starts:");
        boolean check = false;
        try {
             GenreatePdfCmd pdfCmd = new GenreatePdfCmd();
                int sectionId=0;
                        String sectionName="";
                        int FormId = 0;
                        
                        DefineSTDInDtlSrBean defSTDInDtl = new DefineSTDInDtlSrBean();
                        java.util.List<com.cptu.egp.eps.model.table.TblTemplateSections> tblTempSects = defSTDInDtl.getTemplateSection(templateId);
                        int ittId = -1, gccId = -1;
                       File file = null;
                        for(int i=0;i<tblTempSects.size();i++){
                            sectionId =  tblTempSects.get(i).getSectionId();
                            sectionName =  tblTempSects.get(i).getContentType();
                            file = new File(pdfCmd.path+"/"+folderName+"/"+templateId+"_"+sectionId);
                            if("ITT".equalsIgnoreCase(sectionName)){
                                ittId = sectionId;
                            }else if ("GCC".equalsIgnoreCase(sectionName)){
                                gccId = sectionId;
                            }
                            String reqQuery = "sectionId="+sectionId+"&templateId="+templateId;

                            if(sectionName.equalsIgnoreCase("TDS")){
                                reqQuery = "sectionId="+ittId+"&templateId="+templateId;
                            }else if (sectionName.equalsIgnoreCase("PCC")){
                                reqQuery = "sectionId="+gccId+"&templateId="+templateId;
                            }
                            if("ITT".equalsIgnoreCase(sectionName) || "GCC".equalsIgnoreCase(sectionName)){
                                reqURL =  reqURL.replace("DefineSTDInDtl","ITTView") ;
                                String genId=templateId+"_"+sectionId;
                                if(!file.exists())
                                pdfCmd.genrateCmd(reqURL, reqQuery, folderName,genId);
                            }
                            if("TDS".equalsIgnoreCase(sectionName) || "PCC".equalsIgnoreCase(sectionName)){
                                reqURL =  reqURL.replace("DefineSTDInDtl","TDSView") ;
                                String genId=templateId+"_"+sectionId;
                                if(!file.exists())
                                pdfCmd.genrateCmd(reqURL, reqQuery, folderName,genId);
                            }
                            if("Form".equalsIgnoreCase(sectionName)){
                                String strForAllFormPDF = "";
                                java.util.List<com.cptu.egp.eps.model.table.TblTemplateSectionForm> tblTemplateForms = defSTDInDtl.getTemplateSectionForm(templateId, sectionId);
                                String genId=templateId+"_"+sectionId;
                                reqURL =  reqURL.replace("DefineSTDInDtl","ViewCompleteForm") ;
                                if(tblTemplateForms != null){
                                    if(!tblTemplateForms.isEmpty()){
                                        for(int j=0;j<tblTemplateForms.size();j++){
                                            FormId = tblTemplateForms.get(j).getFormId();
                                            reqQuery = "templateId="+templateId+"&sectionId="+sectionId+"&formId="+FormId;
                                                strForAllFormPDF = "\"" + reqURL + "?" + reqQuery + "&isPDF=true" + "\" ";
                                        }
                                        if(!file.exists())
                                        pdfCmd.genrateCmdForm(strForAllFormPDF, "", folderName,genId);
                                    }
                                    tblTemplateForms = null;
                                }
                            }
                        }
                        pdfCmd = null;
                        check=true;
        } catch (Exception e) {
            LOGGER.error("generatePDFOnLoad "+logUserId+" :"+e);
        }
        LOGGER.debug("generatePDFOnLoad "+logUserId+" Ends:");
        return check;
    }

    /**
     * Check if template form is ok or not.
     * @param templateId
     * @return
     */
    public SPTenderCommonData isTemplateFormOk(int templateId){
        LOGGER.debug("isTemplateFormOk "+logUserId+" Starts:");
        TenderCommonService tCommSrv = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        SPTenderCommonData spTenCommData = null;
        try{
            tCommSrv.setLogUserId(logUserId);
            List<SPTenderCommonData> lstCD = tCommSrv.returndata("IsTemplateFormOK", templateId+"", "");
            if(lstCD != null){
                if(lstCD.size() > 0){
                    spTenCommData = lstCD.get(0);
                }
                lstCD = null;
            }
        }catch(Exception ex){
            LOGGER.error("isTemplateFormOk "+logUserId+" :"+ex);
        }
        LOGGER.debug("isTemplateFormOk "+logUserId+" Ends:");
        return spTenCommData;
    }

    /**
     * Set User Id at Bean Level.
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

}
