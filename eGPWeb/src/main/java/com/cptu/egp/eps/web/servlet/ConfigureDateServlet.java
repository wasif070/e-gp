/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblCmsDateConfig;
import com.cptu.egp.eps.model.table.TblCmsDateConfigHistory;
import com.cptu.egp.eps.model.table.TblContractSign;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.service.serviceimpl.ConsolodateService;
import com.cptu.egp.eps.service.serviceinterface.AccPaymentService;
import com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.DateUtils;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author dixit
 */
public class ConfigureDateServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
    private CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
    private UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    private ConsolodateService c_ConsSrv = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
    private AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
    private static final Logger LOGGER = Logger.getLogger(ConfigureDateServlet.class);
    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String logUserId = "0";
        String userTypeId = "";
        try {
            if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
                cmsConfigDateService.setLogUserId(logUserId);
                commonservice.setUserId(logUserId);
                registerService.setUserId(logUserId);
                accPaymentService.setLogUserId(logUserId);
                service.setLogUserId(logUserId);
            }
            if (request.getSession().getAttribute("userTypeId") != null) {
                userTypeId = request.getSession().getAttribute("userTypeId").toString();
            }
            LOGGER.debug("processRequest : " + logUserId + " Starts");
            String action = request.getParameter("action");
            //System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+action);

            String tenderId = "";
            if (request.getParameter("tenderId") != null) {
                tenderId = request.getParameter("tenderId");
            }

            String lotId = "";
            if (request.getParameter("lotId") != null) {
                lotId = request.getParameter("lotId");
            }

            String ContractSignId = "";
            if (request.getParameter("contractSignId") != null) {
                ContractSignId = request.getParameter("contractSignId");
            }
            String procnature = commonservice.getProcNature(tenderId).toString();
            int ContractId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
            if ("saveConfigureDates".equalsIgnoreCase(action)) {
                boolean flag = false;
                String Operation = "";
                String mailSubject = "";
                String Startdate = "";
                if (request.getParameter("ConStartdate") != null) {
                    Startdate = request.getParameter("ConStartdate");
                }
                String EndDate = "";
                if (request.getParameter("ConEnddate") != null) {
                    EndDate = request.getParameter("ConEnddate");
                }
                Date CstartDate = new Date();
                Date CendDate = new Date();
                CstartDate = DateUtils.formatStdString(DateUtils.formatStdDate(DateUtils.convertStringtoDate(Startdate, "dd-MMM-yyyy")));
                CendDate = DateUtils.formatStdString(DateUtils.formatStdDate(DateUtils.convertStringtoDate(EndDate, "dd-MMM-yyyy")));
                long noofDays = (CendDate.getTime() - CstartDate.getTime()) / (24 * 60 * 60 * 1000);
                TblCmsDateConfig tblCmsDateConfig = new TblCmsDateConfig();
                tblCmsDateConfig.setTblTenderMaster(new TblTenderMaster(Integer.parseInt(tenderId)));
                tblCmsDateConfig.setAppPkgLotId(Integer.parseInt(lotId));
                tblCmsDateConfig.setActualStartDt(new java.sql.Date(CstartDate.getTime()));
                tblCmsDateConfig.setActualEndDate(new java.sql.Date(CendDate.getTime()));
                tblCmsDateConfig.setNoOfDays((int) noofDays);
                tblCmsDateConfig.setCreatedBy(Integer.parseInt(logUserId));
                tblCmsDateConfig.setCreatedDate(new java.sql.Date(new java.util.Date().getTime()));
                tblCmsDateConfig.setUserTypeId(Integer.parseInt(userTypeId));
                tblCmsDateConfig.setTblContractSign(new TblContractSign(Integer.parseInt(ContractSignId)));
                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+request.getParameter("submitbtn"));
                if ("Submit".equalsIgnoreCase(request.getParameter("submitbtn"))) {
                    try {
                        Operation = "has Configured";
                        if ("works".equalsIgnoreCase(procnature)) {
                            mailSubject = "e-GP: PE has Configured Contract Commencement Date and Contract End Date";
                        } else if("goods".equalsIgnoreCase(procnature)){
                            mailSubject = "e-GP: PE has Configured Contract Commencement Date and Delivery Date";
                        }else{
                            mailSubject = "e-GP: PE has Configured Contract Commencement Date and Completion Date";
                        }
                        flag = cmsConfigDateService.addConfigDates(tblCmsDateConfig);
                        if (flag) {
                            TblCmsDateConfigHistory tblCmsDateConfigHistory = new TblCmsDateConfigHistory();
                            tblCmsDateConfigHistory.setTblCmsDateConfig(new TblCmsDateConfig(tblCmsDateConfig.getActContractDtId()));
                            tblCmsDateConfigHistory.setActualStartDate(tblCmsDateConfig.getActualStartDt());
                            tblCmsDateConfigHistory.setActualEndDate(tblCmsDateConfig.getActualEndDate());
                            tblCmsDateConfigHistory.setNoofDays(tblCmsDateConfig.getNoOfDays());
                            tblCmsDateConfigHistory.setCreatedDate(tblCmsDateConfig.getCreatedDate());
                            flag = cmsConfigDateService.addConfigDatesHistory(tblCmsDateConfigHistory);
                            if ("goods".equalsIgnoreCase(procnature)) {
                                // ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                service.changeEndDate(CstartDate, Integer.parseInt(lotId));
                            }
                            sendMail(tenderId, lotId, logUserId, Operation, mailSubject);
                        }
                        if (flag) {
                            //response.sendRedirect("officer/ConfigureDates.jsp?lotId="+lotId+"&contractSignId="+ContractSignId+"&tenderId="+tenderId+"&msg=succ");
                            response.sendRedirect("officer/ConfigureDatesUploadDoc.jsp?lotId=" + lotId + "&contractSignId=" + ContractSignId + "&tenderId=" + tenderId + "&keyId=" + tblCmsDateConfig.getActContractDtId() + "&docx=cd&msg=succ");
                        } else {
                            response.sendRedirect("officer/ConfigureDates.jsp?lotId=" + lotId + "&contractSignId=" + ContractSignId + "&tenderId=" + tenderId + "&msg=fail");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), ContractId, "ContractId", EgpModule.Commencement_Date.getName(), "Configure Date", "");
                    }
                } else {
                    try {
                        Operation = "has edited";
                        if ("works".equalsIgnoreCase(procnature)) {
                            mailSubject = "e-GP: PE has Edited Contract Commencement Date and Contract End Date";
                        } else if("goods".equalsIgnoreCase(procnature)){
                            mailSubject = "e-GP: PE has Edited Contract Commencement Date and Delivery Date";
                        }else{
                            mailSubject = "e-GP: PE has Edited Contract Commencement Date and Completion Date";
                        }
                        String actContractDtId = "";
                        if (request.getParameter("actContractId") != null) {
                            actContractDtId = request.getParameter("actContractId");
                        }
                        tblCmsDateConfig.setActContractDtId(Integer.parseInt(actContractDtId));
                        flag = cmsConfigDateService.updateConfigDates(tblCmsDateConfig);
                        if (flag) {
                            TblCmsDateConfigHistory tblCmsDateConfigHistory = new TblCmsDateConfigHistory();
                            tblCmsDateConfigHistory.setTblCmsDateConfig(new TblCmsDateConfig(tblCmsDateConfig.getActContractDtId()));
                            tblCmsDateConfigHistory.setActualStartDate(tblCmsDateConfig.getActualStartDt());
                            tblCmsDateConfigHistory.setActualEndDate(tblCmsDateConfig.getActualEndDate());
                            tblCmsDateConfigHistory.setNoofDays(tblCmsDateConfig.getNoOfDays());
                            tblCmsDateConfigHistory.setCreatedDate(tblCmsDateConfig.getCreatedDate());
                            flag = cmsConfigDateService.addConfigDatesHistory(tblCmsDateConfigHistory);
                            if ("goods".equalsIgnoreCase(procnature)) {
                                // ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                service.changeEndDate(CstartDate, Integer.parseInt(lotId));
                            }
                            sendMail(tenderId, lotId, logUserId, Operation, mailSubject);
                        }
                        if (flag) {
                            //response.sendRedirect("officer/ConfigureDates.jsp?lotId="+lotId+"&contractSignId="+ContractSignId+"&tenderId="+tenderId+"&msg=update");
                            response.sendRedirect("officer/ConfigureDatesUploadDoc.jsp?lotId=" + lotId + "&contractSignId=" + ContractSignId + "&tenderId=" + tenderId + "&keyId=" + tblCmsDateConfig.getActContractDtId() + "&docx=cd&msg=update");
                        } else {
                            response.sendRedirect("officer/ConfigureDates.jsp?lotId=" + lotId + "&contractSignId=" + ContractSignId + "&tenderId=" + tenderId + "&msg=updatefail");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), ContractId, "ContractId", EgpModule.Commencement_Date.getName(), "Edit Commencement Date", "");
                    }
                }
            } else if ("validateConStartdate".equalsIgnoreCase(action)) {
                try
                {
                String ContractStartDate = request.getParameter("param1");
                String ContractSigningDate = request.getParameter("param2");
                String str = validateConStartDate(ContractStartDate, ContractSigningDate);
                out.print(str);
                } catch(Exception e)
                {
                    e.printStackTrace();
                }
            } else if ("validateConEnddate".equalsIgnoreCase(action)) {
                try {
                String ContractEndDate = request.getParameter("param1");
                String ContractStartDate = request.getParameter("param2");
                String EditedEndDate = request.getParameter("param3");
                String str = validateEndDate(ContractEndDate, ContractStartDate, EditedEndDate);
                out.print(str);
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            LOGGER.debug("processRequest : " + logUserId + " Ends");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String validateConStartDate(String strContractStartDate, String strContractSigningDate) {
        String str = "";
        LOGGER.error("validateConStartDate : Starts");
        try {
            Date ContractStartDate = DateUtils.formatStdString(DateUtils.formatStdDate(DateUtils.convertStringtoDate(strContractStartDate, "dd-MMM-yyyy")));
            Date ContractSigningDate = DateUtils.convertStringtoDate(strContractSigningDate, "yyyy-MM-dd HH:mm:ss");
            Date ConSignDate = DateUtils.formatStdString(DateUtils.formatStdDate(ContractSigningDate));
            if (ContractStartDate.before(ContractSigningDate)) {
                if (!ContractStartDate.equals(ConSignDate)) {
                    str = "Contract Start Date must be greater than or equal to Contract Signing Date";
                }
            }
        } catch (Exception e) {
            LOGGER.error("validateConStartDate : " + e);
        }
        LOGGER.error("validateConSt artDate : Ends");
        return str;
    }

     /** validate Commencement End Date */
    private String validateEndDate(String strContractEndDate, String strContractStartDate, String EditedEndDate) {
        String str = "";
        LOGGER.error("validateEndDate : Starts");
        try {
            Date ContractEndDate = DateUtils.formatStdString(DateUtils.formatStdDate(DateUtils.convertStringtoDate(strContractEndDate, "dd-MMM-yyyy")));
            Date ContractStartDate = DateUtils.formatStdString(DateUtils.formatStdDate(DateUtils.convertStringtoDate(strContractStartDate, "dd-MMM-yyyy")));
            if (EditedEndDate != null && !"".equalsIgnoreCase(EditedEndDate)) {
                Date EditedEndDt = DateUtils.convertStringtoDate(EditedEndDate, "yyyy-MM-dd HH:mm:ss");
                if (ContractEndDate.before(EditedEndDt)) {
                    str = "Contract Delivery Date cannot be PrePond";
                }
            }
            if (ContractEndDate.before(ContractStartDate) || ContractEndDate.equals(ContractStartDate)) {
                str = "Contract End Date must be greater than Contract Start Date";
            }
        } catch (Exception e) {
            LOGGER.error("validateEndDate : " + e);
        }
        LOGGER.error("validateEndDate : Ends");
        return str;
    }

    /** send mail notification to tenderer on configuring the commencement date by PE */
    private boolean sendMail(String tenderId, String lotId, String logUserId, String Operation, String mailSubject) {
        boolean flag = false;
        String procnature = commonservice.getProcNature(tenderId).toString();
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peName = "";
            String strTo[] = {obj[9].toString()};
            String strFrom = commonservice.getEmailId(logUserId);
            List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(logUserId));
            if (peNameObj.get(0) != null) {
                peName = peNameObj.get(0).toString();
            }
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            registerService.contentAdmMsgBox(strTo[0], strFrom, mailSubject, msgBoxContentUtility.ConfiguredCommencementDt(Operation, c_isPhysicalPrComplete, peName, obj));
            String mailText = mailContentUtility.ConfiguredCommencementDt(Operation, c_isPhysicalPrComplete, peName, obj);
            sendMessageUtil.setEmailTo(strTo);
            sendMessageUtil.setEmailFrom(strFrom);
            sendMessageUtil.setEmailSub(mailSubject);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            sendMessageUtil.setSmsNo(accPaymentService.getMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
            StringBuilder sb = new StringBuilder();
            sb.append("Dear User,%0AThis is to inform you that PE "+Operation+" Commencement Date");
            sb.append("for Contract No."+obj[1].toString() +" ("+ obj[15].toString() +")"+"");
            sb.append("%0AThanks,%0AeGP System");
            sendMessageUtil.setSmsBody(sb.toString());
            sendMessageUtil.sendSMS();
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }
}
