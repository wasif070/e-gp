/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblEvalBidderMarks;
import com.cptu.egp.eps.model.table.TblEvalBidderStatus;
import com.cptu.egp.eps.model.table.TblEvalSerFormDetail;
import com.cptu.egp.eps.model.table.TblEvalServiceForms;
import com.cptu.egp.eps.model.table.TblEvalTerreport;
import com.cptu.egp.eps.model.table.TblEvalTerreportCriteria;
import com.cptu.egp.eps.model.table.TblTenderForms;
import com.cptu.egp.eps.service.serviceimpl.EvaluationService;
import com.cptu.egp.eps.service.serviceimpl.ReportCreationService;
import com.cptu.egp.eps.service.serviceinterface.EvalServiceCriteriaService;
import com.cptu.egp.eps.web.servicebean.EvalSerCertiSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *This servlet is handle request of evalCertiService.jsp file
 * @author Administrator
 */
public class ServletEvalCertiService extends HttpServlet {

    /** 
     * Sevlet used in EvalCertiService.jsp page update and insert and making of ajax call.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    EvalServiceCriteriaService evalServiceCriteriaService = (EvalServiceCriteriaService)AppContext.getSpringBean("EvalServiceCriteriaService");
    EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession objHS=request.getSession();
        // Coad added by Dipal for Audit Trail Log.
            
            AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), objHS.getAttribute("sessionId"), objHS.getAttribute("userTypeId"), request.getHeader("referer"));
            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            String idType = null;
            int auditId = 0;
            String auditAction =null;
            String moduleName = null;
            String remarks = null;
            
        evalServiceCriteriaService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
        try {
            int tenderId = 0;
            String action = request.getParameter("funName");
            String pageName = null;
            if (action.equalsIgnoreCase("criteriaCombo")) {
                PrintWriter out = response.getWriter();
                String count = request.getParameter("count");
                int crtiId = Integer.parseInt(request.getParameter("objectId"));
                String isReoi = request.getParameter("isREOI");
                String msg = "";
                String eleName = request.getParameter("eleName");
                
                msg = getSubCriteria(crtiId, count, eleName,isReoi);
                out.print(msg);
                out.flush();
                out.close();
            }else if (action.equalsIgnoreCase("add") || action.equalsIgnoreCase("update")) {
                tenderId = Integer.parseInt(request.getParameter("tenderId"));
                boolean flag = false;
                EvalSerCertiSrBean evalSerCertiSrBean = new EvalSerCertiSrBean();
                if (action.equalsIgnoreCase("add")) {
                    List<TblEvalServiceForms> list = insertData(request);
                    if (list != null) {
                        flag = evalSerCertiSrBean.insertTable(list);
                    }
                }
                if (action.equalsIgnoreCase("update")) {
                    flag = evalSerCertiSrBean.deleteRecord(tenderId,request.getParameter("isCorri"));
                    if (flag) {
                        List<TblEvalServiceForms> list = insertData(request);
                        if (list != null) {
                            flag = evalSerCertiSrBean.insertTable(list);
                        }
                    }
                }
                if (flag) {
                    pageName = "officer/EvalCertiServiceView.jsp?tenderId=" + tenderId + "&flag=" + action+("yes".equalsIgnoreCase(request.getParameter("getcorri")) ? "&iscorri=true" : "");
                    //response.sendRedirectFilter("EvalCertiServiceView.jsp?tenderId=" + tenderId + "&flag=" + action);
                } else {
                    pageName = "officer/EvalCertiService.jsp?tenderId=" + tenderId + "&flag=" + action+("yes".equalsIgnoreCase(request.getParameter("getcorri")) ? "&iscorri=true" : "");
                    //response.sendRedirectFilter("EvalCertiService.jsp?tenderId=" + tenderId + "&flag=" + action);
                }
            }else if (action.equalsIgnoreCase("evalBidderMarkInsert")) { // insert in to TblEvalSerFormDetail
                String operation = request.getParameter("action");
                if(operation.equalsIgnoreCase("true")){
                    if(deleteAndInsertEvalSerFormDetail(request)){
                        pageName = "officer/SeekEvalClari.jsp?uId=" + request.getParameter("userId") + "&tenderId=" + request.getParameter("tenderId") + "&st=cl&lnk=et";
                        //response.sendRedirectFilter("SeekEvalClari.jsp?uId=" + request.getParameter("userId") + "&tenderId=" + request.getParameter("tenderId") + "&st=cl&lnk=et");
                    }else{
                        pageName = "officer/EvalServiceMarking.jsp?uId=" + request.getParameter("userId") + "&formId=" + request.getParameter("tenderFormId") + "&tenderId=" + request.getParameter("tenderId") + "&msg=failUpdate";
                        //response.sendRedirectFilter("EvalServiceMarking.jsp?uId=" + request.getParameter("userId") + "&formId=" + request.getParameter("tenderFormId") + "&tenderId=" + request.getParameter("tenderId") + "&msg=failUpdate");
                    }
                }else {
                    if(insertEvalSerFormDetail(request)){
                        pageName = "officer/SeekEvalClari.jsp?uId=" + request.getParameter("userId") + "&tenderId=" + request.getParameter("tenderId") + "&st=cl&lnk=et";
                        //response.sendRedirectFilter("SeekEvalClari.jsp?uId=" + request.getParameter("userId") + "&tenderId=" + request.getParameter("tenderId") + "&st=cl&lnk=et");
                    }else{
                        pageName = "officer/EvalServiceMarking.jsp?uId=" + request.getParameter("userId") + "&formId=" + request.getParameter("tenderFormId") + "&tenderId=" + request.getParameter("tenderId") + "&msg=failCreate";
                        //response.sendRedirectFilter("EvalServiceMarking.jsp?uId=" + request.getParameter("userId") + "&formId=" + request.getParameter("tenderFormId") + "&tenderId=" + request.getParameter("tenderId") + "&msg=failCreate");
                    }
                }
            }else if("dropComMember".equals(action))
            {
                if(!dropComMember(request)){
                    System.out.println("Error deleting Committee Member");
                }
                pageName = "officer/EvalComMemberReport.jsp?tenderId="+request.getParameter("tId")+"&uId="+request.getParameter("bId");
                //response.sendRedirectFilter("EvalComMemberReport.jsp?tenderId="+request.getParameter("tId")+"&uId="+request.getParameter("bId"));
            }
            else if("insPassMarks".equals(action))
            {
                String tId = request.getParameter("tId");
                String uId = request.getParameter("uId");
                if(insPassMarks(request)){
                    pageName = "officer/EvalComm.jsp?tenderid="+tId;
                    //Evalclarify.jsp?tenderId="+tId+"&st=rp");
                }else{
                    pageName = "officer/EvalComMemberReport.jsp?msg=error&tenderId="+tId+"&uId="+uId;
                    //EvalComMemberReport.jsp?msg=error&tenderId="+tId+"&uId="+uId);
                }
            }
            else if("evalTERReportIns".equals(action))
            {
                idType = "tenderId";
                auditId = Integer.parseInt(request.getParameter("tId"));
                String role=request.getParameter("role");
                if(role != null && role.equalsIgnoreCase("cp"))
                {
                    auditAction ="Configure "+request.getParameter("rep").toUpperCase()+" by TEC CP";
                }
                else if (role != null && role.equalsIgnoreCase("m"))
                {
                    auditAction ="Fill evaluation form of "+request.getParameter("rep").toUpperCase()+" by TEC Member";
                }
                else
                {
                    auditAction ="Fill evaluation form of "+request.getParameter("rep").toUpperCase();
                }
                
                moduleName = EgpModule.Evaluation.getName();
                remarks = "User Id:"+objHS.getAttribute("userId")+" has filled Evaluation Form for "+request.getParameter("rep").toUpperCase();
                
                if(evalTERReportIns(request))
                {
                    
                    makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                    pageName = "officer/Evalclarify.jsp?tenderId="+request.getParameter("tId")+"&st=rp&comType=TEC&rep="+request.getParameter("rep");
                    //response.sendRedirect(request.getContextPath()+"/report/TER1.jsp?msg=pass&tenderid="+request.getParameter("tId")+"&lotId="+request.getParameter("lId")+"&stat="+request.getParameter("stat"));
                }
                else
                {
                    auditAction="Error in "+auditAction;
                    makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                    pageName = request.getHeader("referer")+"&msg=fail";
                    //response.sendRedirect(request.getContextPath()+"/report/TER1.jsp?msg=fail&tenderid="+request.getParameter("tId")+"&lotId="+request.getParameter("lId")+"&stat="+request.getParameter("stat"));
                }
            }

            if(pageName!=null){
                response.sendRedirect(pageName);
            }
        }catch(Exception e ){
            System.out.println("Exception in "+getClass().getName()+" : "+e);
            e.printStackTrace();
        }
    }
    //For Ajax request

    private String getSubCriteria(int crtiId, String count, String eleName,String isReoi) {
        StringBuilder str = new StringBuilder();
        EvalSerCertiSrBean evalSerCertiSrBean = new EvalSerCertiSrBean();
        List<Object[]> obj = null;
        

        //if(obj!=null && obj.size()>1){
        //str += "";
        str.append("<table width='100%' cellspacing='0' class='tableList_1'>");
        str.append("<tr>");
        str.append("<th width='80%' class='t-align-left ff'>Sub Criteria</th>");
        if("true".equalsIgnoreCase(isReoi)){
            str.append("<th width='20%'  class='t-align-left ff'>Select</th>");
        }else{
                str.append("<th width='20%'  class='t-align-left ff'>Max Points</th>");
        }
        str.append("</tr>");
        int i = 0;
        //if (crtiId == 0) {
            obj = evalSerCertiSrBean.getSubCriteria(crtiId);
            for (i = 0; i < obj.size(); i++) {
                str.append("<tr>");
                str.append("<td class='t-align-left ff'>");
                str.append(obj.get(i)[1] + "<input type = 'hidden' value='" + obj.get(i)[0] + "' name='hdnSubCri_" + count + "' />");
                str.append("</td>");
                str.append("<td  class='t-align-left ff'>");
                if("true".equalsIgnoreCase(isReoi)){
                    str.append("<input type='checkbox' value='1' name='subCriMark_" + count + "' id='subCriMark_" + i + "_" + count + "'  /><br />");
                }else{
                    str.append("<input type='text' value='' name='subCriMark_" + count + "' id='subCriMark_" + i + "_" + count + "' style='width: 35px' onblur='return totalMark(this);'/><br />");
                }
                str.append("</td>");
                str.append("</tr>");
            }
        //} else {
//            str.append("<tr>");
//            str.append("<td class='t-align-left ff'>");
//            str.append(eleName + "<input type = 'hidden' value='" + crtiId + "' name='hdnSubCri_" + count + "'>");
//            str.append("</td>");
//            str.append("<td class='t-align-left ff'>");
//            str.append("<input type='text' name='subCriMark_" + count + "' id='subCriMark_" + i + "_" + count + "' style='width: 25px' onblur='return totalMark(this);' /><br />");
//            str.append("</td>");
//            str.append("</tr>");
        //}
        str.append("<input type = 'hidden' value='" + i + "' id='totalCount_" + count + "'>");
        str.append("</table>");
        return str.toString();
        //}else{
        // return "Problem";
        // }
    }
    //For Inserting to TblEvalServiceForms
    private List<TblEvalServiceForms> insertData(HttpServletRequest request) {
        List<TblEvalServiceForms> list = new ArrayList<TblEvalServiceForms>();
        if (request.getParameter("counter") != null) {
            int counter = Integer.parseInt(request.getParameter("counter"));
            String[] mark = null;
            String[] id = null;
            String form = null;
            String isCorri = request.getParameter("isCorri"); 
            //int k = 0;
            int tenderId = Integer.parseInt(request.getParameter("tenderId"));
            int tenderLotId = Integer.parseInt(request.getParameter("pkgLotId"));
            HttpSession session = request.getSession();
            int userId = Integer.parseInt(session.getAttribute("userId").toString());
            for (int i = 1; i <= counter; i++) {
                mark = request.getParameterValues("subCriMark_" + i);
                id = request.getParameterValues("hdnSubCri_" + i);
                form = request.getParameter("fromId_" + i);

                if(mark != null && id != null){    
                    for (int j = 0; j < mark.length; j++) {
                        TblEvalServiceForms tblEvalServiceForms = new TblEvalServiceForms();
                        tblEvalServiceForms.setTenderId(tenderId);
                        tblEvalServiceForms.setTenderFormId(Integer.parseInt(form));
                        tblEvalServiceForms.setMaxMarks(Integer.parseInt(mark[j]));
                        tblEvalServiceForms.setSubCrietId(Integer.parseInt(id[j]));
                        tblEvalServiceForms.setConfigBy(userId);
                        tblEvalServiceForms.setPkgLotId(tenderLotId);
                        tblEvalServiceForms.setConfigDt(new Date());
                        tblEvalServiceForms.setIsCurrent(isCorri);
                        list.add(tblEvalServiceForms);
                        //j++;
                    }
                }
                /*else{
                        TblEvalServiceForms tblEvalServiceForms = new TblEvalServiceForms();
                        tblEvalServiceForms.setTenderId(tenderId);
                        tblEvalServiceForms.setTenderFormId(Integer.parseInt(form));
                        tblEvalServiceForms.setMaxMarks(0);
                        tblEvalServiceForms.setSubCrietId(0);
                        tblEvalServiceForms.setConfigBy(userId);
                        tblEvalServiceForms.setPkgLotId(tenderLotId);
                        tblEvalServiceForms.setConfigDt(new Date());
                        list.add(tblEvalServiceForms);
                }*/
                //k++;

            }
            return list;
        } else {
            return null;
        }
    }
    /**
     * insert in to TblEvalSerFormDetail
     * @param request to be inserted
     * @return true if data inserted successfully else false
     */
    private boolean insertEvalSerFormDetail(HttpServletRequest request) {
        EvalServiceCriteriaService evalServiceCriteriaService = (EvalServiceCriteriaService)AppContext.getSpringBean("EvalServiceCriteriaService");
        List<TblEvalSerFormDetail> list = new ArrayList<TblEvalSerFormDetail>();
        int count = Integer.parseInt(request.getParameter("count"));
        boolean isREOI = Boolean.parseBoolean(request.getParameter("eventType"));
        int evalBy = Integer.parseInt(request.getSession().getAttribute("userId").toString());
        for (int i = 0; i < count; i++) {
            String ratedScore[] = request.getParameterValues("ratedScore_" + i);
            String maxMarks[] = request.getParameterValues("maxMarks_" + i);
            String actualMarks[] = request.getParameterValues("actualMarks_" + i);
            String ratingWeightage[] = request.getParameterValues("ratingWeightage_" + i);
            String bidderId = request.getParameter("bidderId_" + i);
            String subCriteriaId[] = request.getParameterValues("subCriteriaId_" + i);
            String remarks[] = null;
            if(request.getParameterValues("remarks_"+i) != null){
                remarks = request.getParameterValues("remarks_" + i);
            }
            for (int j = 0; j < ratedScore.length; j++) {
                TblEvalSerFormDetail tblEvalSerFormDetail = new TblEvalSerFormDetail();
                tblEvalSerFormDetail.setTblTenderForms(new TblTenderForms(Integer.parseInt(request.getParameter("tenderFormId"))));
                tblEvalSerFormDetail.setUserId(Integer.parseInt(request.getParameter("userId")));
                tblEvalSerFormDetail.setMaxMarks(isREOI?"0":maxMarks[j]);
                tblEvalSerFormDetail.setActualMarks(isREOI?"0": new BigDecimal(actualMarks[j]).setScale(2, 0).toString());
                tblEvalSerFormDetail.setEvalBy(evalBy);
                tblEvalSerFormDetail.setEvalDate(new Date());
                tblEvalSerFormDetail.setEvalStatus("evaluation");
                tblEvalSerFormDetail.setEvalStatusDt(null);
                tblEvalSerFormDetail.setRatingWeightage(ratingWeightage[j]);
                tblEvalSerFormDetail.setBidId(Integer.parseInt(bidderId));
                tblEvalSerFormDetail.setRatedScore(isREOI?"0.0":ratedScore[j]);
                tblEvalSerFormDetail.setSubCriteriaId(Integer.parseInt(subCriteriaId[j]));
                tblEvalSerFormDetail.setTenderId(Integer.parseInt(request.getParameter("tenderId")));
                tblEvalSerFormDetail.setIsMemberDropped("no");
                if(remarks != null){
                    tblEvalSerFormDetail.setRemarks(remarks[j]);
                }else{
                    tblEvalSerFormDetail.setRemarks("");
                }
                tblEvalSerFormDetail.setEvalCount(Integer.parseInt(request.getParameter("evalCount"))); //Change by dohatec for re-evaluation
                list.add(tblEvalSerFormDetail);
            }
        }
        return evalServiceCriteriaService.insertTblEvalSerFormDetail(list);
    }

    /**
     * delete data from tbl_EvalSerFormDetail and insert in to TblEvalSerFormDetail
     * @param request
     * @return true if operation done successfully else false
     */private boolean  deleteAndInsertEvalSerFormDetail(HttpServletRequest request) {

        
        int flagMarks = Integer.parseInt(request.getParameter("flagMarks"));
        boolean isREOI = Boolean.parseBoolean(request.getParameter("eventType"));
        StringBuilder whereContion = new StringBuilder();
        for (int i = 0; i < flagMarks; i++) {
            whereContion.append(request.getParameter("esFormDetailId_" + i)).append(",");
        }
        evalServiceCriteriaService.insertDeleteTblEvalSerFormDetail(whereContion.substring(0, whereContion.length()-1));
        List<TblEvalSerFormDetail> list = new ArrayList<TblEvalSerFormDetail>();
        int count = Integer.parseInt(request.getParameter("count"));
        int evalBy = Integer.parseInt(request.getSession().getAttribute("userId").toString());
        for (int i = 0; i < count; i++) {
            String ratedScore[] = request.getParameterValues("ratedScore_" + i);
            String maxMarks[] = request.getParameterValues("maxMarks_" + i);
            String actualMarks[] = request.getParameterValues("actualMarks_" + i);
            String ratingWeightage[] = request.getParameterValues("ratingWeightage_" + i);
            String bidderId = request.getParameter("bidderId_" + i);
            String subCriteriaId[] = request.getParameterValues("subCriteriaId_" + i);
            String remarks[] = null;
            if(request.getParameterValues("remarks_"+i) != null){
                remarks = request.getParameterValues("remarks_" + i);
            }
            for (int j = 0; j < ratedScore.length; j++) {
                TblEvalSerFormDetail tblEvalSerFormDetail = new TblEvalSerFormDetail();
                tblEvalSerFormDetail.setTblTenderForms(new TblTenderForms(Integer.parseInt(request.getParameter("tenderFormId"))));
                tblEvalSerFormDetail.setUserId(Integer.parseInt(request.getParameter("userId")));
                tblEvalSerFormDetail.setMaxMarks(isREOI?"0":maxMarks[j]);
                tblEvalSerFormDetail.setActualMarks(isREOI?"0":new BigDecimal(actualMarks[j]).setScale(2, 0).toString());
                tblEvalSerFormDetail.setEvalBy(evalBy);
                tblEvalSerFormDetail.setEvalDate(new Date());
                tblEvalSerFormDetail.setEvalStatus("evaluation");
                tblEvalSerFormDetail.setEvalStatusDt(null);
                tblEvalSerFormDetail.setRatingWeightage(ratingWeightage[j]);
                tblEvalSerFormDetail.setBidId(Integer.parseInt(bidderId));
                tblEvalSerFormDetail.setRatedScore(isREOI?"0.0":ratedScore[j]);
                tblEvalSerFormDetail.setSubCriteriaId(Integer.parseInt(subCriteriaId[j]));
                tblEvalSerFormDetail.setTenderId(Integer.parseInt(request.getParameter("tenderId")));
                tblEvalSerFormDetail.setIsMemberDropped("no");
                tblEvalSerFormDetail.setEvalCount(Integer.parseInt(request.getParameter("evalCount"))); //Change by dohatec for re-evaluation
                if(remarks != null){
                    tblEvalSerFormDetail.setRemarks(remarks[j]);
                }else{
                    tblEvalSerFormDetail.setRemarks("");
                }
                list.add(tblEvalSerFormDetail);
            }
        }
        return evalServiceCriteriaService.insertTblEvalSerFormDetail(list);
    }

    private boolean dropComMember(HttpServletRequest request){
        EvalServiceCriteriaService evalServiceCriteriaService = (EvalServiceCriteriaService)AppContext.getSpringBean("EvalServiceCriteriaService");
        return evalServiceCriteriaService.dropMemberFrmReport(request.getParameter("tId"), request.getParameter("mId"), request.getParameter("bId"), request.getParameter("dropMem"));
    }

    private boolean insPassMarks(HttpServletRequest request){
        EvalServiceCriteriaService criteria = (EvalServiceCriteriaService)AppContext.getSpringBean("EvalServiceCriteriaService");
        TblEvalBidderStatus bidderStatus = new TblEvalBidderStatus();
        bidderStatus.setEvalBy(Integer.parseInt(request.getParameter("mId")));
        bidderStatus.setUserId(Integer.parseInt(request.getParameter("uId")));
        bidderStatus.setTenderId(Integer.parseInt(request.getParameter("tId")));
        bidderStatus.setBidderStatus("evaluated");
        bidderStatus.setEvalDt(new Date());
        bidderStatus.setBidderMarks(new BigDecimal((request.getParameter("bmarks")!=null ? request.getParameter("bmarks") : "0.00")).setScale(2,0).toString());
        bidderStatus.setPassingMarks(request.getParameter("pmarks"));
        bidderStatus.setRemarks(request.getParameter("comments"));
        bidderStatus.setResult(request.getParameter("result"));
        bidderStatus.setEvalStatus(request.getParameter("stat"));
        bidderStatus.setPkgLotId(0);
        bidderStatus.setEvalCount(Integer.parseInt(request.getParameter("evalCount"))); //Change by dohatec for re-evaluation
        List<TblEvalBidderMarks> list = new ArrayList<TblEvalBidderMarks>();
        int counter=0;
        if(request.getParameter("counter")!=null){
            counter = Integer.parseInt(request.getParameter("counter"));
    }
        for (int i=0;i<counter;i++) {
            for (int j=0;j<request.getParameterValues("avgMarks_"+i).length;j++) {
//                System.out.println(request.getParameter("uId"));
//                System.out.println(request.getParameterValues("avgMarks_"+i)[j]);
//                System.out.println(request.getParameter("tId") );
//                System.out.println(request.getParameter("mId"));
//                System.out.println(request.getParameter("formId_"+i));
//                System.out.println(request.getParameterValues("criteriaId_"+i)[j]);
                list.add(new TblEvalBidderMarks(0,  Integer.parseInt(request.getParameter("uId")), request.getParameterValues("avgMarks_"+i)[j],Integer.parseInt(request.getParameter("tId")) ,Integer.parseInt(request.getParameter("mId")),Integer.parseInt(request.getParameter("formId_"+i)),Integer.parseInt(request.getParameterValues("criteriaId_"+i)[j]),Integer.parseInt(request.getParameter("evalCount")))); //Change by dohatec for re-evaluation
            }
        }
        return  criteria.insertEvalBidderStatus(bidderStatus,list);
    }

    private boolean evalTERReportIns(HttpServletRequest request){
        ReportCreationService creationService = (ReportCreationService)AppContext.getSpringBean("ReportCreationService");
        List<TblEvalTerreport> list = new ArrayList<TblEvalTerreport>();
        int counter=0;
        int createdBy = Integer.parseInt(request.getSession().getAttribute("userId").toString());
        if(request.getParameterValues("critValue")!=null){
            counter = request.getParameterValues("critValue").length;
        }
     //   int evalCount = evalService.getEvaluationNo(Integer.parseInt(request.getParameter("tId").toString()));
        int evalCount = evalService.getEvaluationNo(Integer.parseInt(request.getParameter("tId").toString()));
        for(int i=0;i<counter;i++){
            int userId;
            int critId;
            String value;
            String[] array = request.getParameterValues("critValue")[i].split("_");
            value = array[0];
            userId = Integer.parseInt(array[1]);
            critId = Integer.parseInt(array[2]);
            list.add(new TblEvalTerreport(0, new TblEvalTerreportCriteria(critId),Integer.parseInt(request.getParameter("tId")), userId, value, Integer.parseInt(request.getParameter("lId")), request.getParameter("stat"), request.getParameter("rType"),createdBy,new Date(),request.getParameter("role"),Integer.parseInt(request.getParameter("rId")),evalCount));
        }        
        return creationService.evalTERReportIns(list);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
