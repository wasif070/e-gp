/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.service.serviceimpl.AskProcurementImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.DateUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class AskProcurementServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final Logger LOGGER = Logger.getLogger(AskProcurementServlet.class);
    private String logUserId ="0";
    private final AskProcurementImpl askProcurementImpl = (AskProcurementImpl) AppContext.getSpringBean("AskProcurementImpl");

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
            askProcurementImpl.setLogUserId(logUserId);
        }
        LOGGER.debug("processRequest : "+logUserId+" Starts");
        try {
            if (request.getParameter("action").equals("fetchData")) {

                LOGGER.debug("processRequest : fetchdata : "+logUserId+" Starts");

                response.setContentType("text/xml;charset=UTF-8");
                
                //String status = request.getParameter("status");
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));

                int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));
                int totalPages = 0;
                int totalCount = 0;
                List<Object[]> data;
                String status = "";
                if (request.getParameter("replied") != null) {
                    status = request.getParameter("replied");
                }
                String not = "";
                if (status.equalsIgnoreCase("yes")) {
                    not = "not";
                } else {
                    not = "";
                }
                String searchField = "", searchString = "", searchOper = "";
                if (_search) {
                    searchField = request.getParameter("searchField");
                    searchString = request.getParameter("searchString");
                    searchOper = request.getParameter("searchOper");
                    String sord = request.getParameter("sord");
                    String sidx = request.getParameter("sidx");
                    String orderClause = null;
                     String order = null;
                     if(sidx.equals("")){
                         orderClause = "order by category";
                         order = "asc";
                     } else {
                        orderClause = "order by "+sord;
                        order = sidx;
                     }
                    totalCount = (int) askProcurementImpl.countListAskProcurement(searchField, searchString, searchOper, not);
                    data = askProcurementImpl.getListAskProcurement(searchField, searchString, searchOper, offset, Integer.parseInt(rows), not,orderClause,order);
                } else {
                    totalCount = (int) askProcurementImpl.countListAskPro(not);
                    if (request.getParameter("replied") == null) {
                        data = askProcurementImpl.getListAskPro(offset, Integer.parseInt(rows), not);

                    }else{
                        data = askProcurementImpl.getListAskProOrder(offset, Integer.parseInt(rows), not);
                    }
                }
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                        totalPages = totalCount / Integer.parseInt(request.getParameter("rows"));
                    } else {
                        totalPages = (totalCount / Integer.parseInt(request.getParameter("rows"))) + 1;
                    }

                } else {
                    totalPages = 0;
                }
                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + totalCount + "</records>");
                int j = 0;
                int no = Integer.parseInt(request.getParameter("page"));
                if (no == 1) {
                    j = 1;
                } else {
                    if (Integer.parseInt(rows) == 30) {
                        j = ((no - 1) * 30) + 1;
                    } else if (Integer.parseInt(rows) == 20) {
                        j = ((no - 1) * 20) + 1;
                    } else {
                        j = ((no - 1) * 10) + 1;
                    }
                }
                String replied = "";
                if(request.getParameter("replied") != null){
                    replied = request.getParameter("replied");
                }
                int rowId = 0;
                for (Object[] obj : data) {
                    out.print("<row id='" + rowId + "'>");
                    out.print("<cell><![CDATA[" + j + "]]></cell>");
                    out.print("<cell><![CDATA[" + obj[1] + "]]></cell>");
                    out.print("<cell><![CDATA[" + obj[3] + "]]></cell>");                    
                    out.print("<cell><![CDATA[" + obj[9] + "]]></cell>");
                    out.print("<cell><![CDATA[" + DateUtils.gridDateToStrWithoutSec((Date) obj[4]) + "]]></cell>");
                    if (status.equalsIgnoreCase("yes")) {
                        if (obj[7] == null) {
                            out.print("<cell><![CDATA[ - ]]></cell>");

                        } else {
                            out.print("<cell><![CDATA[" + DateUtils.gridDateToStrWithoutSec((Date) obj[7]) + "]]></cell>");
                        }
                    }
                    if(replied.equalsIgnoreCase("yes")){
                        out.print("<cell><![CDATA[<a href=\"QuestionWindow.jsp?action=view&procQueId=" + obj[2] + "\">View</a>]]></cell>");//<a href=\"QuestionWindow.jsp?action=create&procQueId=" + obj[2] + "\">Reply</a> | - remove
                    }else{
                    out.print("<cell><![CDATA[<a href=\"QuestionWindow.jsp?action=create&procQueId=" + obj[2] + "\">Reply</a> | <a href=\"QuestionWindow.jsp?action=view&procQueId=" + obj[2] + "\">View</a> | <a href=\""+request.getContextPath()+"/AskProcurementServlet?action=deleteAPE&procQueId=" + obj[2] + "\"  onclick=\"return confirm('Are you sure you want to delete?');\">Delete</a>]]></cell>");
                    }
                    out.print("</row>");
                    j++;
                    rowId++;
                }
                //No Record Found Code
//                if (data.isEmpty()) {
//                    out.print("<row id='" + rowId + "'>");
//                    out.print("<cell><![CDATA[No data found]]></cell>");
//                    out.print("<cell><![CDATA[No data found]]></cell>");
//                    out.print("<cell><![CDATA[No data found]]></cell>");
//                    out.print("<cell><![CDATA[No data found]]></cell>");
//                    out.print("<cell><![CDATA[No data found]]></cell>");
//                    out.print("<cell><![CDATA[No data found]]></cell>");
//                    if ("".equals(not)) {
//                        out.print("<cell><![CDATA[No data found]]></cell>");
//                    }
//                    out.print("<cell><![CDATA[No data found]]></cell>");
//                    out.print("<cell><![CDATA[No data found]]></cell>");
//                    out.print("</row>");
//                }
                out.print("</rows>");

                LOGGER.debug("processRequest : fetchdata : "+logUserId+" Ends");

            } else if (request.getParameter("action").equals("fetchDataTender")) {

                LOGGER.debug("processRequest : fetchDataTender : "+logUserId+" Starts");

                response.setContentType("text/xml;charset=UTF-8");
                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                int userId = 0;
                
                //String status = request.getParameter("status");
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");

                int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));
                int totalPages = 0;
                int totalCount = 0;
                List<Object[]> data = null;
                String searchField = "", searchString = "", searchOper = "";
                if (_search) {
                    searchField = request.getParameter("searchField");
                    searchString = request.getParameter("searchString");
                    searchOper = request.getParameter("searchOper");
                    totalCount = (int) askProcurementImpl.countListAskProcurement(userId,searchField, searchString, searchOper);
                    data = askProcurementImpl.getListAskProcurementTender(userId, offset, Integer.parseInt(request.getParameter("rows")),searchField, searchString, searchOper);
                } else {
                    totalCount = (int) askProcurementImpl.countListAskProcurement(userId);
                    data = askProcurementImpl.getListAskProcurementTender(userId, offset, Integer.parseInt(request.getParameter("rows")));
                }
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                        totalPages = totalCount / Integer.parseInt(request.getParameter("rows"));
                    } else {
                        totalPages = (totalCount / Integer.parseInt(request.getParameter("rows"))) + 1;
                    }

                } else {
                    totalPages = 0;
                }
                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + totalCount + "</records>");
                int j = 0;
                int no = Integer.parseInt(request.getParameter("page"));
                if (no == 1) {
                    j = 1;
                } else {
                    if (Integer.parseInt(rows) == 30) {
                        j = ((no - 1) * 30) + 1;
                    } else if (Integer.parseInt(rows) == 20) {
                        j = ((no - 1) * 20) + 1;
                    } else {
                        j = ((no - 1) * 10) + 1;
                    }
                }
                int rowId = 0;
                for (Object[] obj : data) {                    
                    out.print("<row id='" + rowId + "'>");
                    out.print("<cell><![CDATA[" + j + "]]></cell>");
                    out.print("<cell><![CDATA[" + obj[1] + "]]></cell>");
                    out.print("<cell><![CDATA[" + obj[3] + "]]></cell>");
                    String link;
                    String replied;
                    if (obj[8] == null) {
                        link = "Posted";
                        replied = "No";                        
                    } else {
                        link = "View";
                        replied = "Yes";
                    }
                    //out.print("<cell><![CDATA[" + replied + "]]></cell>");
                    out.print("<cell><![CDATA[<a href=\"ViewQuestion.jsp?procQueId=" + obj[2] + "\">" + link + "</a>]]></cell>");
                    out.print("</row>");
                    j++;
                    rowId++;
                }
                //No Record Found Code
                if (data.isEmpty()) {
                    out.print("<row id='" + rowId + "'>");
                    out.print("<cell><![CDATA[No data found]]></cell>");
                    out.print("<cell><![CDATA[No data found]]></cell>");
                    out.print("<cell><![CDATA[No data found]]></cell>");
                    //out.print("<cell><![CDATA[No data found]]></cell>");
                    out.print("<cell><![CDATA[No data found]]></cell>");
                    out.print("</row>");
                }
                out.print("</rows>");
            }else if(request.getParameter("action").equals("deleteAPE")){
                String procQueId = request.getParameter("procQueId");
                if(askProcurementImpl.deleteAPE(procQueId)){
                    response.sendRedirect(request.getHeader("referer")+"?action=delsucc");
                }else{
                    response.sendRedirect(request.getHeader("referer")+"?action=delfail");
                }
            }

            LOGGER.debug("processRequest : fetchDataTender : "+logUserId+" Ends");
        } catch (Exception e) {
            LOGGER.error("processRequest : "+logUserId+" : "+e);
        } finally {
            out.close();
        }
        LOGGER.debug("processRequest : "+logUserId+" Ends");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
