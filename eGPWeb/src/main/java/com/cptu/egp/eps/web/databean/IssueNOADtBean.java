/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author rishita
 */
public class IssueNOADtBean {

    private int tenderId = 0;
    private int noaAcceptId = 0;
    private int roundId = 0;
    private int noaIssueId = 0;
    private String perReq = "";
    private String acceptRejType = "";
    private String comments = "";
    private Date acceptRejDt = null;
    private String acceptRejDtString = "";
    private int hdUserId = 0;
    private int pkgLotId = 0;
    private String contractNo = "";
    private Date contractDt = null;
    private String contractDtString = "";
    private BigDecimal contractAmt = BigDecimal.ZERO;
    private String contractAmtString = "";
    private String contractAmtWords = "";
    private short noaIssueDays = (short) 0;
    private Date noaAcceptDt = null;
    private String noaAcceptDtString = "";
    private BigDecimal perfSecAmt = BigDecimal.ZERO;
    private String perfSecAmtString = "";
    private String perfSecAmtWords = "";
    private Short perSecSubDays = (short) 0;
    private String rank = "";
    private Date perSecSubDt = null;
    private String perSecSubDtString = "";
    private short contractSignDays = (short) 0;
    private Date contractSignDt = null;
    private String contractSignDtString;
    private int createdBy = 0;
    private Date createdDt = null;
    private String createdDtString = "";
    private String contractName = "";
    private String clauseRef = "";
    private String acceptRejStatus = "";
    private String companyName = "";
    private String amountCnt;
    private String salvageAmtStr;
    private String RO;
    private BigDecimal advanceContractAmt;
    private BigDecimal salvageAmt;

    private String contractAmt1;
    private String contractAmtWords1;
    private String contractAmt2;
    private String contractAmtWords2;
    private String contractAmt3;
    private String contractAmtWords3;
    private String contractAmt4;
    private String contractAmtWords4;

    private String perSecAmt1;
    private String perSecAmtWords1;
    private String perSecAmt2;
    private String perSecAmtWords2;
    private String perSecAmt3;
    private String perSecAmtWords3;
    private String perSecAmt4;
    private String perSecAmtWords4;

    private String currencyID1;
    private String currencyID2;
    private String currencyID3;
    private String currencyID4;

    public String getRO() {
        return RO;
    }

    public void setRO(String RO) {
        this.RO = RO;
    }

    public String getAmountCnt() {
        return amountCnt;
    }

    public void setAmountCnt(String amountCnt) {
        setAdvanceContractAmt(new BigDecimal(amountCnt));
        this.amountCnt = amountCnt;
    }

    public BigDecimal getAdvanceContractAmt() {
        return advanceContractAmt;
    }

    public void setAdvanceContractAmt(BigDecimal advanceContractAmt) {
        this.advanceContractAmt = advanceContractAmt;
    }

    public int getRoundId() {
        return roundId;
    }

    public void setRoundId(int roundId) {
        this.roundId = roundId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }
    //SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public String getAcceptRejType() {
        return acceptRejType;
    }

    public void setAcceptRejType(String acceptRejType) {
        this.acceptRejType = acceptRejType;
    }

    public String getAcceptRejStatus() {
        return acceptRejStatus;
    }

    public void setAcceptRejStatus(String acceptRejStatus) {
        this.acceptRejStatus = acceptRejStatus;
    }

    public String getCreatedDtString() {
        return createdDtString;
    }

    public void setCreatedDtString(String createdDtString) throws ParseException {
        this.createdDtString = createdDtString;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        setCreatedDt(dateFormat.parse(createdDtString));
    }

    public Date getAcceptRejDt() {
        return acceptRejDt;
    }

    public void setAcceptRejDt(Date acceptRejDt) {
        this.acceptRejDt = acceptRejDt;
    }

    public String getAcceptRejDtString() {
        return acceptRejDtString;
    }

    public void setAcceptRejDtString(String acceptRejDtString) throws ParseException {
        this.acceptRejDtString = acceptRejDtString;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        if (acceptRejDtString.equalsIgnoreCase("null")) {
            setAcceptRejDt(null);
        } else {
            setAcceptRejDt(dateFormat.parse(acceptRejDtString));
        }
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public int getNoaIssueId() {
        return noaIssueId;
    }

    public void setNoaIssueId(int noaIssueId) {
        this.noaIssueId = noaIssueId;
    }

    public int getNoaAcceptId() {
        return noaAcceptId;
    }

    public void setNoaAcceptId(int noaAcceptId) {
        this.noaAcceptId = noaAcceptId;
    }

    public int getHdUserId() {
        return hdUserId;
    }

    public void setHdUserId(int hdUserId) {
        this.hdUserId = hdUserId;
    }

    public String getPerReq() {
        return perReq;
    }

    public void setPerReq(String perReq) {
        this.perReq = perReq;
    }

    public IssueNOADtBean() {
    }

    public String getPerfSecAmtString() {
        return perfSecAmtString;
    }

    public void setPerfSecAmtString(String perfSecAmtString) {
        this.perfSecAmtString = perfSecAmtString;
        BigDecimal perfSecAmtBD = new BigDecimal(perfSecAmtString);
        setPerfSecAmt(perfSecAmtBD);
    }

    public String getNoaAcceptDtString() {
        return noaAcceptDtString;
    }

    public void setNoaAcceptDtString(String noaAcceptDtString) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        this.noaAcceptDtString = noaAcceptDtString;
        setNoaAcceptDt(dateFormat.parse(noaAcceptDtString));
    }

    public String getContractAmtString() {
        return contractAmtString;
    }

    public void setContractAmtString(String contractAmtString) {
        this.contractAmtString = contractAmtString;
        BigDecimal contractAmtBD = new BigDecimal(contractAmtString).setScale(3,0);
        setContractAmt(contractAmtBD);
    }

    public String getContractDtString() {
        return contractDtString;
    }

    public void setContractDtString(String contractDtString) throws ParseException {
        this.contractDtString = contractDtString;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        setContractDt(dateFormat.parse(contractDtString));
    }

    public String getContractSignDtString() {
        return contractSignDtString;
    }

    public void setContractSignDtString(String contractSignDtString) throws ParseException {
        this.contractSignDtString = contractSignDtString;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        setContractSignDt(dateFormat.parse(contractSignDtString));
    }

    public String getPerSecSubDtString() {
        return perSecSubDtString;
    }

    public void setPerSecSubDtString(String perSecSubDtString) throws ParseException {
        this.perSecSubDtString = perSecSubDtString;
        if (!"".equals(perSecSubDtString)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            setPerSecSubDt(dateFormat.parse(perSecSubDtString));
        }
    }

    public String getClauseRef() {
        return clauseRef;
    }

    public void setClauseRef(String clauseRef) {
        this.clauseRef = clauseRef;
    }

    public BigDecimal getContractAmt() {
        return contractAmt;
    }

    public void setContractAmt(BigDecimal contractAmt) {
        this.contractAmt = contractAmt;
    }

    public String getContractAmtWords() {
        return contractAmtWords;
    }

    public void setContractAmtWords(String contractAmtWords) {
        this.contractAmtWords = contractAmtWords;
    }

    public Date getContractDt() {
        return contractDt;
    }

    public void setContractDt(Date contractDt) {
        this.contractDt = contractDt;
    }

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public short getContractSignDays() {
        return contractSignDays;
    }

    public void setContractSignDays(short contractSignDays) {
        this.contractSignDays = contractSignDays;
    }

    public Date getContractSignDt() {
        return contractSignDt;
    }

    public void setContractSignDt(Date contractSignDt) {
        this.contractSignDt = contractSignDt;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    public Date getNoaAcceptDt() {
        return noaAcceptDt;
    }

    public void setNoaAcceptDt(Date noaAcceptDt) {
        this.noaAcceptDt = noaAcceptDt;
    }

    public short getNoaIssueDays() {
        return noaIssueDays;
    }

    public void setNoaIssueDays(short noaIssueDays) {
        this.noaIssueDays = noaIssueDays;
    }

    public Short getPerSecSubDays() {
        return perSecSubDays;
    }

    public void setPerSecSubDays(Short perSecSubDays) {
        if (perSecSubDays == null) {
            this.perSecSubDays = (short) 0;
        } else {
            this.perSecSubDays = perSecSubDays;
        }
    }

    public Date getPerSecSubDt() {
        return perSecSubDt;
    }

    public void setPerSecSubDt(Date perSecSubDt) {
        this.perSecSubDt = perSecSubDt;
    }

    public BigDecimal getPerfSecAmt() {
        return perfSecAmt;
    }

    public void setPerfSecAmt(BigDecimal perfSecAmt) {
        if (perfSecAmt == null) {
            this.perfSecAmt = BigDecimal.ZERO;
        } else {
            this.perfSecAmt = perfSecAmt;
        }
    }

    public String getPerfSecAmtWords() {
        return perfSecAmtWords;
    }

    public void setPerfSecAmtWords(String perfSecAmtWords) {
        if (perfSecAmtWords == null) {
            this.perfSecAmtWords = "";
        } else {
            this.perfSecAmtWords = perfSecAmtWords;
        }
    }

    public int getPkgLotId() {
        return pkgLotId;
    }

    public void setPkgLotId(int pkgLotId) {
        this.pkgLotId = pkgLotId;
    }

    public int getTenderId() {
        return tenderId;
    }

    public void setTenderId(int tenderId) {
        this.tenderId = tenderId;
    }
    

    public String getContractAmt1() {
        return contractAmt1;
    }

    public void setContractAmt1(String contractAmt1) {
        this.contractAmt1 = contractAmt1;
    }

    public String getContractAmtWords1() {
        return contractAmtWords1;
    }

    public void setContractAmtWords1(String contractAmtWords1) {
        this.contractAmtWords1 = contractAmtWords1;
    }

    public String getContractAmt2() {
        return contractAmt2;
    }

    public void setContractAmt2(String contractAmt2) {
        this.contractAmt2 = contractAmt2;
    }

    public String getContractAmtWords2() {
        return contractAmtWords2;
    }

    public void setContractAmtWords2(String contractAmtWords2) {
        this.contractAmtWords2 = contractAmtWords2;
    }


    public String getContractAmt3() {
        return contractAmt3;
    }

    public void setContractAmt3(String contractAmt3) {
        this.contractAmt3 = contractAmt3;
    }

    public String getContractAmtWords3() {
        return contractAmtWords3;
    }

    public void setContractAmtWords3(String contractAmtWords3) {
        this.contractAmtWords3 = contractAmtWords3;
    }

    public String getContractAmt4() {
        return contractAmt4;
    }

    public void setContractAmt4(String contractAmt4) {
        this.contractAmt4 = contractAmt4;
    }

    public String getContractAmtWords4() {
        return contractAmtWords4;
    }

    public void setContractAmtWords4(String contractAmtWords4) {
        this.contractAmtWords4 = contractAmtWords4;
    }

    public String getPerSecAmt1() {
        return perSecAmt1;
    }

    public void setPerSecAmt1(String perSecAmt1) {
        this.perSecAmt1 = perSecAmt1;
    }

    public String getPerSecAmtWords1() {
        return perSecAmtWords1;
    }
    public void setPerSecAmtWords1(String perSecAmtWords1) {
        this.perSecAmtWords1 = perSecAmtWords1;
    }

    public String getPerSecAmt2() {
        return perSecAmt2;
    }

    public void setPerSecAmt2(String perSecAmt2) {
        this.perSecAmt2 = perSecAmt2;
    }

    public String getPerSecAmtWords2() {
        return perSecAmtWords2;
    }
    public void setPerSecAmtWords2(String perSecAmtWords2) {
        this.perSecAmtWords2 = perSecAmtWords2;
    }

    public String getPerSecAmt3() {
        return perSecAmt3;
    }

    public void setPerSecAmt3(String perSecAmt3) {
        this.perSecAmt3 = perSecAmt3;
    }

    public String getPerSecAmtWords3() {
        return perSecAmtWords3;
    }
    public void setPerSecAmtWords3(String perSecAmtWords3) {
        this.perSecAmtWords3 = perSecAmtWords3;
    }
    public String getPerSecAmt4() {
        return perSecAmt4;
    }

    public void setPerSecAmt4(String perSecAmt4) {
        this.perSecAmt4 = perSecAmt4;
    }

    public String getPerSecAmtWords4() {
        return perSecAmtWords4;
    }
    public void setPerSecAmtWords4(String perSecAmtWords4) {
        this.perSecAmtWords4 = perSecAmtWords4;
    }

    public String getCurrencyID1() {
        return currencyID1;
    }
    public void setCurrencyID1(String currencyID1) {
        this.currencyID1 = currencyID1;
    }
    public String getCurrencyID2() {
        return currencyID2;
    }
    public void setCurrencyID2(String currencyID2) {
        this.currencyID2 = currencyID2;
    }
    public String getCurrencyID3() {
        return currencyID3;
    }
    public void setCurrencyID3(String currencyID3) {
        this.currencyID3 = currencyID3;
    }
    public String getCurrencyID4() {
        return currencyID4;
    }
    public void setCurrencyID4(String currencyID4) {
        this.currencyID4 = currencyID4;
    }
     public BigDecimal getSalvageAmt() {
        return salvageAmt;
    }

    public void setSalvageAmt(BigDecimal salvageAmt) {
        this.salvageAmt = salvageAmt;
    }

    public String getSalvageAmtStr() {
        return salvageAmtStr;
    }

    public void setSalvageAmtStr(String salvageAmtStr) {
        this.salvageAmtStr = salvageAmtStr;
        if(salvageAmtStr==null)
        {
           this.salvageAmt=BigDecimal.ZERO; 
        }else{
        this.salvageAmt=new BigDecimal(salvageAmtStr).setScale(3, RoundingMode.UP);
        }
    }



}
