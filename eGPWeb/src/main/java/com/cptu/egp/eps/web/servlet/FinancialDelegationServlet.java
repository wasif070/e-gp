/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.FinancialDelegationData;
import com.cptu.egp.eps.service.serviceinterface.FinancialDelegationService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Istiak (Dohatec) - Jun 14, 2015
 */
public class FinancialDelegationServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            FinancialDelegationService oFinancialDelegationService = (FinancialDelegationService) AppContext.getSpringBean("FinancialDelegationService");

            String action = request.getParameter("action").trim();

            if(action.equalsIgnoreCase("ViewTimeline")) //  Show Table
            {
                String flag = request.getParameter("flag").trim();

                String id = "";
                if(request.getParameter("id") != null)
                    id = request.getParameter("id").trim();

                String cType = "";
                if(request.getParameter("configureType") != null)
                    cType = request.getParameter("configureType").trim();

                List<FinancialDelegationData> finDelegation = oFinancialDelegationService.financialDelegationData(action, cType, id, "", "", "", "", "", "", "", "", "", "", "", "");

                if("configuration".equalsIgnoreCase(flag))
                    ShowFinancialDelegationForConfiguration(out, finDelegation, oFinancialDelegationService);
                else
                    ShowFinancialDelegation(out, finDelegation);
            }
            else if(action.equalsIgnoreCase("DeleteRows"))  // Delete Rules
            {
                String id = request.getParameter("rowNo").trim();
                String flag = request.getParameter("flag").trim();
                List<FinancialDelegationData> timelineAfterDel = oFinancialDelegationService.financialDelegationData(action, id, "", "", "", "", "", "", "", "", "", "", "", "", "");
                if("false".equalsIgnoreCase(timelineAfterDel.get(0).getFieldName1()) || timelineAfterDel == null)
                    out.print("false");
                else
                    out.print("true");
            }
            else if(action.equalsIgnoreCase("AddUpdate"))   // Insert or Update a new Rule
            {
                try{
                    boolean isSuccess = InsertOrUpdate(request, oFinancialDelegationService);
                    if(isSuccess)
                        response.sendRedirect("/admin/FinancialDelegationConfig.jsp?isEdit=y&msg=y");
                    else
                        response.sendRedirect("/admin/FinancialDelegationConfig.jsp?isEdit=y&msg=n");
                }catch(Exception e){
                    response.sendRedirect("/admin/FinancialDelegationConfig.jsp?isEdit=y&msg=n");
                }
            }
            else if(action.equalsIgnoreCase("DeleteRanks"))  //  Delegation Designation Rank Delete
            {
                String id = request.getParameter("rowNo").trim();
                List<FinancialDelegationData> ranksAfterDel = oFinancialDelegationService.financialDelegationData(action, id, "", "", "", "", "", "", "", "", "", "", "", "", "");
                if("false".equalsIgnoreCase(ranksAfterDel.get(0).getFieldName1()) || ranksAfterDel == null)
                    out.print("false");
                else
                    out.print("true");
            }
            else if(action.equalsIgnoreCase("RanksAddUpdate"))  //  Delegation Designation Rank Configuration Save and Update
            {
                String[] newOldEdit = request.getParameterValues("NewOldEdit");
                String[] designation = request.getParameterValues("Designation");
                String[] ranksId = request.getParameterValues("RanksId");

                try{
                    boolean isSuccess = oFinancialDelegationService.rankInsertOrUpdate(designation, ranksId, newOldEdit);
                    if(isSuccess)
                        response.sendRedirect("/admin/DelegationDesignationRank.jsp?f=e&msg=y");
                    else
                        response.sendRedirect("/admin/DelegationDesignationRank.jsp?f=e&msg=n");
                }catch(Exception e){
                    response.sendRedirect("/admin/DelegationDesignationRank.jsp?f=e&msg=n");
                }
            }
        } catch(Exception e){
            String msg = "No records found";
            ErrorMessage(out, msg);
            System.out.print("\n FinancialDelegationServlet : " + e);
        } finally {
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void ShowFinancialDelegation(PrintWriter out, List<FinancialDelegationData> finDelegation) {
        int i ;
        if(finDelegation.size() > 0)
        {
            i =0;
            for(FinancialDelegationData data : finDelegation)
            {
                /* Column Name 
                 * FieldName1() = FinancialDeligationId
                 * FieldName2() = BudgetType
                 * FieldName3() = TenderType
                 * FieldName4() = ProcurementMethod
                 * FieldName5() = ProcurementType
                 * FieldName6() = ProcurementNature
                 * FieldName7() = TenderEmergency
                 * FieldName8() = MinValueBDT
                 * FieldName9() = MaxValueBDT
                 * FieldName10() = MinProjectValueBDT
                 * FieldName11() = MaxProejectValueBDT
                 * FieldName12() = isBoD
                 * FieldName13() = ApprovingAuthority
                 * FieldName14() = isCorporation
                 *  FieldName15() = OfficeLevel
                 */
                
                //Changed By Emtaz on 19/April/2016
                String BudgetTypeForBhutan = "";
                if(data.getFieldName2().equalsIgnoreCase("Development"))
                {
                    BudgetTypeForBhutan = "Capital";
                }
                else if(data.getFieldName2().equalsIgnoreCase("Revenue"))
                {
                    BudgetTypeForBhutan = "Recurrent";
                }
                
                
                String ApprovingAuthorityForBhutan = "";
                if(data.getFieldName13().equalsIgnoreCase("PE"))
                {
                    ApprovingAuthorityForBhutan = "PA";
                }
                else if(data.getFieldName13().equalsIgnoreCase("HOPE"))
                {
                    ApprovingAuthorityForBhutan = "HOPA";
                }
                
                else
                {
                    ApprovingAuthorityForBhutan = data.getFieldName13();
                }
                
                
                
                i++;
                out.print("<tr>");
                out.print("<td class='t-align-center' width='10%'>" + BudgetTypeForBhutan + "</td>");
                out.print("<td class='t-align-center' width='10%'>" + data.getFieldName3() + "</td>");
                out.print("<td class='t-align-center' width='10%'>" + data.getFieldName4() + "</td>");  
                out.print("<td class='t-align-center' width='10%'>" + data.getFieldName6() + "</td>");
                out.print("<td class='t-align-center' width='10%'>" + data.getFieldName7() + "</td>");
                out.print("<td class='t-align-center' width='10%' id='minDigit_"+i+"'>" + data.getFieldName8() + "</td>");
                out.print("<td class='t-align-center' width='10%' id='minwords_"+i+"'></td>");
                out.print("<td class='t-align-center' width='10%' id='maxDigit_"+i+"'>" + data.getFieldName9() + "</td>");
                out.print("<td class='t-align-center' width='10%' id='maxwords_"+i+"'></td>");
                out.print("<td class='t-align-center' width='10%' id='minprojectDigit_"+i+"''>" + data.getFieldName10() + "</td>");
                out.print("<td class='t-align-center' width='10%' id='minprojectwords_"+i+"'></td>");
                out.print("<td class='t-align-center' width='10%' id='maxprojectDigit_"+i+"'>" + data.getFieldName11() + "</td>");
                out.print("<td class='t-align-center' width='10%' id='maxprojectwords_"+i+"'></td>");
                out.print("<td class='t-align-center' width='10%'>" + data.getFieldName12() + "</td>");
                out.print("<td class='t-align-center' width='10%'>" + data.getFieldName14() + "</td>");
                out.print("<td class='t-align-center' width='10%'>" + data.getFieldName15() + "</td>");
                out.print("<td class='t-align-center' width='10%'>" + ApprovingAuthorityForBhutan + "</td>");
          
                String edit = "<a href='FinancialDelegationConfig.jsp?id=" + data.getFieldName1() + "&cType=single&isEdit=y'>Edit</a>";
                String delete = "<a href='javascript:void(0);' id='del_" + data.getFieldName1() + "' onclick='DeleteRow(" + data.getFieldName1() + ")'>Delete</a>";
                out.print("<td class='t-align-center' width='10%'>" + edit + " | " + delete + "</td>");
                out.print("</tr>");
            }
             out.print("<tr> <input type='hidden' id='recordNum' name='recordNum' value='"+ i + "'> </tr>");             
        }
        else
        {
            String msg = "No records found";
            ErrorMessage(out, msg);
        }
    }

    private void ShowFinancialDelegationForConfiguration(PrintWriter out, List<FinancialDelegationData> finDelegation, FinancialDelegationService oFinancialDelegationService) {
        if(finDelegation.size() > 0)
        {
            List<FinancialDelegationData> budgetType = oFinancialDelegationService.financialDelegationData("getBudgetType", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            List<FinancialDelegationData> tenderType = oFinancialDelegationService.financialDelegationData("getTenderType", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            List<FinancialDelegationData> procMethod = oFinancialDelegationService.financialDelegationData("getProcurementMethod", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            List<FinancialDelegationData> procNature = oFinancialDelegationService.financialDelegationData("getProcurementNature", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            List<FinancialDelegationData> appAuthority = oFinancialDelegationService.financialDelegationData("getApprovingAuthority", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            List<FinancialDelegationData> isBoD = oFinancialDelegationService.financialDelegationData("getIsBOd", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            List<FinancialDelegationData> isCorporation = oFinancialDelegationService.financialDelegationData("getIsCorporation", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            List<FinancialDelegationData> OfficeLevel = oFinancialDelegationService.financialDelegationData("getPEOfficeLevel", "", "", "", "", "", "", "", "", "", "", "", "", "", "");


            for(FinancialDelegationData data : finDelegation)
            {
                 /* Column Name
                 * FieldName1() = FinancialDeligationId
                 * FieldName2() = BudgetType
                 * FieldName3() = TenderType
                 * FieldName4() = ProcurementMethod
                 * FieldName5() = ProcurementType
                 * FieldName6() = ProcurementNature
                 * FieldName7() = TenderEmergency
                 * FieldName8() = MinValueBDT
                 * FieldName9() = MaxValueBDT
                 * FieldName10() = MinProjectValueBDT
                 * FieldName11() = MaxProejectValueBDT
                 * FieldName12() = isBoD
                 * FieldName13() = ApprovingAuthority
                 * FieldName14() = isCorporation
                 *  FieldName15() = OfficeLevel
                 */
                out.print("<tr>");
                out.print("<td class='t-align-center' width='4%'>");
                out.print("<input id='chk_" + data.getFieldName1() + "' name='RulesNo' type='checkbox' value='" + data.getFieldName1() + "' />");
                out.print("<input type='hidden' name='FinancialDeligationId' id='financialDeligationId_"+ data.getFieldName1() + "' value='" + data.getFieldName1() + "'>");
                out.print("<input type='hidden' name='NewOldEdit' id='NewOldEdit_"+ data.getFieldName1() + "' value='Old'>");
                out.print("</td>");

                out.print("<td class='t-align-center' width='6%'>" + LoadAllDropdown(budgetType, data.getFieldName1(), data.getFieldName2(), "BudgetType") + " ");
                out.print("<input type='hidden' name='BTOld' id='BTOld_" + data.getFieldName1() + "' value='" + data.getFieldName2() + "'>");
                out.print("</td>");

                out.print("<td class='t-align-center' width='6%'>" + LoadAllDropdown(tenderType, data.getFieldName1(), data.getFieldName3(), "TenderType") + " ");
                out.print("<input type='hidden' name='TTOld' id='TTOld_" + data.getFieldName1() + "' value='" + data.getFieldName3() + "'>");
                out.print("</td>");

                out.print("<td class='t-align-center' width='6%'>" + LoadAllDropdown(procMethod, data.getFieldName1(), data.getFieldName4(), "ProcurementMethod") + " ");
                out.print("<input type='hidden' name='PMOld' id='PMOld_" + data.getFieldName1() + "' value='" + data.getFieldName4() + "'>");
                out.print("</td>");

                out.print("<td class='t-align-center' width='6%'>" + LoadAllDropdown(procNature, data.getFieldName1(), data.getFieldName6(), "ProcurementNature") + " ");
                out.print("<input type='hidden' name='PNOld' id='PNOld_" + data.getFieldName1() + "' value='" + data.getFieldName6() + "'>");
                out.print("</td>");
               
                out.print("<td class='t-align-center' width='6%'>" + LoadTenderEmergency(data.getFieldName1(), data.getFieldName7()) + " ");
                out.print("<input type='hidden' name='TEOld' id='TEOld_" + data.getFieldName1() + "' value='" + data.getFieldName7() + "'>");
                out.print("</td>");
                
                out.print("<td class='t-align-center' width='6%'>");
                out.print("<input type='text' class='formTxtBox_1 MinBDT' name='MinBDT' onblur='return MinValueWord(this);'  onChange='NewOrEdit(" + data.getFieldName1() + ")' id='MinBDT_" + data.getFieldName1() + "' value='" + data.getFieldName8() + "'> ");
                out.print("<input type='hidden' name='MinOld' id='MinOld_" + data.getFieldName1() + "' value='" + data.getFieldName8() + "'>");
                out.print("</td>");

                out.print("<td class='t-align-center' width='6%'>");
                out.print("<span id='MinBDTword_" + data.getFieldName1() + "'></span></td>");
                out.print("</td>");

                out.print("<td class='t-align-center' width='6%'>");
                out.print("<input type='text' class='formTxtBox_1 MaxBDT' name='MaxBDT'  onblur='return MaxValueWord(this);' onChange='NewOrEdit(" + data.getFieldName1() + ")' id='MaxBDT_" + data.getFieldName1() + "' value='" + data.getFieldName9() + "'>");
                out.print("<input type='hidden' name='MaxOld' id='MaxOld_" + data.getFieldName1() + "' value='" + data.getFieldName9() + "'>");
                out.print("</td>");

                out.print("<td class='t-align-center' width='6%'>");
                out.print("<span id='MaxBDTword_" + data.getFieldName1() + "'></span></td>");
                out.print("</td>");

                out.print("<td class='t-align-center' width='6%'>");
                out.print("<input type='text' class='formTxtBox_1 MinProjectBDT' name='MinProjectBDT'  onblur='return ProjectMinValueWord(this);' onChange='NewOrEdit(" + data.getFieldName1() + ")' id='MinProjectBDT_" + data.getFieldName1() + "' value='" + data.getFieldName10() + "'>");
                out.print("<input type='hidden' name='MaxOld' id='MinProjectOld_" + data.getFieldName1() + "' value='" + data.getFieldName10() + "'>");
                out.print("</td>");

                out.print("<td class='t-align-center' width='6%'>");
                out.print("<span id='MinProjectBDTword_" + data.getFieldName1() + "'></span></td>");
                out.print("</td>");

                out.print("<td class='t-align-center' width='6%'>");
                out.print("<input type='text' class='formTxtBox_1 MaxProjectBDT' name='MaxProjectBDT' onblur='return ProjectMaxValueWord(this);' onChange='NewOrEdit(" + data.getFieldName1() + ")' id='MaxProjectBDT_" + data.getFieldName1() + "' value='" + data.getFieldName11() + "'>");
                out.print("<input type='hidden' name='MaxOld' id='MaxProjectOld_" + data.getFieldName1() + "' value='" + data.getFieldName11() + "'>");
                out.print("</td>");

                out.print("<td class='t-align-center' width='6%'>");
                out.print("<span id='MaxProjectBDTword_" + data.getFieldName1() + "'></span></td>");
                out.print("</td>");

                out.print("<td class='t-align-center' width='6%'>" + LoadAllDropdown(isBoD, data.getFieldName1(), data.getFieldName12(), "isBoD") + " ");
                out.print("<input type='hidden' name='isBoDOld' id='isBoDOld_" + data.getFieldName1() + "' value='" + data.getFieldName12() + "'>");
                out.print("</td>");

                out.print("<td class='t-align-center' width='6%'>" + LoadAllDropdown(isCorporation, data.getFieldName1(), data.getFieldName14(), "isCorporation") + " ");
                out.print("<input type='hidden' name='isCorporationOld' id='isCorporationOld_" + data.getFieldName1() + "' value='" + data.getFieldName14() + "'>");
                out.print("</td>");

                out.print("<td class='t-align-center' width='6%'>" + LoadAllDropdown(OfficeLevel, data.getFieldName1(), data.getFieldName15(), "OfficeLevel") + " ");
                out.print("<input type='hidden' name='OfficeLevelOld' id='OfficeLevelOld_" + data.getFieldName1() + "' value='" + data.getFieldName15() + "'>");
                out.print("</td>");

                out.print("<td class='t-align-center' width='6%'>" + LoadAllDropdown(appAuthority, data.getFieldName1(), data.getFieldName13(), "ApprovingAuthority") + " ");
                out.print("<input type='hidden' name='AAOld' id='AAOld_" + data.getFieldName1() + "' value='" + data.getFieldName13() + "'>");
                out.print("</td>");
                
                out.print("</tr>");
            }
        }
        else
        {
            String msg = "No records found";
            ErrorMessage(out, msg);
        }
    }

    private void ErrorMessage(PrintWriter out, String msg) {
        out.print("<tr>");
        out.print("<td colspan=\"17\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\">" + msg + "</td>");
        out.print("</tr>");
    }

    private boolean InsertOrUpdate(HttpServletRequest request, FinancialDelegationService oFinancialDelegationService) {
        
        String[] financialDeligationId = request.getParameterValues("FinancialDeligationId");
        String[] newOldEdit = request.getParameterValues("NewOldEdit");
        String[] budgetType = request.getParameterValues("BudgetType");
        String[] tenderType = request.getParameterValues("TenderType");
        String[] procurementMethod = request.getParameterValues("ProcurementMethod");
        String[] procurementType = null;
        String[] procurementNature = request.getParameterValues("ProcurementNature");
        String[] tenderEmergency = request.getParameterValues("TenderEmergency");
        String[] minValueBDT = request.getParameterValues("MinBDT");
        String[] maxValueBDT = request.getParameterValues("MaxBDT");
        String[] minProjectValueBDT = request.getParameterValues("MinProjectBDT");
        String[] maxProjectValueBDT = request.getParameterValues("MaxProjectBDT");
        String[] isBoD = request.getParameterValues("isBoD");
        String[] isCorporation = request.getParameterValues("isCorporation");
        String[] officeLevel = request.getParameterValues("OfficeLevel");
        String[] approvingAuthority = request.getParameterValues("ApprovingAuthority");
        return oFinancialDelegationService.insertOrUpdate(financialDeligationId, newOldEdit, budgetType, tenderType, procurementMethod, procurementType, procurementNature, tenderEmergency, minValueBDT, maxValueBDT, minProjectValueBDT,maxProjectValueBDT, isBoD,isCorporation,approvingAuthority,officeLevel);
    }

    private String LoadAllDropdown(List<FinancialDelegationData> list, String id, String value, String name) {
        StringBuilder sb = new StringBuilder();
        String selected = null;
        sb.append("<select name='").append(name).append("' class='formTxtBox_1 ").append(name).append("' id='").append(name).append("_").append(id).append("' style='width:100px;' onChange='NewOrEdit(").append(id).append(")'>");
        for(FinancialDelegationData finDelegation : list){
            selected = "";
            String item = finDelegation.getFieldName2();
            if(item.equalsIgnoreCase(value))
                selected = " selected='selected'";
            //Changed by Emtaz on 19/April/2016
            if(item.equalsIgnoreCase("Development"))
                sb.append("<option value='").append(item).append("' ").append(selected).append(">").append("Capital").append("</option>");
            else if(item.equalsIgnoreCase("Revenue"))
                sb.append("<option value='").append(item).append("' ").append(selected).append(">").append("Recurrent").append("</option>");
            else if(item.equalsIgnoreCase("PE"))
                sb.append("<option value='").append(item).append("' ").append(selected).append(">").append("PA").append("</option>");
            else if(item.equalsIgnoreCase("HOPE"))
                sb.append("<option value='").append(item).append("' ").append(selected).append(">").append("HOPA").append("</option>");
            
            else
                sb.append("<option value='").append(item).append("' ").append(selected).append(">").append(item).append("</option>");
        }
        sb.append("</select>");
        return sb.toString();
    }

    private String LoadTenderEmergency(String id, String value) {
        StringBuilder sb = new StringBuilder();
        String selected = " selected='selected'";
        String normal = "";
        String disaster = "";
        String urgent = "";

        if("Normal".equalsIgnoreCase(value))
            normal = selected;
        else if("National Disaster".equalsIgnoreCase(value))
            disaster = selected;
        else if("Urgent(Catastrophe)".equalsIgnoreCase(value))
            urgent = selected;

        sb.append("<select name='TenderEmergency' class='formTxtBox_1 TenderEmergency' id='TenderEmergency_").append(id).append("' style='width:100px;' onChange='NewOrEdit(").append(id).append(")'>");
        sb.append("<option value='Normal'").append(normal).append(">Normal</option>");
	sb.append("<option value='National Disaster'").append(disaster).append(">National Disaster</option>");
	sb.append("<option value='Urgent(Catastrophe)'").append(urgent).append(">Urgent(Catastrophe)</option>");
        sb.append("</select>");
        return sb.toString();
    }
    
}
