/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *For jsp file evalAns to cp
 * tbl_EvalMemStatus and tbl_EvalCpMemClarification update
 * @author Administrator
 */
public class ServletEvalanstoCP extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        int counter = 0;


        int tenderId = 0;
        String st = "";
        boolean isCp = false;

        boolean isError = true;
        CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
        CommonMsgChk commonMsgChk = null;
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletEvalanstoCP</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServletEvalanstoCP at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
             */
            tenderId = Integer.parseInt(request.getParameter("tenderId"));
            isCp = Boolean.parseBoolean(request.getParameter("isCp"));
            counter = Integer.parseInt(request.getParameter("counter"));
            st = request.getParameter("st");
            String uId = request.getParameter("uId");
            String remrk = "";
            for (int i = 1; i <= counter; i++) {
                String evalCpQueId = "";
                String evalStatusId = "";
                String evalStatus = "";
                String decedentCb = "No";
                String ansByCp = "";
                if (request.getParameter("ansByCp_" + i) != null) {
                    ansByCp = request.getParameter("ansByCp_" + i);
                }
                if (request.getParameter("decedentCb_" + i) != null) {
                    decedentCb = request.getParameter("decedentCb_" + i);
                    if("Yes".equalsIgnoreCase(decedentCb)){
                        remrk = request.getParameter("remark_" + i);
                }
                }
                evalCpQueId = request.getParameter("evalCpId_" + i);
                String updateData = "";
                if (isCp) {
                    updateData = "noteOfDescent='" + decedentCb + "', cpcomments='"+remrk+"'";
                } else {
                    updateData = "memAnswer='" + ansByCp + "'";
                }
                String whereCondition = "evalCpQueId =" + evalCpQueId;
                commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_EvalCpMemClarification", updateData, whereCondition).get(0);
                commonMsgChk.getMsg();
                if (!commonMsgChk.getFlag()) {
                    isError = false;
                    break;
                }
                //CheckIf cp or not
                if (!isCp) {
                    evalStatusId = request.getParameter("evalStatusId_" + i);
                    evalStatus = request.getParameter("evalStatus_" + i);

                    updateData = "isComplied='" + evalStatus + "'";
                    whereCondition = "evalMemStatusId=" + evalStatusId;
                    commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_EvalMemStatus", updateData, whereCondition).get(0);
                    if (!commonMsgChk.getFlag()) {
                        isError = false;
                        break;
                    }
                }
            }
            
            if (isError) {
                response.sendRedirect("officer/EvalViewanstoCp.jsp?err=Sucess&tenderId=" + tenderId + "&st=" + st + "&uId=" + uId);
            } else {
                response.sendRedirect("officer/EvalanstoCp.jsp?err=0&tenderId=" + tenderId + "&st=" + st + "&uId" + uId);
            }
        } catch (Exception ex) {
            Logger.getLogger(ServletEvalanstoCP.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (commonMsgChk != null || commonXMLSPService != null) {
                commonMsgChk = null;
                commonXMLSPService = null;
            }
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
