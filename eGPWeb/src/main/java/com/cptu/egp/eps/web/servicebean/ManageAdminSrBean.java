/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.view.VwGetAdmin;
import com.cptu.egp.eps.service.serviceinterface.AdminRegistrationService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
/**
 * @author Administrator
 */
public class ManageAdminSrBean {

    final Logger logger = Logger.getLogger(ManageAdminSrBean.class);

    private AdminRegistrationService adminRegistrationService = (AdminRegistrationService) AppContext.getSpringBean("AdminRegistrationService");
    
    private int offset = 0;
    private int limit = 0;
    private String colName = "";
    private String op_ENUM = "";
    private String condition = "";
    private boolean _search = false;
    private String getSortOrder = "";
    private String sortCol = "";
    private String logUserId="0";

     public void setLogUserId(String logUserId) {
        adminRegistrationService.setLogUserId(logUserId);
        this.logUserId = logUserId;
     }

    public String getSortOrder() {
        return getSortOrder;
    }

    public void setSortOrder(String getSortOrder) {
        this.getSortOrder = getSortOrder;
    }

    public String getSortCol() {
        return sortCol;
    }

    public void setSortCol(String sortCol) {
        this.sortCol = sortCol;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public boolean isSearch() {
        return _search;
    }

    public void setSearch(boolean _search) {
        this._search = _search;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getOp_ENUM() {
        return op_ENUM;
    }

    public void setOp_ENUM(String op_ENUM) {
        this.op_ENUM = op_ENUM;
    }

    public float getAllCountOfDept(){
        logger.debug("getAllCountOfDept : "+logUserId+" Starts ");
        float f = 0;
        try {
            f =  adminRegistrationService.getCntDepartmentMaster("TblDepartmentMaster", "departmentType like 'Organization'");
        } catch (Exception e) {
             logger.error("getAllCountOfDept : "+logUserId+" : "+e);
    }
        logger.debug("getAllCountOfDept : "+logUserId+" Ends");
        return f;
    }

    List<VwGetAdmin> adminList = new ArrayList<VwGetAdmin>();
    public List<VwGetAdmin> getOrgMasterList(){
        logger.debug("getOrgMasterList : "+logUserId+" Starts ");
        try{
        if (adminList.isEmpty()) {
            for (VwGetAdmin vwGetAdmin : adminRegistrationService.findVwOrgMaster(getOffset(), getLimit(),"id."+ getSortCol(), Operation_enum.ORDERBY, Operation_enum.ASC)) {
                adminList.add(vwGetAdmin);
            }
        }
        logger.debug("getOrgMasterList : "+logUserId+" : adinList"+adminList);
        }catch(Exception e)
        {
            logger.error("getOrgMasterList : "+logUserId+" : "+e);
        }
        logger.debug("getOrgMasterList : "+logUserId+" Ends");
        return adminList;
    }

    public void setOrgMasterList(List<VwGetAdmin> adminList) {
        this.adminList = adminList;
    }
}
