/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.utility;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
/**
 *
 * @author Ahsan
 */
public class FinalSubmissionScheduler extends QuartzJobBean {

    private CommonScheduledTask commonScheduledTask;
    public static int i = 0;
    
    public FinalSubmissionScheduler(){
    }

        /**
     * @return the commonScheduledTask
     */
    public CommonScheduledTask getCommonScheduledTask() {
        return commonScheduledTask;
    }

    /**
     * @param commonScheduledTask the commonScheduledTask to set
     */
    public void setCommonScheduledTask(CommonScheduledTask commonScheduledTask) {
        this.commonScheduledTask = commonScheduledTask;
    }
    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        //throw new UnsupportedOperationException("Not supported yet.");
        if(i == 0){
            i++;
            commonScheduledTask.FinalSubmissionMailSMS();
        }
    }
}
