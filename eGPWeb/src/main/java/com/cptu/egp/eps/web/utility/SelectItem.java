/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.utility;

/**
 *
 * @author Administrator
 */
public class SelectItem {
    
    Object objectId;
    String objectValue;

    public Object getObjectId() {
        return objectId;
    }

    public void setObjectId(Object objectId) {
        this.objectId = objectId;
    }

    public String getObjectValue() {
        return objectValue;
    }

    public void setObjectValue(String objectValue) {
        this.objectValue = objectValue;
    }

    public SelectItem(Object objectId, String objectValue) {
        this.objectId = objectId;
        this.objectValue = objectValue;
    }
}
