/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.Charts;

import com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch;
import com.cptu.egp.eps.service.serviceimpl.CMSService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.awt.Color;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.gantt.Task;
import org.jfree.data.gantt.TaskSeries;
import org.jfree.data.gantt.TaskSeriesCollection;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.time.Day;
import org.jfree.data.time.SimpleTimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.ui.RectangleInsets;
import org.jfree.util.Rotation;

/**
 *
 * @author shreyansh Jogi
 */
public class ChartServices {

    CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");

    public JFreeChart PieChartForPE(BigDecimal processedbyPE, BigDecimal CV, BigDecimal percent) {

        DefaultPieDataset datasetPE = new DefaultPieDataset();
        datasetPE.setValue("PE " + percent + "%", processedbyPE);
        datasetPE.setValue("Pending " + new BigDecimal(100).subtract(percent).setScale(2, 0) + "%", CV.subtract(processedbyPE));
        JFreeChart chart = ChartFactory.createPieChart3D("Invoice Processed by PE", datasetPE, false, false, false);
        final PiePlot3D plot = (PiePlot3D) chart.getPlot();
        plot.setStartAngle(300);
        plot.setAutoPopulateSectionOutlinePaint(true);
        plot.setBackgroundPaint(Color.white);
        plot.setAutoPopulateSectionPaint(true);
        plot.setDirection(Rotation.ANTICLOCKWISE);
        plot.setForegroundAlpha(0.4f);
        plot.setNoDataMessage("No data to display");
        return chart;
    }

    public JFreeChart PieChartForTen(BigDecimal processedbyTen, BigDecimal CV, BigDecimal percent) {


        DefaultPieDataset dataset = new DefaultPieDataset();
        dataset.setValue("Tenderer " + percent + "%", processedbyTen);
        dataset.setValue("Pending " + new BigDecimal(100).subtract(percent).setScale(2, 0) + "%", CV.subtract(processedbyTen));
        JFreeChart chart = ChartFactory.createPieChart3D("Invoice Generated by Tenderer", dataset, false, false, false);
        final PiePlot3D plot1 = (PiePlot3D) chart.getPlot();
        plot1.setStartAngle(300);
        plot1.setAutoPopulateSectionOutlinePaint(true);
        plot1.setBackgroundPaint(Color.white);
        plot1.setAutoPopulateSectionPaint(true);
        plot1.setDirection(Rotation.ANTICLOCKWISE);
        plot1.setForegroundAlpha(0.4f);
        plot1.setNoDataMessage("No data to display");
        return chart;

    }

    public JFreeChart PieChartForAcc(BigDecimal processedbyAcc, BigDecimal CV, BigDecimal percent) {

        DefaultPieDataset datasetPE = new DefaultPieDataset();
        datasetPE.setValue("Accounts Officer " + percent + "%", processedbyAcc);
        datasetPE.setValue("Pending " + new BigDecimal(100).subtract(percent).setScale(2, 0) + "%", CV.subtract(processedbyAcc));
        JFreeChart chart = ChartFactory.createPieChart3D("Invoice Processed by Accounts Officer", datasetPE, false, false, false);
        final PiePlot3D plot2 = (PiePlot3D) chart.getPlot();
        plot2.setStartAngle(300);
        plot2.setBackgroundPaint(Color.white);
        plot2.setAutoPopulateSectionOutlinePaint(true);
        plot2.setAutoPopulateSectionPaint(true);
        plot2.setDirection(Rotation.ANTICLOCKWISE);
        plot2.setForegroundAlpha(0.4f);
        plot2.setNoDataMessage("No data to display");
        return chart;
    }

    public JFreeChart Barchart(int tenderId, int lotId) {
        final String homeCnt = "Off Site Count";
        final String fieldCnt = "On Site Count";
        List<Object[]> list = cmss.getStaffInputTotal(tenderId, lotId);
        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        if (!list.isEmpty()) {
            for (Object[] os : list) {
                dataset.addValue(Integer.parseInt(os[3].toString()), fieldCnt, os[0].toString());
                dataset.addValue(Integer.parseInt(os[2].toString()), homeCnt, os[0].toString());
            }
        }
        JFreeChart chart = ChartFactory.createBarChart3D(
                "Staffing Schedule Report", // chart title
                "Employee Name", // domain axis label
                "Total No of Days", // range axis label
                dataset, // data
                PlotOrientation.VERTICAL, // orientation
                true, // include legend
                true, // tooltips?
                false // URLs?
                );
        chart.setBackgroundPaint(Color.white);
        final CategoryPlot plot = chart.getCategoryPlot();
        plot.setBackgroundPaint(Color.WHITE);
        plot.setDomainGridlinePaint(Color.black);
        plot.setRangeGridlinePaint(Color.black);
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        final BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(true);
        final CategoryAxis domainAxis = plot.getDomainAxis();
        domainAxis.setCategoryLabelPositions(
                CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 10.0));
        return chart;

    }

    public JFreeChart GanttChart(int formMapId) {

        List<Object[]> list = cmss.getAllServiceWorkPlan(formMapId);
        final TaskSeries s1 = new TaskSeries("Scheduled");
        if (!list.isEmpty()) {
            for (Object[] obj : list) {
                s1.add(new Task(obj[1].toString(),
                        new SimpleTimePeriod((Date) obj[2],
                        (Date) obj[4])));
            }
        }
        final TaskSeriesCollection collection = new TaskSeriesCollection();
        collection.add(s1);
        final JFreeChart chart = ChartFactory.createGanttChart(
                "Work Plan Gantt Chart", // chart title
                "Activity", // domain axis label
                "Date", // range axis label
                collection, // data
                true, // include legend
                true, // tooltips
                false // urls
                );
        final CategoryPlot plot = chart.getCategoryPlot();
        plot.setBackgroundPaint(Color.WHITE);
        plot.setDomainGridlinePaint(Color.black);
        plot.setRangeGridlinePaint(Color.black);
        return chart;
    }

    public JFreeChart LineChart(int formMapId, String formType) {


        List<TblCmsSrvPaymentSch> list = cmss.getPaymentScheduleData(formMapId);
        TimeSeries series = new TimeSeries("PE", Day.class);
        TimeSeries series1 = new TimeSeries("Consultant", Day.class);
        TimeSeriesCollection dataset = new TimeSeriesCollection();
        TimeSeries series2 = null;
        if (!"Time based".equalsIgnoreCase(formType)) {
            series2 = new TimeSeries("Actual", Day.class);
        }
        if (!list.isEmpty()) {
            int i = 1;
            int j = 1;
            HashSet<Date> hm1 = new HashSet<Date>();
            HashSet<Date> hm2 = new HashSet<Date>();
            HashSet<Date> hm3 = new HashSet<Date>();
            for (TblCmsSrvPaymentSch sch : list) {

                hm1.add(sch.getPeenddate());

                hm2.add(sch.getEndDate());
                if (!"Time based".equalsIgnoreCase(formType)) {
                    if(sch.getCompletedDt()!=null){
                        hm3.add(sch.getCompletedDt());
                    }
                }
                j++;
            }
            for (int k = 0; k < hm1.size(); k++) {
                String pre[] = hm1.toArray()[k].toString().split(" ");
                String temp[] = pre[0].toString().split("-");
                series.add(new Day(Integer.parseInt(temp[2].toString()), Integer.parseInt(temp[1].toString()), Integer.parseInt(temp[0].toString())), k+1);
            }
            for (int k = 0; k < hm2.size(); k++) {
                String pre1[] = hm2.toArray()[k].toString().split(" ");
                String temp1[] = pre1[0].toString().split("-");
                series1.add(new Day(Integer.parseInt(temp1[2].toString()), Integer.parseInt(temp1[1].toString()), Integer.parseInt(temp1[0].toString())), k+1);
            }

            if (!hm3.isEmpty()) {
                for (int k = 0; k < hm3.size(); k++) {
                    String actual[] = hm3.toArray()[k].toString().split(" ");
                    String temp2[] = actual[0].toString().split("-");
                    series2.add(new Day(Integer.parseInt(temp2[2].toString()), Integer.parseInt(temp2[1].toString()), Integer.parseInt(temp2[0].toString())), k+1);
                }
            }
            dataset.addSeries(series);
            dataset.addSeries(series1);
            if (!"Time based".equalsIgnoreCase(formType)) {
                dataset.addSeries(series2);
            }
        }
        JFreeChart chart = ChartFactory.createTimeSeriesChart(
                "Payment Schedule",
                "Time",
                "Milestone",
                dataset,
                true,
                true,
                false);
        // NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...

        chart.setBackgroundPaint(Color.white);
        // get a reference to the plot for further customisation...
        XYPlot plot = (XYPlot) chart.getPlot();

        plot.setBackgroundPaint(Color.white);

        plot.setAxisOffset(new RectangleInsets(
                5.0, 5.0, 5.0, 5.0));
        plot.setDomainGridlinePaint(Color.black);
        plot.setRangeGridlinePaint(Color.black);
        // change the auto tick unit selection to integer units only...
        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        XYToolTipGenerator toolTipGenerator = null;
        if (true) {
            toolTipGenerator = StandardXYToolTipGenerator.getTimeSeriesInstance();
        }
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer(true, true);

        renderer.setToolTipGenerator(toolTipGenerator);

        plot.setRenderer(renderer);
        return chart;
    }
}
