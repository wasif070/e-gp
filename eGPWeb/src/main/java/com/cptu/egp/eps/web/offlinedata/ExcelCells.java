/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.offlinedata;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 *
 * @author Administrator
 */
public class ExcelCells {

  private static Map<String,String> pre_qualificationMacros = new HashMap<String,String>();
  private static Map<String,String> pre_qualificationFormSheet = new HashMap<String,String>();
  private static Map<String,String> preq_timeCells = new HashMap<String,String>();

  private static Map<String,String> tenderSheetMacrosCells = new HashMap<String,String>();
  private static Map<String,String> tenderSheetCells = new HashMap<String,String>();
  private static Map<String,String> tender_timeCells = new HashMap<String,String>();

  private static Map<String,String> contractAwardMacros = new HashMap<String,String>();
  private static Map<String,String> contractAwardCells = new HashMap<String,String>();
  
  private static Map<String,String> EOImacros = new HashMap<String,String>();
  private static Map<String,String> EOISheetCells = new HashMap<String,String>();
  private static Map<String,String> EOItimeCells = new HashMap<String,String>();




     static{
        
        pre_qualificationMacros.put("1~3","MinistryName");
        pre_qualificationMacros.put("1~4","Agency");
        pre_qualificationMacros.put("1~7","ProcuringEntityDistrict");
        pre_qualificationMacros.put("1~8","Invitation1");
        pre_qualificationMacros.put("1~9","ProcurementNature");
        pre_qualificationMacros.put("1~10","InvitationFor");
        pre_qualificationMacros.put("1~13", "ProcurementType");
        pre_qualificationMacros.put("1~14", "ProcurementMethod");
        pre_qualificationMacros.put("1~16", "Cbobudget");
        pre_qualificationMacros.put("1~17", "Cbosource");
        pre_qualificationMacros.put("24~3", "PrequalificationClosingDateandTime");
        pre_qualificationMacros.put("1~50", "LocationofDeliveryWorksConsultancy");
        pre_qualificationMacros.put("1~93", "IstheContractSignedwiththesamepersonstatedintheNOA");
        pre_qualificationMacros.put("1~95", "WasthePerformanceSecurityprovidedinduetime");
        pre_qualificationMacros.put("1~97", "WastheContractSignedinduetime");


        pre_qualificationFormSheet.put("3~3", "ProcuringEntityName");
        pre_qualificationFormSheet.put("4~3", "ProcuringEntityCode");
        pre_qualificationFormSheet.put("7~3", "InvitationRefNo");
        pre_qualificationFormSheet.put("8~3", "Tenderissuingdate");
        pre_qualificationFormSheet.put("14~3", "DevelopmentPartner");
        pre_qualificationFormSheet.put("16~3", "ProjectOrProgrammeCode");
        pre_qualificationFormSheet.put("17~3", "ProjectOrProgrammeName");
        pre_qualificationFormSheet.put("18~3", "TenderPackageNo");
        pre_qualificationFormSheet.put("19~3", "TenderPackageName");
        pre_qualificationFormSheet.put("21~3", "PrequalificationPublicationDate");
        pre_qualificationFormSheet.put("27~3", "SellingPrequalificationDocumentPrincipal");
        pre_qualificationFormSheet.put("28~3", "SellingPrequalificationDocumentOthers");
        pre_qualificationFormSheet.put("29~3", "ReceivingPrequalification");
        pre_qualificationFormSheet.put("32~3", "Place_Date_TimeofPrequalificationMeeting");
        pre_qualificationFormSheet.put("32~4", "DateTimeofPrequalificationMeeting");
        pre_qualificationFormSheet.put("34~3", "EligibilityofApplicant");
        pre_qualificationFormSheet.put("35~3", "BriefDescriptionofGoodsorWorks");
        pre_qualificationFormSheet.put("36~3", "BriefDescriptionofRelatedServices");
        pre_qualificationFormSheet.put("37~3", "PrequalificationDocumentPrice");
        pre_qualificationFormSheet.put("43~1", "LotNo_1");
        pre_qualificationFormSheet.put("43~2", "IdentificationOfLot_1");
        pre_qualificationFormSheet.put("43~3", "Location_1");
        pre_qualificationFormSheet.put("43~4", "CompletionTimeInWeeksORmonths_1");
        pre_qualificationFormSheet.put("44~1", "LotNo_2");
        pre_qualificationFormSheet.put("44~2", "IdentificationOfLot_2");
        pre_qualificationFormSheet.put("44~3", "Location_2");
        pre_qualificationFormSheet.put("44~4", "CompletionTimeInWeeksORmonths_2");
        pre_qualificationFormSheet.put("45~1", "LotNo_3");
        pre_qualificationFormSheet.put("45~2", "IdentificationOfLot_3");
        pre_qualificationFormSheet.put("45~3", "Location_3");
        pre_qualificationFormSheet.put("45~4", "CompletionTimeInWeeksORmonths_3");
        pre_qualificationFormSheet.put("46~1", "LotNo_4");
        pre_qualificationFormSheet.put("46~2", "IdentificationOfLot_4");
        pre_qualificationFormSheet.put("46~3", "Location_4");
        pre_qualificationFormSheet.put("46~4", "CompletionTimeInWeeksORmonths_4");
        pre_qualificationFormSheet.put("48~3", "NameofOfficialInvitingPrequalification");
        pre_qualificationFormSheet.put("49~3", "DesignationofOfficialInvitingPrequalification");
        pre_qualificationFormSheet.put("50~3", "AddressofOfficialInvitingPrequalification");
        pre_qualificationFormSheet.put("51~3", "ContactdetailsofOfficialInvitingPrequalification");
        pre_qualificationFormSheet.put("51~4", "FaxNo");
        pre_qualificationFormSheet.put("51~5", "E_mail");
        pre_qualificationFormSheet.put("23~2", "DateofNotificationofAward");
        pre_qualificationFormSheet.put("24~2", "DateofContractSigning");
        pre_qualificationFormSheet.put("25~2", "ProposedDateofContractCompletion");
        pre_qualificationFormSheet.put("26~2", "NoofTendersProposalsSold");
        pre_qualificationFormSheet.put("27~2", "NoofTendersProposalsReceived");
        pre_qualificationFormSheet.put("28~2", "NoofResponsiveTendersProposals");
        pre_qualificationFormSheet.put("30~2", "BriefDescriptionofContract");
        pre_qualificationFormSheet.put("31~2", "ContractValue");
        pre_qualificationFormSheet.put("32~2", "NameofSupplierContractorConsultant");
        pre_qualificationFormSheet.put("33~2", "LocationofSupplierContractorConsultant");
        pre_qualificationFormSheet.put("36~2", "IfNogivereasonwhy_NOA");
        pre_qualificationFormSheet.put("38~2", "IfNoGiveReasonWhy_Performance_Security");
        pre_qualificationFormSheet.put("40~2", "IfNoGiveReasonWhy_Contract_signed");

        preq_timeCells.put("3~26", "PrequalificationClosingHrs");
        preq_timeCells.put("4~26", "PrequalificationClosingMms");
        preq_timeCells.put("5~26", "PrequalificationClosingAmorPm");
        preq_timeCells.put("3~46", "PrequalificationMeetingHrs");
        preq_timeCells.put("4~46", "PrequalificationMeetingMms");
        preq_timeCells.put("5~46", "PrequalificationMeetingAmorPm");


        tenderSheetMacrosCells.put("1~3","MinistryName");
        tenderSheetMacrosCells.put("1~4","Agency");
        tenderSheetMacrosCells.put("1~7","ProcuringEntityDistrict");
        tenderSheetMacrosCells.put("1~8","EventType");
        tenderSheetMacrosCells.put("1~9","ProcurementNature");
        tenderSheetMacrosCells.put("1~10","InvitationFor");
        tenderSheetMacrosCells.put("1~13","ProcurementType");
        tenderSheetMacrosCells.put("1~14","ProcurementMethod");
        tenderSheetMacrosCells.put("1~16","Cbobudget");
        tenderSheetMacrosCells.put("1~17","Cbosource");
        tenderSheetMacrosCells.put("1~50","LocationofDeliveryWorksConsultancy");
        tenderSheetMacrosCells.put("1~93", "IstheContractSignedwiththesamepersonstatedintheNOA");
        tenderSheetMacrosCells.put("1~95", "WasthePerformanceSecurityprovidedinduetime");
        tenderSheetMacrosCells.put("1~97", "WastheContractSignedinduetime");

        tender_timeCells.put("3~26","TenderClosingHrs");
        tender_timeCells.put("4~26","TenderClosingMins");
        tender_timeCells.put("5~26","TenderClosingAmorPm");
        tender_timeCells.put("3~28","TenderOpeningHrs");
        tender_timeCells.put("4~28","TenderOpeningMins");
        tender_timeCells.put("5~28","TenderOpeningAmorPm");
        tender_timeCells.put("3~46", "TenderMeetingHrs");
        tender_timeCells.put("4~46", "TenderMeetingMms");
        tender_timeCells.put("5~46", "TenderMeetingAmorPm");
        

        tenderSheetCells.put("3~3", "ProcuringEntityName");
        tenderSheetCells.put("4~3", "ProcuringEntityCode");
        tenderSheetCells.put("7~3", "InvitationRefNo");
        tenderSheetCells.put("8~3", "Tenderissuingdate");
        tenderSheetCells.put("14~3", "DevelopmentPartner");
        tenderSheetCells.put("16~3", "ProjectOrProgrammeCode");
        tenderSheetCells.put("17~3", "ProjectOrProgrammeName");
        tenderSheetCells.put("18~3", "TenderPackageNo");
        tenderSheetCells.put("19~3", "TenderPackageName");
        tenderSheetCells.put("21~3", "TenderPublicationDate");
        tenderSheetCells.put("22~3", "TenderLastSellingDate");
        tenderSheetCells.put("24~3", "TenderClosingDate");
        tenderSheetCells.put("25~3", "TenderOpeningDate");
        tenderSheetCells.put("27~3", "SellingTenderDocumentPrincipal");
        tenderSheetCells.put("28~3", "SellingTenderDocumentOthers");
        tenderSheetCells.put("29~3", "ReceivingTenderDocument");
        tenderSheetCells.put("30~3", "OpeningTenderDocument");
        tenderSheetCells.put("32~3", "PlaceofTenderMeeting");
        tenderSheetCells.put("32~4", "DateTimeofTenderMeeting");
        tenderSheetCells.put("34~3", "EligibilityofTender");
        tenderSheetCells.put("35~3", "BriefDescriptionofGoodsorWorks");
        tenderSheetCells.put("36~3", "BriefDescriptionofRelatedServices");
        tenderSheetCells.put("37~3", "TenderDocumentPrice");
        tenderSheetCells.put("43~1", "LotNo_1");
        tenderSheetCells.put("43~2", "IdentificationOfLot_1");
        tenderSheetCells.put("43~3", "Location_1");
        tenderSheetCells.put("43~4", "TenderSecurityAmount_1");
        tenderSheetCells.put("43~5", "CompletionTimeInWeeksORmonths_1");
        tenderSheetCells.put("44~1", "LotNo_2");
        tenderSheetCells.put("44~2", "IdentificationOfLot_2");
        tenderSheetCells.put("44~3", "Location_2");
        tenderSheetCells.put("44~4", "TenderSecurityAmount_2");
        tenderSheetCells.put("44~5", "CompletionTimeInWeeksORmonths_2");
        tenderSheetCells.put("45~1", "LotNo_3");
        tenderSheetCells.put("45~2", "IdentificationOfLot_3");
        tenderSheetCells.put("45~3", "Location_3");
        tenderSheetCells.put("45~4", "TenderSecurityAmount_3");
        tenderSheetCells.put("45~5", "CompletionTimeInWeeksORmonths_3");
        tenderSheetCells.put("46~1", "LotNo_4");
        tenderSheetCells.put("46~2", "IdentificationOfLot_4");
        tenderSheetCells.put("46~3", "Location_4");
        tenderSheetCells.put("46~4", "TenderSecurityAmount_4");
        tenderSheetCells.put("46~5", "CompletionTimeInWeeksORmonths_4");
        tenderSheetCells.put("48~3", "NameofOfficialInvitingTender");
        tenderSheetCells.put("49~3", "DesignationofOfficialInvitingTender");
        tenderSheetCells.put("50~3", "AddressofOfficialInvitingTender");
        tenderSheetCells.put("51~3", "ContactdetailsofOfficialInvitingTender");
        tenderSheetCells.put("51~4", "FaxNo");
        tenderSheetCells.put("51~5", "E_mail");
        tenderSheetCells.put("23~2", "DateofNotificationofAward");
        tenderSheetCells.put("24~2", "DateofContractSigning");
        tenderSheetCells.put("25~2", "ProposedDateofContractCompletion");
        tenderSheetCells.put("26~2", "NoofTendersProposalsSold");
        tenderSheetCells.put("27~2", "NoofTendersProposalsReceived");
        tenderSheetCells.put("28~2", "NoofResponsiveTendersProposals");
        tenderSheetCells.put("30~2", "BriefDescriptionofContract");
        tenderSheetCells.put("31~2", "ContractValue");
        tenderSheetCells.put("32~2", "NameofSupplierContractorConsultant");
        tenderSheetCells.put("33~2", "LocationofSupplierContractorConsultant");
        tenderSheetCells.put("36~2", "IfNogivereasonwhy_NOA");
        tenderSheetCells.put("38~2", "IfNoGiveReasonWhy_Performance_Security");
        tenderSheetCells.put("40~2", "IfNoGiveReasonWhy_Contract_signed");

        contractAwardMacros.put("1~3",  "MinistryName");
        contractAwardMacros.put("1~4",  "Agency");
        contractAwardMacros.put("1~18", "ProcuringEntityDistrict");
        contractAwardMacros.put("1~5",  "Contractawardfor");
        contractAwardMacros.put("1~13", "ProcurementMethod");
        contractAwardMacros.put("1~10", "BudgetFund");
        contractAwardMacros.put("1~9",  "SourceFund");
        contractAwardMacros.put("1~25", "LocationofDeliveryORWorksORConsultancy");
        contractAwardMacros.put("1~36", "IstheContractSignedwiththesamepersonstatedintheNOA");
        contractAwardMacros.put("1~38", "WasthePerformanceSecurityprovidedinduetime");
        contractAwardMacros.put("1~40", "WastheContractSignedinduetime");

        contractAwardCells.put("7~2", "ProcuringEntityName");
        contractAwardCells.put("8~2", "ProcuringEntityCode");
        contractAwardCells.put("11~2", "InvitPropRefNo");
        contractAwardCells.put("16~2", "DevelopmentPartner");
        contractAwardCells.put("18~2", "ProjectOrProgrammeCode");
        contractAwardCells.put("19~2", "ProjectOrProgrammeName");
        contractAwardCells.put("20~2", "TenderPackageNo");
        contractAwardCells.put("21~2", "TenderPackageName");
        contractAwardCells.put("22~2", "DateOfAdvertisement");
        contractAwardCells.put("23~2", "DateofNotificationofAward");
        contractAwardCells.put("24~2", "DateofContractSigning");
        contractAwardCells.put("25~2", "ProposedDateofContractCompletion");
        contractAwardCells.put("26~2", "NoofTendersProposalsSold");
        contractAwardCells.put("27~2", "NoofTendersProposalsReceived");
        contractAwardCells.put("28~2", "NoofResponsiveTendersProposals");
        contractAwardCells.put("30~2", "BriefDescriptionofContract");
        contractAwardCells.put("31~2", "ContractValue");
        contractAwardCells.put("32~2", "NameofSupplierORContractorORConsultant");
        contractAwardCells.put("33~2", "LocationofSupplierORContractorORConsultant");
        contractAwardCells.put("36~2", "IfNogivereasonwhy_NOA");
        contractAwardCells.put("38~2", "IfNoGiveReasonWhy_Performance_Security");
        contractAwardCells.put("40~2", "IfNoGiveReasonWhy_Contract_signed");
        contractAwardCells.put("42~2", "NameofAuthorisedOfficer");
        contractAwardCells.put("43~2", "DesignationofAuthorisedOfficer");

        EOImacros.put("1~3", "MinistryName");
        EOImacros.put("1~4", "Agency");
        EOImacros.put("1~6", "ProcuringEntityDistrict");
        EOImacros.put("1~87", "InterestForSelectionOfConsult");
        EOImacros.put("1~88", "InterestForSelectionOfBased");
        EOImacros.put("1~27", "ProcurementSubmethod");
        EOImacros.put("1~7", "BudgetFund");
        EOImacros.put("1~12", "SourceFund");
        EOImacros.put("1~17", "Associationwithforeignfirms");

        EOItimeCells.put("1~20", "EOIClosingtimeHrs");
        EOItimeCells.put("1~21", "EOIClosingTimeMns");
        EOItimeCells.put("1~22", "EOIClosingTimeAmorPm");

        EOISheetCells.put("7~3", "ProcuringEntityName");
        EOISheetCells.put("8~3", "ProcuringEntityCode");
        EOISheetCells.put("11~3", "EoiRefNo");
        EOISheetCells.put("12~3", "EoiDate");
        EOISheetCells.put("17~3", "DevelopmentPartner");
        EOISheetCells.put("19~3", "ProjectOrProgramCode");
        EOISheetCells.put("20~3", "ProjectOrProgrammeName");
        EOISheetCells.put("21~3", "EoiClosingDateandtime");
        EOISheetCells.put("23~3", "BriefDescriptionoftheAssignment");
        EOISheetCells.put("24~3", "ExpResDeliverCapacityRequired");
        EOISheetCells.put("25~3", "OtherDetails");
        EOISheetCells.put("28~1","RefNo_1");
        EOISheetCells.put("28~2", "PhasingOfServices_1");
        EOISheetCells.put("28~3", "Location_1");
        EOISheetCells.put("28~4", "IndicativeStartDateMonOrYer_1");
        EOISheetCells.put("28~5", "IndicativeCompletionDateMonOrYer_1");
        EOISheetCells.put("29~1", "RefNo_2");
        EOISheetCells.put("29~2", "PhasingOfServices_2");
        EOISheetCells.put("29~3", "Location_2");
        EOISheetCells.put("29~4", "IndicativeStartDateMonOrYer_2");
        EOISheetCells.put("29~5", "IndicativeCompletionDateMonOrYer_2");
        EOISheetCells.put("30~1", "RefNo_3");
        EOISheetCells.put("30~2", "PhasingOfServices_3");
        EOISheetCells.put("30~3", "Location_3");
        EOISheetCells.put("30~4", "IndicativeStartDateMonOrYer_3");
        EOISheetCells.put("30~5", "IndicativeCompletionDateMonOrYer_3");
        EOISheetCells.put("31~1", "RefNo_4");
        EOISheetCells.put("31~2", "PhasingOfServices_4");
        EOISheetCells.put("31~3", "Location_4");
        EOISheetCells.put("31~4", "IndicativeStartDateMonOrYer_4");
        EOISheetCells.put("31~5", "IndicativeCompletionDateMonOrYer_4");
        EOISheetCells.put("32~1", "RefNo_5");
        EOISheetCells.put("32~2", "PhasingOfServices_5");
        EOISheetCells.put("32~3", "Location_5");
        EOISheetCells.put("32~4", "IndicativeStartDateMonOrYer_5");
        EOISheetCells.put("32~5", "IndicativeCompletionDateMonOrYer_5");
        EOISheetCells.put("33~1", "RefNo_6");
        EOISheetCells.put("33~2", "PhasingOfServices_6");
        EOISheetCells.put("33~3", "Location_6");
        EOISheetCells.put("33~4", "IndicativeStartDateMonOrYer_6");
        EOISheetCells.put("33~5", "IndicativeCompletionDateMonOrYer_6");
        EOISheetCells.put("34~1", "RefNo_7");
        EOISheetCells.put("34~2", "PhasingOfServices_7");
        EOISheetCells.put("34~3", "Location_7");
        EOISheetCells.put("34~4", "IndicativeStartDateMonOrYer_7");
        EOISheetCells.put("34~5", "IndicativeCompletionDateMonOrYer_7");
        EOISheetCells.put("35~1", "RefNo_8");
        EOISheetCells.put("35~2", "PhasingOfServices_8");
        EOISheetCells.put("35~3", "Location_8");
        EOISheetCells.put("35~4", "IndicativeStartDateMonOrYer_8");
        EOISheetCells.put("35~5", "IndicativeCompletionDateMonOrYer_8");
        EOISheetCells.put("36~1", "RefNo_9");
        EOISheetCells.put("36~2", "PhasingOfServices_9");
        EOISheetCells.put("36~3", "Location_9");
        EOISheetCells.put("36~4", "IndicativeStartDateMonOrYer_9");
        EOISheetCells.put("36~5", "IndicativeCompletionDateMonOrYer_9");
        EOISheetCells.put("37~1", "RefNo_10");
        EOISheetCells.put("37~2", "PhasingOfServices_10");
        EOISheetCells.put("37~3", "Location_10");
        EOISheetCells.put("37~4", "IndicativeStartDateMonOrYer_10");
        EOISheetCells.put("37~5", "IndicativeCompletionDateMonOrYer_10");
        EOISheetCells.put("39~3", "NameoftheOfficialInvitingEOI");
        EOISheetCells.put("40~3", "DesignationoftheOfficialInvitingEOI");
        EOISheetCells.put("41~3", "AddressoftheOfficialInvitingEOI");
        EOISheetCells.put("42~3", "ContactDetailsoftheOfficialInvitingEOI");
        EOISheetCells.put("42~4", "Fax");
        EOISheetCells.put("42~5", "Email");
        
      }

    /**
     * @return the preq_timeCells
     */
    public static Map<String, String> getPreq_timeCells() {
        return preq_timeCells;
    }

    /**
     * @param aPreq_timeCells the preq_timeCells to set
     */
    public static void setPreq_timeCells(Map<String, String> aPreq_timeCells) {
        preq_timeCells = aPreq_timeCells;
    }

    /**
     * @return the pre_qualificationMacros
     */
    public static Map<String, String> getPre_qualificationMacros() {
        return pre_qualificationMacros;
    }

    /**
     * @param aPre_qualificationMacros the pre_qualificationMacros to set
     */
    public static void setPre_qualificationMacros(Map<String, String> aPre_qualificationMacros) {
        pre_qualificationMacros = aPre_qualificationMacros;
    }

    /**
     * @return the pre_qualificationFormSheet
     */
    public static Map<String, String> getPre_qualificationFormSheet() {
        return pre_qualificationFormSheet;
    }

    /**
     * @param aPre_qualificationFormSheet the pre_qualificationFormSheet to set
     */
    public static void setPre_qualificationFormSheet(Map<String, String> aPre_qualificationFormSheet) {
        pre_qualificationFormSheet = aPre_qualificationFormSheet;
    }

    /**
     * @return the tenderSheetMacrosCells
     */
    public static Map<String, String> getTenderSheetMacrosCells() {
        return tenderSheetMacrosCells;
    }

    /**
     * @param aTenderSheetMacrosCells the tenderSheetMacrosCells to set
     */
    public static void setTenderSheetMacrosCells(Map<String, String> aTenderSheetMacrosCells) {
        tenderSheetMacrosCells = aTenderSheetMacrosCells;
    }

    /**
     * @return the tenderSheetCells
     */
    public static Map<String, String> getTenderSheetCells() {
        return tenderSheetCells;
    }

    /**
     * @param aTenderSheetCells the tenderSheetCells to set
     */
    public static void setTenderSheetCells(Map<String, String> aTenderSheetCells) {
        tenderSheetCells = aTenderSheetCells;
    }

    /**
     * @return the tender_timeCells
     */
    public static Map<String, String> getTender_timeCells() {
        return tender_timeCells;
    }

    /**
     * @param aTender_timeCells the tender_timeCells to set
     */
    public static void setTender_timeCells(Map<String, String> aTender_timeCells) {
        tender_timeCells = aTender_timeCells;
    }

    /**
     * @return the contractAwardMacros
     */
    public static Map<String, String> getContractAwardMacros() {
        return contractAwardMacros;
    }

    /**
     * @param aContractAwardMacros the contractAwardMacros to set
     */
    public static void setContractAwardMacros(Map<String, String> aContractAwardMacros) {
        contractAwardMacros = aContractAwardMacros;
    }

    /**
     * @return the contractAwardCells
     */
    public static Map<String, String> getContractAwardCells() {
        return contractAwardCells;
    }

    /**
     * @param aContractAwardCells the contractAwardCells to set
     */
    public static void setContractAwardCells(Map<String, String> aContractAwardCells) {
        contractAwardCells = aContractAwardCells;
    }

    /**
     * @return the EOImacros
     */
    public static Map<String, String> getEOImacros() {
        return EOImacros;
    }

    /**
     * @param aEOImacros the EOImacros to set
     */
    public static void setEOImacros(Map<String, String> aEOImacros) {
        EOImacros = aEOImacros;
    }

    /**
     * @return the EOISheetCells
     */
    public static Map<String, String> getEOISheetCells() {
        return EOISheetCells;
    }

    /**
     * @param aEOISheetCells the EOISheetCells to set
     */
    public static void setEOISheetCells(Map<String, String> aEOISheetCells) {
        EOISheetCells = aEOISheetCells;
    }

    /**
     * @return the EOItimeCells
     */
    public static Map<String, String> getEOItimeCells() {
        return EOItimeCells;
    }

    /**
     * @param aEOItimeCells the EOItimeCells to set
     */
    public static void setEOItimeCells(Map<String, String> aEOItimeCells) {
        EOItimeCells = aEOItimeCells;
    }


   

}
