/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPPromiseReportData;
import com.cptu.egp.eps.service.serviceimpl.PromiseReportService;
import com.cptu.egp.eps.web.servicebean.GenreatePdfCmd;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.GeneratePdfConstant;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Shreyansh Shah
 */
public class PromiseReportServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        boolean isPDF = false;
        isPDF = Boolean.parseBoolean(request.getParameter("isPDF"));
        //if(!isPDF){
        //HttpSession session = request.getSession();
        //Object userTypeid = session.getAttribute("userTypeId");
        // }
        try {
            String funName = request.getParameter("funName");
            int PromiseId = Integer.parseInt(request.getParameter("PEId"));
            String strPageNo = (request.getParameter("pageNo") != null ? request.getParameter("pageNo") : "");
            String isApprove = request.getParameter("approve");

            int pageNo = Integer.parseInt(strPageNo);
            String strOffset = request.getParameter("size");
            String Fyear = request.getParameter("fyear");
            String PType = request.getParameter("ptype");
            String DeptId = request.getParameter("deptid");
            String Officeid = request.getParameter("officeid");
            String districtId = request.getParameter("districtId");
            String quater = request.getParameter("quater");
            
            //String PEID = request.getParameter("PEId");
            //  String PageNo = request.getParameter("pageNo");
            //    String Size = request.getParameter("size");
            int recordOffset = Integer.parseInt(strOffset);
            int TotTender = 0;
            float columnval = 0;
            float DaysTot = 0;
            float Average = 0;
            int RatioTotal = 0;
            String styleClass = "";

            List<SPPromiseReportData> promiseReportData = null;
            if (funName.equalsIgnoreCase("PromiseReport")) {
                promiseReportData = searchMyReportData(request);
                if (promiseReportData != null && !promiseReportData.isEmpty()) {
                    int i = 0;
                    for (; i < promiseReportData.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }
                        //SPPromiseReportData promiseReport = promiseReport.getFieldName1()
                        out.print("<tr class='" + styleClass + "'>");
                        if (promiseReportData.get(i).getFieldName1() != null) {
                            out.print("<td class=\"t-align-center\">" + promiseReportData.get(i).getFieldName1() + "</td>");
                        }
                        if (promiseReportData.get(i).getFieldName2() != null) {
                            out.print("<td class=\"t-align-center\">" + promiseReportData.get(i).getFieldName2() + "</td>");
                        }
                        if (promiseReportData.get(i).getFieldName3() != null) {
                            out.print("<td class=\"t-align-center\">" + promiseReportData.get(i).getFieldName3() + "</td>");
                            TotTender = TotTender + Integer.parseInt(promiseReportData.get(i).getFieldName3().toString());
                        }
                        if (promiseReportData.get(i).getFieldName4() != null) {
                            out.print("<td class=\"t-align-center\">" + promiseReportData.get(i).getFieldName4() + "</td>");
                            if (PromiseId == 10) {
                                RatioTotal = RatioTotal + Integer.parseInt(promiseReportData.get(i).getFieldName4().toString());
                            } else {
                                columnval = columnval + new Float(promiseReportData.get(i).getFieldName4().toString());
                            }
                        }
                        if (promiseReportData.get(i).getFieldName5() != null) {                          
                                out.print("<td class=\"t-align-center\">" + promiseReportData.get(i).getFieldName5() + "</td>");
                            if (PromiseId != 10) {
                                DaysTot = DaysTot + new Float(promiseReportData.get(i).getFieldName5().toString());
                            }
                        }
                        if (promiseReportData.get(i).getFieldName6() != null) {
                             if (PromiseId != 10) {
                            out.print("<td class=\"t-align-center\">" + promiseReportData.get(i).getFieldName6() + "</td>");
                            }
                        }
                        if (promiseReportData.get(i).getFieldName7() != null) {
                            out.print("<td class=\"t-align-center\">" + promiseReportData.get(i).getFieldName7() + "</td>");
                        }
                        if (promiseReportData.get(i).getFieldName8() != null) {
                            out.print("<td class=\"t-align-center\">" + promiseReportData.get(i).getFieldName8() + "</td>");
                        }
                        if (promiseReportData.get(i).getFieldName9() != null) {
                            out.print("<td class=\"t-align-center\">" + promiseReportData.get(i).getFieldName9() + "</td>");
                        }
                        if (promiseReportData.get(i).getFieldName10() != null) {
                            out.print("<td class=\"t-align-center\">" + promiseReportData.get(i).getFieldName10() + "</td>");
                        }
                        if (promiseReportData.get(i).getFieldName11() != null) {
                            out.print("<td class=\"t-align-center\">" + promiseReportData.get(i).getFieldName11() + "</td>");
                        }
                        if (promiseReportData.get(i).getFieldName12() != null) {
                            out.print("<td class=\"t-align-center\">" + promiseReportData.get(i).getFieldName12() + "</td>");
                        }
                        out.print("</tr>");
                        int totalPages = 1;

                        out.print("<input type=\"hidden\" id=\"cntTenBrief\" value=\"" + i + "\">");
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                    }
                    if (PromiseId == 6 || PromiseId == 8 || PromiseId == 9 || PromiseId == 14) {
                        Average = new Float(columnval) / new Float(TotTender);
                        out.print("<tr>");
                        out.print("<td class=\"t-align-center\" colspan=2 style=\"font-weight: bold\"> All Ministries/Divisions/Organizations/PAs </td>");
                        out.print("<td class=\"t-align-center\">" + TotTender + "</td>");
                        out.print("<td class=\"t-align-center\">" + new BigDecimal(columnval).setScale(0, RoundingMode.HALF_UP) + "</td>");
                        out.print("<td class=\"t-align-center\">" + new BigDecimal(Average).setScale(2, RoundingMode.HALF_UP) + "</td>");
                        out.print("</tr>");
			} else if (PromiseId == 12 || PromiseId == 20 || PromiseId == 24) {
                        out.print("<tr>");
                        out.print("<td class=\"t-align-center\" colspan=2 style=\"font-weight: bold\"> All Ministries/Divisions/Organizations/PAs </td>");
                        out.print("<td class=\"t-align-center\">" + TotTender + "</td>");
                        out.print("<td class=\"t-align-center\">" + new BigDecimal(columnval).setScale(0, RoundingMode.HALF_UP) + "</td>");
                        out.print("<td class=\"t-align-center\">" + new BigDecimal(columnval * 100 / TotTender).setScale(2, RoundingMode.HALF_UP) + "</td>");
                        out.print("</tr>");
                    } else if (PromiseId == 10) {
                         Average = new Float(RatioTotal) / new Float(TotTender);
                        out.print("<tr>");
                        out.print("<td class=\"t-align-center\" colspan=2 style=\"font-weight: bold\"> All Ministries/Divisions/Organizations/PAs </td>");
                        out.print("<td class=\"t-align-center\">" + TotTender + "</td>");
                        out.print("<td class=\"t-align-center\">" + RatioTotal + "</td>");
                        out.print("<td class=\"t-align-center\">1:" + new BigDecimal(Average).setScale(2, RoundingMode.HALF_UP) + "</td>");
                        out.print("</tr>");
                    } else if (PromiseId == 33) {
                        out.print("<tr>");
                        out.print("<td class=\"t-align-center\" colspan=2 style=\"font-weight: bold\"> All Ministries/Divisions/Organizations/PAs </td>");
                        out.print("<td class=\"t-align-center\">" + TotTender + "</td>");
                        if (TotTender != 0) {
                        out.print("<td class=\"t-align-center\">" + new BigDecimal(columnval/TotTender).setScale(2, RoundingMode.HALF_UP) + "</td>");
                        } else { out.print("<td class=\"t-align-center\"> " + TotTender + " </td>");}
                        out.print("</tr>");
                    } else if (PromiseId == 16) {
                        out.print("<tr>");
                        out.print("<td class=\"t-align-center\" colspan=2 style=\"font-weight: bold\"> All Ministries/Divisions/Organizations/PAs </td>");
                        out.print("<td class=\"t-align-center\">" + TotTender + "</td>");
                        out.print("<td class=\"t-align-center\">" + new BigDecimal(columnval).setScale(0, RoundingMode.HALF_UP) + "</td>");
                        out.print("<td class=\"t-align-center\">" + new BigDecimal(DaysTot).setScale(0, RoundingMode.HALF_UP) + "</td>");
                        if (TotTender != 0) {
                            out.print("<td class=\"t-align-center\">" + new BigDecimal(new Float(columnval) / new Float(TotTender)).setScale(2, RoundingMode.HALF_UP) + "</td>");
                        } else {
                            out.print("<td class=\"t-align-center\">0.00</td>");
                        }
                        if (TotTender != 0) {
                            out.print("<td class=\"t-align-center\">" + new BigDecimal(new Float(DaysTot) / new Float(TotTender)).setScale(2, RoundingMode.HALF_UP) + "</td>");
                        } else {
                            out.print("<td class=\"t-align-center\">0.00</td>");
                        }
                        out.print("</tr>");
                    } else if (PromiseId == 19  || PromiseId == 25 ||  PromiseId == 26 || PromiseId == 27) {
                        if (TotTender > 0) {
                            Average = new Float(columnval) / new Float(TotTender);
                        }                        
                        out.print("<tr>");
                        out.print("<td class=\"t-align-center\" colspan=2 style=\"font-weight: bold\"> All Ministries/Divisions/Organizations/PAs </td>");
                        out.print("<td class=\"t-align-center\">" + TotTender + "</td>");
                        out.print("<td class=\"t-align-center\">" + new BigDecimal(columnval).setScale(0, RoundingMode.HALF_UP) + "</td>");
                        out.print("<td class=\"t-align-center\">" + new BigDecimal(Average).setScale(2, RoundingMode.HALF_UP) + "</td>");
                        out.print("</tr>");
                    }
                     else if (PromiseId == 15 ||  PromiseId == 17 || PromiseId == 21 || PromiseId == 22 || PromiseId == 23 || PromiseId == 28 || PromiseId == 29){
                        out.print("<tr>");
                        out.print("<td class=\"t-align-center\" colspan=2 style=\"font-weight: bold\"> All Ministries/Divisions/Organizations/PAs </td>");
                        out.print("<td class=\"t-align-center\">" + TotTender + "</td>");
                        out.print("<td class=\"t-align-center\">" + new BigDecimal(columnval).setScale(0, RoundingMode.HALF_UP) + "</td>");
                        if (TotTender != 0) {
                             Average = new Float(columnval) * 100 / new Float(TotTender);
                            out.print("<td class=\"t-align-center\">" + new BigDecimal(Average).setScale(2, RoundingMode.HALF_UP) + "</td>");
                        } else {
                            out.print("<td class=\"t-align-center\">" + TotTender + "</td>");
                        }
                        out.print("</tr>");
                    }
                    else if (PromiseId == 34 || PromiseId == 37 || PromiseId == 39 || PromiseId == 40) {
                        if (columnval > 0) {
                            Average = new Float(DaysTot) * 100 / new Float(columnval);
                        }
                        out.print("<tr>");
                        out.print("<td class=\"t-align-center\" colspan=2 style=\"font-weight: bold\"> All Ministries/Divisions/Organizations/PAs </td>");
                        out.print("<td class=\"t-align-center\">" + TotTender + "</td>");
                        out.print("<td class=\"t-align-center\">" + new BigDecimal(columnval).setScale(0, RoundingMode.HALF_UP) + "</td>");
                        out.print("<td class=\"t-align-center\">" + new BigDecimal(DaysTot).setScale(0, RoundingMode.HALF_UP) + "</td>");
                        out.print("<td class=\"t-align-center\">" + new BigDecimal(Average).setScale(2, RoundingMode.HALF_UP) + "</td>");
                        out.print("</tr>");
                    }
                    else {
                        out.print("<tr>");
                        out.print("<td class=\"t-align-center\" colspan=2 style=\"font-weight: bold\"> All Ministries/Divisions/Organizations/PAs </td>");
                        out.print("<td class=\"t-align-center\">" + TotTender + "</td>");
                        out.print("<td class=\"t-align-center\">" + new BigDecimal(columnval).setScale(0, RoundingMode.HALF_UP) + "</td>");
                        if (TotTender != 0) {
                            out.print("<td class=\"t-align-center\">" + new BigDecimal(columnval * 100 / TotTender).setScale(2, RoundingMode.HALF_UP) + "</td>");
                        } else {
                            out.print("<td class=\"t-align-center\">" + TotTender + "</td>");
                        }
                        out.print("</tr>");
                    }
                } else {
                    out.print("<tr>");
                    out.print("<td colspan=\"7\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }
                if (!isPDF) {
                    try {
//                                                       String Fyear = request.getParameter("fyear");
//                    String PType = request.getParameter("ptype");
//                    String DeptId = request.getParameter("deptid");
//                    String Officeid = request.getParameter("officeid");
//                    String PEID = request.getParameter("PEId");
//                    String PageNo = request.getParameter("pageNo");
//                    String Size = request.getParameter("size");
                        //String PEID = request.getParameter("PEId");
                        //  String PageNo = request.getParameter("pageNo");
                        //    String Size = request.getParameter("size");
                        String projectId = request.getParameter("projectId");
                        String userId = String.valueOf(request.getSession().getAttribute("userId"));
                        GenreatePdfCmd obj = new GenreatePdfCmd();
                        GeneratePdfConstant constant = new GeneratePdfConstant();
                        String cdate = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date(new Date().getTime()));
                        String folderName = constant.PROMISREPORT;
                        String genId = cdate + "_" + userId;
                        String reqURL = request.getRequestURL().toString();
                        reqURL = reqURL.replace("PromiseReportServlet", "admin/PromisReportPDF.jsp");
                        String reqQuery = "&userId=" + request.getSession().getAttribute("userId") + "&fyear=" + Fyear + "&ptype=" + PType + "&deptid=" + DeptId + "&officeid=" + Officeid + "&PEId=" + PromiseId  + "&districtId="+districtId+ "&pageNo=" + strPageNo + "&size=" + strOffset + "&quater="+quater + "&projectId=" + projectId;
                        obj.genrateCmd(reqURL, reqQuery, folderName, genId);
                    } catch (Exception e) {
                        System.out.println("exception-" + e);
                    }
                }

            } else if (funName.equalsIgnoreCase("OverallPromiseReport")) {                
                String categoryIndicator = request.getParameter("ctgIndicator");
                int startCategory = Integer.parseInt(categoryIndicator.split(",")[0].toString());
                int endCategory = Integer.parseInt(categoryIndicator.split(",")[1].toString());
                int PI10TotTender = 0;
                for (int k = startCategory; k <= endCategory; k++) {
                    TotTender = 0;
                    PI10TotTender = 0;
                    columnval = 0;
                    DaysTot = 0;
                    Average = 0;
                    if (k % 2 == 0) {
                        styleClass = "bgColor-white";
                    } else {
                        styleClass = "bgColor-Green";
                    }
                    out.print("<tr class='" + styleClass + "'>");
                    if(k == 1)
                    {
                        out.print("<td class=\"t-align-center\" rowspan=\"4\">1</td>");
                        out.print("<td class=\"t-align-center\" rowspan=\"4\">Invitation for Tender</td>");
                    }
                    else if(k == 5)
                    {
                        out.print("<td class=\"t-align-center\" rowspan=\"6\">2</td>");
                        out.print("<td class=\"t-align-center\" rowspan=\"6\">Tender Submission</td>");
                    }
                    else if(k == 11)
                    {
                        out.print("<td class=\"t-align-center\" rowspan=\"3\">3</td>");
                        out.print("<td class=\"t-align-center\" rowspan=\"3\">Tender Opening Committee (TOC) and Tender Evaluation Committee (TEC)</td>");
                    }
                    else if(k == 14)
                    {
                        out.print("<td class=\"t-align-center\" rowspan=\"5\">4</td>");
                        out.print("<td class=\"t-align-center\" rowspan=\"5\">Tender Evaluation</td>");
                    }
                    else if(k == 19)
                    {
                        out.print("<td class=\"t-align-center\" rowspan=\"6\">5</td>");
                        out.print("<td class=\"t-align-center\" rowspan=\"6\">Tender Evaluation Report (TER) Approval</td>");
                    }
                    else if(k == 25)
                    {
                        out.print("<td class=\"t-align-center\" rowspan=\"5\">6</td>");
                        out.print("<td class=\"t-align-center\" rowspan=\"5\">Contract Award</td>");
                    }
                    else if(k == 30)
                    {
                        out.print("<td class=\"t-align-center\" rowspan=\"3\">7</td>");
                        out.print("<td class=\"t-align-center\" rowspan=\"3\">Delivery/Completion</td>");
                    }
                    else if(k == 33)
                    {
                        out.print("<td class=\"t-align-center\" rowspan=\"3\">8</td>");
                        out.print("<td class=\"t-align-center\" rowspan=\"3\">Payment</td>");
                    }
                    else if(k == 36)
                    {
                        out.print("<td class=\"t-align-center\" rowspan=\"4\">9</td>");
                        out.print("<td class=\"t-align-center\" rowspan=\"4\">Complaints</td>");
                    }
                    else if(k == 40)
                    {
                        out.print("<td class=\"t-align-center\" >10</td>");
                        out.print("<td class=\"t-align-center\">Contract Amendments</td>");
                    }
                    else if(k == 41)
                    {
                        out.print("<td class=\"t-align-center\" >11</td>");
                        out.print("<td class=\"t-align-center\" >Contract Dispute Resolution</td>");
                    }
                    else if(k == 42)
                    {
                        out.print("<td class=\"t-align-center\" >12</td>");
                        out.print("<td class=\"t-align-center\" >Fraud and Corruption</td>");
                    }
                    out.print("<td class=\"t-align-left\">" + getHeadings(k) + "</td>");
                    out.print("<td class=\"t-align-left\"><p>" + getSubHeadings(k) + "</p></td>");
                    Object objUserId = request.getSession().getAttribute("userId");
                    int userId = 0;
                    if (objUserId != null) {
                        userId = Integer.parseInt(objUserId.toString());
                    }
                    String fyear = request.getParameter("fyear");
                    String ptype = request.getParameter("ptype");
                    String officeId = request.getParameter("officeid");
                    String deptid = request.getParameter("deptid");
                    String strPEId = k + "";
                    String distId = request.getParameter("districtId");
                    String projectId = request.getParameter("projectId");
                    //quater = request.getParameter("quater");
                    PromiseReportService promiseReport = (PromiseReportService) AppContext.getSpringBean("PromiseReportService");
                    promiseReportData = promiseReport.searchData(fyear, ptype, deptid, officeId, strPEId, strPageNo, strOffset,distId,quater,projectId);
                    //promiseReportData = searchMyReportData(request);
                    if (promiseReportData != null && !promiseReportData.isEmpty()) {
                        int i = 0;
                        for (; i < promiseReportData.size(); i++) {

                            //SPPromiseReportData promiseReport = promiseReport.getFieldName1()
                            if (promiseReportData.get(i).getFieldName1() != null) {
                            }
                            if (promiseReportData.get(i).getFieldName2() != null) {
                            }
                            if (promiseReportData.get(i).getFieldName3() != null) {
                                if (k != 40) {TotTender = TotTender + Integer.parseInt(promiseReportData.get(i).getFieldName3().toString());}
                            }
                            if (promiseReportData.get(i).getFieldName4() != null) {
                                columnval = columnval + new Float(promiseReportData.get(i).getFieldName4().toString());
				if (k == 10) { RatioTotal = RatioTotal + Integer.parseInt(promiseReportData.get(i).getFieldName4().toString());  }
                                if (k == 40) { TotTender = TotTender + Integer.parseInt(promiseReportData.get(i).getFieldName4().toString());}
                            }
                            if (promiseReportData.get(i).getFieldName5() != null) {
                                if (k != 10) {  DaysTot = DaysTot + new Float(promiseReportData.get(i).getFieldName5().toString()); }
                            }
                            if (promiseReportData.get(i).getFieldName6() != null) {
                                if (k==10) {PI10TotTender= PI10TotTender+Integer.parseInt(promiseReportData.get(i).getFieldName6().toString()); }
                            }
                            if (promiseReportData.get(i).getFieldName7() != null) {
                            }
                            if (promiseReportData.get(i).getFieldName8() != null) {
                            }
                            if (promiseReportData.get(i).getFieldName9() != null) {
                            }
                            if (promiseReportData.get(i).getFieldName10() != null) {
                            }
                            if (promiseReportData.get(i).getFieldName11() != null) {
                            }
                            if (promiseReportData.get(i).getFieldName12() != null) {
                            }
                            int totalPages = 1;

                            out.print("<input type=\"hidden\" id=\"cntTenBrief\" value=\"" + i + "\">");
                            out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                        }
                        if (k == 6 || k == 8 || k == 9 || k == 14 || k == 19 || k == 25 || k == 26 || k == 27) {
                            if (TotTender != 0) {
                                Average = new Float(columnval) / new Float(TotTender);
                            }
                            out.print("<td style=\"text-align:right;\">" + new BigDecimal(Average).setScale(2, RoundingMode.HALF_UP) + "</td>");
                        } else if (k == 10) {
                             Average = new Float(RatioTotal) / new Float(TotTender);
                            out.print("<td style=\"text-align:right;\">1:" + new BigDecimal(Average).setScale(2, RoundingMode.HALF_UP) + "</td>");
                        } else if (k == 16) {
                            if (DaysTot != 0) {
                                Average = new Float(DaysTot) / new Float(TotTender);
                            }
                            out.print("<td style=\"text-align:right;\">" + new BigDecimal(Average).setScale(2, RoundingMode.HALF_UP) + "</td>");
                        } else if (k == 34 || k == 37 || k == 39 || k == 40) {
                            if (columnval != 0) {
                                Average = new Float(DaysTot) * 100 / new Float(columnval);
                            }
                            out.print("<td style=\"text-align:right;\">" + new BigDecimal(Average).setScale(2, RoundingMode.HALF_UP) + "</td>");
                        }  else if (k == 33) {
                             if (TotTender != 0) {
                                out.print("<td style=\"text-align:right;\">" + new BigDecimal(columnval / TotTender).setScale(2, RoundingMode.HALF_UP) + "</td>");
                            } else {
                                out.print("<td style=\"text-align:right;\">0.00</td>");
                            }
                        } else {
                            if (TotTender != 0) {
                                out.print("<td style=\"text-align:right;\">" + new BigDecimal(columnval * 100 / TotTender).setScale(2, RoundingMode.HALF_UP) + "</td>");
                            } else {
                                out.print("<td style=\"text-align:right;\">0.00</td>");
                            }
                        }
                         if (k != 10) {
                        out.print("<td style=\"text-align:right;\">" + TotTender + "</td>");
                        } else {out.print("<td style=\"text-align:right;\">" + PI10TotTender + "</td>");}
                        // out.print("</tr>");
                    } else {
                        out.print("<td style=\"text-align:right;color: red;font-weight: bold\">No Data</td>");
                        out.print("<td style=\"text-align:right;color: red;font-weight: bold\">No Data</td>");
                    }
                    out.print("</tr>");

                }
                if (!isPDF) {
                    try {
                        String userId = String.valueOf(request.getSession().getAttribute("userId"));
                        GenreatePdfCmd obj = new GenreatePdfCmd();
                        GeneratePdfConstant constant = new GeneratePdfConstant();
                        String cdate = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date(new Date().getTime()));
                        String folderName = constant.PROMISOVERALLREPORT;
                        String genId = cdate + "_" + userId;
                        String reqURL = request.getRequestURL().toString();
                        reqURL = reqURL.replace("PromiseReportServlet", "admin/PromisOverallReportPDF.jsp");
                        // String reqQuery = "&userId=" + request.getSession().getAttribute("userId");
                        String projectId = request.getParameter("projectId");
                        String reqQuery = "&userId=" + request.getSession().getAttribute("userId") + "&fyear=" + Fyear + "&ptype=" + PType + "&deptid=" + DeptId + "&officeid=" + Officeid +"&districtId="+districtId+ "&PEId=" + PromiseId + "&pageNo=" + strPageNo + "&size=" + strOffset+ "&indiCategory="+categoryIndicator + "&quater="+ quater + "&projectId"+ projectId ;
                        obj.genrateCmd(reqURL, reqQuery, folderName, genId);
                    } catch (Exception e) {
                        System.out.println("exception-" + e);
                    }
                }
            }
            out.flush();
        } finally {
            out.close();
        }
    }

    public int getMinFirstNumber(int number1, int number2) {
        for (int i = 2; i < number2; i++) {
            if (number2 % i == 0 && number1 % i == 0) {
                //System.out.println(i+"Number to be devided \n");
                number2 = number2 / i;
                number1 = number1 / i;
                getMinFirstNumber(number1, number2);
                break;
            }
        }
        return number1;
    }

    public String getMinNumber(int number1, int number2) {
        for (int i = 2; i < number2; i++) {
            if (number2 % i == 0 && number1 % i == 0) {
                //System.out.println(i+"Number to be devided \n");
                number2 = number2 / i;
                number1 = number1 / i;
                getMinNumber(number1, number2);
                break;
            }
        }
        return number2 + ":" + number1;
    }

    public String getHeadings(int piId) {
        String Heading = "";
        switch (piId) {
            case 1:
                Heading = "Advertisement of Tender/Proposal Opportunities in Newspaper";
                break;
            case 2:
                //Change CPTU to GPPMD by Proshanto
                Heading = "Advertisement of Tender/Proposal Opportunities in GPPMD's Website";
                break;
            case 3:
                //Change GoB to RGoB by Proshanto
                Heading = "Tenders/Proposals Following RGoB Procurement Rules";
                break;
            case 4:
                Heading = "Tenders/Proposals Following Development Partner Rules";
                break;
            case 5:
                Heading = "Multiple Locations Submission Tenders/Proposals";
                break;
            case 6:
                Heading = "Tender/Proposal Preparation Time in Open Tendering Method";
                break;
            case 7:
                Heading = "Tender/Proposal Time Compliance";
                break;
            case 8:
                Heading = "Sale of Tender/Proposal Documents";
                break;
            case 9:
                Heading = "Bidder/Consultant Participation";
                break;
            case 10:
                Heading = "Bidder/Consultant Participation Index";
                break;
            case 11:
                Heading = "Tender/Proposal Opening Committee Formation";
                break;
            case 12:
                Heading = "Tender/Proposal Evaluation Committee Formation";
                break;
            case 13:
                Heading = "External Member in TEC";
                break;
            case 14:
                Heading = "Tender/Proposal Evaluation Time";
                break;
            case 15:
                Heading = "Compliance of Tender/Proposal Evaluation Time";
                break;
            case 16:
                Heading = "Tender/Proposal Acceptance";
                break;
            case 17:
                Heading = "Re-Tendering";
                break;
            case 18:
                Heading = "Tender/Proposal Cancellation";
                break;
            case 19:
                Heading = "Tender/Proposal Evaluation Approval Time";
                break;
            case 20:
                Heading = "Compliance of Financial Delegation";
                break;
            case 21:
                Heading = "Submission of Evaluation Report to Appropriate Authority";
                break;
            case 22:
                Heading = "TER Approval Compliance";
                break;
            case 23:
                Heading = "Additional Review of TER";
                break;
            case 24:
                Heading = "Higher Tier Approval";
                break;
            case 25:
                Heading = "Time for Issuance of Letter of Acceptance (LOA) to Bidder/Consultant";
                break;
            case 26:
                Heading = "Tender/Proposal Processing Lead Time";
                break;
            case 27:
                Heading = "Total Tender/Proposal Processing Time";
                break;
            case 28:
                Heading = "Publication of Award Information";
                break;
            case 29:
                Heading = "Efficiency in Contract Award";
                break;
            case 30:
                Heading = "Delivery Time";
                break;
            case 31:
                Heading = "Liquidated Damage";
                break;
            case 32:
                Heading = "Completion Rate";
                break;
            case 33:
                Heading = "Payment Release Compliance";
                break;
            case 34:
                Heading = "Late Payment";
                break;
            case 35:
                Heading = "Interests Paid for Delayed Payment";
                break;
            case 36:
                Heading = "Tender/Proposal Procedure Complaints";
                break;
            case 37:
                Heading = "Resolution of Complaints With Award Modification";
                break;
            case 38:
                Heading = "Resolution of Complaints";
                break;
            case 39:
                Heading = "Independent Review Panel";
                break;
            case 40:
                Heading = "Contract Amendments/Variations";
                break;
            case 41:
                Heading = "Unresolved Disputes";
                break;
            case 42:
                Heading = "Fraud and Corruption";
                break;
            case 43:
                Heading = "Procurement Training";
                break;
            case 44:
                Heading = "Percentage of Procurement Entities with Trained Persons";
                break;
            case 45:
                Heading = "Number of Procurement Persons by Organization";
                break;

        }
        return Heading;
    }

    public String getSubHeadings(int piId) {
        String SubHeading = "";
        switch (piId) {
            case 1:
                SubHeading = "1. Percentage of Invitation for Tender/Proposal (IFT) Published in Newspaper";
                break;
            case 2:
                //Change CPTU to GPPMD by Proshanto
                SubHeading = "2. Percentage of Invitation for Tender/Proposal (above threshold) Advertised in GPPMD’s Website";
                break;
            case 3:
                //Change GoB to RGoB by Proshanto
                SubHeading = "3. Percentage of Tenders/Proposals Following RGoB Procurement Rules";
                break;
            case 4:
                SubHeading = "4. Percentage of Tenders/Proposals Following Development Partner Rules";
                break;
            case 5:
                SubHeading = "5. Percentage of Tenders/Proposals Allowed to Submit in Multiple Locations";
                break;
            case 6:
                SubHeading = "6. Average Number of Days Between Publishing of Advertisement and Tender/Proposal Submission Deadline";
                break;
            case 7:
                SubHeading = "7. Percentage of Tenders/Proposals having Sufficient Tender/Proposal Submission Time";
                break;
            case 8:
                SubHeading = "8. Average Number of Tenderers/Consultants Purchased Tender/Proposal Documents";
                break;
            case 9:
                SubHeading = "9. Average Number of Tenderers/Consultants Submitted Tenders/Proposals";
                break;
            case 10:
                SubHeading = "10. Ratio of Number of Tender/Proposal Document Sold and Number of Tender/Proposal Submission";
                break;
            case 11:
                SubHeading = "11. Percentage of Cases TOC Included At Least One Member From PEC/TEC";
                break;
            case 12:
                SubHeading = "12. Percentage of Cases TEC Formed by Approving Authority";
                break;
            case 13:
                SubHeading = "13. Percentage of Cases TEC Included Two External Members Outside The Procuring Entity";
                break;
            case 14:
                SubHeading = "14. Average Number of Days Between Tender/Proposal Opening and Completion of Evaluation";
                break;
            case 15:
                SubHeading = "15. Percentage of Cases Tender/Proposal Evaluation has been Completed within Timeline";
                break;
            case 16:
                SubHeading = "16. Average Number of Responsive Tenders/Proposals";
                break;
            case 17:
                SubHeading = "17. Percentage of Cases TEC Recommended Re-Tendering";
                break;
            case 18:
                SubHeading = "18. Percentage of Cases where Tender/Proposal Process Cancelled";
                break;
            case 19:
                SubHeading = "19. Average Number of Days Taken Between Submission of Tender/Proposal Evaluation and Approval Contract";
                break;
            case 20:
                SubHeading = "20. Average Number of Tenders/Proposals Approved by Proper Financial Delegated Authority";
                break;
            case 21:
                SubHeading = "21. Percentage of Cases TEC Submitted Report Directly to The Contract Approving Authority";
                break;
            case 22:
                SubHeading = "22. Percentage of Cases Contract Award Decision Made within Timeline by Contract Approving Authority";
                break;
            case 23:
                SubHeading = "23. Percentage of Cases TER Reviewed by Person/Committee Other Than The Contract Approving Authority";
                break;
            case 24:
                SubHeading = "24. Percentage of Tenders/Proposals Approved by Higher Tier than the Contract Approving Authority";
                break;
            case 25:
                SubHeading = "25. Average Number of Days Between Final Approval and Letter of Acceptance (LOA)";
                break;
            case 26:
                SubHeading = "26. Average Number of Days Between Tender/Proposal Opening and Letter of Acceptance (LOA)";
                break;
            case 27:
                SubHeading = "27. Average Number of Days Between Invitation for Tender/Proposal (IFT) and Letter of Acceptance (LOA)";
                break;
            case 28:
                //Change CPTU to GPPMD by Proshanto
                SubHeading = "28. Percentage of Contract Award Published in GPPMD’s Website";
                break;
            case 29:
                SubHeading = "29. Percentage of Contract Awarded within Initial Tender/Proposal Validity Period";
                break;
            case 30:
                SubHeading = "30. Percent of Contracts Completed/Delivered within the Original Schedule as Mentioned in Contract";
                break;
            case 31:
                SubHeading = "31. Percentage of Contracts Having Liquidated Damage Imposed for Delayed Delivery/Completion";
                break;
            case 32:
                SubHeading = "32. Percentage of Contracts Fully Completed and Accepted";
                break;
            case 33:
                SubHeading = "33. Average Number of Days Taken to Release Payments";
                break;
            case 34:
                SubHeading = "34. Percentage of Cases (considering each installment as a case) with Delayed Payment";
                break;
            case 35:
                SubHeading = "35. Percentage of Contracts where Interest for Delayed Payments was Made";
                break;
            case 36:
                SubHeading = "36. Percentage of Tender/Proposal Procedure Complaints";
                break;
            case 37:
                SubHeading = "37. Percentage of Complaints Resulting in Modification of Awards";
                break;
            case 38:
                SubHeading = "38. Percentage of Cases Complaints have been Resolved";
                break;
            case 39:
                SubHeading = "39. Percentage of Cases Review Panel’s Decisions Upheld";
                break;
            case 40:
                SubHeading = "40. Percentage of Contract Amendments/Variations";
                break;
            case 41:
                SubHeading = "41. Percentage of Contracts With Unresolved Disputes";
                break;
            case 42:
                SubHeading = "42. Percentage of Tenders/Proposals of Fraud and Corruption";
                break;
            case 43:
                SubHeading = "43. Average no of Trained Procurement Staff in Each Procuring Entity";
                break;
            case 44:
                SubHeading = "44. Percentage of Procuring Entity Which Has At Least One Trained/Certified Procurement Staff";
                break;
            case 45:
                SubHeading = "45. Total Number of Procurement Persons in The Organization With Procurement Training";
                break;

        }
        return SubHeading;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private List<SPPromiseReportData> searchMyReportData(HttpServletRequest request) {

        HttpSession session = request.getSession();

        Object objUserId = session.getAttribute("userId");
        int userId = 0;
        if (objUserId != null) {
            userId = Integer.parseInt(objUserId.toString());
        }
        String fyear = request.getParameter("fyear");
        String strPageNo = request.getParameter("pageNo");
        int pageNo = Integer.parseInt(strPageNo);
        String strOffset = request.getParameter("size");
        int recordOffset = Integer.parseInt(strOffset);
        String ptype = request.getParameter("ptype");
        String officeId = request.getParameter("officeid");
        String deptid = request.getParameter("deptid");
        String strPEId = request.getParameter("PEId");
        String districtId = request.getParameter("districtId");
        String quater = request.getParameter("quater");
        String projectId = request.getParameter("projectId");
        PromiseReportService promiseReport = (PromiseReportService) AppContext.getSpringBean("PromiseReportService");
        return promiseReport.searchData(fyear, ptype, deptid, officeId, strPEId, strPageNo, strOffset,districtId,quater,projectId);
    }
}
