/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

import com.cptu.egp.eps.service.serviceinterface.RssService;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.log4j.Logger;


/**
 *This class is used when rss feed are used....
 * @author Nishith
 */
public class RssXmlWriter {
    static final Logger LOGGER = Logger.getLogger(RssXmlWriter.class);
      String path  = AppMessage.getAppPath();
      String url = AppMessage.getAppUrl();
    RssService rssService = (RssService) AppContext.getSpringBean("RssService");
    String rssMsg = "";
    String tagDaily= "";

    /**
     *For getting feed from stored procedure....
     * @param tag give tag name
     * @return RssMessage...
     */
    public String getFeed(String tag) {
        LOGGER.debug("getFeed Starts:");
        tagDaily  = tag;
        try {
            //System.out.println("Path-------:"+path);
            //System.out.println("Path-------:"+url);
            rssMsg = rssService.getRssFeed(tag,url);
            //rssMsg = HandleSpecialChar.handleSpecialChar(rssMsg); 
            //System.out.println("==============>>"+tag+"rssMsg----------->\n\n"+rssMsg);
            filePath();
            fileCreate();
            rssMsg =writeOnFile();
        } catch (Exception e) {
            LOGGER.error("getFeed :"+e);
    }
        LOGGER.debug("getFeed Ends:");
       // System.out.println("rssMsg"+rssMsg);
        return rssMsg;
    }
    File dailyTender;

    /**
     *For creating dailyTender and WeeklyTender.xml file path
     * @throws IOException  
     */
    public void filePath() throws IOException {
        
        LOGGER.debug("filePath Starts:");
      
        /*File file = new File("/");
        //System.out.println("getCanonicalPath()"+file.getCanonicalPath());
       //System.out.println("getAbsolutePath()"+file.getAbsolutePath());
       // System.out.println("getPath()"+file.getParent());*/
        if(tagDaily.equalsIgnoreCase("daily")){
        dailyTender = new File(path + File.separator+"RSS"+ File.separator + "DailyTender.xml");
        //    System.out.println("dailyTender");
        }else{
        dailyTender = new File(path + File.separator +"RSS"+ File.separator+"WeeklyTender.xml");
       // System.out.println("dailyTender");
        }
        LOGGER.debug("filePath Ends:");
    }

    /**
     *Creating file
     * @return true or false
     */
    public boolean fileCreate() {
        LOGGER.debug("fileCreate Starts:");
        boolean flag = false;
        String filePath = path+File.separator+"RSS";
        File f = new File(filePath);
        try {
          //  System.out.println("dailyTender----------->"+dailyTender);
            
            if(!f.exists()){
                f.mkdirs();
            }
            if(dailyTender.exists()){
                dailyTender.delete();
                flag =  this.dailyTender.createNewFile();
            }else{
            flag =  this.dailyTender.createNewFile();
            }
          //  System.out.println("fileCreate:::::::"+flag);
        } catch (IOException ex) {
          //  System.out.println("ex--------->"+ex);
            LOGGER.debug("fileCreate :"+ ex);
        }finally{
            f=null;
            filePath = null;
        }
        LOGGER.debug("fileCreate Ends:");
       // System.out.println("flag------------>"+flag);
        return flag;
    }

    /**
     *Write the content to the file
     * @return String success or fail.
     */
    public String writeOnFile() {
        LOGGER.debug("writeOnFile Starts:");
        fileCreate();
        String val = "";
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(dailyTender);
            if (dailyTender.exists()) {

                fos.write(rssMsg.getBytes());
                fos.flush();
                fos.close();
            }
            val =  "Sucess";
        } catch (FileNotFoundException ex) {
          //  System.out.println("ex------->writeonfile"+ex);
            LOGGER.debug("writeOnFile :"+ ex);
            val =  "Error";
        } catch (IOException ex) {
           // System.out.println("ex------->writeonfile"+ex);
            LOGGER.debug("writeOnFile :"+ ex);
            val  = "Error";
        }
        LOGGER.debug("writeOnFile Ends:");
        //System.out.println("Ends------->writeonfile");
        return val;
    }
}
