/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

import java.math.BigDecimal;

/**
 *
 * @author shreyansh.shah
 */
public class ProcTranReportDtBean {

    private final BigDecimal crore = new BigDecimal(10000000);
    private int counter;
    private String pNature;
    private String pMethod;
    private String nAPPPlanNo;
    private String nAPPPlanAmount;
    private String iAPPPlanNo;
    private String iAPPPlanAmount;
    private String nAPPPkgLiveNo;
    private String nAPPPkgLiveAmount;
    private String iAPPPkgLiveNo;
    private String iAPPPkgLiveAmount;
    private String nAPPPkgProcessNo;
    private String nAPPPkgProcessAmount;
    private String iAPPPkgProcessNo;
    private String iAPPPkgProcessAmount;
    private String nNoaIssuedNo;
    private String nNoaIssuedAmount;
    private String iNoaIssuedNo;
    private String iNoaIssuedAmount;
    private String nContractAwardedNo;
    private String nContractAwardedAmt;
    private String iContractAwardedNo;
    private String iContractAwardedAmt;


    public String getpMethod() {
        return pMethod;
    }

    public void setpMethod(String pMethod) {
        this.pMethod = pMethod;
    }

    public String getpNature() {
        return pNature;
    }

    public void setpNature(String pNature) {
        this.pNature = pNature;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getiAPPPkgLiveAmount() {
        return iAPPPkgLiveAmount;
    }

    public void setiAPPPkgLiveAmount(String iAPPPkgLiveAmount) {
        this.iAPPPkgLiveAmount = iAPPPkgLiveAmount;
    }

    public String getiAPPPkgLiveNo() {
        return iAPPPkgLiveNo;
    }

    public void setiAPPPkgLiveNo(String iAPPPkgLiveNo) {
        this.iAPPPkgLiveNo = iAPPPkgLiveNo;
    }

    public String getiAPPPkgProcessAmount() {
        return iAPPPkgProcessAmount;
    }

    public void setiAPPPkgProcessAmount(String iAPPPkgProcessAmount) {
        this.iAPPPkgProcessAmount = iAPPPkgProcessAmount;
    }

    public String getiAPPPkgProcessNo() {
        return iAPPPkgProcessNo;
    }

    public void setiAPPPkgProcessNo(String iAPPPkgProcessNo) {
        this.iAPPPkgProcessNo = iAPPPkgProcessNo;
    }

    public String getiAPPPlanAmount() {
        return iAPPPlanAmount;
    }

    public void setiAPPPlanAmount(String iAPPPlanAmount) {
        this.iAPPPlanAmount = iAPPPlanAmount;
    }

    public String getiAPPPlanNo() {
        return iAPPPlanNo;
    }

    public void setiAPPPlanNo(String iAPPPlanNo) {
        this.iAPPPlanNo = iAPPPlanNo;
    }

    public String getiContractAwardedAmt() {
        return iContractAwardedAmt;
    }

    public void setiContractAwardedAmt(String iContractAwardedAmt) {
        this.iContractAwardedAmt = iContractAwardedAmt;
    }

    public String getiContractAwardedNo() {
        return iContractAwardedNo;
    }

    public void setiContractAwardedNo(String iContractAwardedNo) {
        this.iContractAwardedNo = iContractAwardedNo;
    }

    public String getiNoaIssuedAmount() {
        return iNoaIssuedAmount;
    }

    public void setiNoaIssuedAmount(String iNoaIssuedAmount) {
        this.iNoaIssuedAmount = iNoaIssuedAmount;
    }

    public String getiNoaIssuedNo() {
        return iNoaIssuedNo;
    }

    public void setiNoaIssuedNo(String iNoaIssuedNo) {
        this.iNoaIssuedNo = iNoaIssuedNo;
    }

    public String getnAPPPkgLiveAmount() {
        return nAPPPkgLiveAmount;
    }

    public void setnAPPPkgLiveAmount(String nAPPPkgLiveAmount) {
        this.nAPPPkgLiveAmount = nAPPPkgLiveAmount;
    }

    public String getnAPPPkgLiveNo() {
        return nAPPPkgLiveNo;
    }

    public void setnAPPPkgLiveNo(String nAPPPkgLiveNo) {
        this.nAPPPkgLiveNo = nAPPPkgLiveNo;
    }

    public String getnAPPPkgProcessAmount() {
        return nAPPPkgProcessAmount;
    }

    public void setnAPPPkgProcessAmount(String nAPPPkgProcessAmount) {
        this.nAPPPkgProcessAmount = nAPPPkgProcessAmount;
    }

    public String getnAPPPkgProcessNo() {
        return nAPPPkgProcessNo;
    }

    public void setnAPPPkgProcessNo(String nAPPPkgProcessNo) {
        this.nAPPPkgProcessNo = nAPPPkgProcessNo;
    }

    public String getnAPPPlanAmount() {
        return nAPPPlanAmount;
    }

    public void setnAPPPlanAmount(String nAPPPlanAmount) {
        this.nAPPPlanAmount = nAPPPlanAmount;
    }

    public String getnAPPPlanNo() {
        return nAPPPlanNo;
    }

    public void setnAPPPlanNo(String nAPPPlanNo) {
        this.nAPPPlanNo = nAPPPlanNo;
    }

    public String getnContractAwardedAmt() {
        return nContractAwardedAmt;
    }

    public void setnContractAwardedAmt(String nContractAwardedAmt) {
        this.nContractAwardedAmt = nContractAwardedAmt;
    }

    public String getnContractAwardedNo() {
        return nContractAwardedNo;
    }

    public void setnContractAwardedNo(String nContractAwardedNo) {
        this.nContractAwardedNo = nContractAwardedNo;
    }

    public String getnNoaIssuedAmount() {
        return nNoaIssuedAmount;
    }

    public void setnNoaIssuedAmount(String nNoaIssuedAmount) {
        this.nNoaIssuedAmount = nNoaIssuedAmount;
    }

    public String getnNoaIssuedNo() {
        return nNoaIssuedNo;
    }

    public void setnNoaIssuedNo(String nNoaIssuedNo) {
        this.nNoaIssuedNo = nNoaIssuedNo;
    }

    public ProcTranReportDtBean(int counter, String pNature, String pMethod, String nAPPPlanNo, String nAPPPlanAmount, String iAPPPlanNo, String iAPPPlanAmount, String nAPPPkgLiveNo, String nAPPPkgLiveAmount, String iAPPPkgLiveNo, String iAPPPkgLiveAmount, String nAPPPkgProcessNo, String nAPPPkgProcessAmount, String iAPPPkgProcessNo, String iAPPPkgProcessAmount, String nNoaIssuedNo, String nNoaIssuedAmount, String iNoaIssuedNo, String iNoaIssuedAmount, String nContractAwardedNo, String nContractAwardedAmt, String iContractAwardedNo, String iContractAwardedAmt) {
        this.counter = counter;
        this.pNature = pNature;
        this.pMethod = pMethod;
        this.nAPPPlanNo = nAPPPlanNo;
        this.nAPPPlanAmount = nAPPPlanAmount;
        this.iAPPPlanNo = iAPPPlanNo;
        this.iAPPPlanAmount = iAPPPlanAmount;
        this.nAPPPkgLiveNo = nAPPPkgLiveNo;
        this.nAPPPkgLiveAmount = nAPPPkgLiveAmount;
        this.iAPPPkgLiveNo = iAPPPkgLiveNo;
        this.iAPPPkgLiveAmount = iAPPPkgLiveAmount;
        this.nAPPPkgProcessNo = nAPPPkgProcessNo;
        this.nAPPPkgProcessAmount = nAPPPkgProcessAmount;
        this.iAPPPkgProcessNo = iAPPPkgProcessNo;
        this.iAPPPkgProcessAmount = iAPPPkgProcessAmount;
        this.nNoaIssuedNo = nNoaIssuedNo;
        this.nNoaIssuedAmount = nNoaIssuedAmount;
        this.iNoaIssuedNo = iNoaIssuedNo;
        this.iNoaIssuedAmount = iNoaIssuedAmount;
        this.nContractAwardedNo = nContractAwardedNo;
        this.nContractAwardedAmt = nContractAwardedAmt;
        this.iContractAwardedNo = iContractAwardedNo;
        this.iContractAwardedAmt = iContractAwardedAmt;
    }

    

}
