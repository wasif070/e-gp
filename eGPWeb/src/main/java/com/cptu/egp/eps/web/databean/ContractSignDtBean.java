/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author rishita
 */
public class ContractSignDtBean {

    private String lastDtContractDtString;
    private int contractSignId;
    private Date lastDtContractDt;
    private String contractSignDtString;
    private Date contractSignDt;
    private String contractSignLocation;
    private String isPubAggOnWeb;
    private int createdBy;
    private String witnessInfo="";
    private int noaId;
    private String paymentTerms="";
    private String isRepeatOrder="";
    private String createdDtString;
    private Date createdDt;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    
    public String getCreatedDtString() {
        return createdDtString;
    }

    public void setCreatedDtString(String createdDtString) throws ParseException {
        this.createdDtString = createdDtString;
        dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        setCreatedDt(dateFormat.parse(createdDtString));
    }

    public Date getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    public int getContractSignId() {
        return contractSignId;
    }

    public void setContractSignId(int contractSignId) {
        this.contractSignId = contractSignId;
    }

    public String getContractSignDtString() {
        return contractSignDtString;
    }

    public void setContractSignDtString(String contractSignDtString) throws ParseException {
        dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        this.contractSignDtString = contractSignDtString;
        setContractSignDt(dateFormat.parse(contractSignDtString));
    }

    public String getLastDtContractDtString() {
        return lastDtContractDtString;
    }

    public void setLastDtContractDtString(String lastDtContractDtString) throws ParseException {
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        this.lastDtContractDtString = lastDtContractDtString;
        setLastDtContractDt(dateFormat.parse(lastDtContractDtString));
    }

    public Date getContractSignDt() {
        return contractSignDt;
    }

    public void setContractSignDt(Date contractSignDt) {
        this.contractSignDt = contractSignDt;
    }

    public String getContractSignLocation() {
        return contractSignLocation;
    }

    public void setContractSignLocation(String contractSignLocation) {
        this.contractSignLocation = contractSignLocation;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public String getIsPubAggOnWeb() {
        return isPubAggOnWeb;
    }

    public void setIsPubAggOnWeb(String isPubAggOnWeb) {
        this.isPubAggOnWeb = isPubAggOnWeb;
    }

    public Date getLastDtContractDt() {
        return lastDtContractDt;
    }

    public void setLastDtContractDt(Date lastDtContractDt) {
        this.lastDtContractDt = lastDtContractDt;
    }

    public int getNoaId() {
        return noaId;
    }

    public void setNoaId(int noaId) {
        this.noaId = noaId;
    }

    public String getWitnessInfo() {
        return witnessInfo;
    }

    public void setWitnessInfo(String witnessInfo) {
        this.witnessInfo = witnessInfo;
    }

    public String getIsRepeatOrder() {
        return isRepeatOrder;
    }

    public void setIsRepeatOrder(String isRepeatOrder) {
        this.isRepeatOrder = isRepeatOrder;
    }

    public String getPaymentTerms() {
        return paymentTerms;
    }

    public void setPaymentTerms(String paymentTerms) {
        this.paymentTerms = paymentTerms;
    }
}
