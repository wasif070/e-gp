/**
 * 
 */
package com.cptu.egp.eps.web.controller;

/**
 * This class is an annotated controller using spring 2.5 annotation for ComplaintManagementSystem
 *
 * @version 9/6/2011
 * @author teja.ravali
 * @project e-GP-ComplaintManagement_Module
 * @section 01
 */
import java.lang.Integer;
import java.util.*;
import java.io.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import com.cptu.egp.eps.model.table.TblComplaintType;
import com.cptu.egp.eps.model.table.TblComplaintMaster;
import com.cptu.egp.eps.service.serviceinterface.ComplaintMgmtService;
import com.cptu.egp.eps.model.table.TblEmployeeTrasfer;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.model.table.TblTendererMaster;
import com.cptu.egp.eps.model.table.TblComplaintLevel;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblUserTypeMaster;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.service.serviceimpl.UserTypeServiceImpl;
import com.cptu.egp.eps.service.serviceimpl.UserRegisterServiceImpl;
import com.cptu.egp.eps.model.table.TblComplaintHistory;
import com.cptu.egp.eps.model.table.TblComplaintDocs;
import com.cptu.egp.eps.model.table.TblReviewPanel;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import com.cptu.egp.eps.web.servicebean.MessageProcessSrBean;
import com.cptu.egp.eps.model.table.TblDesignationMaster;
import com.cptu.egp.eps.model.table.TblEmployeeMaster;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import org.springframework.context.ApplicationContext;
import com.cptu.egp.eps.web.utility.XMLReader;
import com.cptu.egp.eps.dao.generic.ComplaintMgmtConstants;
import com.cptu.egp.eps.dao.generic.Complaint_Level_enum;
import com.cptu.egp.eps.dao.generic.User_Type_enum;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.AccPaymentService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.CheckExtension;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SHA1HashEncryption;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.File;

@Controller
public class ComplManagController {

    final static Logger log = Logger.getLogger(ComplManagController.class);
    private static final String destinationDir = FilePathUtility.getFilePath().get("ComplaintDocUpload");
    private String destinationComplainDir = "";
    private ComplaintMgmtService mgmtService;
    private UserTypeServiceImpl userTypeService;
    private UserRegisterServiceImpl tendererMasterService;
    private ApplicationContext context;
    private TenderCommonService tenderCommonService;
    private CommonService commonservice;
    private UserRegisterService registerService;
    private AccPaymentService accPaymentService;
    private MakeAuditTrailService AuditTrail;

    @Autowired
    public void setContext(ApplicationContext context) {
        this.context = context;
    }
    private MessageProcessSrBean message = new MessageProcessSrBean();

    /**
     * service classes injection into the controller using @Autowired annotation.
     */
    @Autowired
    public void setMgmtService(ComplaintMgmtService mgmtService) {
        this.mgmtService = mgmtService;

    }

    @Autowired
    public void setUserTypeService(UserTypeServiceImpl userTypeService) {
        this.userTypeService = userTypeService;

    }

    @Autowired
    public void settendererMasterService(UserRegisterServiceImpl tendererMasterService) {
        this.tendererMasterService = tendererMasterService;

    }

    @Autowired
    public void setTenderCommonService(TenderCommonService tenderCommonService) {
        this.tenderCommonService = tenderCommonService;
    }

    @Autowired
    public void setCommonservice(CommonService commonservice) {
        this.commonservice = commonservice;
    }

    @Autowired
    public void setRegisterService(UserRegisterService registerService) {
        this.registerService = registerService;
    }

    @Autowired
    public void setAccPaymentService(AccPaymentService accPaymentService) {
        this.accPaymentService = accPaymentService;
    }

    @Autowired
    public void setAuditTrail(MakeAuditTrailService AuditTrail) {
        this.AuditTrail = AuditTrail;
    }

    @RequestMapping(value = "/resources/common/ViewComplaintsCPTU123345.htm")
    public ModelAndView viewComplCPTU(HttpServletRequest req, HttpServletResponse res) {
        ModelAndView mov;
        try {
            if (req.getParameter("reviewPanel") != null) {
                try {
                    String panelId = req.getParameter("reviewPanel");
                    int getReviewPanel = Integer.parseInt(panelId);
                    if (getReviewPanel != 0) {
                        int complId = Integer.parseInt(req.getParameter("complaintId"));
                        log.debug("this is review panel on second call to view the complaint by dgcptu " + getReviewPanel);
                        List<TblComplaintMaster> complaintMasterList = mgmtService.getComplaintById("complaintId", Operation_enum.EQ, complId);
                        TblComplaintMaster complaintmaster = complaintMasterList.get(0);
                        complaintmaster.setPanelId(getReviewPanel);
                        mgmtService.updateMaster(complaintmaster);
                    }
                } catch (Exception e) {
                    log.error("EXCEPTION AT METHOD: complaintFeePaymentSave(LINE:335) :" + e);
                    return new ModelAndView("/StackTrace");
                }
            }

            mov = new ModelAndView("/complaintMgmt/view_complaints_CPTU");

            List<TblReviewPanel> lstReviewPanels = mgmtService.getReviewPanels();
            List<Integer> lstCompaintsCount = new ArrayList<Integer>();
            List<TblComplaintMaster> lstComplaintMaster = null;
            if (lstReviewPanels != null) {
                for (TblReviewPanel reviewPanel : lstReviewPanels) {
                    lstComplaintMaster = mgmtService.getPendingComplaintsByReviewPanel("panelId", Operation_enum.EQ, reviewPanel.getReviewPanelId());
                    lstCompaintsCount.add((Integer) lstComplaintMaster.size());
                }
                mov.addObject("lstReviewPanels", lstReviewPanels);
                mov.addObject("lstCompaintsCount", lstCompaintsCount);
            }
            log.debug("View Complaints CPTU page calling ");
            return mov;
        } catch (Exception e) {
            log.error("EXCEPTION AT METHOD: complaintFeePaymentSave(LINE:335) :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    /**
     * Maps the request specified in value to this method using @RequestMapping
     * @param tenderId is retrieved from request and mapped to id
     * @param req,res,map are the instances of HttpServletRequest,HttpServletResponse,ModelMap respectively
     * @return  ModelAndView object to redirect to compl_mgmt_tenderer jsp
     */
    @RequestMapping(value = "/tenderer/complaint.htm")
    public ModelAndView complManagement(@RequestParam("tenderId") Integer id, ModelMap map, HttpServletRequest req, HttpServletResponse res) {

        ModelAndView modelAndView;
        try {
            int userId = 0;
            if (req.getSession().getAttribute("userId") != null) {
                userId = Integer.parseInt(req.getSession().getAttribute("userId").toString());
            }
            log.debug("In complaint level enum values>>>>>>> ******Complaint_Level_enum.PE : Complaint_Level_enum..getLevel()***" + Complaint_Level_enum.PE + ":" + Complaint_Level_enum.PE.getLevel());
            List complaintList = mgmtService.getComplaintTenderer(id, userId);
            if (complaintList != null && complaintList.size() >= 1) {
                log.debug("list size of the complaints is" + complaintList.size());
            }
            modelAndView = new ModelAndView("compl_mgmt_tenderer", "complaintList", complaintList);
            req.setAttribute("complaintList", complaintList);
            modelAndView.addObject("message", id);
            return modelAndView;
        } catch (Exception e) {

            log.error("EXCEPTION AT METHOD: complaintPayment(LINE:105) :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    /**
     * Maps the request specified in value to this method using @RequestMapping to register a complaint by a tenderer.
     * Complaint Types are retrieved from database ,which is to be selected by tenderer
     *
     * @param tenderId is retrieved from request and mapped to id
     * @param req,res,map are the instances of HttpServletRequest,HttpServletResponse,ModelMap respectively
     * @return  ModelAndView object to redirect to register_complaint jsp
     */
    @RequestMapping(value = "/tenderer/fileComplaint.htm")
    public ModelAndView fileComplaint(@RequestParam("tenderId") Integer id, ModelMap map, HttpServletRequest req, HttpServletResponse res) {

        log.debug("in filecomplaint jsp tender id is" + id);
        log.debug("In fileComplaint........regardingtenders.size()" + mgmtService.getComplaintTypes().size());
        return new ModelAndView("register_complaint", "regardingtenders", mgmtService.getComplaintTypes());
    }

    @RequestMapping(value = "/tenderer/ViewInstruction.htm")
    public ModelAndView ViewInstruction(HttpServletRequest req, HttpServletResponse res) {
        return new ModelAndView("ViewInstruction");
    }

    /**
     * Maps the request specified in value to this method using @RequestMapping to register a complaint by a tenderer
     * @param int complaintId is retrieved from request and mapped to id
     * @param req,res,map are the instances of HttpServletRequest,HttpServletResponse,ModelMap respectively
     * @return  ModelAndView object to redirect to view_compl_History_Tenderer jsp
     * To view the recent history of the complaint when either of the tenderer or PE or HOPE or Secretary logs in
     */
    @RequestMapping(value = {"/tenderer/ViewComplHistoryTenderer.htm", "/ViewComplHistoryTenderer.htm", "/resources/common/ViewComplHistoryTenderer.htm"})
    public ModelAndView viewComplaintHistory(@RequestParam("complaintId") Integer id, ModelMap map, HttpServletRequest req, HttpServletResponse res, HttpSession session) {

        ModelAndView mav;
        try {

            /** create  lists to get records from TblCompalintHistory table  */
            List listPe = null;
            List listHope = null;
            List listSecretary = null;
            List listReviewPanel = null;
            List listPeTender = null;
            List listHopeTender = null;
            List listSecretaryTender = null;
            List listReviewPanelTender = null;
            ArrayList list = new ArrayList();
            TblComplaintHistory pehistory = null;
            List docsListHistory = null;

            TblComplaintMaster complaint = mgmtService.getComplaintDetails(id);
            List<TblComplaintDocs> docList = mgmtService.getDocsList("complaintMaster.complaintId", Operation_enum.EQ, id);
            if (docList != null && docList.size() >= 1) {
                log.debug("complaint docs list size is" + docList.size());
            }
            session.setAttribute("tenderId", complaint.getTenderMaster().getTenderId());
            ArrayList<TblComplaintHistory> historyList = new ArrayList<TblComplaintHistory>();
            int complaintLevelId = complaint.getComplaintLevel().getComplaintLevelId();
            log.debug("complaintlevel id is" + complaintLevelId);
            /** retrieving  PE related history by using complaintLevelId*/
            if (complaintLevelId == Complaint_Level_enum.PE.getLevel() || complaintLevelId == Complaint_Level_enum.HOPE.getLevel() || complaintLevelId == Complaint_Level_enum.SECRETARY.getLevel() || complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                log.debug("complaint level is PE");
                listPe = mgmtService.getComplaintHistory(id, Complaint_Level_enum.PE.getLevel(), User_Type_enum.OFFICER.getType());
                listPeTender = mgmtService.getComplaintHistory(id, Complaint_Level_enum.PE.getLevel(), User_Type_enum.TENDERER.getType());
                Map pe = new HashMap();


                if (listPeTender != null && listPeTender.size() >= 1) {

                    docsListHistory = mgmtService.getDocs(id, Complaint_Level_enum.PE.getLevel());

                    pe.put("tendererComments", ((TblComplaintHistory) listPeTender.get(0)).getComments());
                    pe.put("level", ((TblComplaintHistory) listPeTender.get(0)).getComplaintLevel().getComplaintLevel());

                    pe.put("response", ((TblComplaintHistory) listPeTender.get(0)).getComplaintStatus());

                    pe.put("responseDate", ((TblComplaintHistory) listPeTender.get(0)).getCommentsdt());
                    pe.put("levelId", ((TblComplaintHistory) listPeTender.get(0)).getComplaintLevel().getComplaintLevelId());
                    pe.put("complaintId", ((TblComplaintHistory) listPeTender.get(0)).getComplaint().getComplaintId());
                    pe.put("docsListHistory", docsListHistory);
                    if (listPe != null && listPe.size() >= 1) {
                        pe.put("officerComments", ((TblComplaintHistory) listPe.get(0)).getComments());
                        pe.put("level", ((TblComplaintHistory) listPe.get(0)).getComplaintLevel().getComplaintLevel());
                        pe.put("response", ((TblComplaintHistory) listPe.get(0)).getComplaintStatus());
                        pe.put("responseDate", ((TblComplaintHistory) listPe.get(0)).getCommentsdt());
                        pe.put("levelId", ((TblComplaintHistory) listPe.get(0)).getComplaintLevel().getComplaintLevelId());
                        pe.put("complaintId", ((TblComplaintHistory) listPe.get(0)).getComplaint().getComplaintId());
                    }
                    pe.put("ResolvedStatus", complaint.getPeResolved());
                    list.add(pe);
                    log.debug(" PE list retrieved***************");

                }
            }

            /** retrieving  HOPE related history by using complaintLevelId*/
            if (complaintLevelId == Complaint_Level_enum.HOPE.getLevel() || complaintLevelId == Complaint_Level_enum.SECRETARY.getLevel() || complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                listHope = mgmtService.getComplaintHistory(id, Complaint_Level_enum.HOPE.getLevel(), User_Type_enum.OFFICER.getType());
                listHopeTender = mgmtService.getComplaintHistory(id, Complaint_Level_enum.HOPE.getLevel(), User_Type_enum.TENDERER.getType());
                Map hope = new HashMap();
                if (listHopeTender != null && listHopeTender.size() >= 1) {
                    docsListHistory = mgmtService.getDocs(id, Complaint_Level_enum.HOPE.getLevel());
                    hope.put("tendererComments", ((TblComplaintHistory) listHopeTender.get(0)).getComments());
                    hope.put("level", ((TblComplaintHistory) listHopeTender.get(0)).getComplaintLevel().getComplaintLevel());
                    hope.put("response", ((TblComplaintHistory) listHopeTender.get(0)).getComplaintStatus());
                    hope.put("responseDate", ((TblComplaintHistory) listHopeTender.get(0)).getCommentsdt());
                    hope.put("levelId", ((TblComplaintHistory) listHopeTender.get(0)).getComplaintLevel().getComplaintLevelId());
                    hope.put("complaintId", ((TblComplaintHistory) listHopeTender.get(0)).getComplaint().getComplaintId());
                    hope.put("docsListHistory", docsListHistory);
                    if (listHope != null && listHope.size() >= 1) {

                        hope.put("officerComments", ((TblComplaintHistory) listHope.get(0)).getComments());
                        hope.put("level", ((TblComplaintHistory) listHope.get(0)).getComplaintLevel().getComplaintLevel());
                        hope.put("response", ((TblComplaintHistory) listHope.get(0)).getComplaintStatus());
                        hope.put("responseDate", ((TblComplaintHistory) listHope.get(0)).getCommentsdt());
                        hope.put("levelId", ((TblComplaintHistory) listHope.get(0)).getComplaintLevel().getComplaintLevelId());
                        hope.put("complaintId", ((TblComplaintHistory) listHope.get(0)).getComplaint().getComplaintId());
                    }
                    hope.put("ResolvedStatus", complaint.getHopeResolved());
                    list.add(hope);
                    log.debug("after Hope ***************");
                }
            }
            //for secretary
            if (complaintLevelId == Complaint_Level_enum.SECRETARY.getLevel() || complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                listSecretary = mgmtService.getComplaintHistory(id, Complaint_Level_enum.SECRETARY.getLevel(), User_Type_enum.OFFICER.getType());
                listSecretaryTender = mgmtService.getComplaintHistory(id, Complaint_Level_enum.SECRETARY.getLevel(), User_Type_enum.TENDERER.getType());
                Map secretary = new HashMap();
                if (listSecretaryTender != null && listSecretaryTender.size() >= 1) {
                    docsListHistory = mgmtService.getDocs(id, Complaint_Level_enum.SECRETARY.getLevel());
                    secretary.put("tendererComments", ((TblComplaintHistory) listSecretaryTender.get(0)).getComments());
                    secretary.put("level", ((TblComplaintHistory) listSecretaryTender.get(0)).getComplaintLevel().getComplaintLevel());
                    secretary.put("response", ((TblComplaintHistory) listSecretaryTender.get(0)).getComplaintStatus());
                    secretary.put("responseDate", ((TblComplaintHistory) listSecretaryTender.get(0)).getCommentsdt());
                    secretary.put("levelId", ((TblComplaintHistory) listSecretaryTender.get(0)).getComplaintLevel().getComplaintLevelId());
                    secretary.put("complaintId", ((TblComplaintHistory) listSecretaryTender.get(0)).getComplaint().getComplaintId());
                    secretary.put("docsListHistory", docsListHistory);
                    if (listSecretary != null && listSecretary.size() >= 1) {
                        secretary.put("officerComments", ((TblComplaintHistory) listSecretary.get(0)).getComments());
                        secretary.put("level", ((TblComplaintHistory) listSecretary.get(0)).getComplaintLevel().getComplaintLevel());
                        secretary.put("response", ((TblComplaintHistory) listSecretary.get(0)).getComplaintStatus());
                        secretary.put("responseDate", ((TblComplaintHistory) listSecretary.get(0)).getCommentsdt());
                        secretary.put("levelId", ((TblComplaintHistory) listSecretary.get(0)).getComplaintLevel().getComplaintLevelId());
                        secretary.put("complaintId", ((TblComplaintHistory) listSecretary.get(0)).getComplaint().getComplaintId());
                    }
                    secretary.put("ResolvedStatus", complaint.getSecResolved());
                    list.add(secretary);
                    log.debug("after Secretary ***************");
                }
            }

            //for review panel
            if (complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                listReviewPanel = mgmtService.getComplaintHistory(id, Complaint_Level_enum.REVIEW_PANEL.getLevel(), User_Type_enum.OFFICER.getType());
                listReviewPanelTender = mgmtService.getComplaintHistory(id, Complaint_Level_enum.REVIEW_PANEL.getLevel(), User_Type_enum.REVIEWPANELMEMBER.getType());
                Map review = new HashMap();
                if (listReviewPanelTender != null && listReviewPanelTender.size() >= 1) {
                    docsListHistory = mgmtService.getDocs(id, Complaint_Level_enum.REVIEW_PANEL.getLevel());
                    review.put("tendererComments", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComments());
                    review.put("level", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComplaintLevel().getComplaintLevel());
                    review.put("response", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComplaintStatus());
                    review.put("responseDate", ((TblComplaintHistory) listReviewPanelTender.get(0)).getCommentsdt());
                    review.put("levelId", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComplaintLevel().getComplaintLevelId());
                    review.put("complaintId", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComplaint().getComplaintId());
                    review.put("docsListHistory", docsListHistory);
                    if (listReviewPanel != null && listReviewPanel.size() >= 1) {
                        review.put("officerComments", ((TblComplaintHistory) listReviewPanel.get(0)).getComments());
                        review.put("level", ((TblComplaintHistory) listReviewPanel.get(0)).getComplaintLevel().getComplaintLevel());
                        review.put("response", ((TblComplaintHistory) listReviewPanel.get(0)).getComplaintStatus());
                        review.put("responseDate", ((TblComplaintHistory) listReviewPanel.get(0)).getCommentsdt());
                        review.put("levelId", ((TblComplaintHistory) listReviewPanel.get(0)).getComplaintLevel().getComplaintLevelId());
                        review.put("complaintId", ((TblComplaintHistory) listReviewPanel.get(0)).getComplaint().getComplaintId());
                    }
                    review.put("ResolvedStatus", complaint.getRpResolved());
                    list.add(review);
                    log.debug("after review panel ***************");
                }
            }
            log.debug("after ifff************");
            mav = new ModelAndView("view_compl_History_Tenderer");
            mav.addObject("complaint", complaint);
            mav.addObject("docList", docList);
            mav.addObject("historyList", list);

            String uid = session.getAttribute("userId").toString();
            int userId = Integer.parseInt(uid);
            if (complaintLevelId == Complaint_Level_enum.PE.getLevel()) {
                if (req.getSession().getAttribute("userTypeId").toString().equals("2")) {
                    AuditTrail.generateAudit(new AuditTrail(req.getHeader("X-FORWARDED-FOR") != null ? req.getHeader("X-FORWARDED-FOR") : req.getRemoteAddr(), req.getSession().getAttribute("sessionId"), req.getSession().getAttribute("userTypeId"), req.getHeader("referer")), id, "tenderId", EgpModule.Complaint_Management.getName(), "View Complaint", "");
                } else {
                    AuditTrail.generateAudit(new AuditTrail(req.getHeader("X-FORWARDED-FOR") != null ? req.getHeader("X-FORWARDED-FOR") : req.getRemoteAddr(), req.getSession().getAttribute("sessionId"), req.getSession().getAttribute("userTypeId"), req.getHeader("referer")), id, "tenderId", EgpModule.Complaint_Management.getName(), "View Complaint by PE", "");
                }
            } else if (complaintLevelId == Complaint_Level_enum.HOPE.getLevel()) {
                AuditTrail.generateAudit(new AuditTrail(req.getHeader("X-FORWARDED-FOR") != null ? req.getHeader("X-FORWARDED-FOR") : req.getRemoteAddr(), req.getSession().getAttribute("sessionId"), req.getSession().getAttribute("userTypeId"), req.getHeader("referer")), id, "tenderId", EgpModule.Complaint_Management.getName(), "View Complaint by Hope", "");
            } else if (complaintLevelId == Complaint_Level_enum.SECRETARY.getLevel()) {
                AuditTrail.generateAudit(new AuditTrail(req.getHeader("X-FORWARDED-FOR") != null ? req.getHeader("X-FORWARDED-FOR") : req.getRemoteAddr(), req.getSession().getAttribute("sessionId"), req.getSession().getAttribute("userTypeId"), req.getHeader("referer")), id, "tenderId", EgpModule.Complaint_Management.getName(), "View Complaint by Secretary", "");
            } else if (complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                AuditTrail.generateAudit(new AuditTrail(req.getHeader("X-FORWARDED-FOR") != null ? req.getHeader("X-FORWARDED-FOR") : req.getRemoteAddr(), req.getSession().getAttribute("sessionId"), req.getSession().getAttribute("userTypeId"), req.getHeader("referer")), id, "tenderId", EgpModule.Complaint_Management.getName(), "View Complaint by Review Panel", "");
            }
            log.debug("in the view controller");
            log.debug("IN view controller id is ............................." + id);
            return mav;
        } catch (Exception e) {
            log.error("EXCEPTION AT METHOD: viewComplaintHistory(LINE:166) :" + e);
            return new ModelAndView("/StackTrace");
        }

    }

    /**
     * @param int complaintId is retrieved from request
     * @param int complaintLveleId is retrieved from request
     * method to show the entire  history of a complaint based on complaintlevelId
     * @return modelAndView object to render the response in view_history_tenderer jsp
     * */
    @RequestMapping(value = {"/tenderer/ViewHistory_tenderer.htm", "/resources/common/ViewHistory_tenderer.htm", "/ViewHistory_tenderer.htm"})
    public ModelAndView viewHistoryTenderer(@RequestParam("complaintId") Integer id, @RequestParam("complaintLevelId") Integer levelId, ModelMap map, HttpServletRequest req, HttpServletResponse res, HttpSession session) {

        ModelAndView modelAndView;
        try {
            log.debug("in view history compl level id" + levelId);
            log.debug("in view history compl id" + id);
            List listTenderer = null;
            TblComplaintMaster complaintmaster = null;
            listTenderer = mgmtService.getComplaintHistoryDetails(id, levelId);
            /** invoking a service class method  */
            List<TblComplaintMaster> complaintMasterList = mgmtService.getComplaintById("complaintId", Operation_enum.EQ, id);
            if (complaintMasterList != null && complaintMasterList.size() >= 1) {
                complaintmaster = complaintMasterList.get(0);
            } else {
                log.debug("list is null");
            }
            log.debug("in the view controller for view history");

            List<Object[]> complainDocs = mgmtService.getDocs(id, levelId);

            modelAndView = new ModelAndView("view_history_tenderer");
            modelAndView.addObject("complaint", complaintmaster);
            modelAndView.addObject("listTenderer", listTenderer);
            modelAndView.addObject("complainDocs", complainDocs);
            return modelAndView;


        } catch (Exception e) {
            log.error("EXCEPTION AT METHOD: viewHistoryTenderer(LINE:336) :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    /**
     * @param int complaintId retrieved from request and binded to id using @RequestParam
     * @param int complaintLevelId retrieved from request and binded to complaintLevelId using @RequestParam
     * method is to get the relevant documents pertaining to one complaint and a specific role
     * @return modelAndView object to render the response in view_uploaded_doc jsp
     * */
    @RequestMapping(value = {"/tenderer/ViewUploadeddoc.htm", "/resources/common/ViewUploadeddoc.htm", "/ViewUploadeddoc.htm"})
    public ModelAndView viewdoc(@RequestParam("complaintId") Integer id, @RequestParam("complaintLevelId") Integer complaintLevelId, ModelMap map, HttpServletRequest req, HttpServletResponse res) {
        ModelAndView modelAndView;
        try {
            log.debug("in the view controller for view documents history");

            /** calling service class method to get the documents by passing complaintId and complaintLevelId  */
            List docsList = mgmtService.getDocs(id, complaintLevelId);
            modelAndView = new ModelAndView("view_uploaded_doc", "docsList", docsList);

            return modelAndView;
        } catch (Exception e) {

            log.error("EXCEPTION AT METHOD: viewHistoryTenderer(LINE:336) :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    /**
     * method to render history,complaint details before escalating a complaint from one level to another.,from pe to hope or hope to secretary or
     * secretary to review panel
     * @param complaintid , @param complaintlevelid retrieved from request
     * @return mav to render the historyList,doclist,complaint,secretary list in escalate_compl jsp
     */
    @RequestMapping(value = {"/EscalateCompl.htm", "/tenderer/EscalateCompl.htm"})
    public ModelAndView escalate(@RequestParam("complaintId") Integer id, @RequestParam("complaintLevelId") Integer levelId, ModelMap map, HttpServletRequest req, HttpServletResponse res, HttpSession session) {
        log.debug("in the view controller ******* escalate");

        ModelAndView mav;
        try {
            TblComplaintHistory history = new TblComplaintHistory();
            List listPe = null;
            List listHope = null;
            List listSecretary = null;
            List listReviewPanel = null;
            List listPeTender = null;
            List listHopeTender = null;
            List listSecretaryTender = null;
            List listReviewPanelTender = null;
            ArrayList list = new ArrayList();
            List docsListHistory = null;
            String role = "";
            TblEmployeeTrasfer et = null;
            List<TblEmployeeTrasfer> seceretaryListOffices = new ArrayList<TblEmployeeTrasfer>();

            TblComplaintMaster complaint = mgmtService.getComplaintDetails(id);
            List<TblComplaintDocs> docList = mgmtService.getDocsList("complaintMaster.complaintId", Operation_enum.EQ, id);

            log.debug("complaint docs list size is" + docList.size());
            complaint.setComplaintStatus("Rejected");

            ArrayList<TblComplaintHistory> historyList = new ArrayList<TblComplaintHistory>();

            int complaintLevelId = complaint.getComplaintLevel().getComplaintLevelId();
            log.debug("complaintlevel id is" + complaintLevelId);

            List<TblTenderDetails> tenderDetails = mgmtService.findOffice("tblTenderMaster.tenderId", Operation_enum.EQ, complaint.getTenderMaster().getTenderId());
            int officeid = tenderDetails.get(0).getOfficeId();
            log.debug("tbl tender details officeid is" + officeid);

            if (levelId == Complaint_Level_enum.HOPE.getLevel()) {

                role = "Secretary";
                /** invoking method to get the upper level office hierarchies for HOPE */
                List officehier = mgmtService.getOfficeHirarchies(officeid);

                int[] officeIds = new int[officehier.size()];
                log.debug("list size for secretary offices" + officehier.size());
                for (int i = 0; i < officehier.size(); i++) {
                    officeIds[i] = ((Integer) officehier.get(i)).intValue();
                    log.debug("officeIds: " + officeIds[i]);
                }
                /** invoking method to get secretaries list by passing officeIds array and role */
                List secretaryList = mgmtService.getGovUserSecretaries(officeIds, role);

                if (secretaryList != null && secretaryList.size() >= 1) {
                    log.debug("secretary list size is" + secretaryList.size());
                }
                int[] secretary = new int[secretaryList.size()];
                for (int i = 0; i < secretaryList.size(); i++) {
                    secretary[i] = ((Integer) secretaryList.get(i)).intValue();
                    /** to get govUserId of each secretary */
                    List<TblEmployeeTrasfer> lstEmployeeTrasfer = mgmtService.findByGovUser("govUserId", Operation_enum.EQ, secretary[i]);
                    et = lstEmployeeTrasfer.get(0);
                    log.debug("et is " + et);
                    log.debug("et name is" + et.getEmployeeName());
                    seceretaryListOffices.add(et);
                    log.debug("secretaryIds: " + secretary[i]);
                }
            }

            //for PE
            if (complaintLevelId == Complaint_Level_enum.PE.getLevel() || complaintLevelId == Complaint_Level_enum.HOPE.getLevel() || complaintLevelId == Complaint_Level_enum.SECRETARY.getLevel() || complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                log.debug("complaint level is PE");
                listPe = mgmtService.getComplaintHistory(id, Complaint_Level_enum.PE.getLevel(), User_Type_enum.OFFICER.getType());
                listPeTender = mgmtService.getComplaintHistory(id, Complaint_Level_enum.PE.getLevel(), User_Type_enum.TENDERER.getType());
                Map pe = new HashMap();

                if (listPeTender != null && listPeTender.size() >= 1) {

                    docsListHistory = mgmtService.getDocs(id, Complaint_Level_enum.PE.getLevel());
                    pe.put("tendererComments", ((TblComplaintHistory) listPeTender.get(0)).getComments());
                    pe.put("level", ((TblComplaintHistory) listPeTender.get(0)).getComplaintLevel().getComplaintLevel());
                    pe.put("response", ((TblComplaintHistory) listPeTender.get(0)).getComplaintStatus());
                    pe.put("responseDate", ((TblComplaintHistory) listPeTender.get(0)).getCommentsdt());
                    pe.put("levelId", ((TblComplaintHistory) listPeTender.get(0)).getComplaintLevel().getComplaintLevelId());
                    pe.put("complaintId", ((TblComplaintHistory) listPeTender.get(0)).getComplaint().getComplaintId());
                    pe.put("docsListHistory", docsListHistory);
                    if (listPe != null && listPe.size() >= 1) {
                        pe.put("officerComments", ((TblComplaintHistory) listPe.get(0)).getComments());
                        pe.put("level", ((TblComplaintHistory) listPe.get(0)).getComplaintLevel().getComplaintLevel());
                        pe.put("response", ((TblComplaintHistory) listPe.get(0)).getComplaintStatus());
                        pe.put("responseDate", ((TblComplaintHistory) listPe.get(0)).getCommentsdt());
                        pe.put("levelId", ((TblComplaintHistory) listPe.get(0)).getComplaintLevel().getComplaintLevelId());
                        pe.put("complaintId", ((TblComplaintHistory) listPe.get(0)).getComplaint().getComplaintId());
                    }
                    list.add(pe);
                    log.debug("after PE ***************");
                }
            }

            /** to view the recent history of hope */
            if (complaintLevelId == Complaint_Level_enum.HOPE.getLevel() || complaintLevelId == Complaint_Level_enum.SECRETARY.getLevel() || complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                listHope = mgmtService.getComplaintHistory(id, Complaint_Level_enum.HOPE.getLevel(), User_Type_enum.OFFICER.getType());
                listHopeTender = mgmtService.getComplaintHistory(id, Complaint_Level_enum.HOPE.getLevel(), User_Type_enum.TENDERER.getType());
                /**creating a HashMap  for HOPE */
                Map hope = new HashMap();
                if (listHopeTender != null && listHopeTender.size() >= 1) {
                    docsListHistory = mgmtService.getDocs(id, Complaint_Level_enum.HOPE.getLevel());
                    hope.put("tendererComments", ((TblComplaintHistory) listHopeTender.get(0)).getComments());
                    hope.put("level", ((TblComplaintHistory) listHopeTender.get(0)).getComplaintLevel().getComplaintLevel());
                    hope.put("response", ((TblComplaintHistory) listHopeTender.get(0)).getComplaintStatus());
                    hope.put("responseDate", ((TblComplaintHistory) listHopeTender.get(0)).getCommentsdt());
                    hope.put("levelId", ((TblComplaintHistory) listHopeTender.get(0)).getComplaintLevel().getComplaintLevelId());
                    hope.put("complaintId", ((TblComplaintHistory) listHopeTender.get(0)).getComplaint().getComplaintId());
                    hope.put("docsListHistory", docsListHistory);
                    if (listHope != null && listHope.size() >= 1) {
                        hope.put("officerComments", ((TblComplaintHistory) listHope.get(0)).getComments());
                        hope.put("level", ((TblComplaintHistory) listHope.get(0)).getComplaintLevel().getComplaintLevel());
                        hope.put("response", ((TblComplaintHistory) listHope.get(0)).getComplaintStatus());
                        hope.put("responseDate", ((TblComplaintHistory) listHope.get(0)).getCommentsdt());
                        hope.put("levelId", ((TblComplaintHistory) listHope.get(0)).getComplaintLevel().getComplaintLevelId());
                        hope.put("complaintId", ((TblComplaintHistory) listHope.get(0)).getComplaint().getComplaintId());

                    }
                    list.add(hope);
                    log.debug("after Hope ***************");
                }
            }

            /**creating a HashMap  for secretary */
            if (complaintLevelId == Complaint_Level_enum.SECRETARY.getLevel() || complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                listSecretary = mgmtService.getComplaintHistory(id, Complaint_Level_enum.SECRETARY.getLevel(), User_Type_enum.OFFICER.getType());
                listSecretaryTender = mgmtService.getComplaintHistory(id, Complaint_Level_enum.SECRETARY.getLevel(), User_Type_enum.TENDERER.getType());
                Map secretary = new HashMap();
                if (listSecretaryTender != null && listSecretaryTender.size() >= 1) {
                    docsListHistory = mgmtService.getDocs(id, Complaint_Level_enum.SECRETARY.getLevel());
                    secretary.put("tendererComments", ((TblComplaintHistory) listSecretaryTender.get(0)).getComments());
                    secretary.put("level", ((TblComplaintHistory) listSecretaryTender.get(0)).getComplaintLevel().getComplaintLevel());
                    secretary.put("response", ((TblComplaintHistory) listSecretaryTender.get(0)).getComplaintStatus());
                    secretary.put("responseDate", ((TblComplaintHistory) listSecretaryTender.get(0)).getCommentsdt());
                    secretary.put("levelId", ((TblComplaintHistory) listSecretaryTender.get(0)).getComplaintLevel().getComplaintLevelId());
                    secretary.put("complaintId", ((TblComplaintHistory) listSecretaryTender.get(0)).getComplaint().getComplaintId());
                    secretary.put("docsListHistory", docsListHistory);
                    if (listSecretary != null && listSecretary.size() >= 1) {
                        secretary.put("officerComments", ((TblComplaintHistory) listSecretary.get(0)).getComments());
                        secretary.put("level", ((TblComplaintHistory) listSecretary.get(0)).getComplaintLevel().getComplaintLevel());
                        secretary.put("response", ((TblComplaintHistory) listSecretary.get(0)).getComplaintStatus());
                        secretary.put("responseDate", ((TblComplaintHistory) listSecretary.get(0)).getCommentsdt());
                        secretary.put("levelId", ((TblComplaintHistory) listSecretary.get(0)).getComplaintLevel().getComplaintLevelId());
                        secretary.put("complaintId", ((TblComplaintHistory) listSecretary.get(0)).getComplaint().getComplaintId());
                    }
                    list.add(secretary);
                    log.debug("after Secretary ***************");
                }
            }

            /**creating a HashMap  for review panel*/
            if (complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                listReviewPanel = mgmtService.getComplaintHistory(id, Complaint_Level_enum.REVIEW_PANEL.getLevel(), User_Type_enum.OFFICER.getType());
                listReviewPanelTender = mgmtService.getComplaintHistory(id, Complaint_Level_enum.REVIEW_PANEL.getLevel(), User_Type_enum.TENDERER.getType());
                Map review = new HashMap();
                if (listReviewPanelTender != null && listReviewPanelTender.size() >= 1) {
                    docsListHistory = mgmtService.getDocs(id, Complaint_Level_enum.REVIEW_PANEL.getLevel());
                    review.put("tendererComments", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComments());
                    review.put("level", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComplaintLevel().getComplaintLevel());
                    review.put("response", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComplaintStatus());
                    review.put("responseDate", ((TblComplaintHistory) listReviewPanelTender.get(0)).getCommentsdt());
                    review.put("levelId", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComplaintLevel().getComplaintLevelId());
                    review.put("complaintId", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComplaint().getComplaintId());
                    review.put("docsListHistory", docsListHistory);
                    if (listReviewPanel != null && listReviewPanel.size() >= 1) {
                        review.put("officerComments", ((TblComplaintHistory) listReviewPanel.get(0)).getComments());
                        review.put("level", ((TblComplaintHistory) listReviewPanel.get(0)).getComplaintLevel().getComplaintLevel());
                        review.put("response", ((TblComplaintHistory) listReviewPanel.get(0)).getComplaintStatus());
                        review.put("responseDate", ((TblComplaintHistory) listReviewPanel.get(0)).getCommentsdt());
                        review.put("levelId", ((TblComplaintHistory) listReviewPanel.get(0)).getComplaintLevel().getComplaintLevelId());
                        review.put("complaintId", ((TblComplaintHistory) listReviewPanel.get(0)).getComplaint().getComplaintId());
                    }
                    list.add(review);
                    log.debug("after review panel ***************");
                }
            }
            log.debug("after ifff************");


            mav = new ModelAndView("escalate_compl");
            mav.addObject("complaint", complaint);
            mav.addObject("docList", docList);
            mav.addObject("historyList", list);
            mav.addObject("secretaryList", seceretaryListOffices);
            AuditTrail.generateAudit(new AuditTrail(req.getHeader("X-FORWARDED-FOR") != null ? req.getHeader("X-FORWARDED-FOR") : req.getRemoteAddr(), req.getSession().getAttribute("sessionId"), req.getSession().getAttribute("userTypeId"), req.getHeader("referer")), id, "tenderId", EgpModule.Complaint_Management.getName(), "Escalate Complaint", "");
            log.debug("in the view controller");
            System.out.println("IN escalate /............" + id);
            return mav;
        } catch (Exception e) {

            log.error("EXCEPTION AT METHOD: escalate() :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    /**
     * METHOD to escalate a complaint from one level to  another level ,to retrieve the comments of tenderer from form
     * updating the TblComplaintMaster and creating records in TblComplaintHistory table and TblComplaintDocs
     * sending messages to relevant govUser after escalation
     * @return modelAndView to redirect to popup to upload documents
     */
    @RequestMapping({"/escalateSubmit", "/tenderer/escalateSubmit.htm"})
    public ModelAndView escalateSubmit(@RequestParam("complaintId") Integer id, @RequestParam("complaintLevelId") Integer levelId, ModelMap map, HttpServletRequest req, HttpServletResponse res, HttpSession session) {

        ModelAndView modelAndView = null;
        try {
            int userId = 0;
            String role = "";
            log.debug("in escalate complaint submit");
            int officeid = 0;
            TblComplaintHistory history = new TblComplaintHistory();
            TblComplaintMaster master = null;
            TblEmployeeTrasfer et = null;
            TblComplaintLevel complaintlevel = null;
            String utid = session.getAttribute("userTypeId").toString();
            byte usertypeid = (byte) Integer.parseInt(utid);
            List<TblUserTypeMaster> lstUsertype = userTypeService.getTblusertypemasterdao().findTblUserTypeMaster("userTypeId", Operation_enum.EQ, usertypeid);
            TblUserTypeMaster usertype = lstUsertype.get(0);

            String tendererComments = req.getParameter("txtaComments");
            log.debug("tenderer comments is" + tendererComments);

            if (session.getAttribute("userId") != null) {
                userId = Integer.parseInt(session.getAttribute("userId").toString());
            }

            List<TblComplaintMaster> complaintMasterList = mgmtService.getComplaintById("complaintId", Operation_enum.EQ, id);
            if (complaintMasterList != null && complaintMasterList.size() >= 1) {
                master = complaintMasterList.get(0);
            } else {
                log.debug(" complaintMasterList is null");
            }
            if (levelId == master.getComplaintLevel().getComplaintLevelId()) {
                List<TblTenderDetails> tenderDetails = mgmtService.findOffice("tblTenderMaster.tenderId", Operation_enum.EQ, master.getTenderMaster().getTenderId());
                if (tenderDetails != null && tenderDetails.size() >= 1) {
                    officeid = tenderDetails.get(0).getOfficeId();
                } else {
                    log.debug(" tenderdetails object is null");
                }
                log.debug("tbl tender details officeid is" + officeid);

                if (levelId == 1) {
                    role = "HOPE";
                    List list = mgmtService.getGovtUserDetails(officeid, role);
                    if (list != null && list.size() > 0) {
                        Integer hopeGovUserId = (Integer) list.get(0);
                        log.debug("gov user ID HOPE is" + hopeGovUserId);
                        List<TblEmployeeTrasfer> lstEmployeeTrasfer = mgmtService.findByGovUser("govUserId", Operation_enum.EQ, hopeGovUserId);
                        et = lstEmployeeTrasfer.get(0);
                        log.debug("gov user is" + et);
                    } else {
                        log.debug("gov user ID HOPE is null");
                    }
                } else if (levelId == 2) {
                    log.debug("in escalate secretary");
                    int govUserId = Integer.parseInt(req.getParameter("secretary"));
                    List<TblEmployeeTrasfer> lstEmployeeTrasfer = mgmtService.findByGovUser("govUserId", Operation_enum.EQ, govUserId);
                    et = lstEmployeeTrasfer.get(0);
                } else if (levelId == 3) {
                    log.debug("in escalate review panel");
                    List<TblEmployeeTrasfer> lstEmployeeTrasfer = mgmtService.findByGovUser("govUserId", Operation_enum.EQ, mgmtService.getGovUserId());
                    et = lstEmployeeTrasfer.get(0);
                }



                List<TblComplaintLevel> complaintLevelList = mgmtService.findByLevel("complaintLevelId", Operation_enum.EQ, levelId + 1);
                complaintlevel = complaintLevelList.get(0);
                master.setComplaintLevel(complaintlevel);
                master.setTendererComments(tendererComments);
                master.setComplaintStatus(ComplaintMgmtConstants.COMP_PAY_STATUS_PENDING);
                master.setLastModificationDt(new Date());
                master.setEmployeeTrasfer(et);

                /** updating TblComplaintMaster table by calling  updateMaster of service class */
                mgmtService.updateMaster(master);

                history.setComplaint(master);
                history.setGovUserId(et.getGovUserId());
                history.setTendererId(master.getTendererMaster().getTendererId());
                history.setComplaintLevel(complaintlevel);
                history.setFileStatus(ComplaintMgmtConstants.COMP_PAY_STATUS_PENDING);
                history.setComplaintStatus(ComplaintMgmtConstants.COMP_PAY_STATUS_PENDING);
                history.setPanelId(0);
                history.setUserRole(usertype);
                history.setCommentsdt(new Date());
                history.setComments(tendererComments);
                /** inserting record into TblComplaintHIstory table */
                mgmtService.addHistory(history);

                String uploadStatus = req.getParameter("uploadstatus");
                if (uploadStatus.equalsIgnoreCase(ComplaintMgmtConstants.YES)) {
                    log.debug("in if" + uploadStatus);
                    modelAndView = new ModelAndView("popup");
                    modelAndView.addObject("uploadedDocs", mgmtService.getDocsByComplaintId(master.getComplaintId()));

                } else if (uploadStatus.equalsIgnoreCase(ComplaintMgmtConstants.NO)) {
                    log.debug("in else " + uploadStatus);
                    List complaintList = mgmtService.getComplaintTenderer(Integer.parseInt(req.getParameter("tenderId")), userId);
                    if (complaintList != null && complaintList.size() >= 1) {
                        log.debug("list size of the complaints is" + complaintList.size());
                    }
                    modelAndView = new ModelAndView("compl_mgmt_tenderer", "complaintList", complaintList);
                    req.setAttribute("complaintList", complaintList);
                    modelAndView.addObject("msgg", "escalate");
                    modelAndView.addObject("master", master);

                }
                modelAndView.addObject("complaintObject", master);
                java.util.List<SPTenderCommonData> thisTenderInfoLst = tenderCommonService.returndata("tenderinfobar", master.getTenderMaster().getTenderId() + "", null);
                MailContentUtility utility = new MailContentUtility();
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                if (levelId == 1 || levelId == 2) {
                    String msgText = utility.sendMailToHighAutForEscalateComp(master.getTenderMaster().getTenderId(), thisTenderInfoLst, master.getComplaintId(), master.getTendererMaster().getFirstName(), master.getComplaintType().getComplaintType());
                    String[] strTo = {commonservice.getEmailId(et.getTblLoginMaster().getUserId() + "")};
                    String strFrom = commonservice.getEmailId(req.getSession().getAttribute("userId").toString());
                    String cc = "";
                    String subject = "e-GP: Bidder has Escalate Complaint";
                    registerService.contentAdmMsgBox(strTo[0], strFrom, subject, msgText);
                    sendMessageUtil.setEmailTo(strTo);
                    sendMessageUtil.setEmailFrom(strFrom);
                    sendMessageUtil.setEmailSub(subject);
                    sendMessageUtil.setEmailMessage(msgText);
                    sendMessageUtil.sendEmail();
                    sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
                    //sendMessageUtil.setSmsNo("919427410774");
                    StringBuilder sb = new StringBuilder();
                    sb.append("Dear User,%0AThis is to inform you that Bidder " + master.getTendererMaster().getFirstName() + " has escalate complaint for " + master.getTenderMaster().getTenderId());
                    sb.append("%0AThanks,%0AeGP System");
                    sendMessageUtil.setSmsBody(sb.toString());
                    sendMessageUtil.sendSMS();
                    strTo = null;
                    sendMessageUtil = null;
                    utility = null;
                    msgBoxContentUtility = null;
                } else {
                    String msgText = utility.sendMailToHighAutForEscalateCompforRP(master.getTenderMaster().getTenderId(), thisTenderInfoLst, master.getComplaintId(), master.getTendererMaster().getFirstName(), master.getComplaintType().getComplaintType());
                    String[] strTo = {commonservice.getEmailIdOfDG("16")};
                    String strFrom = commonservice.getEmailId(req.getSession().getAttribute("userId").toString());
                    String cc = "";
                    String subject = "e-GP:  Bidder has Esaclate Complaint";
                    registerService.contentAdmMsgBox(strTo[0], strFrom, subject, msgText);
                    sendMessageUtil.setEmailTo(strTo);
                    sendMessageUtil.setEmailFrom(strFrom);
                    sendMessageUtil.setEmailSub(subject);
                    sendMessageUtil.setEmailMessage(msgText);
                    sendMessageUtil.sendEmail();
                    sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
                    //sendMessageUtil.setSmsNo("919427410774");
                    StringBuilder sb = new StringBuilder();
                    sb.append("Dear User,%0AThis is to inform you that Bidder " + master.getTendererMaster().getFirstName() + " has requested to assign complaint to Review Panel for " + master.getTenderMaster().getTenderId());
                    sb.append("%0AThanks,%0AeGP System");
                    sendMessageUtil.setSmsBody(sb.toString());
                    sendMessageUtil.sendSMS();
                    strTo = null;
                    sendMessageUtil = null;
                    utility = null;
                    msgBoxContentUtility = null;
                }
                log.debug("after the process of send mail *****");
            } else {
                List complaintList = mgmtService.getComplaintTenderer(Integer.parseInt(req.getParameter("tenderId")), userId);
                if (complaintList != null && complaintList.size() >= 1) {
                    log.error("list size of the complaints is" + complaintList.size());
                }
                modelAndView = new ModelAndView("compl_mgmt_tenderer", "complaintList", complaintList);
                req.setAttribute("complaintList", complaintList);
                modelAndView.addObject("msgg", "escalate");
                modelAndView.addObject("master", master);
            }
            return modelAndView;
        } catch (Exception e) {

            log.error("EXCEPTION AT METHOD: escalateSubmit() :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    /** Go back to dashboard of tenderer by passing tenderId
     * @return modelAndView to return dashboard of tenderer
     *  */
    @RequestMapping(value = "/tenderer/TendererDashboard.htm")
    public ModelAndView dashboard(@RequestParam("tenderId") Integer id, ModelMap map, HttpServletRequest req, HttpServletResponse res) {
        log.debug("in the view controller ******* dashboard");
        ModelAndView modelAndView = null;
        try {

            List<TblComplaintMaster> complaintList = mgmtService.getComplaintById("tenderMaster.tenderId", Operation_enum.EQ, id);
            if (complaintList != null && complaintList.size() >= 1) {
                log.debug("list size of the complaints is" + complaintList.size());
            }
            modelAndView = new ModelAndView("compl_mgmt_tenderer", "complaintList", complaintList);
            modelAndView.addObject("message", id);
            log.debug("IN view controller  id is dashboard ............................." + id);
            req.setAttribute("tenderid", id);
            return modelAndView;
        } catch (Exception e) {

            log.error("EXCEPTION AT METHOD: dashboard() :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    /** Go back to dashboard of pe by passing tenderId
     * @return modelAndView to return dashboard of PE
     *  */
    @RequestMapping(value = "/TendererDashboard.htm")
    public ModelAndView dashboardPe(@RequestParam("tenderId") Integer id, ModelMap map, HttpServletRequest req, HttpServletResponse res) {
        log.debug("in the view controller ******* dashboard");
        ModelAndView mav = null;
        try {
            mav = new ModelAndView("compl_mgmt_PE");
            List listpe = mgmtService.getComplaintHistoryRP(id);
            if (listpe != null && listpe.size() > 0) {
                log.debug("in complaint officer history is.............." + listpe.size());
            }
            mav.addObject("listpe", listpe);
            return mav;
        } catch (Exception e) {

            log.error("EXCEPTION AT METHOD: dashboard() :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    @RequestMapping(value = "/resources/common/TendererDashboard.htm")
    public ModelAndView dashboardRp(HttpServletRequest req, HttpServletResponse res) {
        log.debug("in the view controller ******* dashboard");
        ModelAndView mav = null;
        int tenderId = 0;
        try {
            mav = new ModelAndView("compl_mgmt_RP");
            List<TblComplaintMaster> listpe = mgmtService.searchPendingComplaintDetailsRP(4);
            if (listpe != null && listpe.size() > 0) {
                log.debug("in complaint officer history is.............." + listpe.size());
            }
            mav.addObject("listpe", listpe);
            return mav;
        } catch (Exception e) {

            log.error("EXCEPTION AT METHOD: dashboard() :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    @RequestMapping(value = "/resources/common/rpProcessedComplaint.htm")
    public ModelAndView dashboardRpProcessed(HttpServletRequest req, HttpServletResponse res) {
        log.debug("in the view controller ******* dashboard");
        ModelAndView mav = null;
        int tenderId = 0;
        try {
            mav = new ModelAndView("compl_mgmt_RP");
            int userId = (Integer) req.getSession().getAttribute("userId");
            List<TblComplaintMaster> listpe = mgmtService.searchProcessedComplaintDetailsRP(4);
            if (listpe != null && listpe.size() > 0) {
                log.debug("in complaint officer history is.............." + listpe.size());
            }
            mav.addObject("listpe", listpe);
            mav.addObject("status", "processed");
            return mav;
        } catch (Exception e) {

            log.error("EXCEPTION AT METHOD: dashboard() :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    @RequestMapping(value = "/goToFileUpload.htm")
    public ModelAndView forFileUpload(@RequestParam("upStatus") String upStatus, @RequestParam("compId") Integer compId, @RequestParam("tenderId") Integer tenderId, HttpServletRequest req, HttpServletResponse res) {
        ModelAndView mav = null;
        try {
            TblComplaintMaster complaint = new TblComplaintMaster();
            complaint = mgmtService.getComplaintDetails(compId);
            int userId = 0;
            if (req.getSession().getAttribute("userId") != null) {
                userId = Integer.parseInt(req.getSession().getAttribute("userId").toString());
            }
            if (upStatus.equalsIgnoreCase(ComplaintMgmtConstants.YES)) {
                log.debug(" forFileUpload " + upStatus);
                mav = new ModelAndView("popup", "complaintObject", complaint);

            } else if (upStatus.equalsIgnoreCase(ComplaintMgmtConstants.NO)) {
                log.debug(" forFileUpload " + upStatus);
                //List<TblComplaintMaster> complaintList = mgmtService.getComplaintById("tenderMaster.tenderId", Operation_enum.EQ, tenderId);
                List complaintList = mgmtService.getComplaintTenderer(tenderId, userId);
                mav = new ModelAndView("compl_mgmt_tenderer");
                mav.addObject("complaint", complaint);
                mav.addObject("complaintList", complaintList);
                mav.addObject("complaintId", complaint.getComplaintId());
                mav.addObject("msg", "sbmtcompl");
                mav.addObject("parentLink","618");
            }
            mav.addObject("parentLink","618");
            return mav;
        } catch (Exception ex) {
            log.error(" forFileUpload " + ex.toString());
            return new ModelAndView("/StackTrace");
        }
    }

    /** method to save  a complaint registered by the tenderer in TblComplaintMaster and TblComplaintHistory
     *  @return saveComplaint to redirect to  popup jsp for uploading files.
     */
    @RequestMapping(value = "/tenderer/submitComplaint.htm")
    public ModelAndView complaintSubmit(@RequestParam("tenderId") Integer id, ModelMap map, HttpServletRequest req, HttpServletResponse res, HttpSession session) {
        log.debug("in complaint submit form");
        ModelAndView saveComplaint = null;
        try {
            String utid = session.getAttribute("userTypeId").toString();
            byte usertypeid = (byte) Integer.parseInt(utid);
            TblComplaintMaster complaintmaster = null;
            TblComplaintLevel complaintlevel = null;
            TblTenderMaster tenderMaster = null;
            TblTendererMaster tendererMaster = null;
            TblEmployeeTrasfer et = null;
            TblComplaintType complaintType = null;
            TblComplaintMaster complaint = new TblComplaintMaster();
            TblComplaintHistory history = new TblComplaintHistory();
            Integer aobj = 0;
            List list = mgmtService.getGovtUser(id);

            if (mgmtService.getGovtUser(id) != null && mgmtService.getGovtUser(id).size() == 1) {
                aobj = (Integer) list.get(0);
            }
            List<TblEmployeeTrasfer> lstEmployeeTrasfer = mgmtService.findByGovUser("govUserId", Operation_enum.EQ, aobj);
            if (lstEmployeeTrasfer != null && lstEmployeeTrasfer.size() > 0) {
                et = lstEmployeeTrasfer.get(0);
            } else {
                log.debug(" lstEmployeeTrasfer is null");
            }
            String uid = session.getAttribute("userId").toString();
            int userId = Integer.parseInt(uid);
            log.debug("user ID" + userId);

            List<TblComplaintLevel> complaintLevelList = mgmtService.findByLevel("complaintLevelId", Operation_enum.EQ, Complaint_Level_enum.PE.getLevel());
            if (complaintLevelList != null && complaintLevelList.size() > 0) {
                complaintlevel = complaintLevelList.get(0);
                log.debug("complaintLevelId is" + complaintlevel.getComplaintLevelId());
            } else {
                log.debug(" complaintLevelList is null");
            }


            List<TblTenderMaster> tenderList = mgmtService.findByEntity("tenderId", Operation_enum.EQ, id);
            if (tenderList != null && tenderList.size() > 0) {
                tenderMaster = tenderList.get(0);
                log.debug("tender master" + tenderMaster.getTenderId());
            } else {
                log.debug(" tenderList is null");
            }

            List<TblTendererMaster> tendererMasterList = tendererMasterService.getTblTendererMasterDao().findTblTendererMaster("tblLoginMaster.userId", Operation_enum.EQ, userId);
            if (tendererMasterList != null && tendererMasterList.size() > 0) {
                tendererMaster = tendererMasterList.get(0);
                log.debug(tendererMaster.getTendererId() + "tendererid");
            } else {
                log.debug("tendererMasterList is null");
            }

            int complaintTypeId = Integer.parseInt(req.getParameter("complaintType"));
            List<TblComplaintType> complaintTypeList = mgmtService.getComplaintTypeById("complaintTypeId", Operation_enum.EQ, complaintTypeId);
            if (complaintTypeList != null && complaintTypeList.size() > 0) {
                complaintType = complaintTypeList.get(0);
            } else {
                log.debug("complaintTypeList is null");
            }

            String txtaComplaintBrief = req.getParameter("txtaComplaintBrief");
            String txtaComplaintDetail = req.getParameter("txtaComplaintDetail");

            complaint.setTendererComments(" ");
            complaint.setOfficerComments(" ");
            complaint.setPanelId(0);
            complaint.setOfficerProcessDate(new Date());
            complaint.setLastModificationDt(new Date());
            complaint.setComplaintCreationDt(new Date());
            complaint.setNotificationRemarksToPe(" ");
            complaint.setNotificationDtToPe(new Date());
            complaint.setAssignedtoPanelDt(new Date());
            complaint.setPaymentStatus(ComplaintMgmtConstants.COMP_PAY_STATUS_PENDING);
            complaint.setComplaintStatus(ComplaintMgmtConstants.COMP_STATUS_PENDING);
            complaint.setEmployeeTrasfer(et);
            complaint.setTenderMaster(tenderMaster);
            complaint.setTendererMaster(tendererMaster);
            complaint.setComplaintLevel(complaintlevel);
            complaint.setComplaintType(complaintType);
            complaint.setComplaintSubject(txtaComplaintBrief);
            complaint.setComplaintDetails(txtaComplaintDetail);
            complaint.setTenderRefNo(req.getParameter("refno"));

            /** save complaint to master table  */
            mgmtService.addComplaint(complaint);
            List<TblComplaintMaster> complaintMasterList = mgmtService.getComplaintById("complaintId", Operation_enum.EQ, complaint.getComplaintId());
            if (complaintMasterList != null && complaintMasterList.size() > 0) {
                complaintmaster = complaintMasterList.get(0);
            } else {
                log.debug("complaintMasterList is null");
            }

            List<TblUserTypeMaster> lstUsertype = userTypeService.getTblusertypemasterdao().findTblUserTypeMaster("userTypeId", Operation_enum.EQ, usertypeid);
            TblUserTypeMaster usertype = lstUsertype.get(0);

            history.setComplaint(complaint);
            history.setGovUserId(et.getGovUserId());
            history.setTendererId(tendererMaster.getTendererId());
            history.setComplaintLevel(complaintlevel);
            history.setFileStatus(ComplaintMgmtConstants.COMP_STATUS_PENDING);
            history.setComplaintStatus(ComplaintMgmtConstants.COMP_STATUS_PENDING);
            history.setPanelId(0);
            history.setUserRole(usertype);
            history.setCommentsdt(new Date());
            history.setComments(" ");
            /** save complaint to history table  */
            mgmtService.addHistory(history); //add to history

            log.debug("complaint id is" + complaint.getComplaintId());
            session.setAttribute("complaintId", complaint.getComplaintId());


            String uploadStatus = req.getParameter("uploadstatus");
            saveComplaint = new ModelAndView("successPage");
            saveComplaint.addObject("complaintId", complaint.getComplaintId());
            if (uploadStatus.equalsIgnoreCase(ComplaintMgmtConstants.YES)) {
                log.debug("in if" + uploadStatus);
                saveComplaint.addObject("upStatus", ComplaintMgmtConstants.YES);
            } else if (uploadStatus.equalsIgnoreCase(ComplaintMgmtConstants.NO)) {
                log.debug("in else " + uploadStatus);
                saveComplaint.addObject("upStatus", ComplaintMgmtConstants.NO);
            }
            saveComplaint.addObject("tenderId", id);


            log.debug("end of complaint insertion");

            /** sending message to PE */
            java.util.List<SPTenderCommonData> thisTenderInfoLst = tenderCommonService.returndata("tenderinfobar", id + "", null);
            MailContentUtility utility = new MailContentUtility();
            String msgText = utility.sendMailToPEForRegisterComp(id, thisTenderInfoLst, complaint.getComplaintId(), complaint.getTendererMaster().getFirstName(), complaint.getComplaintType().getComplaintType(), complaint.getComplaintSubject());
            String[] strTo = {commonservice.getEmailId(et.getTblLoginMaster().getUserId() + "")};
            String strFrom = commonservice.getEmailId(req.getSession().getAttribute("userId").toString());
            String cc = "";
            String subject = "e-GP: Complaint Registered by Tenderer";
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            registerService.contentAdmMsgBox(strTo[0], strFrom, subject, msgText);
            sendMessageUtil.setEmailTo(strTo);
            sendMessageUtil.setEmailFrom(strFrom);
            sendMessageUtil.setEmailSub(subject);
            sendMessageUtil.setEmailMessage(msgText);
            sendMessageUtil.sendEmail();
            sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
            //sendMessageUtil.setSmsNo("919427410774");
            StringBuilder sb = new StringBuilder();
            sb.append("Dear User,%0AThis is to inform you that Bidder " + complaint.getTendererMaster().getFirstName() + " has registered complaint for " + complaint.getTenderMaster().getTenderId());
            sb.append("%0AThanks,%0Ae-GP System");
            sendMessageUtil.setSmsBody(sb.toString());
            sendMessageUtil.sendSMS();
            strTo = null;
            sendMessageUtil = null;
            utility = null;
            msgBoxContentUtility = null;

//            String priority = ComplaintMgmtConstants.PRIORITY;
//            String mstxt = ComplaintMgmtConstants.MSG_TEXT;
//            String procURL = "";
//            String msfBoxType = ComplaintMgmtConstants.MSF_BOX_TYPE;
//            String action = ComplaintMgmtConstants.ACTION;
//            String msgStatus = ComplaintMgmtConstants.MSG_STATUS;
//            Integer folderid = 0;
//            String msgReadStatus = ComplaintMgmtConstants.NO;
//            String isMsgReply = ComplaintMgmtConstants.NO;
//            String msgDocId = "0";
//            Integer msgid = 0;
//            Integer updmsgId = 0;
//            Integer messageInboxId = 0;
//            List status = new ArrayList();
//
//            status = message.composeMail(userId, to, from, cc, subject, priority, mstxt, procURL, msfBoxType, action, msgStatus, folderid, msgReadStatus, isMsgReply, msgDocId, updmsgId, messageInboxId);
//            log.error("Sending Message>>>>>>>>status: " + status);
//
//            /** for sending mail */
//            List emailList = mgmtService.getEmailId(et.getGovUserId());
//            log.error("list&&&&&&&&&&" + emailList);
//            String emailTo = emailList.get(0).toString();
//
//
//            String[] mail = {emailTo};
//            String mailText = ComplaintMgmtConstants.MAIL_TEXT;
//            String mailFrom = XMLReader.getMessage("emailId");
//            SendMessageUtil sendMessage = new SendMessageUtil();
//            sendMessage.setEmailTo(mail);
//            sendMessage.setEmailFrom(mailFrom);
//            sendMessage.setEmailSub(ComplaintMgmtConstants.MAIL_SUBJECT);
//            sendMessage.setEmailMessage(mailText);
//            sendMessage.sendEmail();
            AuditTrail.generateAudit(new AuditTrail(req.getHeader("X-FORWARDED-FOR") != null ? req.getHeader("X-FORWARDED-FOR") : req.getRemoteAddr(), req.getSession().getAttribute("sessionId"), req.getSession().getAttribute("userTypeId"), req.getHeader("referer")), id, "tenderId", EgpModule.Complaint_Management.getName(), "Register Complaint", "");
            log.debug("after the process of send mail *****");
            return saveComplaint;

        } catch (Exception e) {

            log.error("EXCEPTION AT METHOD: complaintSubmit() :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    /** method to save the uploaded documents in TblComplaintDocs
     * @param int complId is passed to save the document uploaded by tender related to this complaint
     * @return mav to redirect to complaint_msg jsp
     *  */
    @RequestMapping(value = "/tenderer/fileUpload.htm", method = RequestMethod.POST)
    public ModelAndView fileUpload(@RequestParam("complId") Integer id, HttpServletRequest servletRequest, HttpServletResponse servletResponse, HttpSession session) {
        ModelAndView mav = null;
        File f = null;
        boolean checkext;

        try {
            log.debug(" in file upload in tenderer");
            TblComplaintMaster master = null;
            TblComplaintLevel complaintlevel = null;
            TblUserTypeMaster usertype = null;
            TblComplaintDocs doc = new TblComplaintDocs();

            String uid = session.getAttribute("userTypeId").toString();
            byte usertypeid = (byte) Integer.parseInt(uid);
            //to retrieve complaint object from database
            List<TblComplaintMaster> complaintMasterList = mgmtService.getComplaintById("complaintId", Operation_enum.EQ, id);
            if (complaintMasterList != null && complaintMasterList.size() > 0) {
                master = complaintMasterList.get(0);
            } else {
                log.debug("complaintMasterList is null");
            }
            // to retrieve complaintlevel instance
            List<TblComplaintLevel> complaintLevelList = mgmtService.findByLevel("complaintLevelId", Operation_enum.EQ, master.getComplaintLevel().getComplaintLevelId());
            if (complaintLevelList != null && complaintLevelList.size() > 0) {
                complaintlevel = complaintLevelList.get(0);
            } else {
                log.debug("complaintLevelList is null");
            }

            List<TblUserTypeMaster> lstUsertype = userTypeService.getTblusertypemasterdao().findTblUserTypeMaster("userTypeId", Operation_enum.EQ, usertypeid);
            if (lstUsertype != null && lstUsertype.size() > 0) {
                usertype = lstUsertype.get(0);
            } else {
                log.debug("lstUsertype is null");
            }

            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) servletRequest;
            MultipartFile file = multipartRequest.getFile("file");
            checkext = checkExnAndSize(file.getOriginalFilename(), file.getSize(), "tenderer");
            if (!checkext) {
                mav = new ModelAndView("popup");
                mav.addObject("complaintObject", master);
                mav.addObject("uploadedDocs", mgmtService.getDocsByComplaintId(master.getComplaintId()));
                mav.addObject("errormsg", "typecheck");
                return mav;
            }
            List docname = null;
            docname = mgmtService.getDocsByComplaintId(master.getComplaintId());
            // Check For Existing File
            boolean foundfile = false;
            for (int i = 0; i < docname.size(); i++) {
                if (((TblComplaintDocs) docname.get(i)).getDocName().equals(file.getOriginalFilename())) {
                    foundfile = true;
                    break;
                }
            }
            if (foundfile == true) {
                mav = new ModelAndView("popup");
                mav.addObject("complaintObject", master);
                mav.addObject("uploadedDocs", docname);
                mav.addObject("errormsg", "fileexist");
                return mav;
            }
            log.debug("file name is" + file.getOriginalFilename());
            f = new File(destinationDir + id);
            if (!f.exists()) {
                f.mkdir();
            }
            destinationComplainDir = destinationDir + id;
            File destination = new File(destinationComplainDir + "\\" + file.getOriginalFilename());
            file.transferTo(destination);
            log.debug("file transferred");
            File newFile = new File(destinationComplainDir+ "\\" , file.getOriginalFilename());
            if(newFile.exists()){
            doc.setDocName(file.getOriginalFilename());
            doc.setDocSize(String.valueOf(file.getSize()));
            doc.setComplaintMaster(master);
            doc.setComplaintLevel(complaintlevel);
            doc.setUserRole(usertype);
            doc.setUploadedBy(master.getTendererMaster().getTendererId());
            doc.setUploadedDt(new Date());
            doc.setDocDescription(multipartRequest.getParameter("uploaddesc"));
            /** Saving the document uploaded */
            mgmtService.saveDoc(doc);
            mav = new ModelAndView("popup");
            mav.addObject("complaintObject", master);
            mav.addObject("uploadedDocs", mgmtService.getDocsByComplaintId(master.getComplaintId()));
            mav.addObject("errormsg", "upload");
            }
            else {
                mav = new ModelAndView("popup");
                mav.addObject("complaintObject", master);
                mav.addObject("uploadedDocs", mgmtService.getDocsByComplaintId(master.getComplaintId()));
                mav.addObject("errormsg", "fileerror");
            }
            log.debug("file upload end");
            return mav;            
        } catch (Exception e) {
            log.error("EXCEPTION AT METHOD: fileUpload() :" + e);
            return new ModelAndView("/StackTrace");

        } finally {
            AuditTrail.generateAudit(new AuditTrail(servletRequest.getHeader("X-FORWARDED-FOR") != null ? servletRequest.getHeader("X-FORWARDED-FOR") : servletRequest.getRemoteAddr(), servletRequest.getSession().getAttribute("sessionId"), servletRequest.getSession().getAttribute("userTypeId"), servletRequest.getHeader("referer")), id, "tenderId", EgpModule.Complaint_Management.getName(), "Upload document by tenderer", "");
        }
    }

    /**for PE OFFICER COMPLAINT
     * method to retrieve complaints related to PE for a specific tenderId
     * @param int tenderId to retrieve complaints related to it
     * @return mav to redirect to compl_mgmt_PE jsp
     */
    @RequestMapping(value = "/complaintOfficer.htm")
    public ModelAndView complaintOfficer(@RequestParam("tenderId") Integer id, HttpServletRequest servletRequest, HttpServletResponse servletResponse, HttpSession session) {

        ModelAndView mav = null;
        try {
            mav = new ModelAndView("compl_mgmt_PE");
            List listpe = mgmtService.getComplaintHistoryPe(id);
            if (listpe != null && listpe.size() > 0) {
                log.debug("in complaint history" + listpe.size());
            }
            mav.addObject("listpe", listpe);
            return mav;
        } catch (Exception e) {

            log.error("EXCEPTION AT METHOD: complaintOfficer() :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    /** method for  processing complaint by PE
     * @param complaintId for processing this complaint
     * @return mav to redirect to process_complaint_PE jsp
     */
    @RequestMapping(value = "/processComplaint.htm")
    public ModelAndView processComplaint(@RequestParam("complaintId") Integer id, @RequestParam("complaintlevelid") Integer levelId, @RequestParam("uploadedBy") Integer uploadedBy, ModelMap map, HttpServletRequest req, HttpServletResponse res, HttpSession session) {
        ModelAndView mav = null;
        try {
            log.debug("in process complaint**********");
            TblComplaintMaster master = null;
            List listPe = null;
            List listHope = null;
            List listSecretary = null;
            List listReviewPanel = null;
            List listPeTender = null;
            List listHopeTender = null;
            List listSecretaryTender = null;
            List listReviewPanelTender = null;
            TblComplaintMaster complaint = mgmtService.getComplaintDetails(id);
            List<TblComplaintMaster> complaintMasterList = mgmtService.getComplaintById("complaintId", Operation_enum.EQ, id);
            if (complaintMasterList != null && complaintMasterList.size() > 0) {
                master = complaintMasterList.get(0);
            } else {
                log.debug("complaintMasterList is null");
            }
            int complaintLevelId = complaint.getComplaintLevel().getComplaintLevelId();
            ArrayList list = new ArrayList();
            log.debug("complaintlevel id is" + complaintLevelId);
            /** retrieving  PE related history by using complaintLevelId*/
            if (complaintLevelId == Complaint_Level_enum.PE.getLevel() || complaintLevelId == Complaint_Level_enum.HOPE.getLevel() || complaintLevelId == Complaint_Level_enum.SECRETARY.getLevel() || complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                log.debug("complaint level is PE");
                listPe = mgmtService.getComplaintHistory(id, Complaint_Level_enum.PE.getLevel(), User_Type_enum.OFFICER.getType());
                listPeTender = mgmtService.getComplaintHistory(id, Complaint_Level_enum.PE.getLevel(), User_Type_enum.TENDERER.getType());
                Map pe = new HashMap();


                if (listPeTender != null && listPeTender.size() >= 1) {


                    pe.put("tendererComments", ((TblComplaintHistory) listPeTender.get(0)).getComments());
                    pe.put("level", ((TblComplaintHistory) listPeTender.get(0)).getComplaintLevel().getComplaintLevel());

                    pe.put("response", ((TblComplaintHistory) listPeTender.get(0)).getComplaintStatus());

                    pe.put("responseDate", ((TblComplaintHistory) listPeTender.get(0)).getCommentsdt());
                    pe.put("levelId", ((TblComplaintHistory) listPeTender.get(0)).getComplaintLevel().getComplaintLevelId());
                    pe.put("complaintId", ((TblComplaintHistory) listPeTender.get(0)).getComplaint().getComplaintId());
                    if (listPe != null && listPe.size() >= 1) {
                        pe.put("officerComments", ((TblComplaintHistory) listPe.get(0)).getComments());
                        pe.put("level", ((TblComplaintHistory) listPe.get(0)).getComplaintLevel().getComplaintLevel());
                        pe.put("response", ((TblComplaintHistory) listPe.get(0)).getComplaintStatus());
                        pe.put("responseDate", ((TblComplaintHistory) listPe.get(0)).getCommentsdt());
                        pe.put("levelId", ((TblComplaintHistory) listPe.get(0)).getComplaintLevel().getComplaintLevelId());
                        pe.put("complaintId", ((TblComplaintHistory) listPe.get(0)).getComplaint().getComplaintId());
                    }
                    list.add(pe);
                    log.debug(" PE list retrieved***************");

                }
            }

            /** retrieving  HOPE related history by using complaintLevelId*/
            if (complaintLevelId == Complaint_Level_enum.HOPE.getLevel() || complaintLevelId == Complaint_Level_enum.SECRETARY.getLevel() || complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                listHope = mgmtService.getComplaintHistory(id, Complaint_Level_enum.HOPE.getLevel(), User_Type_enum.OFFICER.getType());
                listHopeTender = mgmtService.getComplaintHistory(id, Complaint_Level_enum.HOPE.getLevel(), User_Type_enum.TENDERER.getType());
                Map hope = new HashMap();
                if (listHopeTender != null && listHopeTender.size() >= 1) {
                    hope.put("tendererComments", ((TblComplaintHistory) listHopeTender.get(0)).getComments());
                    hope.put("level", ((TblComplaintHistory) listHopeTender.get(0)).getComplaintLevel().getComplaintLevel());
                    hope.put("response", ((TblComplaintHistory) listHopeTender.get(0)).getComplaintStatus());
                    hope.put("responseDate", ((TblComplaintHistory) listHopeTender.get(0)).getCommentsdt());
                    hope.put("levelId", ((TblComplaintHistory) listHopeTender.get(0)).getComplaintLevel().getComplaintLevelId());
                    hope.put("complaintId", ((TblComplaintHistory) listHopeTender.get(0)).getComplaint().getComplaintId());
                    if (listHope != null && listHope.size() >= 1) {

                        hope.put("officerComments", ((TblComplaintHistory) listHope.get(0)).getComments());
                        hope.put("level", ((TblComplaintHistory) listHope.get(0)).getComplaintLevel().getComplaintLevel());
                        hope.put("response", ((TblComplaintHistory) listHope.get(0)).getComplaintStatus());
                        hope.put("responseDate", ((TblComplaintHistory) listHope.get(0)).getCommentsdt());
                        hope.put("levelId", ((TblComplaintHistory) listHope.get(0)).getComplaintLevel().getComplaintLevelId());
                        hope.put("complaintId", ((TblComplaintHistory) listHope.get(0)).getComplaint().getComplaintId());
                    }
                    list.add(hope);
                    log.debug("after Hope ***************");
                }
            }
            //for secretary
            if (complaintLevelId == Complaint_Level_enum.SECRETARY.getLevel() || complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                listSecretary = mgmtService.getComplaintHistory(id, Complaint_Level_enum.SECRETARY.getLevel(), User_Type_enum.OFFICER.getType());
                listSecretaryTender = mgmtService.getComplaintHistory(id, Complaint_Level_enum.SECRETARY.getLevel(), User_Type_enum.TENDERER.getType());
                Map secretary = new HashMap();
                if (listSecretaryTender != null && listSecretaryTender.size() >= 1) {
                    secretary.put("tendererComments", ((TblComplaintHistory) listSecretaryTender.get(0)).getComments());
                    secretary.put("level", ((TblComplaintHistory) listSecretaryTender.get(0)).getComplaintLevel().getComplaintLevel());
                    secretary.put("response", ((TblComplaintHistory) listSecretaryTender.get(0)).getComplaintStatus());
                    secretary.put("responseDate", ((TblComplaintHistory) listSecretaryTender.get(0)).getCommentsdt());
                    secretary.put("levelId", ((TblComplaintHistory) listSecretaryTender.get(0)).getComplaintLevel().getComplaintLevelId());
                    secretary.put("complaintId", ((TblComplaintHistory) listSecretaryTender.get(0)).getComplaint().getComplaintId());
                    if (listSecretary != null && listSecretary.size() >= 1) {
                        secretary.put("officerComments", ((TblComplaintHistory) listSecretary.get(0)).getComments());
                        secretary.put("level", ((TblComplaintHistory) listSecretary.get(0)).getComplaintLevel().getComplaintLevel());
                        secretary.put("response", ((TblComplaintHistory) listSecretary.get(0)).getComplaintStatus());
                        secretary.put("responseDate", ((TblComplaintHistory) listSecretary.get(0)).getCommentsdt());
                        secretary.put("levelId", ((TblComplaintHistory) listSecretary.get(0)).getComplaintLevel().getComplaintLevelId());
                        secretary.put("complaintId", ((TblComplaintHistory) listSecretary.get(0)).getComplaint().getComplaintId());
                    }

                    list.add(secretary);
                    log.debug("after Secretary ***************");
                }
            }

            //for review panel
            if (complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                listReviewPanel = mgmtService.getComplaintHistory(id, Complaint_Level_enum.REVIEW_PANEL.getLevel(), User_Type_enum.OFFICER.getType());
                listReviewPanelTender = mgmtService.getComplaintHistory(id, Complaint_Level_enum.REVIEW_PANEL.getLevel(), User_Type_enum.TENDERER.getType());
                Map review = new HashMap();
                if (listReviewPanelTender != null && listReviewPanelTender.size() >= 1) {
                    review.put("tendererComments", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComments());
                    review.put("level", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComplaintLevel().getComplaintLevel());
                    review.put("response", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComplaintStatus());
                    review.put("responseDate", ((TblComplaintHistory) listReviewPanelTender.get(0)).getCommentsdt());
                    review.put("levelId", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComplaintLevel().getComplaintLevelId());
                    review.put("complaintId", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComplaint().getComplaintId());
                    if (listReviewPanel != null && listReviewPanel.size() >= 1) {
                        review.put("officerComments", ((TblComplaintHistory) listReviewPanel.get(0)).getComments());
                        review.put("level", ((TblComplaintHistory) listReviewPanel.get(0)).getComplaintLevel().getComplaintLevel());
                        review.put("response", ((TblComplaintHistory) listReviewPanel.get(0)).getComplaintStatus());
                        review.put("responseDate", ((TblComplaintHistory) listReviewPanel.get(0)).getCommentsdt());
                        review.put("levelId", ((TblComplaintHistory) listReviewPanel.get(0)).getComplaintLevel().getComplaintLevelId());
                        review.put("complaintId", ((TblComplaintHistory) listReviewPanel.get(0)).getComplaint().getComplaintId());
                    }

                    list.add(review);
                    log.debug("after review panel ***************");
                }
            }
            log.debug("after ifff************");

            List docList = mgmtService.getDocsProcessList(id, levelId, uploadedBy);
            mav = new ModelAndView("process_complaint_PE");
            mav.addObject("docList", docList);
            mav.addObject("complaint", complaint);
            mav.addObject("historyList", list);
            mav.addObject("complaint", master);
            return mav;
        } catch (Exception e) {

            log.error("EXCEPTION AT METHOD: complaintOfficer() :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    @RequestMapping(value = "/resources/common/processcomplaintRP.htm")
    public ModelAndView processComplaintRP(@RequestParam("complaintId") Integer id, @RequestParam("complaintlevelid") Integer levelId, @RequestParam("uploadedBy") Integer uploadedBy, ModelMap map, HttpServletRequest req, HttpServletResponse res, HttpSession session) {
        ModelAndView mav = null;
        try {
            log.debug("in process complaint RP**********");
            List listHope = null;
            List listSecretary = null;
            List listReviewPanel = null;
            List listPeTender = null;
            List listHopeTender = null;
            List listSecretaryTender = null;
            List listReviewPanelTender = null;
            TblComplaintMaster master = null;
            List<TblComplaintMaster> complaintMasterList = mgmtService.getComplaintById("complaintId", Operation_enum.EQ, id);
            if (complaintMasterList != null && complaintMasterList.size() > 0) {
                master = complaintMasterList.get(0);
            } else {
                log.debug("complaintMasterList is null");
            }
            List listPe = null;

            TblComplaintMaster complaint = mgmtService.getComplaintDetails(id);
            if (complaintMasterList != null && complaintMasterList.size() > 0) {
                master = complaintMasterList.get(0);
            } else {
                log.debug("complaintMasterList is null");
            }
            int complaintLevelId = complaint.getComplaintLevel().getComplaintLevelId();
            ArrayList list = new ArrayList();
            log.debug("complaintlevel id is" + complaintLevelId);
            /** retrieving  PE related history by using complaintLevelId*/
            if (complaintLevelId == Complaint_Level_enum.PE.getLevel() || complaintLevelId == Complaint_Level_enum.HOPE.getLevel() || complaintLevelId == Complaint_Level_enum.SECRETARY.getLevel() || complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                log.debug("complaint level is PE");
                listPe = mgmtService.getComplaintHistory(id, Complaint_Level_enum.PE.getLevel(), User_Type_enum.OFFICER.getType());
                listPeTender = mgmtService.getComplaintHistory(id, Complaint_Level_enum.PE.getLevel(), User_Type_enum.TENDERER.getType());
                Map pe = new HashMap();


                if (listPeTender != null && listPeTender.size() >= 1) {


                    pe.put("tendererComments", ((TblComplaintHistory) listPeTender.get(0)).getComments());
                    pe.put("level", ((TblComplaintHistory) listPeTender.get(0)).getComplaintLevel().getComplaintLevel());

                    pe.put("response", ((TblComplaintHistory) listPeTender.get(0)).getComplaintStatus());

                    pe.put("responseDate", ((TblComplaintHistory) listPeTender.get(0)).getCommentsdt());
                    pe.put("levelId", ((TblComplaintHistory) listPeTender.get(0)).getComplaintLevel().getComplaintLevelId());
                    pe.put("complaintId", ((TblComplaintHistory) listPeTender.get(0)).getComplaint().getComplaintId());
                    if (listPe != null && listPe.size() >= 1) {
                        pe.put("officerComments", ((TblComplaintHistory) listPe.get(0)).getComments());
                        pe.put("level", ((TblComplaintHistory) listPe.get(0)).getComplaintLevel().getComplaintLevel());
                        pe.put("response", ((TblComplaintHistory) listPe.get(0)).getComplaintStatus());
                        pe.put("responseDate", ((TblComplaintHistory) listPe.get(0)).getCommentsdt());
                        pe.put("levelId", ((TblComplaintHistory) listPe.get(0)).getComplaintLevel().getComplaintLevelId());
                        pe.put("complaintId", ((TblComplaintHistory) listPe.get(0)).getComplaint().getComplaintId());
                    }
                    list.add(pe);
                    log.debug(" PE list retrieved***************");

                }
            }

            /** retrieving  HOPE related history by using complaintLevelId*/
            if (complaintLevelId == Complaint_Level_enum.HOPE.getLevel() || complaintLevelId == Complaint_Level_enum.SECRETARY.getLevel() || complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                listHope = mgmtService.getComplaintHistory(id, Complaint_Level_enum.HOPE.getLevel(), User_Type_enum.OFFICER.getType());
                listHopeTender = mgmtService.getComplaintHistory(id, Complaint_Level_enum.HOPE.getLevel(), User_Type_enum.TENDERER.getType());
                Map hope = new HashMap();
                if (listHopeTender != null && listHopeTender.size() >= 1) {
                    hope.put("tendererComments", ((TblComplaintHistory) listHopeTender.get(0)).getComments());
                    hope.put("level", ((TblComplaintHistory) listHopeTender.get(0)).getComplaintLevel().getComplaintLevel());
                    hope.put("response", ((TblComplaintHistory) listHopeTender.get(0)).getComplaintStatus());
                    hope.put("responseDate", ((TblComplaintHistory) listHopeTender.get(0)).getCommentsdt());
                    hope.put("levelId", ((TblComplaintHistory) listHopeTender.get(0)).getComplaintLevel().getComplaintLevelId());
                    hope.put("complaintId", ((TblComplaintHistory) listHopeTender.get(0)).getComplaint().getComplaintId());
                    if (listHope != null && listHope.size() >= 1) {

                        hope.put("officerComments", ((TblComplaintHistory) listHope.get(0)).getComments());
                        hope.put("level", ((TblComplaintHistory) listHope.get(0)).getComplaintLevel().getComplaintLevel());
                        hope.put("response", ((TblComplaintHistory) listHope.get(0)).getComplaintStatus());
                        hope.put("responseDate", ((TblComplaintHistory) listHope.get(0)).getCommentsdt());
                        hope.put("levelId", ((TblComplaintHistory) listHope.get(0)).getComplaintLevel().getComplaintLevelId());
                        hope.put("complaintId", ((TblComplaintHistory) listHope.get(0)).getComplaint().getComplaintId());
                    }
                    list.add(hope);
                    log.debug("after Hope ***************");
                }
            }
            //for secretary
            if (complaintLevelId == Complaint_Level_enum.SECRETARY.getLevel() || complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                listSecretary = mgmtService.getComplaintHistory(id, Complaint_Level_enum.SECRETARY.getLevel(), User_Type_enum.OFFICER.getType());
                listSecretaryTender = mgmtService.getComplaintHistory(id, Complaint_Level_enum.SECRETARY.getLevel(), User_Type_enum.TENDERER.getType());
                Map secretary = new HashMap();
                if (listSecretaryTender != null && listSecretaryTender.size() >= 1) {
                    secretary.put("tendererComments", ((TblComplaintHistory) listSecretaryTender.get(0)).getComments());
                    secretary.put("level", ((TblComplaintHistory) listSecretaryTender.get(0)).getComplaintLevel().getComplaintLevel());
                    secretary.put("response", ((TblComplaintHistory) listSecretaryTender.get(0)).getComplaintStatus());
                    secretary.put("responseDate", ((TblComplaintHistory) listSecretaryTender.get(0)).getCommentsdt());
                    secretary.put("levelId", ((TblComplaintHistory) listSecretaryTender.get(0)).getComplaintLevel().getComplaintLevelId());
                    secretary.put("complaintId", ((TblComplaintHistory) listSecretaryTender.get(0)).getComplaint().getComplaintId());
                    if (listSecretary != null && listSecretary.size() >= 1) {
                        secretary.put("officerComments", ((TblComplaintHistory) listSecretary.get(0)).getComments());
                        secretary.put("level", ((TblComplaintHistory) listSecretary.get(0)).getComplaintLevel().getComplaintLevel());
                        secretary.put("response", ((TblComplaintHistory) listSecretary.get(0)).getComplaintStatus());
                        secretary.put("responseDate", ((TblComplaintHistory) listSecretary.get(0)).getCommentsdt());
                        secretary.put("levelId", ((TblComplaintHistory) listSecretary.get(0)).getComplaintLevel().getComplaintLevelId());
                        secretary.put("complaintId", ((TblComplaintHistory) listSecretary.get(0)).getComplaint().getComplaintId());
                    }

                    list.add(secretary);
                    log.debug("after Secretary ***************");
                }
            }

            //for review panel
            if (complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                listReviewPanel = mgmtService.getComplaintHistory(id, Complaint_Level_enum.REVIEW_PANEL.getLevel(), User_Type_enum.OFFICER.getType());
                listReviewPanelTender = mgmtService.getComplaintHistory(id, Complaint_Level_enum.REVIEW_PANEL.getLevel(), User_Type_enum.TENDERER.getType());
                Map review = new HashMap();
                if (listReviewPanelTender != null && listReviewPanelTender.size() >= 1) {
                    review.put("tendererComments", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComments());
                    review.put("level", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComplaintLevel().getComplaintLevel());
                    review.put("response", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComplaintStatus());
                    review.put("responseDate", ((TblComplaintHistory) listReviewPanelTender.get(0)).getCommentsdt());
                    review.put("levelId", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComplaintLevel().getComplaintLevelId());
                    review.put("complaintId", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComplaint().getComplaintId());
                    if (listReviewPanel != null && listReviewPanel.size() >= 1) {
                        review.put("officerComments", ((TblComplaintHistory) listReviewPanel.get(0)).getComments());
                        review.put("level", ((TblComplaintHistory) listReviewPanel.get(0)).getComplaintLevel().getComplaintLevel());
                        review.put("response", ((TblComplaintHistory) listReviewPanel.get(0)).getComplaintStatus());
                        review.put("responseDate", ((TblComplaintHistory) listReviewPanel.get(0)).getCommentsdt());
                        review.put("levelId", ((TblComplaintHistory) listReviewPanel.get(0)).getComplaintLevel().getComplaintLevelId());
                        review.put("complaintId", ((TblComplaintHistory) listReviewPanel.get(0)).getComplaint().getComplaintId());
                    }

                    list.add(review);
                    log.debug("after review panel ***************");
                }
            }
            List docList = mgmtService.getDocsProcessList(id, levelId, uploadedBy);
            mav = new ModelAndView("Process_complaint_RP");
            mav.addObject("docList", docList);
            mav.addObject("complaint", master);
            mav.addObject("complaint", complaint);
            mav.addObject("historyList", list);
            return mav;
        } catch (Exception e) {

            log.error("EXCEPTION AT METHOD: complaintOfficer() :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    /** method for  processing complaint by  HOPE
     * @param complaintId for processing this complaint
     * @param complaintlevelid of the complaint
     * @return mav to redirect to process_complaint_PE jsp
     */
    @RequestMapping(value = "/resources/common/processComplaint.htm")
    public ModelAndView processComplaintHope(@RequestParam("complaintId") Integer id, @RequestParam("complaintlevelid") Integer levelId, @RequestParam("uploadedBy") Integer uploadedBy, ModelMap map, HttpServletRequest req, HttpServletResponse res, HttpSession session) {
        ModelAndView mav = null;
        try {
            log.debug("in process complaint**********");
            TblComplaintMaster master = null;
            List listPe = null;
            List listHope = null;
            List listSecretary = null;
            List listReviewPanel = null;
            List listPeTender = null;
            List listHopeTender = null;
            List listSecretaryTender = null;
            List listReviewPanelTender = null;
            List<TblComplaintMaster> complaintMasterList = mgmtService.getComplaintById("complaintId", Operation_enum.EQ, id);
            /** to retrieve complaint object from database with complaintId id*/
            if (complaintMasterList != null && complaintMasterList.size() > 0) {
                master = complaintMasterList.get(0);
            } else {
                log.debug("complaintMasterList is null");
            }
            TblComplaintMaster complaint = mgmtService.getComplaintDetails(id);
            if (complaintMasterList != null && complaintMasterList.size() > 0) {
                master = complaintMasterList.get(0);
            } else {
                log.debug("complaintMasterList is null");
            }
            int complaintLevelId = complaint.getComplaintLevel().getComplaintLevelId();
            ArrayList list = new ArrayList();
            log.debug("complaintlevel id is" + complaintLevelId);
            /** retrieving  PE related history by using complaintLevelId*/
            if (complaintLevelId == Complaint_Level_enum.PE.getLevel() || complaintLevelId == Complaint_Level_enum.HOPE.getLevel() || complaintLevelId == Complaint_Level_enum.SECRETARY.getLevel() || complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                log.debug("complaint level is PE");
                listPe = mgmtService.getComplaintHistory(id, Complaint_Level_enum.PE.getLevel(), User_Type_enum.OFFICER.getType());
                listPeTender = mgmtService.getComplaintHistory(id, Complaint_Level_enum.PE.getLevel(), User_Type_enum.TENDERER.getType());
                Map pe = new HashMap();


                if (listPeTender != null && listPeTender.size() >= 1) {


                    pe.put("tendererComments", ((TblComplaintHistory) listPeTender.get(0)).getComments());
                    pe.put("level", ((TblComplaintHistory) listPeTender.get(0)).getComplaintLevel().getComplaintLevel());

                    pe.put("response", ((TblComplaintHistory) listPeTender.get(0)).getComplaintStatus());

                    pe.put("responseDate", ((TblComplaintHistory) listPeTender.get(0)).getCommentsdt());
                    pe.put("levelId", ((TblComplaintHistory) listPeTender.get(0)).getComplaintLevel().getComplaintLevelId());
                    pe.put("complaintId", ((TblComplaintHistory) listPeTender.get(0)).getComplaint().getComplaintId());
                    if (listPe != null && listPe.size() >= 1) {
                        pe.put("officerComments", ((TblComplaintHistory) listPe.get(0)).getComments());
                        pe.put("level", ((TblComplaintHistory) listPe.get(0)).getComplaintLevel().getComplaintLevel());
                        pe.put("response", ((TblComplaintHistory) listPe.get(0)).getComplaintStatus());
                        pe.put("responseDate", ((TblComplaintHistory) listPe.get(0)).getCommentsdt());
                        pe.put("levelId", ((TblComplaintHistory) listPe.get(0)).getComplaintLevel().getComplaintLevelId());
                        pe.put("complaintId", ((TblComplaintHistory) listPe.get(0)).getComplaint().getComplaintId());
                    }
                    list.add(pe);
                    log.debug(" PE list retrieved***************");

                }
            }

            /** retrieving  HOPE related history by using complaintLevelId*/
            if (complaintLevelId == Complaint_Level_enum.HOPE.getLevel() || complaintLevelId == Complaint_Level_enum.SECRETARY.getLevel() || complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                listHope = mgmtService.getComplaintHistory(id, Complaint_Level_enum.HOPE.getLevel(), User_Type_enum.OFFICER.getType());
                listHopeTender = mgmtService.getComplaintHistory(id, Complaint_Level_enum.HOPE.getLevel(), User_Type_enum.TENDERER.getType());
                Map hope = new HashMap();
                if (listHopeTender != null && listHopeTender.size() >= 1) {
                    hope.put("tendererComments", ((TblComplaintHistory) listHopeTender.get(0)).getComments());
                    hope.put("level", ((TblComplaintHistory) listHopeTender.get(0)).getComplaintLevel().getComplaintLevel());
                    hope.put("response", ((TblComplaintHistory) listHopeTender.get(0)).getComplaintStatus());
                    hope.put("responseDate", ((TblComplaintHistory) listHopeTender.get(0)).getCommentsdt());
                    hope.put("levelId", ((TblComplaintHistory) listHopeTender.get(0)).getComplaintLevel().getComplaintLevelId());
                    hope.put("complaintId", ((TblComplaintHistory) listHopeTender.get(0)).getComplaint().getComplaintId());
                    if (listHope != null && listHope.size() >= 1) {

                        hope.put("officerComments", ((TblComplaintHistory) listHope.get(0)).getComments());
                        hope.put("level", ((TblComplaintHistory) listHope.get(0)).getComplaintLevel().getComplaintLevel());
                        hope.put("response", ((TblComplaintHistory) listHope.get(0)).getComplaintStatus());
                        hope.put("responseDate", ((TblComplaintHistory) listHope.get(0)).getCommentsdt());
                        hope.put("levelId", ((TblComplaintHistory) listHope.get(0)).getComplaintLevel().getComplaintLevelId());
                        hope.put("complaintId", ((TblComplaintHistory) listHope.get(0)).getComplaint().getComplaintId());
                    }
                    list.add(hope);
                    log.debug("after Hope ***************");
                }
            }
            //for secretary
            if (complaintLevelId == Complaint_Level_enum.SECRETARY.getLevel() || complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                listSecretary = mgmtService.getComplaintHistory(id, Complaint_Level_enum.SECRETARY.getLevel(), User_Type_enum.OFFICER.getType());
                listSecretaryTender = mgmtService.getComplaintHistory(id, Complaint_Level_enum.SECRETARY.getLevel(), User_Type_enum.TENDERER.getType());
                Map secretary = new HashMap();
                if (listSecretaryTender != null && listSecretaryTender.size() >= 1) {
                    secretary.put("tendererComments", ((TblComplaintHistory) listSecretaryTender.get(0)).getComments());
                    secretary.put("level", ((TblComplaintHistory) listSecretaryTender.get(0)).getComplaintLevel().getComplaintLevel());
                    secretary.put("response", ((TblComplaintHistory) listSecretaryTender.get(0)).getComplaintStatus());
                    secretary.put("responseDate", ((TblComplaintHistory) listSecretaryTender.get(0)).getCommentsdt());
                    secretary.put("levelId", ((TblComplaintHistory) listSecretaryTender.get(0)).getComplaintLevel().getComplaintLevelId());
                    secretary.put("complaintId", ((TblComplaintHistory) listSecretaryTender.get(0)).getComplaint().getComplaintId());
                    if (listSecretary != null && listSecretary.size() >= 1) {
                        secretary.put("officerComments", ((TblComplaintHistory) listSecretary.get(0)).getComments());
                        secretary.put("level", ((TblComplaintHistory) listSecretary.get(0)).getComplaintLevel().getComplaintLevel());
                        secretary.put("response", ((TblComplaintHistory) listSecretary.get(0)).getComplaintStatus());
                        secretary.put("responseDate", ((TblComplaintHistory) listSecretary.get(0)).getCommentsdt());
                        secretary.put("levelId", ((TblComplaintHistory) listSecretary.get(0)).getComplaintLevel().getComplaintLevelId());
                        secretary.put("complaintId", ((TblComplaintHistory) listSecretary.get(0)).getComplaint().getComplaintId());
                    }

                    list.add(secretary);
                    log.debug("after Secretary ***************");
                }
            }

            //for review panel
            if (complaintLevelId == Complaint_Level_enum.REVIEW_PANEL.getLevel()) {
                listReviewPanel = mgmtService.getComplaintHistory(id, Complaint_Level_enum.REVIEW_PANEL.getLevel(), User_Type_enum.OFFICER.getType());
                listReviewPanelTender = mgmtService.getComplaintHistory(id, Complaint_Level_enum.REVIEW_PANEL.getLevel(), User_Type_enum.TENDERER.getType());
                Map review = new HashMap();
                if (listReviewPanelTender != null && listReviewPanelTender.size() >= 1) {
                    review.put("tendererComments", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComments());
                    review.put("level", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComplaintLevel().getComplaintLevel());
                    review.put("response", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComplaintStatus());
                    review.put("responseDate", ((TblComplaintHistory) listReviewPanelTender.get(0)).getCommentsdt());
                    review.put("levelId", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComplaintLevel().getComplaintLevelId());
                    review.put("complaintId", ((TblComplaintHistory) listReviewPanelTender.get(0)).getComplaint().getComplaintId());
                    if (listReviewPanel != null && listReviewPanel.size() >= 1) {
                        review.put("officerComments", ((TblComplaintHistory) listReviewPanel.get(0)).getComments());
                        review.put("level", ((TblComplaintHistory) listReviewPanel.get(0)).getComplaintLevel().getComplaintLevel());
                        review.put("response", ((TblComplaintHistory) listReviewPanel.get(0)).getComplaintStatus());
                        review.put("responseDate", ((TblComplaintHistory) listReviewPanel.get(0)).getCommentsdt());
                        review.put("levelId", ((TblComplaintHistory) listReviewPanel.get(0)).getComplaintLevel().getComplaintLevelId());
                        review.put("complaintId", ((TblComplaintHistory) listReviewPanel.get(0)).getComplaint().getComplaintId());
                    }

                    list.add(review);
                    log.debug("after review panel ***************");
                }
            }

            List docList = mgmtService.getDocsProcessList(id, levelId, uploadedBy);
            mav = new ModelAndView("process_complaint_PE");
            mav.addObject("docList", docList);
            mav.addObject("complaint", master);
            mav.addObject("historyList", list);

            return mav;
        } catch (Exception e) {
            mav = new ModelAndView("/StackTrace");
            log.error("EXCEPTION AT METHOD: processComplaintHope() :" + e);
            return mav;
        }
    }

    //for  pe and hope response
    @RequestMapping(value = {"/response.htm", "/resources/common/response.htm"})
    public ModelAndView responsePE(@RequestParam("complaintId") Integer id, ModelMap map, HttpServletRequest req, HttpServletResponse res, HttpSession session) {
        ModelAndView mav = null;
        boolean isPE = false;
        boolean isHope = false;
        boolean isSec = false;
        TblComplaintMaster complaint = mgmtService.getComplaintDetails(id);
        try {
            TblComplaintMaster complaintmaster = null;
            TblUserTypeMaster usertype = null;
            String utid = session.getAttribute("userTypeId").toString();
            byte usertypeid = (byte) Integer.parseInt(utid);
            TblComplaintHistory history = new TblComplaintHistory();

            List<TblComplaintMaster> complaintMasterList = mgmtService.getComplaintById("complaintId", Operation_enum.EQ, id);
            if (complaintMasterList != null && complaintMasterList.size() > 0) {
                complaintmaster = complaintMasterList.get(0);
            } else {
                log.debug(" complaintMasterList is null");
            }
            if (complaintmaster.getComplaintLevel().getComplaintLevelId() == Integer.parseInt(req.getParameter("complaintLevelId").toString())) {
                List<TblUserTypeMaster> lstUsertype = userTypeService.getTblusertypemasterdao().findTblUserTypeMaster("userTypeId", Operation_enum.EQ, usertypeid);
                if (lstUsertype != null && lstUsertype.size() > 0) {
                    usertype = lstUsertype.get(0);
                } else {
                    log.debug("lstUsertype is null");
                }

                String txtaComplaintDetail = req.getParameter("txtaComplaintDetail");
                String status = req.getParameter("status");

                log.debug("txtaComplaintDetail************" + txtaComplaintDetail);
                log.debug("status***************" + status);
                complaintmaster.setComplaintStatus(status);
                complaintmaster.setOfficerComments(txtaComplaintDetail);
                complaintmaster.setOfficerProcessDate(new Date());
                if (Integer.parseInt(req.getParameter("complaintLevelId").toString()) == 1) {
                    if (req.getParameter("ResolvedSatisfactory").equals("Yes")) {
                        complaintmaster.setPeResolved("Yes");
                    } else {
                        complaintmaster.setPeResolved("No");
                    }
                } else if (Integer.parseInt(req.getParameter("complaintLevelId").toString()) == 2) {
                    if (req.getParameter("ResolvedSatisfactory").equals("Yes")) {
                        complaintmaster.setHopeResolved("Yes");
                    } else {
                        complaintmaster.setHopeResolved("No");
                    }
                } else if (Integer.parseInt(req.getParameter("complaintLevelId").toString()) == 3) {
                    if (req.getParameter("ResolvedSatisfactory").equals("Yes")) {
                        complaintmaster.setSecResolved("Yes");
                    } else {
                        complaintmaster.setSecResolved("No");
                    }
                } else if (Integer.parseInt(req.getParameter("complaintLevelId").toString()) == 4) {
                    if (req.getParameter("ResolvedSatisfactory").equals("Yes")) {
                        complaintmaster.setRpResolved("Yes");
                    } else {
                        complaintmaster.setRpResolved("No");
                    }
                }
                mgmtService.updateMaster(complaintmaster); //update master
                log.debug("after upload status");
                history.setComplaint(complaintmaster);
                log.debug("complaintmaster**********" + complaintmaster);
                history.setGovUserId(complaintmaster.getEmployeeTrasfer().getGovUserId());
                history.setTendererId(complaintmaster.getTendererMaster().getTendererId());
                history.setComplaintLevel(complaintmaster.getComplaintLevel());
                log.debug("ComplaintLevel*************" + complaintmaster.getComplaintLevel().getComplaintLevelId());
                history.setFileStatus("status");
                history.setComplaintStatus(status);
                history.setPanelId(0);
                history.setUserRole(usertype);
                log.debug("UserRole*************" + usertype);
                history.setCommentsdt(new Date());
                history.setComments(txtaComplaintDetail);

                mgmtService.addHistory(history); //add to history
                String uploadStatus = req.getParameter("uploadstatus");
                if (uploadStatus.equalsIgnoreCase("yes")) {
                    if (req.getSession().getAttribute("userTypeId").toString() == null ? "17" != null : !req.getSession().getAttribute("userTypeId").toString().equals("17")) {
                        String ck[] = req.getSession().getAttribute("procurementRole").toString().split(",");
                        for (int ii = 0; ii < ck.length; ii++) {
                            if (ck[ii].equalsIgnoreCase("PE")) {
                                isPE = true;
                                isHope = false;
                                isSec = false;
                            } else if (ck[ii].equalsIgnoreCase("HOPE")) {
                                isHope = true;
                                isPE = false;
                                isSec = false;
                            } else if (ck[ii].equalsIgnoreCase("Secretary")) {
                                isSec = true;
                                isPE = false;
                                isHope = false;
                            }
                        }
                    }
                    log.debug("in if" + uploadStatus);
                    mav = new ModelAndView("pe_file_upload");
                    List listpe = mgmtService.getComplaintHistoryPe(complaintmaster.getTenderMaster().getTenderId());
                    mav.addObject("complaintObject", complaintmaster);
                    mav.addObject("uploadedDocs", mgmtService.getDocsByComplaintId(complaintmaster.getComplaintId()));
                    mav.addObject("listpe", listpe);

                    mav.addObject("msg", "msg");

                } else if (uploadStatus.equalsIgnoreCase("no")) {
                    log.debug("in else " + uploadStatus);

                    if (req.getSession().getAttribute("procurementRole") != null) {
                        List listpe = mgmtService.getComplaintHistoryPe(complaintmaster.getTenderMaster().getTenderId());
                        String ck[] = req.getSession().getAttribute("procurementRole").toString().split(",");

                        for (int ii = 0; ii < ck.length; ii++) {
                            if (ck[ii].equalsIgnoreCase("PE")) {
                                isPE = true;
                                isHope = false;
                                isSec = false;
                                mav = new ModelAndView("compl_mgmt_PE", "complaintId", id);
                                mav.addObject("complaintObject", complaintmaster);
                                mav.addObject("listpe", listpe);
                                mav.addObject("complaint", complaint);
                                mav.addObject("msg", "msg");
                            } else if (ck[ii].equalsIgnoreCase("HOPE")) {
                                isHope = true;
                                isPE = false;
                                isSec = false;
                                mav = new ModelAndView("compl_mgt_HOPE");
                                List<TblComplaintMaster> hopeSearchList = mgmtService.searchPendingComplaintDetails("2");
                                mav.addObject("hopeSearchList", hopeSearchList);
                                mav.addObject("msg", "msg");
                            } else if (ck[ii].equalsIgnoreCase("Secretary")) {
                                isSec = true;
                                isPE = false;
                                isHope = false;
                                mav = new ModelAndView("compl_mgt_HOPE");
                                List<TblComplaintMaster> hopeSearchList = mgmtService.searchPendingComplaintDetails("3");
                                mav.addObject("hopeSearchList", hopeSearchList);
                                mav.addObject("msg", "msg");
                            }
                        }

                    } else {
                        mav = new ModelAndView("compl_mgmt_RP");
                        List<TblComplaintMaster> listreviw = mgmtService.searchPendingComplaintDetailsRP(4);
                        if (listreviw != null && listreviw.size() > 0) {
                            log.debug("in complaint officer history is.............." + listreviw.size());
                        }
                        mav.addObject("listpe", listreviw);
                        mav.addObject("msg", "msg");
                    }

                }
                java.util.List<SPTenderCommonData> thisTenderInfoLst = tenderCommonService.returndata("tenderinfobar", complaintmaster.getTenderMaster().getTenderId() + "", null);
                MailContentUtility utility = new MailContentUtility();
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                String subject = "";
                String userName = req.getSession().getAttribute("userName").toString();
                if (isPE) {
                    String msgText = utility.sendMailToTenForProcessComp("PE", complaintmaster.getTenderMaster().getTenderId(), thisTenderInfoLst, complaintmaster.getComplaintId(), complaintmaster.getTendererMaster().getFirstName(), complaintmaster.getComplaintType().getComplaintType(), status, userName, complaintmaster.getComplaintSubject());
                    String[] strTo = {commonservice.getEmailId(complaintmaster.getTendererMaster().getTblLoginMaster().getUserId() + "")};
                    String strFrom = commonservice.getEmailId(req.getSession().getAttribute("userId").toString());
                    subject = "e-GP:  PE has Processed Complaint";
                    registerService.contentAdmMsgBox(strTo[0], strFrom, subject, msgText);
                    sendMessageUtil.setEmailTo(strTo);
                    sendMessageUtil.setEmailFrom(strFrom);
                    sendMessageUtil.setEmailSub(subject);
                    sendMessageUtil.setEmailMessage(msgText);
                    sendMessageUtil.sendEmail();
                    sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
                    StringBuilder sb = new StringBuilder();
                    sb.append("Dear User,%0AThis is to inform you that PE " + userName + " has processed complaint for " + complaintmaster.getTenderMaster().getTenderId());
                    sb.append("%0AThanks,%0AeGP System");
                    sendMessageUtil.setSmsBody(sb.toString());
                    sendMessageUtil.sendSMS();
                    if (!"clarification".equalsIgnoreCase(status)) {
                        AuditTrail.generateAudit(new AuditTrail(req.getHeader("X-FORWARDED-FOR") != null ? req.getHeader("X-FORWARDED-FOR") : req.getRemoteAddr(), req.getSession().getAttribute("sessionId"), req.getSession().getAttribute("userTypeId"), req.getHeader("referer")), complaintmaster.getTenderMaster().getTenderId(), "tenderId", EgpModule.Complaint_Management.getName(), status + "ed by PE", "");
                    } else {
                        AuditTrail.generateAudit(new AuditTrail(req.getHeader("X-FORWARDED-FOR") != null ? req.getHeader("X-FORWARDED-FOR") : req.getRemoteAddr(), req.getSession().getAttribute("sessionId"), req.getSession().getAttribute("userTypeId"), req.getHeader("referer")), complaintmaster.getTenderMaster().getTenderId(), "tenderId", EgpModule.Complaint_Management.getName(), "Seek " + status + " by PE", "");
                    }





                } else if (isHope) {
                    subject = "e-GP:  HOPE has Processed Complaint";
                    String msgText = utility.sendMailToTenForProcessComp("HOPE", complaintmaster.getTenderMaster().getTenderId(), thisTenderInfoLst, complaintmaster.getComplaintId(), complaintmaster.getTendererMaster().getFirstName(), complaintmaster.getComplaintType().getComplaintType(), status, userName, complaintmaster.getComplaintSubject());
                    String[] strTo = {commonservice.getEmailId(complaintmaster.getTendererMaster().getTblLoginMaster().getUserId() + "")};
                    String strFrom = commonservice.getEmailId(req.getSession().getAttribute("userId").toString());
                    registerService.contentAdmMsgBox(strTo[0], strFrom, subject, msgText);
                    sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
                    StringBuilder sb = new StringBuilder();
                    sb.append("Dear User,%0AThis is to inform you that HOPE " + userName + " has processed complaint for " + complaintmaster.getTenderMaster().getTenderId());
                    sb.append("%0AThanks,%0AeGP System");
                    sendMessageUtil.setSmsBody(sb.toString());
                    sendMessageUtil.sendSMS();
                    sendMessageUtil.setEmailTo(strTo);
                    sendMessageUtil.setEmailFrom(strFrom);
                    sendMessageUtil.setEmailSub(subject);
                    sendMessageUtil.setEmailMessage(msgText);
                    sendMessageUtil.sendEmail();
                    if (!"clarification".equalsIgnoreCase(status)) {
                        AuditTrail.generateAudit(new AuditTrail(req.getHeader("X-FORWARDED-FOR") != null ? req.getHeader("X-FORWARDED-FOR") : req.getRemoteAddr(), req.getSession().getAttribute("sessionId"), req.getSession().getAttribute("userTypeId"), req.getHeader("referer")), complaintmaster.getTenderMaster().getTenderId(), "tenderId", EgpModule.Complaint_Management.getName(), status + "ed by HOPE", "");
                    } else {
                        AuditTrail.generateAudit(new AuditTrail(req.getHeader("X-FORWARDED-FOR") != null ? req.getHeader("X-FORWARDED-FOR") : req.getRemoteAddr(), req.getSession().getAttribute("sessionId"), req.getSession().getAttribute("userTypeId"), req.getHeader("referer")), complaintmaster.getTenderMaster().getTenderId(), "tenderId", EgpModule.Complaint_Management.getName(), "Seek " + status + " by HOPE", "");
                    }

                } else if (isSec) {
                    subject = "e-GP:  Secretary has Processed Complaint";
                    String msgText = utility.sendMailToTenForProcessComp("Secretary", complaintmaster.getTenderMaster().getTenderId(), thisTenderInfoLst, complaintmaster.getComplaintId(), complaintmaster.getTendererMaster().getFirstName(), complaintmaster.getComplaintType().getComplaintType(), status, userName, complaintmaster.getComplaintSubject());
                    String[] strTo = {commonservice.getEmailId(complaintmaster.getTendererMaster().getTblLoginMaster().getUserId() + "")};
                    String strFrom = commonservice.getEmailId(req.getSession().getAttribute("userId").toString());
                    registerService.contentAdmMsgBox(strTo[0], strFrom, subject, msgText);
                    sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
                    StringBuilder sb = new StringBuilder();
                    sb.append("Dear User,%0AThis is to inform you that Secretary " + userName + " has processed complaint for " + complaintmaster.getTenderMaster().getTenderId());
                    sb.append("%0AThanks,%0AeGP System");
                    sendMessageUtil.setSmsBody(sb.toString());
                    sendMessageUtil.sendSMS();
                    sendMessageUtil.setEmailTo(strTo);
                    sendMessageUtil.setEmailFrom(strFrom);
                    sendMessageUtil.setEmailSub(subject);
                    sendMessageUtil.setEmailMessage(msgText);
                    sendMessageUtil.sendEmail();
                    if (!"clarification".equalsIgnoreCase(status)) {
                        AuditTrail.generateAudit(new AuditTrail(req.getHeader("X-FORWARDED-FOR") != null ? req.getHeader("X-FORWARDED-FOR") : req.getRemoteAddr(), req.getSession().getAttribute("sessionId"), req.getSession().getAttribute("userTypeId"), req.getHeader("referer")), complaintmaster.getTenderMaster().getTenderId(), "tenderId", EgpModule.Complaint_Management.getName(), status + "ed by Secretary", "");
                    } else {
                        AuditTrail.generateAudit(new AuditTrail(req.getHeader("X-FORWARDED-FOR") != null ? req.getHeader("X-FORWARDED-FOR") : req.getRemoteAddr(), req.getSession().getAttribute("sessionId"), req.getSession().getAttribute("userTypeId"), req.getHeader("referer")), complaintmaster.getTenderMaster().getTenderId(), "tenderId", EgpModule.Complaint_Management.getName(), "Seek " + status + " by Secretary", "");
                    }
                } else {
                    subject = "e-GP:  Review Panel has Processed Complaint";
                    String msgText = utility.sendMailToTenForProcessComp("Review Panel", complaintmaster.getTenderMaster().getTenderId(), thisTenderInfoLst, complaintmaster.getComplaintId(), complaintmaster.getTendererMaster().getFirstName(), complaintmaster.getComplaintType().getComplaintType(), status, userName, complaintmaster.getComplaintSubject());
                    String[] strTo = {commonservice.getEmailId(complaintmaster.getTendererMaster().getTblLoginMaster().getUserId() + "")};
                    String strFrom = commonservice.getEmailId(req.getSession().getAttribute("userId").toString());
                    registerService.contentAdmMsgBox(strTo[0], strFrom, subject, msgText);
                    sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
                    StringBuilder sb = new StringBuilder();
                    sb.append("Dear User,%0AThis is to inform you that Review Panel " + userName + " has processed complaint for " + complaintmaster.getTenderMaster().getTenderId());
                    sb.append("%0AThanks,%0AeGP System");
                    sendMessageUtil.setSmsBody(sb.toString());
                    sendMessageUtil.sendSMS();
                    sendMessageUtil.setEmailTo(strTo);
                    sendMessageUtil.setEmailFrom(strFrom);
                    sendMessageUtil.setEmailSub(subject);
                    sendMessageUtil.setEmailMessage(msgText);
                    sendMessageUtil.sendEmail();
                    if (!"clarification".equalsIgnoreCase(status)) {
                        AuditTrail.generateAudit(new AuditTrail(req.getHeader("X-FORWARDED-FOR") != null ? req.getHeader("X-FORWARDED-FOR") : req.getRemoteAddr(), req.getSession().getAttribute("sessionId"), req.getSession().getAttribute("userTypeId"), req.getHeader("referer")), complaintmaster.getTenderMaster().getTenderId(), "tenderId", EgpModule.Complaint_Management.getName(), status + "ed by Review Panel", "");
                    } else {
                        AuditTrail.generateAudit(new AuditTrail(req.getHeader("X-FORWARDED-FOR") != null ? req.getHeader("X-FORWARDED-FOR") : req.getRemoteAddr(), req.getSession().getAttribute("sessionId"), req.getSession().getAttribute("userTypeId"), req.getHeader("referer")), complaintmaster.getTenderMaster().getTenderId(), "tenderId", EgpModule.Complaint_Management.getName(), "Seek " + status + " by Review Panel", "");
                    }

                }
                sendMessageUtil = null;
                utility = null;
                msgBoxContentUtility = null;
                mav.addObject("complaint", complaint);
            } else {
                if (req.getSession().getAttribute("procurementRole") != null) {
                    List listpe = mgmtService.getComplaintHistoryPe(complaintmaster.getTenderMaster().getTenderId());
                    String ck[] = req.getSession().getAttribute("procurementRole").toString().split(",");

                    for (int ii = 0; ii < ck.length; ii++) {
                        if (ck[ii].equalsIgnoreCase("PE")) {
                            isPE = true;
                            isHope = false;
                            isSec = false;
                            mav = new ModelAndView("compl_mgmt_PE", "complaintId", id);
                            mav.addObject("complaintObject", complaintmaster);
                            mav.addObject("listpe", listpe);
                            mav.addObject("complaint", complaint);
                            mav.addObject("msg", "msg");
                        } else if (ck[ii].equalsIgnoreCase("HOPE")) {
                            isHope = true;
                            isPE = false;
                            isSec = false;
                            mav = new ModelAndView("compl_mgt_HOPE");
                            List<TblComplaintMaster> hopeSearchList = mgmtService.searchPendingComplaintDetails("2");
                            mav.addObject("hopeSearchList", hopeSearchList);
                            mav.addObject("msg", "msg");
                        } else if (ck[ii].equalsIgnoreCase("Secretary")) {
                            isSec = true;
                            isPE = false;
                            isHope = false;
                            mav = new ModelAndView("compl_mgt_HOPE");
                            List<TblComplaintMaster> hopeSearchList = mgmtService.searchPendingComplaintDetails("3");
                            mav.addObject("hopeSearchList", hopeSearchList);
                            mav.addObject("msg", "msg");
                        }
                    }

                } else {
                    mav = new ModelAndView("compl_mgmt_RP");
                    List<TblComplaintMaster> listreviw = mgmtService.searchPendingComplaintDetailsRP(4);
                    if (listreviw != null && listreviw.size() > 0) {
                        log.debug("in complaint officer history is.............." + listreviw.size());
                    }
                    mav.addObject("listpe", listreviw);
                    mav.addObject("msg", "msg");
                }
            }
            return mav;
        } catch (Exception e) {

            log.error("EXCEPTION AT METHOD: responsePE() :" + e);
            return new ModelAndView("/StackTrace");
        } finally {
        }
    }

    @RequestMapping(value = {"/peFileUpload.htm", "/resources/common/peFileUpload.htm"})
    public ModelAndView peFileUpload(@RequestParam("complId") Integer id, HttpServletRequest servletRequest, HttpServletResponse servletResponse, HttpSession session) {
        ModelAndView mav = null;
        File f = null;
        boolean checkext;

        try {
            TblComplaintMaster master = null;
            TblComplaintLevel complaintlevel = null;
            TblUserTypeMaster usertype = null;
            TblComplaintDocs doc = new TblComplaintDocs();
            List<TblComplaintLevel> complaintLevelList = null;
            String uid = session.getAttribute("userTypeId").toString();
            byte usertypeid = (byte) Integer.parseInt(uid);
            int uploadedBy = 0;
            int officeid = 0;
            Integer hopeGovUserId = 0;
            List<TblComplaintMaster> complaintMasterList = mgmtService.getComplaintById("complaintId", Operation_enum.EQ, id);
            if (complaintMasterList != null && complaintMasterList.size() > 0) {
                master = complaintMasterList.get(0);
            } else {
                log.debug(" complaintMasterList is null");
            }
            //    String role = session.getAttribute("procurementRole").toString();
            List<TblTenderDetails> tenderDetails = mgmtService.findOffice("tblTenderMaster.tenderId", Operation_enum.EQ, master.getTenderMaster().getTenderId());
            if (tenderDetails != null && tenderDetails.size() > 0) {
                officeid = tenderDetails.get(0).getOfficeId();
            } else {
                log.debug("tenderDetails is null");
            }
            log.debug("tbl tender details officeid is" + officeid);
            complaintLevelList = mgmtService.findByLevel("complaintLevelId", Operation_enum.EQ, master.getComplaintLevel().getComplaintLevelId());
            if (complaintLevelList != null && complaintLevelList.size() > 0) {
                complaintlevel = complaintLevelList.get(0);
            } else {
                log.debug("complaintLevelList is null");
            }
            /*            if (role.equals("PE")) {
            log.error("in pe file upload");
            uploadedBy = master.getEmployeeTrasfer().getGovUserId();
            complaintLevelList = mgmtService.findByLevel("complaintLevelId", Operation_enum.EQ, Complaint_Level_enum.PE.getLevel());
            if (complaintLevelList != null && complaintLevelList.size() > 0) {
            complaintlevel = complaintLevelList.get(0);
            } else {
            log.error("complaintLevelList is null");
            }
            } else if (role.equals("HOPE")) {
            log.error("in hope file upload");
            List list = mgmtService.getGovtUserDetails(officeid, role);
            if (list != null && list.size() > 0) {
            hopeGovUserId = (Integer) list.get(0);
            } else {
            log.error("list is null");
            }
            log.error("tbl tender details hopeGovUserId is" + hopeGovUserId);
            uploadedBy = hopeGovUserId;
            complaintLevelList = mgmtService.findByLevel("complaintLevelId", Operation_enum.EQ, Complaint_Level_enum.HOPE.getLevel());
            if (complaintLevelList != null && complaintLevelList.size() > 0) {
            complaintlevel = complaintLevelList.get(0);
            } else {
            log.error("complaintLevelList is null");
            }
            } else if (role.equals("Secretary")) {
            log.error("in secretary file upload");
            uploadedBy = master.getEmployeeTrasfer().getGovUserId();
            complaintLevelList = mgmtService.findByLevel("complaintLevelId", Operation_enum.EQ, Complaint_Level_enum.SECRETARY.getLevel());
            if (complaintLevelList != null && complaintLevelList.size() > 0) {
            complaintlevel = complaintLevelList.get(0);
            } else {
            log.error("complaintLevelList is null");
            }
            } */
            List<TblUserTypeMaster> lstUsertype = userTypeService.getTblusertypemasterdao().findTblUserTypeMaster("userTypeId", Operation_enum.EQ, usertypeid);
            if (lstUsertype != null && lstUsertype.size() > 0) {
                usertype = lstUsertype.get(0);
            } else {
                log.debug("lstUsertype is null");
            }
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) servletRequest;
            MultipartFile file = multipartRequest.getFile("file");
            log.debug("file name is" + file.getOriginalFilename());
            checkext = checkExnAndSize(file.getOriginalFilename(), file.getSize(), "tenderer");
            if (!checkext) {
                mav = new ModelAndView("pe_file_upload");
                List listpe = mgmtService.getComplaintHistoryPe(master.getTenderMaster().getTenderId());
                mav.addObject("complaintObject", master);
                mav.addObject("uploadedDocs", mgmtService.getDocsByComplaintId(master.getComplaintId()));
                mav.addObject("tenderId", master.getTenderMaster().getTenderId());
                mav.addObject("listpe", listpe);
                mav.addObject("errormsg", "typecheck");
                return mav;
            }
            List docname = null;
            docname = mgmtService.getDocsByComplaintId(master.getComplaintId());
            // Check For Existing File
            boolean foundfile = false;
            for (int i = 0; i < docname.size(); i++) {
                if (((TblComplaintDocs) docname.get(i)).getDocName().equals(file.getOriginalFilename())) {
                    foundfile = true;
                    break;
                }
            }
            if (foundfile == true) {
                mav = new ModelAndView("pe_file_upload");
                List listpe = mgmtService.getComplaintHistoryPe(master.getTenderMaster().getTenderId());
                mav.addObject("complaintObject", master);
                mav.addObject("uploadedDocs", mgmtService.getDocsByComplaintId(master.getComplaintId()));
                mav.addObject("tenderId", master.getTenderMaster().getTenderId());
                mav.addObject("listpe", listpe);
                mav.addObject("errormsg", "fileexist");
                return mav;
            }
            log.debug("file name is" + file.getOriginalFilename());
            f = new File(destinationDir + id);
            if (!f.exists()) {
                f.mkdir();
            }
            destinationComplainDir = destinationDir + id;
            File destination = new File(destinationComplainDir + "\\" + file.getOriginalFilename());
            file.transferTo(destination);
            File newFile = new File(destinationComplainDir+ "\\" , file.getOriginalFilename());
            if(newFile.exists()){
            doc.setDocName(file.getOriginalFilename());
            doc.setDocSize(String.valueOf(file.getSize()));
            doc.setComplaintMaster(master);
            doc.setComplaintLevel(complaintlevel);
            doc.setUserRole(usertype);
            doc.setUploadedBy(master.getTendererMaster().getTendererId());
            doc.setUploadedDt(new Date());
            doc.setDocDescription(multipartRequest.getParameter("uploaddesc"));
            mgmtService.saveDoc(doc);  //save doc
            //  List docList = mgmtService.getDocsProcessList(id, complaintlevel.getComplaintLevelId(), uploadedBy);
            log.debug("file upload end");
            mav = new ModelAndView("pe_file_upload");
            List listpe = mgmtService.getComplaintHistoryPe(master.getTenderMaster().getTenderId());
            mav.addObject("complaintObject", master);
            mav.addObject("uploadedDocs", mgmtService.getDocsByComplaintId(master.getComplaintId()));
            mav.addObject("tenderId", master.getTenderMaster().getTenderId());
            mav.addObject("listpe", listpe);
            mav.addObject("errormsg", "upload");
            }
            else {
                mav = new ModelAndView("pe_file_upload");
                List listpe = mgmtService.getComplaintHistoryPe(master.getTenderMaster().getTenderId());
                mav.addObject("complaintObject", master);
                mav.addObject("uploadedDocs", mgmtService.getDocsByComplaintId(master.getComplaintId()));
                mav.addObject("tenderId", master.getTenderMaster().getTenderId());
                mav.addObject("listpe", listpe);
                mav.addObject("errormsg", "fileerror");
            }
            return mav;
        } catch (Exception e) {
            System.out.println("EXCEPTION AT METHOD: peFileUpload() :" + e);
            log.error("EXCEPTION AT METHOD: peFileUpload() :" + e);
            return new ModelAndView("/StackTrace");
        } finally {
            if (servletRequest.getSession().getAttribute("procurementRole") != null) {
                String ck[] = servletRequest.getSession().getAttribute("procurementRole").toString().split(",");
                for (int ii = 0; ii < ck.length; ii++) {
                    if (ck[ii].equalsIgnoreCase("PE")) {
                        AuditTrail.generateAudit(new AuditTrail(servletRequest.getHeader("X-FORWARDED-FOR") != null ? servletRequest.getHeader("X-FORWARDED-FOR") : servletRequest.getRemoteAddr(), servletRequest.getSession().getAttribute("sessionId"), servletRequest.getSession().getAttribute("userTypeId"), servletRequest.getHeader("referer")), id, "tenderId", EgpModule.Complaint_Management.getName(), "Upload document by PE", "");
                    } else if (ck[ii].equalsIgnoreCase("HOPE")) {
                        AuditTrail.generateAudit(new AuditTrail(servletRequest.getHeader("X-FORWARDED-FOR") != null ? servletRequest.getHeader("X-FORWARDED-FOR") : servletRequest.getRemoteAddr(), servletRequest.getSession().getAttribute("sessionId"), servletRequest.getSession().getAttribute("userTypeId"), servletRequest.getHeader("referer")), id, "tenderId", EgpModule.Complaint_Management.getName(), "Upload document by Hope", "");
                    } else if (ck[ii].equalsIgnoreCase("Secretary")) {
                        AuditTrail.generateAudit(new AuditTrail(servletRequest.getHeader("X-FORWARDED-FOR") != null ? servletRequest.getHeader("X-FORWARDED-FOR") : servletRequest.getRemoteAddr(), servletRequest.getSession().getAttribute("sessionId"), servletRequest.getSession().getAttribute("userTypeId"), servletRequest.getHeader("referer")), id, "tenderId", EgpModule.Complaint_Management.getName(), "Upload document by Secretary", "");
                    }
                }
            } else {
                if (session.getAttribute("userTypeId").toString().equals("17")) {
                    AuditTrail.generateAudit(new AuditTrail(servletRequest.getHeader("X-FORWARDED-FOR") != null ? servletRequest.getHeader("X-FORWARDED-FOR") : servletRequest.getRemoteAddr(), servletRequest.getSession().getAttribute("sessionId"), servletRequest.getSession().getAttribute("userTypeId"), servletRequest.getHeader("referer")), id, "tenderId", EgpModule.Complaint_Management.getName(), "Upload document by Review Panel", "");
                }
            }
        }
    }

    //for HOPE OFFICER COMPLAINT  
    @RequestMapping(value = "/resources/common/complaintOfficerHope.htm")
    public ModelAndView complaintOfficerHOPE(HttpServletRequest servletRequest, HttpServletResponse servletResponse) {
        ModelAndView mav = null;
        try {
            log.debug("in hope complaint");
            mav = new ModelAndView("compl_mgt_HOPE");
            String query = "";
            if (servletRequest.getSession().getAttribute("procurementRole") != null) {
                if (servletRequest.getSession().getAttribute("procurementRole").toString().equalsIgnoreCase("HOPE")) {
                    query = "2";
                } else if (servletRequest.getSession().getAttribute("procurementRole").toString().equalsIgnoreCase("secretary")) {
                    query = "3";
                } else {
                    query = "4";
                }
                List<TblComplaintMaster> hopeSearchList = mgmtService.searchPendingComplaintDetails(query);
                mav.addObject("hopeSearchList", hopeSearchList);

            }
            return mav;
        } catch (Exception e) {

            log.error("EXCEPTION AT METHOD: complaintOfficerHOPE() :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    @RequestMapping(value = "/resources/common/complaintOfficerProcessedHope.htm")
    public ModelAndView complaintOfficerProcessedHOPE(HttpServletRequest servletRequest, HttpServletResponse servletResponse) {
        ModelAndView mav = null;
        try {
            log.debug("in hope complaint");
            mav = new ModelAndView("compl_mgt_HOPE");
            String query = "";
            if (servletRequest.getSession().getAttribute("procurementRole") != null) {
                if (servletRequest.getSession().getAttribute("procurementRole").toString().equalsIgnoreCase("HOPE")) {
                    query = "2,3,4";
                } else if (servletRequest.getSession().getAttribute("procurementRole").toString().equalsIgnoreCase("secretary")) {
                    query = "3,4";
                } else {
                    query = "4";
                }
                List<TblComplaintMaster> hopeSearchList = mgmtService.searchProcessedComplaintDetails(query);
                mav.addObject("hopeSearchList", hopeSearchList);
                mav.addObject("status", "processed");
            }
            return mav;
        } catch (Exception e) {

            log.error("EXCEPTION AT METHOD: complaintOfficerHOPE() :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    //to search complaint list hope
    @RequestMapping(value = "/resources/common/search.htm")
    public ModelAndView hopeSearchList(HttpServletRequest servletRequest, HttpServletResponse servletResponse, HttpSession session) {

        ModelAndView mav = null;
        try {
            TblTenderDetails details = null;
            int complid = 0;
            int tenderid = 0;
            String refNo = null;
            int officeid = 0;
            log.debug("in search complaint hope");
            String refNumber = null;
            Integer govUserId = 0;
            String role = session.getAttribute("procurementRole").toString();
            String user = session.getAttribute("userId").toString();
            int userId = Integer.parseInt(user);

            List listGovId = mgmtService.getGovUserDetails(userId);
            if (listGovId != null && listGovId.size() > 0) {
                govUserId = (Integer) listGovId.get(0);
            } else {
                log.debug("listGovId is null");
            }
            log.debug("gov user id is" + govUserId);
            String complId = servletRequest.getParameter("txtCompID");
            log.debug("complId" + complId);
            String tenderId = servletRequest.getParameter("txtTenderID");
            log.debug("tenderId" + tenderId);
            refNo = servletRequest.getParameter("txtReferneceNo");
            refNumber = refNo;
            log.debug("refId" + refNo);

            if ("".equalsIgnoreCase(servletRequest.getParameter("txtCompID"))) {

                complid = 0;
                log.debug("complid is null>>>>>>>>>" + complid);
            } else {
                complid = Integer.parseInt(complId);
                log.debug("complid is not null>>>>>>>>>" + complid);
                List<Object[]> refNoCompanyName = mgmtService.getTenderDetails(complid);
                if (refNoCompanyName != null && refNoCompanyName.size() > 0) {
                    refNumber = refNoCompanyName.get(0).toString();
                } else {
                    log.debug("refNoCompanyName is null");
                }
            }
            if ("".equalsIgnoreCase(servletRequest.getParameter("txtTenderID"))) {
                tenderid = 0;
                log.debug("tenderid is  null>>>>>>>>>>" + tenderid);
            } else {
                tenderid = Integer.parseInt(tenderId);
                log.debug("tenderid is not null>>>>>>>>>>" + tenderid);
                List<TblTenderDetails> detailsRef = mgmtService.getTenderDetailsByRef("tblTenderMaster.tenderId", Operation_enum.EQ, tenderid);
                if (detailsRef != null && detailsRef.size() > 0) {
                    refNumber = detailsRef.get(0).getReoiRfpRefNo();
                } else {
                    log.debug("detailsRef is null");
                }
            }

            TblTenderDetails refDetails = null;
            List<TblTenderDetails> tenderDetails = mgmtService.getTenderDetailsByRef("reoiRfpRefNo", Operation_enum.EQ, refNo);
            if (tenderDetails != null && tenderDetails.size() == 1) {
                log.debug("in tenderdetails size is" + tenderDetails.size());
                refDetails = tenderDetails.get(0);
                log.debug("ref ob is" + refDetails);
            }
            List<TblComplaintMaster> hopeSearchList = mgmtService.searchComplaintDetails(tenderid, complid, refDetails, govUserId, role, "Clarification", false, 0);
            log.debug("in search of hope...........");
            mav = new ModelAndView("compl_mgt_HOPE");
            mav.addObject("hopeSearchList", hopeSearchList);
            mav.addObject("refNumber", refNumber);
            // To display process link after search in hope,secretary complaint management
            if ("".equalsIgnoreCase(servletRequest.getParameter("txtTenderID")) || "".equalsIgnoreCase(servletRequest.getParameter("txtReferneceNo"))) {
                if (!hopeSearchList.get(0).getComplaintStatus().equals("Pending")) {
                    mav.addObject("status", "status");
                }
            } else {
                if (servletRequest.getParameter("compstatus").equals("Process")) {
                    mav.addObject("status", "status");
                }

            }

            return mav;
        } catch (Exception e) {

            log.error("EXCEPTION AT METHOD: hopeSearchList() :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    //downloading file
    @RequestMapping(value = {"/tenderer/download.htm", "/download.htm", "/resources/common/download.htm"})
    public void processDoc(@RequestParam("docname") String docname, @RequestParam("complaintId") String complaintId, @RequestParam("tenderId") String tenderId, HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws IOException {
        String filename = docname;
        log.debug("doc name is" + docname);
        File file = new File(destinationDir + complaintId + "/" + docname);
        InputStream in = new BufferedInputStream(new FileInputStream(file));
        String contentType = "";
        boolean ifXLSDOC = false;

        if (filename.toLowerCase().endsWith(".xls")) {
            contentType = "application/vnd.ms-excel";
            ifXLSDOC = true;
        } else if (filename.toLowerCase().endsWith(".xlt")) {
            contentType = "application/vnd.ms-excel";
            ifXLSDOC = true;
        } else if (filename.toLowerCase().endsWith(".csv")) {
            contentType = "application/vnd.ms-excel";
            ifXLSDOC = true;
        } else if (filename.toLowerCase().endsWith(".doc")) {
            contentType = "application/msword";
            ifXLSDOC = true;
        } else if (filename.toLowerCase().endsWith(".rtf")) {
            contentType = "application/msword";
            ifXLSDOC = true;
        } else if (filename.toLowerCase().endsWith(".pdf")) {
            contentType = "application/pdf";
            ifXLSDOC = true;
        } else if (filename.toLowerCase().endsWith(".ppt")) {
            contentType = "application/ms-powerpoint";
            ifXLSDOC = true;
        } else if (filename.toLowerCase().endsWith(".pot")) {
            contentType = "application/ms-powerpoint";
            ifXLSDOC = true;
        } else if (filename.toLowerCase().endsWith(".html")) {
            contentType = "text/html";
        } else if (filename.toLowerCase().endsWith(".htm")) {
            contentType = "text/html";
        } else if (filename.toLowerCase().endsWith(".gif")) {
            contentType = "image/gif";
        } else if (filename.toLowerCase().endsWith(".jpg")) {
            contentType = "image/jpeg";
        } else if (filename.toLowerCase().endsWith(".jpeg")) {
            contentType = "image/jpeg";
        } else if (filename.toLowerCase().endsWith(".dot")) {
            contentType = "application/msword";
            ifXLSDOC = true;
        } else if (filename.toLowerCase().endsWith(".vsd")) {
            contentType = "application/vnd.visio";
            ifXLSDOC = true;
        } else if (filename.toLowerCase().endsWith(".mpp")) {
            contentType = "application/vnd.ms-project";
            ifXLSDOC = true;
        } else if (filename.toLowerCase().endsWith(".zip")) {
            contentType = "application/ZIP";
        } else if (filename.toLowerCase().endsWith(".txt")) {
            contentType = "text/plain";
        } else {
            contentType = "application/octet-stream";
        }
//		 Set the headers.
        servletResponse.setContentType(contentType);
        if (ifXLSDOC) {
            servletResponse.addHeader("Content-Disposition", "attachment; filename=" + filename);
        } else {
            servletResponse.addHeader("Content-Disposition", "attachment; filename=" + filename);
        }
//		 Send the file.
        OutputStream out = servletResponse.getOutputStream();

        try {
            servletResponse.setContentLength(in.available());
            byte[] buf = new byte[4 * 1024]; // 4K byte buffer
            int bytesRead;
            while ((bytesRead = in.read(buf)) != -1) {
                out.write(buf, 0, bytesRead);
            }
            if (in != null) {
                in.close();
            }
            out.flush();
            out.close();
            servletResponse.reset();
            servletResponse.resetBuffer();

        } catch (Exception e) {
            servletResponse.sendRedirect("/StackTrace.jsp");
        } finally {
            if (servletRequest.getSession().getAttribute("procurementRole") != null) {
                String ck[] = servletRequest.getSession().getAttribute("procurementRole").toString().split(",");
                for (int ii = 0; ii < ck.length; ii++) {
                    if (ck[ii].equalsIgnoreCase("PE")) {
                        AuditTrail.generateAudit(new AuditTrail(servletRequest.getHeader("X-FORWARDED-FOR") != null ? servletRequest.getHeader("X-FORWARDED-FOR") : servletRequest.getRemoteAddr(), servletRequest.getSession().getAttribute("sessionId"), servletRequest.getSession().getAttribute("userTypeId"), servletRequest.getHeader("referer")), Integer.parseInt(tenderId), "tenderId", EgpModule.Complaint_Management.getName(), "Download Document by PE", "");
                    } else if (ck[ii].equalsIgnoreCase("HOPE")) {
                        AuditTrail.generateAudit(new AuditTrail(servletRequest.getHeader("X-FORWARDED-FOR") != null ? servletRequest.getHeader("X-FORWARDED-FOR") : servletRequest.getRemoteAddr(), servletRequest.getSession().getAttribute("sessionId"), servletRequest.getSession().getAttribute("userTypeId"), servletRequest.getHeader("referer")), Integer.parseInt(tenderId), "tenderId", EgpModule.Complaint_Management.getName(), "Download Document by Hope", "");
                    } else if (ck[ii].equalsIgnoreCase("Secretary")) {
                        AuditTrail.generateAudit(new AuditTrail(servletRequest.getHeader("X-FORWARDED-FOR") != null ? servletRequest.getHeader("X-FORWARDED-FOR") : servletRequest.getRemoteAddr(), servletRequest.getSession().getAttribute("sessionId"), servletRequest.getSession().getAttribute("userTypeId"), servletRequest.getHeader("referer")), Integer.parseInt(tenderId), "tenderId", EgpModule.Complaint_Management.getName(), "Download Document by Secretary", "");
                    }
                }
            } else {
                if (servletRequest.getSession().getAttribute("userTypeId").toString().equals("17")) {
                    AuditTrail.generateAudit(new AuditTrail(servletRequest.getHeader("X-FORWARDED-FOR") != null ? servletRequest.getHeader("X-FORWARDED-FOR") : servletRequest.getRemoteAddr(), servletRequest.getSession().getAttribute("sessionId"), servletRequest.getSession().getAttribute("userTypeId"), servletRequest.getHeader("referer")), Integer.parseInt(tenderId), "tenderId", EgpModule.Complaint_Management.getName(), "Download Document by Review Panel", "");
                } else {
                    AuditTrail.generateAudit(new AuditTrail(servletRequest.getHeader("X-FORWARDED-FOR") != null ? servletRequest.getHeader("X-FORWARDED-FOR") : servletRequest.getRemoteAddr(), servletRequest.getSession().getAttribute("sessionId"), servletRequest.getSession().getAttribute("userTypeId"), servletRequest.getHeader("referer")), Integer.parseInt(tenderId), "tenderId", EgpModule.Complaint_Management.getName(), "Download Document by Tenderer", "");
                }
            }
        }

    }

    // for deleting documents
    @RequestMapping(value = {"/tenderer/delete.htm", "/delete.htm", "/resources/common/delete.htm"})
    public ModelAndView deleteDoc(@RequestParam("docId") Integer docId) {
        ModelAndView modelAndView = null;
        try {
            List<TblComplaintDocs> complaintDocList = mgmtService.findByDocId("complaintDocId", Operation_enum.EQ, docId);
            if (complaintDocList != null && complaintDocList.size() > 0) {
                mgmtService.deleteDoc(complaintDocList.get(0));
            } else {
                log.debug("complaintDocList is null");
            }
            List docsList = mgmtService.getDocs(complaintDocList.get(0).getComplaintMaster().getComplaintId(), complaintDocList.get(0).getComplaintLevel().getComplaintLevelId());
            modelAndView = new ModelAndView("view_uploaded_doc", "docsList", docsList);
            return modelAndView;
        } catch (Exception e) {

            log.error("EXCEPTION AT METHOD: deleteDoc() :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    //deletion
    @RequestMapping(value = {"/tenderer/deletePopUp.htm", "/deletePopUp.htm"})
    public ModelAndView deleteDocPopUp(@RequestParam("docId") Integer docId, @RequestParam("complaintId") String complaintId, @RequestParam("docname") String docName, @RequestParam("tenderId") String tenderId, HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws IOException {

        ModelAndView modelAndView = null;
        boolean delfile = false;
        try {
            List<TblComplaintDocs> complaintDocList = mgmtService.findByDocId("complaintDocId", Operation_enum.EQ, docId);
            if (complaintDocList != null && complaintDocList.size() > 0) {
                mgmtService.deleteDoc(complaintDocList.get(0));
                delfile = deleteFile(destinationDir + complaintId + "\\" + docName);
            } else {
                log.debug("complaintDocList is null");
            }
            //List docsList = mgmtService.getDocs(complaintDocList.get(0).getComplaintMaster().getComplaintId(), complaintDocList.get(0).getComplaintLevel().getComplaintLevelId());
            List docsList = null;
            TblComplaintMaster tcm = null;

            if (complaintDocList != null && complaintDocList.size() > 0) {
                docsList = mgmtService.getDocsByComplaintId(complaintDocList.get(0).getComplaintMaster().getComplaintId());
                tcm = complaintDocList.get(0).getComplaintMaster();
            } else {
                tcm = mgmtService.getComplaintDetails(Integer.parseInt(complaintId));
            }
            if (servletRequest.getParameter("flag") != null) {
                modelAndView = new ModelAndView("pe_file_upload");
                List listpe = mgmtService.getComplaintHistoryPe(tcm.getTenderMaster().getTenderId());
                modelAndView.addObject("complaintObject", tcm);
                modelAndView.addObject("uploadedDocs", mgmtService.getDocsByComplaintId(tcm.getComplaintId()));
                modelAndView.addObject("tenderId", tcm.getTenderMaster().getTenderId());
                modelAndView.addObject("listpe", listpe);
                modelAndView.addObject("errormsg", "Del");
            } else {
                modelAndView = new ModelAndView("popup", "uploadedDocs", docsList);
                modelAndView.addObject("complaintObject", tcm);
                modelAndView.addObject("errormsg", "Del");
            }
            return modelAndView;
        } catch (Exception e) {

            log.error("EXCEPTION AT METHOD: deleteDocPopUp() :" + e);
            return new ModelAndView("/StackTrace");
        } finally {
            if (servletRequest.getSession().getAttribute("procurementRole") != null) {
                String ck[] = servletRequest.getSession().getAttribute("procurementRole").toString().split(",");
                for (int ii = 0; ii < ck.length; ii++) {
                    if (ck[ii].equalsIgnoreCase("PE")) {
                        AuditTrail.generateAudit(new AuditTrail(servletRequest.getHeader("X-FORWARDED-FOR") != null ? servletRequest.getHeader("X-FORWARDED-FOR") : servletRequest.getRemoteAddr(), servletRequest.getSession().getAttribute("sessionId"), servletRequest.getSession().getAttribute("userTypeId"), servletRequest.getHeader("referer")), Integer.parseInt(tenderId), "tenderId", EgpModule.Complaint_Management.getName(), "Delete Document by PE", "");
                    } else if (ck[ii].equalsIgnoreCase("HOPE")) {
                        AuditTrail.generateAudit(new AuditTrail(servletRequest.getHeader("X-FORWARDED-FOR") != null ? servletRequest.getHeader("X-FORWARDED-FOR") : servletRequest.getRemoteAddr(), servletRequest.getSession().getAttribute("sessionId"), servletRequest.getSession().getAttribute("userTypeId"), servletRequest.getHeader("referer")), Integer.parseInt(tenderId), "tenderId", EgpModule.Complaint_Management.getName(), "Delete Document by Hope", "");
                    } else if (ck[ii].equalsIgnoreCase("Secretary")) {
                        AuditTrail.generateAudit(new AuditTrail(servletRequest.getHeader("X-FORWARDED-FOR") != null ? servletRequest.getHeader("X-FORWARDED-FOR") : servletRequest.getRemoteAddr(), servletRequest.getSession().getAttribute("sessionId"), servletRequest.getSession().getAttribute("userTypeId"), servletRequest.getHeader("referer")), Integer.parseInt(tenderId), "tenderId", EgpModule.Complaint_Management.getName(), "Delete Document by Secretary", "");
                    }
                }
            } else {
                if (servletRequest.getSession().getAttribute("userTypeId").toString().equals("17")) {
                    AuditTrail.generateAudit(new AuditTrail(servletRequest.getHeader("X-FORWARDED-FOR") != null ? servletRequest.getHeader("X-FORWARDED-FOR") : servletRequest.getRemoteAddr(), servletRequest.getSession().getAttribute("sessionId"), servletRequest.getSession().getAttribute("userTypeId"), servletRequest.getHeader("referer")), Integer.parseInt(tenderId), "tenderId", EgpModule.Complaint_Management.getName(), "Delete Document by Review Panel", "");
                } else {
                    AuditTrail.generateAudit(new AuditTrail(servletRequest.getHeader("X-FORWARDED-FOR") != null ? servletRequest.getHeader("X-FORWARDED-FOR") : servletRequest.getRemoteAddr(), servletRequest.getSession().getAttribute("sessionId"), servletRequest.getSession().getAttribute("userTypeId"), servletRequest.getHeader("referer")), Integer.parseInt(tenderId), "tenderId", EgpModule.Complaint_Management.getName(), "Delete Document by Tenderer", "");
                }
            }
        }
    }

    @RequestMapping(value = {"/clarify.htm", "/tenderer/clarify.htm"})
    public ModelAndView clarify(@RequestParam("complaintId") Integer complaintId) {
        TblComplaintMaster master = null;
        ModelAndView modelAndView = null;
        try {
            List<TblComplaintMaster> complaintMasterList = mgmtService.getComplaintById("complaintId", Operation_enum.EQ, complaintId);
            if (complaintMasterList != null && complaintMasterList.size() > 0) {
                master = complaintMasterList.get(0);
            } else {
                log.debug("complaintMasterList is null");
            }
            modelAndView = new ModelAndView("clarify_tenderer", "master", master);
            return modelAndView;
        } catch (Exception e) {

            log.error("EXCEPTION AT METHOD: clarify() :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    //to submit clarification
    @RequestMapping(value = "/tenderer/clarifySubmit.htm")
    public ModelAndView clarifySubmit(@RequestParam("complaintId") Integer complaintId, @RequestParam("tenderId") Integer tenderId, HttpServletRequest servletRequest, HttpServletResponse servletResponse, HttpSession session) {
        try {
            log.debug("in clarify submit");
            TblComplaintHistory history = new TblComplaintHistory();
            String uid = session.getAttribute("userTypeId").toString();
            byte usertypeid = (byte) Integer.parseInt(uid);
            TblUserTypeMaster usertype = null;
            TblComplaintMaster master = null;
            List<TblUserTypeMaster> lstUsertype = userTypeService.getTblusertypemasterdao().findTblUserTypeMaster("userTypeId", Operation_enum.EQ, usertypeid);
            if (lstUsertype != null && lstUsertype.size() > 0) {
                usertype = lstUsertype.get(0);
            } else {
                log.debug("lstUsertype is null");
            }
            log.debug("user type " + usertype);

            List<TblComplaintMaster> complaintMasterList = mgmtService.getComplaintById("complaintId", Operation_enum.EQ, complaintId);
            if (complaintMasterList != null && complaintMasterList.size() > 0) {
                master = complaintMasterList.get(0);
            } else {
                log.debug("complaintMasterList is null");
            }
            log.debug("master " + master);
            String details = servletRequest.getParameter("comments");

            log.debug("details is " + details);
            master.setTendererComments(details);
            master.setLastModificationDt(new Date());
            master.setComplaintStatus(ComplaintMgmtConstants.COMP_STATUS_PENDING);
            mgmtService.updateMaster(master);//update master
            log.debug("master updated");
            String role = "";
            history.setComplaint(master);
            history.setGovUserId(master.getEmployeeTrasfer().getGovUserId());
            history.setTendererId(master.getTendererMaster().getTendererId());
            history.setComplaintLevel(master.getComplaintLevel());
            history.setFileStatus(ComplaintMgmtConstants.COMP_STATUS_NEED_CLARIFY);
            history.setComplaintStatus(ComplaintMgmtConstants.COMP_STATUS_PENDING);
            history.setPanelId(0);
            history.setUserRole(usertype);
            history.setCommentsdt(new Date());
            history.setComments(details);
            mgmtService.addHistory(history); //add to history
            java.util.List<SPTenderCommonData> thisTenderInfoLst = tenderCommonService.returndata("tenderinfobar", master.getTenderMaster().getTenderId() + "", null);
            MailContentUtility utility = new MailContentUtility();
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String subject = "";
            String userName = servletRequest.getSession().getAttribute("userName").toString();
            String msgText = utility.sendMailToAuthForClarifyComp(master.getTenderMaster().getTenderId(), thisTenderInfoLst, master.getComplaintId(), master.getTendererMaster().getFirstName(), master.getComplaintType().getComplaintType());
            String[] strTo = {commonservice.getEmailId(master.getEmployeeTrasfer().getTblLoginMaster().getUserId() + "")};
            String strFrom = commonservice.getEmailId(servletRequest.getSession().getAttribute("userId").toString());
            subject = "e-GP: Bidder has responded clarification";
            registerService.contentAdmMsgBox(strTo[0], strFrom, subject, msgText);
            sendMessageUtil.setEmailTo(strTo);
            sendMessageUtil.setEmailFrom(strFrom);
            sendMessageUtil.setEmailSub(subject);
            sendMessageUtil.setEmailMessage(msgText);
            sendMessageUtil.sendEmail();
            sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
            StringBuilder sb = new StringBuilder();
            sb.append("Dear User,%0AThis is to inform you that Bidder has responded for Clarification for " + master.getTenderMaster().getTenderId());
            sb.append("%0AThanks,%0Ae-GP System");
            sendMessageUtil.setSmsBody(sb.toString());
            sendMessageUtil.sendSMS();
            log.debug("history added");

            List<TblComplaintMaster> complaintList = mgmtService.getComplaintById("tenderMaster.tenderId", Operation_enum.EQ, master.getTenderMaster().getTenderId());
            if (complaintList != null) {
                log.debug("list size of the complaints is" + complaintList.size());
            }
            // ModelAndView modelAndView = new ModelAndView("compl_mgmt_tenderer", "complaintList", complaintList);
            ModelAndView mav = new ModelAndView("popup");
            mav.addObject("complaintObject", master);
            mav.addObject("uploadedDocs", mgmtService.getDocsByComplaintId(master.getComplaintId()));
            mav.addObject("parentLink","618");
            return mav;
        } catch (Exception e) {
            log.error("EXCEPTION AT METHOD: clarifySubmit() :" + e);
            return new ModelAndView("/StackTrace");
        } finally {
            AuditTrail.generateAudit(new AuditTrail(servletRequest.getHeader("X-FORWARDED-FOR") != null ? servletRequest.getHeader("X-FORWARDED-FOR") : servletRequest.getRemoteAddr(), servletRequest.getSession().getAttribute("sessionId"), servletRequest.getSession().getAttribute("userTypeId"), servletRequest.getHeader("referer")), tenderId, "tenderId", EgpModule.Complaint_Management.getName(), "Clarification given", "");
        }
    }

    @RequestMapping(value = "/admin/reviewPanel.htm")
    public ModelAndView reviewPanelCreation(HttpServletRequest request, HttpServletResponse response) {
        log.debug("in review panel creation");
        ModelAndView mav = new ModelAndView("reviewPanelCreation");
        return mav;
    }

    @RequestMapping(value = "/admin/ViewInstructionRP.htm")
    public ModelAndView ViewInstructionRP(HttpServletRequest request, HttpServletResponse response) {
        log.debug("in review panel creation");
        ModelAndView mav = new ModelAndView("ViewInstructionRP");
        return mav;
    }

    @RequestMapping(value = "/admin/ViewreviewPanel.htm")
    public ModelAndView ViewreviewPanelCreation(HttpServletRequest request, HttpServletResponse response) {
        log.debug("in View review panel creation");
        ModelAndView mav = new ModelAndView("ViewreviewPanel");
        return mav;
    }

    @RequestMapping(value = "/admin/ViewReviewPanelDetail.htm")
    public ModelAndView ViewreviewPanelDetail(HttpServletRequest request, HttpServletResponse response) {
        log.debug("in View review panel detail");
        ModelAndView mav = new ModelAndView("ViewreviewPaneldetail");
        // AuditTrail.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), loginMaster.getUserId(), "userId", EgpModule.Complaint_Management.getName(), "View Review Panel", "");
        return mav;
    }

    // to submit review panel creation
    @RequestMapping(value = "/admin/submitReviewPanel.htm")
    public ModelAndView reviewPanelCreation(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        try {
            byte userTypeId = 17;
            TblUserTypeMaster userType = new TblUserTypeMaster();
            userType.setUserTypeId(userTypeId);

            //table login master
            byte failedAttempt = 0;
            TblLoginMaster loginMaster = new TblLoginMaster();
            String emailId = "";
            if (request.getParameter("isEdit") == null) {
                emailId = request.getParameter("emailId");
            } else {
                emailId = request.getParameter("emailId");
            }
            String password = request.getParameter("password");
            if (request.getParameter("isEdit") == null) {
                log.debug("in review panel creation for the login master table");


                loginMaster.setEmailId(emailId);
                loginMaster.setPassword(SHA1HashEncryption.encodeStringSHA1(password));
                loginMaster.setHintQuestion("");
                loginMaster.setHintAnswer("");
                loginMaster.setRegistrationType(ComplaintMgmtConstants.REGISTRATION_TYPE);
                loginMaster.setIsJvca(ComplaintMgmtConstants.NO);
                loginMaster.setBusinessCountryName(ComplaintMgmtConstants.COUNTRY_NAME);
                loginMaster.setNextScreen(ComplaintMgmtConstants.NEXT_SCREEN);
                loginMaster.setTblUserTypeMaster(userType);
                loginMaster.setIsEmailVerified(ComplaintMgmtConstants.YES);
                loginMaster.setFailedAttempt(failedAttempt);
                loginMaster.setFirstLogin(ComplaintMgmtConstants.YES);
                loginMaster.setIsPasswordReset(ComplaintMgmtConstants.NO);
                loginMaster.setResetPasswordCode("");
                loginMaster.setStatus(ComplaintMgmtConstants.APPROVED);
                loginMaster.setRegisteredDate(new Date());
                loginMaster.setNationality(ComplaintMgmtConstants.NATIONALITY);
                log.debug("after adding login master *************");
                mgmtService.addTblLoginMaster(loginMaster);


            }
            TblDesignationMaster desigMaster = new TblDesignationMaster();
            desigMaster.setDesignationId(1);
            //table review panel
            log.debug("in review panel creation for the REVIEW PANEL table");
            TblReviewPanel reviewPanel = new TblReviewPanel();
            String reviewPanelName = request.getParameter("reviewPanelName");
            String reviewMembers = request.getParameter("rwPanelMem");
            reviewPanel.setReviewPanelName(reviewPanelName);
            reviewPanel.setReviewMembers(reviewMembers);
            if (request.getParameter("isEdit") != null) {
                reviewPanel.setReviewPanelId(Integer.parseInt(request.getParameter("rpId")));
            }
            if (request.getParameter("isEdit") != null) {
                reviewPanel.setUserId(Integer.parseInt(request.getParameter("userId")));
                mgmtService.updateOrSaveReveiwPanel(reviewPanel);
            } else {
                reviewPanel.setUserId(loginMaster.getUserId());
                mgmtService.addReviewPanel(reviewPanel);
            }


            //table employee master
            log.debug("in the review panel creation for employee master table");
            TblEmployeeMaster employeeMaster = new TblEmployeeMaster();
            String employeeName = request.getParameter("fullName");
            String employeeNameBangla = request.getParameter("bngName");
            byte[] empBanglaName = employeeNameBangla.getBytes();
            String mobileNo = request.getParameter("mobileNo");
            String nationalId = request.getParameter("ntnlId");
            String uid = session.getAttribute("userId").toString();
            int userSessionId = Integer.parseInt(uid);

            //int userId=loginMaster.getUserId();
            if (request.getParameter("isEdit") != null) {
                employeeMaster.setEmployeeId(Integer.parseInt(request.getParameter("empId")));
            }
            employeeMaster.setEmployeeName(employeeName);
            employeeMaster.setEmployeeNameBangla(empBanglaName);
            employeeMaster.setTblDesignationMaster(desigMaster);
            employeeMaster.setMobileNo(mobileNo);
            employeeMaster.setCreatedBy(userSessionId);
            employeeMaster.setNationalId(nationalId);
            if (request.getParameter("isEdit") != null) {
                employeeMaster.setTblLoginMaster(new TblLoginMaster(Integer.parseInt(request.getParameter("userId"))));
                mgmtService.updateEmpMaster(employeeMaster);
            } else {
                employeeMaster.setTblLoginMaster(loginMaster);
                mgmtService.addEmployee(employeeMaster);
            }




            //table employee transfer
            log.debug("in the review panel creation for employee transfer table");
            TblEmployeeTrasfer empTransfer = new TblEmployeeTrasfer();
            if (request.getParameter("isEdit") == null) {
                empTransfer.setTblLoginMaster(loginMaster);
                empTransfer.setTransferDt(new Date());
                empTransfer.setCreatedBy(userSessionId);
                empTransfer.setRemarks("");
                empTransfer.setEmployeeName(employeeName);
                empTransfer.setEmployeeNameBangla(empBanglaName);
                empTransfer.setMobileNo(mobileNo);
                empTransfer.setNationalId(nationalId);
                empTransfer.setFinPowerBy(ComplaintMgmtConstants.FIN_POWER_BY);
                empTransfer.setEmployeeId(employeeMaster.getEmployeeId());
                empTransfer.setAction(ComplaintMgmtConstants.ACTION);
                empTransfer.setEmailId(emailId);
                empTransfer.setReplacedBy("");
                empTransfer.setReplacedByEmailId("");
                empTransfer.setIsCurrent(ComplaintMgmtConstants.YES);
                empTransfer.setComments("");
                mgmtService.addTblEmployeeTrasfer(empTransfer);
            }

            ModelAndView mav = new ModelAndView("reviewPanelCreation");
            if (request.getParameter("isEdit") != null) {
                AuditTrail.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), loginMaster.getUserId(), "userId", EgpModule.Complaint_Management.getName(), "Edit Review Panel", "");
                mav.addObject("msg", "edited");
            } else {
                AuditTrail.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), loginMaster.getUserId(), "userId", EgpModule.Complaint_Management.getName(), "Create Review Panel", "");
                mav.addObject("msg", "created");
            }
            return mav;
        } catch (Exception e) {
            log.error("EXCEPTION AT METHOD: reviewPanelCreation() :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    //notification to pe
    @RequestMapping(value = {"/resources/common/ComplNotification.htm", "/ComplNotification.htm"})
    public ModelAndView complNotification(HttpServletRequest req, HttpServletResponse res) {

        try {
            log.debug("in the controller ************************************************************************");
            int complaintId = Integer.parseInt(req.getParameter("complaintId"));
            log.debug("Complaint id is @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" + complaintId);
            TblComplaintMaster master = mgmtService.getComplaintDetails(complaintId);
            ModelAndView notCPTU = new ModelAndView("compl_notification_CPTU");
            notCPTU.addObject("master", master);
            log.debug("Complaints Notification CPTU page calling ");

            return notCPTU;
        } catch (Exception e) {
            log.error("EXCEPTION AT METHOD: complNotification() :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    public boolean checkExnAndSize(String extn, long size, String userType) {

        //    LOGGER.debug("checkExnAndSize  : " + logUserId + LOGGERSTART);
        boolean chextn = false;
        float fsize = 0.0f;
        float dsize = 0.0f;
        int j = extn.lastIndexOf('.');
        String lst = extn.substring(j + 1);
        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        String str = configurationMaster.getAllowedExtension();
        String[] str1 = str.split(",");
        for (int i = 0; i < str1.length; i++) {
            if (str1[i].trim().equalsIgnoreCase(lst)) {
                chextn = true;
            }
        }
        if (chextn) {
            fsize = size / (1024 * 1024);

            dsize = configurationMaster.getFileSize();

            if (dsize > fsize) {
                chextn = true;
            } else {
                chextn = false;
            }
        }
        //  LOGGER.debug("checkExnAndSize  : " + logUserId + LOGGEREND);
        return chextn;
    }

    public boolean deleteFile(String filePath) {
        boolean flag = false;
        File f = new File(filePath);
        if (f.delete()) {
            flag = true;
        }
        return flag;
    }
}
