/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.web.servicebean.ManageEmployeeGridSrBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */

// Search Formation changed by Emtaz on 15/May/2016
public class EditDepartmentServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        final Logger logger = Logger.getLogger(EditDepartmentServlet.class);
        String logUserId = "0";
        try {
            //<editor-fold>
            /*if (request.getParameter("action").equals("fetchData")) {
                ManageEmployeeGridSrBean manageEmployeeGridSrBean = new ManageEmployeeGridSrBean();
                if (request.getSession().getAttribute("userId") != null) {
                    logUserId = request.getSession().getAttribute("userId").toString();
                    manageEmployeeGridSrBean.setLogUserId(logUserId);
                }
                logger.debug(request.getParameter("action") + " : " + logUserId + " Starts");
                response.setContentType("text/xml;charset=UTF-8");
                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String sord = request.getParameter("sord");
                String sidx = request.getParameter("sidx");
                String deptType = request.getParameter("deptType");
                manageEmployeeGridSrBean.setSearch(_search);
                manageEmployeeGridSrBean.setLimit(Integer.parseInt(rows));
                int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));
                logger.debug("offset : " + logUserId + offset);
                
                manageEmployeeGridSrBean.setOffset(offset);
                manageEmployeeGridSrBean.setSortOrder(sord);
                manageEmployeeGridSrBean.setSortCol(sidx);

                PrintWriter out = response.getWriter();
                String searchField = "", searchString = "", searchOper = "";
                
                List<TblDepartmentMaster> departmentMasterList = null;
                logger.debug("queryString : " + logUserId + request.getQueryString());
                if(_search){
                    searchField = request.getParameter("searchField");
                    searchString = request.getParameter("searchString");
                    searchOper = request.getParameter("searchOper");
                    departmentMasterList = manageEmployeeGridSrBean.getDepartmentMasterList_search(deptType, searchField, searchString, searchOper);//parameter change
                } else {
                    departmentMasterList = manageEmployeeGridSrBean.getDepartmentMasterList(deptType);//parameter change
                }
                
                int totalCount = 0;
                int totalPages = 0;
                
                if(_search){
                    
                    totalCount = (int) manageEmployeeGridSrBean.getSearchCountOfDept(deptType, searchField, searchString, searchOper);
                } else {
                    totalCount = (int) manageEmployeeGridSrBean.getAllCountOfDept(deptType);
                }
                
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                        totalPages = totalCount / Integer.parseInt(request.getParameter("rows"));
                    } else {
                        totalPages = (totalCount / Integer.parseInt(request.getParameter("rows"))) + 1;
                    }

                } else {
                    totalPages = 0;
                }

                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + departmentMasterList.size() + "</records>");
                int j =0;
                int no = Integer.parseInt(request.getParameter("page"));
                if(no == 1){
                    j = 1;
                }else{
                    if(Integer.parseInt(rows) == 30){
                        j = ((no-1)*30)+1;
                    }else if(Integer.parseInt(rows) == 20){
                        j = ((no-1)*20)+1;
                    }
                    else{
                        j = ((no-1)*10)+1;
                    }
                }
                // be sure to put text data in CDATA
                for (int i = 0; i < departmentMasterList.size(); i++) {
                    //System.out.println(i + ".==>" + departmentMasterList.get(i).getDepartmentType());
                    out.print("<row id='" + departmentMasterList.get(i).getDepartmentId() + "'>");
                    out.print("<cell>" + j + "</cell>");
                    out.print("<cell><![CDATA[" + departmentMasterList.get(i).getDepartmentName() + "]]></cell>");
                    String link="<a href=\"EditEmployee.jsp?deptid="+ departmentMasterList.get(i).getDepartmentId() +"&division=" + departmentMasterList.get(i).getDepartmentType() +"\">Edit</a> | <a href=\"ViewEmployee.jsp?deptid="+ departmentMasterList.get(i).getDepartmentId() +"&division=" + departmentMasterList.get(i).getDepartmentType() +"&action=view&msg=" + "\">View</a>";
                    out.print("<cell><![CDATA[" + link+ "]]></cell>");
                    out.print("</row>");
                    j++;
                }
                out.print("</rows>");
            }*/
            //</editor-fold>
            if (request.getParameter("action").equals("fetchData")) {
                ManageEmployeeGridSrBean manageEmployeeGridSrBean = new ManageEmployeeGridSrBean();
                if (request.getSession().getAttribute("userId") != null) {
                    logUserId = request.getSession().getAttribute("userId").toString();
                    manageEmployeeGridSrBean.setLogUserId(logUserId);
                }
                logger.debug(request.getParameter("action") + " : " + logUserId + " Starts");
                response.setContentType("text/html;charset=UTF-8");
                int pageNo = Integer.parseInt(request.getParameter("pageNo"));
                int Size = Integer.parseInt(request.getParameter("size"));
                String deptType = request.getParameter("deptType");
                String DeptName = request.getParameter("DeptName");
                
                manageEmployeeGridSrBean.setLimit(1000000);
                int offset = 0;
                manageEmployeeGridSrBean.setOffset(offset);
                
                logger.debug("offset : " + logUserId + offset);
                
                manageEmployeeGridSrBean.setOffset(offset);
              
                PrintWriter out = response.getWriter();
                
                List<TblDepartmentMaster> departmentMasterList = null;
                List<TblDepartmentMaster> departmentMasterListSearched = new ArrayList<TblDepartmentMaster>();
                logger.debug("queryString : " + logUserId + request.getQueryString());
                
                departmentMasterList = manageEmployeeGridSrBean.getDepartmentMasterList(deptType);//parameter change
                
                for(int j=0;j<departmentMasterList.size();j++)
                {
                    boolean ToAdd = true;
                    if(DeptName!=null && !DeptName.equalsIgnoreCase(""))
                    {
                        if(!DeptName.equalsIgnoreCase(departmentMasterList.get(j).getDepartmentName()))
                        {
                            ToAdd = false;
                        }
                    }
                    if(ToAdd)
                    {
                        //SPCommonSearchData commonAppData = getGovData.get(j);
                        departmentMasterListSearched.add(departmentMasterList.get(j));
                    }
                }
                
                int RecordFrom = (pageNo-1)*Size;
                int k= 0;
                String styleClass = "";
                if (departmentMasterListSearched != null && !departmentMasterListSearched.isEmpty()) {
                    for(k=RecordFrom;k<RecordFrom+Size && k<departmentMasterListSearched.size();k++)
                    {
                        if(k%2==0){
                            styleClass = "bgColor-white";
                        }else{
                            styleClass = "bgColor-Green";
                        }
                        out.print("<tr class='"+styleClass+"'>");
                        out.print("<td width=\"5%\" class=\"t-align-center\">" + (k+1) + "</td>");
                        out.print("<td width=\"70%\" class=\"t-align-center\">" + departmentMasterListSearched.get(k).getDepartmentName() + "</td>");
                        String link = "<a href=\"EditEmployee.jsp?deptid="+ departmentMasterListSearched.get(k).getDepartmentId() +"&division=" + departmentMasterListSearched.get(k).getDepartmentType() +"\">Edit</a> | <a href=\"ViewEmployee.jsp?deptid="+ departmentMasterListSearched.get(k).getDepartmentId() +"&division=" + departmentMasterListSearched.get(k).getDepartmentType() +"&action=view&msg=" + "\">View</a>";
                        out.print("<td width=\"25%\" class=\"t-align-center\">" + link + "</td>");
                        out.print("</tr>");
                    }

                }
                else
                {
                    out.print("<tr>");
                    out.print("<td colspan=\"3\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }  
                int totalPages = 1;
                if (departmentMasterListSearched.size() > 0) {
                    totalPages = (int) (Math.ceil(Math.ceil(departmentMasterListSearched.size()) / Size));
                    System.out.print("totalPages--"+totalPages+"records "+ departmentMasterListSearched.size());
                    if (totalPages == 0) {
                        totalPages = 1;
                    }
                }
                out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
            }
        } catch (Exception ex) {
            logger.error(request.getParameter("action") + " : " + logUserId + ex);
        } finally {
            logger.debug(request.getParameter("action") + " : " + logUserId + " Ends");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
