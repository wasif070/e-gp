/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.daointerface.HibernateQueryDao;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.List;

/**
 *
 * @author taher
 */
public class GetDepartMentChildIdsByParentId {

    public String getDepIds(String deptid) {
        StringBuffer depIds = new StringBuffer();
        depIds.append(deptid);
        depIds.append(",");
        HibernateQueryDao hqd = (HibernateQueryDao) AppContext.getSpringBean("HibernateQueryDao");
        List<Object> dms = hqd.getSingleColQuery("select departmentId from TblDepartmentMaster where parentDepartmentId=" + deptid);
        for (Object depId : dms) {
            depIds.append(depId.toString());
            depIds.append(",");
            depIds.append(_toDepartmentMasterDataBean(depId));
        }

        return (depIds.lastIndexOf(",") == depIds.length()-1) ? depIds.substring(0, depIds.length()-1) : depIds.toString();
    }

    public String _toDepartmentMasterDataBean(Object deptid) {
        StringBuffer depIds = new StringBuffer();
        HibernateQueryDao hqd = (HibernateQueryDao) AppContext.getSpringBean("HibernateQueryDao");
        List<Object> dms = hqd.getSingleColQuery("select departmentId from TblDepartmentMaster where parentDepartmentId=" + deptid.toString());
        for (Object depId : dms) {
            depIds.append(depId.toString());
            depIds.append(",");
            depIds.append(_toDepartmentMasterDataBean(depId));
        }
        return depIds.toString();
    }
}
