/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblCountryMaster;
import com.cptu.egp.eps.model.table.TblScBankDevPartnerMaster;
import com.cptu.egp.eps.model.table.TblStateMaster;
import com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class ScBankServlet extends HttpServlet
{
    static final Logger LOGGER = Logger.getLogger(ScBankServlet.class);
    String logUserId = "0";
    private AuditTrail auditTrail;
    ScBankDevpartnerService scBankDevpartnerService = (ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");
    public void setAuditTrail(AuditTrail auditTrail) {
            this.auditTrail  = auditTrail;
            scBankDevpartnerService.setAuditTrail(auditTrail);
    }
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        
        }

    // <editor-fold defaultstate="expanded" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        PrintWriter out = response.getWriter();
        String isBranch = request.getParameter("isBranch");

        String partnerType = request.getParameter("partnerType");
        String officeId = request.getParameter("officeId");
        String action = request.getParameter("action");
        String funName = request.getParameter("funName");
        HttpSession session = request.getSession();
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
        }
        
        
        
        
        
        scBankDevpartnerService.setUserId(logUserId);
        if (funName != null && funName.equals("verifyOffice")) {
            LOGGER.debug("verifyOffice function name "+logUserId+" Starts:");
            try {
                String title = request.getParameter("title");
                String officeName = request.getParameter("officeName");
                String type = request.getParameter("type");
                int headOfficeId=0;
                String headOffice=request.getParameter("headOffice");
                String mainOfc = request.getParameter("mainOffice");
                if("Yes".equalsIgnoreCase(isBranch) && headOffice!=null){
                    headOfficeId=Integer.parseInt(headOffice);
                }
                String msg = scBankDevpartnerService.verifyOfficeName(officeName, isBranch, type,headOfficeId,mainOfc);
                //String msg  = "OK";
                if (!msg.isEmpty() && !"OK".equalsIgnoreCase(msg)) {
                    if("Yes".equalsIgnoreCase(isBranch)){
                        if(title.equalsIgnoreCase("development partner")){
                            title = "Regional/Country Office";
                        }
                        if(title.equalsIgnoreCase("scheduled bank")){
                            title = "Scheduled Financial Institute Branch";
                        }
                    }
                    msg = title + " " + msg;
                }
                
                out.print(msg);
                LOGGER.debug("verifyOffice function name "+logUserId+" Ends:");
                out.flush();
            }
            finally {
                out.close();
            }
        }
        else if (funName != null && funName.equals("verifyMobile")) {
            LOGGER.debug("verifyMobile function name "+logUserId+" Starts:");
            try {
                //String mobileNo = request.getParameter("mobileNo");
                //String msg = scBankDevpartnerService.verifyMobileNo(mobileNo);
               // out.print(msg);
                out.flush();
                LOGGER.debug("verifyMobile function name "+logUserId+" Ends:");
            }
            finally {
                out.close();
            }
        }
        else if (funName != null && funName.equals("verifyNtnlId")) {
            LOGGER.debug("verifyNtnlId function name "+logUserId+" Starts:");
            try {
               // String ntnlId = request.getParameter("ntnlId");
                //String msg = scBankDevpartnerService.verifyNationalId(ntnlId);Commented bez no need to check the uniqueness of ntnID
                //String msg = "Ok";
                out.print("Ok");
                LOGGER.debug("verifyNtnlId function name "+logUserId+" Ends:");
                out.flush();
            }
            finally {
                out.close();
            }
        }
        else {
            String mobileNo = "-";
            TblScBankDevPartnerMaster tblScBankDevPartnerMaster = createScDevPartMaster(request);
            if (action.equals("create")) {
                setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
                LOGGER.debug("create action "+logUserId+" Starts:");
                int flag = scBankDevpartnerService.createScBankDevPartner(tblScBankDevPartnerMaster);

                if (isBranch.equalsIgnoreCase("Yes")) {
                    if (flag > -1) {//SchBankDevPartnerCreation
                        LOGGER.debug(partnerType + " - " + isBranch + "Creation Successfull");
                        response.sendRedirect("admin/ViewSchBankDevPartBrnch.jsp?partnerType=" + partnerType + "&officeId=" + flag + "&msg=success&from=created");
                    }
                    else {
                        response.sendRedirect("admin/SchBankDevPartnerBranchCreation.jsp?partnerType=" + partnerType + "&msg=fail");
                    }
                }
                else {
                    if (flag > -1) {
                        response.sendRedirect("admin/ViewSchBankDevPartner.jsp?partnerType=" + partnerType + "&officeId=" + flag + "&msg=success&from=created");
                    }
                    else {
                        response.sendRedirect("admin/SchBankDevPartnerCreation.jsp?partnerType=" + partnerType + "&msg=fail");
                    }
                }
                LOGGER.debug("create action "+logUserId+" Ends:");
            }
            else if ("update".equals(action)) {
                setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
                LOGGER.debug("update action "+logUserId+" Starts:");
                boolean flag = scBankDevpartnerService.updateScBankDevPartner(tblScBankDevPartnerMaster);
                if (isBranch.equals("Yes")) {
                    if (flag) {
                        response.sendRedirect("admin/ViewSchBankDevPartBrnch.jsp?partnerType=" + partnerType + "&officeId=" + officeId + "&msg=success&from=updated");
                    }
                    else {
                        response.sendRedirect("admin/EditSchBankDevPartnerBranch.jsp?partnerType=" + partnerType + "&officeId=" + officeId + "&msg=fail");
                    }
                }
                else {
                    if (flag) {
                        response.sendRedirect("admin/ViewSchBankDevPartner.jsp?partnerType=" + partnerType + "&officeId=" + officeId + "&msg=success&from=updated");
                    }
                    else {
                        response.sendRedirect("admin/EditSchBankDevPartner.jsp?partnerType=" + partnerType + "&officeId=" + officeId + "&msg=fail");
                    }
                }
                setAuditTrail(null);
                LOGGER.debug("update action "+logUserId+" Ends:");
            }
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

    private TblScBankDevPartnerMaster createScDevPartMaster(HttpServletRequest request)
    {
        LOGGER.debug("createScDevPartMaster "+logUserId+" Starts:");
        TblScBankDevPartnerMaster tblScBankDevPartnerMaster = new TblScBankDevPartnerMaster();

        String action = request.getParameter("action");
        if ("create".equals(action)) {
            LOGGER.debug("createScDevPartMaster create"+logUserId+" Starts:");
            HttpSession session = request.getSession();
            Object objUserId = session.getAttribute("userId");
            tblScBankDevPartnerMaster.setCreatedBy((Integer) objUserId);
            tblScBankDevPartnerMaster.setCreatedDate(new Date());
            String swiftCode = request.getParameter("swiftCode");
            if (swiftCode == null) {
                swiftCode = "";
            }
            tblScBankDevPartnerMaster.setSwiftCode(swiftCode);
            LOGGER.debug("createScDevPartMaster create"+logUserId+" Ends:");
        }
        else if ("update".equals(action)) {
            LOGGER.debug("createScDevPartMaster update"+logUserId+" Starts:");
            int createdBy = Integer.parseInt(request.getParameter("createdBy"));
            tblScBankDevPartnerMaster.setCreatedBy(createdBy);
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date createdDate = dateFormat.parse(request.getParameter("createdDate"));
                tblScBankDevPartnerMaster.setCreatedDate(createdDate);
            }
            catch (ParseException ex) {
                LOGGER.debug("createScDevPartMaster "+logUserId+" :"+ ex);
            }
            String swiftCode = request.getParameter("swiftCode");
            if (swiftCode == null) {
                swiftCode = "";
            }
            tblScBankDevPartnerMaster.setSwiftCode(swiftCode);
            int officeId = Integer.parseInt(request.getParameter("officeId"));
            tblScBankDevPartnerMaster.setSbankDevelopId(officeId);
            LOGGER.debug("createScDevPartMaster update"+logUserId+" Ends:");
        }
        String isBranch = request.getParameter("isBranch");
        tblScBankDevPartnerMaster.setIsBranchOffice(isBranch);
        if (isBranch.equalsIgnoreCase("Yes")) {
            int branchHead = Integer.parseInt(request.getParameter("branchHead"));
            tblScBankDevPartnerMaster.setSbankDevelHeadId(branchHead);
        }
        else {
            tblScBankDevPartnerMaster.setSbankDevelHeadId(0);
        }
        String partnerType = request.getParameter("partnerType");
        tblScBankDevPartnerMaster.setPartnerType(partnerType);
        String bank = request.getParameter("bankName");
        tblScBankDevPartnerMaster.setSbDevelopName(bank.trim());
        String address = request.getParameter("officeAddress");
        tblScBankDevPartnerMaster.setOfficeAddress(address);
        short countryId = Short.parseShort(request.getParameter("country"));
        tblScBankDevPartnerMaster.setTblCountryMaster(new TblCountryMaster(countryId));
        short stateId = Short.parseShort(request.getParameter("state"));
        tblScBankDevPartnerMaster.setTblStateMaster(new TblStateMaster(stateId));
        tblScBankDevPartnerMaster.setDistrict("");
        String thana = request.getParameter("thanaUpzilla");
        if (thana == null) {
            thana = " ";
        }
//        if (countryId != 136) {
//            thana = " ";
//        }
        tblScBankDevPartnerMaster.setThanaUpzilla(thana);
        String city = request.getParameter("city");
        tblScBankDevPartnerMaster.setCityTown(city);
        String postCode = request.getParameter("postCode");
        tblScBankDevPartnerMaster.setPostCode(postCode);
        String phoneNo = request.getParameter("phoneNo");
        tblScBankDevPartnerMaster.setPhoneNo(phoneNo);
        String faxNo = request.getParameter("faxNo");
        if (faxNo == null) {
            faxNo = "";
        }
        tblScBankDevPartnerMaster.setFaxNo(faxNo);
        String site = request.getParameter("webSite");
        if (site == null) {
            site = "";
        }
        tblScBankDevPartnerMaster.setWebsite(site);
        LOGGER.debug("createScDevPartMaster "+logUserId+" Ends:");
        return tblScBankDevPartnerMaster;
    }

    
}
