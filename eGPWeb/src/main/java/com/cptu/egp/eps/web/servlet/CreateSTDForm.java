/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblCmsSrvBoqMaster;
import com.cptu.egp.eps.model.table.TblCmsTemplateSrvBoqDetail;
import com.cptu.egp.eps.model.table.TblTemplateFormulas;
import com.cptu.egp.eps.model.table.TblTemplateMandatoryDoc;
import com.cptu.egp.eps.model.table.TblTemplateMaster;
import com.cptu.egp.eps.model.table.TblTemplateSectionForm;
import com.cptu.egp.eps.model.table.TblTemplateSections;
import com.cptu.egp.eps.model.table.TblTemplateTables;
import com.cptu.egp.eps.model.table.TblTenderMandatoryDoc;
import com.cptu.egp.eps.service.serviceimpl.SpXMLCommonImpl;
import com.cptu.egp.eps.service.serviceimpl.TemplateSectionFormImpl;
import com.cptu.egp.eps.service.serviceimpl.TemplateTableImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author dipti
 */
//@WebServlet(name = "CreateSTDForm", urlPatterns = {"/CreateSTDForm"})
public class CreateSTDForm extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(CreateSTDForm.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND = "Ends";
    private String logUserId = "0";
    private final TemplateSectionFormImpl templateForm = (TemplateSectionFormImpl) AppContext.getSpringBean("AddFormService");
   // private final TemplateSectionFormImpl delForm = (TemplateSectionFormImpl) AppContext.getSpringBean("AddFormService");
    private final TemplateTableImpl templateTable = (TemplateTableImpl) AppContext.getSpringBean("AddFormTableService");
    private final SpXMLCommonImpl spXMLCommonImpl = (SpXMLCommonImpl) AppContext.getSpringBean("SpXMLCommonService");

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public static String handleSpecialCharHere(String str) {
        str = str.replaceAll("&", "&amp;");
        str = str.replaceAll(">", "&gt;");
        str = str.replaceAll("<", "&lt;");
        str = str.replaceAll("'", "&rsquo;");
        //str = str.replaceAll("'", "&#39;");
        str = str.replaceAll("\"", "quot;");
        return str;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LOGGER.debug("processRequest : " + logUserId + LOGGERSTART);

        logUserId = request.getSession().getAttribute("userId").toString();
        templateForm.setUserId(logUserId);
        templateTable.setUserId(logUserId);
        spXMLCommonImpl.setLogUserId(logUserId);
        templateForm.setUserId(logUserId);
        templateForm.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
        templateTable.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            HttpSession httpSession = request.getSession();
            if (httpSession.getAttribute("userId") == null) {
                response.sendRedirect("SessionTimedOut.jsp");
                //response.sendRedirectFilter("SessionTimedOut.jsp");
            } else {

                String action = request.getParameter("action");
                if ("formCreation".equalsIgnoreCase(action)) {
                    LOGGER.debug("processRequest : action : " + action + " : logUserId " + LOGGERSTART);
                    String operation = request.getParameter("btnCreateEdit");
                    String strFormType = "";
                    if(request.getParameter("rad")!=null)
                    {
                        strFormType = request.getParameter("rad");
                    }
                    short templateId = Short.parseShort(request.getParameter("templateId"));
                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    int formId = 0;
                    TblTemplateMaster tblTemplateMaster = new TblTemplateMaster(templateId);
                    TblTemplateSections tblTemplateSections = new TblTemplateSections(sectionId);
                    String formName = request.getParameter("formName");
                    String formHeader = request.getParameter("formHeader");
                    String formFooter = request.getParameter("formFooter");
                    byte noOfTable = (byte) Integer.parseInt(request.getParameter("noOfTables"));
                    String isMultipleFilling = "";
                    if (request.getParameter("multipleTimeFill").equals("yes")) {
                        isMultipleFilling = "yes";
                    } else {
                        isMultipleFilling = "no";
                    }
                    byte filledBy = 1;
                    String isBoq = "";
                    String isEnc = "";
                    String isMandatoryForm = "";
                    String typeOfForm = "";
                    if (request.getParameter("boqForm").equals("yes")) {
                        isBoq = "yes";
                    } else {
                        isBoq = "no";
                    }
                    if (request.getParameter("mandatoryForm").equals("yes")) {
                        isMandatoryForm = "yes";
                    } else {
                        isMandatoryForm = "no";
                    }
                    if (request.getParameter("typeOfForm").equals("manufactured")) {
                        typeOfForm = "manufactured";
                    } else if (request.getParameter("typeOfForm").equals("imported")) {
                        typeOfForm = "imported";
                    } else if (request.getParameter("typeOfForm").equals("BOQsalvage")) {
                        typeOfForm = "BOQsalvage";
                    }
                      else
                         typeOfForm = "na";
                    isEnc = "yes";
                    TblTemplateSectionForm tblForm = new TblTemplateSectionForm();
                    if ("Edit".equals(operation)) {
                        formId = Integer.parseInt(request.getParameter("formId"));
                        tblForm.setFormId(formId);
                    }
                    tblForm.setTblTemplateMaster(tblTemplateMaster);
                    tblForm.setTblTemplateSections(tblTemplateSections);
                    tblForm.setFormName(formName);
                    tblForm.setFormHeader(formHeader);
                    tblForm.setFormFooter(formFooter);
                    tblForm.setNoOfTables(noOfTable);
                    tblForm.setFilledBy(filledBy);
                    tblForm.setIsPriceBid(isBoq);
                    tblForm.setIsEncryption(isEnc);
                    tblForm.setIsMultipleFilling(isMultipleFilling);
                    tblForm.setStatus("p");
                    tblForm.setIsMandatory(isMandatoryForm);
                    tblForm.setFormType(typeOfForm);
                    if ("Create".equals(operation)) {
                        if (templateForm.addForm(tblForm)) {
                            if(strFormType!=null && !"".equalsIgnoreCase(strFormType))
                            {
                                TblCmsTemplateSrvBoqDetail tctsbd = new TblCmsTemplateSrvBoqDetail();
                                tctsbd.setTblTemplateSectionForm(new TblTemplateSectionForm(tblForm.getFormId()));
                                tctsbd.setTblCmsSrvBoqMaster(new TblCmsSrvBoqMaster(Integer.parseInt(strFormType)));
                                templateForm.addCmsTemplateSrvBoqDetail(tctsbd);
                                tctsbd = null;
                            }

                            int frmId = tblForm.getFormId();
                            tblForm = null;
                            response.sendRedirect("admin/CreateFormTable.jsp?templateId=" + templateId + "&sectionId=" + sectionId + "&formId=" + frmId + "&noOfTables=" + noOfTable);
                            //response.sendRedirectFilter("CreateFormTable.jsp?templateId=" + templateId + "&sectionId=" + sectionId + "&formId=" + frmId + "&noOfTables=" + noOfTable);
                        }
                        } else {

                        if (templateForm.editForm(tblForm)) {

                            //if(strFormType!=null && !"".equalsIgnoreCase(strFormType))
                            //{
                            //    String srvBoqDetailIdstr = request.getParameter("srvBoqDetailId");
                            //    templateForm.updateCmsTemplateSrvBoqDetail(Integer.parseInt(srvBoqDetailIdstr),Integer.parseInt(strFormType),tblForm.getFormId());
                            //}

                            int frmId = tblForm.getFormId();
                            tblForm = null;
                            response.sendRedirect("admin/TableDashboard.jsp?templateId=" + templateId + "&sectionId=" + sectionId + "&formId=" + frmId + "&noOfTables=" + noOfTable);
                            //response.sendRedirectFilter("TableDashboard.jsp?templateId=" + templateId + "&sectionId=" + sectionId + "&formId=" + frmId + "&noOfTables=" + noOfTable);
                        }
                    }
                    LOGGER.debug("processRequest : action : " + action + " : logUserId " + LOGGEREND);
                } else if ("delForm".equalsIgnoreCase(action)) {
                    LOGGER.debug("processRequest : action : " + action + " : logUserId " + LOGGERSTART);
                    short templateId = Short.parseShort(request.getParameter("templateId"));
                    int formId = Integer.parseInt(request.getParameter("formId"));
                    templateForm.deleteForm(formId);
                    LOGGER.debug("processRequest : action : " + action + " : logUserId " + LOGGEREND);
                    //response.sendRedirectFilter("admin/DefineSTDInDtl.jsp?templateId=" + templateId);
                    response.sendRedirect("admin/DefineSTDInDtl.jsp?templateId=" + templateId);
                } else if ("formTableCreation".equalsIgnoreCase(action)) {

                    LOGGER.debug("processRequest : action : " + action + " : logUserId " + LOGGERSTART);
                    templateTable.setUserId(logUserId);
                    String operation = request.getParameter("btnCreateEdit");

                    short noOfTables = Short.parseShort(request.getParameter("TableCount"));
                    short templateId = Short.parseShort(request.getParameter("templateId"));
                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    int formId = Integer.parseInt(request.getParameter("formId"));
                    String isMulFilling = "";
                    int tableId = 0;
                    if ("Edit".equals(operation)) {
                        tableId = Integer.parseInt(request.getParameter("tableId"));
                    }
                    for (short i = 1; i <= noOfTables; i++) {
                        TblTemplateTables tblTable = new TblTemplateTables();
                        if ("Edit".equals(operation)) {
                            tblTable.setTableId(tableId);
                        }
                        tblTable.setTblTemplateMaster(new TblTemplateMaster(templateId));
                        tblTable.setTblTemplateSectionForm(new TblTemplateSectionForm(formId));
                        tblTable.setSectionId(sectionId);
                        tblTable.setTableName(handleSpecialCharHere(request.getParameter("TableName" + i)));
                        tblTable.setTableHeader(request.getParameter("TableHeader" + i));
                        tblTable.setTableFooter(request.getParameter("TableFooter" + i));
                        tblTable.setNoOfCols(Short.parseShort(request.getParameter("NoOfCols" + i)));
                        tblTable.setNoOfRows(Short.parseShort(request.getParameter("NoOfRows" + i)));
                        if ("yes".equals(request.getParameter("MultipleFilling" + i))) {
                            isMulFilling = "yes";
                        } else {
                            isMulFilling = "no";
                        }
                        tblTable.setIsMultipleFilling(isMulFilling);
                        if ("Edit".equals(operation)) {
                            templateTable.editFormTable(tblTable);
                        } else {
                            templateTable.addFormTable(tblTable);
                        }
                        tblTable = null;
                    }
                    LOGGER.debug("processRequest : action : " + action + " : logUserId " + LOGGEREND);
                    response.sendRedirect("admin/TableDashboard.jsp?templateId=" + templateId + "&sectionId=" + sectionId + "&formId=" + formId);
                    //response.sendRedirectFilter("TableDashboard.jsp?templateId=" + templateId + "&sectionId=" + sectionId + "&formId=" + formId);
                } else if ("formTableDel".equalsIgnoreCase(action)) {
                    LOGGER.debug("processRequest : action : " + action + " : logUserId " + LOGGERSTART);
                    short templateId = Short.parseShort(request.getParameter("templateId"));
                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    int formId = Integer.parseInt(request.getParameter("formId"));
                    int tableId = Integer.parseInt(request.getParameter("tableId"));


                    templateTable.setUserId(logUserId);

                    templateTable.deleteFormTable(tableId);
                    LOGGER.debug("processRequest : action : " + action + " : logUserId " + LOGGEREND);
                    response.sendRedirect("admin/TableDashboard.jsp?templateId=" + templateId + "&sectionId=" + sectionId + "&formId=" + formId);
                    //response.sendRedirectFilter("TableDashboard.jsp?templateId=" + templateId + "&sectionId=" + sectionId + "&formId=" + formId);
                } else if ("tableMatrix".equalsIgnoreCase(action)) {
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                    LOGGER.debug("processRequest : action : " + action + " : logUserId " + LOGGERSTART);
                    templateTable.setUserId(logUserId);
                    spXMLCommonImpl.setLogUserId(logUserId);

                    short templateId = Short.parseShort(request.getParameter("templateId"));
                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    int formId = Integer.parseInt(request.getParameter("formId"));
                    int tableId = Integer.parseInt(request.getParameter("tableId"));
                    short noOfRows = Short.parseShort(request.getParameter("rows"));
                    short noOfCols = Short.parseShort(request.getParameter("cols"));
                    StringBuffer rootColumnStr = new StringBuffer();
                    StringBuffer rootRowStr = new StringBuffer();
                    StringBuffer rootCellDetail = new StringBuffer();
                    String colHeader = "";
                    String cellValue = "";
                    int cellCounter = 0;
                    int cellDataType = 0;
                    String comboId = "";
                    String operation = request.getParameter("subBtnCreateEdit");

                    rootColumnStr.append("<root>");
                    rootRowStr.append("<root>");
                    rootCellDetail.append("<root>");

                    for (short j = 0; j < noOfCols; j++) {
                        if (request.getParameter("Header" + j) != null && !(request.getParameter("Header" + j).equals(""))) {
                            colHeader = HandleSpecialChar.handleSpecialChar(request.getParameter("Header" + j));
                        }
                        if (request.getParameter("DataType" + (j)) != null) {
                            cellDataType = Integer.parseInt(request.getParameter("DataType" + (j)));
                        }
                        rootColumnStr.append("<tbl_TemplateColumns "
                                + "columnId=\"" + (j + 1) + "\" "
                                + "columnHeader=\"" + colHeader + "\"  "
                                + "dataType=\"" + request.getParameter("DataType" + j) + "\"  "
                                + "filledBy=\"" + request.getParameter("FillBy" + j) + "\"  "
                                + "columnType=\"" + request.getParameter("columnType" + j) + "\"  "
                                + "sortOrder=\"" + request.getParameter("SortOrder" + j) + "\"  "
                                + "tableId=\"" + tableId + "\"  "
                                + "formId=\"" + formId + "\"  "
                                + "sectionId=\"" + sectionId + "\"  "
                                + "showorhide=\"" + request.getParameter("ShowOrHide" + j) + "\" />");

                        for (short i = 0; i < noOfRows; i++) {
                            if (request.getParameter("Cell" + (i + 1) + "_" + (j + 1)) != null) {
                                cellValue = HandleSpecialChar.handleSpecialChar(request.getParameter("Cell" + (i + 1) + "_" + (j + 1)));
                            } else {
                                cellValue = "";
                            }
                            if (request.getParameter("DataType" + (i + 1) + "_" + (j + 1)) != null) {
                                cellDataType = Integer.parseInt(request.getParameter("DataType" + (i + 1) + "_" + (j + 1)));
                            }

                            if ("2".equals(request.getParameter("FillBy" + j))) {
                                if (cellDataType == 9 || cellDataType == 10) {

                                    if (cellDataType == 9) {
                                        comboId = "selComboWithCalc" + ((int) (i) + 1) + "_" + ((int) (j) + 1);
                                    } else if (cellDataType == 10) {
                                        comboId = "selComboWithOutCalc" + ((int) (i) + 1) + "_" + ((int) (j) + 1);
                                    }
                                    //comboId = "comboValue"+((int)(i)+1)+"_"+((int)(j)+1);

                                    rootCellDetail.append("<tbl_ListCellDetail "
                                            + "listBoxId=\"" + request.getParameter(comboId) + "\" "
                                            + "tenderTableId=\"" + tableId + "\"  "
                                            + "columnId=\"" + (j + 1) + "\"  "
                                            + "cellId=\"" + cellCounter + "\"  "
                                            + "location=\"" + (i + 1) + "\" />");
                                }
                            }

                            rootRowStr.append("<tbl_TemplateCells "
                                    + "tableId=\"" + tableId + "\"  "
                                    + "columnId=\"" + (j + 1) + "\" "
                                    + "cellId=\"" + cellCounter + "\" "
                                    + "rowId=\"" + request.getParameter("rowsort"+(i+1)) + "\" "
                                    + "cellDatatype=\"" + cellDataType + "\" "
                                    + "cellvalue=\"" + cellValue + "\" />");
                            cellCounter++;
                        }
                    }
                    rootColumnStr.append("</root>");
                    rootRowStr.append("</root>");
                    rootCellDetail.append("</root>");

                    templateTable.updateColNRowCnt(tableId, noOfRows, noOfCols);
                    boolean insUpdSuccess = false;
                    String strAction = "";
                    try{
                        if ("Edit".equals(operation)) {
                            strAction = "Edit Table Matrix";
                                insUpdSuccess = spXMLCommonImpl.insertTableMatrix("tbl_TemplateColumns", rootColumnStr.toString(), true, true, "tableId = " + tableId);
                                insUpdSuccess = spXMLCommonImpl.insertTableMatrix("tbl_TemplateCells", rootRowStr.toString(), false, true, "tableId = " + tableId);
                                insUpdSuccess = spXMLCommonImpl.insertTableMatrix("tbl_ListCellDetail", rootCellDetail.toString(), false, true, "tenderTableId = " + tableId);
                        } else {
                            strAction = "Create Table Matrix";
                                insUpdSuccess = spXMLCommonImpl.insertTableMatrix("tbl_TemplateColumns", rootColumnStr.toString(), true, false, "");
                                insUpdSuccess = spXMLCommonImpl.insertTableMatrix("tbl_TemplateCells", rootRowStr.toString(), false, false, "");
                                insUpdSuccess = spXMLCommonImpl.insertTableMatrix("tbl_ListCellDetail", rootCellDetail.toString(), false, false, "");
                        }
                    }catch(Exception e){
                                strAction = "Error in "+strAction+" "+e.getMessage();
                    }finally{
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), templateId, "templateId", EgpModule.STD.getName(), strAction, "");
                    }

                    rootColumnStr = null;
                    rootRowStr = null;
                    LOGGER.debug("processRequest : action : " + action + " : logUserId " + LOGGEREND);
                    if (insUpdSuccess) {
                        response.sendRedirect("admin/TableDashboard.jsp?templateId=" + templateId + "&sectionId=" + sectionId + "&formId=" + formId);
                        //response.sendRedirectFilter("TableDashboard.jsp?templateId=" + templateId + "&sectionId=" + sectionId + "&formId=" + formId);
                    } else {
                        response.sendRedirect("admin/TableDashboard.jsp?templateId=" + templateId + "&sectionId=" + sectionId + "&formId=" + formId + "&er=t");
                        //response.sendRedirect("TableDashboard.jsp?templateId=" + templateId + "&sectionId=" + sectionId + "&formId=" + formId + "&er=t");
                    }
                } else if ("formulaCreation".equals(action)) {
                    LOGGER.debug("processRequest : action : " + action + " : logUserId " + LOGGERSTART);

                    short templateId = Short.parseShort(request.getParameter("templateId"));
                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    int formId = Integer.parseInt(request.getParameter("formId"));
                    int tableId = Integer.parseInt(request.getParameter("tableId"));
                    int columnId = Integer.parseInt(request.getParameter("SaveTo"));
                    String formula = request.getParameter("hidFormula");

                    TblTemplateFormulas tblFormula = new TblTemplateFormulas();
                    tblFormula.setFormId(formId);
                    tblFormula.setTableId(tableId);
                    tblFormula.setIsGrandTotal("no");
                    tblFormula.setColumnId(columnId);
                    tblFormula.setFormula(formula);
                    templateTable.addTableFormula(tblFormula);
                    tblFormula = null;

                    boolean isBOQForm = false;
                    isBOQForm = templateForm.isBOQForm(formId);

                    // For total Formulas Start
                    boolean doInsCellMaster = false;
                    String a[] = request.getParameter("AddComboTo").split(",");
                    for (int i = 0; i < a.length; i++) {
                        if (a[i].length() > 6) {
                            tblFormula = new TblTemplateFormulas();
                            tblFormula.setFormId(formId);
                            tblFormula.setTableId(tableId);
                            tblFormula.setIsGrandTotal("yes");
                            short totFormulaCnt = templateTable.getFormulaCountOfTotal(tableId);
                            if (totFormulaCnt == 0) {
                                doInsCellMaster = true;
                            }
                            tblFormula.setColumnId(Integer.parseInt(a[i].substring(6, a[i].length())));
                            if (a[i] != null) {
                                switch (Integer.parseInt(request.getParameter(a[i]))) {
                                    case 1:
                                        tblFormula.setFormula("TOTAL(" + tblFormula.getColumnId() + ")");
                                        break;
                                    case 2:
                                        tblFormula.setFormula("AVG(" + tblFormula.getColumnId() + ")");
                                        break;
                                    default:
                                        break;
                                }
                                templateTable.addTableFormula(tblFormula);
                            }
                            tblFormula = null;
                        }
                    }

                    if (!isBOQForm && doInsCellMaster) {
                            StringBuffer rootCellsStr = new StringBuffer();
                            short noOfCols = Short.parseShort(request.getParameter("colCnt"));
                        if (templateTable.updateOnlyRowCnt(tableId)) {
                                int cellCounter = templateTable.getMaxCellCountForTable(tableId);
                                int rowNumber = templateTable.getNoOfRowsInTable(tableId, (short) 1);
                                rootCellsStr.append("<root>");
                            for (int i = 1; i <= noOfCols; i++) {
                                    cellCounter++;
                                rootCellsStr.append("<tbl_TemplateCells " + "tableId=\"" + tableId + "\" " + "columnId=\"" + (i) + "\" " + "cellId=\"" + cellCounter + "\" " + "rowId=\"" + (rowNumber + 1) + "\" " + "cellDatatype=\"3\" " + "cellvalue=\"\" " + "colId=\"" + (i) + "\" />");

                                }
                                rootCellsStr.append("</root>");
                                spXMLCommonImpl.insertTableMatrix("tbl_TemplateCells", rootCellsStr.toString(), false, false, "");
                            }
                        }
                    // For total Formulas End
                    LOGGER.debug("processRequest : action : " + action + " : logUserId " + LOGGEREND);
                    response.sendRedirect("admin/CreateFormula.jsp?templateId=" + templateId + "&sectionId=" + sectionId + "&formId=" + formId + "&tableId=" + tableId);
                    //response.sendRedirectFilter("CreateFormula.jsp?templateId=" + templateId + "&sectionId=" + sectionId + "&formId=" + formId + "&tableId=" + tableId);
                } else if ("formulaDel".equals(action)) {
                    LOGGER.debug("processRequest : action : " + action + " : logUserId " + LOGGERSTART);
                    short templateId = Short.parseShort(request.getParameter("templateId"));
                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    int formId = Integer.parseInt(request.getParameter("formId"));
                    int tableId = Integer.parseInt(request.getParameter("tableId"));
                    String columnsId = "";

                    boolean isBOQForm = false;
                    isBOQForm = templateForm.isBOQForm(formId);

                    //start
                    if (request.getParameterValues("chkDelete") != null) {
                        String ids[] = request.getParameterValues("chkDelete");
                        for (int i = 0; i < ids.length; i++) {
                            if (i == (ids.length - 1)) {
                                columnsId += ids[i];
                            } else {
                                columnsId += ids[i] + ",";
                            }
                        }

                        if (isBOQForm) {
                            templateTable.deleteTableFormula(columnsId, tableId);
                        } else {
                            short totFormulaCntForColSelected = templateTable.getFormulaCountOfTotal(tableId, columnsId);
                            short totFormulaCntInTable = templateTable.getFormulaCountOfTotal(tableId);
                            //System.out.println(" 333 " + totFormulaCntForColSelected);
                            boolean flag = false;
                            if (totFormulaCntForColSelected == 1 && totFormulaCntInTable == 1) {
                                flag = templateTable.deleteFormulaEffectsFromTemplateCells(tableId);
                                flag = templateTable.deleteFormulaEffectsFromTemplateTables(tableId);
                                if (flag) {
                                    templateTable.deleteTableFormula(columnsId, tableId);
                                }
                            } else {
                                templateTable.deleteTableFormula(columnsId, tableId);
                            }
                        }
                    }

                    String totalColumnsId = "";
                    if (request.getParameterValues("chkTotalDelete") != null) {
                        String totalids[] = request.getParameterValues("chkTotalDelete");
                        for (int ii = 0; ii < totalids.length; ii++) {
                            if (ii == (totalids.length - 1)) {
                                totalColumnsId += totalids[ii];
                            } else {
                                totalColumnsId += totalids[ii] + ",";
                            }
                        }
                        if (totalColumnsId.length() > 0) {
                            short totFormulaCnt = templateTable.getFormulaCountOfTotal(tableId);
                            boolean flag = false;
                            if (totFormulaCnt == 1) {
                                flag = templateTable.deleteFormulaEffectsFromTemplateCells(tableId);
                                flag = templateTable.deleteFormulaEffectsFromTemplateTables(tableId);
                                if (flag) {
                                    templateTable.deleteTableFormula(totalColumnsId, tableId);
                                }
                            } else {
                                templateTable.deleteTableFormula(totalColumnsId, tableId);
                            }
                        }
                        //System.out.println(" 101010 ");
                    }
                    //end
//                    if(request.getParameterValues("chkDelete")!=null){
//                        String ids[] = request.getParameterValues("chkDelete");
//                        for(int i=0;i<ids.length;i++){
//                            columnsId += ids[i] + ",";
//                        }
//                    }
//                    if(request.getParameterValues("chkTotalDelete")!=null){
//                        String ids[] = request.getParameterValues("chkTotalDelete");
//                        for(int i=0;i<ids.length;i++){
//                            columnsId += ids[i] + ",";
//                        }
//                    }
//                    columnsId += "-1";
//                    templateTable.deleteTableFormula(columnsId, tableId);
                    LOGGER.debug("processRequest : action : " + action + " : logUserId " + LOGGEREND);
                    response.sendRedirect("admin/CreateFormula.jsp?templateId=" + templateId + "&sectionId=" + sectionId + "&formId=" + formId + "&tableId=" + tableId);
                    //response.sendRedirectFilter("CreateFormula.jsp?templateId=" + templateId + "&sectionId=" + sectionId + "&formId=" + formId + "&tableId=" + tableId);
                }
                else if("saveSTDMandDocs".equals(action)){
                    if(saveSTDMandDocs(request)){
                        response.sendRedirect(request.getHeader("referer")+"&msg=savesucc");
                    }else{
                        response.sendRedirect(request.getHeader("referer")+"&msg=saverr");
            }
                }
                else if("delSTDMandDocs".equals(action)){
                    if(delSTDMandDocs(request)){
                        response.sendRedirect(request.getHeader("referer")+"&msg=delsucc");
                    }else{
                        response.sendRedirect(request.getHeader("referer")+"&msg=delerr");
                    }
                }
                else if("updateSTDMandDocs".equals(action)){
                    if(updateSTDMandDocs(request)){
                        if("1".equals(request.getParameter("pageName"))){
                            response.sendRedirect("/admin/DefineMandatoryDocs.jsp?tId="+request.getParameter("tId")+"&fId="+request.getParameter("fId")+"&sId="+request.getParameter("sId"));
                            //response.sendRedirectFilter("DefineMandatoryDocs.jsp?tId="+request.getParameter("tId")+"&fId="+request.getParameter("fId")+"&sId="+request.getParameter("sId"));
                        }
                        if("2".equals(request.getParameter("pageName"))){
                            response.sendRedirect("admin/ViewMandatoryDocs.jsp?tId="+request.getParameter("tId")+"&fId="+request.getParameter("fId")+"&sId="+request.getParameter("sId"));
                            //response.sendRedirect("ViewMandatoryDocs.jsp?tId="+request.getParameter("tId")+"&fId="+request.getParameter("fId")+"&sId="+request.getParameter("sId"));
                        }
                    }else{
                        response.sendRedirect(request.getHeader("referer")+"&msg=uperr");
                    }
                }
                /////123
                else if("saveTendMandDocs".equals(action)){
                    int res = saveTendMandDocs(request);
                    if(res == 0){
                        response.sendRedirect(request.getHeader("referer")+"&msg=savesucc");
                    }else if(res == 1){
                        response.sendRedirect(request.getHeader("referer")+"&msg=duperr");
                    }
                    else{
                        response.sendRedirect(request.getHeader("referer")+"&msg=saverr");
                    }
                }
                else if("delTendMandDocs".equals(action)){
                    if(delTendMandDocs(request)){
                        response.sendRedirect(request.getHeader("referer")+"&msg=delsucc");
                    }else{
                        response.sendRedirect(request.getHeader("referer")+"&msg=delerr");
                    }
                }
                else if("updateTendMandDocs".equals(action)){
                    int result = updateTendMandDocs(request);
                    if(result == 0){
                        if("1".equals(request.getParameter("pageName"))){
                            response.sendRedirect("officer/DefineMandatoryDocs.jsp?tId="+request.getParameter("tId")+"&fId="+request.getParameter("fId"));
                            //response.sendRedirectFilter("DefineMandatoryDocs.jsp?tId="+request.getParameter("tId")+"&fId="+request.getParameter("fId"));
                        }
                        if("2".equals(request.getParameter("pageName"))){
                            response.sendRedirect("officer/ViewMandatoryDocs.jsp?tId="+request.getParameter("tId")+"&fId="+request.getParameter("fId"));
                            //response.sendRedirectFilter("ViewMandatoryDocs.jsp?tId="+request.getParameter("tId")+"&fId="+request.getParameter("fId"));
                        }
                    }else if(result == 1){
                        response.sendRedirect(request.getHeader("referer")+"&msg=duperr");
                    }
                    else
                    {
                        response.sendRedirect(request.getHeader("referer")+"&msg=uperr");
                    }
                }
            }
        } catch (Exception ex) {
            LOGGER.error("processRequest : " + ex.toString());
        } finally {
            templateForm.setAuditTrail(null);
            templateTable.setAuditTrail(null);
            out.close();
        }
        LOGGER.debug("processRequest : " + logUserId + "ends");
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private boolean saveSTDMandDocs(HttpServletRequest request){
        String[] docName=request.getParameterValues("docName");
        List<TblTemplateMandatoryDoc> list = new ArrayList<TblTemplateMandatoryDoc>();
        int tId = Integer.parseInt(request.getParameter("tId"));
        int sId = Integer.parseInt(request.getParameter("sId"));
        int fId = Integer.parseInt(request.getParameter("fId"));
        for (int i = 0; i < docName.length; i++) {
            list.add(new TblTemplateMandatoryDoc(0, tId, sId, fId, docName[i], Integer.parseInt(request.getSession().getAttribute("userId").toString()), new Date()));
        }
            return templateForm.saveSTDMandDocs(list);

    }

    private boolean updateSTDMandDocs(HttpServletRequest request){
        return templateForm.updateSTDMandDoc(request.getParameter("mId") , request.getParameter("docName"));
    }

    private boolean delSTDMandDocs(HttpServletRequest request){
        return  templateForm.delSTDMandDoc(request.getParameter("mId"));
    }
    private int saveTendMandDocs(HttpServletRequest request){
        AuditTrail auditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer"));
        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");

        String[] docName=request.getParameterValues("docName");
        List<TblTenderMandatoryDoc> list = new ArrayList<TblTenderMandatoryDoc>();
        int tId = Integer.parseInt(request.getParameter("tId"));
        int fId = Integer.parseInt(request.getParameter("fId"));
        List<Object[]> mandDocsForDuplicate = templateForm.getTendMandDocs(request.getParameter("tId"), request.getParameter("fId"));
        for (int i = 0; i < docName.length; i++) 
        {
            for(int j = i+1; j < docName.length; j++)
            {
                if(docName[i].equalsIgnoreCase(docName[j]))
                {
                    return 1;
                }
            }
        }
        for(Object[] data : mandDocsForDuplicate)
        {
            for (int i = 0; i < docName.length; i++) 
            {
                if(docName[i].equalsIgnoreCase(data[1].toString()))
                {
                    return 1;
                }
                
            }
            
        }
        for (int i = 0; i < docName.length; i++) {
            list.add(new TblTenderMandatoryDoc(0, tId, fId,0, docName[i], Integer.parseInt(request.getSession().getAttribute("userId").toString()), new Date()));
            makeAuditTrailService.generateAudit(auditTrail,tId , "tenderId", EgpModule.Tender_Document.getName(), "Add Document", docName[i]);
        }
        boolean isOk = templateForm.saveTendMandDocs(list);
        if(isOk)
        {   
            return 0;
        }
        else return 2;
    }

    private int updateTendMandDocs(HttpServletRequest request){
        AuditTrail auditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer"));
        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
        int tenderId = 0;
        if(request.getParameter("tId")!=null && !"".equals(request.getParameter("tId"))){
            tenderId = Integer.parseInt(request.getParameter("tId"));
        }
        List<Object[]> mandDocsForDuplicate = templateForm.getTendMandDocs(request.getParameter("tId"), request.getParameter("fId"));
        for(Object[] data : mandDocsForDuplicate)
        {
           if(request.getParameter("docName").equalsIgnoreCase(data[1].toString()) && !request.getParameter("mId").equals(data[0].toString()))
            {
                return 1;
            }
        }
        if(templateForm.updateTendMandDoc(request.getParameter("mId") , request.getParameter("docName"))){
            
            makeAuditTrailService.generateAudit(auditTrail,tenderId , "tenderId", EgpModule.Tender_Document.getName(), "Edit Document", request.getParameter("docName"));
            return 0;
        }else{
            makeAuditTrailService.generateAudit(auditTrail,tenderId , "tenderId", EgpModule.Tender_Document.getName(), "Error in Edit Document", request.getParameter("docName"));
            return 2;
        }
    }

    private boolean delTendMandDocs(HttpServletRequest request)
    {
        AuditTrail auditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer"));
        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");

         TemplateSectionFormImpl templateForm = (TemplateSectionFormImpl) AppContext.getSpringBean("AddFormService");
        List<Object[]> mandDocList = templateForm.getTendMandDocs(request.getParameter("mId"));

        int tenderId = 0;
        if(request.getParameter("tId")!=null && !"".equals(request.getParameter("tId"))){
            tenderId = Integer.parseInt(request.getParameter("tId"));
        }

        if(templateForm.delTendMandDoc(request.getParameter("mId"))){
            makeAuditTrailService.generateAudit(auditTrail,tenderId , "tenderId", EgpModule.Tender_Document.getName(), "Delete Document", mandDocList.get(0)[1].toString());
            return true;
        }else{
            makeAuditTrailService.generateAudit(auditTrail,tenderId , "tenderId", EgpModule.Tender_Document.getName(), "Error in Delete Document", mandDocList.get(0)[1].toString());
            return false;
        }
    }
}
