/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.service.serviceimpl.PublicForumPostService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class getAdminPost extends HttpServlet {

    private String logUserId = "0";
    Logger logger = Logger.getLogger(getAllPostServlet.class);
    PublicForumPostService publicForumPostService = (PublicForumPostService) AppContext.getSpringBean("PublicForumPostService");

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        logger.debug("processRequest : " + logUserId + "starts");
        try {
            String todo = request.getParameter("todo");
            if ("deletepost".equals(todo)) {
                String ppfid = request.getParameter("ppfid");
                if(publicForumPostService.deletePPF(ppfid)){
                    response.sendRedirect(request.getHeader("referer")+"?msg=delsucc");
                }else{
                    response.sendRedirect(request.getHeader("referer")+"?msg=delfail");
                }
            } else {

                int counter = 0;
                String styleClass = "";
                int totalRecord = 0;
                List<SPCommonSearchData> colResult = null;
                String viewType = request.getParameter("viewType");
                colResult = searchAllPosts(request);// define the method to get list
                int totalpage = 0;
                if (colResult == null || (colResult != null && colResult.isEmpty())) {
//displaying the data

                    out.print("<tr>");
                    out.print("<td colspan=\"6\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");

                } else {

                    out.print("<tr> <th width=\"5%\" class=\"t-align-center\">S.No</th>");
                    out.print("<th width=\"20%\" class=\"t-align-center\">Topics</th>");
                    if(viewType!=null && !"".equalsIgnoreCase(viewType)){
                    out.print("<th width=\"14%\" class=\"t-align-center\">"+(("MYREPLIED".equalsIgnoreCase(viewType)) ?"Replies":"&nbsp;")+"</th>");
                    }
                    out.print("<th width=\"18%\" class=\"t-align-center\">Username</th>");
                    out.print("<th width=\"18%\" class=\"t-align-center\">Date and Time</th>");
                    out.print("<th width=\"10%\" class=\"t-align-center\">Verification Status</th>");
                    out.print("<th width=\"10%\" class=\"t-align-center\">Action</th> </tr>");

                    for (SPCommonSearchData data : colResult) {
                        if (counter % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }

                        out.print("<tr class='" + styleClass + "'>");
                        out.print("<td class=\"t-align-center\">" + data.getFieldName1() + "</td>");
                        out.print("<td class=\"t-align-center\">" + "<a href=viewTopic.jsp?ppfid=" + data.getFieldName2() + "&ViewType=" + request.getParameter("viewType") + ">" + data.getFieldName3() + "</a> </td>");
                        if ("MYREPLIED".equalsIgnoreCase(viewType)) {
                            if (data.getFieldName11().length() > 50) {
                                String display = data.getFieldName11().substring(0, 50);
                                out.print("<td class=\"t-align-center\">" + display + "...</td>");
                            } else {
                                out.print("<td class=\"t-align-center\">" + data.getFieldName11() + "</td>");
                            }
                        } /*else{
                            out.print("&nbsp;");
                        }*/
                        out.print("<td class=\"t-align-center\">" + data.getFieldName4() + "</td>");
                        out.print("<td class=\"t-align-center\">" + data.getFieldName6() + "</td>");
                        out.print("<td class=\"t-align-center\">" + data.getFieldName9() + "</td>");
                        if (data.getFieldName10().toString().equals("0")) {
                            out.print("<td class=\"t-align-center\">" + "<a href=admin/VerifyTopicContent.jsp?ppfid=" + data.getFieldName2() + ">Verify" + "</a>" + " | <a href=\"" + request.getContextPath() + "/getAdminPost?ppfid=" + data.getFieldName2() + "&todo=deletepost\" onclick=\"return confirm('Do you want to delete the topic?');\">Delete" + "</a></td>");
                        } else {
                            out.print("<td class=\"t-align-center\">" + "<a href=admin/VerifyReplyContent.jsp?ppfid=" + data.getFieldName2() + ">Verify" + "</a>" + " | <a href=\"" + request.getContextPath() + "/getAdminPost?ppfid=" + data.getFieldName2() + "&todo=deletepost\" onclick=\"return confirm('Do you want to delete the replies?');\">Delete" + "</a></td>");
                        }
                        out.print("</tr>");
                        totalpage = Integer.parseInt(data.getFieldName7());
                        totalRecord = Integer.parseInt(data.getFieldName8());
                        counter++;
                    }
                }
                if (totalpage == 0) {
                    totalpage = 1;
                } else {
                    if (totalRecord % 10 != 0) {
                        totalpage = totalpage + 1;
                    }
                }
                out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalpage + "\">");
            }
        } catch (Exception exp) {
            logger.error("processRequest : " + exp);
        }
        logger.debug("processRequest : " + logUserId + "ends");
    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private List<SPCommonSearchData> searchAllPosts(HttpServletRequest request) {
        List<SPCommonSearchData> searchAllPosts = null;
        logger.debug("searchAllPosts : " + logUserId + "starts");
//define the currently login user
        HttpSession session = request.getSession();
        Object objUsrTypeId = session.getAttribute("userTypeId");
        Object objUName = session.getAttribute("userName");
        String userId = "";
//getting the whole data from jsp
        String uid = session.getAttribute("userId").toString();
        if (objUName == null) {
            userId = "";

        } else {
            userId = objUsrTypeId.toString();
        }
        if (uid == null) {
            logUserId = "0";
        } else {
            logUserId = uid;

        }
        String action = request.getParameter("action");
        String keyword = request.getParameter("keyword");
        String pageNo = request.getParameter("pageNo");
        String recordOffset = request.getParameter("size");
        String dop = request.getParameter("textfield5");
        if (dop == null) {
            dop = "";
        }
        String postedby = request.getParameter("select2");
        String viewType = request.getParameter("viewType");
//calling the procedure
        if (!"".equals(keyword) || !"".equals(dop) || !"".equals(postedby)) {
            searchAllPosts = publicForumPostService.getpost("search_Procurementforum", "", keyword, postedby, dop, logUserId, pageNo, recordOffset, action, "");
        } else if ("8".equals(userId)) {
            searchAllPosts = publicForumPostService.getpost("search_Procurementforum", viewType, "", "", "", "", pageNo, recordOffset, action, "");
        } else {
            searchAllPosts = publicForumPostService.getpost("search_Procurementforum", "", "", "", "", logUserId, pageNo, recordOffset, action, "");
        }
        logger.debug("searchAllPosts : " + logUserId + " ends ");
        return searchAllPosts;
    }
}
