/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SearchNOAData;
import com.cptu.egp.eps.service.serviceimpl.SearchNOAService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author rishita
 */
public class AdvSearchNOAServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String keyword = request.getParameter("keyword");
            String styleClass = "";

            int officeId = 0;
            int contractId = 0;
            if (request.getParameter("contractId") != null && !request.getParameter("contractId").trim().equalsIgnoreCase("")) {
                contractId = Integer.parseInt(request.getParameter("contractId"));
            }
            int departmentId = 0;
            if (!request.getParameter("departmentId").trim().equalsIgnoreCase("")) {
                departmentId = Integer.parseInt(request.getParameter("departmentId"));
            }
            if (!request.getParameter("officeId").trim().equalsIgnoreCase("")) {
                officeId = Integer.parseInt(request.getParameter("officeId"));
            }
            String stateName = "";
            if (!request.getParameter("stateName").equalsIgnoreCase("null") && !request.getParameter("stateName").trim().equalsIgnoreCase("")) {
                stateName = request.getParameter("stateName");
            }
            int tenderId = 0;
            if (!request.getParameter("tenderId").trim().equalsIgnoreCase("")) {
                tenderId = Integer.parseInt(request.getParameter("tenderId"));
            }
            String advDt = "";
            if(request.getParameter("advDt") != null){
                advDt = request.getParameter("advDt");
            }
            //Code By Proshanto Kumar Saha
            String advDtTo = "";
            if(request.getParameter("advDtTo") != null){
                advDtTo = request.getParameter("advDtTo");
            }
            String noaDt = "";
            if(request.getParameter("noaDt") != null){
                noaDt = request.getParameter("noaDt");
            }
            //Code By Proshanto Kumar Saha
            String noaDtTo = "";
            if(request.getParameter("noaDtTo") != null){
                noaDtTo = request.getParameter("noaDtTo");
            }
            String procurementMethod = "";
            if(request.getParameter("procurementMethod") != null){
                procurementMethod = request.getParameter("procurementMethod");
            }
            String contractAwardTo = "";
            if(request.getParameter("contractAwardTo") != null){
                contractAwardTo = request.getParameter("contractAwardTo");
            }
            String contractNo = request.getParameter("contractNo");
            String contractDtFrom = "";
            String contractDtTo = "";
            if(request.getParameter("contractDtFrom") != null){
                contractDtFrom = request.getParameter("contractDtFrom");
            }
            if(request.getParameter("contractDtTo") != null){
                contractDtTo = request.getParameter("contractDtTo");
            }
            String tenderRefNo = request.getParameter("tenderRefNo");
            String contractAmount = request.getParameter("contractAmount");
            String cpvCode = "";
            if(request.getParameter("cpvCode") != null){
                cpvCode = request.getParameter("cpvCode");
            }

            String strPageNo = request.getParameter("pageNo");
            int pageNo = Integer.parseInt(strPageNo);
            String strOffset = request.getParameter("size");
            int recordOffset = Integer.parseInt(strOffset);

            SearchNOAService noaService = (SearchNOAService) AppContext.getSpringBean("SearchNOAService");
            //Added 2 parameter advDtTo and noaDtTo by Proshanto Kumar Saha
            List<SearchNOAData> noaList = noaService.returndata(keyword, contractDtFrom, contractDtTo, departmentId, contractId, contractNo, tenderId, tenderRefNo, cpvCode, contractAmount, officeId, pageNo, recordOffset,stateName,advDt,advDtTo,procurementMethod,noaDt,noaDtTo,contractAwardTo);
            if (!noaList.isEmpty()) {
                for (int i = 0; i < noaList.size(); i++) {
                    if (i % 2 == 0) {
                        styleClass = "bgColor-white";
                    } else {
                        styleClass = "bgColor-Green";
                    }

                    SearchNOAData searchNOAData = noaList.get(i);
                    out.print("<tr class='" + styleClass + "'>");
                    out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * 10 + (i + 1)) + "</td>");
                    out.print("<td class=\"t-align-center\">" + searchNOAData.getMDADetails() + "</td>");
                    String linkTenderId[] = searchNOAData.getKeyword().split(",");
                    String[] RefTitleAdv = searchNOAData.getTenderRefNo().split(",");
                    String thisContractNo = searchNOAData.getContractNo();

                    //out.print("<td class=\"t-align-center\"><a href=\"javascript:void(0);\" onclick=\"javascript:window.open('" + request.getContextPath() + "/resources/common/ViewContracts.jsp?pkgLotId=" + linkTenderId[linkTenderId.length-2] + "&tenderid=" + linkTenderId[linkTenderId.length-3] + "&userId=" + linkTenderId[linkTenderId.length-1] + "', '', 'resizable=yes,scrollbars=1','');\">" + RefTitleAdv[0] + "</a><br/>" + linkTenderId[0] + "<br/>" + RefTitleAdv[1] + "</td>");

                   out.print("<td class=\"t-align-center\"><a href=\"javascript:void(0);\" onclick=\"javascript:window.open('" + request.getContextPath() + "/resources/common/ViewContracts.jsp?contractNo="+thisContractNo+"&pkgLotId=" + linkTenderId[linkTenderId.length-2] + "&tenderid=" + linkTenderId[linkTenderId.length-3] + "&userId=" + linkTenderId[linkTenderId.length-1] + "', '', 'resizable=yes,scrollbars=1','');\"><p>" + linkTenderId[linkTenderId.length-3]+", "+RefTitleAdv[0] + "</p></a><br/>" + " <span class=\"more\"> "+ linkTenderId[0] +" </span>" + RefTitleAdv[1] + "</td>");
                   
                  // out.print("<td class=\"t-align-center\"><a onclick=\"javascript:window.open('" + request.getContextPath() + "/resources/common/ViewContracts.jsp?pkgLotId=" + linkTenderId[linkTenderId.length-2] + "&tenderid=" + linkTenderId[linkTenderId.length-3] + "&userId=" + linkTenderId[linkTenderId.length-1] + "', '', 'resizable=yes,scrollbars=1','');\" href='javascript:void(0);'><span id='eContract_"+ i +"'>" + RefTitleAdv[0]+ "</span></a>" + linkTenderId[0] +" <br />" + RefTitleAdv[1] + "</td>");


                    // out.print("<td class=\"t-align-center\"><a href=\"javascript:void(0);\" onclick=\"javascript:window.open('" + request.getContextPath() + "/resources/common/ViewContracts.jsp?pkgLotId=" + linkTenderId[2] + "&tenderid=" + linkTenderId[1] + "&userId=" + linkTenderId[3] + "', '', 'resizable=yes,scrollbars=1','');\">" + RefTitleAdv[0] + "</a><br/>" + linkTenderId[0] + "<br/>" + RefTitleAdv[1] + "</td>");
                    out.print("<td class=\"t-align-center\">" + searchNOAData.getProcuringEntity() + "</td>");
//                    out.print("<td class=\"t-align-left\">" + searchNOAData.getDistrict() + "</td>");
                    out.print("<td class=\"t-align-center\">" + searchNOAData.getNOADate() + "</td>");
                    out.print("<td class=\"t-align-center\">" + searchNOAData.getContractAwardedTo() + "</td>");
                    out.print("<td class=\"t-align-center\">" + searchNOAData.getContractAmt() + "</td>");
                    out.print("</tr>");
                }
            } else {
                out.print("<tr>");
                out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"8\" style=\"color: red;font-weight: bold\">No Record Found!</td>");
                out.print("</tr>");
            }
            int totalPages = 1;

            if (noaList.size() > 0) {
                int rowCount = Integer.parseInt(noaList.get(0).getTotalRowCount());
                totalPages = (int) Math.ceil(Math.ceil(rowCount) / recordOffset);
                if (totalPages == 0) {
                    totalPages = 1;
                }
            }
            out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
