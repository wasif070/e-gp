/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.model.table.TblContractSignDocs;
import com.cptu.egp.eps.model.table.TblNoaDocuments;
import com.cptu.egp.eps.model.table.TblTenderDocStatistics;
import com.cptu.egp.eps.model.table.TblTenderForms;
import com.cptu.egp.eps.model.table.TblTenderLotSecurity;
import com.cptu.egp.eps.model.table.TblTenderSection;
import com.cptu.egp.eps.model.table.TblTenderSectionDocs;
import com.cptu.egp.eps.service.serviceimpl.TenderDocumentService;
import com.cptu.egp.eps.service.serviceimpl.TenderFormService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.AppExceptionHandler;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import com.cptu.egp.eps.web.utility.GeneratePdfConstant;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.util.FileSystemUtils;

/**
 *
 * @author Yagnesh
 */
public class TenderDocumentSrBean {

    private String logUserId = "0";
    final Logger logger = Logger.getLogger(TenderDocumentSrBean.class);
    TenderDocumentService tDocService = (TenderDocumentService) AppContext.getSpringBean("TenderDocumentService");
    TenderFormService tenderFormService = (TenderFormService) AppContext.getSpringBean("TenderAddFormService");
    TenderFormService tfs = (TenderFormService) AppContext.getSpringBean("TenderAddFormService");

    /**
     * For logging purpose
     *
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        tDocService.setLogUserId(logUserId);
        tenderFormService.setLogUserId(logUserId);
        tfs.setLogUserId(logUserId);
        this.logUserId = logUserId;

    }

    /**
     * it gives the template section name
     *
     * @param tenderStdId from Tbl_TenderStd
     * @return List of TblTenderSection
     */
    public List<TblTenderSection> getTenderSections(int tenderStdId) {
        logger.debug("getTenderSections : " + logUserId + "starts");
        List<TblTenderSection> getTenderSections = null;
        try {
            getTenderSections = tDocService.getTemplateSection(tenderStdId);
        } catch (Exception e) {
            logger.error("getTenderSections : " + e);
        }

        logger.debug("getTenderSections : " + logUserId + "ends");

        return getTenderSections;
    }

    /**
     * it gives the tender form
     *
     * @param sectionId
     * @return List of TblTenderForms
     */
    public List<TblTenderForms> getTenderForm(int sectionId) {
        logger.debug("getTenderForm : " + logUserId + "starts");
        List<TblTenderForms> getTenderFormLot = null;
        try {
            getTenderFormLot = tenderFormService.getForms(sectionId);
        } catch (Exception e) {
            logger.error("getTenderForm : " + e);
        }

        logger.debug("getTenderForm : " + logUserId + "ends");

        return getTenderFormLot;
    }

    /**
     * it gives the tender form package
     *
     * @param sectionId
     * @return List of TblTenderForms
     */
    public List<TblTenderForms> getTenderFormPackage(int sectionId) {
        logger.debug("getTenderFormPackage : " + logUserId + "starts");
        List<TblTenderForms> getTenderFormLot = null;
        try {
            getTenderFormLot = tenderFormService.getFormsPackage(sectionId);
        } catch (Exception e) {
            logger.error("getTenderFormPackage : " + e);
        }

        logger.debug("getTenderFormPackage : " + logUserId + "ends");

        return getTenderFormLot;
    }

    /**
     * it gives the tender form lot
     *
     * @param sectionId
     * @param appPkgLotId
     * @return List of TblTenderForms
     */
    public List<TblTenderForms> getTenderFormLot(int sectionId, int appPkgLotId) {
        logger.debug("getTenderFormLot : " + logUserId + "starts");
        List<TblTenderForms> getTenderFormLot = null;
        try {
            getTenderFormLot = tenderFormService.getFormsLot(sectionId, appPkgLotId);
        } catch (Exception e) {
            logger.error("getTenderFormLot : " + e);
        }

        logger.debug("getTenderFormLot : " + logUserId + "ends");

        return getTenderFormLot;
    }

    /**
     * it gives the lot list
     *
     * @param tenderId
     * @return List of TblTenderLotSecurity
     */
    public List<TblTenderLotSecurity> getLotList(int tenderId) {
        logger.debug("getLotList : " + logUserId + "starts");
        List<TblTenderLotSecurity> getLotList = null;
        try {
            getLotList = tenderFormService.getLotDetails(tenderId);
        } catch (Exception e) {
            logger.error("getLotList : " + e);
        }

        logger.debug("getLotList : " + logUserId + "ends");

        return getLotList;
    }

    /**
     * it gives the tender STD Id
     *
     * @param tenderId
     * @return int TenderstdId
     */
    public int getTenderSTDId(int tenderId) {
        logger.debug("getCompanyDetails : " + logUserId + "starts");
        int getTenderSTDId = 0;
        try {
            getTenderSTDId = tDocService.getTenderSTDId(tenderId);
        } catch (Exception e) {
            logger.error("getCompanyDetails : " + e);
        }

        logger.debug("getCompanyDetails : " + logUserId + "ends");

        return getTenderSTDId;
    }

    /**
     * it gives the tenderSTDId according to lot id
     *
     * @param tenderId
     * @param lotId
     * @return int tenderStdId
     */
    public int getTenderSTDId(int tenderId, int lotId) {
        logger.debug("getTenderSTDId : " + logUserId + "starts");
        int getTenderSTDId = 0;
        try {
            getTenderSTDId = tDocService.getTenderSTDId(tenderId, lotId);
        } catch (Exception e) {
            logger.error("getTenderSTDId : " + e);
        }

        logger.debug("getTenderSTDId : " + logUserId + "ends");

        return getTenderSTDId;
    }
//    public List<TblTemplateSectionDocs> getTemplateSectionDocs(short templateId, short sectionId) {
//        TemplateDocumentsService templateDocument = (TemplateDocumentsService) AppContext.getSpringBean("TemplateDocumentService");
//        return templateDocument.getAllDocForSection(templateId, sectionId);
//    }

    /**
     * it gives the lot details
     *
     * @param tenderId
     * @return List of TblTenderLotSecurity
     */
    public List<TblTenderLotSecurity> getLotDetails(int tenderId) {
        logger.debug("getLotDetails : " + logUserId + "starts");
        List<TblTenderLotSecurity> tblTenderLotSecuritys = null;
        try {
            tblTenderLotSecuritys = tDocService.getLotDetails(tenderId);
        } catch (Exception e) {
            logger.error("getLotDetails : " + e);
        }
        logger.debug("getCompanyDetails : " + logUserId + "ends");
        return tblTenderLotSecuritys;
    }

    /**
     * it gives the lot details by lot id
     *
     * @param tenderId
     * @param appPkgLotId
     * @return List of TblTenderLotSecurity
     */
    public List<TblTenderLotSecurity> getLotDetailsByLotId(int tenderId, int appPkgLotId) {
        logger.debug("getLotDetailsByLotId : " + logUserId + "starts");
        List<TblTenderLotSecurity> tblTenderLotSecuritys = null;
        try {
            tblTenderLotSecuritys = tDocService.getLotDetailsByLotId(tenderId, appPkgLotId);
        } catch (Exception e) {
            logger.error("getLotDetailsByLotId : " + e);
        }
        logger.debug("getLotDetailsByLotId : " + logUserId + "ends");
        return tblTenderLotSecuritys;
    }

    /**
     * it find the template id according to tender id
     *
     * @param tenderId
     * @return String stdTemplateId
     */
    public String findStdTemplateId(String tenderId) {
        logger.debug("findStdTemplateId : " + logUserId + "starts");
        String findStdTemplateId = "";
        try {
            findStdTemplateId = tDocService.findStdTemplateId(tenderId);
        } catch (Exception e) {
            logger.error("findStdTemplateId : " + e);
        }

        logger.debug("findStdTemplateId : " + logUserId + "ends");
        return findStdTemplateId;
    }

    /**
     * it dumps std
     *
     * @param tenderid
     * @param templateid
     * @param userid
     * @return Object of commonMsgChk
     */
    public CommonMsgChk dumpSTD(String tenderid, String templateid, String userid) {
        logger.debug("dumpSTD : " + logUserId + "starts");
        logger.debug("dumpSTD : " + logUserId + "ends");
        return tDocService.dumpSTD(tenderid, templateid, userid);

    }

    /**
     *
     * @param tenderId
     * @param stdTemplateId
     * @return boolean true or false
     */
    public boolean changeDumpSTD(int tenderId, int stdTemplateId) {
        boolean flag = false;
        logger.debug("changeDumpSTD : " + logUserId + "starts");
        try {
            if (tDocService.deleteStd(tenderId, stdTemplateId)) {
                flag = true;
            } else {
                flag = false;
            }
        } catch (Exception e) {
            logger.error("changeDumpSTD : " + e);
        }

        logger.debug("changeDumpSTD : " + logUserId + "ends");
        return flag;

    }

    /**
     * Check for dump std for particular tenderId
     *
     * @param tenderid
     * @return Template Name
     * @throws Exception
     */
    public String checkForDump(String tenderid) throws Exception {
        String checkForDump = "";
        logger.debug("checkForDump : " + logUserId + "starts");
        try {
            checkForDump = tDocService.checkForDump(tenderid);
        } catch (Exception e) {
            logger.error("checkForDump : " + e);
        }

        logger.debug("checkForDump : " + logUserId + "ends");
        return checkForDump;
    }

    /**
     * it adds tender document statistics
     *
     * @param Object of tblTenderDocStatistics
     */
    public void addTenderDocStatistics(TblTenderDocStatistics tblTenderDocStatistics) {
        logger.debug("addTenderDocStatistics : " + logUserId + "starts");
        try {
            tDocService.addTenderDocStatistics(tblTenderDocStatistics);
        } catch (Exception e) {
            logger.error("addTenderDocStatistics : " + e);
        }

        logger.debug("addTenderDocStatistics : " + logUserId + "ends");

    }

    /**
     * it gives the package number
     *
     * @param tenderId
     * @return {packageNo}
     */
    public Object getPkgNo(String tenderId) {
        logger.debug("getPkgNo : " + logUserId + "starts");
        Object getPkgNo = null;
        try {
            getPkgNo = tDocService.getPkgNo(tenderId);
        } catch (Exception e) {
            logger.error("getPkgNo : " + e);
        }

        logger.debug("getPkgNo : " + logUserId + "ends");
        return getPkgNo;
    }

    /*public Object getWorkCategory(String tenderId) {
        logger.debug("getPkgNo : " + logUserId + "starts");
        Object workCategory = null;
        try {
            workCategory = tDocService.getWorkCategory(tenderId);
        } catch (Exception e) {
            logger.error("getPkgNo : " + e);
        }

        logger.debug("getPkgNo : " + logUserId + "ends");
        return workCategory;
    }*/

    /**
     * it gives the Given Estimated cost
     *
     * @param tenderId
     * @return {estCost}
     */
    public List<Object[]> getGivenEst(String tenderId) {
        logger.debug("getGivenEst : " + logUserId + "starts");
        List<Object[]> estCost = new ArrayList<Object[]>();
        try {
            estCost = tDocService.getGivenEst(tenderId);
        } catch (Exception e) {
            logger.error("getPkgNo : " + e);
        }

        logger.debug("getGivenEst : " + logUserId + "ends");
        return estCost;
    }

    /**
     * it gives the bidder category
     *
     * @param packageNo
     * @return {bidderCategory}
     */
    public Object[] getBidderCategory(String packageNo) {
        logger.debug("getBidderCategory : " + logUserId + "starts");
        Object[] bidderandworkCategory = null;
        try {
            bidderandworkCategory = tDocService.getBidderCategory(packageNo);
        } catch (Exception e) {
            logger.error("getPkgNo : " + e);
        }

        logger.debug("getPkgNo : " + logUserId + "ends");
        return bidderandworkCategory;
    }

    /**
     * it gives the package number
     *
     * @param tenderId
     * @return {packageNo}
     */
    public Object getDptId(String tenderId) {
        logger.debug("getDptId : " + logUserId + "starts");
        Object dptId = null;
        try {
            dptId = tDocService.getDptId(tenderId);
        } catch (Exception e) {
            logger.error("getDptId : " + e);
        }

        logger.debug("getDptId : " + logUserId + "ends");
        return dptId;
    }

    /**
     * it gives the package number
     *
     * @param dptId
     * @return {packageNo}
     */
    public Object getDptType(String dptId) {
        logger.debug("getPkgNo : " + logUserId + "starts");
        Object dptType = null;
        try {
            dptType = tDocService.getDptType(dptId);
        } catch (Exception e) {
            logger.error("getPkgNo : " + e);
        }

        logger.debug("getPkgNo : " + logUserId + "ends");
        return dptType;
    }

    /**
     * List of Boq Form
     *
     * @param pkgId
     * @return List of {tenderFormId,formName}
     */
    public List<Object[]> getAllBoQForms(int pkgId, int tenderId) {
        logger.debug("getAllBoQForms : " + logUserId + "starts");
        List<Object[]> getTenderFormLot = null;
        try {
            getTenderFormLot = tenderFormService.getAllBoQForms(pkgId, tenderId);
        } catch (Exception e) {
            logger.error("getAllBoQForms : " + e);
        }

        logger.debug("getAllBoQForms : " + logUserId + "ends");

        return getTenderFormLot;
    }
    public List<Object[]> getAllBoQForms(int pkgId, int tenderId,int userId) {
        logger.debug("getAllBoQForms : " + logUserId + "starts");
        List<Object[]> getTenderFormLot = null;
        try {
            getTenderFormLot = tenderFormService.getAllBoQForms(pkgId, tenderId, userId);
        } catch (Exception e) {
            logger.error("getAllBoQForms : " + e);
        }

        logger.debug("getAllBoQForms : " + logUserId + "ends");

        return getTenderFormLot;
    }

    public List<Object[]> getAllBoQFormsForRO(int pkgId) {
        logger.debug("getAllBoQForms : " + logUserId + "starts");
        List<Object[]> getTenderFormLot = null;
        try {
            getTenderFormLot = tenderFormService.getAllBoQFormsForRO(pkgId);
        } catch (Exception e) {
            logger.error("getAllBoQForms : " + e);
        }

        logger.debug("getAllBoQForms : " + logUserId + "ends");

        return getTenderFormLot;
    }

    /**
     * it gives the forms
     *
     * @param tenderSectionId
     * @param isPriceBid
     * @param pkgLotId
     * @return List of Object [0]tenderFormId,[1] formName,[2]formStatus
     */
    public List<Object[]> getForms(int tenderSectionId, String isPriceBid, int pkgLotId) {
        logger.debug("getForms : " + logUserId + "starts");
        List<Object[]> getForms = null;
        try {
            getForms = tfs.getForms(tenderSectionId, isPriceBid, pkgLotId);
        } catch (Exception e) {
            logger.error("getForms : " + e);
        }

        logger.debug("getForms : " + logUserId + "ends");
        return getForms;
    }

    /**
     * Making a Zip file for the tender documents
     *
     * @param templateId tender Template
     * @param tenderId from tbl_TenderMaster
     * @return true or false for success or false
     */
    public boolean copySTDDocs4ZIP(String templateId, String tenderId) {
        logger.debug("copySTDDocs4ZIP : " + logUserId + "starts");
        boolean flag = false;
        File delFile = new File(FilePathUtility.getFilePath().get("TenderSecUploadServlet") + "\\" + tenderId);
        if (delFile.exists()) {
            FileSystemUtils.deleteRecursively(delFile);
        }
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        String procMethod = commonService.getProcMethod(tenderId).toString();
        String appId = "0";
        if ("LTM".equalsIgnoreCase(procMethod)) {
            appId = commonService.getAppIdByTenderId(tenderId);
        }
        String docAvlMethod = commonService.getDocAvlMethod(tenderId).toString();
        if ("Lot".equals(docAvlMethod)) {
            List<Object> lots = commonService.getLotNo(tenderId);
            for (Object lot : lots) {
                String folderName = "Lot_" + lot.toString();
                flag = dumpFiles(folderName, templateId, tenderId, appId);
            }
        } else {
            String folderName = "Package";
            flag = dumpFiles(folderName, templateId, tenderId, appId);
        }
        logger.debug("copySTDDocs4ZIP : " + logUserId + "ends");
        return flag;
    }

    /**
     * Method Dumps Files from STD and Tender into a single Folder
     *
     * @param folderName
     * @param templateId tender Template
     * @param tenderId from tbl_TenderMaster
     * @param appId
     * @return true or false for success or false
     */
    public boolean dumpFiles(String folderName, String templateId, String tenderId, String appId) {
        logger.debug("dumpFiles : " + logUserId + "starts");
        boolean flag = false;
        int cnt = 1;
        try {
            List<Object[]> tSections = getTemplateSections(templateId);
            for (Object[] tts : tSections) {
                File file1 = new File(FilePathUtility.getFilePath().get("TemplateFileUploadServlet") + "\\" + tts[0].toString());
                File file3 = new File(FilePathUtility.getFilePath().get("STDPDF") + "\\" + templateId + "_" + tts[0].toString());
                if (file1.exists() || file3.exists()) {
                    File file2 = new File(FilePathUtility.getFilePath().get("TenderSecUploadServlet") + "\\" + tenderId + "\\" + folderName + "\\Section" + cnt + "_" + tts[1].toString());
                    if (!file2.exists()) {
                        file2.mkdirs();
                    }
                    if (file1.exists()) {
                        FileSystemUtils.copyRecursively(file1, file2);
                    }
                    if (file3.exists()) {
                        FileSystemUtils.copyRecursively(file3, file2);
                    }
                }
                cnt++;
            }
            if (!"0".equalsIgnoreCase(appId)) {
                File file1 = new File(FilePathUtility.getFilePath().get("AppDownloadDocs") + "\\" + appId + "\\");
                if (file1.exists()) {
                    File file2 = new File(FilePathUtility.getFilePath().get("TenderSecUploadServlet") + "\\" + tenderId + "\\" + folderName + "\\EstimatedCost");
                    if (!file2.exists()) {
                        file2.mkdirs();
                    }
                    if (file1.exists()) {
                        FileSystemUtils.copyRecursively(file1, file2);
                    }
                }
            }
            flag = true;
        } catch (Exception e) {
            logger.error("dumpFiles : " + e);
        }
        logger.debug("dumpFiles : " + logUserId + "ends");
        return flag;
    }

    /**
     * List of {sectionId,sectionName}
     *
     * @param templateId tender Template
     * @return List of {sectionId,sectionName}
     */
    public List<Object[]> getTemplateSections(String templateId) {
        return tDocService.getTemplateSections(templateId);
    }

    /**
     * Tender PDF Generation
     *
     * @param tenderId from tbl_TenderMaster
     * @param reqURL
     * @param reqQuery
     * @param folderName
     * @return true or false for success or fail
     */
    public boolean tenderPDFGenreation(int tenderId, String reqURL, String reqQuery, String folderName) {
        boolean flag = false;

        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        String docAvlMethod = commonService.getDocAvlMethod(tenderId + "").toString();
        try {

            File folderFile = new File(FilePathUtility.getFilePath().get("TenderSecUploadServlet") + tenderId);
            if (!folderFile.exists()) {
                folderFile.mkdir();
            }
            if ("Lot".equalsIgnoreCase(docAvlMethod)) {

                List<Object[]> lots = commonService.getLotDetails(tenderId + "");
                for (Object[] lot : lots) {
                    folderFile = new File(FilePathUtility.getFilePath().get("TenderSecUploadServlet") + tenderId + "\\" + "Lot_" + lot[1].toString());
                    if (!folderFile.exists()) {
                        folderFile.mkdir();
                    }
                    flag = isPDFGenerated(tenderId, reqURL, reqQuery, folderName, folderFile, lot[0].toString());
                }
            } else {
                folderFile = new File(FilePathUtility.getFilePath().get("TenderSecUploadServlet") + tenderId + "\\" + "Package");
                if (!folderFile.exists()) {
                    folderFile.mkdir();
                }
                flag = isPDFGenerated(tenderId, reqURL, reqQuery, folderName, folderFile, "0");

            }
            folderFile = null;
        } catch (Exception e) {
            logger.error("tenderPDFGenreation : " + e);
        }
        return flag;
    }

    /**
     * Check whether PDF generated or not and generates if not
     *
     * @param tenderId
     * @param reqURL
     * @param reqQuery
     * @param folderName
     * @param folderFile
     * @param porLotId
     * @return true or false for success or fail
     */
    public boolean isPDFGenerated(int tenderId, String reqURL, String reqQuery, String folderName, File folderFile, String porLotId) {
        boolean flag = false;
        logger.debug("isPDFGenerated Starts");
        String tempURLForms = reqURL;
        GenreatePdfCmd obj = new GenreatePdfCmd();
        //http://localhost:8080/resources/common/ViewTender.jsp?id=1700
        try {
            String pdfId = "";
            int tenderStdId = 0;
            int tenderittSectionId = 0;
            File file = null;
            File dtFile = null;
            String tempURL = reqURL;
            String path = obj.getPathForPdf(reqURL);
            //System.out.println("Path -------------------> "+path);
            GeneratePdfConstant pdfConst = new GeneratePdfConstant();
            TenderDocumentSrBean dDocSrBean = new TenderDocumentSrBean();
            if ("0".equalsIgnoreCase(porLotId)) {
                tenderStdId = dDocSrBean.getTenderSTDId(tenderId);
            } else {
                tenderStdId = dDocSrBean.getTenderSTDId(tenderId, Integer.parseInt(porLotId));
            }
            List<TblTenderSection> tSections = dDocSrBean.getTenderSections(tenderStdId);
            String fileName = null;
            String copyfolderName = null;
            int int_cnt = 0;
            if (tSections != null) {
                if (!tSections.isEmpty()) {
                    int ittId = -1, gccId = -1;
                    for (TblTenderSection tts : tSections) {
                        int_cnt++;
                        //tempURL = reqURL;
                        if (tts.getContentType().equalsIgnoreCase("ITT")) {
                            ittId = tts.getTenderSectionId();
                        } else if (tts.getContentType().equalsIgnoreCase("GCC")) {
                            gccId = tts.getTenderSectionId();
                        }
                        if ((tts.getContentType().equalsIgnoreCase("TDS")) || (tts.getContentType().equalsIgnoreCase("PCC")) || (tts.getContentType().equalsIgnoreCase("PDC"))) {
                            tempURL = tempURL.replace("TenderPdfGenerate", "TenderTDSView");
                            if (tts.getContentType().equalsIgnoreCase("TDS")) {
                                tenderittSectionId = ittId;
                                copyfolderName = "Section" + int_cnt + "_" + tts.getSectionName();
                                //System.out.println("CopyfolderName----------------->"+copyfolderName);
                            } else if (tts.getContentType().equalsIgnoreCase("PCC") || (tts.getContentType().equalsIgnoreCase("PDC"))) {
                                if(tts.getContentType().equalsIgnoreCase("PCC"))
                                {
                                    tempURL = tempURL.replace("TestTenderAllForms", "TenderTDSView");
                                }
                                copyfolderName = "Section" + int_cnt + "_" + tts.getSectionName();
                                tenderittSectionId = gccId;
                            }
                            dtFile = new File(folderFile + "\\" + copyfolderName + "\\");
                            if (!dtFile.exists()) {
                                dtFile.mkdir();
                            }
                            reqQuery = "tenderStdId=" + tenderStdId + "&tenderId=" + tenderId + "&sectionId=" + tenderittSectionId + "&porlId=" + porLotId;
                            pdfId = tenderId + "_" + tenderittSectionId + "_" + tenderStdId;
                            folderName = pdfConst.TENDERDOC;
                            file = new File(path + "/" + folderName + "/" + pdfId + "/TenderDoc.pdf");
                            pdfId = tenderId + "_" + tenderittSectionId + "_" + tenderStdId;
                            obj.genrateCmd(tempURL, reqQuery, folderName, pdfId);
                            dtFile = new File(dtFile + "\\" + copyfolderName + ".pdf");
                            copyfile(file, dtFile);
                            //<a href="TenderTDSDashBoard.jsp?tenderStdId=<%=tenderStdId%>&tenderId=<%=tenderId%>&sectionId=<%=tenderittSectionId%>">View</a>
                        }
                        if (tts.getContentType().equals("Form") || tts.getContentType().equals("TOR") || tts.getContentType().equals("POR")) {
                            copyfolderName = "Section" + int_cnt + "_" + tts.getSectionName();
                            StringBuilder strForAllFormPDF = new StringBuilder();
                            //java.util.List<com.cptu.egp.eps.model.table.TblTenderForms> forms = dDocSrBean.getTenderForm(tts.getTenderSectionId());
                            dtFile = new File(folderFile + "\\" + copyfolderName + "\\");
                            if (!dtFile.exists()) {
                                dtFile.mkdir();
                            }
                            // if (forms != null) {
                            //    if (!forms.isEmpty()) {
                            tempURL = tempURLForms.replace("TenderPdfGenerate", "TestTenderAllForms");
                            pdfId = tenderId + "_" + tts.getTenderSectionId() + "_" + tenderStdId;
                            folderName = pdfConst.TENDERDOC;
                            //for (int jj = 0; jj < forms.size(); jj++) {
                            //http://localhost:8080/eGPWeb/officer/ViewTenderCompleteForm.jsp?tenderId=1356&sectionId=538&formId=313
                            reqQuery = "tenderId=" + tenderId + "&sectionId=" + tts.getTenderSectionId() + "&porlId=" + porLotId;
                            /*  if(jj==0){
                            strForAllFormPDF.append(tempURL + "?" + reqQuery + "&isPDF=true" + "\" ");
                            }else if(jj==(forms.size()-1)){
                            strForAllFormPDF.append("\"" + tempURL + "?" + reqQuery + "&isPDF=true");
                            }else{
                            strForAllFormPDF.append("\"" + tempURL + "?" + reqQuery + "&isPDF=true" + "\" ");
                            } */

                            strForAllFormPDF.append("\"" + tempURL + "?" + reqQuery + "&isPDF=true" + "\" ");
                            //} //end for loop
                            obj.genrateCmdForm(strForAllFormPDF.toString(), "", folderName, pdfId);

                            file = new File(path + "/" + folderName + "/" + pdfId + "/TenderDoc.pdf");
                            //dtFile = new File(FilePathUtility.getFilePath().get("SevletEngEstUpload")+tenderId+"\\"+tts.getContentType()+"_TenderDoc.pdf");
                            dtFile = new File(dtFile + "\\" + copyfolderName + ".pdf");

                            copyfile(file, dtFile);

                            /// }
                            //  }//end if(forms != null)
                        }
                    } //end for
                }
            }
            flag = true;
        } catch (Exception e) {
            logger.error("isPDFGenerated : " + e);
        }
        logger.debug("isPDFGenerated Ends");
        return flag;
    }

    /**
     * Coppy File
     *
     * @param f1
     * @param f2
     * @return true if copy done successfully else return false
     */
    private boolean copyfile(File f1, File f2) {
        boolean flag = false;
        try {
            //File f1 = srFile;
            //File f2 = new File(dtFile);
            if (!f1.exists()) {
                throw new FileNotFoundException(f1.getName() + " is un available at " + f1.getAbsolutePath());
            }
            if (f2.exists()) {
                f2.delete();
            }
            f2.createNewFile();
            //System.out.println("F2 Name-------->"+f2+"Size of F2------>"+f2.length());
            InputStream in = new FileInputStream(f1);

            //For Append the file.
            //  OutputStream out = new FileOutputStream(f2,true);
            //For Overwrite the file.
            OutputStream out = new FileOutputStream(f2);

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.flush();
            out.close();
            flag = true;
            logger.debug("File copied");
        } catch (FileNotFoundException ex) {
            logger.error("copyfile : " + ex);
            ex.printStackTrace();
            AppExceptionHandler expHandle = new AppExceptionHandler();
            expHandle.stack2string(ex);
        } catch (IOException e) {
            logger.error("copyfile : " + e);
            e.printStackTrace();
            AppExceptionHandler expHandle = new AppExceptionHandler();
            expHandle.stack2string(e);
        }
        if (f1.exists()) {
            f1.delete();
        }
        //System.out.println("f2Lenght----------->"+f2.length());
        return flag;
    }

    /**
     * Tender Document Statistics for the user of tender
     *
     * @param tenderId from tbl_TenderMaster
     * @param lotId
     * @param userId bidder userId
     * @return count of statistics
     */
    public long tendDocStatCount(String tenderId, String lotId, String userId) {
        return tDocService.tendDocStatCount(tenderId, lotId, userId);
    }

    /**
     * Get Common Mapped document list
     *
     * @param tenderId
     * @param userId
     * @return Document details in array of Objects.
     */
    public List<Object[]> commonDocMapList(String tenderId, String userId) {
        return tDocService.commonDocMapList(tenderId, userId);
    }

    /**
     * Get Template id from section id.
     *
     * @param sectionId
     * @return template id
     */
    public int getTemplateIdfromSectionId(String sectionId) {
        return tDocService.getTemplateIdfromSectionId(sectionId);
    }

    /**
     * Get Tender Document Details
     *
     * @param tenderId
     * @return List of Tender documents as TblTenderSectionDocs objects
     */
    public List<TblTenderSectionDocs> getTenderDocsTenderId(short tenderId) {
        return tDocService.getDocsTenderId(tenderId);
    }

    /**
     * Get NOA document list
     *
     * @param tenderId
     * @param bidderId
     * @return document list
     */
    public List<TblNoaDocuments> getNoaDoc(int tenderId, int bidderId) {
        return tDocService.getNoaDoc(tenderId, bidderId);
    }

    /**
     * Get NOA id
     *
     * @param tenderId
     * @param bidderId
     * @return NOA id
     */
    public int getNoaId(int tenderId, int bidderId) {
        return tDocService.getNoaId(tenderId, bidderId);
    }

    /**
     * Get contract Document list
     *
     * @param noaId
     * @return Contract document list
     */
    public List<TblContractSignDocs> getContractDocList(int noaId) {
        return tDocService.getContractDocList(noaId);
    }

    /**
     * Dohatec Start for dumping old tender documents*
     */
    /**
     * Making a Zip file for the tender documents
     *
     * @param templateId tender Template
     * @param tenderId from tbl_TenderMaster
     * @return true or false for success or false
     */
    public boolean copyOldTenderDocs4ZIP(String templateId, String tenderId, String oldTenderId) {
        logger.debug("copyOldTenderDocs4ZIP : " + logUserId + "starts");
        boolean flag = false;
        File delFile = new File(FilePathUtility.getFilePath().get("TenderSecUploadServlet") + "\\" + tenderId);
        if (delFile.exists()) {
            FileSystemUtils.deleteRecursively(delFile);
        }
        try {
            /* File File1 = new File(FilePathUtility.getFilePath().get("TenderSecUploadServlet") + "\\" + oldTenderId);
            File File2 = new File(FilePathUtility.getFilePath().get("TenderSecUploadServlet") + "\\" + tenderId);
            if (File1.exists()) {
                FileSystemUtils.copyRecursively(File1, File2);
                flag = true;
            }
            else{*/
            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
            String procMethod = commonService.getProcMethod(tenderId).toString();
            String appId = "0";
            if ("LTM".equalsIgnoreCase(procMethod)) {
                appId = commonService.getAppIdByTenderId(tenderId);
            }
            String docAvlMethod = commonService.getDocAvlMethod(tenderId).toString();
            if ("Lot".equals(docAvlMethod)) {
                List<Object> lots = commonService.getLotNo(tenderId);
                for (Object lot : lots) {
                    String folderName = "Lot_" + lot.toString();
                    flag = dumpFiles(folderName, templateId, tenderId, appId);
                }
            } else {
                String folderName = "Package";
                flag = dumpFiles(folderName, templateId, tenderId, appId);
            }
            //  }

        } catch (Exception e) {
            logger.error("copyOldTenderDocs4ZIP : " + e);
            flag = false;
        }

        logger.debug("copyOldTenderDocs4ZIP : " + logUserId + "ends");
        return flag;
    }

    /**
     * Dohatec End for dumping old tender documents*
     */
    /**
     * Get old mapped contract Document list
     *
     * @param Tender Id
     * @param BIdder Id
     * @return Contract document list
     */
    public List<Object[]> mappedWinningDocMapList(String tenderId, String userId) {
        return tDocService.mappedWinningDocMapList(tenderId, userId);
    }

    public List<Object[]> getAllBoQFormsForNOA(int pkgId) {
        logger.debug("getAllBoQFormsForNOA : " + logUserId + "starts");
        List<Object[]> getTenderFormLot = null;
        try {
            getTenderFormLot = tenderFormService.getAllBoQFormsForNOA(pkgId);
        } catch (Exception e) {
            logger.error("getAllBoQFormsForNOA : " + e);
        }
        logger.debug("getAllBoQFormsForNOA : " + logUserId + "ends");

        return getTenderFormLot;
    }
    public List<Object[]> getAllBoQFormsForNOA(int pkgId, int tenderId, int userId) {
        logger.debug("getAllBoQFormsForNOA : " + logUserId + "starts");
        List<Object[]> getTenderFormLot = null;
        try {
            getTenderFormLot = tenderFormService.getAllBoQFormsForNOA(pkgId,tenderId,userId);
        } catch (Exception e) {
            logger.error("getAllBoQFormsForNOA : " + e);
        }
        logger.debug("getAllBoQFormsForNOA : " + logUserId + "ends");

        return getTenderFormLot;
    }

}
