/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblContractSign;
import com.cptu.egp.eps.model.table.TblCurrencyMaster;
import com.cptu.egp.eps.model.table.TblNoaAcceptance;
import com.cptu.egp.eps.model.table.TblNoaIssueDetails;
import com.cptu.egp.eps.model.table.TblTenderLotSecurity;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.model.table.TblTenderPayment;
import com.cptu.egp.eps.service.serviceimpl.NOAServiceImpl;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.AccPaymentService;
import com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.databean.ContractSignDtBean;
import com.cptu.egp.eps.web.databean.IssueNOADtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.DateUtils;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author rishita
 */
public class IssueNOASrBean {

    final Logger logger = Logger.getLogger(TempCompanyDocumentsSrBean.class);
    private NOAServiceImpl noaServiceImpl = (NOAServiceImpl) AppContext.getSpringBean("NOAServiceImpl");
    private CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
    private CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
    private UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    private AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
    private String logUserId = "0";

    public void setLogUserId(String logUserId) {
        noaServiceImpl.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /**
     * insert into TblNoaIssueDetails , TblNoaAcceptance
     *
     * @param issueNOADtBean
     * @param tenderId of tbl_TenderMaster return true if inserted successfully
     * else false
     */
    public boolean addIssueNOA(IssueNOADtBean issueNOADtBean, String roundId, String tenderId, boolean flag, String userId) {
        logUserId = userId;
        logger.debug("addIssueNOA : " + logUserId + " Starts");
        boolean flagg = false;
        try {
            TblNoaIssueDetails tblNoaIssueDetails = _toTblNoaIssueDetails(issueNOADtBean);
            boolean checkForEntry = noaServiceImpl.entryForUniqueIssueNOA(issueNOADtBean.getHdUserId(), issueNOADtBean.getTenderId(), flag, issueNOADtBean.getPkgLotId());
            if (checkForEntry || flag) {
                tblNoaIssueDetails.setPkgLotId(issueNOADtBean.getPkgLotId());
                tblNoaIssueDetails.setCreatedDt(new Date());
                if (issueNOADtBean.getRO() != null) {
                    tblNoaIssueDetails.setIsRepeatOrder("yes");
                } else {
                    tblNoaIssueDetails.setIsRepeatOrder("no");
                }

                tblNoaIssueDetails.setTblTenderMaster(new TblTenderMaster(issueNOADtBean.getTenderId()));
                tblNoaIssueDetails.setUserId(issueNOADtBean.getHdUserId());
                if (noaServiceImpl.addTblNoaIssueDetails(tblNoaIssueDetails)) {
                    TblNoaAcceptance tblNoaAcceptance = new TblNoaAcceptance();
                    tblNoaAcceptance.setTblNoaIssueDetails(tblNoaIssueDetails);
                    setNoaIssueId(tblNoaIssueDetails.getNoaIssueId());
                    tblNoaAcceptance.setComments(issueNOADtBean.getClauseRef());
                    tblNoaAcceptance.setUserId(issueNOADtBean.getHdUserId());
                    tblNoaAcceptance.setAcceptRejStatus("pending");
                    tblNoaAcceptance.setAcceptRejType("");
                    if (issueNOADtBean.getClauseRef() == null) {
                        tblNoaAcceptance.setComments("");
                    }
                    tblNoaAcceptance.setAcceptRejDt(null);
                    if (noaServiceImpl.addTblNoaAcceptance(tblNoaAcceptance)) {
                        String refNo = "";
                        String emailID = "";
                        String PEOfficeName = "";
                        for (Object[] obj : noaServiceImpl.getDataTenderDetails(tenderId)) {
                            refNo = obj[0].toString();
                            PEOfficeName = obj[1].toString();
                        }
                        for (Object obj : noaServiceImpl.getEmailIdNOA(tblNoaAcceptance.getUserId())) {
                            emailID = obj.toString();
                        }
                        String lotNo = "";
                        String lotDesc = "";
                        for (TblTenderLotSecurity tblTenderLotSecurity : noaServiceImpl.getLotDetailsNOA(issueNOADtBean.getPkgLotId())) {
                            lotNo = tblTenderLotSecurity.getLotNo();
                            lotDesc = tblTenderLotSecurity.getLotDesc();
                        }

                        String pkgLotID = "" + issueNOADtBean.getPkgLotId();
                        if (sendMailAndSMS(tblNoaIssueDetails.getContractAmt(), tblNoaIssueDetails.getContractAmtWords(), emailID, roundId, tenderId, refNo, PEOfficeName, tblNoaIssueDetails.getNoaAcceptDt(), pkgLotID, lotDesc)) 
                        {
                            //if (sendMailAndSMS(emailID, tenderId, refNo, PEOfficeName, tblNoaIssueDetails.getNoaAcceptDt(), lotNo, lotDesc)) {//String mailId,String tenderId, String refNo,String PEOfficeName,Date lastDate
                            flagg = true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("addIssueNOA : " + logUserId + " : " + e);
        }
        logger.debug("addIssueNOA : " + logUserId + " Ends");
        return flagg;
    }

    public boolean addIssueNOAForICT(IssueNOADtBean issueNOADtBean, String roundId, String tenderId, boolean flag, int NoOfCurrency, String userId) {
        logUserId = userId;
        logger.debug("addIssueNOA : " + logUserId + " Starts");
        boolean flagg = false;
        try {

            TblNoaIssueDetails tblNoaIssueDetails = _toTblNoaIssueDetails(issueNOADtBean);
            boolean checkForEntry = noaServiceImpl.entryForUniqueIssueNOA(issueNOADtBean.getHdUserId(), issueNOADtBean.getTenderId(), flag, issueNOADtBean.getPkgLotId());
            if (checkForEntry || flag) {
                tblNoaIssueDetails.setPkgLotId(issueNOADtBean.getPkgLotId());
                tblNoaIssueDetails.setCreatedDt(new Date());
                if (issueNOADtBean.getRO() != null) {
                    tblNoaIssueDetails.setIsRepeatOrder("yes");
                } else {
                    tblNoaIssueDetails.setIsRepeatOrder("no");
                }

                tblNoaIssueDetails.setTblTenderMaster(new TblTenderMaster(issueNOADtBean.getTenderId()));
                tblNoaIssueDetails.setUserId(issueNOADtBean.getHdUserId());

                //changes made here for ICT
                if (NoOfCurrency >= 1) {
                    tblNoaIssueDetails.setContractAmt(new BigDecimal(issueNOADtBean.getContractAmt1()));
                    tblNoaIssueDetails.setContractAmtWords(issueNOADtBean.getContractAmtWords1());
                    tblNoaIssueDetails.setPerfSecAmt(new BigDecimal(issueNOADtBean.getPerSecAmt1()));
                    tblNoaIssueDetails.setPerfSecAmtWords(issueNOADtBean.getPerSecAmtWords1());
                    String curID1 = issueNOADtBean.getCurrencyID1().replace("\r\n", "");
                    //curID=curID.replace("\r\n","");
                    //int cID = Integer.parseInt(curID);
                    tblNoaIssueDetails.setTblCurrencyMaster(new TblCurrencyMaster(Integer.parseInt(curID1)));

                    noaServiceImpl.addTblNoaIssueDetails(tblNoaIssueDetails);
                    flagg = true;
                }

                if (NoOfCurrency >= 2) {
                    tblNoaIssueDetails.setContractAmt(new BigDecimal(issueNOADtBean.getContractAmt2()));
                    tblNoaIssueDetails.setContractAmtWords(issueNOADtBean.getContractAmtWords2());
                    tblNoaIssueDetails.setPerfSecAmt(new BigDecimal(issueNOADtBean.getPerSecAmt2()));
                    tblNoaIssueDetails.setPerfSecAmtWords(issueNOADtBean.getPerSecAmtWords2());
                    //tblNoaIssueDetails.setCurrencyId(Integer.parseInt(issueNOADtBean.getCurrencyID2()));
                    String curID2 = issueNOADtBean.getCurrencyID2().replace("\r\n", "");
                    tblNoaIssueDetails.setTblCurrencyMaster(new TblCurrencyMaster(Integer.parseInt(curID2)));

                    noaServiceImpl.addTblNoaIssueDetails(tblNoaIssueDetails);
                    flagg = true;
                }

                if (NoOfCurrency >= 3) {
                    tblNoaIssueDetails.setContractAmt(new BigDecimal(issueNOADtBean.getContractAmt3()));
                    tblNoaIssueDetails.setContractAmtWords(issueNOADtBean.getContractAmtWords3());
                    tblNoaIssueDetails.setPerfSecAmt(new BigDecimal(issueNOADtBean.getPerSecAmt3()));
                    tblNoaIssueDetails.setPerfSecAmtWords(issueNOADtBean.getPerSecAmtWords3());
                    //tblNoaIssueDetails.setCurrencyId(Integer.parseInt(issueNOADtBean.getCurrencyID3()));
                    //tblNoaIssueDetails.setTblCurrencyMaster(new TblCurrencyMaster(Integer.parseInt(issueNOADtBean.getCurrencyID3())));
                    String curID3 = issueNOADtBean.getCurrencyID3().replace("\r\n", "");
                    tblNoaIssueDetails.setTblCurrencyMaster(new TblCurrencyMaster(Integer.parseInt(curID3)));

                    noaServiceImpl.addTblNoaIssueDetails(tblNoaIssueDetails);
                    flagg = true;
                }
                if (NoOfCurrency >= 4) {
                    tblNoaIssueDetails.setContractAmt(new BigDecimal(issueNOADtBean.getContractAmt4()));
                    tblNoaIssueDetails.setContractAmtWords(issueNOADtBean.getContractAmtWords4());
                    tblNoaIssueDetails.setPerfSecAmt(new BigDecimal(issueNOADtBean.getPerSecAmt4()));
                    tblNoaIssueDetails.setPerfSecAmtWords(issueNOADtBean.getPerSecAmtWords4());
                    //tblNoaIssueDetails.setCurrencyId(Integer.parseInt(issueNOADtBean.getCurrencyID4()));
                    //tblNoaIssueDetails.setTblCurrencyMaster(new TblCurrencyMaster(Integer.parseInt(issueNOADtBean.getCurrencyID4())));
                    String curID4 = issueNOADtBean.getCurrencyID4().replace("\r\n", "");
                    tblNoaIssueDetails.setTblCurrencyMaster(new TblCurrencyMaster(Integer.parseInt(curID4)));

                    noaServiceImpl.addTblNoaIssueDetails(tblNoaIssueDetails);
                    flagg = true;
                }

                //if (noaServiceImpl.addTblNoaIssueDetails(tblNoaIssueDetails)) {
                if (flagg) {
                    TblNoaAcceptance tblNoaAcceptance = new TblNoaAcceptance();
                    tblNoaAcceptance.setTblNoaIssueDetails(tblNoaIssueDetails);
                    setNoaIssueId(tblNoaIssueDetails.getNoaIssueId());
                    tblNoaAcceptance.setComments(issueNOADtBean.getClauseRef());
                    tblNoaAcceptance.setUserId(issueNOADtBean.getHdUserId());
                    tblNoaAcceptance.setAcceptRejStatus("pending");
                    tblNoaAcceptance.setAcceptRejType("");
                    if (issueNOADtBean.getClauseRef() == null) {
                        tblNoaAcceptance.setComments("");
                    }
                    tblNoaAcceptance.setAcceptRejDt(null);
                    if (noaServiceImpl.addTblNoaAcceptance(tblNoaAcceptance)) {
                        String refNo = "";
                        String emailID = "";
                        String PEOfficeName = "";
                        for (Object[] obj : noaServiceImpl.getDataTenderDetails(tenderId)) {
                            refNo = obj[0].toString();
                            PEOfficeName = obj[1].toString();
                        }
                        for (Object obj : noaServiceImpl.getEmailIdNOA(tblNoaAcceptance.getUserId())) {
                            emailID = obj.toString();
                        }
                        String lotNo = "";
                        String lotDesc = "";
                        for (TblTenderLotSecurity tblTenderLotSecurity : noaServiceImpl.getLotDetailsNOA(issueNOADtBean.getPkgLotId())) {
                            lotNo = tblTenderLotSecurity.getLotNo();
                            lotDesc = tblTenderLotSecurity.getLotDesc();
                        }
                        //System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>LOT NO:"+lotNo);
                        //added by salahuddin
                        String pkgLotID = "" + issueNOADtBean.getPkgLotId();
                        //System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>PKG LOT NO:"+pkgLotID);
                        if (sendMailAndSMS(tblNoaIssueDetails.getContractAmt(), tblNoaIssueDetails.getContractAmtWords(),emailID, roundId, tenderId, refNo, PEOfficeName, tblNoaIssueDetails.getNoaAcceptDt(), pkgLotID, lotDesc)) {
                            //if (sendMailAndSMS(emailID, tenderId, refNo, PEOfficeName, tblNoaIssueDetails.getNoaAcceptDt(), lotNo, lotDesc)) {//String mailId,String tenderId, String refNo,String PEOfficeName,Date lastDate
                            flagg = true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("addIssueNOA : " + logUserId + " : " + e);
        }
        logger.debug("addIssueNOA : " + logUserId + " Ends");
        return flagg;
    }

    /**
     * update information (TblNoaIssueDetails,TblNoaAcceptance)
     *
     * @param issueNOADtBean
     * @return true if updated successfully else false
     */
    public boolean updateIssueNOA(IssueNOADtBean issueNOADtBean) {
        logger.debug("updateIssueNOA : " + logUserId + " Starts");
        boolean flag = false;
        try {
            TblNoaIssueDetails tblNoaIssueDetails = _toTblNoaIssueDetails(issueNOADtBean);
            tblNoaIssueDetails.setNoaIssueId(issueNOADtBean.getNoaIssueId());
            tblNoaIssueDetails.setCreatedDt(issueNOADtBean.getCreatedDt());
            tblNoaIssueDetails.setTblTenderMaster(new TblTenderMaster(issueNOADtBean.getTenderId()));
            tblNoaIssueDetails.setUserId(issueNOADtBean.getHdUserId());
            if (noaServiceImpl.updateTblNoaIssueDetails(tblNoaIssueDetails)) {
                TblNoaAcceptance tblNoaAcceptance = new TblNoaAcceptance();
                tblNoaAcceptance.setTblNoaIssueDetails(tblNoaIssueDetails);
                setNoaIssueId(tblNoaIssueDetails.getNoaIssueId());
                tblNoaAcceptance.setNoaAcceptId(issueNOADtBean.getNoaAcceptId());
                tblNoaAcceptance.setComments(issueNOADtBean.getClauseRef());
                tblNoaAcceptance.setUserId(issueNOADtBean.getHdUserId());
                tblNoaAcceptance.setAcceptRejStatus(issueNOADtBean.getAcceptRejStatus());
                if (issueNOADtBean.getAcceptRejType() == null) {
                    tblNoaAcceptance.setAcceptRejType("");
                } else {
                    tblNoaAcceptance.setAcceptRejType(issueNOADtBean.getAcceptRejType());
                }
                tblNoaAcceptance.setAcceptRejDt(issueNOADtBean.getAcceptRejDt());
                if (noaServiceImpl.updateTblNoaAcceptance(tblNoaAcceptance)) {
                    flag = true;
                }
            }
        } catch (Exception e) {
            logger.error("updateIssueNOA : " + logUserId + " : " + e);
        }
        logger.debug("updateIssueNOA : " + logUserId + " Ends");
        return flag;
    }

    /**
     * for updating information into TblNoaIssueDetails ,TblNoaAcceptance
     *
     * @param issueNOADtBean
     * @return true if updated successfully else false
     */
    public boolean updateSerIssueNOA(IssueNOADtBean issueNOADtBean) {//for service
        boolean flag = false;
        logger.debug("updateSerIssueNOA : " + logUserId + " Starts");
        try {
            TblNoaIssueDetails tblNoaIssueDetails = _toTblNoaIssueDetails(issueNOADtBean);
            if (issueNOADtBean.getPerReq().equalsIgnoreCase("No")) {
                tblNoaIssueDetails.setPerSecSubDt(null);
                tblNoaIssueDetails.setPerSecSubDays((short) 0);
                tblNoaIssueDetails.setPerfSecAmt(BigDecimal.ZERO);
                tblNoaIssueDetails.setPerfSecAmtWords("");
            }
            tblNoaIssueDetails.setNoaIssueId(issueNOADtBean.getNoaIssueId());
            tblNoaIssueDetails.setCreatedDt(issueNOADtBean.getCreatedDt());
            tblNoaIssueDetails.setTblTenderMaster(new TblTenderMaster(issueNOADtBean.getTenderId()));
            tblNoaIssueDetails.setUserId(issueNOADtBean.getHdUserId());
            if (noaServiceImpl.updateTblNoaIssueDetails(tblNoaIssueDetails)) {
                TblNoaAcceptance tblNoaAcceptance = new TblNoaAcceptance();
                tblNoaAcceptance.setTblNoaIssueDetails(tblNoaIssueDetails);
                setNoaIssueId(tblNoaIssueDetails.getNoaIssueId());
                tblNoaAcceptance.setNoaAcceptId(issueNOADtBean.getNoaAcceptId());
                tblNoaAcceptance.setComments(issueNOADtBean.getClauseRef());
                tblNoaAcceptance.setUserId(issueNOADtBean.getHdUserId());
                tblNoaAcceptance.setAcceptRejStatus(issueNOADtBean.getAcceptRejStatus());
                if (issueNOADtBean.getAcceptRejType() == null) {
                    tblNoaAcceptance.setAcceptRejType("");
                } else {
                    tblNoaAcceptance.setAcceptRejType(issueNOADtBean.getAcceptRejType());
                }
                tblNoaAcceptance.setAcceptRejDt(issueNOADtBean.getAcceptRejDt());
                if (noaServiceImpl.updateTblNoaAcceptance(tblNoaAcceptance)) {
                    flag = true;
                }
            }
        } catch (Exception e) {
            logger.error("updateSerIssueNOA : " + logUserId + " : " + e);
        }
        logger.debug("updateSerIssueNOA : " + logUserId + " Ends");
        return flag;
    }

    /**
     * insert into TblNoaIssueDetails , TblNoaAcceptance
     *
     * @param issueNOADtBean
     * @return true if inserted successfully else false
     */
    public boolean addSerIssueNOA(IssueNOADtBean issueNOADtBean) {//for service
        boolean flag = false;
        logger.debug("addSerIssueNOA : " + logUserId + " Starts");
        try {
            TblNoaIssueDetails tblNoaIssueDetails = _toTblNoaIssueDetails(issueNOADtBean);
            if (issueNOADtBean.getPerReq().equalsIgnoreCase("No")) {
                tblNoaIssueDetails.setPerSecSubDt(null);
                tblNoaIssueDetails.setPerSecSubDays((short) 0);
                tblNoaIssueDetails.setPerfSecAmt(BigDecimal.ZERO);
                tblNoaIssueDetails.setPerfSecAmtWords("");
            }
            tblNoaIssueDetails.setCreatedDt(new Date());
            tblNoaIssueDetails.setTblTenderMaster(new TblTenderMaster(issueNOADtBean.getTenderId()));
            tblNoaIssueDetails.setUserId(issueNOADtBean.getHdUserId());
            if (noaServiceImpl.addTblNoaIssueDetails(tblNoaIssueDetails)) {
                TblNoaAcceptance tblNoaAcceptance = new TblNoaAcceptance();
                tblNoaAcceptance.setTblNoaIssueDetails(tblNoaIssueDetails);
                setNoaIssueId(tblNoaIssueDetails.getNoaIssueId());
                tblNoaAcceptance.setComments(issueNOADtBean.getClauseRef());
                tblNoaAcceptance.setUserId(issueNOADtBean.getHdUserId());
                tblNoaAcceptance.setAcceptRejStatus("pending");
                tblNoaAcceptance.setAcceptRejType("");
                tblNoaAcceptance.setAcceptRejDt(null);
                if (noaServiceImpl.addTblNoaAcceptance(tblNoaAcceptance)) {
                    flag = true;
                }
            }
        } catch (Exception e) {
            logger.error("addSerIssueNOA : " + logUserId + " : " + e);
        }
        logger.debug("addSerIssueNOA : " + logUserId + " Ends");
        return flag;
    }
    private int noaIssueId;

    public int getNoaIssueId() {
        return noaIssueId;
    }

    public void setNoaIssueId(int noaIssueId) {
        this.noaIssueId = noaIssueId;
    }

    /**
     * for copy Properties
     *
     * @param issueNOADtBean
     * @return TblNoaIssueDetails
     */
    public TblNoaIssueDetails _toTblNoaIssueDetails(IssueNOADtBean issueNOADtBean) {
        logger.debug("_toTblNoaIssueDetails : " + logUserId + " Starts");
        TblNoaIssueDetails tblNoaIssueDetails = null;
        try {
            tblNoaIssueDetails = new TblNoaIssueDetails();
            BeanUtils.copyProperties(issueNOADtBean, tblNoaIssueDetails);
        } catch (Exception e) {
            logger.error("_toTblNoaIssueDetails : " + logUserId + " : " + e);
        }
        logger.debug("_toTblNoaIssueDetails : " + logUserId + " Ends");
        return tblNoaIssueDetails;
    }

    /**
     * for copy Properties
     *
     * @param ContractSignDtBean
     * @return TblContractSign
     */
    public TblContractSign _toTblContractSign(ContractSignDtBean ContractSignDtBean) {
        logger.debug("_toTblContractSign : " + logUserId + " Starts");
        TblContractSign tblContractSign = null;
        try {
            tblContractSign = new TblContractSign();
            BeanUtils.copyProperties(ContractSignDtBean, tblContractSign);
        } catch (Exception e) {
            logger.error("_toTblContractSign : " + logUserId + " : " + e);
        }
        logger.debug("_toTblContractSign : " + logUserId + " Ends");
        return tblContractSign;
    }

    /**
     * fetching information TblNoaAcceptance, TblNoaIssueDetails,
     * TblTenderLotSecurity
     *
     * @param tenderId of Tbl_TenderMaster
     * @return List of objects
     */
    public List<Object[]> getDetails(int tenderId, int pkgLotId) {
        logger.debug("getDetails : " + logUserId + " Starts");
        List<Object[]> list = null;
        try {
            list = noaServiceImpl.getDetailsNOA(tenderId, pkgLotId);
        } catch (Exception e) {
            logger.error("getDetails : " + logUserId + " : " + e);
        }
        logger.debug("getDetails : " + logUserId + " Ends");
        return list;
    }

    public List<Object[]> getDetailsNOAforInvoice(int tenderId, int pkgLotId) {
        logger.debug("getDetailsNOAforInvoice : " + logUserId + " Starts");
        List<Object[]> list = null;
        try {
            list = noaServiceImpl.getDetailsNOAforInvoice(tenderId, pkgLotId);
        } catch (Exception e) {
            logger.error("getDetailsNOAforInvoice : " + logUserId + " : " + e);
        }
        logger.debug("getDetailsNOAforInvoice : " + logUserId + " Ends");
        return list;
    }

    public List<Object[]> getDetailsForRO(int tenderId, int pkgLotId, int roundId) {
        logger.debug("getDetails : " + logUserId + " Starts");
        List<Object[]> list = null;
        try {
            list = noaServiceImpl.getDetailsNOAForRO(tenderId, pkgLotId, roundId);
        } catch (Exception e) {
            logger.error("getDetails : " + logUserId + " : " + e);
        }
        logger.debug("getDetails : " + logUserId + " Ends");
        return list;
    }

    /**
     * fetching information from TblNoaAcceptance, TblNoaIssueDetails,
     * TblTenderLotSecurity
     *
     * @param tenderId of Tbl_TenderMaster
     * @return list of Objects
     */
    public List<Object[]> getNOAListing(int tenderId) {
        logger.debug("getNOAListing : " + logUserId + " Starts");
        List<Object[]> list = null;
        try {
            list = noaServiceImpl.getNOAListing(tenderId);
        } catch (Exception e) {
            logger.error("getNOAListing : " + logUserId + " : " + e);
        }
        logger.debug("getNOAListing : " + logUserId + " Ends");
        return list;
    }

    public List<Object[]> getNOAListingForRO(int tenderId, int roundId) {
        logger.debug("getNOAListingForRO : " + logUserId + " Starts");
        List<Object[]> list = null;
        try {
            list = noaServiceImpl.getNOAListingForRO(tenderId, roundId);
        } catch (Exception e) {
            logger.error("getNOAListingForRO : " + logUserId + " : " + e);
        }
        logger.debug("getNOAListingForRO : " + logUserId + " Ends");
        return list;
    }

    public List<Object[]> getNOAListingForROForPF(int tenderId, int roundId) {
        logger.debug("getNOAListingForROForPF : " + logUserId + " Starts");
        List<Object[]> list = null;
        try {
            list = noaServiceImpl.getNOAListingForROForPF(tenderId, roundId);
        } catch (Exception e) {
            logger.error("getNOAListingForROForPF : " + logUserId + " : " + e);
        }
        logger.debug("getNOAListingForROForPF : " + logUserId + " Ends");
        return list;
    }

    /**
     * fetching information from TblTenderPayment
     *
     * @param tenderId of Tbl_TenderMaster
     * @param pkgLotId
     * @param userId
     * @return List of TblTenderPayment
     */
    public List<TblTenderPayment> getPaymentDetails(int tenderId, int pkgLotId, int userId) {
        logger.debug("getPaymentDetails : " + logUserId + " Starts");
        List<TblTenderPayment> list = null;
        try {
            list = noaServiceImpl.getPaymentDetails(tenderId, pkgLotId, userId);
        } catch (Exception e) {
            logger.error("getPaymentDetails : " + logUserId + " : " + e);
        }
        logger.debug("getPaymentDetails : " + logUserId + " Ends");
        return list;
    }

    public List<TblTenderPayment> getPaymentDetailsForRO(int tenderId, int pkgLotId, int userId, int noaId) {
        logger.debug("getPaymentDetailsForRO : " + logUserId + " Starts");
        List<TblTenderPayment> list = null;
        try {
            list = noaServiceImpl.getPaymentDetailsForRO(tenderId, pkgLotId, userId, noaId);
        } catch (Exception e) {
            logger.error("getPaymentDetailsForRO : " + logUserId + " : " + e);
        }
        logger.debug("getPaymentDetailsForRO : " + logUserId + " Ends");
        return list;
    }

    /**
     * fetching information from TblNoaAcceptance, TblNoaIssueDetails
     *
     * @param tenderId of Tbl_TenderMaster
     * @return List of Objects
     */
    public List<Object[]> getDetailsService(int tenderId) {
        logger.debug("getDetailsService : " + logUserId + " Starts");
        List<Object[]> list = null;
        try {
            list = noaServiceImpl.getDetailsNOAService(tenderId);
        } catch (Exception e) {
            logger.error("getDetailsService : " + logUserId + " : " + e);
        }
        logger.debug("getDetailsService : " + logUserId + " Ends");
        return list;
    }

    /**
     * fetching information from TblNoaAcceptance,TblNoaIssueDetails
     *
     * @param noaIssueId of TblNoaIssueDetails
     * @return List of objects
     */
    public List<Object[]> getListNOA(int noaIssueId) {
        logger.debug("getListNOA : " + logUserId + " Starts");
        List<Object[]> list = null;
        try {
            list = noaServiceImpl.getListNOA(noaIssueId);
        } catch (Exception e) {
            logger.error("getListNOA : " + logUserId + " : " + e);
        }
        logger.debug("getListNOA : " + logUserId + " Ends");
        return list;
    }

    /**
     * fetching information from TblNoaIssueDetails
     *
     * @param naoIssueId
     * @return List of TblNoaIssueDetails
     */
    public List<TblNoaIssueDetails> getDetailsCS(int naoIssueId) {
        logger.debug("getDetailsCS : " + logUserId + " Starts");
        List<TblNoaIssueDetails> list = null;
        try {
            list = noaServiceImpl.getDetailsCS(naoIssueId);
        } catch (Exception e) {
            logger.error("getDetailsCS : " + logUserId + " : " + e);
        }
        logger.debug("getDetailsCS : " + logUserId + " Ends");
        return list;
    }

    /**
     * insert into TblContractSign
     *
     * @param contractSignDtBean return true if inserted successfully else false
     */
    public boolean addTblContractSign(ContractSignDtBean contractSignDtBean, String[] witnessInfo, String tenderId) {
        logger.debug("addTblContractSign : " + logUserId + " Starts");
        boolean flag = false;
        try {
            TblContractSign tblContractSign = _toTblContractSign(contractSignDtBean);
            tblContractSign.setCreatedDt(new Date());
            String wInfo = "";
            for (int i = 0; i < witnessInfo.length; i++) {
                wInfo = wInfo + witnessInfo[i] + "@";
            }
            tblContractSign.setWitnessInfo(wInfo.substring(0, wInfo.length() - 1));
            if (noaServiceImpl.addTblContractSign(tblContractSign)) {
                String emailId = "";
                int userId = 0;
                for (TblNoaIssueDetails details : noaServiceImpl.getUserIdNOA(contractSignDtBean.getNoaId())) {
                    userId = details.getUserId();
                }
                for (Object obj : noaServiceImpl.getEmailIdNOA(userId)) {
                    emailId = obj.toString();
                }
                String refNo = "";
                String PEOfficeName = "";
                for (Object[] obj : noaServiceImpl.getDataTenderDetails(tenderId)) {
                    refNo = obj[0].toString();
                    PEOfficeName = obj[1].toString();
                }
                String[] mailTo = {emailId};
                UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                registerService.contentAdmMsgBox(mailTo[0], XMLReader.getMessage("emailIdNoReply"), "Contract Signed Successfully", msgBoxContentUtility.contentContractSign(tenderId, refNo, PEOfficeName, DateUtils.customDateFormate(tblContractSign.getContractSignDt())));

                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                MailContentUtility mailContentUtility = new MailContentUtility();

                sendMessageUtil.setEmailTo(mailTo);
                sendMessageUtil.setEmailSub("e-GP: Contract Signed Successfully");
                sendMessageUtil.setEmailMessage(mailContentUtility.getContractMailContent(tenderId, refNo, PEOfficeName, DateUtils.customDateFormate(tblContractSign.getContractSignDt())));
                try {
                    sendMessageUtil.sendEmail();
                } catch (Exception e) {
                    logger.error(" sendMailAndSMS : error in send mail : " + logUserId + " : " + e);
                }

                flag = true;
            }
        } catch (Exception e) {
            logger.error("addTblContractSign : " + logUserId + " : " + e);
        }
        logger.debug("addTblContractSign : " + logUserId + " Ends");
        return flag;
    }
    private int contractSignId;

    public int getContractSignId() {
        return contractSignId;
    }

    public void setContractSignId(int contractSignId) {
        this.contractSignId = contractSignId;
    }

    /**
     * fetching information from TblContractSign
     *
     * @param noaIssueId
     * @return true or false
     */
    public boolean isAvlTCS(int noaIssueId) {
        logger.debug("isAvlTCS : " + logUserId + " Starts");
        boolean flag = false;
        try {
            for (TblContractSign getID : noaServiceImpl.getCSID(noaIssueId)) {
                setContractSignId(getID.getContractSignId());
            }
            flag = noaServiceImpl.isAvlTCS(noaIssueId);
        } catch (Exception e) {
            logger.error("isAvlTCS : " + logUserId + " : " + e);
        }
        logger.debug("isAvlTCS : " + logUserId + " Ends");
        return flag;
    }

    /**
     * fetching information from Tbl_ContractSign
     *
     * @param contractSignId of Tbl_ContractSign
     * @return List of TblContractSign
     */
    public List<TblContractSign> getDetailsContractSign(int contractSignId) {
        logger.debug("getDetailsContractSign : " + logUserId + " Starts");
        List<TblContractSign> list = null;
        try {
            list = noaServiceImpl.getDetailsContractSign(contractSignId);
        } catch (Exception e) {
            logger.error("getDetailsContractSign : " + logUserId + " : " + e);
        }
        logger.debug("getDetailsContractSign : " + logUserId + " Ends");
        return list;
    }

    /**
     * updating information into TblContractSign
     *
     * @param contractSignDtBean
     * @return true if updated successfully else false
     */
    public boolean updateContractSign(ContractSignDtBean contractSignDtBean) {
        logger.debug("updateContractSign : " + logUserId + " Starts");
        boolean flag = false;
        try {
            TblContractSign tblContractSign = _toTblContractSign(contractSignDtBean);
            tblContractSign.setContractSignId(contractSignDtBean.getContractSignId());
            if (noaServiceImpl.updateContractSign(tblContractSign)) {
                flag = true;
            }
        } catch (Exception e) {
            logger.error("updateContractSign : " + logUserId + " : " + e);
        }
        logger.debug("updateContractSign : " + logUserId + " Ends");
        return flag;
    }

    //contract Signing listing bidder side
    /**
     * fetching information from TblContractSign ,TblNoaIssueDetails
     *
     * @param tenderId of Tbl_TenderMaster
     * @param userId
     * @return List Of objects
     */
    public List<Object[]> getCSListingBidder(int tenderId, int userId) {
        logger.debug("getCSListingBidder : " + logUserId + " Starts");
        List<Object[]> list = null;
        try {
            list = noaServiceImpl.getCSListingBidder(tenderId, userId);
        } catch (Exception e) {
            logger.error("getCSListingBidder : " + logUserId + " : " + e);
        }
        logger.debug("getCSListingBidder : " + logUserId + " Ends");
        return list;
    }

    public List<Object[]> getCSListingBidderForRO(int tenderId, int userId, int roundId) {
        logger.debug("getCSListingBidder : " + logUserId + " Starts");
        List<Object[]> list = null;
        try {
            list = noaServiceImpl.getCSListingBidderForRO(tenderId, userId, roundId);
        } catch (Exception e) {
            logger.error("getCSListingBidder : " + logUserId + " : " + e);
        }
        logger.debug("getCSListingBidder : " + logUserId + " Ends");
        return list;
    }
    //contract Signing listing bidder side

    /**
     * fetching information from TblContractSign ,TblNoaIssueDetails
     *
     * @param tenderId of Tbl_TenderMaster
     * @return Report type
     */
    public String getReptType(int tenderId) {
        logger.debug("getReptType : " + logUserId + " Starts");
        String reptType = null;
        try {
            reptType = noaServiceImpl.getReptType(tenderId);
        } catch (Exception e) {
            logger.error("getReptType : " + logUserId + " : " + e);
        }
        logger.debug("getReptType : " + logUserId + " Ends");
        return reptType;
    }

    /**
     * for sending mail and sms
     *
     * @param logUserId
     * @param tenderId of Tbl_TenderMaster
     * @param refNo
     * @param PEOfficeName
     * @param lastDate
     * @return true or false
     */
    private boolean sendMailAndSMS(BigDecimal amountInNumber, String amountInWords, String mailId, String roundId, String tenderId, String refNo, String PEOfficeName, Date lastDate, String lotNo, String lotDesc) {
        logger.debug("sendMailAndSMS : " + logUserId + " Starts");
        boolean mailSent = false;
        
        /*Nittish Start For NOA Mail 
        SP used in  p_get_tendercommondata
        */ 
        
        String bidderName;
        String bidderAddress;
        String hopaName;
        String bidDatedTo;
        String procurementNature;
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            String[] mailTo = {mailId};
            //System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>LotNo:"+lotNo);
            MailContentUtility mailContentUtility = new MailContentUtility();
            
            /*SP used in  p_get_tendercommondata
                generate HopaName
            */
            TenderCommonService objTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            List<SPTenderCommonData> tenderinfo = objTenderCommonService.returndata("getTenderInfoAfterTCApprove", tenderId, null);
            hopaName = tenderinfo.get(0).getFieldName6();
            
            /*SP used in  p_get_tendercommondata
                generate Time Of bidding Date
            */
            
            List<SPTenderCommonData> bidDated = objTenderCommonService.returndata("getTenderBidDated", mailId, tenderId);
            bidDatedTo = bidDated.get(0).getFieldName1();
            
            /*SP used in  p_get_tendercommondata
                generate Tender Types
            */
            
            List<SPTenderCommonData> getProcurementNature = objTenderCommonService.returndata("getTenderProcurementNature", tenderId, null);
            procurementNature = getProcurementNature.get(0).getFieldName1();
            
            /*SP used in  p_get_tendercommondata
                generate Bidder Name and Address
            */
            
            List<SPTenderCommonData> bidderInfo = objTenderCommonService.returndata("getBidderInfoAfterTCApprove", tenderId, roundId);
            if (bidderInfo.get(0).getFieldName1().equalsIgnoreCase("company")) 
            {
                bidderName = bidderInfo.get(0).getFieldName2();
                bidderAddress = bidderInfo.get(0).getFieldName3()
                        + (bidderInfo.get(0).getFieldName4().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName4())
                        + (bidderInfo.get(0).getFieldName5().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName5())
                        + (bidderInfo.get(0).getFieldName6().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName6())
                        + (bidderInfo.get(0).getFieldName7().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName7());
            } 
            else 
            {
                bidderName = bidderInfo.get(0).getFieldName2()
                        + (bidderInfo.get(0).getFieldName3().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName3())
                        + (bidderInfo.get(0).getFieldName4().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName4())
                        + (bidderInfo.get(0).getFieldName5().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName5());
                bidderAddress = bidderInfo.get(0).getFieldName6()
                        + (bidderInfo.get(0).getFieldName7().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName7())
                        + (bidderInfo.get(0).getFieldName8().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName8())
                        + (bidderInfo.get(0).getFieldName9().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName9())
                        + (bidderInfo.get(0).getFieldName10().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName10())
                        + (bidderInfo.get(0).getFieldName11().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName11());
            }
            
            //Nitish END
            
            //added by dohatec
            List<Object[]> noaDetails = noaServiceImpl.getDetailsNOA(Integer.parseInt(tenderId), Integer.parseInt(lotNo));
            Object[] obj = null;
            obj = noaDetails.get(0);

            String messageSub = "e-GP: Letter of Acceptance (LOA)";
            String peName = "";
            String strFrom = commonservice.getEmailId(logUserId);

            List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(logUserId));
            if (peNameObj.get(0) != null) {
                peName = peNameObj.get(0).toString();
            }

            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            registerService.contentAdmMsgBox(mailTo[0], strFrom, messageSub, msgBoxContentUtility.contApproval(tenderId, refNo, "", "", strFrom, peName, lotNo, lotDesc));
            sendMessageUtil.setSmsNo(accPaymentService.getMobileNoWithCountryCode(accPaymentService.getUserId(mailTo[0])));
            StringBuilder sb = new StringBuilder();
            sb.append("Dear User,%0AYou have been notified for being awarded ");
            sb.append("for Contract No." + obj[2].toString());
            sb.append("%0AThanks,%0AeGP System");
            sendMessageUtil.setSmsBody(sb.toString());
            sendMessageUtil.sendSMS();

            sendMessageUtil.setEmailTo(mailTo);
            sendMessageUtil.setEmailSub("e-GP: Letter of Acceptance (LOA)");
            sendMessageUtil.setEmailMessage(mailContentUtility.getNOAMailContent(procurementNature, amountInNumber ,amountInWords ,bidDatedTo, hopaName, bidderName, bidderAddress, tenderId, refNo, PEOfficeName, DateUtils.customDateFormate(lastDate), lotNo, lotDesc));
            try {
                sendMessageUtil.sendEmail();
            } catch (Exception e) {
                logger.error(" sendMailAndSMS : error in send mail : " + logUserId + " : " + e);
            }
            mailSent = true;
        } catch (Exception ex) {
            logger.error("sendMailAndSMS : " + logUserId + " : " + ex);
        }
        logger.debug("sendMailAndSMS : " + logUserId + " Ends");
        return mailSent;
    }
}
