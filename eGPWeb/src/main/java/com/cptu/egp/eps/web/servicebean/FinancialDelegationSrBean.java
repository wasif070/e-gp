/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.FinancialDelegationData;
import com.cptu.egp.eps.service.serviceinterface.FinancialDelegationService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Istiak (Dohatec) - Jun 15, 2015
 */
public class FinancialDelegationSrBean {

    FinancialDelegationService oFinancialDelegationService = (FinancialDelegationService) AppContext.getSpringBean("FinancialDelegationService");
    final static Logger LOGGER = Logger.getLogger(FinancialDelegationSrBean.class);

    /*  Financial Delegation Configuration - START */
    
    public String getBudgetType(){
        List<FinancialDelegationData> budgetType = oFinancialDelegationService.financialDelegationData("getBudgetType", "", "", "", "", "", "", "", "", "", "", "", "", "", "");

        return getAllDropDown(budgetType);
    }

    public String getTenderType(){
        List<FinancialDelegationData> tenderType = oFinancialDelegationService.financialDelegationData("getTenderType", "", "", "", "", "", "", "", "", "", "", "", "", "", "");

        return getAllDropDown(tenderType);
    }

    public String getProcurementMethod(){
        List<FinancialDelegationData> procMethod = oFinancialDelegationService.financialDelegationData("getProcurementMethod", "", "", "", "", "", "", "", "", "", "", "", "", "", "");

        return getAllDropDown(procMethod);
    }

    public String getProcurementType(){
        List<FinancialDelegationData> procType = oFinancialDelegationService.financialDelegationData("getProcurementType", "", "", "", "", "", "", "", "", "", "", "", "", "", "");

        return getAllDropDown(procType);
    }

    public String getProcurementNature(){
        List<FinancialDelegationData> procNature = oFinancialDelegationService.financialDelegationData("getProcurementNature", "", "", "", "", "", "", "", "", "", "", "", "", "", "");

        return getAllDropDown(procNature);
    }

   public String getApprovingAuthority(){
        List<FinancialDelegationData> appAuthority = oFinancialDelegationService.financialDelegationData("getApprovingAuthority", "", "", "", "", "", "", "", "", "", "", "", "", "", "");

        return getAllDropDown(appAuthority);
    }

    public String getIsBoD(){
        List<FinancialDelegationData> IsBoD = oFinancialDelegationService.financialDelegationData("getIsBOd", "", "", "", "", "", "", "", "", "", "", "", "", "", "");

        return getAllDropDown(IsBoD);
    }

    public String getIsCorporation(){
        List<FinancialDelegationData> IsBoD = oFinancialDelegationService.financialDelegationData("getIsCorporation", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
        return getAllDropDown(IsBoD);
    }

    public String getOfficeLevel(){
        List<FinancialDelegationData> OfficeLevel = oFinancialDelegationService.financialDelegationData("getPEOfficeLevel", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
        return getAllDropDown(OfficeLevel);
    }

    private String getAllDropDown(List<FinancialDelegationData> list) {
        StringBuilder sb = new StringBuilder();
        for(FinancialDelegationData item : list){
            String value = item.getFieldName2();
            //Changed by Emtaz on 19/April/2016
            if(value.equalsIgnoreCase("Development"))
                sb.append("<option value='").append(value).append("'>").append("Capital").append("</option>");
            else if(value.equalsIgnoreCase("Revenue"))
                sb.append("<option value='").append(value).append("'>").append("Recurrent").append("</option>");
            else if(value.equalsIgnoreCase("HOPE"))
                sb.append("<option value='").append(value).append("'>").append("HOPA").append("</option>");
            else if(value.equalsIgnoreCase("PE"))
                sb.append("<option value='").append(value).append("'>").append("PA").append("</option>");
            else
                sb.append("<option value='").append(value).append("'>").append(value).append("</option>");
        }
        return sb.toString();
    }

    /*  Financial Delegation Configuration - END */

    /*  Financial Delegation Rank Configuration - START */

    public String showDelegationRanks(String flag){
        StringBuilder sb = new StringBuilder();
        int count = 1;
        List<FinancialDelegationData> ranks = getDelegationRanks();

        if(flag.equalsIgnoreCase("v")){
            for(FinancialDelegationData list : ranks){
                //Changed by Emtaz on 19/April/2016
                String ChangedApprovingAuthorityForBhutan = "";
                if(list.getFieldName2().equalsIgnoreCase("HOPE"))
                {
                    ChangedApprovingAuthorityForBhutan = "HOPA";
                }
                else if(list.getFieldName2().equalsIgnoreCase("PE"))
                {
                    ChangedApprovingAuthorityForBhutan = "PA";
                }
                else
                {
                    ChangedApprovingAuthorityForBhutan = list.getFieldName2();
                }
                sb.append("<tr>");
                    sb.append("<td class='t-align-center'>").append(count++).append("</td>");
                    sb.append("<td class='t-align-center'>").append(ChangedApprovingAuthorityForBhutan).append("</td>");
                sb.append("</tr>");
            }
        }else{
            List<FinancialDelegationData> procRole = getAllProcurementRole();
            for(FinancialDelegationData list : ranks){
                sb.append("<tr>");
                    sb.append("<td class='t-align-center'>");
                        sb.append("<input id='chk_").append(list.getFieldName1()).append("' name='ChkRanksId' type='checkbox' value='").append(list.getFieldName1()).append("' />");
                        sb.append("<input type='hidden' name='RanksId' id='RanksId_").append(list.getFieldName1()).append("' value='").append(list.getFieldName1()).append("'>");
                        sb.append("<input type='hidden' name='NewOldEdit' id='NewOldEdit_").append(list.getFieldName1()).append("' value='Old'>");
                    sb.append("</td>");

                    sb.append("<td class='t-align-center'>");
                        int rank = count++;
                        sb.append(rank);
                        sb.append("<input type='hidden' name='Count' id='Count_").append(list.getFieldName1()).append("' value='").append(rank).append("'>");
                    sb.append("</td>");

                    sb.append("<td class='t-align-center'>");
                        sb.append(ProcurementRoleDropdown(procRole, list.getFieldName2(), list.getFieldName1()));
                        sb.append("<input type='hidden' name='Old' id='Old_").append(list.getFieldName1()).append("' value='").append(list.getFieldName2()).append("'>");
                    sb.append("</td>");
                sb.append("</tr>");
            }
        }
        return sb.toString();
    }

    private List<FinancialDelegationData> getAllProcurementRole(){
        return oFinancialDelegationService.financialDelegationData("getAllProcurementRole", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
    }

    private List<FinancialDelegationData> getDelegationRanks(){
         return oFinancialDelegationService.financialDelegationData("ShowRanksTable", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
    }

    private String ProcurementRoleDropdown(List<FinancialDelegationData> list, String name, String id) {
        StringBuilder sb = new StringBuilder();
        String selected = null;
        sb.append("<select name='Designation'").append(" class='formTxtBox_1'").append(" id='Designation").append("_").append(id).append("' style='width:100px;' onChange='NewOrEdit(").append(id).append(")'>");
        for(FinancialDelegationData finDelegation : list){
            selected = "";
            //Chganged by Emtaz on 19/April/2016
            String item = finDelegation.getFieldName2();
            if(item.equalsIgnoreCase(name))
                selected = " selected='selected'";
            if(item.equalsIgnoreCase("HOPE"))
                sb.append("<option value='").append(item).append("' ").append(selected).append(">").append("HOPA").append("</option>");
            else if(item.equalsIgnoreCase("PE"))
                sb.append("<option value='").append(item).append("' ").append(selected).append(">").append("PA").append("</option>");
            else
                sb.append("<option value='").append(item).append("' ").append(selected).append(">").append(item).append("</option>");
        }
        sb.append("</select>");
        return sb.toString();
    }

    public String getProcurementRoleDropDown(){
        return getAllDropDown(getAllProcurementRole());
    }

    /*  Financial Delegation Rank Configuration - END */
}
