/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.service.serviceimpl.CmsServiceForServiceCase;
import com.cptu.egp.eps.service.serviceimpl.ConsolodateService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.DateUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author shreyansh
 */
public class CmsServiceServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String action = request.getParameter("action");
            String tenderId = request.getParameter("tenderId");
            int max = Integer.parseInt(request.getParameter("size"));
            int first = Integer.parseInt(request.getParameter("pageNo"));
            int wpId = Integer.parseInt(request.getParameter("wpId"));
            CmsServiceForServiceCase serviceCase = (CmsServiceForServiceCase) AppContext.getSpringBean("CmsService");
            ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
            long rowcount = service.getRowcountForBoq(request.getParameter("wpId"));
            String styleClass = "";
            if ("viewDatesInService".equalsIgnoreCase(action)) {
                List<Object[]> list = serviceCase.getDatesForEditInService(wpId, first, max);
                if (list != null && !list.isEmpty()) {
                    String link = "";
                    int i = 0;
                    for (i = 0; i < list.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }
                        out.print("<tr class='" + styleClass + "'>");
                        out.print("<td class=\"t-align-center\">" + list.get(i)[0] + "</td>");
                        out.print("<td class=\"t-align-center\">" + list.get(i)[1] + "</td>");
                        out.print("<td class=\"t-align-center\">" + list.get(i)[2] + "</td>");
                        out.print("<td class=\"t-align-center\">" + list.get(i)[3] + "</td>");
                        out.print("<td class=\"t-align-center\">" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i)[4]).split(" ")[0] + "</td>");
                        out.print("<td class=\"t-align-center\">" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i)[5]).split(" ")[0] + "</td>");
                        System.out.println(request.getParameter("flag"));
                        if (!"".equalsIgnoreCase(request.getParameter("flag"))) {

                             out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                    + "readonly onclick =GetCal('txtsdate_" + i + "','txtsdate_" + i + "') "
                                    + "name=txtsdate_" + i + " id=txtsdate_" + i + " value='"+DateUtils.gridDateToStrWithoutSec((Date) list.get(i)[6]).split(" ")[0]+"' />"
                                    + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                    + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtsdate_" + i + "','calc_" + i + "') /></a></td>");

                             out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                    + "readonly onclick =GetCal('txtedate_" + i + "','txtedate_" + i + "') "
                                    + "name=txtedate_" + i + " id=txtedate_" + i + " value='"+DateUtils.gridDateToStrWithoutSec((Date) list.get(i)[6]).split(" ")[0]+"' />"
                                    + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calcc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                    + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtedate_" + i + "','calcc_" + i + "') /></a></td>");

                        } else {
                            out.print("<td class=\"t-align-center\">" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i)[6]).split(" ")[0] + "</td>");
                            out.print("<td class=\"t-align-center\">" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i)[7]).split(" ")[0] + "</td>");
                        }

                        out.print("</tr>");
                        out.print("<input type=hidden name=lumsumId_" + i + " id=lumsumId_" + i + " value=" + list.get(i)[8] + " />");
                    }
                    out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");
                    int totalPages = 1;

                    if (list.size() > 0) {
                        int cc = (int) rowcount;
                        totalPages = (int) Math.ceil(Math.ceil(cc) / (max));
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                } else {
                    out.print("<tr>");
                    out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"5\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }

            }else if("editdates".equalsIgnoreCase(action)){
                int listcount = Integer.parseInt(request.getParameter("listcount"));
                for(int i=0;i<listcount;i++){
                    serviceCase.updateDates(Integer.parseInt(request.getParameter("lumsumId_"+i)), DateUtils.convertStringtoDate(request.getParameter("txtsdate_" + i).trim(), "dd-MMM-yyyy"), DateUtils.convertStringtoDate(request.getParameter("txtedate_" + i).trim(), "dd-MMM-yyyy"));
                }
                response.sendRedirect("officer/WorkScheduleMain.jsp?tenderId="+tenderId+"&msg=edit");
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
