/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblProgrammeMaster;
import com.cptu.egp.eps.web.servicebean.ProgrammeMasterSrBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author Naresh.Akurathi
 */
public class ProgrammeGridServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final Logger LOGGER = Logger.getLogger(ProgrammeGridServlet.class);

    /**
     * This servlet handles a request for grid view of program page
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        ProgrammeMasterSrBean progMasterSrBean = new ProgrammeMasterSrBean();
        String logUserId = "0";
        if (request.getSession().getAttribute("userId") != null) {
          logUserId = request.getSession().getAttribute("userId").toString();
          progMasterSrBean.setLogUserId(logUserId);
          }

        int totalRecords = 0;
        Integer totalPages = 0 ;
        LOGGER.debug("processRequest : "+logUserId+" Starts");
        try {
            if(request.getSession().getAttribute("userTypeId")!=null){
                   LOGGER.debug("ProgramGridServlet : action : getting data : " + logUserId + " Starts");
                   

                   
                   String user =request.getSession().getAttribute("userTypeId").toString();
                   int userId = Integer.parseInt(user);
                   LOGGER.debug("User ID==="+logUserId+" : "+userId);

                   Integer recPerPage = Integer.parseInt(request.getParameter("rows"));
                   Integer pageNumber =Integer.parseInt(request.getParameter("page"));

                   String sord = null;
                   String sidx = null;

                   if (request.getParameter("sidx") != null && !"".equalsIgnoreCase(request.getParameter("sidx")))
                   {
                       sord = request.getParameter("sord");
                       sidx = request.getParameter("sidx");
                   }

                   
                     List<TblProgrammeMaster> lstdata = null;
                    
                     String searchField = "", searchString = "", searchOper = "";
                      boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                    if(_search){
                        searchField = request.getParameter("searchField");
                        searchString = request.getParameter("searchString");
                        searchOper = request.getParameter("searchOper");
                       LOGGER.debug("searchField="+searchField+"searchstring = "+searchString+"searchoperator = "+searchOper);
                     lstdata =  progMasterSrBean.getProgrammeMasterBySearch(searchField,  searchOper ,searchString);
                    }else{
                     lstdata = progMasterSrBean.getAllProgrammeMaster(sord,sidx);
                 }
                    LOGGER.debug("Page Number====="+logUserId+" : "+pageNumber);
                    LOGGER.debug("Record Per Page===="+logUserId+" : "+recPerPage);
                    Collections.reverse(lstdata);
                    if(!lstdata.isEmpty()){
                        totalRecords =  lstdata.size();
                          if(totalRecords%recPerPage == 0){
                              totalPages = totalRecords/recPerPage;
                          }else{
                               totalPages = totalRecords/recPerPage+1;
                              }
                    }
                    LOGGER.debug("Total Pages=="+logUserId+" : "+totalPages);
                    response.setContentType("text/xml");
                    StringBuffer sb = new StringBuffer();
                    sb.append("<?xml version='1.0' encoding='utf-8'?><rows><page>"+pageNumber+"</page><total>"+totalPages+"</total><records>"+totalRecords+"</records>");
                    int j = ((pageNumber-1)*recPerPage) + 1;
                    int i =j;String link = "";
                    if(!lstdata.isEmpty()){

                         while(j<(i+recPerPage) ){
                             if(j<=totalRecords){
                             sb.append("<row>");
                             sb.append("<cell><![CDATA["+j+"]]></cell>");
                             sb.append("<cell><![CDATA["+lstdata.get(j-1).getProgCode()+"]]></cell>");
                             sb.append("<cell><![CDATA["+lstdata.get(j-1).getProgName()+"]]></cell>");
                             if(lstdata.get(j-1).getProgOwner()==null){
                                 sb.append("<cell><![CDATA[-]]></cell>");
                             }else{
                             sb.append("<cell><![CDATA["+lstdata.get(j-1).getProgOwner()+"]]></cell>");
                                 }
                             if(userId == 4){
                                 link ="<cell><a  href='ProgrammeMaster.jsp?action=edit&id="+lstdata.get(j-1).getProgId()+"'>Edit</a>&nbsp;|&nbsp;<a  href='ProgrammeMasterViewDetails.jsp?id="+lstdata.get(j-1).getProgId()+"'>View</a></cell>";
                             }else if (userId == 1){
                                 link ="<cell><a  href='ProgrammeMasterViewDetails.jsp?id="+lstdata.get(j-1).getProgId()+"'>View</a></cell>";
                             }
                             sb.append("<cell><![CDATA[" + link + "]]></cell></row>");
                             j++;
                             }else {break;}
                         }
                     }
                     sb.append("</rows>");
                     String url = sb.toString();
                     out.print(sb.toString());
                     LOGGER.debug("xml generation : "+logUserId+" : "+url);
                     LOGGER.debug("ProgramGridServlet : action : getting data : " + logUserId + " Ends");
            }else{
                response.sendRedirect("SessionTimedOut.jsp");
            }           
           
        } catch(Exception e){
            LOGGER.debug("ProgramGridServlet : "+logUserId+" : "+e);
        }finally {
            out.close();
        }
        LOGGER.debug("processRequest : "+logUserId+" Ends");
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
