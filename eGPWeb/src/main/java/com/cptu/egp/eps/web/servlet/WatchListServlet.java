/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblAppMaster;
import com.cptu.egp.eps.model.table.TblAppPackages;
import com.cptu.egp.eps.model.table.TblAppWatchList;
import com.cptu.egp.eps.model.table.TblCancelTenderRequest;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.model.table.TblTenderWatchList;
import com.cptu.egp.eps.service.serviceinterface.APPService;
import com.cptu.egp.eps.service.serviceinterface.TenderService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
//@WebServlet(name = "WatchListServlet", urlPatterns = {"/WatchListServlet"})
public class WatchListServlet extends HttpServlet
{

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private final APPService appService = (APPService) AppContext.getSpringBean("APPService");
    private final TenderService tenderService = (TenderService) AppContext.getSpringBean("TenderService");
    private static final Logger LOGGER = Logger.getLogger(WatchListServlet.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND  = "Ends";
    private String logUserId ="0";
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
            appService.setLogUserId(logUserId);
            tenderService.setUserId(logUserId);
            tenderService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
            appService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
        }
        LOGGER.debug("processRequest : "+logUserId+ LOGGERSTART);
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String type = request.getParameter("type");
            String funName = request.getParameter("funName");
            if(funName != null && "Cancel".equals(funName)){
                int tenderId = Integer.parseInt(request.getParameter("tenderId"));
                String remarks = request.getParameter("Comments");
                TblCancelTenderRequest tblCancelTenderRequest = new TblCancelTenderRequest();
                tblCancelTenderRequest.setTblTenderMaster(new TblTenderMaster(tenderId));
                tblCancelTenderRequest.setRemarks(remarks);
                tblCancelTenderRequest.setCreatedBy(Integer.parseInt(logUserId));
                tblCancelTenderRequest.setReqDateTime(new Date());
                tblCancelTenderRequest.setTenderStatus("Pending");
                tblCancelTenderRequest.setWorkflowStatus("Pending");
                tenderService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
                if(tenderService.insertCancelTender(tblCancelTenderRequest)){
                    out.print("cancel");
                }else{
                    out.print("not success");
                }
            }
            if (type != null) {
                if (funName != null) {
                    if (funName.equalsIgnoreCase("Add")) {
                        LOGGER.debug("processRequest : action : Add : "+logUserId+ LOGGERSTART);
                        int tenderId = Integer.parseInt(request.getParameter("tenderId"));
                        int userId = Integer.parseInt(request.getParameter("userId"));
                        int watchId = addToTenderWatchList(userId, tenderId);
                        out.print(watchId);
                        LOGGER.debug("processRequest : action : Add : "+logUserId+ LOGGEREND);
                    }
                    if (funName.equalsIgnoreCase("Remove")) {
                        LOGGER.debug("processRequest : action : Remove : "+logUserId+ LOGGERSTART);
                        int watchId = Integer.parseInt(request.getParameter("watchId"));
                        Boolean processFlag = removeFromTenderWatchList(watchId);
                        out.write(processFlag.toString());
                        LOGGER.debug("processRequest : action : Remove : "+logUserId+ LOGGEREND);
                    }
                }
            }
            else {
                if (funName != null) {
                    if (funName.equalsIgnoreCase("Add")) {
                        LOGGER.debug("processRequest : action : Add : "+logUserId+ LOGGERSTART);
                        int appId = Integer.parseInt(request.getParameter("appId"));
                        int pkgId = Integer.parseInt(request.getParameter("pkgId"));
                        int userId = Integer.parseInt(request.getParameter("userId"));
                        int watchId = addToWatchList(appId, pkgId, userId);
                        out.write(""+watchId);
                        LOGGER.debug("processRequest : action : Add : "+logUserId+ LOGGEREND);
                    }
                    if (funName.equalsIgnoreCase("Remove")) {
                        LOGGER.debug("processRequest : action : Remove : "+logUserId+ LOGGERSTART);
                        int watchId = Integer.parseInt(request.getParameter("watchId"));
                        Boolean processFlag = removeFromWatchList(watchId);
                        out.write(processFlag.toString());
                        LOGGER.debug("processRequest : action : Remove : "+logUserId+ LOGGEREND);
                    }
                }
            }
            out.flush();
        }catch(Exception ex){
             LOGGER.error("processRequest "+logUserId+" : "+ex.toString());
             ex.printStackTrace();
        }
        finally {
            out.close();
        }
        LOGGER.debug("processRequest : "+logUserId+ LOGGEREND);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

    private int addToWatchList(int appId, int pkgId, int userId)
    {
        LOGGER.debug("addToWatchList : "+logUserId+ LOGGERSTART);
        TblAppWatchList tblAppWatchList = new TblAppWatchList();
        tblAppWatchList.setCreationDate(new Date());
        tblAppWatchList.setTblAppMaster(new TblAppMaster(appId));
        tblAppWatchList.setTblAppPackages(new TblAppPackages(pkgId));
        tblAppWatchList.setTblLoginMaster(new TblLoginMaster(userId));
        LOGGER.debug("addToWatchList : "+logUserId+ LOGGEREND);
        return appService.addToWatchList(tblAppWatchList);
    }

    private boolean removeFromWatchList(int watchId)
    {
        LOGGER.debug("removeFromWatchList : "+logUserId+ LOGGERSTART);
        boolean flag = false;
        try{
            flag = appService.removeFromWatchList(watchId);
        }catch(Exception ex){
            LOGGER.error("removeFromWatchList "+logUserId+" : "+ex.toString());
    }
        LOGGER.debug("removeFromWatchList : "+logUserId+ LOGGEREND);
        return flag;
    }

    private int addToTenderWatchList(int userId, int tenderId)
    {
        LOGGER.debug("addToTenderWatchList : "+logUserId+ LOGGERSTART);
        TblTenderWatchList tblTenderWatchList = new TblTenderWatchList();
        tblTenderWatchList.setCreationDate(new Date());
        tblTenderWatchList.setUserId(userId);
        tblTenderWatchList.setTenderId(tenderId);
        LOGGER.debug("addToTenderWatchList : "+logUserId+ LOGGEREND);
        return tenderService.addToWatchList(tblTenderWatchList);
    }

    private boolean removeFromTenderWatchList(int watchListId)
    {
        LOGGER.debug("removeFromTenderWatchList : "+logUserId+ LOGGERSTART);
        boolean flag = false;
        try{
            flag = tenderService.removeFromWatchList(watchListId);
        }catch(Exception ex){
             LOGGER.error("removeFromTenderWatchList "+logUserId+" : "+ex.toString());
    }
        LOGGER.debug("removeFromTenderWatchList : "+logUserId+ LOGGEREND);
        return flag;
    }
}
