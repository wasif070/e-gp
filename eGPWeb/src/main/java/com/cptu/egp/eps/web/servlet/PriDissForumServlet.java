/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;


import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import java.io.File;
import com.cptu.egp.eps.model.table.TblPriTopicMaster;
import com.cptu.egp.eps.model.table.TblPriTopicReply;
import com.cptu.egp.eps.model.table.TblPriTopicReplyDocument;
import com.cptu.egp.eps.service.serviceimpl.ConsolodateService;
import com.cptu.egp.eps.service.serviceimpl.PrivateDissForumServiceImpl;
import com.cptu.egp.eps.service.serviceinterface.PrivateDissForumService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CheckExtension;
import com.cptu.egp.eps.web.utility.FileEncryptDecryptUtil;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Iterator;
import javax.servlet.ServletConfig;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
/**
 *
 * @author dixit
 */
public class PriDissForumServlet extends HttpServlet {

    private static final String TMP_DIR_PATH = "c:\\tmp";
    private File tmpDir;
    private static final String DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("PrivateDissForum");
    private File destinationDir;


    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        tmpDir = new File(TMP_DIR_PATH);
        if (!tmpDir.isDirectory()) {
            tmpDir.mkdir();
        }
    }
    
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private PrivateDissForumServiceImpl privateDissForumService = (PrivateDissForumServiceImpl) AppContext.getSpringBean("PrivateDissForumService");

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        //response.setContentType("text/html;charset=UTF-8");
       
        String logUserId = "0";
        HttpSession session = request.getSession();
        if (request.getSession().getAttribute("userId") != null)
        {
            logUserId = request.getSession().getAttribute("userId").toString();
        }
        privateDissForumService.setLogUserId(logUserId);

       // ConsolodateService servicee = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
        //int conIdd = servicee.getContractId(Integer.parseInt(request.getParameter("tenderId")));

            try
            {
                String tenderId = "";
                if(request.getParameter("tenderId")!=null)
                {
                    tenderId =request.getParameter("tenderId");

                }
                String functionName = request.getParameter("funName");
                if(functionName.equalsIgnoreCase("topicListing"))
                {
                    PrintWriter out = response.getWriter();
                    response.setContentType("text/xml;charset=UTF-8");
                    String rows = request.getParameter("rows");
                    String page = request.getParameter("page");
                    String sord = null;
                    String sidx = null;
                    if (request.getParameter("sidx") != null && !"".equalsIgnoreCase(request.getParameter("sidx"))) {
                        sord = request.getParameter("sord");
                        sidx = request.getParameter("sidx");
                    }
                    boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                    String searchField = "";
                    String searchString = "";
                    String searchOper = "";
                    int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));
                    int totalPages = 0;
                    int totalCount = 0;
                    List<TblPriTopicMaster> getPriTopicMaster = null;
                    List<TblPriTopicReply> getTReply = null;
                    List<TblPriTopicReply> getReply = null;
                    if (_search) {
                            searchField = request.getParameter("searchField");
                            searchString = request.getParameter("searchString");
                            searchOper = request.getParameter("searchOper");
                            getPriTopicMaster = privateDissForumService.searchTopicName(offset, Integer.parseInt(rows), Integer.parseInt(tenderId), searchField, searchString, searchOper, sord, sidx);
                            totalCount = (int) privateDissForumService.getSearchTopicCount(Integer.parseInt(tenderId), searchField, searchString, searchOper);
                    } else {
                        getPriTopicMaster = privateDissForumService.getTopicName(offset, Integer.parseInt(rows), Integer.parseInt(tenderId), sord, sidx);
                        totalCount = (int) privateDissForumService.getTopicCount(Integer.parseInt(tenderId));
                    }
                    if (totalCount > 0) {
                        if (totalCount % Integer.parseInt(rows) == 0) {
                            totalPages = totalCount / Integer.parseInt(rows);
                        } else {
                            totalPages = (totalCount / Integer.parseInt(rows)) + 1;
                        }
                    } else {
                        totalPages = 0;
                    }
                    int srNo = offset + 1;
                    int t = 0;
                    out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                    out.print("<rows>");
                    out.print("<page>" + request.getParameter("page") + "</page>");
                    out.print("<total>" + totalPages + "</total>");
                    out.print("<records>" + totalCount + "</records>");
                    for (int i = 0; i < getPriTopicMaster.size(); i++) {
                        String str1 = "<a href='PriDissForumTopicDetails.jsp?tenderId="+tenderId+"&topicId="+getPriTopicMaster.get(i).getTopicId()+"'>"+getPriTopicMaster.get(i).getTopic()+"</a>";
                        int totalTopicSize = 0,unreadTopicSize=0;
                        getTReply = privateDissForumService.getReply(getPriTopicMaster.get(i).getTopicId(), 1);
                        if(!getTReply.isEmpty())
                        {
                            totalTopicSize = getTReply.size();
                        }
                        getReply = privateDissForumService.getReply(getPriTopicMaster.get(i).getTopicId(), 0);
                        
                        if(!getReply.isEmpty())
                        {
                            unreadTopicSize = getReply.size();
                        }
                        String str2 = "<a href='PriDissForumTopicDetails.jsp?tenderId="+tenderId+"&topicId="+getPriTopicMaster.get(i).getTopicId()+"&type=unread'>("+unreadTopicSize+")</a> "
                                  + "| <a href='PriDissForumTopicDetails.jsp?tenderId="+tenderId+"&topicId="+getPriTopicMaster.get(i).getTopicId()+"&type=all'>("+totalTopicSize+")</a>";
                        out.print("<row id='" + i + "'>");
                        out.print("<cell>" + (i+1) + "</cell>");
                        out.print("<cell><![CDATA["+str1+"]]></cell>");
                        out.print("<cell><![CDATA["+str2+"]]></cell>");
                        out.print("</row>");
                    }
                    if(getPriTopicMaster.isEmpty()){
                    out.print("<row id='" + t + "'>");
                    out.print("<cell><![CDATA[No data found]]></cell>");
                    out.print("<cell><![CDATA[No data found]]></cell>");
                    out.print("<cell><![CDATA[No data found]]></cell>");
                    out.print("<cell><![CDATA[No data found]]></cell>");
                    out.print("</row>");
                    }
                    out.print("</rows>");
                    out.flush();
                    out.close();
                }

                else if(functionName.equalsIgnoreCase("postTopic"))
                {
                    File file = null;
                    String docSizeMsg = "";
                    boolean checkret = false;
                    boolean flag = false;
                    String documentBrief = "";
                    String fileName = "";
                    long fileSize = 0;
                    String queryString = "";
                    boolean isDocumentUpladed=false;
                    
                    TblPriTopicMaster tblPriTopicMaster=new TblPriTopicMaster();
                    tblPriTopicMaster.setPostedBy(Integer.parseInt(logUserId));
                    tblPriTopicMaster.setTenderId(Integer.parseInt(tenderId));
                    tblPriTopicMaster.setPostedDate(new Date());

                    TblPriTopicReplyDocument tblPriTopicReplyDocument =null;


                    String pageName = "resources/common/PriDissForumTopicList.jsp";
                   // response.setContentType("text/html");
                    boolean dis = false;
                    //---------------------- Code for upload file Starts
                    try
                    {
                        DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
                        /*
                         *Set the size threshold, above which content will be stored on disk.
                         */
                        fileItemFactory.setSizeThreshold(4 * 1024 * 1024); //1 MB
                    /*
                         * Set the temporary directory to store the uploaded files of size above threshold.
                         */
                        fileItemFactory.setRepository(tmpDir);

                        ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
                        /*
                         * Parse the request
                         */
                         boolean isMultipart = ServletFileUpload.isMultipartContent(request);
                      //  System.out.println("isMulitpart=="+isMultipart);

                        List items = uploadHandler.parseRequest(request);

                        Iterator itr = items.iterator();
                        //For Supporting Document
                        while (itr.hasNext())
                        {
                            FileItem item = (FileItem) itr.next();
                            //For Supporting Document
                        /*
                             * Handle Form Fields.
                             */
                            if (item.isFormField())
                            {
                                if (item.getFieldName().equals("txtTopic"))
                                {
                                    tblPriTopicMaster.setTopic(item.getString());
                                }
                                if (item.getFieldName().equals("txtarea_topicdetails"))
                                {
                                    tblPriTopicMaster.setDescription(item.getString());
                                }
                                if (item.getFieldName().equals("documentBrief"))
                                {
                                    if (item.getString() != null || item.getString().trim().length() > 0)
                                    {
                                        documentBrief = item.getString();
                                        HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                                        documentBrief = handleSpecialChar.handleSpecialChar(documentBrief);
                                    }
                                }
                            }
                            else
                            {
                                   if (item.getName().lastIndexOf("\\") != -1)
                                   {
                                        fileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1, item.getName().length());
                                   }
                                   else
                                   {
                                        fileName = item.getName();
                                   }

                                   if(fileName!= null && !fileName.equalsIgnoreCase(""))
                                   {
                                       isDocumentUpladed=true;

                                     //  System.out.println("filename="+fileName);

                                       tblPriTopicReplyDocument = new TblPriTopicReplyDocument();
                                       tblPriTopicReplyDocument.setUploadedBy(Integer.parseInt(logUserId));
                                       tblPriTopicReplyDocument.setTopicReplyId(0);
                                       tblPriTopicReplyDocument.setDocName(fileName);
                                       
                                       

                                   //fileName  = fileName.replaceAll(" ", "");
                                    String realPath = DESTINATION_DIR_PATH + tenderId+"_"+logUserId;

                                    destinationDir = new File(realPath);
                                    if (!destinationDir.isDirectory())
                                    {
                                        destinationDir.mkdir();
                                    }

                                    docSizeMsg = docSizeMsg(Integer.parseInt(request.getSession().getAttribute("userId").toString()));


                                    if (!docSizeMsg.equals("ok"))
                                    {
                                        
                                    }
                                    else
                                    {
                                        fileSize = item.getSize();
                                        checkret = checkExnAndSize(fileName, item.getSize(), "common");

                                        if (!checkret)
                                        {
                                            break;
                                        }
                                        else
                                        {
                                            file = new File(destinationDir, fileName);
                                            if (file.isFile()) {
                                                flag = true;
                                                break;

                                            }
                                            item.write(file);
                                            FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                                            fileSize = fileEncryptDecryptUtil.fileEncryptUtil(file, (int)fileSize);
                                            tblPriTopicReplyDocument.setDocSize(fileSize+"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (FileUploadException ex)
                    {
                       ex.printStackTrace();
                    }
                   catch (Exception ex)
                    {
                       ex.printStackTrace();
                    }
                    //---------------------- Code for upload file complited

                    if(isDocumentUpladed) // If file uploaded then check for file validation
                    {
                       // System.out.println("doucmet bried="+documentBrief);
                        tblPriTopicReplyDocument.setDocDescription(documentBrief);
                        if (!docSizeMsg.equals("ok"))
                        {
                            queryString = "?fq=" + docSizeMsg + "&tenderId="+tenderId;
                        }
                        else
                        {
                            if (flag)
                            {
                                docSizeMsg = "File already Exists";
                                queryString = "?fq=" + docSizeMsg + "&tenderId="+tenderId;
                                pageName = "resources/common/PriDissForumPostTopic.jsp";
                                response.sendRedirect(pageName + queryString);
                            }
                            else
                            {
                                if (!checkret) {
                                    flag=true;
                                    CheckExtension ext = new CheckExtension();
                                    TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("officer");
                                    queryString = "?fs=" + configurationMaster.getFileSize() + "&ft=" + configurationMaster.getAllowedExtension()+ "&tenderId="+tenderId;
                                    pageName = "resources/common/PriDissForumPostTopic.jsp";
                                    response.sendRedirect(pageName + queryString);
                                }
                                
                            }
                        }
                    }

                    if(!flag) // Uploaded file is correct.
                    {
                        try
                        {
                            pageName = "resources/common/PriDissForumTopicList.jsp";

                            PrivateDissForumServiceImpl privateDissForumService = (PrivateDissForumServiceImpl) AppContext.getSpringBean("PrivateDissForumService");
                            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"),request.getHeader("referer"));
                            privateDissForumService.setAuditTrail(objAuditTrail);
                            MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                            privateDissForumService.setMakeAuditTrailService(makeAuditTrailService);
                            privateDissForumService.setLogUserId(logUserId);
                           // privateDissForumService.setContractId(conIdd);
                            boolean success=privateDissForumService.postTopic(tblPriTopicMaster, tblPriTopicReplyDocument);
                          //  System.out.println("Retun="+success);
                            queryString = "?tenderId="+tenderId+"&retFlag="+success;
                            response.sendRedirect(pageName+queryString);

                        }
                        catch (Exception ex)
                        {
                            ex.printStackTrace();
                        }
                    }

                }// End of if(functionName.equalsIgnoreCase("postTopic"))
                else if(functionName.equalsIgnoreCase("postResponse"))
                {
                    File file = null;
                    String docSizeMsg = "";
                    boolean checkret = false;
                    boolean flag = false;
                    String documentBrief = "";
                    String fileName = "";
                    long fileSize = 0;
                    String queryString = "";
                    int topicId=0;
                    boolean isDocumentUpladed=false;
                    if(request.getParameter("topicId")!= null)
                    {
                        topicId=Integer.parseInt(request.getParameter("topicId"));
                    }
                    
                    TblPriTopicReply tblPriTopicReply=new TblPriTopicReply();
                    tblPriTopicReply.setPostedBy(Integer.parseInt(logUserId));
                    tblPriTopicReply.setPostedDate(new Date());
                    //tblPriTopicReply.setTblPriTopicMaster(new TblPriTopicMaster(topicId));
                    tblPriTopicReply.setTblPriTopicMaster(new TblPriTopicMaster(topicId,Integer.parseInt(tenderId)));
                    tblPriTopicReply.setHasSeen(0);


                    TblPriTopicReplyDocument tblPriTopicReplyDocument = null;


                    String pageName = "resources/common/PriDissForumTopicDetails.jsp?tenderId="+tenderId;
                   // response.setContentType("text/html");
                    boolean dis = false;


                    //---------------------- Code for upload file Starts
                    try
                    {
                        DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
                        /*
                         *Set the size threshold, above which content will be stored on disk.
                         */
                        fileItemFactory.setSizeThreshold(4 * 1024 * 1024); //1 MB
                    /*
                         * Set the temporary directory to store the uploaded files of size above threshold.
                         */
                        fileItemFactory.setRepository(tmpDir);

                        ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
                        /*
                         * Parse the request
                         */
                         boolean isMultipart = ServletFileUpload.isMultipartContent(request);
                      //  System.out.println("isMulitpart=="+isMultipart);

                        List items = uploadHandler.parseRequest(request);

                        Iterator itr = items.iterator();
                        //For Supporting Document
                        while (itr.hasNext())
                        {
                            FileItem item = (FileItem) itr.next();
                            //For Supporting Document
                        /*
                             * Handle Form Fields.
                             */
                            if (item.isFormField())
                            {
                                if (item.getFieldName().equals("txtarea_replydetails"))
                                {
                                    tblPriTopicReply.setDescription(item.getString());
                                }
                                if (item.getFieldName().equals("documentBrief"))
                                {
                                    if (item.getString() != null || item.getString().trim().length() > 0)
                                    {
                                        documentBrief = item.getString();
                                        HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                                        documentBrief = handleSpecialChar.handleSpecialChar(documentBrief);
                                    }
                                }
                            }
                            else
                            {
                                   
                                   if (item.getName().lastIndexOf("\\") != -1)
                                   {
                                        fileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1, item.getName().length());
                                   }
                                   else
                                   {
                                        fileName = item.getName();
                                   }
                                   if(fileName!= null && !fileName.equalsIgnoreCase(""))
                                   {
                                       isDocumentUpladed=true;

                                      // System.out.println("filename="+fileName);

                                       tblPriTopicReplyDocument = new TblPriTopicReplyDocument();
                                       tblPriTopicReplyDocument.setUploadedBy(Integer.parseInt(logUserId));
                                       tblPriTopicReplyDocument.setTopicId(0);
                                       tblPriTopicReplyDocument.setDocName(fileName);
                                       
                                       
                                       //fileName  = fileName.replaceAll(" ", "");
                                        String realPath = DESTINATION_DIR_PATH + tenderId+"_"+logUserId;

                                        destinationDir = new File(realPath);
                                        if (!destinationDir.isDirectory())
                                        {
                                            destinationDir.mkdir();
                                        }

                                        docSizeMsg = docSizeMsg(Integer.parseInt(request.getSession().getAttribute("userId").toString()));


                                        if (!docSizeMsg.equals("ok"))
                                        {

                                        }
                                        else
                                        {
                                            fileSize = item.getSize();
                                            checkret = checkExnAndSize(fileName, item.getSize(), "common");

                                            if (!checkret)
                                            {
                                                break;
                                            }
                                            else
                                            {
                                                file = new File(destinationDir, fileName);
                                                if (file.isFile()) {
                                                    flag = true;
                                                    break;

                                                }
                                                item.write(file);
                                                FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                                                fileSize = fileEncryptDecryptUtil.fileEncryptUtil(file, (int)fileSize);
                                                tblPriTopicReplyDocument.setDocSize(fileSize+"");
                                            }
                                        }
                                    }
                            }
                        }
                    }
                    catch (FileUploadException ex)
                    {
                       ex.printStackTrace();
                    }
                   catch (Exception ex)
                    {
                       ex.printStackTrace();
                    }
                    //---------------------- Code for upload file complited

                    if(isDocumentUpladed) // If file uploaded then check for file validation
                    {
                     //  System.out.println("documentdescription ="+documentBrief);
                       tblPriTopicReplyDocument.setDocDescription(documentBrief);
                        if (!docSizeMsg.equals("ok"))
                        {
                            queryString = "&fq=" + docSizeMsg + "&topicId="+topicId;
                        }
                        else
                        {
                            if (flag)
                            {
                                docSizeMsg = "File already Exists";
                                queryString = "&fq=" + docSizeMsg +"&topicId="+topicId;
                                pageName = "resources/common/PriDissForumPostReply.jsp?tenderId="+tenderId;
                                response.sendRedirect(pageName + queryString);
                            }
                            else
                            {
                                if (!checkret) {
                                    flag=true; 
                                    CheckExtension ext = new CheckExtension();
                                    TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("officer");
                                    queryString = "&fs=" + configurationMaster.getFileSize() + "&ft=" + configurationMaster.getAllowedExtension()+ "&topicId="+topicId;
                                    pageName = "resources/common/PriDissForumPostReply.jsp?tenderId="+tenderId;
                                    response.sendRedirect(pageName + queryString);
                                }
                            }
                        }

                    }

                    if(!flag) // Uploaded file is correct.
                    {
                        try
                        {
                            pageName = "resources/common/PriDissForumTopicDetails.jsp?tenderId="+tenderId;

                            PrivateDissForumServiceImpl privateDissForumService = (PrivateDissForumServiceImpl) AppContext.getSpringBean("PrivateDissForumService");
                            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer"));
                            privateDissForumService.setAuditTrail(objAuditTrail);
                            MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                            privateDissForumService.setMakeAuditTrailService(makeAuditTrailService);
                            privateDissForumService.setLogUserId(logUserId);
                           // privateDissForumService.setContractId(conIdd);
                            boolean success=privateDissForumService.postResponse(tblPriTopicReply, tblPriTopicReplyDocument);
                           // System.out.println("Retun="+success);
                            queryString = "&topicId="+topicId+"&retFlag="+success;
                            response.sendRedirect(pageName+queryString);
                        }
                        catch (Exception ex)
                        {
                            ex.printStackTrace();
                        }
                    }

                    

                }// End of if(functionName.equalsIgnoreCase("postResponse"))
                else if(functionName.equalsIgnoreCase("downloadDocument"))
                {
                    String uploadedBy="";
                    if(request.getParameter("uploadedBy")!= null)
                        uploadedBy=request.getParameter("uploadedBy");
                    File file = null;
                    file = new File(DESTINATION_DIR_PATH + tenderId+"_"+uploadedBy+ "\\" + request.getParameter("docName"));//request.getSession().getAttribute("userId")
                    InputStream fis = new FileInputStream(file);
                    byte[] buf = new byte[Integer.valueOf(request.getParameter("docSize"))];
                    int offset = 0;
                    int numRead = 0;
                    while ((offset < buf.length)
                            && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {

                        offset += numRead;

                    }
                    fis.close();
                    FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                    buf = fileEncryptDecryptUtil.fileDecryptUtil(buf);
                    response.setContentType("application/octet-stream");
                    response.setHeader("Content-Disposition", "attachment;filename=\"" + request.getParameter("docName") + "\"");
                    ServletOutputStream outputStream = response.getOutputStream();
                    outputStream.write(buf);
                    outputStream.flush();
                    outputStream.close();
                }
                
            }
            catch (Exception ex)
            {
               ex.printStackTrace();
               //System.out.println(ex.toString());
            }
            finally
            {
                //out.close();
            }
        

    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

     /*For check Extension and size*/
    public boolean checkExnAndSize(String extn, long size, String userType) {
        boolean chextn = false;
        float fsize = 0.0f;
        float dsize = 0.0f;
        int j = extn.lastIndexOf('.');
        String lst = extn.substring(j + 1);
        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        String str = configurationMaster.getAllowedExtension();
        String[] str1 = str.split(",");
        for (int i = 0; i < str1.length; i++) {
            if (str1[i].trim().equalsIgnoreCase(lst)) {
                chextn = true;
            }
        }
        if (chextn) {
            fsize = size / (1024 * 1024);
            dsize = configurationMaster.getFileSize();
            if (dsize > fsize) {
                chextn = true;
            } else {
                chextn = false;
            }
        }
        return chextn;
    }
/*For doc size msg*/
    public String docSizeMsg(int userId) 
    {

        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
        return userRegisterService.docSizeCheck(userId);
    }
/*For deleting file*/
    public boolean deleteFile(String filePath)
    {

        File f = new File(filePath);
        if (f.delete()) {
            return true;
        } else {
            return false;
        }
    }
}
