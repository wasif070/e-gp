/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.utility;

import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.service.serviceimpl.CheckExtensionService;

/**
 *
 * @author test
 */
public class CheckExtension {

    CheckExtensionService  checkExtensionService =
             (CheckExtensionService)AppContext.getSpringBean("CheckExtensionService");

    /**
     * get details from Tbl_ConfigurationMaster
     * @param userType
     * @return configuration of particular user
     */
    public TblConfigurationMaster getConfigurationMaster(String userType){

       TblConfigurationMaster tblConfigurationMaster = checkExtensionService.getConfigurationMaster(userType);
       return tblConfigurationMaster;
    }
}
