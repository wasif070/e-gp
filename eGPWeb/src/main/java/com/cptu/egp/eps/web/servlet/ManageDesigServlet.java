/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblDesignationMaster;
import com.cptu.egp.eps.web.servicebean.ManageDesigSrBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class ManageDesigServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
       try {
              if (request.getParameter("action").equals("fetchData")) {
                response.setContentType("text/xml;charset=UTF-8");
                System.out.println(">" + request.getQueryString());
                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String sord = request.getParameter("sord");
                String sidx = request.getParameter("sidx");
                String type=request.getParameter("userType");
//                if (sidx.equals("")) {
//                    sidx = "designation";
//                }
                String userTypeId="";

                //dcBean.setColName("");
                ManageDesigSrBean manageDesigSrBean=new ManageDesigSrBean();
                manageDesigSrBean.setSearch(_search);
                manageDesigSrBean.setLimit(Integer.parseInt(rows));
                int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));

                manageDesigSrBean.setOffset(offset);
                manageDesigSrBean.setSortOrder(sord);
                manageDesigSrBean.setSortCol(sidx);


                System.out.println("queryString:" + request.getQueryString());

                HttpSession session=request.getSession();
                Object objUserTypeId=session.getAttribute("userTypeId");
                userTypeId = objUserTypeId.toString();
                int userId=0;
                if(!objUserTypeId.toString().equals("1"))
                    userId=Integer.parseInt(session.getAttribute("userId").toString());
                
                List<Object []> desigMasters = manageDesigSrBean.getDesignationList(userId,objUserTypeId.toString());
                //System.out.println("designationMasterList size:" + desigMasters.size());
                int totalPages = 0;
                long totalCount = manageDesigSrBean.getAllCountOfDesignation(userId);
                System.out.println("totalCount : "+totalCount);
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                        totalPages = (int) (totalCount / Integer.parseInt(request.getParameter("rows")));
                    } else {
                        totalPages = (int) ((totalCount / Integer.parseInt(request.getParameter("rows"))) + 1);
                    }
                } else {
                    totalPages = 0;
                }

                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + desigMasters.size() + "</records>");
                int pg=0;
                if(page!=null && !page.isEmpty())
                    pg=Integer.parseInt(page) - 1;
                // be sure to put text data in CDATA
                 int j =0;
                int no = Integer.parseInt(request.getParameter("page"));
                if(no == 1){
                    j = 1;
                }else{
                    if(Integer.parseInt(rows) == 30){
                        j = ((no-1)*30)+1;
                    }else if(Integer.parseInt(rows) == 20){
                        j = ((no-1)*20)+1;
                    }
                    else{
                        j = ((no-1)*10)+1;
                    }
                }
                for (int i = 0; i < desigMasters.size(); i++) {
                   
                    out.print("<row id='" + desigMasters.get(i)[3] + "'>");
                    //out.print("<cell>" + ((pg*10)+(i+1)) + "</cell>");
                    out.print("<cell><![CDATA["+j+"]]></cell>");
                    out.print("<cell><![CDATA[" + desigMasters.get(i)[0] + "]]></cell>");
                    out.print("<cell><![CDATA[" + desigMasters.get(i)[1] + "]]></cell>");
                   // out.print("<cell><![CDATA[" +desigMasters.get(i)[2]+" - "+desigMasters.get(i)[4]+"]]></cell>");
                    String editLink=(!userTypeId.equalsIgnoreCase("1"))?"<a href=\"EditDesignation.jsp?designationId="+ desigMasters.get(i)[3]+"\">Edit</a> | " : "" ;
                    String viewLink="<a href=\"ViewDesignation.jsp?designationId="+ desigMasters.get(i)[3]+"&action=view\">View</a>";
                    out.print("<cell><![CDATA[" + editLink + viewLink+"]]></cell>");
                    out.print("</row>");
                    j++;
                }
                out.print("</rows>");
            }
        } catch (Exception ex) {
            System.out.println("Exception :" + ex);
        } finally {
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
