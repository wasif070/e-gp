/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.model.table.TblEvalTscDocs;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.service.serviceimpl.EvaluationService;
import com.cptu.egp.eps.service.serviceimpl.TemplateDocumentsService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CheckExtension;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.servlet.ServletOutputStream;
/**
 *
 * @author shreyansh
 */
public class DocUploadForLotAndPkg extends HttpServlet {

    private static final String TMP_DIR_PATH = "c:\\tmp";
    private File tmpDir;
    private static final String DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("uploadReport");
    private File destinationDir;
    private String logUserId = "0";
    final Logger LOGGER = Logger.getLogger(DocUploadForLotAndPkg.class);

    @Override
    public void init(ServletConfig config) throws ServletException {
        LOGGER.debug("init: " + logUserId + "starts");
        super.init(config);
        tmpDir = new File(TMP_DIR_PATH);
        if (!tmpDir.isDirectory()) {
            tmpDir.mkdir();
        }
        LOGGER.debug("init: " + logUserId + "Ends");
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String realPath = "";
        response.setContentType("text/html");
        String action = "";
        HttpSession session = request.getSession();
        String userIdSession = request.getSession().getAttribute("userId").toString();
        EvaluationService service = (EvaluationService) AppContext.getSpringBean("EvaluationService");
        String pkgLotId = "";
        short pkgId = 0;
        if (request.getParameter("pkgId") != null) {
            pkgId = Short.parseShort(request.getParameter("pkgId"));
        }
        if (request.getParameter("action") != null) {
            action = request.getParameter("action");
        }
        try {
            HttpSession httpSession = request.getSession();
            if (httpSession.getAttribute("userId") == null) {
                response.sendRedirect("SessionTimedOut.jsp");
            } else {
                if (action.equals("removeDoc")) {
                    LOGGER.debug("processRequest : action : " + action + logUserId + "starts");

                    TemplateDocumentsService templateDocument = (TemplateDocumentsService) AppContext.getSpringBean("TemplateDocumentService");
                    int docId = 0;
                    short tenderId = 0;
                    String docname = "";

                    if (request.getParameter("tenderId") != null) {
                        tenderId = Short.parseShort(request.getParameter("tenderId"));
                    }


                    if (request.getParameter("Id") != null) {
                        docId = Integer.parseInt(request.getParameter("Id"));
                    }
                    if (request.getParameter("docName") != null) {
                        docname = request.getParameter("docName");

                    }
                    String fileName="";
                    if(pkgId==0){
                        fileName = DESTINATION_DIR_PATH + tenderId + "\\" + docname;

                    }else{
                        fileName = DESTINATION_DIR_PATH + tenderId + "\\" + pkgId + "\\" + docname;
                    }

                    System.out.println("filename "+fileName);
                    if (deleteFile(fileName)) {
                        service.deleteDoc(tenderId, docId);
                        // For Fixing delete doc issue
                        LOGGER.debug("processRequest : action : " + action + logUserId + "Ends");
                        if(request.getParameter("doc")!=null){
                            response.sendRedirect("officer/TSCUploadReport.jsp?tenderid="+tenderId+"&st=cl&msg=success");
                        }else{
                             response.sendRedirect("officer/UploadReport.jsp?tenderid=" + tenderId + "&st=cl&pckLotId=" + pkgId + "&msg=success");
                        }
                    } else {
                        response.sendRedirect("officer/UploadReport.jsp?tenderid=" + tenderId + "&st=cl&pckLotId=" + pkgId + "&msg=0");
                    }
                } else if (action.equals("downloadDoc")) {
                    LOGGER.debug("processRequest : action : " + action + logUserId + "Starts");
                     // Changes has been done for remove doc package id is checked while download is not checked
                    String strPath = DESTINATION_DIR_PATH + request.getParameter("tenderId") + "\\";
                    ServletOutputStream stream = null;
                    BufferedInputStream buf = null;
                    if(pkgId> 0){
                        strPath = strPath+pkgId+"\\";
                    }
                    String fileName=strPath+request.getParameter("docName");
                    File doc = new File(fileName);
                    response.reset();
                    stream = response.getOutputStream();
                    response.setContentType("application/octet-stream");
                    response.addHeader("Content-Disposition", "attachment; filename=\""
                      + request.getParameter("docName")+"\"");
                    response.setContentLength((int) doc.length());
                    FileInputStream input = new FileInputStream(doc);
                    buf = new BufferedInputStream(input);
                    int readBytes = 0;
                    while ((readBytes = buf.read()) != -1)
                    {
                        stream.write(readBytes);
                    }
                    LOGGER.debug("processRequest : action : " + action + logUserId + "Ends");
                } else {
                    LOGGER.debug("processRequest : action : " + action + logUserId + "Starts");

                    short tenderId = 0;
                    String fileName = "";
                    long fileSize = 0;
                    boolean isExtNSizeOK = false;
                    String documentBrief = "";
                    DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
                    /*
                     *Set the size threshold, above which content will be stored on disk.
                     */
                    fileItemFactory.setSizeThreshold(1 * 1024 * 1024); //1 MB
                        /*
                     * Set the temporary directory to store the uploaded files of size above threshold.
                     */
                    fileItemFactory.setRepository(tmpDir);
                    ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
                    try {
                        /*
                         * Parse the request
                         */
                        List items = uploadHandler.parseRequest(request);
                        Iterator itr = items.iterator();
                        //For Supporting Document
                        while (itr.hasNext()) {
                            FileItem item = (FileItem) itr.next();
                            //For Supporting Document
                            /*
                             * Handle Form Fields.
                             */
                            if (item.isFormField()) {
                                LOGGER.debug("File Name = " + item.getFieldName() + ", Value = " + item.getString());
                                if (item.getFieldName().equals("pkgLotId")) {
                                    pkgLotId = item.getString();
                                } else if (item.getFieldName().equals("tenderId")) {
                                    tenderId = Short.parseShort(item.getString());
                                } else if (item.getFieldName().equals("documentBrief")) {
                                    documentBrief = item.getString();
                                } else if (item.getFieldName().equals("")) {
                                }

                            } else {
                                //Handle Uploaded files.
                                LOGGER.debug("Field Name = " + item.getFieldName()
                                        + ", File Name = " + item.getName()
                                        + ", Content type = " + item.getContentType()
                                        + ", File Size = " + item.getSize());
                                /*
                                 * Write file to the ultimate location.
                                 */
                                if (item.getName().lastIndexOf("\\") != -1) {
                                    fileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1, item.getName().length());
                                } else {
                                    fileName = item.getName();
                                }
                                //fileName  = fileName.replaceAll(" ", "");
                                fileSize = item.getSize();
                                // Changes has been done for remove doc package id is checked while upload is not checked
                                if(pkgId==0){
                                        realPath = DESTINATION_DIR_PATH + tenderId;
                                }else{
                                        fileName = DESTINATION_DIR_PATH + tenderId + "\\" + pkgLotId;
                                }
                                //realPath = DESTINATION_DIR_PATH + tenderId + "\\" + pkgLotId;
                                // Changes has been complited remove doc
                                destinationDir = new File(realPath);
                                if (!destinationDir.isDirectory()) {
                                    destinationDir.mkdirs();
                                }

                                isExtNSizeOK = checkExnAndSize(fileName, item.getSize(), "egpadmin");
                                if (!isExtNSizeOK) {
                                    break;
                                }

                                File file = new File(destinationDir, fileName);
                                item.write(file);
                            }
                        }
                        TblEvalTscDocs docs = new TblEvalTscDocs();
                        docs.setDocSize(fileSize + "");
                        docs.setDocumentDesc(documentBrief);
                        docs.setPkgLotId(Integer.parseInt(pkgLotId));
                        docs.setDocumentName(fileName);
                        docs.setUploadedBy(Integer.parseInt(userIdSession));
                        docs.setUploadedDt(new Date());
                        docs.setTblTenderMaster(new TblTenderMaster(tenderId));
                        if(isExtNSizeOK){
                            service.insertTblEvalTscDocs(docs);
                        }
                        if (isExtNSizeOK) {
                            response.sendRedirect("officer/UploadReport.jsp?tenderid=" + tenderId + "&st=cl&pckLotId=" + pkgLotId+"&msg=upload");
                        } else {
                            CheckExtension ext = new CheckExtension();
                            TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("egpadmin");
                            response.sendRedirect("officer/UploadReport.jsp?tenderid=" + tenderId + "&st=cl&pckLotId=" + pkgLotId + "&fs=" + configurationMaster.getFileSize() + "&ft=" + configurationMaster.getAllowedExtension());
                            if (ext != null) {
                                ext = null;
                            }
                            if (configurationMaster != null) {
                                configurationMaster = null;
                            }
                        }
                    } catch (FileUploadException ex) {
                        LOGGER.debug("processRequest : " + logUserId + ":" + ex);
                    } catch (Exception ex) {
                        LOGGER.debug("processRequest : " + logUserId + ":" + ex);
                    }
                }
                LOGGER.debug("processRequest : action : " + action + logUserId + "Ends");
            }
        } finally {
        }
    }

    public boolean checkExnAndSize(String extn, long size, String userType) {
        LOGGER.debug("checkExnAndSize Starts");
        boolean chextn = false;
        float fsize = 0.0f;
        float dsize = 0.0f;
        int j = extn.lastIndexOf('.');
        String lst = extn.substring(j + 1);

        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        String str = configurationMaster.getAllowedExtension();
        String[] str1 = str.split(",");
        for (int i = 0; i < str1.length; i++) {
            if (str1[i].trim().equalsIgnoreCase(lst)) {
                chextn = true;
            }
        }
        if (chextn) {
            fsize = size / (1024 * 1024);
            dsize = configurationMaster.getFileSize();
            if (dsize > fsize) {
                chextn = true;
            } else {
                chextn = false;
            }
        }
        LOGGER.debug("checkExnAndSize Ends");
        return chextn;
    }

    public boolean deleteFile(String filePath) {
        File f = new File(filePath);
        if (f.delete()) {
            return true;
        } else {
            return false;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
