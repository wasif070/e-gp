/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearch;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.model.table.TblCmsPrDetail;
import com.cptu.egp.eps.model.table.TblCmsPrMaster;
import com.cptu.egp.eps.model.table.TblCmsRoitems;
import com.cptu.egp.eps.model.table.TblCmsRomap;
import com.cptu.egp.eps.model.table.TblCmsRomaster;
import com.cptu.egp.eps.model.table.TblCmsTrackVariation;
import com.cptu.egp.eps.model.table.TblCmsVariContractVal;
import com.cptu.egp.eps.model.table.TblCmsVariationOrder;
import com.cptu.egp.eps.model.table.TblCmsWpDetail;
import com.cptu.egp.eps.model.table.TblCmsWpDetailHistory;
import com.cptu.egp.eps.model.table.TblCmsWpMaster;
import com.cptu.egp.eps.model.table.TblCmsWptenderBoqmap;
import com.cptu.egp.eps.model.table.TblContractSign;
import com.cptu.egp.eps.model.table.TblEvalRoundMaster;
import com.cptu.egp.eps.model.table.TblTenderCells;
import com.cptu.egp.eps.model.table.TblTenderLotSecurity;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.model.table.TblTenderPerfSecurity;
import com.cptu.egp.eps.service.serviceimpl.ConsolodateService;
import com.cptu.egp.eps.service.serviceimpl.RepeatOrderService;
import com.cptu.egp.eps.service.serviceimpl.TblTenderPerformanceSec;
import com.cptu.egp.eps.service.serviceinterface.AccPaymentService;
import com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService;
import com.cptu.egp.eps.service.serviceinterface.CmsContractTerminationService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.cptu.egp.eps.web.servicebean.TenderTablesSrBean;
import com.cptu.egp.eps.web.utility.AppExceptionHandler;
import com.cptu.egp.eps.web.utility.CommonUtils;
import com.cptu.egp.eps.web.utility.DateUtils;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 *
 * @author shreyansh
 */
public class ConsolidateServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
    private CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
    private ConsolodateService c_ConsSrv = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
    private UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    private AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
    private final static CmsContractTerminationService cmsContractTerminationService =
            (CmsContractTerminationService) AppContext.getSpringBean("CmsContractTerminationService");
    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
   private SPCommonSearch spCommonSearch = (SPCommonSearch) AppContext.getSpringBean("SPCommonSearch");

    /*
     * Action
     * 'dumpform' -
     * ''
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String action = request.getParameter("action");
        HttpSession session = request.getSession();
        Object user = session.getAttribute("userId");
        Object userType = session.getAttribute("userTypeId");
        ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
        TblCmsWpMaster cmsWpMaster = new TblCmsWpMaster();
        List<TblCmsWpMaster> masters = new ArrayList<TblCmsWpMaster>();
        List<TblCmsWptenderBoqmap> wptenderBoqmaps = new ArrayList<TblCmsWptenderBoqmap>();
        List<TblCmsWpDetailHistory> list1 = new ArrayList<TblCmsWpDetailHistory>();
        List<TblCmsPrDetail> prDetails = new ArrayList<TblCmsPrDetail>();
        List<TblCmsPrMaster> prMasters = new ArrayList<TblCmsPrMaster>();
        List<TblCmsWpDetail> tcwds = new ArrayList<TblCmsWpDetail>();
        TenderTablesSrBean beanCommon = new TenderTablesSrBean();
        String tenderType = beanCommon.getTenderType(Integer.parseInt(request.getParameter("tenderId")));
        String userId = null;
        String userTypeId = null;
        if (user == null) {
            userId = "";
        } else {

            userId = user.toString();
        }
        if (userType == null) {
            userTypeId = "";
        } else {

            userTypeId = userType.toString();
        }
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        cmsConfigDateService.setLogUserId(userId);
        commonservice.setUserId(userId);
        c_ConsSrv.setLogUserId(userId);
        registerService.setUserId(userId);
        accPaymentService.setLogUserId(userId);
        cmsContractTerminationService.setLogUserId(userId);
        String procnature = commonService.getProcNature(request.getParameter("tenderId")).toString();
        int conId = c_ConsSrv.getContractId(Integer.parseInt(request.getParameter("tenderId")));
        if ("consolidate".equalsIgnoreCase(action)) {
            try {
                String count = request.getParameter("count_" + request.getParameter("lotId"));
                if (Integer.parseInt(count) > 1) {
                    int j = 0;
                    boolean allow = true;
                    List<Object[]> list = null;
                    for (int i = 0; i < Integer.parseInt(count); i++) {

                        String check = request.getParameter("boqcheck_" + i);

                        if (check != null) {

                            String tenderFormId = request.getParameter("tenderformid_" + i);
                            List<Object> counttable = service.countTable(tenderFormId);
                            if (counttable.size() > 1) {
                                allow = false;
                                break;
                            } else {
                                if (j == 0) {
                                    list = service.getFirstObject(counttable.get(0).toString());
                                    j++;
                                } else {

                                    List<Object[]> listsecond = service.getFirstObject(counttable.get(0).toString());
                                    if (list.size() != listsecond.size()) {
                                        allow = false;
                                        break;
                                    } else {
                                        boolean tobreak = true;
                                        for (int ii = 0; ii < list.size(); ii++) {

                                            if (!list.get(ii)[0].toString().equals(listsecond.get(ii)[0].toString())) {
                                                allow = false;
                                                tobreak = false;
                                                break;
                                            } else if (!list.get(ii)[1].toString().equals(listsecond.get(ii)[1].toString())) {
                                                allow = false;
                                                tobreak = false;
                                                break;
                                            } else {
                                                allow = true;
                                            }
                                        }
                                        if (!tobreak) {
                                            break;
                                        }
                                    }

                                }

                            }
                        }
                    }
                    if (allow) {
                        cmsWpMaster.setCreatedBy(Integer.parseInt(userId));
                        cmsWpMaster.setCreatedDate(new java.util.Date());
                        cmsWpMaster.setWpLotId(Integer.parseInt(request.getParameter("lotId")));
                        cmsWpMaster.setWpName("Consolidate");
                        cmsWpMaster.setIsDateEdited("no");
                        cmsWpMaster.setIsRepeatOrder("no");

                        masters.add(cmsWpMaster);
                        boolean flag = service.AddToWpMaster(masters);
                        List<Object> wpId = service.getWpId(Integer.parseInt(request.getParameter("lotId")));
                        //if (wpId != null && !wpId.isEmpty()) {
                        // for (Object o : wpId) {
                        for (int i = 0; i < Integer.parseInt(count); i++) {
                            String check = request.getParameter("boqcheck_" + i);
                            if (check != null) {
                                TblCmsWptenderBoqmap tcwb = new TblCmsWptenderBoqmap();
                                tcwb.setWpTenderFormId(Integer.parseInt(request.getParameter("tenderformid_" + i)));
                                tcwb.setTblCmsWpMaster(new TblCmsWpMaster(cmsWpMaster.getWpId()));
                                wptenderBoqmaps.add(tcwb);
                            }
                            // }
                            //}
                        }

                        boolean flag1 = service.AddToWpChild(wptenderBoqmaps);
                        if (flag && flag1) {
                            response.sendRedirect("officer/NOA.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=con");
                        }
                    } else {
                        response.sendRedirect("officer/NOA.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=tt");
                    }

                } else {
                    cmsWpMaster.setCreatedBy(Integer.parseInt(userId));
                    cmsWpMaster.setCreatedDate(new java.util.Date());
                    cmsWpMaster.setWpLotId(Integer.parseInt(request.getParameter("lotId")));
                    cmsWpMaster.setWpName("Consolidate");
                    cmsWpMaster.setIsDateEdited("no");
                    cmsWpMaster.setIsRepeatOrder("no");
                    masters.add(cmsWpMaster);
                    boolean flag = service.AddToWpMaster(masters);
                    List<Object> wpId = service.getWpId(Integer.parseInt(request.getParameter("lotId")));
                    if (wpId != null && !wpId.isEmpty()) {
                        for (Object o : wpId) {
                            for (int i = 0; i < Integer.parseInt(count); i++) {
                                String check = request.getParameter("boqcheck_" + i);

                                if (check != null) {
                                    if (count.equals("1") && wpId.size() == 1) {
                                        TblCmsWptenderBoqmap tcwb = new TblCmsWptenderBoqmap();
                                        tcwb.setWpTenderFormId(Integer.parseInt(request.getParameter("tenderformid_" + i)));
                                        tcwb.setTblCmsWpMaster(new TblCmsWpMaster(cmsWpMaster.getWpId()));
                                        wptenderBoqmaps.add(tcwb);
                                    } else {
                                        // changed by shreyansh to solve duplicate boq form problem
                                        if ((Integer) o != cmsWpMaster.getWpId()) {
                                            TblCmsWptenderBoqmap tcwb = new TblCmsWptenderBoqmap();
                                            tcwb.setWpTenderFormId(Integer.parseInt(request.getParameter("tenderformid_" + i)));
                                            tcwb.setTblCmsWpMaster(new TblCmsWpMaster(cmsWpMaster.getWpId()));
                                            wptenderBoqmaps.add(tcwb);
                                        }
                                    }

                                }
                            }
                        }
                    }
                    boolean flag1 = service.AddToWpChild(wptenderBoqmaps);
                    if (flag && flag1) {
                        response.sendRedirect("officer/NOA.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=con");
                    }
                }


            } catch (Exception e) {
                AppExceptionHandler handler = new AppExceptionHandler();
                handler.stack2string(e);
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), Integer.parseInt(request.getParameter("tenderId")), "tenderId", EgpModule.Noa.getName(), "Consolidation by PE", "");
            }
        } else if ("configdate".equalsIgnoreCase(action)) {
            CmsConfigDateService ccds = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
            boolean flag = ccds.getConfigDatesdatabyPassingLotId(Integer.parseInt(request.getParameter("lotId")));

        } else if ("saveeditdates".equalsIgnoreCase(action)) {
            String listcount = request.getParameter("listcount");
            String cms = request.getParameter("cms");
            String lotId = request.getParameter("lotId");
            int detailid = 0;
            List<Object> getMaxCount = service.getWPDetailHistMaxCount(Integer.parseInt(request.getParameter("wpId")), "originalupdate");
            String strCount = "";
            int hCount = 0;
            try {
                if (getMaxCount.get(0) != null) {
                    strCount = getMaxCount.get(0).toString();
                    if ("0".equalsIgnoreCase(strCount)) {
                        hCount++;
                    } else {
                        hCount = Integer.parseInt(strCount);
                        hCount++;
                    }
                } else {
                    hCount++;
                }
                List<TblCmsWpDetail> tcwdhs = null;
                if ("works".equalsIgnoreCase(procnature)) {
                    tcwdhs = service.getDetailwp(request.getParameter("wpId"));
                } else {
                    tcwdhs = service.getAllDetail(request.getParameter("wpId"));
                }

                for (int i = 0; i < Integer.parseInt(listcount); i++) {

                    detailid = Integer.parseInt(request.getParameter("detailid_" + i));
                    List<Object[]> dates = service.checkForDateForWorks(detailid);
                    if (!dates.isEmpty() && dates != null) {
                        if (!DateUtils.gridDateToStr((Date) dates.get(0)[0]).split(" ")[0].equalsIgnoreCase(request.getParameter("txtsdate_" + i)) || !dates.get(0)[1].toString().equalsIgnoreCase(request.getParameter("noofdays_" + i))) {
                            service.updateTableForWorks(detailid, DateUtils.convertStringtoDate(request.getParameter("txtsdate_" + i).trim(), "dd-MMM-yyyy"), DateUtils.convertStringtoDate(request.getParameter("txtedate_" + i).trim(), "dd-MMM-yyyy"), Integer.parseInt(request.getParameter("noofdays_" + i).trim()));
                            service.updateTableForWorksForMaster(request.getParameter("wpId"));
                            TblCmsWpDetailHistory wpDetailHistory = new TblCmsWpDetailHistory();
                            if ("3".equalsIgnoreCase(userTypeId)) {
                                wpDetailHistory.setAmendmentFlag("originalupdate");
                                wpDetailHistory.setHistoryCount(hCount);
                            } else {
                                wpDetailHistory.setAmendmentFlag("originalbytenderer");
                                wpDetailHistory.setHistoryCount(0);
                            }
                            wpDetailHistory.setWpId(tcwdhs.get(i).getTblCmsWpMaster().getWpId());
                            wpDetailHistory.setWpRowId(tcwdhs.get(i).getWpRowId());
                            wpDetailHistory.setWpRate(tcwdhs.get(i).getWpRate());
                            wpDetailHistory.setWpNoOfDays(Integer.parseInt(request.getParameter("noofdays_" + i)));
                            wpDetailHistory.setWpStartDate(DateUtils.convertStringtoDate(request.getParameter("txtsdate_" + i), "dd-MMM-yyyy"));
                            wpDetailHistory.setWpEndDate(DateUtils.convertStringtoDate(request.getParameter("txtedate_" + i), "dd-MMM-yyyy"));
                            wpDetailHistory.setWpQty(tcwdhs.get(i).getWpQty());
                            wpDetailHistory.setDetail(new TblCmsWpDetail(tcwdhs.get(i).getWpDetailId()));
                            wpDetailHistory.setActContractDtHistId(0);
                            wpDetailHistory.setTenderTableId(tcwdhs.get(i).getTenderTableId());
                            wpDetailHistory.setCreatedDate(new java.sql.Date(new java.util.Date().getTime()));
                            list1.add(wpDetailHistory);
                            boolean indetailHistory = service.addToDetailHistory(list1);
                        }

                    }

                }
                String wpId = request.getParameter("wpId");
                service.sendToPEInWorks(wpId);
                if (request.getParameter("pe") != null) {
                    //response.sendRedirect("officer/DeliverySchedule.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=sent");
                    response.sendRedirect("officer/DeliveryScheduleUploadDoc.jsp?keyId=" + wpId + "&tenderId=" + request.getParameter("tenderId") + "&lotId=" + lotId + "&wpId=" + wpId + "&docx=Workprogram&msg=sent");
                } else {
                    response.sendRedirect("tenderer/CMS.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=sent");
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "ContractId", EgpModule.Work_Program.getName(), "Edit Dates/Days", "");
            }
        } else if ("variationordertenside".equalsIgnoreCase(action)) {
            try {
                int wpId = Integer.parseInt(request.getParameter("wpId"));
                List<TblCmsVariationOrder> list = service.getMaxVarOrdIdForAcceptVari(Integer.parseInt(request.getParameter("wpId")));
                if (list != null && !list.isEmpty()) {
                    List<TblCmsTrackVariation> vardet = service.getVariOrder(list.get(0).getVariOrdId() + "");
                    if (vardet != null && !vardet.isEmpty()) {
                        for (TblCmsTrackVariation details : vardet) {
                            BigDecimal qty = details.getQty();
                            if ("I".equalsIgnoreCase(details.getFlag())) {
                                TblCmsWpDetail wpDetail = new TblCmsWpDetail();
                                wpDetail.setAmendmentFlag("variationI");
                                wpDetail.setCreatedBy(Integer.parseInt(userId));
                                wpDetail.setGroupId(details.getGroupname());
                                wpDetail.setItemInvGenQty(new BigDecimal(0));
                                wpDetail.setItemInvStatus("pending");
                                wpDetail.setIsRepeatOrder("no");
                                wpDetail.setTblCmsWpMaster(new TblCmsWpMaster(Integer.parseInt(request.getParameter("wpId"))));
                                wpDetail.setTenderTableId(details.getTenderTableId());
                                wpDetail.setUserTypeId(Integer.parseInt(userTypeId));
                                wpDetail.setWpEndDate(details.getEndDate());
                                wpDetail.setWpDescription(details.getDescription());
                                wpDetail.setWpItemStatus("pending");
                                wpDetail.setWpNoOfDays(0);
                                wpDetail.setWpQty(details.getQty());
                                wpDetail.setWpRate(details.getRate());
                                wpDetail.setWpRowId(details.getRowId());
                                wpDetail.setWpSrNo(details.getSrno());
                                wpDetail.setWpStartDate(details.getStartDate());
                                wpDetail.setWpUom(details.getUom());
                                wpDetail.setWpQualityCheckReq("no");
                                tcwds.add(wpDetail);
                            } else {
                                int detailid = details.getWpdetailid();
                                boolean flagcheckPr = service.checkForPrEntryInWprks(Integer.parseInt(request.getParameter("wpId")));
                                if (flagcheckPr) {
                                    String qt = service.getAcceptedQty(details.getRowId() + "", details.getTenderTableId() + "", Integer.parseInt(request.getParameter("wpId")));
                                    qty = new BigDecimal(qt).add(qty);
                                }
                                service.updateTableForWorksinVariOrder(detailid, qty);
                            }
                        }
                        boolean inDetail = service.addToWpDetails(tcwds);
                        List<Object> newCV = service.countNewContractValueForWorks(Integer.parseInt(request.getParameter("lotId")));
                        BigDecimal cntrctVal = null;
                        if(newCV!=null && !newCV.isEmpty()){
                            cntrctVal = (BigDecimal)newCV.get(0);
                        }
                        TblCmsVariContractVal contractVals = new TblCmsVariContractVal();
                        contractVals.setContractValue(cntrctVal);
                        contractVals.setLotId(Integer.parseInt(request.getParameter("lotId")));
                        contractVals.setVariOrdCntValId(vardet.get(0).getVariOrdId());
                        contractVals.setIsCurrent("yes");
                        service.countContVal(contractVals);
                    }
                }
                service.tensideVariOrderAccepted(wpId);
                sendMailForVariationOrderAcceptedFromTenderer(request.getParameter("tenderId"), request.getParameter("lotId"), user.toString(), "e-GP: Contractor has accepted Variation Order");
                response.sendRedirect("tenderer/CMS.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=accvari");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Variation_Order.getName(), "Accept Variation Order", "");
            }

        } else if ("variationorder".equalsIgnoreCase(action)) {
            String listcount = request.getParameter("listcount");
            String cms = request.getParameter("cms");
            String lotId = request.getParameter("lotId");
            int detailid = 0;
            int changecount = 0;
            List<TblCmsTrackVariation> variations = new ArrayList<TblCmsTrackVariation>();
            boolean flaB = service.checkForPrEntryInWprks(Integer.parseInt(request.getParameter("wpId")));
            List<TblCmsWpDetail> tcwdhs = service.getAllDetail(request.getParameter("wpId"));
            TblCmsVariationOrder order = new TblCmsVariationOrder();
            try {
                order.setCreatedBy(Integer.parseInt(userId));
                order.setVariOrdCreationDt(new Date());
                order.setVariationNo("Variation Order-" + (service.countVariationOrder(Integer.parseInt(request.getParameter("wpId"))) + 1));
                order.setVariOrdStatus("pending");
                order.setWpId(Integer.parseInt(request.getParameter("wpId")));
                int con = Integer.parseInt(request.getParameter("addcount"));
                for (int i = 0; i < Integer.parseInt(listcount); i++) {
                    detailid = Integer.parseInt(request.getParameter("detailid_" + i));
                    List<Object> qty = service.getQuantityForWorks(detailid);
                    List<Object[]> allDetail = service.getAllDetailByWpDetailId(detailid);
                    BigDecimal bd = null;
                    if (flaB) {
                        bd = new BigDecimal(request.getParameter("Qtyenter_" + i)).add(new BigDecimal(request.getParameter("qtyaccepted_" + i)));
                    } else {
                        bd = new BigDecimal(request.getParameter("Qtyenter_" + i));
                    }
                    if (!new BigDecimal(qty.get(0).toString()).equals(bd)) {
                        if (changecount == 0) {
                            boolean flag = service.addToVariationTable(order);
                        }

                        // service.updateTableForWorksinVariOrder(detailid, finalqty);
                        TblCmsTrackVariation tctv = new TblCmsTrackVariation();
                        tctv.setDescription((String) allDetail.get(0)[2]);
                        tctv.setEndDate((Date) allDetail.get(0)[7]);
                        tctv.setFlag("U");
                        tctv.setGroupname((String) allDetail.get(0)[1]);
                        tctv.setQty(new BigDecimal(request.getParameter("Qtyenter_" + i)));
                        tctv.setRate((BigDecimal) allDetail.get(0)[11]);
                        tctv.setRowId(Integer.parseInt(allDetail.get(0)[9].toString()));
                        tctv.setSrno((String) allDetail.get(0)[0]);
                        tctv.setStartDate((Date) allDetail.get(0)[6]);
                        tctv.setTenderTableId(Integer.parseInt(allDetail.get(0)[10].toString()));
                        tctv.setUom((String) allDetail.get(0)[3]);
                        tctv.setVariOrdId(order.getVariOrdId());
                        tctv.setWpdetailid(detailid);
                        variations.add(tctv);
                        TblCmsWpDetailHistory wpDetailHistory = new TblCmsWpDetailHistory();
                        wpDetailHistory.setAmendmentFlag("variationU");
                        wpDetailHistory.setHistoryCount(0);
                        wpDetailHistory.setWpId(tcwdhs.get(i).getTblCmsWpMaster().getWpId());
                        wpDetailHistory.setWpRowId(tcwdhs.get(i).getWpRowId());
                        wpDetailHistory.setWpRate(tcwdhs.get(i).getWpRate());
                        wpDetailHistory.setWpNoOfDays(tcwdhs.get(i).getWpNoOfDays());
                        wpDetailHistory.setWpStartDate(tcwdhs.get(i).getWpStartDate());
                        wpDetailHistory.setWpEndDate(tcwdhs.get(i).getWpEndDate());
                        wpDetailHistory.setWpQty(bd);
                        wpDetailHistory.setDetail(new TblCmsWpDetail(tcwdhs.get(i).getWpDetailId()));
                        wpDetailHistory.setActContractDtHistId(0);
                        wpDetailHistory.setTenderTableId(tcwdhs.get(i).getTenderTableId());
                        wpDetailHistory.setVariOrdNo(order.getVariOrdId());
                        wpDetailHistory.setCreatedDate(new java.sql.Date(new java.util.Date().getTime()));
                        list1.add(wpDetailHistory);
                        boolean indetailHistory = service.addToDetailHistory(list1);
                        changecount++;
                    }


                }
                if (con > 0) {
                    if (changecount == 0) {
                        boolean flag = service.addToVariationTable(order);
                        changecount++;
                    }
                    int rowId = 0;
                    int TenderTableId = 0;
                    if (!tcwdhs.isEmpty() && tcwdhs != null) {
                        rowId = tcwdhs.get(tcwdhs.size() - 1).getWpRowId();
                        TenderTableId = tcwdhs.get(tcwdhs.size() - 1).getTenderTableId();
                    }
                    for (int k = 0; k < con; k++) {
                        rowId++;
                        TblCmsTrackVariation tctv = new TblCmsTrackVariation();
                        tctv.setDescription(request.getParameter("desc_" + k));
                        tctv.setEndDate(DateUtils.convertStringtoDate(request.getParameter("edate_" + k), "dd-MMM-yyyy"));
                        tctv.setFlag("I");
                        tctv.setGroupname(request.getParameter("group_" + k));
                        tctv.setQty(new BigDecimal(request.getParameter("qty_" + k)));
                        tctv.setRate(new BigDecimal(request.getParameter("rate_" + k)));
                        tctv.setRowId(rowId);
                        tctv.setSrno(request.getParameter("sno_" + k));
                        tctv.setStartDate(DateUtils.convertStringtoDate(request.getParameter("sdate_" + k), "dd-MMM-yyyy"));
                        tctv.setTenderTableId(TenderTableId);
                        tctv.setUom(request.getParameter("UOM_" + k));
                        tctv.setVariOrdId(order.getVariOrdId());
                        tctv.setWpdetailid(1);
                        variations.add(tctv);
//                    TblCmsWpDetail wpDetail = new TblCmsWpDetail();
//                    wpDetail.setAmendmentFlag("variationI");
//                    wpDetail.setCreatedBy(Integer.parseInt(userId));
//                    wpDetail.setGroupId(request.getParameter("group_" + k));
//                    wpDetail.setItemInvGenQty(new BigDecimal(0));
//                    wpDetail.setItemInvStatus("pending");
//                    wpDetail.setTblCmsWpMaster(new TblCmsWpMaster(Integer.parseInt(request.getParameter("wpId"))));
//                    wpDetail.setTenderTableId(TenderTableId);
//                    wpDetail.setUserTypeId(Integer.parseInt(userTypeId));
//                    wpDetail.setWpEndDate(DateUtils.convertStringtoDate(request.getParameter("edate_" + k), "dd-MMM-yyyy"));
//                    wpDetail.setWpDescription(request.getParameter("desc_" + k));
//                    wpDetail.setWpItemStatus("pending");
//                    wpDetail.setWpNoOfDays(0);
//                    wpDetail.setWpQty(new BigDecimal(request.getParameter("qty_" + k)));
//                    wpDetail.setWpRate(new BigDecimal(request.getParameter("rate_" + k)));
//                    wpDetail.setWpRowId(rowId);
//                    wpDetail.setWpSrNo(request.getParameter("sno_" + k));
//                    wpDetail.setWpStartDate(DateUtils.convertStringtoDate(request.getParameter("sdate_" + k), "dd-MMM-yyyy"));
//                    wpDetail.setWpUom(request.getParameter("UOM_" + k));
//                    wpDetail.setWpQualityCheckReq("no");
//                    tcwds.add(wpDetail);


                    }
                    //boolean inDetail = service.addToWpDetails(tcwds);
//                for (int k = 0; k < con; k++) {
//                    TblCmsWpDetailHistory wpDetailHistory = new TblCmsWpDetailHistory();
//                    wpDetailHistory.setAmendmentFlag("variationI");
//                    wpDetailHistory.setHistoryCount(0);
//                    wpDetailHistory.setWpId(Integer.parseInt(request.getParameter("wpId")));
//                    wpDetailHistory.setWpRowId(Integer.parseInt(request.getParameter("rowId_" + k)));
//                    wpDetailHistory.setWpRate(new BigDecimal(request.getParameter("rate_" + k)));
//                    wpDetailHistory.setWpNoOfDays(0);
//                    wpDetailHistory.setWpStartDate(DateUtils.convertStringtoDate(request.getParameter("sdate_" + k), "dd-MMM-yyyy"));
//                    wpDetailHistory.setWpEndDate(DateUtils.convertStringtoDate(request.getParameter("edate_" + k), "dd-MMM-yyyy"));
//                    wpDetailHistory.setWpQty(new BigDecimal(request.getParameter("qty_" + k)));
//                    wpDetailHistory.setDetail(new TblCmsWpDetail(Integer.parseInt(request.getParameter("detailid_" + k))));
//                    wpDetailHistory.setActContractDtHistId(0);
//                    wpDetailHistory.setTenderTableId(Integer.parseInt(request.getParameter("tendertableId_" + k)));
//                    wpDetailHistory.setVariOrdNo(order.getVariOrdId());
//                    list1.add(wpDetailHistory);
//                }
//                boolean indetailHistory = service.addToDetailHistory(list1);

                }
                boolean intrackvariation = service.AddToTrackVariation(variations);

                if (changecount > 0) {
//                    List<Object> os = service.getWpId(Integer.parseInt(request.getParameter("lotId")));
//                    BigDecimal contcatcVal = new BigDecimal(0);
//                    for (Object obj : os) {
//                        List<TblCmsWpDetail> listdetail = service.getAllDetail(obj.toString());
//                        for (int i = 0; i < listdetail.size(); i++) {
//                            BigDecimal Frate = listdetail.get(i).getWpQty().multiply(listdetail.get(i).getWpRate());
//                            contcatcVal = contcatcVal.add(Frate);
//                        }
//                    }
//                    TblCmsVariContractVal contractVals = new TblCmsVariContractVal();
//                    contractVals.setContractValue(contcatcVal);
//                    contractVals.setLotId(Integer.parseInt(request.getParameter("lotId")));
//                    contractVals.setVariOrdCntValId(order.getVariOrdId());
//                    contractVals.setIsCurrent("yes");
//                    service.countContVal(contractVals);
                }
                sendMailForVariationOrderFromPE(request.getParameter("tenderId"), lotId, user.toString(), "e-GP: Procuring Entity has requested for Variation Order");
                if (changecount == 0) {
                    response.sendRedirect("officer/DeliverySchedule.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=changed");
                } else {
                    response.sendRedirect("officer/variOrderUploadDoc.jsp?keyId=" + request.getParameter("wpId") + "&tenderId=" + request.getParameter("tenderId") + "&lotId=" + lotId + "&wpId=" + request.getParameter("wpId") + "&msg=noedit");
                    //response.sendRedirect("officer/DeliverySchedule.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=noedit");
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Variation_Order.getName(), "Place variation order", "");
            }

        } else if ("finish".equalsIgnoreCase(action)) {
            String respMsg = "";
            try {
                String count = request.getParameter("count_" + request.getParameter("lotId"));
                TblCmsWptenderBoqmap tcwb = new TblCmsWptenderBoqmap();
                if (count != null) {
                    for (int i = 0; i < Integer.parseInt(count); i++) {
                        cmsWpMaster.setCreatedBy(Integer.parseInt(userId));
                        cmsWpMaster.setCreatedDate(new java.util.Date());
                        cmsWpMaster.setWpLotId(Integer.parseInt(request.getParameter("lotId")));
                        cmsWpMaster.setWpName("Consolidate");
                        cmsWpMaster.setIsRepeatOrder("no");
                        cmsWpMaster.setIsDateEdited("no");
                        boolean inMaster = service.add(cmsWpMaster);
                        tcwb.setWpTenderFormId(Integer.parseInt(request.getParameter("tenderformid_" + i)));
                        tcwb.setTblCmsWpMaster(new TblCmsWpMaster(cmsWpMaster.getWpId()));
                        boolean inchild = service.addinchild(tcwb);
                    }
                }

                TenderTablesSrBean bean = new TenderTablesSrBean();
                List<Object> os = service.getWpId(Integer.parseInt(request.getParameter("lotId")));
                boolean isTotFormulaCre = false;
                List<SPCommonSearchDataMore> mores = service.getQualifiedUser(request.getParameter("tenderId"),request.getParameter("lotId"));
                if (os != null && !os.isEmpty()) {
                    for (Object obj : os) {
                        List<Object> list = null;
                       /* if(tenderType.equals("ICT"))
                        {
                            //list = service.getTenderFormIdForICT(Integer.parseInt(request.getParameter("tenderId")), Integer.parseInt(mores.get(0).getFieldName1()), Integer.parseInt(request.getParameter("lotId")));
                            List<SPCommonSearchData> listCommonSearch1 = spCommonSearch.executeProcedure("getTenderFormIdForICT", request.getParameter("tenderId"), mores.get(0).getFieldName1(), request.getParameter("lotId"), null, null, null,null,null,null);
                            for( SPCommonSearchData listData:listCommonSearch1)
                            {
                             System.out.println(listCommonSearch1);
                             System.out.println(listData);
                             Object b = listData.getFieldName1();
                             System.out.println(b);
                             System.out.println(listData.getFieldName1());
                             System.out.println(b);
                            list.add(b);
                          //  listData.
                            list.addAll(os);
                            }
                        }*/
                       // else
                       // {
                            list = service.getTenderFormId(obj.toString());
                      //  }
                        if (!list.isEmpty() && list != null) {
                            for (Object o : list) {
                                List<Object> counttable = service.countTable(o.toString());
                                for (Object obs : counttable) {
                                    //boolean checkforentry = service.checkForWpDetailEntry(obj.toString(), o.toString());
                                    //added by dohatec to correct the result/
                                    boolean checkforentry = service.checkForWpDetailEntrybyTenderTableId(obj.toString(), obs.toString());
                                    if (checkforentry) {
                                        isTotFormulaCre = bean.isTotalFormulaCreated(Integer.parseInt(obs.toString()));
                                        short rows = bean.getNoOfRowsInTable(Integer.parseInt(obs.toString()), (short) 1);
                                        boolean isDiscountForm = service.getDiscountFormId(o.toString());
                                        if (isTotFormulaCre) {
                                            rows = (short) (rows - 1);
                                        }
                                        System.out.println("rows "+rows);

                                        java.util.ListIterator<com.cptu.egp.eps.model.table.TblTenderCells> tblCellsDtl = bean.getCelldetailsForConsolidate(Integer.parseInt(obs.toString())).listIterator();
                                        java.util.ListIterator<com.cptu.egp.eps.model.table.TblTenderCells> tblCellsDtlLi = bean.getCellValueForRow(Integer.parseInt(request.getParameter("tenderId")), Integer.parseInt(o.toString()), Integer.parseInt(mores.get(0).getFieldName1())).listIterator();
                                        for (int i = 1; i <= rows; i++) {


                                            TblCmsWpDetail tblCmsWpDetail = new TblCmsWpDetail();

//                                        if (tblCellsDtlLi.hasNext()) {
//                                            TblTenderCells cellsr = tblCellsDtlLi.next();
//                                            tblCmsWpDetail.setWpRate(new BigDecimal(cellsr.getCellvalue()));
//                                            cellsr = null;
//                                        }

                                            tblCmsWpDetail.setTblCmsWpMaster(new TblCmsWpMaster(Integer.parseInt(obj.toString())));

                                            tblCmsWpDetail.setWpRowId(i);
                                            if (procnature.equalsIgnoreCase("goods")) {
                                                tblCmsWpDetail.setWpStartDate(new Date());
                                            } else {
                                                tblCmsWpDetail.setWpStartDate(null);
                                            }

                                            tblCmsWpDetail.setWpQualityCheckReq("no");
                                            tblCmsWpDetail.setWpItemStatus("pending");
                                            tblCmsWpDetail.setCreatedBy(Integer.parseInt(userId));
                                            tblCmsWpDetail.setUserTypeId(Integer.parseInt(userTypeId));
                                            tblCmsWpDetail.setWpNoOfDays(0);
                                            tblCmsWpDetail.setIsRepeatOrder("no");
                                            tblCmsWpDetail.setItemInvGenQty(new BigDecimal(0));
                                            tblCmsWpDetail.setItemInvStatus("pending");
                                            tblCmsWpDetail.setAmendmentFlag("original");
                                            for (int j = 1; j <= 6; j++) {

                                                if (tblCellsDtl.hasNext()) {
                                                    TblTenderCells cells = tblCellsDtl.next();
                                                    if (procnature.equalsIgnoreCase("works")) {
                                                        // Added for already created tender TenderId : 4
                                                       // if (Integer.parseInt(request.getParameter("tenderId")) == 2229 || Integer.parseInt(request.getParameter("tenderId")) == 1) {
													     /* To resolve issue 2822*/
							if (Integer.parseInt(request.getParameter("tenderId")) == 1) {
                                                            //srno, item code, desc, uom, qty
                                                            if (j == 6) {
                                                                continue;
                                                            }
                                                            if (j == 1) {
                                                                tblCmsWpDetail.setWpSrNo(cells.getCellvalue());
                                                                tblCmsWpDetail.setWpEndDate(null);
                                                            } else if (j == 2) {
                                                                tblCmsWpDetail.setGroupId("");
                                                                //tblCmsWpDetail.setGroupId(cells.getCellvalue());
                                                            } else if (j == 3) {
                                                                tblCmsWpDetail.setWpDescription(cells.getCellvalue());

                                                            } else if (j == 4) {
                                                                tblCmsWpDetail.setWpUom(cells.getCellvalue());
                                                            } else {
                                                                tblCmsWpDetail.setWpQty(new BigDecimal(cells.getCellvalue()));
                                                                if (tblCellsDtlLi.hasNext()) {
                                                                    TblTenderCells cellsr = tblCellsDtlLi.next();
                                                                    BigDecimal bdUnitRate, bdTotRate, bdQty;
                                                                    bdTotRate = new BigDecimal(cellsr.getCellvalue()).setScale(3, 0);
                                                                    bdQty = new BigDecimal(cells.getCellvalue()).setScale(3, 0);
                                                                    bdUnitRate = bdTotRate.divide(bdQty, 3, RoundingMode.HALF_UP);
                                                                    tblCmsWpDetail.setWpRate(bdUnitRate);
                                                                    cellsr = null;
                                                                    bdUnitRate = null;
                                                                    bdTotRate = null;
                                                                    bdQty = null;
                                                                }
                                                            }
                                                        }else if(isDiscountForm){
                                                            if (j == 1) {
                                                                tblCmsWpDetail.setWpSrNo("1");
                                                                tblCmsWpDetail.setWpEndDate(null);
                                                                tblCmsWpDetail.setGroupId("N/A");
                                                                tblCmsWpDetail.setWpDescription("N/A");
                                                                tblCmsWpDetail.setWpDescription("N/A");
                                                                tblCmsWpDetail.setWpUom("N/A");
                                                                tblCmsWpDetail.setWpQty(BigDecimal.ONE);
                                                                if (tblCellsDtlLi.hasNext()) {
                                                                    TblTenderCells cellsr = tblCellsDtlLi.next();
                                                                    System.out.println("cellsr.getCellvalue() "+cellsr.getCellvalue());
                                                                    BigDecimal bdTotRate;
                                                                    bdTotRate = new BigDecimal(cellsr.getCellvalue()).setScale(3, 0);                                                                    
                                                                    tblCmsWpDetail.setWpRate(bdTotRate);
                                                                    cellsr = null;
                                                                }
                                                            }                                                            
                                                        } else {
                                                            //srno, itemcode, desc, uom, qty
                                                            if (j == 2) {
                                                                continue;
                                                            }
                                                            if (j == 1) {
                                                                tblCmsWpDetail.setWpSrNo(cells.getCellvalue());
                                                                tblCmsWpDetail.setWpEndDate(null);
                                                            }// else if (j == 2) {
                                                            //    tblCmsWpDetail.setGroupId(cells.getCellvalue());
                                                            //} 
                                                            else if (j == 3) {
                                                                tblCmsWpDetail.setGroupId(null);
                                                                tblCmsWpDetail.setWpDescription(cells.getCellvalue());

                                                            } else if (j == 4) {
                                                                tblCmsWpDetail.setWpUom(cells.getCellvalue());
                                                            } else if (j == 5){
                                                                System.out.println("cells.getCellvalue() "+cells.getCellvalue()+ " tblCellsDtlLi "+tblCellsDtlLi);
                                                                if(!cells.getCellvalue().equals(""))
                                                                {
                                                                    tblCmsWpDetail.setWpQty(new BigDecimal(cells.getCellvalue()));
                                                                    if (tblCellsDtlLi.hasNext()) {
                                                                        TblTenderCells cellsr = tblCellsDtlLi.next();
                                                                        System.out.println("cellsr.getCellvalue() "+cellsr.getCellvalue());
                                                                        BigDecimal bdUnitRate, bdTotRate, bdQty;
                                                                        bdTotRate = new BigDecimal(cellsr.getCellvalue()).setScale(3, 0);
                                                                        bdQty = new BigDecimal(cells.getCellvalue()).setScale(3, 0);
                                                                        bdUnitRate = bdTotRate.divide(bdQty, 3, RoundingMode.HALF_UP);
                                                                        tblCmsWpDetail.setWpRate(bdUnitRate);
                                                                        cellsr = null;
                                                                        bdUnitRate = null;
                                                                        bdTotRate = null;
                                                                        bdQty = null;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    } else { //for goods
                                                        if(isDiscountForm){
                                                            if (j == 1) {
                                                                tblCmsWpDetail.setWpSrNo("1");
                                                                tblCmsWpDetail.setWpEndDate(new Date());
                                                                tblCmsWpDetail.setGroupId("N/A");
                                                                tblCmsWpDetail.setWpDescription("N/A");
                                                                tblCmsWpDetail.setWpDescription("N/A");
                                                                tblCmsWpDetail.setWpUom("N/A");
                                                                tblCmsWpDetail.setWpQty(BigDecimal.ONE);
                                                                if (tblCellsDtlLi.hasNext()) {
                                                                    TblTenderCells cellsr = tblCellsDtlLi.next();
                                                                    System.out.println("cellsr.getCellvalue() "+cellsr.getCellvalue());
                                                                    BigDecimal bdTotRate;
                                                                    bdTotRate = new BigDecimal(cellsr.getCellvalue()).setScale(3, 0);                                                                    
                                                                    tblCmsWpDetail.setWpRate(bdTotRate);
                                                                    cellsr = null;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (j == 5) {
                                                                continue;
                                                            }
                                                            if (j == 1) {
                                                                tblCmsWpDetail.setWpSrNo(cells.getCellvalue());

                                                            } else if (j == 2) {
                                                                tblCmsWpDetail.setWpDescription(cells.getCellvalue());
                                                                tblCmsWpDetail.setGroupId(null);

                                                            } else if (j == 3) {
                                                                tblCmsWpDetail.setWpUom(cells.getCellvalue());

                                                            } else if (j == 4) {
                                                                tblCmsWpDetail.setWpQty(new BigDecimal(cells.getCellvalue()));
                                                                if (tblCellsDtlLi.hasNext()) {
                                                                    TblTenderCells cellsr = tblCellsDtlLi.next();
                                                                  //  tblCmsWpDetail.setWpRate(new BigDecimal(cellsr.getCellvalue()).divide(new BigDecimal(cells.getCellvalue()), 3, RoundingMode.HALF_UP));
                                                                    System.out.println("row ID = "+i+"and o"+o.toString());
                                                                    List<SPCommonSearchData> listCommonSearch = spCommonSearch.executeProcedure("getCellValueForNOAOfICT", request.getParameter("tenderId"), obs.toString(), String.valueOf(i),  String.valueOf(j), mores.get(0).getFieldName1(), o.toString(), null, null, null, null);
                                                                 //   System.out.println("amount value: "+listCommonSearch.get(0).getFieldName1());
                                                                //    System.out.println("amount value2: "+listCommonSearch.get(0).getFieldName2());
                                                                //      System.out.println("amount value3: "+listCommonSearch.get(0).getFieldName3());
                                                                    MathContext mc = new MathContext(3, RoundingMode.HALF_UP);
                                                                    //BigDecimal totalAmount = new BigDecimal(cellsr.getCellvalue()).add(new BigDecimal(listCommonSearch.get(0).getFieldName2()),mc);
                                                                    //BigDecimal totalAmount = new BigDecimal(cellsr.getCellvalue());
                                                                  //
                                                                  //  System.out.println("totalAmount: "+totalAmount);
                                                                  //  System.out.println(totalAmount.divide(new BigDecimal(listCommonSearch.get(0).getFieldName1()),3,RoundingMode.HALF_UP));

                                                                   // System.out.println("cur ID"+listCommonSearch.get(0).getFieldName3());
                                                                    if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                                                    {
                                                                      //  tblCmsWpDetail.setWpRate(new BigDecimal(cellsr.getCellvalue()).divide(new BigDecimal(listCommonSearch.get(0).getFieldName1()),3,RoundingMode.HALF_UP));
                                                                       // tblCmsWpDetail.setWpRate(totalAmount.divide(new BigDecimal(listCommonSearch.get(0).getFieldName1()),3,RoundingMode.HALF_UP));
                                                                        tblCmsWpDetail.setWpRate(new BigDecimal(listCommonSearch.get(0).getFieldName1()));
                                                                        tblCmsWpDetail.setCurrencyName(listCommonSearch.get(0).getFieldName3());
                                                                        tblCmsWpDetail.setSupplierVat(new BigDecimal(listCommonSearch.get(0).getFieldName2()));
                                                                        tblCmsWpDetail.setInlandOthers(new BigDecimal(listCommonSearch.get(0).getFieldName4()));
                                                                    }
                                                                    else
                                                                    {
                                                                        tblCmsWpDetail.setWpRate(new BigDecimal(cellsr.getCellvalue()).divide(new BigDecimal(cells.getCellvalue()), 3, RoundingMode.HALF_UP));
                                                                    }

                                                                    cellsr = null;
                                                                }
                                                            } else {
                                                                tblCmsWpDetail.setWpNoOfDays(Integer.parseInt(cells.getCellvalue()));
                                                                List<TblTenderLotSecurity> appList = cmsConfigDateService.getTenderLotSecurity(Integer.parseInt(request.getParameter("tenderId")), Integer.parseInt(request.getParameter("lotId")));
                                                                Date CstartDate = DateUtils.formatStdString(appList.get(0).getStartTime());
                                                                long days = (CstartDate.getTime()) + Long.parseLong(cells.getCellvalue()) * (24 * 60 * 60 * 1000);
                                                                CstartDate.setTime(days);
                                                                tblCmsWpDetail.setWpEndDate(CstartDate);
                                                            }
                                                        }
                                                    }
                                                    tblCmsWpDetail.setTenderTableId(Integer.parseInt(obs.toString()));
                                                    cells = null;
                                                }
                                            }
                                            tcwds.add(tblCmsWpDetail);

                                        }

                                        boolean inDetail = service.addToWpDetails(tcwds);
                                    }
                                }
                            }

                        }

                    }
                }
                if (procnature.equalsIgnoreCase("goods")) {
                    for (int j = 0; j < tcwds.size(); j++) {
                        TblCmsWpDetailHistory history = new TblCmsWpDetailHistory();
                        history.setAmendmentFlag(tcwds.get(j).getAmendmentFlag());
                        history.setHistoryCount(0);
                        history.setWpId(tcwds.get(j).getTblCmsWpMaster().getWpId());
                        history.setWpRowId(tcwds.get(j).getWpRowId());
                        history.setWpRate(tcwds.get(j).getWpRate());
                        history.setWpNoOfDays(tcwds.get(j).getWpNoOfDays());
                        history.setWpStartDate(new Date());
                        history.setWpEndDate(tcwds.get(j).getWpEndDate());
                        history.setWpQty(tcwds.get(j).getWpQty());
                        history.setDetail(new TblCmsWpDetail(tcwds.get(j).getWpDetailId()));
                        history.setActContractDtHistId(0);
                        history.setTenderTableId(tcwds.get(j).getTenderTableId());
                        history.setCreatedDate(new java.sql.Date(new java.util.Date().getTime()));
                        if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                        {
                            history.setCurrencyName(tcwds.get(j).getCurrencyName());
                            history.setSupplierVat(tcwds.get(j).getSupplierVat());
                            history.setInlandOthers(tcwds.get(j).getInlandOthers());
                        }
                        list1.add(history);
                    }
                    boolean indetailHistory = service.addToDetailHistory(list1);
                }
                respMsg = "fin";
            } catch (Exception e) {
                service.deleteIfNOARejects(request.getParameter("lotId"));
                respMsg = "t";
                e.printStackTrace();
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), Integer.parseInt(request.getParameter("tenderId")), "tenderId", EgpModule.Noa.getName(), "Finish Consolidation by PE", "");
            }
            response.sendRedirect("officer/NOA.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=" + respMsg);
        } else if ("delete".equalsIgnoreCase(action)) {
            try {
                String wpid = request.getParameter("wpId");
                service.delete(wpid);
                response.sendRedirect("officer/NOA.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=del");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), Integer.parseInt(request.getParameter("tenderId")), "tenderId", EgpModule.Noa.getName(), "Delete Consolidation by PE", "");
            }
        } else if ("finishdates".equalsIgnoreCase(action)) {
            try {
                String lotId = request.getParameter("lotId");
                service.finishEditDates(Integer.parseInt(lotId));
                if (procnature.equalsIgnoreCase("goods")) {
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), Integer.parseInt(request.getParameter("tenderId")), "tenderId", EgpModule.Noa.getName(), "Finish edit days by PE", "");
                    response.sendRedirect("officer/NOA.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=editfinish");
                } else {
                    response.sendRedirect("officer/NOA.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=issuenoa");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("saveAsDreaft".equalsIgnoreCase(action)) {
            try {
                String listcount = request.getParameter("listcount");
                boolean forSecondPr = service.checkForPrEntry(Integer.parseInt(request.getParameter("wpId")));
                String tenderId = request.getParameter("tenderId");
                String lotId = request.getParameter("lotId");
                String isEdit = "";
                if (request.getParameter("isEdit") != null) {
                    isEdit = request.getParameter("isEdit");
                }
                List<Object[]> listcontarct = service.getContractId(tenderId, lotId);
                TblCmsPrMaster prMaster = new TblCmsPrMaster();
                prMaster.setCreatedBy(Integer.parseInt(userId));
                prMaster.setCreatedDate(new Date());
                prMaster.setPreviousPrId(new Integer(0));
                if (forSecondPr) {
                    prMaster.setProgressRepId(Integer.parseInt(request.getParameter("prId")));
                    prMaster.setProgressRepcreatedDt(DateUtils.convertStringtoDate(request.getParameter("prReqDate"), "yyyy-MM-dd"));
                } else {
                    prMaster.setProgressRepcreatedDt(new Date());
                }
                prMaster.setTblContractSign(new TblContractSign(Integer.parseInt(listcontarct.get(0)[0].toString())));
                prMaster.setProgressRepNo("progress report as on " + new java.sql.Date(new Date().getTime()));
                prMaster.setStatus("pending");
                prMaster.setTblCmsWpMaster(new TblCmsWpMaster(Integer.parseInt(request.getParameter("wpId"))));
                prMasters.add(prMaster);
                boolean prm = service.addToPrMaster(prMasters);
                for (int j = 0; j < Integer.parseInt(listcount); j++) {
                    BigDecimal qty = new BigDecimal(request.getParameter("qty_" + j));
                    String qtyentered = request.getParameter("Qtyenter_" + j);
                    String totalAccepted = request.getParameter("Qtyaccepted_" + j);
                    BigDecimal qtenter = new BigDecimal(0);
                    BigDecimal qtaccepted = new BigDecimal(0);
                    if (!"".equalsIgnoreCase(qtyentered)) {
                        qtenter = new BigDecimal(request.getParameter("Qtyenter_" + j));
                    }
                    if (!"".equalsIgnoreCase(totalAccepted)) {
                        qtaccepted = new BigDecimal(request.getParameter("Qtyaccepted_" + j));
                    }
                    TblCmsPrDetail prDetail = new TblCmsPrDetail();
                    prDetail.setTblCmsPrMaster(new TblCmsPrMaster(prMasters.get(0).getProgressRepId()));
                    prDetail.setDeliveryStatus("pending"); // yet pending
                    prDetail.setRemarks(request.getParameter("remarks_" + j));
                    if (forSecondPr) {
                        prDetail.setProgressReptDtlId(Integer.parseInt(request.getParameter("prRepDetId_" + j)));
                    }
                    prDetail.setTenderTableId(Integer.parseInt(request.getParameter("tableId_" + j)));
                    prDetail.setTotalPendingQty(qty.subtract(qtenter).subtract(qtaccepted));
                    if ("".equalsIgnoreCase(request.getParameter("txtdate_" + j))) {
                        prDetail.setItemEntryDt(null);
                    } else {
                        prDetail.setItemEntryDt(DateUtils.convertStringtoDate(request.getParameter("txtdate_" + j), "dd-MMM-yyyy"));
                    }

                    prDetail.setQtyAcceptTillThisPr(qtaccepted);
                    prDetail.setQtyDlvrdCurrPr(qtenter);
                    prDetail.setRowId(Integer.parseInt(request.getParameter("row_" + j)));
                    prDetail.setQtyDuringThisPr(qty);
                    prDetails.add(prDetail);

                }
                boolean prd = service.addTpPrDetail(prDetails);
                if (request.getParameter("frmSrv") != null) {
                    response.sendRedirect("officer/SrvPreparePR.jsp?tenderId=" + request.getParameter("tenderId") + "&wpId=" + request.getParameter("wpId") + "&lotId=" + lotId + "&isedit=y&msg=endsave&frmSrv=true");
                } else {
                    response.sendRedirect("officer/PreparePRreport.jsp?tenderId=" + request.getParameter("tenderId") + "&wpId=" + request.getParameter("wpId") + "&lotId=" + lotId + "&isedit=y&msg=endsave");
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (request.getParameter("isEdit") != null) {
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "ContractId", EgpModule.Progress_Report.getName(), "Edit Progress Report", "");
                } else {
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "ContractId", EgpModule.Progress_Report.getName(), "Draft Progress Report", "");
                }

            }
        } else if ("save".equalsIgnoreCase(action)) {
            try {
                String listcount = request.getParameter("listcount");
                String tenderId = request.getParameter("tenderId");
                String lotId = request.getParameter("lotId");
                String isEdit = "";
                if (request.getParameter("isEdit") != null) {
                    isEdit = request.getParameter("isEdit");
                }
                boolean forSecondPr = service.checkForPrEntry(Integer.parseInt(request.getParameter("wpId")));
                List<Object[]> listcontarct = service.getContractId(tenderId, lotId);
                TblCmsPrMaster prMaster = new TblCmsPrMaster();
                prMaster.setCreatedBy(Integer.parseInt(userId));
                prMaster.setCreatedDate(new Date());
                prMaster.setPreviousPrId(new Integer(0));
                if (forSecondPr) {
                    prMaster.setProgressRepId(Integer.parseInt(request.getParameter("prId")));
                    prMaster.setProgressRepcreatedDt(DateUtils.convertStringtoDate(request.getParameter("prReqDate"), "yyyy-MM-dd"));
                } else {
                    prMaster.setProgressRepcreatedDt(new Date());
                }
                prMaster.setTblContractSign(new TblContractSign(Integer.parseInt(listcontarct.get(0)[0].toString())));
                prMaster.setProgressRepNo("Progress Report as on " + new java.sql.Date(new Date().getTime()));
                prMaster.setStatus("completed");
                prMaster.setTblCmsWpMaster(new TblCmsWpMaster(Integer.parseInt(request.getParameter("wpId"))));
                prMasters.add(prMaster);
                boolean prm = service.addToPrMaster(prMasters);


                for (int j = 0; j < Integer.parseInt(listcount); j++) {
                    BigDecimal qty = new BigDecimal(request.getParameter("qty_" + j));
                    String qtyentered = request.getParameter("Qtyenter_" + j);
                    String totalAccepted = request.getParameter("Qtyaccepted_" + j);
                    BigDecimal qtenter = new BigDecimal(0);
                    BigDecimal qtaccepted = new BigDecimal(0);
                    if (!"".equalsIgnoreCase(qtyentered)) {
                        qtenter = new BigDecimal(request.getParameter("Qtyenter_" + j));
                    }
                    if (!"".equalsIgnoreCase(totalAccepted)) {
                        qtaccepted = new BigDecimal(request.getParameter("Qtyaccepted_" + j));
                    }

                    TblCmsPrDetail prDetail = new TblCmsPrDetail();
                    if (qtaccepted.equals(0)) {
                        prDetail.setQtyAcceptTillThisPr(qtenter);
                    } else {
                        prDetail.setQtyAcceptTillThisPr(qtaccepted.add(qtenter));
                    }
                    prDetail.setTblCmsPrMaster(new TblCmsPrMaster(prMasters.get(0).getProgressRepId()));
                    // yet pending
                    prDetail.setRemarks(request.getParameter("remarks_" + j));
                    if (forSecondPr) {
                        prDetail.setProgressReptDtlId(Integer.parseInt(request.getParameter("prRepDetId_" + j)));
                    }
                    prDetail.setTenderTableId(Integer.parseInt(request.getParameter("tableId_" + j)));
                    prDetail.setRowId(Integer.parseInt(request.getParameter("row_" + j)));
                    prDetail.setTotalPendingQty(qty.subtract(qtenter).subtract(qtaccepted));
                    if ((qty.subtract(qtenter).subtract(qtaccepted)).toString().equalsIgnoreCase(("0.000"))) {
                        prDetail.setDeliveryStatus("completed");
                        service.updateMasterForQtyZero(request.getParameter("row_" + j), request.getParameter("tableId_" + j));
                    } else {
                        prDetail.setDeliveryStatus("pending");
                    }

                    if ("".equalsIgnoreCase(request.getParameter("txtdate_" + j))) {
                        prDetail.setItemEntryDt(null);
                    } else {
                        prDetail.setItemEntryDt(DateUtils.convertStringtoDate(request.getParameter("txtdate_" + j), "dd-MMM-yyyy"));
                    }
                    prDetail.setQtyDlvrdCurrPr(qtenter);
                    prDetail.setQtyDuringThisPr(qty);
                    prDetails.add(prDetail);

                }
                boolean prd = service.addTpPrDetail(prDetails);
                sendMailForFinalizedReport(request.getParameter("tenderId"), lotId, user.toString(), "has Finalized", "e-GP: PE has Finalized Progress Report");
//            response.sendRedirect("officer/ProgressReport.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=endPr");
                if (request.getParameter("frmSrv") != null) {
                    response.sendRedirect("officer/ProgressReportUploadDoc.jsp?tenderId=" + request.getParameter("tenderId") + "&prRepId=" + prMasters.get(0).getProgressRepId() + "&wpId=" + request.getParameter("wpId") + "&lotId=" + lotId + "&isedit=" + isEdit + "&msg=endPr&&frmSrv=true");
                } else {
                    response.sendRedirect("officer/ProgressReportUploadDoc.jsp?tenderId=" + request.getParameter("tenderId") + "&prRepId=" + prMasters.get(0).getProgressRepId() + "&wpId=" + request.getParameter("wpId") + "&lotId=" + lotId + "&isedit=" + isEdit + "&msg=endPr");
                }



            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Progress_Report.getName(), "Finalized Progress report", "");
            }
        } else if ("editdates".equalsIgnoreCase(action)) {
            String listcount = request.getParameter("listcount");
            String cms = request.getParameter("cms");
            String lotId = request.getParameter("lotId");
            String wpId = request.getParameter("wpId");
            int detailid = 0;
            int nod = 0;
            List<TblCmsWpDetailHistory> tcwdhs = service.getHistory(Integer.parseInt(request.getParameter("wpId")));
            List<Object> getMaxCount = service.getWPDetailHistMaxCount(Integer.parseInt(request.getParameter("wpId")), "originalupdate");
            String strCount = "";
            int hCount = 0;
            try {
                if (getMaxCount.get(0) != null) {
                    strCount = getMaxCount.get(0).toString();
                    if ("0".equalsIgnoreCase(strCount)) {
                        hCount++;
                    } else {
                        hCount = Integer.parseInt(strCount);
                        hCount++;
                    }
                } else {
                    hCount++;
                }
                for (int i = 0; i < Integer.parseInt(listcount); i++) {

                    detailid = Integer.parseInt(request.getParameter("detailid_" + i));
                    nod = Integer.parseInt(request.getParameter("nod_" + i));
                    List<Object> list = service.checkForDate(detailid);
                    if (!list.isEmpty() && list != null) {

                        service.updateTable(detailid, nod, DateUtils.convertStringtoDate(request.getParameter("txtdate_" + i), "dd-MMM-yyyy"));
                        TblCmsWpDetailHistory wpDetailHistory = new TblCmsWpDetailHistory();
                        wpDetailHistory.setAmendmentFlag(tcwdhs.get(i).getAmendmentFlag() + "update");
                        wpDetailHistory.setHistoryCount(hCount);
                        wpDetailHistory.setWpId(tcwdhs.get(i).getWpId());
                        wpDetailHistory.setWpRowId(tcwdhs.get(i).getWpRowId());
                        wpDetailHistory.setWpRate(tcwdhs.get(i).getWpRate());
                        wpDetailHistory.setWpNoOfDays(nod);
                        wpDetailHistory.setWpStartDate(new Date());
                        wpDetailHistory.setWpEndDate(DateUtils.convertStringtoDate(request.getParameter("txtdate_" + i), "dd-MMM-yyyy"));
                        wpDetailHistory.setWpQty(tcwdhs.get(i).getWpQty());
                        wpDetailHistory.setDetail(new TblCmsWpDetail(tcwdhs.get(i).getDetail().getWpDetailId()));
                        wpDetailHistory.setActContractDtHistId(0);
                        wpDetailHistory.setTenderTableId(tcwdhs.get(i).getTenderTableId());
                        wpDetailHistory.setCreatedDate(new java.sql.Date(new java.util.Date().getTime()));
                        // Dohatec Start
                        wpDetailHistory.setCurrencyName(tcwdhs.get(i).getCurrencyName());
                        wpDetailHistory.setSupplierVat(tcwdhs.get(i).getSupplierVat());
                        wpDetailHistory.setInlandOthers(tcwdhs.get(i).getInlandOthers());
                        // Dohatec End
                        list1.add(wpDetailHistory);

                        boolean indetailHistory = service.addToDetailHistory(list1);
                    }



                }

                sendMailForDeliverySchedule(request.getParameter("tenderId"), lotId, user.toString(), "has edited", "e-GP: PE has Edited Delivery Schedule Date(s)");
                if (cms == null) {
                    response.sendRedirect("officer/DeliverySchedule.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=edit");
                } else {
                    //response.sendRedirect("officer/DeliverySchedule.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=edit&flag=cms");
                    response.sendRedirect("officer/DeliveryScheduleUploadDoc.jsp?keyId=" + wpId + "&tenderId=" + request.getParameter("tenderId") + "&lotId=" + lotId + "&wpId=" + wpId + "&msg=edit&flag=cms");
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "ContractId", EgpModule.Delivery_Schedule.getName(), "Edit Dates", "");
            }
        } else if ("saveasdrafteditdates".equalsIgnoreCase(action)) {
            String listcount = request.getParameter("listcount");
            String cms = request.getParameter("cms");
            String lotId = request.getParameter("lotId");
            int detailid = 0;
            try {
                for (int i = 0; i < Integer.parseInt(listcount); i++) {
                    String sdate = request.getParameter("txtsdate_" + i);
                    detailid = Integer.parseInt(request.getParameter("detailid_" + i));
                    if (sdate != null && !"".equalsIgnoreCase(sdate)) {
                        service.updateTableForWorks(detailid, DateUtils.convertStringtoDate(request.getParameter("txtsdate_" + i), "dd-MMM-yyyy"), DateUtils.convertStringtoDate(request.getParameter("txtedate_" + i), "dd-MMM-yyyy"), Integer.parseInt(request.getParameter("noofdays_" + i)));
                    }
                    /*TblCmsWpDetailHistory wpDetailHistory = new TblCmsWpDetailHistory();
                    wpDetailHistory.setAmendmentFlag(tcwdhs.get(i).getAmendmentFlag());
                    wpDetailHistory.setHistoryCount(0);
                    wpDetailHistory.setWpId(tcwdhs.get(i).getTblCmsWpMaster().getWpId());
                    wpDetailHistory.setWpRowId(tcwdhs.get(i).getWpRowId());
                    wpDetailHistory.setWpRate(tcwdhs.get(i).getWpRate());
                    wpDetailHistory.setWpNoOfDays(Integer.parseInt(request.getParameter("noofdays_" + i)));
                    wpDetailHistory.setWpStartDate(DateUtils.convertStringtoDate(request.getParameter("txtsdate_" + i), "dd-MMM-yyyy"));
                    wpDetailHistory.setWpEndDate(DateUtils.convertStringtoDate(request.getParameter("txtedate_" + i), "dd-MMM-yyyy"));
                    wpDetailHistory.setWpQty(tcwdhs.get(i).getWpQty());
                    wpDetailHistory.setTenderTableId(tcwdhs.get(i).getTenderTableId());
                    wpDetailHistory.setDetail(new TblCmsWpDetail(tcwdhs.get(i).getWpDetailId()));
                    wpDetailHistory.setActContractDtHistId(0);
                    list1.add(wpDetailHistory);
                    boolean indetailHistory = service.addToDetailHistory(list1);*/

                }
                response.sendRedirect("tenderer/CMS.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=edit");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("dateedit".equalsIgnoreCase(action)) {
            String listcount = request.getParameter("listcount");
            String cms = request.getParameter("cms");
            String lotId = request.getParameter("lotId");
            int detailid = 0;
            int days = 0;
            List<TblCmsWpDetailHistory> tcwdhs = service.getHistory(Integer.parseInt(request.getParameter("wpId")));
            try {
                for (int i = 0; i < Integer.parseInt(listcount); i++) {
                    days = Integer.parseInt(request.getParameter("nod_" + i));
                    detailid = Integer.parseInt(request.getParameter("detailid_" + i));
                    List<Object> list = service.checkForDate(detailid);
                    if (!list.isEmpty() && list != null) {
                        // if (!DateUtils.gridDateToStr((Date) list.get(0)).split(" ")[0].equalsIgnoreCase(request.getParameter("txtdate_" + i))) {
                        service.updateTable(detailid, days, DateUtils.convertStringtoDate(request.getParameter("txtdate_" + i), "dd-MMM-yyyy"));
                        TblCmsWpDetailHistory wpDetailHistory = new TblCmsWpDetailHistory();
                        wpDetailHistory.setAmendmentFlag(tcwdhs.get(i).getAmendmentFlag() + "edit");
                        wpDetailHistory.setHistoryCount(0);
                        wpDetailHistory.setWpId(tcwdhs.get(i).getWpId());
                        wpDetailHistory.setWpRowId(tcwdhs.get(i).getWpRowId());
                        wpDetailHistory.setWpRate(tcwdhs.get(i).getWpRate());
                        wpDetailHistory.setWpNoOfDays(days);
                        wpDetailHistory.setWpStartDate(new Date());
                        wpDetailHistory.setWpEndDate(DateUtils.convertStringtoDate(request.getParameter("txtdate_" + i), "dd-MMM-yyyy"));
                        wpDetailHistory.setWpQty(tcwdhs.get(i).getWpQty());
                        wpDetailHistory.setDetail(new TblCmsWpDetail(tcwdhs.get(i).getDetail().getWpDetailId()));
                        wpDetailHistory.setActContractDtHistId(0);
                        wpDetailHistory.setTenderTableId(tcwdhs.get(i).getTenderTableId());
                        wpDetailHistory.setCreatedDate(new java.sql.Date(new java.util.Date().getTime()));
                        list1.add(wpDetailHistory);
                        boolean indetailHistory = service.addToDetailHistory(list1);
                        //}

                    }



                }

                response.sendRedirect("officer/NOA.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=edit");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), Integer.parseInt(request.getParameter("tenderId")), "tenderId", EgpModule.Noa.getName(), "Edit Days by PE", "");
            }

        } else if ("preparepr".equals(action)) {
            String max = request.getParameter("size");
            String first = request.getParameter("pageNo");
            boolean forSecondPr = service.forSecondPR(Integer.parseInt(request.getParameter("wpId")));
            try {
                if (procnature.equalsIgnoreCase("goods")) {
                    List<TblCmsWpDetail> list = null;
                    List<Object[]> listt = null;
                    listt = service.getListForEdit(request.getParameter("wpId"), Integer.parseInt(first), Integer.parseInt(max));
                    if ("".equalsIgnoreCase(request.getParameter("isedit"))) {
                        list = service.getDatesByWpId(request.getParameter("wpId"), Integer.parseInt(first), Integer.parseInt(max));
                    }
                    long rowcount = service.getRowcountForBoq(request.getParameter("wpId"));
                    String styleClass = "bgColor-White";
                    if ("".equalsIgnoreCase(request.getParameter("isedit"))) {
                        if (list != null && !list.isEmpty()) {
                            String link = "";
                            int i = 0;
                            for (i = 0; i < list.size(); i++) {


                                if (list.get(i).getAmendmentFlag().equalsIgnoreCase("variationI")) {
                                    out.print("<tr class='" + styleClass + "' style=background-color:#FFFF99>");
                                } else if (list.get(i).getAmendmentFlag().equalsIgnoreCase("variationU")) {
                                    out.print("<tr class='" + styleClass + "' style=background-color:#CDC0B0>");
                                } else {
                                    out.print("<tr class='" + styleClass + "'>");
                                }
                                out.print("<td class=\"t-align-center\">" + list.get(i).getWpSrNo() + "</td>");
                                out.print("<td class=\"t-align-center\">" + list.get(i).getWpDescription() + "</td>");
                                out.print("<td class=\"t-align-center\">" + list.get(i).getWpUom() + "</td>");
                                out.print("<td style=\"text-align :right;\">" + list.get(i).getWpQty() + "</td>");
                                if (forSecondPr) {
                                    out.print("<td style=\"text-align :right;\"><label name=disp_" + i + " id=disp_" + i + ">" + listt.get(i)[4].toString() + "</label> </td>");

                                } else {
                                    out.print("<td style=\"text-align :right;\"><label name=disp_" + i + " id=disp_" + i + ">0</label> </td>");
                                }
                                if (!listt.isEmpty()) {
                                    if (list.get(i).getWpQty().equals(listt.get(i)[4]) || list.get(i).getWpQty().equals(0)) {
                                        out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " style=\"display:none\" readonly /> </td>");

                                    } else {
                                        out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " onkeypress=\"checkEnter(event," + i + ")\" onchange=\"checkPending(" + i + ")\" /> </td>");
                                    }
                                } else {
                                    out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                            + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " onkeypress=\"checkEnter(event," + i + ")\" onchange=\"checkPending(" + i + ")\" /> </td>");
                                }
                                out.print("<td class=\"t-align-center\"><label id=Qtypending_" + i + " name=Qtypending_" + i + "></label></td>");

                                if (!listt.isEmpty()) {
                                    if (list.get(i).getWpQty().equals(listt.get(i)[4]) || list.get(i).getWpQty().equals(0)) {
                                        out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                                + "readonly style=\"display:none\" onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                + "name=txtdate_" + i + " id=txtdate_" + i + " /></td>");
                                    } else {
                                        out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                                + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                + "name=txtdate_" + i + " id=txtdate_" + i + " />"
                                                + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtdate_" + i + "','calc_" + i + "') /></a></td>");
                                    }
                                } else {
                                    out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                            + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                            + "name=txtdate_" + i + " id=txtdate_" + i + " />"
                                            + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                            + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtdate_" + i + "','calc_" + i + "') /></a></td>");
                                }

                                if (!listt.isEmpty()) {
                                    if (list.get(i).getWpQty().equals(listt.get(i)[4]) || list.get(i).getWpQty().equals(0)) {
                                        out.print("<td class=\"t-align-center\"><input type=text  class=formTxtBox_1 "
                                                + "name=remarks_" + i + " style=\"display:none\" id=remarks_" + i + "> </input> </td>");
                                    } else {
                                        out.print("<td class=\"t-align-center\"><input type=text  class=formTxtBox_1 "
                                                + "name=remarks_" + i + " id=remarks_" + i + " onblur=\"validateRemarks(" + i + ")\"> </input> </td>");
                                    }
                                } else {
                                    out.print("<td class=\"t-align-center\"><input type=text  class=formTxtBox_1 "
                                            + "name=remarks_" + i + " id=remarks_" + i + " onblur=\"validateRemarks(" + i + ")\"> </input> </td>");
                                }
                                out.print("<input type=hidden name=detailid_" + i + " id=detailid_" + i + " value=\"" + list.get(i).getWpDetailId() + "\" />");
                                out.print("<input type=hidden name=tableId_" + i + " id=tableId_" + i + " value=\"" + list.get(i).getTenderTableId() + "\" />");
                                out.print("<input type=hidden name=qty_" + i + " id=qty_" + i + " value=\"" + list.get(i).getWpQty() + "\" />");
                                out.print("<input type=hidden name=row_" + i + " id=row_" + i + " value=\"" + list.get(i).getWpRowId() + "\" />");
                                if (!listt.isEmpty()) {
                                    out.print("<input type=hidden name=prRepDetId_" + i + " id=prRepDetId_" + i + " value=\"" + listt.get(i)[10].toString() + "\" />");

                                }
                                if (forSecondPr && !listt.isEmpty()) {
                                    out.print("<input type=hidden name=Qtyaccepted_" + i + " id=Qtyaccepted_" + i + " value=\"" + listt.get(i)[4].toString() + "\" /> ");

                                } else {
                                    out.print("<input type=hidden name=Qtyaccepted_" + i + " id=Qtyaccepted_" + i + " value=0 />");
                                }
                                out.print("</tr>");

                            }
                            if (!listt.isEmpty()) {
                                out.print("<input type=hidden name=prId id=prId value=\"" + listt.get(0)[11].toString() + "\" />");
                                out.print("<input type=hidden name=prReqDate id=prReqDate value=\"" + listt.get(0)[13].toString() + "\" />");
                            }
                            out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");
                            int totalPages = 1;

                            if (list.size() > 0) {
                                int cc = (int) rowcount;
                                totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                                if (totalPages == 0) {
                                    totalPages = 1;
                                }
                            }

                            out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                        }
                    } else {
                        if (listt != null && !listt.isEmpty()) {
                            String link = "";
                            int i = 0;
                            String setblank = "";
                            for (Object[] oob : listt) {


                                if (listt.get(i)[14].toString().equalsIgnoreCase("variationI")) {
                                    out.print("<tr class='" + styleClass + "' style=background-color:#FFFF99>");
                                } else if (listt.get(i)[14].toString().equalsIgnoreCase("variationU")) {
                                    out.print("<tr class='" + styleClass + "' style=background-color:#CDC0B0>");
                                } else {
                                    out.print("<tr class='" + styleClass + "'>");
                                }
                                out.print("<td class=\"t-align-center\">" + oob[0].toString() + "</td>");
                                out.print("<td class=\"t-align-center\">" + oob[1].toString() + "</td>");
                                out.print("<td class=\"t-align-center\">" + oob[2].toString() + "</td>");
                                out.print("<td style=\"text-align :right;\">" + oob[3].toString() + "</td>");
                                out.print("<td style=\"text-align :right;\"><label name=disp_" + i + " id=disp_" + i + ">" + oob[4].toString() + "</label> </td>");
                                if (oob[3].equals(oob[4])) {
                                    out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                            + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " style=\"display:none\" readonly /> </td>");

                                } else {
                                    if (oob[5].toString().equals("0.000")) {
                                        out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " onkeypress=\"checkEnter(event," + i + ")\" onchange=\"checkPending(" + i + ")\"  /> </td>");
                                    } else {
                                        out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " onkeypress=\"checkEnter(event," + i + ")\" onchange=\"checkPending(" + i + ")\" value=\"" + oob[5].toString() + "\" /> </td>");
                                    }
                                }

                                out.print("<td style=\"text-align :right;\"><label id=Qtypending_" + i + " name=Qtypending_" + i + ">" + oob[6].toString() + "</label></td>");

                                if (oob[3].equals(oob[4])) {
                                    out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text  class=formTxtBox_1 "
                                            + "readonly style=\"display:none\" onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                            + "name=txtdate_" + i + " id=txtdate_" + i + " ></input></td>");
                                } else {
                                    if (oob[5].toString().equals("0.000")) {
                                        if (oob[7] != null) {
                                            out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text value=\"" + DateUtils.gridDateToStr((Date) oob[7]).split(" ")[0] + "\" class=formTxtBox_1 "
                                                    + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                    + "name=txtdate_" + i + " id=txtdate_" + i + " ></input>"
                                                    + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                    + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtdate_" + i + "','calc_" + i + "') /></a></td>");

                                        } else {
                                            out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                                    + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                    + "name=txtdate_" + i + " id=txtdate_" + i + " ></input>"
                                                    + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                    + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtdate_" + i + "','calc_" + i + "') /></a></td>");

                                        }
                                    } else {
                                        if (oob[7] != null) {
                                            out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text value=\"" + DateUtils.gridDateToStr((Date) oob[7]).split(" ")[0] + "\" class=formTxtBox_1 "
                                                    + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                    + "name=txtdate_" + i + " id=txtdate_" + i + " ></input>"
                                                    + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                    + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtdate_" + i + "','calc_" + i + "') /></a></td>");

                                        } else {
                                            out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                                    + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                    + "name=txtdate_" + i + " id=txtdate_" + i + " ></input>"
                                                    + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                    + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtdate_" + i + "','calc_" + i + "') /></a></td>");
                                        }
                                    }
                                }

                                if (oob[3].equals(oob[4])) {
                                    out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                            + "name=remarks_" + i + " style=\"display:none\" id=remarks_" + i + " ></input> </td>");
                                } else {
                                    if ("".equalsIgnoreCase(oob[8].toString())) {
                                        out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                + "name=remarks_" + i + " id=remarks_" + i + " onblur=\"validateRemarks(" + i + ")\"></input> </td>");

                                    } else {
                                        out.print("<td class=\"t-align-center\"><input type=text value=\"" + oob[8].toString() + "\" class=formTxtBox_1 "
                                                + "name=remarks_" + i + " id=remarks_" + i + " onblur=\"validateRemarks(" + i + ")\"></input> </td>");

                                    }
                                }
                                out.print("<input type=hidden name=tableId_" + i + " id=tableId_" + i + " value=\"" + oob[12].toString() + "\" />");
                                out.print("<input type=hidden name=qty_" + i + " id=qty_" + i + " value=\"" + oob[3].toString() + "\" />");
                                out.print("<input type=hidden name=row_" + i + " id=row_" + i + " value=\"" + oob[9].toString() + "\" />");

                                out.print("<input type=hidden name=Qtyaccepted_" + i + " id=Qtyaccepted_" + i + " value=\"" + oob[4].toString() + "\" />");
                                out.print("<input type=hidden name=prRepDetId_" + i + " id=prRepDetId_" + i + " value=\"" + oob[10].toString() + "\" />");
                                out.print("</tr>");
                                i++;
                            }
                            out.print("<input type=hidden name=prId id=prId value=\"" + listt.get(0)[11].toString() + "\" />");

                            out.print("<input type=hidden name=prReqDate id=prReqDate value=\"" + listt.get(0)[13].toString() + "\" />");
                            if (i == 0) {
                                i++;
                            }
                            out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");
                            int totalPages = 1;

                            if (listt.size() > 0) {
                                int cc = (int) rowcount;
                                totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                                if (totalPages == 0) {
                                    totalPages = 1;
                                }
                            }
                            out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                        }
                    }
                } else {

                    List<Object[]> list = null;
                    List<SPCommonSearchDataMore> listt = null;
                    listt = service.getListForEditForWorks(request.getParameter("wpId"), Integer.parseInt(first), Integer.parseInt(max));
                    if ("".equalsIgnoreCase(request.getParameter("isedit"))) {
                        list = service.getDatesByWpIdForWorks(request.getParameter("wpId"), Integer.parseInt(first), Integer.parseInt(max));
                    }
                    long rowcount = service.getRowcountForBoq(request.getParameter("wpId"));
                    String styleClass = "bgColor-White";
                    if ("".equalsIgnoreCase(request.getParameter("isedit"))) {
                        if (list != null && !list.isEmpty()) {
                            String link = "";
                            int i = 0;
                            if (!forSecondPr) {
                                for (i = 0; i < list.size(); i++) {

                                    if (list.get(i)[12].toString().equalsIgnoreCase("variationI")) {
                                        out.print("<tr class='" + styleClass + "' style=background-color:#FFFF99>");
                                    } else if (list.get(i)[12].toString().equalsIgnoreCase("variationU")) {
                                        out.print("<tr class='" + styleClass + "' style=background-color:#CDC0B0>");
                                    } else {
                                        out.print("<tr class='" + styleClass + "'>");
                                    }
                                    out.print("<td class=\"t-align-center\">" + list.get(i)[0] + "</td>");
                                    out.print("<td class=\"t-align-center\">" + list.get(i)[1] + "</td>");
                                    out.print("<td class=\"t-align-center\">" + list.get(i)[2] + "</td>");
                                    out.print("<td style=\"text-align :right;\">" + list.get(i)[3] + "</td>");
                                    out.print("<td style=\"text-align :right;\">" + list.get(i)[4] + "</td>");
                                    if (forSecondPr) {
                                        out.print("<td style=\"text-align :right;\"><label name=disp_" + i + " id=disp_" + i + ">" + listt.get(i).getFieldName6() + "</label> </td>");

                                    } else {
                                        out.print("<td style=\"text-align :right;\"><label name=disp_" + i + " id=disp_" + i + ">0</label> </td>");
                                    }
                                    if (!listt.isEmpty()) {
                                        if (list.get(i)[4].equals(listt.get(i).getFieldName6())) {
                                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                    + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " style=\"display:none\" readonly  /> </td>");

                                        } else {
                                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                    + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " onkeypress=\"checkEnter(event," + i + ")\" onchange=\"checkPending(" + i + ")\" /> </td>");
                                        }
                                    } else {
                                        if (list.get(i)[4].toString().equals("0.000")) {
                                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                    + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " style=\"display:none\" readonly  /> </td>");
                                        } else {
                                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                    + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " onkeypress=\"checkEnter(event," + i + ")\" onchange=\"checkPending(" + i + ")\" /> </td>");
                                        }

                                    }
                                    out.print("<td style=\"text-align :right;\"><label id=Qtypending_" + i + " name=Qtypending_" + i + "></label></td>");



                                    if (!listt.isEmpty()) {
                                        if (list.get(i)[4].equals(listt.get(i).getFieldName6())) {
                                            out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                                    + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                    + "name=txtdate_" + i + " style=\"display:none\" id=txtdate_" + i + " /></td>");

                                        } else {
                                            out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                                    + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                    + "name=txtdate_" + i + " id=txtdate_" + i + " />"
                                                    + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                    + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtdate_" + i + "','calc_" + i + "') /></a></td>");
                                        }
                                    } else {
                                        if (list.get(i)[4].toString().equals("0.000")) {
                                            out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                                    + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                    + "name=txtdate_" + i + " style=\"display:none\" id=txtdate_" + i + " /></td>");
                                        } else {
                                            out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                                    + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                    + "name=txtdate_" + i + " id=txtdate_" + i + " />"
                                                    + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                    + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtdate_" + i + "','calc_" + i + "') /></a></td>");
                                        }

                                    }


                                    if (!listt.isEmpty()) {
                                        if (list.get(i)[4].equals(listt.get(i).getFieldName6())) {
                                            out.print("<td class=\"t-align-center\"><input type=text  class=formTxtBox_1 "
                                                    + "name=remarks_" + i + " style=\"display:none\" id=remarks_" + i + "> </input> </td>");

                                        } else {
                                            out.print("<td class=\"t-align-center\"><input type=text  class=formTxtBox_1 "
                                                    + "name=remarks_" + i + " id=remarks_" + i + " onblur=\"validateRemarks(" + i + ")\"> </input> </td>");
                                        }
                                    } else {
                                        if (list.get(i)[4].toString().equals("0.000")) {
                                            out.print("<td class=\"t-align-center\"><input type=text  class=formTxtBox_1 "
                                                    + "name=remarks_" + i + " style=\"display:none\" id=remarks_" + i + "> </input> </td>");
                                        } else {
                                            out.print("<td class=\"t-align-center\"><input type=text  class=formTxtBox_1 "
                                                    + "name=remarks_" + i + " id=remarks_" + i + " onblur=\"validateRemarks(" + i + ")\"> </input> </td>");
                                        }

                                    }
                                    out.print("<input type=hidden name=detailid_" + i + " id=detailid_" + i + " value=\"" + list.get(i)[5] + "\" />");
                                    out.print("<input type=hidden name=tableId_" + i + " id=tableId_" + i + " value=\"" + list.get(i)[10] + "\" />");
                                    out.print("<input type=hidden name=qty_" + i + " id=qty_" + i + " value=\"" + list.get(i)[4] + "\" />");
                                    out.print("<input type=hidden name=row_" + i + " id=row_" + i + " value=\"" + list.get(i)[9] + "\" />");
                                    if (!listt.isEmpty()) {
                                        out.print("<input type=hidden name=prRepDetId_" + i + " id=prRepDetId_" + i + " value=\"" + listt.get(i).getFieldName12() + "\" />");

                                    }
                                    if (forSecondPr && !listt.isEmpty()) {
                                        out.print("<input type=hidden name=Qtyaccepted_" + i + " id=Qtyaccepted_" + i + " value=\"" + listt.get(i).getFieldName6() + "\" /> ");

                                    } else {
                                        out.print("<input type=hidden name=Qtyaccepted_" + i + " id=Qtyaccepted_" + i + " value=0 />");
                                    }
                                    out.print("</tr>");

                                }
                            } else {
                                for (SPCommonSearchDataMore oob : listt) {

                                    if (list.get(i)[12].toString().equalsIgnoreCase("variationI")) {
                                        out.print("<tr class='" + styleClass + "' style=background-color:#FFFF99>");
                                    } else if (list.get(i)[12].toString().equalsIgnoreCase("variationU")) {
                                        out.print("<tr class='" + styleClass + "' style=background-color:#CDC0B0>");
                                    } else {
                                        out.print("<tr class='" + styleClass + "'>");
                                    }
                                    out.print("<td class=\"t-align-center\">" + oob.getFieldName1() + "</td>");
                                    out.print("<td class=\"t-align-center\">" + oob.getFieldName2() + "</td>");
                                    out.print("<td class=\"t-align-center\">" + oob.getFieldName3() + "</td>");
                                    out.print("<td class=\"t-align-center\">" + oob.getFieldName4() + "</td>");
                                    out.print("<td style=\"text-align :right;\">" + oob.getFieldName5() + "</td>");
                                    out.print("<td style=\"text-align :right;\"><label name=disp_" + i + " id=disp_" + i + ">" + oob.getFieldName6() + "</label> </td>");


                                    if (oob.getFieldName6() != null) {
                                        if (oob.getFieldName5().equals(oob.getFieldName6())) {
                                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                    + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " style=\"display:none\" readonly  /> </td>");
                                        } else {

                                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                    + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " onkeypress=\"checkEnter(event," + i + ")\" onchange=\"checkPending(" + i + ")\"  /> </td>");
                                        }
                                    }
                                    out.print("<td style=\"text-align :right;\"><label id=Qtypending_" + i + " name=Qtypending_" + i + ">" + oob.getFieldName9() + "</label></td>");


                                    if (oob.getFieldName6() != null) {
                                        if (oob.getFieldName5().equals(oob.getFieldName6())) {
                                            out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                                    + "readonly style=\"display:none\" onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                    + "name=txtdate_" + i + " id=txtdate_" + i + " ></input></td>");
                                        } else {

                                            out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                                    + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                    + "name=txtdate_" + i + " id=txtdate_" + i + " ></input>"
                                                    + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                    + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtdate_" + i + "','calc_" + i + "') /></a></td>");
                                        }
                                    }


                                    if (oob.getFieldName6() != null) {
                                        if (oob.getFieldName5().equals(oob.getFieldName6())) {
                                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                    + "name=remarks_" + i + " style=\"display:none\" id=remarks_" + i + " ></input> </td>");

                                        } else {

                                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                    + "name=remarks_" + i + " id=remarks_" + i + " onblur=\"validateRemarks(" + i + ")\"></input> </td>");
                                        }
                                    }
                                    out.print("<input type=hidden name=tableId_" + i + " id=tableId_" + i + " value=\"" + oob.getFieldName8() + "\" />");
                                    out.print("<input type=hidden name=qty_" + i + " id=qty_" + i + " value=\"" + oob.getFieldName5() + "\" />");
                                    out.print("<input type=hidden name=row_" + i + " id=row_" + i + " value=\"" + oob.getFieldName14() + "\" />");

                                    out.print("<input type=hidden name=Qtyaccepted_" + i + " id=Qtyaccepted_" + i + " value=\"" + oob.getFieldName6() + "\" />");

                                    out.print("</tr>");
                                    out.print("<input type=hidden name=prId id=prId value=\"" + listt.get(i).getFieldName13() + "\" />");
                                    out.print("<input type=hidden name=prRepDetId_" + i + " id=prRepDetId_" + i + " value=\"" + listt.get(i).getFieldName12() + "\" />");
                                    i++;
                                }
                            }


                            if (!listt.isEmpty()) {
                                out.print("<input type=hidden name=prId id=prId value=\"" + listt.get(i - 1).getFieldName13() + "\" />");
                                // out.print("<input type=hidden name=prReqDate id=prReqDate value=" + listt.get(0)[14].toString() + " />");
                            }
                            out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");
                            int totalPages = 1;

                            if (list.size() > 0) {
                                int cc = (int) rowcount;
                                totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                                if (totalPages == 0) {
                                    totalPages = 1;
                                }
                            }

                            out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                        }
                    } else {
                        if (listt != null && !listt.isEmpty()) {
                            String link = "";
                            int i = 0;
                            for (SPCommonSearchDataMore oob : listt) {

                                if (oob.getFieldName15().equalsIgnoreCase("variationI")) {
                                    out.print("<tr class='" + styleClass + "' style=background-color:#FFFF99>");
                                } else if (oob.getFieldName15().equalsIgnoreCase("variationU")) {
                                    out.print("<tr class='" + styleClass + "' style=background-color:#CDC0B0>");
                                } else {
                                    out.print("<tr class='" + styleClass + "'>");
                                }
                                out.print("<td class=\"t-align-center\">" + oob.getFieldName1() + "</td>");
                                out.print("<td class=\"t-align-center\">" + oob.getFieldName2() + "</td>");
                                out.print("<td class=\"t-align-center\">" + oob.getFieldName3() + "</td>");
                                out.print("<td class=\"t-align-center\">" + oob.getFieldName4() + "</td>");
                                out.print("<td style=\"text-align :right;\">" + oob.getFieldName5() + "</td>");
                                out.print("<td style=\"text-align :right;\"><label name=disp_" + i + " id=disp_" + i + ">" + oob.getFieldName6() + "</label> </td>");


                                if (oob.getFieldName6() != null) {
                                    if (oob.getFieldName5().equals(oob.getFieldName6())) {
                                        out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " style=\"display:none\" readonly  /> </td>");

                                    } else {
                                        if (oob.getFieldName7().toString().equals("0.000")) {
                                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                    + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " onkeypress=\"checkEnter(event," + i + ")\" onchange=\"checkPending(" + i + ")\"  /> </td>");
                                        } else {
                                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                    + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " onkeypress=\"checkEnter(event," + i + ")\" onchange=\"checkPending(" + i + ")\" value=\"" + oob.getFieldName7() + "\" /> </td>");
                                        }
                                    }
                                }

                                out.print("<td style=\"text-align :right;\"><label id=Qtypending_" + i + " name=Qtypending_" + i + ">" + oob.getFieldName9() + "</label></td>");
                                if (oob.getFieldName6() != null) {
                                    if (oob.getFieldName5().equals(oob.getFieldName6())) {
                                        out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text value=\"" + oob.getFieldName10() + "\" class=formTxtBox_1 "
                                                + "readonly style=\"display:none\" onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                + "name=txtdate_" + i + " id=txtdate_" + i + " ></input></td>");
                                    } else {
                                        if (oob.getFieldName7().toString().equals("0.000")) {
                                            if (!"".equalsIgnoreCase(oob.getFieldName10())) {
                                                out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text value=\"" + oob.getFieldName10().split(" ")[0] + "\" class=formTxtBox_1 "
                                                        + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                        + "name=txtdate_" + i + " id=txtdate_" + i + " ></input>"
                                                        + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                        + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtdate_" + i + "','calc_" + i + "') /></a></td>");

                                            } else {
                                                out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                                        + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                        + "name=txtdate_" + i + " id=txtdate_" + i + " ></input>"
                                                        + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                        + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtdate_" + i + "','calc_" + i + "') /></a></td>");

                                            }
                                        } else {
                                            if (!"".equalsIgnoreCase(oob.getFieldName10())) {
                                                out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text value=\"" + oob.getFieldName10().split(" ")[0] + "\" class=formTxtBox_1 "
                                                        + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                        + "name=txtdate_" + i + " id=txtdate_" + i + " ></input>"
                                                        + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                        + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtdate_" + i + "','calc_" + i + "') /></a></td>");

                                            } else {
                                                out.print("<td class=\"t-align-center\"><input  onchange =\"checkDate(this)\" type=text class=formTxtBox_1 "
                                                        + "readonly onclick =GetCal('txtdate_" + i + "','txtdate_" + i + "') "
                                                        + "name=txtdate_" + i + " id=txtdate_" + i + " ></input>"
                                                        + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                        + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtdate_" + i + "','calc_" + i + "') /></a></td>");

                                            }
                                        }
                                    }
                                }

                                if (oob.getFieldName6() != null) {
                                    if (oob.getFieldName5().equals(oob.getFieldName6())) {
                                        out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                + "name=remarks_" + i + " style=\"display:none\" id=remarks_" + i + " ></input> </td>");
                                    } else {
                                        if (oob.getFieldName7().toString().equals("0.000")) {
                                            if ("".equalsIgnoreCase(oob.getFieldName11())) {
                                                out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                        + "name=remarks_" + i + " id=remarks_" + i + " onblur=\"validateRemarks(remarks_" + i + ")\"></input> </td>");

                                            } else {
                                                out.print("<td class=\"t-align-center\"><input type=text value=\"" + oob.getFieldName11() + "\" class=formTxtBox_1 "
                                                        + "name=remarks_" + i + " id=remarks_" + i + " onblur=\"validateRemarks(" + i + ")\"></input> </td>");

                                            }
                                        } else {
                                            if ("".equalsIgnoreCase(oob.getFieldName11())) {
                                                out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                        + "name=remarks_" + i + " id=remarks_" + i + " onblur=\"validateRemarks(" + i + ")\"></input> </td>");

                                            } else {
                                                out.print("<td class=\"t-align-center\"><input type=text value=\"" + oob.getFieldName11() + "\" class=formTxtBox_1 "
                                                        + "name=remarks_" + i + " id=remarks_" + i + " onblur=\"validateRemarks(" + i + ")\"></input> </td>");

                                            }
                                        }
                                    }
                                }
                                out.print("<input type=hidden name=tableId_" + i + " id=tableId_" + i + " value=\"" + oob.getFieldName8() + "\" />");
                                out.print("<input type=hidden name=qty_" + i + " id=qty_" + i + " value=\"" + oob.getFieldName5() + "\" />");
                                out.print("<input type=hidden name=row_" + i + " id=row_" + i + " value=\"" + oob.getFieldName14() + "\" />");

                                out.print("<input type=hidden name=Qtyaccepted_" + i + " id=Qtyaccepted_" + i + " value=\"" + oob.getFieldName6() + "\" />");

                                out.print("</tr>");
                                out.print("<input type=hidden name=prId id=prId value=\"" + listt.get(i).getFieldName13() + "\" />");
                                out.print("<input type=hidden name=prRepDetId_" + i + " id=prRepDetId_" + i + " value=\"" + listt.get(i).getFieldName12() + "\" />");
                                i++;
                            }
                            if (i == 0) {
                                i++;
                            }

                            out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");
                            int totalPages = 1;
                            if (listt.size() > 0) {
                                int cc = (int) rowcount;
                                totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                                if (totalPages == 0) {
                                    totalPages = 1;
                                }
                            }
                            out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                        }
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if ("variorder".equalsIgnoreCase(action)) {
            String max = request.getParameter("size");
            String first = request.getParameter("pageNo");
            boolean flaB = service.checkForPrEntryInWprks(Integer.parseInt(request.getParameter("wpId")));
            List<Object[]> list = null;
            String styleClass = "";
            long rowcount = service.getRowcountForBoq(request.getParameter("wpId"));
            List<SPCommonSearchDataMore> listt = null;
            try {
                if (flaB) {
                    listt = service.getListForEditForWorks(request.getParameter("wpId"), Integer.parseInt(first), Integer.parseInt(max));
                    if (listt != null && !listt.isEmpty()) {
                        String link = "";
                        int i = 0;
                        BigDecimal grandTotal = new BigDecimal(0);

                        for (i = 0; i < listt.size(); i++) {
                            if (i % 2 == 0) {
                                styleClass = "bgColor-white";
                            } else {
                                styleClass = "bgColor-Green";
                            }
                            if (listt.get(i).getFieldName15().equalsIgnoreCase("variationI")) {
                                out.print("<tr class='" + styleClass + "' style=background-color:#FFFF99>");
                            } else if (listt.get(i).getFieldName15().equalsIgnoreCase("variationU")) {
                                out.print("<tr class='" + styleClass + "' style=background-color:#CDC0B0>");
                            } else {
                                out.print("<tr class='" + styleClass + "'>");
                            }
                            out.print("<td class=\"t-align-center\" ></td>");
                            out.print("<td class=\"t-align-center\">" + listt.get(i).getFieldName1() + "</td>");
                            out.print("<td class=\"t-align-center\">" + listt.get(i).getFieldName2() + "</td>");
                            out.print("<td class=\"t-align-center\">" + listt.get(i).getFieldName3() + "</td>");
                            out.print("<td class=\"t-align-center\">" + listt.get(i).getFieldName4() + "</td>");
                            out.print("<td  class=\"t-align-left\"><input  type=text class=formTxtBox_1 "
                                    + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " value=\"" + new BigDecimal(listt.get(i).getFieldName5()).subtract(new BigDecimal(listt.get(i).getFieldName6())) + "\" onchange=checkqty(" + i + ") /></td>");
                            out.print("<td style=\"text-align:right;\"><input  type=hidden class=formTxtBox_1 "
                                    + "name=rates_" + i + " id=rates_" + i + " value=\"" + listt.get(i).getFieldName19() + "\" onchange=checkqty(" + i + ") />" + listt.get(i).getFieldName19() + "</td>");
                            out.print("<td class=\"t-align-center\">" + listt.get(i).getFieldName16().split(" ")[0] + "</td>");
                            out.print("<td class=\"t-align-center\">" + listt.get(i).getFieldName17().split(" ")[0] + "</td>");
                            out.print("<td style=\"text-align:right;\"><span id=Totall_" + i + " >"
                                    + new BigDecimal(listt.get(i).getFieldName5()).subtract(new BigDecimal(listt.get(i).getFieldName6())).multiply(new BigDecimal(listt.get(i).getFieldName19())).setScale(3, 0)
                                    + "</span></td>");
                            grandTotal = grandTotal.add(new BigDecimal(listt.get(i).getFieldName5()).subtract(new BigDecimal(listt.get(i).getFieldName6())).multiply(new BigDecimal(listt.get(i).getFieldName19())).setScale(3, 0));

                            out.print("</tr>");

                            out.print("<input type=hidden name=detailid_" + i + " id=detailid_" + i + " value=\"" + listt.get(i).getFieldName18() + "\" />");
                            out.print("<input type=hidden name=rowId_" + i + " id=rowId_" + i + " value=\"" + listt.get(i).getFieldName14() + "\" />");
                            out.print("<input type=hidden name=tendertableId_" + i + " id=tendertableId_" + i + " value=\"" + listt.get(i).getFieldName8() + "\" />");
                            out.print("<input type=hidden name=rates_" + i + " id=rates_" + i + " value=\"" + listt.get(i).getFieldName19() + "\" />");


                            out.print("<input type=hidden name=qtyaccepted_" + i + " id=qtyaccepted_" + i + " value=\"" + listt.get(i).getFieldName6() + "\" />");

                        }
                        if (listt.size() % 2 == 0) {
                            out.print("<tr class='bgColor-white'><td colspan=\"9\" class='ff' style='text-align:right;' >Grand Total :</td><td><span id='grandTotalCell'>" + grandTotal + "</span></td></tr>");
                        } else {
                            out.print("<tr class='bgColor-Green'><td colspan=\"9\" class='ff' style='text-align:right;'>Grand Total :</td><td><span id='grandTotalCell'>" + grandTotal + "</span></td></tr>");
                        }

                        out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");

                        int totalPages = 1;

                        if (listt.size() > 0) {
                            int cc = (int) rowcount;
                            totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                    } else {
                        out.print("<tr>");
                        out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"9\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        out.print("</tr>");
                    }
                } else {
                    list = service.getDatesByWpIdForWorks(request.getParameter("wpId"), Integer.parseInt(first), Integer.parseInt(max));
                    if (list != null && !list.isEmpty()) {
                        String link = "";
                        int i = 0;
                        BigDecimal grandTotal = new BigDecimal(0);

                        for (i = 0; i < list.size(); i++) {
                            if (i % 2 == 0) {
                                styleClass = "bgColor-white";
                            } else {
                                styleClass = "bgColor-Green";
                            }
                            if (list.get(i)[12].toString().equalsIgnoreCase("variationI")) {
                                out.print("<tr class='" + styleClass + "' style=background-color:#FFFF99>");
                            } else if (list.get(i)[12].toString().equalsIgnoreCase("variationU")) {
                                out.print("<tr class='" + styleClass + "' style=background-color:#CDC0B0>");
                            } else {
                                out.print("<tr class='" + styleClass + "'>");
                            }
                            out.print("<td class=\"t-align-center\" ></td>");
                            out.print("<td class=\"t-align-center\"><label>" + list.get(i)[0] + "</label></td>");
                            out.print("<td class=\"t-align-center\"><label>" + list.get(i)[1] + "</label></td>");
                            out.print("<td class=\"t-align-center\"><label>" + list.get(i)[2] + "</label></td>");
                            out.print("<td class=\"t-align-center\"><label>" + list.get(i)[3] + "</label></td>");
                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 name=Qtyenter_" + i + " id=Qtyenter_" + i + " value=\"" + list.get(i)[4] + "\" onchange=checkqty(" + i + ") /></td>");
                            out.print("<td style=\"text-align:right;\"><input  type=hidden class=formTxtBox_1 "
                                    + "name=rates_" + i + " id=rates_" + i + " value=\"" + list.get(i)[11] + "\" onchange=checkqty(" + i + ") />" + list.get(i)[11] + "</td>");
                            //out.print("<td style=\"text-align:right;\"><label>" + list.get(i)[11] + "</label></td>");
                            out.print("<td class=\"t-align-center\"><label>" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i)[6]).split(" ")[0] + "</label></td>");
                            out.print("<td class=\"t-align-center\"><label>" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i)[7]).split(" ")[0] + "</label></td>");
                            out.print("<td style=\"text-align:right;\"><span id=Totall_" + i + " >"
                                    + new BigDecimal(list.get(i)[4].toString()).multiply(new BigDecimal(list.get(i)[11].toString())).setScale(3, 0)
                                    + "</span></td>");

                            grandTotal = grandTotal.add(new BigDecimal(list.get(i)[4].toString()).multiply(new BigDecimal(list.get(i)[11].toString())).setScale(3, 0));

                            out.print("</tr>");
                            out.print("<input type=hidden name=detailid_" + i + " id=detailid_" + i + " value=\"" + list.get(i)[5] + "\" />");
                        }

                        if (list.size() % 2 == 0) {
                            out.print("<tr class='bgColor-white'><td colspan=\"9\" class='ff' style='text-align:right;' >Grand Total :</td><td><span id='grandTotalCell'>" + grandTotal + "</span></td></tr>");
                        } else {
                            out.print("<tr class='bgColor-Green'><td colspan=\"9\" class='ff' style='text-align:right;'>Grand Total :</td><td><span id='grandTotalCell'>" + grandTotal + "</span></td></tr>");
                        }
                        out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");
                        int totalPages = 1;

                        if (list.size() > 0) {
                            int cc = (int) rowcount;
                            totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                    } else {
                        out.print("<tr>");
                        out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"9\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        out.print("</tr>");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if ("editvariorder".equalsIgnoreCase(action)) {
            String max = request.getParameter("size");
            String first = request.getParameter("pageNo");
            int varId = Integer.parseInt(request.getParameter("varId"));
            boolean flaB = service.checkForPrEntryInWprks(Integer.parseInt(request.getParameter("wpId")));
            List<TblCmsTrackVariation> list = null;
            String styleClass = "";
            long rowcount = service.getRowcountForBoq(request.getParameter("wpId"));
            list = service.editVariOrder(varId + "", Integer.parseInt(first), Integer.parseInt(max));
            try {
                if (list != null && !list.isEmpty()) {
                    String link = "";
                    int i = 0;
                    for (i = 0; i < list.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }
                        if (list.get(i).getFlag().equalsIgnoreCase("I")) {
                            out.print("<tr class='" + styleClass + "' style=background-color:#FFFF99>");

                            out.print("<td class=\"t-align-center\" >"
                                    + "<input class=formTxtBox_1 type=checkbox name=chk" + i + "  id=chk" + i + " /></td>");
                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                    + "name=srno_" + i + " id=srno_" + i + " value=\"" + list.get(i).getSrno() + "\" /></td>");
                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                    + "name=group_" + i + " id=group_" + i + " value=\"" + list.get(i).getGroupname() + "\" /></td>");
                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                    + "name=desc_" + i + " id=desc_" + i + " value=\"" + list.get(i).getDescription() + "\" /></td>");
                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                    + "name=uom_" + i + " id=uom_" + i + " value=\"" + list.get(i).getUom() + "\" /></td>");
                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                    + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " onchange=checkqty(" + i + ") value=\"" + list.get(i).getQty() + "\" /></td>");
                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                    + "name=rate_" + i + " id=rate_" + i + " onchange=checkrate(" + i + ")  value=\"" + list.get(i).getRate() + "\" /></td>");
                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                    + "readonly onchange=clearField(" + i + ") onclick =GetCal('txtsdate_" + i + "','txtsdate_" + i + "'," + i + ") "
                                    + "name=txtsdate_" + i + " id=txtsdate_" + i + " "
                                    + "value=\"" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i).getStartDate()).split(" ")[0] + "\" />"
                                    + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                    + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtsdate_" + i + "','calc_" + i + "'," + i + ") /></a></td>");
                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                    + "readonly onchange=clearField(" + i + ") onclick =GetCal('txtedate_" + i + "','txtedate_" + i + "'," + i + ") "
                                    + "name=txtedate_" + i + " id=txtedate_" + i + " "
                                    + "value=\"" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i).getEndDate()).split(" ")[0] + "\" />"
                                    + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calcc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                    + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtedate_" + i + "','calcc_" + i + "'," + i + ") /></a></td>");
                            out.print("</tr>");
                        } else {
                            out.print("<tr class='" + styleClass + "' style=background-color:#CDC0B0>");
                            out.print("<td class=\"t-align-center\" ></td>");
                            out.print("<td class=\"t-align-center\">" + list.get(i).getSrno() + "</td>");
                            out.print("<td class=\"t-align-center\">" + list.get(i).getGroupname() + "</td>");
                            out.print("<td class=\"t-align-center\">" + list.get(i).getDescription() + "</td>");
                            out.print("<td class=\"t-align-center\">" + list.get(i).getUom() + "</td>");

                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                    + "name=Qtyenter_" + i + " id=Qtyenter_" + i + " value=\"" + list.get(i).getQty() + "\" onchange=checkqty(" + i + ") /></td>");

                            out.print("<td class=\"t-align-center\">" + list.get(i).getRate() + "</td>");
                            out.print("<td class=\"t-align-center\">" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i).getStartDate()).split(" ")[0] + "</td>");
                            out.print("<td class=\"t-align-center\">" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i).getEndDate()).split(" ")[0] + "</td>");
                            out.print("</tr>");

                        }

                        out.print("<input type=hidden name=trackid_" + i + " id=trackid_" + i + " value=\"" + list.get(i).getVariTrackId() + "\" />");
                        out.print("<input type=hidden name=flag_" + i + " id=flag_" + i + " value=\"" + list.get(i).getFlag() + "\" />");
                        out.print("<input type=hidden name=rowId_" + i + " id=rowId_" + i + " value=\"" + list.get(i).getRowId() + "\" />");
                        out.print("<input type=hidden name=detail_" + i + " id=detail_" + i + " value=\"" + list.get(i).getWpdetailid() + "\" />");
                        out.print("<input type=hidden name=variid_" + i + " id=variid_" + i + " value=\"" + list.get(i).getVariOrdId() + "\" />");
                        out.print("<input type=hidden name=tendertableId_" + i + " id=tendertableId_" + i + " value=\"" + list.get(i).getTenderTableId() + "\" />");

                    }
                    out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");
                    int totalPages = 1;

                    if (list.size() > 0) {
                        int cc = (int) rowcount;
                        totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                } else {
                    out.print("<tr>");
                    out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"9\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if ("viewvariorder".equalsIgnoreCase(action)) {
            String max = request.getParameter("size");
            int varId = Integer.parseInt(request.getParameter("varId"));
            String first = request.getParameter("pageNo");
            List<TblCmsTrackVariation> list = null;
            String styleClass = "";
            long rowcount = service.getRowcountForBoq(request.getParameter("wpId"));
            list = service.editVariOrder(varId + "", Integer.parseInt(first), Integer.parseInt(max));
            try {
                if (list != null && !list.isEmpty()) {
                    String link = "";
                    int i = 0;
                    for (i = 0; i < list.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }
                        if (list.get(i).getFlag().equalsIgnoreCase("I")) {
                            out.print("<tr class='" + styleClass + "' style=background-color:#FFFF99>");
                        } else {
                            out.print("<tr class='" + styleClass + "' style=background-color:#CDC0B0>");
                        }
                        out.print("<td class=\"t-align-center\"><label>" + list.get(i).getSrno() + "</label></td>");
                        out.print("<input type=\"hidden\" name=\"srno_" + i + "\" id=\"srno_" + i + "\" value=\"" + list.get(i).getSrno() + "\" />");
                        out.print("<td class=\"t-align-center\"><label>" + list.get(i).getGroupname() + "</label></td>");
                        out.print("<input type=\"hidden\" name=\"group_" + i + "\" id=\"group_" + i + "\" value=\"" + list.get(i).getGroupname() + "\" />");
                        out.print("<td class=\"t-align-center\"><label>" + list.get(i).getDescription() + "</label></td>");
                        out.print("<input type=\"hidden\" name=\"desc_" + i + "\" id=\"desc_" + i + "\" value=\"" + list.get(i).getDescription() + "\" />");
                        out.print("<td class=\"t-align-center\"><label>" + list.get(i).getUom() + "</label></td>");
                        out.print("<input type=\"hidden\" name=\"uom_" + i + "\" id=\"uom_" + i + "\" value=\"" + list.get(i).getUom() + "\" />");
                        out.print("<td class=\"t-align-center\"><label>" + list.get(i).getQty() + "</label></td>");
                        out.print("<input type=hidden name=Qtyenter_" + i + " id=Qtyenter_" + i + " value=" + list.get(i).getQty() + " />");
                        out.print("<td class=\"t-align-center\"><label>" + list.get(i).getRate() + "</label></td>");
                        out.print("<input type=hidden name=rate_" + i + " id=rate_" + i + " value=\"" + list.get(i).getRate() + "\" />");
                        out.print("<td class=\"t-align-center\"><label>" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i).getStartDate()).split(" ")[0] + "</label></td>");
                        out.print("<input type=hidden name=txtsdate_" + i + " id=txtsdate_" + i + " value=\"" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i).getStartDate()).split(" ")[0] + "\" />");
                        out.print("<td class=\"t-align-center\"><label>" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i).getEndDate()).split(" ")[0] + "</label></td>");
                        out.print("<input type=hidden name=txtedate_" + i + " id=txtedate_" + i + " value=\"" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i).getEndDate()).split(" ")[0] + "\" />");
                        out.print("</tr>");

                        out.print("<input type=hidden name=trackid_" + i + " id=trackid_" + i + " value=\"" + list.get(i).getVariTrackId() + "\" />");
                        out.print("<input type=hidden name=flag_" + i + " id=flag_" + i + " value=\"" + list.get(i).getFlag() + "\" />");
                        out.print("<input type=hidden name=rowId_" + i + " id=rowId_" + i + " value=\"" + list.get(i).getRowId() + "\" />");
                        out.print("<input type=hidden name=detail_" + i + " id=detail_" + i + " value=\"" + list.get(i).getWpdetailid() + "\" />");
                        out.print("<input type=hidden name=variid_" + i + " id=variid_" + i + " value=\"" + list.get(i).getVariOrdId() + "\" />");
                        out.print("<input type=hidden name=tendertableId_" + i + " id=tendertableId_" + i + " value=\"" + list.get(i).getTenderTableId() + "\" />");
                    }
                    out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");
                    int totalPages = 1;

                    if (list.size() > 0) {
                        int cc = (int) rowcount;
                        totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                } else {
                    out.print("<tr>");
                    out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"9\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Variation_Order.getName(), "View Variation Order", "PE Side");
            }

        } else if ("viewvariorderhistory".equalsIgnoreCase(action)) {
            List<TblCmsTrackVariation> list = null;
            String styleClass = "";
            long rowcount = service.getRowcountForBoq(request.getParameter("wpId"));
            list = service.viewVariationOderHistory(request.getParameter("varId"));
            try {
                if (list != null && !list.isEmpty()) {
                    String link = "";
                    int i = 0;
                    for (i = 0; i < list.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }
                        if (list.get(i).getFlag().equalsIgnoreCase("I")) {
                            out.print("<tr class='" + styleClass + "' style=background-color:#FFFF99>");
                        } else {
                            out.print("<tr class='" + styleClass + "' style=background-color:#CDC0B0>");
                        }
                        out.print("<td class=\"t-align-center\"><label>" + list.get(i).getSrno() + "</label></td>");
                        out.print("<td class=\"t-align-center\"><label>" + list.get(i).getGroupname() + "</label></td>");
                        out.print("<td class=\"t-align-center\"><label>" + list.get(i).getDescription() + "</label></td>");
                        out.print("<td class=\"t-align-center\"><label>" + list.get(i).getUom() + "</label></td>");
                        out.print("<td style=\"text-align :right;\"><label>" + list.get(i).getQty() + "</label></td>");
                        out.print("<td class=\"t-align-center\"><label>" + list.get(i).getRate() + "</label></td>");
                        out.print("<td class=\"t-align-center\"><label>" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i).getStartDate()).split(" ")[0] + "</label></td>");
                        out.print("<td class=\"t-align-center\"><label>" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i).getEndDate()).split(" ")[0] + "</label></td>");
                        out.print("</tr>");
                    }
//                out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");
//                int totalPages = 1;
//                if (list.size() > 0) {
//                    int cc = (int) rowcount;
//                    totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
//                    if (totalPages == 0) {
//                        totalPages = 1;
//                    }
//                }
//                out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                } else {
                    out.print("<tr>");
                    out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"9\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Variation_Order.getName(), "Variation Order History", "");
            }

        } else if ("deleterowforvariation".equalsIgnoreCase(action)) {
            try {
                String val = request.getParameter("val");
                int len = val.length();
                String str = val.substring(0, len - 1);
                service.deleteRowForVariation(str);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("processinWF".equalsIgnoreCase(action)) {
            try {
                int listcount = Integer.parseInt(request.getParameter("listcount"));
                int varId = Integer.parseInt(request.getParameter("varId"));
//                for (int i = 0; i < listcount; i++) {
//                    String flag = request.getParameter("flag_" + i);
//                    BigDecimal qty = new BigDecimal(request.getParameter("Qtyenter_" + i));
//                    asfas
//
//
//                    if ("I".equalsIgnoreCase(flag)) {
//                        TblCmsWpDetail wpDetail = new TblCmsWpDetail();
//                        wpDetail.setAmendmentFlag("variationI");
//                        wpDetail.setCreatedBy(Integer.parseInt(userId));
//                        wpDetail.setGroupId(request.getParameter("group_" + i));
//                        wpDetail.setItemInvGenQty(new BigDecimal(0));
//                        wpDetail.setItemInvStatus("pending");
//                        wpDetail.setIsRepeatOrder("no");
//                        wpDetail.setTblCmsWpMaster(new TblCmsWpMaster(Integer.parseInt(request.getParameter("wpId"))));
//                        wpDetail.setTenderTableId(Integer.parseInt(request.getParameter("tendertableId_" + i)));
//                        wpDetail.setUserTypeId(Integer.parseInt(userTypeId));
//                        wpDetail.setWpEndDate(DateUtils.convertStringtoDate(request.getParameter("txtedate_" + i), "dd-MMM-yyyy"));
//                        wpDetail.setWpDescription(request.getParameter("desc_" + i));
//                        wpDetail.setWpItemStatus("pending");
////                    Date dsdate = new Date(DateUtils.convertStringtoDate(request.getParameter("txtsdate_" + i), "dd-MMM-yyyy").getTime());
////                    Date dedate = new Date(DateUtils.convertStringtoDate(request.getParameter("txtedate_" + i), "dd-MMM-yyyy").getTime());
////                    long days = (dedate.getTime()/24 * 60 * 60 * 1000) - (dsdate.getTime()/24 * 60 * 60 * 1000);
//                        wpDetail.setWpNoOfDays(0);
//                        wpDetail.setWpQty(qty);
//                        wpDetail.setWpRate(new BigDecimal(request.getParameter("rate_" + i)));
//                        wpDetail.setWpRowId(Integer.parseInt(request.getParameter("rowId_" + i)));
//                        wpDetail.setWpSrNo(request.getParameter("srno_" + i));
//                        wpDetail.setWpStartDate(DateUtils.convertStringtoDate(request.getParameter("txtsdate_" + i), "dd-MMM-yyyy"));
//                        wpDetail.setWpUom(request.getParameter("uom_" + i));
//                        wpDetail.setWpQualityCheckReq("no");
//                        tcwds.add(wpDetail);
//                    } else {
//                        int detailid = Integer.parseInt(request.getParameter("detail_" + i));
//                        boolean flagcheckPr = service.checkForPrEntryInWprks(Integer.parseInt(request.getParameter("wpId")));
//                        if (flagcheckPr) {
//                            String qt = service.getAcceptedQty(request.getParameter("rowId_" + i), request.getParameter("tendertableId_" + i), Integer.parseInt(request.getParameter("wpId")));
//                            qty = new BigDecimal(qt).add(qty);
//                        }
//                        service.updateTableForWorksinVariOrder(detailid, qty);
//                    }
//
//                }
                service.sendToContractorForApproved(varId);
                sendMailForVariationOrderPublishedFromPE(request.getParameter("tenderId"), request.getParameter("lotId"), request.getParameter("varId"), userId, "e-GP : PE has Published Variation Order");
                response.sendRedirect("officer/DeliverySchedule.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=process");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Variation_Order.getName(), "Issue to Contractor", "");
            }

        } else if ("editforvariationorder".equalsIgnoreCase(action)) {
            int listcount = Integer.parseInt(request.getParameter("listcount"));
            List<TblCmsTrackVariation> variations = new ArrayList<TblCmsTrackVariation>();
            try {
                for (int i = 0; i < listcount; i++) {
                    if (request.getParameter("Qtyenter_" + i) == null) {
                        continue;
                    }
                    String flag = request.getParameter("flag_" + i);
                    BigDecimal qty = new BigDecimal(request.getParameter("Qtyenter_" + i));
                    int trackid = Integer.parseInt(request.getParameter("trackid_" + i));
                    TblCmsTrackVariation tctv = new TblCmsTrackVariation();
                    if ("I".equalsIgnoreCase(flag)) {
                        tctv.setDescription(request.getParameter("desc_" + i));
                        tctv.setEndDate(DateUtils.convertStringtoDate(request.getParameter("txtedate_" + i), "dd-MMM-yyyy"));
                        tctv.setFlag(flag);
                        tctv.setQty(qty);
                        tctv.setRate(new BigDecimal(request.getParameter("rate_" + i)));
                        tctv.setRowId(Integer.parseInt(request.getParameter("rowId_" + i)));
                        tctv.setSrno(request.getParameter("srno_" + i));
                        tctv.setStartDate(DateUtils.convertStringtoDate(request.getParameter("txtsdate_" + i), "dd-MMM-yyyy"));
                        tctv.setTenderTableId(Integer.parseInt(request.getParameter("tendertableId_" + i)));
                        tctv.setUom(request.getParameter("uom_" + i));
                        tctv.setVariOrdId(Integer.parseInt(request.getParameter("variid_" + i)));
                        tctv.setVariTrackId(trackid);
                        tctv.setGroupname(request.getParameter("group_" + i));
                        tctv.setWpdetailid(Integer.parseInt(request.getParameter("detail_" + i)));
                        variations.add(tctv);
                    } else {
                        service.updateTrackvariationTable(trackid, qty);
                    }
                }
                boolean flag = service.AddToTrackVariation(variations);
                response.sendRedirect("officer/DeliverySchedule.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=editt");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Variation_Order.getName(), "Edit Variation Order", "");
            }

        } else if ("viewpr".equals(action)) {
            try {
                String max = request.getParameter("size");
                String first = request.getParameter("pageNo");
                List<Object[]> listt = null;
                long rowcount = service.getRowcountForBoq(request.getParameter("wpId"));
                String styleClass = "bgColor-White";
                if (procnature.equalsIgnoreCase("goods") || procnature.equalsIgnoreCase("services")) {
                    listt = service.getListForView(request.getParameter("wpId"), Integer.parseInt(first), Integer.parseInt(max), Integer.parseInt(request.getParameter("repId")));
                    if (listt != null && !listt.isEmpty()) {
                        String link = "";
                        int i = 0;
                        for (Object[] oob : listt) {

                            out.print("<tr class='" + styleClass + "'>");
                            out.print("<td class=\"t-align-center\">" + oob[0].toString() + "</td>");
                            out.print("<td class=\"t-align-center\">" + oob[1].toString() + "</td>");
                            out.print("<td class=\"t-align-center\">" + oob[2].toString() + "</td>");
                            out.print("<td style=\"text-align :right;\">" + oob[3].toString() + "</td>");
                            out.print("<td style=\"text-align :right;\"><label name=disp_" + i + " id=disp_" + i + ">" + oob[4].toString() + "</label> </td>");
                            out.print("<td style=\"text-align :right;\">" + oob[5].toString() + " </td>");
                            out.print("<td style=\"text-align :right;\"><label id=Qtypending_" + i + " name=Qtypending_" + i + ">" + oob[6].toString() + "</label></td>");
                            if (oob[7] != null) {
                                out.print("<td class=\"t-align-center\">" + DateUtils.gridDateToStr((Date) oob[7]).toString().split(" ")[0] + "</td>");

                            } else {
                                out.print("<td class=\"t-align-center\">-</td>");

                            }
                            if ("".equalsIgnoreCase(oob[8].toString())) {
                                out.print("<td class=\"t-align-center\">-</td>");

                            } else {
                                out.print("<td class=\"t-align-center\">" + oob[8].toString() + "</td>");

                            }
                            out.print("</tr>");
                            i++;
                        }
                        out.print("<input type=hidden name=prId id=prId value=\"" + listt.get(0)[11].toString() + "\" />");

                        out.print("<input type=hidden name=prReqDate id=prReqDate value=\"" + listt.get(0)[13].toString() + "\" />");
                        if (i == 0) {
                            i++;
                        }
                        out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");
                        int totalPages = 1;

                        if (listt.size() > 0) {
                            int cc = (int) rowcount;
                            totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                    }
                } else {
                    listt = service.getListForViewForWorks(request.getParameter("wpId"), Integer.parseInt(first), Integer.parseInt(max), Integer.parseInt(request.getParameter("repId")));
                    if (listt != null && !listt.isEmpty()) {
                        String link = "";
                        int i = 0;
                        for (Object[] oob : listt) {

                            if (oob[15].toString().equalsIgnoreCase("variationI")) {
                                out.print("<tr class='" + styleClass + "' style=background-color:#FFFF99>");
                            } else if (oob[15].toString().equalsIgnoreCase("variationU")) {
                                out.print("<tr class='" + styleClass + "' style=background-color:#CDC0B0>");
                            } else {
                                out.print("<tr class='" + styleClass + "'>");
                            }
                            out.print("<td class=\"t-align-center\">" + oob[0].toString() + "</td>");
                            out.print("<td class=\"t-align-center\">" + oob[1].toString() + "</td>");
                            out.print("<td class=\"t-align-center\">" + oob[2].toString() + "</td>");
                            out.print("<td class=\"t-align-center\">" + oob[3].toString() + "</td>");
                            out.print("<td style=\"text-align :right;\">" + oob[4].toString() + "</td>");
                            out.print("<td style=\"text-align :right;\"><label name=disp_" + i + " id=disp_" + i + ">" + oob[5].toString() + "</label> </td>");
                            out.print("<td style=\"text-align :right;\">" + oob[6].toString() + " </td>");
                            out.print("<td style=\"text-align :right;\"><label id=Qtypending_" + i + " name=Qtypending_" + i + ">" + oob[7].toString() + "</label></td>");
                            if (oob[7] != null) {
                                out.print("<td class=\"t-align-center\">" + DateUtils.gridDateToStr((Date) oob[8]).toString().split(" ")[0] + "</td>");

                            } else {
                                out.print("<td class=\"t-align-center\">-</td>");

                            }
                            if ("".equalsIgnoreCase(oob[9].toString())) {
                                out.print("<td class=\"t-align-center\">-</td>");

                            } else {
                                out.print("<td class=\"t-align-left\"><div class=\"break-word\">" + oob[9].toString() + "</div></td>");

                            }
                            out.print("</tr>");
                            i++;
                        }
                        out.print("<input type=hidden name=prId id=prId value=\"" + listt.get(0)[12].toString() + "\" />");

                        out.print("<input type=hidden name=prReqDate id=prReqDate value=\"" + listt.get(0)[14].toString() + "\" />");
                        if (i == 0) {
                            i++;
                        }
                        out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");
                        int totalPages = 1;

                        if (listt.size() > 0) {
                            int cc = (int) rowcount;
                            totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Progress_Report.getName(), "View Progress report", "");

            }

        } else if ("finaliseRO".equalsIgnoreCase(action)) {
            try {
                int lotId = Integer.parseInt(request.getParameter("lotId"));
                int tenderId = Integer.parseInt(request.getParameter("tenderId"));
                service.finaliseRO(lotId);
                sendMailForFinaliseRo(request.getParameter("tenderId"), request.getParameter("lotId"), user.toString());
                response.sendRedirect("officer/repeatOrderMain.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=finaliseRO");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Repeat_Order.getName(), "Finalize Repeat Order", "");
            }
        } else if ("perfsec".equalsIgnoreCase(action)) {
            try {
                TblTenderPerformanceSec tenderPerformanceSec = (TblTenderPerformanceSec) AppContext.getSpringBean("TblTenderPerformanceSec");
                int tenderId = Integer.parseInt(request.getParameter("tenderId"));
                int pkgid = Integer.parseInt(request.getParameter("pckid"));
                int rId = Integer.parseInt(request.getParameter("roudId"));
                int romId = Integer.parseInt(request.getParameter("romId"));
                String apppkgid = request.getParameter("forupdate");
                if ("".equalsIgnoreCase(apppkgid)) {

                    int wpId = 0;
                    List<TblCmsRoitems> list = service.allROItemsForWf(romId);
                    if (!list.isEmpty() && list != null) {
                        int firstWpid = list.get(0).getWpId();
                        List<TblCmsWpDetailHistory> wpDetailHistory = new ArrayList<TblCmsWpDetailHistory>();
                        for (int i = 0; i < list.size(); i++) {
                            if (i == 0) {
                                TblCmsWpMaster master = new TblCmsWpMaster();
                                master.setCreatedBy(Integer.parseInt(userId));
                                master.setCreatedDate(new Date());
                                master.setIsDateEdited("yes");
                                master.setWpLotId(romId);
                                master.setWpLotId(pkgid);
                                master.setIsRepeatOrder("yes");
                                master.setWpName("Repeat Order");
                                master.setWrkWpStatus("pending");
                                service.add(master);
                                wpId = master.getWpId();
                                TblCmsRomap romap = new TblCmsRomap();
                                romap.setRomId(romId);
                                romap.setWpId(wpId);
                                service.addToRoMap(romap);
//                                TblCmsWptenderBoqmap boqmap = new TblCmsWptenderBoqmap();
//                                boqmap.setTblCmsWpMaster(new TblCmsWpMaster(master.getWpId()));
//                                boqmap.setWpTenderFormId(list.get(i).getTendertableid());
//                                service.addinchild(boqmap);
                                TblCmsWpDetail wpDetail = new TblCmsWpDetail();
                                wpDetail.setAmendmentFlag("orignal");
                                wpDetail.setCreatedBy(Integer.parseInt(userId));
                                wpDetail.setGroupId(null);
                                wpDetail.setItemInvGenQty(new BigDecimal(0));
                                wpDetail.setItemInvStatus("pending");
                                wpDetail.setTblCmsWpMaster(new TblCmsWpMaster(wpId));
                                wpDetail.setTenderTableId(list.get(i).getTendertableid());
                                wpDetail.setUserTypeId(Integer.parseInt(userTypeId));
                                Date date = new Date();
                                date.setTime(new Date().getTime() + (list.get(i).getNoofdays() * 24 * 60 * 60 * 1000));
                                wpDetail.setWpEndDate(date);
                                wpDetail.setIsRepeatOrder("yes");
                                wpDetail.setWpDescription(list.get(i).getDescription());
                                wpDetail.setWpItemStatus("pending");
                                wpDetail.setWpNoOfDays(list.get(i).getNoofdays());
                                wpDetail.setWpQty(list.get(i).getQty());
                                wpDetail.setWpRate(list.get(i).getRate());
                                wpDetail.setWpRowId(list.get(i).getRowid());
                                wpDetail.setWpSrNo(list.get(i).getSrno());
                                wpDetail.setWpStartDate(new Date());
                                wpDetail.setWpUom(list.get(i).getUom());
                                wpDetail.setWpQualityCheckReq("no");
                                service.addOneByOneToWpDetails(wpDetail);
                                TblCmsWpDetailHistory history = new TblCmsWpDetailHistory();
                                history.setAmendmentFlag("original");
                                history.setDetail(new TblCmsWpDetail(wpDetail.getWpDetailId()));
                                history.setHistoryCount(0);
                                history.setTenderTableId(list.get(i).getTendertableid());
                                history.setVariOrdNo(0);
                                history.setWpEndDate(wpDetail.getWpEndDate());
                                history.setWpId(master.getWpId());
                                history.setWpNoOfDays(list.get(i).getNoofdays());
                                history.setWpQty(list.get(i).getQty());
                                history.setWpRate(list.get(i).getRate());
                                history.setWpRowId(list.get(i).getRowid());
                                history.setWpStartDate(wpDetail.getWpStartDate());
                                history.setActContractDtHistId(0);
                                history.setCreatedDate(new java.sql.Date(new java.util.Date().getTime()));
                                wpDetailHistory.add(history);

                            } else {
                                if (firstWpid != list.get(i).getWpId()) {
                                    firstWpid = list.get(i).getWpId();
                                    TblCmsWpMaster master = new TblCmsWpMaster();
                                    master.setCreatedBy(Integer.parseInt(userId));
                                    master.setCreatedDate(new Date());
                                    master.setIsDateEdited("yes");
                                    master.setIsRepeatOrder("yes");
                                    master.setWpLotId(romId);
                                    master.setWpLotId(pkgid);
                                    master.setWpName("Repeat Order");
                                    master.setWrkWpStatus("pending");
                                    service.add(master);
                                    wpId = master.getWpId();
                                    TblCmsRomap romap = new TblCmsRomap();
                                    romap.setRomId(romId);
                                    romap.setWpId(wpId);
                                    service.addToRoMap(romap);
//                                    TblCmsWptenderBoqmap boqmap = new TblCmsWptenderBoqmap();
//                                    boqmap.setTblCmsWpMaster(new TblCmsWpMaster(master.getWpId()));
//                                    boqmap.setWpTenderFormId(list.get(i).getTendertableid());
//                                    service.addinchild(boqmap);
                                    TblCmsWpDetail wpDetail = new TblCmsWpDetail();
                                    wpDetail.setAmendmentFlag("orignal");
                                    wpDetail.setCreatedBy(Integer.parseInt(userId));
                                    wpDetail.setGroupId(null);
                                    wpDetail.setItemInvGenQty(new BigDecimal(0));
                                    wpDetail.setItemInvStatus("pending");
                                    wpDetail.setTblCmsWpMaster(new TblCmsWpMaster(wpId));
                                    wpDetail.setTenderTableId(list.get(i).getTendertableid());
                                    wpDetail.setUserTypeId(Integer.parseInt(userTypeId));
                                    Date date = new Date();
                                    date.setTime(new Date().getTime() + ((long) list.get(i).getNoofdays() * 24 * 60 * 60 * 1000));
                                    wpDetail.setWpEndDate(date);
                                    wpDetail.setWpDescription(list.get(i).getDescription());
                                    wpDetail.setWpItemStatus("pending");
                                    wpDetail.setWpNoOfDays(list.get(i).getNoofdays());
                                    wpDetail.setWpQty(list.get(i).getQty());
                                    wpDetail.setWpRate(list.get(i).getRate());
                                    wpDetail.setWpRowId(list.get(i).getRowid());
                                    wpDetail.setWpSrNo(list.get(i).getSrno());
                                    wpDetail.setWpStartDate(new Date());
                                    wpDetail.setWpUom(list.get(i).getUom());
                                    wpDetail.setWpQualityCheckReq("no");
                                    wpDetail.setIsRepeatOrder("yes");
                                    service.addOneByOneToWpDetails(wpDetail);
                                    TblCmsWpDetailHistory history = new TblCmsWpDetailHistory();
                                    history.setAmendmentFlag("original");
                                    history.setDetail(new TblCmsWpDetail(wpDetail.getWpDetailId()));
                                    history.setHistoryCount(0);
                                    history.setTenderTableId(list.get(i).getTendertableid());
                                    history.setVariOrdNo(0);
                                    history.setWpEndDate(wpDetail.getWpEndDate());
                                    history.setWpId(master.getWpId());
                                    history.setWpNoOfDays(list.get(i).getNoofdays());
                                    history.setWpQty(list.get(i).getQty());
                                    history.setWpRate(list.get(i).getRate());
                                    history.setWpRowId(list.get(i).getRowid());
                                    history.setWpStartDate(wpDetail.getWpStartDate());
                                    history.setActContractDtHistId(0);
                                    history.setCreatedDate(new java.sql.Date(new java.util.Date().getTime()));
                                    wpDetailHistory.add(history);
                                } else {
                                    TblCmsWpDetail wpDetail = new TblCmsWpDetail();
                                    wpDetail.setAmendmentFlag("orignal");
                                    wpDetail.setCreatedBy(Integer.parseInt(userId));
                                    wpDetail.setGroupId(null);
                                    wpDetail.setItemInvGenQty(new BigDecimal(0));
                                    wpDetail.setItemInvStatus("pending");
                                    wpDetail.setTblCmsWpMaster(new TblCmsWpMaster(wpId));
                                    wpDetail.setTenderTableId(list.get(i).getTendertableid());
                                    wpDetail.setUserTypeId(Integer.parseInt(userTypeId));
                                    Date date = new Date();
                                    date.setTime(new Date().getTime() + ((long) list.get(i).getNoofdays() * 24 * 60 * 60 * 1000));
                                    wpDetail.setWpEndDate(date);
                                    wpDetail.setWpDescription(list.get(i).getDescription());
                                    wpDetail.setWpItemStatus("pending");
                                    wpDetail.setWpNoOfDays(list.get(i).getNoofdays());
                                    wpDetail.setWpQty(list.get(i).getQty());
                                    wpDetail.setWpRate(list.get(i).getRate());
                                    wpDetail.setWpRowId(list.get(i).getRowid());
                                    wpDetail.setWpSrNo(list.get(i).getSrno());
                                    wpDetail.setWpStartDate(new Date());
                                    wpDetail.setWpUom(list.get(i).getUom());
                                    wpDetail.setWpQualityCheckReq("no");
                                    wpDetail.setIsRepeatOrder("yes");
                                    service.addOneByOneToWpDetails(wpDetail);
                                    TblCmsWpDetailHistory history = new TblCmsWpDetailHistory();
                                    history.setAmendmentFlag("original");
                                    history.setDetail(new TblCmsWpDetail(wpDetail.getWpDetailId()));
                                    history.setHistoryCount(0);
                                    history.setTenderTableId(list.get(i).getTendertableid());
                                    history.setVariOrdNo(0);
                                    history.setWpEndDate(wpDetail.getWpEndDate());
                                    history.setWpId(wpId);
                                    history.setWpNoOfDays(list.get(i).getNoofdays());
                                    history.setWpQty(list.get(i).getQty());
                                    history.setWpRate(list.get(i).getRate());
                                    history.setWpRowId(list.get(i).getRowid());
                                    history.setWpStartDate(wpDetail.getWpStartDate());
                                    history.setActContractDtHistId(0);
                                    history.setCreatedDate(new java.sql.Date(new java.util.Date().getTime()));
                                    wpDetailHistory.add(history);
                                }
                            }
                        }
                        try {
                            service.addToDetailHistory(wpDetailHistory);
                        } catch (Exception ex) {
                            Logger.getLogger(ConsolidateServlet.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }

                TblTenderPerfSecurity tblTenderPerfSecurity = new TblTenderPerfSecurity();
                tblTenderPerfSecurity.setCreatedBy(Integer.parseInt(userId));
                tblTenderPerfSecurity.setCreatedDt(new java.util.Date());
                tblTenderPerfSecurity.setPerSecurityAmt(new BigDecimal(request.getParameter("taka")));
                tblTenderPerfSecurity.setPercentage(new BigDecimal(request.getParameter("percentage")));
                tblTenderPerfSecurity.setLowestAmt(new BigDecimal(request.getParameter("lamount")));
                tblTenderPerfSecurity.setRoundId(rId);
                tblTenderPerfSecurity.setTblTenderMaster(new TblTenderMaster(tenderId));
                tblTenderPerfSecurity.setPkgLotId(pkgid);
                if ("y".equalsIgnoreCase(request.getParameter("isEdit"))) {
                    tblTenderPerfSecurity.setPerCostLotId(Integer.parseInt(apppkgid));
                    tenderPerformanceSec.updatePerfSec(tblTenderPerfSecurity);
                } else {
                    tenderPerformanceSec.addPerfSec(tblTenderPerfSecurity);
                }
                response.sendRedirect("officer/repeatOrderMain.jsp?tenderId=" + tenderId + "&msgFlag=y");
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), conId, "contractId", EgpModule.Repeat_Order.getName(), "Add Performance Security", "");
                out.close();
            }
        } else if ("editRO".equalsIgnoreCase(action)) {
            String max = request.getParameter("size");
            String first = request.getParameter("pageNo");
            int romId = Integer.parseInt(request.getParameter("romId"));
            int addcount = Integer.parseInt(request.getParameter("addcount"));
            int delcount = Integer.parseInt(request.getParameter("delcount"));
            int wpId = Integer.parseInt(request.getParameter("wpId"));
            service.deleteRO(romId, wpId);
            List<TblCmsRoitems> list = new ArrayList<TblCmsRoitems>();
            try {
                for (int i = 0; i < delcount; i++) {
                    TblCmsRoitems roitems = new TblCmsRoitems();
                    if (request.getParameter("delcheck_" + i) == null) {
                        roitems.setDescription(request.getParameter("desc1_" + i));
                        roitems.setNoofdays(Integer.parseInt(request.getParameter("nod_" + i)));
                        roitems.setQty(new BigDecimal(request.getParameter("qty_" + i)));
                        roitems.setRate(new BigDecimal(request.getParameter("rate1_" + i)));
                        roitems.setRowid(Integer.parseInt(request.getParameter("rowid1_" + i)));
                        roitems.setSrno(request.getParameter("srno1_" + i));
                        roitems.setTendertableid(Integer.parseInt(request.getParameter("tendertableid1_" + i)));
                        roitems.setUom(request.getParameter("uom1_" + i));
                        roitems.setWpId(wpId);
                        roitems.setDetailId(Integer.parseInt(request.getParameter("detailid1_" + i)));
                        roitems.setRomId(romId);
                        list.add(roitems);
                    }
                }
                for (int j = 0; j < addcount; j++) {
                    TblCmsRoitems roitems = new TblCmsRoitems();
                    if (request.getParameter("addcheck_" + j) != null) {
                        roitems.setDescription(request.getParameter("desc_" + j));
                        roitems.setNoofdays(Integer.parseInt(request.getParameter("NOD_" + j)));
                        roitems.setQty(new BigDecimal(request.getParameter("Qty_" + j)));
                        roitems.setRate(new BigDecimal(request.getParameter("rate_" + j)));
                        roitems.setRowid(Integer.parseInt(request.getParameter("rowid_" + j)));
                        roitems.setSrno(request.getParameter("srno_" + j));
                        roitems.setTendertableid(Integer.parseInt(request.getParameter("tendertableid_" + j)));
                        roitems.setUom(request.getParameter("uom_" + j));
                        roitems.setWpId(wpId);
                        roitems.setDetailId(Integer.parseInt(request.getParameter("detailid_" + j)));
                        roitems.setRomId(romId);
                        list.add(roitems);
                    }
                }
                boolean placed = service.placedOrderQty(list);
                response.sendRedirect("officer/repeatOrderMain.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=editted");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Repeat_Order.getName(), "Edit Repeat Order", "");
            }

        } else if ("allROitems".equalsIgnoreCase(action)) {
            String max = request.getParameter("size");
            String first = request.getParameter("pageNo");
            int romId = Integer.parseInt(request.getParameter("romId"));
            int wpId = Integer.parseInt(request.getParameter("wpId"));
            long rowcount = service.getRowcountForBoq(request.getParameter("wpId"));
            List<Object[]> list = service.getRepeatOrderItems(romId, wpId);
            BigDecimal totalAmount = new BigDecimal(0);
            int i = 0;
            String styleClass = "";
            try {
                for (i = 0; i < list.size(); i++) {
                    if (i % 2 == 0) {
                        styleClass = "bgColor-white";
                    } else {
                        styleClass = "bgColor-Green";
                    }
                    out.print("<tr class='" + styleClass + "'>");
                    out.print("<td class=\"t-align-center\"><input class=formTxtBox_1 type=checkbox name=delcheck_" + i + " id=delcheck_" + i + " onclick = disableQty(" + i + ") /></td>");
                    out.print("<td class=\"t-align-center\">" + list.get(i)[1] + "</td>");
                    out.print("<input type=hidden name=srno1_" + i + " id=srno1_" + i + " value=\"" + list.get(i)[1] + "\" /></td>");
                    out.print("<td class=\"t-align-center\">" + list.get(i)[2] + "</td>");
                    out.print("<input type=hidden name=desc1_" + i + " id=desc1_" + i + " value=\"" + list.get(i)[2] + "\" /></td>");
                    out.print("<td class=\"t-align-center\">" + list.get(i)[3] + "</td>");
                    out.print("<input type=hidden name=uom1_" + i + " id=uom1_" + i + " value=\"" + list.get(i)[3] + "\" /></td>");
                    out.print("<td class=\"t-align-center\"><input  class=formTxtBox_1 type=text onchange=checkQty(" + i + ") name=qty_" + i + " id=qty_" + i + " value=\"" + list.get(i)[4] + "\" /></td>");
                    out.print("<td style=\"text-align :right;\">" + list.get(i)[5] + " </td>");
                    out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 onchange=checkdays(" + i + ") name=nod_" + i + " id=nod_" + i + " value=\"" + list.get(i)[8] + "\" /></td>");
                    out.print("<input type=hidden class=formTxtBox_1 name=unitratedel_" + i + " id=unitratedel_" + i + " value=\"" + new BigDecimal(list.get(i)[4].toString()).multiply(new BigDecimal(list.get(i)[5].toString())).setScale(3, 0) + "\" />");
                    out.print("<td style=\"text-align :right;\"><label id =unitratedellbl_" + i + " >" + new BigDecimal(list.get(i)[4].toString()).multiply(new BigDecimal(list.get(i)[5].toString())).setScale(3, 0) + "</label></td>");
                    out.print("</tr>");
                    //totalAmount = totalAmount.add(new BigDecimal(list.get(i)[4].toString()).multiply(new BigDecimal(list.get(i)[5].toString())).setScale(3, 0));
                    out.print("<input type=hidden name=romId_" + i + " id=romId_" + i + " value=\"" + list.get(i)[9] + "\" />");
                    out.print("<input type=hidden name=rate1_" + i + " id=rate1_" + i + " value=\"" + list.get(i)[5] + "\" />");
                    out.print("<input type=hidden name=rowid1_" + i + " id=rowid1_" + i + " value=\"" + list.get(i)[6] + "\" />");
                    out.print("<input type=hidden name=tendertableid1_" + i + " id=tendertableid1_" + i + " value=\"" + list.get(i)[7] + "\" />");
                    out.print("<input type=hidden name=detailid1_" + i + " id=detailid1_" + i + " value=\"" + list.get(i)[0] + "\" />");
                }
                out.print("<tr>&nbsp;</tr><tr><th colspan=6 class=\"t-align-center\"> </th>");
                out.print("<th class=\"t-align-center ff\">Total Amount (In BTN)</th>");
                out.print("<th style=\"text-align :right;\"><label id=totalamount></label></th></tr>");
                out.print("<input type=hidden name=delcount id=delcount value=" + i + " />");
                out.print("<script>allcountCntVal()</script>");
                int totalPages = 1;

                if (list.size() > 0) {
                    int cc = (int) rowcount;
                    totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                    if (totalPages == 0) {
                        totalPages = 1;
                    }
                }
                out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("viewRO".equalsIgnoreCase(action)) {
            String max = request.getParameter("size");
            String first = request.getParameter("pageNo");
            int romId = Integer.parseInt(request.getParameter("romId"));
            int wpId = Integer.parseInt(request.getParameter("wpId"));
            long rowcount = service.getRowcountForBoq(request.getParameter("wpId"));
            List<Object[]> list = service.getRepeatOrderItems(romId, wpId);
            int i = 0;
            String styleClass = "";
            try {
                for (i = 0; i < list.size(); i++) {
                    if (i % 2 == 0) {
                        styleClass = "bgColor-white";
                    } else {
                        styleClass = "bgColor-Green";
                    }
                    out.print("<tr class='" + styleClass + "'>");
                    out.print("<td class=\"t-align-center\">" + list.get(i)[1] + "</td>");
                    out.print("<td class=\"t-align-center\">" + list.get(i)[2] + "</td>");
                    out.print("<td class=\"t-align-center\">" + list.get(i)[3] + "</td>");
                    out.print("<td style=\"text-align :right;\">" + list.get(i)[4] + "</td>");
                    out.print("<td class=\"t-align-center\">" + list.get(i)[8] + "</td>");
                    out.print("</tr>");
                }
                int totalPages = 1;

                if (list.size() > 0) {
                    int cc = (int) rowcount;
                    totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                    if (totalPages == 0) {
                        totalPages = 1;
                    }
                }
                out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Repeat_Order.getName(), "View Repeat Order", "");
            }
        } else if ("viewROWF".equalsIgnoreCase(action)) {
            String max = request.getParameter("size");
            String first = request.getParameter("pageNo");
            int romId = Integer.parseInt(request.getParameter("romId"));
            List<TblCmsRoitems> list = service.allROItemsForWf(romId);
            int i = 0;
            String styleClass = "";
            try {
                for (i = 0; i < list.size(); i++) {
                    if (i % 2 == 0) {
                        styleClass = "bgColor-white";
                    } else {
                        styleClass = "bgColor-Green";
                    }
                    out.print("<tr class='" + styleClass + "'>");
                    out.print("<td class=\"t-align-center\">" + list.get(i).getSrno() + "</td>");
                    out.print("<td class=\"t-align-center\">" + list.get(i).getDescription() + "</td>");
                    out.print("<td class=\"t-align-center\">" + list.get(i).getUom() + "</td>");
                    out.print("<td style=\"text-align :right;\">" + list.get(i).getQty() + "</td>");
                    out.print("<td style=\"text-align :right;\">" + list.get(i).getRate() + "</td>");
                    out.print("<td class=\"t-align-center\">" + list.get(i).getNoofdays() + "</td>");
                    out.print("</tr>");
                }
//            int totalPages = 1;
//
//            if (list.size() > 0) {
//                int cc = (int) rowcount;
//                totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
//                if (totalPages == 0) {
//                    totalPages = 1;
//                }
                //}
                //out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("remainingitem".equalsIgnoreCase(action)) {
            String max = request.getParameter("size");
            String first = request.getParameter("pageNo");
            int romId = Integer.parseInt(request.getParameter("romId"));
            int i = 0;
            String styleClass = "";
            BigDecimal totalAmount = new BigDecimal(0);
            List<Object[]> list = service.editRepeatOrder(Integer.parseInt(request.getParameter("wpId")), romId);
            long rowcount = service.getRowcountForBoq(request.getParameter("wpId"));
            try {
                for (i = 0; i < list.size(); i++) {
                    if (i % 2 == 0) {
                        styleClass = "bgColor-white";
                    } else {
                        styleClass = "bgColor-Green";
                    }
                    out.print("<tr class='" + styleClass + "'>");
                    out.print("<td class=\"t-align-center\"><input class=formTxtBox_1 type=checkbox name=addcheck_" + i + " id=addcheck_" + i + "  onClick=enableQty(" + i + ") /></td>");
                    out.print("<td class=\"t-align-center\">" + list.get(i)[1] + "</td>");
                    out.print("<input type=hidden name=srno_" + i + " id=srno_" + i + " value=\"" + list.get(i)[1] + "\" /></td>");
                    out.print("<td class=\"t-align-center\">" + list.get(i)[2] + "</td>");
                    out.print("<input type=hidden name=desc_" + i + " id=desc_" + i + " value=\"" + list.get(i)[2] + "\" /></td>");
                    out.print("<td class=\"t-align-center\">" + list.get(i)[3] + "</td>");
                    out.print("<input type=hidden name=uom_" + i + " id=uom_" + i + " value=\"" + list.get(i)[3] + "\" /></td>");
                    out.print("<td class=\"t-align-center\"><input  class=formTxtBox_1 type=text onchange=checkqty(" + i + ") name=Qty_" + i + " readonly id=Qty_" + i + " value=\"" + list.get(i)[4] + "\" /></td>");
                    out.print("<td style=\"text-align :right;\">" + list.get(i)[5] + "</td>");
                    out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 onchange=checkDays(" + i + ") name=NOD_" + i + " id=NOD_" + i + " value=\"" + list.get(i)[8] + "\" /></td>");
                    out.print("<td style=\"text-align :right;\"><label id=unitrateaddlbl_" + i + ">" + new BigDecimal(list.get(i)[4].toString()).multiply(new BigDecimal(list.get(i)[5].toString())).setScale(3, 0) + "</labelm></td>");
                    out.print("<input type=hidden name=unitrateadd_" + i + " id=unitrateadd_" + i + " value=\"" + new BigDecimal(list.get(i)[4].toString()).multiply(new BigDecimal(list.get(i)[5].toString())).setScale(3, 0) + "\" />");
                    out.print("</tr>");
                    totalAmount = totalAmount.add(new BigDecimal(list.get(i)[4].toString()).multiply(new BigDecimal(list.get(i)[5].toString())).setScale(3, 0));
                    out.print("<input type=hidden name=detailid_" + i + " id=detailid_" + i + " value=\"" + list.get(i)[0] + "\" />");
                    out.print("<input type=hidden name=rate_" + i + " id=rate_" + i + " value=\"" + list.get(i)[5] + "\" />");
                    out.print("<input type=hidden name=tendertableid_" + i + " id=tendertableid_" + i + " value=\"" + list.get(i)[7] + "\" />");
                    out.print("<input type=hidden name=rowid_" + i + " id=rowid_" + i + " value=\"" + list.get(i)[6] + "\" />");
                }
                out.print("<input type=hidden name=addcount id=addcount value=" + i + " />");
                out.print("<script>countCntVal()</script>");


                int totalPages = 1;

                if (list.size() > 0) {
                    int cc = (int) rowcount;
                    totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                    if (totalPages == 0) {
                        totalPages = 1;
                    }
                }
                out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("placerptorder".equalsIgnoreCase(action)) {
            int lotId = Integer.parseInt(request.getParameter("lotId"));
            int wpId = Integer.parseInt(request.getParameter("wpId"));
            int count = Integer.parseInt(request.getParameter("listcount"));
            List<Object> checkForSecondWPRO = service.checkForSecondWPRO(lotId);
            TblCmsRomaster romaster = new TblCmsRomaster();
            int romId = 0;
            try {
                if (!checkForSecondWPRO.isEmpty()) {
                    romId = Integer.parseInt(checkForSecondWPRO.get(0).toString());
                }
                if (checkForSecondWPRO.isEmpty()) {
                    romaster.setCreatedBy(Integer.parseInt(userId));
                    romaster.setLotId(lotId);
                    romaster.setOrderCreationDate(new Date());
                    romaster.setRptOrderStatus("pending");
                    romaster.setRptOrderWfstatus("pending");
                    romaster.setIsCurrent("yes");
                    romaster.setAcceptRejStatus("pending");
                    boolean flag = service.addRepeatOrder(romaster);
                    TblEvalRoundMaster roundMaster = new TblEvalRoundMaster();
                    roundMaster.setCreatedBy(Integer.parseInt(userId));
                    roundMaster.setCreatedDt(new Date());
                    roundMaster.setPkgLotId(lotId);
                    roundMaster.setReportType("RO");
                    roundMaster.setRomId(romaster.getRomId());
                    roundMaster.setTenderId(Integer.parseInt(request.getParameter("tenderId")));
                    roundMaster.setUserId(Integer.parseInt(request.getParameter("tenderId")));
                    RepeatOrderService ros = (RepeatOrderService) AppContext.getSpringBean("RepeatOrderService");
                    ros.generateRoundId(roundMaster);

                }

                List<TblCmsRoitems> list = new ArrayList<TblCmsRoitems>();
                for (int i = 0; i < count; i++) {
                    if (request.getParameter("desc_" + i) != null) {
                        TblCmsRoitems roitems = new TblCmsRoitems();
                        roitems.setDescription(request.getParameter("desc_" + i));
                        roitems.setNoofdays(Integer.parseInt(request.getParameter("nod_" + i)));
                        roitems.setQty(new BigDecimal(request.getParameter("qty_" + i)));
                        roitems.setRate(new BigDecimal(request.getParameter("rate_" + i)));
                        roitems.setRowid(Integer.parseInt(request.getParameter("rowid_" + i)));
                        roitems.setSrno(request.getParameter("srno_" + i));
                        roitems.setTendertableid(Integer.parseInt(request.getParameter("tendertableid_" + i)));
                        roitems.setUom(request.getParameter("uom_" + i));
                        roitems.setWpId(wpId);
                        roitems.setDetailId(Integer.parseInt(request.getParameter("detailid_" + i)));
                        if (checkForSecondWPRO.isEmpty()) {
                            roitems.setRomId(romaster.getRomId());
                        } else {
                            roitems.setRomId(romId);
                        }
                        list.add(roitems);
                    }
                }
                boolean placed = service.placedOrderQty(list);
                response.sendRedirect("officer/repeatOrderMain.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=placed");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Repeat_Order.getName(), "Place Repeat Order", "");
            }
        } else if ("fetchitemforrepeatorder".equalsIgnoreCase(action)) {
            String max = request.getParameter("size");
            String first = request.getParameter("pageNo");
            BigDecimal totalAmount = new BigDecimal(0);
            long rowcount = service.getRowcountForBoq(request.getParameter("wpId"));
            String styleClass = "";
            List<TblCmsWpDetail> list = service.getDatesByWpId(request.getParameter("wpId"), Integer.parseInt(first), Integer.parseInt(max));
            int i = 0;
            try {
                for (i = 0; i < list.size(); i++) {
                    if (i % 2 == 0) {
                        styleClass = "bgColor-white";
                    } else {
                        styleClass = "bgColor-Green";
                    }
                    out.print("<tr class='" + styleClass + "'>");
                    out.print("<td class=\"t-align-center\"><input class=formTxtBox_1 type=checkbox name=check_" + i + " id=check_" + i + "  /></td>");
                    out.print("<td class=\"t-align-center\">" + list.get(i).getWpSrNo() + "</td>");
                    out.print("<input type=hidden name=srno_" + i + " id=srno_" + i + " value=\"" + list.get(i).getWpSrNo() + "\" /></td>");
                    out.print("<td class=\"t-align-center\">" + list.get(i).getWpDescription() + "</td>");
                    out.print("<input type=hidden name=desc_" + i + " id=desc_" + i + " value=\"" + list.get(i).getWpDescription() + "\" /></td>");
                    out.print("<td class=\"t-align-center\">" + list.get(i).getWpUom() + "</td>");
                    out.print("<input type=hidden name=uom_" + i + " id=uom_" + i + " value=\"" + list.get(i).getWpUom() + "\" /></td>");
                    out.print("<td class=\"t-align-center\"><input  class=formTxtBox_1 type=text onchange=checkQty(" + i + ") name=qty_" + i + " id=qty_" + i + " value=\"" + list.get(i).getWpQty() + "\" /></td>");
                    out.print("<td style=\"text-align :right;\">" + list.get(i).getWpRate() + "</td>");
                    out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 onchange=checkDays(" + i + ") name=nod_" + i + " id=nod_" + i + " value=\"" + list.get(i).getWpNoOfDays() + "\" /></td>");
                    out.print("<td style=\"text-align :right;\"><label id=unitratelbl_" + i + ">" + list.get(i).getWpRate().multiply(list.get(i).getWpQty()).setScale(3, 0) + "</label></td>");
                    out.print("</tr>");
                    out.print("<input type=hidden name=detailid_" + i + " id=detailid_" + i + " value=\"" + list.get(i).getWpDetailId() + "\" />");
                    out.print("<input type=hidden name=rate_" + i + " id=rate_" + i + " value=\"" + list.get(i).getWpRate() + "\" />");
                    out.print("<input type=hidden name=tendertableid_" + i + " id=tendertableid_" + i + " value=\"" + list.get(i).getTenderTableId() + "\" />");
                    out.print("<input type=hidden name=rowid_" + i + " id=rowid_" + i + " value=\"" + list.get(i).getWpRowId() + "\" />");
                    out.print("<input type=\"hidden\" name=\"unitrate_" + i + "\" id=\"unitrate_" + i + "\" value=\"" + list.get(i).getWpRate().multiply(list.get(i).getWpQty()).setScale(3, 0) + "\" />");
                    totalAmount = totalAmount.add(list.get(i).getWpRate().multiply(list.get(i).getWpQty()).setScale(3, 0));

                }
                out.print("<tr><td colspan=6 class=\"t-align-center\"> </td>");
                out.print("<td class=\"t-align-center ff\">Total Amount (In BTN)</td>");
                out.print("<td style=\"text-align :right;\"><label id=totalamount>" + totalAmount + "</label></td></tr>");
                out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");

                int totalPages = 1;

                if (list.size() > 0) {
                    int cc = (int) rowcount;
                    totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                    if (totalPages == 0) {
                        totalPages = 1;
                    }
                }
                out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if ("fetchdates".equals(action)) {

            //List<SPTenderCommonData> listDP = tenderCommonService1.returndata("chkDomesticPreference", tenderId, null);
            String max = request.getParameter("size");
            String first = request.getParameter("pageNo");
            List<TblCmsWpDetail> list = service.getDatesByWpId(request.getParameter("wpId"), Integer.parseInt(first), Integer.parseInt(max));
            List<Object[]> worklist = service.getDatesByWpIdForWorks(request.getParameter("wpId"), Integer.parseInt(first), Integer.parseInt(max));
            long rowcount = service.getRowcountForBoq(request.getParameter("wpId"));
            String styleClass = "";
            try {
                if (list != null && !list.isEmpty()) {
                    if (procnature.equalsIgnoreCase("goods")) {
                        int i = 0;
                        for (i = 0; i < list.size(); i++) {
                            if (i % 2 == 0) {
                                styleClass = "bgColor-white";
                            } else {
                                styleClass = "bgColor-Green";
                            }
                            out.print("<tr class='" + styleClass + "'>");
                            out.print("<td class=\"t-align-center\">" + list.get(i).getWpSrNo() + "</td>");
                            out.print("<td class=\"t-align-center\">" + list.get(i).getWpDescription() + "</td>");
                            out.print("<td class=\"t-align-center\">" + list.get(i).getWpUom() + "</td>");
                            out.print("<td style=\"text-align :right;\">" + list.get(i).getWpQty() + "</td>");
                            out.print("<td style=\"text-align :right;\">" + list.get(i).getWpRate() + "</td>");
                            if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                            Double x,y,z;
                            x=0.00;
                            y=0.00;
                            z=0.00;
                            x=  list.get(i).getSupplierVat().doubleValue();
                            y= list.get(i).getInlandOthers().doubleValue();
                            z=x+y;
                           // BigDecimal VATInland = new BigDecimal((list.get(i).getSupplierVat())).add(list.get(i).getInlandOthers());
                                 //BigDecimal VATInland = (BigDecimal)list.get(i).getSupplierVat().add(list.get(i).getInlandOthers());
                                 //(BigDecimal)newCV.get(0)
                                    //new BigDecimal(qt).add(qty)
                            out.print("<td style=\"text-align :right;\">" + list.get(i).getCurrencyName() + "</td>");
                           // out.print("<td style=\"text-align :right;\">" + list.get(i).getSupplierVat() + "</td>");
                            //out.print("<td style=\"text-align :right;\">" + list.get(i).getSupplierVat()+ list.get(i).getInlandOthers() + "</td>");
                          // out.print("<td style=\"text-align :right;\">" + list.get(i).getSupplierVat().add(list.get(i).getInlandOthers()) + "</td>");
                             out.print("<td style=\"text-align :right;\">" + (x+y) + "</td>");
                            // out.print("<td style=\"text-align :right;\">" + z + "</td>");
                            }
                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 onchange=checkDays(" + i + ") onkeypress=\"checkEnter(event," + i + ")\"  value=" + list.get(i).getWpNoOfDays() + " name=nod_" + i + " id=nod_" + i + " /></td>");
                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                    + "readonly name=txtdate_" + i + " id=txtdate_" + i + " "
                                    + "value=\"" + DateUtils.gridDateToStr(list.get(i).getWpEndDate()).split(" ")[0] + "\" /></td>");
                            out.print("</tr>");
                            out.print("<input type=hidden name=detailid_" + i + " id=detailid_" + i + " value=\"" + list.get(i).getWpDetailId() + "\" />");
                        }
                        out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");

                        int totalPages = 1;

                        if (list.size() > 0) {
                            int cc = (int) rowcount;
                            totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                    } else {
                        int i = 0;
                        String grp = "";
                        for (i = 0; i < worklist.size(); i++) {
                            if (i % 2 == 0) {
                                styleClass = "bgColor-white";
                            } else {
                                styleClass = "bgColor-Green";
                            }
                            if (worklist.get(i)[12].toString().equalsIgnoreCase("variationI")) {
                                out.print("<tr class='" + styleClass + "' style=background-color:#FFFF99>");
                            } else if (worklist.get(i)[12].toString().equalsIgnoreCase("variationU")) {
                                out.print("<tr class='" + styleClass + "' style=background-color:#CDC0B0>");
                            } else {
                                out.print("<tr class='" + styleClass + "'>");
                            }

                            out.print("<td class=\"t-align-center\">" + worklist.get(i)[0].toString() + "</td>");
                            out.print("<td class=\"t-align-center\">" + worklist.get(i)[1].toString() + "</td>");

                            out.print("<td class=\"t-align-center\">" + worklist.get(i)[2].toString() + "</td>");
                            out.print("<td class=\"t-align-center\">" + worklist.get(i)[3].toString() + "</td>");
                            out.print("<td style=\"text-align :right;\">" + worklist.get(i)[4].toString() + "</td>");
                            Date sdate = (Date) worklist.get(i)[6];
                            Date edate = (Date) worklist.get(i)[7];
                            long days = 0;
                            if (worklist.get(i)[6] != null && worklist.get(i)[7] != null) {
                                days = (edate.getTime() - sdate.getTime()) / (24 * 60 * 60 * 1000);
                            }
                            if (i == 0) {
                                grp = worklist.get(i)[1].toString();
                                if (worklist.get(i)[6] != null) {
                                    out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                            + "readonly onchange=clearField(" + i + ") onclick =GetCal('txtsdate_" + i + "','txtsdate_" + i + "'," + i + ") "
                                            + "name=txtsdate_" + i + " id=txtsdate_" + i + " value=\"" + DateUtils.gridDateToStr((Date) worklist.get(i)[6]).split(" ")[0] + "\"  />"
                                            + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                            + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtsdate_" + i + "','calc_" + i + "'," + i + ") /></a></td>");
                                    out.print("<td class=\"t-align-center\"><input type=text value=\"" + days + "\" class=formTxtBox_1 onchange=calculatedate(" + i + ",this) style=width:40px name=noofdays_" + i + " id=noofdays_" + i + " /></td>");
                                } else {
                                    out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                            + "readonly onchange=clearField(" + i + ") onclick =GetCal('txtsdate_" + i + "','txtsdate_" + i + "'," + i + ") "
                                            + "name=txtsdate_" + i + " id=txtsdate_" + i + "  />"
                                            + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                            + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtsdate_" + i + "','calc_" + i + "'," + i + ") /></a></td>");
                                    out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 onchange=calculatedate(" + i + ",this) style=width:40px name=noofdays_" + i + " id=noofdays_" + i + " /></td>");
                                }



                            } else {
                                if (grp.equalsIgnoreCase(worklist.get(i)[1].toString())) {
                                    if (worklist.get(i)[6] != null) {
                                        out.print("<td class=\"t-align-center\"><label id=lblsDate_" + i + " name=lblsDate_" + i + ">" + DateUtils.gridDateToStr((Date) worklist.get(i)[6]).split(" ")[0] + "</label></td>");
                                        out.print("<input type=hidden name=txtsdate_" + i + " id=txtsdate_" + i + " value=\"" + DateUtils.gridDateToStr((Date) worklist.get(i)[6]).split(" ")[0] + "\" />");
                                        out.print("<input type=hidden name=noofdays_" + i + " id=noofdays_" + i + " value=\"" + worklist.get(i)[8].toString() + "\" />");
                                        out.print("<td class=\"t-align-center\"><label name=lblnoofdays_" + i + " id=lblnoofdays_" + i + " >" + days + "</label></td>");
                                    } else {
                                        out.print("<td class=\"t-align-center\"><label id=lblsDate_" + i + " name=lblsDate_" + i + "></label></td>");
                                        out.print("<td class=\"t-align-center\"><label name=lblnoofdays_" + i + " id=lblnoofdays_" + i + " ></label></td>");
                                        out.print("<input type=hidden name=txtsdate_" + i + " id=txtsdate_" + i + " />");
                                        out.print("<input type=hidden name=noofdays_" + i + " id=noofdays_" + i + "  />");
                                    }
                                } else {
                                    grp = worklist.get(i)[1].toString();

                                    if (worklist.get(i)[6] != null) {
                                        out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                + "readonly onchange=clearField(" + i + ") onclick =GetCal('txtsdate_" + i + "','txtsdate_" + i + "'," + i + ") "
                                                + "name=txtsdate_" + i + " id=txtsdate_" + i + " value=\"" + DateUtils.gridDateToStr((Date) worklist.get(i)[6]).split(" ")[0] + "\"  />"
                                                + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtsdate_" + i + "','calc_" + i + "'," + i + ") /></a></td>");
                                        out.print("<td class=\"t-align-center\"><input type=text value=\"" + days + "\" class=formTxtBox_1 onchange=calculatedate(" + i + ",this) style=width:40px name=noofdays_" + i + " id=noofdays_" + i + " ></input></td>");
                                    } else {
                                        out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                + "readonly onchange=clearField(" + i + ") onclick =GetCal('txtsdate_" + i + "','txtsdate_" + i + "'," + i + ") "
                                                + "name=txtsdate_" + i + " id=txtsdate_" + i + "  />"
                                                + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtsdate_" + i + "','calc_" + i + "'," + i + ") /></a></td>");
                                        out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 onchange=calculatedate(" + i + ",this) style=width:40px name=noofdays_" + i + " id=noofdays_" + i + " /></td>");
                                    }
                                }
                            }
                            out.print("<input type=hidden name=detailid_" + i + " id=detailid_" + i + " value=\"" + worklist.get(i)[5].toString() + "\" />");
                            if (worklist.get(i)[6] != null) {
                                out.print("<td class=\"t-align-center\"><label id=lbltxtedate_" + i + " name=lbltxtedate_" + i + ">" + DateUtils.gridDateToStr((Date) worklist.get(i)[7]).split(" ")[0] + "</label></td>");
                                out.print("<input type=hidden name=txtedate_" + i + " id=txtedate_" + i + " value=\"" + DateUtils.gridDateToStr((Date) worklist.get(i)[7]).split(" ")[0] + "\"  />");
                            } else {
                                out.print("<td class=\"t-align-center\"><label id=lbltxtedate_" + i + " name=lbltxtedate_" + i + "></label></td>");
                            }
                            out.print("<input type=hidden name=txtedate_" + i + " id=txtedate_" + i + "  />");
                            out.print("</tr>");
                            out.print("<input type=\"hidden\" name=\"group_" + i + "\" id=\"group_" + i + "\" value=\"" + worklist.get(i)[1].toString() + "\" />");
                        }
                        out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");

                        int totalPages = 1;

                        if (list.size() > 0) {
                            int cc = (int) rowcount;
                            totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                    }
                } else {
                    out.print("<tr>");
                    out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"8\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
			} else if ("fetchdatesDifferGroup".equals(action)) {

            //List<SPTenderCommonData> listDP = tenderCommonService1.returndata("chkDomesticPreference", tenderId, null);
            String max = request.getParameter("size");
            String first = request.getParameter("pageNo");
            List<TblCmsWpDetail> list = service.getDatesByWpId(request.getParameter("wpId"), Integer.parseInt(first), Integer.parseInt(max));
            List<Object[]> worklist = service.getDatesByWpIdForWorks(request.getParameter("wpId"), Integer.parseInt(first), Integer.parseInt(max));
            long rowcount = service.getRowcountForBoq(request.getParameter("wpId"));
            String styleClass = "";
            try {
                if (list != null && !list.isEmpty()) {
                    if (procnature.equalsIgnoreCase("goods")) {
                        int i = 0;
                        for (i = 0; i < list.size(); i++) {
                            if (i % 2 == 0) {
                                styleClass = "bgColor-white";
                            } else {
                                styleClass = "bgColor-Green";
                            }
                            out.print("<tr class='" + styleClass + "'>");
                            out.print("<td class=\"t-align-center\">" + list.get(i).getWpSrNo() + "</td>");
                            out.print("<td class=\"t-align-center\">" + list.get(i).getWpDescription() + "</td>");
                            out.print("<td class=\"t-align-center\">" + list.get(i).getWpUom() + "</td>");
                            out.print("<td style=\"text-align :right;\">" + list.get(i).getWpQty() + "</td>");
                            out.print("<td style=\"text-align :right;\">" + list.get(i).getWpRate() + "</td>");
                            if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                            Double x,y,z;
                            x=0.00;
                            y=0.00;
                            z=0.00;
                            x=  list.get(i).getSupplierVat().doubleValue();
                            y= list.get(i).getInlandOthers().doubleValue();
                            z=x+y;
                           // BigDecimal VATInland = new BigDecimal((list.get(i).getSupplierVat())).add(list.get(i).getInlandOthers());
                                 //BigDecimal VATInland = (BigDecimal)list.get(i).getSupplierVat().add(list.get(i).getInlandOthers());
                                 //(BigDecimal)newCV.get(0)
                                    //new BigDecimal(qt).add(qty)
                            out.print("<td style=\"text-align :right;\">" + list.get(i).getCurrencyName() + "</td>");
                           // out.print("<td style=\"text-align :right;\">" + list.get(i).getSupplierVat() + "</td>");
                            //out.print("<td style=\"text-align :right;\">" + list.get(i).getSupplierVat()+ list.get(i).getInlandOthers() + "</td>");
                          // out.print("<td style=\"text-align :right;\">" + list.get(i).getSupplierVat().add(list.get(i).getInlandOthers()) + "</td>");
                             out.print("<td style=\"text-align :right;\">" + (x+y) + "</td>");
                            // out.print("<td style=\"text-align :right;\">" + z + "</td>");
                            }
                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 onchange=checkDays(" + i + ") onkeypress=\"checkEnter(event," + i + ")\"  value=" + list.get(i).getWpNoOfDays() + " name=nod_" + i + " id=nod_" + i + " /></td>");
                            out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                    + "readonly name=txtdate_" + i + " id=txtdate_" + i + " "
                                    + "value=\"" + DateUtils.gridDateToStr(list.get(i).getWpEndDate()).split(" ")[0] + "\" /></td>");
                            out.print("</tr>");
                            out.print("<input type=hidden name=detailid_" + i + " id=detailid_" + i + " value=\"" + list.get(i).getWpDetailId() + "\" />");
                        }
                        out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");

                        int totalPages = 1;

                        if (list.size() > 0) {
                            int cc = (int) rowcount;
                            totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                    } else {
                        int i = 0;
                        String grp = "";
                        for (i = 0; i < worklist.size(); i++) {
                            if (i % 2 == 0) {
                                styleClass = "bgColor-white";
                            } else {
                                styleClass = "bgColor-Green";
                            }
                            if (worklist.get(i)[12].toString().equalsIgnoreCase("variationI")) {
                                out.print("<tr class='" + styleClass + "' style=background-color:#FFFF99>");
                            } else if (worklist.get(i)[12].toString().equalsIgnoreCase("variationU")) {
                                out.print("<tr class='" + styleClass + "' style=background-color:#CDC0B0>");
                            } else {
                                out.print("<tr class='" + styleClass + "'>");
                            }

                            out.print("<td class=\"t-align-center\">" + worklist.get(i)[0].toString() + "</td>");
                            out.print("<td class=\"t-align-center\">" + worklist.get(i)[1].toString() + "</td>");

                            out.print("<td class=\"t-align-center\">" + worklist.get(i)[2].toString() + "</td>");
                            out.print("<td class=\"t-align-center\">" + worklist.get(i)[3].toString() + "</td>");
                            out.print("<td style=\"text-align :right;\">" + worklist.get(i)[4].toString() + "</td>");
                            Date sdate = (Date) worklist.get(i)[6];
                            Date edate = (Date) worklist.get(i)[7];
                            long days = 0;
                            if (worklist.get(i)[6] != null && worklist.get(i)[7] != null) {
                                days = (edate.getTime() - sdate.getTime()) / (24 * 60 * 60 * 1000);
                            }
                            if (i == 0) {
                                grp = worklist.get(i)[1].toString();
                                if (worklist.get(i)[6] != null) {
                                    out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                            + "readonly onchange=clearField(" + i + ") onclick =GetCal('txtsdate_" + i + "','txtsdate_" + i + "'," + i + ") "
                                            + "name=txtsdate_" + i + " id=txtsdate_" + i + " value=\"" + DateUtils.gridDateToStr((Date) worklist.get(i)[6]).split(" ")[0] + "\"  />"
                                            + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                            + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtsdate_" + i + "','calc_" + i + "'," + i + ") /></a></td>");
                                    out.print("<td class=\"t-align-center\"><input type=text value=\"" + days + "\" class=formTxtBox_1 onchange=calculatedate(" + i + ",this) style=width:40px name=noofdays_" + i + " id=noofdays_" + i + " /></td>");
                                } else {
                                    out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                            + "readonly onchange=clearField(" + i + ") onclick =GetCal('txtsdate_" + i + "','txtsdate_" + i + "'," + i + ") "
                                            + "name=txtsdate_" + i + " id=txtsdate_" + i + "  />"
                                            + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                            + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtsdate_" + i + "','calc_" + i + "'," + i + ") /></a></td>");
                                    out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 onchange=calculatedate(" + i + ",this) style=width:40px name=noofdays_" + i + " id=noofdays_" + i + " /></td>");
                                }



                            } else {
                                if (grp.equalsIgnoreCase(worklist.get(i)[1].toString())) {
                                    if (worklist.get(i)[6] != null) {
                                         out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                            + "readonly onchange=clearField(" + i + ") onclick =GetCal('txtsdate_" + i + "','txtsdate_" + i + "'," + i + ") "
                                            + "name=txtsdate_" + i + " id=txtsdate_" + i + " value=\"" + DateUtils.gridDateToStr((Date) worklist.get(i)[6]).split(" ")[0] + "\"  />"
                                            + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                            + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtsdate_" + i + "','calc_" + i + "'," + i + ") /></a></td>");
                                    out.print("<td class=\"t-align-center\"><input type=text value=\"" + days + "\" class=formTxtBox_1 onchange=calculatedate(" + i + ",this) style=width:40px name=noofdays_" + i + " id=noofdays_" + i + " /></td>");

                                    } else {
                                         out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                            + "readonly onchange=clearField(" + i + ") onclick =GetCal('txtsdate_" + i + "','txtsdate_" + i + "'," + i + ") "
                                            + "name=txtsdate_" + i + " id=txtsdate_" + i + "  />"
                                            + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                            + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtsdate_" + i + "','calc_" + i + "'," + i + ") /></a></td>");
                                        out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 onchange=calculatedate(" + i + ",this) style=width:40px name=noofdays_" + i + " id=noofdays_" + i + " /></td>");

                                    }
                                } else {
                                    grp = worklist.get(i)[1].toString();

                                    if (worklist.get(i)[6] != null) {
                                       out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                            + "readonly onchange=clearField(" + i + ") onclick =GetCal('txtsdate_" + i + "','txtsdate_" + i + "'," + i + ") "
                                            + "name=txtsdate_" + i + " id=txtsdate_" + i + " value=\"" + DateUtils.gridDateToStr((Date) worklist.get(i)[6]).split(" ")[0] + "\"  />"
                                            + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                            + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtsdate_" + i + "','calc_" + i + "'," + i + ") /></a></td>");
                                    out.print("<td class=\"t-align-center\"><input type=text value=\"" + days + "\" class=formTxtBox_1 onchange=calculatedate(" + i + ",this) style=width:40px name=noofdays_" + i + " id=noofdays_" + i + " /></td>");
                                    } else {
                                        out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 "
                                                + "readonly onchange=clearField(" + i + ") onclick =GetCal('txtsdate_" + i + "','txtsdate_" + i + "'," + i + ") "
                                                + "name=txtsdate_" + i + " id=txtsdate_" + i + "  />"
                                                + "&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_" + i + " src=../resources/images/Dashboard/calendarIcn.png "
                                                + "alt=Select a Date border= style=vertical-align:middle onclick=GetCal('txtsdate_" + i + "','calc_" + i + "'," + i + ") /></a></td>");
                                        out.print("<td class=\"t-align-center\"><input type=text class=formTxtBox_1 onchange=calculatedate(" + i + ",this) style=width:40px name=noofdays_" + i + " id=noofdays_" + i + " /></td>");
                                    }
                                }
                            }
                            out.print("<input type=hidden name=detailid_" + i + " id=detailid_" + i + " value=\"" + worklist.get(i)[5].toString() + "\" />");
                            if (worklist.get(i)[6] != null) {
                                out.print("<td class=\"t-align-center\"><label id=lbltxtedate_" + i + " name=lbltxtedate_" + i + ">" + DateUtils.gridDateToStr((Date) worklist.get(i)[7]).split(" ")[0] + "</label></td>");
                                out.print("<input type=hidden name=txtedate_" + i + " id=txtedate_" + i + " value=\"" + DateUtils.gridDateToStr((Date) worklist.get(i)[7]).split(" ")[0] + "\"  />");
                            } else {
                                out.print("<td class=\"t-align-center\"><label id=lbltxtedate_" + i + " name=lbltxtedate_" + i + "></label></td>");
                            }
                            out.print("<input type=hidden name=txtedate_" + i + " id=txtedate_" + i + "  />");
                            out.print("</tr>");
                            out.print("<input type=\"hidden\" name=\"group_" + i + "\" id=\"group_" + i + "\" value=\"" + i + "\" />");
                        }
                        out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");

                        int totalPages = 1;

                        if (list.size() > 0) {
                            int cc = (int) rowcount;
                            totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                    }
                } else {
                    out.print("<tr>");
                    out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"8\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("view".equals(action)) {
            String max = request.getParameter("size");
            String first = request.getParameter("pageNo");

            long rowcount = service.getRowcountForBoq(request.getParameter("wpId"));
            String styleClass = "";
            try {
                if (procnature.equalsIgnoreCase("goods")) {
                    List<TblCmsWpDetail> list = service.getDatesByWpId(request.getParameter("wpId"), Integer.parseInt(first), Integer.parseInt(max));
                    if (list != null && !list.isEmpty()) {
                        String link = "";
                        int i = 0;
                       // double InlandVAT = 0;
                        //static BigDecimal InlandOthers=0;

                        for (i = 0; i < list.size(); i++) {
                            if (i % 2 == 0) {
                                styleClass = "bgColor-white";
                            } else {
                                styleClass = "bgColor-Green";
                            }
                            out.print("<tr class='" + styleClass + "'>");
                            out.print("<td class=\"t-align-center\">" + list.get(i).getWpSrNo() + "</td>");
                            out.print("<td class=\"t-align-center\">" + list.get(i).getWpDescription() + "</td>");
                            out.print("<td class=\"t-align-center\">" + list.get(i).getWpUom() + "</td>");
                            out.print("<td style=\"text-align :right;\">" + list.get(i).getWpQty() + "</td>");
                            out.print("<td style=\"text-align :right;\">" + list.get(i).getWpRate() + "</td>");
                            if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                               // InlandVAT=list.get(i).getSupplierVat()+ list.get(i).getInlandOthers();
                            Double x,y,z;
                            x=0.00;
                            y=0.00;
                            z=0.00;
                            
                           // System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>value:"+list.get(i).getSupplierVat());
                            if(list.get(i).getSupplierVat()!=null)
                            x=  list.get(i).getSupplierVat().doubleValue();
                         //   System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>value:"+x);
                            if(list.get(i).getInlandOthers()!=null)
                            y=list.get(i).getInlandOthers().doubleValue();
                          
                            z=x+y;

                            out.print("<td style=\"text-align :right;\">" + list.get(i).getCurrencyName() + "</td>");
                            //out.print("<td style=\"text-align :right;\">" + list.get(i).getSupplierVat() + "</td>");
                           // out.print("<td style=\"text-align :right;\">" + list.get(i).getSupplierVat()+ list.get(i).getInlandOthers() + "</td>");
                             out.print("<td style=\"text-align :right;\">" + (x+y) + "</td>");
                            // out.print("<td style=\"text-align :right;\">" + z + "</td>");
                            }
                            out.print("<td class=\"t-align-center\">" + list.get(i).getWpNoOfDays() + "</td>");
                            out.print("<td class=\"t-align-center\">" + DateUtils.gridDateToStrWithoutSec(list.get(i).getWpEndDate()).split(" ")[0] + "</td>");
                            out.print("</tr>");
                            out.print("<input type=hidden name=detailid_" + i + " id=detailid_" + i + " value=\"" + list.get(i).getWpDetailId() + "\" />");
                        }
                        out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");
                        int totalPages = 1;

                        if (list.size() > 0) {
                            int cc = (int) rowcount;
                            totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                    } else {
                        out.print("<tr>");
                        out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"5\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        out.print("</tr>");
                    }
                } else {
                    List<Object[]> list = service.getDatesByWpIdForWorks(request.getParameter("wpId"), Integer.parseInt(first), Integer.parseInt(max));
                    if (list != null && !list.isEmpty()) {
                        String link = "";
                        int i = 0;
                        for (i = 0; i < list.size(); i++) {
                            if (i % 2 == 0) {
                                styleClass = "bgColor-white";
                            } else {
                                styleClass = "bgColor-Green";
                            }
                            out.print("<tr class='" + styleClass + "'>");
                            out.print("<td class=\"t-align-center\">" + list.get(i)[0] + "</td>");
                            out.print("<td class=\"t-align-center\">" + list.get(i)[1] + "</td>");
                            out.print("<td class=\"t-align-center\">" + list.get(i)[2] + "</td>");
                            out.print("<td class=\"t-align-center\">" + list.get(i)[3] + "</td>");
                            out.print("<td class=\"t-align-center\">" + list.get(i)[4] + "</td>");
                            if (list.get(i)[6] != null) {
                                out.print("<td class=\"t-align-center\">" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i)[6]).split(" ")[0] + "</td>");
                            } else {
                                out.print("<td class=\"t-align-center\">-</td>");
                            }
                            if (list.get(i)[7] != null) {
                                out.print("<td class=\"t-align-center\">" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i)[7]).split(" ")[0] + "</td>");
                            } else {
                                out.print("<td class=\"t-align-center\">-</td>");
                            }

                            out.print("</tr>");
                        }
                        int totalPages = 1;

                        if (list.size() > 0) {
                            int cc = (int) rowcount;
                            totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                    } else {
                        out.print("<tr>");
                        out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"7\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        out.print("</tr>");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (request.getParameter("tenview") != null) {
                    //makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), Integer.parseInt(request.getParameter("tenderId")), "tenderId", EgpModule.Noa.getName(), "View Consolidation by Tenderer", "");
                } else {
                    //makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), Integer.parseInt(request.getParameter("tenderId")), "tenderId", EgpModule.Noa.getName(), "View Consolidation by PE", "");
                }

            }

        } else if ("viewInWorks".equals(action)) {
            String max = request.getParameter("size");
            String first = request.getParameter("pageNo");
            List<Object[]> list = service.getDatesByWpIdForWorks(request.getParameter("wpId"), Integer.parseInt(first), Integer.parseInt(max));
            long rowcount = service.getRowcountForBoq(request.getParameter("wpId"));
            String styleClass = "";
            try {
                if (list != null && !list.isEmpty()) {
                    String link = "";
                    int i = 0;
                    for (i = 0; i < list.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }
                        if (list.get(i)[12].toString().equalsIgnoreCase("variationI")) {
                            out.print("<tr class='" + styleClass + "' style=background-color:#FFFF99>");
                        } else if (list.get(i)[12].toString().equalsIgnoreCase("variationU")) {
                            out.print("<tr class='" + styleClass + "' style=background-color:#CDC0B0>");
                        } else {
                            out.print("<tr class='" + styleClass + "'>");
                        }
                        out.print("<td class=\"t-align-center\">" + list.get(i)[0] + "</td>");
                        out.print("<td class=\"t-align-center\">" + list.get(i)[1] + "</td>");
                        out.print("<td class=\"t-align-center\">" + list.get(i)[2] + "</td>");
                        out.print("<td class=\"t-align-center\">" + list.get(i)[3] + "</td>");
                        out.print("<td style=\"text-align :right;\">" + list.get(i)[4] + "</td>");
                        out.print("<td class=\"t-align-center\">" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i)[6]).split(" ")[0] + "</td>");
                        out.print("<td class=\"t-align-center\">" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i)[7]).split(" ")[0] + "</td>");
                        out.print("</tr>");
                        out.print("<input type=hidden name=detailid_" + i + " id=detailid_" + i + " value=\"" + list.get(i)[5] + "\" />");
                    }
                    out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");
                    int totalPages = 1;

                    if (list.size() > 0) {
                        int cc = (int) rowcount;
                        totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                } else {
                    out.print("<tr>");
                    out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"5\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("viewInWorksforvari".equals(action)) {
            String max = request.getParameter("size");
            String first = request.getParameter("pageNo");
            List<Object[]> list = service.getDatesByWpIdForWorks(request.getParameter("wpId"), Integer.parseInt(first), Integer.parseInt(max));
            long rowcount = service.getRowcountForBoq(request.getParameter("wpId"));
            String styleClass = "";
            try {
                if (list != null && !list.isEmpty()) {
                    String link = "";
                    int i = 0;
                    for (i = 0; i < list.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }
                        if (list.get(i)[12].toString().equalsIgnoreCase("variationI")) {
                            out.print("<tr class='" + styleClass + "' style=background-color:#FFFF99>");
                        } else if (list.get(i)[12].toString().equalsIgnoreCase("variationU")) {
                            out.print("<tr class='" + styleClass + "' style=background-color:#CDC0B0>");
                        } else {
                            out.print("<tr class='" + styleClass + "'>");
                        }

                        out.print("<td class=\"t-align-center\">" + list.get(i)[0] + "</td>");
                        out.print("<td class=\"t-align-center\">" + list.get(i)[1] + "</td>");
                        out.print("<td class=\"t-align-center\">" + list.get(i)[2] + "</td>");
                        out.print("<td class=\"t-align-center\">" + list.get(i)[3] + "</td>");
                        out.print("<td class=\"t-align-center\">" + list.get(i)[4] + "</td>");
                        out.print("<td class=\"t-align-center\">" + list.get(i)[11] + "</td>");
                        out.print("<td class=\"t-align-center\">" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i)[6]).split(" ")[0] + "</td>");
                        out.print("<td class=\"t-align-center\">" + DateUtils.gridDateToStrWithoutSec((Date) list.get(i)[7]).split(" ")[0] + "</td>");
                        out.print("</tr>");
                        out.print("<input type=hidden name=detailid_" + i + " id=detailid_" + i + " value=\"" + list.get(i)[5] + "\" />");
                    }
                    out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");
                    int totalPages = 1;

                    if (list.size() > 0) {
                        int cc = (int) rowcount;
                        totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(max));
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                } else {
                    out.print("<tr>");
                    out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"5\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "ContractId", EgpModule.Variation_Order.getName(), "View Variation Order", "Tenderer side");
            }
        }

    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);






    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);






    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";



    }// </editor-fold>

    /** send mail notification to tenderer on configuring the Delivery Schedule date by PE */
    private boolean sendMailForDeliverySchedule(String tenderId, String lotId, String logUserId, String Operation, String mailSubject) {
        boolean flag = false;
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peName = "";
            String strTo[] = {obj[9].toString()};
            String strFrom = commonservice.getEmailId(logUserId);
            List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(logUserId));
            if (peNameObj.get(0) != null) {
                peName = peNameObj.get(0).toString();
            }
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            registerService.contentAdmMsgBox(strTo[0], strFrom, mailSubject, msgBoxContentUtility.EditDeliveryScheduleDt(Operation, c_isPhysicalPrComplete, peName, obj));
            String mailText = mailContentUtility.EditDeliveryScheduleDt(Operation, c_isPhysicalPrComplete, peName, obj);
            sendMessageUtil.setEmailTo(strTo);
            sendMessageUtil.setEmailFrom(strFrom);
            sendMessageUtil.setEmailSub(mailSubject);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            sendMessageUtil.setSmsNo(accPaymentService.getMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
            StringBuilder sb = new StringBuilder();
            sb.append("Dear User,%0AThis is to inform you that PE " + Operation + " Delivery Schedule Date(s) ");
            sb.append("for Contract No." + obj[1].toString() + " (" + obj[15].toString() + ")" + " ");
            sb.append("%0AThanks,%0AeGP System");
            sendMessageUtil.setSmsBody(sb.toString());
            sendMessageUtil.sendSMS();
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }

    /** send mail notification to tenderer on finalizing  the progress report by PE */
    private boolean sendMailForFinalizedReport(String tenderId, String lotId, String logUserId, String Operation, String mailSubject) {
        boolean flag = false;
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peName = "";
            String strTo[] = {obj[9].toString()};
            String strFrom = commonservice.getEmailId(logUserId);
            List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(logUserId));
            if (peNameObj.get(0) != null) {
                peName = peNameObj.get(0).toString();
            }
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            registerService.contentAdmMsgBox(strTo[0], strFrom, mailSubject, msgBoxContentUtility.finalizingProgressReport(Operation, c_isPhysicalPrComplete, peName, obj));
            String mailText = mailContentUtility.finalizingProgressReport(Operation, c_isPhysicalPrComplete, peName, obj);
            sendMessageUtil.setEmailTo(strTo);
            sendMessageUtil.setEmailFrom(strFrom);
            sendMessageUtil.setEmailSub(mailSubject);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            sendMessageUtil.setSmsNo(accPaymentService.getMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
            StringBuilder sb = new StringBuilder();
            sb.append("Dear User,%0AThis is to inform you that PE has Finalized Progress Report ");
            sb.append("for Contract No." + obj[1].toString() + " (" + obj[15].toString() + ")" + " ");
            sb.append("%0AThanks,%0AeGP System");
            sendMessageUtil.setSmsBody(sb.toString());
            sendMessageUtil.sendSMS();
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }

    /** send mail notification to tenderer on placing the variation order by PE */
    private boolean sendMailForVariationOrderFromPE(String tenderId, String lotId, String logUserId, String mailSubject) {
        boolean flag = false;
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peName = "";
            String strTo[] = {obj[9].toString()};
            String strFrom = commonservice.getEmailId(logUserId);
            List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(logUserId));
            if (peNameObj.get(0) != null) {
                peName = peNameObj.get(0).toString();
            }
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            registerService.contentAdmMsgBox(strTo[0], strFrom, mailSubject, msgBoxContentUtility.variationOrderFromPEToContractor(c_isPhysicalPrComplete, peName, obj));
            String mailText = mailContentUtility.variationOrderFromPEToContractor(c_isPhysicalPrComplete, peName, obj);
            sendMessageUtil.setEmailTo(strTo);
            sendMessageUtil.setEmailFrom(strFrom);
            sendMessageUtil.setEmailSub(mailSubject);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            sendMessageUtil.setSmsNo(accPaymentService.getMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
            StringBuilder sb = new StringBuilder();
            sb.append("Dear User,%0AThis is to inform you that PE has placed Variation Order");
            sb.append("for Contract No." + obj[1].toString() + " (" + obj[15].toString() + ")" + " ");
            sb.append("%0AThanks,%0AeGP System");
            sendMessageUtil.setSmsBody(sb.toString());
            sendMessageUtil.sendSMS();
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }

    /** send mail notification to PE on accepting the variation order by tenderer */
    private boolean sendMailForVariationOrderAcceptedFromTenderer(String tenderId, String lotId, String logUserId, String mailSubject) {
        boolean flag = false;
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peEmailId = "";
            String peName = "";
            String strFrom = commonservice.getEmailId(logUserId);
            List<Object[]> peEmailObj = accPaymentService.getPEemailID(Integer.parseInt(tenderId));
            if (!peEmailObj.isEmpty()) {
                Object[] objj = peEmailObj.get(0);
                peEmailId = objj[0].toString();
            }
            if ("1".equals(obj[12].toString())) {
                peName = obj[10].toString() + " " + obj[11].toString();
            } else {
                peName = obj[8].toString();
            }
            String strTo[] = {peEmailId};
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            registerService.contentAdmMsgBox(strTo[0], strFrom, mailSubject, msgBoxContentUtility.variationOrderFromContractorToPE(c_isPhysicalPrComplete, peName, obj));
            String mailText = mailContentUtility.variationOrderFromContractorToPE(c_isPhysicalPrComplete, peName, obj);
            sendMessageUtil.setEmailTo(strTo);
            sendMessageUtil.setEmailFrom(strFrom);
            sendMessageUtil.setEmailSub(mailSubject);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
            StringBuilder sb = new StringBuilder();
            sb.append("Dear User,%0AThis is to inform you that Contractor has accepted Variation Order ");
            sb.append("for Contract No." + obj[1].toString() + " (" + obj[15].toString() + ")" + " ");
            sb.append("%0AThanks,%0AeGP System");
            sendMessageUtil.setSmsBody(sb.toString());
            sendMessageUtil.sendSMS();
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }

    /** send mail notification to tenderer on publishing the approved variation order by PE */
    private boolean sendMailForVariationOrderPublishedFromPE(String tenderId, String lotId, String childId, String logUserId, String mailSubject) {
        boolean flag = false;
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);

            String strFrom = commonservice.getEmailId(logUserId);
            List<Object[]> WFUserEmailId = cmsContractTerminationService.getWFUserEmailId(Integer.parseInt(childId), Integer.parseInt(childId), Integer.parseInt(logUserId));
            List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(logUserId));
            String peName = "";
            int emailIndex = 0;
            String[] strTo = null;
            if (peNameObj.get(0) != null) {
                peName = peNameObj.get(0).toString();
            }
            strTo = new String[WFUserEmailId.size() + 1];
            for (Object[] emailId : WFUserEmailId) {
                strTo[emailIndex] = emailId[0].toString();
                emailIndex++;
            }
            strTo[emailIndex] = obj[9].toString();
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String emailId[] = new String[1];
            String mobileno = "";
            for (int i = 0; i < strTo.length; i++) {
                emailId[0] = strTo[i];
                registerService.contentAdmMsgBox(emailId[0], strFrom, mailSubject, msgBoxContentUtility.variationOrderPublished(c_isPhysicalPrComplete, peName, obj));
                String mailText = mailContentUtility.variationOrderPublished(c_isPhysicalPrComplete, peName, obj);
                sendMessageUtil.setEmailTo(emailId);
                sendMessageUtil.setEmailFrom(strFrom);
                sendMessageUtil.setEmailSub(mailSubject);
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                if ("2".equalsIgnoreCase(accPaymentService.getUserTypeId(emailId[0]))) {
                    mobileno = accPaymentService.getMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                } else {
                    mobileno = accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                }
                sendMessageUtil.setSmsNo(mobileno);
                StringBuilder sb = new StringBuilder();
                sb.append("Dear User,%0AThis is to inform you that PE has Issued Variation Order ");
                sb.append("for Contract No." + obj[1].toString() + " (" + obj[15].toString() + ")" + " ");
                sb.append("%0AThanks,%0AeGP System");
                sendMessageUtil.setSmsBody(sb.toString());
                sendMessageUtil.sendSMS();
            }
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }

    /** send mail notification to tenderer on finalizing Repeat Order by PE */
    private boolean sendMailForFinaliseRo(String tenderId, String lotId, String logUserId) {
        boolean flag = false;
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String strFrom = commonservice.getEmailId(logUserId);
            List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(logUserId));
            String peName = "";
            int emailIndex = 0;
            if (peNameObj.get(0) != null) {
                peName = peNameObj.get(0).toString();
            }
            String strTo[] = {obj[9].toString()};
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String mailSubject = "Repeat Order has been Finalized by PE " + peName + "";
            String emailId[] = new String[1];
            String mobileno = "";
            for (int i = 0; i < strTo.length; i++) {
                emailId[0] = strTo[i];
                registerService.contentAdmMsgBox(emailId[0], strFrom, mailSubject, msgBoxContentUtility.MailForFinaliseRo(c_isPhysicalPrComplete, peName, obj));
                String mailText = mailContentUtility.MailForFinaliseRo(c_isPhysicalPrComplete, peName, obj);
                sendMessageUtil.setEmailTo(emailId);
                sendMessageUtil.setEmailFrom(strFrom);
                sendMessageUtil.setEmailSub(mailSubject);
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                if ("2".equalsIgnoreCase(accPaymentService.getUserTypeId(emailId[0]))) {
                    mobileno = accPaymentService.getMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                } else {
                    mobileno = accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                }
                sendMessageUtil.setSmsNo(mobileno);
                StringBuilder sb = new StringBuilder();
                sb.append("Dear User,%0AThis is to inform you that PE has Finalised Repeat Order ");
                sb.append("for Contract No." + obj[1].toString() + " (" + obj[15].toString() + ")" + " ");
                sb.append("%0AThanks,%0AeGP System");
                sendMessageUtil.setSmsBody(sb.toString());
                sendMessageUtil.sendSMS();
            }
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }
}