/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblAskProcurement;
import com.cptu.egp.eps.model.table.TblQuestionCategory;
import com.cptu.egp.eps.service.serviceimpl.AskProcurementImpl;
import com.cptu.egp.eps.service.serviceinterface.AskProcurementService;
import com.cptu.egp.eps.web.databean.AskProcurementDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author rishita
 */
public class AskProcurementSrBean {

    //private AskProcurementServiceImpl askProcurementSrvImpl = (AskProcurementServiceImpl) AppContext.getSpringBean("askProcurementService");
    private final AskProcurementImpl askProcurementImpl = (AskProcurementImpl) AppContext.getSpringBean("AskProcurementImpl");
    private static final Logger LOGGER = Logger.getLogger(AskProcurementSrBean.class);
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";
    private String logUserId = "0";
    private AuditTrail auditTrail;
    private final AskProcurementService askProcurementService = (AskProcurementService) AppContext.getSpringBean("askProcurementService");
    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        askProcurementImpl.setLogUserId(logUserId);
        askProcurementService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        askProcurementService.setAuditTrail(auditTrail);
        askProcurementImpl.setAuditTrail(auditTrail);
    }
    
    
    /**
     * Fetching data for Procurement Expert when procurement Expert gives reply
     * @param procQueId primary key
     * @return List of data
     */
    public List<Object[]> getListQuestion(int procQueId) {
        LOGGER.debug("getListQuestion : " + logUserId + LOGGERSTART);
        List<Object[]> list = null;
        try {
            list = askProcurementImpl.getListAskProcurement(procQueId);
        } catch (Exception e) {
            LOGGER.error("getListQuestion : " + logUserId, e);
        }
        LOGGER.debug("getListQuestion : " + logUserId + LOGGEREND);
        return list;
    }

    /**
     * updating data for Procurement Expert when procurement Expert gives reply
     * @param askProcurementDtBean
     * @return true if updated successfully
     */
    public boolean updateAskProcurement(AskProcurementDtBean askProcurementDtBean) {
        LOGGER.debug("updateAskProcurement : " + logUserId + LOGGERSTART);
        boolean updateAskProc = false;
        try {
            TblAskProcurement tblAskProcurement = toTblAskProcurement(askProcurementDtBean);
            tblAskProcurement.setTblQuestionCategory(new TblQuestionCategory(askProcurementDtBean.getQueCatId()));
            tblAskProcurement.setAnswerDate(new Date());
            if (askProcurementImpl.updateAskProcuremennt(tblAskProcurement)) {
                updateAskProc = true;
            }
        } catch (Exception e) {
            LOGGER.error("updateAskProcurement : " + logUserId, e);
        }
        LOGGER.debug("updateAskProcurement : " + logUserId + LOGGEREND);
        return updateAskProc;
    }

    /**
     * copies properties
     * @param askProcurementDtBean
     * @return Object with copied properties
     */
    public TblAskProcurement toTblAskProcurement(AskProcurementDtBean askProcurementDtBean) {
        LOGGER.debug("toTblAskProcurement : " + logUserId + LOGGERSTART);
        TblAskProcurement tblAskProcurement = new TblAskProcurement();
        try {
            BeanUtils.copyProperties(askProcurementDtBean, tblAskProcurement);
        } catch (Exception e) {
            LOGGER.error("_toTblAskProcurement : " + logUserId, e);
        }
        LOGGER.debug("toTblAskProcurement : " + logUserId + LOGGEREND);
        return tblAskProcurement;
    }
    

    /**
     * Fetching information for Procurement Category
     * @return list of TblQuestionCategory
     */
    public List<TblQuestionCategory> getAllQusetions() {
        LOGGER.debug("getAllQusetions : " + logUserId + LOGGERSTART);
        List<TblQuestionCategory> tqc = null;
        try {
            tqc = askProcurementService.getAllQusetions();
        } catch (Exception e) {
            LOGGER.error("getAllQusetions : " + logUserId, e);
        }
        LOGGER.debug("getAllQusetions : " + logUserId + LOGGEREND);
        return tqc;
    }

    /**
     * Fetching all information for particular user for ask procurement
     * @param id - postedBy of tbl_AskProcurements
     * @return List of TblAskProcurement
     */
    public List<TblAskProcurement> getAllAskProcurement(int id) {
        LOGGER.debug("getAllAskProcurement : " + logUserId + LOGGERSTART);
        List<TblAskProcurement> tap = null;
        try {
            tap = askProcurementService.getAskProcurementDetails(id);
        } catch (Exception e) {
            LOGGER.error("getAllAskProcurement : " + logUserId, e);
        }
        LOGGER.debug("getAllAskProcurement : " + logUserId + LOGGEREND);
        return tap;
    }

    /**
     * Fetching all information
     * @param id primary key of Tbl_AskProcurement (procQueId)
     * @return List of TblAskProcurement
     */
    public List<TblAskProcurement> getAskProcurement(int id) {
        LOGGER.debug("getAskProcurement : " + logUserId + LOGGERSTART);
        List<TblAskProcurement> tap = null;
        try {
            tap = askProcurementService.getAskProcurement(id);
        } catch (Exception e) {
            LOGGER.error("getAskProcurement : " + logUserId, e);
        }
        LOGGER.debug("getAskProcurement : " + logUserId + LOGGEREND);
        return tap;
    }

    /**
     * insert values for asking question to Procurement Expert
     * @param tblAskProcurement
     * @return message about values inserted or not
     */
    public String addTblAskProcurement(TblAskProcurement tblAskProcurement) {
        LOGGER.debug("addTblAskProcurement : " + logUserId + LOGGERSTART);
        String msg = "";
        try {
            msg = askProcurementService.addTblAskProcurement(tblAskProcurement);
        } catch (Exception e) {
            LOGGER.error("addTblAskProcurement : " + logUserId, e);
        }
        LOGGER.debug("addTblAskProcurement : " + logUserId + LOGGEREND);
        return msg;
    }
}
