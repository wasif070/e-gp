/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.model.table.TblTenderPostQueConfig;
import com.cptu.egp.eps.service.serviceinterface.TenderPostQueConfigService;
import com.cptu.egp.eps.web.databean.PreTendQueryDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.DateUtils;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author dixit
 */
public class TenderPostQueConfigServlet extends HttpServlet {

    /**this servlet handles all the request for the ConfigTenderClari.jsp page
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private TenderPostQueConfigService tenderPostQueConfigService = (TenderPostQueConfigService) AppContext.getSpringBean("TenderPostQueConfigService");
    private static final Logger LOGGER = Logger.getLogger(TenderPostQueConfigServlet.class);
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String logUserId = "0";
        if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
                tenderPostQueConfigService.setLogUserId(logUserId);
            }
        try {
            LOGGER.debug("processRequest : "+logUserId+" Starts");
            String action = request.getParameter("action");

            /*this saveclarification action saves the clarification to database*/

            if("saveclarification".equalsIgnoreCase(action))
            {
                LOGGER.debug("saveclarification : "+logUserId+" Starts");
                String lastdate ="";
                String tenderid ="";
                String isclarification ="";
                int queryId = 0;
                boolean flag = false;
                boolean IsUpdatedBasedOnSBD=false;
                if(request.getParameter("tenderId")!=null)
                {
                    tenderid = request.getParameter("tenderId");
                }
                if(request.getParameter("IsUpdatedBasedOnSBD")!=null && request.getParameter("IsUpdatedBasedOnSBD").equalsIgnoreCase("YES"))
                {
                    IsUpdatedBasedOnSBD = true;
                }
                if(request.getParameter("claricombo")!=null)
                {
                    isclarification = request.getParameter("claricombo");
                }
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                Date ldate = new Date();
                if(request.getParameter("lastdate")!=null)
                {
                    lastdate = request.getParameter("lastdate");
                }
                if("Yes".equalsIgnoreCase(isclarification))
                {
                    ldate = format.parse(lastdate);
                }else{
                    ldate = null;
                }
                if(request.getParameter("queryId")!=null)
                {
                    queryId = Integer.parseInt(request.getParameter("queryId"));
                }
                int userid = Integer.parseInt(logUserId);
                Date date = new Date();
                int tenderID = Integer.parseInt(tenderid);
                TblTenderPostQueConfig tblTenderPostQueConfig = new TblTenderPostQueConfig();

                tblTenderPostQueConfig.setCreatedBy(userid);
                tblTenderPostQueConfig.setCreatedDt(date);
                tblTenderPostQueConfig.setIsQueAnsConfig(isclarification);
                tblTenderPostQueConfig.setPostQueLastDt(ldate);
                tblTenderPostQueConfig.setTblTenderMaster(new TblTenderMaster(tenderID));

                LOGGER.debug("saveclarification : "+logUserId+" Ends");

                if("Submit".equalsIgnoreCase(request.getParameter("submitlastdate")))
                {
                    tenderPostQueConfigService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
                    flag = tenderPostQueConfigService.saveConfiguration(tblTenderPostQueConfig);
                    if(IsUpdatedBasedOnSBD)
                    {
                        response.sendRedirect("officer/Notice.jsp?tenderid="+tenderid);
                    }
                    else
                    {
                        if(flag)
                        {
                            response.sendRedirect("officer/Notice.jsp?tenderid="+tenderid+"&message=success");
                        }else
                        {
                            response.sendRedirect("officer/Notice.jsp?tenderid="+tenderid+"&message=fail");
                        }
                    }
                }
                else
                {
                    tblTenderPostQueConfig.setPostQueConfigId(queryId);
                    tenderPostQueConfigService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
                    flag = tenderPostQueConfigService.updateConfiguration(tblTenderPostQueConfig);
                    if(IsUpdatedBasedOnSBD)
                    {
                        response.sendRedirect("officer/Notice.jsp?tenderid="+tenderid);
                    }
                    else
                    {
                        if(flag)
                        {
                            response.sendRedirect("officer/Notice.jsp?tenderid="+tenderid+"&message=update");
                        }else
                        {
                            response.sendRedirect("officer/Notice.jsp?tenderid="+tenderid+"&message=updatefail");
                        }
                    }
                }
            }
            else if("validatelastdate".equalsIgnoreCase(action))
            {
                String lastdate = request.getParameter("param1");
                String tenderid = request.getParameter("param2");
                String str = validateLastDate(lastdate,tenderid);
                out.print(str);
            }
        } catch (ParseException ex) {
            LOGGER.error("TenderPostQueConfigServlet : " + logUserId + " " + ex);
        } finally {
            out.close();
        }
        LOGGER.debug("processRequest : "+logUserId+" Ends");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>


    /*this method checks the date validation */

        private String validateLastDate(String lastdate,String tenderid) {
        String str = "";
        try {
            LOGGER.error("validateLastDate : Starts");
            int tenderID = Integer.parseInt(tenderid);
            PreTendQueryDtBean preTendDtBean = new PreTendQueryDtBean();
            List<TblTenderDetails> getPreBidStartDate = tenderPostQueConfigService.getPrebidStartDate(tenderid);
            //List<SPTenderCommonData> getConfigQus = preTendDtBean.getDataFromSP("GetConfigPrebid", tenderID, 0);
            List<SPTenderCommonData> getClosingDt = preTendDtBean.getDataFromSP("GetClosingDt", tenderID, 0);

            //Date lastDate = DateUtils.formatStdString(lastdate);
            Date lastDate = DateUtils.convertDateToStr(lastdate);
            if(!getPreBidStartDate.isEmpty())
            {
                SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                if(getPreBidStartDate.get(0).getPreBidStartDt()!=null){
                    String pretendermetdatestr = s.format(getPreBidStartDate.get(0).getPreBidStartDt());
                    Date pretendermetdate = DateUtils.convertStringtoDate(pretendermetdatestr,"yyyy-MM-dd HH:mm:ss");
                    if(pretendermetdate.before(lastDate))
                    {
                        str = "Last Date and Time for posting of query must be prior to Pre - Tender Meeting start Date and Time";
                    }
                }
            }
            if(!getClosingDt.isEmpty() && "".equals(str))
            {
                if(getClosingDt.get(0).getFieldName2()!=null){
                    Date closingdate = DateUtils.convertStringtoDate(getClosingDt.get(0).getFieldName2(),"dd-MMM-yyyy hh:mm");
                    if(closingdate.before(lastDate))
                    {
                        str = "Last Date and Time for posting of query must be prior to Tender Closing Date and Time";
                    }
                }else{
                    str= "Eror";
                }
            }
            if(lastDate!=null && "".equals(str))
            {
                SimpleDateFormat sd1 = new SimpleDateFormat("dd/MM/yyyy");
                Date date = new Date();
                if(lastDate.before(date))
                {
                    str = "Last Date and Time for posting of query must be after today's Date and Time";
                }
            }
        } catch (Exception ex) {
            LOGGER.error("validateLastDate : "+ex.toString());
        }
        LOGGER.error("validateLastDate : Ends");
         return str;
    }
}
