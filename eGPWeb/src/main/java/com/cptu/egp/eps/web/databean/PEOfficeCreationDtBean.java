/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;
import java.lang.*;


/**
 *
 * @author rishita
 */
public class PEOfficeCreationDtBean {

    private Short countryId;
    private Short stateId;
    private int officeId;
    private String officeName;
    private String address;
    private String city;
    private String upjilla;
    private String phoneNo;
    private String faxNo;
    private String postCode;
    private short txtdepartmentid;
    private short txtchk;
    private int departmentId;
    private byte[] officeNameInBangla;
    private String deptBanglaString;
    private String peCode;
    private String strCreatedDate;
    private String officeType;

    public short getTxtchk() {
        return txtchk;
    }

    public void setTxtchk(short txtchk) {
        this.txtchk = txtchk;
    }

    
    public String getStrCreatedDate() {
        return strCreatedDate;
    }

    public int getDepartmentid() {
        return departmentId;
    }

    public void setDepartmentid(int departmentId) {
        this.departmentId = departmentId;
    }

    public void setStrCreatedDate(String strCreatedDate) {
        this.strCreatedDate = strCreatedDate;
    }

    public int getOfficeId() {
        return officeId;
    }

    public void setOfficeId(int officeId) {
        this.officeId = officeId;
    }


    public short getTxtdepartmentid() {
        return txtdepartmentid;
    }

    public void setTxtdepartmentid(short txtdepartmentid) {
        this.txtdepartmentid = txtdepartmentid;
    }

    public String getPeCode() {
        return peCode;
    }

    public void setPeCode(String peCode) {
        this.peCode = peCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Short getCountryId() {
        return countryId;
    }

    public void setCountryId(Short countryId) {
        this.countryId = countryId;
    }

    public String getDeptBanglaString() {
        return deptBanglaString;
    }

    public void setDeptBanglaString(String deptBanglaString) {
        this.deptBanglaString = deptBanglaString;
    }

    public String getFaxNo() {
        return faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    public byte[] getOfficeNameInBangla() {
        return officeNameInBangla;
    }

    public void setOfficeNameInBangla(byte[] officeNameInBangla) {
        this.officeNameInBangla = officeNameInBangla;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public Short getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        String delimiters = ",";
        String[] newState = stateId.split(delimiters);
        this.stateId = Short.parseShort(newState[1]);
    }

    public String getUpjilla() {
        return upjilla;
    }

    public void setUpjilla(String upjilla) {
        this.upjilla = upjilla;
    }

     /**
     * @return the officeType
     */
    public String getOfficeType() {
        return officeType;
    }

    /**
     * @param officeType the officeType to set
     */
    public void setOfficeType(String officeType) {
        this.officeType = officeType;
    }
}
