/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.utility;

import com.cptu.egp.eps.service.serviceinterface.UserLoginService;
import org.apache.log4j.Logger;

/**
 * <b>LogoutUtils</b> <Description Goes Here> Nov 25, 2010 5:55:33 PM
 * @author Administrator
 */
public class LogoutUtils {
    static final Logger logger = Logger.getLogger(LogoutUtils.class);
    private String logUserId="0";

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public void updateSessionMaster(int sessionId)
    {
        logger.debug("updateSessionMaster");
        UserLoginService userLoginService=(UserLoginService) AppContext.getSpringBean("UserLoginService");
        userLoginService.setUserId(logUserId);
        userLoginService.updateSessionMaster(sessionId);
    }
}
