package com.cptu.egp.eps.web.utility;

import java.applet.Applet;
import java.awt.Label;
import java.io.IOException;
import java.security.AccessController;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivilegedAction;
import java.security.spec.InvalidKeySpecException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 *
 * @author Sanjay Prajapati
 */
public class SignerAPI extends Applet {

    private static final String VERSION = "$1.0 $";
    private static final String COPYRIGHT = "Copyright (c) 2010, CPTU, Dhaka";
    String data = null;
    String encrypt = null;

    /**
     * getVersion gets the API version
     *
     * @return the api version
     */
    public String getVersion() {
        return VERSION;
    }

    /**
     * gets the copyright string
     *
     * @return the copyright string
     */
    public String getCopyright() {
        return COPYRIGHT;
    }

    @Override
    public void init() {
        add(new Label(getCopyright()));
    }

    /**
     * Checks the Java Platform environment with its version
     * @return
     */
    public int checkEnvironment() {
        String java_version = System.getProperty("java.version");
        if (!java_version.substring(0, 3).equals("1.6")) {
            return (1);
        }

        return (0);
    }
    private String messageDigestType = "SHA1"; // Encryption Algorithm 

    /**
     * Set Encrypt Algorithm
     * @param Algorithm Type
     */
    public void setMessageDigestType(String type) {
        messageDigestType = type;
    }

    /**
     * Get Message sign string with SHA1 algorithm
     * @return Sign String value with SHA1 Algorithm
     */
    public String getMessageDigest() {
        String digest = null;
        try {
            MessageDigest md = MessageDigest.getInstance(messageDigestType);
            byte[] badigest = md.digest(data.getBytes());
            StringBuilder buf = new StringBuilder();
            for (int i = 0; i < badigest.length; i++) {
                String hex = Integer.toHexString(0xff & badigest[i]);
                if (hex.length() == 1) {
                    buf.append("0");
                }
                buf.append(hex);
            }
            digest = buf.toString();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (digest);
    }

    /**
     * Get Policy checking status - get applet permission
     * @return boolean true / false
     */
    public boolean policyTest() {
        byte[] tData = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
        boolean flag = true;

        SecretKeySpec key64 = new SecretKeySpec(
                new byte[]{0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07},
                "Blowfish");

        try {
            Cipher c = Cipher.getInstance("Blowfish/ECB/NoPadding");

            c.init(Cipher.ENCRYPT_MODE, key64);

            c.doFinal(tData);
        } catch (SecurityException e) {
            if (e.getMessage() == null ? "Unsupported keysize or algorithm parameters" == null : e.getMessage().equals("Unsupported keysize or algorithm parameters")) {
                flag = false;
            } else {
                flag = false;
            }
        } catch (GeneralSecurityException e) {
            flag = false;
        }

        SecretKeySpec key192 = new SecretKeySpec(
                new byte[]{0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                    0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
                    0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17},
                "Blowfish");

        try {
            Cipher c = Cipher.getInstance("Blowfish/ECB/NoPadding");

            c.init(Cipher.ENCRYPT_MODE, key192);

            c.doFinal(tData);

        } catch (SecurityException e) {
            if (e.getMessage() == null ? "Unsupported keysize or algorithm parameters" == null : e.getMessage().equals("Unsupported keysize or algorithm parameters")) {
                flag = false;
            } else {
                flag = false;
            }
        } catch (GeneralSecurityException e) {
            flag = false;
        }

        return flag;
    }
    String password;

    /**
     * Get Symmetric Encrypt
     * @param password2
     * @return Encrypted String
     */
    public String getSymEncrypt(String password2) {
        this.password = password2;
        String encrypted = AccessController.doPrivileged(new PrivilegedAction<String>() {

            @Override
            public String run() {
                try {
                    char[] bpassword = password.toCharArray();
                    byte[] salt = new byte[8];
                    Random r = new Random();
                    r.nextBytes(salt);
                    PBEKeySpec kspec = new PBEKeySpec(bpassword);
                    SecretKeyFactory kfact = SecretKeyFactory.getInstance("pbewithmd5anddes");
                    SecretKey key = kfact.generateSecret(kspec);
                    PBEParameterSpec pspec = new PBEParameterSpec(salt, 1000);
                    Cipher cipher = Cipher.getInstance("pbewithmd5anddes");
                    cipher.init(Cipher.ENCRYPT_MODE, key, pspec);
                    byte[] enc = cipher.doFinal(data.getBytes());
                    BASE64Encoder encoder = new BASE64Encoder();
                    String saltString = encoder.encode(salt);
                    String ciphertextString = encoder.encode(enc);
                    return saltString + ciphertextString;
                } catch (IllegalBlockSizeException ex) {
                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (BadPaddingException ex) {
                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvalidKeyException ex) {
                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvalidAlgorithmParameterException ex) {
                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NoSuchPaddingException ex) {
                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvalidKeySpecException ex) {
                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NoSuchAlgorithmException ex) {
                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        });
        return encrypted;
    }

    /**
     * Get Symmetric Decrypted String
     * @param password2
     * @return Decrypt String 
     */
    public String getSymDecrypt(String password2) {
        this.password = password2;
        String decrypted = AccessController.doPrivileged(new PrivilegedAction<String>() {

            @Override
            public String run() {
                try {
                    String salt = encrypt.substring(0, 12);
                    String ciphertext = encrypt.substring(12, encrypt.length());
                    BASE64Decoder decoder = new BASE64Decoder();
                    byte[] saltArray = decoder.decodeBuffer(salt);
                    byte[] ciphertextArray = decoder.decodeBuffer(ciphertext);
                    PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray());
                    SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("pbewithmd5anddes");
                    SecretKey key = keyFactory.generateSecret(keySpec);
                    PBEParameterSpec paramSpec = new PBEParameterSpec(saltArray, 1000);
                    Cipher cipher = Cipher.getInstance("pbewithmd5anddes");
                    cipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
                    byte[] plaintextArray = cipher.doFinal(ciphertextArray);
                    return new String(plaintextArray);
                } catch (IllegalBlockSizeException ex) {
                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (BadPaddingException ex) {
                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvalidKeyException ex) {
                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvalidAlgorithmParameterException ex) {
                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NoSuchPaddingException ex) {
                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvalidKeySpecException ex) {
                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NoSuchAlgorithmException ex) {
                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SignerAPI.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        });
        return decrypted;
    }
}
