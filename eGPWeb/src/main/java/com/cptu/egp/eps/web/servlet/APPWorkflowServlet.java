/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonAppSearchData;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig;
import com.cptu.egp.eps.service.serviceinterface.AppAdvSearchService;
import com.cptu.egp.eps.web.servicebean.APPSrBean;
import com.cptu.egp.eps.web.servicebean.WorkFlowSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class APPWorkflowServlet extends HttpServlet {

    private final AppAdvSearchService appAdvSearchService = (AppAdvSearchService) AppContext.getSpringBean("AppAdvSearchService");
    private static final Logger LOGGER = Logger.getLogger(APPWorkflowServlet.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND  = "Ends";
    private String logUserId ="0";

     public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        LOGGER.debug("processRequest : "+logUserId+ LOGGERSTART);
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            HttpSession session = request.getSession();
            if(session.getAttribute("userId")!=null){
                logUserId = session.getAttribute("userId").toString();
                appAdvSearchService.setLogUserId(logUserId);
            }
            String pckNo = request.getParameter("pkgNo");
            int appid = Integer.parseInt(request.getParameter("appid"));
            String value = request.getParameter("estimatedCost");
            Integer EstimatedCost = 0;
            if (value != null && !value.equals("")) {
                EstimatedCost = Integer.parseInt(value);
            }
            String status = request.getParameter("status");
            String procurementNature = request.getParameter("procurementNature");
            String procurementType = request.getParameter("procurementType");

            WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
            short eventid = 1;
            int objectid = appid;
            short activityid = 1;
            int childid = appid;
            int initiator = 0;
            int approver = 0;
            boolean evntexist = false;
            boolean iswfLevelExist = false;
            String donor = "";
            donor = workFlowSrBean.isDonorReq(String.valueOf(eventid),
            Integer.valueOf(objectid));
            String[] norevs = donor.split("_");
            int norevrs = -1;
            String strrev = norevs[1];
            if(!strrev.equals("null")){
               norevrs = Integer.parseInt(norevs[1]);
            }
            evntexist = workFlowSrBean.isWorkFlowEventExist(eventid, objectid);
            List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(objectid, childid, activityid);
            
             if (tblWorkFlowLevelConfig.size() > 0) {
                 Iterator twflc = tblWorkFlowLevelConfig.iterator();
                 iswfLevelExist = true;
                 while (twflc.hasNext()) {
                     TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
                     TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                     if (workFlowlel.getWfRoleId() == 1) {
                         initiator = lmaster.getUserId();
                     }
                     if (workFlowlel.getWfRoleId() == 2) {
                         approver = lmaster.getUserId();
                     }
                 }
             }


            
            APPSrBean appSrBean = new APPSrBean();
            
            List<CommonAppSearchData> commonData = appAdvSearchService.getSearchResults(" ", " ", procurementNature, 0, 0, " ", " ", procurementType, appid, " ", pckNo, new BigDecimal(EstimatedCost), 0, " ", " ", new BigDecimal(0), new BigDecimal(0), 1, 1000, status,0);
            StringBuilder strProc = new StringBuilder();
            int srno = 1;
            if(commonData.isEmpty()){
                out.print("<td style=\"color: red\" colspan=\"6\" class=\"t-align-left\">No Data Found</td>");
            }
            String pkgValues[] = new String[commonData.size()];
            for (int i = 0; i < commonData.size(); i++) {
                strProc.delete(0, strProc.length());
                CommonAppSearchData commonAppSearchData = commonData.get(i);
                
                // commented as per client requirement and change below linet to next one
                //if((initiator == approver && norevrs == 0) || (commonAppSearchData.getWorkFlowStatus()!= null && commonAppSearchData.getWorkFlowStatus().equalsIgnoreCase("approved")))
                if((initiator == approver && norevrs == 0) || (commonAppSearchData.getWorkFlowStatus()!= null))
                {
                    String link = "-";
                    String linkUpload = "-";
                    //if(commonAppSearchData.getWorkFlowStatus().equalsIgnoreCase("approved")) {
                         link = "<a href='"+ request.getContextPath() + "/resources/common/ViewPackageDetail.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "&pkgType=Package' target=\"_blank\">View</a>&nbsp";
                        if("BeingRevised".equalsIgnoreCase(commonAppSearchData.getStatus()))
                            link=link+"  || <a href='"+ request.getContextPath() + "/resources/common/ViewPackageDetail.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "&pkgType=RevisePackage' target=\"_blank\">View Old package </a>&nbsp";

                    //}

                    if("Works".equalsIgnoreCase(commonAppSearchData.getProcurementNature())){
                        linkUpload = "<a onclick = window.open('" + request.getContextPath()+ "/officer/APPWorkflowDocs.jsp?pckid=" + commonAppSearchData.getPackageId() + "','window','scrollbars=1','resizable=yes','width=600,height=300')>Download</a>";
                    }
                    strProc.append(commonAppSearchData.getProcurementNature());
                    if (strProc.toString().equals("null")) {
                        strProc.delete(0, strProc.length());
                    }
              //aprojit-Start
                    String proType= "";
                    if("NCT".equalsIgnoreCase(commonAppSearchData.getProcurementType()))
                        proType="NCB";
                    else if("ICT".equalsIgnoreCase(commonAppSearchData.getProcurementType()))
                        proType="ICB";
               //aprojit-End     
                    out.print("<tr>");
                    out.print("<td class=\"t-align-center\">" + srno + "</td>");
                    out.print("<td class=\"t-align-left\">" + commonAppSearchData.getPackageNo() + "<br/>" + commonAppSearchData.getPackageDesc() + "</td>");
                    out.print("<td class=\"t-align-center\">" + strProc + "<br/>" + proType + "</td>");
                    out.print("<td class=\"t-align-center\">" + commonAppSearchData.getPkgEstCost().setScale(2, 0) + "</td>");
                    out.print("<td class=\"t-align-center \" style=\"display: none\">" + linkUpload + "</td>");
                    out.print("<td class=\"t-align-center noprint\">" + link + "</td>");
                    pkgValues[i] = commonAppSearchData.getPackageId().toString();
                    if(i ==(commonData.size()-1)){
                        StringBuilder pkgpdfString = new StringBuilder();
                        for(int x=0;x<pkgValues.length;x++){
                            if(x==(pkgValues.length-1)){
                                pkgpdfString.append(pkgValues[x]);
                            } else {
                                pkgpdfString.append(pkgValues[x] + ",");
                            }
                        }
                         out.print("<script type=\"text/javascript\">$(document).ready(function() {document.getElementById('hdnPkgPdf').value=\""+pkgpdfString.toString()+"\";});</script>");
                    }
                    out.print("</tr>");
                }
                srno++;
            }
                 } catch(Exception ex){
            LOGGER.error("processRequest "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("processRequest : "+logUserId+ LOGGEREND);
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
