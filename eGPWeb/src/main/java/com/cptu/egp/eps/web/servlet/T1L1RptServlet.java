/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblBidderRank;
import com.cptu.egp.eps.model.table.TblEvalRoundMaster;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.service.serviceimpl.EvaluationService;
import com.cptu.egp.eps.service.serviceimpl.ReportCreationService;
import com.cptu.egp.eps.service.serviceinterface.EvalServiceCriteriaService;
import com.cptu.egp.eps.web.databean.T1L1DtBean;
import com.cptu.egp.eps.web.servicebean.T1L1SrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class T1L1RptServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet T1L1RptServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet T1L1RptServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
            String action = request.getParameter("action");
            String tenderId = request.getParameter("tenderId");
            String rptId = request.getParameter("rptId");
            String roundId = request.getParameter("roundId");
            EvalServiceCriteriaService evalServiceCriteriaService = (EvalServiceCriteriaService)AppContext.getSpringBean("EvalServiceCriteriaService");
            ReportCreationService reportCreationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");
            EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
            boolean flag = false;
            String pageName = "";
            if("save".equalsIgnoreCase(action)){
                flag = t1l1Save(request,reportCreationService);
                if(flag){
                    pageName ="officer/Evalclarify.jsp?tenderId="+tenderId+"&st=rp&comType=TEC";
                }
            }

            response.sendRedirect(pageName);

        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public boolean t1l1Save(HttpServletRequest request, ReportCreationService reportCreationService){
        boolean flag = false;
        EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService"); //Change by dohatec for re-evaluation
        int tenderId = Integer.parseInt(request.getParameter("tenderId"));
        int rptId = Integer.parseInt(request.getParameter("rptId"));
        String roundId = request.getParameter("roundId");
        T1L1SrBean t1L1SrBean = new T1L1SrBean();
        List<T1L1DtBean> t1l1 = t1L1SrBean.getT1L1Data(tenderId+"", rptId+"", roundId,request.getSession().getAttribute("userId").toString());
        int evalCount =  evalService.getEvaluationNo(tenderId); //Change by dohatec for re-evaluation
        int winnerBidder = 0;
        boolean isLottery = false;
        int i = 1;
        int rank = 1;
        double t1l1Amount = 0;
        List<TblBidderRank> list1 = new ArrayList<TblBidderRank>();
        TblEvalRoundMaster roundMaster = null;
        //java.text.DecimalFormat df = new java.text.DecimalFormat("###.##");
        for(T1L1DtBean t1L1DtBean : t1l1){
                            if(i==1){
                                t1l1Amount = t1L1DtBean.getAmount();
                            }else{
                                if(t1l1Amount != t1L1DtBean.getAmount()){
                                    t1l1Amount = t1L1DtBean.getAmount();
                                    rank++;
                                }else{
                                    if(rank==1){
                                        isLottery = true;
                                    }

                                }
                            }

                TblBidderRank tblBidderRank = new TblBidderRank();
                tblBidderRank.setAmount(new BigDecimal(String.valueOf(t1L1DtBean.getAmount())).setScale(2,0));
                tblBidderRank.setT1Score(new BigDecimal(String.valueOf(t1L1DtBean.getT1Score())).setScale(2,0));
                tblBidderRank.setL1Score(new BigDecimal(String.valueOf(t1L1DtBean.getL1Score())).setScale(2,0));
                tblBidderRank.setCompanyName(t1L1DtBean.getCompanyName());
                tblBidderRank.setTblTenderMaster(new TblTenderMaster(tenderId));
                tblBidderRank.setCreatedBy(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
                tblBidderRank.setPkgLotId(0);
                tblBidderRank.setRank(rank);
                tblBidderRank.setReportId(0);
                tblBidderRank.setRowId(0);
                tblBidderRank.setTableId(0);
                tblBidderRank.setUserId(t1L1DtBean.getBidderId());
                tblBidderRank.setCreatedTime(new Date());
                list1.add(tblBidderRank);
                i++;
                //tblBidderRank.setAmount(new BigDecimal(t1L1DtBean.getAmount()));
        }
        if(isLottery){
                List<Integer> bidders = new ArrayList<Integer>();
                for (TblBidderRank bidderRank : list1) {
                    if(bidderRank.getRank()==1){
                        bidders.add(bidderRank.getUserId());
                    }
                }
                winnerBidder = getLotteryWinner(bidders);
            }else{
                winnerBidder = list1.get(0).getUserId();
            }
            roundMaster =  new TblEvalRoundMaster(0, tenderId, 0, "T1L1", 0,winnerBidder, new Date(), Integer.parseInt((request.getSession().getAttribute("userId").toString())),evalCount); //Change by dohatec for re-evaluation
            flag = reportCreationService.saveT1L1Rpt(roundMaster, list1);
        return flag;
    }

    private int getLotteryWinner(List<Integer> bidders) {
        Collections.shuffle(bidders);
        int index = new Random().nextInt(bidders.size());
        return bidders.get(index);
    }

}
