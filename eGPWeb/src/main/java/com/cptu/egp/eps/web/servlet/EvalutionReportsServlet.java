/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author dixit
 */
public class EvalutionReportsServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final Logger LOGGER = Logger.getLogger(EvalutionReportsServlet.class);
    CommonSearchService commonSearchService1 = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
    TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String logUserId = "0";
        PrintWriter out = response.getWriter();
        if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
                commonSearchService1.setLogUserId(logUserId);
                tenderCS.setLogUserId(logUserId);
        }
        String funName = request.getParameter("funName");       
        try {
            LOGGER.debug("processRequest : "+logUserId+" Starts");
            if("viewEvalutionReports".equalsIgnoreCase(funName))
            {
                LOGGER.debug("viewEvalutionReports : "+logUserId+" Starts");
                String styleClass = "";
                String strPageNo = request.getParameter("pageNo");

                int pageNo = Integer.parseInt(strPageNo);
                String strOffset = request.getParameter("size");
                String tenderId = request.getParameter("tenderId");
                String refNo = request.getParameter("refNo");
                int recordOffset = Integer.parseInt(strOffset);
                List<SPCommonSearchDataMore> list =null;
                list = dataMore.geteGPData("getTECReportsList", logUserId, strPageNo, strOffset, tenderId, refNo, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                if (list != null && !list.isEmpty()) {

                    for (int i = 0; i < list.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }
                            String action = "<a href=EvalReportListing.jsp?tenderid="+list.get(i).getFieldName1()+"&st=rp>"+ list.get(i).getFieldName9()+"</a>";
                            out.print("<tr class='" + styleClass + "'>");
                            out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * recordOffset + (i + 1)) + "</td>");
                            out.print("<td class=\"t-align-left\">" + list.get(i).getFieldName1() + ",<br/>" + list.get(i).getFieldName5() + "</td>");
                            out.print("<td class=\"t-align-left\">" + list.get(i).getFieldName2() + "</td>");
                            out.print("<td class=\"t-align-left\">" + list.get(i).getFieldName3() + "</td>");
                            out.print("<td class=\"t-align-left\">" + list.get(i).getFieldName4() + "</td>");
                            out.print("<td class=\"t-align-center\">" + action + "</td>");
                            out.print("</tr>");
                    }
                    int totalPages = 0;
                    if (!list.isEmpty()) {
                        totalPages = Integer.parseInt(list.get(0).getFieldName8());
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                } else {
                    out.print("<tr>");
                    out.print("<td colspan=\"6\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }
                LOGGER.debug("viewEvalutionReports : "+logUserId+" Ends");
            } else if("viewReviewReports".equalsIgnoreCase(funName)){
                LOGGER.debug("viewReviewReports : "+logUserId+" Starts");
                String styleClass = "";
                String strPageNo = request.getParameter("pageNo");

                int pageNo = Integer.parseInt(strPageNo);
                String strOffset = request.getParameter("size");
                String tenderId = request.getParameter("tenderId");
                String refNo = request.getParameter("refNo");
                int recordOffset = Integer.parseInt(strOffset);
                List<SPCommonSearchDataMore> list =null;
                list = dataMore.geteGPData("getReviewReportsList", logUserId, strPageNo, strOffset, tenderId, refNo, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                if (list != null && !list.isEmpty()) {

                    for (int i = 0; i < list.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }
                        String action = "<a href=ReviewReportProcessing.jsp?tenderid="+list.get(i).getFieldName1()+"&lotId="+list.get(i).getFieldName10()+"&evalRptFFid="+list.get(i).getFieldName9()+"&rId="+list.get(i).getFieldName11()+">Review Process</a>";
                        if(list.get(i).getFieldName12().equalsIgnoreCase("Forward")){
                            action = "<a href=ReviewReportProcessing.jsp?tenderid="+list.get(i).getFieldName1()+"&lotId="+list.get(i).getFieldName10()+"&evalRptFFid="+list.get(i).getFieldName9()+"&rId="+list.get(i).getFieldName11()+">Reviewed</a>";
                        }
                        out.print("<tr class='" + styleClass + "'>");
                        out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * recordOffset + (i + 1)) + "</td>");
                        out.print("<td class=\"t-align-left\">" + list.get(i).getFieldName1() + ",<br/>" + list.get(i).getFieldName5() + "</td>");
                        out.print("<td class=\"t-align-left\">" + list.get(i).getFieldName2() + "</td>");
                        out.print("<td class=\"t-align-left\">" + list.get(i).getFieldName3() + "</td>");
                        out.print("<td class=\"t-align-left\">" + list.get(i).getFieldName4() + "</td>");
                        out.print("<td class=\"t-align-center\">" + action + "</td>");
                        out.print("</tr>");
                    }
                    int totalPages = 0;
                    if (!list.isEmpty()) {
                        totalPages = Integer.parseInt(list.get(0).getFieldName8());
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                } else {
                    out.print("<tr>");
                    out.print("<td colspan=\"6\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }
                LOGGER.debug("viewReviewReports : "+logUserId+" Ends");
            }
        } finally { 
            out.close();
        }
        LOGGER.debug("processRequest : "+logUserId+" Starts");
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
