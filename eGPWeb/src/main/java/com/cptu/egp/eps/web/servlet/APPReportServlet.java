/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.service.serviceimpl.APPReportService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class APPReportServlet extends HttpServlet {

    private final APPReportService appReportService = (APPReportService) AppContext.getSpringBean("APPReportService");
    private static final Logger LOGGER = Logger.getLogger(APPReportServlet.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND  = "Ends";
    private String logUserId ="0";
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        LOGGER.debug("processRequest : "+logUserId+ LOGGERSTART);
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            HttpSession session = request.getSession();
            if(session.getAttribute("userId")!=null){
                logUserId = session.getAttribute("userId").toString();
                appReportService.setLogUserId(logUserId);
            }
            
            String param1 = "";
            String param2 = "";
            String funName = "";
            String str = "";

            if(request.getParameter("param1") != null && !"".equals(request.getParameter("param1"))){
                param1 = request.getParameter("param1");
            }
            if(request.getParameter("param2") != null && !"".equals(request.getParameter("param2"))){
                param2 = request.getParameter("param2");
            }
            if(request.getParameter("funName")!=null && !"".equals(request.getParameter("funName"))){
                funName = request.getParameter("funName");
            }

            if("getProject".equalsIgnoreCase(funName)) {
                    str = getPrj(param1,param2);
            }
            out.write(str);
        }catch(Exception ex){
             LOGGER.error("processRequest "+logUserId+" : "+ex.toString());
        }finally {
            out.close();
        }
        LOGGER.debug("processRequest  : "+logUserId+ LOGGEREND);
    } 

   

    public String getPrj(String param1, String param2){
        LOGGER.debug("processRequest : action : getPrj :"+logUserId+ LOGGERSTART);
        StringBuilder str = new StringBuilder();
        str.append("");
        try{
            List<Object[]> prjList = appReportService.getProjectByOfficeAndBudget(Integer.parseInt(param1),param2);
            if(!prjList.isEmpty()){
                str.append("<option value=''>- Select Project Name -</option>");
                for(int i = 0;i<prjList.size();i++)
                {
                    str.append("<option value='").append((Integer)prjList.get(i)[0]).append("'>").append((String)prjList.get(i)[1]).append("</option>");
                }
            }
            else
            {
                str.append("<option value='0'>No project found</option>");
            }
        }
        catch(Exception ex){
             LOGGER.error("processRequest action : getPrj :"+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("processRequest  action : getPrj :"+logUserId+ LOGGEREND);
        return str.toString();
    }


    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
