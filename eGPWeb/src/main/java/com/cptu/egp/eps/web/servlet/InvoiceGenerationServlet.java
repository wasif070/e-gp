/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;


import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblCmsInvoiceDetails;
import com.cptu.egp.eps.model.table.TblCmsInvoiceMaster;
import com.cptu.egp.eps.model.table.TblCmsWpDetail;
import com.cptu.egp.eps.model.table.TblCmsWpMaster;
import com.cptu.egp.eps.service.serviceimpl.ConsolodateService;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.AccPaymentService;
import com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.servicebean.CommonSearchServiceSrBean;
import com.cptu.egp.eps.web.servicebean.TenderTablesSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

/**
 *
 * @author shreyansh
 */
public class InvoiceGenerationServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    private ConsolodateService c_ConsSrv = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
    private CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
    private CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
    private AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String logUserId = "0";
        if (request.getSession().getAttribute("userId") != null) {
            logUserId = request.getSession().getAttribute("userId").toString();
        }
        try {
            ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
            String action = request.getParameter("action");
            HttpSession session = request.getSession();
            Object user = session.getAttribute("userId");
            Object userType = session.getAttribute("userTypeId");
            List<TblCmsInvoiceMaster> masters = new ArrayList<TblCmsInvoiceMaster>();
            List<TblCmsInvoiceDetails> detailses = new ArrayList<TblCmsInvoiceDetails>();
            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
            String procnature = commonService.getProcNature(request.getParameter("tenderId")).toString();
            String userId = null;
            String userTypeId = null;
            TenderTablesSrBean beanCommon = new TenderTablesSrBean();
            String tenderType = beanCommon.getTenderType(Integer.parseInt(request.getParameter("tenderId")));
            if (user == null) {
                userId = "";
            } else {

                userId = user.toString();
            }
            if (userType == null) {
                userTypeId = "";
            } else {

                userTypeId = userType.toString();
            }
            String str = "";
            if ("Services".equalsIgnoreCase(procnature)) {
                str = "Consultant";
            } else if ("goods".equalsIgnoreCase(procnature)) {
                str = "Supplier";
            } else {
                str = "Contractor";
            }
            if ("displayItem".equalsIgnoreCase(action)) {
                String PaymentType = request.getParameter("paymentType");
                int wpId = Integer.parseInt(request.getParameter("wpId"));
                int lotId = Integer.parseInt(request.getParameter("lotId"));
                int prId = Integer.parseInt(request.getParameter("prId"));
                int max = Integer.parseInt(request.getParameter("size"));
                int first = Integer.parseInt(request.getParameter("pageNo"));

                StringBuilder builder = new StringBuilder();
                List<Object[]> list = new ArrayList<Object[]>();

                if (procnature.equalsIgnoreCase("goods")) {
                    if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                    {
                    if ("anyitemanyp".equalsIgnoreCase(PaymentType)) {
                            list = service.returndata("getListForAnyItemAnyPercentICT",String.valueOf(prId), null);
                        } else if ("allitem100p".equalsIgnoreCase(PaymentType)) {
                            list = service.returndata("getListAllItemWithFullPercentICT",String.valueOf(lotId), String.valueOf(wpId));
                         } else if ("itemwise100p".equalsIgnoreCase(PaymentType)) {
                            list = service.returndata("getListForEachItemFullPercentICT",String.valueOf(prId), String.valueOf(wpId));
                        }
                    }
                    else
                    {
                        if ("anyitemanyp".equalsIgnoreCase(PaymentType)) {
                          list = service.getListForAnyItemAnyPercent(first, max, prId);
                        } else if ("allitem100p".equalsIgnoreCase(PaymentType)) {
                            list = service.getListAllItemWithFullPercent(first, max, lotId, wpId);
                        } else if ("itemwise100p".equalsIgnoreCase(PaymentType)) {
                            list = service.getListForEachItemFullPercent(first, max, prId, wpId);
                        }
                    }
                    try {
                        if (list != null && !list.isEmpty()) {
                            List<String> currencyList = new ArrayList<String>();
                            int i = 0;
                            boolean showNoRecFound = true;
                           for (Object[] oob : list) {
                                double qty = 0;
                                if ("anyitemanyp".equalsIgnoreCase(PaymentType)) {
                                    qty = Double.parseDouble(oob[5].toString()) - Double.parseDouble(oob[6].toString());

                                } else if ("allitem100p".equalsIgnoreCase(PaymentType)) {
                                    qty = Double.parseDouble(oob[3].toString());
                                }
                                if ("itemwise100p".equalsIgnoreCase(PaymentType)) {
                                    qty = Double.parseDouble(oob[3].toString());
                                    showNoRecFound = false;
                                } else {
                                    if (qty == 0) {
                                        continue;
                                    } else {
                                        showNoRecFound = false;
                                    }
                                }
                                BigDecimal fqty = new BigDecimal(qty).setScale(3, 0);
                                if (tenderType.equals("ICT")  || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")) {
                                    builder.append("<tr><td class=\"t-align-center\"><input type=checkbox onclick=CalculateICT(" + i + ") value=check id=inv_" + i + " name=inv_" + i + " disabled='disabled' /></td>");
                                } else {
                                    builder.append("<tr><td class=\"t-align-center\"><input type=checkbox onclick=Calculate(" + i + ") value=check id=inv_" + i + " name=inv_" + i + " /></td>");
                                }

                                 builder.append("<td class=\"t-align-center\">" + oob[0].toString() + "</td>");

                                builder.append("<td class=\"t-align-center\">" + oob[1].toString() + "</td>");
                                builder.append("<td class=\"t-align-center\">" + oob[2].toString() + "</td>");
                                builder.append("<td style=\"text-align :right;\">" + oob[3].toString() + "</td>");
                                BigDecimal actualqty = new BigDecimal(oob[3].toString());
                                BigDecimal actualrate = new BigDecimal(oob[4].toString());

                                //actualrate.divide(actualqty)
                                builder.append("<td style=\"text-align :right;\">" + actualrate.setScale(3, 0) + "</td>");

                                if (tenderType.equals("ICT")  || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")) {
                                    builder.append("<td style=\"text-align :right;\">" + oob[8].toString() + "</td>");

                                    BigDecimal supplierVat = new BigDecimal(oob[9].toString());
                                    builder.append("<td style=\"text-align :right;\">" + supplierVat.setScale(3, 0) + "</td>");
                                    if (!currencyList.contains(oob[8].toString())) {
                                        currencyList.add(oob[8].toString());
                                    }
                                    builder.append("<input type=hidden name=cur_" + i + " id=cur_" + i + " value=" + oob[8].toString() + " />");
                                    builder.append("<input type=hidden name=sup_" + i + " id=sup_" + i + " value=" + supplierVat.setScale(3, 0) + " />");

                                }
                                if ("itemwise100p".equalsIgnoreCase(PaymentType)) {
                                    builder.append("<td style=\"text-align :right;\">" + oob[3].toString() + " </td>");
                                } else {
                                    builder.append("<td style=\"text-align :right;\">" + fqty + " </td>");
                                }
                                builder.append("<input type=hidden name=qty_" + i + " id=qty_" + i + " value=" + fqty + " />");
                                builder.append("<input type=hidden name=rate_" + i + " id=rate_" + i + " value=" + actualrate.setScale(3, 0) + " />");
                                builder.append("<td class=\"t-align-center\"><label id=totalrateofqty_" + i + ">0.000</lable></td>");
                                builder.append("</tr>");
                                builder.append("<input type=\"hidden\" id=detailId_" + i + " name=detailId_" + i + " value=\"" + oob[7].toString() + "\">");
                                i++;
                            }

                            builder.append("<input type=hidden name=listcount id=listcount value=" + i + " />");
                            int totalPages = 1;

                            if (list.size() > 0) {
                                int cc = (int) i;
                                totalPages = (int) Math.ceil(Math.ceil(cc) / max);
                                if (totalPages == 0) {
                                    totalPages = 1;
                                }
                            }

                            if (showNoRecFound) {
                                out.print("<tr>");
                                out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"8\" style=\"color: red;font-weight: bold\"> No Records Found.</td>");
                                out.print("</tr>");
                            } else {
                                builder.append("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                                out.print(builder);
                                if (tenderType.equals("ICT")  || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")) {
                                    for (int currencyLoop = 0; currencyLoop < currencyList.size(); currencyLoop++) {
                                        if (currencyList.get(currencyLoop).equals("BTN") || currencyList.get(currencyLoop).equals("Taka")) {
                                            continue;
                                        } else {
                                            out.print("<tr><td colspan=8 class=\"t-align-center\"> </td>");
                                            out.print("<td class=\"t-align-center ff\">Total Amount (In " + currencyList.get(currencyLoop) + ")</td>");
                                            out.print("<td class=\"t-align-center ff\"><label name =totamt_" + currencyList.get(currencyLoop) + " id=totamt_" + currencyList.get(currencyLoop) + ">0</lable></td></tr>");
                                            out.print("<input type=hidden name=totalamount_" + currencyList.get(currencyLoop) + " id=totalamount_" + currencyList.get(currencyLoop) + " />");
                                        }
                                    }

                                    out.print("<tr><td colspan=8 class=\"t-align-center\"> </td>");
                                    out.print("<td class=\"t-align-center ff\">Total Amount (In Nu.)</td>");
                                    out.print("<td class=\"t-align-center ff\"><label name =totamt_BDT id=totamt_BDT>0</lable></td></tr>");
                                    out.print("<input type=hidden name=totalamount_BDT id=totalamount_BDT />");

                                } else {
                                    out.print("<tr><td colspan=6 class=\"t-align-center\"> </td>");
                                    out.print("<td class=\"t-align-center ff\">Total Amount (In Nu.)</td>");
                                    out.print("<td class=\"t-align-center ff\"><label id=totamt>0</lable></td></tr>");
                                    out.print("<input type=\"hidden\" id=\"totalamount\" name=\"totalamount\" />");
                                }
                            }
                            // out.print("<input type=\"hidden\" id=\"totalamount\" name=\"totalamount\" />");
                        } else {
                            out.print("<tr>");
                            out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"8\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                            out.print("</tr>");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if ("works".equalsIgnoreCase(procnature)) {
                    if ("anyitemanyp".equalsIgnoreCase(PaymentType)) {
                        list = service.getListForAnyItemAnyPercentForWorks(first, max, prId);
                    } else if ("allitem100p".equalsIgnoreCase(PaymentType)) {
                        list = service.getListAllItemWithFullPercentForWorks(first, max, lotId, wpId);
                    } else if ("itemwise100p".equalsIgnoreCase(PaymentType)) {
                        list = service.getListForEachItemFullPercentForWorks(first, max, prId, wpId);
                    }
                    try {
                        if (list != null && !list.isEmpty()) {

                            int i = 0;
                            boolean showNoRecFound = true;
                            for (Object[] oob : list) {
                                double qty;
                                if ("anyitemanyp".equalsIgnoreCase(PaymentType)) {
                                    qty = Double.parseDouble(oob[6].toString()) - Double.parseDouble(oob[7].toString());

                                } else {
                                    qty = Double.parseDouble(oob[4].toString());
                                }
                                if ("itemwise100p".equalsIgnoreCase(PaymentType)) {
                                    qty = Double.parseDouble(oob[4].toString());
                                    showNoRecFound = false;
                                } else {
                                    if (qty == 0) {
                                        continue;
                                    } else {
                                        showNoRecFound = false;
                                    }
                                }
                                builder.append("<tr><td class=\"t-align-center\"><input type=checkbox onclick=Calculate(" + i + ") value=check id=inv_" + i + " name=inv_" + i + " /></td>");
                                builder.append("<td class=\"t-align-center\">" + oob[0].toString() + "</td>");
                                builder.append("<td class=\"t-align-center\">" + oob[1].toString() + "</td>");

                                builder.append("<td class=\"t-align-center\">" + oob[2].toString() + "</td>");
                                builder.append("<td style=\"text-align :right;\">" + oob[3].toString() + "</td>");
                                builder.append("<td class=\"t-align-center\">" + oob[4].toString() + "</td>");
                                BigDecimal actualqty = new BigDecimal(oob[4].toString());
                                BigDecimal actualrate = new BigDecimal(oob[5].toString());
                                //Double rate = Double.parseDouble(actualrate.toString()) / Double.parseDouble(actualqty.toString());
                                Double rate = Double.parseDouble(actualrate.toString());
                                builder.append("<td style=\"text-align :right;\">" + new BigDecimal(rate).setScale(3, 0) + "</td>");
                                if ("itemwise100p".equalsIgnoreCase(PaymentType)) {
                                    builder.append("<td style=\"text-align :right;\">" + oob[4].toString() + " </td>");
                                } else {
                                    builder.append("<td style=\"text-align :right;\">" + new BigDecimal(qty).setScale(3, 0) + " </td>");
                                }
                                builder.append("<input type=hidden name=qty_" + i + " id=qty_" + i + " value=" + qty + " />");
                                builder.append("<input type=hidden name=rate_" + i + " id=rate_" + i + " value=" + rate + " />");
                                builder.append("<td class=\"t-align-center\"><label id=totalrateofqty_" + i + ">0.000</lable></td>");
                                builder.append("</tr>");
                                builder.append("<input type=\"hidden\" id=detailId_" + i + " name=detailId_" + i + " value=\"" + oob[8].toString() + "\">");
                                i++;
                            }

                            builder.append("<input type=hidden name=listcount id=listcount value=" + i + " />");
                            int totalPages = 1;

                            if (list.size() > 0) {
                                int cc = (int) i;
                                totalPages = (int) Math.ceil(Math.ceil(cc) / max);
                                if (totalPages == 0) {
                                    totalPages = 1;
                                }
                            }

                            if (showNoRecFound) {
                                out.print("<tr>");
                                out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"9\" style=\"color: red;font-weight: bold\"> No Records Found.</td>");
                                out.print("</tr>");
                            } else {
                                builder.append("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                                out.print(builder);
                                out.print("<tr><td colspan=7 class=\"t-align-center\"> </td>");
                                out.print("<td class=\"t-align-center ff\">Total Amount (In Nu.)</td>");
                                out.print("<td class=\"t-align-center ff\"><label id=totamt>0</lable></td></tr>");
                            }
                            out.print("<input type=\"hidden\" id=\"totalamount\" name=\"totalamount\" />");
                        } else {
                            out.print("<tr>");
                            out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"9\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                            out.print("</tr>");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                    list = service.getListForAnyItemAnyPercent(first, max, prId);
                    try {
                        if (list != null && !list.isEmpty()) {

                            int i = 0;
                            boolean showNoRecFound = true;
                            for (Object[] oob : list) {
                                double qty = 0;

                                qty = Double.parseDouble(oob[5].toString()) - Double.parseDouble(oob[6].toString());


                                if (qty == 0) {
                                    continue;
                                } else {
                                    showNoRecFound = false;
                                }

                                BigDecimal fqty = new BigDecimal(qty).setScale(3, 0);
                                builder.append("<tr><td class=\"t-align-center\"><input type=checkbox onclick=Calculate(" + i + ") value=check id=inv_" + i + " name=inv_" + i + " /></td>");
                                builder.append("<td class=\"t-align-center\">" + oob[0].toString() + "</td>");

                                builder.append("<td class=\"t-align-center\">" + oob[1].toString() + "</td>");
                                builder.append("<td class=\"t-align-center\">" + oob[2].toString() + "</td>");
                                builder.append("<td style=\"text-align :right;\">" + oob[3].toString() + "</td>");
                                BigDecimal actualqty = new BigDecimal(oob[3].toString());
                                BigDecimal actualrate = new BigDecimal(oob[4].toString());

                                //actualrate.divide(actualqty)
                                builder.append("<td style=\"text-align :right;\">" + actualrate.setScale(3, 0) + "</td>");
                                if ("itemwise100p".equalsIgnoreCase(PaymentType)) {
                                    builder.append("<td style=\"text-align :right;\">" + oob[3].toString() + " </td>");
                                } else {
                                    builder.append("<td style=\"text-align :right;\">" + fqty + " </td>");
                                }
                                builder.append("<input type=hidden name=qty_" + i + " id=qty_" + i + " value=" + fqty + " />");
                                builder.append("<input type=hidden name=rate_" + i + " id=rate_" + i + " value=" + actualrate.setScale(3, 0) + " />");
                                builder.append("<td class=\"t-align-center\"><label id=totalrateofqty_" + i + ">0.000</lable></td>");
                                builder.append("</tr>");
                                builder.append("<input type=\"hidden\" id=detailId_" + i + " name=detailId_" + i + " value=\"" + oob[7].toString() + "\">");
                                i++;
                            }

                            builder.append("<input type=hidden name=listcount id=listcount value=" + i + " />");
                            int totalPages = 1;

                            if (list.size() > 0) {
                                int cc = (int) i;
                                totalPages = (int) Math.ceil(Math.ceil(cc) / max);
                                if (totalPages == 0) {
                                    totalPages = 1;
                                }
                            }

                            if (showNoRecFound) {
                                out.print("<tr>");
                                out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"8\" style=\"color: red;font-weight: bold\"> No Records Found.</td>");
                                out.print("</tr>");
                            } else {
                                builder.append("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                                out.print(builder);
                                out.print("<tr><td colspan=6 class=\"t-align-center\"> </td>");
                                out.print("<td class=\"t-align-center ff\">Total Amount (In Nu.)</td>");
                                out.print("<td class=\"t-align-center ff\"><label id=totamt>0</lable></td></tr>");
                            }
                            out.print("<input type=\"hidden\" id=\"totalamount\" name=\"totalamount\" />");
                        } else {
                            out.print("<tr>");
                            out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"8\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                            out.print("</tr>");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }



            }  else if ("submit".equalsIgnoreCase(action)) {
                int tenderId = Integer.parseInt(request.getParameter("tenderId"));
                int wpId = Integer.parseInt(request.getParameter("wpId"));
                int lotId = Integer.parseInt(request.getParameter("lotId"));
                int prId = Integer.parseInt(request.getParameter("prId"));
                long lng = service.getInvoiceCountByLotId(lotId);
                String invAmt = request.getParameter("invAmt");
                BigDecimal totalamount = new BigDecimal(request.getParameter("totalamount"));
                int count = Integer.parseInt(request.getParameter("listcount"));
                TblCmsInvoiceMaster tcim = new TblCmsInvoiceMaster();
                for (int i = 0; i < count; i++) {
                    String check = request.getParameter("inv_" + i);
                    if (check != null) {
                        tcim.setCreatedBy(Integer.parseInt(userId));
                        tcim.setCreatedDate(new java.util.Date());
                        tcim.setInvStatus("createdbyten");
                        tcim.setTotalInvAmt((totalamount).setScale(3, 0));
                        tcim.setTenderId(tenderId);
                        tcim.setTblCmsWpMaster(new TblCmsWpMaster(wpId));
                        tcim.setTillPrid(prId);
                        tcim.setIsAdvInv(invAmt);
                        tcim.setInvoiceNo("Invoice" + String.valueOf(lng + 1));

                    }
                }
                boolean flag = service.addToInvoiceMaster(tcim);
                int invoiceId = tcim.getInvoiceId();
                for (int j = 0; j < count; j++) {
                    String check = request.getParameter("inv_" + j);
                    if (check != null) {
                        int detailid = Integer.parseInt(request.getParameter("detailId_" + j));
                        List<TblCmsWpDetail> tcwds = service.getDetailFromWpDetail(detailid);
                        TblCmsWpDetail cmsWpDetail = new TblCmsWpDetail();
                        TblCmsInvoiceDetails details = new TblCmsInvoiceDetails();
                        details.setTblCmsInvoiceMaster(new TblCmsInvoiceMaster(tcim.getInvoiceId()));
                        details.setWpDetailId(detailid);
                        BigDecimal qty = new BigDecimal(request.getParameter("qty_" + j));
                        //BigDecimal bd = new BigDecimal((Double.parseDouble(tcwds.get(0).getWpRate().toString()) / Double.parseDouble(tcwds.get(0).getWpQty().toString())) * Double.parseDouble(qty.toString())).setScale(3, 0);
                        BigDecimal bd = tcwds.get(0).getWpRate().multiply(new BigDecimal(qty.toString())).setScale(3, 0);
                        details.setItemInvAmt(bd);
                        details.setItemInvQty(qty);
                        detailses.add(details);
                        boolean updat = service.updateWpDetailForInvoice(detailid, Double.parseDouble(qty.toString()));
                    }
                }
                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                int conId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Payment.getName(), "Generate Invoice (Tenderer side)", "");
                boolean flag1 = service.addToInvoiceDetail(detailses);
                sendMailForInvoiceGeneratedbySupplier(request.getParameter("tenderId"), request.getParameter("lotId"), user.toString(), "has Generated",str,invoiceId);
                //sendMailForInvoiceGeneratedbyPE(request.getParameter("tenderId"),request.getParameter("lotId"),user.toString(),"has requested to","e-GP: Request for Generate Invoice");
                if (flag && flag1) {
                    //response.sendRedirect("tenderer/Invoice.jsp?tenderId=" + tenderId + "&msg=crinv");
                        response.sendRedirect("tenderer/ViewInvoice.jsp?tenderId=" + tenderId + "&lotId=" + lotId + "&wpId=" + wpId + "&invoiceId=" + invoiceId + "&msg=crinv");
                    }
            } else if ("submitICT".equalsIgnoreCase(action)) {

                TblCmsInvoiceMaster tcim = new TblCmsInvoiceMaster();
                boolean flag = false;
                int invoiceId = 0;
                int tenderId = Integer.parseInt(request.getParameter("tenderId"));
                int wpId = Integer.parseInt(request.getParameter("wpId"));
                int lotId = Integer.parseInt(request.getParameter("lotId"));
                int prId = Integer.parseInt(request.getParameter("prId"));
                long lng = service.getInvoiceCountByLotId(lotId);
                List<Object> lngICT = service.getInvoiceCountByLotIdForICT(lotId);
                String invAmt = request.getParameter("invAmt");
                int checkedCount = Integer.parseInt(request.getParameter("checkedCount"));
                String curArr[] = new String[checkedCount];
                String totalAmount[] = new String[checkedCount];
                String invoiceNo = "";
                boolean BDTFlag = false;
                int count = Integer.parseInt(request.getParameter("listcount"));
               for(int k=0;k<count;k++)
               {
               if(request.getParameter("cur_"+k).toString().equals("BTN"))
                   BDTFlag = true;
               }

                for (int i = 0; i < checkedCount; i++) {
                    List<Object[]> listItembyCurrency = null;
                    curArr[i] = request.getParameter("curcheck_" + i);
                    tcim.setInvoiceNo("Invoice" + String.valueOf(lngICT.size() + 1));
                    totalAmount[i] = request.getParameter("totalamount_" + curArr[i]);
                    if(tenderType.equals("ICT")  || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                    {
                        if(checkedCount != count && !curArr[i].equals("BTN"))
                            listItembyCurrency = service.getWPDetailIDByCurrency(wpId,curArr[i],prId);
                        else if (checkedCount == count || BDTFlag == true)
                            listItembyCurrency = service.getWPDetailIDByCurrency(wpId,curArr[i],prId);                    
                    }
                    else
                    {
                        totalAmount[i] = request.getParameter("totalamount");
                        listItembyCurrency = service.getWPDetailID(wpId,prId);
                        tcim.setInvoiceNo("Invoice" + String.valueOf(lng + 1));
                    }

                    BigDecimal totalamount = new BigDecimal(totalAmount[i]);
                    tcim.setCreatedBy(Integer.parseInt(userId));
                    tcim.setCreatedDate(new java.util.Date());
                    tcim.setInvStatus("createdbyten");
                    tcim.setTotalInvAmt((totalamount).setScale(3, 0));
                    tcim.setTenderId(tenderId);
                    tcim.setTblCmsWpMaster(new TblCmsWpMaster(wpId));
                    tcim.setTillPrid(prId);
                    tcim.setIsAdvInv(invAmt);

                    flag = service.addToInvoiceMaster(tcim);
                    invoiceId = tcim.getInvoiceId();

                    if(listItembyCurrency!=null ){
                    for (Object[] item : listItembyCurrency) {

                        int detailid = Integer.parseInt(item[0].toString());
                        List<TblCmsWpDetail> tcwds = service.getDetailFromWpDetail(detailid);
                        TblCmsWpDetail cmsWpDetail = new TblCmsWpDetail();
                        TblCmsInvoiceDetails details = new TblCmsInvoiceDetails();
                        details.setTblCmsInvoiceMaster(new TblCmsInvoiceMaster(tcim.getInvoiceId()));
                        details.setWpDetailId(detailid);
                       // BigDecimal qty = new BigDecimal(request.getParameter("qty_" + i));
                        BigDecimal qty = new BigDecimal(item[1].toString());
                        //BigDecimal bd = new BigDecimal((Double.parseDouble(tcwds.get(0).getWpRate().toString()) / Double.parseDouble(tcwds.get(0).getWpQty().toString())) * Double.parseDouble(qty.toString())).setScale(3, 0);
                        BigDecimal bd = tcwds.get(0).getWpRate().multiply(new BigDecimal(qty.toString())).setScale(3, 0);
                        details.setItemInvAmt(bd);
                        details.setItemInvQty(qty);
                        detailses.add(details);
                        boolean updat = service.updateWpDetailForInvoice(detailid, Double.parseDouble(qty.toString()));
                    }
               //     }
           //     }
            }
                }
                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                int conId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Payment.getName(), "Generate Invoice (Tenderer side)", "");
                boolean flag1 = service.addToInvoiceDetail(detailses);
                sendMailForInvoiceGeneratedbySupplier(request.getParameter("tenderId"), request.getParameter("lotId"), user.toString(), "has Generated", str, invoiceId);
                //sendMailForInvoiceGeneratedbyPE(request.getParameter("tenderId"),request.getParameter("lotId"),user.toString(),"has requested to","e-GP: Request for Generate Invoice");
                if (flag && flag1) {
                    if(tenderType.equals("ICT")  || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                    {
                            List<Object[]> invoicId = null;
                           invoicId = service.getInvoiceIdListForICT(String.valueOf(wpId));
                           invoiceNo = invoicId.get(0)[0].toString();
                           response.sendRedirect("tenderer/ViewInvoice.jsp?tenderId=" + tenderId + "&lotId=" + lotId + "&wpId=" + wpId + "&invoiceNo=" + invoiceNo + "&msg=crinv");

                    }
                    else
                           //response.sendRedirect("tenderer/Invoice.jsp?tenderId=" + tenderId + "&msg=crinv");
                           response.sendRedirect("tenderer/ViewInvoice.jsp?tenderId=" + tenderId + "&lotId=" + lotId + "&wpId=" + wpId + "&invoiceId=" + invoiceId + "&msg=crinv");
                }
            }

            else if ("invAmtSubmit".equalsIgnoreCase(action)) {
                int tenderId = Integer.parseInt(request.getParameter("tenderId"));
                int wpId = Integer.parseInt(request.getParameter("wpId"));
                int lotId = Integer.parseInt(request.getParameter("lotId"));
                List<Object> lngICT = service.getInvoiceCountByLotIdForICT(lotId);
                long lng = service.getInvoiceCountByLotId(lotId);
                int prId = Integer.parseInt(request.getParameter("prId"));
                String invAmt = request.getParameter("invAmt");
                String invoiceNo = "";
                String totalAdvCount = "";
                if (request.getParameter("totalAdvCount") != null) {
                        totalAdvCount = request.getParameter("totalAdvCount");
                    }
                boolean flag = false;
                int invoiceId = 0;
                String curArr[] = new String[Integer.parseInt(totalAdvCount)];
                for(int i =0;i<Integer.parseInt(totalAdvCount);i++)
                {
                List<Object> listItembyCurrency = null;
                curArr[i] = request.getParameter("curcheck_" + i);
                BigDecimal totalamount = new BigDecimal(request.getParameter("totalamount_"+i));
                TblCmsInvoiceMaster tcim = new TblCmsInvoiceMaster();

                tcim.setCreatedBy(Integer.parseInt(userId));
                tcim.setCreatedDate(new java.util.Date());
                tcim.setInvStatus("createdbyten");
                tcim.setTotalInvAmt((totalamount).setScale(3, 0));
                tcim.setTenderId(tenderId);
                tcim.setTblCmsWpMaster(new TblCmsWpMaster(wpId));
                tcim.setTillPrid(prId);
                tcim.setIsAdvInv(invAmt);
               
                if(tenderType.equals("ICT")  || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes") )
                     tcim.setInvoiceNo("Invoice" + String.valueOf(lngICT.size() + 1));
                else
                      tcim.setInvoiceNo("Invoice" + String.valueOf(lng + 1));
                flag = service.addToInvoiceMaster(tcim);

                if(flag == false)
                    break;
                invoiceId = tcim.getInvoiceId();

                if(tenderType.equals("ICT")  || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                    {
                       listItembyCurrency = service.getWPDetailIDByCurrencyForAdv(wpId,curArr[i]);

                        int detailid = Integer.parseInt(listItembyCurrency.get(0).toString());
                        List<TblCmsWpDetail> tcwds = service.getDetailFromWpDetail(detailid);
                        TblCmsWpDetail cmsWpDetail = new TblCmsWpDetail();
                        TblCmsInvoiceDetails details = new TblCmsInvoiceDetails();
                        details.setTblCmsInvoiceMaster(new TblCmsInvoiceMaster(tcim.getInvoiceId()));
                        details.setWpDetailId(detailid);
                        BigDecimal qty = BigDecimal.ZERO;
                        BigDecimal bd = BigDecimal.ZERO;
                        details.setItemInvAmt(bd);
                        details.setItemInvQty(qty);
                        detailses.add(details);
                    }
                }


                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                int conId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Payment.getName(), "Generate Invoice (Tenderer side)", "");

                if (flag) {
                    if(tenderType.equals("ICT")  || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                    {
                           service.addToInvoiceDetail(detailses);

                           List<Object[]> invoicId = null;
                           invoicId = service.getInvoiceMasterDatawithInvAmt(String.valueOf(wpId));
                           invoiceNo = invoicId.get(0)[2].toString();
                           response.sendRedirect("tenderer/ViewInvoice.jsp?tenderId=" + tenderId + "&lotId=" + lotId + "&wpId=" + wpId + "&invoiceNo=" + invoiceNo + "&msg=crinv");
                    }
                    else
                    response.sendRedirect("tenderer/ViewInvoice.jsp?tenderId=" + tenderId + "&lotId=" + lotId + "&wpId=" + wpId + "&invoiceId=" + invoiceId + "&msg=crinv");
                }
            } else if ("generate".equalsIgnoreCase(action)) {
//                int invId = Integer.parseInt(request.getParameter("invId"));
//                boolean upd = service.updateStatusInInvMaster(invId);
//                sendMailForInvoiceGeneratedbySupplier(request.getParameter("tenderId"),request.getParameter("lotId"),user.toString(),"has generated","e-GP: Supplier has Generated Invoice");
//                if (upd) {
//                    response.sendRedirect("tenderer/Invoice.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=gnratinv");
//                }
            } else if ("PSRrequestAction".equalsIgnoreCase(action)) {
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    tenderId = request.getParameter("tenderId");
                }
                String lotId = "";
                if (request.getParameter("lotId") != null) {
                    lotId = request.getParameter("lotId");
                }
                String status = "";
                if (request.getParameter("status") != null) {
                    status = request.getParameter("status");
                }
                String strps = "";
                if ("NEWPS".equalsIgnoreCase(status)) {
                    strps = "New Performance Security";
                } else {
                    strps = "Performance Security";
                }
                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                int conId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Payment.getName(), "Request For Release of " + strps + "", "");
                sendMailForReleasePerfomanceSecurityToPEBySupplier(tenderId, lotId, logUserId, str, strps);
                if ("services".equalsIgnoreCase(procnature)) {
                    response.sendRedirect("" + request.getContextPath() + "/tenderer/InvoiceServiceCase.jsp?tenderId=" + tenderId + "&msgg=" + status + "");
                } else {
                    response.sendRedirect("" + request.getContextPath() + "/tenderer/Invoice.jsp?tenderId=" + tenderId + "&msgg=" + status + "");
                }
            }

        } finally {
            out.flush();
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(InvoiceGenerationServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(InvoiceGenerationServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /** it sends mail notification to PE on generating invoice by supplier  */
    private boolean sendMailForInvoiceGeneratedbySupplier(String tenderId, String lotId, String logUserId, String Operation, String str, int invoiceId) {
        boolean flag = false;
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        Object[] invmstobj = null;
        String InvoiceAmt = "";
        String MileStoneName = "";
        String InvoiceNo = "";
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peEmailId = "";
            List<Object[]> peEmailObj = accPaymentService.getPEemailID(Integer.parseInt(tenderId));
            if (!peEmailObj.isEmpty()) {
                Object[] objj = peEmailObj.get(0);
                peEmailId = objj[0].toString();
            }
            String strTo[] = {peEmailId};
            String strFrom = commonservice.getEmailId(logUserId);
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            List<Object[]> getInvAmtMstObj = c_ConsSrv.getInvAmtAndMilestoneName(invoiceId);
            List<Object[]> getInvAmt = c_ConsSrv.getInvoiceTotalAmt(Integer.toString(invoiceId));
           /* if (!getInvAmt.isEmpty()) {
              InvoiceAmt =  getInvAmt.get(0)[0].toString();
            }*/
            List<Object> getInvNo = c_ConsSrv.getInvoiceNo(Integer.toString(invoiceId));
            if (getInvNo.get(0) != null) {
                InvoiceNo = getInvNo.get(0).toString();
            }
            String pnature = commonservice.getProcNature(tenderId).toString();
            String serviceType = commonservice.getServiceTypeForTender(Integer.parseInt(tenderId));
            if ("services".equalsIgnoreCase(pnature)) {
                if (!"Time based".equalsIgnoreCase(serviceType.toString())) {
                    if (!getInvAmtMstObj.isEmpty()) {
                        invmstobj = getInvAmtMstObj.get(0);
                        InvoiceAmt = invmstobj[0].toString();
                        MileStoneName = invmstobj[1].toString();
                        InvoiceNo = invmstobj[2].toString();
                    }
                }
            }
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String mailSubject = "e-GP: Invoice Generated by " + str;
            registerService.contentAdmMsgBox(strTo[0], strFrom, mailSubject, msgBoxContentUtility.invoiceGeneratedbySupplie(Operation, c_isPhysicalPrComplete, obj, InvoiceAmt, MileStoneName, InvoiceNo));
            String mailText = mailContentUtility.invoiceGeneratedbySupplie(Operation, c_isPhysicalPrComplete, obj, InvoiceAmt, MileStoneName, InvoiceNo);
            sendMessageUtil.setEmailTo(strTo);
            sendMessageUtil.setEmailFrom(strFrom);
            sendMessageUtil.setEmailSub(mailSubject);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
            StringBuilder sb = new StringBuilder();
            sb.append("Dear User,%0AThis is to inform you that " + str + " has Generated Invoice ");
            sb.append("for Contract No." + obj[1].toString() + " (" + obj[15].toString() + ")" + " ");
            sb.append("%0AThanks,%0AeGP System");
            sendMessageUtil.setSmsBody(sb.toString());
            sendMessageUtil.sendSMS();
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }

    /** it sends mail notification to PE on requesting performance security / bank gurantee by supplier  */
    private boolean sendMailForReleasePerfomanceSecurityToPEBySupplier(String tenderId, String lotId, String logUserId, String str, String strps) {
        boolean flag = false;

        //Code Comment Start by Proshanto Kumar Saha,Dohatec
        //List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        //Code Comment End by Proshanto Kumar Saha,Dohatec

        //Code Start by Proshanto Kumar Saha,Dohatec
        //This portion is used to collect GetCinfoBar with or without contract sign by checking contract sign is not done,performance security is given and tender validity is over.
        String mailText=null;
        List<Object[]> GetCinfoBar=null;
        boolean checkStatus = false;
        CommonSearchServiceSrBean objCommonSearchServiceSrBean=new CommonSearchServiceSrBean();
        checkStatus=objCommonSearchServiceSrBean.getCSignTValidityAndPSecurityStatus(tenderId, logUserId);
        if(checkStatus)
        {
        GetCinfoBar = cmsConfigDateService.getContractInfoBarWithoutCSign(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        }
        else
        {
        GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        }
        //Code End by Proshanto Kumar Saha,Dohatec
       
        Object[] obj = null;
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peEmailId = "";
            List<Object[]> peEmailObj = accPaymentService.getPEemailID(Integer.parseInt(tenderId));
            if (!peEmailObj.isEmpty()) {
                Object[] objj = peEmailObj.get(0);
                peEmailId = objj[0].toString();
            }
            String strTo[] = {peEmailId};
            String mailSubject = "e-GP : Request for Release of " + strps + "";
            String strFrom = commonservice.getEmailId(logUserId);
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();

            //Code Comment Start by Proshanto Kumar Saha,Dohatec
            //registerService.contentAdmMsgBox(strTo[0], strFrom, mailSubject, msgBoxContentUtility.ReleasePerfomanceSecurityToPE(c_isPhysicalPrComplete, obj, strps));
            //mailText = mailContentUtility.ReleasePerfomanceSecurityToPE(c_isPhysicalPrComplete, obj, strps);
            //Code Comment End by Proshanto Kumar Saha,Dohatec

            //Code Start by Proshanto Kumar Saha,Dohatec
            //This portion is used to add mailmessage and set mailtext with or without contract sign by checking contract sign is not done,performance security is given and tender validity is over.
            if(checkStatus)
            {
             registerService.contentAdmMsgBox(strTo[0], strFrom, mailSubject, msgBoxContentUtility.ReleasePerfomanceSecurityToPEWithoutConSign(c_isPhysicalPrComplete, obj, strps,tenderId));
             mailText = mailContentUtility.ReleasePerfomanceSecurityToPEWithoutConSign(c_isPhysicalPrComplete, obj, strps,tenderId);
            }
            else
            {
             registerService.contentAdmMsgBox(strTo[0], strFrom, mailSubject, msgBoxContentUtility.ReleasePerfomanceSecurityToPE(c_isPhysicalPrComplete, obj, strps));
             mailText = mailContentUtility.ReleasePerfomanceSecurityToPE(c_isPhysicalPrComplete, obj, strps);
            }
            //Code End by Proshanto Kumar Saha,Dohatec
            sendMessageUtil.setEmailTo(strTo);
            sendMessageUtil.setEmailFrom(strFrom);
            sendMessageUtil.setEmailSub(mailSubject);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
            StringBuilder sb = new StringBuilder();
            sb.append("Dear User,%0AThis is to inform you that " + str + " has requested for releasing " + strps + " ");

            //Code Comment Start by Proshanto Kumar Saha,Dohatec
            //sb.append("for Contract No." + obj[1].toSting() + " (" + obj[15].toString() + ")" + " ");
            //Code Comment End by Proshanto Kumar Saha,Dohatecco

            //Code Start by Proshanto Kumar Saha,Dohatec
            //This portion is used to sent message in mobile with tenderId or with contract no by checking contract sign is not done,performance security is given and tender validity is over.
            if(checkStatus)
            {
            sb.append("for Tender ID :" + tenderId + " ");
            }
            else
            {
            sb.append("for Contract No." + obj[1].toString() + " (" + obj[15].toString() + ")" + " ");
            }
            //Code End by Proshanto Kumar Saha,Dohatec

            sb.append("%0AThanks,%0AeGP System");
            sendMessageUtil.setSmsBody(sb.toString());
            sendMessageUtil.sendSMS();
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }
}
