/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblProcurementRole;
import com.cptu.egp.eps.model.table.TblTenderPayment;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.service.serviceimpl.ConsolodateService;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceimpl.WorkFlowServiceImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author Chalapathi.Bavisetti
 */
public class CommonSearchServiceSrBean extends HttpServlet {

    private final CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");

    private static final Logger LOGGER = Logger.getLogger(CommonSearchServiceSrBean.class);
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";
    private String logUserId = "0";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        try {
            logUserId = request.getSession().getAttribute("userId").toString();
            int uid = 0;
            String str = "";
            String funName  = request.getParameter("funName");
            LOGGER.debug(funName+" : "+logUserId+LOGGERSTART);
            if ("AAFillComboAPP".equalsIgnoreCase(funName)) {

            String objectId = request.getParameter("objectId");
            String prorole  = request.getParameter("Prorole");
            String childId  = request.getParameter("childId");
            String activityId = request.getParameter("activityId");
            String wfroleId = request.getParameter("wfroleId");
            String revId = request.getParameter("revId");
                str = getUserNames(funName, objectId, prorole, childId, activityId, wfroleId, revId, uid);
             out.print(str);
            } else if ("GetDebaredData".equalsIgnoreCase(funName)) {
            String cmpName = request.getParameter("cmpName");
            String dstName = request.getParameter("dstName");
            String debfrm = request.getParameter("debfrm");
            String debto = request.getParameter("debto");
            String pageNo = request.getParameter("pageNo");
            String size = request.getParameter("size");
                getDebarmentList(funName, cmpName, dstName, debfrm, debto, pageNo, size, response);

            } else if ("proRoles".equalsIgnoreCase(funName)) {
               String revId = request.getParameter("revId");
               String selected = request.getParameter("selected");
                str = getProcureRoles(Integer.parseInt(revId), Integer.parseInt(selected));
               out.print(str);
            }
        LOGGER.debug(funName+" : "+logUserId+LOGGEREND);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public String getUserNames(String funName, String objectId, String Prorole, String wfroleId, String activityId, String childId, String revId, int uid) {
        LOGGER.debug("getUserNames :" +logUserId+LOGGERSTART);
        StringBuilder str = new StringBuilder();
        String procName = "";
        TenderCommonService tenderCommonService = null;
       // tenderCommonService.setLogUserId(logUserId);
        List<SPTenderCommonData> sptcd = new ArrayList<SPTenderCommonData>();
        SPCommonSearchData commonSearchData;
         WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
         byte proId = Byte.parseByte(Prorole);
          procName =  workFlowSrBean.getProcurementById(proId);
        if (Integer.parseInt(Prorole) == 21) {
               tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            sptcd = tenderCommonService.returndata("getTenderAA", objectId, null);
           }
        List<SPCommonSearchData> commonSearchDatas = commonSearchService.searchData(funName, objectId, procName, wfroleId, activityId, childId, null, null, null, null);
        if (Integer.parseInt(Prorole) == 1 || Integer.parseInt(Prorole) == 6
                || Integer.parseInt(Prorole) == 19 //|| Integer.parseInt(Prorole) == 7
                || Integer.parseInt(Prorole) == 21) {
            if (!commonSearchDatas.isEmpty()) {
                commonSearchData = commonSearchDatas.get(0);
                str .append( "<label id='uservalue" + revId + "'>" + commonSearchData.getFieldName2() + "</label><input type='hidden' name='txtuserid' value='" + commonSearchData.getFieldName1() + "' id='txtuserid" + revId + "' /> ");
            } else if (!sptcd.isEmpty() && Integer.parseInt(Prorole) == 21) {
                  SPTenderCommonData tenderCommonData =   sptcd.get(0);
                str.append("<label id='uservalue" + revId + "'>" + tenderCommonData.getFieldName3() + "</label><input type='hidden' name='txtuserid' value='" + tenderCommonData.getFieldName4() + "' id='txtuserid" + revId + "' /> ");
            } else {
                str.append("<label id='uservalue" + revId + "'>User not available</label><input type='hidden' name='txtuserid' value='0' id='txtuserid" + revId + "' /> ");
                }

        } else {
                Iterator it =  commonSearchDatas.iterator();
            str.append(" <select name='cmbUserNames" + revId + "' class='formTxtBox_1' id='cmbUserNames" + revId + "' style='width:200px;' onchange='changeuser(this.id)' >");
                boolean check = false;
                int value = 0;

            while (it.hasNext()) {
                  commonSearchData =  (SPCommonSearchData) it.next();
                if (!check) {
                    value =  Integer.parseInt(commonSearchData.getFieldName1());
                    check = true;
                  }
                if (uid == Integer.parseInt(commonSearchData.getFieldName1()) && uid != 0) {
                    str.append( "<option selected='true' value='" + commonSearchData.getFieldName1() + "'>" + commonSearchData.getFieldName2() + "</option>");
                } else {
                    str.append( "<option value='" + commonSearchData.getFieldName1() + "'>" + commonSearchData.getFieldName2() + "</option>");
                }
            }

            if (commonSearchDatas.isEmpty()) {
                str.append( "<option selected='true' value='Usernotavailable'>User not available</option>");
                str.append( "</select><input type='hidden' name='txtuserid' value='0' id='txtuserid" + revId + "' />");
            } else {
                str .append("</select><input type='hidden' name='txtuserid' value='" + value + "' id='txtuserid" + revId + "' />");
                 }


            }
        LOGGER.debug("getUserNames :" +logUserId+LOGGEREND);
        return str.toString();
    }

    public void getDebarmentList(String funName, String companyName, String District, String DebarPeriodFrm, String DebarPeriodTo, String pageNo, String Size, HttpServletResponse response) throws IOException {
        LOGGER.debug("getDebarmentList :" +logUserId+LOGGERSTART);
        SPCommonSearchData commonSearchData;
        PrintWriter out = response.getWriter();
        String stdebardate = "";
        String endebardate = "";
        if (DebarPeriodFrm != null && !"".equals(DebarPeriodFrm)) {
        String[] stdate = DebarPeriodFrm.split("/");
            stdebardate = stdate[2] + "-" + stdate[1] + "-" + stdate[0];
       }
        if (DebarPeriodTo != null && !"".equals(DebarPeriodTo)) {
        String[] enddate = DebarPeriodTo.split("/");
            endebardate = enddate[2] + "-" + enddate[1] + "-" + enddate[0];
       }
        List<SPCommonSearchData> commonSearchDatas = commonSearchService.searchData(funName, companyName, District, stdebardate, endebardate, pageNo, Size, null, null, null);
        commonSearchService.setLogUserId(logUserId);
       Iterator it =  commonSearchDatas.iterator();
        if (commonSearchDatas.isEmpty()) {
            out.print("<tr>");
            out.print("<td colspan=\"6\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
            out.print("</tr>");
        } else {
            while (it.hasNext()) {
                commonSearchData = (SPCommonSearchData) it.next();
            out.print("<tr>");
                out.print("<td class=\"t-align-left\">" + commonSearchData.getFieldName1() + "</td>");
                out.print("<td class=\"t-align-left\">" + commonSearchData.getFieldName2() + "</td>");
                out.print("<td class=\"t-align-left\">" + commonSearchData.getFieldName3() + "</td>");
                out.print("<td class=\"t-align-left\">" + commonSearchData.getFieldName4() + "</td>");
                out.print("<td class=\"t-align-left\">" + commonSearchData.getFieldName5() + "</td>");
                out.print("<td class=\"t-align-left\">" + commonSearchData.getFieldName6() + "</td>");
            out.print("</tr>");
       }
   }
         LOGGER.debug("getDebarmentList :" +logUserId+LOGGEREND);
    }

    public String getProcureRoles(int rwId, int selectedval) {
        LOGGER.debug("getProcureRoles :" +logUserId+LOGGERSTART);
       boolean check = false;
       int value = 0;
       String procureRoleR="";
         WorkFlowServiceImpl workFlowServiceImpl =
                (WorkFlowServiceImpl) AppContext.getSpringBean("WorkFlowService");
        workFlowServiceImpl.setUserId(logUserId);
        List<TblProcurementRole>  tblProcurementRoles =  workFlowServiceImpl.getProcurementRoles();
       Iterator it =  tblProcurementRoles.iterator();
        StringBuilder str = new StringBuilder();
        str.append( "<select name='reviewer" + rwId + "' onchange='changeProRole(this.id)' class='formTxtBox_1' id='cmdreviewer" + rwId + "' style='width:100px;'>");
        while (it.hasNext()) {
          TblProcurementRole tblProcurementRole = (TblProcurementRole) it.next();
            if (!check && selectedval == 0) {
            value =  tblProcurementRole.getProcurementRoleId();
            check = true;
            } else if (selectedval != 0) {
             value = selectedval;
            check = true;
          }
           if(tblProcurementRole.getProcurementRole().equalsIgnoreCase("HOPE")){procureRoleR="HOPA";}else if(tblProcurementRole.getProcurementRole().equalsIgnoreCase("PE")){procureRoleR="PE";}else{procureRoleR=tblProcurementRole.getProcurementRole();}
            if (tblProcurementRole.getProcurementRoleId() != 20 && tblProcurementRole.getProcurementRoleId() != 22 && selectedval != 0 && selectedval == tblProcurementRole.getProcurementRoleId()) {
                str.append("<option selected='true' value='" + tblProcurementRole.getProcurementRoleId() + "'>" + procureRoleR + "</option>");
            } else if (tblProcurementRole.getProcurementRoleId() != 20 && tblProcurementRole.getProcurementRoleId() != 22) {
                str.append("<option value='" + tblProcurementRole.getProcurementRoleId() + "'>" + procureRoleR + "</option>");
       }
   }
        str.append("</select><input type='hidden' name='revlev" + rwId + "' value='" + value + "' id='revlev" + rwId + "' />");
        LOGGER.debug("getProcureRoles :" +logUserId + LOGGEREND);
        return str.toString();
    }
    //code Start by Proshanto Kumar Saha,Dohatec
    //This method is used to check whether contract sign is done or not,performance security is given and tender validity is over.
    public boolean getCSignTValidityAndPSecurityStatus(String tenderId,String userId) {
        logUserId = userId;
        LOGGER.debug("getCSignTValidityAndPSecurityStatus : " + logUserId + " Starts");
        boolean flag = false;
        try {
             List<Object []> chkObj = null;
             boolean isNoaAndCSDone = false;
             boolean isPerformanceSecurityRelease=false;
             String contractSignDate=null;
             IssueNOASrBean objIssueNOASrBean=new IssueNOASrBean();
             chkObj = objIssueNOASrBean.getNOAListing(Integer.parseInt(tenderId));
             if(!chkObj.isEmpty()){
                 if(chkObj.size() > 0){
                     for (Object[] obj : chkObj) {
                            ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                            isNoaAndCSDone = service.checkContractSigning(tenderId, Integer.parseInt(obj[0].toString()));
             List<TblTenderPayment> detailsPayment = objIssueNOASrBean.getPaymentDetails(Integer.parseInt(tenderId), (Integer) obj[0], (Integer) obj[8]);
    
             if (detailsPayment.get(0).getIsVerified().equalsIgnoreCase("yes"))
                 {
                  isPerformanceSecurityRelease= true;
                 }
             else {
                    isPerformanceSecurityRelease= false;
                  }
  
                   }
                      }
                       }
              TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
             // List<SPTenderCommonData> validityList = tenderCommonService.returndata("ValidityExtReq", tenderId, null);
               List<SPTenderCommonData> contractSigningDateList = tenderCommonService.returndata("ContractSignDateReq", tenderId, null);
              if (!contractSigningDateList.isEmpty()) {
              contractSignDate = contractSigningDateList.get(0).getFieldName1().toString();
                 }

              SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
              Date validityDate = formatter.parse(contractSignDate);
              Date currentDt = new Date();
              Calendar c = Calendar.getInstance();
              c.setTime(new Date()); // Now use today date.
              c.add(Calendar.DATE, 0); // Adding 0 days
              currentDt =  c.getTime();
              if(!isNoaAndCSDone && isPerformanceSecurityRelease && currentDt.after(validityDate))
               {
                flag = true;
               }
              } catch (Exception e) {
               LOGGER.error("getCSignTValidityAndPSecurityStatus : " + logUserId + " : " + e);
             }
             LOGGER.debug("getCSignTValidityAndPSecurityStatus : " + logUserId + " Ends");
             return flag;
          }
     //code End by Proshanto Kumar Saha,Dohatec
           }
