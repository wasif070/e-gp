/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.security;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * Filters all requests for application resources and uses an
 * AuthorizationManager to authorize access.
 *
 * @author Darshan
 */
public class SecurityFilters implements Filter {

    /**
     * Filter should be configured with an system error page.
     */
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    /**
     * Obtain user from current session and invoke a singleton
     * AuthorizationManager to determine if user is authorized for the requested
     * resource. If not, forward them to a standard error page.
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        HttpServletRequest req = (HttpServletRequest) request;
        System.out.println("From Filter :- \nRequest From " + req.getRequestURL() + " \nProtocol " + req.getProtocol());
        HttpServletResponse res = (HttpServletResponse) response;

        HttpSession httpSession = req.getSession();
        boolean goToSessTimeOut = false;
        if (  ( req.getRequestURL().indexOf("ContentAdminViewDocs") < 0 ) &&
                ( req.getRequestURL().indexOf("SessionTimedOut.jsp") > 0
                || req.getRequestURL().indexOf("Index.jsp") > 0
                || req.getRequestURL().indexOf("PA_Registration.mp4") > 0
                || req.getRequestURL().indexOf("userManuals.jsp") > 0
                || req.getRequestURL().indexOf("prr.jsp") > 0
                || req.getRequestURL().indexOf("eSBDs.jsp") > 0
                || req.getRequestURL().indexOf("DocumentBriefcaseSrBean") > 0
                || req.getRequestURL().indexOf("videoTutorial.jsp") > 0
                || req.getRequestURL().indexOf("TestTenderAllForms.jsp") > 0
                || req.getRequestURL().indexOf("TenderTDSView.jsp") > 0
                || req.getRequestURL().indexOf("eLearning.jsp") > 0
                || req.getRequestURL().indexOf("quizModule.jsp") > 0
                || req.getRequestURL().indexOf("moduleWiseQuestion.jsp") > 0
                || req.getRequestURL().indexOf("SelectBOQ.jsp") > 0
                || req.getRequestURL().indexOf("aboutUs.jsp") > 0
                || req.getRequestURL().indexOf("AboutEGP.jsp") > 0
                || req.getRequestURL().indexOf("LoginSrBean") > 0
                || req.getRequestURL().indexOf("contactUs.jsp") > 0
                || req.getRequestURL().indexOf("Contact.jsp") > 0
                || req.getRequestURL().indexOf("SubscribeToRSS.jsp") > 0
                || req.getRequestURL().indexOf("StdTenderSearch.jsp") > 0
                || req.getRequestURL().indexOf("TenderListing.jsp") > 0
                || req.getRequestURL().indexOf("SearchAPP.jsp") > 0
                || req.getRequestURL().indexOf("AppListing.jsp") > 0
                || req.getRequestURL().indexOf("StdSearch.jsp") > 0
                || req.getRequestURL().indexOf("AppDetails.jsp") > 0
                || req.getRequestURL().indexOf("ViewMarquee.jsp") > 0
                || req.getRequestURL().indexOf("SearchNOA.jsp") > 0
                || req.getRequestURL().indexOf("ContractListing.jsp") > 0
                || req.getRequestURL().indexOf("RegistrationDetails.jsp") > 0
                //Edit by Palash
                || req.getRequestURL().indexOf("ReportDownload.jsp") > 0
                // End
                //Edit by Proshanto
                || req.getRequestURL().indexOf("DebarmentRules.jsp") > 0
                // End
                //Edit by Proshanto
                || req.getRequestURL().indexOf("DebarmentCommitteeMember.jsp") > 0
                // End
                //Edit by Proshanto
                || req.getRequestURL().indexOf("Chairmanship.jsp") > 0
                // End
                //Edit by Proshanto
                || req.getRequestURL().indexOf("DebarmentDecisions.jsp") > 0
                // End
                || req.getRequestURL().indexOf("PEOfficeDetailsPage.jsp") > 0
                || req.getRequestURL().indexOf("BranchDetail.jsp") > 0
                || req.getRequestURL().indexOf("NewUserRegistrationLoginDetails.jsp") > 0
                || req.getRequestURL().indexOf("BidderRegistration.jsp") > 0
                || req.getRequestURL().indexOf("NewUserRegistrationLoginDetailsNew.jsp") > 0
                || req.getRequestURL().indexOf("ComapnyDetailNew.jsp") > 0
                || req.getRequestURL().indexOf("EmailVerification.jsp") > 0
                || req.getRequestURL().indexOf("VerifyCode.jsp") > 0
                || req.getRequestURL().indexOf("MemScheduleBank.jsp") > 0
                || req.getRequestURL().indexOf("MandRegDoc.jsp") > 0
                || req.getRequestURL().indexOf("TermsNConditions.jsp") > 0
                || req.getRequestURL().indexOf("Terms.jsp") > 0
                || req.getRequestURL().indexOf("PrivacyPolicy.jsp") > 0
                || req.getRequestURL().indexOf("ViewPackageDetail.jsp") > 0
                || req.getRequestURL().indexOf("ViewAppDetails.jsp") > 0
                || req.getRequestURL().indexOf("HelpDesk.jsp") > 0
                || req.getRequestURL().indexOf("PDFHeader.html") > 0
                || req.getRequestURL().indexOf("ViewTender.jsp") > 0
                || req.getRequestURL().indexOf("ViewNotice.jsp") > 0
                //Edit by Nitish
                || req.getRequestURL().indexOf("FAQ.jsp") > 0
                || req.getRequestURL().indexOf("eGpGuidelines.jsp") > 0
                //End Nitish
                || req.getRequestURL().indexOf("RegistrationSteps.jsp") > 0
                || req.getRequestURL().indexOf("procForum.jsp") > 0
                || req.getRequestURL().indexOf("AppReviseConfiguration.jsp") > 0
    //            || req.getRequestURL().indexOf("DebarmentList.jsp") > 0
                || req.getRequestURL().indexOf("UserStatus.jsp") > 0
                || req.getRequestURL().indexOf("AllTenders.jsp") > 0
                || req.getRequestURL().indexOf("AdvTenderListing.jsp") > 0
                || req.getRequestURL().indexOf("ownCategory.jsp") > 0
                || req.getRequestURL().indexOf("AdvAPPSearch.jsp") > 0
                || req.getRequestURL().indexOf("AdvAppListing.jsp") > 0
                || req.getRequestURL().indexOf("DeptTree.jsp") > 0
                || req.getRequestURL().indexOf("CPVTree.jsp") > 0
                || req.getRequestURL().indexOf("jquery.txt") > 0
                || req.getRequestURL().indexOf("ViewAwardedContracts.jsp") > 0
                || req.getRequestURL().indexOf("ViewContracts.jsp") > 0
                || req.getRequestURL().indexOf("AnnualProcurementPlans.jsp") > 0
                || req.getRequestURL().indexOf("Tenders.jsp") > 0
                || req.getRequestURL().indexOf("ViewTenderDetails.jsp") > 0
                || req.getRequestURL().indexOf("Logout.jsp") > 0
                || req.getRequestURL().indexOf("test.jsp") > 0
                || req.getRequestURL().indexOf("test1.jsp") > 0
                || req.getRequestURL().indexOf("test2.jsp") > 0
                || req.getRequestURL().indexOf("viewTopic.jsp") > 0
                || req.getRequestURL().indexOf("VerifyCode.jsp") > 0
                || req.getRequestURL().indexOf("ChngPassword.jsp") > 0
                || req.getRequestURL().indexOf("ResetPassword.jsp") > 0
                || req.getRequestURL().indexOf("ForgotPassword.jsp") > 0
                || req.getRequestURL().indexOf("PasswordRecovery.jsp") > 0
                || req.getRequestURL().indexOf("ViewNewsDetails.jsp") > 0
                || req.getRequestURL().indexOf("ViewNews.jsp") > 0
                || req.getRequestURL().indexOf("merchant_client.jsp") > 0
                || req.getRequestURL().indexOf("PromiseReportServlet") > 0
                //                || req.getRequestURL().indexOf("ePaymentSuccessful.jsp") > 0
                //                || req.getRequestURL().indexOf("ePaymentFailure.jsp") > 0
                //                || req.getRequestURL().indexOf("RegVasFeePayment.jsp") > 0
                //                || req.getRequestURL().indexOf("OnlinePmtForVAS.jsp") > 0
                //                || req.getRequestURL().indexOf("OnlinePaymentVASSuc.jsp") > 0
                || req.getRequestURL().indexOf("SupportingBrowser.jsp") > 0
                || req.getRequestURL().indexOf("OtherLinks.jsp") > 0
                || req.getRequestURL().indexOf("ChangeForgotPassward.jsp") > 0
                || req.getRequestURL().indexOf("DebarmentRpt.jsp") > 0
                || req.getRequestURL().indexOf("DebarmentListing.jsp") > 0
                || req.getRequestURL().indexOf("PostTopic.jsp") > 0
                || req.getRequestURL().indexOf("PostReply.jsp") > 0
                || req.getRequestURL().indexOf("viewTopic.jsp") > 0
                //                || req.getRequestURL().indexOf("OnlinePaymentServlet") > 0
                || req.getRequestURL().indexOf("ResetPasswordSrBean") > 0
                || req.getRequestURL().indexOf("ForgotPasswordSrBean") > 0
                || req.getRequestURL().indexOf("MarqueeServlet") > 0
                || req.getRequestURL().indexOf("ScBankDevPartnerJqGridSrBean") > 0
                || req.getRequestURL().indexOf("SearchNoaServlet") > 0
                || req.getRequestURL().indexOf("GetCpvTree") > 0
                || req.getRequestURL().indexOf("getDataForTree") > 0
                || req.getRequestURL().indexOf("SearchServlet") > 0
                || req.getRequestURL().indexOf("CommonServlet") > 0
                || req.getRequestURL().indexOf("TenderDetailsServlet") > 0
                //Edit by Palash
                || req.getRequestURL().indexOf("ReportDownloadServlet") > 0
                //End
                || req.getRequestURL().indexOf("ComboServlet") > 0
                || req.getRequestURL().indexOf("Signer.jar") > 0
                || req.getRequestURL().indexOf("SearchAPPServlet") > 0
                || req.getRequestURL().indexOf("getAllPostServlet") > 0
                || req.getRequestURL().indexOf("PublicProcForumServlet") > 0
                || req.getRequestURL().indexOf("AskProcurementServlet") > 0
                || req.getRequestURL().indexOf("AdvSearchNOAServlet") > 0
                || req.getRequestURL().indexOf("GeneratePdf") > 0
                || req.getRequestURL().indexOf("InitDebarment") > 0
                || req.getRequestURL().indexOf("PreTendQuerySrBean") > 0
                || req.getRequestURL().indexOf(".pdf") > 0
                || req.getRequestURL().indexOf(".gif") > 0
                || req.getRequestURL().indexOf(".css") > 0
                || req.getRequestURL().indexOf(".png") > 0
                || req.getRequestURL().indexOf(".jpg") > 0
                //|| req.getRequestURL().indexOf(".jpeg") > 0
                || req.getRequestURL().indexOf("AdvancedSearchforAPPService") > 0
                || req.getRequestURL().indexOf("AdvanceSearchTenderService") > 0
                || req.getRequestURL().indexOf("AwardedContractService") > 0
                || req.getRequestURL().indexOf("ReportService") > 0
                || req.getRequestURL().indexOf("NewsAndEventService") > 0
                || req.getRequestURL().indexOf("DebarredTendererService") > 0
                || req.getRequestURL().indexOf("PEOfficeIdShareService") > 0
                || req.getRequestURL().indexOf("AmendmentService") > 0
                //|| req.getRequestURL().indexOf("TenderSecUploadServlet") > 0
                || req.getRequestURL().indexOf("PromisIndicatorShareService") > 0
                //|| req.getRequestURL().indexOf("PromisIntegrationService") >0
                || req.getRequestURL().indexOf("RHDPEOfficeIdShareService") > 0
                || req.getRequestURL().indexOf("RHDCMSShareService") > 0
                || req.getRequestURL().indexOf("LotPckDocs.jsp") > 0
                || req.getRequestURL().indexOf("PostTopic.jsp") > 0
                || req.getRequestURL().indexOf("PostReply.jsp") > 0
                || req.getRequestURL().indexOf("viewTopic.jsp") > 0
                || req.getRequestURL().indexOf("QuestionListing.jsp") > 0
                || req.getRequestURL().indexOf("ViewQuestion.jsp") > 0
                || req.getRequestURL().indexOf("AskProcurement.jsp") > 0
                || req.getRequestURL().indexOf("PaymentRedirect.jsp") > 0
                || req.getRequestURL().indexOf("BracPaymentSuccess.jsp") > 0
                || req.getRequestURL().indexOf("Help.jsp") > 0
                || req.getRequestURL().indexOf("AdvSearchNOA.jsp") > 0
                || req.getRequestURL().indexOf("AdvContractListing.jsp") > 0
                || req.getRequestURL().indexOf("PreTendQuerySrBean") > 0
                || req.getRequestURL().indexOf("Reregistration.jsp") > 0
                || req.getRequestURL().indexOf("NewSupportingDocuments.jsp") > 0
                || req.getRequestURL().toString().endsWith(".js")
                || req.getRequestURL().toString().endsWith(".xml")
                || req.getRequestURL().toString().endsWith("/")
                || req.getRequestURL().toString().endsWith("cptuEgpLogo.gif")
                || req.getRequestURL().toString().endsWith("favicon2.ico")
                || req.getRequestURL().toString().endsWith("solaimanlipi.ttf")
                || req.getRequestURL().indexOf("MakeAdvertisement") > 0
                || req.getRequestURL().toString().endsWith("MakeAdvertisements.jsp")
                || (req.getQueryString() != null && req.getQueryString().toString().trim().endsWith("isPDF=true&xulfz=d4f8r5s1"))
                //for offline data dohaec start
                || req.getRequestURL().indexOf("viewContractAwardOfflineServlet") > 0
                || req.getRequestURL().indexOf("viewAwardedContractOffline.jsp") > 0
                || req.getRequestURL().indexOf("AwardedContractOfflineSrBean") > 0
                || req.getRequestURL().indexOf("SearchAwardedContractOffline.jsp") > 0
                || req.getRequestURL().indexOf("TenderDashboardOfflineServlet") > 0 // Offline Tender Servlet
                //|| req.getRequestURL().indexOf("SearchTenderOffline.jsp") > 0   // Offline Tender
                || req.getRequestURL().indexOf("BidOpeningInfo.jsp") > 0 // Bid Opening Information
                || req.getRequestURL().indexOf("OpeningReports.jsp") > 0 // Bid Opening Information
                || req.getRequestURL().indexOf("IndReport.jsp") > 0 // Bid Opening Information
                || req.getRequestURL().indexOf("TOR1.jsp") > 0 // Bid Opening Information
                || req.getRequestURL().indexOf("TOR2.jsp") > 0 // Bid Opening Information
                || req.getRequestURL().indexOf("ViewREOI.jsp") > 0 // View Offline REOI
                || req.getRequestURL().indexOf("ViewTenderWithPQ.jsp") > 0 // View Offline TenderWithPQ
                || req.getRequestURL().indexOf("ViewTenderWithoutPQ.jsp") > 0 // View Offline TenderWithoutPQ
                || req.getRequestURL().indexOf("APPServlet") > 0
                || req.getRequestURL().indexOf("SystemRequirement.jsp") > 0 //dohatec
                || req.getRequestURL().indexOf("NewFeature.jsp") > 0 //dohatec
                || req.getRequestURL().indexOf("ServiceLevel.jsp") > 0 //dohatec
                || req.getRequestURL().indexOf("BidderDashboard.jsp") > 0 //for offline data dohaec end
                || req.getRequestURI().indexOf("Grievance.jsp") > 0 //Grievance submenu from homapage
                ) ){
            // without session pages
            System.out.println("Successfully bypass");
        } else if (httpSession.getAttribute("userId") == null
                && req.getRequestURL().indexOf("OnlinePaymentServlet") < 0
                && req.getRequestURL().indexOf("ePaymentSuccessful.jsp") < 0
                && req.getRequestURL().indexOf("ePaymentFailure.jsp") < 0
                && req.getRequestURL().indexOf("RegVasFeePayment.jsp") < 0
                && req.getRequestURL().indexOf("OnlinePmtForVAS.jsp") < 0
                && req.getRequestURL().indexOf("PaymentRedirect.jsp") < 0
                && req.getRequestURL().indexOf("OnlinePaymentVASSuc.jsp") < 0) {
            goToSessTimeOut = true;
            System.out.println("userid null Request From " + req.getRequestURL() + " Query String " + req.getQueryString());
        }else if( (req.getRequestURL().indexOf("ContentAdminViewDocs") > 0)
                && (httpSession.getAttribute("userTypeId") != null)
                && (! httpSession.getAttribute("userTypeId").toString().equals("8") ) ){
            goToSessTimeOut = true;
            System.out.println("userid null Request From " + req.getRequestURL() + " Query String " + req.getQueryString());
        } 

        if (goToSessTimeOut) {
            res.sendRedirect("/SessionTimedOut.jsp");
        } else {
            // System.out.println("req "+req.getHeader("referer"));
            String extention = req.getRequestURL().toString();
            System.out.println(extention);
            if (extention.lastIndexOf(".") > 0) {
                extention = extention.substring(extention.lastIndexOf("."), extention.length());
                /*if(extention.indexOf("?") > 0){
                    extention = extention.substring(0, extention.indexOf("?"));
                }*/
            }
            if (isValidExtension(extention)) {
                if ( //req.getRequestURL().indexOf("CompanyDetails.jsp") > 0
                        //|| req.getRequestURL().indexOf("InsertCompanyDetails.jsp") > 0
                        //|| req.getRequestURL().indexOf("PersonalDetails.jsp") > 0
                        //|| req.getRequestURL().indexOf("SupportingDocuments.jsp") > 0
                        //|| req.getRequestURL().indexOf("IndividualConsultant.jsp") > 0
                        //|| req.getRequestURL().indexOf("staging.eprocure.gov.bd") > 0
                        //|| req.getRequestURL().indexOf("training.eprocure.gov.bd") > 0
                        //||
                        req.getRequestURL().indexOf("development.eprocure.gov.bd") > 0
                        // || req.getRequestURL().indexOf("staging.eprocure.gov.bd/eGP/") > 0
                        // || req.getRequestURL().indexOf("staging.eprocure.gov.bd") > 0
                        || req.getRequestURL().indexOf(":8090") > 0
                        || req.getRequestURL().indexOf("egp.gov.bt") > 0
                        || req.getRequestURL().indexOf("103.7.255.154") > 0
                        || req.getRequestURL().indexOf(":80") > 0
                        || req.getRequestURL().indexOf(":8080") > 0
                        || req.getRequestURL().indexOf(":24909") > 0
                        || req.getRequestURL().indexOf(":8084") > 0
                        || (req.getRequestURL().indexOf("Logout.jsp") > 0
                        && (req.getRequestURL().indexOf("192.168.100.152") == -1 && req.getRequestURL().indexOf("122.170.119.66") == -1 && req.getRequestURL().indexOf("inhouse.eprocure.gov.bd") == -1 && req.getRequestURL().indexOf("localhost") == -1))
                        || (req.getRequestURL().indexOf("FinalSubmission.jsp") > 0 && (req.getRequestURL().indexOf("8090") > 0 || req.getRequestURL().indexOf("80") > 0))) {
                    System.out.println("Non-ssl");
                    chain.doFilter(req, res);
                } else {
                    chain.doFilter(req, new SendRedirectOverloadedResponse(req, res));
                }
            } else {
                chain.doFilter(req, res);
            }
        }
    }

    public void destroy() {
    }

    public boolean isValidExtension(String ext) {
        boolean bool = false;
        if (ext.equals(".js") || ext.equals(".png") || ext.equals(".gif") || ext.equals(".css")) {
            //System.out.println(ext + "FALSE");
            bool = false;
        } else {
            //System.out.println(ext + "TRUE");
            bool = true;
        }
        return bool;
    }
}

/*
public class SecurityFilters implements Filter {
    /Filter should be configured with an system error page.
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    //Obtain user from current session and invoke a singleton AuthorizationManager to determine if
    // user is authorized for the requested resource.  If not, forward them to a standard error page.
     //
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        HttpServletRequest req = (HttpServletRequest) request;
       System.out.println("doFilter() " + req.getRequestURL().toString());
        HttpServletResponse res = (HttpServletResponse) response;

        HttpSession httpSession = req.getSession();
        boolean goToSessTimeOut = false;
        if(req.getRequestURL().indexOf("SessionTimedOut.jsp") > 0
                || req.getRequestURL().indexOf("Index.jsp") > 0
                || req.getRequestURL().indexOf("aboutUs.jsp") > 0
                || req.getRequestURL().indexOf("LoginSrBean") > 0
                || req.getRequestURL().indexOf("contactUs.jsp") > 0
                || req.getRequestURL().indexOf("SubscribeToRSS.jsp") > 0
                || req.getRequestURL().indexOf("StdTenderSearch.jsp") > 0
                || req.getRequestURL().indexOf("SearchAPP.jsp") > 0
                || req.getRequestURL().indexOf("StdSearch.jsp") > 0
                || req.getRequestURL().indexOf("ViewMarquee.jsp") > 0
                || req.getRequestURL().indexOf("SearchNOA.jsp") > 0
                || req.getRequestURL().indexOf("RegistrationDetails.jsp") > 0
                || req.getRequestURL().indexOf("PEOfficeDetailsPage.jsp") > 0
                || req.getRequestURL().indexOf("BranchDetail.jsp") > 0
                || req.getRequestURL().indexOf("NewUserRegistrationLoginDetails.jsp") > 0
                || req.getRequestURL().indexOf("EmailVerification.jsp") > 0
                || req.getRequestURL().indexOf("VerifyCode.jsp") > 0
                || req.getRequestURL().indexOf("MemScheduleBank.jsp") > 0
                || req.getRequestURL().indexOf("MandRegDoc.jsp") > 0
                || req.getRequestURL().indexOf("TermsNConditions.jsp") > 0
                || req.getRequestURL().indexOf("PrivacyPolicy.jsp") > 0
                || req.getRequestURL().indexOf("ViewPackageDetail.jsp") > 0
                || req.getRequestURL().indexOf("HelpDesk.jsp") > 0
                || req.getRequestURL().indexOf("ViewTender.jsp") > 0
                || req.getRequestURL().indexOf("FAQ.jsp") > 0
                || req.getRequestURL().indexOf("RegistrationSteps.jsp")> 0
                || req.getRequestURL().indexOf("MandRegDoc.jsp")> 0
                || req.getRequestURL().indexOf("procForum.jsp")> 0
                || req.getRequestURL().indexOf("UserStatus.jsp")> 0
                || req.getRequestURL().indexOf("AllTenders.jsp")> 0
                || req.getRequestURL().indexOf("AdvAPPSearch.jsp") > 0
                || req.getRequestURL().indexOf("DeptTree.jsp") > 0
                || req.getRequestURL().indexOf("CPVTree.jsp") > 0
                || req.getRequestURL().indexOf("jquery.txt") > 0
                || req.getRequestURL().indexOf("ViewAwardedContracts.jsp") > 0
                || req.getRequestURL().indexOf("AnnualProcurementPlans.jsp") > 0
                || req.getRequestURL().indexOf("Tenders.jsp") > 0
                || req.getRequestURL().indexOf("ViewTenderDetails.jsp") > 0
                || req.getRequestURL().indexOf("Logout.jsp") > 0
                || req.getRequestURL().indexOf("test.jsp") > 0
                || req.getRequestURL().indexOf("test1.jsp") > 0
                || req.getRequestURL().indexOf("test2.jsp") > 0
                || req.getRequestURL().indexOf("viewTopic.jsp") > 0
                || req.getRequestURL().indexOf(".pdf") > 0
                || req.getRequestURL().indexOf(".gif") > 0
                || req.getRequestURL().indexOf(".css") > 0
                || req.getRequestURL().indexOf(".png") > 0
                || req.getRequestURL().indexOf(".jpg") > 0
                || req.getRequestURL().indexOf("GeneratePdf") > 0
                || req.getRequestURL().indexOf("AdvancedSearchforAnnuaProcurementPlanService") > 0
                || req.getRequestURL().indexOf("AdvanceSearchTendersService") >0
                || req.getRequestURL().indexOf("AwardedContractsService") > 0
                || req.getRequestURL().indexOf("ReportsService") >0
                || req.getRequestURL().indexOf("NewsAndEventsService") >0
                || req.getRequestURL().indexOf("DebarmentService") >0
                || req.getRequestURL().indexOf("PromisIntegrationService") >0
                || req.getRequestURL().indexOf("AmendmentsService") >0
                || req.getRequestURL().indexOf("PromisIndicatorService") > 0
                || req.getRequestURL().indexOf("RHDCMSIntegrationService") > 0
                || req.getRequestURL().indexOf("RHDPEIntegrationService") > 0
                //|| req.getRequestURL().indexOf("DocumentBriefcaseSrBean") > 0
                || req.getRequestURL().indexOf("eprocure.gov.bd") > 0
                || req.getRequestURL().indexOf(":8080/") > 0
                || req.getRequestURL().indexOf(":8090/") > 0
                || req.getRequestURL().indexOf("eGPTest/") > 0
                || req.getRequestURL().indexOf("eGP/") > 0
                || req.getRequestURL().indexOf("localhost/") > 0
                || req.getRequestURL().toString().endsWith("192.168.100.152/")
                || req.getRequestURL().toString().endsWith("122.170.119.66/")
                || req.getRequestURL().toString().endsWith(".js")
                || req.getRequestURL().toString().endsWith("cptuEgpLogo.gif")
                || req.getRequestURL().toString().endsWith("favicon.ico")
                ){
                // without session pages
                System.out.println("Successfully bypass");
        }else{
            if(httpSession.getAttribute("userId")==null && (req.getRequestURL().indexOf("MarqueeServlet") ==-1 && req.getRequestURL().indexOf("ScBankDevPartnerJqGridSrBean") ==-1 && req.getRequestURL().indexOf("SearchNoaServlet") ==-1 && req.getRequestURL().indexOf("GetCpvTree") ==-1 && req.getRequestURL().indexOf("getDataForTree") ==-1 && req.getRequestURL().indexOf("SearchServlet") ==-1 && req.getRequestURL().indexOf("CommonServlet") ==-1 && req.getRequestURL().indexOf("TenderDetailsServlet") ==-1 && req.getRequestURL().indexOf("Signer.jar")== -1 && req.getRequestURL().indexOf("SearchAPPServlet")== -1 &&  req.getRequestURL().indexOf("getAllPostServlet") == -1))
            {
                System.out.println("userid null");
                goToSessTimeOut = true;
            }
            else if(httpSession.getAttribute("userId")!=null && !isAccess(Integer.parseInt(httpSession.getAttribute("userTypeId").toString()), req.getRequestURL().toString()))
            {
                System.out.println("access error");
                goToSessTimeOut = true;
            }
        }

        System.out.println("req.getServletPath() "+req.getServletPath()+ " reg.protocol "+ req.getProtocol());
        if(goToSessTimeOut){
            String strRedirect = "/SessionTimedOut.jsp";
            String strReq = req.getRequestURL().toString();
            if (strReq.indexOf("localhost")  > 0)
            {
              strReq = strReq.replace("http", "https");
              strReq = strReq.replace("httpss", "https");
              res.sendRedirect(req.getContextPath()+strRedirect);
            }else{
                res.sendRedirect(strRedirect);
            }
         }else{
           // System.out.println("req "+req.getHeader("referer"));
            String extention = req.getRequestURL().toString();
            System.out.println(extention);
            if(extention.lastIndexOf(".") > 0){
                extention = extention.substring(extention.lastIndexOf("."), extention.length());
                //if(extention.indexOf("?") > 0){
                //   extention = extention.substring(0, extention.indexOf("?"));
                //}
            }
           // System.out.println("req.getRequestURL().indexOf(192.168.100.152) "+req.getRequestURL().indexOf("192.168.100.152")+" req.getRequestURL().indexOf(inhouse.eprocure.gov.bd) "+req.getRequestURL().indexOf("inhouse.eprocure.gov.bd")+ " req.getRequestURL().indexOf(localhost) "+req.getRequestURL().indexOf("localhost"));

            if(isValidExtension(extention)){
                if(
                    //req.getRequestURL().indexOf("CompanyDetails.jsp") > 0
                    //|| req.getRequestURL().indexOf("InsertCompanyDetails.jsp") > 0
                    //|| req.getRequestURL().indexOf("PersonalDetails.jsp") > 0
                    //|| req.getRequestURL().indexOf("SupportingDocuments.jsp") > 0
                    //|| req.getRequestURL().indexOf("IndividualConsultant.jsp") > 0
                    //|| req.getRequestURL().indexOf("staging.eprocure.gov.bd") > 0
                    //|| req.getRequestURL().indexOf("training.eprocure.gov.bd") > 0
                    //||
                    req.getRequestURL().indexOf("development.eprocure.gov.bd") > 0
                    || req.getRequestURL().indexOf(":8090") > 0
                    || req.getRequestURL().indexOf(":8080") > 0
                    || req.getRequestURL().indexOf("darshn.eprocure.com") > 0
                    || (req.getRequestURL().indexOf("Logout.jsp") > 0 &&
                            (req.getRequestURL().indexOf("192.168.100.152") == -1 && req.getRequestURL().indexOf("122.170.119.66") == -1 && req.getRequestURL().indexOf("inhouse.eprocure.gov.bd")  == -1 && req.getRequestURL().indexOf("localhost")  == -1)
                        )
                    || (req.getRequestURL().indexOf("FinalSubmission.jsp") > 0 &&
                        req.getRequestURL().indexOf("tender/FinalSubmission.jsp") == -1 &&
                        (req.getRequestURL().indexOf("192.168.100.152:8090") > 0 || req.getRequestURL().indexOf("122.170.119.66:8090") > 0))
                ){
                    System.out.println("FOR NUR");
                    chain.doFilter(req, res);
                }else{
                    chain.doFilter(req, new SendRedirectOverloadedResponse(req, res));
                }
            }else{
                chain.doFilter(req, res);
            }
        }
    }

    public void destroy() {
    }

    private boolean isValidExtension(String ext){
        boolean bool = false;
        if(ext.equals(".js") || ext.equals(".png") || ext.equals(".gif") || ext.equals(".css")){
            //System.out.println(ext + "FALSE");
            bool = false;
        }else{
            //System.out.println(ext + "TRUE");
            bool = true;
        }
        return bool;
    }

    private boolean isAccess(int userTypeId, String reqURL)
    {
       System.out.println("userTypeId "+userTypeId+" reqURL "+reqURL+" officer "+reqURL.indexOf("/officer/") +" tenderer "+reqURL.indexOf("/tenderer/")+" admin "+reqURL.indexOf("/admin/")+ " partner "+reqURL.indexOf("/partner/"));
       if(reqURL.indexOf("/resources/") > 0  || reqURL.indexOf("/help/") > 0 || reqURL.contains("Servlet") || reqURL.contains("ChangePassword.jsp") || reqURL.contains("UserPreference.jsp") || reqURL.contains("ChangeHintQusAns.jsp") || reqURL.contains("Signer.jar"))
        {
            System.out.println("common reqURL " +reqURL);
            return true;
        }
        else if((userTypeId == 1 || userTypeId == 3 || userTypeId == 4 || userTypeId == 5 || userTypeId == 6 || userTypeId == 7 || userTypeId == 8 || userTypeId == 12 || userTypeId == 14 || userTypeId == 15) && (reqURL.indexOf("/tenderer/")  > 0 && reqURL.indexOf("/ViewRegistrationDetail.jsp") == -1 && reqURL.indexOf("/ViewCompanyDetail.jsp") == -1 && reqURL.indexOf("/ViewPersonalDetail.jsp") == -1 && reqURL.indexOf("/CommonDocLib.jsp") == -1) )
         {
            System.out.println("not user 1 reqURL " +reqURL);
            return false;
        }
        else if(userTypeId == 2)
        {
            System.out.println("user 1 reqURL " +reqURL);
            if((reqURL.indexOf("/officer/") > 0 ||
                    reqURL.indexOf("/partner/") > 0 &&
                    (reqURL.indexOf("/ForViewTenderPayment.jsp") == -1 && reqURL.indexOf("/ViewTenderPaymentDetails.jsp") == -1 && reqURL.indexOf("/LotPayment.jsp") == -1)
                    ) ||
                    (reqURL.indexOf("/admin/") > 0 )||
                    (reqURL.indexOf("/report/") > 0 &&
                        (reqURL.indexOf("/TOR1.jsp") == -1 && reqURL.indexOf("/TOR2.jsp") == -1)
                    )
                )
            {
                return false;
            }
        }
        return true;
    }
}
*/
