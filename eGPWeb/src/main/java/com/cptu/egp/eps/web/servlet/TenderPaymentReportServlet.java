/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.web.servicebean.GenreatePdfCmd;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.GeneratePdfConstant;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author shreyansh
 */
public class TenderPaymentReportServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    MakeAuditTrailService auditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String strPageNo = request.getParameter("pageNo");
            int pageNo = Integer.parseInt(strPageNo);

            String strOffset = request.getParameter("size");
            int recordOffset = Integer.parseInt(strOffset);
            String styleClass = "";
            List<SPCommonSearchDataMore> list = findTenderReport(request);
            if (list != null && !list.isEmpty()) {
                String link = "";

                for (int i = 0; i < list.size(); i++) {
                    if (i % 2 == 0) {
                        styleClass = "bgColor-white";
                    } else {
                        styleClass = "bgColor-Green";
                    }
                    out.print("<tr class='" + styleClass + "'>");
                    out.print("<td class=\"t-align-center\">" + list.get(i).getFieldName19() + "</td>");
                    out.print("<td class=\"t-align-center\">" + list.get(i).getFieldName1() + ",<br />" + list.get(i).getFieldName2() + ",<br />" + list.get(i).getFieldName3() + "</td>");
                    out.print("<td class=\"t-align-center\">" + list.get(i).getFieldName7() + ",<br />" + list.get(i).getFieldName6() + ",<br />" + list.get(i).getFieldName5() + ",<br />" + list.get(i).getFieldName4() + "</td>");
                    //System.out.println("list.get(i).getFieldName9() "+list.get(i).getFieldName9());
                    if(list.get(i).getFieldName9().trim().equalsIgnoreCase("Online")){
                        out.print("<td class=\"t-align-center\">" + list.get(i).getFieldName8() + "</td>");
                    }else{
                        out.print("<td class=\"t-align-center\">" + list.get(i).getFieldName8() + ",<br />" + list.get(i).getFieldName9() + "</td>");
                    }
                    out.print("<td class=\"t-align-right\" style=\"text-align: right;\">" + list.get(i).getFieldName10() + "</td>");
                    out.print("<td class=\"t-align-center\">" + list.get(i).getFieldName11() + "</td>");
                    if(list.get(i).getFieldName9().trim().equalsIgnoreCase("Online")){
                             out.print("<td class=\"t-align-center\">" + list.get(i).getFieldName9() + "</td>");
                    }else{
                              out.print("<td class=\"t-align-center\">Offline</td>");
                    }
                    out.print("<td class=\"t-align-left\">" + list.get(i).getFieldName12() + "</td>");
                    out.print("<td class=\"t-align-center noprint\"><a target=\"_blank\" href=\""+request.getContextPath()+"/admin/ViewTenderPaymentDetails.jsp?payId="+list.get(i).getFieldName13()+"&uId="+list.get(i).getFieldName17()+"&tenderId="+list.get(i).getFieldName1()+"&lotId="+list.get(i).getFieldName14()+"&payTyp="+list.get(i).getFieldName18()+"\">View Details</a></td>");
                    out.print("</tr>");
                }
                int totalPages = 1;

                if (list.size() > 0) {
                    int count = Integer.parseInt(list.get(0).getFieldName20());
                    totalPages = (int) Math.ceil(Math.ceil(count) / recordOffset);
                    if (totalPages == 0) {
                        totalPages = 1;
                    }
                }
                out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

            } else {
                out.print("<tr>");
                out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"9\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                out.print("</tr>");
            }



        } finally {
            out.close();
        }
    }

    private List<SPCommonSearchDataMore> findTenderReport(HttpServletRequest request) {
        String deptment = "";
        String cmbOffice = "";
        String bankName = "";
        String amount = "";
        String cmbamount = "";
        String DateTO = "";
        String DateFrom = "";
        String paymentfor = "";
        String tenderId= "";
        String[] finalDateto;
        String[] finalDateFrom;
        String branchName = "";
        String paymentMode = "All";
        if (request.getParameter("txtdepartmentid") != null) {
            deptment = request.getParameter("txtdepartmentid");
        }
        if (request.getParameter("paymentMode") != null) {
            paymentMode = request.getParameter("paymentMode");
        }
        if (request.getParameter("cmbOffice") != null) {
            cmbOffice = request.getParameter("cmbOffice");
        }
        if (request.getParameter("bankName") != null) {
            bankName = request.getParameter("bankName");
        }
        if (request.getParameter("amount") != null) {
            amount = request.getParameter("amount");
        }
        if (request.getParameter("cmbamount") != null) {
            cmbamount = request.getParameter("cmbamount");
        }
        if (request.getParameter("DateTO") != null) {
            DateTO = request.getParameter("DateTO");
        }
        if (request.getParameter("DateFrom") != null) {
            DateFrom = request.getParameter("DateFrom");
        }
        if (request.getParameter("paymentfor") != null) {
            paymentfor = request.getParameter("paymentfor");
        }
        if (request.getParameter("tenderid") != null) {
            tenderId = request.getParameter("tenderid");
        }
        if (request.getParameter("bankName") != null) {
            bankName = request.getParameter("bankName");
        }
        if (request.getParameter("branchName") != null) {
            branchName = request.getParameter("branchName");
        }
        String strPageNo = request.getParameter("pageNo");
        int pageNo = Integer.parseInt(strPageNo);
        if(DateFrom!=""){
        finalDateFrom = DateFrom.split("/");
        DateFrom = finalDateFrom[2]+"-"+finalDateFrom[1]+"-"+finalDateFrom[0];
        }
        if(DateTO!=""){
        finalDateto = DateTO.split("/");
        DateTO = finalDateto[2]+"-"+finalDateto[1]+"-"+finalDateto[0];
        }
        String strOffset = request.getParameter("size");
        int recordOffset = Integer.parseInt(strOffset);
        auditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), Integer.parseInt(request.getSession().getAttribute("userId").toString()), "userId", EgpModule.Report.getName(), "Generate Tender Payment Report", "");
        CommonSearchDataMoreService csdms = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
        List<SPCommonSearchDataMore> list = csdms.geteGPData("getTenderPaymentLisForAdmin", strPageNo, strOffset, deptment, cmbOffice, bankName, cmbamount, amount, DateTO, DateFrom, paymentfor, tenderId, branchName, paymentMode, null, null, null, null, null);
        String userId = String.valueOf(request.getSession().getAttribute("userId"));
        GenreatePdfCmd obj = new GenreatePdfCmd();
        GeneratePdfConstant constant = new GeneratePdfConstant();
        String cdate = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date(new Date().getTime()));
        String folderName = constant.TenderPayment;
        String genId = cdate + "_" + userId;
        String reqURL = request.getRequestURL().toString();
        int userTypeID = Integer.parseInt(request.getSession().getAttribute("userTypeId").toString());
        if(userTypeID== 1 || userTypeID== 8) {
            strOffset = "0";
        }
        reqURL = reqURL.replace("TenderPaymentReportServlet", "report/TenderPaymentPdf.jsp");
        String reqQuery = "strPageNo=" + strPageNo + "&txtdepartmentid="+deptment+"&cmbOffice="+cmbOffice+"&bankName="+bankName+"&amount="+amount+"&strOffset=" + strOffset + "&paymentfor=" + paymentfor.split(" ")[0] + "&DateFrom=" + DateFrom + "&DateTO=" + DateTO+"&tenderid="+tenderId+"&branchName="+branchName+"&paymentMode="+paymentMode;
        obj.genrateCmd(reqURL, reqQuery, folderName, genId);


        return list;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
