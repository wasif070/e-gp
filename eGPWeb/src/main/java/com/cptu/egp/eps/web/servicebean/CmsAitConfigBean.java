/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.CommonAppData;
import com.cptu.egp.eps.model.table.TblCmsAitConfig;
import com.cptu.egp.eps.service.serviceinterface.CmsAitConfigurationService;
import com.cptu.egp.eps.web.servlet.APPServlet;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Sreenu.Durga
 */
public class CmsAitConfigBean {

    final Logger logger = Logger.getLogger(CmsAitConfigBean.class);
    private final static CmsAitConfigurationService cmsAitConfigurationService =
            (CmsAitConfigurationService) AppContext.getSpringBean("CmsAitConfigurationService");
    private String logUserId = "0";
    private static final String LOGGERStart = " Starts";
    private static final String LOGGEREnd = " Ends";
    private static final int ZERO = 0;
    private static final String SPACE = " : ";

    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        cmsAitConfigurationService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /***
     * This method inserts a TblCmsAitConfig object in database
     * @param tblCmsAitConfig
     * @return int
     */
    public int insertCmsAitConfiguration(TblCmsAitConfig tblCmsAitConfig) {
        logger.debug("insertCmsAitConfiguration : " + logUserId + LOGGERStart);
        int id = 0;
        try {
            id = cmsAitConfigurationService.insertCmsAitConfig(tblCmsAitConfig);
        } catch (Exception e) {
            logger.error("insertCmsAitConfiguration : " + logUserId + SPACE + e);
        }
        logger.debug("insertCmsAitConfiguration : " + logUserId + LOGGEREnd);
        return id;
    }

    /****
     * This method updates an TblCmsAitConfig object in DB
     * @param tblCmsAitConfig
     * @return boolean
     */
    public boolean updateCmsAitConfiguration(TblCmsAitConfig tblCmsAitConfig) {
        logger.debug("updateCmsAitConfiguration : " + logUserId + LOGGERStart);
        boolean isUpdate = false;
        try {
            isUpdate = cmsAitConfigurationService.updateCmsAitConfig(tblCmsAitConfig);
        } catch (Exception e) {
            logger.error("updateCmsAitConfiguration : " + logUserId + SPACE + e);
        }
        logger.debug("updateCmsAitConfiguration : " + logUserId + LOGGEREnd);
        return isUpdate;
    }

    /****
     * This method deletes an TblCmsAitConfig object in DB
     * @param tblCmsAitConfig
     * @return boolean
     */
    public boolean deleteCmsAitConfiguration(TblCmsAitConfig tblCmsAitConfig) {
        logger.debug("deleteCmsAitConfiguration : " + logUserId + LOGGERStart);
        boolean isDeleted = false;
        try {
            isDeleted = cmsAitConfigurationService.deleteCmsAitConfig(tblCmsAitConfig);
        } catch (Exception e) {
            logger.error("deleteCmsAitConfiguration : " + logUserId + SPACE + e);
        }
        logger.debug("deleteCmsAitConfiguration : " + logUserId + LOGGEREnd);
        return isDeleted;
    }

    /****
     * This method returns all TblCmsAitConfig objects from database
     * @return List<TblCmsAitConfig>
     */
    public List<TblCmsAitConfig> getAllCmsAitConfiguration() {
        logger.debug("getAllCmsAitConfiguration : " + logUserId + LOGGERStart);
        List<TblCmsAitConfig> cmsAitConfigList = new ArrayList<TblCmsAitConfig>();
        try {
            cmsAitConfigList = cmsAitConfigurationService.getAllCmsAitConfig();
        } catch (Exception e) {
            logger.error("getAllCmsAitConfiguration : " + logUserId + SPACE + e);
        }
        logger.debug("getAllCmsAitConfiguration : " + logUserId + LOGGEREnd);
        return cmsAitConfigList;
    }

    /***
     *  This method returns no. of TblCmsAitConfig objects from database
     * @return long
     */
    public long getCmsAitConfigurationCount() {
        logger.debug("getCmsAitConfigurationCount : " + logUserId + LOGGERStart);
        long count = 0;
        try {
            count = cmsAitConfigurationService.getCmsAitConfigCount();
        } catch (Exception e) {
            logger.error("getCmsAitConfigurationCount : " + logUserId + SPACE + e);
        }
        logger.debug("getCmsAitConfigurationCount : " + logUserId + LOGGEREnd);
        return count;
    }

    /***
     * This method returns TblCmsAitConfig for the given Id
     * @param int id
     * @return TblCmsAitConfig
     */
    public TblCmsAitConfig getCmsAitConfiguration(int id) {
        logger.debug("insertCmsAitConfig : " + logUserId + LOGGERStart);
        TblCmsAitConfig tblCmsAitConfig = new TblCmsAitConfig();
        try {
            tblCmsAitConfig = cmsAitConfigurationService.getCmsAitConfig(id);
        } catch (Exception e) {
            logger.error("getCmsAitConfiguration : " + logUserId + SPACE + e);
        }
        logger.debug("insertCmsAitConfig : " + logUserId + LOGGEREnd);
        return tblCmsAitConfig;
    }

    /***
     * This method gives the list of CmsAitConfig objects for the given financial year id
     * @param financialYearId
     * @return List<TblCmsAitConfig>
     */
    public List<TblCmsAitConfig> getCmsAitConfigForFianancialYear(int financialYearId) {
        logger.debug("getCmsAitConfigForFianancialYear : " + logUserId + LOGGERStart);
        List<TblCmsAitConfig> cmsAitConfigList = new ArrayList<TblCmsAitConfig>();
        try {
            cmsAitConfigList = cmsAitConfigurationService.getCmsAitConfigForFianancialYear(financialYearId);
        } catch (Exception e) {
            logger.error("getCmsAitConfigForFianancialYear : " + logUserId + SPACE + e);
        }
        logger.debug("getCmsAitConfigForFianancialYear : " + logUserId + LOGGEREnd);
        return cmsAitConfigList;
    }

    /***
     * This method gives the Ids of financial years which already used in the TblCmsAitConfig objects.
     * @return List<String>
     */
    public List<String> getCmsAitConfigFianacialYearsList() {
        logger.debug("getCmsAitConfigFianacialYearsList : " + logUserId + LOGGERStart);
        List<String> cmsAitConfigFianacialYearsList = new ArrayList<String>();
        try {
            cmsAitConfigFianacialYearsList = cmsAitConfigurationService.getCmsAitConfigFianacialYearsList();
        } catch (Exception e) {
            logger.error("getCmsAitConfigFianacialYearsList : " + logUserId + SPACE + e);
        }
        logger.debug("getCmsAitConfigFianacialYearsList : " + logUserId + LOGGEREnd);
        return cmsAitConfigFianacialYearsList;
    }

    /***
     * This method gives the all financial years.
     * @return List<CommonAppData>
     * @throws Exception
     */
    public List<CommonAppData> getAllFinancialYears() throws Exception {
        logger.debug("getAllFinancialYears : " + logUserId + LOGGERStart);
        APPServlet appServlet = new APPServlet();
        appServlet.setLogUserId(logUserId);
        List<CommonAppData> allFianancialYearsList = new ArrayList<CommonAppData>();
        allFianancialYearsList = appServlet.getAPPDetails("FinancialYear", "", "");
        logger.debug("getAllFinancialYears : " + logUserId + LOGGEREnd);
        return allFianancialYearsList;
    }

    /***
     * This method gives the financial years which the AIT not configured.
     * @return List<CommonAppData>
     * @throws Exception
     */
    public List<CommonAppData> getRemainingCmsAitConfigFinancialYears() throws Exception {
        logger.debug("getRemainingCmsAitConfigFinancialYears : " + logUserId + LOGGERStart);
        List<CommonAppData> allFianancialYearsList = getAllFinancialYears();
        List<String> financialYearsIdList = getCmsAitConfigFianacialYearsList();
        if (allFianancialYearsList != null && financialYearsIdList != null) {
            for (String financialYearId : financialYearsIdList) {
                for (CommonAppData commonAppData : allFianancialYearsList) {
                    if (commonAppData.getFieldName1().equalsIgnoreCase(financialYearId)) {
                        allFianancialYearsList.remove(commonAppData);
                        break;
                    }
                }// inner for loop
            }//outer for loop
        }
        logger.debug("getRemainingCmsAitConfigFinancialYears : " + logUserId + LOGGEREnd);
        return allFianancialYearsList;
    }

    /***
     * This method returns the Flat percentage of the given financial year id
     * @param financialYearId
     * @return double
     */
    public double getFlatPercentage(int financialYearId) {
        logger.debug("getFlatPercentage : " + logUserId + LOGGERStart);
        double flatPercentage = 0;
        List<TblCmsAitConfig> tblCmsAitConfigList = getCmsAitConfigForFianancialYear(financialYearId);
        if (tblCmsAitConfigList != null && !tblCmsAitConfigList.isEmpty()) {
            flatPercentage = tblCmsAitConfigList.get(ZERO).getFlatPercent().doubleValue();
        }
        logger.debug("getFlatPercentage : " + logUserId + LOGGEREnd);
        return flatPercentage;
    }
}
